﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorTarget
struct  SetAnimatorTarget_t2430133416  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// UnityEngine.AvatarTarget HutongGames.PlayMaker.Actions.SetAnimatorTarget::avatarTarget
	int32_t ___avatarTarget_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorTarget::targetNormalizedTime
	FsmFloat_t2134102846 * ___targetNormalizedTime_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorTarget::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorTarget::_animator
	Animator_t2776330603 * ____animator_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_avatarTarget_12() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___avatarTarget_12)); }
	inline int32_t get_avatarTarget_12() const { return ___avatarTarget_12; }
	inline int32_t* get_address_of_avatarTarget_12() { return &___avatarTarget_12; }
	inline void set_avatarTarget_12(int32_t value)
	{
		___avatarTarget_12 = value;
	}

	inline static int32_t get_offset_of_targetNormalizedTime_13() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___targetNormalizedTime_13)); }
	inline FsmFloat_t2134102846 * get_targetNormalizedTime_13() const { return ___targetNormalizedTime_13; }
	inline FsmFloat_t2134102846 ** get_address_of_targetNormalizedTime_13() { return &___targetNormalizedTime_13; }
	inline void set_targetNormalizedTime_13(FsmFloat_t2134102846 * value)
	{
		___targetNormalizedTime_13 = value;
		Il2CppCodeGenWriteBarrier(&___targetNormalizedTime_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2430133416, ____animator_15)); }
	inline Animator_t2776330603 * get__animator_15() const { return ____animator_15; }
	inline Animator_t2776330603 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t2776330603 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier(&____animator_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
