﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer
struct JTokenReferenceEqualityComparer_t3005068644;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Boolean Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::Equals(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JTokenReferenceEqualityComparer_Equals_m1457450161 (JTokenReferenceEqualityComparer_t3005068644 * __this, JToken_t3412245951 * ___x0, JToken_t3412245951 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::GetHashCode(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JTokenReferenceEqualityComparer_GetHashCode_m998156923 (JTokenReferenceEqualityComparer_t3005068644 * __this, JToken_t3412245951 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::.ctor()
extern "C"  void JTokenReferenceEqualityComparer__ctor_m1843785244 (JTokenReferenceEqualityComparer_t3005068644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JContainer/JTokenReferenceEqualityComparer::.cctor()
extern "C"  void JTokenReferenceEqualityComparer__cctor_m840671505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
