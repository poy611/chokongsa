﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies
struct WakeAllRigidBodies_t2407533206;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::.ctor()
extern "C"  void WakeAllRigidBodies__ctor_m2554575504 (WakeAllRigidBodies_t2407533206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::Reset()
extern "C"  void WakeAllRigidBodies_Reset_m201008445 (WakeAllRigidBodies_t2407533206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::OnEnter()
extern "C"  void WakeAllRigidBodies_OnEnter_m1912461927 (WakeAllRigidBodies_t2407533206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::OnUpdate()
extern "C"  void WakeAllRigidBodies_OnUpdate_m2585304252 (WakeAllRigidBodies_t2407533206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::DoWakeAll()
extern "C"  void WakeAllRigidBodies_DoWakeAll_m3223091968 (WakeAllRigidBodies_t2407533206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
