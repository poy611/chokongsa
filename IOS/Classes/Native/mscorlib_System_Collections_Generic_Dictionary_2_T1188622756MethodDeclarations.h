﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949107362MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.UInt32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2983156865(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1188622756 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3313727791_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.UInt32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3574265879(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1188622756 *, String_t*, uint32_t, const MethodInfo*))Transform_1_Invoke_m893572393_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.UInt32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1822239170(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1188622756 *, String_t*, uint32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m842468564_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.UInt32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1418363923(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1188622756 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1647585217_gshared)(__this, ___result0, method)
