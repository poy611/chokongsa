﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ActivateGameObject
struct ActivateGameObject_t1848802188;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::.ctor()
extern "C"  void ActivateGameObject__ctor_m31587930 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::Reset()
extern "C"  void ActivateGameObject_Reset_m1972988167 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnEnter()
extern "C"  void ActivateGameObject_OnEnter_m3977925553 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnUpdate()
extern "C"  void ActivateGameObject_OnUpdate_m2190167218 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnExit()
extern "C"  void ActivateGameObject_OnExit_m2492544455 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::DoActivateGameObject()
extern "C"  void ActivateGameObject_DoActivateGameObject_m3099810041 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::SetActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern "C"  void ActivateGameObject_SetActiveRecursively_m1268422564 (ActivateGameObject_t1848802188 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
