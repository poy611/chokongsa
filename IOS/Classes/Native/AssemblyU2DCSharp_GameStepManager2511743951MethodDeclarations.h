﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameStepManager
struct GameStepManager_t2511743951;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameStepManager::.ctor()
extern "C"  void GameStepManager__ctor_m647263356 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameStepManager::getNextSceneNum(System.Int32,System.Int32)
extern "C"  int32_t GameStepManager_getNextSceneNum_m3324103515 (GameStepManager_t2511743951 * __this, int32_t ___menu0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::FirstStart()
extern "C"  String_t* GameStepManager_FirstStart_m951957463 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::ResultStart()
extern "C"  String_t* GameStepManager_ResultStart_m2072049346 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::MinigameStart()
extern "C"  String_t* GameStepManager_MinigameStart_m1831451030 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::LearnStart()
extern "C"  String_t* GameStepManager_LearnStart_m3605581251 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::ResultPageStart()
extern "C"  String_t* GameStepManager_ResultPageStart_m1461384627 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::GetNextScene(System.Int32)
extern "C"  String_t* GameStepManager_GetNextScene_m3994849017 (GameStepManager_t2511743951 * __this, int32_t ___gameId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::Retry()
extern "C"  String_t* GameStepManager_Retry_m130167653 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::RetryLater()
extern "C"  String_t* GameStepManager_RetryLater_m193010825 (GameStepManager_t2511743951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameStepManager::getSceneName(System.Int32)
extern "C"  String_t* GameStepManager_getSceneName_m1675115063 (GameStepManager_t2511743951 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
