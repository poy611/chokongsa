﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey96
struct U3CAcknowledgeFinishedU3Ec__AnonStorey96_t1805473156;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey96::.ctor()
extern "C"  void U3CAcknowledgeFinishedU3Ec__AnonStorey96__ctor_m3157565847 (U3CAcknowledgeFinishedU3Ec__AnonStorey96_t1805473156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey96::<>m__85(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CAcknowledgeFinishedU3Ec__AnonStorey96_U3CU3Em__85_m4078902284 (U3CAcknowledgeFinishedU3Ec__AnonStorey96_t1805473156 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey96::<>m__8F(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void U3CAcknowledgeFinishedU3Ec__AnonStorey96_U3CU3Em__8F_m2633441577 (U3CAcknowledgeFinishedU3Ec__AnonStorey96_t1805473156 * __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
