﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/<SaveResultDataCouroutine>c__Iterator14
struct U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkMng/<SaveResultDataCouroutine>c__Iterator14::.ctor()
extern "C"  void U3CSaveResultDataCouroutineU3Ec__Iterator14__ctor_m16716129 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<SaveResultDataCouroutine>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveResultDataCouroutineU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1505619931 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<SaveResultDataCouroutine>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveResultDataCouroutineU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m1917389167 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkMng/<SaveResultDataCouroutine>c__Iterator14::MoveNext()
extern "C"  bool U3CSaveResultDataCouroutineU3Ec__Iterator14_MoveNext_m1766229979 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<SaveResultDataCouroutine>c__Iterator14::Dispose()
extern "C"  void U3CSaveResultDataCouroutineU3Ec__Iterator14_Dispose_m3077581150 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<SaveResultDataCouroutine>c__Iterator14::Reset()
extern "C"  void U3CSaveResultDataCouroutineU3Ec__Iterator14_Reset_m1958116366 (U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
