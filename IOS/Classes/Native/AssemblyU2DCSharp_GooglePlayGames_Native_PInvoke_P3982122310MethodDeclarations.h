﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator5_1_t3982122310;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1__ctor_m3214966157_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1__ctor_m3214966157(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1__ctor_m3214966157_gshared)(__this, method)
// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2450095124_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2450095124(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2450095124_gshared)(__this, method)
// System.Object GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m3069925113_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m3069925113(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m3069925113_gshared)(__this, method)
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerable_GetEnumerator_m3327423796_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerable_GetEnumerator_m3327423796(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerable_GetEnumerator_m3327423796_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4217469015_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4217469015(__this, method) ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4217469015_gshared)(__this, method)
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator5_1_MoveNext_m3635576647_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_MoveNext_m3635576647(__this, method) ((  bool (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_MoveNext_m3635576647_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1_Dispose_m1399274122_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_Dispose_m1399274122(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_Dispose_m1399274122_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::Reset()
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098_gshared)(__this, method)
