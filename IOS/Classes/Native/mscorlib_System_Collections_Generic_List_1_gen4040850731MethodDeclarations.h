﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t4040850731;
// System.Collections.Generic.IEnumerable`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerable_1_t1678610840;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t289562932;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<HutongGames.PlayMaker.ParamDataType>
struct ICollection_1_t3567255166;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>
struct ReadOnlyCollection_1_t4229742715;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t3909450202;
// System.Predicate`1<HutongGames.PlayMaker.ParamDataType>
struct Predicate_1_t2283722062;
// System.Collections.Generic.IComparer`1<HutongGames.PlayMaker.ParamDataType>
struct IComparer_1_t952711925;
// System.Comparison`1<HutongGames.PlayMaker.ParamDataType>
struct Comparison_1_t1389026366;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4060523501.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void List_1__ctor_m3523277126_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1__ctor_m3523277126(__this, method) ((  void (*) (List_1_t4040850731 *, const MethodInfo*))List_1__ctor_m3523277126_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4017152736_gshared (List_1_t4040850731 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4017152736(__this, ___collection0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4017152736_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1750475052_gshared (List_1_t4040850731 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1750475052(__this, ___capacity0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1__ctor_m1750475052_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.cctor()
extern "C"  void List_1__cctor_m2284337842_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2284337842(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2284337842_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1734731245_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1734731245(__this, method) ((  Il2CppObject* (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1734731245_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1610398025_gshared (List_1_t4040850731 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1610398025(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4040850731 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1610398025_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3453274628_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3453274628(__this, method) ((  Il2CppObject * (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3453274628_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3595089389_gshared (List_1_t4040850731 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3595089389(__this, ___item0, method) ((  int32_t (*) (List_1_t4040850731 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3595089389_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1660996991_gshared (List_1_t4040850731 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1660996991(__this, ___item0, method) ((  bool (*) (List_1_t4040850731 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1660996991_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m624045509_gshared (List_1_t4040850731 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m624045509(__this, ___item0, method) ((  int32_t (*) (List_1_t4040850731 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m624045509_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2461786480_gshared (List_1_t4040850731 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2461786480(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4040850731 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2461786480_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1127625144_gshared (List_1_t4040850731 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1127625144(__this, ___item0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1127625144_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1128606400_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1128606400(__this, method) ((  bool (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1128606400_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m438117757_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m438117757(__this, method) ((  bool (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m438117757_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2415209833_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2415209833(__this, method) ((  Il2CppObject * (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2415209833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2937341998_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2937341998(__this, method) ((  bool (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2937341998_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2765519115_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2765519115(__this, method) ((  bool (*) (List_1_t4040850731 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2765519115_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2799921328_gshared (List_1_t4040850731 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2799921328(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2799921328_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4158746951_gshared (List_1_t4040850731 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4158746951(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4040850731 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4158746951_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Add(T)
extern "C"  void List_1_Add_m3217637942_gshared (List_1_t4040850731 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3217637942(__this, ___item0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_Add_m3217637942_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3351679231_gshared (List_1_t4040850731 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3351679231(__this, ___newCount0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3351679231_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m668814728_gshared (List_1_t4040850731 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m668814728(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4040850731 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m668814728_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m274410493_gshared (List_1_t4040850731 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m274410493(__this, ___collection0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m274410493_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3830350013_gshared (List_1_t4040850731 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3830350013(__this, ___enumerable0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3830350013_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m602549178_gshared (List_1_t4040850731 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m602549178(__this, ___collection0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m602549178_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4229742715 * List_1_AsReadOnly_m2507616175_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2507616175(__this, method) ((  ReadOnlyCollection_1_t4229742715 * (*) (List_1_t4040850731 *, const MethodInfo*))List_1_AsReadOnly_m2507616175_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Clear()
extern "C"  void List_1_Clear_m929410417_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_Clear_m929410417(__this, method) ((  void (*) (List_1_t4040850731 *, const MethodInfo*))List_1_Clear_m929410417_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool List_1_Contains_m3121938484_gshared (List_1_t4040850731 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3121938484(__this, ___item0, method) ((  bool (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_Contains_m3121938484_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1632112131_gshared (List_1_t4040850731 * __this, ParamDataTypeU5BU5D_t3909450202* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1632112131(__this, ___array0, method) ((  void (*) (List_1_t4040850731 *, ParamDataTypeU5BU5D_t3909450202*, const MethodInfo*))List_1_CopyTo_m1632112131_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2843421620_gshared (List_1_t4040850731 * __this, ParamDataTypeU5BU5D_t3909450202* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2843421620(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4040850731 *, ParamDataTypeU5BU5D_t3909450202*, int32_t, const MethodInfo*))List_1_CopyTo_m2843421620_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m3149734132_gshared (List_1_t4040850731 * __this, Predicate_1_t2283722062 * ___match0, const MethodInfo* method);
#define List_1_Find_m3149734132(__this, ___match0, method) ((  int32_t (*) (List_1_t4040850731 *, Predicate_1_t2283722062 *, const MethodInfo*))List_1_Find_m3149734132_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1489690031_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2283722062 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1489690031(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2283722062 *, const MethodInfo*))List_1_CheckMatch_m1489690031_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1953426644_gshared (List_1_t4040850731 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2283722062 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1953426644(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4040850731 *, int32_t, int32_t, Predicate_1_t2283722062 *, const MethodInfo*))List_1_GetIndex_m1953426644_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Enumerator_t4060523501  List_1_GetEnumerator_m1669380977_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1669380977(__this, method) ((  Enumerator_t4060523501  (*) (List_1_t4040850731 *, const MethodInfo*))List_1_GetEnumerator_m1669380977_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4036740920_gshared (List_1_t4040850731 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4036740920(__this, ___item0, method) ((  int32_t (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_IndexOf_m4036740920_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m697699851_gshared (List_1_t4040850731 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m697699851(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4040850731 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m697699851_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2589788804_gshared (List_1_t4040850731 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2589788804(__this, ___index0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2589788804_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m328769323_gshared (List_1_t4040850731 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m328769323(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4040850731 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m328769323_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2242147168_gshared (List_1_t4040850731 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2242147168(__this, ___collection0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2242147168_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Remove(T)
extern "C"  bool List_1_Remove_m355745071_gshared (List_1_t4040850731 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m355745071(__this, ___item0, method) ((  bool (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_Remove_m355745071_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3228620027_gshared (List_1_t4040850731 * __this, Predicate_1_t2283722062 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3228620027(__this, ___match0, method) ((  int32_t (*) (List_1_t4040850731 *, Predicate_1_t2283722062 *, const MethodInfo*))List_1_RemoveAll_m3228620027_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2497589489_gshared (List_1_t4040850731 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2497589489(__this, ___index0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2497589489_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3288599316_gshared (List_1_t4040850731 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3288599316(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4040850731 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3288599316_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Reverse()
extern "C"  void List_1_Reverse_m4192888283_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_Reverse_m4192888283(__this, method) ((  void (*) (List_1_t4040850731 *, const MethodInfo*))List_1_Reverse_m4192888283_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Sort()
extern "C"  void List_1_Sort_m4081000359_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_Sort_m4081000359(__this, method) ((  void (*) (List_1_t4040850731 *, const MethodInfo*))List_1_Sort_m4081000359_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m599436829_gshared (List_1_t4040850731 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m599436829(__this, ___comparer0, method) ((  void (*) (List_1_t4040850731 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m599436829_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2070865082_gshared (List_1_t4040850731 * __this, Comparison_1_t1389026366 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2070865082(__this, ___comparison0, method) ((  void (*) (List_1_t4040850731 *, Comparison_1_t1389026366 *, const MethodInfo*))List_1_Sort_m2070865082_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::ToArray()
extern "C"  ParamDataTypeU5BU5D_t3909450202* List_1_ToArray_m2191401626_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_ToArray_m2191401626(__this, method) ((  ParamDataTypeU5BU5D_t3909450202* (*) (List_1_t4040850731 *, const MethodInfo*))List_1_ToArray_m2191401626_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2475402752_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2475402752(__this, method) ((  void (*) (List_1_t4040850731 *, const MethodInfo*))List_1_TrimExcess_m2475402752_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m43344744_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m43344744(__this, method) ((  int32_t (*) (List_1_t4040850731 *, const MethodInfo*))List_1_get_Capacity_m43344744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m561868049_gshared (List_1_t4040850731 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m561868049(__this, ___value0, method) ((  void (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_set_Capacity_m561868049_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t List_1_get_Count_m2382297912_gshared (List_1_t4040850731 * __this, const MethodInfo* method);
#define List_1_get_Count_m2382297912(__this, method) ((  int32_t (*) (List_1_t4040850731 *, const MethodInfo*))List_1_get_Count_m2382297912_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1582954333_gshared (List_1_t4040850731 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1582954333(__this, ___index0, method) ((  int32_t (*) (List_1_t4040850731 *, int32_t, const MethodInfo*))List_1_get_Item_m1582954333_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4224303426_gshared (List_1_t4040850731 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m4224303426(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4040850731 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m4224303426_gshared)(__this, ___index0, ___value1, method)
