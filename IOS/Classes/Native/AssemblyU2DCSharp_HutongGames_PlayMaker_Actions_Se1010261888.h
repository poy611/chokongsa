﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com715545838.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioLoop
struct  SetAudioLoop_t1010261888  : public ComponentAction_1_t715545838
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioLoop::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAudioLoop::loop
	FsmBool_t1075959796 * ___loop_14;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetAudioLoop_t1010261888, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_loop_14() { return static_cast<int32_t>(offsetof(SetAudioLoop_t1010261888, ___loop_14)); }
	inline FsmBool_t1075959796 * get_loop_14() const { return ___loop_14; }
	inline FsmBool_t1075959796 ** get_address_of_loop_14() { return &___loop_14; }
	inline void set_loop_14(FsmBool_t1075959796 * value)
	{
		___loop_14 = value;
		Il2CppCodeGenWriteBarrier(&___loop_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
