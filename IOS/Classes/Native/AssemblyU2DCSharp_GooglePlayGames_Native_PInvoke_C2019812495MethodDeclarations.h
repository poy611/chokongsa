﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m1914781182_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m1914781182(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m1914781182_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>::<>m__9D(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372(__this, ___result0, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 *, Il2CppObject *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372_gshared)(__this, ___result0, method)
