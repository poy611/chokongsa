﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55
struct U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177.h"

// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55::.ctor()
extern "C"  void U3CUnlockAchievementU3Ec__AnonStorey55__ctor_m1848535187 (U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55::<>m__2A(GooglePlayGames.BasicApi.Achievement)
extern "C"  void U3CUnlockAchievementU3Ec__AnonStorey55_U3CU3Em__2A_m2844202404 (U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848 * __this, Achievement_t1261647177 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
