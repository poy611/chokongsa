﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter
struct  SetAnimatorLayersAffectMassCenter_t1809925591  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::affectMassCenter
	FsmBool_t1075959796 * ___affectMassCenter_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t1809925591, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_affectMassCenter_12() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t1809925591, ___affectMassCenter_12)); }
	inline FsmBool_t1075959796 * get_affectMassCenter_12() const { return ___affectMassCenter_12; }
	inline FsmBool_t1075959796 ** get_address_of_affectMassCenter_12() { return &___affectMassCenter_12; }
	inline void set_affectMassCenter_12(FsmBool_t1075959796 * value)
	{
		___affectMassCenter_12 = value;
		Il2CppCodeGenWriteBarrier(&___affectMassCenter_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t1809925591, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
