﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserInfo
struct UserInfo_t4092807993;

#include "codegen/il2cpp-codegen.h"

// System.Void UserInfo::.ctor()
extern "C"  void UserInfo__ctor_m3194749826 (UserInfo_t4092807993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
