﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartImageDisapear
struct StartImageDisapear_t2918831698;

#include "codegen/il2cpp-codegen.h"

// System.Void StartImageDisapear::.ctor()
extern "C"  void StartImageDisapear__ctor_m1986209417 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartImageDisapear::Awake()
extern "C"  void StartImageDisapear_Awake_m2223814636 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartImageDisapear::OnDisable()
extern "C"  void StartImageDisapear_OnDisable_m3119648368 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
