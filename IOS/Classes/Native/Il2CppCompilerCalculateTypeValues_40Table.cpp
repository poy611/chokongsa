﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PopupScoring_U3CMakeItSocreU3Ec_1921295241.h"
#include "AssemblyU2DCSharp_RecipeList1641733996.h"
#include "AssemblyU2DCSharp_RecipeUpDown1716098347.h"
#include "AssemblyU2DCSharp_RecipeUpDown_U3CGoUPU3Ec__Iterat3652911668.h"
#include "AssemblyU2DCSharp_RecipeUpDown_U3CGoDownU3Ec__Iter3243370524.h"
#include "AssemblyU2DCSharp_ResultOfGame222677254.h"
#include "AssemblyU2DCSharp_ResultOfGame_ResultState1900874443.h"
#include "AssemblyU2DCSharp_ResultOfGame_U3CokNetU3Ec__Itera3669382025.h"
#include "AssemblyU2DCSharp_ResultPageManager3629511073.h"
#include "AssemblyU2DCSharp_ScoreManagerMiniGame3434204900.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding1076868946.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding_U3Cconfirm3696604900.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing3849909252.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CFinishU3Ec1048149352.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimerCheck3854228761.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimeEndU3Ec664746339.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby3853030226.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_menu3396474364.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimerCheck3376731881.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimeEndU3Ec583418455.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CwaitU3Ec__1632431941.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CRSU3Ec__It4268541650.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping3029572618.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CREandStU3438786049.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimerChec859433390.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimeEndU3910199980.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation1665153935.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CCheckIng55506035.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimerC3278601462.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimeEn1567663498.h"
#include "AssemblyU2DCSharp_Serving3648806892.h"
#include "AssemblyU2DCSharp_Serving_U3ConStartU3Ec__Iterator3665343203.h"
#include "AssemblyU2DCSharp_Serving_U3CShowDataU3Ec__Iterato2136060378.h"
#include "AssemblyU2DCSharp_Serving_U3CShowmetheCoffeSizeUpC3955755680.h"
#include "AssemblyU2DCSharp_Serving_U3CokNetU3Ec__Iterator2B599730315.h"
#include "AssemblyU2DCSharp_ShowRecipe2733639435.h"
#include "AssemblyU2DCSharp_ShowRecipePre977589144.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CenableCoruU3Ec_1488953467.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CFocusDownU3Ec__I659480186.h"
#include "AssemblyU2DCSharp_SoundManage3403985716.h"
#include "AssemblyU2DCSharp_StartFirst396912526.h"
#include "AssemblyU2DCSharp_StartFirst_U3CBlackBlinkU3Ec__It2642540702.h"
#include "AssemblyU2DCSharp_SFM82010.h"
#include "AssemblyU2DCSharp_StructforMinigame1286533533.h"
#include "AssemblyU2DCSharp_TempingLastObj3031672627.h"
#include "AssemblyU2DCSharp_TempingPointer2678149871.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CMainCameraMove3998083026.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideUp3867951914.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideDo3382326578.h"
#include "AssemblyU2DCSharp_TimerPref2057114344.h"
#include "AssemblyU2DCSharp_ErrorLog1460740156.h"
#include "AssemblyU2DCSharp_GiveScore3448718817.h"
#include "AssemblyU2DCSharp_MasterIO348555048.h"
#include "AssemblyU2DCSharp_StructforPopup2315336792.h"
#include "AssemblyU2DCSharp_PopupManager2711269761.h"
#include "AssemblyU2DCSharp_PopupManager_popupIndex3504285908.h"
#include "AssemblyU2DCSharp_ResultWindowEnable999673456.h"
#include "AssemblyU2DCSharp_ResultWindowEnable_U3CWaitTempU33760889752.h"
#include "AssemblyU2DCSharp_SceneindexInc2309712408.h"
#include "AssemblyU2DCSharp_StartImageBGMS2059898468.h"
#include "AssemblyU2DCSharp_StartImageDisapear2918831698.h"
#include "AssemblyU2DCSharp_VariationResultWindow1568540672.h"
#include "AssemblyU2DCSharp_YoutubeStrim2074917330.h"
#include "AssemblyU2DCSharp_JsonNetSample2710899071.h"
#include "AssemblyU2DCSharp_JsonNetSample_Product2509028415.h"
#include "AssemblyU2DCSharp_JsonNetSample_Movie147243648.h"
#include "AssemblyU2DCSharp_GetVideo2032672485.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture1195908624.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture_U3CCheck4170198610.h"
#include "AssemblyU2DCSharp_YoutubeVideo2077346616.h"
#include "AssemblyU2DCSharp_start_mini_game3518442301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Invitat2409308905.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl1377674964.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGam942269343.h"
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress3967815895.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1676615736.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220377.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220352.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (U3CMakeItSocreU3Ec__Iterator16_t1921295241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4000[3] = 
{
	U3CMakeItSocreU3Ec__Iterator16_t1921295241::get_offset_of_U24PC_0(),
	U3CMakeItSocreU3Ec__Iterator16_t1921295241::get_offset_of_U24current_1(),
	U3CMakeItSocreU3Ec__Iterator16_t1921295241::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (RecipeList_t1641733996), -1, sizeof(RecipeList_t1641733996_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4001[11] = 
{
	RecipeList_t1641733996_StaticFields::get_offset_of_instance_2(),
	RecipeList_t1641733996::get_offset_of_coffeeImgList_3(),
	RecipeList_t1641733996::get_offset_of_ingredient_4(),
	RecipeList_t1641733996::get_offset_of_recipeList_5(),
	RecipeList_t1641733996::get_offset_of_lobbyRecipe_6(),
	RecipeList_t1641733996::get_offset_of_coffeeBeanRecipe_7(),
	RecipeList_t1641733996::get_offset_of_extractionRecipe_8(),
	RecipeList_t1641733996::get_offset_of_steamMilkRecipe_9(),
	RecipeList_t1641733996::get_offset_of_iceMixerRecipe_10(),
	RecipeList_t1641733996::get_offset_of_variationRecipe_11(),
	RecipeList_t1641733996::get_offset_of_randomValueByRecipe_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (RecipeUpDown_t1716098347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4002[6] = 
{
	RecipeUpDown_t1716098347::get_offset_of_obj_2(),
	RecipeUpDown_t1716098347::get_offset_of_pos1_3(),
	RecipeUpDown_t1716098347::get_offset_of_pos2_4(),
	RecipeUpDown_t1716098347::get_offset_of_buttonImage_5(),
	RecipeUpDown_t1716098347::get_offset_of__btImg_6(),
	RecipeUpDown_t1716098347::get_offset_of_isUp_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (U3CGoUPU3Ec__Iterator17_t3652911668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4003[3] = 
{
	U3CGoUPU3Ec__Iterator17_t3652911668::get_offset_of_U24PC_0(),
	U3CGoUPU3Ec__Iterator17_t3652911668::get_offset_of_U24current_1(),
	U3CGoUPU3Ec__Iterator17_t3652911668::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (U3CGoDownU3Ec__Iterator18_t3243370524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4004[3] = 
{
	U3CGoDownU3Ec__Iterator18_t3243370524::get_offset_of_U24PC_0(),
	U3CGoDownU3Ec__Iterator18_t3243370524::get_offset_of_U24current_1(),
	U3CGoDownU3Ec__Iterator18_t3243370524::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (ResultOfGame_t222677254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[9] = 
{
	ResultOfGame_t222677254::get_offset_of_reverse_2(),
	ResultOfGame_t222677254::get_offset_of_totalScore_3(),
	ResultOfGame_t222677254::get_offset_of_tempArray_4(),
	ResultOfGame_t222677254::get_offset_of_countArray_5(),
	ResultOfGame_t222677254::get_offset_of__gameImg_6(),
	ResultOfGame_t222677254::get_offset_of_okClick_7(),
	ResultOfGame_t222677254::get_offset_of_pop_8(),
	ResultOfGame_t222677254::get_offset_of_resultPref_9(),
	ResultOfGame_t222677254::get_offset_of_state_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (ResultState_t1900874443)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4006[3] = 
{
	ResultState_t1900874443::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (U3CokNetU3Ec__Iterator19_t3669382025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4007[3] = 
{
	U3CokNetU3Ec__Iterator19_t3669382025::get_offset_of_U24PC_0(),
	U3CokNetU3Ec__Iterator19_t3669382025::get_offset_of_U24current_1(),
	U3CokNetU3Ec__Iterator19_t3669382025::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (ResultPageManager_t3629511073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4008[10] = 
{
	ResultPageManager_t3629511073::get_offset_of_scores_2(),
	ResultPageManager_t3629511073::get_offset_of_texts_3(),
	ResultPageManager_t3629511073::get_offset_of_pageText_4(),
	ResultPageManager_t3629511073::get_offset_of_prev_5(),
	ResultPageManager_t3629511073::get_offset_of_next_6(),
	ResultPageManager_t3629511073::get_offset_of_currentPage_7(),
	ResultPageManager_t3629511073::get_offset_of_LastPage_8(),
	ResultPageManager_t3629511073::get_offset_of_trialFirstIndex_9(),
	ResultPageManager_t3629511073::get_offset_of_reverse_10(),
	ResultPageManager_t3629511073::get_offset_of_allData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (ScoreManagerMiniGame_t3434204900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4009[8] = 
{
	ScoreManagerMiniGame_t3434204900::get_offset_of_nowNum_2(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_nowScene_3(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_ticks_4(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_ready_5(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_stuff_6(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_cuff_7(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_pop_8(),
	ScoreManagerMiniGame_t3434204900::get_offset_of_sw_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (ScoreManager_Grinding_t1076868946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4010[4] = 
{
	ScoreManager_Grinding_t1076868946::get_offset_of_stuff_2(),
	ScoreManager_Grinding_t1076868946::get_offset_of_cuff_3(),
	ScoreManager_Grinding_t1076868946::get_offset_of_popup_4(),
	ScoreManager_Grinding_t1076868946::get_offset_of_nowNum_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (U3CconfirmU3Ec__Iterator1A_t3696604900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4011[10] = 
{
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CtimmerMinusU3E__0_0(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CerrorRateU3E__1_1(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CerrorMinusU3E__2_2(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3ClimitTimeU3E__3_3(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CgoalPointU3E__4_4(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CcurrentTimeU3E__5_5(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CgaugePointU3E__6_6(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U24PC_7(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U24current_8(),
	U3CconfirmU3Ec__Iterator1A_t3696604900::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (ScoreManager_Icing_t3849909252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4012[13] = 
{
	ScoreManager_Icing_t3849909252::get_offset_of_nowNum_2(),
	ScoreManager_Icing_t3849909252::get_offset_of_nowScene_3(),
	ScoreManager_Icing_t3849909252::get_offset_of_ticks_4(),
	ScoreManager_Icing_t3849909252::get_offset_of_timmer_5(),
	ScoreManager_Icing_t3849909252::get_offset_of_recipePre_6(),
	ScoreManager_Icing_t3849909252::get_offset_of_ready_7(),
	ScoreManager_Icing_t3849909252::get_offset_of_stuff_8(),
	ScoreManager_Icing_t3849909252::get_offset_of_cuff_9(),
	ScoreManager_Icing_t3849909252::get_offset_of_pop_10(),
	ScoreManager_Icing_t3849909252::get_offset_of_myFsm_11(),
	ScoreManager_Icing_t3849909252::get_offset_of_missCount_12(),
	ScoreManager_Icing_t3849909252::get_offset_of_currentIngredient_13(),
	ScoreManager_Icing_t3849909252::get_offset_of_sw_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (U3CFinishU3Ec__Iterator1B_t1048149352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4013[2] = 
{
	U3CFinishU3Ec__Iterator1B_t1048149352::get_offset_of_U24PC_0(),
	U3CFinishU3Ec__Iterator1B_t1048149352::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (U3CTimerCheckU3Ec__Iterator1C_t3854228761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4014[4] = 
{
	U3CTimerCheckU3Ec__Iterator1C_t3854228761::get_offset_of_U3CitU3E__0_0(),
	U3CTimerCheckU3Ec__Iterator1C_t3854228761::get_offset_of_U24PC_1(),
	U3CTimerCheckU3Ec__Iterator1C_t3854228761::get_offset_of_U24current_2(),
	U3CTimerCheckU3Ec__Iterator1C_t3854228761::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (U3CTimeEndU3Ec__Iterator1D_t664746339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4015[3] = 
{
	U3CTimeEndU3Ec__Iterator1D_t664746339::get_offset_of_U24PC_0(),
	U3CTimeEndU3Ec__Iterator1D_t664746339::get_offset_of_U24current_1(),
	U3CTimeEndU3Ec__Iterator1D_t664746339::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (ScoreManager_Lobby_t3853030226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4016[32] = 
{
	ScoreManager_Lobby_t3853030226::get_offset_of_nowNum_2(),
	ScoreManager_Lobby_t3853030226::get_offset_of_sw_3(),
	ScoreManager_Lobby_t3853030226::get_offset_of_ticks_4(),
	ScoreManager_Lobby_t3853030226::get_offset_of_stuff_5(),
	ScoreManager_Lobby_t3853030226::get_offset_of_cuff_6(),
	ScoreManager_Lobby_t3853030226::get_offset_of_getMenu_7(),
	ScoreManager_Lobby_t3853030226::get_offset_of_popup_8(),
	ScoreManager_Lobby_t3853030226::get_offset_of_timmer_9(),
	ScoreManager_Lobby_t3853030226::get_offset_of_MayItakeYourOrder_10(),
	ScoreManager_Lobby_t3853030226::get_offset_of_ShowmetheCoffee_11(),
	ScoreManager_Lobby_t3853030226::get_offset_of_desk_12(),
	ScoreManager_Lobby_t3853030226::get_offset_of_bts_13(),
	ScoreManager_Lobby_t3853030226::get_offset_of_imgs_14(),
	ScoreManager_Lobby_t3853030226::get_offset_of_Bars_15(),
	ScoreManager_Lobby_t3853030226::get_offset_of__Ready_16(),
	ScoreManager_Lobby_t3853030226::get_offset_of__Start_17(),
	ScoreManager_Lobby_t3853030226::get_offset_of_lobby_ani_18(),
	ScoreManager_Lobby_t3853030226::get_offset_of_lobby_desk_ani_19(),
	ScoreManager_Lobby_t3853030226::get_offset_of_currentMenuPage_20(),
	ScoreManager_Lobby_t3853030226::get_offset_of_prevButton_21(),
	ScoreManager_Lobby_t3853030226::get_offset_of_nextButton_22(),
	ScoreManager_Lobby_t3853030226::get_offset_of_MenuButton_23(),
	ScoreManager_Lobby_t3853030226::get_offset_of_creamButton_24(),
	ScoreManager_Lobby_t3853030226::get_offset_of_randomNum_25(),
	ScoreManager_Lobby_t3853030226::get_offset_of_randomMenu_26(),
	ScoreManager_Lobby_t3853030226::get_offset_of_randomHI_27(),
	ScoreManager_Lobby_t3853030226::get_offset_of_randomCream_28(),
	ScoreManager_Lobby_t3853030226::get_offset_of_counterBars_29(),
	ScoreManager_Lobby_t3853030226::get_offset_of_menuStr_30(),
	ScoreManager_Lobby_t3853030226::get_offset_of_hiStr_31(),
	ScoreManager_Lobby_t3853030226::get_offset_of_creamText_32(),
	ScoreManager_Lobby_t3853030226::get_offset_of_isOkDone_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (menu_t3396474364)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4017[20] = 
{
	menu_t3396474364::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (U3CTimerCheckU3Ec__Iterator1E_t3376731881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4018[4] = 
{
	U3CTimerCheckU3Ec__Iterator1E_t3376731881::get_offset_of_U3CitU3E__0_0(),
	U3CTimerCheckU3Ec__Iterator1E_t3376731881::get_offset_of_U24PC_1(),
	U3CTimerCheckU3Ec__Iterator1E_t3376731881::get_offset_of_U24current_2(),
	U3CTimerCheckU3Ec__Iterator1E_t3376731881::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (U3CTimeEndU3Ec__Iterator1F_t583418455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4019[3] = 
{
	U3CTimeEndU3Ec__Iterator1F_t583418455::get_offset_of_U24PC_0(),
	U3CTimeEndU3Ec__Iterator1F_t583418455::get_offset_of_U24current_1(),
	U3CTimeEndU3Ec__Iterator1F_t583418455::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (U3CwaitU3Ec__Iterator20_t1632431941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4020[3] = 
{
	U3CwaitU3Ec__Iterator20_t1632431941::get_offset_of_U24PC_0(),
	U3CwaitU3Ec__Iterator20_t1632431941::get_offset_of_U24current_1(),
	U3CwaitU3Ec__Iterator20_t1632431941::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (U3CRSU3Ec__Iterator21_t4268541650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4021[3] = 
{
	U3CRSU3Ec__Iterator21_t4268541650::get_offset_of_U24PC_0(),
	U3CRSU3Ec__Iterator21_t4268541650::get_offset_of_U24current_1(),
	U3CRSU3Ec__Iterator21_t4268541650::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (ScoreManager_Temping_t3029572618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4022[11] = 
{
	ScoreManager_Temping_t3029572618::get_offset_of_nowNum_2(),
	ScoreManager_Temping_t3029572618::get_offset_of_nowScene_3(),
	ScoreManager_Temping_t3029572618::get_offset_of_stuff_4(),
	ScoreManager_Temping_t3029572618::get_offset_of_cuff_5(),
	ScoreManager_Temping_t3029572618::get_offset_of_sw_6(),
	ScoreManager_Temping_t3029572618::get_offset_of_pop_7(),
	ScoreManager_Temping_t3029572618::get_offset_of_timmer_8(),
	ScoreManager_Temping_t3029572618::get_offset_of_res_9(),
	ScoreManager_Temping_t3029572618::get_offset_of_ticks_10(),
	ScoreManager_Temping_t3029572618::get_offset_of_tempingLimitAmount_11(),
	ScoreManager_Temping_t3029572618::get_offset_of_tempingAmount_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (U3CREandStU3Ec__Iterator22_t438786049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[4] = 
{
	U3CREandStU3Ec__Iterator22_t438786049::get_offset_of_U3C_objU3E__0_0(),
	U3CREandStU3Ec__Iterator22_t438786049::get_offset_of_U24PC_1(),
	U3CREandStU3Ec__Iterator22_t438786049::get_offset_of_U24current_2(),
	U3CREandStU3Ec__Iterator22_t438786049::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (U3CTimerCheckU3Ec__Iterator23_t859433390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[4] = 
{
	U3CTimerCheckU3Ec__Iterator23_t859433390::get_offset_of_U3CitU3E__0_0(),
	U3CTimerCheckU3Ec__Iterator23_t859433390::get_offset_of_U24PC_1(),
	U3CTimerCheckU3Ec__Iterator23_t859433390::get_offset_of_U24current_2(),
	U3CTimerCheckU3Ec__Iterator23_t859433390::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (U3CTimeEndU3Ec__Iterator24_t3910199980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4025[3] = 
{
	U3CTimeEndU3Ec__Iterator24_t3910199980::get_offset_of_U24PC_0(),
	U3CTimeEndU3Ec__Iterator24_t3910199980::get_offset_of_U24current_1(),
	U3CTimeEndU3Ec__Iterator24_t3910199980::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (ScoreManager_Variation_t1665153935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[14] = 
{
	ScoreManager_Variation_t1665153935::get_offset_of_nowNum_2(),
	ScoreManager_Variation_t1665153935::get_offset_of_nowScene_3(),
	ScoreManager_Variation_t1665153935::get_offset_of_ticks_4(),
	ScoreManager_Variation_t1665153935::get_offset_of_timmer_5(),
	ScoreManager_Variation_t1665153935::get_offset_of_recipePre_6(),
	ScoreManager_Variation_t1665153935::get_offset_of_ready_7(),
	ScoreManager_Variation_t1665153935::get_offset_of_buttonOk_8(),
	ScoreManager_Variation_t1665153935::get_offset_of_stuff_9(),
	ScoreManager_Variation_t1665153935::get_offset_of_cuff_10(),
	ScoreManager_Variation_t1665153935::get_offset_of_pop_11(),
	ScoreManager_Variation_t1665153935::get_offset_of_myFsm_12(),
	ScoreManager_Variation_t1665153935::get_offset_of_missCount_13(),
	ScoreManager_Variation_t1665153935::get_offset_of_currentIngredient_14(),
	ScoreManager_Variation_t1665153935::get_offset_of_sw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (U3CCheckIngredientU3Ec__Iterator25_t55506035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[3] = 
{
	U3CCheckIngredientU3Ec__Iterator25_t55506035::get_offset_of_U24PC_0(),
	U3CCheckIngredientU3Ec__Iterator25_t55506035::get_offset_of_U24current_1(),
	U3CCheckIngredientU3Ec__Iterator25_t55506035::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (U3CTimerCheckU3Ec__Iterator26_t3278601462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4028[4] = 
{
	U3CTimerCheckU3Ec__Iterator26_t3278601462::get_offset_of_U3CitU3E__0_0(),
	U3CTimerCheckU3Ec__Iterator26_t3278601462::get_offset_of_U24PC_1(),
	U3CTimerCheckU3Ec__Iterator26_t3278601462::get_offset_of_U24current_2(),
	U3CTimerCheckU3Ec__Iterator26_t3278601462::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (U3CTimeEndU3Ec__Iterator27_t1567663498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4029[3] = 
{
	U3CTimeEndU3Ec__Iterator27_t1567663498::get_offset_of_U24PC_0(),
	U3CTimeEndU3Ec__Iterator27_t1567663498::get_offset_of_U24current_1(),
	U3CTimeEndU3Ec__Iterator27_t1567663498::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (Serving_t3648806892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[21] = 
{
	Serving_t3648806892::get_offset_of_rightText_2(),
	Serving_t3648806892::get_offset_of_popup_3(),
	Serving_t3648806892::get_offset_of_MayItakeYourOrder_4(),
	Serving_t3648806892::get_offset_of_ShowmetheCoffee_5(),
	Serving_t3648806892::get_offset_of_desk_6(),
	Serving_t3648806892::get_offset_of_CoffeeImage1_7(),
	Serving_t3648806892::get_offset_of_CoffeeImage2_8(),
	Serving_t3648806892::get_offset_of_CreamImg1_9(),
	Serving_t3648806892::get_offset_of_CreamImg2_10(),
	Serving_t3648806892::get_offset_of_sum_11(),
	Serving_t3648806892::get_offset_of_cnt_12(),
	Serving_t3648806892::get_offset_of_imgTrans_13(),
	Serving_t3648806892::get_offset_of_pos2_14(),
	Serving_t3648806892::get_offset_of_popServing_15(),
	Serving_t3648806892::get_offset_of__teacher_16(),
	Serving_t3648806892::get_offset_of__scoreAlphabet_17(),
	Serving_t3648806892::get_offset_of__greatText_18(),
	Serving_t3648806892::get_offset_of_coffeeText_19(),
	Serving_t3648806892::get_offset_of_nowNum_20(),
	Serving_t3648806892::get_offset_of_reverse_21(),
	Serving_t3648806892::get_offset_of_okClick_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (U3ConStartU3Ec__Iterator28_t3665343203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[5] = 
{
	U3ConStartU3Ec__Iterator28_t3665343203::get_offset_of_U3CsourU3E__0_0(),
	U3ConStartU3Ec__Iterator28_t3665343203::get_offset_of_U3CsourceU3E__1_1(),
	U3ConStartU3Ec__Iterator28_t3665343203::get_offset_of_U24PC_2(),
	U3ConStartU3Ec__Iterator28_t3665343203::get_offset_of_U24current_3(),
	U3ConStartU3Ec__Iterator28_t3665343203::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (U3CShowDataU3Ec__Iterator29_t2136060378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4032[6] = 
{
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U3CsourceU3E__0_0(),
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U3CtempNumU3E__1_1(),
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U3CtempU3E__2_2(),
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U24PC_3(),
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U24current_4(),
	U3CShowDataU3Ec__Iterator29_t2136060378::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4033[5] = 
{
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680::get_offset_of_U3ClimitSizeU3E__0_0(),
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680::get_offset_of_U3CtempU3E__1_1(),
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680::get_offset_of_U24PC_2(),
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680::get_offset_of_U24current_3(),
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (U3CokNetU3Ec__Iterator2B_t599730315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[3] = 
{
	U3CokNetU3Ec__Iterator2B_t599730315::get_offset_of_U24PC_0(),
	U3CokNetU3Ec__Iterator2B_t599730315::get_offset_of_U24current_1(),
	U3CokNetU3Ec__Iterator2B_t599730315::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (ShowRecipe_t2733639435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[3] = 
{
	ShowRecipe_t2733639435::get_offset_of_menuTitle_2(),
	ShowRecipe_t2733639435::get_offset_of_recipe_3(),
	ShowRecipe_t2733639435::get_offset_of_goalPoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (ShowRecipePre_t977589144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4036[12] = 
{
	ShowRecipePre_t977589144::get_offset_of_menuName_2(),
	ShowRecipePre_t977589144::get_offset_of_recipes_3(),
	ShowRecipePre_t977589144::get_offset_of_coffeeImg_4(),
	ShowRecipePre_t977589144::get_offset_of_creamImg_5(),
	ShowRecipePre_t977589144::get_offset_of_recipeTrans_6(),
	ShowRecipePre_t977589144::get_offset_of_imgTrans_7(),
	ShowRecipePre_t977589144::get_offset_of_checks_8(),
	ShowRecipePre_t977589144::get_offset_of_UpDown_9(),
	ShowRecipePre_t977589144::get_offset_of_menuList_10(),
	ShowRecipePre_t977589144::get_offset_of_unitList_11(),
	ShowRecipePre_t977589144::get_offset_of_currentIngredient_12(),
	ShowRecipePre_t977589144::get_offset_of_nowScene_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (U3CenableCoruU3Ec__Iterator2C_t1488953467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4037[3] = 
{
	U3CenableCoruU3Ec__Iterator2C_t1488953467::get_offset_of_U24PC_0(),
	U3CenableCoruU3Ec__Iterator2C_t1488953467::get_offset_of_U24current_1(),
	U3CenableCoruU3Ec__Iterator2C_t1488953467::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (U3CFocusDownU3Ec__Iterator2D_t659480186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4038[3] = 
{
	U3CFocusDownU3Ec__Iterator2D_t659480186::get_offset_of_U24PC_0(),
	U3CFocusDownU3Ec__Iterator2D_t659480186::get_offset_of_U24current_1(),
	U3CFocusDownU3Ec__Iterator2D_t659480186::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (SoundManage_t3403985716), -1, sizeof(SoundManage_t3403985716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4039[16] = 
{
	SoundManage_t3403985716::get_offset_of_efxSource_2(),
	SoundManage_t3403985716::get_offset_of_musicSource_3(),
	SoundManage_t3403985716_StaticFields::get_offset_of_instance_4(),
	SoundManage_t3403985716::get_offset_of__bgmTitle_5(),
	SoundManage_t3403985716::get_offset_of__bgmLobby_6(),
	SoundManage_t3403985716::get_offset_of__bgmGrinding_7(),
	SoundManage_t3403985716::get_offset_of__bgmVariaion_8(),
	SoundManage_t3403985716::get_offset_of__bgmTemping_9(),
	SoundManage_t3403985716::get_offset_of__bgmIcing_10(),
	SoundManage_t3403985716::get_offset_of__bgmLearn_11(),
	SoundManage_t3403985716::get_offset_of__bgmMinigame_12(),
	SoundManage_t3403985716::get_offset_of__bgm_13(),
	SoundManage_t3403985716::get_offset_of__efxBtn_14(),
	SoundManage_t3403985716::get_offset_of__efxOk_15(),
	SoundManage_t3403985716::get_offset_of__efxBad_16(),
	SoundManage_t3403985716::get_offset_of_isPlaying_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (StartFirst_t396912526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4040[2] = 
{
	StartFirst_t396912526::get_offset_of_Blink_2(),
	StartFirst_t396912526::get_offset_of_speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (U3CBlackBlinkU3Ec__Iterator2E_t2642540702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4041[4] = 
{
	U3CBlackBlinkU3Ec__Iterator2E_t2642540702::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CBlackBlinkU3Ec__Iterator2E_t2642540702::get_offset_of_U24PC_1(),
	U3CBlackBlinkU3Ec__Iterator2E_t2642540702::get_offset_of_U24current_2(),
	U3CBlackBlinkU3Ec__Iterator2E_t2642540702::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (SFM_t82010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (StructforMinigame_t1286533533), -1, sizeof(StructforMinigame_t1286533533_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4043[10] = 
{
	StructforMinigame_t1286533533_StaticFields::get_offset_of_stuff_0(),
	StructforMinigame_t1286533533::get_offset_of_sw_1(),
	StructforMinigame_t1286533533::get_offset_of_userId_2(),
	StructforMinigame_t1286533533::get_offset_of_gameId_3(),
	StructforMinigame_t1286533533::get_offset_of_level_4(),
	StructforMinigame_t1286533533::get_offset_of_recordDate_5(),
	StructforMinigame_t1286533533::get_offset_of_score_6(),
	StructforMinigame_t1286533533::get_offset_of_playtime_7(),
	StructforMinigame_t1286533533::get_offset_of_misscount_8(),
	StructforMinigame_t1286533533::get_offset_of_errorRate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (TempingLastObj_t3031672627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (TempingPointer_t2678149871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4045[12] = 
{
	TempingPointer_t2678149871::get_offset_of__camera_2(),
	TempingPointer_t2678149871::get_offset_of__myFsm_3(),
	TempingPointer_t2678149871::get_offset_of__cameraPos_4(),
	TempingPointer_t2678149871::get_offset_of__lookTarget_5(),
	TempingPointer_t2678149871::get_offset_of__filterTarget_6(),
	TempingPointer_t2678149871::get_offset_of__filterDown_7(),
	TempingPointer_t2678149871::get_offset_of_Temper_8(),
	TempingPointer_t2678149871::get_offset_of__tempingMng_9(),
	TempingPointer_t2678149871::get_offset_of_gaugePoint_10(),
	TempingPointer_t2678149871::get_offset_of_FirstGaugeTrans_11(),
	TempingPointer_t2678149871::get_offset_of_speed_12(),
	TempingPointer_t2678149871::get_offset_of_speedDown_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4046[3] = 
{
	U3CMainCameraMoveU3Ec__Iterator2F_t3998083026::get_offset_of_U24PC_0(),
	U3CMainCameraMoveU3Ec__Iterator2F_t3998083026::get_offset_of_U24current_1(),
	U3CMainCameraMoveU3Ec__Iterator2F_t3998083026::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (U3CTempingSlideUpU3Ec__Iterator30_t3867951914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[3] = 
{
	U3CTempingSlideUpU3Ec__Iterator30_t3867951914::get_offset_of_U24PC_0(),
	U3CTempingSlideUpU3Ec__Iterator30_t3867951914::get_offset_of_U24current_1(),
	U3CTempingSlideUpU3Ec__Iterator30_t3867951914::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (U3CTempingSlideDownU3Ec__Iterator31_t3382326578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[3] = 
{
	U3CTempingSlideDownU3Ec__Iterator31_t3382326578::get_offset_of_U24PC_0(),
	U3CTempingSlideDownU3Ec__Iterator31_t3382326578::get_offset_of_U24current_1(),
	U3CTempingSlideDownU3Ec__Iterator31_t3382326578::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (TimerPref_t2057114344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4049[7] = 
{
	TimerPref_t2057114344::get_offset_of_timerText_2(),
	TimerPref_t2057114344::get_offset_of_timerTextMesh_3(),
	TimerPref_t2057114344::get_offset_of_nowNum_4(),
	TimerPref_t2057114344::get_offset_of_nowScene_5(),
	TimerPref_t2057114344::get_offset_of_stuff_6(),
	TimerPref_t2057114344::get_offset_of_cuff_7(),
	TimerPref_t2057114344::get_offset_of_img_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (ErrorLog_t1460740156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4050[2] = 
{
	ErrorLog_t1460740156::get_offset_of_text_2(),
	ErrorLog_t1460740156::get_offset_of_net_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (GiveScore_t3448718817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4051[5] = 
{
	GiveScore_t3448718817::get_offset_of_sw_2(),
	GiveScore_t3448718817::get_offset_of_nowNum_3(),
	GiveScore_t3448718817::get_offset_of_ticks_4(),
	GiveScore_t3448718817::get_offset_of_stuff_5(),
	GiveScore_t3448718817::get_offset_of_popup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (MasterIO_t348555048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[1] = 
{
	MasterIO_t348555048::get_offset_of_stuff_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (StructforPopup_t2315336792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (PopupManager_t2711269761), -1, sizeof(PopupManager_t2711269761_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4054[6] = 
{
	PopupManager_t2711269761_StaticFields::get_offset_of_pm_2(),
	PopupManager_t2711269761::get_offset_of_ad_3(),
	PopupManager_t2711269761::get_offset_of_state_4(),
	PopupManager_t2711269761::get_offset_of_nowNum_5(),
	PopupManager_t2711269761::get_offset_of_stuff_6(),
	PopupManager_t2711269761::get_offset_of_tx_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (popupIndex_t3504285908)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4055[7] = 
{
	popupIndex_t3504285908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (ResultWindowEnable_t999673456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4056[1] = 
{
	ResultWindowEnable_t999673456::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (U3CWaitTempU3Ec__Iterator32_t3760889752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[3] = 
{
	U3CWaitTempU3Ec__Iterator32_t3760889752::get_offset_of_U24PC_0(),
	U3CWaitTempU3Ec__Iterator32_t3760889752::get_offset_of_U24current_1(),
	U3CWaitTempU3Ec__Iterator32_t3760889752::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (SceneindexInc_t2309712408), -1, sizeof(SceneindexInc_t2309712408_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4058[2] = 
{
	SceneindexInc_t2309712408_StaticFields::get_offset_of_n_0(),
	SceneindexInc_t2309712408::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (StartImageBGMS_t2059898468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4059[1] = 
{
	StartImageBGMS_t2059898468::get_offset_of_nowScene_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (StartImageDisapear_t2918831698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[4] = 
{
	StartImageDisapear_t2918831698::get_offset_of_nowNum_2(),
	StartImageDisapear_t2918831698::get_offset_of_nowScene_3(),
	StartImageDisapear_t2918831698::get_offset_of_stuff_4(),
	StartImageDisapear_t2918831698::get_offset_of_sw_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (VariationResultWindow_t1568540672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4061[7] = 
{
	VariationResultWindow_t1568540672::get_offset_of_coffeImg_2(),
	VariationResultWindow_t1568540672::get_offset_of_CreamImg_3(),
	VariationResultWindow_t1568540672::get_offset_of_menuText_4(),
	VariationResultWindow_t1568540672::get_offset_of__showRecipePre_5(),
	VariationResultWindow_t1568540672::get_offset_of__btn_6(),
	VariationResultWindow_t1568540672::get_offset_of_menuTextList_7(),
	VariationResultWindow_t1568540672::get_offset_of_unitTextList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (YoutubeStrim_t2074917330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4062[1] = 
{
	YoutubeStrim_t2074917330::get_offset_of_Player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (JsonNetSample_t2710899071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[1] = 
{
	JsonNetSample_t2710899071::get_offset_of_Output_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (Product_t2509028415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[4] = 
{
	Product_t2509028415::get_offset_of_Name_0(),
	Product_t2509028415::get_offset_of_ExpiryDate_1(),
	Product_t2509028415::get_offset_of_Price_2(),
	Product_t2509028415::get_offset_of_Sizes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (Movie_t147243648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[6] = 
{
	Movie_t147243648::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Movie_t147243648::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
	Movie_t147243648::get_offset_of_U3CClassificationU3Ek__BackingField_2(),
	Movie_t147243648::get_offset_of_U3CStudioU3Ek__BackingField_3(),
	Movie_t147243648::get_offset_of_U3CReleaseDateU3Ek__BackingField_4(),
	Movie_t147243648::get_offset_of_U3CReleaseCountriesU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (GetVideo_t2032672485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4066[2] = 
{
	GetVideo_t2032672485::get_offset_of_videoId1_2(),
	GetVideo_t2032672485::get_offset_of_videoId2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (YoutubeEasyMovieTexture_t1195908624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4067[7] = 
{
	YoutubeEasyMovieTexture_t1195908624::get_offset_of_Player_2(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of__movie_3(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of_LearnPop_4(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of__slider_5(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of__text_6(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of_totalLength_7(),
	YoutubeEasyMovieTexture_t1195908624::get_offset_of_moveSpeed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (U3CCheckPlayU3Ec__Iterator33_t4170198610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[3] = 
{
	U3CCheckPlayU3Ec__Iterator33_t4170198610::get_offset_of_U24PC_0(),
	U3CCheckPlayU3Ec__Iterator33_t4170198610::get_offset_of_U24current_1(),
	U3CCheckPlayU3Ec__Iterator33_t4170198610::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (YoutubeVideo_t2077346616), -1, sizeof(YoutubeVideo_t2077346616_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4069[3] = 
{
	YoutubeVideo_t2077346616_StaticFields::get_offset_of_Instance_2(),
	YoutubeVideo_t2077346616::get_offset_of_drawBackground_3(),
	YoutubeVideo_t2077346616::get_offset_of_backgroundImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (start_mini_game_t3518442301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (InvitationReceivedDelegate_t2409308905), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (MatchDelegate_t1377674964), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (ConflictCallback_t942269343), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (ReportProgress_t3967815895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238940), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4075[23] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D12_12(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D13_13(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D14_14(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D15_15(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D16_16(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D17_17(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D18_18(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D19_19(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D20_20(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D21_21(),
	U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields::get_offset_of_U24U24fieldU2D22_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (U24ArrayTypeU24124_t1676615737)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t1676615737_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (U24ArrayTypeU2420_t3379220379)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220379_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (U24ArrayTypeU2416_t3379220356)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220356_marshaled_pinvoke), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
