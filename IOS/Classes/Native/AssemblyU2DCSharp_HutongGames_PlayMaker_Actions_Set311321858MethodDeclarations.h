﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmVariable
struct SetFsmVariable_t311321858;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::.ctor()
extern "C"  void SetFsmVariable__ctor_m3338216868 (SetFsmVariable_t311321858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::Reset()
extern "C"  void SetFsmVariable_Reset_m984649809 (SetFsmVariable_t311321858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::OnEnter()
extern "C"  void SetFsmVariable_OnEnter_m3372535931 (SetFsmVariable_t311321858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::OnUpdate()
extern "C"  void SetFsmVariable_OnUpdate_m602958120 (SetFsmVariable_t311321858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVariable::DoSetFsmVariable()
extern "C"  void SetFsmVariable_DoSetFsmVariable_m947390181 (SetFsmVariable_t311321858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
