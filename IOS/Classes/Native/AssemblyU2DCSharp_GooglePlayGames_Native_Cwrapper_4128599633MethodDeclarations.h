﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.Cwrapper.InternalHooks::InternalHooks_ConfigureForUnityPlugin(System.Runtime.InteropServices.HandleRef)
extern "C"  void InternalHooks_InternalHooks_ConfigureForUnityPlugin_m4202235629 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
