﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsFixedAngle2d
struct IsFixedAngle2d_t1917965731;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::.ctor()
extern "C"  void IsFixedAngle2d__ctor_m910416419 (IsFixedAngle2d_t1917965731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::Reset()
extern "C"  void IsFixedAngle2d_Reset_m2851816656 (IsFixedAngle2d_t1917965731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::OnEnter()
extern "C"  void IsFixedAngle2d_OnEnter_m2423546170 (IsFixedAngle2d_t1917965731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::OnUpdate()
extern "C"  void IsFixedAngle2d_OnUpdate_m1249046601 (IsFixedAngle2d_t1917965731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::DoIsFixedAngle()
extern "C"  void IsFixedAngle2d_DoIsFixedAngle_m2571732085 (IsFixedAngle2d_t1917965731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
