﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserId
struct UserId_t2542803558;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// NetworkMng
struct NetworkMng_t1515215352;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng/<NewGameCouroutine>c__Iterator12
struct  U3CNewGameCouroutineU3Ec__Iterator12_t148229244  : public Il2CppObject
{
public:
	// UserId NetworkMng/<NewGameCouroutine>c__Iterator12::<user>__0
	UserId_t2542803558 * ___U3CuserU3E__0_0;
	// System.String NetworkMng/<NewGameCouroutine>c__Iterator12::<str>__1
	String_t* ___U3CstrU3E__1_1;
	// System.Int32 NetworkMng/<NewGameCouroutine>c__Iterator12::$PC
	int32_t ___U24PC_2;
	// System.Object NetworkMng/<NewGameCouroutine>c__Iterator12::$current
	Il2CppObject * ___U24current_3;
	// NetworkMng NetworkMng/<NewGameCouroutine>c__Iterator12::<>f__this
	NetworkMng_t1515215352 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CuserU3E__0_0() { return static_cast<int32_t>(offsetof(U3CNewGameCouroutineU3Ec__Iterator12_t148229244, ___U3CuserU3E__0_0)); }
	inline UserId_t2542803558 * get_U3CuserU3E__0_0() const { return ___U3CuserU3E__0_0; }
	inline UserId_t2542803558 ** get_address_of_U3CuserU3E__0_0() { return &___U3CuserU3E__0_0; }
	inline void set_U3CuserU3E__0_0(UserId_t2542803558 * value)
	{
		___U3CuserU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuserU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CstrU3E__1_1() { return static_cast<int32_t>(offsetof(U3CNewGameCouroutineU3Ec__Iterator12_t148229244, ___U3CstrU3E__1_1)); }
	inline String_t* get_U3CstrU3E__1_1() const { return ___U3CstrU3E__1_1; }
	inline String_t** get_address_of_U3CstrU3E__1_1() { return &___U3CstrU3E__1_1; }
	inline void set_U3CstrU3E__1_1(String_t* value)
	{
		___U3CstrU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CNewGameCouroutineU3Ec__Iterator12_t148229244, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CNewGameCouroutineU3Ec__Iterator12_t148229244, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CNewGameCouroutineU3Ec__Iterator12_t148229244, ___U3CU3Ef__this_4)); }
	inline NetworkMng_t1515215352 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline NetworkMng_t1515215352 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(NetworkMng_t1515215352 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
