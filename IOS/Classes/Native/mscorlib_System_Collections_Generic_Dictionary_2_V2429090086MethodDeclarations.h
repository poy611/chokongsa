﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t202289382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2429090086.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1359411213_gshared (Enumerator_t2429090086 * __this, Dictionary_2_t202289382 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1359411213(__this, ___host0, method) ((  void (*) (Enumerator_t2429090086 *, Dictionary_2_t202289382 *, const MethodInfo*))Enumerator__ctor_m1359411213_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m638320116_gshared (Enumerator_t2429090086 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m638320116(__this, method) ((  Il2CppObject * (*) (Enumerator_t2429090086 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m638320116_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2149124744_gshared (Enumerator_t2429090086 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2149124744(__this, method) ((  void (*) (Enumerator_t2429090086 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2149124744_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
extern "C"  void Enumerator_Dispose_m954862575_gshared (Enumerator_t2429090086 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m954862575(__this, method) ((  void (*) (Enumerator_t2429090086 *, const MethodInfo*))Enumerator_Dispose_m954862575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4199265524_gshared (Enumerator_t2429090086 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4199265524(__this, method) ((  bool (*) (Enumerator_t2429090086 *, const MethodInfo*))Enumerator_MoveNext_m4199265524_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2438567950_gshared (Enumerator_t2429090086 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2438567950(__this, method) ((  int32_t (*) (Enumerator_t2429090086 *, const MethodInfo*))Enumerator_get_Current_m2438567950_gshared)(__this, method)
