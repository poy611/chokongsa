﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey77
struct U3CPeersConnectedU3Ec__AnonStorey77_t884522243;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey77::.ctor()
extern "C"  void U3CPeersConnectedU3Ec__AnonStorey77__ctor_m2278969288 (U3CPeersConnectedU3Ec__AnonStorey77_t884522243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey77::<>m__52()
extern "C"  void U3CPeersConnectedU3Ec__AnonStorey77_U3CU3Em__52_m1964325870 (U3CPeersConnectedU3Ec__AnonStorey77_t884522243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
