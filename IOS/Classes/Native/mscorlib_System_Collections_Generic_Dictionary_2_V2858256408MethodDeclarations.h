﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1875722474(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2858256408 *, Dictionary_2_t4157650695 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1869624840(__this, ___item0, method) ((  void (*) (ValueCollection_t2858256408 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m440559313(__this, method) ((  void (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1752450334(__this, ___item0, method) ((  bool (*) (ValueCollection_t2858256408 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2402815299(__this, ___item0, method) ((  bool (*) (ValueCollection_t2858256408 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2463948767(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2850518677(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2858256408 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1199887824(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m27626193(__this, method) ((  bool (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1519436337(__this, method) ((  bool (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2980719965(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m29596337(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2858256408 *, MultiplayerParticipantU5BU5D_t3838171016*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator()
#define ValueCollection_GetEnumerator_m499280558(__this, method) ((  Enumerator_t2089484103  (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Count()
#define ValueCollection_get_Count_m3640925559(__this, method) ((  int32_t (*) (ValueCollection_t2858256408 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
