﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsGetString
struct PlayerPrefsGetString_t359937184;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetString::.ctor()
extern "C"  void PlayerPrefsGetString__ctor_m2183534022 (PlayerPrefsGetString_t359937184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetString::Reset()
extern "C"  void PlayerPrefsGetString_Reset_m4124934259 (PlayerPrefsGetString_t359937184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetString::OnEnter()
extern "C"  void PlayerPrefsGetString_OnEnter_m1823883293 (PlayerPrefsGetString_t359937184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
