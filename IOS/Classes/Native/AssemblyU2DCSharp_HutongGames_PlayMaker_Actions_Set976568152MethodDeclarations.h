﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIDepth
struct SetGUIDepth_t976568152;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::.ctor()
extern "C"  void SetGUIDepth__ctor_m1151823582 (SetGUIDepth_t976568152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::Reset()
extern "C"  void SetGUIDepth_Reset_m3093223819 (SetGUIDepth_t976568152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::OnPreprocess()
extern "C"  void SetGUIDepth_OnPreprocess_m3895710865 (SetGUIDepth_t976568152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::OnGUI()
extern "C"  void SetGUIDepth_OnGUI_m647222232 (SetGUIDepth_t976568152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
