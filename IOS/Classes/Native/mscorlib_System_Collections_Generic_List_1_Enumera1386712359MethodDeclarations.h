﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2148616117(__this, ___l0, method) ((  void (*) (Enumerator_t1386712359 *, List_1_t1367039589 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1333989245(__this, method) ((  void (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1014938473(__this, method) ((  Il2CppObject * (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::Dispose()
#define Enumerator_Dispose_m2366591706(__this, method) ((  void (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::VerifyState()
#define Enumerator_VerifyState_m1716131091(__this, method) ((  void (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::MoveNext()
#define Enumerator_MoveNext_m2593882601(__this, method) ((  bool (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Bson.BsonProperty>::get_Current()
#define Enumerator_get_Current_m44545930(__this, method) ((  BsonProperty_t4293821333 * (*) (Enumerator_t1386712359 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
