﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2121140615MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3030964935(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3825989258 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2493527180_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m853724561(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3825989258 *, Type_t *, int32_t, const MethodInfo*))Transform_1_Invoke_m2386881260_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m810743868(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3825989258 *, Type_t *, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1045734679_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m865361497(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3825989258 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m584313374_gshared)(__this, ___result0, method)
