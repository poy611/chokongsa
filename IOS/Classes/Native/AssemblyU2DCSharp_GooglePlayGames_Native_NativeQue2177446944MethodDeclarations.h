﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey63
struct U3CClaimMilestoneU3Ec__AnonStorey63_t2177446944;
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
struct ClaimMilestoneResponse_t683149430;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Qu683149430.h"

// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey63::.ctor()
extern "C"  void U3CClaimMilestoneU3Ec__AnonStorey63__ctor_m3930287739 (U3CClaimMilestoneU3Ec__AnonStorey63_t2177446944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey63::<>m__40(GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse)
extern "C"  void U3CClaimMilestoneU3Ec__AnonStorey63_U3CU3Em__40_m2831790730 (U3CClaimMilestoneU3Ec__AnonStorey63_t2177446944 * __this, ClaimMilestoneResponse_t683149430 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
