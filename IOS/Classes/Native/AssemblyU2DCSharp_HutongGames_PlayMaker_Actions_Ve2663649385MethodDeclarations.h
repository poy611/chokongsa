﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3HighPassFilter
struct Vector3HighPassFilter_t2663649385;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::.ctor()
extern "C"  void Vector3HighPassFilter__ctor_m4154408685 (Vector3HighPassFilter_t2663649385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::Reset()
extern "C"  void Vector3HighPassFilter_Reset_m1800841626 (Vector3HighPassFilter_t2663649385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::OnEnter()
extern "C"  void Vector3HighPassFilter_OnEnter_m1753856900 (Vector3HighPassFilter_t2663649385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::OnUpdate()
extern "C"  void Vector3HighPassFilter_OnUpdate_m1963515711 (Vector3HighPassFilter_t2663649385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
