﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ObjectCompare
struct ObjectCompare_t355571316;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ObjectCompare::.ctor()
extern "C"  void ObjectCompare__ctor_m1033747778 (ObjectCompare_t355571316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ObjectCompare::Reset()
extern "C"  void ObjectCompare_Reset_m2975148015 (ObjectCompare_t355571316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ObjectCompare::OnEnter()
extern "C"  void ObjectCompare_OnEnter_m685897881 (ObjectCompare_t355571316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ObjectCompare::OnUpdate()
extern "C"  void ObjectCompare_OnUpdate_m3216524490 (ObjectCompare_t355571316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ObjectCompare::DoObjectCompare()
extern "C"  void ObjectCompare_DoObjectCompare_m3595773627 (ObjectCompare_t355571316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
