﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse_m1108133859 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetVariant_m3493815174 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___variant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_AddPlayerToInvite_m2908520879 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___player_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Construct()
extern "C"  IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Construct_m2185716274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetExclusiveBitMask_m4001679775 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___exclusive_bit_mask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers_m4197123561 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___maximum_automatching_players1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Create_m1280644352 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers_m1383316283 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_automatching_players1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Dispose_m3244885484 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
