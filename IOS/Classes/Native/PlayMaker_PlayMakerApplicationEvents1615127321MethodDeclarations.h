﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerApplicationEvents
struct PlayMakerApplicationEvents_t1615127321;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerApplicationEvents::OnApplicationFocus()
extern "C"  void PlayMakerApplicationEvents_OnApplicationFocus_m3979291463 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerApplicationEvents::OnApplicationPause()
extern "C"  void PlayMakerApplicationEvents_OnApplicationPause_m3880135909 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerApplicationEvents::.ctor()
extern "C"  void PlayMakerApplicationEvents__ctor_m3083946724 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
