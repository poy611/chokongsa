﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress
struct NetworkHavePublicIpAddress_t2341933110;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::.ctor()
extern "C"  void NetworkHavePublicIpAddress__ctor_m4034228464 (NetworkHavePublicIpAddress_t2341933110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::Reset()
extern "C"  void NetworkHavePublicIpAddress_Reset_m1680661405 (NetworkHavePublicIpAddress_t2341933110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::OnEnter()
extern "C"  void NetworkHavePublicIpAddress_OnEnter_m2224781511 (NetworkHavePublicIpAddress_t2341933110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
