﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct Dictionary_2_t2995151149;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3610087203.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2615580425_gshared (Enumerator_t3610087203 * __this, Dictionary_2_t2995151149 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2615580425(__this, ___host0, method) ((  void (*) (Enumerator_t3610087203 *, Dictionary_2_t2995151149 *, const MethodInfo*))Enumerator__ctor_m2615580425_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3357436408_gshared (Enumerator_t3610087203 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3357436408(__this, method) ((  Il2CppObject * (*) (Enumerator_t3610087203 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3357436408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3963127308_gshared (Enumerator_t3610087203 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3963127308(__this, method) ((  void (*) (Enumerator_t3610087203 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3963127308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3754402283_gshared (Enumerator_t3610087203 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3754402283(__this, method) ((  void (*) (Enumerator_t3610087203 *, const MethodInfo*))Enumerator_Dispose_m3754402283_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m251095416_gshared (Enumerator_t3610087203 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m251095416(__this, method) ((  bool (*) (Enumerator_t3610087203 *, const MethodInfo*))Enumerator_MoveNext_m251095416_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Current()
extern "C"  ResolverContractKey_t473801005  Enumerator_get_Current_m1597924316_gshared (Enumerator_t3610087203 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1597924316(__this, method) ((  ResolverContractKey_t473801005  (*) (Enumerator_t3610087203 *, const MethodInfo*))Enumerator_get_Current_m1597924316_gshared)(__this, method)
