﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResponseResultAllData
struct ResponseResultAllData_t4071630637;

#include "codegen/il2cpp-codegen.h"

// System.Void ResponseResultAllData::.ctor()
extern "C"  void ResponseResultAllData__ctor_m2010879198 (ResponseResultAllData_t4071630637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
