﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D
struct U3CRunCoroutineU3Ec__AnonStorey3D_t724587615;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D::.ctor()
extern "C"  void U3CRunCoroutineU3Ec__AnonStorey3D__ctor_m3701102876 (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D::<>m__C()
extern "C"  void U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080 (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
