﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen563161977.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3958787668_gshared (InternalEnumerator_1_t563161977 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3958787668(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t563161977 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3958787668_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300(__this, method) ((  void (*) (InternalEnumerator_1_t563161977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t563161977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2933780779_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2933780779(__this, method) ((  void (*) (InternalEnumerator_1_t563161977 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2933780779_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m89275576_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m89275576(__this, method) ((  bool (*) (InternalEnumerator_1_t563161977 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m89275576_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::get_Current()
extern "C"  HandleRef_t1780819301  InternalEnumerator_1_get_Current_m578280667_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m578280667(__this, method) ((  HandleRef_t1780819301  (*) (InternalEnumerator_1_t563161977 *, const MethodInfo*))InternalEnumerator_1_get_Current_m578280667_gshared)(__this, method)
