﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartImageBGMS
struct StartImageBGMS_t2059898468;

#include "codegen/il2cpp-codegen.h"

// System.Void StartImageBGMS::.ctor()
extern "C"  void StartImageBGMS__ctor_m1151217335 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartImageBGMS::Start()
extern "C"  void StartImageBGMS_Start_m98355127 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartImageBGMS::OnDisable()
extern "C"  void StartImageBGMS_OnDisable_m1710313374 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
