﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction
struct BaseFsmVariableIndexAction_t2226508741;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::.ctor()
extern "C"  void BaseFsmVariableIndexAction__ctor_m3209182785 (BaseFsmVariableIndexAction_t2226508741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::Reset()
extern "C"  void BaseFsmVariableIndexAction_Reset_m855615726 (BaseFsmVariableIndexAction_t2226508741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::UpdateCache(UnityEngine.GameObject,System.String)
extern "C"  bool BaseFsmVariableIndexAction_UpdateCache_m1136982360 (BaseFsmVariableIndexAction_t2226508741 * __this, GameObject_t3674682005 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::DoVariableNotFound(System.String)
extern "C"  void BaseFsmVariableIndexAction_DoVariableNotFound_m22759433 (BaseFsmVariableIndexAction_t2226508741 * __this, String_t* ___variableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
