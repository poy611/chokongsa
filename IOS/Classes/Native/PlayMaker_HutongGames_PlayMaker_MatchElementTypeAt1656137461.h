﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute2523058482.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MatchElementTypeAttribute
struct  MatchElementTypeAttribute_t1656137461  : public Attribute_t2523058482
{
public:
	// System.String HutongGames.PlayMaker.MatchElementTypeAttribute::fieldName
	String_t* ___fieldName_0;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(MatchElementTypeAttribute_t1656137461, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier(&___fieldName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
