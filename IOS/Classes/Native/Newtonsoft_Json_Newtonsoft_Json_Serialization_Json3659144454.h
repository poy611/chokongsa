﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2068678036.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_t3659144454  : public JsonSerializerInternalBase_t2068678036
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
