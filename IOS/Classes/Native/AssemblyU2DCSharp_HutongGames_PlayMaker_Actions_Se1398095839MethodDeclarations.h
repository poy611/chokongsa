﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVelocity2d
struct SetVelocity2d_t1398095839;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::.ctor()
extern "C"  void SetVelocity2d__ctor_m3443162167 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::Reset()
extern "C"  void SetVelocity2d_Reset_m1089595108 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::Awake()
extern "C"  void SetVelocity2d_Awake_m3680767386 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::OnEnter()
extern "C"  void SetVelocity2d_OnEnter_m1145753166 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::OnFixedUpdate()
extern "C"  void SetVelocity2d_OnFixedUpdate_m1696527315 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::DoSetVelocity()
extern "C"  void SetVelocity2d_DoSetVelocity_m2011490857 (SetVelocity2d_t1398095839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
