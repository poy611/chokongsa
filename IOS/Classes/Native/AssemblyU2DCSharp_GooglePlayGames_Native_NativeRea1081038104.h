﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B
struct  U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::selfId
	String_t* ___selfId_0;

public:
	inline static int32_t get_offset_of_selfId_0() { return static_cast<int32_t>(offsetof(U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104, ___selfId_0)); }
	inline String_t* get_selfId_0() const { return ___selfId_0; }
	inline String_t** get_address_of_selfId_0() { return &___selfId_0; }
	inline void set_selfId_0(String_t* value)
	{
		___selfId_0 = value;
		Il2CppCodeGenWriteBarrier(&___selfId_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
