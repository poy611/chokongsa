﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// LobbyDeskAnimation
struct LobbyDeskAnimation_t4061140181;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyDeskAnimation/<DeskCryCo>c__IteratorF
struct  U3CDeskCryCoU3Ec__IteratorF_t4183229176  : public Il2CppObject
{
public:
	// UnityEngine.Sprite LobbyDeskAnimation/<DeskCryCo>c__IteratorF::<source>__0
	Sprite_t3199167241 * ___U3CsourceU3E__0_0;
	// System.Int32 LobbyDeskAnimation/<DeskCryCo>c__IteratorF::$PC
	int32_t ___U24PC_1;
	// System.Object LobbyDeskAnimation/<DeskCryCo>c__IteratorF::$current
	Il2CppObject * ___U24current_2;
	// LobbyDeskAnimation LobbyDeskAnimation/<DeskCryCo>c__IteratorF::<>f__this
	LobbyDeskAnimation_t4061140181 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CsourceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDeskCryCoU3Ec__IteratorF_t4183229176, ___U3CsourceU3E__0_0)); }
	inline Sprite_t3199167241 * get_U3CsourceU3E__0_0() const { return ___U3CsourceU3E__0_0; }
	inline Sprite_t3199167241 ** get_address_of_U3CsourceU3E__0_0() { return &___U3CsourceU3E__0_0; }
	inline void set_U3CsourceU3E__0_0(Sprite_t3199167241 * value)
	{
		___U3CsourceU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsourceU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CDeskCryCoU3Ec__IteratorF_t4183229176, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDeskCryCoU3Ec__IteratorF_t4183229176, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CDeskCryCoU3Ec__IteratorF_t4183229176, ___U3CU3Ef__this_3)); }
	inline LobbyDeskAnimation_t4061140181 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline LobbyDeskAnimation_t4061140181 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(LobbyDeskAnimation_t4061140181 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
