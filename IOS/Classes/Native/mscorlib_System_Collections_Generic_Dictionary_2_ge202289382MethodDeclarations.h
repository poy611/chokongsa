﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t202289382;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct IDictionary_2_t4075130023;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>[]
struct KeyValuePair_2U5BU5D_t3199102425;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>
struct IEnumerator_1_t2012935137;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct KeyCollection_t1829048833;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct ValueCollection_t3197862391;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1519612774.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor()
extern "C"  void Dictionary_2__ctor_m3296647083_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3296647083(__this, method) ((  void (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2__ctor_m3296647083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2549677730_gshared (Dictionary_2_t202289382 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2549677730(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2549677730_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1746324173_gshared (Dictionary_2_t202289382 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1746324173(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1746324173_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1778869372_gshared (Dictionary_2_t202289382 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1778869372(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t202289382 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1778869372_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3582188368_gshared (Dictionary_2_t202289382 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3582188368(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3582188368_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1896640769_gshared (Dictionary_2_t202289382 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1896640769(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t202289382 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1896640769_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2242699884_gshared (Dictionary_2_t202289382 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2242699884(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t202289382 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2242699884_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2834230531_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2834230531(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2834230531_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2746352049_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2746352049(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2746352049_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2480308387_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2480308387(__this, method) ((  bool (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2480308387_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m674542013_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m674542013(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m674542013_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1384444258_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1384444258(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1384444258_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1520727983_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1520727983(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1520727983_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1777175271_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1777175271(__this, ___key0, method) ((  bool (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1777175271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1253732256_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1253732256(__this, ___key0, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1253732256_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3861341645_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3861341645(__this, method) ((  bool (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3861341645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2966322489_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2966322489(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2966322489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2041058257_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2041058257(__this, method) ((  bool (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2041058257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3868314678_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2_t101070088  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3868314678(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t202289382 *, KeyValuePair_2_t101070088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3868314678_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m587714124_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2_t101070088  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m587714124(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t202289382 *, KeyValuePair_2_t101070088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m587714124_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3108264602_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2U5BU5D_t3199102425* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3108264602(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t202289382 *, KeyValuePair_2U5BU5D_t3199102425*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3108264602_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3666679921_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2_t101070088  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3666679921(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t202289382 *, KeyValuePair_2_t101070088 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3666679921_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m693671929_gshared (Dictionary_2_t202289382 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m693671929(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m693671929_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1954887092_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1954887092(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1954887092_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3341638513_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3341638513(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3341638513_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1332960716_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1332960716(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1332960716_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m870932115_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m870932115(__this, method) ((  int32_t (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_get_Count_m870932115_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m372307832_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m372307832(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m372307832_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1002358315_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1002358315(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1002358315_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4116764451_gshared (Dictionary_2_t202289382 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4116764451(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t202289382 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4116764451_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1612066548_gshared (Dictionary_2_t202289382 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1612066548(__this, ___size0, method) ((  void (*) (Dictionary_2_t202289382 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1612066548_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1599197168_gshared (Dictionary_2_t202289382 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1599197168(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1599197168_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t101070088  Dictionary_2_make_pair_m3728257596_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3728257596(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t101070088  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m3728257596_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m3141602618_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3141602618(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m3141602618_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1446500346_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1446500346(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1446500346_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3544118751_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2U5BU5D_t3199102425* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3544118751(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t202289382 *, KeyValuePair_2U5BU5D_t3199102425*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3544118751_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Resize()
extern "C"  void Dictionary_2_Resize_m3469608429_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3469608429(__this, method) ((  void (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_Resize_m3469608429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2958609770_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2958609770(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_Add_m2958609770_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Clear()
extern "C"  void Dictionary_2_Clear_m702780374_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m702780374(__this, method) ((  void (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_Clear_m702780374_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2743286460_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2743286460(__this, ___key0, method) ((  bool (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2743286460_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1510658236_gshared (Dictionary_2_t202289382 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1510658236(__this, ___value0, method) ((  bool (*) (Dictionary_2_t202289382 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1510658236_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2429222345_gshared (Dictionary_2_t202289382 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2429222345(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t202289382 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2429222345_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m37711931_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m37711931(__this, ___sender0, method) ((  void (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m37711931_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m951728308_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m951728308(__this, ___key0, method) ((  bool (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m951728308_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3816232725_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3816232725(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t202289382 *, Il2CppObject *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m3816232725_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Keys()
extern "C"  KeyCollection_t1829048833 * Dictionary_2_get_Keys_m433213066_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m433213066(__this, method) ((  KeyCollection_t1829048833 * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_get_Keys_m433213066_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Values()
extern "C"  ValueCollection_t3197862391 * Dictionary_2_get_Values_m1828289290_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1828289290(__this, method) ((  ValueCollection_t3197862391 * (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_get_Values_m1828289290_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m2591461525_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2591461525(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2591461525_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m4053781781_gshared (Dictionary_2_t202289382 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4053781781(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t202289382 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4053781781_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m935052951_gshared (Dictionary_2_t202289382 * __this, KeyValuePair_2_t101070088  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m935052951(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t202289382 *, KeyValuePair_2_t101070088 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m935052951_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::GetEnumerator()
extern "C"  Enumerator_t1519612774  Dictionary_2_GetEnumerator_m1185042672_gshared (Dictionary_2_t202289382 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1185042672(__this, method) ((  Enumerator_t1519612774  (*) (Dictionary_2_t202289382 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1185042672_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1809155135_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1809155135(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1809155135_gshared)(__this /* static, unused */, ___key0, ___value1, method)
