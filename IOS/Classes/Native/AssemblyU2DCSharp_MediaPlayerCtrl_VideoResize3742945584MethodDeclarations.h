﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoResize
struct VideoResize_t3742945584;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MediaPlayerCtrl/VideoResize::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoResize__ctor_m1542430471 (VideoResize_t3742945584 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoResize::Invoke()
extern "C"  void VideoResize_Invoke_m1578206753 (VideoResize_t3742945584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoResize::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoResize_BeginInvoke_m2977844298 (VideoResize_t3742945584 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoResize::EndInvoke(System.IAsyncResult)
extern "C"  void VideoResize_EndInvoke_m3531713687 (VideoResize_t3742945584 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
