﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalculationData
struct  CalculationData_t2213441011  : public Il2CppObject
{
public:
	// System.String CalculationData::user_id
	String_t* ___user_id_0;
	// System.Int32 CalculationData::numcclt_trial_th
	int32_t ___numcclt_trial_th_1;
	// System.Int32 CalculationData::numcclt_grade
	int32_t ___numcclt_grade_2;
	// System.Int32 CalculationData::numcclt_game_id
	int32_t ___numcclt_game_id_3;
	// System.Int32 CalculationData::numcclt_operate_type
	int32_t ___numcclt_operate_type_4;
	// System.Int32 CalculationData::numcclt_quiz_value1
	int32_t ___numcclt_quiz_value1_5;
	// System.Int32 CalculationData::numcclt_quiz_value2
	int32_t ___numcclt_quiz_value2_6;
	// System.Int32 CalculationData::numcclt_correct_value
	int32_t ___numcclt_correct_value_7;
	// System.Single CalculationData::numcclt_play_time
	float ___numcclt_play_time_8;
	// System.Boolean CalculationData::numcclt_is_correct
	bool ___numcclt_is_correct_9;

public:
	inline static int32_t get_offset_of_user_id_0() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___user_id_0)); }
	inline String_t* get_user_id_0() const { return ___user_id_0; }
	inline String_t** get_address_of_user_id_0() { return &___user_id_0; }
	inline void set_user_id_0(String_t* value)
	{
		___user_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_0, value);
	}

	inline static int32_t get_offset_of_numcclt_trial_th_1() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_trial_th_1)); }
	inline int32_t get_numcclt_trial_th_1() const { return ___numcclt_trial_th_1; }
	inline int32_t* get_address_of_numcclt_trial_th_1() { return &___numcclt_trial_th_1; }
	inline void set_numcclt_trial_th_1(int32_t value)
	{
		___numcclt_trial_th_1 = value;
	}

	inline static int32_t get_offset_of_numcclt_grade_2() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_grade_2)); }
	inline int32_t get_numcclt_grade_2() const { return ___numcclt_grade_2; }
	inline int32_t* get_address_of_numcclt_grade_2() { return &___numcclt_grade_2; }
	inline void set_numcclt_grade_2(int32_t value)
	{
		___numcclt_grade_2 = value;
	}

	inline static int32_t get_offset_of_numcclt_game_id_3() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_game_id_3)); }
	inline int32_t get_numcclt_game_id_3() const { return ___numcclt_game_id_3; }
	inline int32_t* get_address_of_numcclt_game_id_3() { return &___numcclt_game_id_3; }
	inline void set_numcclt_game_id_3(int32_t value)
	{
		___numcclt_game_id_3 = value;
	}

	inline static int32_t get_offset_of_numcclt_operate_type_4() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_operate_type_4)); }
	inline int32_t get_numcclt_operate_type_4() const { return ___numcclt_operate_type_4; }
	inline int32_t* get_address_of_numcclt_operate_type_4() { return &___numcclt_operate_type_4; }
	inline void set_numcclt_operate_type_4(int32_t value)
	{
		___numcclt_operate_type_4 = value;
	}

	inline static int32_t get_offset_of_numcclt_quiz_value1_5() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_quiz_value1_5)); }
	inline int32_t get_numcclt_quiz_value1_5() const { return ___numcclt_quiz_value1_5; }
	inline int32_t* get_address_of_numcclt_quiz_value1_5() { return &___numcclt_quiz_value1_5; }
	inline void set_numcclt_quiz_value1_5(int32_t value)
	{
		___numcclt_quiz_value1_5 = value;
	}

	inline static int32_t get_offset_of_numcclt_quiz_value2_6() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_quiz_value2_6)); }
	inline int32_t get_numcclt_quiz_value2_6() const { return ___numcclt_quiz_value2_6; }
	inline int32_t* get_address_of_numcclt_quiz_value2_6() { return &___numcclt_quiz_value2_6; }
	inline void set_numcclt_quiz_value2_6(int32_t value)
	{
		___numcclt_quiz_value2_6 = value;
	}

	inline static int32_t get_offset_of_numcclt_correct_value_7() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_correct_value_7)); }
	inline int32_t get_numcclt_correct_value_7() const { return ___numcclt_correct_value_7; }
	inline int32_t* get_address_of_numcclt_correct_value_7() { return &___numcclt_correct_value_7; }
	inline void set_numcclt_correct_value_7(int32_t value)
	{
		___numcclt_correct_value_7 = value;
	}

	inline static int32_t get_offset_of_numcclt_play_time_8() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_play_time_8)); }
	inline float get_numcclt_play_time_8() const { return ___numcclt_play_time_8; }
	inline float* get_address_of_numcclt_play_time_8() { return &___numcclt_play_time_8; }
	inline void set_numcclt_play_time_8(float value)
	{
		___numcclt_play_time_8 = value;
	}

	inline static int32_t get_offset_of_numcclt_is_correct_9() { return static_cast<int32_t>(offsetof(CalculationData_t2213441011, ___numcclt_is_correct_9)); }
	inline bool get_numcclt_is_correct_9() const { return ___numcclt_is_correct_9; }
	inline bool* get_address_of_numcclt_is_correct_9() { return &___numcclt_is_correct_9; }
	inline void set_numcclt_is_correct_9(bool value)
	{
		___numcclt_is_correct_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
