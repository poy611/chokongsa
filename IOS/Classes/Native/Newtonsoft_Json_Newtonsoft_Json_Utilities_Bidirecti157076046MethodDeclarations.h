﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Bidirect1375314390MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
#define BidirectionalDictionary_2__ctor_m3184648454(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method) ((  void (*) (BidirectionalDictionary_2_t157076046 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BidirectionalDictionary_2__ctor_m2565085922_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
#define BidirectionalDictionary_2__ctor_m2873029502(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, ___duplicateFirstErrorMessage2, ___duplicateSecondErrorMessage3, method) ((  void (*) (BidirectionalDictionary_2_t157076046 *, Il2CppObject*, Il2CppObject*, String_t*, String_t*, const MethodInfo*))BidirectionalDictionary_2__ctor_m1641679706_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, ___duplicateFirstErrorMessage2, ___duplicateSecondErrorMessage3, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::Set(TFirst,TSecond)
#define BidirectionalDictionary_2_Set_m2915562938(__this, ___first0, ___second1, method) ((  void (*) (BidirectionalDictionary_2_t157076046 *, String_t*, String_t*, const MethodInfo*))BidirectionalDictionary_2_Set_m2800109206_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::TryGetByFirst(TFirst,TSecond&)
#define BidirectionalDictionary_2_TryGetByFirst_m3644728544(__this, ___first0, ___second1, method) ((  bool (*) (BidirectionalDictionary_2_t157076046 *, String_t*, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetByFirst_m3250057860_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>::TryGetBySecond(TSecond,TFirst&)
#define BidirectionalDictionary_2_TryGetBySecond_m969217002(__this, ___second0, ___first1, method) ((  bool (*) (BidirectionalDictionary_2_t157076046 *, String_t*, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetBySecond_m1619327686_gshared)(__this, ___second0, ___first1, method)
