﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MasterIO
struct  MasterIO_t348555048  : public MonoBehaviour_t667441552
{
public:
	// StructforMinigame[] MasterIO::stuff
	StructforMinigameU5BU5D_t1843399504* ___stuff_2;

public:
	inline static int32_t get_offset_of_stuff_2() { return static_cast<int32_t>(offsetof(MasterIO_t348555048, ___stuff_2)); }
	inline StructforMinigameU5BU5D_t1843399504* get_stuff_2() const { return ___stuff_2; }
	inline StructforMinigameU5BU5D_t1843399504** get_address_of_stuff_2() { return &___stuff_2; }
	inline void set_stuff_2(StructforMinigameU5BU5D_t1843399504* value)
	{
		___stuff_2 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
