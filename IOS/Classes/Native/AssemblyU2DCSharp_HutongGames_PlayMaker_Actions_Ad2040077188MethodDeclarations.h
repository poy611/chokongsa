﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddComponent
struct AddComponent_t2040077188;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddComponent::.ctor()
extern "C"  void AddComponent__ctor_m449988450 (AddComponent_t2040077188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::Reset()
extern "C"  void AddComponent_Reset_m2391388687 (AddComponent_t2040077188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnEnter()
extern "C"  void AddComponent_OnEnter_m2333899449 (AddComponent_t2040077188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnExit()
extern "C"  void AddComponent_OnExit_m2578058687 (AddComponent_t2040077188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::DoAddComponent()
extern "C"  void AddComponent_DoAddComponent_m2270947369 (AddComponent_t2040077188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
