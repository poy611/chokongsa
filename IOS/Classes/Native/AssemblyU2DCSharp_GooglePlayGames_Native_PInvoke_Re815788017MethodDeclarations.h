﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t815788017;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct IEnumerable_1_t2417134198;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::.ctor(System.IntPtr)
extern "C"  void FetchInvitationsResponse__ctor_m736940026 (FetchInvitationsResponse_t815788017 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::RequestSucceeded()
extern "C"  bool FetchInvitationsResponse_RequestSucceeded_m3221316296 (FetchInvitationsResponse_t815788017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::ResponseStatus()
extern "C"  int32_t FetchInvitationsResponse_ResponseStatus_m1185817332 (FetchInvitationsResponse_t815788017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::Invitations()
extern "C"  Il2CppObject* FetchInvitationsResponse_Invitations_m3568153066 (FetchInvitationsResponse_t815788017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchInvitationsResponse_CallDispose_m2802607350 (FetchInvitationsResponse_t815788017 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::FromPointer(System.IntPtr)
extern "C"  FetchInvitationsResponse_t815788017 * FetchInvitationsResponse_FromPointer_m3352564423 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse::<Invitations>m__D0(System.UIntPtr)
extern "C"  MultiplayerInvitation_t3411188537 * FetchInvitationsResponse_U3CInvitationsU3Em__D0_m426476897 (FetchInvitationsResponse_t815788017 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
