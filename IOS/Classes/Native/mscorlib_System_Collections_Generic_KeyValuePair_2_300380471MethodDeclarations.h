﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3704436026_gshared (KeyValuePair_2_t300380471 * __this, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3704436026(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t300380471 *, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3704436026_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Key()
extern "C"  HandleRef_t1780819301  KeyValuePair_2_get_Key_m2927877486_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2927877486(__this, method) ((  HandleRef_t1780819301  (*) (KeyValuePair_2_t300380471 *, const MethodInfo*))KeyValuePair_2_get_Key_m2927877486_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2433763503_gshared (KeyValuePair_2_t300380471 * __this, HandleRef_t1780819301  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2433763503(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t300380471 *, HandleRef_t1780819301 , const MethodInfo*))KeyValuePair_2_set_Key_m2433763503_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3910513454_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3910513454(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t300380471 *, const MethodInfo*))KeyValuePair_2_get_Value_m3910513454_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2108271919_gshared (KeyValuePair_2_t300380471 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2108271919(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t300380471 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2108271919_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2948487315_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2948487315(__this, method) ((  String_t* (*) (KeyValuePair_2_t300380471 *, const MethodInfo*))KeyValuePair_2_ToString_m2948487315_gshared)(__this, method)
