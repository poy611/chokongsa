﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m853598568_gshared (KeyValuePair_2_t203144266 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m853598568(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t203144266 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m853598568_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m324263168_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m324263168(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t203144266 *, const MethodInfo*))KeyValuePair_2_get_Key_m324263168_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4226889665_gshared (KeyValuePair_2_t203144266 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4226889665(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t203144266 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m4226889665_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m552188452_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m552188452(__this, method) ((  int32_t (*) (KeyValuePair_2_t203144266 *, const MethodInfo*))KeyValuePair_2_get_Value_m552188452_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2709046081_gshared (KeyValuePair_2_t203144266 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2709046081(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t203144266 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2709046081_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4212323239_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4212323239(__this, method) ((  String_t* (*) (KeyValuePair_2_t203144266 *, const MethodInfo*))KeyValuePair_2_ToString_m4212323239_gshared)(__this, method)
