﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2301125574MethodDeclarations.h"

// System.Void System.Func`2<System.UIntPtr,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2920535829(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2432508056 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m161237399_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.UIntPtr,System.String>::Invoke(T)
#define Func_2_Invoke_m3918237889(__this, ___arg10, method) ((  String_t* (*) (Func_2_t2432508056 *, UIntPtr_t , const MethodInfo*))Func_2_Invoke_m527793903_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m942484724(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2432508056 *, UIntPtr_t , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3185104162_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.UIntPtr,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3992217011(__this, ___result0, method) ((  String_t* (*) (Func_2_t2432508056 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m858964037_gshared)(__this, ___result0, method)
