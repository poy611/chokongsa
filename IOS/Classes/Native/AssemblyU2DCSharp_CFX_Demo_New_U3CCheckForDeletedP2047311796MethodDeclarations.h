﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7
struct U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::.ctor()
extern "C"  void U3CCheckForDeletedParticlesU3Ec__Iterator7__ctor_m2106809911 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckForDeletedParticlesU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m210150907 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckForDeletedParticlesU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1149370255 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::MoveNext()
extern "C"  bool U3CCheckForDeletedParticlesU3Ec__Iterator7_MoveNext_m1028694621 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::Dispose()
extern "C"  void U3CCheckForDeletedParticlesU3Ec__Iterator7_Dispose_m1613011124 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::Reset()
extern "C"  void U3CCheckForDeletedParticlesU3Ec__Iterator7_Reset_m4048210148 (U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
