﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoRotate
struct  CFX_AutoRotate_t2564226612  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 CFX_AutoRotate::rotation
	Vector3_t4282066566  ___rotation_2;
	// UnityEngine.Space CFX_AutoRotate::space
	int32_t ___space_3;

public:
	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(CFX_AutoRotate_t2564226612, ___rotation_2)); }
	inline Vector3_t4282066566  get_rotation_2() const { return ___rotation_2; }
	inline Vector3_t4282066566 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Vector3_t4282066566  value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_space_3() { return static_cast<int32_t>(offsetof(CFX_AutoRotate_t2564226612, ___space_3)); }
	inline int32_t get_space_3() const { return ___space_3; }
	inline int32_t* get_address_of_space_3() { return &___space_3; }
	inline void set_space_3(int32_t value)
	{
		___space_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
