﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSpeed2d
struct GetSpeed2d_t1806850603;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::.ctor()
extern "C"  void GetSpeed2d__ctor_m4173642395 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::Reset()
extern "C"  void GetSpeed2d_Reset_m1820075336 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnEnter()
extern "C"  void GetSpeed2d_OnEnter_m3057583026 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnUpdate()
extern "C"  void GetSpeed2d_OnUpdate_m3724319953 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::DoGetSpeed()
extern "C"  void GetSpeed2d_DoGetSpeed_m1767578245 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
