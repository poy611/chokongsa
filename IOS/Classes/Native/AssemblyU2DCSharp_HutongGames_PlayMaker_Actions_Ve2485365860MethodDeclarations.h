﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2MoveTowards
struct Vector2MoveTowards_t2485365860;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2MoveTowards::.ctor()
extern "C"  void Vector2MoveTowards__ctor_m3728621698 (Vector2MoveTowards_t2485365860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2MoveTowards::Reset()
extern "C"  void Vector2MoveTowards_Reset_m1375054639 (Vector2MoveTowards_t2485365860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2MoveTowards::OnUpdate()
extern "C"  void Vector2MoveTowards_OnUpdate_m381811082 (Vector2MoveTowards_t2485365860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2MoveTowards::DoMoveTowards()
extern "C"  void Vector2MoveTowards_DoMoveTowards_m3728625632 (Vector2MoveTowards_t2485365860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
