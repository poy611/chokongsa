﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
struct GetAnimatorLayerCount_t3712536905;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::.ctor()
extern "C"  void GetAnimatorLayerCount__ctor_m2729802253 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::Reset()
extern "C"  void GetAnimatorLayerCount_Reset_m376235194 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::OnEnter()
extern "C"  void GetAnimatorLayerCount_OnEnter_m2801643172 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::DoGetLayerCount()
extern "C"  void GetAnimatorLayerCount_DoGetLayerCount_m1288341460 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
