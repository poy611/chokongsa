﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t9039225;
// ShowRecipePre
struct ShowRecipePre_t977589144;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// PopupManagement
struct PopupManagement_t282433007;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Icing
struct  ScoreManager_Icing_t3849909252  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreManager_Icing::nowNum
	int32_t ___nowNum_2;
	// System.String ScoreManager_Icing::nowScene
	String_t* ___nowScene_3;
	// System.Single ScoreManager_Icing::ticks
	float ___ticks_4;
	// UnityEngine.UI.Text ScoreManager_Icing::timmer
	Text_t9039225 * ___timmer_5;
	// ShowRecipePre ScoreManager_Icing::recipePre
	ShowRecipePre_t977589144 * ___recipePre_6;
	// UnityEngine.GameObject ScoreManager_Icing::ready
	GameObject_t3674682005 * ___ready_7;
	// StructforMinigame ScoreManager_Icing::stuff
	StructforMinigame_t1286533533 * ___stuff_8;
	// ConstforMinigame ScoreManager_Icing::cuff
	ConstforMinigame_t2789090703 * ___cuff_9;
	// PopupManagement ScoreManager_Icing::pop
	PopupManagement_t282433007 * ___pop_10;
	// PlayMakerFSM ScoreManager_Icing::myFsm
	PlayMakerFSM_t3799847376 * ___myFsm_11;
	// System.Int32 ScoreManager_Icing::missCount
	int32_t ___missCount_12;
	// System.Int32 ScoreManager_Icing::currentIngredient
	int32_t ___currentIngredient_13;
	// System.Diagnostics.Stopwatch ScoreManager_Icing::sw
	Stopwatch_t3420517611 * ___sw_14;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_nowScene_3() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___nowScene_3)); }
	inline String_t* get_nowScene_3() const { return ___nowScene_3; }
	inline String_t** get_address_of_nowScene_3() { return &___nowScene_3; }
	inline void set_nowScene_3(String_t* value)
	{
		___nowScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_3, value);
	}

	inline static int32_t get_offset_of_ticks_4() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___ticks_4)); }
	inline float get_ticks_4() const { return ___ticks_4; }
	inline float* get_address_of_ticks_4() { return &___ticks_4; }
	inline void set_ticks_4(float value)
	{
		___ticks_4 = value;
	}

	inline static int32_t get_offset_of_timmer_5() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___timmer_5)); }
	inline Text_t9039225 * get_timmer_5() const { return ___timmer_5; }
	inline Text_t9039225 ** get_address_of_timmer_5() { return &___timmer_5; }
	inline void set_timmer_5(Text_t9039225 * value)
	{
		___timmer_5 = value;
		Il2CppCodeGenWriteBarrier(&___timmer_5, value);
	}

	inline static int32_t get_offset_of_recipePre_6() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___recipePre_6)); }
	inline ShowRecipePre_t977589144 * get_recipePre_6() const { return ___recipePre_6; }
	inline ShowRecipePre_t977589144 ** get_address_of_recipePre_6() { return &___recipePre_6; }
	inline void set_recipePre_6(ShowRecipePre_t977589144 * value)
	{
		___recipePre_6 = value;
		Il2CppCodeGenWriteBarrier(&___recipePre_6, value);
	}

	inline static int32_t get_offset_of_ready_7() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___ready_7)); }
	inline GameObject_t3674682005 * get_ready_7() const { return ___ready_7; }
	inline GameObject_t3674682005 ** get_address_of_ready_7() { return &___ready_7; }
	inline void set_ready_7(GameObject_t3674682005 * value)
	{
		___ready_7 = value;
		Il2CppCodeGenWriteBarrier(&___ready_7, value);
	}

	inline static int32_t get_offset_of_stuff_8() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___stuff_8)); }
	inline StructforMinigame_t1286533533 * get_stuff_8() const { return ___stuff_8; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_8() { return &___stuff_8; }
	inline void set_stuff_8(StructforMinigame_t1286533533 * value)
	{
		___stuff_8 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_8, value);
	}

	inline static int32_t get_offset_of_cuff_9() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___cuff_9)); }
	inline ConstforMinigame_t2789090703 * get_cuff_9() const { return ___cuff_9; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_9() { return &___cuff_9; }
	inline void set_cuff_9(ConstforMinigame_t2789090703 * value)
	{
		___cuff_9 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_9, value);
	}

	inline static int32_t get_offset_of_pop_10() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___pop_10)); }
	inline PopupManagement_t282433007 * get_pop_10() const { return ___pop_10; }
	inline PopupManagement_t282433007 ** get_address_of_pop_10() { return &___pop_10; }
	inline void set_pop_10(PopupManagement_t282433007 * value)
	{
		___pop_10 = value;
		Il2CppCodeGenWriteBarrier(&___pop_10, value);
	}

	inline static int32_t get_offset_of_myFsm_11() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___myFsm_11)); }
	inline PlayMakerFSM_t3799847376 * get_myFsm_11() const { return ___myFsm_11; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_myFsm_11() { return &___myFsm_11; }
	inline void set_myFsm_11(PlayMakerFSM_t3799847376 * value)
	{
		___myFsm_11 = value;
		Il2CppCodeGenWriteBarrier(&___myFsm_11, value);
	}

	inline static int32_t get_offset_of_missCount_12() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___missCount_12)); }
	inline int32_t get_missCount_12() const { return ___missCount_12; }
	inline int32_t* get_address_of_missCount_12() { return &___missCount_12; }
	inline void set_missCount_12(int32_t value)
	{
		___missCount_12 = value;
	}

	inline static int32_t get_offset_of_currentIngredient_13() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___currentIngredient_13)); }
	inline int32_t get_currentIngredient_13() const { return ___currentIngredient_13; }
	inline int32_t* get_address_of_currentIngredient_13() { return &___currentIngredient_13; }
	inline void set_currentIngredient_13(int32_t value)
	{
		___currentIngredient_13 = value;
	}

	inline static int32_t get_offset_of_sw_14() { return static_cast<int32_t>(offsetof(ScoreManager_Icing_t3849909252, ___sw_14)); }
	inline Stopwatch_t3420517611 * get_sw_14() const { return ___sw_14; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_14() { return &___sw_14; }
	inline void set_sw_14(Stopwatch_t3420517611 * value)
	{
		___sw_14 = value;
		Il2CppCodeGenWriteBarrier(&___sw_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
