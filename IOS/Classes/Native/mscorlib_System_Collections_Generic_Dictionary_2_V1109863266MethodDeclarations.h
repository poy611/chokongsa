﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1109863266.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2397857754_gshared (Enumerator_t1109863266 * __this, Dictionary_2_t3178029858 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2397857754(__this, ___host0, method) ((  void (*) (Enumerator_t1109863266 *, Dictionary_2_t3178029858 *, const MethodInfo*))Enumerator__ctor_m2397857754_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2752270161_gshared (Enumerator_t1109863266 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2752270161(__this, method) ((  Il2CppObject * (*) (Enumerator_t1109863266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2752270161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m992916635_gshared (Enumerator_t1109863266 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m992916635(__this, method) ((  void (*) (Enumerator_t1109863266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m992916635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m251934972_gshared (Enumerator_t1109863266 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m251934972(__this, method) ((  void (*) (Enumerator_t1109863266 *, const MethodInfo*))Enumerator_Dispose_m251934972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1954141387_gshared (Enumerator_t1109863266 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1954141387(__this, method) ((  bool (*) (Enumerator_t1109863266 *, const MethodInfo*))Enumerator_MoveNext_m1954141387_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3369718143_gshared (Enumerator_t1109863266 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3369718143(__this, method) ((  int32_t (*) (Enumerator_t1109863266 *, const MethodInfo*))Enumerator_get_Current_m3369718143_gshared)(__this, method)
