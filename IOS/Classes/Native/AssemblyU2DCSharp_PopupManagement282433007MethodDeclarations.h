﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PP_popupIndex3545488117.h"

// System.Void PopupManagement::.ctor()
extern "C"  void PopupManagement__ctor_m2958716508 (PopupManagement_t282433007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManagement::Start()
extern "C"  void PopupManagement_Start_m1905854300 (PopupManagement_t282433007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManagement::makeIt(PP/popupIndex)
extern "C"  void PopupManagement_makeIt_m1215430652 (PopupManagement_t282433007 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object PopupManagement::get_PopupObject()
extern "C"  Object_t3071478659 * PopupManagement_get_PopupObject_m1866634147 (PopupManagement_t282433007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PopupManagement PopupManagement::pointingMe()
extern "C"  PopupManagement_t282433007 * PopupManagement_pointingMe_m2502529422 (PopupManagement_t282433007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
