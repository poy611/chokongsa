﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct DefaultComparer_t2476461352;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor()
extern "C"  void DefaultComparer__ctor_m3319287161_gshared (DefaultComparer_t2476461352 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3319287161(__this, method) ((  void (*) (DefaultComparer_t2476461352 *, const MethodInfo*))DefaultComparer__ctor_m3319287161_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1040670298_gshared (DefaultComparer_t2476461352 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1040670298(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2476461352 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1040670298_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3000291470_gshared (DefaultComparer_t2476461352 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3000291470(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2476461352 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3000291470_gshared)(__this, ___x0, ___y1, method)
