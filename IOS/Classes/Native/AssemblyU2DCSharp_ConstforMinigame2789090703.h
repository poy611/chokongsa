﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstforMinigame[,]
struct ConstforMinigameU5BU2CU5D_t3700727639;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstforMinigame
struct  ConstforMinigame_t2789090703  : public Il2CppObject
{
public:
	// System.Single ConstforMinigame::bestTime
	float ___bestTime_1;
	// System.Single ConstforMinigame::limitTime
	float ___limitTime_2;
	// System.Single ConstforMinigame::bestErrorPercent
	float ___bestErrorPercent_3;
	// System.Single ConstforMinigame::limitErrorPercent
	float ___limitErrorPercent_4;
	// System.Int32 ConstforMinigame::errorPenalty
	int32_t ___errorPenalty_5;

public:
	inline static int32_t get_offset_of_bestTime_1() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703, ___bestTime_1)); }
	inline float get_bestTime_1() const { return ___bestTime_1; }
	inline float* get_address_of_bestTime_1() { return &___bestTime_1; }
	inline void set_bestTime_1(float value)
	{
		___bestTime_1 = value;
	}

	inline static int32_t get_offset_of_limitTime_2() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703, ___limitTime_2)); }
	inline float get_limitTime_2() const { return ___limitTime_2; }
	inline float* get_address_of_limitTime_2() { return &___limitTime_2; }
	inline void set_limitTime_2(float value)
	{
		___limitTime_2 = value;
	}

	inline static int32_t get_offset_of_bestErrorPercent_3() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703, ___bestErrorPercent_3)); }
	inline float get_bestErrorPercent_3() const { return ___bestErrorPercent_3; }
	inline float* get_address_of_bestErrorPercent_3() { return &___bestErrorPercent_3; }
	inline void set_bestErrorPercent_3(float value)
	{
		___bestErrorPercent_3 = value;
	}

	inline static int32_t get_offset_of_limitErrorPercent_4() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703, ___limitErrorPercent_4)); }
	inline float get_limitErrorPercent_4() const { return ___limitErrorPercent_4; }
	inline float* get_address_of_limitErrorPercent_4() { return &___limitErrorPercent_4; }
	inline void set_limitErrorPercent_4(float value)
	{
		___limitErrorPercent_4 = value;
	}

	inline static int32_t get_offset_of_errorPenalty_5() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703, ___errorPenalty_5)); }
	inline int32_t get_errorPenalty_5() const { return ___errorPenalty_5; }
	inline int32_t* get_address_of_errorPenalty_5() { return &___errorPenalty_5; }
	inline void set_errorPenalty_5(int32_t value)
	{
		___errorPenalty_5 = value;
	}
};

struct ConstforMinigame_t2789090703_StaticFields
{
public:
	// ConstforMinigame[,] ConstforMinigame::cuff
	ConstforMinigameU5BU2CU5D_t3700727639* ___cuff_0;

public:
	inline static int32_t get_offset_of_cuff_0() { return static_cast<int32_t>(offsetof(ConstforMinigame_t2789090703_StaticFields, ___cuff_0)); }
	inline ConstforMinigameU5BU2CU5D_t3700727639* get_cuff_0() const { return ___cuff_0; }
	inline ConstforMinigameU5BU2CU5D_t3700727639** get_address_of_cuff_0() { return &___cuff_0; }
	inline void set_cuff_0(ConstforMinigameU5BU2CU5D_t3700727639* value)
	{
		___cuff_0 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
