﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>
struct ValueCollection_t3397172774;
// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2628400469.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m782769003_gshared (ValueCollection_t3397172774 * __this, Dictionary_2_t401599765 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m782769003(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3397172774 *, Dictionary_2_t401599765 *, const MethodInfo*))ValueCollection__ctor_m782769003_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m310347623_gshared (ValueCollection_t3397172774 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m310347623(__this, ___item0, method) ((  void (*) (ValueCollection_t3397172774 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m310347623_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1731674032_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1731674032(__this, method) ((  void (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1731674032_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m932473315_gshared (ValueCollection_t3397172774 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m932473315(__this, ___item0, method) ((  bool (*) (ValueCollection_t3397172774 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m932473315_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1798610760_gshared (ValueCollection_t3397172774 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1798610760(__this, ___item0, method) ((  bool (*) (ValueCollection_t3397172774 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1798610760_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2609047280_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2609047280(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2609047280_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m4293264564_gshared (ValueCollection_t3397172774 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m4293264564(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3397172774 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4293264564_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1837200067_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1837200067(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1837200067_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3502616470_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3502616470(__this, method) ((  bool (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3502616470_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2975213174_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2975213174(__this, method) ((  bool (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2975213174_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m741492456_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m741492456(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m741492456_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4027784434_gshared (ValueCollection_t3397172774 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4027784434(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3397172774 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4027784434_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2628400469  ValueCollection_GetEnumerator_m88584795_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m88584795(__this, method) ((  Enumerator_t2628400469  (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_GetEnumerator_m88584795_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m871408816_gshared (ValueCollection_t3397172774 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m871408816(__this, method) ((  int32_t (*) (ValueCollection_t3397172774 *, const MethodInfo*))ValueCollection_get_Count_m871408816_gshared)(__this, method)
