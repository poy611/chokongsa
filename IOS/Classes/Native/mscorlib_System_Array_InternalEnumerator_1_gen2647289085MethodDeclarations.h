﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2647289085.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2927575_gshared (InternalEnumerator_1_t2647289085 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2927575(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2647289085 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2927575_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097(__this, method) ((  void (*) (InternalEnumerator_1_t2647289085 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2647289085 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1740621422_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1740621422(__this, method) ((  void (*) (InternalEnumerator_1_t2647289085 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1740621422_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m511491285_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m511491285(__this, method) ((  bool (*) (InternalEnumerator_1_t2647289085 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m511491285_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::get_Current()
extern "C"  JsonPosition_t3864946409  InternalEnumerator_1_get_Current_m2332499742_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2332499742(__this, method) ((  JsonPosition_t3864946409  (*) (InternalEnumerator_1_t2647289085 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2332499742_gshared)(__this, method)
