﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey82
struct U3CShowSelectSavedGameUIU3Ec__AnonStorey82_t2630013818;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t2500674014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2500674014.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey82::.ctor()
extern "C"  void U3CShowSelectSavedGameUIU3Ec__AnonStorey82__ctor_m3339536689 (U3CShowSelectSavedGameUIU3Ec__AnonStorey82_t2630013818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ShowSelectSavedGameUI>c__AnonStorey82::<>m__6B(GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse)
extern "C"  void U3CShowSelectSavedGameUIU3Ec__AnonStorey82_U3CU3Em__6B_m201302030 (U3CShowSelectSavedGameUIU3Ec__AnonStorey82_t2630013818 * __this, SnapshotSelectUIResponse_t2500674014 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
