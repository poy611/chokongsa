﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng
struct NetworkMng_t1515215352;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ResultData
struct ResultData_t1421128071;
// ResultSaveData
struct ResultSaveData_t3126241828;
// CalculationData
struct CalculationData_t2213441011;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_ResultSaveData3126241828.h"
#include "AssemblyU2DCSharp_CalculationData2213441011.h"

// System.Void NetworkMng::.ctor()
extern "C"  void NetworkMng__ctor_m183825827 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::Start()
extern "C"  void NetworkMng_Start_m3425930915 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW NetworkMng::GET(System.String)
extern "C"  WWW_t3134621005 * NetworkMng_GET_m800466334 (NetworkMng_t1515215352 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW NetworkMng::POST(System.String,System.String)
extern "C"  WWW_t3134621005 * NetworkMng_POST_m4134036778 (NetworkMng_t1515215352 * __this, String_t* ___url0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NetworkMng::WaitForRequest(UnityEngine.WWW)
extern "C"  Il2CppObject * NetworkMng_WaitForRequest_m3137602126 (NetworkMng_t1515215352 * __this, WWW_t3134621005 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestLogin(System.String,System.String)
extern "C"  void NetworkMng_RequestLogin_m3564897283 (NetworkMng_t1515215352 * __this, String_t* ___user_id0, String_t* ___user_name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::Login(System.String)
extern "C"  void NetworkMng_Login_m2908927832 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestUserInfo(System.String)
extern "C"  void NetworkMng_RequestUserInfo_m2257194521 (NetworkMng_t1515215352 * __this, String_t* ___user_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::InfoUser(System.String)
extern "C"  void NetworkMng_InfoUser_m764712616 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestInfoAgain(System.String)
extern "C"  void NetworkMng_RequestInfoAgain_m838322558 (NetworkMng_t1515215352 * __this, String_t* ___user_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::InfoAgain(System.String)
extern "C"  void NetworkMng_InfoAgain_m1094220879 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestNewGame()
extern "C"  void NetworkMng_RequestNewGame_m1143716516 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NetworkMng::NewGameCouroutine()
extern "C"  Il2CppObject * NetworkMng_NewGameCouroutine_m2165886630 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::NewGame(System.String)
extern "C"  void NetworkMng_NewGame_m2171240943 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestSaveGameDataInLocal()
extern "C"  void NetworkMng_RequestSaveGameDataInLocal_m2383761503 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestSaveGameData(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void NetworkMng_RequestSaveGameData_m2757540381 (NetworkMng_t1515215352 * __this, int32_t ___gameId0, int32_t ___trialTh1, int32_t ___grade2, int32_t ___guestTh3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NetworkMng::SaveGameDataCouroutine(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Il2CppObject * NetworkMng_SaveGameDataCouroutine_m1330162185 (NetworkMng_t1515215352 * __this, int32_t ___gameId0, int32_t ___trialTh1, int32_t ___grade2, int32_t ___guestTh3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::SaveGameData(System.String)
extern "C"  void NetworkMng_SaveGameData_m1317338344 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestSaveResultData(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void NetworkMng_RequestSaveResultData_m649158341 (NetworkMng_t1515215352 * __this, int32_t ___nowNum0, int32_t ___gameId1, int32_t ___grade2, int32_t ___guestTh3, int32_t ___trialTh4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NetworkMng::SaveResultDataCouroutine(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Il2CppObject * NetworkMng_SaveResultDataCouroutine_m2404413246 (NetworkMng_t1515215352 * __this, int32_t ___nowNum0, int32_t ___gameId1, int32_t ___grade2, int32_t ___guestTh3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::SaveResultData(System.String)
extern "C"  void NetworkMng_SaveResultData_m2967416349 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::RequestResullAllData(System.Int32,System.Int32)
extern "C"  void NetworkMng_RequestResullAllData_m2818822622 (NetworkMng_t1515215352 * __this, int32_t ___startNum0, int32_t ___endNum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NetworkMng::RequstResultAllDataCouroutine(System.Int32,System.Int32)
extern "C"  Il2CppObject * NetworkMng_RequstResultAllDataCouroutine_m4066055868 (NetworkMng_t1515215352 * __this, int32_t ___startNum0, int32_t ___endNum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::AllData(System.String)
extern "C"  void NetworkMng_AllData_m1446575510 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::AllCalculationData(System.String)
extern "C"  void NetworkMng_AllCalculationData_m945905903 (NetworkMng_t1515215352 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::WriteSaveData(System.String)
extern "C"  void NetworkMng_WriteSaveData_m2649945435 (NetworkMng_t1515215352 * __this, String_t* ___strData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ResultData NetworkMng::ReadSaveData()
extern "C"  ResultData_t1421128071 * NetworkMng_ReadSaveData_m43045332 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::SendAllData()
extern "C"  void NetworkMng_SendAllData_m496953924 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::SendAllCalculationData()
extern "C"  void NetworkMng_SendAllCalculationData_m3493818523 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::ReadAndWrite(ResultSaveData)
extern "C"  void NetworkMng_ReadAndWrite_m3263926171 (NetworkMng_t1515215352 * __this, ResultSaveData_t3126241828 * ___resultSaveData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::ReadAndWriteCalculationLog(CalculationData)
extern "C"  void NetworkMng_ReadAndWriteCalculationLog_m4255829061 (NetworkMng_t1515215352 * __this, CalculationData_t2213441011 * ___resultSaveData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NetworkMng::pathForDocumentsFile(System.String)
extern "C"  String_t* NetworkMng_pathForDocumentsFile_m3416743598 (NetworkMng_t1515215352 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::DeleteLogFile()
extern "C"  void NetworkMng_DeleteLogFile_m3987142710 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::DeleteCalculationLogFile()
extern "C"  void NetworkMng_DeleteCalculationLogFile_m2663773091 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng::OnApplicationQuit()
extern "C"  void NetworkMng_OnApplicationQuit_m1822623393 (NetworkMng_t1515215352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
