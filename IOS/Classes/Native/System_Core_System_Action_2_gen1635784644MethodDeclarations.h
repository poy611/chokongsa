﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen599046810MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2638266102(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1635784644 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3309597785_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::Invoke(T1,T2)
#define Action_2_Invoke_m247084213(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1635784644 *, TurnBasedMatch_t3573041681 *, bool, const MethodInfo*))Action_2_Invoke_m923698738_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2168585004(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1635784644 *, TurnBasedMatch_t3573041681 *, bool, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m2135774937_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3502380278(__this, ___result0, method) ((  void (*) (Action_2_t1635784644 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2751209_gshared)(__this, ___result0, method)
