﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkIsServer
struct NetworkIsServer_t3191877225;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::.ctor()
extern "C"  void NetworkIsServer__ctor_m2932420333 (NetworkIsServer_t3191877225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::Reset()
extern "C"  void NetworkIsServer_Reset_m578853274 (NetworkIsServer_t3191877225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::OnEnter()
extern "C"  void NetworkIsServer_OnEnter_m4244089732 (NetworkIsServer_t3191877225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::DoCheckIsServer()
extern "C"  void NetworkIsServer_DoCheckIsServer_m1052613749 (NetworkIsServer_t3191877225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
