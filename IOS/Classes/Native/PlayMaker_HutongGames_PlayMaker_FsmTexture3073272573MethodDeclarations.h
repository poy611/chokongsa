﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// UnityEngine.Texture
struct Texture_t2526458961;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "mscorlib_System_Type2863145774.h"

// UnityEngine.Texture HutongGames.PlayMaker.FsmTexture::get_Value()
extern "C"  Texture_t2526458961 * FsmTexture_get_Value_m3156202285 (FsmTexture_t3073272573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::set_Value(UnityEngine.Texture)
extern "C"  void FsmTexture_set_Value_m2261522310 (FsmTexture_t3073272573 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor()
extern "C"  void FsmTexture__ctor_m4043693302 (FsmTexture_t3073272573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor(System.String)
extern "C"  void FsmTexture__ctor_m434176460 (FsmTexture_t3073272573 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmTexture__ctor_m656749193 (FsmTexture_t3073272573 * __this, FsmObject_t821476169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmTexture::Clone()
extern "C"  NamedVariable_t3211770239 * FsmTexture_Clone_m432273799 (FsmTexture_t3073272573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmTexture::get_VariableType()
extern "C"  int32_t FsmTexture_get_VariableType_m2003499916 (FsmTexture_t3073272573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmTexture::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmTexture_TestTypeConstraint_m1677695616 (FsmTexture_t3073272573 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
