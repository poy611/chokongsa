﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel2277052758.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2290942609.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_Network1492793700.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_HostData3270478838.h"
#include "UnityEngine_UnityEngine_MasterServer2117519177.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties624110065.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_SortingLayer3376264497.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility1448936472.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394.h"
#include "UnityEngine_UnityEngine_UnityString3369712284.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Application2856536070.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback1945583101.h"
#include "UnityEngine_UnityEngine_DebugLogHandler2406589519.h"
#include "UnityEngine_UnityEngine_Debug4195163081.h"
#include "UnityEngine_UnityEngine_Display1321072632.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDele581305515.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "UnityEngine_UnityEngine_IMECompositionMode3300198960.h"
#include "UnityEngine_UnityEngine_TouchType970257423.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_DeviceOrientation1141857680.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus1153071304.h"
#include "UnityEngine_UnityEngine_LocationService3853025142.h"
#include "UnityEngine_UnityEngine_Input4200062272.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846.h"
#include "UnityEngine_UnityEngine_Time4241968337.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509.h"
#include "UnityEngine_UnityEngine_Motion3026528250.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd3393793052.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inter3068494210.h"
#include "UnityEngine_UnityEngine_iOS_ADInterstitialAd_Inters507319425.h"
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration572715224.h"
#include "UnityEngine_UnityEngine_iOS_Device1621710112.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier514511185.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit3375281836.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1344855248.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification1652317979.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2782999289.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1315979427.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener247595084.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play1328382603.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (Rect_t4241904616)+ sizeof (Il2CppObject), sizeof(Rect_t4241904616_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2000[4] = 
{
	Rect_t4241904616::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (Matrix4x4_t1651859333)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t1651859333_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2001[16] = 
{
	Matrix4x4_t1651859333::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (Bounds_t2711641849)+ sizeof (Il2CppObject), sizeof(Bounds_t2711641849_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	Bounds_t2711641849::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t2711641849::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (Vector4_t4282066567)+ sizeof (Il2CppObject), sizeof(Vector4_t4282066567_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[5] = 
{
	0,
	Vector4_t4282066567::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Ray_t3134616544)+ sizeof (Il2CppObject), sizeof(Ray_t3134616544_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[2] = 
{
	Ray_t3134616544::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t3134616544::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (Plane_t4206452690)+ sizeof (Il2CppObject), sizeof(Plane_t4206452690_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2005[2] = 
{
	Plane_t4206452690::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t4206452690::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (MathfInternal_t4096243933)+ sizeof (Il2CppObject), sizeof(MathfInternal_t4096243933_marshaled_pinvoke), sizeof(MathfInternal_t4096243933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2006[3] = 
{
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (Mathf_t4203372500)+ sizeof (Il2CppObject), sizeof(Mathf_t4203372500_marshaled_pinvoke), sizeof(Mathf_t4203372500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[1] = 
{
	Mathf_t4203372500_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (Keyframe_t4079056114)+ sizeof (Il2CppObject), sizeof(Keyframe_t4079056114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2008[4] = 
{
	Keyframe_t4079056114::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (AnimationCurve_t3667593487), sizeof(AnimationCurve_t3667593487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2009[1] = 
{
	AnimationCurve_t3667593487::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (Mesh_t4241756145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (InternalShaderChannel_t2277052758)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[9] = 
{
	InternalShaderChannel_t2277052758::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (InternalVertexChannelType_t2290942609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[3] = 
{
	InternalVertexChannelType_t2290942609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (NetworkConnectionError_t1049203712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2013[16] = 
{
	NetworkConnectionError_t1049203712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (NetworkDisconnection_t468395618)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	NetworkDisconnection_t468395618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (MasterServerEvent_t2733244747)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2015[6] = 
{
	MasterServerEvent_t2733244747::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (NetworkPeerType_t796297792)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2016[5] = 
{
	NetworkPeerType_t796297792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (NetworkLogLevel_t2722760996)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2017[4] = 
{
	NetworkLogLevel_t2722760996::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (NetworkPlayer_t3231273765)+ sizeof (Il2CppObject), sizeof(NetworkPlayer_t3231273765_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	NetworkPlayer_t3231273765::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (NetworkViewID_t3400394436)+ sizeof (Il2CppObject), sizeof(NetworkViewID_t3400394436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2019[3] = 
{
	NetworkViewID_t3400394436::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (NetworkView_t3656680617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (Network_t1492793700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (BitStream_t239125475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[1] = 
{
	BitStream_t239125475::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (RPC_t3134615963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (HostData_t3270478838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[10] = 
{
	HostData_t3270478838::get_offset_of_m_Nat_0(),
	HostData_t3270478838::get_offset_of_m_GameType_1(),
	HostData_t3270478838::get_offset_of_m_GameName_2(),
	HostData_t3270478838::get_offset_of_m_ConnectedPlayers_3(),
	HostData_t3270478838::get_offset_of_m_PlayerLimit_4(),
	HostData_t3270478838::get_offset_of_m_IP_5(),
	HostData_t3270478838::get_offset_of_m_Port_6(),
	HostData_t3270478838::get_offset_of_m_PasswordProtected_7(),
	HostData_t3270478838::get_offset_of_m_Comment_8(),
	HostData_t3270478838::get_offset_of_m_GUID_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (MasterServer_t2117519177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (NetworkMessageInfo_t3807997963)+ sizeof (Il2CppObject), sizeof(NetworkMessageInfo_t3807997963_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[3] = 
{
	NetworkMessageInfo_t3807997963::get_offset_of_m_TimeStamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_Sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_ViewID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (DrivenTransformProperties_t624110065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[26] = 
{
	DrivenTransformProperties_t624110065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (DrivenRectTransformTracker_t4185719096)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t4185719096_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (RectTransform_t972643934), -1, sizeof(RectTransform_t972643934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2029[1] = 
{
	RectTransform_t972643934_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (Edge_t1676794651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[5] = 
{
	Edge_t1676794651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (Axis_t1676694783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	Axis_t1676694783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (ReapplyDrivenProperties_t779639188), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (ResourceRequest_t3731857623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[2] = 
{
	ResourceRequest_t3731857623::get_offset_of_m_Path_1(),
	ResourceRequest_t3731857623::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (Resources_t2918352667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SerializePrivateVariables_t2835496234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (SerializeField_t3754825534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (Shader_t3191267369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Material_t3870600107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (SortingLayer_t3376264497)+ sizeof (Il2CppObject), sizeof(SortingLayer_t3376264497_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	SortingLayer_t3376264497::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (Sprite_t3199167241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (SpriteRenderer_t2548470764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (DataUtility_t1448936472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (Hash128_t346790303)+ sizeof (Il2CppObject), sizeof(Hash128_t346790303_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2044[4] = 
{
	Hash128_t346790303::get_offset_of_m_u32_0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (WWW_t3134621005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (WWWForm_t461342257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[6] = 
{
	WWWForm_t461342257::get_offset_of_formData_0(),
	WWWForm_t461342257::get_offset_of_fieldNames_1(),
	WWWForm_t461342257::get_offset_of_fileNames_2(),
	WWWForm_t461342257::get_offset_of_types_3(),
	WWWForm_t461342257::get_offset_of_boundary_4(),
	WWWForm_t461342257::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (WWWTranscoder_t609724394), -1, sizeof(WWWTranscoder_t609724394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[8] = 
{
	WWWTranscoder_t609724394_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (UnityString_t3369712284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (AsyncOperation_t3699081103), sizeof(AsyncOperation_t3699081103_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	AsyncOperation_t3699081103::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (Application_t2856536070), -1, sizeof(Application_t2856536070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2050[2] = 
{
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandler_0(),
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (LogCallback_t2984951347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (Behaviour_t200106419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Camera_t2727095145), -1, sizeof(Camera_t2727095145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	Camera_t2727095145_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t2727095145_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t2727095145_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (CameraCallback_t1945583101), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (DebugLogHandler_t2406589519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (Debug_t4195163081), -1, sizeof(Debug_t4195163081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2056[1] = 
{
	Debug_t4195163081_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (Display_t1321072632), -1, sizeof(Display_t1321072632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[4] = 
{
	Display_t1321072632::get_offset_of_nativeDisplay_0(),
	Display_t1321072632_StaticFields::get_offset_of_displays_1(),
	Display_t1321072632_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t1321072632_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (DisplaysUpdatedDelegate_t581305515), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (MonoBehaviour_t667441552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (TouchPhase_t1567063616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2060[6] = 
{
	TouchPhase_t1567063616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (IMECompositionMode_t3300198960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	IMECompositionMode_t3300198960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (TouchType_t970257423)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2062[4] = 
{
	TouchType_t970257423::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (Touch_t4210255029)+ sizeof (Il2CppObject), sizeof(Touch_t4210255029_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[14] = 
{
	Touch_t4210255029::get_offset_of_m_FingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RawPosition_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_PositionDelta_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TimeDelta_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TapCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Phase_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Type_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Pressure_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_maximumPossiblePressure_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Radius_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RadiusVariance_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AltitudeAngle_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AzimuthAngle_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (DeviceOrientation_t1141857680)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2064[8] = 
{
	DeviceOrientation_t1141857680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (LocationInfo_t3215517959)+ sizeof (Il2CppObject), sizeof(LocationInfo_t3215517959_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2065[6] = 
{
	LocationInfo_t3215517959::get_offset_of_m_Timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Latitude_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Longitude_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_Altitude_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_HorizontalAccuracy_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LocationInfo_t3215517959::get_offset_of_m_VerticalAccuracy_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (LocationServiceStatus_t1153071304)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2066[5] = 
{
	LocationServiceStatus_t1153071304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (LocationService_t3853025142), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (Input_t4200062272), -1, sizeof(Input_t4200062272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2068[1] = 
{
	Input_t4200062272_StaticFields::get_offset_of_locationServiceInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (HideFlags_t1436803931)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[10] = 
{
	HideFlags_t1436803931::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (Object_t3071478659), sizeof(Object_t3071478659_marshaled_pinvoke), sizeof(Object_t3071478659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2070[2] = 
{
	Object_t3071478659::get_offset_of_m_CachedPtr_0(),
	Object_t3071478659_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Component_t3501516275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Light_t4202674828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (GameObject_t3674682005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (Transform_t1659122786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (Enumerator_t3875554846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[2] = 
{
	Enumerator_t3875554846::get_offset_of_outer_0(),
	Enumerator_t3875554846::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (Time_t4241968337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (Random_t3156561159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (YieldInstruction_t2048002629), sizeof(YieldInstruction_t2048002629_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (PlayerPrefsException_t3680716996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (PlayerPrefs_t1845493509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (Motion_t3026528250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (ADBannerView_t2392257822), -1, sizeof(ADBannerView_t2392257822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2082[4] = 
{
	ADBannerView_t2392257822::get_offset_of__bannerView_0(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasClicked_1(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasLoaded_2(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerFailedToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (BannerWasClickedDelegate_t3510840818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (BannerWasLoadedDelegate_t3604098244), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (BannerFailedToLoadDelegate_t2244784594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (ADInterstitialAd_t3393793052), -1, sizeof(ADInterstitialAd_t3393793052_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	ADInterstitialAd_t3393793052::get_offset_of_interstitialView_0(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasLoaded_1(),
	ADInterstitialAd_t3393793052_StaticFields::get_offset_of_onInterstitialWasViewed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (InterstitialWasLoadedDelegate_t3068494210), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (InterstitialWasViewedDelegate_t507319425), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (DeviceGeneration_t572715224)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2089[38] = 
{
	DeviceGeneration_t572715224::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (Device_t1621710112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (CalendarIdentifier_t514511185)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2091[12] = 
{
	CalendarIdentifier_t514511185::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (CalendarUnit_t3375281836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[12] = 
{
	CalendarUnit_t3375281836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (LocalNotification_t1344855248), -1, sizeof(LocalNotification_t1344855248_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2093[2] = 
{
	LocalNotification_t1344855248::get_offset_of_notificationWrapper_0(),
	LocalNotification_t1344855248_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (RemoteNotification_t1652317979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[1] = 
{
	RemoteNotification_t1652317979::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (DirectorPlayer_t2782999289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (PlayState_t1315979427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	PlayState_t1315979427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Playable_t70832698)+ sizeof (Il2CppObject), sizeof(Playable_t70832698_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[2] = 
{
	Playable_t70832698::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Playable_t70832698::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (GenericMixerPlayable_t247595084)+ sizeof (Il2CppObject), sizeof(GenericMixerPlayable_t247595084_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	GenericMixerPlayable_t247595084::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Playables_t1328382603), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
