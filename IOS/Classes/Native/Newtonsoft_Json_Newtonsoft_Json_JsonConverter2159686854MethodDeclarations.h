﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Newtonsoft.Json.JsonConverter::get_CanRead()
extern "C"  bool JsonConverter_get_CanRead_m1693179362 (JsonConverter_t2159686854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConverter::get_CanWrite()
extern "C"  bool JsonConverter_get_CanWrite_m1471606197 (JsonConverter_t2159686854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConverter::.ctor()
extern "C"  void JsonConverter__ctor_m2152927101 (JsonConverter_t2159686854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
