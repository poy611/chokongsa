﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnAllResultFail
struct OnAllResultFail_t306118534;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnAllResultFail::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAllResultFail__ctor_m505110317 (OnAllResultFail_t306118534 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResultFail::Invoke()
extern "C"  void OnAllResultFail_Invoke_m3825347015 (OnAllResultFail_t306118534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnAllResultFail::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAllResultFail_BeginInvoke_m3752994396 (OnAllResultFail_t306118534 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResultFail::EndInvoke(System.IAsyncResult)
extern "C"  void OnAllResultFail_EndInvoke_m4113526717 (OnAllResultFail_t306118534 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
