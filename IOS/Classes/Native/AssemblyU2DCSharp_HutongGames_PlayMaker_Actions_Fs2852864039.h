﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1093022331.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase
struct  FsmStateActionAnimatorBase_t2852864039  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase/AnimatorFrameUpdateSelector HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::everyFrameOption
	int32_t ___everyFrameOption_12;
	// System.Int32 HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::IklayerIndex
	int32_t ___IklayerIndex_13;

public:
	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t2852864039, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_everyFrameOption_12() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t2852864039, ___everyFrameOption_12)); }
	inline int32_t get_everyFrameOption_12() const { return ___everyFrameOption_12; }
	inline int32_t* get_address_of_everyFrameOption_12() { return &___everyFrameOption_12; }
	inline void set_everyFrameOption_12(int32_t value)
	{
		___everyFrameOption_12 = value;
	}

	inline static int32_t get_offset_of_IklayerIndex_13() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t2852864039, ___IklayerIndex_13)); }
	inline int32_t get_IklayerIndex_13() const { return ___IklayerIndex_13; }
	inline int32_t* get_address_of_IklayerIndex_13() { return &___IklayerIndex_13; }
	inline void set_IklayerIndex_13(int32_t value)
	{
		___IklayerIndex_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
