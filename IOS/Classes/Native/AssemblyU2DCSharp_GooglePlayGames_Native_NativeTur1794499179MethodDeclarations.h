﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey8C
struct U3CBridgeMatchToUserCallbackU3Ec__AnonStorey8C_t1794499179;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey8C::.ctor()
extern "C"  void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey8C__ctor_m2668672656 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey8C_t1794499179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey8C::<>m__7B(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey8C_U3CU3Em__7B_m1439846399 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey8C_t1794499179 * __this, TurnBasedMatchResponse_t4255652325 * ___callbackResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
