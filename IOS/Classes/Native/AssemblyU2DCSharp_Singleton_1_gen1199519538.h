﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GPGSMng
struct GPGSMng_t946704145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GPGSMng>
struct  Singleton_1_t1199519538  : public MonoBehaviour_t667441552
{
public:

public:
};

struct Singleton_1_t1199519538_StaticFields
{
public:
	// T Singleton`1::instance
	GPGSMng_t946704145 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1199519538_StaticFields, ___instance_2)); }
	inline GPGSMng_t946704145 * get_instance_2() const { return ___instance_2; }
	inline GPGSMng_t946704145 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GPGSMng_t946704145 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
