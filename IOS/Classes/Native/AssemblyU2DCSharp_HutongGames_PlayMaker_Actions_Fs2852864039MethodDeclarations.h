﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase
struct FsmStateActionAnimatorBase_t2852864039;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::.ctor()
extern "C"  void FsmStateActionAnimatorBase__ctor_m1231659039 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::Reset()
extern "C"  void FsmStateActionAnimatorBase_Reset_m3173059276 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnPreprocess()
extern "C"  void FsmStateActionAnimatorBase_OnPreprocess_m3740592176 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnUpdate()
extern "C"  void FsmStateActionAnimatorBase_OnUpdate_m2200803533 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::DoAnimatorMove()
extern "C"  void FsmStateActionAnimatorBase_DoAnimatorMove_m29928200 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::DoAnimatorIK(System.Int32)
extern "C"  void FsmStateActionAnimatorBase_DoAnimatorIK_m4212416842 (FsmStateActionAnimatorBase_t2852864039 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
