﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkViewIsMine
struct NetworkViewIsMine_t3054165182;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::.ctor()
extern "C"  void NetworkViewIsMine__ctor_m3453630008 (NetworkViewIsMine_t3054165182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::_getNetworkView()
extern "C"  void NetworkViewIsMine__getNetworkView_m1783293202 (NetworkViewIsMine_t3054165182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::Reset()
extern "C"  void NetworkViewIsMine_Reset_m1100062949 (NetworkViewIsMine_t3054165182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::OnEnter()
extern "C"  void NetworkViewIsMine_OnEnter_m2615413775 (NetworkViewIsMine_t3054165182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::checkIsMine()
extern "C"  void NetworkViewIsMine_checkIsMine_m705472059 (NetworkViewIsMine_t3054165182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
