﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_Category1962353414.h"
#include "System_System_Text_RegularExpressions_CategoryUtils734718049.h"
#include "System_System_Text_RegularExpressions_LinkRef378484359.h"
#include "System_System_Text_RegularExpressions_InterpreterF3485052664.h"
#include "System_System_Text_RegularExpressions_PatternCompi3989440925.h"
#include "System_System_Text_RegularExpressions_PatternCompi3356653547.h"
#include "System_System_Text_RegularExpressions_PatternCompil396179390.h"
#include "System_System_Text_RegularExpressions_LinkStack1760044604.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "System_System_Text_RegularExpressions_Interpreter4223808840.h"
#include "System_System_Text_RegularExpressions_Interpreter_3630763131.h"
#include "System_System_Text_RegularExpressions_Interpreter_1764265010.h"
#include "System_System_Text_RegularExpressions_Interpreter_1939935045.h"
#include "System_System_Text_RegularExpressions_Interval2482260685.h"
#include "System_System_Text_RegularExpressions_IntervalColl1888974603.h"
#include "System_System_Text_RegularExpressions_IntervalColl3676602947.h"
#include "System_System_Text_RegularExpressions_IntervalColl1292950321.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse3926544077.h"
#include "System_System_Text_RegularExpressions_QuickSearch2109051075.h"
#include "System_System_Text_RegularExpressions_ReplacementEv697641157.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre2279826820.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3183027782.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo2628834993.h"
#include "System_System_Text_RegularExpressions_Syntax_Group3733269553.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul1862766086.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu2867563114.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan1061768724.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBa3370744674.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet2377834527.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser3981028276.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3434259338.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre3534504316.h"
#include "System_System_Text_RegularExpressions_Syntax_Alter3434519311.h"
#include "System_System_Text_RegularExpressions_Syntax_Liter2061497825.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit3788287627.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1741476861.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs2734841617.h"
#include "System_System_Text_RegularExpressions_Syntax_Chara2058232957.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho3681078449.h"
#include "System_System_DefaultUriParser3145002206.h"
#include "System_System_GenericUriParser444686856.h"
#include "System_System_Uri1116831938.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "System_System_UriFormatException308538560.h"
#include "System_System_UriHostNameType959572879.h"
#include "System_System_UriKind238866934.h"
#include "System_System_UriParser3685110593.h"
#include "System_System_UriPartial875461417.h"
#include "System_System_UriTypeConverter2523083502.h"
#include "System_System_ComponentModel_PropertyChangedEventHa950507765.h"
#include "System_System_ComponentModel_RefreshEventHandler1536895921.h"
#include "System_System_Net_Security_LocalCertificateSelecti2431285719.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "System_System_Net_BindIPEndPoint3006124499.h"
#include "System_System_Net_DownloadProgressChangedEventHand1678450121.h"
#include "System_System_Net_HttpContinueDelegate1707598350.h"
#include "System_System_Text_RegularExpressions_MatchEvaluat1719977010.h"
#include "System_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220352.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1676615740.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220348.h"
#include "System_Core_U3CModuleU3E86524790.h"
#include "System_Core_System_Runtime_CompilerServices_Extens2299149759.h"
#include "System_Core_Locale2281372282.h"
#include "System_Core_System_MonoTODOAttribute2091695241.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTra131863657.h"
#include "System_Core_System_Linq_Check10677726.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "System_Core_System_Linq_Enumerable_Fallback3964967226.h"
#include "System_Core_System_Linq_SortDirection313822039.h"
#include "System_Core_System_Security_Cryptography_Aes2466798581.h"
#include "System_Core_System_Security_Cryptography_AesManaged23175804.h"
#include "System_Core_System_Security_Cryptography_AesTransf1787635017.h"
#include "System_Core_System_Action3771233898.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (Category_t1962353414)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1300[146] = 
{
	Category_t1962353414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (CategoryUtils_t734718049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (LinkRef_t378484359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (InterpreterFactory_t3485052664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[4] = 
{
	InterpreterFactory_t3485052664::get_offset_of_mapping_0(),
	InterpreterFactory_t3485052664::get_offset_of_pattern_1(),
	InterpreterFactory_t3485052664::get_offset_of_namesMapping_2(),
	InterpreterFactory_t3485052664::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (PatternCompiler_t3989440925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[1] = 
{
	PatternCompiler_t3989440925::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (PatternLinkStack_t3356653547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1306[1] = 
{
	PatternLinkStack_t3356653547::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (Link_t396179390)+ sizeof (Il2CppObject), sizeof(Link_t396179390_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1307[2] = 
{
	Link_t396179390::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t396179390::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (LinkStack_t1760044604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	LinkStack_t1760044604::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (Mark_t3811539797)+ sizeof (Il2CppObject), sizeof(Mark_t3811539797_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1309[3] = 
{
	Mark_t3811539797::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3811539797::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (Interpreter_t4223808840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1310[16] = 
{
	Interpreter_t4223808840::get_offset_of_program_1(),
	Interpreter_t4223808840::get_offset_of_program_start_2(),
	Interpreter_t4223808840::get_offset_of_text_3(),
	Interpreter_t4223808840::get_offset_of_text_end_4(),
	Interpreter_t4223808840::get_offset_of_group_count_5(),
	Interpreter_t4223808840::get_offset_of_match_min_6(),
	Interpreter_t4223808840::get_offset_of_qs_7(),
	Interpreter_t4223808840::get_offset_of_scan_ptr_8(),
	Interpreter_t4223808840::get_offset_of_repeat_9(),
	Interpreter_t4223808840::get_offset_of_fast_10(),
	Interpreter_t4223808840::get_offset_of_stack_11(),
	Interpreter_t4223808840::get_offset_of_deep_12(),
	Interpreter_t4223808840::get_offset_of_marks_13(),
	Interpreter_t4223808840::get_offset_of_mark_start_14(),
	Interpreter_t4223808840::get_offset_of_mark_end_15(),
	Interpreter_t4223808840::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (IntStack_t3630763131)+ sizeof (Il2CppObject), sizeof(IntStack_t3630763131_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1311[2] = 
{
	IntStack_t3630763131::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t3630763131::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (RepeatContext_t1764265010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1312[7] = 
{
	RepeatContext_t1764265010::get_offset_of_start_0(),
	RepeatContext_t1764265010::get_offset_of_min_1(),
	RepeatContext_t1764265010::get_offset_of_max_2(),
	RepeatContext_t1764265010::get_offset_of_lazy_3(),
	RepeatContext_t1764265010::get_offset_of_expr_pc_4(),
	RepeatContext_t1764265010::get_offset_of_previous_5(),
	RepeatContext_t1764265010::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (Mode_t1939935045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1313[4] = 
{
	Mode_t1939935045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (Interval_t2482260685)+ sizeof (Il2CppObject), sizeof(Interval_t2482260685_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[3] = 
{
	Interval_t2482260685::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t2482260685::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (IntervalCollection_t1888974603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1315[1] = 
{
	IntervalCollection_t1888974603::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (Enumerator_t3676602947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1316[2] = 
{
	Enumerator_t3676602947::get_offset_of_list_0(),
	Enumerator_t3676602947::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (CostDelegate_t1292950321), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (Parser_t3926544077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[6] = 
{
	Parser_t3926544077::get_offset_of_pattern_0(),
	Parser_t3926544077::get_offset_of_ptr_1(),
	Parser_t3926544077::get_offset_of_caps_2(),
	Parser_t3926544077::get_offset_of_refs_3(),
	Parser_t3926544077::get_offset_of_num_groups_4(),
	Parser_t3926544077::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (QuickSearch_t2109051075), -1, sizeof(QuickSearch_t2109051075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1319[7] = 
{
	QuickSearch_t2109051075::get_offset_of_str_0(),
	QuickSearch_t2109051075::get_offset_of_len_1(),
	QuickSearch_t2109051075::get_offset_of_ignore_2(),
	QuickSearch_t2109051075::get_offset_of_reverse_3(),
	QuickSearch_t2109051075::get_offset_of_shift_4(),
	QuickSearch_t2109051075::get_offset_of_shiftExtended_5(),
	QuickSearch_t2109051075_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (ReplacementEvaluator_t697641157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1320[4] = 
{
	ReplacementEvaluator_t697641157::get_offset_of_regex_0(),
	ReplacementEvaluator_t697641157::get_offset_of_n_pieces_1(),
	ReplacementEvaluator_t697641157::get_offset_of_pieces_2(),
	ReplacementEvaluator_t697641157::get_offset_of_replacement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (ExpressionCollection_t2279826820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (Expression_t3183027782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (CompositeExpression_t2628834993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1323[1] = 
{
	CompositeExpression_t2628834993::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (Group_t3733269553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (RegularExpression_t1862766086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1325[1] = 
{
	RegularExpression_t1862766086::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (CapturingGroup_t2867563114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1326[2] = 
{
	CapturingGroup_t2867563114::get_offset_of_gid_1(),
	CapturingGroup_t2867563114::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (BalancingGroup_t1061768724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[1] = 
{
	BalancingGroup_t1061768724::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (NonBacktrackingGroup_t3370744674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (Repetition_t2377834527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1329[3] = 
{
	Repetition_t2377834527::get_offset_of_min_1(),
	Repetition_t2377834527::get_offset_of_max_2(),
	Repetition_t2377834527::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (Assertion_t3981028276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (CaptureAssertion_t3434259338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[3] = 
{
	CaptureAssertion_t3434259338::get_offset_of_alternate_1(),
	CaptureAssertion_t3434259338::get_offset_of_group_2(),
	CaptureAssertion_t3434259338::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (ExpressionAssertion_t3534504316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1332[2] = 
{
	ExpressionAssertion_t3534504316::get_offset_of_reverse_1(),
	ExpressionAssertion_t3534504316::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (Alternation_t3434519311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (Literal_t2061497825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[2] = 
{
	Literal_t2061497825::get_offset_of_str_0(),
	Literal_t2061497825::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (PositionAssertion_t3788287627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[1] = 
{
	PositionAssertion_t3788287627::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (Reference_t1741476861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[2] = 
{
	Reference_t1741476861::get_offset_of_group_0(),
	Reference_t1741476861::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (BackslashNumber_t2734841617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1337[2] = 
{
	BackslashNumber_t2734841617::get_offset_of_literal_2(),
	BackslashNumber_t2734841617::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (CharacterClass_t2058232957), -1, sizeof(CharacterClass_t2058232957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1338[6] = 
{
	CharacterClass_t2058232957_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t2058232957::get_offset_of_negate_1(),
	CharacterClass_t2058232957::get_offset_of_ignore_2(),
	CharacterClass_t2058232957::get_offset_of_pos_cats_3(),
	CharacterClass_t2058232957::get_offset_of_neg_cats_4(),
	CharacterClass_t2058232957::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (AnchorInfo_t3681078449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1339[6] = 
{
	AnchorInfo_t3681078449::get_offset_of_expr_0(),
	AnchorInfo_t3681078449::get_offset_of_pos_1(),
	AnchorInfo_t3681078449::get_offset_of_offset_2(),
	AnchorInfo_t3681078449::get_offset_of_str_3(),
	AnchorInfo_t3681078449::get_offset_of_width_4(),
	AnchorInfo_t3681078449::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (DefaultUriParser_t3145002206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (GenericUriParser_t444686856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (Uri_t1116831938), -1, sizeof(Uri_t1116831938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1342[36] = 
{
	Uri_t1116831938::get_offset_of_isUnixFilePath_0(),
	Uri_t1116831938::get_offset_of_source_1(),
	Uri_t1116831938::get_offset_of_scheme_2(),
	Uri_t1116831938::get_offset_of_host_3(),
	Uri_t1116831938::get_offset_of_port_4(),
	Uri_t1116831938::get_offset_of_path_5(),
	Uri_t1116831938::get_offset_of_query_6(),
	Uri_t1116831938::get_offset_of_fragment_7(),
	Uri_t1116831938::get_offset_of_userinfo_8(),
	Uri_t1116831938::get_offset_of_isUnc_9(),
	Uri_t1116831938::get_offset_of_isOpaquePart_10(),
	Uri_t1116831938::get_offset_of_isAbsoluteUri_11(),
	Uri_t1116831938::get_offset_of_userEscaped_12(),
	Uri_t1116831938::get_offset_of_cachedAbsoluteUri_13(),
	Uri_t1116831938::get_offset_of_cachedToString_14(),
	Uri_t1116831938::get_offset_of_cachedLocalPath_15(),
	Uri_t1116831938::get_offset_of_cachedHashCode_16(),
	Uri_t1116831938_StaticFields::get_offset_of_hexUpperChars_17(),
	Uri_t1116831938_StaticFields::get_offset_of_SchemeDelimiter_18(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFile_19(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeFtp_20(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeGopher_21(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttp_22(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeHttps_23(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeMailto_24(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNews_25(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNntp_26(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetPipe_27(),
	Uri_t1116831938_StaticFields::get_offset_of_UriSchemeNetTcp_28(),
	Uri_t1116831938_StaticFields::get_offset_of_schemes_29(),
	Uri_t1116831938::get_offset_of_parser_30(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map12_31(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_32(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_33(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_34(),
	Uri_t1116831938_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (UriScheme_t1290668975)+ sizeof (Il2CppObject), sizeof(UriScheme_t1290668975_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1343[3] = 
{
	UriScheme_t1290668975::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t1290668975::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (UriFormatException_t308538560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (UriHostNameType_t959572879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1345[6] = 
{
	UriHostNameType_t959572879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (UriKind_t238866934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1346[4] = 
{
	UriKind_t238866934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (UriParser_t3685110593), -1, sizeof(UriParser_t3685110593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1347[6] = 
{
	UriParser_t3685110593_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t3685110593_StaticFields::get_offset_of_table_1(),
	UriParser_t3685110593::get_offset_of_scheme_name_2(),
	UriParser_t3685110593::get_offset_of_default_port_3(),
	UriParser_t3685110593_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t3685110593_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (UriPartial_t875461417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1348[5] = 
{
	UriPartial_t875461417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (UriTypeConverter_t2523083502), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (PropertyChangedEventHandler_t950507765), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (RefreshEventHandler_t1536895921), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (LocalCertificateSelectionCallback_t2431285719), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (RemoteCertificateValidationCallback_t1894914657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (BindIPEndPoint_t3006124499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (DownloadProgressChangedEventHandler_t1678450121), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (HttpContinueDelegate_t1707598350), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (MatchEvaluator_t1719977010), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238935), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1358[4] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D2_1(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D3_2(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (U24ArrayTypeU2416_t3379220354)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220354_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (U24ArrayTypeU24128_t1676615741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1676615741_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (U24ArrayTypeU2412_t3379220350)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220350_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (U3CModuleU3E_t86524793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (ExtensionAttribute_t2299149759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (Locale_t2281372285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (MonoTODOAttribute_t2091695243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (KeyBuilder_t373726642), -1, sizeof(KeyBuilder_t373726642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1366[1] = 
{
	KeyBuilder_t373726642_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (SymmetricTransform_t131863658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1367[12] = 
{
	SymmetricTransform_t131863658::get_offset_of_algo_0(),
	SymmetricTransform_t131863658::get_offset_of_encrypt_1(),
	SymmetricTransform_t131863658::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t131863658::get_offset_of_temp_3(),
	SymmetricTransform_t131863658::get_offset_of_temp2_4(),
	SymmetricTransform_t131863658::get_offset_of_workBuff_5(),
	SymmetricTransform_t131863658::get_offset_of_workout_6(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t131863658::get_offset_of_m_disposed_9(),
	SymmetricTransform_t131863658::get_offset_of_lastBlock_10(),
	SymmetricTransform_t131863658::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1368[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1371[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (Check_t10677726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (Enumerable_t839044124), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (Fallback_t3964967226)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1374[3] = 
{
	Fallback_t3964967226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1375[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1376[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1377[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1378[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1379[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1380[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1381[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1382[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1383[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1384[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1385[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1386[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1390[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1392[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1393[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (SortDirection_t313822039)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1394[3] = 
{
	SortDirection_t313822039::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1395[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (Aes_t2466798581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (AesManaged_t23175804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (AesTransform_t1787635017), -1, sizeof(AesTransform_t1787635017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1398[14] = 
{
	AesTransform_t1787635017::get_offset_of_expandedKey_12(),
	AesTransform_t1787635017::get_offset_of_Nk_13(),
	AesTransform_t1787635017::get_offset_of_Nr_14(),
	AesTransform_t1787635017_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t1787635017_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T0_18(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T1_19(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T2_20(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T3_21(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (Action_t3771233898), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
