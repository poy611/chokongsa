﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllInvitations>c__AnonStorey8A
struct U3CGetAllInvitationsU3Ec__AnonStorey8A_t2725428433;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t3460122515;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3460122515.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllInvitations>c__AnonStorey8A::.ctor()
extern "C"  void U3CGetAllInvitationsU3Ec__AnonStorey8A__ctor_m794937066 (U3CGetAllInvitationsU3Ec__AnonStorey8A_t2725428433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllInvitations>c__AnonStorey8A::<>m__79(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse)
extern "C"  void U3CGetAllInvitationsU3Ec__AnonStorey8A_U3CU3Em__79_m715428962 (U3CGetAllInvitationsU3Ec__AnonStorey8A_t2725428433 * __this, TurnBasedMatchesResponse_t3460122515 * ___allMatches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
