﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<NetworkMng>::.ctor()
#define Singleton_1__ctor_m631010213(__this, method) ((  void (*) (Singleton_1_t1768030745 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// System.Void Singleton`1<NetworkMng>::.cctor()
#define Singleton_1__cctor_m1899351208(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m1977804114_gshared)(__this /* static, unused */, method)
// T Singleton`1<NetworkMng>::get_GetInstance()
#define Singleton_1_get_GetInstance_m207825188(__this /* static, unused */, method) ((  NetworkMng_t1515215352 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_GetInstance_m102549980_gshared)(__this /* static, unused */, method)
