﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Dictionary_2_t4114722875;
// System.Collections.Generic.IEqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IEqualityComparer_1_t3762879195;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct IDictionary_2_t3692596220;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct ICollection_1_t3866434778;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>[]
struct KeyValuePair_2U5BU5D_t669296016;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>
struct IEnumerator_1_t1630401334;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct KeyCollection_t1446515030;
// System.Collections.Generic.Dictionary`2/ValueCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ValueCollection_t2815328588;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1137078971.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2084123012_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2084123012(__this, method) ((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2__ctor_m2084123012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3844689403_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3844689403(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3844689403_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3236980372_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3236980372(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3236980372_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m353681621_gshared (Dictionary_2_t4114722875 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m353681621(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t4114722875 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m353681621_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3277098473_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3277098473(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3277098473_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m154114632_gshared (Dictionary_2_t4114722875 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m154114632(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t4114722875 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m154114632_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m869344453_gshared (Dictionary_2_t4114722875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m869344453(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4114722875 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m869344453_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2726194004_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2726194004(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2726194004_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2021106882_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2021106882(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2021106882_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2940493248_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2940493248(__this, method) ((  bool (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2940493248_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3318449914_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3318449914(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3318449914_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1925568041_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1925568041(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1925568041_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3992679048_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3992679048(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3992679048_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m840901418_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m840901418(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m840901418_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2681455207_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2681455207(__this, ___key0, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2681455207_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2935450730_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2935450730(__this, method) ((  bool (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2935450730_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3728760668_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3728760668(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3728760668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m549661742_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m549661742(__this, method) ((  bool (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m549661742_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4045524861_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2_t4013503581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4045524861(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t4114722875 *, KeyValuePair_2_t4013503581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4045524861_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3280984233_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2_t4013503581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3280984233(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4114722875 *, KeyValuePair_2_t4013503581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3280984233_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1228585505_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2U5BU5D_t669296016* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1228585505(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4114722875 *, KeyValuePair_2U5BU5D_t669296016*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1228585505_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1801328142_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2_t4013503581  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1801328142(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t4114722875 *, KeyValuePair_2_t4013503581 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1801328142_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m14743616_gshared (Dictionary_2_t4114722875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14743616(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14743616_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2601697871_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2601697871(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2601697871_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919255750_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919255750(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919255750_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3558245651_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3558245651(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3558245651_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m4125988772_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m4125988772(__this, method) ((  int32_t (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_get_Count_m4125988772_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2367844707_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2367844707(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , const MethodInfo*))Dictionary_2_get_Item_m2367844707_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1491497028_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1491497028(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1491497028_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1428154620_gshared (Dictionary_2_t4114722875 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1428154620(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t4114722875 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1428154620_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m719430203_gshared (Dictionary_2_t4114722875 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m719430203(__this, ___size0, method) ((  void (*) (Dictionary_2_t4114722875 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m719430203_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1735555831_gshared (Dictionary_2_t4114722875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1735555831(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1735555831_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t4013503581  Dictionary_2_make_pair_m1821195659_gshared (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1821195659(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4013503581  (*) (Il2CppObject * /* static, unused */, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1821195659_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::pick_key(TKey,TValue)
extern "C"  TypeNameKey_t2971844791  Dictionary_2_pick_key_m1105444819_gshared (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1105444819(__this /* static, unused */, ___key0, ___value1, method) ((  TypeNameKey_t2971844791  (*) (Il2CppObject * /* static, unused */, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1105444819_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m340350575_gshared (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m340350575(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m340350575_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1221951736_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2U5BU5D_t669296016* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1221951736(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t4114722875 *, KeyValuePair_2U5BU5D_t669296016*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1221951736_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m241100596_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m241100596(__this, method) ((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_Resize_m241100596_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2759030705_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2759030705(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2759030705_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3785223599_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3785223599(__this, method) ((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_Clear_m3785223599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1977940121_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1977940121(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , const MethodInfo*))Dictionary_2_ContainsKey_m1977940121_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2272286745_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2272286745(__this, ___value0, method) ((  bool (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2272286745_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m190306082_gshared (Dictionary_2_t4114722875 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m190306082(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t4114722875 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m190306082_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2934090882_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2934090882(__this, ___sender0, method) ((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2934090882_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3544614071_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3544614071(__this, ___key0, method) ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , const MethodInfo*))Dictionary_2_Remove_m3544614071_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1239394290_gshared (Dictionary_2_t4114722875 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1239394290(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1239394290_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Keys()
extern "C"  KeyCollection_t1446515030 * Dictionary_2_get_Keys_m4293065705_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4293065705(__this, method) ((  KeyCollection_t1446515030 * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_get_Keys_m4293065705_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Values()
extern "C"  ValueCollection_t2815328588 * Dictionary_2_get_Values_m1529039557_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1529039557(__this, method) ((  ValueCollection_t2815328588 * (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_get_Values_m1529039557_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToTKey(System.Object)
extern "C"  TypeNameKey_t2971844791  Dictionary_2_ToTKey_m555303726_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m555303726(__this, ___key0, method) ((  TypeNameKey_t2971844791  (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m555303726_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2947632010_gshared (Dictionary_2_t4114722875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2947632010(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t4114722875 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2947632010_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m858159770_gshared (Dictionary_2_t4114722875 * __this, KeyValuePair_2_t4013503581  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m858159770(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t4114722875 *, KeyValuePair_2_t4013503581 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m858159770_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1137078971  Dictionary_2_GetEnumerator_m1334213007_gshared (Dictionary_2_t4114722875 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1334213007(__this, method) ((  Enumerator_t1137078971  (*) (Dictionary_2_t4114722875 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1334213007_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m709646406_gshared (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m709646406(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m709646406_gshared)(__this /* static, unused */, ___key0, ___value1, method)
