﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t3069007197;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::.ctor(System.IntPtr)
extern "C"  void TurnBasedMatchConfig__ctor_m2267763325 (TurnBasedMatchConfig_t3069007197 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdAtIndex(System.UIntPtr)
extern "C"  String_t* TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406 (TurnBasedMatchConfig_t3069007197 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdsToInvite()
extern "C"  Il2CppObject* TurnBasedMatchConfig_PlayerIdsToInvite_m1157952936 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::Variant()
extern "C"  uint32_t TurnBasedMatchConfig_Variant_m4237649269 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::ExclusiveBitMask()
extern "C"  int64_t TurnBasedMatchConfig_ExclusiveBitMask_m1648745241 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MinimumAutomatchingPlayers()
extern "C"  uint32_t TurnBasedMatchConfig_MinimumAutomatchingPlayers_m442887146 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MaximumAutomatchingPlayers()
extern "C"  uint32_t TurnBasedMatchConfig_MaximumAutomatchingPlayers_m1650107672 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfig_CallDispose_m2489810643 (TurnBasedMatchConfig_t3069007197 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
