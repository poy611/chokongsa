﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1707094285(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2433494851 *, Type_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m853598568_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
#define KeyValuePair_2_get_Key_m2637520827(__this, method) ((  Type_t * (*) (KeyValuePair_2_t2433494851 *, const MethodInfo*))KeyValuePair_2_get_Key_m324263168_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1462629628(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2433494851 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m4226889665_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
#define KeyValuePair_2_get_Value_m2297951903(__this, method) ((  int32_t (*) (KeyValuePair_2_t2433494851 *, const MethodInfo*))KeyValuePair_2_get_Value_m552188452_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1766746876(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2433494851 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2709046081_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
#define KeyValuePair_2_ToString_m392418572(__this, method) ((  String_t* (*) (KeyValuePair_2_t2433494851 *, const MethodInfo*))KeyValuePair_2_ToString_m4212323239_gshared)(__this, method)
