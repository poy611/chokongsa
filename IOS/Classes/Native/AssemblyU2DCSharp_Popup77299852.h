﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;
// ConstforMinigame
struct ConstforMinigame_t2789090703;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PP_popupIndex3545488117.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Popup
struct  Popup_t77299852  : public MonoBehaviour_t667441552
{
public:
	// PP/popupIndex Popup::state
	int32_t ___state_2;
	// System.Int32 Popup::reverse
	int32_t ___reverse_3;
	// System.Int32 Popup::nowNum
	int32_t ___nowNum_4;
	// System.String Popup::nowScene
	String_t* ___nowScene_5;
	// StructforMinigame[] Popup::stuff
	StructforMinigameU5BU5D_t1843399504* ___stuff_6;
	// ConstforMinigame Popup::cuff
	ConstforMinigame_t2789090703 * ___cuff_7;
	// System.Boolean Popup::okClick
	bool ___okClick_8;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___reverse_3)); }
	inline int32_t get_reverse_3() const { return ___reverse_3; }
	inline int32_t* get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(int32_t value)
	{
		___reverse_3 = value;
	}

	inline static int32_t get_offset_of_nowNum_4() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___nowNum_4)); }
	inline int32_t get_nowNum_4() const { return ___nowNum_4; }
	inline int32_t* get_address_of_nowNum_4() { return &___nowNum_4; }
	inline void set_nowNum_4(int32_t value)
	{
		___nowNum_4 = value;
	}

	inline static int32_t get_offset_of_nowScene_5() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___nowScene_5)); }
	inline String_t* get_nowScene_5() const { return ___nowScene_5; }
	inline String_t** get_address_of_nowScene_5() { return &___nowScene_5; }
	inline void set_nowScene_5(String_t* value)
	{
		___nowScene_5 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_5, value);
	}

	inline static int32_t get_offset_of_stuff_6() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___stuff_6)); }
	inline StructforMinigameU5BU5D_t1843399504* get_stuff_6() const { return ___stuff_6; }
	inline StructforMinigameU5BU5D_t1843399504** get_address_of_stuff_6() { return &___stuff_6; }
	inline void set_stuff_6(StructforMinigameU5BU5D_t1843399504* value)
	{
		___stuff_6 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_6, value);
	}

	inline static int32_t get_offset_of_cuff_7() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___cuff_7)); }
	inline ConstforMinigame_t2789090703 * get_cuff_7() const { return ___cuff_7; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_7() { return &___cuff_7; }
	inline void set_cuff_7(ConstforMinigame_t2789090703 * value)
	{
		___cuff_7 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_7, value);
	}

	inline static int32_t get_offset_of_okClick_8() { return static_cast<int32_t>(offsetof(Popup_t77299852, ___okClick_8)); }
	inline bool get_okClick_8() const { return ___okClick_8; }
	inline bool* get_address_of_okClick_8() { return &___okClick_8; }
	inline void set_okClick_8(bool value)
	{
		___okClick_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
