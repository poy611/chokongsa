﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En567139676MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4242215098(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2816364073 *, Dictionary_2_t1499040681 *, const MethodInfo*))Enumerator__ctor_m3269824848_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2772990321(__this, method) ((  Il2CppObject * (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3227461403_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m309084091(__this, method) ((  void (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3877227877_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4267486770(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4254063836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2289638797(__this, method) ((  Il2CppObject * (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m838689719_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2368064735(__this, method) ((  Il2CppObject * (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3870371977_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::MoveNext()
#define Enumerator_MoveNext_m621626859(__this, method) ((  bool (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_MoveNext_m3675212181_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Current()
#define Enumerator_get_Current_m4186969329(__this, method) ((  KeyValuePair_2_t1397821387  (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_get_Current_m3503833607_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3494966836(__this, method) ((  Fsm_t1527112426 * (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_get_CurrentKey_m1259765598_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m58137972(__this, method) ((  RaycastHit2D_t1374744384  (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_get_CurrentValue_m2877790494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::Reset()
#define Enumerator_Reset_m1370829836(__this, method) ((  void (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_Reset_m2383229858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::VerifyState()
#define Enumerator_VerifyState_m3873196821(__this, method) ((  void (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_VerifyState_m2581578283_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1150371133(__this, method) ((  void (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_VerifyCurrent_m1150504659_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::Dispose()
#define Enumerator_Dispose_m1335941596(__this, method) ((  void (*) (Enumerator_t2816364073 *, const MethodInfo*))Enumerator_Dispose_m3589753842_gshared)(__this, method)
