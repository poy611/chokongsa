﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRoot
struct  GetRoot_t1738725734  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetRoot::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRoot::storeRoot
	FsmGameObject_t1697147867 * ___storeRoot_12;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GetRoot_t1738725734, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_storeRoot_12() { return static_cast<int32_t>(offsetof(GetRoot_t1738725734, ___storeRoot_12)); }
	inline FsmGameObject_t1697147867 * get_storeRoot_12() const { return ___storeRoot_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeRoot_12() { return &___storeRoot_12; }
	inline void set_storeRoot_12(FsmGameObject_t1697147867 * value)
	{
		___storeRoot_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeRoot_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
