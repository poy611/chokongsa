﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakePosition
struct iTweenShakePosition_t1348015515;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::.ctor()
extern "C"  void iTweenShakePosition__ctor_m201690107 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::Reset()
extern "C"  void iTweenShakePosition_Reset_m2143090344 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnEnter()
extern "C"  void iTweenShakePosition_OnEnter_m4237360402 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnExit()
extern "C"  void iTweenShakePosition_OnExit_m3470744646 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::DoiTween()
extern "C"  void iTweenShakePosition_DoiTween_m3960285814 (iTweenShakePosition_t1348015515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
