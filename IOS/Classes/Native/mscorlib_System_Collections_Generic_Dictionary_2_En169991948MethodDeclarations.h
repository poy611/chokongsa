﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1519612774MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2088700703(__this, ___dictionary0, method) ((  void (*) (Enumerator_t169991948 *, Dictionary_2_t3147635852 *, const MethodInfo*))Enumerator__ctor_m2159893197_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3055853346(__this, method) ((  Il2CppObject * (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2266147124_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1670836278(__this, method) ((  void (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3658641352_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3079664511(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3550727057_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1578400382(__this, method) ((  Il2CppObject * (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4211289872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1767747984(__this, method) ((  Il2CppObject * (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2238810530_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
#define Enumerator_MoveNext_m4284450722(__this, method) ((  bool (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_MoveNext_m2906371636_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
#define Enumerator_get_Current_m3661215694(__this, method) ((  KeyValuePair_2_t3046416558  (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_get_Current_m604579836_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2652867695(__this, method) ((  String_t* (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_get_CurrentKey_m15664513_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m776589267(__this, method) ((  int32_t (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_get_CurrentValue_m455036005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Reset()
#define Enumerator_Reset_m1361929969(__this, method) ((  void (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_Reset_m1185419679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyState()
#define Enumerator_VerifyState_m3050962490(__this, method) ((  void (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_VerifyState_m4289293928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1257161506(__this, method) ((  void (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_VerifyCurrent_m1587732432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
#define Enumerator_Dispose_m1373104001(__this, method) ((  void (*) (Enumerator_t169991948 *, const MethodInfo*))Enumerator_Dispose_m3545407151_gshared)(__this, method)
