﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Bidirect1375314390MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
#define BidirectionalDictionary_2__ctor_m2161201716(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method) ((  void (*) (BidirectionalDictionary_2_t25693564 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))BidirectionalDictionary_2__ctor_m2565085922_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
#define BidirectionalDictionary_2__ctor_m3119903660(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, ___duplicateFirstErrorMessage2, ___duplicateSecondErrorMessage3, method) ((  void (*) (BidirectionalDictionary_2_t25693564 *, Il2CppObject*, Il2CppObject*, String_t*, String_t*, const MethodInfo*))BidirectionalDictionary_2__ctor_m1641679706_gshared)(__this, ___firstEqualityComparer0, ___secondEqualityComparer1, ___duplicateFirstErrorMessage2, ___duplicateSecondErrorMessage3, method)
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::Set(TFirst,TSecond)
#define BidirectionalDictionary_2_Set_m3015213672(__this, ___first0, ___second1, method) ((  void (*) (BidirectionalDictionary_2_t25693564 *, String_t*, Il2CppObject *, const MethodInfo*))BidirectionalDictionary_2_Set_m2800109206_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::TryGetByFirst(TFirst,TSecond&)
#define BidirectionalDictionary_2_TryGetByFirst_m511475570(__this, ___first0, ___second1, method) ((  bool (*) (BidirectionalDictionary_2_t25693564 *, String_t*, Il2CppObject **, const MethodInfo*))BidirectionalDictionary_2_TryGetByFirst_m3250057860_gshared)(__this, ___first0, ___second1, method)
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>::TryGetBySecond(TSecond,TFirst&)
#define BidirectionalDictionary_2_TryGetBySecond_m2622622616(__this, ___second0, ___first1, method) ((  bool (*) (BidirectionalDictionary_2_t25693564 *, Il2CppObject *, String_t**, const MethodInfo*))BidirectionalDictionary_2_TryGetBySecond_m1619327686_gshared)(__this, ___second0, ___first1, method)
