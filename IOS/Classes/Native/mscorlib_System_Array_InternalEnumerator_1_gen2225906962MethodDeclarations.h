﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2225906962.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2938744211_gshared (InternalEnumerator_1_t2225906962 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2938744211(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2225906962 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2938744211_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845(__this, method) ((  void (*) (InternalEnumerator_1_t2225906962 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2225906962 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4035216170_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4035216170(__this, method) ((  void (*) (InternalEnumerator_1_t2225906962 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4035216170_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3315677917_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3315677917(__this, method) ((  bool (*) (InternalEnumerator_1_t2225906962 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3315677917_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::get_Current()
extern "C"  KeyValuePair_2_t3443564286  InternalEnumerator_1_get_Current_m332770876_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m332770876(__this, method) ((  KeyValuePair_2_t3443564286  (*) (InternalEnumerator_1_t2225906962 *, const MethodInfo*))InternalEnumerator_1_get_Current_m332770876_gshared)(__this, method)
