﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>
struct U3CU3Ec_t1205074931;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::.cctor()
extern "C"  void U3CU3Ec__cctor_m3523957673_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define U3CU3Ec__cctor_m3523957673(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))U3CU3Ec__cctor_m3523957673_gshared)(__this /* static, unused */, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::.ctor()
extern "C"  void U3CU3Ec__ctor_m3731458180_gshared (U3CU3Ec_t1205074931 * __this, const MethodInfo* method);
#define U3CU3Ec__ctor_m3731458180(__this, method) ((  void (*) (U3CU3Ec_t1205074931 *, const MethodInfo*))U3CU3Ec__ctor_m3731458180_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::<GetEnumerator>b__25_0(System.Collections.DictionaryEntry)
extern "C"  KeyValuePair_2_t1944668977  U3CU3Ec_U3CGetEnumeratorU3Eb__25_0_m329333814_gshared (U3CU3Ec_t1205074931 * __this, DictionaryEntry_t1751606614  ___de0, const MethodInfo* method);
#define U3CU3Ec_U3CGetEnumeratorU3Eb__25_0_m329333814(__this, ___de0, method) ((  KeyValuePair_2_t1944668977  (*) (U3CU3Ec_t1205074931 *, DictionaryEntry_t1751606614 , const MethodInfo*))U3CU3Ec_U3CGetEnumeratorU3Eb__25_0_m329333814_gshared)(__this, ___de0, method)
