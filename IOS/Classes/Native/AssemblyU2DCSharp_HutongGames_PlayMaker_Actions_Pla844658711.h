﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat
struct  PlayerPrefsSetFloat_t844658711  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::keys
	FsmStringU5BU5D_t2523845914* ___keys_11;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::values
	FsmFloatU5BU5D_t2945380875* ___values_12;

public:
	inline static int32_t get_offset_of_keys_11() { return static_cast<int32_t>(offsetof(PlayerPrefsSetFloat_t844658711, ___keys_11)); }
	inline FsmStringU5BU5D_t2523845914* get_keys_11() const { return ___keys_11; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_keys_11() { return &___keys_11; }
	inline void set_keys_11(FsmStringU5BU5D_t2523845914* value)
	{
		___keys_11 = value;
		Il2CppCodeGenWriteBarrier(&___keys_11, value);
	}

	inline static int32_t get_offset_of_values_12() { return static_cast<int32_t>(offsetof(PlayerPrefsSetFloat_t844658711, ___values_12)); }
	inline FsmFloatU5BU5D_t2945380875* get_values_12() const { return ___values_12; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_values_12() { return &___values_12; }
	inline void set_values_12(FsmFloatU5BU5D_t2945380875* value)
	{
		___values_12 = value;
		Il2CppCodeGenWriteBarrier(&___values_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
