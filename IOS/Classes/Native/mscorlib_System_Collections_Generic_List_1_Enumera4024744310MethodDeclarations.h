﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m273653602(__this, ___l0, method) ((  void (*) (Enumerator_t4024744310 *, List_1_t4005071540 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2626543536(__this, method) ((  void (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3723657116(__this, method) ((  Il2CppObject * (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::Dispose()
#define Enumerator_Dispose_m1400415687(__this, method) ((  void (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::VerifyState()
#define Enumerator_VerifyState_m1623599488(__this, method) ((  void (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::MoveNext()
#define Enumerator_MoveNext_m1361999644(__this, method) ((  bool (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativePlayer>::get_Current()
#define Enumerator_get_Current_m4040030519(__this, method) ((  NativePlayer_t2636885988 * (*) (Enumerator_t4024744310 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
