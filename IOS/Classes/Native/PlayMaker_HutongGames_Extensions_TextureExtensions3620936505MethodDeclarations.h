﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

// System.Void HutongGames.Extensions.TextureExtensions::FloodFillArea(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color32)
extern "C"  void TextureExtensions_FloodFillArea_m2293198327 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___aTex0, int32_t ___aX1, int32_t ___aY2, Color32_t598853688  ___aFillColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
