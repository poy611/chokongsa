﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmEnum
struct GetFsmEnum_t3246538643;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::.ctor()
extern "C"  void GetFsmEnum__ctor_m2883349043 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::Reset()
extern "C"  void GetFsmEnum_Reset_m529781984 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnEnter()
extern "C"  void GetFsmEnum_OnEnter_m36253002 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnUpdate()
extern "C"  void GetFsmEnum_OnUpdate_m257402425 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::DoGetFsmEnum()
extern "C"  void GetFsmEnum_DoGetFsmEnum_m819477575 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
