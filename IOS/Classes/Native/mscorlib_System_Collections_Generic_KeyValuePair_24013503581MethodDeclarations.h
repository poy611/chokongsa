﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1883880326_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1883880326(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4013503581 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1883880326_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  TypeNameKey_t2971844791  KeyValuePair_2_get_Key_m2652238242_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2652238242(__this, method) ((  TypeNameKey_t2971844791  (*) (KeyValuePair_2_t4013503581 *, const MethodInfo*))KeyValuePair_2_get_Key_m2652238242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m362021603_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m362021603(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4013503581 *, TypeNameKey_t2971844791 , const MethodInfo*))KeyValuePair_2_set_Key_m362021603_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3736459618_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3736459618(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4013503581 *, const MethodInfo*))KeyValuePair_2_get_Value_m3736459618_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1218260323_gshared (KeyValuePair_2_t4013503581 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1218260323(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4013503581 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1218260323_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2354625887_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2354625887(__this, method) ((  String_t* (*) (KeyValuePair_2_t4013503581 *, const MethodInfo*))KeyValuePair_2_ToString_m2354625887_gshared)(__this, method)
