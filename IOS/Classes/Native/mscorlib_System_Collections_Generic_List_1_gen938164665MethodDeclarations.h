﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t938164665;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.JsonPosition>
struct IEnumerable_1_t2870892070;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.JsonPosition>
struct IEnumerator_1_t1481844162;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.JsonPosition>
struct ICollection_1_t464569100;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>
struct ReadOnlyCollection_1_t1127056649;
// Newtonsoft.Json.JsonPosition[]
struct JsonPositionU5BU5D_t3412818772;
// System.Predicate`1<Newtonsoft.Json.JsonPosition>
struct Predicate_1_t3476003292;
// System.Collections.Generic.IComparer`1<Newtonsoft.Json.JsonPosition>
struct IComparer_1_t2144993155;
// System.Comparison`1<Newtonsoft.Json.JsonPosition>
struct Comparison_1_t2581307596;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957837435.h"

// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::.ctor()
extern "C"  void List_1__ctor_m3760283910_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1__ctor_m3760283910(__this, method) ((  void (*) (List_1_t938164665 *, const MethodInfo*))List_1__ctor_m3760283910_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m843838533_gshared (List_1_t938164665 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m843838533(__this, ___collection0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m843838533_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2516258379_gshared (List_1_t938164665 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2516258379(__this, ___capacity0, method) ((  void (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1__ctor_m2516258379_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::.cctor()
extern "C"  void List_1__cctor_m3782494963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3782494963(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3782494963_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1160463236_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1160463236(__this, method) ((  Il2CppObject* (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1160463236_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m812585610_gshared (List_1_t938164665 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m812585610(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t938164665 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m812585610_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1439507993_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1439507993(__this, method) ((  Il2CppObject * (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1439507993_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m296178116_gshared (List_1_t938164665 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m296178116(__this, ___item0, method) ((  int32_t (*) (List_1_t938164665 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m296178116_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m962151676_gshared (List_1_t938164665 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m962151676(__this, ___item0, method) ((  bool (*) (List_1_t938164665 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m962151676_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m362874396_gshared (List_1_t938164665 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m362874396(__this, ___item0, method) ((  int32_t (*) (List_1_t938164665 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m362874396_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2610234255_gshared (List_1_t938164665 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2610234255(__this, ___index0, ___item1, method) ((  void (*) (List_1_t938164665 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2610234255_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1546979641_gshared (List_1_t938164665 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1546979641(__this, ___item0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1546979641_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m241183229_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m241183229(__this, method) ((  bool (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m241183229_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3138789600_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3138789600(__this, method) ((  bool (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3138789600_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m332081874_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m332081874(__this, method) ((  Il2CppObject * (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m332081874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4043792747_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4043792747(__this, method) ((  bool (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4043792747_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2108474414_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2108474414(__this, method) ((  bool (*) (List_1_t938164665 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2108474414_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2368712665_gshared (List_1_t938164665 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2368712665(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2368712665_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m788170662_gshared (List_1_t938164665 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m788170662(__this, ___index0, ___value1, method) ((  void (*) (List_1_t938164665 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m788170662_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Add(T)
extern "C"  void List_1_Add_m3454644726_gshared (List_1_t938164665 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define List_1_Add_m3454644726(__this, ___item0, method) ((  void (*) (List_1_t938164665 *, JsonPosition_t3864946409 , const MethodInfo*))List_1_Add_m3454644726_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3295769344_gshared (List_1_t938164665 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3295769344(__this, ___newCount0, method) ((  void (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3295769344_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1487065319_gshared (List_1_t938164665 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1487065319(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t938164665 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1487065319_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3604335102_gshared (List_1_t938164665 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3604335102(__this, ___collection0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3604335102_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2865307326_gshared (List_1_t938164665 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2865307326(__this, ___enumerable0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2865307326_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m106107161_gshared (List_1_t938164665 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m106107161(__this, ___collection0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m106107161_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1127056649 * List_1_AsReadOnly_m906928844_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m906928844(__this, method) ((  ReadOnlyCollection_1_t1127056649 * (*) (List_1_t938164665 *, const MethodInfo*))List_1_AsReadOnly_m906928844_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Clear()
extern "C"  void List_1_Clear_m2669952037_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_Clear_m2669952037(__this, method) ((  void (*) (List_1_t938164665 *, const MethodInfo*))List_1_Clear_m2669952037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Contains(T)
extern "C"  bool List_1_Contains_m1543461527_gshared (List_1_t938164665 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define List_1_Contains_m1543461527(__this, ___item0, method) ((  bool (*) (List_1_t938164665 *, JsonPosition_t3864946409 , const MethodInfo*))List_1_Contains_m1543461527_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m4225731106_gshared (List_1_t938164665 * __this, JsonPositionU5BU5D_t3412818772* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m4225731106(__this, ___array0, method) ((  void (*) (List_1_t938164665 *, JsonPositionU5BU5D_t3412818772*, const MethodInfo*))List_1_CopyTo_m4225731106_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m63478261_gshared (List_1_t938164665 * __this, JsonPositionU5BU5D_t3412818772* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m63478261(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t938164665 *, JsonPositionU5BU5D_t3412818772*, int32_t, const MethodInfo*))List_1_CopyTo_m63478261_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Find(System.Predicate`1<T>)
extern "C"  JsonPosition_t3864946409  List_1_Find_m2702041201_gshared (List_1_t938164665 * __this, Predicate_1_t3476003292 * ___match0, const MethodInfo* method);
#define List_1_Find_m2702041201(__this, ___match0, method) ((  JsonPosition_t3864946409  (*) (List_1_t938164665 *, Predicate_1_t3476003292 *, const MethodInfo*))List_1_Find_m2702041201_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3663653006_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3476003292 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3663653006(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3476003292 *, const MethodInfo*))List_1_CheckMatch_m3663653006_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3286795371_gshared (List_1_t938164665 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3476003292 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3286795371(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t938164665 *, int32_t, int32_t, Predicate_1_t3476003292 *, const MethodInfo*))List_1_GetIndex_m3286795371_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::GetEnumerator()
extern "C"  Enumerator_t957837435  List_1_GetEnumerator_m3847491721_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3847491721(__this, method) ((  Enumerator_t957837435  (*) (List_1_t938164665 *, const MethodInfo*))List_1_GetEnumerator_m3847491721_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3239983233_gshared (List_1_t938164665 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3239983233(__this, ___item0, method) ((  int32_t (*) (List_1_t938164665 *, JsonPosition_t3864946409 , const MethodInfo*))List_1_IndexOf_m3239983233_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4092963660_gshared (List_1_t938164665 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4092963660(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t938164665 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4092963660_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m4104812741_gshared (List_1_t938164665 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m4104812741(__this, ___index0, method) ((  void (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_CheckIndex_m4104812741_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3208554924_gshared (List_1_t938164665 * __this, int32_t ___index0, JsonPosition_t3864946409  ___item1, const MethodInfo* method);
#define List_1_Insert_m3208554924(__this, ___index0, ___item1, method) ((  void (*) (List_1_t938164665 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))List_1_Insert_m3208554924_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2549060897_gshared (List_1_t938164665 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2549060897(__this, ___collection0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2549060897_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Remove(T)
extern "C"  bool List_1_Remove_m94884946_gshared (List_1_t938164665 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define List_1_Remove_m94884946(__this, ___item0, method) ((  bool (*) (List_1_t938164665 *, JsonPosition_t3864946409 , const MethodInfo*))List_1_Remove_m94884946_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1883060612_gshared (List_1_t938164665 * __this, Predicate_1_t3476003292 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1883060612(__this, ___match0, method) ((  int32_t (*) (List_1_t938164665 *, Predicate_1_t3476003292 *, const MethodInfo*))List_1_RemoveAll_m1883060612_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3000672486_gshared (List_1_t938164665 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3000672486(__this, ___index0, method) ((  void (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3000672486_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2884563861_gshared (List_1_t938164665 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2884563861(__this, ___index0, ___count1, method) ((  void (*) (List_1_t938164665 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2884563861_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Reverse()
extern "C"  void List_1_Reverse_m3391118778_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_Reverse_m3391118778(__this, method) ((  void (*) (List_1_t938164665 *, const MethodInfo*))List_1_Reverse_m3391118778_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Sort()
extern "C"  void List_1_Sort_m824462376_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_Sort_m824462376(__this, method) ((  void (*) (List_1_t938164665 *, const MethodInfo*))List_1_Sort_m824462376_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m714524348_gshared (List_1_t938164665 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m714524348(__this, ___comparer0, method) ((  void (*) (List_1_t938164665 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m714524348_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4176038523_gshared (List_1_t938164665 * __this, Comparison_1_t2581307596 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m4176038523(__this, ___comparison0, method) ((  void (*) (List_1_t938164665 *, Comparison_1_t2581307596 *, const MethodInfo*))List_1_Sort_m4176038523_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::ToArray()
extern "C"  JsonPositionU5BU5D_t3412818772* List_1_ToArray_m3096640467_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_ToArray_m3096640467(__this, method) ((  JsonPositionU5BU5D_t3412818772* (*) (List_1_t938164665 *, const MethodInfo*))List_1_ToArray_m3096640467_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1273212353_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1273212353(__this, method) ((  void (*) (List_1_t938164665 *, const MethodInfo*))List_1_TrimExcess_m1273212353_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4269541425_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4269541425(__this, method) ((  int32_t (*) (List_1_t938164665 *, const MethodInfo*))List_1_get_Capacity_m4269541425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m505958162_gshared (List_1_t938164665 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m505958162(__this, ___value0, method) ((  void (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_set_Capacity_m505958162_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::get_Count()
extern "C"  int32_t List_1_get_Count_m2363626382_gshared (List_1_t938164665 * __this, const MethodInfo* method);
#define List_1_get_Count_m2363626382(__this, method) ((  int32_t (*) (List_1_t938164665 *, const MethodInfo*))List_1_get_Count_m2363626382_gshared)(__this, method)
// T System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::get_Item(System.Int32)
extern "C"  JsonPosition_t3864946409  List_1_get_Item_m4235105479_gshared (List_1_t938164665 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4235105479(__this, ___index0, method) ((  JsonPosition_t3864946409  (*) (List_1_t938164665 *, int32_t, const MethodInfo*))List_1_get_Item_m4235105479_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1444360067_gshared (List_1_t938164665 * __this, int32_t ___index0, JsonPosition_t3864946409  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1444360067(__this, ___index0, ___value1, method) ((  void (*) (List_1_t938164665 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))List_1_set_Item_m1444360067_gshared)(__this, ___index0, ___value1, method)
