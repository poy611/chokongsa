﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2946749061.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Char>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3227173731_gshared (Nullable_1_t2946749061 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3227173731(__this, ___value0, method) ((  void (*) (Nullable_1_t2946749061 *, Il2CppChar, const MethodInfo*))Nullable_1__ctor_m3227173731_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Char>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4110133180_gshared (Nullable_1_t2946749061 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m4110133180(__this, method) ((  bool (*) (Nullable_1_t2946749061 *, const MethodInfo*))Nullable_1_get_HasValue_m4110133180_gshared)(__this, method)
// T System.Nullable`1<System.Char>::get_Value()
extern "C"  Il2CppChar Nullable_1_get_Value_m2771617605_gshared (Nullable_1_t2946749061 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2771617605(__this, method) ((  Il2CppChar (*) (Nullable_1_t2946749061 *, const MethodInfo*))Nullable_1_get_Value_m2771617605_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1183086125_gshared (Nullable_1_t2946749061 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1183086125(__this, ___other0, method) ((  bool (*) (Nullable_1_t2946749061 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1183086125_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Char>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2109816946_gshared (Nullable_1_t2946749061 * __this, Nullable_1_t2946749061  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2109816946(__this, ___other0, method) ((  bool (*) (Nullable_1_t2946749061 *, Nullable_1_t2946749061 , const MethodInfo*))Nullable_1_Equals_m2109816946_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Char>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m400581137_gshared (Nullable_1_t2946749061 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m400581137(__this, method) ((  int32_t (*) (Nullable_1_t2946749061 *, const MethodInfo*))Nullable_1_GetHashCode_m400581137_gshared)(__this, method)
// T System.Nullable`1<System.Char>::GetValueOrDefault()
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m4158337871_gshared (Nullable_1_t2946749061 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m4158337871(__this, method) ((  Il2CppChar (*) (Nullable_1_t2946749061 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m4158337871_gshared)(__this, method)
// T System.Nullable`1<System.Char>::GetValueOrDefault(T)
extern "C"  Il2CppChar Nullable_1_GetValueOrDefault_m3272789694_gshared (Nullable_1_t2946749061 * __this, Il2CppChar ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3272789694(__this, ___defaultValue0, method) ((  Il2CppChar (*) (Nullable_1_t2946749061 *, Il2CppChar, const MethodInfo*))Nullable_1_GetValueOrDefault_m3272789694_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Char>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2065776661_gshared (Nullable_1_t2946749061 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2065776661(__this, method) ((  String_t* (*) (Nullable_1_t2946749061 *, const MethodInfo*))Nullable_1_ToString_m2065776661_gshared)(__this, method)
