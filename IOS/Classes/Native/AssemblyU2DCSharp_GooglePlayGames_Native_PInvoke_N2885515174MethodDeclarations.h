﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t2885515174;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange::.ctor(System.IntPtr)
extern "C"  void NativeSnapshotMetadataChange__ctor_m2251755526 (NativeSnapshotMetadataChange_t2885515174 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeSnapshotMetadataChange_CallDispose_m210196074 (NativeSnapshotMetadataChange_t2885515174 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange::FromPointer(System.IntPtr)
extern "C"  NativeSnapshotMetadataChange_t2885515174 * NativeSnapshotMetadataChange_FromPointer_m3489550175 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
