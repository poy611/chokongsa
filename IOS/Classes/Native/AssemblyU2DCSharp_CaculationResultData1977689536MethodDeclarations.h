﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaculationResultData
struct CaculationResultData_t1977689536;

#include "codegen/il2cpp-codegen.h"

// System.Void CaculationResultData::.ctor()
extern "C"  void CaculationResultData__ctor_m2195041499 (CaculationResultData_t1977689536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
