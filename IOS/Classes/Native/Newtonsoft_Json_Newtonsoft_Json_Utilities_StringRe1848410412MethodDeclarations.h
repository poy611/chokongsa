﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRe1751946808.h"
#include "mscorlib_System_String7231557.h"

// System.Int32 Newtonsoft.Json.Utilities.StringReferenceExtensions::IndexOf(Newtonsoft.Json.Utilities.StringReference,System.Char,System.Int32,System.Int32)
extern "C"  int32_t StringReferenceExtensions_IndexOf_m2848264498 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___s0, Il2CppChar ___c1, int32_t ___startIndex2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::StartsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern "C"  bool StringReferenceExtensions_StartsWith_m974581255 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___s0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::EndsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern "C"  bool StringReferenceExtensions_EndsWith_m1129998656 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___s0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
