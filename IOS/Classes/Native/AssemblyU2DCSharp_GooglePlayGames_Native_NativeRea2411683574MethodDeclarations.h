﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70
struct U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t815788017;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re815788017.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70::.ctor()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey70__ctor_m2101470309 (U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70::<>m__49(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey70_U3CU3Em__49_m4152491022 (U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * __this, FetchInvitationsResponse_t815788017 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
