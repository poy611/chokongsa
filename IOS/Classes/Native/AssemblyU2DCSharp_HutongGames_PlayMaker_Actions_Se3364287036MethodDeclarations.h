﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmArrayItem
struct SetFsmArrayItem_t3364287036;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::.ctor()
extern "C"  void SetFsmArrayItem__ctor_m485886074 (SetFsmArrayItem_t3364287036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::Reset()
extern "C"  void SetFsmArrayItem_Reset_m2427286311 (SetFsmArrayItem_t3364287036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::OnEnter()
extern "C"  void SetFsmArrayItem_OnEnter_m2471777745 (SetFsmArrayItem_t3364287036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::DoSetFsmArray()
extern "C"  void SetFsmArrayItem_DoSetFsmArray_m4009094280 (SetFsmArrayItem_t3364287036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::OnUpdate()
extern "C"  void SetFsmArrayItem_OnUpdate_m2744225426 (SetFsmArrayItem_t3364287036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
