﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRandomRotation
struct SetRandomRotation_t2145798833;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::.ctor()
extern "C"  void SetRandomRotation__ctor_m289393061 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::Reset()
extern "C"  void SetRandomRotation_Reset_m2230793298 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::OnEnter()
extern "C"  void SetRandomRotation_OnEnter_m2620553276 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::DoRandomRotation()
extern "C"  void SetRandomRotation_DoRandomRotation_m2423224779 (SetRandomRotation_t2145798833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
