﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m541880586(__this, ___l0, method) ((  void (*) (Enumerator_t3192089135 *, List_1_t3172416365 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1500423432(__this, method) ((  void (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1880274430(__this, method) ((  Il2CppObject * (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::Dispose()
#define Enumerator_Dispose_m208036207(__this, method) ((  void (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::VerifyState()
#define Enumerator_VerifyState_m798871848(__this, method) ((  void (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::MoveNext()
#define Enumerator_MoveNext_m979688353(__this, method) ((  bool (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Current()
#define Enumerator_get_Current_m1902143131(__this, method) ((  Participant_t1804230813 * (*) (Enumerator_t3192089135 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
