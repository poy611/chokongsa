﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetColorValue
struct SetColorValue_t353523614;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetColorValue::.ctor()
extern "C"  void SetColorValue__ctor_m2350033752 (SetColorValue_t353523614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::Reset()
extern "C"  void SetColorValue_Reset_m4291433989 (SetColorValue_t353523614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnEnter()
extern "C"  void SetColorValue_OnEnter_m2916333871 (SetColorValue_t353523614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnUpdate()
extern "C"  void SetColorValue_OnUpdate_m3640563444 (SetColorValue_t353523614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::DoSetColorValue()
extern "C"  void SetColorValue_DoSetColorValue_m2555580411 (SetColorValue_t353523614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
