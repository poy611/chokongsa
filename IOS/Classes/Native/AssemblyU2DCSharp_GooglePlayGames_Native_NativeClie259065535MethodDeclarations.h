﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m2531779806_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m2531779806(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m2531779806_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>::<>m__18()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398_gshared)(__this, method)
