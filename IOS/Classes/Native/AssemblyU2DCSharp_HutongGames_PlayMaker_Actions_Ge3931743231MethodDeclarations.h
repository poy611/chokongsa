﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTouchInfo
struct GetTouchInfo_t3931743231;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::.ctor()
extern "C"  void GetTouchInfo__ctor_m3849591111 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::Reset()
extern "C"  void GetTouchInfo_Reset_m1496024052 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnEnter()
extern "C"  void GetTouchInfo_OnEnter_m881944414 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnUpdate()
extern "C"  void GetTouchInfo_OnUpdate_m704032421 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::DoGetTouchInfo()
extern "C"  void GetTouchInfo_DoGetTouchInfo_m3752312223 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
