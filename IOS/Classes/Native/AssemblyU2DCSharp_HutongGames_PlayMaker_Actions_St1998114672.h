﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2825067351.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StopAnimation
struct  StopAnimation_t1998114672  : public BaseAnimationAction_t2825067351
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.StopAnimation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StopAnimation::animName
	FsmString_t952858651 * ___animName_14;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(StopAnimation_t1998114672, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_animName_14() { return static_cast<int32_t>(offsetof(StopAnimation_t1998114672, ___animName_14)); }
	inline FsmString_t952858651 * get_animName_14() const { return ___animName_14; }
	inline FsmString_t952858651 ** get_address_of_animName_14() { return &___animName_14; }
	inline void set_animName_14(FsmString_t952858651 * value)
	{
		___animName_14 = value;
		Il2CppCodeGenWriteBarrier(&___animName_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
