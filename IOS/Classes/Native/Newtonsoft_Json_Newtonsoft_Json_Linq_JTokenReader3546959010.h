﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t3546959010  : public JsonReader_t816925123
{
public:
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_t3412245951 * ____root_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_t3412245951 * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_t3412245951 * ____current_18;

public:
	inline static int32_t get_offset_of__initialPath_15() { return static_cast<int32_t>(offsetof(JTokenReader_t3546959010, ____initialPath_15)); }
	inline String_t* get__initialPath_15() const { return ____initialPath_15; }
	inline String_t** get_address_of__initialPath_15() { return &____initialPath_15; }
	inline void set__initialPath_15(String_t* value)
	{
		____initialPath_15 = value;
		Il2CppCodeGenWriteBarrier(&____initialPath_15, value);
	}

	inline static int32_t get_offset_of__root_16() { return static_cast<int32_t>(offsetof(JTokenReader_t3546959010, ____root_16)); }
	inline JToken_t3412245951 * get__root_16() const { return ____root_16; }
	inline JToken_t3412245951 ** get_address_of__root_16() { return &____root_16; }
	inline void set__root_16(JToken_t3412245951 * value)
	{
		____root_16 = value;
		Il2CppCodeGenWriteBarrier(&____root_16, value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t3546959010, ____parent_17)); }
	inline JToken_t3412245951 * get__parent_17() const { return ____parent_17; }
	inline JToken_t3412245951 ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_t3412245951 * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier(&____parent_17, value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t3546959010, ____current_18)); }
	inline JToken_t3412245951 * get__current_18() const { return ____current_18; }
	inline JToken_t3412245951 ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_t3412245951 * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier(&____current_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
