﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2550930342(__this, ___l0, method) ((  void (*) (Enumerator_t2984008859 *, List_1_t2964336089 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4189964524(__this, method) ((  void (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3106384738(__this, method) ((  Il2CppObject * (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::Dispose()
#define Enumerator_Dispose_m3742724875(__this, method) ((  void (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::VerifyState()
#define Enumerator_VerifyState_m183678148(__this, method) ((  void (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::MoveNext()
#define Enumerator_MoveNext_m1271805127(__this, method) ((  bool (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVar>::get_Current()
#define Enumerator_get_Current_m277438389(__this, method) ((  FsmVar_t1596150537 * (*) (Enumerator_t2984008859 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
