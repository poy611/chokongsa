﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m876371164_gshared (KeyValuePair_2_t1220774118 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m876371164(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1220774118 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m876371164_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1333639052_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1333639052(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1220774118 *, const MethodInfo*))KeyValuePair_2_get_Key_m1333639052_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1955292109_gshared (KeyValuePair_2_t1220774118 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1955292109(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1220774118 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1955292109_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3015219660_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3015219660(__this, method) ((  int32_t (*) (KeyValuePair_2_t1220774118 *, const MethodInfo*))KeyValuePair_2_get_Value_m3015219660_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2848328013_gshared (KeyValuePair_2_t1220774118 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2848328013(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1220774118 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2848328013_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1519115317_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1519115317(__this, method) ((  String_t* (*) (KeyValuePair_2_t1220774118 *, const MethodInfo*))KeyValuePair_2_ToString_m1519115317_gshared)(__this, method)
