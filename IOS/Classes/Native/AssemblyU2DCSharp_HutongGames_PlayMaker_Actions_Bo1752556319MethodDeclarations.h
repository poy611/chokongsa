﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolFlip
struct BoolFlip_t1752556319;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolFlip::.ctor()
extern "C"  void BoolFlip__ctor_m287877671 (BoolFlip_t1752556319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolFlip::Reset()
extern "C"  void BoolFlip_Reset_m2229277908 (BoolFlip_t1752556319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolFlip::OnEnter()
extern "C"  void BoolFlip_OnEnter_m1164263486 (BoolFlip_t1752556319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
