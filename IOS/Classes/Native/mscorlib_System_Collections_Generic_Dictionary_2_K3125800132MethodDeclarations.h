﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke876575735MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1167447923(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3125800132 *, Dictionary_2_t1499040681 *, const MethodInfo*))KeyCollection__ctor_m3025266057_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1504536067(__this, ___item0, method) ((  void (*) (KeyCollection_t3125800132 *, Fsm_t1527112426 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1666867757_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4010487866(__this, method) ((  void (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3174903524_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m208905131(__this, ___item0, method) ((  bool (*) (KeyCollection_t3125800132 *, Fsm_t1527112426 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3440796865_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3357213648(__this, ___item0, method) ((  bool (*) (KeyCollection_t3125800132 *, Fsm_t1527112426 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3011973734_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m557254284(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2276350902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m299142572(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3125800132 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2180767062_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1285296443(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m458376805_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1775426636(__this, method) ((  bool (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2362028002_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1390515198(__this, method) ((  bool (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m164219668_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m849894576(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4206686406_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2514864680(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3125800132 *, FsmU5BU5D_t3357880879*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3308236862_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::GetEnumerator()
#define KeyCollection_GetEnumerator_m259327349(__this, method) ((  Enumerator_t2113976735  (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_GetEnumerator_m119094283_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Count()
#define KeyCollection_get_Count_m2406634936(__this, method) ((  int32_t (*) (KeyCollection_t3125800132 *, const MethodInfo*))KeyCollection_get_Count_m3090149838_gshared)(__this, method)
