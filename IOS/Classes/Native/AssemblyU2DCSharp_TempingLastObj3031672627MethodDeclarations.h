﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempingLastObj
struct TempingLastObj_t3031672627;

#include "codegen/il2cpp-codegen.h"

// System.Void TempingLastObj::.ctor()
extern "C"  void TempingLastObj__ctor_m3727658696 (TempingLastObj_t3031672627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingLastObj::OnEnable()
extern "C"  void TempingLastObj_OnEnable_m2077385662 (TempingLastObj_t3031672627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
