﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempingPointer/<TempingSlideUp>c__Iterator30
struct U3CTempingSlideUpU3Ec__Iterator30_t3867951914;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::.ctor()
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30__ctor_m4151656113 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<TempingSlideUp>c__Iterator30::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideUpU3Ec__Iterator30_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15851595 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<TempingSlideUp>c__Iterator30::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideUpU3Ec__Iterator30_System_Collections_IEnumerator_get_Current_m248240607 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TempingPointer/<TempingSlideUp>c__Iterator30::MoveNext()
extern "C"  bool U3CTempingSlideUpU3Ec__Iterator30_MoveNext_m3540086667 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::Dispose()
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30_Dispose_m3910156974 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::Reset()
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30_Reset_m1798089054 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
