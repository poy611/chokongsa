﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos
struct  NetworkGetNetworkDisconnectionInfos_t2164373997  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::disconnectionLabel
	FsmString_t952858651 * ___disconnectionLabel_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::lostConnectionEvent
	FsmEvent_t2133468028 * ___lostConnectionEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNetworkDisconnectionInfos::disConnectedEvent
	FsmEvent_t2133468028 * ___disConnectedEvent_13;

public:
	inline static int32_t get_offset_of_disconnectionLabel_11() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___disconnectionLabel_11)); }
	inline FsmString_t952858651 * get_disconnectionLabel_11() const { return ___disconnectionLabel_11; }
	inline FsmString_t952858651 ** get_address_of_disconnectionLabel_11() { return &___disconnectionLabel_11; }
	inline void set_disconnectionLabel_11(FsmString_t952858651 * value)
	{
		___disconnectionLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___disconnectionLabel_11, value);
	}

	inline static int32_t get_offset_of_lostConnectionEvent_12() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___lostConnectionEvent_12)); }
	inline FsmEvent_t2133468028 * get_lostConnectionEvent_12() const { return ___lostConnectionEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_lostConnectionEvent_12() { return &___lostConnectionEvent_12; }
	inline void set_lostConnectionEvent_12(FsmEvent_t2133468028 * value)
	{
		___lostConnectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___lostConnectionEvent_12, value);
	}

	inline static int32_t get_offset_of_disConnectedEvent_13() { return static_cast<int32_t>(offsetof(NetworkGetNetworkDisconnectionInfos_t2164373997, ___disConnectedEvent_13)); }
	inline FsmEvent_t2133468028 * get_disConnectedEvent_13() const { return ___disConnectedEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_disConnectedEvent_13() { return &___disConnectedEvent_13; }
	inline void set_disConnectedEvent_13(FsmEvent_t2133468028 * value)
	{
		___disConnectedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___disConnectedEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
