﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D
struct U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Player>
struct List_1_t800745875;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"

// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::.ctor()
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D__ctor_m2877353375 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::<>m__20()
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D_U3CU3Em__20_m1485724518 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::<>m__21()
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D_U3CU3Em__21_m1485725479 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::<>m__22(GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Player>)
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D_U3CU3Em__22_m1277477880 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, int32_t ___status0, List_1_t800745875 * ___players1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::<>m__35()
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D_U3CU3Em__35_m1485759114 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadFriends>c__AnonStorey4D::<>m__36()
extern "C"  void U3CLoadFriendsU3Ec__AnonStorey4D_U3CU3Em__36_m1485760075 (U3CLoadFriendsU3Ec__AnonStorey4D_t3800822908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
