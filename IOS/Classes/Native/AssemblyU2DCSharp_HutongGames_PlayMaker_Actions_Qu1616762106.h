﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionSlerp
struct  QuaternionSlerp_t1616762106  : public QuaternionBaseAction_t1884049229
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::fromQuaternion
	FsmQuaternion_t3871136040 * ___fromQuaternion_13;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::toQuaternion
	FsmQuaternion_t3871136040 * ___toQuaternion_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionSlerp::amount
	FsmFloat_t2134102846 * ___amount_15;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::storeResult
	FsmQuaternion_t3871136040 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_fromQuaternion_13() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t1616762106, ___fromQuaternion_13)); }
	inline FsmQuaternion_t3871136040 * get_fromQuaternion_13() const { return ___fromQuaternion_13; }
	inline FsmQuaternion_t3871136040 ** get_address_of_fromQuaternion_13() { return &___fromQuaternion_13; }
	inline void set_fromQuaternion_13(FsmQuaternion_t3871136040 * value)
	{
		___fromQuaternion_13 = value;
		Il2CppCodeGenWriteBarrier(&___fromQuaternion_13, value);
	}

	inline static int32_t get_offset_of_toQuaternion_14() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t1616762106, ___toQuaternion_14)); }
	inline FsmQuaternion_t3871136040 * get_toQuaternion_14() const { return ___toQuaternion_14; }
	inline FsmQuaternion_t3871136040 ** get_address_of_toQuaternion_14() { return &___toQuaternion_14; }
	inline void set_toQuaternion_14(FsmQuaternion_t3871136040 * value)
	{
		___toQuaternion_14 = value;
		Il2CppCodeGenWriteBarrier(&___toQuaternion_14, value);
	}

	inline static int32_t get_offset_of_amount_15() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t1616762106, ___amount_15)); }
	inline FsmFloat_t2134102846 * get_amount_15() const { return ___amount_15; }
	inline FsmFloat_t2134102846 ** get_address_of_amount_15() { return &___amount_15; }
	inline void set_amount_15(FsmFloat_t2134102846 * value)
	{
		___amount_15 = value;
		Il2CppCodeGenWriteBarrier(&___amount_15, value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t1616762106, ___storeResult_16)); }
	inline FsmQuaternion_t3871136040 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmQuaternion_t3871136040 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmQuaternion_t3871136040 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
