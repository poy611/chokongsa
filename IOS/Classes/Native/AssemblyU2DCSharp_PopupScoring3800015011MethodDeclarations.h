﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupScoring
struct PopupScoring_t3800015011;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupScoring::.ctor()
extern "C"  void PopupScoring__ctor_m1973377368 (PopupScoring_t3800015011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScoring::OnEnable()
extern "C"  void PopupScoring_OnEnable_m1444400942 (PopupScoring_t3800015011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PopupScoring::MakeItSocre()
extern "C"  Il2CppObject * PopupScoring_MakeItSocre_m927543535 (PopupScoring_t3800015011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
