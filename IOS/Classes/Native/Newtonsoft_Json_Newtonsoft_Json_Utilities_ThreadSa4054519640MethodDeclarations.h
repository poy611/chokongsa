﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa3745804690MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m1071204695(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t4054519640 *, Func_2_t1364512968 *, const MethodInfo*))ThreadSafeStore_2__ctor_m72384408_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::Get(TKey)
#define ThreadSafeStore_2_Get_m103717629(__this, ___key0, method) ((  Func_2_t184564025 * (*) (ThreadSafeStore_2_t4054519640 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_Get_m2316141778_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m278232501(__this, ___key0, method) ((  Func_2_t184564025 * (*) (ThreadSafeStore_2_t4054519640 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_AddValue_m1276670208_gshared)(__this, ___key0, method)
