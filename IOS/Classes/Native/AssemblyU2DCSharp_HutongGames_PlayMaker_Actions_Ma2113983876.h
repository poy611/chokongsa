﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerGetNextHostData
struct  MasterServerGetNextHostData_t2113983876  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::loopEvent
	FsmEvent_t2133468028 * ___loopEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::finishedEvent
	FsmEvent_t2133468028 * ___finishedEvent_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::index
	FsmInt_t1596138449 * ___index_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::useNat
	FsmBool_t1075959796 * ___useNat_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::gameType
	FsmString_t952858651 * ___gameType_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::gameName
	FsmString_t952858651 * ___gameName_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::connectedPlayers
	FsmInt_t1596138449 * ___connectedPlayers_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::playerLimit
	FsmInt_t1596138449 * ___playerLimit_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::ipAddress
	FsmString_t952858651 * ___ipAddress_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::port
	FsmInt_t1596138449 * ___port_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::passwordProtected
	FsmBool_t1075959796 * ___passwordProtected_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::comment
	FsmString_t952858651 * ___comment_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::guid
	FsmString_t952858651 * ___guid_23;
	// System.Int32 HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::nextItemIndex
	int32_t ___nextItemIndex_24;
	// System.Boolean HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::noMoreItems
	bool ___noMoreItems_25;

public:
	inline static int32_t get_offset_of_loopEvent_11() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___loopEvent_11)); }
	inline FsmEvent_t2133468028 * get_loopEvent_11() const { return ___loopEvent_11; }
	inline FsmEvent_t2133468028 ** get_address_of_loopEvent_11() { return &___loopEvent_11; }
	inline void set_loopEvent_11(FsmEvent_t2133468028 * value)
	{
		___loopEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_11, value);
	}

	inline static int32_t get_offset_of_finishedEvent_12() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___finishedEvent_12)); }
	inline FsmEvent_t2133468028 * get_finishedEvent_12() const { return ___finishedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_finishedEvent_12() { return &___finishedEvent_12; }
	inline void set_finishedEvent_12(FsmEvent_t2133468028 * value)
	{
		___finishedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_12, value);
	}

	inline static int32_t get_offset_of_index_13() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___index_13)); }
	inline FsmInt_t1596138449 * get_index_13() const { return ___index_13; }
	inline FsmInt_t1596138449 ** get_address_of_index_13() { return &___index_13; }
	inline void set_index_13(FsmInt_t1596138449 * value)
	{
		___index_13 = value;
		Il2CppCodeGenWriteBarrier(&___index_13, value);
	}

	inline static int32_t get_offset_of_useNat_14() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___useNat_14)); }
	inline FsmBool_t1075959796 * get_useNat_14() const { return ___useNat_14; }
	inline FsmBool_t1075959796 ** get_address_of_useNat_14() { return &___useNat_14; }
	inline void set_useNat_14(FsmBool_t1075959796 * value)
	{
		___useNat_14 = value;
		Il2CppCodeGenWriteBarrier(&___useNat_14, value);
	}

	inline static int32_t get_offset_of_gameType_15() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___gameType_15)); }
	inline FsmString_t952858651 * get_gameType_15() const { return ___gameType_15; }
	inline FsmString_t952858651 ** get_address_of_gameType_15() { return &___gameType_15; }
	inline void set_gameType_15(FsmString_t952858651 * value)
	{
		___gameType_15 = value;
		Il2CppCodeGenWriteBarrier(&___gameType_15, value);
	}

	inline static int32_t get_offset_of_gameName_16() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___gameName_16)); }
	inline FsmString_t952858651 * get_gameName_16() const { return ___gameName_16; }
	inline FsmString_t952858651 ** get_address_of_gameName_16() { return &___gameName_16; }
	inline void set_gameName_16(FsmString_t952858651 * value)
	{
		___gameName_16 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_16, value);
	}

	inline static int32_t get_offset_of_connectedPlayers_17() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___connectedPlayers_17)); }
	inline FsmInt_t1596138449 * get_connectedPlayers_17() const { return ___connectedPlayers_17; }
	inline FsmInt_t1596138449 ** get_address_of_connectedPlayers_17() { return &___connectedPlayers_17; }
	inline void set_connectedPlayers_17(FsmInt_t1596138449 * value)
	{
		___connectedPlayers_17 = value;
		Il2CppCodeGenWriteBarrier(&___connectedPlayers_17, value);
	}

	inline static int32_t get_offset_of_playerLimit_18() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___playerLimit_18)); }
	inline FsmInt_t1596138449 * get_playerLimit_18() const { return ___playerLimit_18; }
	inline FsmInt_t1596138449 ** get_address_of_playerLimit_18() { return &___playerLimit_18; }
	inline void set_playerLimit_18(FsmInt_t1596138449 * value)
	{
		___playerLimit_18 = value;
		Il2CppCodeGenWriteBarrier(&___playerLimit_18, value);
	}

	inline static int32_t get_offset_of_ipAddress_19() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___ipAddress_19)); }
	inline FsmString_t952858651 * get_ipAddress_19() const { return ___ipAddress_19; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_19() { return &___ipAddress_19; }
	inline void set_ipAddress_19(FsmString_t952858651 * value)
	{
		___ipAddress_19 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_19, value);
	}

	inline static int32_t get_offset_of_port_20() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___port_20)); }
	inline FsmInt_t1596138449 * get_port_20() const { return ___port_20; }
	inline FsmInt_t1596138449 ** get_address_of_port_20() { return &___port_20; }
	inline void set_port_20(FsmInt_t1596138449 * value)
	{
		___port_20 = value;
		Il2CppCodeGenWriteBarrier(&___port_20, value);
	}

	inline static int32_t get_offset_of_passwordProtected_21() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___passwordProtected_21)); }
	inline FsmBool_t1075959796 * get_passwordProtected_21() const { return ___passwordProtected_21; }
	inline FsmBool_t1075959796 ** get_address_of_passwordProtected_21() { return &___passwordProtected_21; }
	inline void set_passwordProtected_21(FsmBool_t1075959796 * value)
	{
		___passwordProtected_21 = value;
		Il2CppCodeGenWriteBarrier(&___passwordProtected_21, value);
	}

	inline static int32_t get_offset_of_comment_22() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___comment_22)); }
	inline FsmString_t952858651 * get_comment_22() const { return ___comment_22; }
	inline FsmString_t952858651 ** get_address_of_comment_22() { return &___comment_22; }
	inline void set_comment_22(FsmString_t952858651 * value)
	{
		___comment_22 = value;
		Il2CppCodeGenWriteBarrier(&___comment_22, value);
	}

	inline static int32_t get_offset_of_guid_23() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___guid_23)); }
	inline FsmString_t952858651 * get_guid_23() const { return ___guid_23; }
	inline FsmString_t952858651 ** get_address_of_guid_23() { return &___guid_23; }
	inline void set_guid_23(FsmString_t952858651 * value)
	{
		___guid_23 = value;
		Il2CppCodeGenWriteBarrier(&___guid_23, value);
	}

	inline static int32_t get_offset_of_nextItemIndex_24() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___nextItemIndex_24)); }
	inline int32_t get_nextItemIndex_24() const { return ___nextItemIndex_24; }
	inline int32_t* get_address_of_nextItemIndex_24() { return &___nextItemIndex_24; }
	inline void set_nextItemIndex_24(int32_t value)
	{
		___nextItemIndex_24 = value;
	}

	inline static int32_t get_offset_of_noMoreItems_25() { return static_cast<int32_t>(offsetof(MasterServerGetNextHostData_t2113983876, ___noMoreItems_25)); }
	inline bool get_noMoreItems_25() const { return ___noMoreItems_25; }
	inline bool* get_address_of_noMoreItems_25() { return &___noMoreItems_25; }
	inline void set_noMoreItems_25(bool value)
	{
		___noMoreItems_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
