﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmEnum
struct SetFsmEnum_t888376583;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::.ctor()
extern "C"  void SetFsmEnum__ctor_m1339621183 (SetFsmEnum_t888376583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::Reset()
extern "C"  void SetFsmEnum_Reset_m3281021420 (SetFsmEnum_t888376583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::OnEnter()
extern "C"  void SetFsmEnum_OnEnter_m2572463958 (SetFsmEnum_t888376583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::DoSetFsmEnum()
extern "C"  void SetFsmEnum_DoSetFsmEnum_m2740707887 (SetFsmEnum_t888376583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::OnUpdate()
extern "C"  void SetFsmEnum_OnUpdate_m1570530733 (SetFsmEnum_t888376583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
