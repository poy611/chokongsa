﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRaycastAllInfo
struct GetRaycastAllInfo_t3534604426;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::.ctor()
extern "C"  void GetRaycastAllInfo__ctor_m3704999916 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::Reset()
extern "C"  void GetRaycastAllInfo_Reset_m1351432857 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::StoreRaycastAllInfo()
extern "C"  void GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnEnter()
extern "C"  void GetRaycastAllInfo_OnEnter_m3663726787 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnUpdate()
extern "C"  void GetRaycastAllInfo_OnUpdate_m1039940064 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
