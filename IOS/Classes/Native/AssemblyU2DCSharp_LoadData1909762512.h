﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadData
struct  LoadData_t1909762512  : public Il2CppObject
{
public:
	// System.Int32 LoadData::sdGuestTh
	int32_t ___sdGuestTh_0;
	// System.Int32 LoadData::sdTrialTh
	int32_t ___sdTrialTh_1;
	// System.Int32 LoadData::sdGameId
	int32_t ___sdGameId_2;
	// System.Int32 LoadData::sdGrade
	int32_t ___sdGrade_3;

public:
	inline static int32_t get_offset_of_sdGuestTh_0() { return static_cast<int32_t>(offsetof(LoadData_t1909762512, ___sdGuestTh_0)); }
	inline int32_t get_sdGuestTh_0() const { return ___sdGuestTh_0; }
	inline int32_t* get_address_of_sdGuestTh_0() { return &___sdGuestTh_0; }
	inline void set_sdGuestTh_0(int32_t value)
	{
		___sdGuestTh_0 = value;
	}

	inline static int32_t get_offset_of_sdTrialTh_1() { return static_cast<int32_t>(offsetof(LoadData_t1909762512, ___sdTrialTh_1)); }
	inline int32_t get_sdTrialTh_1() const { return ___sdTrialTh_1; }
	inline int32_t* get_address_of_sdTrialTh_1() { return &___sdTrialTh_1; }
	inline void set_sdTrialTh_1(int32_t value)
	{
		___sdTrialTh_1 = value;
	}

	inline static int32_t get_offset_of_sdGameId_2() { return static_cast<int32_t>(offsetof(LoadData_t1909762512, ___sdGameId_2)); }
	inline int32_t get_sdGameId_2() const { return ___sdGameId_2; }
	inline int32_t* get_address_of_sdGameId_2() { return &___sdGameId_2; }
	inline void set_sdGameId_2(int32_t value)
	{
		___sdGameId_2 = value;
	}

	inline static int32_t get_offset_of_sdGrade_3() { return static_cast<int32_t>(offsetof(LoadData_t1909762512, ___sdGrade_3)); }
	inline int32_t get_sdGrade_3() const { return ___sdGrade_3; }
	inline int32_t* get_address_of_sdGrade_3() { return &___sdGrade_3; }
	inline void set_sdGrade_3(int32_t value)
	{
		___sdGrade_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
