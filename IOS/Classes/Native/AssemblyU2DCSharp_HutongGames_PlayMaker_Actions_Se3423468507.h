﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterialFloat
struct  SetMaterialFloat_t3423468507  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterialFloat::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterialFloat::materialIndex
	FsmInt_t1596138449 * ___materialIndex_14;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterialFloat::material
	FsmMaterial_t924399665 * ___material_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetMaterialFloat::namedFloat
	FsmString_t952858651 * ___namedFloat_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetMaterialFloat::floatValue
	FsmFloat_t2134102846 * ___floatValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetMaterialFloat::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_materialIndex_14() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___materialIndex_14)); }
	inline FsmInt_t1596138449 * get_materialIndex_14() const { return ___materialIndex_14; }
	inline FsmInt_t1596138449 ** get_address_of_materialIndex_14() { return &___materialIndex_14; }
	inline void set_materialIndex_14(FsmInt_t1596138449 * value)
	{
		___materialIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___materialIndex_14, value);
	}

	inline static int32_t get_offset_of_material_15() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___material_15)); }
	inline FsmMaterial_t924399665 * get_material_15() const { return ___material_15; }
	inline FsmMaterial_t924399665 ** get_address_of_material_15() { return &___material_15; }
	inline void set_material_15(FsmMaterial_t924399665 * value)
	{
		___material_15 = value;
		Il2CppCodeGenWriteBarrier(&___material_15, value);
	}

	inline static int32_t get_offset_of_namedFloat_16() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___namedFloat_16)); }
	inline FsmString_t952858651 * get_namedFloat_16() const { return ___namedFloat_16; }
	inline FsmString_t952858651 ** get_address_of_namedFloat_16() { return &___namedFloat_16; }
	inline void set_namedFloat_16(FsmString_t952858651 * value)
	{
		___namedFloat_16 = value;
		Il2CppCodeGenWriteBarrier(&___namedFloat_16, value);
	}

	inline static int32_t get_offset_of_floatValue_17() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___floatValue_17)); }
	inline FsmFloat_t2134102846 * get_floatValue_17() const { return ___floatValue_17; }
	inline FsmFloat_t2134102846 ** get_address_of_floatValue_17() { return &___floatValue_17; }
	inline void set_floatValue_17(FsmFloat_t2134102846 * value)
	{
		___floatValue_17 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t3423468507, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
