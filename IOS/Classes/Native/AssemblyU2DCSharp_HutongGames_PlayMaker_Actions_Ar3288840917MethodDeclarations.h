﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayTransferValue
struct ArrayTransferValue_t3288840917;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::.ctor()
extern "C"  void ArrayTransferValue__ctor_m661971249 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::Reset()
extern "C"  void ArrayTransferValue_Reset_m2603371486 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::OnEnter()
extern "C"  void ArrayTransferValue_OnEnter_m4185906376 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::DoTransferValue()
extern "C"  void ArrayTransferValue_DoTransferValue_m2826718090 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
