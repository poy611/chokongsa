﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t1202205191;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String System.Runtime.Serialization.EnumMemberAttribute::get_Value()
extern "C"  String_t* EnumMemberAttribute_get_Value_m4250259485 (EnumMemberAttribute_t1202205191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
