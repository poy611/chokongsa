﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2976456013.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2282247744_gshared (Nullable_1_t2976456013 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2282247744(__this, ___value0, method) ((  void (*) (Nullable_1_t2976456013 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2282247744_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4062447359_gshared (Nullable_1_t2976456013 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m4062447359(__this, method) ((  bool (*) (Nullable_1_t2976456013 *, const MethodInfo*))Nullable_1_get_HasValue_m4062447359_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3826118370_gshared (Nullable_1_t2976456013 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3826118370(__this, method) ((  int32_t (*) (Nullable_1_t2976456013 *, const MethodInfo*))Nullable_1_get_Value_m3826118370_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2940229002_gshared (Nullable_1_t2976456013 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2940229002(__this, ___other0, method) ((  bool (*) (Nullable_1_t2976456013 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2940229002_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3213479541_gshared (Nullable_1_t2976456013 * __this, Nullable_1_t2976456013  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3213479541(__this, ___other0, method) ((  bool (*) (Nullable_1_t2976456013 *, Nullable_1_t2976456013 , const MethodInfo*))Nullable_1_Equals_m3213479541_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3754933870_gshared (Nullable_1_t2976456013 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3754933870(__this, method) ((  int32_t (*) (Nullable_1_t2976456013 *, const MethodInfo*))Nullable_1_GetHashCode_m3754933870_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m536458732_gshared (Nullable_1_t2976456013 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m536458732(__this, method) ((  int32_t (*) (Nullable_1_t2976456013 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m536458732_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m4144408513_gshared (Nullable_1_t2976456013 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m4144408513(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2976456013 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m4144408513_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2966016216_gshared (Nullable_1_t2976456013 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2966016216(__this, method) ((  String_t* (*) (Nullable_1_t2976456013 *, const MethodInfo*))Nullable_1_ToString_m2966016216_gshared)(__this, method)
