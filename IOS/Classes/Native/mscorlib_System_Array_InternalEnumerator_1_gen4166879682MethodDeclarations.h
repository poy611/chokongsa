﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4166879682.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1089569710.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1146423754_gshared (InternalEnumerator_1_t4166879682 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1146423754(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4166879682 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1146423754_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822(__this, method) ((  void (*) (InternalEnumerator_1_t4166879682 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4166879682 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3029624865_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3029624865(__this, method) ((  void (*) (InternalEnumerator_1_t4166879682 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3029624865_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4096989954_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4096989954(__this, method) ((  bool (*) (InternalEnumerator_1_t4166879682 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4096989954_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::get_Current()
extern "C"  Link_t1089569710  InternalEnumerator_1_get_Current_m909351953_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m909351953(__this, method) ((  Link_t1089569710  (*) (InternalEnumerator_1_t4166879682 *, const MethodInfo*))InternalEnumerator_1_get_Current_m909351953_gshared)(__this, method)
