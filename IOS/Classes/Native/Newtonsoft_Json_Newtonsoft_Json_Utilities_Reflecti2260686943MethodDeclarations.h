﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionUtils/<>c
struct U3CU3Ec_t2260686943;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.String
struct String_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m3937999116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m973867713 (U3CU3Ec_t2260686943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetDefaultConstructor>b__10_0(System.Reflection.ConstructorInfo)
extern "C"  bool U3CU3Ec_U3CGetDefaultConstructorU3Eb__10_0_m900440942 (U3CU3Ec_t2260686943 * __this, ConstructorInfo_t4136801618 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetFieldsAndProperties>b__29_0(System.Reflection.MemberInfo)
extern "C"  String_t* U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__29_0_m2980126921 (U3CU3Ec_t2260686943 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetMemberInfoFromType>b__37_0(System.Reflection.ParameterInfo)
extern "C"  Type_t * U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__37_0_m1410549623 (U3CU3Ec_t2260686943 * __this, ParameterInfo_t2235474049 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetChildPrivateFields>b__39_0(System.Reflection.FieldInfo)
extern "C"  bool U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__39_0_m1073954816 (U3CU3Ec_t2260686943 * __this, FieldInfo_t * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
