﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger
struct SetCollider2dIsTrigger_t2140550286;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::.ctor()
extern "C"  void SetCollider2dIsTrigger__ctor_m3224664984 (SetCollider2dIsTrigger_t2140550286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::Reset()
extern "C"  void SetCollider2dIsTrigger_Reset_m871097925 (SetCollider2dIsTrigger_t2140550286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::OnEnter()
extern "C"  void SetCollider2dIsTrigger_OnEnter_m1623357807 (SetCollider2dIsTrigger_t2140550286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::DoSetIsTrigger()
extern "C"  void SetCollider2dIsTrigger_DoSetIsTrigger_m3896365635 (SetCollider2dIsTrigger_t2140550286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
