﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BuildString
struct BuildString_t1836011181;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BuildString::.ctor()
extern "C"  void BuildString__ctor_m2091962153 (BuildString_t1836011181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::Reset()
extern "C"  void BuildString_Reset_m4033362390 (BuildString_t1836011181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::OnEnter()
extern "C"  void BuildString_OnEnter_m4017630400 (BuildString_t1836011181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::OnUpdate()
extern "C"  void BuildString_OnUpdate_m3421017475 (BuildString_t1836011181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::DoBuildString()
extern "C"  void BuildString_DoBuildString_m189802459 (BuildString_t1836011181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
