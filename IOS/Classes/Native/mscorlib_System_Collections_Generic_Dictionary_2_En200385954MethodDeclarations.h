﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En200385954.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1312038170_gshared (Enumerator_t200385954 * __this, Dictionary_2_t3178029858 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1312038170(__this, ___dictionary0, method) ((  void (*) (Enumerator_t200385954 *, Dictionary_2_t3178029858 *, const MethodInfo*))Enumerator__ctor_m1312038170_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m361712657_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m361712657(__this, method) ((  Il2CppObject * (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m361712657_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1956132699_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1956132699(__this, method) ((  void (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1956132699_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3448413906_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3448413906(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3448413906_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m415201325_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m415201325(__this, method) ((  Il2CppObject * (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m415201325_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m624951167_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m624951167(__this, method) ((  Il2CppObject * (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m624951167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m235682699_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m235682699(__this, method) ((  bool (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_MoveNext_m235682699_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern "C"  KeyValuePair_2_t3076810564  Enumerator_get_Current_m802063697_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m802063697(__this, method) ((  KeyValuePair_2_t3076810564  (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_get_Current_m802063697_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m896804308_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m896804308(__this, method) ((  int32_t (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_get_CurrentKey_m896804308_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3978749716_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3978749716(__this, method) ((  int32_t (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_get_CurrentValue_m3978749716_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Reset()
extern "C"  void Enumerator_Reset_m4111577196_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_Reset_m4111577196(__this, method) ((  void (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_Reset_m4111577196_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4030857077_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4030857077(__this, method) ((  void (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_VerifyState_m4030857077_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2338021789_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2338021789(__this, method) ((  void (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_VerifyCurrent_m2338021789_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m2379202108_gshared (Enumerator_t200385954 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2379202108(__this, method) ((  void (*) (Enumerator_t200385954 *, const MethodInfo*))Enumerator_Dispose_m2379202108_gshared)(__this, method)
