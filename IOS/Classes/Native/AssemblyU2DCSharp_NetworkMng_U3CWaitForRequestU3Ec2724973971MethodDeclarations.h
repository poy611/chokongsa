﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/<WaitForRequest>c__Iterator11
struct U3CWaitForRequestU3Ec__Iterator11_t2724973971;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkMng/<WaitForRequest>c__Iterator11::.ctor()
extern "C"  void U3CWaitForRequestU3Ec__Iterator11__ctor_m2048468584 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<WaitForRequest>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1652383604 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<WaitForRequest>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRequestU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m1260702472 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkMng/<WaitForRequest>c__Iterator11::MoveNext()
extern "C"  bool U3CWaitForRequestU3Ec__Iterator11_MoveNext_m1697174132 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<WaitForRequest>c__Iterator11::Dispose()
extern "C"  void U3CWaitForRequestU3Ec__Iterator11_Dispose_m1381570725 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<WaitForRequest>c__Iterator11::Reset()
extern "C"  void U3CWaitForRequestU3Ec__Iterator11_Reset_m3989868821 (U3CWaitForRequestU3Ec__Iterator11_t2724973971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
