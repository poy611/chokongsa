﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com719239868.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddForce2d
struct  AddForce2d_t4008896548  : public ComponentAction_1_t719239868
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddForce2d::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// UnityEngine.ForceMode2D HutongGames.PlayMaker.Actions.AddForce2d::forceMode
	int32_t ___forceMode_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.AddForce2d::atPosition
	FsmVector2_t533912881 * ___atPosition_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.AddForce2d::vector
	FsmVector2_t533912881 * ___vector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce2d::x
	FsmFloat_t2134102846 * ___x_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce2d::y
	FsmFloat_t2134102846 * ___y_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce2d::vector3
	FsmVector3_t533912882 * ___vector3_19;
	// System.Boolean HutongGames.PlayMaker.Actions.AddForce2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_forceMode_14() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___forceMode_14)); }
	inline int32_t get_forceMode_14() const { return ___forceMode_14; }
	inline int32_t* get_address_of_forceMode_14() { return &___forceMode_14; }
	inline void set_forceMode_14(int32_t value)
	{
		___forceMode_14 = value;
	}

	inline static int32_t get_offset_of_atPosition_15() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___atPosition_15)); }
	inline FsmVector2_t533912881 * get_atPosition_15() const { return ___atPosition_15; }
	inline FsmVector2_t533912881 ** get_address_of_atPosition_15() { return &___atPosition_15; }
	inline void set_atPosition_15(FsmVector2_t533912881 * value)
	{
		___atPosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___atPosition_15, value);
	}

	inline static int32_t get_offset_of_vector_16() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___vector_16)); }
	inline FsmVector2_t533912881 * get_vector_16() const { return ___vector_16; }
	inline FsmVector2_t533912881 ** get_address_of_vector_16() { return &___vector_16; }
	inline void set_vector_16(FsmVector2_t533912881 * value)
	{
		___vector_16 = value;
		Il2CppCodeGenWriteBarrier(&___vector_16, value);
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___x_17)); }
	inline FsmFloat_t2134102846 * get_x_17() const { return ___x_17; }
	inline FsmFloat_t2134102846 ** get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(FsmFloat_t2134102846 * value)
	{
		___x_17 = value;
		Il2CppCodeGenWriteBarrier(&___x_17, value);
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___y_18)); }
	inline FsmFloat_t2134102846 * get_y_18() const { return ___y_18; }
	inline FsmFloat_t2134102846 ** get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(FsmFloat_t2134102846 * value)
	{
		___y_18 = value;
		Il2CppCodeGenWriteBarrier(&___y_18, value);
	}

	inline static int32_t get_offset_of_vector3_19() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___vector3_19)); }
	inline FsmVector3_t533912882 * get_vector3_19() const { return ___vector3_19; }
	inline FsmVector3_t533912882 ** get_address_of_vector3_19() { return &___vector3_19; }
	inline void set_vector3_19(FsmVector3_t533912882 * value)
	{
		___vector3_19 = value;
		Il2CppCodeGenWriteBarrier(&___vector3_19, value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(AddForce2d_t4008896548, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
