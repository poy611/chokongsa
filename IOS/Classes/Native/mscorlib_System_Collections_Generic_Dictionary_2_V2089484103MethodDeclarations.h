﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4272688975MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1825060209(__this, ___host0, method) ((  void (*) (Enumerator_t2089484103 *, Dictionary_2_t4157650695 *, const MethodInfo*))Enumerator__ctor_m76754913_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3513265232(__this, method) ((  Il2CppObject * (*) (Enumerator_t2089484103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3118196448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1503377060(__this, method) ((  void (*) (Enumerator_t2089484103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3702199860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Dispose()
#define Enumerator_Dispose_m2853644883(__this, method) ((  void (*) (Enumerator_t2089484103 *, const MethodInfo*))Enumerator_Dispose_m1628348611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::MoveNext()
#define Enumerator_MoveNext_m505026759(__this, method) ((  bool (*) (Enumerator_t2089484103 *, const MethodInfo*))Enumerator_MoveNext_m3556422944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current()
#define Enumerator_get_Current_m3669063624(__this, method) ((  MultiplayerParticipant_t3337232325 * (*) (Enumerator_t2089484103 *, const MethodInfo*))Enumerator_get_Current_m841474402_gshared)(__this, method)
