﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEventToFsm
struct SendEventToFsm_t1888577243;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::.ctor()
extern "C"  void SendEventToFsm__ctor_m3624413163 (SendEventToFsm_t1888577243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::Reset()
extern "C"  void SendEventToFsm_Reset_m1270846104 (SendEventToFsm_t1888577243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::OnEnter()
extern "C"  void SendEventToFsm_OnEnter_m3529268482 (SendEventToFsm_t1888577243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::OnUpdate()
extern "C"  void SendEventToFsm_OnUpdate_m1166699905 (SendEventToFsm_t1888577243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
