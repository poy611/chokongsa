﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GradeDataInfo
struct GradeDataInfo_t1480749135;

#include "codegen/il2cpp-codegen.h"

// System.Void GradeDataInfo::.ctor()
extern "C"  void GradeDataInfo__ctor_m1021303292 (GradeDataInfo_t1480749135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GradeDataInfo::.cctor()
extern "C"  void GradeDataInfo__cctor_m1113534769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GradeDataInfo GradeDataInfo::GetInstance()
extern "C"  GradeDataInfo_t1480749135 * GradeDataInfo_GetInstance_m2964107273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
