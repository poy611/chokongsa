﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePick2dEvent
struct MousePick2dEvent_t2853398986;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePick2dEvent::.ctor()
extern "C"  void MousePick2dEvent__ctor_m4104051676 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2dEvent::Reset()
extern "C"  void MousePick2dEvent_Reset_m1750484617 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2dEvent::OnEnter()
extern "C"  void MousePick2dEvent_OnEnter_m605411507 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2dEvent::OnUpdate()
extern "C"  void MousePick2dEvent_OnUpdate_m721446896 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2dEvent::DoMousePickEvent()
extern "C"  void MousePick2dEvent_DoMousePickEvent_m1627600103 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.MousePick2dEvent::DoRaycast()
extern "C"  bool MousePick2dEvent_DoRaycast_m1027081508 (MousePick2dEvent_t2853398986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
