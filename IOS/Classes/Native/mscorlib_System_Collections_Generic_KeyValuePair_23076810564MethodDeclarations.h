﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"

// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m676088996_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m676088996(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3076810564 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m676088996_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1851180740_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1851180740(__this, method) ((  int32_t (*) (KeyValuePair_2_t3076810564 *, const MethodInfo*))KeyValuePair_2_get_Key_m1851180740_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1797699333_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1797699333(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3076810564 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1797699333_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m454436868_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m454436868(__this, method) ((  int32_t (*) (KeyValuePair_2_t3076810564 *, const MethodInfo*))KeyValuePair_2_get_Value_m454436868_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1872035973_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1872035973(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3076810564 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1872035973_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m464210813_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m464210813(__this, method) ((  String_t* (*) (KeyValuePair_2_t3076810564 *, const MethodInfo*))KeyValuePair_2_ToString_m464210813_gshared)(__this, method)
