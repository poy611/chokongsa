﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "YoutubeUnity_YoutubeExtractor_AdaptiveType3319469144.h"
#include "YoutubeUnity_YoutubeExtractor_AudioType955051966.h"
#include "mscorlib_System_String7231557.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"

// YoutubeExtractor.AdaptiveType YoutubeExtractor.VideoInfo::get_AdaptiveType()
extern "C"  int32_t VideoInfo_get_AdaptiveType_m2103200368 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_AdaptiveType(YoutubeExtractor.AdaptiveType)
extern "C"  void VideoInfo_set_AdaptiveType_m2963356775 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 YoutubeExtractor.VideoInfo::get_AudioBitrate()
extern "C"  int32_t VideoInfo_get_AudioBitrate_m4289100119 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_AudioBitrate(System.Int32)
extern "C"  void VideoInfo_set_AudioBitrate_m3700425798 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_FrameRate(System.Int32)
extern "C"  void VideoInfo_set_FrameRate_m26869282 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// YoutubeExtractor.AudioType YoutubeExtractor.VideoInfo::get_AudioType()
extern "C"  int32_t VideoInfo_get_AudioType_m3458747046 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_AudioType(YoutubeExtractor.AudioType)
extern "C"  void VideoInfo_set_AudioType_m945538481 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.VideoInfo::get_DownloadUrl()
extern "C"  String_t* VideoInfo_get_DownloadUrl_m2189851804 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_DownloadUrl(System.String)
extern "C"  void VideoInfo_set_DownloadUrl_m2337044983 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 YoutubeExtractor.VideoInfo::get_FormatCode()
extern "C"  int32_t VideoInfo_get_FormatCode_m1039643044 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_FormatCode(System.Int32)
extern "C"  void VideoInfo_set_FormatCode_m2346426515 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.VideoInfo::get_Is3D()
extern "C"  bool VideoInfo_get_Is3D_m862911713 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_Is3D(System.Boolean)
extern "C"  void VideoInfo_set_Is3D_m3760699024 (VideoInfo_t2099471191 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.VideoInfo::get_RequiresDecryption()
extern "C"  bool VideoInfo_get_RequiresDecryption_m4226650415 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_RequiresDecryption(System.Boolean)
extern "C"  void VideoInfo_set_RequiresDecryption_m3847149662 (VideoInfo_t2099471191 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_FileSize(System.Int64)
extern "C"  void VideoInfo_set_FileSize_m741874861 (VideoInfo_t2099471191 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 YoutubeExtractor.VideoInfo::get_Resolution()
extern "C"  int32_t VideoInfo_get_Resolution_m3843029452 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_Resolution(System.Int32)
extern "C"  void VideoInfo_set_Resolution_m1832971451 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.VideoInfo::get_Title()
extern "C"  String_t* VideoInfo_get_Title_m1409263085 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_Title(System.String)
extern "C"  void VideoInfo_set_Title_m3044007174 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.VideoInfo::get_VideoExtension()
extern "C"  String_t* VideoInfo_get_VideoExtension_m2516305905 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// YoutubeExtractor.VideoType YoutubeExtractor.VideoInfo::get_VideoType()
extern "C"  int32_t VideoInfo_get_VideoType_m2599923248 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_VideoType(YoutubeExtractor.VideoType)
extern "C"  void VideoInfo_set_VideoType_m2967638225 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.VideoInfo::get_HtmlPlayerVersion()
extern "C"  String_t* VideoInfo_get_HtmlPlayerVersion_m1135409505 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::set_HtmlPlayerVersion(System.String)
extern "C"  void VideoInfo_set_HtmlPlayerVersion_m3328316562 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32)
extern "C"  void VideoInfo__ctor_m2160009652 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::.ctor(YoutubeExtractor.VideoInfo)
extern "C"  void VideoInfo__ctor_m731028359 (VideoInfo_t2099471191 * __this, VideoInfo_t2099471191 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32,YoutubeExtractor.VideoType,System.Int32,System.Boolean,YoutubeExtractor.AudioType,System.Int32,YoutubeExtractor.AdaptiveType)
extern "C"  void VideoInfo__ctor_m2543604979 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, int32_t ___videoType1, int32_t ___resolution2, bool ___is3D3, int32_t ___audioType4, int32_t ___audioBitrate5, int32_t ___adaptiveType6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32,YoutubeExtractor.VideoType,System.Int32,System.Boolean,YoutubeExtractor.AudioType,System.Int32,YoutubeExtractor.AdaptiveType,System.Int32)
extern "C"  void VideoInfo__ctor_m3233131204 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, int32_t ___videoType1, int32_t ___resolution2, bool ___is3D3, int32_t ___audioType4, int32_t ___audioBitrate5, int32_t ___adaptiveType6, int32_t ___frameRate7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.VideoInfo::ToString()
extern "C"  String_t* VideoInfo_ToString_m1167467600 (VideoInfo_t2099471191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoInfo::.cctor()
extern "C"  void VideoInfo__cctor_m1582972714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
