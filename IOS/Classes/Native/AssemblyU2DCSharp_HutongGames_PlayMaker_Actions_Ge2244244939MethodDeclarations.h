﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
struct GetAnimatorLayersAffectMassCenter_t2244244939;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::.ctor()
extern "C"  void GetAnimatorLayersAffectMassCenter__ctor_m1837935563 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::Reset()
extern "C"  void GetAnimatorLayersAffectMassCenter_Reset_m3779335800 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::OnEnter()
extern "C"  void GetAnimatorLayersAffectMassCenter_OnEnter_m416245986 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::CheckAffectMassCenter()
extern "C"  void GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
