﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndVertical
struct GUILayoutEndVertical_t1014664468;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::.ctor()
extern "C"  void GUILayoutEndVertical__ctor_m3523870674 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::Reset()
extern "C"  void GUILayoutEndVertical_Reset_m1170303615 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::OnGUI()
extern "C"  void GUILayoutEndVertical_OnGUI_m3019269324 (GUILayoutEndVertical_t1014664468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
