﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddScript
struct AddScript_t95714330;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.AddScript::.ctor()
extern "C"  void AddScript__ctor_m3025765980 (AddScript_t95714330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddScript::Reset()
extern "C"  void AddScript_Reset_m672198921 (AddScript_t95714330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddScript::OnEnter()
extern "C"  void AddScript_OnEnter_m3754943283 (AddScript_t95714330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddScript::OnExit()
extern "C"  void AddScript_OnExit_m822783493 (AddScript_t95714330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddScript::DoAddComponent(UnityEngine.GameObject)
extern "C"  void AddScript_DoAddComponent_m3760538791 (AddScript_t95714330 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
