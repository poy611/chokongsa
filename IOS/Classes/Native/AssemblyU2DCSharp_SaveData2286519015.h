﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveData
struct  SaveData_t2286519015  : public Il2CppObject
{
public:
	// System.String SaveData::user_id
	String_t* ___user_id_0;
	// System.Int32 SaveData::sd_trial_th
	int32_t ___sd_trial_th_1;
	// System.Int32 SaveData::sd_grade
	int32_t ___sd_grade_2;
	// System.Int32 SaveData::sd_game_id
	int32_t ___sd_game_id_3;
	// System.Int32 SaveData::sd_guest_th
	int32_t ___sd_guest_th_4;

public:
	inline static int32_t get_offset_of_user_id_0() { return static_cast<int32_t>(offsetof(SaveData_t2286519015, ___user_id_0)); }
	inline String_t* get_user_id_0() const { return ___user_id_0; }
	inline String_t** get_address_of_user_id_0() { return &___user_id_0; }
	inline void set_user_id_0(String_t* value)
	{
		___user_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_0, value);
	}

	inline static int32_t get_offset_of_sd_trial_th_1() { return static_cast<int32_t>(offsetof(SaveData_t2286519015, ___sd_trial_th_1)); }
	inline int32_t get_sd_trial_th_1() const { return ___sd_trial_th_1; }
	inline int32_t* get_address_of_sd_trial_th_1() { return &___sd_trial_th_1; }
	inline void set_sd_trial_th_1(int32_t value)
	{
		___sd_trial_th_1 = value;
	}

	inline static int32_t get_offset_of_sd_grade_2() { return static_cast<int32_t>(offsetof(SaveData_t2286519015, ___sd_grade_2)); }
	inline int32_t get_sd_grade_2() const { return ___sd_grade_2; }
	inline int32_t* get_address_of_sd_grade_2() { return &___sd_grade_2; }
	inline void set_sd_grade_2(int32_t value)
	{
		___sd_grade_2 = value;
	}

	inline static int32_t get_offset_of_sd_game_id_3() { return static_cast<int32_t>(offsetof(SaveData_t2286519015, ___sd_game_id_3)); }
	inline int32_t get_sd_game_id_3() const { return ___sd_game_id_3; }
	inline int32_t* get_address_of_sd_game_id_3() { return &___sd_game_id_3; }
	inline void set_sd_game_id_3(int32_t value)
	{
		___sd_game_id_3 = value;
	}

	inline static int32_t get_offset_of_sd_guest_th_4() { return static_cast<int32_t>(offsetof(SaveData_t2286519015, ___sd_guest_th_4)); }
	inline int32_t get_sd_guest_th_4() const { return ___sd_guest_th_4; }
	inline int32_t* get_address_of_sd_guest_th_4() { return &___sd_guest_th_4; }
	inline void set_sd_guest_th_4(int32_t value)
	{
		___sd_guest_th_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
