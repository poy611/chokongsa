﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorTarget
struct  GetAnimatorTarget_t3370286236  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetPosition
	FsmVector3_t533912882 * ___targetPosition_15;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetRotation
	FsmQuaternion_t3871136040 * ___targetRotation_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetGameObject
	FsmGameObject_t1697147867 * ___targetGameObject_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorTarget::_animator
	Animator_t2776330603 * ____animator_18;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorTarget::_transform
	Transform_t1659122786 * ____transform_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_targetPosition_15() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ___targetPosition_15)); }
	inline FsmVector3_t533912882 * get_targetPosition_15() const { return ___targetPosition_15; }
	inline FsmVector3_t533912882 ** get_address_of_targetPosition_15() { return &___targetPosition_15; }
	inline void set_targetPosition_15(FsmVector3_t533912882 * value)
	{
		___targetPosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___targetPosition_15, value);
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ___targetRotation_16)); }
	inline FsmQuaternion_t3871136040 * get_targetRotation_16() const { return ___targetRotation_16; }
	inline FsmQuaternion_t3871136040 ** get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(FsmQuaternion_t3871136040 * value)
	{
		___targetRotation_16 = value;
		Il2CppCodeGenWriteBarrier(&___targetRotation_16, value);
	}

	inline static int32_t get_offset_of_targetGameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ___targetGameObject_17)); }
	inline FsmGameObject_t1697147867 * get_targetGameObject_17() const { return ___targetGameObject_17; }
	inline FsmGameObject_t1697147867 ** get_address_of_targetGameObject_17() { return &___targetGameObject_17; }
	inline void set_targetGameObject_17(FsmGameObject_t1697147867 * value)
	{
		___targetGameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___targetGameObject_17, value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ____animator_18)); }
	inline Animator_t2776330603 * get__animator_18() const { return ____animator_18; }
	inline Animator_t2776330603 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t2776330603 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier(&____animator_18, value);
	}

	inline static int32_t get_offset_of__transform_19() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t3370286236, ____transform_19)); }
	inline Transform_t1659122786 * get__transform_19() const { return ____transform_19; }
	inline Transform_t1659122786 ** get_address_of__transform_19() { return &____transform_19; }
	inline void set__transform_19(Transform_t1659122786 * value)
	{
		____transform_19 = value;
		Il2CppCodeGenWriteBarrier(&____transform_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
