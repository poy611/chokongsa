﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LobbyDeskAnimation/<DeskCryCo>c__IteratorF
struct U3CDeskCryCoU3Ec__IteratorF_t4183229176;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LobbyDeskAnimation/<DeskCryCo>c__IteratorF::.ctor()
extern "C"  void U3CDeskCryCoU3Ec__IteratorF__ctor_m4065725603 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LobbyDeskAnimation/<DeskCryCo>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDeskCryCoU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2471296729 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LobbyDeskAnimation/<DeskCryCo>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDeskCryCoU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m1243731053 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LobbyDeskAnimation/<DeskCryCo>c__IteratorF::MoveNext()
extern "C"  bool U3CDeskCryCoU3Ec__IteratorF_MoveNext_m2887295321 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyDeskAnimation/<DeskCryCo>c__IteratorF::Dispose()
extern "C"  void U3CDeskCryCoU3Ec__IteratorF_Dispose_m2935315488 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyDeskAnimation/<DeskCryCo>c__IteratorF::Reset()
extern "C"  void U3CDeskCryCoU3Ec__IteratorF_Reset_m1712158544 (U3CDeskCryCoU3Ec__IteratorF_t4183229176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
