﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayGetNext
struct ArrayGetNext_t2083890424;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::.ctor()
extern "C"  void ArrayGetNext__ctor_m1183260782 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::Reset()
extern "C"  void ArrayGetNext_Reset_m3124661019 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::OnEnter()
extern "C"  void ArrayGetNext_OnEnter_m2633973957 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::DoGetNextItem()
extern "C"  void ArrayGetNext_DoGetNextItem_m2178209501 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
