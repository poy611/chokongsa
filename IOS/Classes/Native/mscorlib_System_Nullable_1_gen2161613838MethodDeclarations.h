﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2161613838.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandl2077487315.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m560761413_gshared (Nullable_1_t2161613838 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m560761413(__this, ___value0, method) ((  void (*) (Nullable_1_t2161613838 *, int32_t, const MethodInfo*))Nullable_1__ctor_m560761413_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m442800161_gshared (Nullable_1_t2161613838 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m442800161(__this, method) ((  bool (*) (Nullable_1_t2161613838 *, const MethodInfo*))Nullable_1_get_HasValue_m442800161_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1643018816_gshared (Nullable_1_t2161613838 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1643018816(__this, method) ((  int32_t (*) (Nullable_1_t2161613838 *, const MethodInfo*))Nullable_1_get_Value_m1643018816_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m596422952_gshared (Nullable_1_t2161613838 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m596422952(__this, ___other0, method) ((  bool (*) (Nullable_1_t2161613838 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m596422952_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1168819479_gshared (Nullable_1_t2161613838 * __this, Nullable_1_t2161613838  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1168819479(__this, ___other0, method) ((  bool (*) (Nullable_1_t2161613838 *, Nullable_1_t2161613838 , const MethodInfo*))Nullable_1_Equals_m1168819479_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1037108364_gshared (Nullable_1_t2161613838 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1037108364(__this, method) ((  int32_t (*) (Nullable_1_t2161613838 *, const MethodInfo*))Nullable_1_GetHashCode_m1037108364_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3775229578_gshared (Nullable_1_t2161613838 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3775229578(__this, method) ((  int32_t (*) (Nullable_1_t2161613838 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3775229578_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2827617827_gshared (Nullable_1_t2161613838 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2827617827(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2161613838 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m2827617827_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m816962746_gshared (Nullable_1_t2161613838 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m816962746(__this, method) ((  String_t* (*) (Nullable_1_t2161613838 *, const MethodInfo*))Nullable_1_ToString_m816962746_gshared)(__this, method)
