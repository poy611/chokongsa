﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultWindowEnable/<WaitTemp>c__Iterator32
struct U3CWaitTempU3Ec__Iterator32_t3760889752;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultWindowEnable/<WaitTemp>c__Iterator32::.ctor()
extern "C"  void U3CWaitTempU3Ec__Iterator32__ctor_m2008589315 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResultWindowEnable/<WaitTemp>c__Iterator32::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitTempU3Ec__Iterator32_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2838119289 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResultWindowEnable/<WaitTemp>c__Iterator32::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitTempU3Ec__Iterator32_System_Collections_IEnumerator_get_Current_m324819213 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultWindowEnable/<WaitTemp>c__Iterator32::MoveNext()
extern "C"  bool U3CWaitTempU3Ec__Iterator32_MoveNext_m3628486137 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultWindowEnable/<WaitTemp>c__Iterator32::Dispose()
extern "C"  void U3CWaitTempU3Ec__Iterator32_Dispose_m1712298880 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultWindowEnable/<WaitTemp>c__Iterator32::Reset()
extern "C"  void U3CWaitTempU3Ec__Iterator32_Reset_m3949989552 (U3CWaitTempU3Ec__Iterator32_t3760889752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
