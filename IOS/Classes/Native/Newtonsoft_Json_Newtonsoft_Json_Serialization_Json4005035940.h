﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>
struct ThreadSafeStore_2_t3297694544;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_t3797250685;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3124613658;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t4005035940  : public Il2CppObject
{
public:

public:
};

struct JsonTypeReflector_t4005035940_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t560925241  ____fullyTrusted_0;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>> Newtonsoft.Json.Serialization.JsonTypeReflector::JsonConverterCreatorCache
	ThreadSafeStore_2_t3297694544 * ___JsonConverterCreatorCache_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_t3797250685 * ___AssociatedMetadataTypesCache_2;
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_t3124613658 * ____metadataTypeAttributeReflectionObject_3;

public:
	inline static int32_t get_offset_of__fullyTrusted_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t4005035940_StaticFields, ____fullyTrusted_0)); }
	inline Nullable_1_t560925241  get__fullyTrusted_0() const { return ____fullyTrusted_0; }
	inline Nullable_1_t560925241 * get_address_of__fullyTrusted_0() { return &____fullyTrusted_0; }
	inline void set__fullyTrusted_0(Nullable_1_t560925241  value)
	{
		____fullyTrusted_0 = value;
	}

	inline static int32_t get_offset_of_JsonConverterCreatorCache_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t4005035940_StaticFields, ___JsonConverterCreatorCache_1)); }
	inline ThreadSafeStore_2_t3297694544 * get_JsonConverterCreatorCache_1() const { return ___JsonConverterCreatorCache_1; }
	inline ThreadSafeStore_2_t3297694544 ** get_address_of_JsonConverterCreatorCache_1() { return &___JsonConverterCreatorCache_1; }
	inline void set_JsonConverterCreatorCache_1(ThreadSafeStore_2_t3297694544 * value)
	{
		___JsonConverterCreatorCache_1 = value;
		Il2CppCodeGenWriteBarrier(&___JsonConverterCreatorCache_1, value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t4005035940_StaticFields, ___AssociatedMetadataTypesCache_2)); }
	inline ThreadSafeStore_2_t3797250685 * get_AssociatedMetadataTypesCache_2() const { return ___AssociatedMetadataTypesCache_2; }
	inline ThreadSafeStore_2_t3797250685 ** get_address_of_AssociatedMetadataTypesCache_2() { return &___AssociatedMetadataTypesCache_2; }
	inline void set_AssociatedMetadataTypesCache_2(ThreadSafeStore_2_t3797250685 * value)
	{
		___AssociatedMetadataTypesCache_2 = value;
		Il2CppCodeGenWriteBarrier(&___AssociatedMetadataTypesCache_2, value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t4005035940_StaticFields, ____metadataTypeAttributeReflectionObject_3)); }
	inline ReflectionObject_t3124613658 * get__metadataTypeAttributeReflectionObject_3() const { return ____metadataTypeAttributeReflectionObject_3; }
	inline ReflectionObject_t3124613658 ** get_address_of__metadataTypeAttributeReflectionObject_3() { return &____metadataTypeAttributeReflectionObject_3; }
	inline void set__metadataTypeAttributeReflectionObject_3(ReflectionObject_t3124613658 * value)
	{
		____metadataTypeAttributeReflectionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&____metadataTypeAttributeReflectionObject_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
