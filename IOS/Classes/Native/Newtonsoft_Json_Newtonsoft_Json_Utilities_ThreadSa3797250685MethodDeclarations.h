﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m1951931982(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t3797250685 *, Func_2_t1107244013 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::Get(TKey)
#define ThreadSafeStore_2_Get_m3040894088(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3797250685 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1755708170(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3797250685 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
