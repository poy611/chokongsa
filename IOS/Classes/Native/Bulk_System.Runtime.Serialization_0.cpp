﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;
// System.String
struct String_t;
// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t1202205191;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_Runtime_Serialization_U3CModuleU3E86524790.h"
#include "System_Runtime_Serialization_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2462274566.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2462274566MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2601848894.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2601848894MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Int321153838500.h"
#include "System_Runtime_Serialization_System_Runtime_Serial1202205191.h"
#include "System_Runtime_Serialization_System_Runtime_Serial1202205191MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean System.Runtime.Serialization.DataContractAttribute::get_IsReference()
extern "C"  bool DataContractAttribute_get_IsReference_m1562831651 (DataContractAttribute_t2462274566 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsReferenceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Boolean System.Runtime.Serialization.DataMemberAttribute::get_EmitDefaultValue()
extern "C"  bool DataMemberAttribute_get_EmitDefaultValue_m29364571 (DataMemberAttribute_t2601848894 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_emit_default_1();
		return L_0;
	}
}
// System.Boolean System.Runtime.Serialization.DataMemberAttribute::get_IsRequired()
extern "C"  bool DataMemberAttribute_get_IsRequired_m464274657 (DataMemberAttribute_t2601848894 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_required_0();
		return L_0;
	}
}
// System.String System.Runtime.Serialization.DataMemberAttribute::get_Name()
extern "C"  String_t* DataMemberAttribute_get_Name_m2466648376 (DataMemberAttribute_t2601848894 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_2();
		return L_0;
	}
}
// System.Int32 System.Runtime.Serialization.DataMemberAttribute::get_Order()
extern "C"  int32_t DataMemberAttribute_get_Order_m2253361886 (DataMemberAttribute_t2601848894 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_order_3();
		return L_0;
	}
}
// System.String System.Runtime.Serialization.EnumMemberAttribute::get_Value()
extern "C"  String_t* EnumMemberAttribute_get_Value_m4250259485 (EnumMemberAttribute_t1202205191 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_value_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
