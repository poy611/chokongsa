﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m998904344_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m998904344(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m998904344_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m1360890551_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 * __this, int32_t ___result0, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m1360890551(__this, ___result0, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 *, int32_t, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m1360890551_gshared)(__this, ___result0, method)
