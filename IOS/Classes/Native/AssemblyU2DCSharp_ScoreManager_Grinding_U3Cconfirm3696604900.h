﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// ScoreManager_Grinding
struct ScoreManager_Grinding_t1076868946;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Grinding/<confirm>c__Iterator1A
struct  U3CconfirmU3Ec__Iterator1A_t3696604900  : public Il2CppObject
{
public:
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<timmerMinus>__0
	float ___U3CtimmerMinusU3E__0_0;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<errorRate>__1
	float ___U3CerrorRateU3E__1_1;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<errorMinus>__2
	float ___U3CerrorMinusU3E__2_2;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<limitTime>__3
	float ___U3ClimitTimeU3E__3_3;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<goalPoint>__4
	float ___U3CgoalPointU3E__4_4;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<currentTime>__5
	float ___U3CcurrentTimeU3E__5_5;
	// System.Single ScoreManager_Grinding/<confirm>c__Iterator1A::<gaugePoint>__6
	float ___U3CgaugePointU3E__6_6;
	// System.Int32 ScoreManager_Grinding/<confirm>c__Iterator1A::$PC
	int32_t ___U24PC_7;
	// System.Object ScoreManager_Grinding/<confirm>c__Iterator1A::$current
	Il2CppObject * ___U24current_8;
	// ScoreManager_Grinding ScoreManager_Grinding/<confirm>c__Iterator1A::<>f__this
	ScoreManager_Grinding_t1076868946 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CtimmerMinusU3E__0_0() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CtimmerMinusU3E__0_0)); }
	inline float get_U3CtimmerMinusU3E__0_0() const { return ___U3CtimmerMinusU3E__0_0; }
	inline float* get_address_of_U3CtimmerMinusU3E__0_0() { return &___U3CtimmerMinusU3E__0_0; }
	inline void set_U3CtimmerMinusU3E__0_0(float value)
	{
		___U3CtimmerMinusU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CerrorRateU3E__1_1() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CerrorRateU3E__1_1)); }
	inline float get_U3CerrorRateU3E__1_1() const { return ___U3CerrorRateU3E__1_1; }
	inline float* get_address_of_U3CerrorRateU3E__1_1() { return &___U3CerrorRateU3E__1_1; }
	inline void set_U3CerrorRateU3E__1_1(float value)
	{
		___U3CerrorRateU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CerrorMinusU3E__2_2() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CerrorMinusU3E__2_2)); }
	inline float get_U3CerrorMinusU3E__2_2() const { return ___U3CerrorMinusU3E__2_2; }
	inline float* get_address_of_U3CerrorMinusU3E__2_2() { return &___U3CerrorMinusU3E__2_2; }
	inline void set_U3CerrorMinusU3E__2_2(float value)
	{
		___U3CerrorMinusU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3ClimitTimeU3E__3_3() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3ClimitTimeU3E__3_3)); }
	inline float get_U3ClimitTimeU3E__3_3() const { return ___U3ClimitTimeU3E__3_3; }
	inline float* get_address_of_U3ClimitTimeU3E__3_3() { return &___U3ClimitTimeU3E__3_3; }
	inline void set_U3ClimitTimeU3E__3_3(float value)
	{
		___U3ClimitTimeU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CgoalPointU3E__4_4() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CgoalPointU3E__4_4)); }
	inline float get_U3CgoalPointU3E__4_4() const { return ___U3CgoalPointU3E__4_4; }
	inline float* get_address_of_U3CgoalPointU3E__4_4() { return &___U3CgoalPointU3E__4_4; }
	inline void set_U3CgoalPointU3E__4_4(float value)
	{
		___U3CgoalPointU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__5_5() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CcurrentTimeU3E__5_5)); }
	inline float get_U3CcurrentTimeU3E__5_5() const { return ___U3CcurrentTimeU3E__5_5; }
	inline float* get_address_of_U3CcurrentTimeU3E__5_5() { return &___U3CcurrentTimeU3E__5_5; }
	inline void set_U3CcurrentTimeU3E__5_5(float value)
	{
		___U3CcurrentTimeU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CgaugePointU3E__6_6() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CgaugePointU3E__6_6)); }
	inline float get_U3CgaugePointU3E__6_6() const { return ___U3CgaugePointU3E__6_6; }
	inline float* get_address_of_U3CgaugePointU3E__6_6() { return &___U3CgaugePointU3E__6_6; }
	inline void set_U3CgaugePointU3E__6_6(float value)
	{
		___U3CgaugePointU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CconfirmU3Ec__Iterator1A_t3696604900, ___U3CU3Ef__this_9)); }
	inline ScoreManager_Grinding_t1076868946 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline ScoreManager_Grinding_t1076868946 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(ScoreManager_Grinding_t1076868946 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
