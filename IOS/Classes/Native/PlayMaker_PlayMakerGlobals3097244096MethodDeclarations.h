﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t3097244096;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C"  bool PlayMakerGlobals_get_Initialized_m645627118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C"  void PlayMakerGlobals_set_Initialized_m3462637545 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C"  bool PlayMakerGlobals_get_IsPlayingInEditor_m680276368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlayingInEditor_m4196468235 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsPlaying()
extern "C"  bool PlayMakerGlobals_get_IsPlaying_m1647553534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlaying_m1932520441 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C"  bool PlayMakerGlobals_get_IsEditor_m4210118399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsEditor_m1626540722 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C"  bool PlayMakerGlobals_get_IsBuilding_m1160993350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsBuilding(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsBuilding_m1085677241 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C"  void PlayMakerGlobals_InitApplicationFlags_m3042944910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::Initialize()
extern "C"  void PlayMakerGlobals_Initialize_m3132596247 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern "C"  PlayMakerGlobals_t3097244096 * PlayMakerGlobals_get_Instance_m3643716518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::ResetInstance()
extern "C"  void PlayMakerGlobals_ResetInstance_m173162943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::get_Variables()
extern "C"  FsmVariables_t963491929 * PlayMakerGlobals_get_Variables_m733169483 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerGlobals_set_Variables_m1711829458 (PlayMakerGlobals_t3097244096 * __this, FsmVariables_t963491929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C"  List_1_t1375417109 * PlayMakerGlobals_get_Events_m1315403806 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayMakerGlobals_set_Events_m990891217 (PlayMakerGlobals_t3097244096 * __this, List_1_t1375417109 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent PlayMakerGlobals::AddEvent(System.String)
extern "C"  FsmEvent_t2133468028 * PlayMakerGlobals_AddEvent_m1265974759 (PlayMakerGlobals_t3097244096 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::OnEnable()
extern "C"  void PlayMakerGlobals_OnEnable_m201299401 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::OnDestroy()
extern "C"  void PlayMakerGlobals_OnDestroy_m268816150 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::.ctor()
extern "C"  void PlayMakerGlobals__ctor_m1305972893 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
