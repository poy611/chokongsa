﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider
struct GUILayoutHorizontalSlider_t3080788440;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::.ctor()
extern "C"  void GUILayoutHorizontalSlider__ctor_m1744088670 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::Reset()
extern "C"  void GUILayoutHorizontalSlider_Reset_m3685488907 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::OnGUI()
extern "C"  void GUILayoutHorizontalSlider_OnGUI_m1239487320 (GUILayoutHorizontalSlider_t3080788440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
