﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen1001010649MethodDeclarations.h"

// System.Void System.Func`1<Newtonsoft.Json.JsonSerializerSettings>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m76965131(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t3714567099 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2414815204_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<Newtonsoft.Json.JsonSerializerSettings>::Invoke()
#define Func_1_Invoke_m3008846620(__this, method) ((  JsonSerializerSettings_t2589405525 * (*) (Func_1_t3714567099 *, const MethodInfo*))Func_1_Invoke_m382220194_gshared)(__this, method)
// System.IAsyncResult System.Func`1<Newtonsoft.Json.JsonSerializerSettings>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m4126785478(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t3714567099 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m4066620266_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<Newtonsoft.Json.JsonSerializerSettings>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m1157916729(__this, ___result0, method) ((  JsonSerializerSettings_t2589405525 * (*) (Func_1_t3714567099 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3356710033_gshared)(__this, ___result0, method)
