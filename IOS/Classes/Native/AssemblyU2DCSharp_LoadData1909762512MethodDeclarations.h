﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadData
struct LoadData_t1909762512;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadData::.ctor()
extern "C"  void LoadData__ctor_m1552293579 (LoadData_t1909762512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
