﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4131627146(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2363589633 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m420802513_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>::Invoke(T)
#define Func_2_Invoke_m2071704280(__this, ___arg10, method) ((  JsonConverter_t2159686854 * (*) (Func_2_t2363589633 *, ObjectU5BU5D_t1108656482*, const MethodInfo*))Func_2_Invoke_m1009799647_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m855067500(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2363589633 *, ObjectU5BU5D_t1108656482*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m197680183(__this, ___result0, method) ((  JsonConverter_t2159686854 * (*) (Func_2_t2363589633 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
