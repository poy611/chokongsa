﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomVector2
struct SelectRandomVector2_t3679113470;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector2::.ctor()
extern "C"  void SelectRandomVector2__ctor_m1581051896 (SelectRandomVector2_t3679113470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector2::Reset()
extern "C"  void SelectRandomVector2_Reset_m3522452133 (SelectRandomVector2_t3679113470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector2::OnEnter()
extern "C"  void SelectRandomVector2_OnEnter_m2659145167 (SelectRandomVector2_t3679113470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector2::DoSelectRandomColor()
extern "C"  void SelectRandomVector2_DoSelectRandomColor_m3517310607 (SelectRandomVector2_t3679113470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
