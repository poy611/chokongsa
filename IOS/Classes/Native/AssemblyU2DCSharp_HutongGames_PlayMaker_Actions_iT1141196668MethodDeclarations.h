﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateBy
struct iTweenRotateBy_t1141196668;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::.ctor()
extern "C"  void iTweenRotateBy__ctor_m455109738 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::Reset()
extern "C"  void iTweenRotateBy_Reset_m2396509975 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnEnter()
extern "C"  void iTweenRotateBy_OnEnter_m2960489921 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnExit()
extern "C"  void iTweenRotateBy_OnExit_m2736818615 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::DoiTween()
extern "C"  void iTweenRotateBy_DoiTween_m3032006567 (iTweenRotateBy_t1141196668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
