﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t3498949300;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CallMethod
struct  CallMethod_t4181362247  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CallMethod::behaviour
	FsmObject_t821476169 * ___behaviour_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallMethod::methodName
	FsmString_t952858651 * ___methodName_12;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.CallMethod::parameters
	FsmVarU5BU5D_t3498949300* ___parameters_13;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.CallMethod::storeResult
	FsmVar_t1596150537 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::everyFrame
	bool ___everyFrame_15;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CallMethod::cachedBehaviour
	FsmObject_t821476169 * ___cachedBehaviour_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallMethod::cachedMethodName
	FsmString_t952858651 * ___cachedMethodName_17;
	// System.Type HutongGames.PlayMaker.Actions.CallMethod::cachedType
	Type_t * ___cachedType_18;
	// System.Reflection.MethodInfo HutongGames.PlayMaker.Actions.CallMethod::cachedMethodInfo
	MethodInfo_t * ___cachedMethodInfo_19;
	// System.Reflection.ParameterInfo[] HutongGames.PlayMaker.Actions.CallMethod::cachedParameterInfo
	ParameterInfoU5BU5D_t2015293532* ___cachedParameterInfo_20;
	// System.Object[] HutongGames.PlayMaker.Actions.CallMethod::parametersArray
	ObjectU5BU5D_t1108656482* ___parametersArray_21;
	// System.String HutongGames.PlayMaker.Actions.CallMethod::errorString
	String_t* ___errorString_22;

public:
	inline static int32_t get_offset_of_behaviour_11() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___behaviour_11)); }
	inline FsmObject_t821476169 * get_behaviour_11() const { return ___behaviour_11; }
	inline FsmObject_t821476169 ** get_address_of_behaviour_11() { return &___behaviour_11; }
	inline void set_behaviour_11(FsmObject_t821476169 * value)
	{
		___behaviour_11 = value;
		Il2CppCodeGenWriteBarrier(&___behaviour_11, value);
	}

	inline static int32_t get_offset_of_methodName_12() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___methodName_12)); }
	inline FsmString_t952858651 * get_methodName_12() const { return ___methodName_12; }
	inline FsmString_t952858651 ** get_address_of_methodName_12() { return &___methodName_12; }
	inline void set_methodName_12(FsmString_t952858651 * value)
	{
		___methodName_12 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_12, value);
	}

	inline static int32_t get_offset_of_parameters_13() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___parameters_13)); }
	inline FsmVarU5BU5D_t3498949300* get_parameters_13() const { return ___parameters_13; }
	inline FsmVarU5BU5D_t3498949300** get_address_of_parameters_13() { return &___parameters_13; }
	inline void set_parameters_13(FsmVarU5BU5D_t3498949300* value)
	{
		___parameters_13 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_13, value);
	}

	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___storeResult_14)); }
	inline FsmVar_t1596150537 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmVar_t1596150537 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmVar_t1596150537 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_cachedBehaviour_16() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___cachedBehaviour_16)); }
	inline FsmObject_t821476169 * get_cachedBehaviour_16() const { return ___cachedBehaviour_16; }
	inline FsmObject_t821476169 ** get_address_of_cachedBehaviour_16() { return &___cachedBehaviour_16; }
	inline void set_cachedBehaviour_16(FsmObject_t821476169 * value)
	{
		___cachedBehaviour_16 = value;
		Il2CppCodeGenWriteBarrier(&___cachedBehaviour_16, value);
	}

	inline static int32_t get_offset_of_cachedMethodName_17() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___cachedMethodName_17)); }
	inline FsmString_t952858651 * get_cachedMethodName_17() const { return ___cachedMethodName_17; }
	inline FsmString_t952858651 ** get_address_of_cachedMethodName_17() { return &___cachedMethodName_17; }
	inline void set_cachedMethodName_17(FsmString_t952858651 * value)
	{
		___cachedMethodName_17 = value;
		Il2CppCodeGenWriteBarrier(&___cachedMethodName_17, value);
	}

	inline static int32_t get_offset_of_cachedType_18() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___cachedType_18)); }
	inline Type_t * get_cachedType_18() const { return ___cachedType_18; }
	inline Type_t ** get_address_of_cachedType_18() { return &___cachedType_18; }
	inline void set_cachedType_18(Type_t * value)
	{
		___cachedType_18 = value;
		Il2CppCodeGenWriteBarrier(&___cachedType_18, value);
	}

	inline static int32_t get_offset_of_cachedMethodInfo_19() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___cachedMethodInfo_19)); }
	inline MethodInfo_t * get_cachedMethodInfo_19() const { return ___cachedMethodInfo_19; }
	inline MethodInfo_t ** get_address_of_cachedMethodInfo_19() { return &___cachedMethodInfo_19; }
	inline void set_cachedMethodInfo_19(MethodInfo_t * value)
	{
		___cachedMethodInfo_19 = value;
		Il2CppCodeGenWriteBarrier(&___cachedMethodInfo_19, value);
	}

	inline static int32_t get_offset_of_cachedParameterInfo_20() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___cachedParameterInfo_20)); }
	inline ParameterInfoU5BU5D_t2015293532* get_cachedParameterInfo_20() const { return ___cachedParameterInfo_20; }
	inline ParameterInfoU5BU5D_t2015293532** get_address_of_cachedParameterInfo_20() { return &___cachedParameterInfo_20; }
	inline void set_cachedParameterInfo_20(ParameterInfoU5BU5D_t2015293532* value)
	{
		___cachedParameterInfo_20 = value;
		Il2CppCodeGenWriteBarrier(&___cachedParameterInfo_20, value);
	}

	inline static int32_t get_offset_of_parametersArray_21() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___parametersArray_21)); }
	inline ObjectU5BU5D_t1108656482* get_parametersArray_21() const { return ___parametersArray_21; }
	inline ObjectU5BU5D_t1108656482** get_address_of_parametersArray_21() { return &___parametersArray_21; }
	inline void set_parametersArray_21(ObjectU5BU5D_t1108656482* value)
	{
		___parametersArray_21 = value;
		Il2CppCodeGenWriteBarrier(&___parametersArray_21, value);
	}

	inline static int32_t get_offset_of_errorString_22() { return static_cast<int32_t>(offsetof(CallMethod_t4181362247, ___errorString_22)); }
	inline String_t* get_errorString_22() const { return ___errorString_22; }
	inline String_t** get_address_of_errorString_22() { return &___errorString_22; }
	inline void set_errorString_22(String_t* value)
	{
		___errorString_22 = value;
		Il2CppCodeGenWriteBarrier(&___errorString_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
