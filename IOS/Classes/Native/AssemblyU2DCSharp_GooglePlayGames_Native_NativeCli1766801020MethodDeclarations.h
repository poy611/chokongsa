﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52
struct U3CLoadUsersU3Ec__AnonStorey52_t1766801020;
// GooglePlayGames.Native.PInvoke.NativePlayer[]
struct NativePlayerU5BU5D_t3461726221;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52::.ctor()
extern "C"  void U3CLoadUsersU3Ec__AnonStorey52__ctor_m2188216735 (U3CLoadUsersU3Ec__AnonStorey52_t1766801020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52::<>m__27(GooglePlayGames.Native.PInvoke.NativePlayer[])
extern "C"  void U3CLoadUsersU3Ec__AnonStorey52_U3CU3Em__27_m3763606248 (U3CLoadUsersU3Ec__AnonStorey52_t1766801020 * __this, NativePlayerU5BU5D_t3461726221* ___nativeUsers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
