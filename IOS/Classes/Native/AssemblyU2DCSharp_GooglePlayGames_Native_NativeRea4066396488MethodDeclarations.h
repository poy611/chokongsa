﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F
struct U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t2530940439;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_R2530940439.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::.ctor()
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6F__ctor_m1362105939 (U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::<>m__4C()
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6F_U3CU3Em__4C_m1326929707 (U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::<>m__4D(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6F_U3CU3Em__4D_m345067343 (U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488 * __this, RealTimeRoomResponse_t2530940439 * ___acceptResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
