﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4
struct U3CWaitForLoginU3Ec__Iterator4_t4284811612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::.ctor()
extern "C"  void U3CWaitForLoginU3Ec__Iterator4__ctor_m1811087039 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForLoginU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1017941309 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForLoginU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1368566481 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::MoveNext()
extern "C"  bool U3CWaitForLoginU3Ec__Iterator4_MoveNext_m2426299837 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::Dispose()
extern "C"  void U3CWaitForLoginU3Ec__Iterator4_Dispose_m891172668 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::Reset()
extern "C"  void U3CWaitForLoginU3Ec__Iterator4_Reset_m3752487276 (U3CWaitForLoginU3Ec__Iterator4_t4284811612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
