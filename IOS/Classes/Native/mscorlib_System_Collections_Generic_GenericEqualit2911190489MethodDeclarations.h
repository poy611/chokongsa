﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct GenericEqualityComparer_1_t2911190489;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1913217735_gshared (GenericEqualityComparer_1_t2911190489 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1913217735(__this, method) ((  void (*) (GenericEqualityComparer_1_t2911190489 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1913217735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m2899730180_gshared (GenericEqualityComparer_1_t2911190489 * __this, TypeNameKey_t2971844791  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m2899730180(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t2911190489 *, TypeNameKey_t2971844791 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m2899730180_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2878010904_gshared (GenericEqualityComparer_1_t2911190489 * __this, TypeNameKey_t2971844791  ___x0, TypeNameKey_t2971844791  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m2878010904(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t2911190489 *, TypeNameKey_t2971844791 , TypeNameKey_t2971844791 , const MethodInfo*))GenericEqualityComparer_1_Equals_m2878010904_gshared)(__this, ___x0, ___y1, method)
