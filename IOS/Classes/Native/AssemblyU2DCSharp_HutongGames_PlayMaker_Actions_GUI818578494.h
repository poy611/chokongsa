﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct  GUILayoutBeginScrollView_t818578494  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::scrollPosition
	FsmVector2_t533912881 * ___scrollPosition_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalScrollbar
	FsmBool_t1075959796 * ___horizontalScrollbar_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalScrollbar
	FsmBool_t1075959796 * ___verticalScrollbar_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::useCustomStyle
	FsmBool_t1075959796 * ___useCustomStyle_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalStyle
	FsmString_t952858651 * ___horizontalStyle_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalStyle
	FsmString_t952858651 * ___verticalStyle_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::backgroundStyle
	FsmString_t952858651 * ___backgroundStyle_19;

public:
	inline static int32_t get_offset_of_scrollPosition_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___scrollPosition_13)); }
	inline FsmVector2_t533912881 * get_scrollPosition_13() const { return ___scrollPosition_13; }
	inline FsmVector2_t533912881 ** get_address_of_scrollPosition_13() { return &___scrollPosition_13; }
	inline void set_scrollPosition_13(FsmVector2_t533912881 * value)
	{
		___scrollPosition_13 = value;
		Il2CppCodeGenWriteBarrier(&___scrollPosition_13, value);
	}

	inline static int32_t get_offset_of_horizontalScrollbar_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___horizontalScrollbar_14)); }
	inline FsmBool_t1075959796 * get_horizontalScrollbar_14() const { return ___horizontalScrollbar_14; }
	inline FsmBool_t1075959796 ** get_address_of_horizontalScrollbar_14() { return &___horizontalScrollbar_14; }
	inline void set_horizontalScrollbar_14(FsmBool_t1075959796 * value)
	{
		___horizontalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalScrollbar_14, value);
	}

	inline static int32_t get_offset_of_verticalScrollbar_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___verticalScrollbar_15)); }
	inline FsmBool_t1075959796 * get_verticalScrollbar_15() const { return ___verticalScrollbar_15; }
	inline FsmBool_t1075959796 ** get_address_of_verticalScrollbar_15() { return &___verticalScrollbar_15; }
	inline void set_verticalScrollbar_15(FsmBool_t1075959796 * value)
	{
		___verticalScrollbar_15 = value;
		Il2CppCodeGenWriteBarrier(&___verticalScrollbar_15, value);
	}

	inline static int32_t get_offset_of_useCustomStyle_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___useCustomStyle_16)); }
	inline FsmBool_t1075959796 * get_useCustomStyle_16() const { return ___useCustomStyle_16; }
	inline FsmBool_t1075959796 ** get_address_of_useCustomStyle_16() { return &___useCustomStyle_16; }
	inline void set_useCustomStyle_16(FsmBool_t1075959796 * value)
	{
		___useCustomStyle_16 = value;
		Il2CppCodeGenWriteBarrier(&___useCustomStyle_16, value);
	}

	inline static int32_t get_offset_of_horizontalStyle_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___horizontalStyle_17)); }
	inline FsmString_t952858651 * get_horizontalStyle_17() const { return ___horizontalStyle_17; }
	inline FsmString_t952858651 ** get_address_of_horizontalStyle_17() { return &___horizontalStyle_17; }
	inline void set_horizontalStyle_17(FsmString_t952858651 * value)
	{
		___horizontalStyle_17 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalStyle_17, value);
	}

	inline static int32_t get_offset_of_verticalStyle_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___verticalStyle_18)); }
	inline FsmString_t952858651 * get_verticalStyle_18() const { return ___verticalStyle_18; }
	inline FsmString_t952858651 ** get_address_of_verticalStyle_18() { return &___verticalStyle_18; }
	inline void set_verticalStyle_18(FsmString_t952858651 * value)
	{
		___verticalStyle_18 = value;
		Il2CppCodeGenWriteBarrier(&___verticalStyle_18, value);
	}

	inline static int32_t get_offset_of_backgroundStyle_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t818578494, ___backgroundStyle_19)); }
	inline FsmString_t952858651 * get_backgroundStyle_19() const { return ___backgroundStyle_19; }
	inline FsmString_t952858651 ** get_address_of_backgroundStyle_19() { return &___backgroundStyle_19; }
	inline void set_backgroundStyle_19(FsmString_t952858651 * value)
	{
		___backgroundStyle_19 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundStyle_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
