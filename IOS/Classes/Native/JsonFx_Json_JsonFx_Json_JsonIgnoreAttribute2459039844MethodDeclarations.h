﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern "C"  bool JsonIgnoreAttribute_IsJsonIgnore_m4133572969 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern "C"  bool JsonIgnoreAttribute_IsXmlIgnore_m816865530 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
