﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmString
struct GetFsmString_t2891746531;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmString::.ctor()
extern "C"  void GetFsmString__ctor_m1704915171 (GetFsmString_t2891746531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::Reset()
extern "C"  void GetFsmString_Reset_m3646315408 (GetFsmString_t2891746531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnEnter()
extern "C"  void GetFsmString_OnEnter_m1432668154 (GetFsmString_t2891746531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnUpdate()
extern "C"  void GetFsmString_OnUpdate_m596599177 (GetFsmString_t2891746531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::DoGetFsmString()
extern "C"  void GetFsmString_DoGetFsmString_m3328963943 (GetFsmString_t2891746531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
