﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyComponent
struct DestroyComponent_t1933763019;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::.ctor()
extern "C"  void DestroyComponent__ctor_m1906300155 (DestroyComponent_t1933763019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::Reset()
extern "C"  void DestroyComponent_Reset_m3847700392 (DestroyComponent_t1933763019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::OnEnter()
extern "C"  void DestroyComponent_OnEnter_m1690109458 (DestroyComponent_t1933763019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::DoDestroyComponent(UnityEngine.GameObject)
extern "C"  void DestroyComponent_DoDestroyComponent_m4082829423 (DestroyComponent_t1933763019 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
