﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3794119044(__this, ___l0, method) ((  void (*) (Enumerator_t820418645 *, List_1_t800745875 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4236150862(__this, method) ((  void (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1188936122(__this, method) ((  Il2CppObject * (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::Dispose()
#define Enumerator_Dispose_m1492244585(__this, method) ((  void (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::VerifyState()
#define Enumerator_VerifyState_m3410049826(__this, method) ((  void (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::MoveNext()
#define Enumerator_MoveNext_m4208695482(__this, method) ((  bool (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Player>::get_Current()
#define Enumerator_get_Current_m1531513561(__this, method) ((  Player_t3727527619 * (*) (Enumerator_t820418645 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
