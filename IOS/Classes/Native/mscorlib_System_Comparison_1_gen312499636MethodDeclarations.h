﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<HutongGames.PlayMaker.FsmInt>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3037090798(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t312499636 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<HutongGames.PlayMaker.FsmInt>::Invoke(T,T)
#define Comparison_1_Invoke_m395119562(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t312499636 *, FsmInt_t1596138449 *, FsmInt_t1596138449 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<HutongGames.PlayMaker.FsmInt>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m4185968403(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t312499636 *, FsmInt_t1596138449 *, FsmInt_t1596138449 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<HutongGames.PlayMaker.FsmInt>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3311627362(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t312499636 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
