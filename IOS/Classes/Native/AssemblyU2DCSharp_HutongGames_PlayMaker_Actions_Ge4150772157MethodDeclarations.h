﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
struct GetQuaternionEulerAngles_t4150772157;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::.ctor()
extern "C"  void GetQuaternionEulerAngles__ctor_m37301577 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::Reset()
extern "C"  void GetQuaternionEulerAngles_Reset_m1978701814 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnEnter()
extern "C"  void GetQuaternionEulerAngles_OnEnter_m878805728 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnUpdate()
extern "C"  void GetQuaternionEulerAngles_OnUpdate_m606733155 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnLateUpdate()
extern "C"  void GetQuaternionEulerAngles_OnLateUpdate_m3778371497 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnFixedUpdate()
extern "C"  void GetQuaternionEulerAngles_OnFixedUpdate_m2041344741 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::GetQuatEuler()
extern "C"  void GetQuaternionEulerAngles_GetQuatEuler_m3347674999 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
