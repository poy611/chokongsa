﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerFSM/<DoCoroutine>d__1
struct U3CDoCoroutineU3Ed__1_t3331551771;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Boolean PlayMakerFSM/<DoCoroutine>d__1::MoveNext()
extern "C"  bool U3CDoCoroutineU3Ed__1_MoveNext_m3610815678 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2225328651 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.Reset()
extern "C"  void U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m2342883224 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.IDisposable.Dispose()
extern "C"  void U3CDoCoroutineU3Ed__1_System_IDisposable_Dispose_m2465789885 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_get_Current_m1954460494 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM/<DoCoroutine>d__1::.ctor(System.Int32)
extern "C"  void U3CDoCoroutineU3Ed__1__ctor_m2949731187 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
