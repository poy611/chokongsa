﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2209551164(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2523429889 *, String_t*, Participant_t1804230813 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Key()
#define KeyValuePair_2_get_Key_m1381078316(__this, method) ((  String_t* (*) (KeyValuePair_2_t2523429889 *, const MethodInfo*))KeyValuePair_2_get_Key_m2940899609_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3432264045(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2523429889 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Value()
#define KeyValuePair_2_get_Value_m1553851756(__this, method) ((  Participant_t1804230813 * (*) (KeyValuePair_2_t2523429889 *, const MethodInfo*))KeyValuePair_2_get_Value_m4250204908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m258934509(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2523429889 *, Participant_t1804230813 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::ToString()
#define KeyValuePair_2_ToString_m648874901(__this, method) ((  String_t* (*) (KeyValuePair_2_t2523429889 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
