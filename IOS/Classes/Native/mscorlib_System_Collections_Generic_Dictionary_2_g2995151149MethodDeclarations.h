﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct Dictionary_2_t2995151149;
// System.Collections.Generic.IEqualityComparer`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct IEqualityComparer_1_t1264835409;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct IDictionary_2_t2573024494;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct ICollection_1_t1368390992;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>[]
struct KeyValuePair_2U5BU5D_t3455395990;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>
struct IEnumerator_1_t510829608;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct KeyCollection_t326943304;
// System.Collections.Generic.Dictionary`2/ValueCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct ValueCollection_t1695756862;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu17507245.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m356498773_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m356498773(__this, method) ((  void (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2__ctor_m356498773_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1383809868_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1383809868(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1383809868_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4259126115_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4259126115(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4259126115_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m849025830_gshared (Dictionary_2_t2995151149 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m849025830(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2995151149 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m849025830_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m858504314_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m858504314(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m858504314_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1084264599_gshared (Dictionary_2_t2995151149 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1084264599(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2995151149 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1084264599_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m66565398_gshared (Dictionary_2_t2995151149 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m66565398(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2995151149 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m66565398_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1214844121_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1214844121(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1214844121_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1445254619_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1445254619(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1445254619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m541788109_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m541788109(__this, method) ((  bool (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m541788109_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m778862099_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m778862099(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m778862099_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m85216248_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m85216248(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m85216248_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3418380121_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3418380121(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3418380121_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2090107005_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2090107005(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2090107005_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1292744246_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1292744246(__this, ___key0, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1292744246_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2303739895_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2303739895(__this, method) ((  bool (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2303739895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1036867619_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1036867619(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1036867619_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2540047739_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2540047739(__this, method) ((  bool (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2540047739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2796048332_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2_t2893931855  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2796048332(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2995151149 *, KeyValuePair_2_t2893931855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2796048332_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2062552054_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2_t2893931855  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2062552054(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2995151149 *, KeyValuePair_2_t2893931855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2062552054_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2444584112_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2U5BU5D_t3455395990* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2444584112(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2995151149 *, KeyValuePair_2U5BU5D_t3455395990*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2444584112_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1205647515_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2_t2893931855  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1205647515(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2995151149 *, KeyValuePair_2_t2893931855 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1205647515_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3686313359_gshared (Dictionary_2_t2995151149 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3686313359(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3686313359_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3168067146_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3168067146(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3168067146_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1545861191_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1545861191(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1545861191_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2841191650_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2841191650(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2841191650_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m113930301_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m113930301(__this, method) ((  int32_t (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_get_Count_m113930301_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1905163790_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1905163790(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , const MethodInfo*))Dictionary_2_get_Item_m1905163790_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m775912021_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m775912021(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m775912021_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m765422797_gshared (Dictionary_2_t2995151149 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m765422797(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2995151149 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m765422797_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2944144010_gshared (Dictionary_2_t2995151149 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2944144010(__this, ___size0, method) ((  void (*) (Dictionary_2_t2995151149 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2944144010_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2411640070_gshared (Dictionary_2_t2995151149 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2411640070(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2411640070_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2893931855  Dictionary_2_make_pair_m130855506_gshared (Il2CppObject * __this /* static, unused */, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m130855506(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2893931855  (*) (Il2CppObject * /* static, unused */, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m130855506_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::pick_key(TKey,TValue)
extern "C"  ResolverContractKey_t473801005  Dictionary_2_pick_key_m698626404_gshared (Il2CppObject * __this /* static, unused */, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m698626404(__this /* static, unused */, ___key0, ___value1, method) ((  ResolverContractKey_t473801005  (*) (Il2CppObject * /* static, unused */, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m698626404_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m129587748_gshared (Il2CppObject * __this /* static, unused */, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m129587748(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m129587748_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2254251017_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2U5BU5D_t3455395990* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2254251017(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2995151149 *, KeyValuePair_2U5BU5D_t3455395990*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2254251017_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2519324035_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2519324035(__this, method) ((  void (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_Resize_m2519324035_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m579774208_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m579774208(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m579774208_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2057599360_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2057599360(__this, method) ((  void (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_Clear_m2057599360_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3370108774_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3370108774(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , const MethodInfo*))Dictionary_2_ContainsKey_m3370108774_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m798816358_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m798816358(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m798816358_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3180693107_gshared (Dictionary_2_t2995151149 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3180693107(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2995151149 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3180693107_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2871746001_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2871746001(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2871746001_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2871978442_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2871978442(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , const MethodInfo*))Dictionary_2_Remove_m2871978442_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3472447935_gshared (Dictionary_2_t2995151149 * __this, ResolverContractKey_t473801005  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3472447935(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2995151149 *, ResolverContractKey_t473801005 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3472447935_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Keys()
extern "C"  KeyCollection_t326943304 * Dictionary_2_get_Keys_m4169842080_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4169842080(__this, method) ((  KeyCollection_t326943304 * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_get_Keys_m4169842080_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Values()
extern "C"  ValueCollection_t1695756862 * Dictionary_2_get_Values_m662388000_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m662388000(__this, method) ((  ValueCollection_t1695756862 * (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_get_Values_m662388000_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToTKey(System.Object)
extern "C"  ResolverContractKey_t473801005  Dictionary_2_ToTKey_m148485311_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m148485311(__this, ___key0, method) ((  ResolverContractKey_t473801005  (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m148485311_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2736869183_gshared (Dictionary_2_t2995151149 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2736869183(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2995151149 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2736869183_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m446482349_gshared (Dictionary_2_t2995151149 * __this, KeyValuePair_2_t2893931855  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m446482349(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2995151149 *, KeyValuePair_2_t2893931855 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m446482349_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t17507245  Dictionary_2_GetEnumerator_m252558810_gshared (Dictionary_2_t2995151149 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m252558810(__this, method) ((  Enumerator_t17507245  (*) (Dictionary_2_t2995151149 *, const MethodInfo*))Dictionary_2_GetEnumerator_m252558810_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m560342953_gshared (Il2CppObject * __this /* static, unused */, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m560342953(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, ResolverContractKey_t473801005 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m560342953_gshared)(__this /* static, unused */, ___key0, ___value1, method)
