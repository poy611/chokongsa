﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct ShimEnumerator_t20141587;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m827321969_gshared (ShimEnumerator_t20141587 * __this, Dictionary_2_t304363560 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m827321969(__this, ___host0, method) ((  void (*) (ShimEnumerator_t20141587 *, Dictionary_2_t304363560 *, const MethodInfo*))ShimEnumerator__ctor_m827321969_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m503075088_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m503075088(__this, method) ((  bool (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_MoveNext_m503075088_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2266723844_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2266723844(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_get_Entry_m2266723844_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1719232095_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1719232095(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_get_Key_m1719232095_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3963038001_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3963038001(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_get_Value_m3963038001_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m268218233_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m268218233(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_get_Current_m268218233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Reset()
extern "C"  void ShimEnumerator_Reset_m3215290179_gshared (ShimEnumerator_t20141587 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3215290179(__this, method) ((  void (*) (ShimEnumerator_t20141587 *, const MethodInfo*))ShimEnumerator_Reset_m3215290179_gshared)(__this, method)
