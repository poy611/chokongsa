﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41
struct U3CGetAncestorsU3Ed__41_t2219391906;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::.ctor(System.Int32)
extern "C"  void U3CGetAncestorsU3Ed__41__ctor_m4049231855 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.IDisposable.Dispose()
extern "C"  void U3CGetAncestorsU3Ed__41_System_IDisposable_Dispose_m3228579521 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::MoveNext()
extern "C"  bool U3CGetAncestorsU3Ed__41_MoveNext_m2917006624 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CGetAncestorsU3Ed__41_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m3094545149 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.Collections.IEnumerator.Reset()
extern "C"  void U3CGetAncestorsU3Ed__41_System_Collections_IEnumerator_Reset_m736942492 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAncestorsU3Ed__41_System_Collections_IEnumerator_get_Current_m2892878152 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetAncestorsU3Ed__41_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m541710504 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAncestorsU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m2078057101 (U3CGetAncestorsU3Ed__41_t2219391906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
