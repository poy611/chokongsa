﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKeyDown
struct GetKeyDown_t2999335123;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::.ctor()
extern "C"  void GetKeyDown__ctor_m4150057203 (GetKeyDown_t2999335123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::Reset()
extern "C"  void GetKeyDown_Reset_m1796490144 (GetKeyDown_t2999335123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::OnUpdate()
extern "C"  void GetKeyDown_OnUpdate_m1177534329 (GetKeyDown_t2999335123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
