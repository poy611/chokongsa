﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.CompoundArrayAttribute
struct CompoundArrayAttribute_t2338634768;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.CompoundArrayAttribute::get_Name()
extern "C"  String_t* CompoundArrayAttribute_get_Name_m386044184 (CompoundArrayAttribute_t2338634768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.CompoundArrayAttribute::get_FirstArrayName()
extern "C"  String_t* CompoundArrayAttribute_get_FirstArrayName_m1730240769 (CompoundArrayAttribute_t2338634768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.CompoundArrayAttribute::get_SecondArrayName()
extern "C"  String_t* CompoundArrayAttribute_get_SecondArrayName_m395109285 (CompoundArrayAttribute_t2338634768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CompoundArrayAttribute::.ctor(System.String,System.String,System.String)
extern "C"  void CompoundArrayAttribute__ctor_m200420503 (CompoundArrayAttribute_t2338634768 * __this, String_t* ___name0, String_t* ___firstArrayName1, String_t* ___secondArrayName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
