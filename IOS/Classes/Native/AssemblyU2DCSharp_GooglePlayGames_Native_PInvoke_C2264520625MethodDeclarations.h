﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m1193894963_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m1193894963(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m1193894963_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>::<>m__9E(T1,T2)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 * __this, Il2CppObject * ___result10, Il2CppObject * ___result21, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813(__this, ___result10, ___result21, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813_gshared)(__this, ___result10, ___result21, method)
