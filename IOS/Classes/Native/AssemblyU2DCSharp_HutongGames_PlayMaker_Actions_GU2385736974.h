﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIHorizontalSlider
struct  GUIHorizontalSlider_t2385736974  : public GUIAction_t3055477407
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::leftValue
	FsmFloat_t2134102846 * ___leftValue_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::rightValue
	FsmFloat_t2134102846 * ___rightValue_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIHorizontalSlider::sliderStyle
	FsmString_t952858651 * ___sliderStyle_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIHorizontalSlider::thumbStyle
	FsmString_t952858651 * ___thumbStyle_22;

public:
	inline static int32_t get_offset_of_floatVariable_18() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t2385736974, ___floatVariable_18)); }
	inline FsmFloat_t2134102846 * get_floatVariable_18() const { return ___floatVariable_18; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_18() { return &___floatVariable_18; }
	inline void set_floatVariable_18(FsmFloat_t2134102846 * value)
	{
		___floatVariable_18 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_18, value);
	}

	inline static int32_t get_offset_of_leftValue_19() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t2385736974, ___leftValue_19)); }
	inline FsmFloat_t2134102846 * get_leftValue_19() const { return ___leftValue_19; }
	inline FsmFloat_t2134102846 ** get_address_of_leftValue_19() { return &___leftValue_19; }
	inline void set_leftValue_19(FsmFloat_t2134102846 * value)
	{
		___leftValue_19 = value;
		Il2CppCodeGenWriteBarrier(&___leftValue_19, value);
	}

	inline static int32_t get_offset_of_rightValue_20() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t2385736974, ___rightValue_20)); }
	inline FsmFloat_t2134102846 * get_rightValue_20() const { return ___rightValue_20; }
	inline FsmFloat_t2134102846 ** get_address_of_rightValue_20() { return &___rightValue_20; }
	inline void set_rightValue_20(FsmFloat_t2134102846 * value)
	{
		___rightValue_20 = value;
		Il2CppCodeGenWriteBarrier(&___rightValue_20, value);
	}

	inline static int32_t get_offset_of_sliderStyle_21() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t2385736974, ___sliderStyle_21)); }
	inline FsmString_t952858651 * get_sliderStyle_21() const { return ___sliderStyle_21; }
	inline FsmString_t952858651 ** get_address_of_sliderStyle_21() { return &___sliderStyle_21; }
	inline void set_sliderStyle_21(FsmString_t952858651 * value)
	{
		___sliderStyle_21 = value;
		Il2CppCodeGenWriteBarrier(&___sliderStyle_21, value);
	}

	inline static int32_t get_offset_of_thumbStyle_22() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t2385736974, ___thumbStyle_22)); }
	inline FsmString_t952858651 * get_thumbStyle_22() const { return ___thumbStyle_22; }
	inline FsmString_t952858651 ** get_address_of_thumbStyle_22() { return &___thumbStyle_22; }
	inline void set_thumbStyle_22(FsmString_t952858651 * value)
	{
		___thumbStyle_22 = value;
		Il2CppCodeGenWriteBarrier(&___thumbStyle_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
