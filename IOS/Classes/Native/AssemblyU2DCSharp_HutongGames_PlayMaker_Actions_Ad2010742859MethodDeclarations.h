﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddTorque2d
struct AddTorque2d_t2010742859;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::.ctor()
extern "C"  void AddTorque2d__ctor_m1568771915 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnPreprocess()
extern "C"  void AddTorque2d_OnPreprocess_m1721094020 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::Reset()
extern "C"  void AddTorque2d_Reset_m3510172152 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnEnter()
extern "C"  void AddTorque2d_OnEnter_m3742985314 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnFixedUpdate()
extern "C"  void AddTorque2d_OnFixedUpdate_m1744548583 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::DoAddTorque()
extern "C"  void AddTorque2d_DoAddTorque_m1158537257 (AddTorque2d_t2010742859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
