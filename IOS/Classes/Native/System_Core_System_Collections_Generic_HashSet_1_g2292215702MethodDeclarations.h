﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct HashSet_1_t2292215702;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3928821330;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerable_1_t2143732587;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerator_1_t754684679;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t2382929755;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2679459368.h"

// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor()
extern "C"  void HashSet_1__ctor_m2684562293_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m2684562293(__this, method) ((  void (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1__ctor_m2684562293_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m1218058393_gshared (HashSet_1_t2292215702 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m1218058393(__this, ___comparer0, method) ((  void (*) (HashSet_1_t2292215702 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1218058393_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m1489452818_gshared (HashSet_1_t2292215702 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m1489452818(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t2292215702 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1489452818_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m1470774342_gshared (HashSet_1_t2292215702 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m1470774342(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2292215702 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))HashSet_1__ctor_m1470774342_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3908461135_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3908461135(__this, method) ((  Il2CppObject* (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3908461135_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2108046272_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2108046272(__this, method) ((  bool (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2108046272_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2960124170_gshared (HashSet_1_t2292215702 * __this, ParticipantStatusU5BU5D_t2382929755* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2960124170(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2292215702 *, ParticipantStatusU5BU5D_t2382929755*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m2960124170_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m507638682_gshared (HashSet_1_t2292215702 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m507638682(__this, ___item0, method) ((  void (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m507638682_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m4272484700_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m4272484700(__this, method) ((  Il2CppObject * (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m4272484700_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m1347770999_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m1347770999(__this, method) ((  int32_t (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_get_Count_m1347770999_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m3534851768_gshared (HashSet_1_t2292215702 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m3534851768(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t2292215702 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m3534851768_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m1077193050_gshared (HashSet_1_t2292215702 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m1077193050(__this, ___size0, method) ((  void (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1077193050_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m1268288960_gshared (HashSet_1_t2292215702 * __this, int32_t ___index0, int32_t ___hash1, int32_t ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m1268288960(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t2292215702 *, int32_t, int32_t, int32_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m1268288960_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m3418223818_gshared (HashSet_1_t2292215702 * __this, ParticipantStatusU5BU5D_t2382929755* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m3418223818(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2292215702 *, ParticipantStatusU5BU5D_t2382929755*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m3418223818_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m2075312205_gshared (HashSet_1_t2292215702 * __this, ParticipantStatusU5BU5D_t2382929755* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m2075312205(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t2292215702 *, ParticipantStatusU5BU5D_t2382929755*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2075312205_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Resize()
extern "C"  void HashSet_1_Resize_m2282359891_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2282359891(__this, method) ((  void (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_Resize_m2282359891_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m3631786573_gshared (HashSet_1_t2292215702 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m3631786573(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m3631786573_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m3415855401_gshared (HashSet_1_t2292215702 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m3415855401(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_GetItemHashCode_m3415855401_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Add(T)
extern "C"  bool HashSet_1_Add_m1680942105_gshared (HashSet_1_t2292215702 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Add_m1680942105(__this, ___item0, method) ((  bool (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_Add_m1680942105_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Clear()
extern "C"  void HashSet_1_Clear_m2742692016_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m2742692016(__this, method) ((  void (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_Clear_m2742692016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Contains(T)
extern "C"  bool HashSet_1_Contains_m1806231499_gshared (HashSet_1_t2292215702 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m1806231499(__this, ___item0, method) ((  bool (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_Contains_m1806231499_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Remove(T)
extern "C"  bool HashSet_1_Remove_m442960943_gshared (HashSet_1_t2292215702 * __this, int32_t ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m442960943(__this, ___item0, method) ((  bool (*) (HashSet_1_t2292215702 *, int32_t, const MethodInfo*))HashSet_1_Remove_m442960943_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m581295523_gshared (HashSet_1_t2292215702 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m581295523(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2292215702 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))HashSet_1_GetObjectData_m581295523_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m2491950241_gshared (HashSet_1_t2292215702 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m2491950241(__this, ___sender0, method) ((  void (*) (HashSet_1_t2292215702 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m2491950241_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetEnumerator()
extern "C"  Enumerator_t2679459368  HashSet_1_GetEnumerator_m3034668625_gshared (HashSet_1_t2292215702 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m3034668625(__this, method) ((  Enumerator_t2679459368  (*) (HashSet_1_t2292215702 *, const MethodInfo*))HashSet_1_GetEnumerator_m3034668625_gshared)(__this, method)
