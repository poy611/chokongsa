﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::GetAttribute(System.Object)
extern "C"  Il2CppObject * CachedAttributeGetter_1_GetAttribute_m3559125336_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___type0, const MethodInfo* method);
#define CachedAttributeGetter_1_GetAttribute_m3559125336(__this /* static, unused */, ___type0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m3559125336_gshared)(__this /* static, unused */, ___type0, method)
// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::.cctor()
extern "C"  void CachedAttributeGetter_1__cctor_m3626143424_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define CachedAttributeGetter_1__cctor_m3626143424(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m3626143424_gshared)(__this /* static, unused */, method)
