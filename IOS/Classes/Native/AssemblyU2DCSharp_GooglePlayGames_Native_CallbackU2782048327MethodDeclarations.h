﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m706101797_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m706101797(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m706101797_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>::<>m__14()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2267037969_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2267037969(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2267037969_gshared)(__this, method)
