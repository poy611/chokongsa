﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowRecipePre
struct ShowRecipePre_t977589144;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowRecipePre::.ctor()
extern "C"  void ShowRecipePre__ctor_m2362317011 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre::Start()
extern "C"  void ShowRecipePre_Start_m1309454803 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre::OnEnable()
extern "C"  void ShowRecipePre_OnEnable_m523540947 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShowRecipePre::enableCoru()
extern "C"  Il2CppObject * ShowRecipePre_enableCoru_m1233405435 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre::RightChoice()
extern "C"  void ShowRecipePre_RightChoice_m3387411854 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre::IcingRightChoice()
extern "C"  void ShowRecipePre_IcingRightChoice_m3568626182 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShowRecipePre::FocusDown()
extern "C"  Il2CppObject * ShowRecipePre_FocusDown_m3625254227 (ShowRecipePre_t977589144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
