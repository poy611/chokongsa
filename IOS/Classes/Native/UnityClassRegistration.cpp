struct ClassRegistrationContext;
void InvokeRegisterStaticallyLinkedModuleClasses(ClassRegistrationContext& context)
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_AI();
	RegisterModule_AI();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

}

void RegisterAllClasses()
{
	//Total: 99 classes
	//0. RenderSettings
	void RegisterClass_RenderSettings();
	RegisterClass_RenderSettings();

	//1. LevelGameManager
	void RegisterClass_LevelGameManager();
	RegisterClass_LevelGameManager();

	//2. GameManager
	void RegisterClass_GameManager();
	RegisterClass_GameManager();

	//3. MeshFilter
	void RegisterClass_MeshFilter();
	RegisterClass_MeshFilter();

	//4. Component
	void RegisterClass_Component();
	RegisterClass_Component();

	//5. EditorExtension
	void RegisterClass_EditorExtension();
	RegisterClass_EditorExtension();

	//6. Renderer
	void RegisterClass_Renderer();
	RegisterClass_Renderer();

	//7. GUIElement
	void RegisterClass_GUIElement();
	RegisterClass_GUIElement();

	//8. Behaviour
	void RegisterClass_Behaviour();
	RegisterClass_Behaviour();

	//9. GUITexture
	void RegisterClass_GUITexture();
	RegisterClass_GUITexture();

	//10. GUILayer
	void RegisterClass_GUILayer();
	RegisterClass_GUILayer();

	//11. Texture
	void RegisterClass_Texture();
	RegisterClass_Texture();

	//12. NamedObject
	void RegisterClass_NamedObject();
	RegisterClass_NamedObject();

	//13. Texture2D
	void RegisterClass_Texture2D();
	RegisterClass_Texture2D();

	//14. RenderTexture
	void RegisterClass_RenderTexture();
	RegisterClass_RenderTexture();

	//15. Mesh
	void RegisterClass_Mesh();
	RegisterClass_Mesh();

	//16. NetworkView
	void RegisterClass_NetworkView();
	RegisterClass_NetworkView();

	//17. RectTransform
	void RegisterClass_RectTransform();
	RegisterClass_RectTransform();

	//18. Transform
	void RegisterClass_Transform();
	RegisterClass_Transform();

	//19. Shader
	void RegisterClass_Shader();
	RegisterClass_Shader();

	//20. TextAsset
	void RegisterClass_TextAsset();
	RegisterClass_TextAsset();

	//21. Material
	void RegisterClass_Material();
	RegisterClass_Material();

	//22. Sprite
	void RegisterClass_Sprite();
	RegisterClass_Sprite();

	//23. Camera
	void RegisterClass_Camera();
	RegisterClass_Camera();

	//24. MonoBehaviour
	void RegisterClass_MonoBehaviour();
	RegisterClass_MonoBehaviour();

	//25. Light
	void RegisterClass_Light();
	RegisterClass_Light();

	//26. GameObject
	void RegisterClass_GameObject();
	RegisterClass_GameObject();

	//27. Motion
	void RegisterClass_Motion();
	RegisterClass_Motion();

	//28. ParticleSystem
	void RegisterClass_ParticleSystem();
	RegisterClass_ParticleSystem();

	//29. Rigidbody
	void RegisterClass_Rigidbody();
	RegisterClass_Rigidbody();

	//30. Joint
	void RegisterClass_Joint();
	RegisterClass_Joint();

	//31. SpringJoint
	void RegisterClass_SpringJoint();
	RegisterClass_SpringJoint();

	//32. Collider
	void RegisterClass_Collider();
	RegisterClass_Collider();

	//33. CharacterController
	void RegisterClass_CharacterController();
	RegisterClass_CharacterController();

	//34. Rigidbody2D
	void RegisterClass_Rigidbody2D();
	RegisterClass_Rigidbody2D();

	//35. Collider2D
	void RegisterClass_Collider2D();
	RegisterClass_Collider2D();

	//36. Joint2D
	void RegisterClass_Joint2D();
	RegisterClass_Joint2D();

	//37. HingeJoint2D
	void RegisterClass_HingeJoint2D();
	RegisterClass_HingeJoint2D();

	//38. AnchoredJoint2D
	void RegisterClass_AnchoredJoint2D();
	RegisterClass_AnchoredJoint2D();

	//39. WheelJoint2D
	void RegisterClass_WheelJoint2D();
	RegisterClass_WheelJoint2D();

	//40. NavMeshAgent
	void RegisterClass_NavMeshAgent();
	RegisterClass_NavMeshAgent();

	//41. AudioClip
	void RegisterClass_AudioClip();
	RegisterClass_AudioClip();

	//42. SampleClip
	void RegisterClass_SampleClip();
	RegisterClass_SampleClip();

	//43. AudioListener
	void RegisterClass_AudioListener();
	RegisterClass_AudioListener();

	//44. AudioBehaviour
	void RegisterClass_AudioBehaviour();
	RegisterClass_AudioBehaviour();

	//45. AudioSource
	void RegisterClass_AudioSource();
	RegisterClass_AudioSource();

	//46. AnimationClip
	void RegisterClass_AnimationClip();
	RegisterClass_AnimationClip();

	//47. Animation
	void RegisterClass_Animation();
	RegisterClass_Animation();

	//48. Animator
	void RegisterClass_Animator();
	RegisterClass_Animator();

	//49. DirectorPlayer
	void RegisterClass_DirectorPlayer();
	RegisterClass_DirectorPlayer();

	//50. GUIText
	void RegisterClass_GUIText();
	RegisterClass_GUIText();

	//51. TextMesh
	void RegisterClass_TextMesh();
	RegisterClass_TextMesh();

	//52. Font
	void RegisterClass_Font();
	RegisterClass_Font();

	//53. Canvas
	void RegisterClass_Canvas();
	RegisterClass_Canvas();

	//54. CanvasGroup
	void RegisterClass_CanvasGroup();
	RegisterClass_CanvasGroup();

	//55. CanvasRenderer
	void RegisterClass_CanvasRenderer();
	RegisterClass_CanvasRenderer();

	//56. MeshRenderer
	void RegisterClass_MeshRenderer();
	RegisterClass_MeshRenderer();

	//57. PhysicMaterial
	void RegisterClass_PhysicMaterial();
	RegisterClass_PhysicMaterial();

	//58. PhysicsMaterial2D
	void RegisterClass_PhysicsMaterial2D();
	RegisterClass_PhysicsMaterial2D();

	//59. Flare
	void RegisterClass_Flare();
	RegisterClass_Flare();

	//60. SpriteRenderer
	void RegisterClass_SpriteRenderer();
	RegisterClass_SpriteRenderer();

	//61. RuntimeAnimatorController
	void RegisterClass_RuntimeAnimatorController();
	RegisterClass_RuntimeAnimatorController();

	//62. PreloadData
	void RegisterClass_PreloadData();
	RegisterClass_PreloadData();

	//63. Cubemap
	void RegisterClass_Cubemap();
	RegisterClass_Cubemap();

	//64. Texture3D
	void RegisterClass_Texture3D();
	RegisterClass_Texture3D();

	//65. Texture2DArray
	void RegisterClass_Texture2DArray();
	RegisterClass_Texture2DArray();

	//66. TimeManager
	void RegisterClass_TimeManager();
	RegisterClass_TimeManager();

	//67. GlobalGameManager
	void RegisterClass_GlobalGameManager();
	RegisterClass_GlobalGameManager();

	//68. AudioManager
	void RegisterClass_AudioManager();
	RegisterClass_AudioManager();

	//69. InputManager
	void RegisterClass_InputManager();
	RegisterClass_InputManager();

	//70. Physics2DSettings
	void RegisterClass_Physics2DSettings();
	RegisterClass_Physics2DSettings();

	//71. GraphicsSettings
	void RegisterClass_GraphicsSettings();
	RegisterClass_GraphicsSettings();

	//72. QualitySettings
	void RegisterClass_QualitySettings();
	RegisterClass_QualitySettings();

	//73. PhysicsManager
	void RegisterClass_PhysicsManager();
	RegisterClass_PhysicsManager();

	//74. MeshCollider
	void RegisterClass_MeshCollider();
	RegisterClass_MeshCollider();

	//75. BoxCollider
	void RegisterClass_BoxCollider();
	RegisterClass_BoxCollider();

	//76. TagManager
	void RegisterClass_TagManager();
	RegisterClass_TagManager();

	//77. Avatar
	void RegisterClass_Avatar();
	RegisterClass_Avatar();

	//78. AnimatorController
	void RegisterClass_AnimatorController();
	RegisterClass_AnimatorController();

	//79. ScriptMapper
	void RegisterClass_ScriptMapper();
	RegisterClass_ScriptMapper();

	//80. DelayedCallManager
	void RegisterClass_DelayedCallManager();
	RegisterClass_DelayedCallManager();

	//81. MonoScript
	void RegisterClass_MonoScript();
	RegisterClass_MonoScript();

	//82. MonoManager
	void RegisterClass_MonoManager();
	RegisterClass_MonoManager();

	//83. FlareLayer
	void RegisterClass_FlareLayer();
	RegisterClass_FlareLayer();

	//84. NavMeshAreas
	void RegisterClass_NavMeshAreas();
	RegisterClass_NavMeshAreas();

	//85. PlayerSettings
	void RegisterClass_PlayerSettings();
	RegisterClass_PlayerSettings();

	//86. CapsuleCollider
	void RegisterClass_CapsuleCollider();
	RegisterClass_CapsuleCollider();

	//87. SkinnedMeshRenderer
	void RegisterClass_SkinnedMeshRenderer();
	RegisterClass_SkinnedMeshRenderer();

	//88. BuildSettings
	void RegisterClass_BuildSettings();
	RegisterClass_BuildSettings();

	//89. ResourceManager
	void RegisterClass_ResourceManager();
	RegisterClass_ResourceManager();

	//90. NetworkManager
	void RegisterClass_NetworkManager();
	RegisterClass_NetworkManager();

	//91. MasterServerInterface
	void RegisterClass_MasterServerInterface();
	RegisterClass_MasterServerInterface();

	//92. LightmapSettings
	void RegisterClass_LightmapSettings();
	RegisterClass_LightmapSettings();

	//93. ParticleSystemRenderer
	void RegisterClass_ParticleSystemRenderer();
	RegisterClass_ParticleSystemRenderer();

	//94. LightProbes
	void RegisterClass_LightProbes();
	RegisterClass_LightProbes();

	//95. RuntimeInitializeOnLoadManager
	void RegisterClass_RuntimeInitializeOnLoadManager();
	RegisterClass_RuntimeInitializeOnLoadManager();

	//96. CloudWebServicesManager
	void RegisterClass_CloudWebServicesManager();
	RegisterClass_CloudWebServicesManager();

	//97. UnityConnectSettings
	void RegisterClass_UnityConnectSettings();
	RegisterClass_UnityConnectSettings();

	//98. NavMeshSettings
	void RegisterClass_NavMeshSettings();
	RegisterClass_NavMeshSettings();

}
