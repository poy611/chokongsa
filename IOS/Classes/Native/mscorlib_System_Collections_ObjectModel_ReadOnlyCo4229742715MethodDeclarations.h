﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>
struct ReadOnlyCollection_1_t4229742715;
// System.Collections.Generic.IList`1<HutongGames.PlayMaker.ParamDataType>
struct IList_1_t1072345086;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t3909450202;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t289562932;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1487145259_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1487145259(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1487145259_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3719933333_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3719933333(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3719933333_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1360507349_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1360507349(__this, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1360507349_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2715679292_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2715679292(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2715679292_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m790519230_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m790519230(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m790519230_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m589532162_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m589532162(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m589532162_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3898062598_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3898062598(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3898062598_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m237280275_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m237280275(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m237280275_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2256932369_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2256932369(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2256932369_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m960010330_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m960010330(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m960010330_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m234185173_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m234185173(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m234185173_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2936347964_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2936347964(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2936347964_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3587018600_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3587018600(__this, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3587018600_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2134209040_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2134209040(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2134209040_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4022097300_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4022097300(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4022097300_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m96672703_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m96672703(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m96672703_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2316526345_gshared (ReadOnlyCollection_1_t4229742715 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2316526345(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2316526345_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m383228367_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m383228367(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m383228367_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1815604044_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1815604044(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1815604044_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2607372600_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2607372600(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2607372600_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m645918079_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m645918079(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m645918079_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2275960218_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2275960218(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2275960218_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2784746687_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2784746687(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2784746687_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3322106838_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3322106838(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3322106838_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3467444483_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3467444483(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3467444483_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3697815493_gshared (ReadOnlyCollection_1_t4229742715 * __this, ParamDataTypeU5BU5D_t3909450202* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3697815493(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4229742715 *, ParamDataTypeU5BU5D_t3909450202*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3697815493_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m568264678_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m568264678(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m568264678_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4266999305_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4266999305(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4266999305_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3245267090_gshared (ReadOnlyCollection_1_t4229742715 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3245267090(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3245267090_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3196871622_gshared (ReadOnlyCollection_1_t4229742715 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3196871622(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4229742715 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3196871622_gshared)(__this, ___index0, method)
