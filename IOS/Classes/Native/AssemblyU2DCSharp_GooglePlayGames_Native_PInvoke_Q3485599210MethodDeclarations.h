﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
struct AcceptResponse_t3485599210;
// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1491989755.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::.ctor(System.IntPtr)
extern "C"  void AcceptResponse__ctor_m2001832451 (AcceptResponse_t3485599210 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::ResponseStatus()
extern "C"  int32_t AcceptResponse_ResponseStatus_m2106765950 (AcceptResponse_t3485599210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::AcceptedQuest()
extern "C"  NativeQuest_t2496300529 * AcceptResponse_AcceptedQuest_m2820113269 (AcceptResponse_t3485599210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::RequestSucceeded()
extern "C"  bool AcceptResponse_RequestSucceeded_m790719993 (AcceptResponse_t3485599210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void AcceptResponse_CallDispose_m2462371853 (AcceptResponse_t3485599210 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::FromPointer(System.IntPtr)
extern "C"  AcceptResponse_t3485599210 * AcceptResponse_FromPointer_m3851194661 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
