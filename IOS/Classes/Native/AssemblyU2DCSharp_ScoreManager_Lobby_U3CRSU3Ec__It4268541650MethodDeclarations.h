﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Lobby/<RS>c__Iterator21
struct U3CRSU3Ec__Iterator21_t4268541650;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Lobby/<RS>c__Iterator21::.ctor()
extern "C"  void U3CRSU3Ec__Iterator21__ctor_m2449600521 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<RS>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRSU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416652019 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<RS>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRSU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3498697863 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Lobby/<RS>c__Iterator21::MoveNext()
extern "C"  bool U3CRSU3Ec__Iterator21_MoveNext_m3154842419 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<RS>c__Iterator21::Dispose()
extern "C"  void U3CRSU3Ec__Iterator21_Dispose_m322305542 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<RS>c__Iterator21::Reset()
extern "C"  void U3CRSU3Ec__Iterator21_Reset_m96033462 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
