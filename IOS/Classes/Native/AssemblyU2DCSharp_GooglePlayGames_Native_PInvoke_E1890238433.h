﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.NativeEvent>
struct Func_2_t619068207;

#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_B2237584300.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct  FetchAllResponse_t1890238433  : public BaseReferenceHolder_t2237584300
{
public:

public:
};

struct FetchAllResponse_t1890238433_StaticFields
{
public:
	// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.NativeEvent> GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<>f__am$cache0
	Func_2_t619068207 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(FetchAllResponse_t1890238433_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t619068207 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t619068207 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t619068207 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
