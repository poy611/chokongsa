﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowRecipe
struct ShowRecipe_t2733639435;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowRecipe::.ctor()
extern "C"  void ShowRecipe__ctor_m3357232624 (ShowRecipe_t2733639435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipe::Start()
extern "C"  void ShowRecipe_Start_m2304370416 (ShowRecipe_t2733639435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
