﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorPlay
struct AnimatorPlay_t630493411;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::.ctor()
extern "C"  void AnimatorPlay__ctor_m4046154979 (AnimatorPlay_t630493411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::Reset()
extern "C"  void AnimatorPlay_Reset_m1692587920 (AnimatorPlay_t630493411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnEnter()
extern "C"  void AnimatorPlay_OnEnter_m801260538 (AnimatorPlay_t630493411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnUpdate()
extern "C"  void AnimatorPlay_OnUpdate_m2497799561 (AnimatorPlay_t630493411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::DoAnimatorPlay()
extern "C"  void AnimatorPlay_DoAnimatorPlay_m376569191 (AnimatorPlay_t630493411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
