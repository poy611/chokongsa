﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Grinding
struct ScoreManager_Grinding_t1076868946;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Grinding::.ctor()
extern "C"  void ScoreManager_Grinding__ctor_m2273009241 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding::Awake()
extern "C"  void ScoreManager_Grinding_Awake_m2510614460 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding::Start()
extern "C"  void ScoreManager_Grinding_Start_m1220147033 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding::ClickSound()
extern "C"  void ScoreManager_Grinding_ClickSound_m3785992946 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding::OnEnable()
extern "C"  void ScoreManager_Grinding_OnEnable_m2835488397 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Grinding::confirm()
extern "C"  Il2CppObject * ScoreManager_Grinding_confirm_m2331669279 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
