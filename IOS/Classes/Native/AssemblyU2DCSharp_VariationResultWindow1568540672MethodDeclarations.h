﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VariationResultWindow
struct VariationResultWindow_t1568540672;

#include "codegen/il2cpp-codegen.h"

// System.Void VariationResultWindow::.ctor()
extern "C"  void VariationResultWindow__ctor_m2133979499 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VariationResultWindow::OnEnable()
extern "C"  void VariationResultWindow_OnEnable_m1348917819 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VariationResultWindow::Update()
extern "C"  void VariationResultWindow_Update_m3455717122 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
