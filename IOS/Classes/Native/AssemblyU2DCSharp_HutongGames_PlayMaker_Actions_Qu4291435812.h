﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionAngleAxis
struct  QuaternionAngleAxis_t4291435812  : public QuaternionBaseAction_t1884049229
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionAngleAxis::angle
	FsmFloat_t2134102846 * ___angle_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionAngleAxis::axis
	FsmVector3_t533912882 * ___axis_14;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionAngleAxis::result
	FsmQuaternion_t3871136040 * ___result_15;

public:
	inline static int32_t get_offset_of_angle_13() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t4291435812, ___angle_13)); }
	inline FsmFloat_t2134102846 * get_angle_13() const { return ___angle_13; }
	inline FsmFloat_t2134102846 ** get_address_of_angle_13() { return &___angle_13; }
	inline void set_angle_13(FsmFloat_t2134102846 * value)
	{
		___angle_13 = value;
		Il2CppCodeGenWriteBarrier(&___angle_13, value);
	}

	inline static int32_t get_offset_of_axis_14() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t4291435812, ___axis_14)); }
	inline FsmVector3_t533912882 * get_axis_14() const { return ___axis_14; }
	inline FsmVector3_t533912882 ** get_address_of_axis_14() { return &___axis_14; }
	inline void set_axis_14(FsmVector3_t533912882 * value)
	{
		___axis_14 = value;
		Il2CppCodeGenWriteBarrier(&___axis_14, value);
	}

	inline static int32_t get_offset_of_result_15() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t4291435812, ___result_15)); }
	inline FsmQuaternion_t3871136040 * get_result_15() const { return ___result_15; }
	inline FsmQuaternion_t3871136040 ** get_address_of_result_15() { return &___result_15; }
	inline void set_result_15(FsmQuaternion_t3871136040 * value)
	{
		___result_15 = value;
		Il2CppCodeGenWriteBarrier(&___result_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
