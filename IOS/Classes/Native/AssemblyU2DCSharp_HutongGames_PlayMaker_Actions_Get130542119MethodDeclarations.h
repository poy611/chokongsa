﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetJointBreakInfo
struct GetJointBreakInfo_t130542119;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::.ctor()
extern "C"  void GetJointBreakInfo__ctor_m228273903 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::Reset()
extern "C"  void GetJointBreakInfo_Reset_m2169674140 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::OnEnter()
extern "C"  void GetJointBreakInfo_OnEnter_m4014584582 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
