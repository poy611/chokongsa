﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2341948711_gshared (Nullable_1_t560925241 * __this, bool ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2341948711(__this, ___value0, method) ((  void (*) (Nullable_1_t560925241 *, bool, const MethodInfo*))Nullable_1__ctor_m2341948711_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2192914532_gshared (Nullable_1_t560925241 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2192914532(__this, method) ((  bool (*) (Nullable_1_t560925241 *, const MethodInfo*))Nullable_1_get_HasValue_m2192914532_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C"  bool Nullable_1_get_Value_m3360599055_gshared (Nullable_1_t560925241 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3360599055(__this, method) ((  bool (*) (Nullable_1_t560925241 *, const MethodInfo*))Nullable_1_get_Value_m3360599055_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1858237981_gshared (Nullable_1_t560925241 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1858237981(__this, ___other0, method) ((  bool (*) (Nullable_1_t560925241 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1858237981_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3048239746_gshared (Nullable_1_t560925241 * __this, Nullable_1_t560925241  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3048239746(__this, ___other0, method) ((  bool (*) (Nullable_1_t560925241 *, Nullable_1_t560925241 , const MethodInfo*))Nullable_1_Equals_m3048239746_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2135813621_gshared (Nullable_1_t560925241 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2135813621(__this, method) ((  int32_t (*) (Nullable_1_t560925241 *, const MethodInfo*))Nullable_1_GetHashCode_m2135813621_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern "C"  bool Nullable_1_GetValueOrDefault_m1317481051_gshared (Nullable_1_t560925241 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1317481051(__this, method) ((  bool (*) (Nullable_1_t560925241 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1317481051_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::GetValueOrDefault(T)
extern "C"  bool Nullable_1_GetValueOrDefault_m70479156_gshared (Nullable_1_t560925241 * __this, bool ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m70479156(__this, ___defaultValue0, method) ((  bool (*) (Nullable_1_t560925241 *, bool, const MethodInfo*))Nullable_1_GetValueOrDefault_m70479156_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3292806979_gshared (Nullable_1_t560925241 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3292806979(__this, method) ((  String_t* (*) (Nullable_1_t560925241 *, const MethodInfo*))Nullable_1_ToString_m3292806979_gshared)(__this, method)
