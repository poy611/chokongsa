﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIText
struct GUIText_t3371372606;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m1963534853 (GUIText_t3371372606 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C"  Material_t3870600107 * GUIText_get_material_m2804124146 (GUIText_t3371372606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_color(UnityEngine.Color)
extern "C"  void GUIText_set_color_m3031640689 (GUIText_t3371372606 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void GUIText_INTERNAL_set_color_m3915663645 (GUIText_t3371372606 * __this, Color_t4194546905 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
