﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t2292627584;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::.ctor(System.IntPtr)
extern "C"  void ReadResponse__ctor_m1226479433 (ReadResponse_t2292627584 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::ResponseStatus()
extern "C"  int32_t ReadResponse_ResponseStatus_m3911377987 (ReadResponse_t2292627584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::RequestSucceeded()
extern "C"  bool ReadResponse_RequestSucceeded_m4281893847 (ReadResponse_t2292627584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::Data()
extern "C"  ByteU5BU5D_t4260760469* ReadResponse_Data_m1888371449 (ReadResponse_t2292627584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ReadResponse_CallDispose_m834732935 (ReadResponse_t2292627584 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::FromPointer(System.IntPtr)
extern "C"  ReadResponse_t2292627584 * ReadResponse_FromPointer_m1747665637 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::<Data>m__D3(System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  ReadResponse_U3CDataU3Em__D3_m909704157 (ReadResponse_t2292627584 * __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
