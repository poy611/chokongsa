﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct DefaultComparer_t3608878137;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor()
extern "C"  void DefaultComparer__ctor_m3802309532_gshared (DefaultComparer_t3608878137 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3802309532(__this, method) ((  void (*) (DefaultComparer_t3608878137 *, const MethodInfo*))DefaultComparer__ctor_m3802309532_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4048255375_gshared (DefaultComparer_t3608878137 * __this, TypeConvertKey_t866134174  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4048255375(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3608878137 *, TypeConvertKey_t866134174 , const MethodInfo*))DefaultComparer_GetHashCode_m4048255375_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1643653869_gshared (DefaultComparer_t3608878137 * __this, TypeConvertKey_t866134174  ___x0, TypeConvertKey_t866134174  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1643653869(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3608878137 *, TypeConvertKey_t866134174 , TypeConvertKey_t866134174 , const MethodInfo*))DefaultComparer_Equals_m1643653869_gshared)(__this, ___x0, ___y1, method)
