﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertIntToFloat
struct ConvertIntToFloat_t2467947539;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::.ctor()
extern "C"  void ConvertIntToFloat__ctor_m1344642691 (ConvertIntToFloat_t2467947539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::Reset()
extern "C"  void ConvertIntToFloat_Reset_m3286042928 (ConvertIntToFloat_t2467947539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::OnEnter()
extern "C"  void ConvertIntToFloat_OnEnter_m3103165850 (ConvertIntToFloat_t2467947539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::OnUpdate()
extern "C"  void ConvertIntToFloat_OnUpdate_m842420201 (ConvertIntToFloat_t2467947539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::DoConvertIntToFloat()
extern "C"  void ConvertIntToFloat_DoConvertIntToFloat_m2559402907 (ConvertIntToFloat_t2467947539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
