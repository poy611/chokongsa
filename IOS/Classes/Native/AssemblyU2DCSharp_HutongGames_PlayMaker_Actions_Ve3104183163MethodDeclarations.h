﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2AddXY
struct Vector2AddXY_t3104183163;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2AddXY::.ctor()
extern "C"  void Vector2AddXY__ctor_m65492299 (Vector2AddXY_t3104183163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2AddXY::Reset()
extern "C"  void Vector2AddXY_Reset_m2006892536 (Vector2AddXY_t3104183163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2AddXY::OnEnter()
extern "C"  void Vector2AddXY_OnEnter_m2200285794 (Vector2AddXY_t3104183163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2AddXY::OnUpdate()
extern "C"  void Vector2AddXY_OnUpdate_m2917909537 (Vector2AddXY_t3104183163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2AddXY::DoVector2AddXYZ()
extern "C"  void Vector2AddXY_DoVector2AddXYZ_m3254601733 (Vector2AddXY_t3104183163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
