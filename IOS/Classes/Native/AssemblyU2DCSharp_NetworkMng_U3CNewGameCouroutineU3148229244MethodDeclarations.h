﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/<NewGameCouroutine>c__Iterator12
struct U3CNewGameCouroutineU3Ec__Iterator12_t148229244;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkMng/<NewGameCouroutine>c__Iterator12::.ctor()
extern "C"  void U3CNewGameCouroutineU3Ec__Iterator12__ctor_m1055414895 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<NewGameCouroutine>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CNewGameCouroutineU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m176355267 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<NewGameCouroutine>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewGameCouroutineU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m1101717847 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkMng/<NewGameCouroutine>c__Iterator12::MoveNext()
extern "C"  bool U3CNewGameCouroutineU3Ec__Iterator12_MoveNext_m1787897637 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<NewGameCouroutine>c__Iterator12::Dispose()
extern "C"  void U3CNewGameCouroutineU3Ec__Iterator12_Dispose_m539715308 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<NewGameCouroutine>c__Iterator12::Reset()
extern "C"  void U3CNewGameCouroutineU3Ec__Iterator12_Reset_m2996815132 (U3CNewGameCouroutineU3Ec__Iterator12_t148229244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
