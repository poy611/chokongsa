﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t3572035536;
// UnityEngine.UI.Slider
struct Slider_t79469677;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeEasyMovieTexture
struct  YoutubeEasyMovieTexture_t1195908624  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject YoutubeEasyMovieTexture::Player
	GameObject_t3674682005 * ___Player_2;
	// MediaPlayerCtrl YoutubeEasyMovieTexture::_movie
	MediaPlayerCtrl_t3572035536 * ____movie_3;
	// UnityEngine.GameObject YoutubeEasyMovieTexture::LearnPop
	GameObject_t3674682005 * ___LearnPop_4;
	// UnityEngine.UI.Slider YoutubeEasyMovieTexture::_slider
	Slider_t79469677 * ____slider_5;
	// UnityEngine.UI.Text YoutubeEasyMovieTexture::_text
	Text_t9039225 * ____text_6;
	// System.Int32 YoutubeEasyMovieTexture::totalLength
	int32_t ___totalLength_7;
	// System.Single YoutubeEasyMovieTexture::moveSpeed
	float ___moveSpeed_8;

public:
	inline static int32_t get_offset_of_Player_2() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ___Player_2)); }
	inline GameObject_t3674682005 * get_Player_2() const { return ___Player_2; }
	inline GameObject_t3674682005 ** get_address_of_Player_2() { return &___Player_2; }
	inline void set_Player_2(GameObject_t3674682005 * value)
	{
		___Player_2 = value;
		Il2CppCodeGenWriteBarrier(&___Player_2, value);
	}

	inline static int32_t get_offset_of__movie_3() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ____movie_3)); }
	inline MediaPlayerCtrl_t3572035536 * get__movie_3() const { return ____movie_3; }
	inline MediaPlayerCtrl_t3572035536 ** get_address_of__movie_3() { return &____movie_3; }
	inline void set__movie_3(MediaPlayerCtrl_t3572035536 * value)
	{
		____movie_3 = value;
		Il2CppCodeGenWriteBarrier(&____movie_3, value);
	}

	inline static int32_t get_offset_of_LearnPop_4() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ___LearnPop_4)); }
	inline GameObject_t3674682005 * get_LearnPop_4() const { return ___LearnPop_4; }
	inline GameObject_t3674682005 ** get_address_of_LearnPop_4() { return &___LearnPop_4; }
	inline void set_LearnPop_4(GameObject_t3674682005 * value)
	{
		___LearnPop_4 = value;
		Il2CppCodeGenWriteBarrier(&___LearnPop_4, value);
	}

	inline static int32_t get_offset_of__slider_5() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ____slider_5)); }
	inline Slider_t79469677 * get__slider_5() const { return ____slider_5; }
	inline Slider_t79469677 ** get_address_of__slider_5() { return &____slider_5; }
	inline void set__slider_5(Slider_t79469677 * value)
	{
		____slider_5 = value;
		Il2CppCodeGenWriteBarrier(&____slider_5, value);
	}

	inline static int32_t get_offset_of__text_6() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ____text_6)); }
	inline Text_t9039225 * get__text_6() const { return ____text_6; }
	inline Text_t9039225 ** get_address_of__text_6() { return &____text_6; }
	inline void set__text_6(Text_t9039225 * value)
	{
		____text_6 = value;
		Il2CppCodeGenWriteBarrier(&____text_6, value);
	}

	inline static int32_t get_offset_of_totalLength_7() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ___totalLength_7)); }
	inline int32_t get_totalLength_7() const { return ___totalLength_7; }
	inline int32_t* get_address_of_totalLength_7() { return &___totalLength_7; }
	inline void set_totalLength_7(int32_t value)
	{
		___totalLength_7 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_8() { return static_cast<int32_t>(offsetof(YoutubeEasyMovieTexture_t1195908624, ___moveSpeed_8)); }
	inline float get_moveSpeed_8() const { return ___moveSpeed_8; }
	inline float* get_address_of_moveSpeed_8() { return &___moveSpeed_8; }
	inline void set_moveSpeed_8(float value)
	{
		___moveSpeed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
