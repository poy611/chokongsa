﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationTime
struct SetAnimationTime_t2332135127;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::.ctor()
extern "C"  void SetAnimationTime__ctor_m1149572463 (SetAnimationTime_t2332135127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::Reset()
extern "C"  void SetAnimationTime_Reset_m3090972700 (SetAnimationTime_t2332135127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::OnEnter()
extern "C"  void SetAnimationTime_OnEnter_m324270470 (SetAnimationTime_t2332135127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::OnUpdate()
extern "C"  void SetAnimationTime_OnUpdate_m596009341 (SetAnimationTime_t2332135127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::DoSetAnimationTime(UnityEngine.GameObject)
extern "C"  void SetAnimationTime_DoSetAnimationTime_m3709770887 (SetAnimationTime_t2332135127 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
