﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct GetAnimatorLayerName_t4020332743;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::.ctor()
extern "C"  void GetAnimatorLayerName__ctor_m3972242815 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::Reset()
extern "C"  void GetAnimatorLayerName_Reset_m1618675756 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::OnEnter()
extern "C"  void GetAnimatorLayerName_OnEnter_m2786114966 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::DoGetLayerName()
extern "C"  void GetAnimatorLayerName_DoGetLayerName_m639249302 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
