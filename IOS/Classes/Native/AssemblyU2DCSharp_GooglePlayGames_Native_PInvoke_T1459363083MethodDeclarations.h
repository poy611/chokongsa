﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t1459363083;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::.ctor(System.IntPtr)
extern "C"  void MatchInboxUIResponse__ctor_m1324327012 (MatchInboxUIResponse_t1459363083 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::UiStatus()
extern "C"  int32_t MatchInboxUIResponse_UiStatus_m4059168656 (MatchInboxUIResponse_t1459363083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::Match()
extern "C"  NativeTurnBasedMatch_t302853426 * MatchInboxUIResponse_Match_m1963052201 (MatchInboxUIResponse_t1459363083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MatchInboxUIResponse_CallDispose_m2328545740 (MatchInboxUIResponse_t1459363083 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::FromPointer(System.IntPtr)
extern "C"  MatchInboxUIResponse_t1459363083 * MatchInboxUIResponse_FromPointer_m3118728645 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
