﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoFirstFrameReady
struct VideoFirstFrameReady_t2520860170;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MediaPlayerCtrl/VideoFirstFrameReady::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoFirstFrameReady__ctor_m4202279345 (VideoFirstFrameReady_t2520860170 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoFirstFrameReady::Invoke()
extern "C"  void VideoFirstFrameReady_Invoke_m3520108875 (VideoFirstFrameReady_t2520860170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoFirstFrameReady::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoFirstFrameReady_BeginInvoke_m3655778136 (VideoFirstFrameReady_t2520860170 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoFirstFrameReady::EndInvoke(System.IAsyncResult)
extern "C"  void VideoFirstFrameReady_EndInvoke_m931374145 (VideoFirstFrameReady_t2520860170 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
