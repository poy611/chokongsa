﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<ConstforMinigame>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3678646624(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1571433379 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ConstforMinigame>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1309886528(__this, method) ((  void (*) (InternalEnumerator_1_t1571433379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ConstforMinigame>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m319992940(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1571433379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ConstforMinigame>::Dispose()
#define InternalEnumerator_1_Dispose_m2808967991(__this, method) ((  void (*) (InternalEnumerator_1_t1571433379 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ConstforMinigame>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3719419436(__this, method) ((  bool (*) (InternalEnumerator_1_t1571433379 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ConstforMinigame>::get_Current()
#define InternalEnumerator_1_get_Current_m1117046247(__this, method) ((  ConstforMinigame_t2789090703 * (*) (InternalEnumerator_1_t1571433379 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
