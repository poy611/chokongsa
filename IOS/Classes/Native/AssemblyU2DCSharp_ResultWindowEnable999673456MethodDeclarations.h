﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultWindowEnable
struct ResultWindowEnable_t999673456;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultWindowEnable::.ctor()
extern "C"  void ResultWindowEnable__ctor_m3445190699 (ResultWindowEnable_t999673456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultWindowEnable::OnEnable()
extern "C"  void ResultWindowEnable_OnEnable_m914219899 (ResultWindowEnable_t999673456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResultWindowEnable::WaitTemp()
extern "C"  Il2CppObject * ResultWindowEnable_WaitTemp_m3111104682 (ResultWindowEnable_t999673456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
