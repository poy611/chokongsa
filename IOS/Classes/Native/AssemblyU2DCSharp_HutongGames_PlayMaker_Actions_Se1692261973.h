﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFogColor
struct  SetFogColor_t1692261973  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetFogColor::fogColor
	FsmColor_t2131419205 * ___fogColor_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFogColor::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_fogColor_11() { return static_cast<int32_t>(offsetof(SetFogColor_t1692261973, ___fogColor_11)); }
	inline FsmColor_t2131419205 * get_fogColor_11() const { return ___fogColor_11; }
	inline FsmColor_t2131419205 ** get_address_of_fogColor_11() { return &___fogColor_11; }
	inline void set_fogColor_11(FsmColor_t2131419205 * value)
	{
		___fogColor_11 = value;
		Il2CppCodeGenWriteBarrier(&___fogColor_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetFogColor_t1692261973, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
