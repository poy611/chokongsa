﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65
struct U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::.ctor()
extern "C"  void U3CCreateQuickGameU3Ec__AnonStorey65__ctor_m83850739 (U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::<>m__41()
extern "C"  void U3CCreateQuickGameU3Ec__AnonStorey65_U3CU3Em__41_m1284311865 (U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
