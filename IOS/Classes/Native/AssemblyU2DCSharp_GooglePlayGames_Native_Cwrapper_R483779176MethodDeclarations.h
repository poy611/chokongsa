﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_Length_m3300603846 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_GetElement_m2082844962 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_Variant_m669807416 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t RealTimeRoomConfig_RealTimeRoomConfig_ExclusiveBitMask_m3216536272 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool RealTimeRoomConfig_RealTimeRoomConfig_Valid_m3078683912 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MaximumAutomatchingPlayers_m1498051663 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MinimumAutomatchingPlayers_m293273889 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoomConfig_RealTimeRoomConfig_Dispose_m680968503 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
