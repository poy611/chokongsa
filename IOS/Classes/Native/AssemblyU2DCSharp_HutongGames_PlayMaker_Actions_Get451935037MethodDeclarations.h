﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCosine
struct GetCosine_t451935037;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCosine::.ctor()
extern "C"  void GetCosine__ctor_m648502937 (GetCosine_t451935037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::Reset()
extern "C"  void GetCosine_Reset_m2589903174 (GetCosine_t451935037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnEnter()
extern "C"  void GetCosine_OnEnter_m4127760432 (GetCosine_t451935037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnUpdate()
extern "C"  void GetCosine_OnUpdate_m2540081171 (GetCosine_t451935037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::DoCosine()
extern "C"  void GetCosine_DoCosine_m1137603311 (GetCosine_t451935037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
