﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FFmpeg.AutoGen.size_t
struct size_t_t1323646292;
struct size_t_t1323646292_marshaled_pinvoke;
struct size_t_t1323646292_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct size_t_t1323646292;
struct size_t_t1323646292_marshaled_pinvoke;

extern "C" void size_t_t1323646292_marshal_pinvoke(const size_t_t1323646292& unmarshaled, size_t_t1323646292_marshaled_pinvoke& marshaled);
extern "C" void size_t_t1323646292_marshal_pinvoke_back(const size_t_t1323646292_marshaled_pinvoke& marshaled, size_t_t1323646292& unmarshaled);
extern "C" void size_t_t1323646292_marshal_pinvoke_cleanup(size_t_t1323646292_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct size_t_t1323646292;
struct size_t_t1323646292_marshaled_com;

extern "C" void size_t_t1323646292_marshal_com(const size_t_t1323646292& unmarshaled, size_t_t1323646292_marshaled_com& marshaled);
extern "C" void size_t_t1323646292_marshal_com_back(const size_t_t1323646292_marshaled_com& marshaled, size_t_t1323646292& unmarshaled);
extern "C" void size_t_t1323646292_marshal_com_cleanup(size_t_t1323646292_marshaled_com& marshaled);
