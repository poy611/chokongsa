﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// StartFirst
struct StartFirst_t396912526;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartFirst/<BlackBlink>c__Iterator2E
struct  U3CBlackBlinkU3Ec__Iterator2E_t2642540702  : public Il2CppObject
{
public:
	// System.Single StartFirst/<BlackBlink>c__Iterator2E::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Int32 StartFirst/<BlackBlink>c__Iterator2E::$PC
	int32_t ___U24PC_1;
	// System.Object StartFirst/<BlackBlink>c__Iterator2E::$current
	Il2CppObject * ___U24current_2;
	// StartFirst StartFirst/<BlackBlink>c__Iterator2E::<>f__this
	StartFirst_t396912526 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBlackBlinkU3Ec__Iterator2E_t2642540702, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CBlackBlinkU3Ec__Iterator2E_t2642540702, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CBlackBlinkU3Ec__Iterator2E_t2642540702, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CBlackBlinkU3Ec__Iterator2E_t2642540702, ___U3CU3Ef__this_3)); }
	inline StartFirst_t396912526 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline StartFirst_t396912526 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(StartFirst_t396912526 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
