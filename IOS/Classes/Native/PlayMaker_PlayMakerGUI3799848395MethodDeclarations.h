﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGUI
struct PlayMakerGUI_t3799848395;
// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// UnityEngine.Texture
struct Texture_t2526458961;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"

// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C"  bool PlayMakerGUI_get_EnableStateLabels_m369831644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_EnableStateLabels(System.Boolean)
extern "C"  void PlayMakerGUI_set_EnableStateLabels_m2134116951 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerGUI PlayMakerGUI::get_Instance()
extern "C"  PlayMakerGUI_t3799848395 * PlayMakerGUI_get_Instance_m1734509702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_Enabled()
extern "C"  bool PlayMakerGUI_get_Enabled_m2747798576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C"  GUISkin_t3371348110 * PlayMakerGUI_get_GUISkin_m2871771085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUISkin(UnityEngine.GUISkin)
extern "C"  void PlayMakerGUI_set_GUISkin_m2923764070 (Il2CppObject * __this /* static, unused */, GUISkin_t3371348110 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIColor_m3696729802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIColor_m2497813007 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIBackgroundColor_m579738172 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIBackgroundColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIBackgroundColor_m4250287389 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIContentColor_m4207416069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIContentColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIContentColor_m3079522542 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 PlayMakerGUI::get_GUIMatrix()
extern "C"  Matrix4x4_t1651859333  PlayMakerGUI_get_GUIMatrix_m643020872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C"  void PlayMakerGUI_set_GUIMatrix_m925123147 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C"  Texture_t2526458961 * PlayMakerGUI_get_MouseCursor_m3692364755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_MouseCursor(UnityEngine.Texture)
extern "C"  void PlayMakerGUI_set_MouseCursor_m346590176 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C"  bool PlayMakerGUI_get_LockCursor_m563741236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_LockCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_LockCursor_m2221249063 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C"  bool PlayMakerGUI_get_HideCursor_m2495307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_HideCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_HideCursor_m2729864958 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C"  void PlayMakerGUI_InitLabelStyle_m3613734111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C"  void PlayMakerGUI_DrawStateLabels_m3093789020 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C"  void PlayMakerGUI_DrawStateLabel_m1299770249 (PlayMakerGUI_t3799848395 * __this, PlayMakerFSM_t3799847376 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C"  String_t* PlayMakerGUI_GenerateStateLabel_m3664809373 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::Awake()
extern "C"  void PlayMakerGUI_Awake_m777492309 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnEnable()
extern "C"  void PlayMakerGUI_OnEnable_m1195353172 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnGUI()
extern "C"  void PlayMakerGUI_OnGUI_m35285740 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C"  void PlayMakerGUI_CallOnGUI_m575607312 (PlayMakerGUI_t3799848395 * __this, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnDisable()
extern "C"  void PlayMakerGUI_OnDisable_m3137147225 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DoEditGUI()
extern "C"  void PlayMakerGUI_DoEditGUI_m3828862422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnApplicationQuit()
extern "C"  void PlayMakerGUI_OnApplicationQuit_m1060313200 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::.ctor()
extern "C"  void PlayMakerGUI__ctor_m539887090 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerGUI::<DrawStateLabels>b__1(PlayMakerFSM,PlayMakerFSM)
extern "C"  int32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___x0, PlayMakerFSM_t3799847376 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::.cctor()
extern "C"  void PlayMakerGUI__cctor_m3369501691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
