﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeScorePageToken
struct NativeScorePageToken_t1393223995;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeScorePageToken::.ctor(System.IntPtr)
extern "C"  void NativeScorePageToken__ctor_m1368099803 (NativeScorePageToken_t1393223995 * __this, IntPtr_t ___selfPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeScorePageToken::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeScorePageToken_CallDispose_m442263861 (NativeScorePageToken_t1393223995 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
