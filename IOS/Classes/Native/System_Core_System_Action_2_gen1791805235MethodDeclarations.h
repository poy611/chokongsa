﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3406652893MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1608892953(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1791805235 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m884641853_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::Invoke(T1,T2)
#define Action_2_Invoke_m3245632706(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1791805235 *, int32_t, List_1_t2555968713 *, const MethodInfo*))Action_2_Invoke_m3208528846_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2648428689(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1791805235 *, int32_t, List_1_t2555968713 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3751373293_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1024993449(__this, ___result0, method) ((  void (*) (Action_2_t1791805235 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2024909005_gshared)(__this, ___result0, method)
