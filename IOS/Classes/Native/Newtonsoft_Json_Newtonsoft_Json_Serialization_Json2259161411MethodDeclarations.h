﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0
struct U3CU3Ec__DisplayClass74_0_t2259161411;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t18794611;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_ErrorC18794611.h"

// System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass74_0__ctor_m4225382339 (U3CU3Ec__DisplayClass74_0_t2259161411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0::<CreateSerializationErrorCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern "C"  void U3CU3Ec__DisplayClass74_0_U3CCreateSerializationErrorCallbackU3Eb__0_m733072676 (U3CU3Ec__DisplayClass74_0_t2259161411 * __this, Il2CppObject * ___o0, StreamingContext_t2761351129  ___context1, ErrorContext_t18794611 * ___econtext2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
