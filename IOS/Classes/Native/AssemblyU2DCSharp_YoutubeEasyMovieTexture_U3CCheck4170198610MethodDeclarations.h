﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33
struct U3CCheckPlayU3Ec__Iterator33_t4170198610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::.ctor()
extern "C"  void U3CCheckPlayU3Ec__Iterator33__ctor_m3454047369 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckPlayU3Ec__Iterator33_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2143567603 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckPlayU3Ec__Iterator33_System_Collections_IEnumerator_get_Current_m2436925575 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::MoveNext()
extern "C"  bool U3CCheckPlayU3Ec__Iterator33_MoveNext_m2964566195 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::Dispose()
extern "C"  void U3CCheckPlayU3Ec__Iterator33_Dispose_m3523052166 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::Reset()
extern "C"  void U3CCheckPlayU3Ec__Iterator33_Reset_m1100480310 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
