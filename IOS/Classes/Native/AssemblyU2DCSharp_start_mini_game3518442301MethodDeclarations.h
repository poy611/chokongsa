﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// start_mini_game
struct start_mini_game_t3518442301;

#include "codegen/il2cpp-codegen.h"

// System.Void start_mini_game::.ctor()
extern "C"  void start_mini_game__ctor_m2312171726 (start_mini_game_t3518442301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void start_mini_game::Start()
extern "C"  void start_mini_game_Start_m1259309518 (start_mini_game_t3518442301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void start_mini_game::Update()
extern "C"  void start_mini_game_Update_m389741567 (start_mini_game_t3518442301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void start_mini_game::onStart()
extern "C"  void start_mini_game_onStart_m2876431439 (start_mini_game_t3518442301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
