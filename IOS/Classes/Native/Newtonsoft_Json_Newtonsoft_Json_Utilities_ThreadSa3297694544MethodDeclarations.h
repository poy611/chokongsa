﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m3045322293(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t3297694544 *, Func_2_t607687872 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::Get(TKey)
#define ThreadSafeStore_2_Get_m647699419(__this, ___key0, method) ((  Func_2_t2363589633 * (*) (ThreadSafeStore_2_t3297694544 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m986503063(__this, ___key0, method) ((  Func_2_t2363589633 * (*) (ThreadSafeStore_2_t3297694544 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
