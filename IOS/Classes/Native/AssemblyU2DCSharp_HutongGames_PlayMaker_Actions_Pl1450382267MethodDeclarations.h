﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayRandomAnimation
struct PlayRandomAnimation_t1450382267;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::.ctor()
extern "C"  void PlayRandomAnimation__ctor_m964129243 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::Reset()
extern "C"  void PlayRandomAnimation_Reset_m2905529480 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnEnter()
extern "C"  void PlayRandomAnimation_OnEnter_m2501962482 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::DoPlayRandomAnimation()
extern "C"  void PlayRandomAnimation_DoPlayRandomAnimation_m3075573787 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::DoPlayAnimation(System.String)
extern "C"  void PlayRandomAnimation_DoPlayAnimation_m326462500 (PlayRandomAnimation_t1450382267 * __this, String_t* ___animName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnUpdate()
extern "C"  void PlayRandomAnimation_OnUpdate_m3679952273 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnExit()
extern "C"  void PlayRandomAnimation_OnExit_m1336554086 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::StopAnimation()
extern "C"  void PlayRandomAnimation_StopAnimation_m2835977243 (PlayRandomAnimation_t1450382267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
