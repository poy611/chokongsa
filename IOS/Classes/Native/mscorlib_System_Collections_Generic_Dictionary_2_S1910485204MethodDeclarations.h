﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>
struct ShimEnumerator_t1910485204;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t2194707177;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m840560212_gshared (ShimEnumerator_t1910485204 * __this, Dictionary_2_t2194707177 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m840560212(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1910485204 *, Dictionary_2_t2194707177 *, const MethodInfo*))ShimEnumerator__ctor_m840560212_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3265263437_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3265263437(__this, method) ((  bool (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_MoveNext_m3265263437_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2764729895_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2764729895(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_get_Entry_m2764729895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3296894914_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3296894914(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_get_Key_m3296894914_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3973551572_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3973551572(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_get_Value_m3973551572_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1781825372_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1781825372(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_get_Current_m1781825372_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::Reset()
extern "C"  void ShimEnumerator_Reset_m4024715942_gshared (ShimEnumerator_t1910485204 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4024715942(__this, method) ((  void (*) (ShimEnumerator_t1910485204 *, const MethodInfo*))ShimEnumerator_Reset_m4024715942_gshared)(__this, method)
