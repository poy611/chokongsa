﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections
struct  NetworkGetMaximumConnections_t1349448613  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::result
	FsmInt_t1596138449 * ___result_11;

public:
	inline static int32_t get_offset_of_result_11() { return static_cast<int32_t>(offsetof(NetworkGetMaximumConnections_t1349448613, ___result_11)); }
	inline FsmInt_t1596138449 * get_result_11() const { return ___result_11; }
	inline FsmInt_t1596138449 ** get_address_of_result_11() { return &___result_11; }
	inline void set_result_11(FsmInt_t1596138449 * value)
	{
		___result_11 = value;
		Il2CppCodeGenWriteBarrier(&___result_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
