﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTan
struct  GetTan_t2986517587  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTan::angle
	FsmFloat_t2134102846 * ___angle_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetTan::DegToRad
	FsmBool_t1075959796 * ___DegToRad_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTan::result
	FsmFloat_t2134102846 * ___result_13;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTan::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_angle_11() { return static_cast<int32_t>(offsetof(GetTan_t2986517587, ___angle_11)); }
	inline FsmFloat_t2134102846 * get_angle_11() const { return ___angle_11; }
	inline FsmFloat_t2134102846 ** get_address_of_angle_11() { return &___angle_11; }
	inline void set_angle_11(FsmFloat_t2134102846 * value)
	{
		___angle_11 = value;
		Il2CppCodeGenWriteBarrier(&___angle_11, value);
	}

	inline static int32_t get_offset_of_DegToRad_12() { return static_cast<int32_t>(offsetof(GetTan_t2986517587, ___DegToRad_12)); }
	inline FsmBool_t1075959796 * get_DegToRad_12() const { return ___DegToRad_12; }
	inline FsmBool_t1075959796 ** get_address_of_DegToRad_12() { return &___DegToRad_12; }
	inline void set_DegToRad_12(FsmBool_t1075959796 * value)
	{
		___DegToRad_12 = value;
		Il2CppCodeGenWriteBarrier(&___DegToRad_12, value);
	}

	inline static int32_t get_offset_of_result_13() { return static_cast<int32_t>(offsetof(GetTan_t2986517587, ___result_13)); }
	inline FsmFloat_t2134102846 * get_result_13() const { return ___result_13; }
	inline FsmFloat_t2134102846 ** get_address_of_result_13() { return &___result_13; }
	inline void set_result_13(FsmFloat_t2134102846 * value)
	{
		___result_13 = value;
		Il2CppCodeGenWriteBarrier(&___result_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(GetTan_t2986517587, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
