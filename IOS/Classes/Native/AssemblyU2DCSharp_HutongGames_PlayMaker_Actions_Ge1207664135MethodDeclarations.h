﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmColor
struct GetFsmColor_t1207664135;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::.ctor()
extern "C"  void GetFsmColor__ctor_m490423567 (GetFsmColor_t1207664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::Reset()
extern "C"  void GetFsmColor_Reset_m2431823804 (GetFsmColor_t1207664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnEnter()
extern "C"  void GetFsmColor_OnEnter_m2537341222 (GetFsmColor_t1207664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnUpdate()
extern "C"  void GetFsmColor_OnUpdate_m481725917 (GetFsmColor_t1207664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::DoGetFsmColor()
extern "C"  void GetFsmColor_DoGetFsmColor_m1716459675 (GetFsmColor_t1207664135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
