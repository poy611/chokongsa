﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuList
struct MenuList_t3755595453;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuList::.ctor()
extern "C"  void MenuList__ctor_m2878352510 (MenuList_t3755595453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MenuList::GetMenuName(System.Int32)
extern "C"  String_t* MenuList_GetMenuName_m2677322154 (MenuList_t3755595453 * __this, int32_t ___menuNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MenuList::GetMenuInPopup(System.Int32)
extern "C"  String_t* MenuList_GetMenuInPopup_m1960107628 (MenuList_t3755595453 * __this, int32_t ___menuNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
