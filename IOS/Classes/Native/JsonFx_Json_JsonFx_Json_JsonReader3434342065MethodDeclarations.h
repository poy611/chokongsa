﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonReader
struct JsonReader_t3434342065;
// System.String
struct String_t;
// JsonFx.Json.JsonReaderSettings
struct JsonReaderSettings_t24058356;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "JsonFx_Json_JsonFx_Json_JsonReaderSettings24058356.h"
#include "mscorlib_System_Type2863145774.h"
#include "JsonFx_Json_JsonFx_Json_JsonToken37183731.h"

// System.Void JsonFx.Json.JsonReader::.ctor(System.String)
extern "C"  void JsonReader__ctor_m3511759990 (JsonReader_t3434342065 * __this, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonReader::.ctor(System.String,JsonFx.Json.JsonReaderSettings)
extern "C"  void JsonReader__ctor_m1191774896 (JsonReader_t3434342065 * __this, String_t* ___input0, JsonReaderSettings_t24058356 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::Deserialize(System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m691121176 (JsonReader_t3434342065 * __this, int32_t ___start0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::Read(System.Type,System.Boolean)
extern "C"  Il2CppObject * JsonReader_Read_m3191825511 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, bool ___typeIsHint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::ReadObject(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadObject_m1839350999 (JsonReader_t3434342065 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable JsonFx.Json.JsonReader::ReadArray(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadArray_m1842253292 (JsonReader_t3434342065 * __this, Type_t * ___arrayType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonFx.Json.JsonReader::ReadUnquotedKey()
extern "C"  String_t* JsonReader_ReadUnquotedKey_m3071438223 (JsonReader_t3434342065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::ReadString(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadString_m3863899461 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::ReadNumber(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadNumber_m1947135245 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m1765830364 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___start1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize()
extern "C"  int32_t JsonReader_Tokenize_m3827593607 (JsonReader_t3434342065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize(System.Boolean)
extern "C"  int32_t JsonReader_Tokenize_m338063038 (JsonReader_t3434342065 * __this, bool ___allowUnquotedString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonReader::MatchLiteral(System.String)
extern "C"  bool JsonReader_MatchLiteral_m706834448 (JsonReader_t3434342065 * __this, String_t* ___literal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
