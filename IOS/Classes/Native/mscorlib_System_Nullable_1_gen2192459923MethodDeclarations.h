﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2192459923.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateParseHandling2108333400.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.DateParseHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m438218851_gshared (Nullable_1_t2192459923 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m438218851(__this, ___value0, method) ((  void (*) (Nullable_1_t2192459923 *, int32_t, const MethodInfo*))Nullable_1__ctor_m438218851_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateParseHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m423407420_gshared (Nullable_1_t2192459923 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m423407420(__this, method) ((  bool (*) (Nullable_1_t2192459923 *, const MethodInfo*))Nullable_1_get_HasValue_m423407420_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateParseHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1730704517_gshared (Nullable_1_t2192459923 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1730704517(__this, method) ((  int32_t (*) (Nullable_1_t2192459923 *, const MethodInfo*))Nullable_1_get_Value_m1730704517_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateParseHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3757150125_gshared (Nullable_1_t2192459923 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3757150125(__this, ___other0, method) ((  bool (*) (Nullable_1_t2192459923 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3757150125_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateParseHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3450631922_gshared (Nullable_1_t2192459923 * __this, Nullable_1_t2192459923  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3450631922(__this, ___other0, method) ((  bool (*) (Nullable_1_t2192459923 *, Nullable_1_t2192459923 , const MethodInfo*))Nullable_1_Equals_m3450631922_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.DateParseHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2921986833_gshared (Nullable_1_t2192459923 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2921986833(__this, method) ((  int32_t (*) (Nullable_1_t2192459923 *, const MethodInfo*))Nullable_1_GetHashCode_m2921986833_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateParseHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3106448207_gshared (Nullable_1_t2192459923 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3106448207(__this, method) ((  int32_t (*) (Nullable_1_t2192459923 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3106448207_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateParseHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m62179710_gshared (Nullable_1_t2192459923 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m62179710(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2192459923 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m62179710_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.DateParseHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m256784341_gshared (Nullable_1_t2192459923 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m256784341(__this, method) ((  String_t* (*) (Nullable_1_t2192459923 *, const MethodInfo*))Nullable_1_ToString_m256784341_gshared)(__this, method)
