﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3659144454;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.Serialization.JsonContainerContract
struct JsonContainerContract_t3227540113;
// System.String
struct String_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// Newtonsoft.Json.Linq.JTokenReader
struct JTokenReader_t3546959010;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.Collections.IList
struct IList_t1751339649;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t505348133;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext>
struct List_1_t164164032;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Dictionary_2_t696655424;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3227540113.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenReader3546959010.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonA145179369.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonD989352188.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonI624170136.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonO505348133.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalReader__ctor_m948700458 (JsonSerializerInternalReader_t3659144454 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_GetContractSafe_m1273160499 (JsonSerializerInternalReader_t3659144454 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Deserialize(Newtonsoft.Json.JsonReader,System.Type,System.Boolean)
extern "C"  Il2CppObject * JsonSerializerInternalReader_Deserialize_m4218677033 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, bool ___checkAdditionalContent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetInternalSerializer()
extern "C"  JsonSerializerProxy_t3893567258 * JsonSerializerInternalReader_GetInternalSerializer_m3470711210 (JsonSerializerInternalReader_t3659144454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJToken(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_CreateJToken_m864232728 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJObject(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_CreateJObject_m3929757817 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueInternal(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateValueInternal_m1494398188 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerMember5, Il2CppObject * ___existingValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CoerceEmptyStringToNull(System.Type,Newtonsoft.Json.Serialization.JsonContract,System.String)
extern "C"  bool JsonSerializerInternalReader_CoerceEmptyStringToNull_m1610978350 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType0, JsonContract_t1328848902 * ___contract1, String_t* ___s2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetExpectedDescription(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  String_t* JsonSerializerInternalReader_GetExpectedDescription_m4032814287 (JsonSerializerInternalReader_t3659144454 * __this, JsonContract_t1328848902 * ___contract0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetConverter(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonConverter_t2159686854 * JsonSerializerInternalReader_GetConverter_m543681313 (JsonSerializerInternalReader_t3659144454 * __this, JsonContract_t1328848902 * ___contract0, JsonConverter_t2159686854 * ___memberConverter1, JsonContainerContract_t3227540113 * ___containerContract2, JsonProperty_t902655177 * ___containerProperty3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObject(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateObject_m2983839557 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerMember5, Il2CppObject * ___existingValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataPropertiesToken(Newtonsoft.Json.Linq.JTokenReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern "C"  bool JsonSerializerInternalReader_ReadMetadataPropertiesToken_m822882719 (JsonSerializerInternalReader_t3659144454 * __this, JTokenReader_t3546959010 * ___reader0, Type_t ** ___objectType1, JsonContract_t1328848902 ** ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerMember5, Il2CppObject * ___existingValue6, Il2CppObject ** ___newValue7, String_t** ___id8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataProperties(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern "C"  bool JsonSerializerInternalReader_ReadMetadataProperties_m4259176651 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t ** ___objectType1, JsonContract_t1328848902 ** ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerMember5, Il2CppObject * ___existingValue6, Il2CppObject ** ___newValue7, String_t** ___id8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolveTypeName(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  void JsonSerializerInternalReader_ResolveTypeName_m1455871692 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t ** ___objectType1, JsonContract_t1328848902 ** ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerMember5, String_t* ___qualifiedTypeName6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureArrayContract(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JsonArrayContract_t145179369 * JsonSerializerInternalReader_EnsureArrayContract_m2714306693 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateList(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateList_m3670290740 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, Il2CppObject * ___existingValue4, String_t* ___id5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasNoDefinedType(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalReader_HasNoDefinedType_m459585552 (JsonSerializerInternalReader_t3659144454 * __this, JsonContract_t1328848902 * ___contract0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureType(Newtonsoft.Json.JsonReader,System.Object,System.Globalization.CultureInfo,Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern "C"  Il2CppObject * JsonSerializerInternalReader_EnsureType_m3849890492 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___value1, CultureInfo_t1065375142 * ___culture2, JsonContract_t1328848902 * ___contract3, Type_t * ___targetType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern "C"  bool JsonSerializerInternalReader_SetPropertyValue_m3480906970 (JsonSerializerInternalReader_t3659144454 * __this, JsonProperty_t902655177 * ___property0, JsonConverter_t2159686854 * ___propertyConverter1, JsonContainerContract_t3227540113 * ___containerContract2, JsonProperty_t902655177 * ___containerProperty3, JsonReader_t816925123 * ___reader4, Il2CppObject * ___target5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CalculatePropertyDetails(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter&,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object,System.Boolean&,System.Object&,Newtonsoft.Json.Serialization.JsonContract&,System.Boolean&)
extern "C"  bool JsonSerializerInternalReader_CalculatePropertyDetails_m1307870504 (JsonSerializerInternalReader_t3659144454 * __this, JsonProperty_t902655177 * ___property0, JsonConverter_t2159686854 ** ___propertyConverter1, JsonContainerContract_t3227540113 * ___containerContract2, JsonProperty_t902655177 * ___containerProperty3, JsonReader_t816925123 * ___reader4, Il2CppObject * ___target5, bool* ___useExistingValue6, Il2CppObject ** ___currentValue7, JsonContract_t1328848902 ** ___propertyContract8, bool* ___gottenCurrentValue9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::AddReference(Newtonsoft.Json.JsonReader,System.String,System.Object)
extern "C"  void JsonSerializerInternalReader_AddReference_m2338248447 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, String_t* ___id1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalReader_HasFlag_m2265828307 (JsonSerializerInternalReader_t3659144454 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldSetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalReader_ShouldSetPropertyValue_m1219481536 (JsonSerializerInternalReader_t3659144454 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewList(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,System.Boolean&)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateNewList_m359174208 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonArrayContract_t145179369 * ___contract1, bool* ___createdFromNonDefaultCreator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewDictionary(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.Boolean&)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateNewDictionary_m454890797 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonDictionaryContract_t989352188 * ___contract1, bool* ___createdFromNonDefaultCreator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserializing(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern "C"  void JsonSerializerInternalReader_OnDeserializing_m3728268691 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserialized(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern "C"  void JsonSerializerInternalReader_OnDeserialized_m332398968 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateDictionary(System.Collections.IDictionary,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateDictionary_m3098187004 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___dictionary0, JsonReader_t816925123 * ___reader1, JsonDictionaryContract_t989352188 * ___contract2, JsonProperty_t902655177 * ___containerProperty3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateMultidimensionalArray(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateMultidimensionalArray_m2818336698 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___list0, JsonReader_t816925123 * ___reader1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___containerProperty3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ThrowUnexpectedEndException(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.String)
extern "C"  void JsonSerializerInternalReader_ThrowUnexpectedEndException_m2978189441 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___currentObject2, String_t* ___message3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateList(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateList_m1279953865 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___list0, JsonReader_t816925123 * ___reader1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___containerProperty3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializable(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateISerializable_m1086567575 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonISerializableContract_t624170136 * ___contract1, JsonProperty_t902655177 * ___member2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializableItem(Newtonsoft.Json.Linq.JToken,System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateISerializableItem_m2509667853 (JsonSerializerInternalReader_t3659144454 * __this, JToken_t3412245951 * ___token0, Type_t * ___type1, JsonISerializableContract_t624170136 * ___contract2, JsonProperty_t902655177 * ___member3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObjectUsingCreatorWithParameters(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_m1682256240 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonObjectContract_t505348133 * ___contract1, JsonProperty_t902655177 * ___containerProperty2, ObjectConstructor_1_t2948332186 * ___creator3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::DeserializeConvertable(Newtonsoft.Json.JsonConverter,Newtonsoft.Json.JsonReader,System.Type,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_DeserializeConvertable_m2891136326 (JsonSerializerInternalReader_t3659144454 * __this, JsonConverter_t2159686854 * ___converter0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, Il2CppObject * ___existingValue3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolvePropertyAndCreatorValues(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Type)
extern "C"  List_1_t164164032 * JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_m3506870239 (JsonSerializerInternalReader_t3659144454 * __this, JsonObjectContract_t505348133 * ___contract0, JsonProperty_t902655177 * ___containerProperty1, JsonReader_t816925123 * ___reader2, Type_t * ___objectType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadForType(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern "C"  bool JsonSerializerInternalReader_ReadForType_m2347582334 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, bool ___hasConverter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,System.String,System.Boolean&)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateNewObject_m1393220128 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonObjectContract_t505348133 * ___objectContract1, JsonProperty_t902655177 * ___containerMember2, JsonProperty_t902655177 * ___containerProperty3, String_t* ___id4, bool* ___createdFromNonDefaultCreator5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateObject(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateObject_m3983230547 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___newObject0, JsonReader_t816925123 * ___reader1, JsonObjectContract_t505348133 * ___contract2, JsonProperty_t902655177 * ___member3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldDeserialize(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalReader_ShouldDeserialize_m1680907083 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CheckPropertyName(Newtonsoft.Json.JsonReader,System.String)
extern "C"  bool JsonSerializerInternalReader_CheckPropertyName_m2210507953 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, String_t* ___memberName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetExtensionData(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.String,System.Object)
extern "C"  void JsonSerializerInternalReader_SetExtensionData_m2470256576 (JsonSerializerInternalReader_t3659144454 * __this, JsonObjectContract_t505348133 * ___contract0, JsonProperty_t902655177 * ___member1, JsonReader_t816925123 * ___reader2, String_t* ___memberName3, Il2CppObject * ___o4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadExtensionDataValue(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ReadExtensionDataValue_m1951731394 (JsonSerializerInternalReader_t3659144454 * __this, JsonObjectContract_t505348133 * ___contract0, JsonProperty_t902655177 * ___member1, JsonReader_t816925123 * ___reader2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EndProcessProperty(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,System.Int32,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Boolean)
extern "C"  void JsonSerializerInternalReader_EndProcessProperty_m92185328 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___newObject0, JsonReader_t816925123 * ___reader1, JsonObjectContract_t505348133 * ___contract2, int32_t ___initialDepth3, JsonProperty_t902655177 * ___property4, int32_t ___presence5, bool ___setDefaultValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyPresence(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>)
extern "C"  void JsonSerializerInternalReader_SetPropertyPresence_m2573427894 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonProperty_t902655177 * ___property1, Dictionary_2_t696655424 * ___requiredProperties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HandleError(Newtonsoft.Json.JsonReader,System.Boolean,System.Int32)
extern "C"  void JsonSerializerInternalReader_HandleError_m1679699313 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, bool ___readPastError1, int32_t ___initialDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
