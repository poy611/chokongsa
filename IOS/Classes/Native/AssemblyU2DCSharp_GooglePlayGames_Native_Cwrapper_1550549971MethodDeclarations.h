﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadata_SnapshotMetadata_Dispose_m84177869 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_CoverImageURL(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_CoverImageURL_m2203124652 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_Description_m3412118779 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_IsOpen(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadata_SnapshotMetadata_IsOpen_m1972405166 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_FileName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_FileName_m4109404960 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadata_SnapshotMetadata_Valid_m811751198 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t SnapshotMetadata_SnapshotMetadata_PlayedTime_m3644402463 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t SnapshotMetadata_SnapshotMetadata_LastModifiedTime_m3105604107 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
