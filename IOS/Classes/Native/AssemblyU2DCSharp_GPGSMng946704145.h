﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Singleton_1_gen1199519538.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPGSMng
struct  GPGSMng_t946704145  : public Singleton_1_t1199519538
{
public:
	// System.Boolean GPGSMng::<bLogin>k__BackingField
	bool ___U3CbLoginU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CbLoginU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GPGSMng_t946704145, ___U3CbLoginU3Ek__BackingField_3)); }
	inline bool get_U3CbLoginU3Ek__BackingField_3() const { return ___U3CbLoginU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CbLoginU3Ek__BackingField_3() { return &___U3CbLoginU3Ek__BackingField_3; }
	inline void set_U3CbLoginU3Ek__BackingField_3(bool value)
	{
		___U3CbLoginU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
