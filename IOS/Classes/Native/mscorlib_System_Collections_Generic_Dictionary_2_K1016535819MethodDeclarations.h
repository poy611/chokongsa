﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1016535819.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4206982322_gshared (Enumerator_t1016535819 * __this, Dictionary_2_t401599765 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4206982322(__this, ___host0, method) ((  void (*) (Enumerator_t1016535819 *, Dictionary_2_t401599765 *, const MethodInfo*))Enumerator__ctor_m4206982322_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2118578489_gshared (Enumerator_t1016535819 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2118578489(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016535819 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2118578489_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m496521411_gshared (Enumerator_t1016535819 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m496521411(__this, method) ((  void (*) (Enumerator_t1016535819 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m496521411_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2134420436_gshared (Enumerator_t1016535819 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2134420436(__this, method) ((  void (*) (Enumerator_t1016535819 *, const MethodInfo*))Enumerator_Dispose_m2134420436_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3696678515_gshared (Enumerator_t1016535819 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3696678515(__this, method) ((  bool (*) (Enumerator_t1016535819 *, const MethodInfo*))Enumerator_MoveNext_m3696678515_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Current()
extern "C"  HandleRef_t1780819301  Enumerator_get_Current_m2356603077_gshared (Enumerator_t1016535819 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2356603077(__this, method) ((  HandleRef_t1780819301  (*) (Enumerator_t1016535819 *, const MethodInfo*))Enumerator_get_Current_m2356603077_gshared)(__this, method)
