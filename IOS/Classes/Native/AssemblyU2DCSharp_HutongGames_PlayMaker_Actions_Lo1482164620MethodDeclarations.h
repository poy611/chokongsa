﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LoadLevel
struct LoadLevel_t1482164620;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LoadLevel::.ctor()
extern "C"  void LoadLevel__ctor_m3843619114 (LoadLevel_t1482164620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::Reset()
extern "C"  void LoadLevel_Reset_m1490052055 (LoadLevel_t1482164620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::OnEnter()
extern "C"  void LoadLevel_OnEnter_m3732789889 (LoadLevel_t1482164620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevel::OnUpdate()
extern "C"  void LoadLevel_OnUpdate_m3180896226 (LoadLevel_t1482164620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
