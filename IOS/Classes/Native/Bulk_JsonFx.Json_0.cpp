﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// JsonFx.Json.JsonDeserializationException
struct JsonDeserializationException_t1696613230;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JsonFx.Json.JsonNameAttribute
struct JsonNameAttribute_t1794268491;
// JsonFx.Json.JsonReader
struct JsonReader_t3434342065;
// JsonFx.Json.JsonReaderSettings
struct JsonReaderSettings_t24058356;
// System.Type
struct Type_t;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// JsonFx.Json.JsonSerializationException
struct JsonSerializationException_t3458665517;
// JsonFx.Json.JsonSpecifiedPropertyAttribute
struct JsonSpecifiedPropertyAttribute_t3902267749;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// JsonFx.Json.JsonTypeCoercionException
struct JsonTypeCoercionException_t4269543025;
// System.Exception
struct Exception_t3991598821;
// JsonFx.Json.JsonWriter
struct JsonWriter_t3589747297;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// JsonFx.Json.JsonWriterSettings
struct JsonWriterSettings_t323204516;
// System.Enum
struct Enum_t2862688501;
// System.Uri
struct Uri_t1116831938;
// System.Version
struct Version_t763695022;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.Enum[]
struct EnumU5BU5D_t3205174168;
// JsonFx.Json.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t3884844371;
// JsonFx.Json.TypeCoercionUtility
struct TypeCoercionUtility_t700629014;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>
struct Dictionary_2_t626389457;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t520966972;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "JsonFx_Json_U3CModuleU3E86524790.h"
#include "JsonFx_Json_U3CModuleU3E86524790MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonDeserializationExcepti1696613230.h"
#include "JsonFx_Json_JsonFx_Json_JsonDeserializationExcepti1696613230MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Int321153838500.h"
#include "JsonFx_Json_JsonFx_Json_JsonSerializationException3458665517MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonIgnoreAttribute2459039844.h"
#include "JsonFx_Json_JsonFx_Json_JsonIgnoreAttribute2459039844MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "JsonFx_Json_JsonFx_Json_JsonNameAttribute1794268491.h"
#include "JsonFx_Json_JsonFx_Json_JsonNameAttribute1794268491MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "JsonFx_Json_JsonFx_Json_JsonReader3434342065.h"
#include "JsonFx_Json_JsonFx_Json_JsonReader3434342065MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonReaderSettings24058356MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonReaderSettings24058356.h"
#include "JsonFx_Json_JsonFx_Json_JsonToken37183731.h"
#include "mscorlib_System_Double3868226565.h"
#include "JsonFx_Json_JsonFx_Json_TypeCoercionUtility700629014MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge696267445MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge520966972.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2862622538.h"
#include "JsonFx_Json_JsonFx_Json_TypeCoercionUtility700629014.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge696267445.h"
#include "mscorlib_System_Collections_ArrayList3948406897MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Char2862622538MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232.h"
#include "mscorlib_System_Globalization_NumberStyles2609490573.h"
#include "mscorlib_System_Decimal1954350631MethodDeclarations.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_StringComparer4230573202MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202.h"
#include "JsonFx_Json_JsonFx_Json_JsonSerializationException3458665517.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonSpecifiedPropertyAttri3902267749.h"
#include "JsonFx_Json_JsonFx_Json_JsonSpecifiedPropertyAttri3902267749MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonToken37183731MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonTypeCoercionException4269543025.h"
#include "JsonFx_Json_JsonFx_Json_JsonTypeCoercionException4269543025MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriter3589747297.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriter3589747297MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriterSettings323204516MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriterSettings323204516.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4216882900MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_IO_StringWriter4216882900.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_IO_TextWriter2304124208MethodDeclarations.h"
#include "mscorlib_System_TypeCode1814089915.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Guid2862754429.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Version763695022.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3884844371MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3884844371.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_Guid2862754429MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode856910923MethodDeclarations.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614MethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyName2915647011MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_Assembly1418687608.h"
#include "mscorlib_System_Reflection_Assembly1418687608MethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyName2915647011.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725MethodDeclarations.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230874053MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230874053.h"
#include "mscorlib_System_Environment4152990825MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge626389457.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge626389457MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti3880899288.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge520966972MethodDeclarations.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586MethodDeclarations.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "mscorlib_System_Version763695022MethodDeclarations.h"
#include "System_System_ComponentModel_TypeDescriptor1537159061MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter1753450284MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586.h"
#include "mscorlib_System_Globalization_DateTimeStyles1282965087.h"
#include "System_System_UriKind238866934.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JsonFx.Json.JsonDeserializationException::.ctor(System.String,System.Int32)
extern "C"  void JsonDeserializationException__ctor_m393045188 (JsonDeserializationException_t1696613230 * __this, String_t* ___message0, int32_t ___index1, const MethodInfo* method)
{
	{
		__this->set_index_12((-1));
		String_t* L_0 = ___message0;
		JsonSerializationException__ctor_m2046370034(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___index1;
		__this->set_index_12(L_1);
		return;
	}
}
// System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern const Il2CppType* JsonIgnoreAttribute_t2459039844_0_0_0_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonIgnoreAttribute_IsJsonIgnore_m4133572969_MetadataUsageId;
extern "C"  bool JsonIgnoreAttribute_IsJsonIgnore_m4133572969 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonIgnoreAttribute_IsJsonIgnore_m4133572969_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (Il2CppObject *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Type_t * L_5 = V_0;
		Type_t * L_6 = V_0;
		Il2CppObject * L_7 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_8 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfo_t * L_9 = Type_GetField_m1054209156(L_5, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_002d;
	}

IL_0026:
	{
		Il2CppObject * L_10 = ___value0;
		V_1 = ((Il2CppObject *)IsInst(L_10, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var));
	}

IL_002d:
	{
		Il2CppObject * L_11 = V_1;
		if (L_11)
		{
			goto IL_0036;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0036:
	{
		Il2CppObject * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonIgnoreAttribute_t2459039844_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_15 = InterfaceFuncInvoker2< bool, Type_t *, bool >::Invoke(2 /* System.Boolean System.Reflection.ICustomAttributeProvider::IsDefined(System.Type,System.Boolean) */, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var, L_13, L_14, (bool)1);
		return L_15;
	}
}
// System.Boolean JsonFx.Json.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern const Il2CppType* XmlIgnoreAttribute_t2315070405_0_0_0_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonIgnoreAttribute_IsXmlIgnore_m816865530_MetadataUsageId;
extern "C"  bool JsonIgnoreAttribute_IsXmlIgnore_m816865530 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonIgnoreAttribute_IsXmlIgnore_m816865530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (Il2CppObject *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Type_t * L_5 = V_0;
		Type_t * L_6 = V_0;
		Il2CppObject * L_7 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_8 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		FieldInfo_t * L_9 = Type_GetField_m1054209156(L_5, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_002d;
	}

IL_0026:
	{
		Il2CppObject * L_10 = ___value0;
		V_1 = ((Il2CppObject *)IsInst(L_10, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var));
	}

IL_002d:
	{
		Il2CppObject * L_11 = V_1;
		if (L_11)
		{
			goto IL_0036;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0036:
	{
		Il2CppObject * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(XmlIgnoreAttribute_t2315070405_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		bool L_15 = InterfaceFuncInvoker2< bool, Type_t *, bool >::Invoke(2 /* System.Boolean System.Reflection.ICustomAttributeProvider::IsDefined(System.Type,System.Boolean) */, ICustomAttributeProvider_t1425685797_il2cpp_TypeInfo_var, L_13, L_14, (bool)1);
		return L_15;
	}
}
// System.String JsonFx.Json.JsonNameAttribute::get_Name()
extern "C"  String_t* JsonNameAttribute_get_Name_m3918014895 (JsonNameAttribute_t1794268491 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_jsonName_0();
		return L_0;
	}
}
// System.String JsonFx.Json.JsonNameAttribute::GetJsonName(System.Object)
extern const Il2CppType* JsonNameAttribute_t1794268491_0_0_0_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MemberInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonNameAttribute_t1794268491_il2cpp_TypeInfo_var;
extern const uint32_t JsonNameAttribute_GetJsonName_m3130059236_MetadataUsageId;
extern "C"  String_t* JsonNameAttribute_GetJsonName_m3130059236 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonNameAttribute_GetJsonName_m3130059236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	MemberInfo_t * V_1 = NULL;
	String_t* V_2 = NULL;
	JsonNameAttribute_t1794268491 * V_3 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0005:
	{
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (MemberInfo_t *)NULL;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsEnum_m3878730619(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_5 = V_0;
		Il2CppObject * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		String_t* L_7 = Enum_GetName_m3333121265(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0028;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0028:
	{
		Type_t * L_10 = V_0;
		String_t* L_11 = V_2;
		NullCheck(L_10);
		FieldInfo_t * L_12 = Type_GetField_m1054209156(L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		goto IL_0039;
	}

IL_0032:
	{
		Il2CppObject * L_13 = ___value0;
		V_1 = ((MemberInfo_t *)IsInstClass(L_13, MemberInfo_t_il2cpp_TypeInfo_var));
	}

IL_0039:
	{
		MemberInfo_t * L_14 = V_1;
		if (L_14)
		{
			goto IL_0042;
		}
	}
	{
		ArgumentException_t928607144 * L_15 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0042:
	{
		MemberInfo_t * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t1794268491_0_0_0_var), /*hidden argument*/NULL);
		bool L_18 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0056;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0056:
	{
		MemberInfo_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t1794268491_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_21 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_3 = ((JsonNameAttribute_t1794268491 *)CastclassClass(L_21, JsonNameAttribute_t1794268491_il2cpp_TypeInfo_var));
		JsonNameAttribute_t1794268491 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = JsonNameAttribute_get_Name_m3918014895(L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void JsonFx.Json.JsonReader::.ctor(System.String)
extern Il2CppClass* JsonReaderSettings_t24058356_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m3511759990_MetadataUsageId;
extern "C"  void JsonReader__ctor_m3511759990 (JsonReader_t3434342065 * __this, String_t* ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m3511759990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___input0;
		JsonReaderSettings_t24058356 * L_1 = (JsonReaderSettings_t24058356 *)il2cpp_codegen_object_new(JsonReaderSettings_t24058356_il2cpp_TypeInfo_var);
		JsonReaderSettings__ctor_m302022697(L_1, /*hidden argument*/NULL);
		JsonReader__ctor_m1191774896(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonFx.Json.JsonReader::.ctor(System.String,JsonFx.Json.JsonReaderSettings)
extern Il2CppClass* JsonReaderSettings_t24058356_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m1191774896_MetadataUsageId;
extern "C"  void JsonReader__ctor_m1191774896 (JsonReader_t3434342065 * __this, String_t* ___input0, JsonReaderSettings_t24058356 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m1191774896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonReaderSettings_t24058356 * L_0 = (JsonReaderSettings_t24058356 *)il2cpp_codegen_object_new(JsonReaderSettings_t24058356_il2cpp_TypeInfo_var);
		JsonReaderSettings__ctor_m302022697(L_0, /*hidden argument*/NULL);
		__this->set_Settings_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		JsonReaderSettings_t24058356 * L_1 = ___settings1;
		__this->set_Settings_0(L_1);
		String_t* L_2 = ___input0;
		__this->set_Source_1(L_2);
		String_t* L_3 = __this->get_Source_1();
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		__this->set_SourceLength_2(L_4);
		return;
	}
}
// System.Object JsonFx.Json.JsonReader::Deserialize(System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m691121176 (JsonReader_t3434342065 * __this, int32_t ___start0, Type_t * ___type1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___start0;
		__this->set_index_3(L_0);
		Type_t * L_1 = ___type1;
		Il2CppObject * L_2 = JsonReader_Read_m3191825511(__this, L_1, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object JsonFx.Json.JsonReader::Read(System.Type,System.Boolean)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral78043;
extern Il2CppCodeGenString* _stringLiteral237817416;
extern Il2CppCodeGenString* _stringLiteral506745205;
extern Il2CppCodeGenString* _stringLiteral3256836432;
extern const uint32_t JsonReader_Read_m3191825511_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_Read_m3191825511 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, bool ___typeIsHint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m3191825511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	JsonReader_t3434342065 * G_B6_0 = NULL;
	JsonReader_t3434342065 * G_B5_0 = NULL;
	Type_t * G_B7_0 = NULL;
	JsonReader_t3434342065 * G_B7_1 = NULL;
	JsonReader_t3434342065 * G_B10_0 = NULL;
	JsonReader_t3434342065 * G_B9_0 = NULL;
	Type_t * G_B11_0 = NULL;
	JsonReader_t3434342065 * G_B11_1 = NULL;
	JsonReader_t3434342065 * G_B14_0 = NULL;
	JsonReader_t3434342065 * G_B13_0 = NULL;
	Type_t * G_B15_0 = NULL;
	JsonReader_t3434342065 * G_B15_1 = NULL;
	JsonReader_t3434342065 * G_B18_0 = NULL;
	JsonReader_t3434342065 * G_B17_0 = NULL;
	Type_t * G_B19_0 = NULL;
	JsonReader_t3434342065 * G_B19_1 = NULL;
	{
		Type_t * L_0 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0010;
		}
	}
	{
		___expectedType0 = (Type_t *)NULL;
	}

IL_0010:
	{
		int32_t L_2 = JsonReader_Tokenize_m3827593607(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_0170;
		}
		if (L_4 == 1)
		{
			goto IL_0157;
		}
		if (L_4 == 2)
		{
			goto IL_00cc;
		}
		if (L_4 == 3)
		{
			goto IL_0090;
		}
		if (L_4 == 4)
		{
			goto IL_00ae;
		}
		if (L_4 == 5)
		{
			goto IL_00e5;
		}
		if (L_4 == 6)
		{
			goto IL_010b;
		}
		if (L_4 == 7)
		{
			goto IL_0131;
		}
		if (L_4 == 8)
		{
			goto IL_0082;
		}
		if (L_4 == 9)
		{
			goto IL_0074;
		}
		if (L_4 == 10)
		{
			goto IL_0066;
		}
		if (L_4 == 11)
		{
			goto IL_0170;
		}
		if (L_4 == 12)
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0170;
	}

IL_0058:
	{
		bool L_5 = ___typeIsHint1;
		G_B5_0 = __this;
		if (L_5)
		{
			G_B6_0 = __this;
			goto IL_005f;
		}
	}
	{
		Type_t * L_6 = ___expectedType0;
		G_B7_0 = L_6;
		G_B7_1 = G_B5_0;
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = ((Type_t *)(NULL));
		G_B7_1 = G_B6_0;
	}

IL_0060:
	{
		NullCheck(G_B7_1);
		Il2CppObject * L_7 = JsonReader_ReadObject_m1839350999(G_B7_1, G_B7_0, /*hidden argument*/NULL);
		return L_7;
	}

IL_0066:
	{
		bool L_8 = ___typeIsHint1;
		G_B9_0 = __this;
		if (L_8)
		{
			G_B10_0 = __this;
			goto IL_006d;
		}
	}
	{
		Type_t * L_9 = ___expectedType0;
		G_B11_0 = L_9;
		G_B11_1 = G_B9_0;
		goto IL_006e;
	}

IL_006d:
	{
		G_B11_0 = ((Type_t *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_006e:
	{
		NullCheck(G_B11_1);
		Il2CppObject * L_10 = JsonReader_ReadArray_m1842253292(G_B11_1, G_B11_0, /*hidden argument*/NULL);
		return L_10;
	}

IL_0074:
	{
		bool L_11 = ___typeIsHint1;
		G_B13_0 = __this;
		if (L_11)
		{
			G_B14_0 = __this;
			goto IL_007b;
		}
	}
	{
		Type_t * L_12 = ___expectedType0;
		G_B15_0 = L_12;
		G_B15_1 = G_B13_0;
		goto IL_007c;
	}

IL_007b:
	{
		G_B15_0 = ((Type_t *)(NULL));
		G_B15_1 = G_B14_0;
	}

IL_007c:
	{
		NullCheck(G_B15_1);
		Il2CppObject * L_13 = JsonReader_ReadString_m3863899461(G_B15_1, G_B15_0, /*hidden argument*/NULL);
		return L_13;
	}

IL_0082:
	{
		bool L_14 = ___typeIsHint1;
		G_B17_0 = __this;
		if (L_14)
		{
			G_B18_0 = __this;
			goto IL_0089;
		}
	}
	{
		Type_t * L_15 = ___expectedType0;
		G_B19_0 = L_15;
		G_B19_1 = G_B17_0;
		goto IL_008a;
	}

IL_0089:
	{
		G_B19_0 = ((Type_t *)(NULL));
		G_B19_1 = G_B18_0;
	}

IL_008a:
	{
		NullCheck(G_B19_1);
		Il2CppObject * L_16 = JsonReader_ReadNumber_m1947135245(G_B19_1, G_B19_0, /*hidden argument*/NULL);
		return L_16;
	}

IL_0090:
	{
		int32_t L_17 = __this->get_index_3();
		NullCheck(_stringLiteral97196323);
		int32_t L_18 = String_get_Length_m2979997331(_stringLiteral97196323, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_17+(int32_t)L_18)));
		bool L_19 = ((bool)0);
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		return L_20;
	}

IL_00ae:
	{
		int32_t L_21 = __this->get_index_3();
		NullCheck(_stringLiteral3569038);
		int32_t L_22 = String_get_Length_m2979997331(_stringLiteral3569038, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_21+(int32_t)L_22)));
		bool L_23 = ((bool)1);
		Il2CppObject * L_24 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_23);
		return L_24;
	}

IL_00cc:
	{
		int32_t L_25 = __this->get_index_3();
		NullCheck(_stringLiteral3392903);
		int32_t L_26 = String_get_Length_m2979997331(_stringLiteral3392903, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_25+(int32_t)L_26)));
		return NULL;
	}

IL_00e5:
	{
		int32_t L_27 = __this->get_index_3();
		NullCheck(_stringLiteral78043);
		int32_t L_28 = String_get_Length_m2979997331(_stringLiteral78043, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_27+(int32_t)L_28)));
		double L_29 = (std::numeric_limits<double>::quiet_NaN());
		Il2CppObject * L_30 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_29);
		return L_30;
	}

IL_010b:
	{
		int32_t L_31 = __this->get_index_3();
		NullCheck(_stringLiteral237817416);
		int32_t L_32 = String_get_Length_m2979997331(_stringLiteral237817416, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_31+(int32_t)L_32)));
		double L_33 = (std::numeric_limits<double>::infinity());
		Il2CppObject * L_34 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_33);
		return L_34;
	}

IL_0131:
	{
		int32_t L_35 = __this->get_index_3();
		NullCheck(_stringLiteral506745205);
		int32_t L_36 = String_get_Length_m2979997331(_stringLiteral506745205, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_35+(int32_t)L_36)));
		double L_37 = (-std::numeric_limits<double>::infinity());
		Il2CppObject * L_38 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_37);
		return L_38;
	}

IL_0157:
	{
		int32_t L_39 = __this->get_index_3();
		NullCheck(_stringLiteral3256836432);
		int32_t L_40 = String_get_Length_m2979997331(_stringLiteral3256836432, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_39+(int32_t)L_40)));
		return NULL;
	}

IL_0170:
	{
		return NULL;
	}
}
// System.Object JsonFx.Json.JsonReader::ReadObject(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t696267445_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m987880680_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral264921567;
extern Il2CppCodeGenString* _stringLiteral2235684418;
extern Il2CppCodeGenString* _stringLiteral622120811;
extern Il2CppCodeGenString* _stringLiteral3637437851;
extern Il2CppCodeGenString* _stringLiteral1581823241;
extern Il2CppCodeGenString* _stringLiteral525226562;
extern Il2CppCodeGenString* _stringLiteral237437267;
extern const uint32_t JsonReader_ReadObject_m1839350999_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadObject_m1839350999 (JsonReader_t3434342065 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadObject_m1839350999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Dictionary_2_t520966972 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Type_t * V_3 = NULL;
	TypeU5BU5D_t3339007067* V_4 = NULL;
	int32_t V_5 = 0;
	Type_t * V_6 = NULL;
	MemberInfo_t * V_7 = NULL;
	String_t* V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	String_t* G_B20_0 = NULL;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)123))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_4 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_4, _stringLiteral264921567, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0026:
	{
		V_0 = (Type_t *)NULL;
		V_1 = (Dictionary_2_t520966972 *)NULL;
		Type_t * L_5 = ___objectType0;
		if (!L_5)
		{
			goto IL_00a0;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_6 = __this->get_Settings_0();
		NullCheck(L_6);
		TypeCoercionUtility_t700629014 * L_7 = L_6->get_Coercion_0();
		Type_t * L_8 = ___objectType0;
		NullCheck(L_7);
		Il2CppObject * L_9 = TypeCoercionUtility_InstantiateObject_m3710207535(L_7, L_8, (&V_1), /*hidden argument*/NULL);
		V_2 = L_9;
		Dictionary_2_t520966972 * L_10 = V_1;
		if (L_10)
		{
			goto IL_00a6;
		}
	}
	{
		Type_t * L_11 = ___objectType0;
		NullCheck(L_11);
		Type_t * L_12 = Type_GetInterface_m1133348080(L_11, _stringLiteral2235684418, /*hidden argument*/NULL);
		V_3 = L_12;
		Type_t * L_13 = V_3;
		if (!L_13)
		{
			goto IL_00a6;
		}
	}
	{
		Type_t * L_14 = V_3;
		NullCheck(L_14);
		TypeU5BU5D_t3339007067* L_15 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(90 /* System.Type[] System.Type::GetGenericArguments() */, L_14);
		V_4 = L_15;
		TypeU5BU5D_t3339007067* L_16 = V_4;
		NullCheck(L_16);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_00a6;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_17 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Type_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_19) == ((Il2CppObject*)(Type_t *)L_20)))
		{
			goto IL_0089;
		}
	}
	{
		Type_t * L_21 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral622120811, L_21, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_24 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_24, L_22, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_0089:
	{
		TypeU5BU5D_t3339007067* L_25 = V_4;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
		int32_t L_26 = 1;
		Type_t * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_27) == ((Il2CppObject*)(Type_t *)L_28)))
		{
			goto IL_00a6;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_29 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 1);
		int32_t L_30 = 1;
		Type_t * L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_0 = L_31;
		goto IL_00a6;
	}

IL_00a0:
	{
		Dictionary_2_t696267445 * L_32 = (Dictionary_2_t696267445 *)il2cpp_codegen_object_new(Dictionary_2_t696267445_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m987880680(L_32, /*hidden argument*/Dictionary_2__ctor_m987880680_MethodInfo_var);
		V_2 = L_32;
	}

IL_00a6:
	{
		int32_t L_33 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_33+(int32_t)1)));
		int32_t L_34 = __this->get_index_3();
		int32_t L_35 = __this->get_SourceLength_2();
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_00d3;
		}
	}
	{
		int32_t L_36 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_37 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_37, _stringLiteral3637437851, L_36, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_37);
	}

IL_00d3:
	{
		JsonReaderSettings_t24058356 * L_38 = __this->get_Settings_0();
		NullCheck(L_38);
		bool L_39 = JsonReaderSettings_get_AllowUnquotedObjectKeys_m3720636459(L_38, /*hidden argument*/NULL);
		int32_t L_40 = JsonReader_Tokenize_m338063038(__this, L_39, /*hidden argument*/NULL);
		V_5 = L_40;
		int32_t L_41 = V_5;
		if ((((int32_t)L_41) == ((int32_t)((int32_t)13))))
		{
			goto IL_0234;
		}
	}
	{
		int32_t L_42 = V_5;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)9))))
		{
			goto IL_010c;
		}
	}
	{
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) == ((int32_t)((int32_t)16))))
		{
			goto IL_010c;
		}
	}
	{
		int32_t L_44 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_45 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_45, _stringLiteral1581823241, L_44, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
	}

IL_010c:
	{
		int32_t L_46 = V_5;
		if ((((int32_t)L_46) == ((int32_t)((int32_t)9))))
		{
			goto IL_011a;
		}
	}
	{
		String_t* L_47 = JsonReader_ReadUnquotedKey_m3071438223(__this, /*hidden argument*/NULL);
		G_B20_0 = L_47;
		goto IL_0126;
	}

IL_011a:
	{
		Il2CppObject * L_48 = JsonReader_ReadString_m3863899461(__this, (Type_t *)NULL, /*hidden argument*/NULL);
		G_B20_0 = ((String_t*)CastclassSealed(L_48, String_t_il2cpp_TypeInfo_var));
	}

IL_0126:
	{
		V_8 = G_B20_0;
		Type_t * L_49 = V_0;
		if (L_49)
		{
			goto IL_013c;
		}
	}
	{
		Dictionary_2_t520966972 * L_50 = V_1;
		if (!L_50)
		{
			goto IL_013c;
		}
	}
	{
		Dictionary_2_t520966972 * L_51 = V_1;
		String_t* L_52 = V_8;
		Type_t * L_53 = TypeCoercionUtility_GetMemberInfo_m2964064126(NULL /*static, unused*/, L_51, L_52, (&V_7), /*hidden argument*/NULL);
		V_6 = L_53;
		goto IL_0142;
	}

IL_013c:
	{
		Type_t * L_54 = V_0;
		V_6 = L_54;
		V_7 = (MemberInfo_t *)NULL;
	}

IL_0142:
	{
		int32_t L_55 = JsonReader_Tokenize_m3827593607(__this, /*hidden argument*/NULL);
		V_5 = L_55;
		int32_t L_56 = V_5;
		if ((((int32_t)L_56) == ((int32_t)((int32_t)14))))
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_57 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_58 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_58, _stringLiteral525226562, L_57, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_58);
	}

IL_0161:
	{
		int32_t L_59 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_59+(int32_t)1)));
		int32_t L_60 = __this->get_index_3();
		int32_t L_61 = __this->get_SourceLength_2();
		if ((((int32_t)L_60) < ((int32_t)L_61)))
		{
			goto IL_018e;
		}
	}
	{
		int32_t L_62 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_63 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_63, _stringLiteral3637437851, L_62, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_63);
	}

IL_018e:
	{
		Type_t * L_64 = V_6;
		Il2CppObject * L_65 = JsonReader_Read_m3191825511(__this, L_64, (bool)0, /*hidden argument*/NULL);
		V_9 = L_65;
		Il2CppObject * L_66 = V_2;
		if (!((Il2CppObject *)IsInst(L_66, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_01e8;
		}
	}
	{
		Type_t * L_67 = ___objectType0;
		if (L_67)
		{
			goto IL_01d7;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_68 = __this->get_Settings_0();
		String_t* L_69 = V_8;
		NullCheck(L_68);
		bool L_70 = JsonReaderSettings_IsTypeHintName_m2842466433(L_68, L_69, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_01d7;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_71 = __this->get_Settings_0();
		NullCheck(L_71);
		TypeCoercionUtility_t700629014 * L_72 = L_71->get_Coercion_0();
		Il2CppObject * L_73 = V_2;
		Il2CppObject * L_74 = V_9;
		NullCheck(L_72);
		Il2CppObject * L_75 = TypeCoercionUtility_ProcessTypeHint_m4089338635(L_72, ((Il2CppObject *)Castclass(L_73, IDictionary_t537317817_il2cpp_TypeInfo_var)), ((String_t*)IsInstSealed(L_74, String_t_il2cpp_TypeInfo_var)), (&___objectType0), (&V_1), /*hidden argument*/NULL);
		V_2 = L_75;
		goto IL_0223;
	}

IL_01d7:
	{
		Il2CppObject * L_76 = V_2;
		String_t* L_77 = V_8;
		Il2CppObject * L_78 = V_9;
		NullCheck(((Il2CppObject *)Castclass(L_76, IDictionary_t537317817_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_76, IDictionary_t537317817_il2cpp_TypeInfo_var)), L_77, L_78);
		goto IL_0223;
	}

IL_01e8:
	{
		Type_t * L_79 = ___objectType0;
		NullCheck(L_79);
		Type_t * L_80 = Type_GetInterface_m1133348080(L_79, _stringLiteral2235684418, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_020c;
		}
	}
	{
		Type_t * L_81 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_82 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral237437267, L_81, /*hidden argument*/NULL);
		int32_t L_83 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_84 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_84, L_82, L_83, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_84);
	}

IL_020c:
	{
		JsonReaderSettings_t24058356 * L_85 = __this->get_Settings_0();
		NullCheck(L_85);
		TypeCoercionUtility_t700629014 * L_86 = L_85->get_Coercion_0();
		Il2CppObject * L_87 = V_2;
		Type_t * L_88 = V_6;
		MemberInfo_t * L_89 = V_7;
		Il2CppObject * L_90 = V_9;
		NullCheck(L_86);
		TypeCoercionUtility_SetMemberValue_m3887813087(L_86, L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
	}

IL_0223:
	{
		int32_t L_91 = JsonReader_Tokenize_m3827593607(__this, /*hidden argument*/NULL);
		V_5 = L_91;
		int32_t L_92 = V_5;
		if ((((int32_t)L_92) == ((int32_t)((int32_t)15))))
		{
			goto IL_00a6;
		}
	}

IL_0234:
	{
		int32_t L_93 = V_5;
		if ((((int32_t)L_93) == ((int32_t)((int32_t)13))))
		{
			goto IL_024b;
		}
	}
	{
		int32_t L_94 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_95 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_95, _stringLiteral3637437851, L_94, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_95);
	}

IL_024b:
	{
		int32_t L_96 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_96+(int32_t)1)));
		Il2CppObject * L_97 = V_2;
		return L_97;
	}
}
// System.Collections.IEnumerable JsonFx.Json.JsonReader::ReadArray(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3363527141;
extern Il2CppCodeGenString* _stringLiteral3610865321;
extern const uint32_t JsonReader_ReadArray_m1842253292_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadArray_m1842253292 (JsonReader_t3434342065 * __this, Type_t * ___arrayType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadArray_m1842253292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Type_t * V_2 = NULL;
	TypeU5BU5D_t3339007067* V_3 = NULL;
	ArrayList_t3948406897 * V_4 = NULL;
	int32_t V_5 = 0;
	Il2CppObject * V_6 = NULL;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)91))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_4 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_4, _stringLiteral3363527141, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0026:
	{
		Type_t * L_5 = ___arrayType0;
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		V_2 = (Type_t *)NULL;
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Type_t * L_8 = ___arrayType0;
		NullCheck(L_8);
		bool L_9 = Type_get_HasElementType_m4257202252(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_10 = ___arrayType0;
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_10);
		V_2 = L_11;
		goto IL_0062;
	}

IL_0049:
	{
		Type_t * L_12 = ___arrayType0;
		NullCheck(L_12);
		bool L_13 = VirtFuncInvoker0< bool >::Invoke(94 /* System.Boolean System.Type::get_IsGenericType() */, L_12);
		if (!L_13)
		{
			goto IL_0062;
		}
	}
	{
		Type_t * L_14 = ___arrayType0;
		NullCheck(L_14);
		TypeU5BU5D_t3339007067* L_15 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(90 /* System.Type[] System.Type::GetGenericArguments() */, L_14);
		V_3 = L_15;
		TypeU5BU5D_t3339007067* L_16 = V_3;
		NullCheck(L_16);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0062;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_17 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		Type_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_2 = L_19;
	}

IL_0062:
	{
		ArrayList_t3948406897 * L_20 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_20, /*hidden argument*/NULL);
		V_4 = L_20;
	}

IL_0069:
	{
		int32_t L_21 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_21+(int32_t)1)));
		int32_t L_22 = __this->get_index_3();
		int32_t L_23 = __this->get_SourceLength_2();
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_24 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_25 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_25, _stringLiteral3610865321, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_0096:
	{
		int32_t L_26 = JsonReader_Tokenize_m3827593607(__this, /*hidden argument*/NULL);
		V_5 = L_26;
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)11))))
		{
			goto IL_011c;
		}
	}
	{
		Type_t * L_28 = V_2;
		bool L_29 = V_1;
		Il2CppObject * L_30 = JsonReader_Read_m3191825511(__this, L_28, L_29, /*hidden argument*/NULL);
		V_6 = L_30;
		ArrayList_t3948406897 * L_31 = V_4;
		Il2CppObject * L_32 = V_6;
		NullCheck(L_31);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_31, L_32);
		Il2CppObject * L_33 = V_6;
		if (L_33)
		{
			goto IL_00cd;
		}
	}
	{
		Type_t * L_34 = V_2;
		if (!L_34)
		{
			goto IL_00c9;
		}
	}
	{
		Type_t * L_35 = V_2;
		NullCheck(L_35);
		bool L_36 = Type_get_IsValueType_m1914757235(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (Type_t *)NULL;
	}

IL_00c9:
	{
		V_0 = (bool)1;
		goto IL_010b;
	}

IL_00cd:
	{
		Type_t * L_37 = V_2;
		if (!L_37)
		{
			goto IL_00fe;
		}
	}
	{
		Type_t * L_38 = V_2;
		Il2CppObject * L_39 = V_6;
		NullCheck(L_39);
		Type_t * L_40 = Object_GetType_m2022236990(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_41 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_38, L_40);
		if (L_41)
		{
			goto IL_00fe;
		}
	}
	{
		Il2CppObject * L_42 = V_6;
		NullCheck(L_42);
		Type_t * L_43 = Object_GetType_m2022236990(L_42, /*hidden argument*/NULL);
		Type_t * L_44 = V_2;
		NullCheck(L_43);
		bool L_45 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_43, L_44);
		if (!L_45)
		{
			goto IL_00f8;
		}
	}
	{
		Il2CppObject * L_46 = V_6;
		NullCheck(L_46);
		Type_t * L_47 = Object_GetType_m2022236990(L_46, /*hidden argument*/NULL);
		V_2 = L_47;
		goto IL_010b;
	}

IL_00f8:
	{
		V_2 = (Type_t *)NULL;
		V_0 = (bool)1;
		goto IL_010b;
	}

IL_00fe:
	{
		bool L_48 = V_0;
		if (L_48)
		{
			goto IL_010b;
		}
	}
	{
		Il2CppObject * L_49 = V_6;
		NullCheck(L_49);
		Type_t * L_50 = Object_GetType_m2022236990(L_49, /*hidden argument*/NULL);
		V_2 = L_50;
		V_0 = (bool)1;
	}

IL_010b:
	{
		int32_t L_51 = JsonReader_Tokenize_m3827593607(__this, /*hidden argument*/NULL);
		V_5 = L_51;
		int32_t L_52 = V_5;
		if ((((int32_t)L_52) == ((int32_t)((int32_t)15))))
		{
			goto IL_0069;
		}
	}

IL_011c:
	{
		int32_t L_53 = V_5;
		if ((((int32_t)L_53) == ((int32_t)((int32_t)11))))
		{
			goto IL_0133;
		}
	}
	{
		int32_t L_54 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_55 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_55, _stringLiteral3610865321, L_54, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
	}

IL_0133:
	{
		int32_t L_56 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_56+(int32_t)1)));
		Type_t * L_57 = V_2;
		if (!L_57)
		{
			goto IL_015a;
		}
	}
	{
		Type_t * L_58 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_59 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_58) == ((Il2CppObject*)(Type_t *)L_59)))
		{
			goto IL_015a;
		}
	}
	{
		ArrayList_t3948406897 * L_60 = V_4;
		Type_t * L_61 = V_2;
		NullCheck(L_60);
		Il2CppArray * L_62 = VirtFuncInvoker1< Il2CppArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_60, L_61);
		return L_62;
	}

IL_015a:
	{
		ArrayList_t3948406897 * L_63 = V_4;
		NullCheck(L_63);
		ObjectU5BU5D_t1108656482* L_64 = VirtFuncInvoker0< ObjectU5BU5D_t1108656482* >::Invoke(47 /* System.Object[] System.Collections.ArrayList::ToArray() */, L_63);
		return (Il2CppObject *)L_64;
	}
}
// System.String JsonFx.Json.JsonReader::ReadUnquotedKey()
extern "C"  String_t* JsonReader_ReadUnquotedKey_m3071438223 (JsonReader_t3434342065 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_3();
		V_0 = L_0;
	}

IL_0007:
	{
		int32_t L_1 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = JsonReader_Tokenize_m338063038(__this, (bool)1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)16))))
		{
			goto IL_0007;
		}
	}
	{
		String_t* L_3 = __this->get_Source_1();
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_index_3();
		int32_t L_6 = V_0;
		NullCheck(L_3);
		String_t* L_7 = String_Substring_m675079568(L_3, L_4, ((int32_t)((int32_t)L_5-(int32_t)L_6)), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Object JsonFx.Json.JsonReader::ReadString(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral42811213;
extern Il2CppCodeGenString* _stringLiteral3415327497;
extern const uint32_t JsonReader_ReadString_m3863899461_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadString_m3863899461 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadString_m3863899461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	StringBuilder_t243639308 * V_2 = NULL;
	int32_t V_3 = 0;
	Il2CppChar V_4 = 0x0;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m3015341861(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)34))))
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_3 = __this->get_Source_1();
		int32_t L_4 = __this->get_index_3();
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)39))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_6 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_7 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_7, _stringLiteral42811213, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003b:
	{
		String_t* L_8 = __this->get_Source_1();
		int32_t L_9 = __this->get_index_3();
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_11+(int32_t)1)));
		int32_t L_12 = __this->get_index_3();
		int32_t L_13 = __this->get_SourceLength_2();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_14 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_15 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_15, _stringLiteral3415327497, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_007a:
	{
		int32_t L_16 = __this->get_index_3();
		V_1 = L_16;
		StringBuilder_t243639308 * L_17 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_17, /*hidden argument*/NULL);
		V_2 = L_17;
		goto IL_026d;
	}

IL_008c:
	{
		String_t* L_18 = __this->get_Source_1();
		int32_t L_19 = __this->get_index_3();
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0240;
		}
	}
	{
		StringBuilder_t243639308 * L_21 = V_2;
		String_t* L_22 = __this->get_Source_1();
		int32_t L_23 = V_1;
		int32_t L_24 = __this->get_index_3();
		int32_t L_25 = V_1;
		NullCheck(L_21);
		StringBuilder_Append_m2996071419(L_21, L_22, L_23, ((int32_t)((int32_t)L_24-(int32_t)L_25)), /*hidden argument*/NULL);
		int32_t L_26 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_26+(int32_t)1)));
		int32_t L_27 = __this->get_index_3();
		int32_t L_28 = __this->get_SourceLength_2();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_29 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_30 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_30, _stringLiteral3415327497, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00e7:
	{
		String_t* L_31 = __this->get_Source_1();
		int32_t L_32 = __this->get_index_3();
		NullCheck(L_31);
		Il2CppChar L_33 = String_get_Chars_m3015341861(L_31, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		Il2CppChar L_34 = V_4;
		if ((((int32_t)L_34) > ((int32_t)((int32_t)98))))
		{
			goto IL_0114;
		}
	}
	{
		Il2CppChar L_35 = V_4;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)48))))
		{
			goto IL_020a;
		}
	}
	{
		Il2CppChar L_36 = V_4;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)98))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_01f2;
	}

IL_0114:
	{
		Il2CppChar L_37 = V_4;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)102))))
		{
			goto IL_014c;
		}
	}
	{
		Il2CppChar L_38 = V_4;
		if ((((int32_t)L_38) == ((int32_t)((int32_t)110))))
		{
			goto IL_015a;
		}
	}
	{
		Il2CppChar L_39 = V_4;
		if (((int32_t)((int32_t)L_39-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_0168;
		}
		if (((int32_t)((int32_t)L_39-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_01f2;
		}
		if (((int32_t)((int32_t)L_39-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_0176;
		}
		if (((int32_t)((int32_t)L_39-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_0184;
		}
	}
	{
		goto IL_01f2;
	}

IL_013f:
	{
		StringBuilder_t243639308 * L_40 = V_2;
		NullCheck(L_40);
		StringBuilder_Append_m2143093878(L_40, 8, /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_014c:
	{
		StringBuilder_t243639308 * L_41 = V_2;
		NullCheck(L_41);
		StringBuilder_Append_m2143093878(L_41, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_015a:
	{
		StringBuilder_t243639308 * L_42 = V_2;
		NullCheck(L_42);
		StringBuilder_Append_m2143093878(L_42, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_0168:
	{
		StringBuilder_t243639308 * L_43 = V_2;
		NullCheck(L_43);
		StringBuilder_Append_m2143093878(L_43, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_0176:
	{
		StringBuilder_t243639308 * L_44 = V_2;
		NullCheck(L_44);
		StringBuilder_Append_m2143093878(L_44, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_0184:
	{
		int32_t L_45 = __this->get_index_3();
		int32_t L_46 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_45+(int32_t)4))) >= ((int32_t)L_46)))
		{
			goto IL_01d8;
		}
	}
	{
		String_t* L_47 = __this->get_Source_1();
		int32_t L_48 = __this->get_index_3();
		NullCheck(L_47);
		String_t* L_49 = String_Substring_m675079568(L_47, ((int32_t)((int32_t)L_48+(int32_t)1)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_50 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_51 = Int32_TryParse_m2457543725(NULL /*static, unused*/, L_49, ((int32_t)512), L_50, (&V_3), /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01d8;
		}
	}
	{
		StringBuilder_t243639308 * L_52 = V_2;
		int32_t L_53 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		String_t* L_54 = Char_ConvertFromUtf32_m567781788(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m3898090075(L_52, L_54, /*hidden argument*/NULL);
		int32_t L_55 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_55+(int32_t)4)));
		goto IL_020a;
	}

IL_01d8:
	{
		StringBuilder_t243639308 * L_56 = V_2;
		String_t* L_57 = __this->get_Source_1();
		int32_t L_58 = __this->get_index_3();
		NullCheck(L_57);
		Il2CppChar L_59 = String_get_Chars_m3015341861(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		StringBuilder_Append_m2143093878(L_56, L_59, /*hidden argument*/NULL);
		goto IL_020a;
	}

IL_01f2:
	{
		StringBuilder_t243639308 * L_60 = V_2;
		String_t* L_61 = __this->get_Source_1();
		int32_t L_62 = __this->get_index_3();
		NullCheck(L_61);
		Il2CppChar L_63 = String_get_Chars_m3015341861(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		StringBuilder_Append_m2143093878(L_60, L_63, /*hidden argument*/NULL);
	}

IL_020a:
	{
		int32_t L_64 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_64+(int32_t)1)));
		int32_t L_65 = __this->get_index_3();
		int32_t L_66 = __this->get_SourceLength_2();
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0237;
		}
	}
	{
		int32_t L_67 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_68 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_68, _stringLiteral3415327497, L_67, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_68);
	}

IL_0237:
	{
		int32_t L_69 = __this->get_index_3();
		V_1 = L_69;
		goto IL_026d;
	}

IL_0240:
	{
		int32_t L_70 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_70+(int32_t)1)));
		int32_t L_71 = __this->get_index_3();
		int32_t L_72 = __this->get_SourceLength_2();
		if ((((int32_t)L_71) < ((int32_t)L_72)))
		{
			goto IL_026d;
		}
	}
	{
		int32_t L_73 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_74 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_74, _stringLiteral3415327497, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_026d:
	{
		String_t* L_75 = __this->get_Source_1();
		int32_t L_76 = __this->get_index_3();
		NullCheck(L_75);
		Il2CppChar L_77 = String_get_Chars_m3015341861(L_75, L_76, /*hidden argument*/NULL);
		Il2CppChar L_78 = V_0;
		if ((!(((uint32_t)L_77) == ((uint32_t)L_78))))
		{
			goto IL_008c;
		}
	}
	{
		StringBuilder_t243639308 * L_79 = V_2;
		String_t* L_80 = __this->get_Source_1();
		int32_t L_81 = V_1;
		int32_t L_82 = __this->get_index_3();
		int32_t L_83 = V_1;
		NullCheck(L_79);
		StringBuilder_Append_m2996071419(L_79, L_80, L_81, ((int32_t)((int32_t)L_82-(int32_t)L_83)), /*hidden argument*/NULL);
		int32_t L_84 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_84+(int32_t)1)));
		Type_t * L_85 = ___expectedType0;
		if (!L_85)
		{
			goto IL_02d0;
		}
	}
	{
		Type_t * L_86 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_86) == ((Il2CppObject*)(Type_t *)L_87)))
		{
			goto IL_02d0;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_88 = __this->get_Settings_0();
		NullCheck(L_88);
		TypeCoercionUtility_t700629014 * L_89 = L_88->get_Coercion_0();
		Type_t * L_90 = ___expectedType0;
		StringBuilder_t243639308 * L_91 = V_2;
		NullCheck(L_91);
		String_t* L_92 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_91);
		NullCheck(L_89);
		Il2CppObject * L_93 = TypeCoercionUtility_CoerceType_m3277329918(L_89, L_90, L_92, /*hidden argument*/NULL);
		return L_93;
	}

IL_02d0:
	{
		StringBuilder_t243639308 * L_94 = V_2;
		NullCheck(L_94);
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_94);
		return L_95;
	}
}
// System.Object JsonFx.Json.JsonReader::ReadNumber(System.Type)
extern const Il2CppType* Decimal_t1954350631_0_0_0_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral222848855;
extern const uint32_t JsonReader_ReadNumber_m1947135245_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadNumber_m1947135245 (JsonReader_t3434342065 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadNumber_m1947135245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	Decimal_t1954350631  V_7;
	memset(&V_7, 0, sizeof(V_7));
	double V_8 = 0.0;
	int32_t G_B17_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B18_1 = 0;
	{
		V_0 = (bool)0;
		V_1 = (bool)0;
		int32_t L_0 = __this->get_index_3();
		V_2 = L_0;
		V_3 = 0;
		V_4 = 0;
		String_t* L_1 = __this->get_Source_1();
		int32_t L_2 = __this->get_index_3();
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m3015341861(L_1, L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_4 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = __this->get_index_3();
		int32_t L_6 = __this->get_SourceLength_2();
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0059;
		}
	}
	{
		String_t* L_7 = __this->get_Source_1();
		int32_t L_8 = __this->get_index_3();
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m3015341861(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_10 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0078;
		}
	}

IL_0059:
	{
		int32_t L_11 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_12 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_12, _stringLiteral222848855, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_006a:
	{
		int32_t L_13 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_13+(int32_t)1)));
	}

IL_0078:
	{
		int32_t L_14 = __this->get_index_3();
		int32_t L_15 = __this->get_SourceLength_2();
		if ((((int32_t)L_14) >= ((int32_t)L_15)))
		{
			goto IL_009e;
		}
	}
	{
		String_t* L_16 = __this->get_Source_1();
		int32_t L_17 = __this->get_index_3();
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m3015341861(L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_19 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_006a;
		}
	}

IL_009e:
	{
		int32_t L_20 = __this->get_index_3();
		int32_t L_21 = __this->get_SourceLength_2();
		if ((((int32_t)L_20) >= ((int32_t)L_21)))
		{
			goto IL_013f;
		}
	}
	{
		String_t* L_22 = __this->get_Source_1();
		int32_t L_23 = __this->get_index_3();
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m3015341861(L_22, L_23, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_013f;
		}
	}
	{
		V_0 = (bool)1;
		int32_t L_25 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_25+(int32_t)1)));
		int32_t L_26 = __this->get_index_3();
		int32_t L_27 = __this->get_SourceLength_2();
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_28 = __this->get_Source_1();
		int32_t L_29 = __this->get_index_3();
		NullCheck(L_28);
		Il2CppChar L_30 = String_get_Chars_m3015341861(L_28, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_31 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0119;
		}
	}

IL_00fa:
	{
		int32_t L_32 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_33 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_33, _stringLiteral222848855, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_010b:
	{
		int32_t L_34 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_34+(int32_t)1)));
	}

IL_0119:
	{
		int32_t L_35 = __this->get_index_3();
		int32_t L_36 = __this->get_SourceLength_2();
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_013f;
		}
	}
	{
		String_t* L_37 = __this->get_Source_1();
		int32_t L_38 = __this->get_index_3();
		NullCheck(L_37);
		Il2CppChar L_39 = String_get_Chars_m3015341861(L_37, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_40 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_010b;
		}
	}

IL_013f:
	{
		int32_t L_41 = __this->get_index_3();
		int32_t L_42 = V_2;
		bool L_43 = V_0;
		G_B16_0 = ((int32_t)((int32_t)L_41-(int32_t)L_42));
		if (L_43)
		{
			G_B17_0 = ((int32_t)((int32_t)L_41-(int32_t)L_42));
			goto IL_014d;
		}
	}
	{
		G_B18_0 = 0;
		G_B18_1 = G_B16_0;
		goto IL_014e;
	}

IL_014d:
	{
		G_B18_0 = 1;
		G_B18_1 = G_B17_0;
	}

IL_014e:
	{
		V_3 = ((int32_t)((int32_t)G_B18_1-(int32_t)G_B18_0));
		int32_t L_44 = __this->get_index_3();
		int32_t L_45 = __this->get_SourceLength_2();
		if ((((int32_t)L_44) >= ((int32_t)L_45)))
		{
			goto IL_02b5;
		}
	}
	{
		String_t* L_46 = __this->get_Source_1();
		int32_t L_47 = __this->get_index_3();
		NullCheck(L_46);
		Il2CppChar L_48 = String_get_Chars_m3015341861(L_46, L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)((int32_t)101))))
		{
			goto IL_018e;
		}
	}
	{
		String_t* L_49 = __this->get_Source_1();
		int32_t L_50 = __this->get_index_3();
		NullCheck(L_49);
		Il2CppChar L_51 = String_get_Chars_m3015341861(L_49, L_50, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)69)))))
		{
			goto IL_02b5;
		}
	}

IL_018e:
	{
		V_1 = (bool)1;
		int32_t L_52 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_52+(int32_t)1)));
		int32_t L_53 = __this->get_index_3();
		int32_t L_54 = __this->get_SourceLength_2();
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_01bd;
		}
	}
	{
		int32_t L_55 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_56 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_56, _stringLiteral222848855, L_55, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_56);
	}

IL_01bd:
	{
		int32_t L_57 = __this->get_index_3();
		V_5 = L_57;
		String_t* L_58 = __this->get_Source_1();
		int32_t L_59 = __this->get_index_3();
		NullCheck(L_58);
		Il2CppChar L_60 = String_get_Chars_m3015341861(L_58, L_59, /*hidden argument*/NULL);
		if ((((int32_t)L_60) == ((int32_t)((int32_t)45))))
		{
			goto IL_01ef;
		}
	}
	{
		String_t* L_61 = __this->get_Source_1();
		int32_t L_62 = __this->get_index_3();
		NullCheck(L_61);
		Il2CppChar L_63 = String_get_Chars_m3015341861(L_61, L_62, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_63) == ((uint32_t)((int32_t)43)))))
		{
			goto IL_0234;
		}
	}

IL_01ef:
	{
		int32_t L_64 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_64+(int32_t)1)));
		int32_t L_65 = __this->get_index_3();
		int32_t L_66 = __this->get_SourceLength_2();
		if ((((int32_t)L_65) >= ((int32_t)L_66)))
		{
			goto IL_0223;
		}
	}
	{
		String_t* L_67 = __this->get_Source_1();
		int32_t L_68 = __this->get_index_3();
		NullCheck(L_67);
		Il2CppChar L_69 = String_get_Chars_m3015341861(L_67, L_68, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_70 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_026b;
		}
	}

IL_0223:
	{
		int32_t L_71 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_72 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_72, _stringLiteral222848855, L_71, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_72);
	}

IL_0234:
	{
		String_t* L_73 = __this->get_Source_1();
		int32_t L_74 = __this->get_index_3();
		NullCheck(L_73);
		Il2CppChar L_75 = String_get_Chars_m3015341861(L_73, L_74, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_76 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		if (L_76)
		{
			goto IL_026b;
		}
	}
	{
		int32_t L_77 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_78 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_78, _stringLiteral222848855, L_77, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_78);
	}

IL_025d:
	{
		int32_t L_79 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_79+(int32_t)1)));
	}

IL_026b:
	{
		int32_t L_80 = __this->get_index_3();
		int32_t L_81 = __this->get_SourceLength_2();
		if ((((int32_t)L_80) >= ((int32_t)L_81)))
		{
			goto IL_0291;
		}
	}
	{
		String_t* L_82 = __this->get_Source_1();
		int32_t L_83 = __this->get_index_3();
		NullCheck(L_82);
		Il2CppChar L_84 = String_get_Chars_m3015341861(L_82, L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_85 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_025d;
		}
	}

IL_0291:
	{
		String_t* L_86 = __this->get_Source_1();
		int32_t L_87 = V_5;
		int32_t L_88 = __this->get_index_3();
		int32_t L_89 = V_5;
		NullCheck(L_86);
		String_t* L_90 = String_Substring_m675079568(L_86, L_87, ((int32_t)((int32_t)L_88-(int32_t)L_89)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_91 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32_TryParse_m2457543725(NULL /*static, unused*/, L_90, 7, L_91, (&V_4), /*hidden argument*/NULL);
	}

IL_02b5:
	{
		String_t* L_92 = __this->get_Source_1();
		int32_t L_93 = V_2;
		int32_t L_94 = __this->get_index_3();
		int32_t L_95 = V_2;
		NullCheck(L_92);
		String_t* L_96 = String_Substring_m675079568(L_92, L_93, ((int32_t)((int32_t)L_94-(int32_t)L_95)), /*hidden argument*/NULL);
		V_6 = L_96;
		bool L_97 = V_0;
		if (L_97)
		{
			goto IL_0380;
		}
	}
	{
		bool L_98 = V_1;
		if (L_98)
		{
			goto IL_0380;
		}
	}
	{
		int32_t L_99 = V_3;
		if ((((int32_t)L_99) >= ((int32_t)((int32_t)19))))
		{
			goto IL_0380;
		}
	}
	{
		String_t* L_100 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_101 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_102 = Decimal_Parse_m2075137301(NULL /*static, unused*/, L_100, 7, L_101, /*hidden argument*/NULL);
		V_7 = L_102;
		Type_t * L_103 = ___expectedType0;
		if (!L_103)
		{
			goto IL_030a;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_104 = __this->get_Settings_0();
		NullCheck(L_104);
		TypeCoercionUtility_t700629014 * L_105 = L_104->get_Coercion_0();
		Type_t * L_106 = ___expectedType0;
		Decimal_t1954350631  L_107 = V_7;
		Decimal_t1954350631  L_108 = L_107;
		Il2CppObject * L_109 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_108);
		NullCheck(L_105);
		Il2CppObject * L_110 = TypeCoercionUtility_CoerceType_m3277329918(L_105, L_106, L_109, /*hidden argument*/NULL);
		return L_110;
	}

IL_030a:
	{
		Decimal_t1954350631  L_111 = V_7;
		Decimal_t1954350631  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Decimal__ctor_m3224507889(&L_112, ((int32_t)-2147483648LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_113 = Decimal_op_GreaterThanOrEqual_m2249474966(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_033d;
		}
	}
	{
		Decimal_t1954350631  L_114 = V_7;
		Decimal_t1954350631  L_115;
		memset(&L_115, 0, sizeof(L_115));
		Decimal__ctor_m3224507889(&L_115, ((int32_t)2147483647LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_116 = Decimal_op_LessThanOrEqual_m3623476551(NULL /*static, unused*/, L_114, L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_033d;
		}
	}
	{
		Decimal_t1954350631  L_117 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		int32_t L_118 = Decimal_op_Explicit_m2040523874(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		int32_t L_119 = L_118;
		Il2CppObject * L_120 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_119);
		return L_120;
	}

IL_033d:
	{
		Decimal_t1954350631  L_121 = V_7;
		Decimal_t1954350631  L_122;
		memset(&L_122, 0, sizeof(L_122));
		Decimal__ctor_m3224510834(&L_122, ((int64_t)std::numeric_limits<int64_t>::min()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_123 = Decimal_op_GreaterThanOrEqual_m2249474966(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_0378;
		}
	}
	{
		Decimal_t1954350631  L_124 = V_7;
		Decimal_t1954350631  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Decimal__ctor_m3224510834(&L_125, ((int64_t)std::numeric_limits<int64_t>::max()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		bool L_126 = Decimal_op_LessThanOrEqual_m3623476551(NULL /*static, unused*/, L_124, L_125, /*hidden argument*/NULL);
		if (!L_126)
		{
			goto IL_0378;
		}
	}
	{
		Decimal_t1954350631  L_127 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		int64_t L_128 = Decimal_op_Explicit_m3678935617(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		int64_t L_129 = L_128;
		Il2CppObject * L_130 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_129);
		return L_130;
	}

IL_0378:
	{
		Decimal_t1954350631  L_131 = V_7;
		Decimal_t1954350631  L_132 = L_131;
		Il2CppObject * L_133 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_132);
		return L_133;
	}

IL_0380:
	{
		Type_t * L_134 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_135 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1954350631_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_134) == ((Il2CppObject*)(Type_t *)L_135))))
		{
			goto IL_03a4;
		}
	}
	{
		String_t* L_136 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_137 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_138 = Decimal_Parse_m2075137301(NULL /*static, unused*/, L_136, ((int32_t)167), L_137, /*hidden argument*/NULL);
		Decimal_t1954350631  L_139 = L_138;
		Il2CppObject * L_140 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_139);
		return L_140;
	}

IL_03a4:
	{
		String_t* L_141 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_142 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_143 = Double_Parse_m3249074997(NULL /*static, unused*/, L_141, ((int32_t)167), L_142, /*hidden argument*/NULL);
		V_8 = L_143;
		Type_t * L_144 = ___expectedType0;
		if (!L_144)
		{
			goto IL_03d3;
		}
	}
	{
		JsonReaderSettings_t24058356 * L_145 = __this->get_Settings_0();
		NullCheck(L_145);
		TypeCoercionUtility_t700629014 * L_146 = L_145->get_Coercion_0();
		Type_t * L_147 = ___expectedType0;
		double L_148 = V_8;
		double L_149 = L_148;
		Il2CppObject * L_150 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_149);
		NullCheck(L_146);
		Il2CppObject * L_151 = TypeCoercionUtility_CoerceType_m3277329918(L_146, L_147, L_150, /*hidden argument*/NULL);
		return L_151;
	}

IL_03d3:
	{
		double L_152 = V_8;
		double L_153 = L_152;
		Il2CppObject * L_154 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_153);
		return L_154;
	}
}
// System.Object JsonFx.Json.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern Il2CppClass* JsonReader_t3434342065_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_Deserialize_m1765830364_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_Deserialize_m1765830364 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___start1, Type_t * ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Deserialize_m1765830364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		JsonReader_t3434342065 * L_1 = (JsonReader_t3434342065 *)il2cpp_codegen_object_new(JsonReader_t3434342065_il2cpp_TypeInfo_var);
		JsonReader__ctor_m3511759990(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___start1;
		Type_t * L_3 = ___type2;
		NullCheck(L_1);
		Il2CppObject * L_4 = JsonReader_Deserialize_m691121176(L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize()
extern "C"  int32_t JsonReader_Tokenize_m3827593607 (JsonReader_t3434342065 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JsonReader_Tokenize_m338063038(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// JsonFx.Json.JsonToken JsonFx.Json.JsonReader::Tokenize(System.Boolean)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1499;
extern Il2CppCodeGenString* _stringLiteral940789087;
extern Il2CppCodeGenString* _stringLiteral1504;
extern Il2CppCodeGenString* _stringLiteral911885606;
extern Il2CppCodeGenString* _stringLiteral1349;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral78043;
extern Il2CppCodeGenString* _stringLiteral237817416;
extern Il2CppCodeGenString* _stringLiteral506745205;
extern Il2CppCodeGenString* _stringLiteral3256836432;
extern const uint32_t JsonReader_Tokenize_m338063038_MetadataUsageId;
extern "C"  int32_t JsonReader_Tokenize_m338063038 (JsonReader_t3434342065 * __this, bool ___allowUnquotedString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Tokenize_m338063038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		int32_t L_0 = __this->get_index_3();
		int32_t L_1 = __this->get_SourceLength_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0010:
	{
		int32_t L_2 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = __this->get_index_3();
		int32_t L_4 = __this->get_SourceLength_2();
		if ((((int32_t)L_3) < ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}
	{
		return (int32_t)(0);
	}

IL_002e:
	{
		String_t* L_5 = __this->get_Source_1();
		int32_t L_6 = __this->get_index_3();
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m3015341861(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_8 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_9 = __this->get_Source_1();
		int32_t L_10 = __this->get_index_3();
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m3015341861(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1499);
		Il2CppChar L_12 = String_get_Chars_m3015341861(_stringLiteral1499, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_021c;
		}
	}
	{
		int32_t L_13 = __this->get_index_3();
		int32_t L_14 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_13+(int32_t)1))) < ((int32_t)L_14)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_15 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_16 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_16, _stringLiteral940789087, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0088:
	{
		int32_t L_17 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_17+(int32_t)1)));
		V_0 = (bool)0;
		String_t* L_18 = __this->get_Source_1();
		int32_t L_19 = __this->get_index_3();
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1499);
		Il2CppChar L_21 = String_get_Chars_m3015341861(_stringLiteral1499, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_00ba;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_00e9;
	}

IL_00ba:
	{
		String_t* L_22 = __this->get_Source_1();
		int32_t L_23 = __this->get_index_3();
		NullCheck(L_22);
		Il2CppChar L_24 = String_get_Chars_m3015341861(L_22, L_23, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1504);
		Il2CppChar L_25 = String_get_Chars_m3015341861(_stringLiteral1504, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)L_25)))
		{
			goto IL_00e9;
		}
	}
	{
		int32_t L_26 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_27 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_27, _stringLiteral940789087, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00e9:
	{
		int32_t L_28 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_28+(int32_t)1)));
		bool L_29 = V_0;
		if (!L_29)
		{
			goto IL_01c6;
		}
	}
	{
		int32_t L_30 = __this->get_index_3();
		V_1 = ((int32_t)((int32_t)L_30-(int32_t)2));
		int32_t L_31 = __this->get_index_3();
		int32_t L_32 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_31+(int32_t)1))) < ((int32_t)L_32)))
		{
			goto IL_014c;
		}
	}
	{
		int32_t L_33 = V_1;
		JsonDeserializationException_t1696613230 * L_34 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_34, _stringLiteral911885606, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_0122:
	{
		int32_t L_35 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_35+(int32_t)1)));
		int32_t L_36 = __this->get_index_3();
		int32_t L_37 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_36+(int32_t)1))) < ((int32_t)L_37)))
		{
			goto IL_014c;
		}
	}
	{
		int32_t L_38 = V_1;
		JsonDeserializationException_t1696613230 * L_39 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_39, _stringLiteral911885606, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39);
	}

IL_014c:
	{
		String_t* L_40 = __this->get_Source_1();
		int32_t L_41 = __this->get_index_3();
		NullCheck(L_40);
		Il2CppChar L_42 = String_get_Chars_m3015341861(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(_stringLiteral1349);
		Il2CppChar L_43 = String_get_Chars_m3015341861(_stringLiteral1349, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)L_43))))
		{
			goto IL_0122;
		}
	}
	{
		String_t* L_44 = __this->get_Source_1();
		int32_t L_45 = __this->get_index_3();
		NullCheck(L_44);
		Il2CppChar L_46 = String_get_Chars_m3015341861(L_44, ((int32_t)((int32_t)L_45+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(_stringLiteral1349);
		Il2CppChar L_47 = String_get_Chars_m3015341861(_stringLiteral1349, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_46) == ((uint32_t)L_47))))
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_48 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_48+(int32_t)2)));
		int32_t L_49 = __this->get_index_3();
		int32_t L_50 = __this->get_SourceLength_2();
		if ((((int32_t)L_49) < ((int32_t)L_50)))
		{
			goto IL_0204;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01a8:
	{
		int32_t L_51 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_51+(int32_t)1)));
		int32_t L_52 = __this->get_index_3();
		int32_t L_53 = __this->get_SourceLength_2();
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_01c6;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01c6:
	{
		String_t* L_54 = __this->get_Source_1();
		int32_t L_55 = __this->get_index_3();
		NullCheck(L_54);
		Il2CppChar L_56 = String_get_Chars_m3015341861(L_54, L_55, /*hidden argument*/NULL);
		NullCheck(_stringLiteral413);
		int32_t L_57 = String_IndexOf_m2775210486(_stringLiteral413, L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_57) < ((int32_t)0)))
		{
			goto IL_01a8;
		}
	}
	{
		goto IL_0204;
	}

IL_01e6:
	{
		int32_t L_58 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_58+(int32_t)1)));
		int32_t L_59 = __this->get_index_3();
		int32_t L_60 = __this->get_SourceLength_2();
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_0204;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0204:
	{
		String_t* L_61 = __this->get_Source_1();
		int32_t L_62 = __this->get_index_3();
		NullCheck(L_61);
		Il2CppChar L_63 = String_get_Chars_m3015341861(L_61, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_64 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_01e6;
		}
	}

IL_021c:
	{
		String_t* L_65 = __this->get_Source_1();
		int32_t L_66 = __this->get_index_3();
		NullCheck(L_65);
		Il2CppChar L_67 = String_get_Chars_m3015341861(L_65, L_66, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)43)))))
		{
			goto IL_024f;
		}
	}
	{
		int32_t L_68 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_68+(int32_t)1)));
		int32_t L_69 = __this->get_index_3();
		int32_t L_70 = __this->get_SourceLength_2();
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_024f;
		}
	}
	{
		return (int32_t)(0);
	}

IL_024f:
	{
		String_t* L_71 = __this->get_Source_1();
		int32_t L_72 = __this->get_index_3();
		NullCheck(L_71);
		Il2CppChar L_73 = String_get_Chars_m3015341861(L_71, L_72, /*hidden argument*/NULL);
		V_2 = L_73;
		Il2CppChar L_74 = V_2;
		if ((((int32_t)L_74) > ((int32_t)((int32_t)44))))
		{
			goto IL_0277;
		}
	}
	{
		Il2CppChar L_75 = V_2;
		if ((((int32_t)L_75) == ((int32_t)((int32_t)34))))
		{
			goto IL_02b4;
		}
	}
	{
		Il2CppChar L_76 = V_2;
		if ((((int32_t)L_76) == ((int32_t)((int32_t)39))))
		{
			goto IL_02b4;
		}
	}
	{
		Il2CppChar L_77 = V_2;
		if ((((int32_t)L_77) == ((int32_t)((int32_t)44))))
		{
			goto IL_02b7;
		}
	}
	{
		goto IL_02bd;
	}

IL_0277:
	{
		Il2CppChar L_78 = V_2;
		if ((((int32_t)L_78) == ((int32_t)((int32_t)58))))
		{
			goto IL_02ba;
		}
	}
	{
		Il2CppChar L_79 = V_2;
		if (((int32_t)((int32_t)L_79-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_02a8;
		}
		if (((int32_t)((int32_t)L_79-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_02bd;
		}
		if (((int32_t)((int32_t)L_79-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_02ab;
		}
	}
	{
		Il2CppChar L_80 = V_2;
		if (((int32_t)((int32_t)L_80-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_02ae;
		}
		if (((int32_t)((int32_t)L_80-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_02bd;
		}
		if (((int32_t)((int32_t)L_80-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_02b1;
		}
	}
	{
		goto IL_02bd;
	}

IL_02a8:
	{
		return (int32_t)(((int32_t)10));
	}

IL_02ab:
	{
		return (int32_t)(((int32_t)11));
	}

IL_02ae:
	{
		return (int32_t)(((int32_t)12));
	}

IL_02b1:
	{
		return (int32_t)(((int32_t)13));
	}

IL_02b4:
	{
		return (int32_t)(((int32_t)9));
	}

IL_02b7:
	{
		return (int32_t)(((int32_t)15));
	}

IL_02ba:
	{
		return (int32_t)(((int32_t)14));
	}

IL_02bd:
	{
		String_t* L_81 = __this->get_Source_1();
		int32_t L_82 = __this->get_index_3();
		NullCheck(L_81);
		Il2CppChar L_83 = String_get_Chars_m3015341861(L_81, L_82, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_84 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		if (L_84)
		{
			goto IL_0314;
		}
	}
	{
		String_t* L_85 = __this->get_Source_1();
		int32_t L_86 = __this->get_index_3();
		NullCheck(L_85);
		Il2CppChar L_87 = String_get_Chars_m3015341861(L_85, L_86, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_87) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0316;
		}
	}
	{
		int32_t L_88 = __this->get_index_3();
		int32_t L_89 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_88+(int32_t)1))) >= ((int32_t)L_89)))
		{
			goto IL_0316;
		}
	}
	{
		String_t* L_90 = __this->get_Source_1();
		int32_t L_91 = __this->get_index_3();
		NullCheck(L_90);
		Il2CppChar L_92 = String_get_Chars_m3015341861(L_90, ((int32_t)((int32_t)L_91+(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		bool L_93 = Char_IsDigit_m1743537211(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_0316;
		}
	}

IL_0314:
	{
		return (int32_t)(8);
	}

IL_0316:
	{
		bool L_94 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral97196323, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_0325;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0325:
	{
		bool L_95 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral3569038, /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_0334;
		}
	}
	{
		return (int32_t)(4);
	}

IL_0334:
	{
		bool L_96 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral3392903, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_0343;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0343:
	{
		bool L_97 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral78043, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_0352;
		}
	}
	{
		return (int32_t)(5);
	}

IL_0352:
	{
		bool L_98 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral237817416, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_0361;
		}
	}
	{
		return (int32_t)(6);
	}

IL_0361:
	{
		bool L_99 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral506745205, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_0370;
		}
	}
	{
		return (int32_t)(7);
	}

IL_0370:
	{
		bool L_100 = JsonReader_MatchLiteral_m706834448(__this, _stringLiteral3256836432, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_037f;
		}
	}
	{
		return (int32_t)(1);
	}

IL_037f:
	{
		bool L_101 = ___allowUnquotedString0;
		if (!L_101)
		{
			goto IL_0385;
		}
	}
	{
		return (int32_t)(((int32_t)16));
	}

IL_0385:
	{
		int32_t L_102 = __this->get_index_3();
		JsonDeserializationException_t1696613230 * L_103 = (JsonDeserializationException_t1696613230 *)il2cpp_codegen_object_new(JsonDeserializationException_t1696613230_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m393045188(L_103, _stringLiteral940789087, L_102, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_103);
	}
}
// System.Boolean JsonFx.Json.JsonReader::MatchLiteral(System.String)
extern "C"  bool JsonReader_MatchLiteral_m706834448 (JsonReader_t3434342065 * __this, String_t* ___literal0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___literal0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		int32_t L_2 = __this->get_index_3();
		V_2 = L_2;
		goto IL_0031;
	}

IL_0012:
	{
		String_t* L_3 = ___literal0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		String_t* L_6 = __this->get_Source_1();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m3015341861(L_6, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)L_8)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_13 = V_2;
		int32_t L_14 = __this->get_SourceLength_2();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0012;
		}
	}

IL_003e:
	{
		return (bool)1;
	}
}
// System.Boolean JsonFx.Json.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern "C"  bool JsonReaderSettings_get_AllowUnquotedObjectKeys_m3720636459 (JsonReaderSettings_t24058356 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_allowUnquotedObjectKeys_1();
		return L_0;
	}
}
// System.Boolean JsonFx.Json.JsonReaderSettings::IsTypeHintName(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern const uint32_t JsonReaderSettings_IsTypeHintName_m2842466433_MetadataUsageId;
extern "C"  bool JsonReaderSettings_IsTypeHintName_m2842466433 (JsonReaderSettings_t24058356 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_IsTypeHintName_m2842466433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_2 = __this->get_typeHintName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_4 = StringComparer_get_Ordinal_m2543279027(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_typeHintName_2();
		String_t* L_6 = ___name0;
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(11 /* System.Boolean System.StringComparer::Equals(System.String,System.String) */, L_4, L_5, L_6);
		return L_7;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Void JsonFx.Json.JsonReaderSettings::.ctor()
extern Il2CppClass* TypeCoercionUtility_t700629014_il2cpp_TypeInfo_var;
extern const uint32_t JsonReaderSettings__ctor_m302022697_MetadataUsageId;
extern "C"  void JsonReaderSettings__ctor_m302022697 (JsonReaderSettings_t24058356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings__ctor_m302022697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeCoercionUtility_t700629014 * L_0 = (TypeCoercionUtility_t700629014 *)il2cpp_codegen_object_new(TypeCoercionUtility_t700629014_il2cpp_TypeInfo_var);
		TypeCoercionUtility__ctor_m3843210747(L_0, /*hidden argument*/NULL);
		__this->set_Coercion_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonFx.Json.JsonSerializationException::.ctor(System.String)
extern "C"  void JsonSerializationException__ctor_m2046370034 (JsonSerializationException_t3458665517 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		InvalidOperationException__ctor_m1485483280(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern "C"  String_t* JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m61992303 (JsonSpecifiedPropertyAttribute_t3902267749 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_specifiedProperty_0();
		return L_0;
	}
}
// System.String JsonFx.Json.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern const Il2CppType* JsonSpecifiedPropertyAttribute_t3902267749_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSpecifiedPropertyAttribute_t3902267749_il2cpp_TypeInfo_var;
extern const uint32_t JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m751699402_MetadataUsageId;
extern "C"  String_t* JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m751699402 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m751699402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonSpecifiedPropertyAttribute_t3902267749 * V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t3902267749_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Attribute_IsDefined_m3508553943(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (String_t*)NULL;
	}

IL_0017:
	{
		MemberInfo_t * L_4 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t3902267749_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_6 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = ((JsonSpecifiedPropertyAttribute_t3902267749 *)CastclassClass(L_6, JsonSpecifiedPropertyAttribute_t3902267749_il2cpp_TypeInfo_var));
		JsonSpecifiedPropertyAttribute_t3902267749 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m61992303(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String)
extern "C"  void JsonTypeCoercionException__ctor_m3517389954 (JsonTypeCoercionException_t4269543025 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ArgumentException__ctor_m3544856547(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonFx.Json.JsonTypeCoercionException::.ctor(System.String,System.Exception)
extern "C"  void JsonTypeCoercionException__ctor_m2864512244 (JsonTypeCoercionException_t4269543025 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		ArgumentException__ctor_m2711220147(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder)
extern Il2CppClass* JsonWriterSettings_t323204516_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m2321787662_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m2321787662 (JsonWriter_t3589747297 * __this, StringBuilder_t243639308 * ___output0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m2321787662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t243639308 * L_0 = ___output0;
		JsonWriterSettings_t323204516 * L_1 = (JsonWriterSettings_t323204516 *)il2cpp_codegen_object_new(JsonWriterSettings_t323204516_il2cpp_TypeInfo_var);
		JsonWriterSettings__ctor_m950536825(L_1, /*hidden argument*/NULL);
		JsonWriter__ctor_m780966760(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder,JsonFx.Json.JsonWriterSettings)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4216882900_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3289454849;
extern Il2CppCodeGenString* _stringLiteral1434631203;
extern const uint32_t JsonWriter__ctor_m780966760_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m780966760 (JsonWriter_t3589747297 * __this, StringBuilder_t243639308 * ___output0, JsonWriterSettings_t323204516 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m780966760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_0 = ___output0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3289454849, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		JsonWriterSettings_t323204516 * L_2 = ___settings1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral1434631203, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		StringBuilder_t243639308 * L_4 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_5 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4216882900 * L_6 = (StringWriter_t4216882900 *)il2cpp_codegen_object_new(StringWriter_t4216882900_il2cpp_TypeInfo_var);
		StringWriter__ctor_m2916801583(L_6, L_4, L_5, /*hidden argument*/NULL);
		__this->set_Writer_0(L_6);
		JsonWriterSettings_t323204516 * L_7 = ___settings1;
		__this->set_settings_1(L_7);
		TextWriter_t2304124208 * L_8 = __this->get_Writer_0();
		JsonWriterSettings_t323204516 * L_9 = __this->get_settings_1();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String JsonFx.Json.JsonWriterSettings::get_NewLine() */, L_9);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void System.IO.TextWriter::set_NewLine(System.String) */, L_8, L_10);
		return;
	}
}
// System.String JsonFx.Json.JsonWriter::Serialize(System.Object)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t3589747297_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Serialize_m2877040541_MetadataUsageId;
extern "C"  String_t* JsonWriter_Serialize_m2877040541 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Serialize_m2877040541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	JsonWriter_t3589747297 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t243639308 * L_1 = V_0;
		JsonWriter_t3589747297 * L_2 = (JsonWriter_t3589747297 *)il2cpp_codegen_object_new(JsonWriter_t3589747297_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m2321787662(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		JsonWriter_t3589747297 * L_3 = V_1;
		Il2CppObject * L_4 = ___value0;
		NullCheck(L_3);
		JsonWriter_Write_m2686199963(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x20, FINALLY_0016);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		{
			JsonWriter_t3589747297 * L_5 = V_1;
			if (!L_5)
			{
				goto IL_001f;
			}
		}

IL_0019:
		{
			JsonWriter_t3589747297 * L_6 = V_1;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_6);
		}

IL_001f:
		{
			IL2CPP_END_FINALLY(22)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0020:
	{
		StringBuilder_t243639308 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		return L_8;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Object)
extern "C"  void JsonWriter_Write_m2686199963 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean)
extern Il2CppClass* IJsonSerializable_t668120466_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3458665517_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2862754429_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t413522987_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t763695022_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNode_t856910923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern Il2CppCodeGenString* _stringLiteral2235684418;
extern const uint32_t JsonWriter_Write_m228622722_MetadataUsageId;
extern "C"  void JsonWriter_Write_m228622722 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, bool ___isProperty1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m228622722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___isProperty1;
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		JsonWriterSettings_t323204516 * L_1 = __this->get_settings_1();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint() */, L_1);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		TextWriter_t2304124208 * L_3 = __this->get_Writer_0();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)32));
	}

IL_001d:
	{
		Il2CppObject * L_4 = ___value0;
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, _stringLiteral3392903);
		return;
	}

IL_0031:
	{
		Il2CppObject * L_6 = ___value0;
		if (!((Il2CppObject *)IsInst(L_6, IJsonSerializable_t668120466_il2cpp_TypeInfo_var)))
		{
			goto IL_00a4;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		{
			bool L_7 = ___isProperty1;
			if (!L_7)
			{
				goto IL_0083;
			}
		}

IL_003c:
		{
			int32_t L_8 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_8+(int32_t)1)));
			int32_t L_9 = __this->get_depth_2();
			JsonWriterSettings_t323204516 * L_10 = __this->get_settings_1();
			NullCheck(L_10);
			int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_10);
			if ((((int32_t)L_9) <= ((int32_t)L_11)))
			{
				goto IL_007d;
			}
		}

IL_005d:
		{
			JsonWriterSettings_t323204516 * L_12 = __this->get_settings_1();
			NullCheck(L_12);
			int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_12);
			int32_t L_14 = L_13;
			Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_15, /*hidden argument*/NULL);
			JsonSerializationException_t3458665517 * L_17 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2046370034(L_17, L_16, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
		}

IL_007d:
		{
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		}

IL_0083:
		{
			Il2CppObject * L_18 = ___value0;
			NullCheck(((Il2CppObject *)Castclass(L_18, IJsonSerializable_t668120466_il2cpp_TypeInfo_var)));
			InterfaceActionInvoker1< JsonWriter_t3589747297 * >::Invoke(0 /* System.Void JsonFx.Json.IJsonSerializable::WriteJson(JsonFx.Json.JsonWriter) */, IJsonSerializable_t668120466_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_18, IJsonSerializable_t668120466_il2cpp_TypeInfo_var)), __this);
			IL2CPP_LEAVE(0xA3, FINALLY_0091);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0091;
	}

FINALLY_0091:
	{ // begin finally (depth: 1)
		{
			bool L_19 = ___isProperty1;
			if (!L_19)
			{
				goto IL_00a2;
			}
		}

IL_0094:
		{
			int32_t L_20 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_20-(int32_t)1)));
		}

IL_00a2:
		{
			IL2CPP_END_FINALLY(145)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(145)
	{
		IL2CPP_JUMP_TBL(0xA3, IL_00a3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a3:
	{
		return;
	}

IL_00a4:
	{
		Il2CppObject * L_21 = ___value0;
		if (!((Enum_t2862688501 *)IsInstClass(L_21, Enum_t2862688501_il2cpp_TypeInfo_var)))
		{
			goto IL_00b9;
		}
	}
	{
		Il2CppObject * L_22 = ___value0;
		VirtActionInvoker1< Enum_t2862688501 * >::Invoke(8 /* System.Void JsonFx.Json.JsonWriter::Write(System.Enum) */, __this, ((Enum_t2862688501 *)CastclassClass(L_22, Enum_t2862688501_il2cpp_TypeInfo_var)));
		return;
	}

IL_00b9:
	{
		Il2CppObject * L_23 = ___value0;
		NullCheck(L_23);
		Type_t * L_24 = Object_GetType_m2022236990(L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		Type_t * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_26 = Type_GetTypeCode_m2969996822(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		int32_t L_27 = V_1;
		if (L_27 == 0)
		{
			goto IL_0152;
		}
		if (L_27 == 1)
		{
			goto IL_01f2;
		}
		if (L_27 == 2)
		{
			goto IL_0152;
		}
		if (L_27 == 3)
		{
			goto IL_011e;
		}
		if (L_27 == 4)
		{
			goto IL_0138;
		}
		if (L_27 == 5)
		{
			goto IL_01a4;
		}
		if (L_27 == 6)
		{
			goto IL_012b;
		}
		if (L_27 == 7)
		{
			goto IL_017d;
		}
		if (L_27 == 8)
		{
			goto IL_01cb;
		}
		if (L_27 == 9)
		{
			goto IL_018a;
		}
		if (L_27 == 10)
		{
			goto IL_01d8;
		}
		if (L_27 == 11)
		{
			goto IL_0197;
		}
		if (L_27 == 12)
		{
			goto IL_01e5;
		}
		if (L_27 == 13)
		{
			goto IL_01b1;
		}
		if (L_27 == 14)
		{
			goto IL_0170;
		}
		if (L_27 == 15)
		{
			goto IL_0163;
		}
		if (L_27 == 16)
		{
			goto IL_0145;
		}
		if (L_27 == 17)
		{
			goto IL_01f2;
		}
		if (L_27 == 18)
		{
			goto IL_01be;
		}
	}
	{
		goto IL_01f2;
	}

IL_011e:
	{
		Il2CppObject * L_28 = ___value0;
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void JsonFx.Json.JsonWriter::Write(System.Boolean) */, __this, ((*(bool*)((bool*)UnBox (L_28, Boolean_t476798718_il2cpp_TypeInfo_var)))));
		return;
	}

IL_012b:
	{
		Il2CppObject * L_29 = ___value0;
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void JsonFx.Json.JsonWriter::Write(System.Byte) */, __this, ((*(uint8_t*)((uint8_t*)UnBox (L_29, Byte_t2862609660_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0138:
	{
		Il2CppObject * L_30 = ___value0;
		VirtActionInvoker1< Il2CppChar >::Invoke(22 /* System.Void JsonFx.Json.JsonWriter::Write(System.Char) */, __this, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_30, Char_t2862622538_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0145:
	{
		Il2CppObject * L_31 = ___value0;
		VirtActionInvoker1< DateTime_t4283661327  >::Invoke(6 /* System.Void JsonFx.Json.JsonWriter::Write(System.DateTime) */, __this, ((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_31, DateTime_t4283661327_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0152:
	{
		TextWriter_t2304124208 * L_32 = __this->get_Writer_0();
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_32, _stringLiteral3392903);
		return;
	}

IL_0163:
	{
		Il2CppObject * L_33 = ___value0;
		VirtActionInvoker1< Decimal_t1954350631  >::Invoke(21 /* System.Void JsonFx.Json.JsonWriter::Write(System.Decimal) */, __this, ((*(Decimal_t1954350631 *)((Decimal_t1954350631 *)UnBox (L_33, Decimal_t1954350631_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0170:
	{
		Il2CppObject * L_34 = ___value0;
		VirtActionInvoker1< double >::Invoke(20 /* System.Void JsonFx.Json.JsonWriter::Write(System.Double) */, __this, ((*(double*)((double*)UnBox (L_34, Double_t3868226565_il2cpp_TypeInfo_var)))));
		return;
	}

IL_017d:
	{
		Il2CppObject * L_35 = ___value0;
		VirtActionInvoker1< int16_t >::Invoke(13 /* System.Void JsonFx.Json.JsonWriter::Write(System.Int16) */, __this, ((*(int16_t*)((int16_t*)UnBox (L_35, Int16_t1153838442_il2cpp_TypeInfo_var)))));
		return;
	}

IL_018a:
	{
		Il2CppObject * L_36 = ___value0;
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void JsonFx.Json.JsonWriter::Write(System.Int32) */, __this, ((*(int32_t*)((int32_t*)UnBox (L_36, Int32_t1153838500_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0197:
	{
		Il2CppObject * L_37 = ___value0;
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void JsonFx.Json.JsonWriter::Write(System.Int64) */, __this, ((*(int64_t*)((int64_t*)UnBox (L_37, Int64_t1153838595_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01a4:
	{
		Il2CppObject * L_38 = ___value0;
		VirtActionInvoker1< int8_t >::Invoke(12 /* System.Void JsonFx.Json.JsonWriter::Write(System.SByte) */, __this, ((*(int8_t*)((int8_t*)UnBox (L_38, SByte_t1161769777_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01b1:
	{
		Il2CppObject * L_39 = ___value0;
		VirtActionInvoker1< float >::Invoke(19 /* System.Void JsonFx.Json.JsonWriter::Write(System.Single) */, __this, ((*(float*)((float*)UnBox (L_39, Single_t4291918972_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01be:
	{
		Il2CppObject * L_40 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, ((String_t*)CastclassSealed(L_40, String_t_il2cpp_TypeInfo_var)));
		return;
	}

IL_01cb:
	{
		Il2CppObject * L_41 = ___value0;
		VirtActionInvoker1< uint16_t >::Invoke(14 /* System.Void JsonFx.Json.JsonWriter::Write(System.UInt16) */, __this, ((*(uint16_t*)((uint16_t*)UnBox (L_41, UInt16_t24667923_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01d8:
	{
		Il2CppObject * L_42 = ___value0;
		VirtActionInvoker1< uint32_t >::Invoke(16 /* System.Void JsonFx.Json.JsonWriter::Write(System.UInt32) */, __this, ((*(uint32_t*)((uint32_t*)UnBox (L_42, UInt32_t24667981_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01e5:
	{
		Il2CppObject * L_43 = ___value0;
		VirtActionInvoker1< uint64_t >::Invoke(18 /* System.Void JsonFx.Json.JsonWriter::Write(System.UInt64) */, __this, ((*(uint64_t*)((uint64_t*)UnBox (L_43, UInt64_t24668076_il2cpp_TypeInfo_var)))));
		return;
	}

IL_01f2:
	{
		Il2CppObject * L_44 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_44, Guid_t2862754429_il2cpp_TypeInfo_var)))
		{
			goto IL_0207;
		}
	}
	{
		Il2CppObject * L_45 = ___value0;
		VirtActionInvoker1< Guid_t2862754429  >::Invoke(7 /* System.Void JsonFx.Json.JsonWriter::Write(System.Guid) */, __this, ((*(Guid_t2862754429 *)((Guid_t2862754429 *)UnBox (L_45, Guid_t2862754429_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0207:
	{
		Il2CppObject * L_46 = ___value0;
		if (!((Uri_t1116831938 *)IsInstClass(L_46, Uri_t1116831938_il2cpp_TypeInfo_var)))
		{
			goto IL_021c;
		}
	}
	{
		Il2CppObject * L_47 = ___value0;
		VirtActionInvoker1< Uri_t1116831938 * >::Invoke(24 /* System.Void JsonFx.Json.JsonWriter::Write(System.Uri) */, __this, ((Uri_t1116831938 *)CastclassClass(L_47, Uri_t1116831938_il2cpp_TypeInfo_var)));
		return;
	}

IL_021c:
	{
		Il2CppObject * L_48 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_48, TimeSpan_t413522987_il2cpp_TypeInfo_var)))
		{
			goto IL_0231;
		}
	}
	{
		Il2CppObject * L_49 = ___value0;
		VirtActionInvoker1< TimeSpan_t413522987  >::Invoke(23 /* System.Void JsonFx.Json.JsonWriter::Write(System.TimeSpan) */, __this, ((*(TimeSpan_t413522987 *)((TimeSpan_t413522987 *)UnBox (L_49, TimeSpan_t413522987_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0231:
	{
		Il2CppObject * L_50 = ___value0;
		if (!((Version_t763695022 *)IsInstSealed(L_50, Version_t763695022_il2cpp_TypeInfo_var)))
		{
			goto IL_0246;
		}
	}
	{
		Il2CppObject * L_51 = ___value0;
		VirtActionInvoker1< Version_t763695022 * >::Invoke(25 /* System.Void JsonFx.Json.JsonWriter::Write(System.Version) */, __this, ((Version_t763695022 *)CastclassSealed(L_51, Version_t763695022_il2cpp_TypeInfo_var)));
		return;
	}

IL_0246:
	{
		Il2CppObject * L_52 = ___value0;
		if (!((Il2CppObject *)IsInst(L_52, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_02b9;
		}
	}

IL_024e:
	try
	{ // begin try (depth: 1)
		{
			bool L_53 = ___isProperty1;
			if (!L_53)
			{
				goto IL_0298;
			}
		}

IL_0251:
		{
			int32_t L_54 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_54+(int32_t)1)));
			int32_t L_55 = __this->get_depth_2();
			JsonWriterSettings_t323204516 * L_56 = __this->get_settings_1();
			NullCheck(L_56);
			int32_t L_57 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_56);
			if ((((int32_t)L_55) <= ((int32_t)L_57)))
			{
				goto IL_0292;
			}
		}

IL_0272:
		{
			JsonWriterSettings_t323204516 * L_58 = __this->get_settings_1();
			NullCheck(L_58);
			int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_58);
			int32_t L_60 = L_59;
			Il2CppObject * L_61 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_60);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_61, /*hidden argument*/NULL);
			JsonSerializationException_t3458665517 * L_63 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2046370034(L_63, L_62, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_63);
		}

IL_0292:
		{
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		}

IL_0298:
		{
			Il2CppObject * L_64 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void JsonFx.Json.JsonWriter::WriteObject(System.Collections.IDictionary) */, __this, ((Il2CppObject *)Castclass(L_64, IDictionary_t537317817_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x2B8, FINALLY_02a6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_02a6;
	}

FINALLY_02a6:
	{ // begin finally (depth: 1)
		{
			bool L_65 = ___isProperty1;
			if (!L_65)
			{
				goto IL_02b7;
			}
		}

IL_02a9:
		{
			int32_t L_66 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_66-(int32_t)1)));
		}

IL_02b7:
		{
			IL2CPP_END_FINALLY(678)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(678)
	{
		IL2CPP_JUMP_TBL(0x2B8, IL_02b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_02b8:
	{
		return;
	}

IL_02b9:
	{
		Type_t * L_67 = V_0;
		NullCheck(L_67);
		Type_t * L_68 = Type_GetInterface_m1133348080(L_67, _stringLiteral2235684418, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0331;
		}
	}

IL_02c6:
	try
	{ // begin try (depth: 1)
		{
			bool L_69 = ___isProperty1;
			if (!L_69)
			{
				goto IL_0310;
			}
		}

IL_02c9:
		{
			int32_t L_70 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_70+(int32_t)1)));
			int32_t L_71 = __this->get_depth_2();
			JsonWriterSettings_t323204516 * L_72 = __this->get_settings_1();
			NullCheck(L_72);
			int32_t L_73 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_72);
			if ((((int32_t)L_71) <= ((int32_t)L_73)))
			{
				goto IL_030a;
			}
		}

IL_02ea:
		{
			JsonWriterSettings_t323204516 * L_74 = __this->get_settings_1();
			NullCheck(L_74);
			int32_t L_75 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_74);
			int32_t L_76 = L_75;
			Il2CppObject * L_77 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_76);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_78 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_77, /*hidden argument*/NULL);
			JsonSerializationException_t3458665517 * L_79 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2046370034(L_79, L_78, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_79);
		}

IL_030a:
		{
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		}

IL_0310:
		{
			Il2CppObject * L_80 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(30 /* System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_80, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x330, FINALLY_031e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_031e;
	}

FINALLY_031e:
	{ // begin finally (depth: 1)
		{
			bool L_81 = ___isProperty1;
			if (!L_81)
			{
				goto IL_032f;
			}
		}

IL_0321:
		{
			int32_t L_82 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_82-(int32_t)1)));
		}

IL_032f:
		{
			IL2CPP_END_FINALLY(798)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(798)
	{
		IL2CPP_JUMP_TBL(0x330, IL_0330)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0330:
	{
		return;
	}

IL_0331:
	{
		Il2CppObject * L_83 = ___value0;
		if (!((Il2CppObject *)IsInst(L_83, IEnumerable_t3464557803_il2cpp_TypeInfo_var)))
		{
			goto IL_03bc;
		}
	}
	{
		Il2CppObject * L_84 = ___value0;
		if (!((XmlNode_t856910923 *)IsInstClass(L_84, XmlNode_t856910923_il2cpp_TypeInfo_var)))
		{
			goto IL_0351;
		}
	}
	{
		Il2CppObject * L_85 = ___value0;
		VirtActionInvoker1< XmlNode_t856910923 * >::Invoke(26 /* System.Void JsonFx.Json.JsonWriter::Write(System.Xml.XmlNode) */, __this, ((XmlNode_t856910923 *)CastclassClass(L_85, XmlNode_t856910923_il2cpp_TypeInfo_var)));
		return;
	}

IL_0351:
	try
	{ // begin try (depth: 1)
		{
			bool L_86 = ___isProperty1;
			if (!L_86)
			{
				goto IL_039b;
			}
		}

IL_0354:
		{
			int32_t L_87 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_87+(int32_t)1)));
			int32_t L_88 = __this->get_depth_2();
			JsonWriterSettings_t323204516 * L_89 = __this->get_settings_1();
			NullCheck(L_89);
			int32_t L_90 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_89);
			if ((((int32_t)L_88) <= ((int32_t)L_90)))
			{
				goto IL_0395;
			}
		}

IL_0375:
		{
			JsonWriterSettings_t323204516 * L_91 = __this->get_settings_1();
			NullCheck(L_91);
			int32_t L_92 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_91);
			int32_t L_93 = L_92;
			Il2CppObject * L_94 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_93);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_95 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_94, /*hidden argument*/NULL);
			JsonSerializationException_t3458665517 * L_96 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2046370034(L_96, L_95, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_96);
		}

IL_0395:
		{
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		}

IL_039b:
		{
			Il2CppObject * L_97 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(27 /* System.Void JsonFx.Json.JsonWriter::WriteArray(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_97, IEnumerable_t3464557803_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x3BB, FINALLY_03a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_03a9;
	}

FINALLY_03a9:
	{ // begin finally (depth: 1)
		{
			bool L_98 = ___isProperty1;
			if (!L_98)
			{
				goto IL_03ba;
			}
		}

IL_03ac:
		{
			int32_t L_99 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_99-(int32_t)1)));
		}

IL_03ba:
		{
			IL2CPP_END_FINALLY(937)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(937)
	{
		IL2CPP_JUMP_TBL(0x3BB, IL_03bb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_03bb:
	{
		return;
	}

IL_03bc:
	try
	{ // begin try (depth: 1)
		{
			bool L_100 = ___isProperty1;
			if (!L_100)
			{
				goto IL_0406;
			}
		}

IL_03bf:
		{
			int32_t L_101 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_101+(int32_t)1)));
			int32_t L_102 = __this->get_depth_2();
			JsonWriterSettings_t323204516 * L_103 = __this->get_settings_1();
			NullCheck(L_103);
			int32_t L_104 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_103);
			if ((((int32_t)L_102) <= ((int32_t)L_104)))
			{
				goto IL_0400;
			}
		}

IL_03e0:
		{
			JsonWriterSettings_t323204516 * L_105 = __this->get_settings_1();
			NullCheck(L_105);
			int32_t L_106 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_105);
			int32_t L_107 = L_106;
			Il2CppObject * L_108 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_107);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_109 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_108, /*hidden argument*/NULL);
			JsonSerializationException_t3458665517 * L_110 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2046370034(L_110, L_109, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_110);
		}

IL_0400:
		{
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		}

IL_0406:
		{
			Il2CppObject * L_111 = ___value0;
			Type_t * L_112 = V_0;
			VirtActionInvoker2< Il2CppObject *, Type_t * >::Invoke(33 /* System.Void JsonFx.Json.JsonWriter::WriteObject(System.Object,System.Type) */, __this, L_111, L_112);
			IL2CPP_LEAVE(0x422, FINALLY_0410);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0410;
	}

FINALLY_0410:
	{ // begin finally (depth: 1)
		{
			bool L_113 = ___isProperty1;
			if (!L_113)
			{
				goto IL_0421;
			}
		}

IL_0413:
		{
			int32_t L_114 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_114-(int32_t)1)));
		}

IL_0421:
		{
			IL2CPP_END_FINALLY(1040)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1040)
	{
		IL2CPP_JUMP_TBL(0x422, IL_0422)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0422:
	{
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.DateTime)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* WriteDelegate_1_Invoke_m3947399451_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3567556939;
extern Il2CppCodeGenString* _stringLiteral115082479;
extern const uint32_t JsonWriter_Write_m1393129343_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1393129343 (JsonWriter_t3589747297 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1393129343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		JsonWriterSettings_t323204516 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		WriteDelegate_1_t3884844371 * L_1 = VirtFuncInvoker0< WriteDelegate_1_t3884844371 * >::Invoke(10 /* JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer() */, L_0);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		JsonWriterSettings_t323204516 * L_2 = __this->get_settings_1();
		NullCheck(L_2);
		WriteDelegate_1_t3884844371 * L_3 = VirtFuncInvoker0< WriteDelegate_1_t3884844371 * >::Invoke(10 /* JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer() */, L_2);
		DateTime_t4283661327  L_4 = ___value0;
		NullCheck(L_3);
		WriteDelegate_1_Invoke_m3947399451(L_3, __this, L_4, /*hidden argument*/WriteDelegate_1_Invoke_m3947399451_MethodInfo_var);
		return;
	}

IL_0020:
	{
		int32_t L_5 = DateTime_get_Kind_m3496013602((&___value0), /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
		{
			goto IL_0043;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)1)) == 1)
		{
			goto IL_003a;
		}
	}
	{
		goto IL_005a;
	}

IL_003a:
	{
		DateTime_t4283661327  L_7 = DateTime_ToUniversalTime_m691668206((&___value0), /*hidden argument*/NULL);
		___value0 = L_7;
	}

IL_0043:
	{
		DateTime_t4283661327  L_8 = ___value0;
		DateTime_t4283661327  L_9 = L_8;
		Il2CppObject * L_10 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3567556939, L_10, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_11);
		return;
	}

IL_005a:
	{
		DateTime_t4283661327  L_12 = ___value0;
		DateTime_t4283661327  L_13 = L_12;
		Il2CppObject * L_14 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral115082479, L_14, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_15);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Guid)
extern Il2CppCodeGenString* _stringLiteral68;
extern const uint32_t JsonWriter_Write_m3030605521_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3030605521 (JsonWriter_t3589747297 * __this, Guid_t2862754429  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3030605521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Guid_ToString_m2135662273((&___value0), _stringLiteral68, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Enum)
extern const Il2CppType* FlagsAttribute_t423505161_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t JsonWriter_Write_m3028561753_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3028561753 (JsonWriter_t3589747297 * __this, Enum_t2862688501 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3028561753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	EnumU5BU5D_t3205174168* V_2 = NULL;
	StringU5BU5D_t4054002952* V_3 = NULL;
	int32_t V_4 = 0;
	{
		V_0 = (String_t*)NULL;
		Enum_t2862688501 * L_0 = ___value0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Type_t * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(FlagsAttribute_t423505161_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(12 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_2, L_3, (bool)1);
		if (!L_4)
		{
			goto IL_0080;
		}
	}
	{
		Type_t * L_5 = V_1;
		Enum_t2862688501 * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		bool L_7 = Enum_IsDefined_m2781598580(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0080;
		}
	}
	{
		Type_t * L_8 = V_1;
		Enum_t2862688501 * L_9 = ___value0;
		EnumU5BU5D_t3205174168* L_10 = JsonWriter_GetFlagList_m4094172542(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		EnumU5BU5D_t3205174168* L_11 = V_2;
		NullCheck(L_11);
		V_3 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))));
		V_4 = 0;
		goto IL_006b;
	}

IL_003b:
	{
		StringU5BU5D_t4054002952* L_12 = V_3;
		int32_t L_13 = V_4;
		EnumU5BU5D_t3205174168* L_14 = V_2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Enum_t2862688501 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		String_t* L_18 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		ArrayElementTypeCheck (L_12, L_18);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (String_t*)L_18);
		StringU5BU5D_t4054002952* L_19 = V_3;
		int32_t L_20 = V_4;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t4054002952* L_24 = V_3;
		int32_t L_25 = V_4;
		EnumU5BU5D_t3205174168* L_26 = V_2;
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		Enum_t2862688501 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30 = Enum_ToString_m1466259273(L_29, _stringLiteral102, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		ArrayElementTypeCheck (L_24, L_30);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (String_t*)L_30);
	}

IL_0065:
	{
		int32_t L_31 = V_4;
		V_4 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_32 = V_4;
		EnumU5BU5D_t3205174168* L_33 = V_2;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		StringU5BU5D_t4054002952* L_34 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Join_m2789530325(NULL /*static, unused*/, _stringLiteral1396, L_34, /*hidden argument*/NULL);
		V_0 = L_35;
		goto IL_009b;
	}

IL_0080:
	{
		Enum_t2862688501 * L_36 = ___value0;
		String_t* L_37 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		String_t* L_38 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_009b;
		}
	}
	{
		Enum_t2862688501 * L_40 = ___value0;
		NullCheck(L_40);
		String_t* L_41 = Enum_ToString_m1466259273(L_40, _stringLiteral102, /*hidden argument*/NULL);
		V_0 = L_41;
	}

IL_009b:
	{
		String_t* L_42 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_42);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.String)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral1456037909;
extern const uint32_t JsonWriter_Write_m2464089609_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2464089609 (JsonWriter_t3589747297 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2464089609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_1, _stringLiteral3392903);
		return;
	}

IL_0014:
	{
		String_t* L_2 = ___value0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		NullCheck(L_4);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_4, ((int32_t)34));
		int32_t L_5 = V_1;
		V_2 = L_5;
		goto IL_015c;
	}

IL_0031:
	{
		String_t* L_6 = ___value0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m3015341861(L_6, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)31))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_9 = ___value0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m3015341861(L_9, L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)127))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_12 = ___value0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		Il2CppChar L_14 = String_get_Chars_m3015341861(L_12, L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)((int32_t)60))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_15 = ___value0;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m3015341861(L_15, L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)((int32_t)9))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_18 = ___value0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m3015341861(L_18, L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) == ((int32_t)((int32_t)34))))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_21 = ___value0;
		int32_t L_22 = V_2;
		NullCheck(L_21);
		Il2CppChar L_23 = String_get_Chars_m3015341861(L_21, L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0158;
		}
	}

IL_0076:
	{
		TextWriter_t2304124208 * L_24 = __this->get_Writer_0();
		String_t* L_25 = ___value0;
		int32_t L_26 = V_1;
		int32_t L_27 = V_2;
		int32_t L_28 = V_1;
		NullCheck(L_25);
		String_t* L_29 = String_Substring_m675079568(L_25, L_26, ((int32_t)((int32_t)L_27-(int32_t)L_28)), /*hidden argument*/NULL);
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_24, L_29);
		int32_t L_30 = V_2;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
		String_t* L_31 = ___value0;
		int32_t L_32 = V_2;
		NullCheck(L_31);
		Il2CppChar L_33 = String_get_Chars_m3015341861(L_31, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		Il2CppChar L_34 = V_3;
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 0)
		{
			goto IL_00e2;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 1)
		{
			goto IL_012a;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 2)
		{
			goto IL_0106;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 3)
		{
			goto IL_013c;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 4)
		{
			goto IL_00f4;
		}
		if (((int32_t)((int32_t)L_34-(int32_t)8)) == 5)
		{
			goto IL_0118;
		}
	}
	{
		Il2CppChar L_35 = V_3;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)34))))
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppChar L_36 = V_3;
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_013c;
		}
	}

IL_00c1:
	{
		TextWriter_t2304124208 * L_37 = __this->get_Writer_0();
		NullCheck(L_37);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_37, ((int32_t)92));
		TextWriter_t2304124208 * L_38 = __this->get_Writer_0();
		String_t* L_39 = ___value0;
		int32_t L_40 = V_2;
		NullCheck(L_39);
		Il2CppChar L_41 = String_get_Chars_m3015341861(L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_38, L_41);
		goto IL_0158;
	}

IL_00e2:
	{
		TextWriter_t2304124208 * L_42 = __this->get_Writer_0();
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_42, _stringLiteral2950);
		goto IL_0158;
	}

IL_00f4:
	{
		TextWriter_t2304124208 * L_43 = __this->get_Writer_0();
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_43, _stringLiteral2954);
		goto IL_0158;
	}

IL_0106:
	{
		TextWriter_t2304124208 * L_44 = __this->get_Writer_0();
		NullCheck(L_44);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_44, _stringLiteral2962);
		goto IL_0158;
	}

IL_0118:
	{
		TextWriter_t2304124208 * L_45 = __this->get_Writer_0();
		NullCheck(L_45);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_45, _stringLiteral2966);
		goto IL_0158;
	}

IL_012a:
	{
		TextWriter_t2304124208 * L_46 = __this->get_Writer_0();
		NullCheck(L_46);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_46, _stringLiteral2968);
		goto IL_0158;
	}

IL_013c:
	{
		TextWriter_t2304124208 * L_47 = __this->get_Writer_0();
		String_t* L_48 = ___value0;
		int32_t L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2862622538_il2cpp_TypeInfo_var);
		int32_t L_50 = Char_ConvertToUtf32_m1170476182(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		int32_t L_51 = L_50;
		Il2CppObject * L_52 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_51);
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_47, _stringLiteral1456037909, L_52);
	}

IL_0158:
	{
		int32_t L_53 = V_2;
		V_2 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_015c:
	{
		int32_t L_54 = V_2;
		int32_t L_55 = V_0;
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_0031;
		}
	}
	{
		TextWriter_t2304124208 * L_56 = __this->get_Writer_0();
		String_t* L_57 = ___value0;
		int32_t L_58 = V_1;
		int32_t L_59 = V_0;
		int32_t L_60 = V_1;
		NullCheck(L_57);
		String_t* L_61 = String_Substring_m675079568(L_57, L_58, ((int32_t)((int32_t)L_59-(int32_t)L_60)), /*hidden argument*/NULL);
		NullCheck(L_56);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_56, L_61);
		TextWriter_t2304124208 * L_62 = __this->get_Writer_0();
		NullCheck(L_62);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_62, ((int32_t)34));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern const uint32_t JsonWriter_Write_m3583389520_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3583389520 (JsonWriter_t3589747297 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3583389520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextWriter_t2304124208 * G_B2_0 = NULL;
	TextWriter_t2304124208 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	TextWriter_t2304124208 * G_B3_1 = NULL;
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		bool L_1 = ___value0;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral97196323;
		G_B3_1 = G_B1_0;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral3569038;
		G_B3_1 = G_B2_0;
	}

IL_0015:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, G_B3_1, G_B3_0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Byte)
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3026117682_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3026117682 (JsonWriter_t3589747297 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3026117682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		uint8_t L_1 = ___value0;
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_0, _stringLiteral115082107, L_3);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.SByte)
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m4051402109_MetadataUsageId;
extern "C"  void JsonWriter_Write_m4051402109 (JsonWriter_t3589747297 * __this, int8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m4051402109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		int8_t L_1 = ___value0;
		int8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(SByte_t1161769777_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_0, _stringLiteral115082107, L_3);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Int16)
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3805530724_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3805530724 (JsonWriter_t3589747297 * __this, int16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3805530724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		int16_t L_1 = ___value0;
		int16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int16_t1153838442_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_0, _stringLiteral115082107, L_3);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt16)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3004616955_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3004616955 (JsonWriter_t3589747297 * __this, uint16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3004616955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		uint16_t L_1 = ___value0;
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_0, _stringLiteral115082107, L_3);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Int32)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3805532522_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3805532522 (JsonWriter_t3589747297 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3805532522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		int32_t L_1 = ___value0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_0, _stringLiteral115082107, L_3);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt32)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3004618753_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3004618753 (JsonWriter_t3589747297 * __this, uint32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3004618753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3967125074(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(37 /* System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = UInt32_ToString_m1403037425((&___value0), _stringLiteral103, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_3);
		return;
	}

IL_0021:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		uint32_t L_5 = ___value0;
		uint32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_4, _stringLiteral115082107, L_7);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Int64)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3805535467_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3805535467 (JsonWriter_t3589747297 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3805535467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3836584058(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(37 /* System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = Int64_ToString_m2505400275((&___value0), _stringLiteral103, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_3);
		return;
	}

IL_0021:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		int64_t L_5 = ___value0;
		int64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_4, _stringLiteral115082107, L_7);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt64)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m3004621698_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3004621698 (JsonWriter_t3589747297 * __this, uint64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3004621698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Decimal_op_Implicit_m3967128019(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(37 /* System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = UInt64_ToString_m54951794((&___value0), _stringLiteral103, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_3);
		return;
	}

IL_0021:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		uint64_t L_5 = ___value0;
		uint64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(UInt64_t24668076_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_4, _stringLiteral115082107, L_7);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Single)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral115082448;
extern const uint32_t JsonWriter_Write_m2145413298_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2145413298 (JsonWriter_t3589747297 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2145413298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		bool L_1 = Single_IsNaN_m2036953453(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		float L_2 = ___value0;
		bool L_3 = Single_IsInfinity_m1725004900(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0010:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, _stringLiteral3392903);
		return;
	}

IL_0021:
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		float L_6 = ___value0;
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_5, _stringLiteral115082448, L_8);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Double)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral115082448;
extern const uint32_t JsonWriter_Write_m1895850569_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1895850569 (JsonWriter_t3589747297 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1895850569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___value0;
		bool L_1 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		double L_2 = ___value0;
		bool L_3 = Double_IsInfinity_m3094155474(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0010:
	{
		TextWriter_t2304124208 * L_4 = __this->get_Writer_0();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, _stringLiteral3392903);
		return;
	}

IL_0021:
	{
		TextWriter_t2304124208 * L_5 = __this->get_Writer_0();
		double L_6 = ___value0;
		double L_7 = L_6;
		Il2CppObject * L_8 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_5, _stringLiteral115082448, L_8);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Decimal)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral115082107;
extern const uint32_t JsonWriter_Write_m2142858567_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2142858567 (JsonWriter_t3589747297 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2142858567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Decimal_t1954350631  L_0 = ___value0;
		bool L_1 = VirtFuncInvoker1< bool, Decimal_t1954350631  >::Invoke(37 /* System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_0);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = Decimal_ToString_m1279024431((&___value0), _stringLiteral103, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_2);
		return;
	}

IL_001c:
	{
		TextWriter_t2304124208 * L_3 = __this->get_Writer_0();
		Decimal_t1954350631  L_4 = ___value0;
		Decimal_t1954350631  L_5 = L_4;
		Il2CppObject * L_6 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(14 /* System.Void System.IO.TextWriter::Write(System.String,System.Object) */, L_3, _stringLiteral115082107, L_6);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Char)
extern "C"  void JsonWriter_Write_m3026516900 (JsonWriter_t3589747297 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = ___value0;
		String_t* L_1 = String_CreateString_m356585284(NULL, L_0, 1, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.TimeSpan)
extern "C"  void JsonWriter_Write_m1677925091 (JsonWriter_t3589747297 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = TimeSpan_get_Ticks_m315930342((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void JsonFx.Json.JsonWriter::Write(System.Int64) */, __this, L_0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Uri)
extern "C"  void JsonWriter_Write_m1068006924 (JsonWriter_t3589747297 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method)
{
	{
		Uri_t1116831938 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Version)
extern "C"  void JsonWriter_Write_m3887240352 (JsonWriter_t3589747297 * __this, Version_t763695022 * ___value0, const MethodInfo* method)
{
	{
		Version_t763695022 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::Write(System.Xml.XmlNode)
extern "C"  void JsonWriter_Write_m2726874998 (JsonWriter_t3589747297 * __this, XmlNode_t856910923 * ___value0, const MethodInfo* method)
{
	{
		XmlNode_t856910923 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Xml.XmlNode::get_OuterXml() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3458665517_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern const uint32_t JsonWriter_WriteArray_m890754409_MetadataUsageId;
extern "C"  void JsonWriter_WriteArray_m890754409 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArray_m890754409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)91));
		int32_t L_1 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = __this->get_depth_2();
		JsonWriterSettings_t323204516 * L_3 = __this->get_settings_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_3);
		if ((((int32_t)L_2) <= ((int32_t)L_4)))
		{
			goto IL_0050;
		}
	}
	{
		JsonWriterSettings_t323204516 * L_5 = __this->get_settings_1();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_8, /*hidden argument*/NULL);
		JsonSerializationException_t3458665517 * L_10 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2046370034(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_11 = ___value0;
			NullCheck(L_11);
			Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_11);
			V_2 = L_12;
		}

IL_0057:
		try
		{ // begin try (depth: 2)
			{
				goto IL_007a;
			}

IL_0059:
			{
				Il2CppObject * L_13 = V_2;
				NullCheck(L_13);
				Il2CppObject * L_14 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_13);
				V_1 = L_14;
				bool L_15 = V_0;
				if (!L_15)
				{
					goto IL_006b;
				}
			}

IL_0063:
			{
				VirtActionInvoker0::Invoke(34 /* System.Void JsonFx.Json.JsonWriter::WriteArrayItemDelim() */, __this);
				goto IL_006d;
			}

IL_006b:
			{
				V_0 = (bool)1;
			}

IL_006d:
			{
				VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
				Il2CppObject * L_16 = V_1;
				VirtActionInvoker1< Il2CppObject * >::Invoke(28 /* System.Void JsonFx.Json.JsonWriter::WriteArrayItem(System.Object) */, __this, L_16);
			}

IL_007a:
			{
				Il2CppObject * L_17 = V_2;
				NullCheck(L_17);
				bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_17);
				if (L_18)
				{
					goto IL_0059;
				}
			}

IL_0082:
			{
				IL2CPP_LEAVE(0x95, FINALLY_0084);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0084;
		}

FINALLY_0084:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_19 = V_2;
				V_3 = ((Il2CppObject *)IsInst(L_19, IDisposable_t1423340799_il2cpp_TypeInfo_var));
				Il2CppObject * L_20 = V_3;
				if (!L_20)
				{
					goto IL_0094;
				}
			}

IL_008e:
			{
				Il2CppObject * L_21 = V_3;
				NullCheck(L_21);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_21);
			}

IL_0094:
			{
				IL2CPP_END_FINALLY(132)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(132)
		{
			IL2CPP_JUMP_TBL(0x95, IL_0095)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_0095:
		{
			IL2CPP_LEAVE(0xA6, FINALLY_0097);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0097;
	}

FINALLY_0097:
	{ // begin finally (depth: 1)
		int32_t L_22 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_22-(int32_t)1)));
		IL2CPP_END_FINALLY(151)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(151)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a6:
	{
		bool L_23 = V_0;
		if (!L_23)
		{
			goto IL_00af;
		}
	}
	{
		VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
	}

IL_00af:
	{
		TextWriter_t2304124208 * L_24 = __this->get_Writer_0();
		NullCheck(L_24);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_24, ((int32_t)93));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteArrayItem(System.Object)
extern "C"  void JsonWriter_WriteArrayItem_m3627136607 (JsonWriter_t3589747297 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObject(System.Collections.IDictionary)
extern "C"  void JsonWriter_WriteObject_m2746452913 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker1< Il2CppObject * >::Invoke(30 /* System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, L_0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3458665517_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral649209863;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern const uint32_t JsonWriter_WriteDictionary_m4272317462_MetadataUsageId;
extern "C"  void JsonWriter_WriteDictionary_m4272317462 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteDictionary_m4272317462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	DictionaryEntry_t1751606614  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DictionaryEntry_t1751606614  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_0);
		V_0 = ((Il2CppObject *)IsInst(L_1, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var));
		Il2CppObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_3 = ___value0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral649209863, L_4, /*hidden argument*/NULL);
		JsonSerializationException_t3458665517 * L_6 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2046370034(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0025:
	{
		V_1 = (bool)0;
		TextWriter_t2304124208 * L_7 = __this->get_Writer_0();
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_7, ((int32_t)123));
		int32_t L_8 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = __this->get_depth_2();
		JsonWriterSettings_t323204516 * L_10 = __this->get_settings_1();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_10);
		if ((((int32_t)L_9) <= ((int32_t)L_11)))
		{
			goto IL_0075;
		}
	}
	{
		JsonWriterSettings_t323204516 * L_12 = __this->get_settings_1();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_12);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_15, /*hidden argument*/NULL);
		JsonSerializationException_t3458665517 * L_17 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2046370034(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0075:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ab;
		}

IL_0077:
		{
			bool L_18 = V_1;
			if (!L_18)
			{
				goto IL_0082;
			}
		}

IL_007a:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_0084;
		}

IL_0082:
		{
			V_1 = (bool)1;
		}

IL_0084:
		{
			Il2CppObject * L_19 = V_0;
			NullCheck(L_19);
			DictionaryEntry_t1751606614  L_20 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, L_19);
			V_2 = L_20;
			Il2CppObject * L_21 = DictionaryEntry_get_Key_m3516209325((&V_2), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
			String_t* L_22 = Convert_ToString_m427788191(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			Il2CppObject * L_23 = V_0;
			NullCheck(L_23);
			DictionaryEntry_t1751606614  L_24 = InterfaceFuncInvoker0< DictionaryEntry_t1751606614  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t951828701_il2cpp_TypeInfo_var, L_23);
			V_3 = L_24;
			Il2CppObject * L_25 = DictionaryEntry_get_Value_m4281303039((&V_3), /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m2713404739(__this, L_22, L_25, /*hidden argument*/NULL);
		}

IL_00ab:
		{
			Il2CppObject * L_26 = V_0;
			NullCheck(L_26);
			bool L_27 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_26);
			if (L_27)
			{
				goto IL_0077;
			}
		}

IL_00b3:
		{
			IL2CPP_LEAVE(0xC4, FINALLY_00b5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00b5;
	}

FINALLY_00b5:
	{ // begin finally (depth: 1)
		int32_t L_28 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_28-(int32_t)1)));
		IL2CPP_END_FINALLY(181)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(181)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00c4:
	{
		bool L_29 = V_1;
		if (!L_29)
		{
			goto IL_00cd;
		}
	}
	{
		VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
	}

IL_00cd:
	{
		TextWriter_t2304124208 * L_30 = __this->get_Writer_0();
		NullCheck(L_30);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_30, ((int32_t)125));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern "C"  void JsonWriter_WriteObjectProperty_m2713404739 (JsonWriter_t3589747297 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
		String_t* L_0 = ___key0;
		VirtActionInvoker1< String_t* >::Invoke(31 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyName(System.String) */, __this, L_0);
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)58));
		Il2CppObject * L_2 = ___value1;
		VirtActionInvoker1< Il2CppObject * >::Invoke(32 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyValue(System.Object) */, __this, L_2);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyName(System.String)
extern "C"  void JsonWriter_WriteObjectPropertyName_m1558042410 (JsonWriter_t3589747297 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void JsonFx.Json.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyValue(System.Object)
extern "C"  void JsonWriter_WriteObjectPropertyValue_m1110441390 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)1);
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObject(System.Object,System.Type)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3458665517_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral107111942;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral380572579;
extern const uint32_t JsonWriter_WriteObject_m3861055119_MetadataUsageId;
extern "C"  void JsonWriter_WriteObject_m3861055119 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObject_m3861055119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	PropertyInfoU5BU5D_t4286713048* V_2 = NULL;
	PropertyInfo_t * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* V_5 = NULL;
	FieldInfoU5BU5D_t2567562023* V_6 = NULL;
	FieldInfo_t * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	String_t* V_9 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_10 = NULL;
	int32_t V_11 = 0;
	FieldInfoU5BU5D_t2567562023* V_12 = NULL;
	int32_t V_13 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B10_0 = 0;
	{
		V_0 = (bool)0;
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)123));
		int32_t L_1 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = __this->get_depth_2();
		JsonWriterSettings_t323204516 * L_3 = __this->get_settings_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_3);
		if ((((int32_t)L_2) <= ((int32_t)L_4)))
		{
			goto IL_0050;
		}
	}
	{
		JsonWriterSettings_t323204516 * L_5 = __this->get_settings_1();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth() */, L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral107111942, L_8, /*hidden argument*/NULL);
		JsonSerializationException_t3458665517 * L_10 = (JsonSerializationException_t3458665517 *)il2cpp_codegen_object_new(JsonSerializationException_t3458665517_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2046370034(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			JsonWriterSettings_t323204516 * L_11 = __this->get_settings_1();
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName() */, L_11);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_0062:
		{
			bool L_14 = V_0;
			if (!L_14)
			{
				goto IL_006d;
			}
		}

IL_0065:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_006f;
		}

IL_006d:
		{
			V_0 = (bool)1;
		}

IL_006f:
		{
			JsonWriterSettings_t323204516 * L_15 = __this->get_settings_1();
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName() */, L_15);
			Type_t * L_17 = ___type1;
			NullCheck(L_17);
			String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_17);
			Type_t * L_19 = ___type1;
			NullCheck(L_19);
			Assembly_t1418687608 * L_20 = VirtFuncInvoker0< Assembly_t1418687608 * >::Invoke(15 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_19);
			NullCheck(L_20);
			AssemblyName_t2915647011 * L_21 = VirtFuncInvoker0< AssemblyName_t2915647011 * >::Invoke(18 /* System.Reflection.AssemblyName System.Reflection.Assembly::GetName() */, L_20);
			NullCheck(L_21);
			String_t* L_22 = AssemblyName_get_Name_m1123490526(L_21, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_23 = String_Concat_m1825781833(NULL /*static, unused*/, L_18, _stringLiteral1396, L_22, /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m2713404739(__this, L_16, L_23, /*hidden argument*/NULL);
		}

IL_00a0:
		{
			Type_t * L_24 = ___type1;
			NullCheck(L_24);
			bool L_25 = VirtFuncInvoker0< bool >::Invoke(94 /* System.Boolean System.Type::get_IsGenericType() */, L_24);
			if (!L_25)
			{
				goto IL_00ba;
			}
		}

IL_00a8:
		{
			Type_t * L_26 = ___type1;
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_26);
			NullCheck(L_27);
			bool L_28 = String_StartsWith_m1500793453(L_27, _stringLiteral380572579, /*hidden argument*/NULL);
			G_B10_0 = ((int32_t)(L_28));
			goto IL_00bb;
		}

IL_00ba:
		{
			G_B10_0 = 0;
		}

IL_00bb:
		{
			V_1 = (bool)G_B10_0;
			Type_t * L_29 = ___type1;
			NullCheck(L_29);
			PropertyInfoU5BU5D_t4286713048* L_30 = Type_GetProperties_m3853308260(L_29, /*hidden argument*/NULL);
			V_2 = L_30;
			PropertyInfoU5BU5D_t4286713048* L_31 = V_2;
			V_10 = L_31;
			V_11 = 0;
			goto IL_013a;
		}

IL_00cb:
		{
			PropertyInfoU5BU5D_t4286713048* L_32 = V_10;
			int32_t L_33 = V_11;
			NullCheck(L_32);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
			int32_t L_34 = L_33;
			PropertyInfo_t * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
			V_3 = L_35;
			PropertyInfo_t * L_36 = V_3;
			NullCheck(L_36);
			bool L_37 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_36);
			if (!L_37)
			{
				goto IL_0134;
			}
		}

IL_00d9:
		{
			PropertyInfo_t * L_38 = V_3;
			NullCheck(L_38);
			bool L_39 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_38);
			if (L_39)
			{
				goto IL_00e4;
			}
		}

IL_00e1:
		{
			bool L_40 = V_1;
			if (!L_40)
			{
				goto IL_0134;
			}
		}

IL_00e4:
		{
			Type_t * L_41 = ___type1;
			PropertyInfo_t * L_42 = V_3;
			Il2CppObject * L_43 = ___value0;
			bool L_44 = JsonWriter_IsIgnored_m1964658177(__this, L_41, L_42, L_43, /*hidden argument*/NULL);
			if (L_44)
			{
				goto IL_0134;
			}
		}

IL_00ef:
		{
			PropertyInfo_t * L_45 = V_3;
			Il2CppObject * L_46 = ___value0;
			NullCheck(L_45);
			Il2CppObject * L_47 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(25 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_45, L_46, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
			V_4 = L_47;
			PropertyInfo_t * L_48 = V_3;
			Il2CppObject * L_49 = V_4;
			bool L_50 = JsonWriter_IsDefaultValue_m135619988(__this, L_48, L_49, /*hidden argument*/NULL);
			if (L_50)
			{
				goto IL_0134;
			}
		}

IL_0104:
		{
			bool L_51 = V_0;
			if (!L_51)
			{
				goto IL_010f;
			}
		}

IL_0107:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_0111;
		}

IL_010f:
		{
			V_0 = (bool)1;
		}

IL_0111:
		{
			PropertyInfo_t * L_52 = V_3;
			String_t* L_53 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
			V_5 = L_53;
			String_t* L_54 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_55 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
			if (!L_55)
			{
				goto IL_012a;
			}
		}

IL_0122:
		{
			PropertyInfo_t * L_56 = V_3;
			NullCheck(L_56);
			String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
			V_5 = L_57;
		}

IL_012a:
		{
			String_t* L_58 = V_5;
			Il2CppObject * L_59 = V_4;
			JsonWriter_WriteObjectProperty_m2713404739(__this, L_58, L_59, /*hidden argument*/NULL);
		}

IL_0134:
		{
			int32_t L_60 = V_11;
			V_11 = ((int32_t)((int32_t)L_60+(int32_t)1));
		}

IL_013a:
		{
			int32_t L_61 = V_11;
			PropertyInfoU5BU5D_t4286713048* L_62 = V_10;
			NullCheck(L_62);
			if ((((int32_t)L_61) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length)))))))
			{
				goto IL_00cb;
			}
		}

IL_0142:
		{
			Type_t * L_63 = ___type1;
			NullCheck(L_63);
			FieldInfoU5BU5D_t2567562023* L_64 = Type_GetFields_m3137302773(L_63, /*hidden argument*/NULL);
			V_6 = L_64;
			FieldInfoU5BU5D_t2567562023* L_65 = V_6;
			V_12 = L_65;
			V_13 = 0;
			goto IL_01cc;
		}

IL_0153:
		{
			FieldInfoU5BU5D_t2567562023* L_66 = V_12;
			int32_t L_67 = V_13;
			NullCheck(L_66);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
			int32_t L_68 = L_67;
			FieldInfo_t * L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
			V_7 = L_69;
			FieldInfo_t * L_70 = V_7;
			NullCheck(L_70);
			bool L_71 = FieldInfo_get_IsPublic_m2574(L_70, /*hidden argument*/NULL);
			if (!L_71)
			{
				goto IL_01c6;
			}
		}

IL_0163:
		{
			FieldInfo_t * L_72 = V_7;
			NullCheck(L_72);
			bool L_73 = FieldInfo_get_IsStatic_m24721619(L_72, /*hidden argument*/NULL);
			if (L_73)
			{
				goto IL_01c6;
			}
		}

IL_016c:
		{
			Type_t * L_74 = ___type1;
			FieldInfo_t * L_75 = V_7;
			Il2CppObject * L_76 = ___value0;
			bool L_77 = JsonWriter_IsIgnored_m1964658177(__this, L_74, L_75, L_76, /*hidden argument*/NULL);
			if (L_77)
			{
				goto IL_01c6;
			}
		}

IL_0178:
		{
			FieldInfo_t * L_78 = V_7;
			Il2CppObject * L_79 = ___value0;
			NullCheck(L_78);
			Il2CppObject * L_80 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_78, L_79);
			V_8 = L_80;
			FieldInfo_t * L_81 = V_7;
			Il2CppObject * L_82 = V_8;
			bool L_83 = JsonWriter_IsDefaultValue_m135619988(__this, L_81, L_82, /*hidden argument*/NULL);
			if (L_83)
			{
				goto IL_01c6;
			}
		}

IL_018e:
		{
			bool L_84 = V_0;
			if (!L_84)
			{
				goto IL_019f;
			}
		}

IL_0191:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim() */, __this);
			VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
			goto IL_01a1;
		}

IL_019f:
		{
			V_0 = (bool)1;
		}

IL_01a1:
		{
			FieldInfo_t * L_85 = V_7;
			String_t* L_86 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
			V_9 = L_86;
			String_t* L_87 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_88 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
			if (!L_88)
			{
				goto IL_01bc;
			}
		}

IL_01b3:
		{
			FieldInfo_t * L_89 = V_7;
			NullCheck(L_89);
			String_t* L_90 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_89);
			V_9 = L_90;
		}

IL_01bc:
		{
			String_t* L_91 = V_9;
			Il2CppObject * L_92 = V_8;
			JsonWriter_WriteObjectProperty_m2713404739(__this, L_91, L_92, /*hidden argument*/NULL);
		}

IL_01c6:
		{
			int32_t L_93 = V_13;
			V_13 = ((int32_t)((int32_t)L_93+(int32_t)1));
		}

IL_01cc:
		{
			int32_t L_94 = V_13;
			FieldInfoU5BU5D_t2567562023* L_95 = V_12;
			NullCheck(L_95);
			if ((((int32_t)L_94) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_95)->max_length)))))))
			{
				goto IL_0153;
			}
		}

IL_01d7:
		{
			IL2CPP_LEAVE(0x1E8, FINALLY_01d9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_01d9;
	}

FINALLY_01d9:
	{ // begin finally (depth: 1)
		int32_t L_96 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_96-(int32_t)1)));
		IL2CPP_END_FINALLY(473)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(473)
	{
		IL2CPP_JUMP_TBL(0x1E8, IL_01e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_01e8:
	{
		bool L_97 = V_0;
		if (!L_97)
		{
			goto IL_01f1;
		}
	}
	{
		VirtActionInvoker0::Invoke(36 /* System.Void JsonFx.Json.JsonWriter::WriteLine() */, __this);
	}

IL_01f1:
	{
		TextWriter_t2304124208 * L_98 = __this->get_Writer_0();
		NullCheck(L_98);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_98, ((int32_t)125));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteArrayItemDelim()
extern "C"  void JsonWriter_WriteArrayItemDelim_m2976019388 (JsonWriter_t3589747297 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim()
extern "C"  void JsonWriter_WriteObjectPropertyDelim_m1543176996 (JsonWriter_t3589747297 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void JsonFx.Json.JsonWriter::WriteLine()
extern "C"  void JsonWriter_WriteLine_m2362711213 (JsonWriter_t3589747297 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		JsonWriterSettings_t323204516 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint() */, L_0);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		TextWriter_t2304124208 * L_2 = __this->get_Writer_0();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(16 /* System.Void System.IO.TextWriter::WriteLine() */, L_2);
		V_0 = 0;
		goto IL_0037;
	}

IL_001d:
	{
		TextWriter_t2304124208 * L_3 = __this->get_Writer_0();
		JsonWriterSettings_t323204516 * L_4 = __this->get_settings_1();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String JsonFx.Json.JsonWriterSettings::get_Tab() */, L_4);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.String) */, L_3, L_5);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_depth_2();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}
}
// System.Boolean JsonFx.Json.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral309353872;
extern const uint32_t JsonWriter_IsIgnored_m1964658177_MetadataUsageId;
extern "C"  bool JsonWriter_IsIgnored_m1964658177 (JsonWriter_t3589747297 * __this, Type_t * ___objType0, MemberInfo_t * ___member1, Il2CppObject * ___obj2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsIgnored_m1964658177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	PropertyInfo_t * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		MemberInfo_t * L_0 = ___member1;
		bool L_1 = JsonIgnoreAttribute_IsJsonIgnore_m4133572969(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)1;
	}

IL_000a:
	{
		MemberInfo_t * L_2 = ___member1;
		String_t* L_3 = JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m751699402(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_6 = ___objType0;
		String_t* L_7 = V_0;
		NullCheck(L_6);
		PropertyInfo_t * L_8 = Type_GetProperty_m1904930970(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		PropertyInfo_t * L_9 = V_1;
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PropertyInfo_t * L_10 = V_1;
		Il2CppObject * L_11 = ___obj2;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(25 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_10, L_11, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		V_2 = L_12;
		Il2CppObject * L_13 = V_2;
		if (!((Il2CppObject *)IsInstSealed(L_13, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_003f;
		}
	}
	{
		Il2CppObject * L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		bool L_15 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_003f;
		}
	}
	{
		return (bool)1;
	}

IL_003f:
	{
		JsonWriterSettings_t323204516 * L_16 = __this->get_settings_1();
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean JsonFx.Json.JsonWriterSettings::get_UseXmlSerializationAttributes() */, L_16);
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		MemberInfo_t * L_18 = ___member1;
		bool L_19 = JsonIgnoreAttribute_IsXmlIgnore_m816865530(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0056;
		}
	}
	{
		return (bool)1;
	}

IL_0056:
	{
		Type_t * L_20 = ___objType0;
		MemberInfo_t * L_21 = ___member1;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, L_22, _stringLiteral309353872, /*hidden argument*/NULL);
		NullCheck(L_20);
		PropertyInfo_t * L_24 = Type_GetProperty_m1904930970(L_20, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		PropertyInfo_t * L_25 = V_3;
		if (!L_25)
		{
			goto IL_008e;
		}
	}
	{
		PropertyInfo_t * L_26 = V_3;
		Il2CppObject * L_27 = ___obj2;
		NullCheck(L_26);
		Il2CppObject * L_28 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(25 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_26, L_27, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		V_4 = L_28;
		Il2CppObject * L_29 = V_4;
		if (!((Il2CppObject *)IsInstSealed(L_29, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_30 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		bool L_31 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_008e;
		}
	}
	{
		return (bool)1;
	}

IL_008e:
	{
		return (bool)0;
	}
}
// System.Boolean JsonFx.Json.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern const Il2CppType* DefaultValueAttribute_t3756983154_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DefaultValueAttribute_t3756983154_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_IsDefaultValue_m135619988_MetadataUsageId;
extern "C"  bool JsonWriter_IsDefaultValue_m135619988 (JsonWriter_t3589747297 * __this, MemberInfo_t * ___member0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsDefaultValue_m135619988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DefaultValueAttribute_t3756983154 * V_0 = NULL;
	{
		MemberInfo_t * L_0 = ___member0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DefaultValueAttribute_t3756983154_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t2523058482 * L_2 = Attribute_GetCustomAttribute_m506754809(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((DefaultValueAttribute_t3756983154 *)IsInstClass(L_2, DefaultValueAttribute_t3756983154_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t3756983154 * L_3 = V_0;
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		DefaultValueAttribute_t3756983154 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_4);
		if (L_5)
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_6 = ___value1;
		return (bool)((((Il2CppObject*)(Il2CppObject *)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0028:
	{
		DefaultValueAttribute_t3756983154 * L_7 = V_0;
		NullCheck(L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_7);
		Il2CppObject * L_9 = ___value1;
		NullCheck(L_8);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_8, L_9);
		return L_10;
	}
}
// System.Enum[] JsonFx.Json.JsonWriter::GetFlagList(System.Type,System.Object)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4230874053_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3059274558_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3158780509_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m287651073_MethodInfo_var;
extern const uint32_t JsonWriter_GetFlagList_m4094172542_MetadataUsageId;
extern "C"  EnumU5BU5D_t3205174168* JsonWriter_GetFlagList_m4094172542 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_GetFlagList_m4094172542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppArray * V_1 = NULL;
	List_1_t4230874053 * V_2 = NULL;
	int32_t V_3 = 0;
	uint64_t V_4 = 0;
	{
		Il2CppObject * L_0 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m2531670463(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppArray * L_3 = Enum_GetValues_m1513312286(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppArray * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = Array_get_Length_m1203127607(L_4, /*hidden argument*/NULL);
		List_1_t4230874053 * L_6 = (List_1_t4230874053 *)il2cpp_codegen_object_new(List_1_t4230874053_il2cpp_TypeInfo_var);
		List_1__ctor_m3059274558(L_6, L_5, /*hidden argument*/List_1__ctor_m3059274558_MethodInfo_var);
		V_2 = L_6;
		uint64_t L_7 = V_0;
		if ((!(((uint64_t)L_7) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t4230874053 * L_8 = V_2;
		Il2CppObject * L_9 = ___value1;
		Type_t * L_10 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppObject * L_11 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_Add_m3158780509(L_8, ((Enum_t2862688501 *)CastclassClass(L_11, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m3158780509_MethodInfo_var);
		List_1_t4230874053 * L_12 = V_2;
		NullCheck(L_12);
		EnumU5BU5D_t3205174168* L_13 = List_1_ToArray_m287651073(L_12, /*hidden argument*/List_1_ToArray_m287651073_MethodInfo_var);
		return L_13;
	}

IL_0038:
	{
		Il2CppArray * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Array_get_Length_m1203127607(L_14, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_15-(int32_t)1));
		goto IL_007d;
	}

IL_0043:
	{
		Il2CppArray * L_16 = V_1;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		Il2CppObject * L_18 = Array_GetValue_m244209261(L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_19 = Convert_ToUInt64_m2531670463(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_3;
		if (L_20)
		{
			goto IL_005a;
		}
	}
	{
		uint64_t L_21 = V_4;
		if ((((int64_t)L_21) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0079;
		}
	}

IL_005a:
	{
		uint64_t L_22 = V_0;
		uint64_t L_23 = V_4;
		uint64_t L_24 = V_4;
		if ((!(((uint64_t)((int64_t)((int64_t)L_22&(int64_t)L_23))) == ((uint64_t)L_24))))
		{
			goto IL_0079;
		}
	}
	{
		uint64_t L_25 = V_0;
		uint64_t L_26 = V_4;
		V_0 = ((int64_t)((int64_t)L_25-(int64_t)L_26));
		List_1_t4230874053 * L_27 = V_2;
		Il2CppArray * L_28 = V_1;
		int32_t L_29 = V_3;
		NullCheck(L_28);
		Il2CppObject * L_30 = Array_GetValue_m244209261(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_m3158780509(L_27, ((Enum_t2862688501 *)IsInstClass(L_30, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m3158780509_MethodInfo_var);
	}

IL_0079:
	{
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31-(int32_t)1));
	}

IL_007d:
	{
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) >= ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		uint64_t L_33 = V_0;
		if ((((int64_t)L_33) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0098;
		}
	}
	{
		List_1_t4230874053 * L_34 = V_2;
		Type_t * L_35 = ___enumType0;
		uint64_t L_36 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_37 = Enum_ToObject_m1448258009(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		List_1_Add_m3158780509(L_34, ((Enum_t2862688501 *)IsInstClass(L_37, Enum_t2862688501_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m3158780509_MethodInfo_var);
	}

IL_0098:
	{
		List_1_t4230874053 * L_38 = V_2;
		NullCheck(L_38);
		EnumU5BU5D_t3205174168* L_39 = List_1_ToArray_m287651073(L_38, /*hidden argument*/List_1_ToArray_m287651073_MethodInfo_var);
		return L_39;
	}
}
// System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_InvalidIeee754_m1092164553_MetadataUsageId;
extern "C"  bool JsonWriter_InvalidIeee754_m1092164553 (JsonWriter_t3589747297 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_InvalidIeee754_m1092164553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Decimal_t1954350631  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1954350631_il2cpp_TypeInfo_var);
		double L_1 = Decimal_op_Explicit_m2624557563(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Decimal_t1954350631  L_2 = Decimal_op_Explicit_m1230483563(NULL /*static, unused*/, (((double)((double)(((double)((double)L_1)))))), /*hidden argument*/NULL);
		Decimal_t1954350631  L_3 = ___value0;
		bool L_4 = Decimal_op_Inequality_m4269318701(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Object)
		V_0 = (bool)1;
		goto IL_001b;
	} // end catch (depth: 1)

IL_001b:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void JsonFx.Json.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m2408474819 (JsonWriter_t3589747297 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_Writer_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		TextWriter_t2304124208 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		TextWriter_Dispose_m183705414(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName()
extern "C"  String_t* JsonWriterSettings_get_TypeHintName_m1522970531 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_typeHintName_5();
		return L_0;
	}
}
// System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint()
extern "C"  bool JsonWriterSettings_get_PrettyPrint_m2574152245 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_prettyPrint_3();
		return L_0;
	}
}
// System.String JsonFx.Json.JsonWriterSettings::get_Tab()
extern "C"  String_t* JsonWriterSettings_get_Tab_m1988222976 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_tab_4();
		return L_0;
	}
}
// System.String JsonFx.Json.JsonWriterSettings::get_NewLine()
extern "C"  String_t* JsonWriterSettings_get_NewLine_m1619438943 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_newLine_2();
		return L_0;
	}
}
// System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth()
extern "C"  int32_t JsonWriterSettings_get_MaxDepth_m4039989341 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_maxDepth_1();
		return L_0;
	}
}
// System.Boolean JsonFx.Json.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern "C"  bool JsonWriterSettings_get_UseXmlSerializationAttributes_m2086015829 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useXmlSerializationAttributes_6();
		return L_0;
	}
}
// JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer()
extern "C"  WriteDelegate_1_t3884844371 * JsonWriterSettings_get_DateTimeSerializer_m909902429 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	{
		WriteDelegate_1_t3884844371 * L_0 = __this->get_dateTimeSerializer_0();
		return L_0;
	}
}
// System.Void JsonFx.Json.JsonWriterSettings::.ctor()
extern Il2CppCodeGenString* _stringLiteral9;
extern const uint32_t JsonWriterSettings__ctor_m950536825_MetadataUsageId;
extern "C"  void JsonWriterSettings__ctor_m950536825 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings__ctor_m950536825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_maxDepth_1(((int32_t)25));
		String_t* L_0 = Environment_get_NewLine_m1034655108(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_newLine_2(L_0);
		__this->set_tab_4(_stringLiteral9);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> JsonFx.Json.TypeCoercionUtility::get_MemberMapCache()
extern Il2CppClass* Dictionary_2_t626389457_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1209065284_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_get_MemberMapCache_m2283621214_MetadataUsageId;
extern "C"  Dictionary_2_t626389457 * TypeCoercionUtility_get_MemberMapCache_m2283621214 (TypeCoercionUtility_t700629014 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_get_MemberMapCache_m2283621214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t626389457 * L_0 = __this->get_memberMapCache_0();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Dictionary_2_t626389457 * L_1 = (Dictionary_2_t626389457 *)il2cpp_codegen_object_new(Dictionary_2_t626389457_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1209065284(L_1, /*hidden argument*/Dictionary_2__ctor_m1209065284_MethodInfo_var);
		__this->set_memberMapCache_0(L_1);
	}

IL_0013:
	{
		Dictionary_2_t626389457 * L_2 = __this->get_memberMapCache_0();
		return L_2;
	}
}
// System.Object JsonFx.Json.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_ProcessTypeHint_m4089338635_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_ProcessTypeHint_m4089338635 (TypeCoercionUtility_t700629014 * __this, Il2CppObject * ___result0, String_t* ___typeInfo1, Type_t ** ___objectType2, Dictionary_2_t520966972 ** ___memberMap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_ProcessTypeHint_m4089338635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		String_t* L_0 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		Type_t ** L_2 = ___objectType2;
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)NULL);
		Dictionary_2_t520966972 ** L_3 = ___memberMap3;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)NULL);
		Il2CppObject * L_4 = ___result0;
		return L_4;
	}

IL_0011:
	{
		String_t* L_5 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3430407454, L_5, (bool)0, "JsonFx.Json, Version=1.4.1003.3007, Culture=neutral, PublicKeyToken=315052dd637f8a52");
		V_0 = L_6;
		Type_t * L_7 = V_0;
		if (L_7)
		{
			goto IL_0025;
		}
	}
	{
		Type_t ** L_8 = ___objectType2;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)NULL);
		Dictionary_2_t520966972 ** L_9 = ___memberMap3;
		*((Il2CppObject **)(L_9)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_9), (Il2CppObject *)NULL);
		Il2CppObject * L_10 = ___result0;
		return L_10;
	}

IL_0025:
	{
		Type_t ** L_11 = ___objectType2;
		Type_t * L_12 = V_0;
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)L_12);
		Type_t * L_13 = V_0;
		Il2CppObject * L_14 = ___result0;
		Dictionary_2_t520966972 ** L_15 = ___memberMap3;
		Il2CppObject * L_16 = TypeCoercionUtility_CoerceType_m3471229122(__this, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Object JsonFx.Json.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern const Il2CppType* IDictionary_t537317817_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t3880899288_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1703456824;
extern Il2CppCodeGenString* _stringLiteral1496450899;
extern Il2CppCodeGenString* _stringLiteral4160581905;
extern const uint32_t TypeCoercionUtility_InstantiateObject_m3710207535_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_InstantiateObject_m3710207535 (TypeCoercionUtility_t700629014 * __this, Type_t * ___objectType0, Dictionary_2_t520966972 ** ___memberMap1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_InstantiateObject_m3710207535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfo_t4136801618 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	TargetInvocationException_t3880899288 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___objectType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsInterface_m996103649(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_2 = ___objectType0;
		NullCheck(L_2);
		bool L_3 = Type_get_IsAbstract_m2161724892(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		Type_t * L_4 = ___objectType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsValueType_m1914757235(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}

IL_0018:
	{
		Type_t * L_6 = ___objectType0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1703456824, L_7, /*hidden argument*/NULL);
		JsonTypeCoercionException_t4269543025 * L_9 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m3517389954(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_002e:
	{
		Type_t * L_10 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t3339007067* L_11 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		NullCheck(L_10);
		ConstructorInfo_t4136801618 * L_12 = Type_GetConstructor_m2586438681(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		ConstructorInfo_t4136801618 * L_13 = V_0;
		if (L_13)
		{
			goto IL_0053;
		}
	}
	{
		Type_t * L_14 = ___objectType0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1496450899, L_15, /*hidden argument*/NULL);
		JsonTypeCoercionException_t4269543025 * L_17 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m3517389954(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t4136801618 * L_18 = V_0;
		NullCheck(L_18);
		Il2CppObject * L_19 = ConstructorInfo_Invoke_m759007899(L_18, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_0094;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005d;
		throw e;
	}

CATCH_005d:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_2 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_20 = V_2;
			NullCheck(L_20);
			Exception_t3991598821 * L_21 = Exception_get_InnerException_m1427945535(L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_007d;
			}
		}

IL_0066:
		{
			TargetInvocationException_t3880899288 * L_22 = V_2;
			NullCheck(L_22);
			Exception_t3991598821 * L_23 = Exception_get_InnerException_m1427945535(L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
			TargetInvocationException_t3880899288 * L_25 = V_2;
			NullCheck(L_25);
			Exception_t3991598821 * L_26 = Exception_get_InnerException_m1427945535(L_25, /*hidden argument*/NULL);
			JsonTypeCoercionException_t4269543025 * L_27 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_27, L_24, L_26, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
		}

IL_007d:
		{
			Type_t * L_28 = ___objectType0;
			NullCheck(L_28);
			String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_28);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral4160581905, L_29, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_31 = V_2;
			JsonTypeCoercionException_t4269543025 * L_32 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_32, L_30, L_31, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
		}
	} // end catch (depth: 1)

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IDictionary_t537317817_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_34 = ___objectType0;
		NullCheck(L_33);
		bool L_35 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_33, L_34);
		if (!L_35)
		{
			goto IL_00ab;
		}
	}
	{
		Dictionary_2_t520966972 ** L_36 = ___memberMap1;
		*((Il2CppObject **)(L_36)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_36), (Il2CppObject *)NULL);
		goto IL_00b4;
	}

IL_00ab:
	{
		Dictionary_2_t520966972 ** L_37 = ___memberMap1;
		Type_t * L_38 = ___objectType0;
		Dictionary_2_t520966972 * L_39 = TypeCoercionUtility_CreateMemberMap_m983839384(__this, L_38, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_37)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_37), (Il2CppObject *)L_39);
	}

IL_00b4:
	{
		Il2CppObject * L_40 = V_1;
		return L_40;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> JsonFx.Json.TypeCoercionUtility::CreateMemberMap(System.Type)
extern Il2CppClass* Dictionary_2_t520966972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m4076445699_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3006860022_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1504882060_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2214620925_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3738003637_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_CreateMemberMap_m983839384_MetadataUsageId;
extern "C"  Dictionary_2_t520966972 * TypeCoercionUtility_CreateMemberMap_m983839384 (TypeCoercionUtility_t700629014 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CreateMemberMap_m983839384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t520966972 * V_0 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_1 = NULL;
	PropertyInfo_t * V_2 = NULL;
	String_t* V_3 = NULL;
	FieldInfoU5BU5D_t2567562023* V_4 = NULL;
	FieldInfo_t * V_5 = NULL;
	String_t* V_6 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_7 = NULL;
	int32_t V_8 = 0;
	FieldInfoU5BU5D_t2567562023* V_9 = NULL;
	int32_t V_10 = 0;
	{
		Dictionary_2_t626389457 * L_0 = TypeCoercionUtility_get_MemberMapCache_m2283621214(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m4076445699(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m4076445699_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Dictionary_2_t626389457 * L_3 = TypeCoercionUtility_get_MemberMapCache_m2283621214(__this, /*hidden argument*/NULL);
		Type_t * L_4 = ___objectType0;
		NullCheck(L_3);
		Dictionary_2_t520966972 * L_5 = Dictionary_2_get_Item_m3006860022(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m3006860022_MethodInfo_var);
		return L_5;
	}

IL_001b:
	{
		Dictionary_2_t520966972 * L_6 = (Dictionary_2_t520966972 *)il2cpp_codegen_object_new(Dictionary_2_t520966972_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1504882060(L_6, /*hidden argument*/Dictionary_2__ctor_m1504882060_MethodInfo_var);
		V_0 = L_6;
		Type_t * L_7 = ___objectType0;
		NullCheck(L_7);
		PropertyInfoU5BU5D_t4286713048* L_8 = Type_GetProperties_m3853308260(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		PropertyInfoU5BU5D_t4286713048* L_9 = V_1;
		V_7 = L_9;
		V_8 = 0;
		goto IL_007a;
	}

IL_0030:
	{
		PropertyInfoU5BU5D_t4286713048* L_10 = V_7;
		int32_t L_11 = V_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		PropertyInfo_t * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_2 = L_13;
		PropertyInfo_t * L_14 = V_2;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_14);
		if (!L_15)
		{
			goto IL_0074;
		}
	}
	{
		PropertyInfo_t * L_16 = V_2;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_16);
		if (!L_17)
		{
			goto IL_0074;
		}
	}
	{
		PropertyInfo_t * L_18 = V_2;
		bool L_19 = JsonIgnoreAttribute_IsJsonIgnore_m4133572969(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0074;
		}
	}
	{
		PropertyInfo_t * L_20 = V_2;
		String_t* L_21 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		String_t* L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_006c;
		}
	}
	{
		Dictionary_2_t520966972 * L_24 = V_0;
		PropertyInfo_t * L_25 = V_2;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_25);
		PropertyInfo_t * L_27 = V_2;
		NullCheck(L_24);
		Dictionary_2_set_Item_m2214620925(L_24, L_26, L_27, /*hidden argument*/Dictionary_2_set_Item_m2214620925_MethodInfo_var);
		goto IL_0074;
	}

IL_006c:
	{
		Dictionary_2_t520966972 * L_28 = V_0;
		String_t* L_29 = V_3;
		PropertyInfo_t * L_30 = V_2;
		NullCheck(L_28);
		Dictionary_2_set_Item_m2214620925(L_28, L_29, L_30, /*hidden argument*/Dictionary_2_set_Item_m2214620925_MethodInfo_var);
	}

IL_0074:
	{
		int32_t L_31 = V_8;
		V_8 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_32 = V_8;
		PropertyInfoU5BU5D_t4286713048* L_33 = V_7;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_34 = ___objectType0;
		NullCheck(L_34);
		FieldInfoU5BU5D_t2567562023* L_35 = Type_GetFields_m3137302773(L_34, /*hidden argument*/NULL);
		V_4 = L_35;
		FieldInfoU5BU5D_t2567562023* L_36 = V_4;
		V_9 = L_36;
		V_10 = 0;
		goto IL_00df;
	}

IL_0093:
	{
		FieldInfoU5BU5D_t2567562023* L_37 = V_9;
		int32_t L_38 = V_10;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		FieldInfo_t * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		V_5 = L_40;
		FieldInfo_t * L_41 = V_5;
		NullCheck(L_41);
		bool L_42 = FieldInfo_get_IsPublic_m2574(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_00d9;
		}
	}
	{
		FieldInfo_t * L_43 = V_5;
		bool L_44 = JsonIgnoreAttribute_IsJsonIgnore_m4133572969(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_00d9;
		}
	}
	{
		FieldInfo_t * L_45 = V_5;
		String_t* L_46 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_6 = L_46;
		String_t* L_47 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_00cf;
		}
	}
	{
		Dictionary_2_t520966972 * L_49 = V_0;
		FieldInfo_t * L_50 = V_5;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		FieldInfo_t * L_52 = V_5;
		NullCheck(L_49);
		Dictionary_2_set_Item_m2214620925(L_49, L_51, L_52, /*hidden argument*/Dictionary_2_set_Item_m2214620925_MethodInfo_var);
		goto IL_00d9;
	}

IL_00cf:
	{
		Dictionary_2_t520966972 * L_53 = V_0;
		String_t* L_54 = V_6;
		FieldInfo_t * L_55 = V_5;
		NullCheck(L_53);
		Dictionary_2_set_Item_m2214620925(L_53, L_54, L_55, /*hidden argument*/Dictionary_2_set_Item_m2214620925_MethodInfo_var);
	}

IL_00d9:
	{
		int32_t L_56 = V_10;
		V_10 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_57 = V_10;
		FieldInfoU5BU5D_t2567562023* L_58 = V_9;
		NullCheck(L_58);
		if ((((int32_t)L_57) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_58)->max_length)))))))
		{
			goto IL_0093;
		}
	}
	{
		Dictionary_2_t626389457 * L_59 = TypeCoercionUtility_get_MemberMapCache_m2283621214(__this, /*hidden argument*/NULL);
		Type_t * L_60 = ___objectType0;
		Dictionary_2_t520966972 * L_61 = V_0;
		NullCheck(L_59);
		Dictionary_2_set_Item_m3738003637(L_59, L_60, L_61, /*hidden argument*/Dictionary_2_set_Item_m3738003637_MethodInfo_var);
		Dictionary_2_t520966972 * L_62 = V_0;
		return L_62;
	}
}
// System.Type JsonFx.Json.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1822791147_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1603378364_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_GetMemberInfo_m2964064126_MetadataUsageId;
extern "C"  Type_t * TypeCoercionUtility_GetMemberInfo_m2964064126 (Il2CppObject * __this /* static, unused */, Dictionary_2_t520966972 * ___memberMap0, String_t* ___memberName1, MemberInfo_t ** ___memberInfo2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_GetMemberInfo_m2964064126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t520966972 * L_0 = ___memberMap0;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Dictionary_2_t520966972 * L_1 = ___memberMap0;
		String_t* L_2 = ___memberName1;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_m1822791147(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m1822791147_MethodInfo_var);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		MemberInfo_t ** L_4 = ___memberInfo2;
		Dictionary_2_t520966972 * L_5 = ___memberMap0;
		String_t* L_6 = ___memberName1;
		NullCheck(L_5);
		MemberInfo_t * L_7 = Dictionary_2_get_Item_m1603378364(L_5, L_6, /*hidden argument*/Dictionary_2_get_Item_m1603378364_MethodInfo_var);
		*((Il2CppObject **)(L_4)) = (Il2CppObject *)L_7;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_4), (Il2CppObject *)L_7);
		MemberInfo_t ** L_8 = ___memberInfo2;
		if (!((PropertyInfo_t *)IsInstClass((*((MemberInfo_t **)L_8)), PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_002b;
		}
	}
	{
		MemberInfo_t ** L_9 = ___memberInfo2;
		NullCheck(((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_9)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_10 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, ((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_9)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		return L_10;
	}

IL_002b:
	{
		MemberInfo_t ** L_11 = ___memberInfo2;
		if (!((FieldInfo_t *)IsInstClass((*((MemberInfo_t **)L_11)), FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		MemberInfo_t ** L_12 = ___memberInfo2;
		NullCheck(((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_12)), FieldInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_13 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, ((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_12)), FieldInfo_t_il2cpp_TypeInfo_var)));
		return L_13;
	}

IL_0041:
	{
		MemberInfo_t ** L_14 = ___memberInfo2;
		*((Il2CppObject **)(L_14)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_14), (Il2CppObject *)NULL);
		return (Type_t *)NULL;
	}
}
// System.Void JsonFx.Json.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_SetMemberValue_m3887813087_MetadataUsageId;
extern "C"  void TypeCoercionUtility_SetMemberValue_m3887813087 (TypeCoercionUtility_t700629014 * __this, Il2CppObject * ___result0, Type_t * ___memberType1, MemberInfo_t * ___memberInfo2, Il2CppObject * ___value3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_SetMemberValue_m3887813087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MemberInfo_t * L_0 = ___memberInfo2;
		if (!((PropertyInfo_t *)IsInstClass(L_0, PropertyInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo2;
		Il2CppObject * L_2 = ___result0;
		Type_t * L_3 = ___memberType1;
		Il2CppObject * L_4 = ___value3;
		Il2CppObject * L_5 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(((PropertyInfo_t *)CastclassClass(L_1, PropertyInfo_t_il2cpp_TypeInfo_var)));
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(27 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, ((PropertyInfo_t *)CastclassClass(L_1, PropertyInfo_t_il2cpp_TypeInfo_var)), L_2, L_5, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		return;
	}

IL_001f:
	{
		MemberInfo_t * L_6 = ___memberInfo2;
		if (!((FieldInfo_t *)IsInstClass(L_6, FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_003c;
		}
	}
	{
		MemberInfo_t * L_7 = ___memberInfo2;
		Il2CppObject * L_8 = ___result0;
		Type_t * L_9 = ___memberType1;
		Il2CppObject * L_10 = ___value3;
		Il2CppObject * L_11 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(((FieldInfo_t *)CastclassClass(L_7, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m1669444927(((FieldInfo_t *)CastclassClass(L_7, FieldInfo_t_il2cpp_TypeInfo_var)), L_8, L_11, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern const Il2CppType* IEnumerable_t3464557803_0_0_0_var;
extern const Il2CppType* DateTime_t4283661327_0_0_0_var;
extern const Il2CppType* Guid_t2862754429_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* Uri_t1116831938_0_0_0_var;
extern const Il2CppType* Version_t763695022_0_0_0_var;
extern const Il2CppType* TimeSpan_t413522987_0_0_0_var;
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2862754429_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t763695022_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t413522987_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeDescriptor_t1537159061_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral358898177;
extern Il2CppCodeGenString* _stringLiteral2887095123;
extern const uint32_t TypeCoercionUtility_CoerceType_m3277329918_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3277329918 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m3277329918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	TypeU5BU5D_t3339007067* V_1 = NULL;
	Type_t * V_2 = NULL;
	FieldInfo_t * V_3 = NULL;
	String_t* V_4 = NULL;
	Dictionary_2_t520966972 * V_5 = NULL;
	DateTime_t4283661327  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Uri_t1116831938 * V_7 = NULL;
	TypeConverter_t1753450284 * V_8 = NULL;
	Exception_t3991598821 * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	FieldInfoU5BU5D_t2567562023* V_11 = NULL;
	int32_t V_12 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___targetType0;
		bool L_1 = TypeCoercionUtility_IsNullable_m1425495541(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = ___value1;
		if (L_2)
		{
			goto IL_0035;
		}
	}
	{
		bool L_3 = __this->get_allowNullValueTypes_1();
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_4 = ___targetType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsValueType_m1914757235(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_7 = ___targetType0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral358898177, L_8, /*hidden argument*/NULL);
		JsonTypeCoercionException_t4269543025 * L_10 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m3517389954(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0033:
	{
		Il2CppObject * L_11 = ___value1;
		return L_11;
	}

IL_0035:
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_004a;
		}
	}
	{
		Type_t * L_13 = ___targetType0;
		NullCheck(L_13);
		TypeU5BU5D_t3339007067* L_14 = VirtFuncInvoker0< TypeU5BU5D_t3339007067* >::Invoke(90 /* System.Type[] System.Type::GetGenericArguments() */, L_13);
		V_1 = L_14;
		TypeU5BU5D_t3339007067* L_15 = V_1;
		NullCheck(L_15);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_004a;
		}
	}
	{
		TypeU5BU5D_t3339007067* L_16 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		Type_t * L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		___targetType0 = L_18;
	}

IL_004a:
	{
		Il2CppObject * L_19 = ___value1;
		NullCheck(L_19);
		Type_t * L_20 = Object_GetType_m2022236990(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Type_t * L_21 = ___targetType0;
		Type_t * L_22 = V_2;
		NullCheck(L_21);
		bool L_23 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_21, L_22);
		if (!L_23)
		{
			goto IL_005c;
		}
	}
	{
		Il2CppObject * L_24 = ___value1;
		return L_24;
	}

IL_005c:
	{
		Type_t * L_25 = ___targetType0;
		NullCheck(L_25);
		bool L_26 = Type_get_IsEnum_m3878730619(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00db;
		}
	}
	{
		Il2CppObject * L_27 = ___value1;
		if (!((String_t*)IsInstSealed(L_27, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00c4;
		}
	}
	{
		Type_t * L_28 = ___targetType0;
		Il2CppObject * L_29 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		bool L_30 = Enum_IsDefined_m2781598580(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00b7;
		}
	}
	{
		Type_t * L_31 = ___targetType0;
		NullCheck(L_31);
		FieldInfoU5BU5D_t2567562023* L_32 = Type_GetFields_m3137302773(L_31, /*hidden argument*/NULL);
		V_11 = L_32;
		V_12 = 0;
		goto IL_00af;
	}

IL_0082:
	{
		FieldInfoU5BU5D_t2567562023* L_33 = V_11;
		int32_t L_34 = V_12;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		FieldInfo_t * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_3 = L_36;
		FieldInfo_t * L_37 = V_3;
		String_t* L_38 = JsonNameAttribute_GetJsonName_m3130059236(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_4 = L_38;
		Il2CppObject * L_39 = ___value1;
		String_t* L_40 = V_4;
		NullCheck(((String_t*)CastclassSealed(L_39, String_t_il2cpp_TypeInfo_var)));
		bool L_41 = String_Equals_m3541721061(((String_t*)CastclassSealed(L_39, String_t_il2cpp_TypeInfo_var)), L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00a9;
		}
	}
	{
		FieldInfo_t * L_42 = V_3;
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_42);
		___value1 = L_43;
		goto IL_00b7;
	}

IL_00a9:
	{
		int32_t L_44 = V_12;
		V_12 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00af:
	{
		int32_t L_45 = V_12;
		FieldInfoU5BU5D_t2567562023* L_46 = V_11;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_0082;
		}
	}

IL_00b7:
	{
		Type_t * L_47 = ___targetType0;
		Il2CppObject * L_48 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_49 = Enum_Parse_m2929309979(NULL /*static, unused*/, L_47, ((String_t*)CastclassSealed(L_48, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_49;
	}

IL_00c4:
	{
		Type_t * L_50 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Type_t * L_51 = Enum_GetUnderlyingType_m2468052512(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Il2CppObject * L_52 = ___value1;
		Il2CppObject * L_53 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_51, L_52, /*hidden argument*/NULL);
		___value1 = L_53;
		Type_t * L_54 = ___targetType0;
		Il2CppObject * L_55 = ___value1;
		Il2CppObject * L_56 = Enum_ToObject_m1129836274(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		return L_56;
	}

IL_00db:
	{
		Il2CppObject * L_57 = ___value1;
		if (!((Il2CppObject *)IsInst(L_57, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_00f3;
		}
	}
	{
		Type_t * L_58 = ___targetType0;
		Il2CppObject * L_59 = ___value1;
		Il2CppObject * L_60 = TypeCoercionUtility_CoerceType_m3471229122(__this, L_58, ((Il2CppObject *)Castclass(L_59, IDictionary_t537317817_il2cpp_TypeInfo_var)), (&V_5), /*hidden argument*/NULL);
		return L_60;
	}

IL_00f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_61 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t3464557803_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_62 = ___targetType0;
		NullCheck(L_61);
		bool L_63 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_61, L_62);
		if (!L_63)
		{
			goto IL_0126;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t3464557803_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_65 = V_2;
		NullCheck(L_64);
		bool L_66 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_64, L_65);
		if (!L_66)
		{
			goto IL_0126;
		}
	}
	{
		Type_t * L_67 = ___targetType0;
		Type_t * L_68 = V_2;
		Il2CppObject * L_69 = ___value1;
		Il2CppObject * L_70 = TypeCoercionUtility_CoerceList_m2295736878(__this, L_67, L_68, ((Il2CppObject *)Castclass(L_69, IEnumerable_t3464557803_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_70;
	}

IL_0126:
	{
		Il2CppObject * L_71 = ___value1;
		if (!((String_t*)IsInstSealed(L_71, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_01e6;
		}
	}
	{
		Type_t * L_72 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_73 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t4283661327_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_72) == ((Il2CppObject*)(Type_t *)L_73))))
		{
			goto IL_0162;
		}
	}
	{
		Il2CppObject * L_74 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2490955586 * L_75 = DateTimeFormatInfo_get_InvariantInfo_m1430381298(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		bool L_76 = DateTime_TryParse_m947353861(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_74, String_t_il2cpp_TypeInfo_var)), L_75, ((int32_t)143), (&V_6), /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0214;
		}
	}
	{
		DateTime_t4283661327  L_77 = V_6;
		DateTime_t4283661327  L_78 = L_77;
		Il2CppObject * L_79 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_78);
		return L_79;
	}

IL_0162:
	{
		Type_t * L_80 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_81 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Guid_t2862754429_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_80) == ((Il2CppObject*)(Type_t *)L_81))))
		{
			goto IL_0180;
		}
	}
	{
		Il2CppObject * L_82 = ___value1;
		Guid_t2862754429  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Guid__ctor_m1994687478(&L_83, ((String_t*)CastclassSealed(L_82, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Guid_t2862754429  L_84 = L_83;
		Il2CppObject * L_85 = Box(Guid_t2862754429_il2cpp_TypeInfo_var, &L_84);
		return L_85;
	}

IL_0180:
	{
		Type_t * L_86 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_87 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_86) == ((Il2CppObject*)(Type_t *)L_87))))
		{
			goto IL_01ad;
		}
	}
	{
		Il2CppObject * L_88 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_88, String_t_il2cpp_TypeInfo_var)));
		int32_t L_89 = String_get_Length_m2979997331(((String_t*)CastclassSealed(L_88, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_89) == ((uint32_t)1))))
		{
			goto IL_0214;
		}
	}
	{
		Il2CppObject * L_90 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_90, String_t_il2cpp_TypeInfo_var)));
		Il2CppChar L_91 = String_get_Chars_m3015341861(((String_t*)CastclassSealed(L_90, String_t_il2cpp_TypeInfo_var)), 0, /*hidden argument*/NULL);
		Il2CppChar L_92 = L_91;
		Il2CppObject * L_93 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_92);
		return L_93;
	}

IL_01ad:
	{
		Type_t * L_94 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_95 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Uri_t1116831938_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_94) == ((Il2CppObject*)(Type_t *)L_95))))
		{
			goto IL_01cd;
		}
	}
	{
		Il2CppObject * L_96 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		bool L_97 = Uri_TryCreate_m1126538084(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_96, String_t_il2cpp_TypeInfo_var)), 0, (&V_7), /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_0214;
		}
	}
	{
		Uri_t1116831938 * L_98 = V_7;
		return L_98;
	}

IL_01cd:
	{
		Type_t * L_99 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_100 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Version_t763695022_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_99) == ((Il2CppObject*)(Type_t *)L_100))))
		{
			goto IL_0214;
		}
	}
	{
		Il2CppObject * L_101 = ___value1;
		Version_t763695022 * L_102 = (Version_t763695022 *)il2cpp_codegen_object_new(Version_t763695022_il2cpp_TypeInfo_var);
		Version__ctor_m48000169(L_102, ((String_t*)CastclassSealed(L_101, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_102;
	}

IL_01e6:
	{
		Type_t * L_103 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_104 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t413522987_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_103) == ((Il2CppObject*)(Type_t *)L_104))))
		{
			goto IL_0214;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_105 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_106 = ___value1;
		Il2CppObject * L_107 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_105, L_106, /*hidden argument*/NULL);
		TimeSpan_t413522987  L_108;
		memset(&L_108, 0, sizeof(L_108));
		TimeSpan__ctor_m477860848(&L_108, ((*(int64_t*)((int64_t*)UnBox (L_107, Int64_t1153838595_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		TimeSpan_t413522987  L_109 = L_108;
		Il2CppObject * L_110 = Box(TimeSpan_t413522987_il2cpp_TypeInfo_var, &L_109);
		return L_110;
	}

IL_0214:
	{
		Type_t * L_111 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t1537159061_il2cpp_TypeInfo_var);
		TypeConverter_t1753450284 * L_112 = TypeDescriptor_GetConverter_m3573588811(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		V_8 = L_112;
		TypeConverter_t1753450284 * L_113 = V_8;
		Type_t * L_114 = V_2;
		NullCheck(L_113);
		bool L_115 = TypeConverter_CanConvertFrom_m3377278021(L_113, L_114, /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_022f;
		}
	}
	{
		TypeConverter_t1753450284 * L_116 = V_8;
		Il2CppObject * L_117 = ___value1;
		NullCheck(L_116);
		Il2CppObject * L_118 = TypeConverter_ConvertFrom_m891071421(L_116, L_117, /*hidden argument*/NULL);
		return L_118;
	}

IL_022f:
	{
		Type_t * L_119 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t1537159061_il2cpp_TypeInfo_var);
		TypeConverter_t1753450284 * L_120 = TypeDescriptor_GetConverter_m3573588811(NULL /*static, unused*/, L_119, /*hidden argument*/NULL);
		V_8 = L_120;
		TypeConverter_t1753450284 * L_121 = V_8;
		Type_t * L_122 = ___targetType0;
		NullCheck(L_121);
		bool L_123 = TypeConverter_CanConvertTo_m3534396116(L_121, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_024b;
		}
	}
	{
		TypeConverter_t1753450284 * L_124 = V_8;
		Il2CppObject * L_125 = ___value1;
		Type_t * L_126 = ___targetType0;
		NullCheck(L_124);
		Il2CppObject * L_127 = TypeConverter_ConvertTo_m3286262527(L_124, L_125, L_126, /*hidden argument*/NULL);
		return L_127;
	}

IL_024b:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_128 = ___value1;
		Type_t * L_129 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppObject * L_130 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_128, L_129, /*hidden argument*/NULL);
		V_10 = L_130;
		goto IL_027b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0256;
		throw e;
	}

CATCH_0256:
	{ // begin catch(System.Exception)
		V_9 = ((Exception_t3991598821 *)__exception_local);
		Il2CppObject * L_131 = ___value1;
		NullCheck(L_131);
		Type_t * L_132 = Object_GetType_m2022236990(L_131, /*hidden argument*/NULL);
		NullCheck(L_132);
		String_t* L_133 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_132);
		Type_t * L_134 = ___targetType0;
		NullCheck(L_134);
		String_t* L_135 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_134);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_136 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2887095123, L_133, L_135, /*hidden argument*/NULL);
		Exception_t3991598821 * L_137 = V_9;
		JsonTypeCoercionException_t4269543025 * L_138 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2864512244(L_138, L_136, L_137, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_138);
	} // end catch (depth: 1)

IL_027b:
	{
		Il2CppObject * L_139 = V_10;
		return L_139;
	}
}
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceType_m3471229122_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3471229122 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, Dictionary_2_t520966972 ** ___memberMap2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m3471229122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	MemberInfo_t * V_2 = NULL;
	Type_t * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___targetType0;
		Dictionary_2_t520966972 ** L_1 = ___memberMap2;
		Il2CppObject * L_2 = TypeCoercionUtility_InstantiateObject_m3710207535(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t520966972 ** L_3 = ___memberMap2;
		if (!(*((Dictionary_2_t520966972 **)L_3)))
		{
			goto IL_0064;
		}
	}
	{
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_5);
		V_4 = L_6;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0044;
		}

IL_001c:
		{
			Il2CppObject * L_7 = V_4;
			NullCheck(L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_7);
			V_1 = L_8;
			Dictionary_2_t520966972 ** L_9 = ___memberMap2;
			Il2CppObject * L_10 = V_1;
			Type_t * L_11 = TypeCoercionUtility_GetMemberInfo_m2964064126(NULL /*static, unused*/, (*((Dictionary_2_t520966972 **)L_9)), ((String_t*)IsInstSealed(L_10, String_t_il2cpp_TypeInfo_var)), (&V_2), /*hidden argument*/NULL);
			V_3 = L_11;
			Il2CppObject * L_12 = V_0;
			Type_t * L_13 = V_3;
			MemberInfo_t * L_14 = V_2;
			Il2CppObject * L_15 = ___value1;
			Il2CppObject * L_16 = V_1;
			NullCheck(L_15);
			Il2CppObject * L_17 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_15, L_16);
			TypeCoercionUtility_SetMemberValue_m3887813087(__this, L_12, L_13, L_14, L_17, /*hidden argument*/NULL);
		}

IL_0044:
		{
			Il2CppObject * L_18 = V_4;
			NullCheck(L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_001c;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x64, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_20 = V_4;
			V_5 = ((Il2CppObject *)IsInst(L_20, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_21 = V_5;
			if (!L_21)
			{
				goto IL_0063;
			}
		}

IL_005c:
		{
			Il2CppObject * L_22 = V_5;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_22);
		}

IL_0063:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0064:
	{
		Il2CppObject * L_23 = V_0;
		return L_23;
	}
}
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t3880899288_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1496450899;
extern Il2CppCodeGenString* _stringLiteral4160581905;
extern Il2CppCodeGenString* _stringLiteral3111219516;
extern Il2CppCodeGenString* _stringLiteral1488310865;
extern Il2CppCodeGenString* _stringLiteral65665;
extern Il2CppCodeGenString* _stringLiteral2032013422;
extern Il2CppCodeGenString* _stringLiteral2887095123;
extern const uint32_t TypeCoercionUtility_CoerceList_m2295736878_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceList_m2295736878 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Type_t * ___arrayType1, Il2CppObject * ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceList_m2295736878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfoU5BU5D_t2079826215* V_0 = NULL;
	ConstructorInfo_t4136801618 * V_1 = NULL;
	ConstructorInfo_t4136801618 * V_2 = NULL;
	ParameterInfoU5BU5D_t2015293532* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	TargetInvocationException_t3880899288 * V_5 = NULL;
	MethodInfo_t * V_6 = NULL;
	ParameterInfoU5BU5D_t2015293532* V_7 = NULL;
	Type_t * V_8 = NULL;
	TargetInvocationException_t3880899288 * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	TargetInvocationException_t3880899288 * V_11 = NULL;
	Exception_t3991598821 * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	ConstructorInfoU5BU5D_t2079826215* V_14 = NULL;
	int32_t V_15 = 0;
	ObjectU5BU5D_t1108656482* V_16 = NULL;
	ObjectU5BU5D_t1108656482* V_17 = NULL;
	Il2CppObject * V_18 = NULL;
	ObjectU5BU5D_t1108656482* V_19 = NULL;
	Il2CppObject * V_20 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	ParameterInfoU5BU5D_t2015293532* G_B20_0 = NULL;
	Type_t * G_B24_0 = NULL;
	ParameterInfoU5BU5D_t2015293532* G_B34_0 = NULL;
	Type_t * G_B38_0 = NULL;
	{
		Type_t * L_0 = ___targetType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsArray_m837983873(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Type_t * L_2 = ___targetType0;
		NullCheck(L_2);
		Type_t * L_3 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_2);
		Il2CppObject * L_4 = ___value2;
		Il2CppArray * L_5 = TypeCoercionUtility_CoerceArray_m2028205290(__this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0016:
	{
		Type_t * L_6 = ___targetType0;
		NullCheck(L_6);
		ConstructorInfoU5BU5D_t2079826215* L_7 = Type_GetConstructors_m3837181109(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		V_1 = (ConstructorInfo_t4136801618 *)NULL;
		ConstructorInfoU5BU5D_t2079826215* L_8 = V_0;
		V_14 = L_8;
		V_15 = 0;
		goto IL_0078;
	}

IL_0027:
	{
		ConstructorInfoU5BU5D_t2079826215* L_9 = V_14;
		int32_t L_10 = V_15;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		ConstructorInfo_t4136801618 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_2 = L_12;
		ConstructorInfo_t4136801618 * L_13 = V_2;
		NullCheck(L_13);
		ParameterInfoU5BU5D_t2015293532* L_14 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_13);
		V_3 = L_14;
		ParameterInfoU5BU5D_t2015293532* L_15 = V_3;
		NullCheck(L_15);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))
		{
			goto IL_003d;
		}
	}
	{
		ConstructorInfo_t4136801618 * L_16 = V_2;
		V_1 = L_16;
		goto IL_0072;
	}

IL_003d:
	{
		ParameterInfoU5BU5D_t2015293532* L_17 = V_3;
		NullCheck(L_17);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0072;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_18 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		int32_t L_19 = 0;
		ParameterInfo_t2235474049 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_20);
		Type_t * L_22 = ___arrayType1;
		NullCheck(L_21);
		bool L_23 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_21, L_22);
		if (!L_23)
		{
			goto IL_0072;
		}
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t4136801618 * L_24 = V_2;
		V_16 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_25 = V_16;
		Il2CppObject * L_26 = ___value2;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_26);
		ObjectU5BU5D_t1108656482* L_27 = V_16;
		NullCheck(L_24);
		Il2CppObject * L_28 = ConstructorInfo_Invoke_m759007899(L_24, L_27, /*hidden argument*/NULL);
		V_13 = L_28;
		goto IL_0281;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006f;
		throw e;
	}

CATCH_006f:
	{ // begin catch(System.Object)
		goto IL_0072;
	} // end catch (depth: 1)

IL_0072:
	{
		int32_t L_29 = V_15;
		V_15 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_30 = V_15;
		ConstructorInfoU5BU5D_t2079826215* L_31 = V_14;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		ConstructorInfo_t4136801618 * L_32 = V_1;
		if (L_32)
		{
			goto IL_0099;
		}
	}
	{
		Type_t * L_33 = ___targetType0;
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1496450899, L_34, /*hidden argument*/NULL);
		JsonTypeCoercionException_t4269543025 * L_36 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m3517389954(L_36, L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_0099:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t4136801618 * L_37 = V_1;
		NullCheck(L_37);
		Il2CppObject * L_38 = ConstructorInfo_Invoke_m759007899(L_37, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		V_4 = L_38;
		goto IL_00e0;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a4;
		throw e;
	}

CATCH_00a4:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_5 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_39 = V_5;
			NullCheck(L_39);
			Exception_t3991598821 * L_40 = Exception_get_InnerException_m1427945535(L_39, /*hidden argument*/NULL);
			if (!L_40)
			{
				goto IL_00c8;
			}
		}

IL_00af:
		{
			TargetInvocationException_t3880899288 * L_41 = V_5;
			NullCheck(L_41);
			Exception_t3991598821 * L_42 = Exception_get_InnerException_m1427945535(L_41, /*hidden argument*/NULL);
			NullCheck(L_42);
			String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_42);
			TargetInvocationException_t3880899288 * L_44 = V_5;
			NullCheck(L_44);
			Exception_t3991598821 * L_45 = Exception_get_InnerException_m1427945535(L_44, /*hidden argument*/NULL);
			JsonTypeCoercionException_t4269543025 * L_46 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_46, L_43, L_45, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_46);
		}

IL_00c8:
		{
			Type_t * L_47 = ___targetType0;
			NullCheck(L_47);
			String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_47);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_49 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral4160581905, L_48, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_50 = V_5;
			JsonTypeCoercionException_t4269543025 * L_51 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_51, L_49, L_50, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
		}
	} // end catch (depth: 1)

IL_00e0:
	{
		Type_t * L_52 = ___targetType0;
		NullCheck(L_52);
		MethodInfo_t * L_53 = Type_GetMethod_m2884801946(L_52, _stringLiteral3111219516, /*hidden argument*/NULL);
		V_6 = L_53;
		MethodInfo_t * L_54 = V_6;
		if (!L_54)
		{
			goto IL_00fa;
		}
	}
	{
		MethodInfo_t * L_55 = V_6;
		NullCheck(L_55);
		ParameterInfoU5BU5D_t2015293532* L_56 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_55);
		G_B20_0 = L_56;
		goto IL_00fb;
	}

IL_00fa:
	{
		G_B20_0 = ((ParameterInfoU5BU5D_t2015293532*)(NULL));
	}

IL_00fb:
	{
		V_7 = G_B20_0;
		ParameterInfoU5BU5D_t2015293532* L_57 = V_7;
		if (!L_57)
		{
			goto IL_0113;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_58 = V_7;
		NullCheck(L_58);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_58)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0113;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_59 = V_7;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 0);
		int32_t L_60 = 0;
		ParameterInfo_t2235474049 * L_61 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_61);
		Type_t * L_62 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_61);
		G_B24_0 = L_62;
		goto IL_0114;
	}

IL_0113:
	{
		G_B24_0 = ((Type_t *)(NULL));
	}

IL_0114:
	{
		V_8 = G_B24_0;
		Type_t * L_63 = V_8;
		if (!L_63)
		{
			goto IL_017e;
		}
	}
	{
		Type_t * L_64 = V_8;
		Type_t * L_65 = ___arrayType1;
		NullCheck(L_64);
		bool L_66 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_64, L_65);
		if (!L_66)
		{
			goto IL_017e;
		}
	}

IL_0124:
	try
	{ // begin try (depth: 1)
		MethodInfo_t * L_67 = V_6;
		Il2CppObject * L_68 = V_4;
		V_17 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_69 = V_17;
		Il2CppObject * L_70 = ___value2;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 0);
		ArrayElementTypeCheck (L_69, L_70);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_70);
		ObjectU5BU5D_t1108656482* L_71 = V_17;
		NullCheck(L_67);
		MethodBase_Invoke_m3435166155(L_67, L_68, L_71, /*hidden argument*/NULL);
		goto IL_017b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_013f;
		throw e;
	}

CATCH_013f:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_9 = ((TargetInvocationException_t3880899288 *)__exception_local);
			TargetInvocationException_t3880899288 * L_72 = V_9;
			NullCheck(L_72);
			Exception_t3991598821 * L_73 = Exception_get_InnerException_m1427945535(L_72, /*hidden argument*/NULL);
			if (!L_73)
			{
				goto IL_0163;
			}
		}

IL_014a:
		{
			TargetInvocationException_t3880899288 * L_74 = V_9;
			NullCheck(L_74);
			Exception_t3991598821 * L_75 = Exception_get_InnerException_m1427945535(L_74, /*hidden argument*/NULL);
			NullCheck(L_75);
			String_t* L_76 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_75);
			TargetInvocationException_t3880899288 * L_77 = V_9;
			NullCheck(L_77);
			Exception_t3991598821 * L_78 = Exception_get_InnerException_m1427945535(L_77, /*hidden argument*/NULL);
			JsonTypeCoercionException_t4269543025 * L_79 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_79, L_76, L_78, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_79);
		}

IL_0163:
		{
			Type_t * L_80 = ___targetType0;
			NullCheck(L_80);
			String_t* L_81 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_80);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_82 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1488310865, L_81, /*hidden argument*/NULL);
			TargetInvocationException_t3880899288 * L_83 = V_9;
			JsonTypeCoercionException_t4269543025 * L_84 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m2864512244(L_84, L_82, L_83, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_84);
		}
	} // end catch (depth: 1)

IL_017b:
	{
		Il2CppObject * L_85 = V_4;
		return L_85;
	}

IL_017e:
	{
		Type_t * L_86 = ___targetType0;
		NullCheck(L_86);
		MethodInfo_t * L_87 = Type_GetMethod_m2884801946(L_86, _stringLiteral65665, /*hidden argument*/NULL);
		V_6 = L_87;
		MethodInfo_t * L_88 = V_6;
		if (!L_88)
		{
			goto IL_0198;
		}
	}
	{
		MethodInfo_t * L_89 = V_6;
		NullCheck(L_89);
		ParameterInfoU5BU5D_t2015293532* L_90 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_89);
		G_B34_0 = L_90;
		goto IL_0199;
	}

IL_0198:
	{
		G_B34_0 = ((ParameterInfoU5BU5D_t2015293532*)(NULL));
	}

IL_0199:
	{
		V_7 = G_B34_0;
		ParameterInfoU5BU5D_t2015293532* L_91 = V_7;
		if (!L_91)
		{
			goto IL_01b1;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_92 = V_7;
		NullCheck(L_92);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_92)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_01b1;
		}
	}
	{
		ParameterInfoU5BU5D_t2015293532* L_93 = V_7;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 0);
		int32_t L_94 = 0;
		ParameterInfo_t2235474049 * L_95 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_94));
		NullCheck(L_95);
		Type_t * L_96 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_95);
		G_B38_0 = L_96;
		goto IL_01b2;
	}

IL_01b1:
	{
		G_B38_0 = ((Type_t *)(NULL));
	}

IL_01b2:
	{
		V_8 = G_B38_0;
		Type_t * L_97 = V_8;
		if (!L_97)
		{
			goto IL_0251;
		}
	}
	{
		Il2CppObject * L_98 = ___value2;
		NullCheck(L_98);
		Il2CppObject * L_99 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_98);
		V_18 = L_99;
	}

IL_01c3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_022e;
		}

IL_01c5:
		{
			Il2CppObject * L_100 = V_18;
			NullCheck(L_100);
			Il2CppObject * L_101 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_100);
			V_10 = L_101;
		}

IL_01ce:
		try
		{ // begin try (depth: 2)
			MethodInfo_t * L_102 = V_6;
			Il2CppObject * L_103 = V_4;
			V_19 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
			ObjectU5BU5D_t1108656482* L_104 = V_19;
			Type_t * L_105 = V_8;
			Il2CppObject * L_106 = V_10;
			Il2CppObject * L_107 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_105, L_106, /*hidden argument*/NULL);
			NullCheck(L_104);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_104, 0);
			ArrayElementTypeCheck (L_104, L_107);
			(L_104)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_107);
			ObjectU5BU5D_t1108656482* L_108 = V_19;
			NullCheck(L_102);
			MethodBase_Invoke_m3435166155(L_102, L_103, L_108, /*hidden argument*/NULL);
			goto IL_022e;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t3880899288_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_01f2;
			throw e;
		}

CATCH_01f2:
		{ // begin catch(System.Reflection.TargetInvocationException)
			{
				V_11 = ((TargetInvocationException_t3880899288 *)__exception_local);
				TargetInvocationException_t3880899288 * L_109 = V_11;
				NullCheck(L_109);
				Exception_t3991598821 * L_110 = Exception_get_InnerException_m1427945535(L_109, /*hidden argument*/NULL);
				if (!L_110)
				{
					goto IL_0216;
				}
			}

IL_01fd:
			{
				TargetInvocationException_t3880899288 * L_111 = V_11;
				NullCheck(L_111);
				Exception_t3991598821 * L_112 = Exception_get_InnerException_m1427945535(L_111, /*hidden argument*/NULL);
				NullCheck(L_112);
				String_t* L_113 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_112);
				TargetInvocationException_t3880899288 * L_114 = V_11;
				NullCheck(L_114);
				Exception_t3991598821 * L_115 = Exception_get_InnerException_m1427945535(L_114, /*hidden argument*/NULL);
				JsonTypeCoercionException_t4269543025 * L_116 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m2864512244(L_116, L_113, L_115, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_116);
			}

IL_0216:
			{
				Type_t * L_117 = ___targetType0;
				NullCheck(L_117);
				String_t* L_118 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_117);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_119 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2032013422, L_118, /*hidden argument*/NULL);
				TargetInvocationException_t3880899288 * L_120 = V_11;
				JsonTypeCoercionException_t4269543025 * L_121 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m2864512244(L_121, L_119, L_120, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_121);
			}
		} // end catch (depth: 2)

IL_022e:
		{
			Il2CppObject * L_122 = V_18;
			NullCheck(L_122);
			bool L_123 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_122);
			if (L_123)
			{
				goto IL_01c5;
			}
		}

IL_0237:
		{
			IL2CPP_LEAVE(0x24E, FINALLY_0239);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0239;
	}

FINALLY_0239:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_124 = V_18;
			V_20 = ((Il2CppObject *)IsInst(L_124, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_125 = V_20;
			if (!L_125)
			{
				goto IL_024d;
			}
		}

IL_0246:
		{
			Il2CppObject * L_126 = V_20;
			NullCheck(L_126);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_126);
		}

IL_024d:
		{
			IL2CPP_END_FINALLY(569)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(569)
	{
		IL2CPP_JUMP_TBL(0x24E, IL_024e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_024e:
	{
		Il2CppObject * L_127 = V_4;
		return L_127;
	}

IL_0251:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_128 = ___value2;
		Type_t * L_129 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppObject * L_130 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, L_128, L_129, /*hidden argument*/NULL);
		V_13 = L_130;
		goto IL_0281;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_025c;
		throw e;
	}

CATCH_025c:
	{ // begin catch(System.Exception)
		V_12 = ((Exception_t3991598821 *)__exception_local);
		Il2CppObject * L_131 = ___value2;
		NullCheck(L_131);
		Type_t * L_132 = Object_GetType_m2022236990(L_131, /*hidden argument*/NULL);
		NullCheck(L_132);
		String_t* L_133 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_132);
		Type_t * L_134 = ___targetType0;
		NullCheck(L_134);
		String_t* L_135 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_134);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_136 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2887095123, L_133, L_135, /*hidden argument*/NULL);
		Exception_t3991598821 * L_137 = V_12;
		JsonTypeCoercionException_t4269543025 * L_138 = (JsonTypeCoercionException_t4269543025 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t4269543025_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m2864512244(L_138, L_136, L_137, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_138);
	} // end catch (depth: 1)

IL_0281:
	{
		Il2CppObject * L_139 = V_13;
		return L_139;
	}
}
// System.Array JsonFx.Json.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceArray_m2028205290_MetadataUsageId;
extern "C"  Il2CppArray * TypeCoercionUtility_CoerceArray_m2028205290 (TypeCoercionUtility_t700629014 * __this, Type_t * ___elementType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceArray_m2028205290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t3948406897 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t3948406897 * L_0 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___value1;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_000f:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			ArrayList_t3948406897 * L_5 = V_0;
			Type_t * L_6 = ___elementType0;
			Il2CppObject * L_7 = V_1;
			Il2CppObject * L_8 = TypeCoercionUtility_CoerceType_m3277329918(__this, L_6, L_7, /*hidden argument*/NULL);
			NullCheck(L_5);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_5, L_8);
		}

IL_0025:
		{
			Il2CppObject * L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_000f;
			}
		}

IL_002d:
		{
			IL2CPP_LEAVE(0x40, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_11 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_11, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_3;
			if (!L_12)
			{
				goto IL_003f;
			}
		}

IL_0039:
		{
			Il2CppObject * L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_13);
		}

IL_003f:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0040:
	{
		ArrayList_t3948406897 * L_14 = V_0;
		Type_t * L_15 = ___elementType0;
		NullCheck(L_14);
		Il2CppArray * L_16 = VirtFuncInvoker1< Il2CppArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_14, L_15);
		return L_16;
	}
}
// System.Boolean JsonFx.Json.TypeCoercionUtility::IsNullable(System.Type)
extern const Il2CppType* Nullable_1_t1122404262_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_IsNullable_m1425495541_MetadataUsageId;
extern "C"  bool TypeCoercionUtility_IsNullable_m1425495541 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_IsNullable_m1425495541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(94 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Nullable_1_t1122404262_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(93 /* System.Type System.Type::GetGenericTypeDefinition() */, L_3);
		return (bool)((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
	}

IL_001b:
	{
		return (bool)0;
	}
}
// System.Void JsonFx.Json.TypeCoercionUtility::.ctor()
extern "C"  void TypeCoercionUtility__ctor_m3843210747 (TypeCoercionUtility_t700629014 * __this, const MethodInfo* method)
{
	{
		__this->set_allowNullValueTypes_1((bool)1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
