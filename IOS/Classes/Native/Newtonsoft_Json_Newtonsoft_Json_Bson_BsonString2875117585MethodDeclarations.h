﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonString
struct BsonString_t2875117585;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 Newtonsoft.Json.Bson.BsonString::get_ByteCount()
extern "C"  int32_t BsonString_get_ByteCount_m1289624790 (BsonString_t2875117585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonString::set_ByteCount(System.Int32)
extern "C"  void BsonString_set_ByteCount_m122660015 (BsonString_t2875117585 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Bson.BsonString::get_IncludeLength()
extern "C"  bool BsonString_get_IncludeLength_m82929603 (BsonString_t2875117585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonString::set_IncludeLength(System.Boolean)
extern "C"  void BsonString_set_IncludeLength_m1646331164 (BsonString_t2875117585 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonString::.ctor(System.Object,System.Boolean)
extern "C"  void BsonString__ctor_m3446449631 (BsonString_t2875117585 * __this, Il2CppObject * ___value0, bool ___includeLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
