﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.FetchScorePageResponse
struct FetchScorePageResponse_t1467305556;
// GooglePlayGames.Native.PInvoke.NativeScorePage
struct NativeScorePage_t3093513808;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"

// System.Void GooglePlayGames.Native.PInvoke.FetchScorePageResponse::.ctor(System.IntPtr)
extern "C"  void FetchScorePageResponse__ctor_m3619226164 (FetchScorePageResponse_t1467305556 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.FetchScorePageResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchScorePageResponse_CallDispose_m2990596604 (FetchScorePageResponse_t1467305556 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.FetchScorePageResponse::GetStatus()
extern "C"  int32_t FetchScorePageResponse_GetStatus_m793795471 (FetchScorePageResponse_t1467305556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeScorePage GooglePlayGames.Native.PInvoke.FetchScorePageResponse::GetScorePage()
extern "C"  NativeScorePage_t3093513808 * FetchScorePageResponse_GetScorePage_m1435676281 (FetchScorePageResponse_t1467305556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.FetchScorePageResponse GooglePlayGames.Native.PInvoke.FetchScorePageResponse::FromPointer(System.IntPtr)
extern "C"  FetchScorePageResponse_t1467305556 * FetchScorePageResponse_FromPointer_m2319365499 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
