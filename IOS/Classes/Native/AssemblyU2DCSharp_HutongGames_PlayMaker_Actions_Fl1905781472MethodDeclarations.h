﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatChanged
struct FloatChanged_t1905781472;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatChanged::.ctor()
extern "C"  void FloatChanged__ctor_m1836121990 (FloatChanged_t1905781472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::Reset()
extern "C"  void FloatChanged_Reset_m3777522227 (FloatChanged_t1905781472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnEnter()
extern "C"  void FloatChanged_OnEnter_m2968369629 (FloatChanged_t1905781472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnUpdate()
extern "C"  void FloatChanged_OnUpdate_m958704646 (FloatChanged_t1905781472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
