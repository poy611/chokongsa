﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataMemberAttribute>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m3099286663(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t1305603220 *, Func_2_t2910563844 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataMemberAttribute>::Get(TKey)
#define ThreadSafeStore_2_Get_m2820281261(__this, ___key0, method) ((  DataMemberAttribute_t2601848894 * (*) (ThreadSafeStore_2_t1305603220 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataMemberAttribute>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m3681153797(__this, ___key0, method) ((  DataMemberAttribute_t2601848894 * (*) (ThreadSafeStore_2_t1305603220 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
