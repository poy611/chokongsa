﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginArea
struct GUILayoutBeginArea_t3761865337;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::.ctor()
extern "C"  void GUILayoutBeginArea__ctor_m3361698317 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::Reset()
extern "C"  void GUILayoutBeginArea_Reset_m1008131258 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::OnGUI()
extern "C"  void GUILayoutBeginArea_OnGUI_m2857096967 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
