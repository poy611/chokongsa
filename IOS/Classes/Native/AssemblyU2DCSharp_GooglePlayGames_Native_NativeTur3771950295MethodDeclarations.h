﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey90
struct U3CRegisterMatchDelegateU3Ec__AnonStorey90_t3771950295;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey90::.ctor()
extern "C"  void U3CRegisterMatchDelegateU3Ec__AnonStorey90__ctor_m1421408676 (U3CRegisterMatchDelegateU3Ec__AnonStorey90_t3771950295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey90::<>m__7F(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C"  void U3CRegisterMatchDelegateU3Ec__AnonStorey90_U3CU3Em__7F_m3926603282 (U3CRegisterMatchDelegateU3Ec__AnonStorey90_t3771950295 * __this, TurnBasedMatch_t3573041681 * ___match0, bool ___autoLaunch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
