﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugObject
struct DebugObject_t2190156288;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugObject::.ctor()
extern "C"  void DebugObject__ctor_m2986477878 (DebugObject_t2190156288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugObject::Reset()
extern "C"  void DebugObject_Reset_m632910819 (DebugObject_t2190156288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugObject::OnEnter()
extern "C"  void DebugObject_OnEnter_m358815629 (DebugObject_t2190156288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
