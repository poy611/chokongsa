﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnumCompare
struct EnumCompare_t921178930;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnumCompare::.ctor()
extern "C"  void EnumCompare__ctor_m4277948484 (EnumCompare_t921178930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::Reset()
extern "C"  void EnumCompare_Reset_m1924381425 (EnumCompare_t921178930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::OnEnter()
extern "C"  void EnumCompare_OnEnter_m216519451 (EnumCompare_t921178930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::OnUpdate()
extern "C"  void EnumCompare_OnUpdate_m1550695048 (EnumCompare_t921178930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::DoEnumCompare()
extern "C"  void EnumCompare_DoEnumCompare_m2149324091 (EnumCompare_t921178930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
