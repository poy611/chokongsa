﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar2601556940.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction522766867.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEven3541123425.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis4294105229.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepe99988271.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect3606982749.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy300513412.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV184977789.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec1643322606.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1922345195.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection1293548283.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility1171612705.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction94975348.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2627072750.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis3565360268.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState2895308594.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial639665897.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE1574154081.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle110812896.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit2757337633.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent2331340366.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1990156785.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1074114320.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping1257491342.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli1294793591.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter436718473.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As2149445162.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2777732396.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2493957633.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1153512176.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit1837657360.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1285073872.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fit909765868.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup169317941.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corne284493240.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1399125956.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons1640775616.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou1336501463.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical2052396382.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1596995480.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup352294875.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder1942933988.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility3144854024.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup423167365.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "YoutubeUnity_U3CModuleU3E86524790.h"
#include "YoutubeUnity_YoutubeExtractor_Decipherer1774992449.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver3917844667.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_E925438340.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_1414174443.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911561.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911564.h"
#include "YoutubeUnity_YoutubeExtractor_HttpHelper4274725662.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"
#include "YoutubeUnity_YoutubeExtractor_VideoNotAvailableExc1343733958.h"
#include "YoutubeUnity_YoutubeExtractor_YoutubeParseExceptio1485001709.h"
#include "YoutubeUnity_YoutubeExtractor_AdaptiveType3319469144.h"
#include "YoutubeUnity_YoutubeExtractor_AudioType955051966.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_FFmpeg_AutoGen_pp_mode1873958964.h"
#include "AssemblyU2DCSharp_FFmpeg_AutoGen_pp_context2297634062.h"
#include "AssemblyU2DCSharp_FFmpeg_AutoGen__iobuf750733356.h"
#include "AssemblyU2DCSharp_FFmpeg_AutoGen_size_t1323646292.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleGUI1334456284.h"
#include "AssemblyU2DCSharp_MedaiPlayerSampleSphereGUI3177899023.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_ERRO1475309647.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT1488282328.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIA_SCALE4148698416.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoEnd3177907679.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoReady259270055.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoError247668236.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoFirstFrameR2520860170.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_VideoResize3742945584.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CDownloadStrea2578073438.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CDownloadStrea1146777291.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Scrollbar_t2601556940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[11] = 
{
	Scrollbar_t2601556940::get_offset_of_m_HandleRect_16(),
	Scrollbar_t2601556940::get_offset_of_m_Direction_17(),
	Scrollbar_t2601556940::get_offset_of_m_Value_18(),
	Scrollbar_t2601556940::get_offset_of_m_Size_19(),
	Scrollbar_t2601556940::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t2601556940::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t2601556940::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t2601556940::get_offset_of_m_Offset_23(),
	Scrollbar_t2601556940::get_offset_of_m_Tracker_24(),
	Scrollbar_t2601556940::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t2601556940::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (Direction_t522766867)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[5] = 
{
	Direction_t522766867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (ScrollEvent_t3541123425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (Axis_t4294105229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2603[3] = 
{
	Axis_t4294105229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (U3CClickRepeatU3Ec__Iterator5_t99988271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[7] = 
{
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CaxisCoordinateU3E__1_2(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24PC_3(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U24current_4(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU24U3EeventData_5(),
	U3CClickRepeatU3Ec__Iterator5_t99988271::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (ScrollRect_t3606982749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[36] = 
{
	ScrollRect_t3606982749::get_offset_of_m_Content_2(),
	ScrollRect_t3606982749::get_offset_of_m_Horizontal_3(),
	ScrollRect_t3606982749::get_offset_of_m_Vertical_4(),
	ScrollRect_t3606982749::get_offset_of_m_MovementType_5(),
	ScrollRect_t3606982749::get_offset_of_m_Elasticity_6(),
	ScrollRect_t3606982749::get_offset_of_m_Inertia_7(),
	ScrollRect_t3606982749::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t3606982749::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t3606982749::get_offset_of_m_Viewport_10(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t3606982749::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t3606982749::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t3606982749::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t3606982749::get_offset_of_m_ViewRect_20(),
	ScrollRect_t3606982749::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t3606982749::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t3606982749::get_offset_of_m_Velocity_23(),
	ScrollRect_t3606982749::get_offset_of_m_Dragging_24(),
	ScrollRect_t3606982749::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t3606982749::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t3606982749::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t3606982749::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t3606982749::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t3606982749::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t3606982749::get_offset_of_m_Rect_33(),
	ScrollRect_t3606982749::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t3606982749::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t3606982749::get_offset_of_m_Tracker_36(),
	ScrollRect_t3606982749::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (MovementType_t300513412)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2606[4] = 
{
	MovementType_t300513412::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (ScrollbarVisibility_t184977789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2607[4] = 
{
	ScrollbarVisibility_t184977789::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (ScrollRectEvent_t1643322606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (Selectable_t1885181538), -1, sizeof(Selectable_t1885181538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2609[14] = 
{
	Selectable_t1885181538_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1885181538::get_offset_of_m_Navigation_3(),
	Selectable_t1885181538::get_offset_of_m_Transition_4(),
	Selectable_t1885181538::get_offset_of_m_Colors_5(),
	Selectable_t1885181538::get_offset_of_m_SpriteState_6(),
	Selectable_t1885181538::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1885181538::get_offset_of_m_Interactable_8(),
	Selectable_t1885181538::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1885181538::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1885181538::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1885181538::get_offset_of_m_CanvasGroupCache_12(),
	Selectable_t1885181538::get_offset_of_U3CisPointerInsideU3Ek__BackingField_13(),
	Selectable_t1885181538::get_offset_of_U3CisPointerDownU3Ek__BackingField_14(),
	Selectable_t1885181538::get_offset_of_U3ChasSelectionU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (Transition_t1922345195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[5] = 
{
	Transition_t1922345195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (SelectionState_t1293548283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2611[5] = 
{
	SelectionState_t1293548283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (SetPropertyUtility_t1171612705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (Slider_t79469677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[15] = 
{
	Slider_t79469677::get_offset_of_m_FillRect_16(),
	Slider_t79469677::get_offset_of_m_HandleRect_17(),
	Slider_t79469677::get_offset_of_m_Direction_18(),
	Slider_t79469677::get_offset_of_m_MinValue_19(),
	Slider_t79469677::get_offset_of_m_MaxValue_20(),
	Slider_t79469677::get_offset_of_m_WholeNumbers_21(),
	Slider_t79469677::get_offset_of_m_Value_22(),
	Slider_t79469677::get_offset_of_m_OnValueChanged_23(),
	Slider_t79469677::get_offset_of_m_FillImage_24(),
	Slider_t79469677::get_offset_of_m_FillTransform_25(),
	Slider_t79469677::get_offset_of_m_FillContainerRect_26(),
	Slider_t79469677::get_offset_of_m_HandleTransform_27(),
	Slider_t79469677::get_offset_of_m_HandleContainerRect_28(),
	Slider_t79469677::get_offset_of_m_Offset_29(),
	Slider_t79469677::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Direction_t94975348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2614[5] = 
{
	Direction_t94975348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (SliderEvent_t2627072750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (Axis_t3565360268)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[3] = 
{
	Axis_t3565360268::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (SpriteState_t2895308594)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[3] = 
{
	SpriteState_t2895308594::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t2895308594::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (StencilMaterial_t639665897), -1, sizeof(StencilMaterial_t639665897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2618[1] = 
{
	StencilMaterial_t639665897_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (MatEntry_t1574154081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[10] = 
{
	MatEntry_t1574154081::get_offset_of_baseMat_0(),
	MatEntry_t1574154081::get_offset_of_customMat_1(),
	MatEntry_t1574154081::get_offset_of_count_2(),
	MatEntry_t1574154081::get_offset_of_stencilId_3(),
	MatEntry_t1574154081::get_offset_of_operation_4(),
	MatEntry_t1574154081::get_offset_of_compareFunction_5(),
	MatEntry_t1574154081::get_offset_of_readMask_6(),
	MatEntry_t1574154081::get_offset_of_writeMask_7(),
	MatEntry_t1574154081::get_offset_of_useAlphaClip_8(),
	MatEntry_t1574154081::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (Text_t9039225), -1, sizeof(Text_t9039225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2620[7] = 
{
	Text_t9039225::get_offset_of_m_FontData_28(),
	Text_t9039225::get_offset_of_m_Text_29(),
	Text_t9039225::get_offset_of_m_TextCache_30(),
	Text_t9039225::get_offset_of_m_TextCacheForLayout_31(),
	Text_t9039225_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t9039225::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t9039225::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (Toggle_t110812896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[5] = 
{
	Toggle_t110812896::get_offset_of_toggleTransition_16(),
	Toggle_t110812896::get_offset_of_graphic_17(),
	Toggle_t110812896::get_offset_of_m_Group_18(),
	Toggle_t110812896::get_offset_of_onValueChanged_19(),
	Toggle_t110812896::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (ToggleTransition_t2757337633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2622[3] = 
{
	ToggleTransition_t2757337633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (ToggleEvent_t2331340366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (ToggleGroup_t1990156785), -1, sizeof(ToggleGroup_t1990156785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2624[4] = 
{
	ToggleGroup_t1990156785::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1990156785::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1990156785_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (ClipperRegistry_t1074114320), -1, sizeof(ClipperRegistry_t1074114320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	ClipperRegistry_t1074114320_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1074114320::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (Clipping_t1257491342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (RectangularVertexClipper_t1294793591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[2] = 
{
	RectangularVertexClipper_t1294793591::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t1294793591::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (AspectRatioFitter_t436718473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[4] = 
{
	AspectRatioFitter_t436718473::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t436718473::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t436718473::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t436718473::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (AspectMode_t2149445162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2631[6] = 
{
	AspectMode_t2149445162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (CanvasScaler_t2777732396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[14] = 
{
	0,
	CanvasScaler_t2777732396::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2777732396::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2777732396::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2777732396::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2777732396::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2777732396::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2777732396::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2777732396::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2777732396::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2777732396::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2777732396::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (ScaleMode_t2493957633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2633[4] = 
{
	ScaleMode_t2493957633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (ScreenMatchMode_t1153512176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2634[4] = 
{
	ScreenMatchMode_t1153512176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (Unit_t1837657360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2635[6] = 
{
	Unit_t1837657360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (ContentSizeFitter_t1285073872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[4] = 
{
	ContentSizeFitter_t1285073872::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1285073872::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1285073872::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (FitMode_t909765868)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2637[4] = 
{
	FitMode_t909765868::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (GridLayoutGroup_t169317941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[6] = 
{
	GridLayoutGroup_t169317941::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t169317941::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t169317941::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t169317941::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t169317941::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t169317941::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (Corner_t284493240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2639[5] = 
{
	Corner_t284493240::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (Axis_t1399125956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2640[3] = 
{
	Axis_t1399125956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (Constraint_t1640775616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2641[4] = 
{
	Constraint_t1640775616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (HorizontalLayoutGroup_t1336501463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (HorizontalOrVerticalLayoutGroup_t2052396382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[3] = 
{
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t2052396382::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (LayoutElement_t1596995480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[7] = 
{
	LayoutElement_t1596995480::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1596995480::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1596995480::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1596995480::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1596995480::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (LayoutGroup_t352294875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[8] = 
{
	LayoutGroup_t352294875::get_offset_of_m_Padding_2(),
	LayoutGroup_t352294875::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t352294875::get_offset_of_m_Rect_4(),
	LayoutGroup_t352294875::get_offset_of_m_Tracker_5(),
	LayoutGroup_t352294875::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t352294875::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t352294875::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t352294875::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (LayoutRebuilder_t1942933988), -1, sizeof(LayoutRebuilder_t1942933988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[9] = 
{
	LayoutRebuilder_t1942933988::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t1942933988::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t1942933988_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (LayoutUtility_t3144854024), -1, sizeof(LayoutUtility_t3144854024_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2652[8] = 
{
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3144854024_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (VerticalLayoutGroup_t423167365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2658[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238939), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238939_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (U24ArrayTypeU2412_t3379220355)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220355_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (U3CModuleU3E_t86524801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (Decipherer_t1774992449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (DownloadUrlResolver_t3917844667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (ExtractionInfo_t925438340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[2] = 
{
	ExtractionInfo_t925438340::get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_0(),
	ExtractionInfo_t925438340::get_offset_of_U3CUriU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (U3CExtractDownloadUrlsU3Ed__1_t1414174443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[14] = 
{
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3E2__current_0(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3E1__state_1(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_json_3(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3E3__json_4(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CsplitByUrlsU3E5__2_5(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CsU3E5__3_6(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CqueriesU3E5__4_7(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CurlU3E5__5_8(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CrequiresDecryptionU3E5__6_9(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CparametersU3E5__7_10(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3Eg__initLocal0_11(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3E7__wrap9_12(),
	U3CExtractDownloadUrlsU3Ed__1_t1414174443::get_offset_of_U3CU3E7__wrapa_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CU3Ec__DisplayClass12_t2936911561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[1] = 
{
	U3CU3Ec__DisplayClass12_t2936911561::get_offset_of_formatCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (U3CU3Ec__DisplayClass15_t2936911564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[1] = 
{
	U3CU3Ec__DisplayClass15_t2936911564::get_offset_of_FormatCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (HttpHelper_t4274725662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (VideoInfo_t2099471191), -1, sizeof(VideoInfo_t2099471191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2676[14] = 
{
	VideoInfo_t2099471191_StaticFields::get_offset_of_Defaults_0(),
	VideoInfo_t2099471191::get_offset_of_U3CAdaptiveTypeU3Ek__BackingField_1(),
	VideoInfo_t2099471191::get_offset_of_U3CAudioBitrateU3Ek__BackingField_2(),
	VideoInfo_t2099471191::get_offset_of_U3CFrameRateU3Ek__BackingField_3(),
	VideoInfo_t2099471191::get_offset_of_U3CAudioTypeU3Ek__BackingField_4(),
	VideoInfo_t2099471191::get_offset_of_U3CDownloadUrlU3Ek__BackingField_5(),
	VideoInfo_t2099471191::get_offset_of_U3CFormatCodeU3Ek__BackingField_6(),
	VideoInfo_t2099471191::get_offset_of_U3CIs3DU3Ek__BackingField_7(),
	VideoInfo_t2099471191::get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_8(),
	VideoInfo_t2099471191::get_offset_of_U3CFileSizeU3Ek__BackingField_9(),
	VideoInfo_t2099471191::get_offset_of_U3CResolutionU3Ek__BackingField_10(),
	VideoInfo_t2099471191::get_offset_of_U3CTitleU3Ek__BackingField_11(),
	VideoInfo_t2099471191::get_offset_of_U3CVideoTypeU3Ek__BackingField_12(),
	VideoInfo_t2099471191::get_offset_of_U3CHtmlPlayerVersionU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (VideoNotAvailableException_t1343733958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (YoutubeParseException_t1485001709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (AdaptiveType_t3319469144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2679[4] = 
{
	AdaptiveType_t3319469144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (AudioType_t955051966)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2680[6] = 
{
	AudioType_t955051966::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (VideoType_t2099809763)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2681[6] = 
{
	VideoType_t2099809763::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (U3CModuleU3E_t86524802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (pp_mode_t1873958964)+ sizeof (Il2CppObject), sizeof(pp_mode_t1873958964_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (pp_context_t2297634062)+ sizeof (Il2CppObject), sizeof(pp_context_t2297634062_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (_iobuf_t750733356)+ sizeof (Il2CppObject), sizeof(_iobuf_t750733356_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (size_t_t1323646292)+ sizeof (Il2CppObject), sizeof(size_t_t1323646292_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	size_t_t1323646292::get_offset_of_value_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (MedaiPlayerSampleGUI_t1334456284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[2] = 
{
	MedaiPlayerSampleGUI_t1334456284::get_offset_of_scrMedia_2(),
	MedaiPlayerSampleGUI_t1334456284::get_offset_of_m_bFinish_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (MedaiPlayerSampleSphereGUI_t3177899023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[1] = 
{
	MedaiPlayerSampleSphereGUI_t3177899023::get_offset_of_scrMedia_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (MediaPlayerCtrl_t3572035536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[37] = 
{
	MediaPlayerCtrl_t3572035536::get_offset_of_m_strFileName_2(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_TargetMaterial_3(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_VideoTexture_4(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_VideoTextureDummy_5(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_CurrentState_6(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iCurrentSeekPosition_7(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_fVolume_8(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iWidth_9(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iHeight_10(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_fSpeed_11(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bFullScreen_12(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bSupportRockchip_13(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnResize_14(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnReady_15(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnEnd_16(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnVideoError_17(),
	MediaPlayerCtrl_t3572035536::get_offset_of_OnVideoFirstFrameReady_18(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_texPtr_19(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iPauseFrame_20(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iAndroidMgrID_21(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bIsFirstFrameReady_22(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bFirst_23(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_ScaleValue_24(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_objResize_25(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bLoop_26(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bAutoPlay_27(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bStop_28(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bInit_29(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bCheckFBO_30(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bPause_31(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_bReadyPlay_32(),
	MediaPlayerCtrl_t3572035536::get_offset_of_m_iID_33(),
	MediaPlayerCtrl_t3572035536::get_offset_of__videoTexture_34(),
	MediaPlayerCtrl_t3572035536::get_offset_of_bFirstIOS_35(),
	MediaPlayerCtrl_t3572035536::get_offset_of_unityMainThreadActionList_36(),
	MediaPlayerCtrl_t3572035536::get_offset_of_checkNewActions_37(),
	MediaPlayerCtrl_t3572035536::get_offset_of_thisLock_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (MEDIAPLAYER_ERROR_t1475309647)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[8] = 
{
	MEDIAPLAYER_ERROR_t1475309647::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (MEDIAPLAYER_STATE_t1488282328)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[8] = 
{
	MEDIAPLAYER_STATE_t1488282328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (MEDIA_SCALE_t4148698416)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2692[8] = 
{
	MEDIA_SCALE_t4148698416::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (VideoEnd_t3177907679), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (VideoReady_t259270055), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (VideoError_t247668236), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (VideoFirstFrameReady_t2520860170), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (VideoResize_t3742945584), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[7] = 
{
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_strURL_0(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U3CwwwU3E__0_1(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U3Cwrite_pathU3E__1_2(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U24PC_3(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U24current_4(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U3CU24U3EstrURL_5(),
	U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t2578073438::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[7] = 
{
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_strURL_0(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U3Cwrite_pathU3E__0_1(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U3CwwwU3E__1_2(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U24PC_3(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U24current_4(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U3CU24U3EstrURL_5(),
	U3CDownloadStreamingVideoAndLoad2U3Ec__Iterator1_t1146777291::get_offset_of_U3CU3Ef__this_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
