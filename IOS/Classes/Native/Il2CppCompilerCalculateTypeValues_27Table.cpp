﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_U3CCopyStreamingAs96045597.h"
#include "AssemblyU2DCSharp_MediaPlayerEvent3360813525.h"
#include "AssemblyU2DCSharp_MediaPlayerFullScreenCtrl326609931.h"
#include "AssemblyU2DCSharp_SeekBarCtrl3766823782.h"
#include "AssemblyU2DCSharp_SphereMirror167396876.h"
#include "AssemblyU2DCSharp_GPGmanager385716591.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSou3931761423.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2572053391.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2320727150.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb3617536533.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DummyCl2268884141.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_3429751962.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb4006482697.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2200833403.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2886292529.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2238854319.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl1804230813.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3727527619.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2965598227.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3119174256.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Ad68742423.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_4159799943.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_4280027939.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_C215995099.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_2104311078.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_3227722211.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_I518463702.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_N147041391.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam4135364200.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam1745712882.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayerSta60064856.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3806932551.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_1709442041.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_Q412997597.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3857037258.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_Q935666006.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3187252993.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2415333593.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa3786215536.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4210182474.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4052853983.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1414359808.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ScorePa1995225314.h"
#include "AssemblyU2DCSharp_GooglePlayGames_GameInfo3799103638.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesAchieve2357341912.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLeaderb3198617382.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs4064039103.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs1599266691.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3624292130.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor1592139576.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatform225202025.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335463.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335464.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3867310193.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScore486124539.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro4293397255.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro2198666563.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger984534948.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U139427695.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U382508163.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_1492737394.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc1723690688.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame727662960.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame724587615.h"
#include "AssemblyU2DCSharp_GooglePlayGames_IOS_IOSClient3522634302.h"
#include "AssemblyU2DCSharp_GooglePlayGames_IOS_IOSTokenClie3192953715.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3586973114.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Conversio4116610761.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_A723931725.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1008856062.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3534128988.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[7] = 
{
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_strURL_0(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U3Cwrite_pathU3E__0_1(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U3CwwwU3E__1_2(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U24PC_3(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U24current_4(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U3CU24U3EstrURL_5(),
	U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator2_t96045597::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (MediaPlayerEvent_t3360813525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[1] = 
{
	MediaPlayerEvent_t3360813525::get_offset_of_m_srcVideo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (MediaPlayerFullScreenCtrl_t326609931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[3] = 
{
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_objVideo_2(),
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_iOrgWidth_3(),
	MediaPlayerFullScreenCtrl_t326609931::get_offset_of_m_iOrgHeight_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (SeekBarCtrl_t3766823782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[8] = 
{
	SeekBarCtrl_t3766823782::get_offset_of_m_srcVideo_2(),
	SeekBarCtrl_t3766823782::get_offset_of_m_srcSlider_3(),
	SeekBarCtrl_t3766823782::get_offset_of_m_fDragTime_4(),
	SeekBarCtrl_t3766823782::get_offset_of_m_bActiveDrag_5(),
	SeekBarCtrl_t3766823782::get_offset_of_m_bUpdate_6(),
	SeekBarCtrl_t3766823782::get_offset_of_m_fDeltaTime_7(),
	SeekBarCtrl_t3766823782::get_offset_of_m_fLastValue_8(),
	SeekBarCtrl_t3766823782::get_offset_of_m_fLastSetValue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (SphereMirror_t167396876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (GPGmanager_t385716591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (Achievement_t1261647177), -1, sizeof(Achievement_t1261647177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2706[13] = 
{
	Achievement_t1261647177_StaticFields::get_offset_of_UnixEpoch_0(),
	Achievement_t1261647177::get_offset_of_mId_1(),
	Achievement_t1261647177::get_offset_of_mIsIncremental_2(),
	Achievement_t1261647177::get_offset_of_mIsRevealed_3(),
	Achievement_t1261647177::get_offset_of_mIsUnlocked_4(),
	Achievement_t1261647177::get_offset_of_mCurrentSteps_5(),
	Achievement_t1261647177::get_offset_of_mTotalSteps_6(),
	Achievement_t1261647177::get_offset_of_mDescription_7(),
	Achievement_t1261647177::get_offset_of_mName_8(),
	Achievement_t1261647177::get_offset_of_mLastModifiedTime_9(),
	Achievement_t1261647177::get_offset_of_mPoints_10(),
	Achievement_t1261647177::get_offset_of_mRevealedImageUrl_11(),
	Achievement_t1261647177::get_offset_of_mUnlockedImageUrl_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (CommonStatusCodes_t671203459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2707[25] = 
{
	CommonStatusCodes_t671203459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (DataSource_t3931761423)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2708[3] = 
{
	DataSource_t3931761423::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (ResponseStatus_t419677757)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2709[8] = 
{
	ResponseStatus_t419677757::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (UIStatus_t427705392)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2710[9] = 
{
	UIStatus_t427705392::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (LeaderboardStart_t2572053391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2711[3] = 
{
	LeaderboardStart_t2572053391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (LeaderboardTimeSpan_t2320727150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[4] = 
{
	LeaderboardTimeSpan_t2320727150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (LeaderboardCollection_t3617536533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2713[3] = 
{
	LeaderboardCollection_t3617536533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (DummyClient_t2268884141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (EventVisibility_t3429751962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[3] = 
{
	EventVisibility_t3429751962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (LeaderboardScoreData_t4006482697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[8] = 
{
	LeaderboardScoreData_t4006482697::get_offset_of_mId_0(),
	LeaderboardScoreData_t4006482697::get_offset_of_mStatus_1(),
	LeaderboardScoreData_t4006482697::get_offset_of_mApproxCount_2(),
	LeaderboardScoreData_t4006482697::get_offset_of_mTitle_3(),
	LeaderboardScoreData_t4006482697::get_offset_of_mPlayerScore_4(),
	LeaderboardScoreData_t4006482697::get_offset_of_mPrevPage_5(),
	LeaderboardScoreData_t4006482697::get_offset_of_mNextPage_6(),
	LeaderboardScoreData_t4006482697::get_offset_of_mScores_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (Invitation_t2200833403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[4] = 
{
	Invitation_t2200833403::get_offset_of_mInvitationType_0(),
	Invitation_t2200833403::get_offset_of_mInvitationId_1(),
	Invitation_t2200833403::get_offset_of_mInviter_2(),
	Invitation_t2200833403::get_offset_of_mVariant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (InvType_t2886292529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	InvType_t2886292529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (MatchOutcome_t2238854319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[4] = 
{
	0,
	MatchOutcome_t2238854319::get_offset_of_mParticipantIds_1(),
	MatchOutcome_t2238854319::get_offset_of_mPlacements_2(),
	MatchOutcome_t2238854319::get_offset_of_mResults_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (ParticipantResult_t2327217482)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[6] = 
{
	ParticipantResult_t2327217482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (Participant_t1804230813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[5] = 
{
	Participant_t1804230813::get_offset_of_mDisplayName_0(),
	Participant_t1804230813::get_offset_of_mParticipantId_1(),
	Participant_t1804230813::get_offset_of_mStatus_2(),
	Participant_t1804230813::get_offset_of_mPlayer_3(),
	Participant_t1804230813::get_offset_of_mIsConnectedToRoom_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (ParticipantStatus_t4028684685)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[9] = 
{
	ParticipantStatus_t4028684685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Player_t3727527619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (TurnBasedMatch_t3573041681), -1, sizeof(TurnBasedMatch_t3573041681_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[12] = 
{
	TurnBasedMatch_t3573041681::get_offset_of_mMatchId_0(),
	TurnBasedMatch_t3573041681::get_offset_of_mData_1(),
	TurnBasedMatch_t3573041681::get_offset_of_mCanRematch_2(),
	TurnBasedMatch_t3573041681::get_offset_of_mAvailableAutomatchSlots_3(),
	TurnBasedMatch_t3573041681::get_offset_of_mSelfParticipantId_4(),
	TurnBasedMatch_t3573041681::get_offset_of_mParticipants_5(),
	TurnBasedMatch_t3573041681::get_offset_of_mPendingParticipantId_6(),
	TurnBasedMatch_t3573041681::get_offset_of_mTurnStatus_7(),
	TurnBasedMatch_t3573041681::get_offset_of_mMatchStatus_8(),
	TurnBasedMatch_t3573041681::get_offset_of_mVariant_9(),
	TurnBasedMatch_t3573041681::get_offset_of_mVersion_10(),
	TurnBasedMatch_t3573041681_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (MatchStatus_t2965598227)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2731[8] = 
{
	MatchStatus_t2965598227::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (MatchTurnStatus_t3119174256)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2732[6] = 
{
	MatchTurnStatus_t3119174256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (AdvertisingResult_t68742423)+ sizeof (Il2CppObject), sizeof(AdvertisingResult_t68742423_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2733[2] = 
{
	AdvertisingResult_t68742423::get_offset_of_mStatus_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AdvertisingResult_t68742423::get_offset_of_mLocalEndpointName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (ConnectionRequest_t4159799943)+ sizeof (Il2CppObject), sizeof(ConnectionRequest_t4159799943_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	ConnectionRequest_t4159799943::get_offset_of_mRemoteEndpoint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionRequest_t4159799943::get_offset_of_mPayload_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (ConnectionResponse_t4280027939)+ sizeof (Il2CppObject), sizeof(ConnectionResponse_t4280027939_marshaled_pinvoke), sizeof(ConnectionResponse_t4280027939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2735[5] = 
{
	ConnectionResponse_t4280027939_StaticFields::get_offset_of_EmptyPayload_0(),
	ConnectionResponse_t4280027939::get_offset_of_mLocalClientId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t4280027939::get_offset_of_mRemoteEndpointId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t4280027939::get_offset_of_mResponseStatus_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t4280027939::get_offset_of_mPayload_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (Status_t215995099)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[7] = 
{
	Status_t215995099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (DummyNearbyConnectionClient_t2104311078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (EndpointDetails_t3227722211)+ sizeof (Il2CppObject), sizeof(EndpointDetails_t3227722211_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2738[4] = 
{
	EndpointDetails_t3227722211::get_offset_of_mEndpointId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EndpointDetails_t3227722211::get_offset_of_mDeviceId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EndpointDetails_t3227722211::get_offset_of_mName_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EndpointDetails_t3227722211::get_offset_of_mServiceId_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (InitializationStatus_t518463702)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[4] = 
{
	InitializationStatus_t518463702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (NearbyConnectionConfiguration_t147041391)+ sizeof (Il2CppObject), sizeof(NearbyConnectionConfiguration_t147041391_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	0,
	0,
	NearbyConnectionConfiguration_t147041391::get_offset_of_mInitializationCallback_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NearbyConnectionConfiguration_t147041391::get_offset_of_mLocalClientId_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (PlayGamesClientConfiguration_t4135364200)+ sizeof (Il2CppObject), -1, sizeof(PlayGamesClientConfiguration_t4135364200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2744[7] = 
{
	PlayGamesClientConfiguration_t4135364200_StaticFields::get_offset_of_DefaultConfiguration_0(),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mEnableSavedGames_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mRequireGooglePlus_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mScopes_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mInvitationDelegate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mMatchDelegate_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayGamesClientConfiguration_t4135364200::get_offset_of_mPermissionRationale_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (Builder_t1745712882), -1, sizeof(Builder_t1745712882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2745[8] = 
{
	Builder_t1745712882::get_offset_of_mEnableSaveGames_0(),
	Builder_t1745712882::get_offset_of_mRequireGooglePlus_1(),
	Builder_t1745712882::get_offset_of_mScopes_2(),
	Builder_t1745712882::get_offset_of_mInvitationDelegate_3(),
	Builder_t1745712882::get_offset_of_mMatchDelegate_4(),
	Builder_t1745712882::get_offset_of_mRationale_5(),
	Builder_t1745712882_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	Builder_t1745712882_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (PlayerStats_t60064856), -1, sizeof(PlayerStats_t60064856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[12] = 
{
	PlayerStats_t60064856_StaticFields::get_offset_of_UNSET_VALUE_0(),
	PlayerStats_t60064856::get_offset_of_U3CValidU3Ek__BackingField_1(),
	PlayerStats_t60064856::get_offset_of_U3CNumberOfPurchasesU3Ek__BackingField_2(),
	PlayerStats_t60064856::get_offset_of_U3CAvgSessonLengthU3Ek__BackingField_3(),
	PlayerStats_t60064856::get_offset_of_U3CDaysSinceLastPlayedU3Ek__BackingField_4(),
	PlayerStats_t60064856::get_offset_of_U3CNumberOfSessionsU3Ek__BackingField_5(),
	PlayerStats_t60064856::get_offset_of_U3CSessPercentileU3Ek__BackingField_6(),
	PlayerStats_t60064856::get_offset_of_U3CSpendPercentileU3Ek__BackingField_7(),
	PlayerStats_t60064856::get_offset_of_U3CSpendProbabilityU3Ek__BackingField_8(),
	PlayerStats_t60064856::get_offset_of_U3CChurnProbabilityU3Ek__BackingField_9(),
	PlayerStats_t60064856::get_offset_of_U3CHighSpenderProbabilityU3Ek__BackingField_10(),
	PlayerStats_t60064856::get_offset_of_U3CTotalSpendNext28DaysU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (QuestState_t3806932551)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2747[7] = 
{
	QuestState_t3806932551::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (MilestoneState_t1709442041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2749[5] = 
{
	MilestoneState_t1709442041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (QuestFetchFlags_t412997597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2751[10] = 
{
	QuestFetchFlags_t412997597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (QuestAcceptStatus_t3857037258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2752[8] = 
{
	QuestAcceptStatus_t3857037258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (QuestClaimMilestoneStatus_t935666006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2753[8] = 
{
	QuestClaimMilestoneStatus_t935666006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (QuestUiResult_t3187252993)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2754[10] = 
{
	QuestUiResult_t3187252993::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (ConflictResolutionStrategy_t2415333593)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[4] = 
{
	ConflictResolutionStrategy_t2415333593::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (SavedGameRequestStatus_t3786215536)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2757[6] = 
{
	SavedGameRequestStatus_t3786215536::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SelectUIStatus_t4210182474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2758[7] = 
{
	SelectUIStatus_t4210182474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (SavedGameMetadataUpdate_t4052853983)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[5] = 
{
	SavedGameMetadataUpdate_t4052853983::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t4052853983::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t4052853983::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t4052853983::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t4052853983::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (Builder_t1414359808)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[5] = 
{
	Builder_t1414359808::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t1414359808::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t1414359808::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t1414359808::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t1414359808::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (ScorePageToken_t1995225314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	ScorePageToken_t1995225314::get_offset_of_mId_0(),
	ScorePageToken_t1995225314::get_offset_of_mInternalObject_1(),
	ScorePageToken_t1995225314::get_offset_of_mCollection_2(),
	ScorePageToken_t1995225314::get_offset_of_mTimespan_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (GameInfo_t3799103638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (PlayGamesAchievement_t2357341912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[16] = 
{
	PlayGamesAchievement_t2357341912::get_offset_of_mProgressCallback_0(),
	PlayGamesAchievement_t2357341912::get_offset_of_mId_1(),
	PlayGamesAchievement_t2357341912::get_offset_of_mIsIncremental_2(),
	PlayGamesAchievement_t2357341912::get_offset_of_mCurrentSteps_3(),
	PlayGamesAchievement_t2357341912::get_offset_of_mTotalSteps_4(),
	PlayGamesAchievement_t2357341912::get_offset_of_mPercentComplete_5(),
	PlayGamesAchievement_t2357341912::get_offset_of_mCompleted_6(),
	PlayGamesAchievement_t2357341912::get_offset_of_mHidden_7(),
	PlayGamesAchievement_t2357341912::get_offset_of_mLastModifiedTime_8(),
	PlayGamesAchievement_t2357341912::get_offset_of_mTitle_9(),
	PlayGamesAchievement_t2357341912::get_offset_of_mRevealedImageUrl_10(),
	PlayGamesAchievement_t2357341912::get_offset_of_mUnlockedImageUrl_11(),
	PlayGamesAchievement_t2357341912::get_offset_of_mImageFetcher_12(),
	PlayGamesAchievement_t2357341912::get_offset_of_mImage_13(),
	PlayGamesAchievement_t2357341912::get_offset_of_mDescription_14(),
	PlayGamesAchievement_t2357341912::get_offset_of_mPoints_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (PlayGamesLeaderboard_t3198617382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[10] = 
{
	PlayGamesLeaderboard_t3198617382::get_offset_of_mId_0(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mUserScope_1(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mRange_2(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mTimeScope_3(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mFilteredUserIds_4(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mLoading_5(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mLocalUserScore_6(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mMaxRange_7(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mScoreList_8(),
	PlayGamesLeaderboard_t3198617382::get_offset_of_mTitle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (PlayGamesLocalUser_t4064039103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[3] = 
{
	PlayGamesLocalUser_t4064039103::get_offset_of_mPlatform_5(),
	PlayGamesLocalUser_t4064039103::get_offset_of_emailAddress_6(),
	PlayGamesLocalUser_t4064039103::get_offset_of_mStats_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (U3CGetStatsU3Ec__AnonStorey34_t1599266691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[2] = 
{
	U3CGetStatsU3Ec__AnonStorey34_t1599266691::get_offset_of_callback_0(),
	U3CGetStatsU3Ec__AnonStorey34_t1599266691::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (PlayGamesPlatform_t3624292130), -1, sizeof(PlayGamesPlatform_t3624292130_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[8] = 
{
	PlayGamesPlatform_t3624292130_StaticFields::get_offset_of_sInstance_0(),
	PlayGamesPlatform_t3624292130_StaticFields::get_offset_of_sNearbyInitializePending_1(),
	PlayGamesPlatform_t3624292130_StaticFields::get_offset_of_sNearbyConnectionClient_2(),
	PlayGamesPlatform_t3624292130::get_offset_of_mConfiguration_3(),
	PlayGamesPlatform_t3624292130::get_offset_of_mLocalUser_4(),
	PlayGamesPlatform_t3624292130::get_offset_of_mClient_5(),
	PlayGamesPlatform_t3624292130::get_offset_of_mDefaultLbUi_6(),
	PlayGamesPlatform_t3624292130::get_offset_of_mIdMap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[1] = 
{
	U3CLoadAchievementsU3Ec__AnonStorey36_t225202025::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (U3CLoadScoresU3Ec__AnonStorey37_t2259335463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[1] = 
{
	U3CLoadScoresU3Ec__AnonStorey37_t2259335463::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (U3CLoadScoresU3Ec__AnonStorey38_t2259335464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	U3CLoadScoresU3Ec__AnonStorey38_t2259335464::get_offset_of_board_0(),
	U3CLoadScoresU3Ec__AnonStorey38_t2259335464::get_offset_of_callback_1(),
	U3CLoadScoresU3Ec__AnonStorey38_t2259335464::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[3] = 
{
	U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193::get_offset_of_board_0(),
	U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193::get_offset_of_callback_1(),
	U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (PlayGamesScore_t486124539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[6] = 
{
	PlayGamesScore_t486124539::get_offset_of_mLbId_0(),
	PlayGamesScore_t486124539::get_offset_of_mValue_1(),
	PlayGamesScore_t486124539::get_offset_of_mRank_2(),
	PlayGamesScore_t486124539::get_offset_of_mPlayerId_3(),
	PlayGamesScore_t486124539::get_offset_of_mMetadata_4(),
	PlayGamesScore_t486124539::get_offset_of_mDate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (PlayGamesUserProfile_t4293397255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[5] = 
{
	PlayGamesUserProfile_t4293397255::get_offset_of_mDisplayName_0(),
	PlayGamesUserProfile_t4293397255::get_offset_of_mPlayerId_1(),
	PlayGamesUserProfile_t4293397255::get_offset_of_mAvatarUrl_2(),
	PlayGamesUserProfile_t4293397255::get_offset_of_mImageLoading_3(),
	PlayGamesUserProfile_t4293397255::get_offset_of_mImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (U3CLoadImageU3Ec__Iterator3_t2198666563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[4] = 
{
	U3CLoadImageU3Ec__Iterator3_t2198666563::get_offset_of_U3CwwwU3E__0_0(),
	U3CLoadImageU3Ec__Iterator3_t2198666563::get_offset_of_U24PC_1(),
	U3CLoadImageU3Ec__Iterator3_t2198666563::get_offset_of_U24current_2(),
	U3CLoadImageU3Ec__Iterator3_t2198666563::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (Logger_t984534948), -1, sizeof(Logger_t984534948_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2779[2] = 
{
	Logger_t984534948_StaticFields::get_offset_of_debugLogEnabled_0(),
	Logger_t984534948_StaticFields::get_offset_of_warningLogEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (U3CdU3Ec__AnonStorey3A_t139427695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[1] = 
{
	U3CdU3Ec__AnonStorey3A_t139427695::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (U3CwU3Ec__AnonStorey3B_t82508163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[1] = 
{
	U3CwU3Ec__AnonStorey3B_t82508163::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (U3CeU3Ec__AnonStorey3C_t1492737394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[1] = 
{
	U3CeU3Ec__AnonStorey3C_t1492737394::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (Misc_t1723690688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (PlayGamesHelperObject_t727662960), -1, sizeof(PlayGamesHelperObject_t727662960_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2784[7] = 
{
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_instance_2(),
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_sIsDummy_3(),
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_sQueue_4(),
	PlayGamesHelperObject_t727662960::get_offset_of_localQueue_5(),
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_sQueueEmpty_6(),
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_sPauseCallbackList_7(),
	PlayGamesHelperObject_t727662960_StaticFields::get_offset_of_sFocusCallbackList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[1] = 
{
	U3CRunCoroutineU3Ec__AnonStorey3D_t724587615::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (IOSClient_t3522634302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (IOSTokenClient_t3192953715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (CallbackUtils_t3586973114), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (ConversionUtils_t4116610761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (Achievement_t723931725), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (AchievementManager_t1008856062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (FetchAllCallback_t3534128988), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
