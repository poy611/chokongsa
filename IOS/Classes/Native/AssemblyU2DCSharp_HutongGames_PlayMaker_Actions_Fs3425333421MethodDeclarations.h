﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateSwitch
struct FsmStateSwitch_t3425333421;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::.ctor()
extern "C"  void FsmStateSwitch__ctor_m1908935257 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::Reset()
extern "C"  void FsmStateSwitch_Reset_m3850335494 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnEnter()
extern "C"  void FsmStateSwitch_OnEnter_m4222442480 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnUpdate()
extern "C"  void FsmStateSwitch_OnUpdate_m1180257363 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::DoFsmStateSwitch()
extern "C"  void FsmStateSwitch_DoFsmStateSwitch_m3415497723 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
