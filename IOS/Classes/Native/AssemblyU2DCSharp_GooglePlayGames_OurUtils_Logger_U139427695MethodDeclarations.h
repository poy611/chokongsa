﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A
struct U3CdU3Ec__AnonStorey3A_t139427695;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A::.ctor()
extern "C"  void U3CdU3Ec__AnonStorey3A__ctor_m1409748492 (U3CdU3Ec__AnonStorey3A_t139427695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A::<>m__9()
extern "C"  void U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302 (U3CdU3Ec__AnonStorey3A_t139427695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
