﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1154988161(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3154616052 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2309492639_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::Invoke(T1,T2)
#define Action_2_Invoke_m1316354634(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3154616052 *, MultiplayerParticipant_t3337232325 *, NativeTurnBasedMatch_t302853426 *, const MethodInfo*))Action_2_Invoke_m172731500_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1775886873(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3154616052 *, MultiplayerParticipant_t3337232325 *, NativeTurnBasedMatch_t302853426 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3413657584_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1625582625(__this, ___result0, method) ((  void (*) (Action_2_t3154616052 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3926193450_gshared)(__this, ___result0, method)
