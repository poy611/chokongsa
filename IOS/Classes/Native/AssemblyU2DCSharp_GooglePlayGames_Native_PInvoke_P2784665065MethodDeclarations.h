﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlatformConfiguration
struct PlatformConfiguration_t2784665065;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.PlatformConfiguration::.ctor(System.IntPtr)
extern "C"  void PlatformConfiguration__ctor_m2795114713 (PlatformConfiguration_t2784665065 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.PlatformConfiguration::AsHandle()
extern "C"  HandleRef_t1780819301  PlatformConfiguration_AsHandle_m3507145373 (PlatformConfiguration_t2784665065 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
