﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget
struct AnimatorInterruptMatchTarget_t1680292610;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::.ctor()
extern "C"  void AnimatorInterruptMatchTarget__ctor_m3043591588 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::Reset()
extern "C"  void AnimatorInterruptMatchTarget_Reset_m690024529 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::OnEnter()
extern "C"  void AnimatorInterruptMatchTarget_OnEnter_m3705483387 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
