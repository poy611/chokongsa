﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector
struct GetQuaternionMultipliedByVector_t1105007759;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::.ctor()
extern "C"  void GetQuaternionMultipliedByVector__ctor_m3619095431 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::Reset()
extern "C"  void GetQuaternionMultipliedByVector_Reset_m1265528372 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnEnter()
extern "C"  void GetQuaternionMultipliedByVector_OnEnter_m2713895326 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnUpdate_m1659935845 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnLateUpdate_m3106121643 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnFixedUpdate_m2676435747 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByVector_DoQuatMult_m831878191 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
