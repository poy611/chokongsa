﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse
struct FetchListResponse_t676237491;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.NativePlayer>
struct IEnumerator_1_t253783741;
// GooglePlayGames.Native.PInvoke.NativePlayer
struct NativePlayer_t2636885988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::.ctor(System.IntPtr)
extern "C"  void FetchListResponse__ctor_m84816908 (FetchListResponse_t676237491 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FetchListResponse_System_Collections_IEnumerable_GetEnumerator_m4068015273 (FetchListResponse_t676237491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchListResponse_CallDispose_m3940764452 (FetchListResponse_t676237491 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::Status()
extern "C"  int32_t FetchListResponse_Status_m2391589591 (FetchListResponse_t676237491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.NativePlayer> GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::GetEnumerator()
extern "C"  Il2CppObject* FetchListResponse_GetEnumerator_m337371005 (FetchListResponse_t676237491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::Length()
extern "C"  UIntPtr_t  FetchListResponse_Length_m1400435882 (FetchListResponse_t676237491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativePlayer GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::GetElement(System.UIntPtr)
extern "C"  NativePlayer_t2636885988 * FetchListResponse_GetElement_m787666437 (FetchListResponse_t676237491 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::FromPointer(System.IntPtr)
extern "C"  FetchListResponse_t676237491 * FetchListResponse_FromPointer_m1132039173 (Il2CppObject * __this /* static, unused */, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativePlayer GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse::<GetEnumerator>m__CB(System.UIntPtr)
extern "C"  NativePlayer_t2636885988 * FetchListResponse_U3CGetEnumeratorU3Em__CB_m2307837547 (FetchListResponse_t676237491 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
