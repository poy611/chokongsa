﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties
struct NetworkGetNextConnectedPlayerProperties_t1267749648;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::.ctor()
extern "C"  void NetworkGetNextConnectedPlayerProperties__ctor_m1911037478 (NetworkGetNextConnectedPlayerProperties_t1267749648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::Reset()
extern "C"  void NetworkGetNextConnectedPlayerProperties_Reset_m3852437715 (NetworkGetNextConnectedPlayerProperties_t1267749648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::OnEnter()
extern "C"  void NetworkGetNextConnectedPlayerProperties_OnEnter_m1947709565 (NetworkGetNextConnectedPlayerProperties_t1267749648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::DoGetNextPlayerProperties()
extern "C"  void NetworkGetNextConnectedPlayerProperties_DoGetNextPlayerProperties_m4221661878 (NetworkGetNextConnectedPlayerProperties_t1267749648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
