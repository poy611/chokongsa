﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2892245493_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2892245493(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2892245493_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2912594893_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2912594893(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2912594893_gshared)(__this, method)
