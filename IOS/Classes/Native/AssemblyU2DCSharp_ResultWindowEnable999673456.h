﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScoreManagerMiniGame
struct ScoreManagerMiniGame_t3434204900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultWindowEnable
struct  ResultWindowEnable_t999673456  : public MonoBehaviour_t667441552
{
public:
	// ScoreManagerMiniGame ResultWindowEnable::manager
	ScoreManagerMiniGame_t3434204900 * ___manager_2;

public:
	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(ResultWindowEnable_t999673456, ___manager_2)); }
	inline ScoreManagerMiniGame_t3434204900 * get_manager_2() const { return ___manager_2; }
	inline ScoreManagerMiniGame_t3434204900 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(ScoreManagerMiniGame_t3434204900 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier(&___manager_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
