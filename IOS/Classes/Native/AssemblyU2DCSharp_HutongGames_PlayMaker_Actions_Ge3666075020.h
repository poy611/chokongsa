﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct  GetAnimatorIsLayerInTransition_t3666075020  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::layerIndex
	FsmInt_t1596138449 * ___layerIndex_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransition
	FsmBool_t1075959796 * ___isInTransition_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransitionEvent
	FsmEvent_t2133468028 * ___isInTransitionEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isNotInTransitionEvent
	FsmEvent_t2133468028 * ___isNotInTransitionEvent_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::_animator
	Animator_t2776330603 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_layerIndex_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___layerIndex_15)); }
	inline FsmInt_t1596138449 * get_layerIndex_15() const { return ___layerIndex_15; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_15() { return &___layerIndex_15; }
	inline void set_layerIndex_15(FsmInt_t1596138449 * value)
	{
		___layerIndex_15 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_15, value);
	}

	inline static int32_t get_offset_of_isInTransition_16() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isInTransition_16)); }
	inline FsmBool_t1075959796 * get_isInTransition_16() const { return ___isInTransition_16; }
	inline FsmBool_t1075959796 ** get_address_of_isInTransition_16() { return &___isInTransition_16; }
	inline void set_isInTransition_16(FsmBool_t1075959796 * value)
	{
		___isInTransition_16 = value;
		Il2CppCodeGenWriteBarrier(&___isInTransition_16, value);
	}

	inline static int32_t get_offset_of_isInTransitionEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isInTransitionEvent_17)); }
	inline FsmEvent_t2133468028 * get_isInTransitionEvent_17() const { return ___isInTransitionEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_isInTransitionEvent_17() { return &___isInTransitionEvent_17; }
	inline void set_isInTransitionEvent_17(FsmEvent_t2133468028 * value)
	{
		___isInTransitionEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___isInTransitionEvent_17, value);
	}

	inline static int32_t get_offset_of_isNotInTransitionEvent_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ___isNotInTransitionEvent_18)); }
	inline FsmEvent_t2133468028 * get_isNotInTransitionEvent_18() const { return ___isNotInTransitionEvent_18; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotInTransitionEvent_18() { return &___isNotInTransitionEvent_18; }
	inline void set_isNotInTransitionEvent_18(FsmEvent_t2133468028 * value)
	{
		___isNotInTransitionEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___isNotInTransitionEvent_18, value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3666075020, ____animator_19)); }
	inline Animator_t2776330603 * get__animator_19() const { return ____animator_19; }
	inline Animator_t2776330603 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t2776330603 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier(&____animator_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
