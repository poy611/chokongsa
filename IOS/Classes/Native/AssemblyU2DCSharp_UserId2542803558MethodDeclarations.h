﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserId
struct UserId_t2542803558;

#include "codegen/il2cpp-codegen.h"

// System.Void UserId::.ctor()
extern "C"  void UserId__ctor_m24616181 (UserId_t2542803558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
