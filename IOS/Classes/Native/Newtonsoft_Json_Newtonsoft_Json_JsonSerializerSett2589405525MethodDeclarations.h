﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t926437290;
// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver>
struct Func_1_t1550586138;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t1492900827;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl2761661122.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandl2077487315.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHandli56081595.h"
#include "Newtonsoft_Json_Newtonsoft_Json_NullValueHandling2754652381.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHa2626038881.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin2475221485.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern "C"  int32_t JsonSerializerSettings_get_ReferenceLoopHandling_m867230569 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern "C"  int32_t JsonSerializerSettings_get_MissingMemberHandling_m1174656715 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern "C"  int32_t JsonSerializerSettings_get_ObjectCreationHandling_m3943358967 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_NullValueHandling_m3645768031 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern "C"  int32_t JsonSerializerSettings_get_DefaultValueHandling_m4290083767 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern "C"  Il2CppObject* JsonSerializerSettings_get_Converters_m1034270961 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern "C"  int32_t JsonSerializerSettings_get_PreserveReferencesHandling_m2538197559 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameHandling_m3002449367 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializerSettings::get_MetadataPropertyHandling()
extern "C"  int32_t JsonSerializerSettings_get_MetadataPropertyHandling_m4152276535 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormat()
extern "C"  int32_t JsonSerializerSettings_get_TypeNameAssemblyFormat_m2704893627 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern "C"  int32_t JsonSerializerSettings_get_ConstructorHandling_m3141127807 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializerSettings_get_ContractResolver_m1302927414 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::get_EqualityComparer()
extern "C"  Il2CppObject * JsonSerializerSettings_get_EqualityComparer_m4031978555 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolverProvider()
extern "C"  Func_1_t1550586138 * JsonSerializerSettings_get_ReferenceResolverProvider_m2954348883 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::get_TraceWriter()
extern "C"  Il2CppObject * JsonSerializerSettings_get_TraceWriter_m983991696 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_Binder()
extern "C"  SerializationBinder_t2137423328 * JsonSerializerSettings_get_Binder_m1099803183 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern "C"  EventHandler_1_t937589677 * JsonSerializerSettings_get_Error_m374503006 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializerSettings_get_Context_m935104197 (JsonSerializerSettings_t2589405525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern "C"  void JsonSerializerSettings__cctor_m2808476985 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
