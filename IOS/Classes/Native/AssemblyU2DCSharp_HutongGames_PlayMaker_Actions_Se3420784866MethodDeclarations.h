﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterialColor
struct SetMaterialColor_t3420784866;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::.ctor()
extern "C"  void SetMaterialColor__ctor_m3524442052 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::Reset()
extern "C"  void SetMaterialColor_Reset_m1170874993 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnEnter()
extern "C"  void SetMaterialColor_OnEnter_m1946311323 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnUpdate()
extern "C"  void SetMaterialColor_OnUpdate_m3634635528 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::DoSetMaterialColor()
extern "C"  void SetMaterialColor_DoSetMaterialColor_m2216301925 (SetMaterialColor_t3420784866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
