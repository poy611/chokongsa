﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTriggerInfo
struct GetTriggerInfo_t1760428088;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::.ctor()
extern "C"  void GetTriggerInfo__ctor_m712361774 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::Reset()
extern "C"  void GetTriggerInfo_Reset_m2653762011 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::StoreTriggerInfo()
extern "C"  void GetTriggerInfo_StoreTriggerInfo_m3589498491 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::OnEnter()
extern "C"  void GetTriggerInfo_OnEnter_m1071593349 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
