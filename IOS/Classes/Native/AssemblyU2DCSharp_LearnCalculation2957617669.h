﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// PopupManagement
struct PopupManagement_t282433007;
// StructforMinigame
struct StructforMinigame_t1286533533;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_LearnCalculation_CAL_STATE3539248406.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearnCalculation
struct  LearnCalculation_t2957617669  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 LearnCalculation::random_op
	int32_t ___random_op_2;
	// UnityEngine.UI.Text LearnCalculation::question
	Text_t9039225 * ___question_3;
	// UnityEngine.UI.Text LearnCalculation::ans
	Text_t9039225 * ___ans_4;
	// System.Boolean LearnCalculation::isRight
	bool ___isRight_5;
	// PopupManagement LearnCalculation::popup
	PopupManagement_t282433007 * ___popup_6;
	// System.Int32 LearnCalculation::randomNum1
	int32_t ___randomNum1_7;
	// System.Int32 LearnCalculation::randomNum2
	int32_t ___randomNum2_8;
	// System.Int32 LearnCalculation::rightNum
	int32_t ___rightNum_9;
	// LearnCalculation/CAL_STATE LearnCalculation::state
	int32_t ___state_10;
	// StructforMinigame LearnCalculation::stuff
	StructforMinigame_t1286533533 * ___stuff_11;
	// System.Diagnostics.Stopwatch LearnCalculation::sw
	Stopwatch_t3420517611 * ___sw_12;

public:
	inline static int32_t get_offset_of_random_op_2() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___random_op_2)); }
	inline int32_t get_random_op_2() const { return ___random_op_2; }
	inline int32_t* get_address_of_random_op_2() { return &___random_op_2; }
	inline void set_random_op_2(int32_t value)
	{
		___random_op_2 = value;
	}

	inline static int32_t get_offset_of_question_3() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___question_3)); }
	inline Text_t9039225 * get_question_3() const { return ___question_3; }
	inline Text_t9039225 ** get_address_of_question_3() { return &___question_3; }
	inline void set_question_3(Text_t9039225 * value)
	{
		___question_3 = value;
		Il2CppCodeGenWriteBarrier(&___question_3, value);
	}

	inline static int32_t get_offset_of_ans_4() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___ans_4)); }
	inline Text_t9039225 * get_ans_4() const { return ___ans_4; }
	inline Text_t9039225 ** get_address_of_ans_4() { return &___ans_4; }
	inline void set_ans_4(Text_t9039225 * value)
	{
		___ans_4 = value;
		Il2CppCodeGenWriteBarrier(&___ans_4, value);
	}

	inline static int32_t get_offset_of_isRight_5() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___isRight_5)); }
	inline bool get_isRight_5() const { return ___isRight_5; }
	inline bool* get_address_of_isRight_5() { return &___isRight_5; }
	inline void set_isRight_5(bool value)
	{
		___isRight_5 = value;
	}

	inline static int32_t get_offset_of_popup_6() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___popup_6)); }
	inline PopupManagement_t282433007 * get_popup_6() const { return ___popup_6; }
	inline PopupManagement_t282433007 ** get_address_of_popup_6() { return &___popup_6; }
	inline void set_popup_6(PopupManagement_t282433007 * value)
	{
		___popup_6 = value;
		Il2CppCodeGenWriteBarrier(&___popup_6, value);
	}

	inline static int32_t get_offset_of_randomNum1_7() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___randomNum1_7)); }
	inline int32_t get_randomNum1_7() const { return ___randomNum1_7; }
	inline int32_t* get_address_of_randomNum1_7() { return &___randomNum1_7; }
	inline void set_randomNum1_7(int32_t value)
	{
		___randomNum1_7 = value;
	}

	inline static int32_t get_offset_of_randomNum2_8() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___randomNum2_8)); }
	inline int32_t get_randomNum2_8() const { return ___randomNum2_8; }
	inline int32_t* get_address_of_randomNum2_8() { return &___randomNum2_8; }
	inline void set_randomNum2_8(int32_t value)
	{
		___randomNum2_8 = value;
	}

	inline static int32_t get_offset_of_rightNum_9() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___rightNum_9)); }
	inline int32_t get_rightNum_9() const { return ___rightNum_9; }
	inline int32_t* get_address_of_rightNum_9() { return &___rightNum_9; }
	inline void set_rightNum_9(int32_t value)
	{
		___rightNum_9 = value;
	}

	inline static int32_t get_offset_of_state_10() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___state_10)); }
	inline int32_t get_state_10() const { return ___state_10; }
	inline int32_t* get_address_of_state_10() { return &___state_10; }
	inline void set_state_10(int32_t value)
	{
		___state_10 = value;
	}

	inline static int32_t get_offset_of_stuff_11() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___stuff_11)); }
	inline StructforMinigame_t1286533533 * get_stuff_11() const { return ___stuff_11; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_11() { return &___stuff_11; }
	inline void set_stuff_11(StructforMinigame_t1286533533 * value)
	{
		___stuff_11 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_11, value);
	}

	inline static int32_t get_offset_of_sw_12() { return static_cast<int32_t>(offsetof(LearnCalculation_t2957617669, ___sw_12)); }
	inline Stopwatch_t3420517611 * get_sw_12() const { return ___sw_12; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_12() { return &___sw_12; }
	inline void set_sw_12(Stopwatch_t3420517611 * value)
	{
		___sw_12 = value;
		Il2CppCodeGenWriteBarrier(&___sw_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
