﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPGSBtn/<LoginCheckerNet>c__IteratorE
struct U3CLoginCheckerNetU3Ec__IteratorE_t3774081813;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::.ctor()
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE__ctor_m734193142 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPGSBtn/<LoginCheckerNet>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerNetU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3205914332 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPGSBtn/<LoginCheckerNet>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerNetU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m809081456 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPGSBtn/<LoginCheckerNet>c__IteratorE::MoveNext()
extern "C"  bool U3CLoginCheckerNetU3Ec__IteratorE_MoveNext_m4003472190 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::Dispose()
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE_Dispose_m1083255987 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::Reset()
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE_Reset_m2675593379 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
