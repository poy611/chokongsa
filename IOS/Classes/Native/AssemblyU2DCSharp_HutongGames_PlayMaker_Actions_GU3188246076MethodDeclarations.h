﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIContentAction
struct GUIContentAction_t3188246076;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::.ctor()
extern "C"  void GUIContentAction__ctor_m2980405162 (GUIContentAction_t3188246076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::Reset()
extern "C"  void GUIContentAction_Reset_m626838103 (GUIContentAction_t3188246076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::OnGUI()
extern "C"  void GUIContentAction_OnGUI_m2475803812 (GUIContentAction_t3188246076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
