﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUITooltip
struct GUITooltip_t3898253232;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUITooltip::.ctor()
extern "C"  void GUITooltip__ctor_m4005681846 (GUITooltip_t3898253232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUITooltip::Reset()
extern "C"  void GUITooltip_Reset_m1652114787 (GUITooltip_t3898253232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUITooltip::OnGUI()
extern "C"  void GUITooltip_OnGUI_m3501080496 (GUITooltip_t3898253232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
