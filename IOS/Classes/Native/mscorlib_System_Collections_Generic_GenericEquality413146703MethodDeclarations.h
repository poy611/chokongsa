﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct GenericEqualityComparer_1_t413146703;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.ResolverContractKey>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m516856846_gshared (GenericEqualityComparer_1_t413146703 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m516856846(__this, method) ((  void (*) (GenericEqualityComparer_1_t413146703 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m516856846_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.ResolverContractKey>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3645039397_gshared (GenericEqualityComparer_1_t413146703 * __this, ResolverContractKey_t473801005  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m3645039397(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t413146703 *, ResolverContractKey_t473801005 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m3645039397_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Serialization.ResolverContractKey>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m251669283_gshared (GenericEqualityComparer_1_t413146703 * __this, ResolverContractKey_t473801005  ___x0, ResolverContractKey_t473801005  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m251669283(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t413146703 *, ResolverContractKey_t473801005 , ResolverContractKey_t473801005 , const MethodInfo*))GenericEqualityComparer_1_Equals_m251669283_gshared)(__this, ___x0, ___y1, method)
