﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnyKey
struct AnyKey_t2823194811;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnyKey::.ctor()
extern "C"  void AnyKey__ctor_m2770516491 (AnyKey_t2823194811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnyKey::Reset()
extern "C"  void AnyKey_Reset_m416949432 (AnyKey_t2823194811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnyKey::OnUpdate()
extern "C"  void AnyKey_OnUpdate_m1822238561 (AnyKey_t2823194811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
