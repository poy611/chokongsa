﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B
struct U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B::.ctor()
extern "C"  void U3CGetServerAuthCodeU3Ec__AnonStorey4B__ctor_m1026197547 (U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
