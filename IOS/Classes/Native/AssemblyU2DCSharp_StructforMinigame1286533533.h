﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StructforMinigame
struct  StructforMinigame_t1286533533  : public Il2CppObject
{
public:
	// System.Diagnostics.Stopwatch StructforMinigame::sw
	Stopwatch_t3420517611 * ___sw_1;
	// System.String StructforMinigame::userId
	String_t* ___userId_2;
	// System.Int32 StructforMinigame::gameId
	int32_t ___gameId_3;
	// System.Int32 StructforMinigame::level
	int32_t ___level_4;
	// System.String StructforMinigame::recordDate
	String_t* ___recordDate_5;
	// System.Single StructforMinigame::score
	float ___score_6;
	// System.Single StructforMinigame::playtime
	float ___playtime_7;
	// System.Int32 StructforMinigame::misscount
	int32_t ___misscount_8;
	// System.Single StructforMinigame::errorRate
	float ___errorRate_9;

public:
	inline static int32_t get_offset_of_sw_1() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___sw_1)); }
	inline Stopwatch_t3420517611 * get_sw_1() const { return ___sw_1; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_1() { return &___sw_1; }
	inline void set_sw_1(Stopwatch_t3420517611 * value)
	{
		___sw_1 = value;
		Il2CppCodeGenWriteBarrier(&___sw_1, value);
	}

	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_gameId_3() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___gameId_3)); }
	inline int32_t get_gameId_3() const { return ___gameId_3; }
	inline int32_t* get_address_of_gameId_3() { return &___gameId_3; }
	inline void set_gameId_3(int32_t value)
	{
		___gameId_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___level_4)); }
	inline int32_t get_level_4() const { return ___level_4; }
	inline int32_t* get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(int32_t value)
	{
		___level_4 = value;
	}

	inline static int32_t get_offset_of_recordDate_5() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___recordDate_5)); }
	inline String_t* get_recordDate_5() const { return ___recordDate_5; }
	inline String_t** get_address_of_recordDate_5() { return &___recordDate_5; }
	inline void set_recordDate_5(String_t* value)
	{
		___recordDate_5 = value;
		Il2CppCodeGenWriteBarrier(&___recordDate_5, value);
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___score_6)); }
	inline float get_score_6() const { return ___score_6; }
	inline float* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(float value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_playtime_7() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___playtime_7)); }
	inline float get_playtime_7() const { return ___playtime_7; }
	inline float* get_address_of_playtime_7() { return &___playtime_7; }
	inline void set_playtime_7(float value)
	{
		___playtime_7 = value;
	}

	inline static int32_t get_offset_of_misscount_8() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___misscount_8)); }
	inline int32_t get_misscount_8() const { return ___misscount_8; }
	inline int32_t* get_address_of_misscount_8() { return &___misscount_8; }
	inline void set_misscount_8(int32_t value)
	{
		___misscount_8 = value;
	}

	inline static int32_t get_offset_of_errorRate_9() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533, ___errorRate_9)); }
	inline float get_errorRate_9() const { return ___errorRate_9; }
	inline float* get_address_of_errorRate_9() { return &___errorRate_9; }
	inline void set_errorRate_9(float value)
	{
		___errorRate_9 = value;
	}
};

struct StructforMinigame_t1286533533_StaticFields
{
public:
	// StructforMinigame[] StructforMinigame::stuff
	StructforMinigameU5BU5D_t1843399504* ___stuff_0;

public:
	inline static int32_t get_offset_of_stuff_0() { return static_cast<int32_t>(offsetof(StructforMinigame_t1286533533_StaticFields, ___stuff_0)); }
	inline StructforMinigameU5BU5D_t1843399504* get_stuff_0() const { return ___stuff_0; }
	inline StructforMinigameU5BU5D_t1843399504** get_address_of_stuff_0() { return &___stuff_0; }
	inline void set_stuff_0(StructforMinigameU5BU5D_t1843399504* value)
	{
		___stuff_0 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
