﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MainUserInfo
struct MainUserInfo_t2526075922;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<MainUserInfo>
struct  Singleton_1_t2778891315  : public MonoBehaviour_t667441552
{
public:

public:
};

struct Singleton_1_t2778891315_StaticFields
{
public:
	// T Singleton`1::instance
	MainUserInfo_t2526075922 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2778891315_StaticFields, ___instance_2)); }
	inline MainUserInfo_t2526075922 * get_instance_2() const { return ___instance_2; }
	inline MainUserInfo_t2526075922 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MainUserInfo_t2526075922 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
