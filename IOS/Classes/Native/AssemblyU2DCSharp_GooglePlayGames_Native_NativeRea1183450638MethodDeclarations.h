﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey6B__ctor_m1360868941 (U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B::<>m__4B()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_U3CU3Em__4B_m138173668 (U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
