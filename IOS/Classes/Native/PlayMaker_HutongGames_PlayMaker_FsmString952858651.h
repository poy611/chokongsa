﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmString
struct  FsmString_t952858651  : public NamedVariable_t3211770239
{
public:
	// System.String HutongGames.PlayMaker.FsmString::value
	String_t* ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmString_t952858651, ___value_5)); }
	inline String_t* get_value_5() const { return ___value_5; }
	inline String_t** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(String_t* value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier(&___value_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
