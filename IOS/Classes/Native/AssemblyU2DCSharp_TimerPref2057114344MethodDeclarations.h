﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimerPref
struct TimerPref_t2057114344;

#include "codegen/il2cpp-codegen.h"

// System.Void TimerPref::.ctor()
extern "C"  void TimerPref__ctor_m799065987 (TimerPref_t2057114344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerPref::Start()
extern "C"  void TimerPref_Start_m4041171075 (TimerPref_t2057114344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerPref::Update()
extern "C"  void TimerPref_Update_m728103914 (TimerPref_t2057114344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
