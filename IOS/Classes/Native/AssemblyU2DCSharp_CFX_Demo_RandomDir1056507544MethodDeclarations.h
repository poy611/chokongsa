﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RandomDir
struct CFX_Demo_RandomDir_t1056507544;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RandomDir::.ctor()
extern "C"  void CFX_Demo_RandomDir__ctor_m630121731 (CFX_Demo_RandomDir_t1056507544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDir::Start()
extern "C"  void CFX_Demo_RandomDir_Start_m3872226819 (CFX_Demo_RandomDir_t1056507544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
