﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1259563459(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1368711897 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m420802513_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::Invoke(T)
#define Func_2_Invoke_m89978174(__this, ___arg10, method) ((  ReflectionObject_t3124613658 * (*) (Func_2_t1368711897 *, Type_t *, const MethodInfo*))Func_2_Invoke_m1009799647_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2830727665(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1368711897 *, Type_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m4210401622(__this, ___result0, method) ((  ReflectionObject_t3124613658 * (*) (Func_2_t1368711897 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
