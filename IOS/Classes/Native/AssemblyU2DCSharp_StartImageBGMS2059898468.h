﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartImageBGMS
struct  StartImageBGMS_t2059898468  : public MonoBehaviour_t667441552
{
public:
	// System.String StartImageBGMS::nowScene
	String_t* ___nowScene_2;

public:
	inline static int32_t get_offset_of_nowScene_2() { return static_cast<int32_t>(offsetof(StartImageBGMS_t2059898468, ___nowScene_2)); }
	inline String_t* get_nowScene_2() const { return ___nowScene_2; }
	inline String_t** get_address_of_nowScene_2() { return &___nowScene_2; }
	inline void set_nowScene_2(String_t* value)
	{
		___nowScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
