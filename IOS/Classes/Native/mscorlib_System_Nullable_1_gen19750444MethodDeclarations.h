﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen19750444.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3432573423_gshared (Nullable_1_t19750444 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3432573423(__this, ___value0, method) ((  void (*) (Nullable_1_t19750444 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3432573423_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4227770655_gshared (Nullable_1_t19750444 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m4227770655(__this, method) ((  bool (*) (Nullable_1_t19750444 *, const MethodInfo*))Nullable_1_get_HasValue_m4227770655_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m198030772_gshared (Nullable_1_t19750444 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m198030772(__this, method) ((  int32_t (*) (Nullable_1_t19750444 *, const MethodInfo*))Nullable_1_get_Value_m198030772_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2945041154_gshared (Nullable_1_t19750444 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2945041154(__this, ___other0, method) ((  bool (*) (Nullable_1_t19750444 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2945041154_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m482920957_gshared (Nullable_1_t19750444 * __this, Nullable_1_t19750444  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m482920957(__this, ___other0, method) ((  bool (*) (Nullable_1_t19750444 *, Nullable_1_t19750444 , const MethodInfo*))Nullable_1_Equals_m482920957_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1122018522_gshared (Nullable_1_t19750444 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1122018522(__this, method) ((  int32_t (*) (Nullable_1_t19750444 *, const MethodInfo*))Nullable_1_GetHashCode_m1122018522_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3544243072_gshared (Nullable_1_t19750444 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3544243072(__this, method) ((  int32_t (*) (Nullable_1_t19750444 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3544243072_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1206152495_gshared (Nullable_1_t19750444 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1206152495(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t19750444 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1206152495_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4196764414_gshared (Nullable_1_t19750444 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m4196764414(__this, method) ((  String_t* (*) (Nullable_1_t19750444 *, const MethodInfo*))Nullable_1_ToString_m4196764414_gshared)(__this, method)
