﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t2530940439;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t3104490121;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::.ctor(System.IntPtr)
extern "C"  void RealTimeRoomResponse__ctor_m3369140640 (RealTimeRoomResponse_t2530940439 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::ResponseStatus()
extern "C"  int32_t RealTimeRoomResponse_ResponseStatus_m762564219 (RealTimeRoomResponse_t2530940439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::RequestSucceeded()
extern "C"  bool RealTimeRoomResponse_RequestSucceeded_m1037700078 (RealTimeRoomResponse_t2530940439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::Room()
extern "C"  NativeRealTimeRoom_t3104490121 * RealTimeRoomResponse_Room_m13501528 (RealTimeRoomResponse_t2530940439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoomResponse_CallDispose_m3989536784 (RealTimeRoomResponse_t2530940439 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::FromPointer(System.IntPtr)
extern "C"  RealTimeRoomResponse_t2530940439 * RealTimeRoomResponse_FromPointer_m700868499 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
