﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3689162173;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t120994540;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t607182279;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t4223261083;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t1443435833;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t1706220122;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t3873466740;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1409029100;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t997957744;
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t1767340410;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t914670954;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVariables
struct  FsmVariables_t963491929  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::floatVariables
	FsmFloatU5BU5D_t2945380875* ___floatVariables_0;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::intVariables
	FsmIntU5BU5D_t1976821196* ___intVariables_1;
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::boolVariables
	FsmBoolU5BU5D_t3689162173* ___boolVariables_2;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::stringVariables
	FsmStringU5BU5D_t2523845914* ___stringVariables_3;
	// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::vector2Variables
	FsmVector2U5BU5D_t120994540* ___vector2Variables_4;
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::vector3Variables
	FsmVector3U5BU5D_t607182279* ___vector3Variables_5;
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::colorVariables
	FsmColorU5BU5D_t530285832* ___colorVariables_6;
	// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::rectVariables
	FsmRectU5BU5D_t4223261083* ___rectVariables_7;
	// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::quaternionVariables
	FsmQuaternionU5BU5D_t1443435833* ___quaternionVariables_8;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::gameObjectVariables
	FsmGameObjectU5BU5D_t1706220122* ___gameObjectVariables_9;
	// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::objectVariables
	FsmObjectU5BU5D_t3873466740* ___objectVariables_10;
	// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::materialVariables
	FsmMaterialU5BU5D_t1409029100* ___materialVariables_11;
	// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::textureVariables
	FsmTextureU5BU5D_t997957744* ___textureVariables_12;
	// HutongGames.PlayMaker.FsmArray[] HutongGames.PlayMaker.FsmVariables::arrayVariables
	FsmArrayU5BU5D_t1767340410* ___arrayVariables_13;
	// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.FsmVariables::enumVariables
	FsmEnumU5BU5D_t914670954* ___enumVariables_14;
	// System.String[] HutongGames.PlayMaker.FsmVariables::categories
	StringU5BU5D_t4054002952* ___categories_15;
	// System.Int32[] HutongGames.PlayMaker.FsmVariables::variableCategoryIDs
	Int32U5BU5D_t3230847821* ___variableCategoryIDs_16;

public:
	inline static int32_t get_offset_of_floatVariables_0() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___floatVariables_0)); }
	inline FsmFloatU5BU5D_t2945380875* get_floatVariables_0() const { return ___floatVariables_0; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_floatVariables_0() { return &___floatVariables_0; }
	inline void set_floatVariables_0(FsmFloatU5BU5D_t2945380875* value)
	{
		___floatVariables_0 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariables_0, value);
	}

	inline static int32_t get_offset_of_intVariables_1() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___intVariables_1)); }
	inline FsmIntU5BU5D_t1976821196* get_intVariables_1() const { return ___intVariables_1; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_intVariables_1() { return &___intVariables_1; }
	inline void set_intVariables_1(FsmIntU5BU5D_t1976821196* value)
	{
		___intVariables_1 = value;
		Il2CppCodeGenWriteBarrier(&___intVariables_1, value);
	}

	inline static int32_t get_offset_of_boolVariables_2() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___boolVariables_2)); }
	inline FsmBoolU5BU5D_t3689162173* get_boolVariables_2() const { return ___boolVariables_2; }
	inline FsmBoolU5BU5D_t3689162173** get_address_of_boolVariables_2() { return &___boolVariables_2; }
	inline void set_boolVariables_2(FsmBoolU5BU5D_t3689162173* value)
	{
		___boolVariables_2 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariables_2, value);
	}

	inline static int32_t get_offset_of_stringVariables_3() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___stringVariables_3)); }
	inline FsmStringU5BU5D_t2523845914* get_stringVariables_3() const { return ___stringVariables_3; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_stringVariables_3() { return &___stringVariables_3; }
	inline void set_stringVariables_3(FsmStringU5BU5D_t2523845914* value)
	{
		___stringVariables_3 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariables_3, value);
	}

	inline static int32_t get_offset_of_vector2Variables_4() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___vector2Variables_4)); }
	inline FsmVector2U5BU5D_t120994540* get_vector2Variables_4() const { return ___vector2Variables_4; }
	inline FsmVector2U5BU5D_t120994540** get_address_of_vector2Variables_4() { return &___vector2Variables_4; }
	inline void set_vector2Variables_4(FsmVector2U5BU5D_t120994540* value)
	{
		___vector2Variables_4 = value;
		Il2CppCodeGenWriteBarrier(&___vector2Variables_4, value);
	}

	inline static int32_t get_offset_of_vector3Variables_5() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___vector3Variables_5)); }
	inline FsmVector3U5BU5D_t607182279* get_vector3Variables_5() const { return ___vector3Variables_5; }
	inline FsmVector3U5BU5D_t607182279** get_address_of_vector3Variables_5() { return &___vector3Variables_5; }
	inline void set_vector3Variables_5(FsmVector3U5BU5D_t607182279* value)
	{
		___vector3Variables_5 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variables_5, value);
	}

	inline static int32_t get_offset_of_colorVariables_6() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___colorVariables_6)); }
	inline FsmColorU5BU5D_t530285832* get_colorVariables_6() const { return ___colorVariables_6; }
	inline FsmColorU5BU5D_t530285832** get_address_of_colorVariables_6() { return &___colorVariables_6; }
	inline void set_colorVariables_6(FsmColorU5BU5D_t530285832* value)
	{
		___colorVariables_6 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariables_6, value);
	}

	inline static int32_t get_offset_of_rectVariables_7() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___rectVariables_7)); }
	inline FsmRectU5BU5D_t4223261083* get_rectVariables_7() const { return ___rectVariables_7; }
	inline FsmRectU5BU5D_t4223261083** get_address_of_rectVariables_7() { return &___rectVariables_7; }
	inline void set_rectVariables_7(FsmRectU5BU5D_t4223261083* value)
	{
		___rectVariables_7 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariables_7, value);
	}

	inline static int32_t get_offset_of_quaternionVariables_8() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___quaternionVariables_8)); }
	inline FsmQuaternionU5BU5D_t1443435833* get_quaternionVariables_8() const { return ___quaternionVariables_8; }
	inline FsmQuaternionU5BU5D_t1443435833** get_address_of_quaternionVariables_8() { return &___quaternionVariables_8; }
	inline void set_quaternionVariables_8(FsmQuaternionU5BU5D_t1443435833* value)
	{
		___quaternionVariables_8 = value;
		Il2CppCodeGenWriteBarrier(&___quaternionVariables_8, value);
	}

	inline static int32_t get_offset_of_gameObjectVariables_9() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___gameObjectVariables_9)); }
	inline FsmGameObjectU5BU5D_t1706220122* get_gameObjectVariables_9() const { return ___gameObjectVariables_9; }
	inline FsmGameObjectU5BU5D_t1706220122** get_address_of_gameObjectVariables_9() { return &___gameObjectVariables_9; }
	inline void set_gameObjectVariables_9(FsmGameObjectU5BU5D_t1706220122* value)
	{
		___gameObjectVariables_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectVariables_9, value);
	}

	inline static int32_t get_offset_of_objectVariables_10() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___objectVariables_10)); }
	inline FsmObjectU5BU5D_t3873466740* get_objectVariables_10() const { return ___objectVariables_10; }
	inline FsmObjectU5BU5D_t3873466740** get_address_of_objectVariables_10() { return &___objectVariables_10; }
	inline void set_objectVariables_10(FsmObjectU5BU5D_t3873466740* value)
	{
		___objectVariables_10 = value;
		Il2CppCodeGenWriteBarrier(&___objectVariables_10, value);
	}

	inline static int32_t get_offset_of_materialVariables_11() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___materialVariables_11)); }
	inline FsmMaterialU5BU5D_t1409029100* get_materialVariables_11() const { return ___materialVariables_11; }
	inline FsmMaterialU5BU5D_t1409029100** get_address_of_materialVariables_11() { return &___materialVariables_11; }
	inline void set_materialVariables_11(FsmMaterialU5BU5D_t1409029100* value)
	{
		___materialVariables_11 = value;
		Il2CppCodeGenWriteBarrier(&___materialVariables_11, value);
	}

	inline static int32_t get_offset_of_textureVariables_12() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___textureVariables_12)); }
	inline FsmTextureU5BU5D_t997957744* get_textureVariables_12() const { return ___textureVariables_12; }
	inline FsmTextureU5BU5D_t997957744** get_address_of_textureVariables_12() { return &___textureVariables_12; }
	inline void set_textureVariables_12(FsmTextureU5BU5D_t997957744* value)
	{
		___textureVariables_12 = value;
		Il2CppCodeGenWriteBarrier(&___textureVariables_12, value);
	}

	inline static int32_t get_offset_of_arrayVariables_13() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___arrayVariables_13)); }
	inline FsmArrayU5BU5D_t1767340410* get_arrayVariables_13() const { return ___arrayVariables_13; }
	inline FsmArrayU5BU5D_t1767340410** get_address_of_arrayVariables_13() { return &___arrayVariables_13; }
	inline void set_arrayVariables_13(FsmArrayU5BU5D_t1767340410* value)
	{
		___arrayVariables_13 = value;
		Il2CppCodeGenWriteBarrier(&___arrayVariables_13, value);
	}

	inline static int32_t get_offset_of_enumVariables_14() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___enumVariables_14)); }
	inline FsmEnumU5BU5D_t914670954* get_enumVariables_14() const { return ___enumVariables_14; }
	inline FsmEnumU5BU5D_t914670954** get_address_of_enumVariables_14() { return &___enumVariables_14; }
	inline void set_enumVariables_14(FsmEnumU5BU5D_t914670954* value)
	{
		___enumVariables_14 = value;
		Il2CppCodeGenWriteBarrier(&___enumVariables_14, value);
	}

	inline static int32_t get_offset_of_categories_15() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___categories_15)); }
	inline StringU5BU5D_t4054002952* get_categories_15() const { return ___categories_15; }
	inline StringU5BU5D_t4054002952** get_address_of_categories_15() { return &___categories_15; }
	inline void set_categories_15(StringU5BU5D_t4054002952* value)
	{
		___categories_15 = value;
		Il2CppCodeGenWriteBarrier(&___categories_15, value);
	}

	inline static int32_t get_offset_of_variableCategoryIDs_16() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929, ___variableCategoryIDs_16)); }
	inline Int32U5BU5D_t3230847821* get_variableCategoryIDs_16() const { return ___variableCategoryIDs_16; }
	inline Int32U5BU5D_t3230847821** get_address_of_variableCategoryIDs_16() { return &___variableCategoryIDs_16; }
	inline void set_variableCategoryIDs_16(Int32U5BU5D_t3230847821* value)
	{
		___variableCategoryIDs_16 = value;
		Il2CppCodeGenWriteBarrier(&___variableCategoryIDs_16, value);
	}
};

struct FsmVariables_t963491929_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmVariables::<GlobalVariablesSynced>k__BackingField
	bool ___U3CGlobalVariablesSyncedU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FsmVariables_t963491929_StaticFields, ___U3CGlobalVariablesSyncedU3Ek__BackingField_17)); }
	inline bool get_U3CGlobalVariablesSyncedU3Ek__BackingField_17() const { return ___U3CGlobalVariablesSyncedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17() { return &___U3CGlobalVariablesSyncedU3Ek__BackingField_17; }
	inline void set_U3CGlobalVariablesSyncedU3Ek__BackingField_17(bool value)
	{
		___U3CGlobalVariablesSyncedU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
