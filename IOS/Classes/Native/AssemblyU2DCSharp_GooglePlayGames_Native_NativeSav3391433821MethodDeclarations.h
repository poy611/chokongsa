﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80
struct U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::.ctor()
extern "C"  void U3CInternalManualOpenU3Ec__AnonStorey80__ctor_m1788323550 (U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::<>m__70()
extern "C"  void U3CInternalManualOpenU3Ec__AnonStorey80_U3CU3Em__70_m2900231872 (U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::<>m__71(System.Byte[],System.Byte[])
extern "C"  void U3CInternalManualOpenU3Ec__AnonStorey80_U3CU3Em__71_m1374167531 (U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821 * __this, ByteU5BU5D_t4260760469* ___originalData0, ByteU5BU5D_t4260760469* ___unmergedData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
