﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScaleTime
struct ScaleTime_t2527600261;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScaleTime::.ctor()
extern "C"  void ScaleTime__ctor_m420375633 (ScaleTime_t2527600261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::Reset()
extern "C"  void ScaleTime_Reset_m2361775870 (ScaleTime_t2527600261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::OnEnter()
extern "C"  void ScaleTime_OnEnter_m3940753384 (ScaleTime_t2527600261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::OnUpdate()
extern "C"  void ScaleTime_OnUpdate_m1037829979 (ScaleTime_t2527600261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::DoTimeScale()
extern "C"  void ScaleTime_DoTimeScale_m826146977 (ScaleTime_t2527600261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
