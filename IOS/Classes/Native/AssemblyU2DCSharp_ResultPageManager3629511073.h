﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image[]
struct ImageU5BU5D_t4039083868;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t3798907012;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Button
struct Button_t3896396478;
// ResponseResultAllData
struct ResponseResultAllData_t4071630637;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultPageManager
struct  ResultPageManager_t3629511073  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image[] ResultPageManager::scores
	ImageU5BU5D_t4039083868* ___scores_2;
	// UnityEngine.UI.Text[] ResultPageManager::texts
	TextU5BU5D_t3798907012* ___texts_3;
	// UnityEngine.UI.Text ResultPageManager::pageText
	Text_t9039225 * ___pageText_4;
	// UnityEngine.UI.Button ResultPageManager::prev
	Button_t3896396478 * ___prev_5;
	// UnityEngine.UI.Button ResultPageManager::next
	Button_t3896396478 * ___next_6;
	// System.Int32 ResultPageManager::currentPage
	int32_t ___currentPage_7;
	// System.Int32 ResultPageManager::LastPage
	int32_t ___LastPage_8;
	// System.Int32 ResultPageManager::trialFirstIndex
	int32_t ___trialFirstIndex_9;
	// System.Int32 ResultPageManager::reverse
	int32_t ___reverse_10;
	// ResponseResultAllData ResultPageManager::allData
	ResponseResultAllData_t4071630637 * ___allData_11;

public:
	inline static int32_t get_offset_of_scores_2() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___scores_2)); }
	inline ImageU5BU5D_t4039083868* get_scores_2() const { return ___scores_2; }
	inline ImageU5BU5D_t4039083868** get_address_of_scores_2() { return &___scores_2; }
	inline void set_scores_2(ImageU5BU5D_t4039083868* value)
	{
		___scores_2 = value;
		Il2CppCodeGenWriteBarrier(&___scores_2, value);
	}

	inline static int32_t get_offset_of_texts_3() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___texts_3)); }
	inline TextU5BU5D_t3798907012* get_texts_3() const { return ___texts_3; }
	inline TextU5BU5D_t3798907012** get_address_of_texts_3() { return &___texts_3; }
	inline void set_texts_3(TextU5BU5D_t3798907012* value)
	{
		___texts_3 = value;
		Il2CppCodeGenWriteBarrier(&___texts_3, value);
	}

	inline static int32_t get_offset_of_pageText_4() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___pageText_4)); }
	inline Text_t9039225 * get_pageText_4() const { return ___pageText_4; }
	inline Text_t9039225 ** get_address_of_pageText_4() { return &___pageText_4; }
	inline void set_pageText_4(Text_t9039225 * value)
	{
		___pageText_4 = value;
		Il2CppCodeGenWriteBarrier(&___pageText_4, value);
	}

	inline static int32_t get_offset_of_prev_5() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___prev_5)); }
	inline Button_t3896396478 * get_prev_5() const { return ___prev_5; }
	inline Button_t3896396478 ** get_address_of_prev_5() { return &___prev_5; }
	inline void set_prev_5(Button_t3896396478 * value)
	{
		___prev_5 = value;
		Il2CppCodeGenWriteBarrier(&___prev_5, value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___next_6)); }
	inline Button_t3896396478 * get_next_6() const { return ___next_6; }
	inline Button_t3896396478 ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(Button_t3896396478 * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier(&___next_6, value);
	}

	inline static int32_t get_offset_of_currentPage_7() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___currentPage_7)); }
	inline int32_t get_currentPage_7() const { return ___currentPage_7; }
	inline int32_t* get_address_of_currentPage_7() { return &___currentPage_7; }
	inline void set_currentPage_7(int32_t value)
	{
		___currentPage_7 = value;
	}

	inline static int32_t get_offset_of_LastPage_8() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___LastPage_8)); }
	inline int32_t get_LastPage_8() const { return ___LastPage_8; }
	inline int32_t* get_address_of_LastPage_8() { return &___LastPage_8; }
	inline void set_LastPage_8(int32_t value)
	{
		___LastPage_8 = value;
	}

	inline static int32_t get_offset_of_trialFirstIndex_9() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___trialFirstIndex_9)); }
	inline int32_t get_trialFirstIndex_9() const { return ___trialFirstIndex_9; }
	inline int32_t* get_address_of_trialFirstIndex_9() { return &___trialFirstIndex_9; }
	inline void set_trialFirstIndex_9(int32_t value)
	{
		___trialFirstIndex_9 = value;
	}

	inline static int32_t get_offset_of_reverse_10() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___reverse_10)); }
	inline int32_t get_reverse_10() const { return ___reverse_10; }
	inline int32_t* get_address_of_reverse_10() { return &___reverse_10; }
	inline void set_reverse_10(int32_t value)
	{
		___reverse_10 = value;
	}

	inline static int32_t get_offset_of_allData_11() { return static_cast<int32_t>(offsetof(ResultPageManager_t3629511073, ___allData_11)); }
	inline ResponseResultAllData_t4071630637 * get_allData_11() const { return ___allData_11; }
	inline ResponseResultAllData_t4071630637 ** get_address_of_allData_11() { return &___allData_11; }
	inline void set_allData_11(ResponseResultAllData_t4071630637 * value)
	{
		___allData_11 = value;
		Il2CppCodeGenWriteBarrier(&___allData_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
