﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<SetStepsAtLeast>c__AnonStorey59
struct U3CSetStepsAtLeastU3Ec__AnonStorey59_t2354526994;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t2513188365;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_A2513188365.h"

// System.Void GooglePlayGames.Native.NativeClient/<SetStepsAtLeast>c__AnonStorey59::.ctor()
extern "C"  void U3CSetStepsAtLeastU3Ec__AnonStorey59__ctor_m505930697 (U3CSetStepsAtLeastU3Ec__AnonStorey59_t2354526994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<SetStepsAtLeast>c__AnonStorey59::<>m__2F(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern "C"  void U3CSetStepsAtLeastU3Ec__AnonStorey59_U3CU3Em__2F_m3585379999 (U3CSetStepsAtLeastU3Ec__AnonStorey59_t2354526994 * __this, FetchResponse_t2513188365 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
