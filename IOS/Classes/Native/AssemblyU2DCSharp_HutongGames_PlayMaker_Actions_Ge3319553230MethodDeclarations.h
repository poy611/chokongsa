﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetChildNum
struct GetChildNum_t3319553230;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.GetChildNum::.ctor()
extern "C"  void GetChildNum__ctor_m3498776104 (GetChildNum_t3319553230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::Reset()
extern "C"  void GetChildNum_Reset_m1145209045 (GetChildNum_t3319553230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::OnEnter()
extern "C"  void GetChildNum_OnEnter_m3051139071 (GetChildNum_t3319553230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChildNum::DoGetChildNum(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GetChildNum_DoGetChildNum_m1852380072 (GetChildNum_t3319553230 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
