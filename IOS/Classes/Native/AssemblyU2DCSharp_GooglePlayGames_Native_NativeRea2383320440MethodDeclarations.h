﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D
struct U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
struct RoomInboxUIResponse_t1117529840;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_R1117529840.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D::.ctor()
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6D__ctor_m1155084531 (U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D::<>m__48(GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse)
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6D_U3CU3Em__48_m587589072 (U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * __this, RoomInboxUIResponse_t1117529840 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
