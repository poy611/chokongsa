﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1621686952.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2684317400_gshared (Enumerator_t1621686952 * __this, Dictionary_2_t304363560 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2684317400(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1621686952 *, Dictionary_2_t304363560 *, const MethodInfo*))Enumerator__ctor_m2684317400_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m205140361_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m205140361(__this, method) ((  Il2CppObject * (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m205140361_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1939311325_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1939311325(__this, method) ((  void (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1939311325_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3885062374_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3885062374(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3885062374_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2799068069_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2799068069(__this, method) ((  Il2CppObject * (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2799068069_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2303323383_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2303323383(__this, method) ((  Il2CppObject * (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2303323383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3030199497_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3030199497(__this, method) ((  bool (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_MoveNext_m3030199497_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
extern "C"  KeyValuePair_2_t203144266  Enumerator_get_Current_m443046727_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m443046727(__this, method) ((  KeyValuePair_2_t203144266  (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_get_Current_m443046727_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1115479638_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1115479638(__this, method) ((  Il2CppObject * (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_get_CurrentKey_m1115479638_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3095157242_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3095157242(__this, method) ((  int32_t (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_get_CurrentValue_m3095157242_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Reset()
extern "C"  void Enumerator_Reset_m1798260010_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1798260010(__this, method) ((  void (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_Reset_m1798260010_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4190145971_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4190145971(__this, method) ((  void (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_VerifyState_m4190145971_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m795826267_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m795826267(__this, method) ((  void (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_VerifyCurrent_m795826267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Dispose()
extern "C"  void Enumerator_Dispose_m4074445690_gshared (Enumerator_t1621686952 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4074445690(__this, method) ((  void (*) (Enumerator_t1621686952 *, const MethodInfo*))Enumerator_Dispose_m4074445690_gshared)(__this, method)
