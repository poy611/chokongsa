﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVelocity2d
struct GetVelocity2d_t623964627;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::.ctor()
extern "C"  void GetVelocity2d__ctor_m2130033859 (GetVelocity2d_t623964627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::Reset()
extern "C"  void GetVelocity2d_Reset_m4071434096 (GetVelocity2d_t623964627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnEnter()
extern "C"  void GetVelocity2d_OnEnter_m1949834202 (GetVelocity2d_t623964627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnUpdate()
extern "C"  void GetVelocity2d_OnUpdate_m3743844777 (GetVelocity2d_t623964627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::DoGetVelocity()
extern "C"  void GetVelocity2d_DoGetVelocity_m1101566121 (GetVelocity2d_t623964627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
