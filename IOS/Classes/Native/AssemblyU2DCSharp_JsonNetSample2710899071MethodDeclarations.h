﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonNetSample
struct JsonNetSample_t2710899071;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JsonNetSample::.ctor()
extern "C"  void JsonNetSample__ctor_m36607692 (JsonNetSample_t2710899071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample::Start()
extern "C"  void JsonNetSample_Start_m3278712780 (JsonNetSample_t2710899071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample::WriteLine(System.String)
extern "C"  void JsonNetSample_WriteLine_m749064965 (JsonNetSample_t2710899071 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample::SerailizeJson()
extern "C"  void JsonNetSample_SerailizeJson_m1224567778 (JsonNetSample_t2710899071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample::DeserializeJson()
extern "C"  void JsonNetSample_DeserializeJson_m386165745 (JsonNetSample_t2710899071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample::LinqToJson()
extern "C"  void JsonNetSample_LinqToJson_m250171675 (JsonNetSample_t2710899071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
