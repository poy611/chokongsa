﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetLogLevel
struct  NetworkSetLogLevel_t1470191228  : public FsmStateAction_t2366529033
{
public:
	// UnityEngine.NetworkLogLevel HutongGames.PlayMaker.Actions.NetworkSetLogLevel::logLevel
	int32_t ___logLevel_11;

public:
	inline static int32_t get_offset_of_logLevel_11() { return static_cast<int32_t>(offsetof(NetworkSetLogLevel_t1470191228, ___logLevel_11)); }
	inline int32_t get_logLevel_11() const { return ___logLevel_11; }
	inline int32_t* get_address_of_logLevel_11() { return &___logLevel_11; }
	inline void set_logLevel_11(int32_t value)
	{
		___logLevel_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
