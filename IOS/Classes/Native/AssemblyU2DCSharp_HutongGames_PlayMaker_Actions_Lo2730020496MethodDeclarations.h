﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LoadLevelNum
struct LoadLevelNum_t2730020496;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::.ctor()
extern "C"  void LoadLevelNum__ctor_m2451390934 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::Reset()
extern "C"  void LoadLevelNum_Reset_m97823875 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::OnEnter()
extern "C"  void LoadLevelNum_OnEnter_m1536337965 (LoadLevelNum_t2730020496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
