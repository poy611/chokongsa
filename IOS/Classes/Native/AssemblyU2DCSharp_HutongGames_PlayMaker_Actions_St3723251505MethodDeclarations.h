﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringChanged
struct StringChanged_t3723251505;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringChanged::.ctor()
extern "C"  void StringChanged__ctor_m3383396645 (StringChanged_t3723251505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::Reset()
extern "C"  void StringChanged_Reset_m1029829586 (StringChanged_t3723251505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnEnter()
extern "C"  void StringChanged_OnEnter_m3840628668 (StringChanged_t3723251505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnUpdate()
extern "C"  void StringChanged_OnUpdate_m2228931079 (StringChanged_t3723251505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
