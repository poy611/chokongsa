﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties
struct NetworkGetConnectedPlayerProperties_t4024683587;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::.ctor()
extern "C"  void NetworkGetConnectedPlayerProperties__ctor_m1757610579 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::Reset()
extern "C"  void NetworkGetConnectedPlayerProperties_Reset_m3699010816 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::OnEnter()
extern "C"  void NetworkGetConnectedPlayerProperties_OnEnter_m533347690 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::getPlayerProperties()
extern "C"  void NetworkGetConnectedPlayerProperties_getPlayerProperties_m2457282427 (NetworkGetConnectedPlayerProperties_t4024683587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
