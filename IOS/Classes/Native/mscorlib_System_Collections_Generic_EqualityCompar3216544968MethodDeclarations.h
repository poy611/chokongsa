﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.ResolverContractKey>
struct DefaultComparer_t3216544968;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.ResolverContractKey>::.ctor()
extern "C"  void DefaultComparer__ctor_m430567922_gshared (DefaultComparer_t3216544968 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m430567922(__this, method) ((  void (*) (DefaultComparer_t3216544968 *, const MethodInfo*))DefaultComparer__ctor_m430567922_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.ResolverContractKey>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m385412217_gshared (DefaultComparer_t3216544968 * __this, ResolverContractKey_t473801005  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m385412217(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3216544968 *, ResolverContractKey_t473801005 , const MethodInfo*))DefaultComparer_GetHashCode_m385412217_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Serialization.ResolverContractKey>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1567796547_gshared (DefaultComparer_t3216544968 * __this, ResolverContractKey_t473801005  ___x0, ResolverContractKey_t473801005  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1567796547(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3216544968 *, ResolverContractKey_t473801005 , ResolverContractKey_t473801005 , const MethodInfo*))DefaultComparer_Equals_m1567796547_gshared)(__this, ___x0, ___y1, method)
