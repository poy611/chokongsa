﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeStrim
struct YoutubeStrim_t2074917330;

#include "codegen/il2cpp-codegen.h"

// System.Void YoutubeStrim::.ctor()
extern "C"  void YoutubeStrim__ctor_m3835307273 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeStrim::Start()
extern "C"  void YoutubeStrim_Start_m2782445065 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeStrim::BackButtonClick()
extern "C"  void YoutubeStrim_BackButtonClick_m740836598 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
