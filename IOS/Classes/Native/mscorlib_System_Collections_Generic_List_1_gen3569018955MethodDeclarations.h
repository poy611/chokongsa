﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::.ctor()
#define List_1__ctor_m2598070516(__this, method) ((  void (*) (List_1_t3569018955 *, const MethodInfo*))List_1__ctor_m1457888136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3719877588(__this, ___collection0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1384868247_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::.ctor(System.Int32)
#define List_1__ctor_m784311452(__this, ___capacity0, method) ((  void (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::.cctor()
#define List_1__cctor_m110001474(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4281458525(__this, method) ((  Il2CppObject* (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3254389721(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3569018955 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1399926676(__this, method) ((  Il2CppObject * (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3055579997(__this, ___item0, method) ((  int32_t (*) (List_1_t3569018955 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2484194575(__this, ___item0, method) ((  bool (*) (List_1_t3569018955 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2936910645(__this, ___item0, method) ((  int32_t (*) (List_1_t3569018955 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2017015008(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3569018955 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m359008328(__this, ___item0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4010276432(__this, method) ((  bool (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m54082541(__this, method) ((  bool (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4075920473(__this, method) ((  Il2CppObject * (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1115698110(__this, method) ((  bool (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m628546427(__this, method) ((  bool (*) (List_1_t3569018955 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1015012256(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1935124663(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3569018955 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Add(T)
#define List_1_Add_m2292431332(__this, ___item0, method) ((  void (*) (List_1_t3569018955 *, Invitation_t2200833403 *, const MethodInfo*))List_1_Add_m1152248952_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1438076815(__this, ___newCount0, method) ((  void (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2962674936(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3569018955 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3308104333(__this, ___collection0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2569076557(__this, ___enumerable0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4177974058(__this, ___collection0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::AsReadOnly()
#define List_1_AsReadOnly_m2029424447(__this, method) ((  ReadOnlyCollection_1_t3757910939 * (*) (List_1_t3569018955 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Clear()
#define List_1_Clear_m3521315830(__this, method) ((  void (*) (List_1_t3569018955 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Contains(T)
#define List_1_Contains_m1683768996(__this, ___item0, method) ((  bool (*) (List_1_t3569018955 *, Invitation_t2200833403 *, const MethodInfo*))List_1_Contains_m786374326_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CopyTo(T[])
#define List_1_CopyTo_m2674131315(__this, ___array0, method) ((  void (*) (List_1_t3569018955 *, InvitationU5BU5D_t4211826234*, const MethodInfo*))List_1_CopyTo_m3016810556_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3333049924(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3569018955 *, InvitationU5BU5D_t4211826234*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Find(System.Predicate`1<T>)
#define List_1_Find_m171895524(__this, ___match0, method) ((  Invitation_t2200833403 * (*) (List_1_t3569018955 *, Predicate_1_t1811890286 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3402734879(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1811890286 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2641782340(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3569018955 *, int32_t, int32_t, Predicate_1_t1811890286 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::GetEnumerator()
#define List_1_GetEnumerator_m219695585(__this, method) ((  Enumerator_t3588691725  (*) (List_1_t3569018955 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::IndexOf(T)
#define List_1_IndexOf_m2702498760(__this, ___item0, method) ((  int32_t (*) (List_1_t3569018955 *, Invitation_t2200833403 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m620863643(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3569018955 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3079417108(__this, ___index0, method) ((  void (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Insert(System.Int32,T)
#define List_1_Insert_m2219779515(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3569018955 *, int32_t, Invitation_t2200833403 *, const MethodInfo*))List_1_Insert_m3135247846_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1339133424(__this, ___collection0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Remove(T)
#define List_1_Remove_m2021285791(__this, ___item0, method) ((  bool (*) (List_1_t3569018955 *, Invitation_t2200833403 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2080972171(__this, ___match0, method) ((  int32_t (*) (List_1_t3569018955 *, Predicate_1_t1811890286 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m93632385(__this, ___index0, method) ((  void (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4092449316_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1383821732(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3569018955 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Reverse()
#define List_1_Reverse_m1212970315(__this, method) ((  void (*) (List_1_t3569018955 *, const MethodInfo*))List_1_Reverse_m2062055293_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Sort()
#define List_1_Sort_m3899967031(__this, method) ((  void (*) (List_1_t3569018955 *, const MethodInfo*))List_1_Sort_m3444130117_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2542119309(__this, ___comparer0, method) ((  void (*) (List_1_t3569018955 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1344945994(__this, ___comparison0, method) ((  void (*) (List_1_t3569018955 *, Comparison_1_t917194590 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::ToArray()
#define List_1_ToArray_m581161188(__this, method) ((  InvitationU5BU5D_t4211826234* (*) (List_1_t3569018955 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::TrimExcess()
#define List_1_TrimExcess_m418259088(__this, method) ((  void (*) (List_1_t3569018955 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::get_Capacity()
#define List_1_get_Capacity_m4031885304(__this, method) ((  int32_t (*) (List_1_t3569018955 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2943232929(__this, ___value0, method) ((  void (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::get_Count()
#define List_1_get_Count_m1990390451(__this, method) ((  int32_t (*) (List_1_t3569018955 *, const MethodInfo*))List_1_get_Count_m2222238344_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::get_Item(System.Int32)
#define List_1_get_Item_m1552240965(__this, ___index0, method) ((  Invitation_t2200833403 * (*) (List_1_t3569018955 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Invitation>::set_Item(System.Int32,T)
#define List_1_set_Item_m418964434(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3569018955 *, int32_t, Invitation_t2200833403 *, const MethodInfo*))List_1_set_Item_m2510829103_gshared)(__this, ___index0, ___value1, method)
