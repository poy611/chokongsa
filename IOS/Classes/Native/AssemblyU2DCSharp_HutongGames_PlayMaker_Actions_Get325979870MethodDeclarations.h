﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTransform
struct GetTransform_t325979870;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTransform::.ctor()
extern "C"  void GetTransform__ctor_m3887102280 (GetTransform_t325979870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::Reset()
extern "C"  void GetTransform_Reset_m1533535221 (GetTransform_t325979870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnEnter()
extern "C"  void GetTransform_OnEnter_m2570439455 (GetTransform_t325979870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnUpdate()
extern "C"  void GetTransform_OnUpdate_m1507771140 (GetTransform_t325979870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::DoGetGameObjectName()
extern "C"  void GetTransform_DoGetGameObjectName_m1570979725 (GetTransform_t325979870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
