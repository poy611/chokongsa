﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetJointConnectedBody
struct SetJointConnectedBody_t2876069553;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::.ctor()
extern "C"  void SetJointConnectedBody__ctor_m3802958245 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::Reset()
extern "C"  void SetJointConnectedBody_Reset_m1449391186 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetJointConnectedBody::OnEnter()
extern "C"  void SetJointConnectedBody_OnEnter_m3312400444 (SetJointConnectedBody_t2876069553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
