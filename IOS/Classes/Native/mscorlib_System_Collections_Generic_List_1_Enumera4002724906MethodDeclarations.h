﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3143762761(__this, ___l0, method) ((  void (*) (Enumerator_t4002724906 *, List_1_t3983052136 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3953682281(__this, method) ((  void (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1799713365(__this, method) ((  Il2CppObject * (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::Dispose()
#define Enumerator_Dispose_m159986542(__this, method) ((  void (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::VerifyState()
#define Enumerator_VerifyState_m1526303655(__this, method) ((  void (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::MoveNext()
#define Enumerator_MoveNext_m2908599253(__this, method) ((  bool (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::get_Current()
#define Enumerator_get_Current_m4149685790(__this, method) ((  FsmLogEntry_t2614866584 * (*) (Enumerator_t4002724906 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
