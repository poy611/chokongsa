﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonNetSample/Movie
struct Movie_t147243648;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"

// System.Void JsonNetSample/Movie::.ctor()
extern "C"  void Movie__ctor_m3834257643 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonNetSample/Movie::get_Name()
extern "C"  String_t* Movie_get_Name_m3943497034 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_Name(System.String)
extern "C"  void Movie_set_Name_m3152406369 (Movie_t147243648 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonNetSample/Movie::get_Description()
extern "C"  String_t* Movie_get_Description_m2265979839 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_Description(System.String)
extern "C"  void Movie_set_Description_m4116036634 (Movie_t147243648 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonNetSample/Movie::get_Classification()
extern "C"  String_t* Movie_get_Classification_m3716793957 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_Classification(System.String)
extern "C"  void Movie_set_Classification_m1064899302 (Movie_t147243648 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonNetSample/Movie::get_Studio()
extern "C"  String_t* Movie_get_Studio_m1576427093 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_Studio(System.String)
extern "C"  void Movie_set_Studio_m3834124790 (Movie_t147243648 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> JsonNetSample/Movie::get_ReleaseDate()
extern "C"  Nullable_1_t72820554  Movie_get_ReleaseDate_m3164204675 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_ReleaseDate(System.Nullable`1<System.DateTime>)
extern "C"  void Movie_set_ReleaseDate_m3422222994 (Movie_t147243648 * __this, Nullable_1_t72820554  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> JsonNetSample/Movie::get_ReleaseCountries()
extern "C"  List_1_t1375417109 * Movie_get_ReleaseCountries_m835213086 (Movie_t147243648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonNetSample/Movie::set_ReleaseCountries(System.Collections.Generic.List`1<System.String>)
extern "C"  void Movie_set_ReleaseCountries_m3474906967 (Movie_t147243648 * __this, List_1_t1375417109 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
