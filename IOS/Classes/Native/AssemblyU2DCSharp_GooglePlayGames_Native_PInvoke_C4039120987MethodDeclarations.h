﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m4016861356_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m4016861356(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m4016861356_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m1336680644_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m1336680644(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m1336680644_gshared)(__this, method)
