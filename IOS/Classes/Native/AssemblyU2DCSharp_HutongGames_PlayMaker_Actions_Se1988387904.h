﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3178143027.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightCookie
struct  SetLightCookie_t1988387904  : public ComponentAction_1_t3178143027
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightCookie::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetLightCookie::lightCookie
	FsmTexture_t3073272573 * ___lightCookie_14;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetLightCookie_t1988387904, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_lightCookie_14() { return static_cast<int32_t>(offsetof(SetLightCookie_t1988387904, ___lightCookie_14)); }
	inline FsmTexture_t3073272573 * get_lightCookie_14() const { return ___lightCookie_14; }
	inline FsmTexture_t3073272573 ** get_address_of_lightCookie_14() { return &___lightCookie_14; }
	inline void set_lightCookie_14(FsmTexture_t3073272573 * value)
	{
		___lightCookie_14 = value;
		Il2CppCodeGenWriteBarrier(&___lightCookie_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
