﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionReport/<>c__DisplayClass2
struct  U3CU3Ec__DisplayClass2_t3035583220  : public Il2CppObject
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.ActionReport/<>c__DisplayClass2::fsm
	PlayMakerFSM_t3799847376 * ___fsm_0;

public:
	inline static int32_t get_offset_of_fsm_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t3035583220, ___fsm_0)); }
	inline PlayMakerFSM_t3799847376 * get_fsm_0() const { return ___fsm_0; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsm_0() { return &___fsm_0; }
	inline void set_fsm_0(PlayMakerFSM_t3799847376 * value)
	{
		___fsm_0 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
