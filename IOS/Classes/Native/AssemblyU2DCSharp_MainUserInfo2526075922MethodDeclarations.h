﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainUserInfo
struct MainUserInfo_t2526075922;

#include "codegen/il2cpp-codegen.h"

// System.Void MainUserInfo::.ctor()
extern "C"  void MainUserInfo__ctor_m2644019913 (MainUserInfo_t2526075922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
