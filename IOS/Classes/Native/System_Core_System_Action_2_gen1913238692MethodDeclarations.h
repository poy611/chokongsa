﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1729022911MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1274431257(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1913238692 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m907218493_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>::Invoke(T1,T2)
#define Action_2_Invoke_m514882738(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1913238692 *, int32_t, PlayerStats_t60064856 *, const MethodInfo*))Action_2_Invoke_m970130894_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m4095175553(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1913238692 *, int32_t, PlayerStats_t60064856 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1465736309_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1578580665(__this, ___result0, method) ((  void (*) (Action_2_t1913238692 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2247698125_gshared)(__this, ___result0, method)
