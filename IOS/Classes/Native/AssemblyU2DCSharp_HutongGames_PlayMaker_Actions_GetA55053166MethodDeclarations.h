﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct GetAnimatorIsHuman_t55053166;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::.ctor()
extern "C"  void GetAnimatorIsHuman__ctor_m57879736 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::Reset()
extern "C"  void GetAnimatorIsHuman_Reset_m1999279973 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::OnEnter()
extern "C"  void GetAnimatorIsHuman_OnEnter_m3474547343 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::DoCheckIsHuman()
extern "C"  void GetAnimatorIsHuman_DoCheckIsHuman_m3251639922 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
