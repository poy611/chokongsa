﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIText
struct GUIText_t3371372606;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t2391043192;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_New
struct  CFX_Demo_New_t4019803630  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GUIText CFX_Demo_New::EffectLabel
	GUIText_t3371372606 * ___EffectLabel_2;
	// UnityEngine.GUIText CFX_Demo_New::EffectIndexLabel
	GUIText_t3371372606 * ___EffectIndexLabel_3;
	// UnityEngine.Renderer CFX_Demo_New::groundRenderer
	Renderer_t3076687687 * ___groundRenderer_4;
	// UnityEngine.Collider CFX_Demo_New::groundCollider
	Collider_t2939674232 * ___groundCollider_5;
	// UnityEngine.GameObject[] CFX_Demo_New::ParticleExamples
	GameObjectU5BU5D_t2662109048* ___ParticleExamples_6;
	// System.Int32 CFX_Demo_New::exampleIndex
	int32_t ___exampleIndex_7;
	// System.Boolean CFX_Demo_New::slowMo
	bool ___slowMo_8;
	// UnityEngine.Vector3 CFX_Demo_New::defaultCamPosition
	Vector3_t4282066566  ___defaultCamPosition_9;
	// UnityEngine.Quaternion CFX_Demo_New::defaultCamRotation
	Quaternion_t1553702882  ___defaultCamRotation_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CFX_Demo_New::onScreenParticles
	List_1_t747900261 * ___onScreenParticles_11;

public:
	inline static int32_t get_offset_of_EffectLabel_2() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___EffectLabel_2)); }
	inline GUIText_t3371372606 * get_EffectLabel_2() const { return ___EffectLabel_2; }
	inline GUIText_t3371372606 ** get_address_of_EffectLabel_2() { return &___EffectLabel_2; }
	inline void set_EffectLabel_2(GUIText_t3371372606 * value)
	{
		___EffectLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___EffectLabel_2, value);
	}

	inline static int32_t get_offset_of_EffectIndexLabel_3() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___EffectIndexLabel_3)); }
	inline GUIText_t3371372606 * get_EffectIndexLabel_3() const { return ___EffectIndexLabel_3; }
	inline GUIText_t3371372606 ** get_address_of_EffectIndexLabel_3() { return &___EffectIndexLabel_3; }
	inline void set_EffectIndexLabel_3(GUIText_t3371372606 * value)
	{
		___EffectIndexLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___EffectIndexLabel_3, value);
	}

	inline static int32_t get_offset_of_groundRenderer_4() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___groundRenderer_4)); }
	inline Renderer_t3076687687 * get_groundRenderer_4() const { return ___groundRenderer_4; }
	inline Renderer_t3076687687 ** get_address_of_groundRenderer_4() { return &___groundRenderer_4; }
	inline void set_groundRenderer_4(Renderer_t3076687687 * value)
	{
		___groundRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___groundRenderer_4, value);
	}

	inline static int32_t get_offset_of_groundCollider_5() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___groundCollider_5)); }
	inline Collider_t2939674232 * get_groundCollider_5() const { return ___groundCollider_5; }
	inline Collider_t2939674232 ** get_address_of_groundCollider_5() { return &___groundCollider_5; }
	inline void set_groundCollider_5(Collider_t2939674232 * value)
	{
		___groundCollider_5 = value;
		Il2CppCodeGenWriteBarrier(&___groundCollider_5, value);
	}

	inline static int32_t get_offset_of_ParticleExamples_6() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___ParticleExamples_6)); }
	inline GameObjectU5BU5D_t2662109048* get_ParticleExamples_6() const { return ___ParticleExamples_6; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_ParticleExamples_6() { return &___ParticleExamples_6; }
	inline void set_ParticleExamples_6(GameObjectU5BU5D_t2662109048* value)
	{
		___ParticleExamples_6 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleExamples_6, value);
	}

	inline static int32_t get_offset_of_exampleIndex_7() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___exampleIndex_7)); }
	inline int32_t get_exampleIndex_7() const { return ___exampleIndex_7; }
	inline int32_t* get_address_of_exampleIndex_7() { return &___exampleIndex_7; }
	inline void set_exampleIndex_7(int32_t value)
	{
		___exampleIndex_7 = value;
	}

	inline static int32_t get_offset_of_slowMo_8() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___slowMo_8)); }
	inline bool get_slowMo_8() const { return ___slowMo_8; }
	inline bool* get_address_of_slowMo_8() { return &___slowMo_8; }
	inline void set_slowMo_8(bool value)
	{
		___slowMo_8 = value;
	}

	inline static int32_t get_offset_of_defaultCamPosition_9() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___defaultCamPosition_9)); }
	inline Vector3_t4282066566  get_defaultCamPosition_9() const { return ___defaultCamPosition_9; }
	inline Vector3_t4282066566 * get_address_of_defaultCamPosition_9() { return &___defaultCamPosition_9; }
	inline void set_defaultCamPosition_9(Vector3_t4282066566  value)
	{
		___defaultCamPosition_9 = value;
	}

	inline static int32_t get_offset_of_defaultCamRotation_10() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___defaultCamRotation_10)); }
	inline Quaternion_t1553702882  get_defaultCamRotation_10() const { return ___defaultCamRotation_10; }
	inline Quaternion_t1553702882 * get_address_of_defaultCamRotation_10() { return &___defaultCamRotation_10; }
	inline void set_defaultCamRotation_10(Quaternion_t1553702882  value)
	{
		___defaultCamRotation_10 = value;
	}

	inline static int32_t get_offset_of_onScreenParticles_11() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630, ___onScreenParticles_11)); }
	inline List_1_t747900261 * get_onScreenParticles_11() const { return ___onScreenParticles_11; }
	inline List_1_t747900261 ** get_address_of_onScreenParticles_11() { return &___onScreenParticles_11; }
	inline void set_onScreenParticles_11(List_1_t747900261 * value)
	{
		___onScreenParticles_11 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenParticles_11, value);
	}
};

struct CFX_Demo_New_t4019803630_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.GameObject> CFX_Demo_New::<>f__am$cacheA
	Comparison_1_t2391043192 * ___U3CU3Ef__amU24cacheA_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_12() { return static_cast<int32_t>(offsetof(CFX_Demo_New_t4019803630_StaticFields, ___U3CU3Ef__amU24cacheA_12)); }
	inline Comparison_1_t2391043192 * get_U3CU3Ef__amU24cacheA_12() const { return ___U3CU3Ef__amU24cacheA_12; }
	inline Comparison_1_t2391043192 ** get_address_of_U3CU3Ef__amU24cacheA_12() { return &___U3CU3Ef__amU24cacheA_12; }
	inline void set_U3CU3Ef__amU24cacheA_12(Comparison_1_t2391043192 * value)
	{
		___U3CU3Ef__amU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
