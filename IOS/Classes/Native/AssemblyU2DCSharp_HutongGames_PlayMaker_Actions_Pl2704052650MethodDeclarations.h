﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsSetInt
struct PlayerPrefsSetInt_t2704052650;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetInt::.ctor()
extern "C"  void PlayerPrefsSetInt__ctor_m2937539276 (PlayerPrefsSetInt_t2704052650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetInt::Reset()
extern "C"  void PlayerPrefsSetInt_Reset_m583972217 (PlayerPrefsSetInt_t2704052650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetInt::OnEnter()
extern "C"  void PlayerPrefsSetInt_OnEnter_m573459363 (PlayerPrefsSetInt_t2704052650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
