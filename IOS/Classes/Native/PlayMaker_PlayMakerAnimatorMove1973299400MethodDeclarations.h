﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorMove
struct PlayMakerAnimatorMove_t1973299400;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerAnimatorMove::OnAnimatorMove()
extern "C"  void PlayMakerAnimatorMove_OnAnimatorMove_m2999358618 (PlayMakerAnimatorMove_t1973299400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMove::.ctor()
extern "C"  void PlayMakerAnimatorMove__ctor_m1475188513 (PlayMakerAnimatorMove_t1973299400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
