﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey79
struct U3CRealTimeMessageReceivedU3Ec__AnonStorey79_t1830858054;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey79::.ctor()
extern "C"  void U3CRealTimeMessageReceivedU3Ec__AnonStorey79__ctor_m748576277 (U3CRealTimeMessageReceivedU3Ec__AnonStorey79_t1830858054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey79::<>m__54()
extern "C"  void U3CRealTimeMessageReceivedU3Ec__AnonStorey79_U3CU3Em__54_m135459453 (U3CRealTimeMessageReceivedU3Ec__AnonStorey79_t1830858054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
