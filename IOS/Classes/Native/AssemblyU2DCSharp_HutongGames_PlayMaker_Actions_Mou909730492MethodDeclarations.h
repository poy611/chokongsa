﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePickEvent
struct MousePickEvent_t909730492;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::.ctor()
extern "C"  void MousePickEvent__ctor_m884406570 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::Reset()
extern "C"  void MousePickEvent_Reset_m2825806807 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnEnter()
extern "C"  void MousePickEvent_OnEnter_m3197885057 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnUpdate()
extern "C"  void MousePickEvent_OnUpdate_m3778715618 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::DoMousePickEvent()
extern "C"  void MousePickEvent_DoMousePickEvent_m432495321 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.MousePickEvent::DoRaycast()
extern "C"  bool MousePickEvent_DoRaycast_m2927267442 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.MousePickEvent::ErrorCheck()
extern "C"  String_t* MousePickEvent_ErrorCheck_m2951563357 (MousePickEvent_t909730492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
