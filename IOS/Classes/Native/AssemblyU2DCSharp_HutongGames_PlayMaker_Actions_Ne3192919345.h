﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning
struct  NetworkGetIsMessageQueueRunning_t3192919345  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkGetIsMessageQueueRunning::result
	FsmBool_t1075959796 * ___result_11;

public:
	inline static int32_t get_offset_of_result_11() { return static_cast<int32_t>(offsetof(NetworkGetIsMessageQueueRunning_t3192919345, ___result_11)); }
	inline FsmBool_t1075959796 * get_result_11() const { return ___result_11; }
	inline FsmBool_t1075959796 ** get_address_of_result_11() { return &___result_11; }
	inline void set_result_11(FsmBool_t1075959796 * value)
	{
		___result_11 = value;
		Il2CppCodeGenWriteBarrier(&___result_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
