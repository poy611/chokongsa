﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m1447645055_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m1447645055(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m1447645055_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451(__this, ___val10, ___val21, ___val32, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451_gshared)(__this, ___val10, ___val21, ___val32, method)
