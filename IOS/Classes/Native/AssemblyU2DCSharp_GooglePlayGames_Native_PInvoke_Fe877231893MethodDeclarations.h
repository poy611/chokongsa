﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse
struct FetchScoreSummaryResponse_t877231893;
// GooglePlayGames.Native.PInvoke.NativeScoreSummary
struct NativeScoreSummary_t3571466807;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"

// System.Void GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse::.ctor(System.IntPtr)
extern "C"  void FetchScoreSummaryResponse__ctor_m2985022725 (FetchScoreSummaryResponse_t877231893 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchScoreSummaryResponse_CallDispose_m1545563467 (FetchScoreSummaryResponse_t877231893 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse::GetStatus()
extern "C"  int32_t FetchScoreSummaryResponse_GetStatus_m4167489836 (FetchScoreSummaryResponse_t877231893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeScoreSummary GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse::GetScoreSummary()
extern "C"  NativeScoreSummary_t3571466807 * FetchScoreSummaryResponse_GetScoreSummary_m3136826322 (FetchScoreSummaryResponse_t877231893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse::FromPointer(System.IntPtr)
extern "C"  FetchScoreSummaryResponse_t877231893 * FetchScoreSummaryResponse_FromPointer_m36732773 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
