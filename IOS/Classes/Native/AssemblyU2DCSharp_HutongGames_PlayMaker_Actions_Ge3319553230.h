﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetChildNum
struct  GetChildNum_t3319553230  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetChildNum::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetChildNum::childIndex
	FsmInt_t1596138449 * ___childIndex_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetChildNum::store
	FsmGameObject_t1697147867 * ___store_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_childIndex_12() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___childIndex_12)); }
	inline FsmInt_t1596138449 * get_childIndex_12() const { return ___childIndex_12; }
	inline FsmInt_t1596138449 ** get_address_of_childIndex_12() { return &___childIndex_12; }
	inline void set_childIndex_12(FsmInt_t1596138449 * value)
	{
		___childIndex_12 = value;
		Il2CppCodeGenWriteBarrier(&___childIndex_12, value);
	}

	inline static int32_t get_offset_of_store_13() { return static_cast<int32_t>(offsetof(GetChildNum_t3319553230, ___store_13)); }
	inline FsmGameObject_t1697147867 * get_store_13() const { return ___store_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_store_13() { return &___store_13; }
	inline void set_store_13(FsmGameObject_t1697147867 * value)
	{
		___store_13 = value;
		Il2CppCodeGenWriteBarrier(&___store_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
