﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t1706220122;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomGameObject
struct  SelectRandomGameObject_t1505534456  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.Actions.SelectRandomGameObject::gameObjects
	FsmGameObjectU5BU5D_t1706220122* ___gameObjects_11;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomGameObject::weights
	FsmFloatU5BU5D_t2945380875* ___weights_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SelectRandomGameObject::storeGameObject
	FsmGameObject_t1697147867 * ___storeGameObject_13;

public:
	inline static int32_t get_offset_of_gameObjects_11() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t1505534456, ___gameObjects_11)); }
	inline FsmGameObjectU5BU5D_t1706220122* get_gameObjects_11() const { return ___gameObjects_11; }
	inline FsmGameObjectU5BU5D_t1706220122** get_address_of_gameObjects_11() { return &___gameObjects_11; }
	inline void set_gameObjects_11(FsmGameObjectU5BU5D_t1706220122* value)
	{
		___gameObjects_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjects_11, value);
	}

	inline static int32_t get_offset_of_weights_12() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t1505534456, ___weights_12)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_12() const { return ___weights_12; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_12() { return &___weights_12; }
	inline void set_weights_12(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_12 = value;
		Il2CppCodeGenWriteBarrier(&___weights_12, value);
	}

	inline static int32_t get_offset_of_storeGameObject_13() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t1505534456, ___storeGameObject_13)); }
	inline FsmGameObject_t1697147867 * get_storeGameObject_13() const { return ___storeGameObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeGameObject_13() { return &___storeGameObject_13; }
	inline void set_storeGameObject_13(FsmGameObject_t1697147867 * value)
	{
		___storeGameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeGameObject_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
