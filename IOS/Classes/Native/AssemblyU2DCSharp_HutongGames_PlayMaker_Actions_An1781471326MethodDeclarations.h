﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStartRecording
struct AnimatorStartRecording_t1781471326;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::.ctor()
extern "C"  void AnimatorStartRecording__ctor_m3684098504 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::Reset()
extern "C"  void AnimatorStartRecording_Reset_m1330531445 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::OnEnter()
extern "C"  void AnimatorStartRecording_OnEnter_m757339039 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
