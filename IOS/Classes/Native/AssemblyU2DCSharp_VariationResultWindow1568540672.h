﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Text
struct Text_t9039225;
// ShowRecipePre
struct ShowRecipePre_t977589144;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t3798907012;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VariationResultWindow
struct  VariationResultWindow_t1568540672  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image VariationResultWindow::coffeImg
	Image_t538875265 * ___coffeImg_2;
	// UnityEngine.UI.Image VariationResultWindow::CreamImg
	Image_t538875265 * ___CreamImg_3;
	// UnityEngine.UI.Text VariationResultWindow::menuText
	Text_t9039225 * ___menuText_4;
	// ShowRecipePre VariationResultWindow::_showRecipePre
	ShowRecipePre_t977589144 * ____showRecipePre_5;
	// UnityEngine.UI.Button VariationResultWindow::_btn
	Button_t3896396478 * ____btn_6;
	// UnityEngine.UI.Text[] VariationResultWindow::menuTextList
	TextU5BU5D_t3798907012* ___menuTextList_7;
	// UnityEngine.UI.Text[] VariationResultWindow::unitTextList
	TextU5BU5D_t3798907012* ___unitTextList_8;

public:
	inline static int32_t get_offset_of_coffeImg_2() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ___coffeImg_2)); }
	inline Image_t538875265 * get_coffeImg_2() const { return ___coffeImg_2; }
	inline Image_t538875265 ** get_address_of_coffeImg_2() { return &___coffeImg_2; }
	inline void set_coffeImg_2(Image_t538875265 * value)
	{
		___coffeImg_2 = value;
		Il2CppCodeGenWriteBarrier(&___coffeImg_2, value);
	}

	inline static int32_t get_offset_of_CreamImg_3() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ___CreamImg_3)); }
	inline Image_t538875265 * get_CreamImg_3() const { return ___CreamImg_3; }
	inline Image_t538875265 ** get_address_of_CreamImg_3() { return &___CreamImg_3; }
	inline void set_CreamImg_3(Image_t538875265 * value)
	{
		___CreamImg_3 = value;
		Il2CppCodeGenWriteBarrier(&___CreamImg_3, value);
	}

	inline static int32_t get_offset_of_menuText_4() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ___menuText_4)); }
	inline Text_t9039225 * get_menuText_4() const { return ___menuText_4; }
	inline Text_t9039225 ** get_address_of_menuText_4() { return &___menuText_4; }
	inline void set_menuText_4(Text_t9039225 * value)
	{
		___menuText_4 = value;
		Il2CppCodeGenWriteBarrier(&___menuText_4, value);
	}

	inline static int32_t get_offset_of__showRecipePre_5() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ____showRecipePre_5)); }
	inline ShowRecipePre_t977589144 * get__showRecipePre_5() const { return ____showRecipePre_5; }
	inline ShowRecipePre_t977589144 ** get_address_of__showRecipePre_5() { return &____showRecipePre_5; }
	inline void set__showRecipePre_5(ShowRecipePre_t977589144 * value)
	{
		____showRecipePre_5 = value;
		Il2CppCodeGenWriteBarrier(&____showRecipePre_5, value);
	}

	inline static int32_t get_offset_of__btn_6() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ____btn_6)); }
	inline Button_t3896396478 * get__btn_6() const { return ____btn_6; }
	inline Button_t3896396478 ** get_address_of__btn_6() { return &____btn_6; }
	inline void set__btn_6(Button_t3896396478 * value)
	{
		____btn_6 = value;
		Il2CppCodeGenWriteBarrier(&____btn_6, value);
	}

	inline static int32_t get_offset_of_menuTextList_7() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ___menuTextList_7)); }
	inline TextU5BU5D_t3798907012* get_menuTextList_7() const { return ___menuTextList_7; }
	inline TextU5BU5D_t3798907012** get_address_of_menuTextList_7() { return &___menuTextList_7; }
	inline void set_menuTextList_7(TextU5BU5D_t3798907012* value)
	{
		___menuTextList_7 = value;
		Il2CppCodeGenWriteBarrier(&___menuTextList_7, value);
	}

	inline static int32_t get_offset_of_unitTextList_8() { return static_cast<int32_t>(offsetof(VariationResultWindow_t1568540672, ___unitTextList_8)); }
	inline TextU5BU5D_t3798907012* get_unitTextList_8() const { return ___unitTextList_8; }
	inline TextU5BU5D_t3798907012** get_address_of_unitTextList_8() { return &___unitTextList_8; }
	inline void set_unitTextList_8(TextU5BU5D_t3798907012* value)
	{
		___unitTextList_8 = value;
		Il2CppCodeGenWriteBarrier(&___unitTextList_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
