﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Object>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164  : public Il2CppObject
{
public:
	// T GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1::result
	Il2CppObject * ___result_0;
	// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<T> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1::<>f__ref$157
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * ___U3CU3Ef__refU24157_1;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164, ___result_0)); }
	inline Il2CppObject * get_result_0() const { return ___result_0; }
	inline Il2CppObject ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(Il2CppObject * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24157_1() { return static_cast<int32_t>(offsetof(U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164, ___U3CU3Ef__refU24157_1)); }
	inline U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * get_U3CU3Ef__refU24157_1() const { return ___U3CU3Ef__refU24157_1; }
	inline U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 ** get_address_of_U3CU3Ef__refU24157_1() { return &___U3CU3Ef__refU24157_1; }
	inline void set_U3CU3Ef__refU24157_1(U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * value)
	{
		___U3CU3Ef__refU24157_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24157_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
