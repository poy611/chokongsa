﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d
struct  WakeAllRigidBodies2d_t3655115336  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::everyFrame
	bool ___everyFrame_11;

public:
	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies2d_t3655115336, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
