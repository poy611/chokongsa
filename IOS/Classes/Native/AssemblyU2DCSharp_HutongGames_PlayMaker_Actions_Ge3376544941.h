﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct  GetAnimatorBody_t3376544941  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBody::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyPosition
	FsmVector3_t533912882 * ___bodyPosition_15;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyRotation
	FsmQuaternion_t3871136040 * ___bodyRotation_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyGameObject
	FsmGameObject_t1697147867 * ___bodyGameObject_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBody::_animator
	Animator_t2776330603 * ____animator_18;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorBody::_transform
	Transform_t1659122786 * ____transform_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_bodyPosition_15() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyPosition_15)); }
	inline FsmVector3_t533912882 * get_bodyPosition_15() const { return ___bodyPosition_15; }
	inline FsmVector3_t533912882 ** get_address_of_bodyPosition_15() { return &___bodyPosition_15; }
	inline void set_bodyPosition_15(FsmVector3_t533912882 * value)
	{
		___bodyPosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___bodyPosition_15, value);
	}

	inline static int32_t get_offset_of_bodyRotation_16() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyRotation_16)); }
	inline FsmQuaternion_t3871136040 * get_bodyRotation_16() const { return ___bodyRotation_16; }
	inline FsmQuaternion_t3871136040 ** get_address_of_bodyRotation_16() { return &___bodyRotation_16; }
	inline void set_bodyRotation_16(FsmQuaternion_t3871136040 * value)
	{
		___bodyRotation_16 = value;
		Il2CppCodeGenWriteBarrier(&___bodyRotation_16, value);
	}

	inline static int32_t get_offset_of_bodyGameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ___bodyGameObject_17)); }
	inline FsmGameObject_t1697147867 * get_bodyGameObject_17() const { return ___bodyGameObject_17; }
	inline FsmGameObject_t1697147867 ** get_address_of_bodyGameObject_17() { return &___bodyGameObject_17; }
	inline void set_bodyGameObject_17(FsmGameObject_t1697147867 * value)
	{
		___bodyGameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___bodyGameObject_17, value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ____animator_18)); }
	inline Animator_t2776330603 * get__animator_18() const { return ____animator_18; }
	inline Animator_t2776330603 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t2776330603 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier(&____animator_18, value);
	}

	inline static int32_t get_offset_of__transform_19() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3376544941, ____transform_19)); }
	inline Transform_t1659122786 * get__transform_19() const { return ____transform_19; }
	inline Transform_t1659122786 ** get_address_of__transform_19() { return &____transform_19; }
	inline void set__transform_19(Transform_t1659122786 * value)
	{
		____transform_19 = value;
		Il2CppCodeGenWriteBarrier(&____transform_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
