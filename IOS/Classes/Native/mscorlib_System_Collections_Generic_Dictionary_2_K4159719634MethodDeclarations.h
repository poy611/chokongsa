﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4159719634.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2410345790_gshared (Enumerator_t4159719634 * __this, Dictionary_2_t3544783580 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2410345790(__this, ___host0, method) ((  void (*) (Enumerator_t4159719634 *, Dictionary_2_t3544783580 *, const MethodInfo*))Enumerator__ctor_m2410345790_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m153875501_gshared (Enumerator_t4159719634 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m153875501(__this, method) ((  Il2CppObject * (*) (Enumerator_t4159719634 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m153875501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2449608887_gshared (Enumerator_t4159719634 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2449608887(__this, method) ((  void (*) (Enumerator_t4159719634 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2449608887_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m706914144_gshared (Enumerator_t4159719634 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m706914144(__this, method) ((  void (*) (Enumerator_t4159719634 *, const MethodInfo*))Enumerator_Dispose_m706914144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1845268583_gshared (Enumerator_t4159719634 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1845268583(__this, method) ((  bool (*) (Enumerator_t4159719634 *, const MethodInfo*))Enumerator_MoveNext_m1845268583_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m594001489_gshared (Enumerator_t4159719634 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m594001489(__this, method) ((  Il2CppObject * (*) (Enumerator_t4159719634 *, const MethodInfo*))Enumerator_get_Current_m594001489_gshared)(__this, method)
