﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3667593487;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"

// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m3734809445 (AnimationClip_t2007702890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m771412448 (Il2CppObject * __this /* static, unused */, AnimationClip_t2007702890 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C"  void AnimationClip_SetCurve_m4139164191 (AnimationClip_t2007702890 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3667593487 * ___curve3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
