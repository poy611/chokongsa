﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t3884844371;
// System.Object
struct Il2CppObject;
// JsonFx.Json.JsonWriter
struct JsonWriter_t3589747297;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriter3589747297.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m2253674582_gshared (WriteDelegate_1_t3884844371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define WriteDelegate_1__ctor_m2253674582(__this, ___object0, ___method1, method) ((  void (*) (WriteDelegate_1_t3884844371 *, Il2CppObject *, IntPtr_t, const MethodInfo*))WriteDelegate_1__ctor_m2253674582_gshared)(__this, ___object0, ___method1, method)
// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::Invoke(JsonFx.Json.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m3947399451_gshared (WriteDelegate_1_t3884844371 * __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method);
#define WriteDelegate_1_Invoke_m3947399451(__this, ___writer0, ___value1, method) ((  void (*) (WriteDelegate_1_t3884844371 *, JsonWriter_t3589747297 *, DateTime_t4283661327 , const MethodInfo*))WriteDelegate_1_Invoke_m3947399451_gshared)(__this, ___writer0, ___value1, method)
// System.IAsyncResult JsonFx.Json.WriteDelegate`1<System.DateTime>::BeginInvoke(JsonFx.Json.JsonWriter,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m3657410720_gshared (WriteDelegate_1_t3884844371 * __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define WriteDelegate_1_BeginInvoke_m3657410720(__this, ___writer0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (WriteDelegate_1_t3884844371 *, JsonWriter_t3589747297 *, DateTime_t4283661327 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))WriteDelegate_1_BeginInvoke_m3657410720_gshared)(__this, ___writer0, ___value1, ___callback2, ___object3, method)
// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m371025254_gshared (WriteDelegate_1_t3884844371 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define WriteDelegate_1_EndInvoke_m371025254(__this, ___result0, method) ((  void (*) (WriteDelegate_1_t3884844371 *, Il2CppObject *, const MethodInfo*))WriteDelegate_1_EndInvoke_m371025254_gshared)(__this, ___result0, method)
