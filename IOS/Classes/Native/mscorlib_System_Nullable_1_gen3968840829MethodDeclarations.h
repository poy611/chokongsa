﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.DateTimeOffset>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3932142427_gshared (Nullable_1_t3968840829 * __this, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3932142427(__this, ___value0, method) ((  void (*) (Nullable_1_t3968840829 *, DateTimeOffset_t3884714306 , const MethodInfo*))Nullable_1__ctor_m3932142427_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2724695620_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2724695620(__this, method) ((  bool (*) (Nullable_1_t3968840829 *, const MethodInfo*))Nullable_1_get_HasValue_m2724695620_gshared)(__this, method)
// T System.Nullable`1<System.DateTimeOffset>::get_Value()
extern "C"  DateTimeOffset_t3884714306  Nullable_1_get_Value_m3326115069_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3326115069(__this, method) ((  DateTimeOffset_t3884714306  (*) (Nullable_1_t3968840829 *, const MethodInfo*))Nullable_1_get_Value_m3326115069_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1572111781_gshared (Nullable_1_t3968840829 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1572111781(__this, ___other0, method) ((  bool (*) (Nullable_1_t3968840829 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1572111781_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3552532986_gshared (Nullable_1_t3968840829 * __this, Nullable_1_t3968840829  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3552532986(__this, ___other0, method) ((  bool (*) (Nullable_1_t3968840829 *, Nullable_1_t3968840829 , const MethodInfo*))Nullable_1_Equals_m3552532986_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2941497865_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2941497865(__this, method) ((  int32_t (*) (Nullable_1_t3968840829 *, const MethodInfo*))Nullable_1_GetHashCode_m2941497865_gshared)(__this, method)
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault()
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m116859463_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m116859463(__this, method) ((  DateTimeOffset_t3884714306  (*) (Nullable_1_t3968840829 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m116859463_gshared)(__this, method)
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault(T)
extern "C"  DateTimeOffset_t3884714306  Nullable_1_GetValueOrDefault_m3985819654_gshared (Nullable_1_t3968840829 * __this, DateTimeOffset_t3884714306  ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3985819654(__this, ___defaultValue0, method) ((  DateTimeOffset_t3884714306  (*) (Nullable_1_t3968840829 *, DateTimeOffset_t3884714306 , const MethodInfo*))Nullable_1_GetValueOrDefault_m3985819654_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.DateTimeOffset>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2837501533_gshared (Nullable_1_t3968840829 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2837501533(__this, method) ((  String_t* (*) (Nullable_1_t3968840829 *, const MethodInfo*))Nullable_1_ToString_m2837501533_gshared)(__this, method)
