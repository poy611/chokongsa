﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringCompare
struct StringCompare_t3934792034;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringCompare::.ctor()
extern "C"  void StringCompare__ctor_m131241492 (StringCompare_t3934792034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::Reset()
extern "C"  void StringCompare_Reset_m2072641729 (StringCompare_t3934792034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnEnter()
extern "C"  void StringCompare_OnEnter_m960750827 (StringCompare_t3934792034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnUpdate()
extern "C"  void StringCompare_OnUpdate_m3147031224 (StringCompare_t3934792034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::DoStringCompare()
extern "C"  void StringCompare_DoStringCompare_m1676816507 (StringCompare_t3934792034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
