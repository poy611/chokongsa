﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse
struct FetchServerAuthCodeResponse_t1052060023;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::.ctor(System.IntPtr)
extern "C"  void FetchServerAuthCodeResponse__ctor_m760709376 (FetchServerAuthCodeResponse_t1052060023 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::Status()
extern "C"  int32_t FetchServerAuthCodeResponse_Status_m889529753 (FetchServerAuthCodeResponse_t1052060023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::Code()
extern "C"  String_t* FetchServerAuthCodeResponse_Code_m719274266 (FetchServerAuthCodeResponse_t1052060023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchServerAuthCodeResponse_CallDispose_m1614441648 (FetchServerAuthCodeResponse_t1052060023 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::FromPointer(System.IntPtr)
extern "C"  FetchServerAuthCodeResponse_t1052060023 * FetchServerAuthCodeResponse_FromPointer_m1567204819 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse::<Code>m__A3(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  FetchServerAuthCodeResponse_U3CCodeU3Em__A3_m394917739 (FetchServerAuthCodeResponse_t1052060023 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
