﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3668798674(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t520592000 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Invoke(T,T)
#define Comparison_1_Invoke_m3294788966(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t520592000 *, Participant_t1804230813 *, Participant_t1804230813 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m4277219503(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t520592000 *, Participant_t1804230813 *, Participant_t1804230813 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m2360147270(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t520592000 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
