﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.IntPtr>
struct Collection_1_t3195860129;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t1627299724;
// System.Collections.Generic.IList`1<System.IntPtr>
struct IList_1_t2410081878;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::.ctor()
extern "C"  void Collection_1__ctor_m3968581345_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3968581345(__this, method) ((  void (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1__ctor_m3968581345_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3826141562_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3826141562(__this, method) ((  bool (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3826141562_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3232526723_gshared (Collection_1_t3195860129 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3232526723(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3195860129 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3232526723_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1146547646_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1146547646(__this, method) ((  Il2CppObject * (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1146547646_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3810558835_gshared (Collection_1_t3195860129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3810558835(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3195860129 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3810558835_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3378785465_gshared (Collection_1_t3195860129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3378785465(__this, ___value0, method) ((  bool (*) (Collection_1_t3195860129 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3378785465_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1052493899_gshared (Collection_1_t3195860129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1052493899(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3195860129 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1052493899_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1733586166_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1733586166(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1733586166_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m943940722_gshared (Collection_1_t3195860129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m943940722(__this, ___value0, method) ((  void (*) (Collection_1_t3195860129 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m943940722_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m539214851_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m539214851(__this, method) ((  bool (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m539214851_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3205047279_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3205047279(__this, method) ((  Il2CppObject * (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3205047279_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4267055848_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4267055848(__this, method) ((  bool (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4267055848_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1561487121_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1561487121(__this, method) ((  bool (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1561487121_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1624495542_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1624495542(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3195860129 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1624495542_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m142947149_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m142947149(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m142947149_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::Add(T)
extern "C"  void Collection_1_Add_m949444990_gshared (Collection_1_t3195860129 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m949444990(__this, ___item0, method) ((  void (*) (Collection_1_t3195860129 *, IntPtr_t, const MethodInfo*))Collection_1_Add_m949444990_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::Clear()
extern "C"  void Collection_1_Clear_m1374714636_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1374714636(__this, method) ((  void (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_Clear_m1374714636_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3020565878_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3020565878(__this, method) ((  void (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_ClearItems_m3020565878_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::Contains(T)
extern "C"  bool Collection_1_Contains_m3415284922_gshared (Collection_1_t3195860129 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3415284922(__this, ___item0, method) ((  bool (*) (Collection_1_t3195860129 *, IntPtr_t, const MethodInfo*))Collection_1_Contains_m3415284922_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1994597870_gshared (Collection_1_t3195860129 * __this, IntPtrU5BU5D_t3228729122* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1994597870(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3195860129 *, IntPtrU5BU5D_t3228729122*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1994597870_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.IntPtr>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3064714909_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3064714909(__this, method) ((  Il2CppObject* (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_GetEnumerator_m3064714909_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.IntPtr>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4147550450_gshared (Collection_1_t3195860129 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4147550450(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3195860129 *, IntPtr_t, const MethodInfo*))Collection_1_IndexOf_m4147550450_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2035146725_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, IntPtr_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2035146725(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, IntPtr_t, const MethodInfo*))Collection_1_Insert_m2035146725_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3214447512_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, IntPtr_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3214447512(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, IntPtr_t, const MethodInfo*))Collection_1_InsertItem_m3214447512_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.IntPtr>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m3841591584_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m3841591584(__this, method) ((  Il2CppObject* (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_get_Items_m3841591584_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::Remove(T)
extern "C"  bool Collection_1_Remove_m1106887477_gshared (Collection_1_t3195860129 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1106887477(__this, ___item0, method) ((  bool (*) (Collection_1_t3195860129 *, IntPtr_t, const MethodInfo*))Collection_1_Remove_m1106887477_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4203966891_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4203966891(__this, ___index0, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4203966891_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1347581707_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1347581707(__this, ___index0, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1347581707_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.IntPtr>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3837319881_gshared (Collection_1_t3195860129 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3837319881(__this, method) ((  int32_t (*) (Collection_1_t3195860129 *, const MethodInfo*))Collection_1_get_Count_m3837319881_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.IntPtr>::get_Item(System.Int32)
extern "C"  IntPtr_t Collection_1_get_Item_m3183433839_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3183433839(__this, ___index0, method) ((  IntPtr_t (*) (Collection_1_t3195860129 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3183433839_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3375479676_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, IntPtr_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3375479676(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, IntPtr_t, const MethodInfo*))Collection_1_set_Item_m3375479676_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4276596093_gshared (Collection_1_t3195860129 * __this, int32_t ___index0, IntPtr_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4276596093(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3195860129 *, int32_t, IntPtr_t, const MethodInfo*))Collection_1_SetItem_m4276596093_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m198117938_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m198117938(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m198117938_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.IntPtr>::ConvertItem(System.Object)
extern "C"  IntPtr_t Collection_1_ConvertItem_m4155062222_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4155062222(__this /* static, unused */, ___item0, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4155062222_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.IntPtr>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2756452014_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2756452014(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2756452014_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3306482002_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3306482002(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3306482002_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.IntPtr>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m128462477_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m128462477(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m128462477_gshared)(__this /* static, unused */, ___list0, method)
