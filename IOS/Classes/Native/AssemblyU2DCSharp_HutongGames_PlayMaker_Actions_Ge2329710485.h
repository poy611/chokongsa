﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct  GetAnimatorCurrentStateInfoIsTag_t2329710485  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::layerIndex
	FsmInt_t1596138449 * ___layerIndex_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tag
	FsmString_t952858651 * ___tag_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatch
	FsmBool_t1075959796 * ___tagMatch_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatchEvent
	FsmEvent_t2133468028 * ___tagMatchEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagDoNotMatchEvent
	FsmEvent_t2133468028 * ___tagDoNotMatchEvent_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::_animator
	Animator_t2776330603 * ____animator_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_layerIndex_15() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___layerIndex_15)); }
	inline FsmInt_t1596138449 * get_layerIndex_15() const { return ___layerIndex_15; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_15() { return &___layerIndex_15; }
	inline void set_layerIndex_15(FsmInt_t1596138449 * value)
	{
		___layerIndex_15 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_15, value);
	}

	inline static int32_t get_offset_of_tag_16() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tag_16)); }
	inline FsmString_t952858651 * get_tag_16() const { return ___tag_16; }
	inline FsmString_t952858651 ** get_address_of_tag_16() { return &___tag_16; }
	inline void set_tag_16(FsmString_t952858651 * value)
	{
		___tag_16 = value;
		Il2CppCodeGenWriteBarrier(&___tag_16, value);
	}

	inline static int32_t get_offset_of_tagMatch_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagMatch_17)); }
	inline FsmBool_t1075959796 * get_tagMatch_17() const { return ___tagMatch_17; }
	inline FsmBool_t1075959796 ** get_address_of_tagMatch_17() { return &___tagMatch_17; }
	inline void set_tagMatch_17(FsmBool_t1075959796 * value)
	{
		___tagMatch_17 = value;
		Il2CppCodeGenWriteBarrier(&___tagMatch_17, value);
	}

	inline static int32_t get_offset_of_tagMatchEvent_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagMatchEvent_18)); }
	inline FsmEvent_t2133468028 * get_tagMatchEvent_18() const { return ___tagMatchEvent_18; }
	inline FsmEvent_t2133468028 ** get_address_of_tagMatchEvent_18() { return &___tagMatchEvent_18; }
	inline void set_tagMatchEvent_18(FsmEvent_t2133468028 * value)
	{
		___tagMatchEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___tagMatchEvent_18, value);
	}

	inline static int32_t get_offset_of_tagDoNotMatchEvent_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ___tagDoNotMatchEvent_19)); }
	inline FsmEvent_t2133468028 * get_tagDoNotMatchEvent_19() const { return ___tagDoNotMatchEvent_19; }
	inline FsmEvent_t2133468028 ** get_address_of_tagDoNotMatchEvent_19() { return &___tagDoNotMatchEvent_19; }
	inline void set_tagDoNotMatchEvent_19(FsmEvent_t2133468028 * value)
	{
		___tagDoNotMatchEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___tagDoNotMatchEvent_19, value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t2329710485, ____animator_20)); }
	inline Animator_t2776330603 * get__animator_20() const { return ____animator_20; }
	inline Animator_t2776330603 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t2776330603 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier(&____animator_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
