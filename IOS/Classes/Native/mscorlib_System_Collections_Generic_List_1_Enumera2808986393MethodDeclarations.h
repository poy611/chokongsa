﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ResultData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1859048026(__this, ___l0, method) ((  void (*) (Enumerator_t2808986393 *, List_1_t2789313623 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1168683448(__this, method) ((  void (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ResultData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4147523182(__this, method) ((  Il2CppObject * (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultData>::Dispose()
#define Enumerator_Dispose_m3025802943(__this, method) ((  void (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultData>::VerifyState()
#define Enumerator_VerifyState_m2702597752(__this, method) ((  void (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ResultData>::MoveNext()
#define Enumerator_MoveNext_m1859377384(__this, method) ((  bool (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ResultData>::get_Current()
#define Enumerator_get_Current_m362520273(__this, method) ((  ResultData_t1421128071 * (*) (Enumerator_t2808986393 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
