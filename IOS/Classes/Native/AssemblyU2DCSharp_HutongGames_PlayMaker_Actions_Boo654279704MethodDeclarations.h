﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolNoneTrue
struct BoolNoneTrue_t654279704;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::.ctor()
extern "C"  void BoolNoneTrue__ctor_m815154510 (BoolNoneTrue_t654279704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::Reset()
extern "C"  void BoolNoneTrue_Reset_m2756554747 (BoolNoneTrue_t654279704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::OnEnter()
extern "C"  void BoolNoneTrue_OnEnter_m1071164837 (BoolNoneTrue_t654279704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::OnUpdate()
extern "C"  void BoolNoneTrue_OnUpdate_m2274898238 (BoolNoneTrue_t654279704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::DoNoneTrue()
extern "C"  void BoolNoneTrue_DoNoneTrue_m2901515463 (BoolNoneTrue_t654279704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
