﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonNameAttribute
struct JsonNameAttribute_t1794268491;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.String JsonFx.Json.JsonNameAttribute::get_Name()
extern "C"  String_t* JsonNameAttribute_get_Name_m3918014895 (JsonNameAttribute_t1794268491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonFx.Json.JsonNameAttribute::GetJsonName(System.Object)
extern "C"  String_t* JsonNameAttribute_GetJsonName_m3130059236 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
