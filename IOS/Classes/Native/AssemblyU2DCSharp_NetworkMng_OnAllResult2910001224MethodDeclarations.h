﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnAllResult
struct OnAllResult_t2910001224;
// System.Object
struct Il2CppObject;
// ResponseResultAllData
struct ResponseResultAllData_t4071630637;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ResponseResultAllData4071630637.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnAllResult::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAllResult__ctor_m619606767 (OnAllResult_t2910001224 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResult::Invoke(ResponseResultAllData)
extern "C"  void OnAllResult_Invoke_m3759898300 (OnAllResult_t2910001224 * __this, ResponseResultAllData_t4071630637 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnAllResult::BeginInvoke(ResponseResultAllData,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAllResult_BeginInvoke_m1401974555 (OnAllResult_t2910001224 * __this, ResponseResultAllData_t4071630637 * ___info0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResult::EndInvoke(System.IAsyncResult)
extern "C"  void OnAllResult_EndInvoke_m1050063999 (OnAllResult_t2910001224 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
