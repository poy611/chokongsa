﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Enum
struct Enum_t2862688501;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Enum2862688501.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// System.Object HutongGames.PlayMaker.FsmEnum::get_RawValue()
extern "C"  Il2CppObject * FsmEnum_get_RawValue_m2809785581 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_RawValue(System.Object)
extern "C"  void FsmEnum_set_RawValue_m3394784734 (FsmEnum_t1076048395 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmEnum::get_EnumType()
extern "C"  Type_t * FsmEnum_get_EnumType_m1642325082 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_EnumType(System.Type)
extern "C"  void FsmEnum_set_EnumType_m1536162257 (FsmEnum_t1076048395 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::InitEnumType()
extern "C"  void FsmEnum_InitEnumType_m210813403 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEnum::get_EnumName()
extern "C"  String_t* FsmEnum_get_EnumName_m507129474 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_EnumName(System.String)
extern "C"  void FsmEnum_set_EnumName_m1216367593 (FsmEnum_t1076048395 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum HutongGames.PlayMaker.FsmEnum::get_Value()
extern "C"  Enum_t2862688501 * FsmEnum_get_Value_m4041924749 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_Value(System.Enum)
extern "C"  void FsmEnum_set_Value_m2534430636 (FsmEnum_t1076048395 * __this, Enum_t2862688501 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::ResetValue()
extern "C"  void FsmEnum_ResetValue_m236975154 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor()
extern "C"  void FsmEnum__ctor_m2450843700 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(System.String,System.Type,System.Int32)
extern "C"  void FsmEnum__ctor_m827203830 (FsmEnum_t1076048395 * __this, String_t* ___name0, Type_t * ___enumType1, int32_t ___intValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(System.String)
extern "C"  void FsmEnum__ctor_m613591886 (FsmEnum_t1076048395 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(HutongGames.PlayMaker.FsmEnum)
extern "C"  void FsmEnum__ctor_m3808772681 (FsmEnum_t1076048395 * __this, FsmEnum_t1076048395 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmEnum::Clone()
extern "C"  NamedVariable_t3211770239 * FsmEnum_Clone_m967866841 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEnum::ToString()
extern "C"  String_t* FsmEnum_ToString_m2554582841 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmEnum::get_VariableType()
extern "C"  int32_t FsmEnum_get_VariableType_m1447646128 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmEnum::get_ObjectType()
extern "C"  Type_t * FsmEnum_get_ObjectType_m2291086488 (FsmEnum_t1076048395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_ObjectType(System.Type)
extern "C"  void FsmEnum_set_ObjectType_m2429928019 (FsmEnum_t1076048395 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEnum::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmEnum_TestTypeConstraint_m1804892050 (FsmEnum_t1076048395 * __this, int32_t ___variableType0, Type_t * ____enumType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FsmEnum::op_Implicit(System.Enum)
extern "C"  FsmEnum_t1076048395 * FsmEnum_op_Implicit_m4146730367 (Il2CppObject * __this /* static, unused */, Enum_t2862688501 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
