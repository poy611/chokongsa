﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionRotateTowards
struct QuaternionRotateTowards_t350618001;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::.ctor()
extern "C"  void QuaternionRotateTowards__ctor_m2043084997 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::Reset()
extern "C"  void QuaternionRotateTowards_Reset_m3984485234 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnEnter()
extern "C"  void QuaternionRotateTowards_OnEnter_m4291323740 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnUpdate()
extern "C"  void QuaternionRotateTowards_OnUpdate_m3315576423 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnLateUpdate()
extern "C"  void QuaternionRotateTowards_OnLateUpdate_m706078893 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnFixedUpdate()
extern "C"  void QuaternionRotateTowards_OnFixedUpdate_m1289554529 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::DoQuatRotateTowards()
extern "C"  void QuaternionRotateTowards_DoQuatRotateTowards_m2666579778 (QuaternionRotateTowards_t350618001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
