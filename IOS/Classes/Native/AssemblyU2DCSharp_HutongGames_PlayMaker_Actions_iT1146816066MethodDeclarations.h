﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPause
struct iTweenPause_t1146816066;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPause::.ctor()
extern "C"  void iTweenPause__ctor_m3972124468 (iTweenPause_t1146816066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::Reset()
extern "C"  void iTweenPause_Reset_m1618557409 (iTweenPause_t1146816066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::OnEnter()
extern "C"  void iTweenPause_OnEnter_m2672383499 (iTweenPause_t1146816066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::DoiTween()
extern "C"  void iTweenPause_DoiTween_m2690642077 (iTweenPause_t1146816066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
