﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties
struct  NetworkGetOnFailedToConnectProperties_t1519752524  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::errorLabel
	FsmString_t952858651 * ___errorLabel_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NoErrorEvent
	FsmEvent_t2133468028 * ___NoErrorEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::RSAPublicKeyMismatchEvent
	FsmEvent_t2133468028 * ___RSAPublicKeyMismatchEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::InvalidPasswordEvent
	FsmEvent_t2133468028 * ___InvalidPasswordEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::ConnectionFailedEvent
	FsmEvent_t2133468028 * ___ConnectionFailedEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::TooManyConnectedPlayersEvent
	FsmEvent_t2133468028 * ___TooManyConnectedPlayersEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::ConnectionBannedEvent
	FsmEvent_t2133468028 * ___ConnectionBannedEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::AlreadyConnectedToServerEvent
	FsmEvent_t2133468028 * ___AlreadyConnectedToServerEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::AlreadyConnectedToAnotherServerEvent
	FsmEvent_t2133468028 * ___AlreadyConnectedToAnotherServerEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::CreateSocketOrThreadFailureEvent
	FsmEvent_t2133468028 * ___CreateSocketOrThreadFailureEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::IncorrectParametersEvent
	FsmEvent_t2133468028 * ___IncorrectParametersEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::EmptyConnectTargetEvent
	FsmEvent_t2133468028 * ___EmptyConnectTargetEvent_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::InternalDirectConnectFailedEvent
	FsmEvent_t2133468028 * ___InternalDirectConnectFailedEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATTargetNotConnectedEvent
	FsmEvent_t2133468028 * ___NATTargetNotConnectedEvent_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATTargetConnectionLostEvent
	FsmEvent_t2133468028 * ___NATTargetConnectionLostEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::NATPunchthroughFailedEvent
	FsmEvent_t2133468028 * ___NATPunchthroughFailedEvent_26;

public:
	inline static int32_t get_offset_of_errorLabel_11() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___errorLabel_11)); }
	inline FsmString_t952858651 * get_errorLabel_11() const { return ___errorLabel_11; }
	inline FsmString_t952858651 ** get_address_of_errorLabel_11() { return &___errorLabel_11; }
	inline void set_errorLabel_11(FsmString_t952858651 * value)
	{
		___errorLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___errorLabel_11, value);
	}

	inline static int32_t get_offset_of_NoErrorEvent_12() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NoErrorEvent_12)); }
	inline FsmEvent_t2133468028 * get_NoErrorEvent_12() const { return ___NoErrorEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_NoErrorEvent_12() { return &___NoErrorEvent_12; }
	inline void set_NoErrorEvent_12(FsmEvent_t2133468028 * value)
	{
		___NoErrorEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___NoErrorEvent_12, value);
	}

	inline static int32_t get_offset_of_RSAPublicKeyMismatchEvent_13() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___RSAPublicKeyMismatchEvent_13)); }
	inline FsmEvent_t2133468028 * get_RSAPublicKeyMismatchEvent_13() const { return ___RSAPublicKeyMismatchEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_RSAPublicKeyMismatchEvent_13() { return &___RSAPublicKeyMismatchEvent_13; }
	inline void set_RSAPublicKeyMismatchEvent_13(FsmEvent_t2133468028 * value)
	{
		___RSAPublicKeyMismatchEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___RSAPublicKeyMismatchEvent_13, value);
	}

	inline static int32_t get_offset_of_InvalidPasswordEvent_14() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___InvalidPasswordEvent_14)); }
	inline FsmEvent_t2133468028 * get_InvalidPasswordEvent_14() const { return ___InvalidPasswordEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_InvalidPasswordEvent_14() { return &___InvalidPasswordEvent_14; }
	inline void set_InvalidPasswordEvent_14(FsmEvent_t2133468028 * value)
	{
		___InvalidPasswordEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___InvalidPasswordEvent_14, value);
	}

	inline static int32_t get_offset_of_ConnectionFailedEvent_15() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___ConnectionFailedEvent_15)); }
	inline FsmEvent_t2133468028 * get_ConnectionFailedEvent_15() const { return ___ConnectionFailedEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_ConnectionFailedEvent_15() { return &___ConnectionFailedEvent_15; }
	inline void set_ConnectionFailedEvent_15(FsmEvent_t2133468028 * value)
	{
		___ConnectionFailedEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionFailedEvent_15, value);
	}

	inline static int32_t get_offset_of_TooManyConnectedPlayersEvent_16() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___TooManyConnectedPlayersEvent_16)); }
	inline FsmEvent_t2133468028 * get_TooManyConnectedPlayersEvent_16() const { return ___TooManyConnectedPlayersEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_TooManyConnectedPlayersEvent_16() { return &___TooManyConnectedPlayersEvent_16; }
	inline void set_TooManyConnectedPlayersEvent_16(FsmEvent_t2133468028 * value)
	{
		___TooManyConnectedPlayersEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___TooManyConnectedPlayersEvent_16, value);
	}

	inline static int32_t get_offset_of_ConnectionBannedEvent_17() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___ConnectionBannedEvent_17)); }
	inline FsmEvent_t2133468028 * get_ConnectionBannedEvent_17() const { return ___ConnectionBannedEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_ConnectionBannedEvent_17() { return &___ConnectionBannedEvent_17; }
	inline void set_ConnectionBannedEvent_17(FsmEvent_t2133468028 * value)
	{
		___ConnectionBannedEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionBannedEvent_17, value);
	}

	inline static int32_t get_offset_of_AlreadyConnectedToServerEvent_18() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___AlreadyConnectedToServerEvent_18)); }
	inline FsmEvent_t2133468028 * get_AlreadyConnectedToServerEvent_18() const { return ___AlreadyConnectedToServerEvent_18; }
	inline FsmEvent_t2133468028 ** get_address_of_AlreadyConnectedToServerEvent_18() { return &___AlreadyConnectedToServerEvent_18; }
	inline void set_AlreadyConnectedToServerEvent_18(FsmEvent_t2133468028 * value)
	{
		___AlreadyConnectedToServerEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___AlreadyConnectedToServerEvent_18, value);
	}

	inline static int32_t get_offset_of_AlreadyConnectedToAnotherServerEvent_19() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___AlreadyConnectedToAnotherServerEvent_19)); }
	inline FsmEvent_t2133468028 * get_AlreadyConnectedToAnotherServerEvent_19() const { return ___AlreadyConnectedToAnotherServerEvent_19; }
	inline FsmEvent_t2133468028 ** get_address_of_AlreadyConnectedToAnotherServerEvent_19() { return &___AlreadyConnectedToAnotherServerEvent_19; }
	inline void set_AlreadyConnectedToAnotherServerEvent_19(FsmEvent_t2133468028 * value)
	{
		___AlreadyConnectedToAnotherServerEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___AlreadyConnectedToAnotherServerEvent_19, value);
	}

	inline static int32_t get_offset_of_CreateSocketOrThreadFailureEvent_20() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___CreateSocketOrThreadFailureEvent_20)); }
	inline FsmEvent_t2133468028 * get_CreateSocketOrThreadFailureEvent_20() const { return ___CreateSocketOrThreadFailureEvent_20; }
	inline FsmEvent_t2133468028 ** get_address_of_CreateSocketOrThreadFailureEvent_20() { return &___CreateSocketOrThreadFailureEvent_20; }
	inline void set_CreateSocketOrThreadFailureEvent_20(FsmEvent_t2133468028 * value)
	{
		___CreateSocketOrThreadFailureEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___CreateSocketOrThreadFailureEvent_20, value);
	}

	inline static int32_t get_offset_of_IncorrectParametersEvent_21() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___IncorrectParametersEvent_21)); }
	inline FsmEvent_t2133468028 * get_IncorrectParametersEvent_21() const { return ___IncorrectParametersEvent_21; }
	inline FsmEvent_t2133468028 ** get_address_of_IncorrectParametersEvent_21() { return &___IncorrectParametersEvent_21; }
	inline void set_IncorrectParametersEvent_21(FsmEvent_t2133468028 * value)
	{
		___IncorrectParametersEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___IncorrectParametersEvent_21, value);
	}

	inline static int32_t get_offset_of_EmptyConnectTargetEvent_22() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___EmptyConnectTargetEvent_22)); }
	inline FsmEvent_t2133468028 * get_EmptyConnectTargetEvent_22() const { return ___EmptyConnectTargetEvent_22; }
	inline FsmEvent_t2133468028 ** get_address_of_EmptyConnectTargetEvent_22() { return &___EmptyConnectTargetEvent_22; }
	inline void set_EmptyConnectTargetEvent_22(FsmEvent_t2133468028 * value)
	{
		___EmptyConnectTargetEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyConnectTargetEvent_22, value);
	}

	inline static int32_t get_offset_of_InternalDirectConnectFailedEvent_23() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___InternalDirectConnectFailedEvent_23)); }
	inline FsmEvent_t2133468028 * get_InternalDirectConnectFailedEvent_23() const { return ___InternalDirectConnectFailedEvent_23; }
	inline FsmEvent_t2133468028 ** get_address_of_InternalDirectConnectFailedEvent_23() { return &___InternalDirectConnectFailedEvent_23; }
	inline void set_InternalDirectConnectFailedEvent_23(FsmEvent_t2133468028 * value)
	{
		___InternalDirectConnectFailedEvent_23 = value;
		Il2CppCodeGenWriteBarrier(&___InternalDirectConnectFailedEvent_23, value);
	}

	inline static int32_t get_offset_of_NATTargetNotConnectedEvent_24() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATTargetNotConnectedEvent_24)); }
	inline FsmEvent_t2133468028 * get_NATTargetNotConnectedEvent_24() const { return ___NATTargetNotConnectedEvent_24; }
	inline FsmEvent_t2133468028 ** get_address_of_NATTargetNotConnectedEvent_24() { return &___NATTargetNotConnectedEvent_24; }
	inline void set_NATTargetNotConnectedEvent_24(FsmEvent_t2133468028 * value)
	{
		___NATTargetNotConnectedEvent_24 = value;
		Il2CppCodeGenWriteBarrier(&___NATTargetNotConnectedEvent_24, value);
	}

	inline static int32_t get_offset_of_NATTargetConnectionLostEvent_25() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATTargetConnectionLostEvent_25)); }
	inline FsmEvent_t2133468028 * get_NATTargetConnectionLostEvent_25() const { return ___NATTargetConnectionLostEvent_25; }
	inline FsmEvent_t2133468028 ** get_address_of_NATTargetConnectionLostEvent_25() { return &___NATTargetConnectionLostEvent_25; }
	inline void set_NATTargetConnectionLostEvent_25(FsmEvent_t2133468028 * value)
	{
		___NATTargetConnectionLostEvent_25 = value;
		Il2CppCodeGenWriteBarrier(&___NATTargetConnectionLostEvent_25, value);
	}

	inline static int32_t get_offset_of_NATPunchthroughFailedEvent_26() { return static_cast<int32_t>(offsetof(NetworkGetOnFailedToConnectProperties_t1519752524, ___NATPunchthroughFailedEvent_26)); }
	inline FsmEvent_t2133468028 * get_NATPunchthroughFailedEvent_26() const { return ___NATPunchthroughFailedEvent_26; }
	inline FsmEvent_t2133468028 ** get_address_of_NATPunchthroughFailedEvent_26() { return &___NATPunchthroughFailedEvent_26; }
	inline void set_NATPunchthroughFailedEvent_26(FsmEvent_t2133468028 * value)
	{
		___NATPunchthroughFailedEvent_26 = value;
		Il2CppCodeGenWriteBarrier(&___NATPunchthroughFailedEvent_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
