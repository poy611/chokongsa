﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A
struct U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A::.ctor()
extern "C"  void U3CParticipantLeftU3Ec__AnonStorey7A__ctor_m3385915344 (U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A::<>m__55()
extern "C"  void U3CParticipantLeftU3Ec__AnonStorey7A_U3CU3Em__55_m587599161 (U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
