﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorInt
struct SetAnimatorInt_t1821808814;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::.ctor()
extern "C"  void SetAnimatorInt__ctor_m4174533496 (SetAnimatorInt_t1821808814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::Reset()
extern "C"  void SetAnimatorInt_Reset_m1820966437 (SetAnimatorInt_t1821808814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::OnEnter()
extern "C"  void SetAnimatorInt_OnEnter_m3913931087 (SetAnimatorInt_t1821808814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::OnActionUpdate()
extern "C"  void SetAnimatorInt_OnActionUpdate_m2584782762 (SetAnimatorInt_t1821808814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::SetParameter()
extern "C"  void SetAnimatorInt_SetParameter_m2575710067 (SetAnimatorInt_t1821808814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
