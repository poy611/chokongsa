﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Invert
struct Vector3Invert_t2217758996;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::.ctor()
extern "C"  void Vector3Invert__ctor_m3598120610 (Vector3Invert_t2217758996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::Reset()
extern "C"  void Vector3Invert_Reset_m1244553551 (Vector3Invert_t2217758996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::OnEnter()
extern "C"  void Vector3Invert_OnEnter_m4031928825 (Vector3Invert_t2217758996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::OnUpdate()
extern "C"  void Vector3Invert_OnUpdate_m3864268650 (Vector3Invert_t2217758996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
