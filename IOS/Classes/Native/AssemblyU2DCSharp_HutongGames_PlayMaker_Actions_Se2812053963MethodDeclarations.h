﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightSpotAngle
struct SetLightSpotAngle_t2812053963;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::.ctor()
extern "C"  void SetLightSpotAngle__ctor_m412477387 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::Reset()
extern "C"  void SetLightSpotAngle_Reset_m2353877624 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnEnter()
extern "C"  void SetLightSpotAngle_OnEnter_m645506274 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnUpdate()
extern "C"  void SetLightSpotAngle_OnUpdate_m1964384673 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::DoSetLightRange()
extern "C"  void SetLightSpotAngle_DoSetLightRange_m176896103 (SetLightSpotAngle_t2812053963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
