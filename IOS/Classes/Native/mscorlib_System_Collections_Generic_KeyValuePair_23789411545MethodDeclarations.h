﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1114107605(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3789411545 *, String_t*, ReflectionMember_t3070212469 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::get_Key()
#define KeyValuePair_2_get_Key_m3867724147(__this, method) ((  String_t* (*) (KeyValuePair_2_t3789411545 *, const MethodInfo*))KeyValuePair_2_get_Key_m2940899609_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2634594868(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3789411545 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::get_Value()
#define KeyValuePair_2_get_Value_m613662323(__this, method) ((  ReflectionMember_t3070212469 * (*) (KeyValuePair_2_t3789411545 *, const MethodInfo*))KeyValuePair_2_get_Value_m4250204908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m223597620(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3789411545 *, ReflectionMember_t3070212469 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>::ToString()
#define KeyValuePair_2_ToString_m2100138478(__this, method) ((  String_t* (*) (KeyValuePair_2_t3789411545 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
