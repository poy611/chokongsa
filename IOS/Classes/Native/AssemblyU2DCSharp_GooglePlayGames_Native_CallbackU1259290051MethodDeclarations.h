﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3857037258.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m261560595_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m261560595(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m261560595_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::<>m__10(T1,T2)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922(__this, ___val10, ___val21, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 *, int32_t, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922_gshared)(__this, ___val10, ___val21, method)
