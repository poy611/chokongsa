﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo
struct ExtractionInfo_t925438340;
// System.Uri
struct Uri_t1116831938;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri1116831938.h"

// System.Boolean YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::get_RequiresDecryption()
extern "C"  bool ExtractionInfo_get_RequiresDecryption_m2265378519 (ExtractionInfo_t925438340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::set_RequiresDecryption(System.Boolean)
extern "C"  void ExtractionInfo_set_RequiresDecryption_m50076542 (ExtractionInfo_t925438340 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::get_Uri()
extern "C"  Uri_t1116831938 * ExtractionInfo_get_Uri_m2410459868 (ExtractionInfo_t925438340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::set_Uri(System.Uri)
extern "C"  void ExtractionInfo_set_Uri_m4068311779 (ExtractionInfo_t925438340 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::.ctor()
extern "C"  void ExtractionInfo__ctor_m443894211 (ExtractionInfo_t925438340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
