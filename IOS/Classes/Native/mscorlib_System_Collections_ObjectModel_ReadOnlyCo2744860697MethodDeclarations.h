﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1879324594(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3569434780(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m990019502(__this, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m554348675(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3293405211(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2723168841(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m154457711(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1962728602(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m228464916(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23018849(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1199302064(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3072208013(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2270302319(__this, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m187421907(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m455824485(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1814900312(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1243745232(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1486752232(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23522729(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3138455515(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2023513730(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1904756791(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2054319778(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m976430127(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::Contains(T)
#define ReadOnlyCollection_1_Contains_m2466997344(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1493542860(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2744860697 *, IEventU5BU5D_t659202564*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m3478098103(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m4179262616(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2744860697 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Count()
#define ReadOnlyCollection_1_get_Count_m525429731(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2744860697 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1435337647(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2744860697 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
