﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMaterial
struct  GetMaterial_t1959127339  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetMaterial::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetMaterial::materialIndex
	FsmInt_t1596138449 * ___materialIndex_14;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.GetMaterial::material
	FsmMaterial_t924399665 * ___material_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMaterial::getSharedMaterial
	bool ___getSharedMaterial_16;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(GetMaterial_t1959127339, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_materialIndex_14() { return static_cast<int32_t>(offsetof(GetMaterial_t1959127339, ___materialIndex_14)); }
	inline FsmInt_t1596138449 * get_materialIndex_14() const { return ___materialIndex_14; }
	inline FsmInt_t1596138449 ** get_address_of_materialIndex_14() { return &___materialIndex_14; }
	inline void set_materialIndex_14(FsmInt_t1596138449 * value)
	{
		___materialIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___materialIndex_14, value);
	}

	inline static int32_t get_offset_of_material_15() { return static_cast<int32_t>(offsetof(GetMaterial_t1959127339, ___material_15)); }
	inline FsmMaterial_t924399665 * get_material_15() const { return ___material_15; }
	inline FsmMaterial_t924399665 ** get_address_of_material_15() { return &___material_15; }
	inline void set_material_15(FsmMaterial_t924399665 * value)
	{
		___material_15 = value;
		Il2CppCodeGenWriteBarrier(&___material_15, value);
	}

	inline static int32_t get_offset_of_getSharedMaterial_16() { return static_cast<int32_t>(offsetof(GetMaterial_t1959127339, ___getSharedMaterial_16)); }
	inline bool get_getSharedMaterial_16() const { return ___getSharedMaterial_16; }
	inline bool* get_address_of_getSharedMaterial_16() { return &___getSharedMaterial_16; }
	inline void set_getSharedMaterial_16(bool value)
	{
		___getSharedMaterial_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
