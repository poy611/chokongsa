﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetScale
struct GetScale_t1712482364;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetScale::.ctor()
extern "C"  void GetScale__ctor_m1998820778 (GetScale_t1712482364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::Reset()
extern "C"  void GetScale_Reset_m3940221015 (GetScale_t1712482364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnEnter()
extern "C"  void GetScale_OnEnter_m408114945 (GetScale_t1712482364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnUpdate()
extern "C"  void GetScale_OnUpdate_m3195188066 (GetScale_t1712482364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::DoGetScale()
extern "C"  void GetScale_DoGetScale_m3997279513 (GetScale_t1712482364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
