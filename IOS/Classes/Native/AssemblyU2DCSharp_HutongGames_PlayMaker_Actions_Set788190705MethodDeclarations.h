﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEventTarget
struct SetEventTarget_t788190705;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::.ctor()
extern "C"  void SetEventTarget__ctor_m370889109 (SetEventTarget_t788190705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::Reset()
extern "C"  void SetEventTarget_Reset_m2312289346 (SetEventTarget_t788190705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventTarget::OnEnter()
extern "C"  void SetEventTarget_OnEnter_m3628844076 (SetEventTarget_t788190705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
