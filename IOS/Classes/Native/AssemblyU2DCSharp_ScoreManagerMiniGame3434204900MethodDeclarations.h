﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManagerMiniGame
struct ScoreManagerMiniGame_t3434204900;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManagerMiniGame::.ctor()
extern "C"  void ScoreManagerMiniGame__ctor_m2886143031 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManagerMiniGame::Start()
extern "C"  void ScoreManagerMiniGame_Start_m1833280823 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManagerMiniGame::StartMinigame()
extern "C"  void ScoreManagerMiniGame_StartMinigame_m3099216352 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManagerMiniGame::conFirm()
extern "C"  void ScoreManagerMiniGame_conFirm_m3547705173 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
