﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m3173673054_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m3173673054(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m3173673054_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>::<>m__9E(T1,T2)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 * __this, Il2CppObject * ___result10, bool ___result21, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458(__this, ___result10, ___result21, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 *, Il2CppObject *, bool, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458_gshared)(__this, ___result10, ___result21, method)
