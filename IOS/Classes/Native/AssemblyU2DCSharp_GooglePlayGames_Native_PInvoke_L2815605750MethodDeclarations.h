﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadScorePage>c__AnonStoreyA3
struct U3CLoadScorePageU3Ec__AnonStoreyA3_t2815605750;
// GooglePlayGames.Native.PInvoke.FetchScorePageResponse
struct FetchScorePageResponse_t1467305556;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_F1467305556.h"

// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadScorePage>c__AnonStoreyA3::.ctor()
extern "C"  void U3CLoadScorePageU3Ec__AnonStoreyA3__ctor_m1996144485 (U3CLoadScorePageU3Ec__AnonStoreyA3_t2815605750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadScorePage>c__AnonStoreyA3::<>m__A6(GooglePlayGames.Native.PInvoke.FetchScorePageResponse)
extern "C"  void U3CLoadScorePageU3Ec__AnonStoreyA3_U3CU3Em__A6_m376935044 (U3CLoadScorePageU3Ec__AnonStoreyA3_t2815605750 * __this, FetchScorePageResponse_t1467305556 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
