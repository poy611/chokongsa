﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B
struct  U3CwU3Ec__AnonStorey3B_t82508163  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B::msg
	String_t* ___msg_0;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CwU3Ec__AnonStorey3B_t82508163, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier(&___msg_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
