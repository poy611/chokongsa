﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse
struct FetchForPlayerResponse_t1267220047;
// GooglePlayGames.Native.PInvoke.NativePlayerStats
struct NativePlayerStats_t1419601613;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::.ctor(System.IntPtr)
extern "C"  void FetchForPlayerResponse__ctor_m955109544 (FetchForPlayerResponse_t1267220047 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::Status()
extern "C"  int32_t FetchForPlayerResponse_Status_m89763059 (FetchForPlayerResponse_t1267220047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativePlayerStats GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::PlayerStats()
extern "C"  NativePlayerStats_t1419601613 * FetchForPlayerResponse_PlayerStats_m3492990287 (FetchForPlayerResponse_t1267220047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchForPlayerResponse_CallDispose_m2311238152 (FetchForPlayerResponse_t1267220047 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::FromPointer(System.IntPtr)
extern "C"  FetchForPlayerResponse_t1267220047 * FetchForPlayerResponse_FromPointer_m897028869 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
