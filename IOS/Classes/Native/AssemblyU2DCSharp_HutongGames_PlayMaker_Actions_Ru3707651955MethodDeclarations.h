﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RunFSMAction
struct RunFSMAction_t3707651955;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"

// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::.ctor()
extern "C"  void RunFSMAction__ctor_m530454611 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::Reset()
extern "C"  void RunFSMAction_Reset_m2471854848 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.RunFSMAction::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool RunFSMAction_Event_m3121221841 (RunFSMAction_t3707651955 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnEnter()
extern "C"  void RunFSMAction_OnEnter_m2352468842 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnUpdate()
extern "C"  void RunFSMAction_OnUpdate_m3340616729 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnFixedUpdate()
extern "C"  void RunFSMAction_OnFixedUpdate_m2409804783 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnLateUpdate()
extern "C"  void RunFSMAction_OnLateUpdate_m1850594655 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerEnter_m586271665 (RunFSMAction_t3707651955 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerStay(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerStay_m1594187468 (RunFSMAction_t3707651955 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerExit(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerExit_m3271065553 (RunFSMAction_t3707651955 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionEnter_m2959133429 (RunFSMAction_t3707651955 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionStay(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionStay_m1974970758 (RunFSMAction_t3707651955 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionExit(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionExit_m2418583841 (RunFSMAction_t3707651955 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void RunFSMAction_DoParticleCollision_m1166279978 (RunFSMAction_t3707651955 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void RunFSMAction_DoControllerColliderHit_m1513875325 (RunFSMAction_t3707651955 * __this, ControllerColliderHit_t2416790841 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerEnter2D_m546192177 (RunFSMAction_t3707651955 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerStay2D_m874301644 (RunFSMAction_t3707651955 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerExit2D_m937287505 (RunFSMAction_t3707651955 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionEnter2D_m4086270481 (RunFSMAction_t3707651955 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionStay2D_m1581430818 (RunFSMAction_t3707651955 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionExit2D_m3533992509 (RunFSMAction_t3707651955 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnGUI()
extern "C"  void RunFSMAction_OnGUI_m25853261 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnExit()
extern "C"  void RunFSMAction_OnExit_m777542382 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::CheckIfFinished()
extern "C"  void RunFSMAction_CheckIfFinished_m1121579912 (RunFSMAction_t3707651955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
