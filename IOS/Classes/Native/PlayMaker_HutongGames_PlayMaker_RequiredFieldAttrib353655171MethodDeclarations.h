﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.RequiredFieldAttribute
struct RequiredFieldAttribute_t353655171;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.RequiredFieldAttribute::.ctor()
extern "C"  void RequiredFieldAttribute__ctor_m3823431216 (RequiredFieldAttribute_t353655171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
