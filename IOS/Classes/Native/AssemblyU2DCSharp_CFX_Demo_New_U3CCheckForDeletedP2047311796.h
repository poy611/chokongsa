﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// CFX_Demo_New
struct CFX_Demo_New_t4019803630;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7
struct  U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796  : public Il2CppObject
{
public:
	// System.Int32 CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::$PC
	int32_t ___U24PC_1;
	// System.Object CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::$current
	Il2CppObject * ___U24current_2;
	// CFX_Demo_New CFX_Demo_New/<CheckForDeletedParticles>c__Iterator7::<>f__this
	CFX_Demo_New_t4019803630 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ec__Iterator7_t2047311796, ___U3CU3Ef__this_3)); }
	inline CFX_Demo_New_t4019803630 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline CFX_Demo_New_t4019803630 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(CFX_Demo_New_t4019803630 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
