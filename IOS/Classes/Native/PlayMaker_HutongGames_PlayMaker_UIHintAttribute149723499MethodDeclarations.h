﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.UIHintAttribute
struct UIHintAttribute_t149723499;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHint1985716317.h"

// HutongGames.PlayMaker.UIHint HutongGames.PlayMaker.UIHintAttribute::get_Hint()
extern "C"  int32_t UIHintAttribute_get_Hint_m952298310 (UIHintAttribute_t149723499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.UIHintAttribute::.ctor(HutongGames.PlayMaker.UIHint)
extern "C"  void UIHintAttribute__ctor_m3168915085 (UIHintAttribute_t149723499 * __this, int32_t ___hint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
