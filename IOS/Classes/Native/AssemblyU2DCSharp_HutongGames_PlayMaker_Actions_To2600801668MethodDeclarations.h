﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchObjectEvent
struct TouchObjectEvent_t2600801668;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TouchObjectEvent::.ctor()
extern "C"  void TouchObjectEvent__ctor_m247054690 (TouchObjectEvent_t2600801668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchObjectEvent::Reset()
extern "C"  void TouchObjectEvent_Reset_m2188454927 (TouchObjectEvent_t2600801668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchObjectEvent::OnUpdate()
extern "C"  void TouchObjectEvent_OnUpdate_m184306858 (TouchObjectEvent_t2600801668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
