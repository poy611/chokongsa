﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolAllTrue
struct BoolAllTrue_t469898003;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::.ctor()
extern "C"  void BoolAllTrue__ctor_m2721947011 (BoolAllTrue_t469898003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::Reset()
extern "C"  void BoolAllTrue_Reset_m368379952 (BoolAllTrue_t469898003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::OnEnter()
extern "C"  void BoolAllTrue_OnEnter_m3842690202 (BoolAllTrue_t469898003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::OnUpdate()
extern "C"  void BoolAllTrue_OnUpdate_m2292838633 (BoolAllTrue_t469898003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::DoAllTrue()
extern "C"  void BoolAllTrue_DoAllTrue_m1998284133 (BoolAllTrue_t469898003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
