﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct DefaultContractResolverState_t822678628;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolverState::.ctor()
extern "C"  void DefaultContractResolverState__ctor_m4068936475 (DefaultContractResolverState_t822678628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
