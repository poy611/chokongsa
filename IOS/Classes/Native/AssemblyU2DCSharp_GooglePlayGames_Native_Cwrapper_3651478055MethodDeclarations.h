﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
struct OnRoomConnectedSetChangedCallback_t3651478055;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRoomConnectedSetChangedCallback__ctor_m2174851710 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void OnRoomConnectedSetChangedCallback_Invoke_m4292857738 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnRoomConnectedSetChangedCallback_BeginInvoke_m4259196311 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnRoomConnectedSetChangedCallback_EndInvoke_m2540829582 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
