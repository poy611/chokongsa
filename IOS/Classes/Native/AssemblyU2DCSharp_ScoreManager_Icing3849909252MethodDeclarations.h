﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Icing
struct ScoreManager_Icing_t3849909252;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"

// System.Void ScoreManager_Icing::.ctor()
extern "C"  void ScoreManager_Icing__ctor_m1789219095 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::Start()
extern "C"  void ScoreManager_Icing_Start_m736356887 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::buttonOkClickSound()
extern "C"  void ScoreManager_Icing_buttonOkClickSound_m783903394 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::startIcing()
extern "C"  void ScoreManager_Icing_startIcing_m1880382451 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::ButtonClickEvent(PlayMakerFSM)
extern "C"  void ScoreManager_Icing_ButtonClickEvent_m3158103361 (ScoreManager_Icing_t3849909252 * __this, PlayMakerFSM_t3799847376 * ____myFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Icing::Finish()
extern "C"  Il2CppObject * ScoreManager_Icing_Finish_m3471962216 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::isRightSelect(System.Int32)
extern "C"  void ScoreManager_Icing_isRightSelect_m2982517332 (ScoreManager_Icing_t3849909252 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::TimeCheckerStart()
extern "C"  void ScoreManager_Icing_TimeCheckerStart_m2412828135 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Icing::TimerCheck()
extern "C"  Il2CppObject * ScoreManager_Icing_TimerCheck_m3021594264 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Icing::TimeEnd()
extern "C"  Il2CppObject * ScoreManager_Icing_TimeEnd_m3972789051 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing::conFirm()
extern "C"  void ScoreManager_Icing_conFirm_m1670790197 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
