﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2975001167.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveFloat
struct  CurveFloat_t907312149  : public CurveFsmAction_t2975001167
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_35;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::fromValue
	FsmFloat_t2134102846 * ___fromValue_36;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::toValue
	FsmFloat_t2134102846 * ___toValue_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveFloat::animCurve
	FsmAnimationCurve_t2685995989 * ___animCurve_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveFloat::calculation
	int32_t ___calculation_39;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFloat::finishInNextStep
	bool ___finishInNextStep_40;

public:
	inline static int32_t get_offset_of_floatVariable_35() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___floatVariable_35)); }
	inline FsmFloat_t2134102846 * get_floatVariable_35() const { return ___floatVariable_35; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_35() { return &___floatVariable_35; }
	inline void set_floatVariable_35(FsmFloat_t2134102846 * value)
	{
		___floatVariable_35 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_35, value);
	}

	inline static int32_t get_offset_of_fromValue_36() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___fromValue_36)); }
	inline FsmFloat_t2134102846 * get_fromValue_36() const { return ___fromValue_36; }
	inline FsmFloat_t2134102846 ** get_address_of_fromValue_36() { return &___fromValue_36; }
	inline void set_fromValue_36(FsmFloat_t2134102846 * value)
	{
		___fromValue_36 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_36, value);
	}

	inline static int32_t get_offset_of_toValue_37() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___toValue_37)); }
	inline FsmFloat_t2134102846 * get_toValue_37() const { return ___toValue_37; }
	inline FsmFloat_t2134102846 ** get_address_of_toValue_37() { return &___toValue_37; }
	inline void set_toValue_37(FsmFloat_t2134102846 * value)
	{
		___toValue_37 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_37, value);
	}

	inline static int32_t get_offset_of_animCurve_38() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___animCurve_38)); }
	inline FsmAnimationCurve_t2685995989 * get_animCurve_38() const { return ___animCurve_38; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_animCurve_38() { return &___animCurve_38; }
	inline void set_animCurve_38(FsmAnimationCurve_t2685995989 * value)
	{
		___animCurve_38 = value;
		Il2CppCodeGenWriteBarrier(&___animCurve_38, value);
	}

	inline static int32_t get_offset_of_calculation_39() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___calculation_39)); }
	inline int32_t get_calculation_39() const { return ___calculation_39; }
	inline int32_t* get_address_of_calculation_39() { return &___calculation_39; }
	inline void set_calculation_39(int32_t value)
	{
		___calculation_39 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_40() { return static_cast<int32_t>(offsetof(CurveFloat_t907312149, ___finishInNextStep_40)); }
	inline bool get_finishInNextStep_40() const { return ___finishInNextStep_40; }
	inline bool* get_address_of_finishInNextStep_40() { return &___finishInNextStep_40; }
	inline void set_finishInNextStep_40(bool value)
	{
		___finishInNextStep_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
