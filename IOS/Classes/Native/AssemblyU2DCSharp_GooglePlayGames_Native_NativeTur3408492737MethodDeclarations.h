﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey89
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey89_t3408492737;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t922401854;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Pl922401854.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey89::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey89__ctor_m3855340490 (U3CCreateWithInvitationScreenU3Ec__AnonStorey89_t3408492737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey89::<>m__78(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey89_U3CU3Em__78_m3845868425 (U3CCreateWithInvitationScreenU3Ec__AnonStorey89_t3408492737 * __this, PlayerSelectUIResponse_t922401854 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
