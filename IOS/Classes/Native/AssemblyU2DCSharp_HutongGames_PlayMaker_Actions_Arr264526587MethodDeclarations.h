﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayResize
struct ArrayResize_t264526587;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayResize::.ctor()
extern "C"  void ArrayResize__ctor_m2309601947 (ArrayResize_t264526587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayResize::OnEnter()
extern "C"  void ArrayResize_OnEnter_m2716074930 (ArrayResize_t264526587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
