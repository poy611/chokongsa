﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetOwner
struct GetOwner_t1709396389;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetOwner::.ctor()
extern "C"  void GetOwner__ctor_m1265119841 (GetOwner_t1709396389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetOwner::Reset()
extern "C"  void GetOwner_Reset_m3206520078 (GetOwner_t1709396389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetOwner::OnEnter()
extern "C"  void GetOwner_OnEnter_m3991118328 (GetOwner_t1709396389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
