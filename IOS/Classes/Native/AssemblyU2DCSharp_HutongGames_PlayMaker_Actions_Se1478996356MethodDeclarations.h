﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITextureAlpha
struct SetGUITextureAlpha_t1478996356;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::.ctor()
extern "C"  void SetGUITextureAlpha__ctor_m1461583714 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::Reset()
extern "C"  void SetGUITextureAlpha_Reset_m3402983951 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnEnter()
extern "C"  void SetGUITextureAlpha_OnEnter_m3814339257 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnUpdate()
extern "C"  void SetGUITextureAlpha_OnUpdate_m1413959338 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::DoGUITextureAlpha()
extern "C"  void SetGUITextureAlpha_DoGUITextureAlpha_m1856069395 (SetGUITextureAlpha_t1478996356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
