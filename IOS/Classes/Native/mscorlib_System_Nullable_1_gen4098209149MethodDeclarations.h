﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen4098209149.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateFormatHandling4014082626.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m31417437_gshared (Nullable_1_t4098209149 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m31417437(__this, ___value0, method) ((  void (*) (Nullable_1_t4098209149 *, int32_t, const MethodInfo*))Nullable_1__ctor_m31417437_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m740879342_gshared (Nullable_1_t4098209149 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m740879342(__this, method) ((  bool (*) (Nullable_1_t4098209149 *, const MethodInfo*))Nullable_1_get_HasValue_m740879342_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2405045829_gshared (Nullable_1_t4098209149 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2405045829(__this, method) ((  int32_t (*) (Nullable_1_t4098209149 *, const MethodInfo*))Nullable_1_get_Value_m2405045829_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4187870547_gshared (Nullable_1_t4098209149 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4187870547(__this, ___other0, method) ((  bool (*) (Nullable_1_t4098209149 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m4187870547_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3348867724_gshared (Nullable_1_t4098209149 * __this, Nullable_1_t4098209149  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3348867724(__this, ___other0, method) ((  bool (*) (Nullable_1_t4098209149 *, Nullable_1_t4098209149 , const MethodInfo*))Nullable_1_Equals_m3348867724_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3981267499_gshared (Nullable_1_t4098209149 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3981267499(__this, method) ((  int32_t (*) (Nullable_1_t4098209149 *, const MethodInfo*))Nullable_1_GetHashCode_m3981267499_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3676118033_gshared (Nullable_1_t4098209149 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3676118033(__this, method) ((  int32_t (*) (Nullable_1_t4098209149 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3676118033_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3923582910_gshared (Nullable_1_t4098209149 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3923582910(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t4098209149 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m3923582910_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.DateFormatHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4200951885_gshared (Nullable_1_t4098209149 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m4200951885(__this, method) ((  String_t* (*) (Nullable_1_t4098209149 *, const MethodInfo*))Nullable_1_ToString_m4200951885_gshared)(__this, method)
