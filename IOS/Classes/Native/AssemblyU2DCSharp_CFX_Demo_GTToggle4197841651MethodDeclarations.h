﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_GTToggle
struct CFX_Demo_GTToggle_t4197841651;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_GTToggle::.ctor()
extern "C"  void CFX_Demo_GTToggle__ctor_m3815592920 (CFX_Demo_GTToggle_t4197841651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::Awake()
extern "C"  void CFX_Demo_GTToggle_Awake_m4053198139 (CFX_Demo_GTToggle_t4197841651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::Update()
extern "C"  void CFX_Demo_GTToggle_Update_m4046125621 (CFX_Demo_GTToggle_t4197841651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::OnClick()
extern "C"  void CFX_Demo_GTToggle_OnClick_m726930527 (CFX_Demo_GTToggle_t4197841651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_GTToggle::UpdateTexture()
extern "C"  void CFX_Demo_GTToggle_UpdateTexture_m538158984 (CFX_Demo_GTToggle_t4197841651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
