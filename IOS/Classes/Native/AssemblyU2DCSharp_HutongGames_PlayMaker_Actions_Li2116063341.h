﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LineCast2d
struct  LineCast2d_t2116063341  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LineCast2d::fromGameObject
	FsmOwnerDefault_t251897112 * ___fromGameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::fromPosition
	FsmVector2_t533912881 * ___fromPosition_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LineCast2d::toGameObject
	FsmGameObject_t1697147867 * ___toGameObject_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::toPosition
	FsmVector2_t533912881 * ___toPosition_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::minDepth
	FsmInt_t1596138449 * ___minDepth_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::maxDepth
	FsmInt_t1596138449 * ___maxDepth_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LineCast2d::hitEvent
	FsmEvent_t2133468028 * ___hitEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::storeDidHit
	FsmBool_t1075959796 * ___storeDidHit_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LineCast2d::storeHitObject
	FsmGameObject_t1697147867 * ___storeHitObject_19;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::storeHitPoint
	FsmVector2_t533912881 * ___storeHitPoint_20;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::storeHitNormal
	FsmVector2_t533912881 * ___storeHitNormal_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LineCast2d::storeHitDistance
	FsmFloat_t2134102846 * ___storeHitDistance_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::repeatInterval
	FsmInt_t1596138449 * ___repeatInterval_23;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.LineCast2d::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_24;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::invertMask
	FsmBool_t1075959796 * ___invertMask_25;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LineCast2d::debugColor
	FsmColor_t2131419205 * ___debugColor_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::debug
	FsmBool_t1075959796 * ___debug_27;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.LineCast2d::_fromTrans
	Transform_t1659122786 * ____fromTrans_28;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.LineCast2d::_toTrans
	Transform_t1659122786 * ____toTrans_29;
	// System.Int32 HutongGames.PlayMaker.Actions.LineCast2d::repeat
	int32_t ___repeat_30;

public:
	inline static int32_t get_offset_of_fromGameObject_11() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___fromGameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_fromGameObject_11() const { return ___fromGameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_fromGameObject_11() { return &___fromGameObject_11; }
	inline void set_fromGameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___fromGameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___fromGameObject_11, value);
	}

	inline static int32_t get_offset_of_fromPosition_12() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___fromPosition_12)); }
	inline FsmVector2_t533912881 * get_fromPosition_12() const { return ___fromPosition_12; }
	inline FsmVector2_t533912881 ** get_address_of_fromPosition_12() { return &___fromPosition_12; }
	inline void set_fromPosition_12(FsmVector2_t533912881 * value)
	{
		___fromPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___fromPosition_12, value);
	}

	inline static int32_t get_offset_of_toGameObject_13() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___toGameObject_13)); }
	inline FsmGameObject_t1697147867 * get_toGameObject_13() const { return ___toGameObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_toGameObject_13() { return &___toGameObject_13; }
	inline void set_toGameObject_13(FsmGameObject_t1697147867 * value)
	{
		___toGameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___toGameObject_13, value);
	}

	inline static int32_t get_offset_of_toPosition_14() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___toPosition_14)); }
	inline FsmVector2_t533912881 * get_toPosition_14() const { return ___toPosition_14; }
	inline FsmVector2_t533912881 ** get_address_of_toPosition_14() { return &___toPosition_14; }
	inline void set_toPosition_14(FsmVector2_t533912881 * value)
	{
		___toPosition_14 = value;
		Il2CppCodeGenWriteBarrier(&___toPosition_14, value);
	}

	inline static int32_t get_offset_of_minDepth_15() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___minDepth_15)); }
	inline FsmInt_t1596138449 * get_minDepth_15() const { return ___minDepth_15; }
	inline FsmInt_t1596138449 ** get_address_of_minDepth_15() { return &___minDepth_15; }
	inline void set_minDepth_15(FsmInt_t1596138449 * value)
	{
		___minDepth_15 = value;
		Il2CppCodeGenWriteBarrier(&___minDepth_15, value);
	}

	inline static int32_t get_offset_of_maxDepth_16() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___maxDepth_16)); }
	inline FsmInt_t1596138449 * get_maxDepth_16() const { return ___maxDepth_16; }
	inline FsmInt_t1596138449 ** get_address_of_maxDepth_16() { return &___maxDepth_16; }
	inline void set_maxDepth_16(FsmInt_t1596138449 * value)
	{
		___maxDepth_16 = value;
		Il2CppCodeGenWriteBarrier(&___maxDepth_16, value);
	}

	inline static int32_t get_offset_of_hitEvent_17() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___hitEvent_17)); }
	inline FsmEvent_t2133468028 * get_hitEvent_17() const { return ___hitEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_hitEvent_17() { return &___hitEvent_17; }
	inline void set_hitEvent_17(FsmEvent_t2133468028 * value)
	{
		___hitEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_17, value);
	}

	inline static int32_t get_offset_of_storeDidHit_18() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___storeDidHit_18)); }
	inline FsmBool_t1075959796 * get_storeDidHit_18() const { return ___storeDidHit_18; }
	inline FsmBool_t1075959796 ** get_address_of_storeDidHit_18() { return &___storeDidHit_18; }
	inline void set_storeDidHit_18(FsmBool_t1075959796 * value)
	{
		___storeDidHit_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeDidHit_18, value);
	}

	inline static int32_t get_offset_of_storeHitObject_19() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___storeHitObject_19)); }
	inline FsmGameObject_t1697147867 * get_storeHitObject_19() const { return ___storeHitObject_19; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeHitObject_19() { return &___storeHitObject_19; }
	inline void set_storeHitObject_19(FsmGameObject_t1697147867 * value)
	{
		___storeHitObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitObject_19, value);
	}

	inline static int32_t get_offset_of_storeHitPoint_20() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___storeHitPoint_20)); }
	inline FsmVector2_t533912881 * get_storeHitPoint_20() const { return ___storeHitPoint_20; }
	inline FsmVector2_t533912881 ** get_address_of_storeHitPoint_20() { return &___storeHitPoint_20; }
	inline void set_storeHitPoint_20(FsmVector2_t533912881 * value)
	{
		___storeHitPoint_20 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitPoint_20, value);
	}

	inline static int32_t get_offset_of_storeHitNormal_21() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___storeHitNormal_21)); }
	inline FsmVector2_t533912881 * get_storeHitNormal_21() const { return ___storeHitNormal_21; }
	inline FsmVector2_t533912881 ** get_address_of_storeHitNormal_21() { return &___storeHitNormal_21; }
	inline void set_storeHitNormal_21(FsmVector2_t533912881 * value)
	{
		___storeHitNormal_21 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitNormal_21, value);
	}

	inline static int32_t get_offset_of_storeHitDistance_22() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___storeHitDistance_22)); }
	inline FsmFloat_t2134102846 * get_storeHitDistance_22() const { return ___storeHitDistance_22; }
	inline FsmFloat_t2134102846 ** get_address_of_storeHitDistance_22() { return &___storeHitDistance_22; }
	inline void set_storeHitDistance_22(FsmFloat_t2134102846 * value)
	{
		___storeHitDistance_22 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitDistance_22, value);
	}

	inline static int32_t get_offset_of_repeatInterval_23() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___repeatInterval_23)); }
	inline FsmInt_t1596138449 * get_repeatInterval_23() const { return ___repeatInterval_23; }
	inline FsmInt_t1596138449 ** get_address_of_repeatInterval_23() { return &___repeatInterval_23; }
	inline void set_repeatInterval_23(FsmInt_t1596138449 * value)
	{
		___repeatInterval_23 = value;
		Il2CppCodeGenWriteBarrier(&___repeatInterval_23, value);
	}

	inline static int32_t get_offset_of_layerMask_24() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___layerMask_24)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_24() const { return ___layerMask_24; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_24() { return &___layerMask_24; }
	inline void set_layerMask_24(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_24 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_24, value);
	}

	inline static int32_t get_offset_of_invertMask_25() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___invertMask_25)); }
	inline FsmBool_t1075959796 * get_invertMask_25() const { return ___invertMask_25; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_25() { return &___invertMask_25; }
	inline void set_invertMask_25(FsmBool_t1075959796 * value)
	{
		___invertMask_25 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_25, value);
	}

	inline static int32_t get_offset_of_debugColor_26() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___debugColor_26)); }
	inline FsmColor_t2131419205 * get_debugColor_26() const { return ___debugColor_26; }
	inline FsmColor_t2131419205 ** get_address_of_debugColor_26() { return &___debugColor_26; }
	inline void set_debugColor_26(FsmColor_t2131419205 * value)
	{
		___debugColor_26 = value;
		Il2CppCodeGenWriteBarrier(&___debugColor_26, value);
	}

	inline static int32_t get_offset_of_debug_27() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___debug_27)); }
	inline FsmBool_t1075959796 * get_debug_27() const { return ___debug_27; }
	inline FsmBool_t1075959796 ** get_address_of_debug_27() { return &___debug_27; }
	inline void set_debug_27(FsmBool_t1075959796 * value)
	{
		___debug_27 = value;
		Il2CppCodeGenWriteBarrier(&___debug_27, value);
	}

	inline static int32_t get_offset_of__fromTrans_28() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ____fromTrans_28)); }
	inline Transform_t1659122786 * get__fromTrans_28() const { return ____fromTrans_28; }
	inline Transform_t1659122786 ** get_address_of__fromTrans_28() { return &____fromTrans_28; }
	inline void set__fromTrans_28(Transform_t1659122786 * value)
	{
		____fromTrans_28 = value;
		Il2CppCodeGenWriteBarrier(&____fromTrans_28, value);
	}

	inline static int32_t get_offset_of__toTrans_29() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ____toTrans_29)); }
	inline Transform_t1659122786 * get__toTrans_29() const { return ____toTrans_29; }
	inline Transform_t1659122786 ** get_address_of__toTrans_29() { return &____toTrans_29; }
	inline void set__toTrans_29(Transform_t1659122786 * value)
	{
		____toTrans_29 = value;
		Il2CppCodeGenWriteBarrier(&____toTrans_29, value);
	}

	inline static int32_t get_offset_of_repeat_30() { return static_cast<int32_t>(offsetof(LineCast2d_t2116063341, ___repeat_30)); }
	inline int32_t get_repeat_30() const { return ___repeat_30; }
	inline int32_t* get_address_of_repeat_30() { return &___repeat_30; }
	inline void set_repeat_30(int32_t value)
	{
		___repeat_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
