﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c
struct U3CU3Ec_t3898342781;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1066515050 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m1158333859 (U3CU3Ec_t3898342781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<GetJsonConverterCreator>b__18_1(System.Object)
extern "C"  Type_t * U3CU3Ec_U3CGetJsonConverterCreatorU3Eb__18_1_m513152994 (U3CU3Ec_t3898342781 * __this, Il2CppObject * ___param0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
