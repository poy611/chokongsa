﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2980502528.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_AutomatchingSlotsAvailable_m2902189243 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t TurnBasedMatch_TurnBasedMatch_CreationTime_m4226460678 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Participants_Length_m2778357555 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_Participants_GetElement_m3392342993 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Version(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Version_m2006707929 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_ParticipantResults(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_ParticipantResults_m1708809668 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMatch_TurnBasedMatch_Status_m3735410325 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Description_m409076355 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PendingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_PendingParticipant_m259424861 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Variant_m425458246 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasPreviousMatchData(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasPreviousMatchData_m3805740416 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Data(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Data_m352361334 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_LastUpdatingParticipant_m2087426610 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasData(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasData_m2996543742 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_SuggestedNextParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_SuggestedNextParticipant_m1768475230 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PreviousMatchData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_PreviousMatchData_m1923701096 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdateTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t TurnBasedMatch_TurnBasedMatch_LastUpdateTime_m571059494 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_RematchId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_RematchId_m861563570 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Number(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Number_m1393226596 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasRematchId(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasRematchId_m1583527829 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_Valid_m1987500182 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_CreatingParticipant_m2180183573 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Id_m2011101988 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatch_TurnBasedMatch_Dispose_m3970694469 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
