﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218;
// GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52
struct U3CLoadUsersU3Ec__AnonStorey52_t1766801020;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53
struct  U3CLoadUsersU3Ec__AnonStorey53_t2015416768  : public Il2CppObject
{
public:
	// UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53::users
	IUserProfileU5BU5D_t3419104218* ___users_0;
	// GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52 GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53::<>f__ref$82
	U3CLoadUsersU3Ec__AnonStorey52_t1766801020 * ___U3CU3Ef__refU2482_1;

public:
	inline static int32_t get_offset_of_users_0() { return static_cast<int32_t>(offsetof(U3CLoadUsersU3Ec__AnonStorey53_t2015416768, ___users_0)); }
	inline IUserProfileU5BU5D_t3419104218* get_users_0() const { return ___users_0; }
	inline IUserProfileU5BU5D_t3419104218** get_address_of_users_0() { return &___users_0; }
	inline void set_users_0(IUserProfileU5BU5D_t3419104218* value)
	{
		___users_0 = value;
		Il2CppCodeGenWriteBarrier(&___users_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2482_1() { return static_cast<int32_t>(offsetof(U3CLoadUsersU3Ec__AnonStorey53_t2015416768, ___U3CU3Ef__refU2482_1)); }
	inline U3CLoadUsersU3Ec__AnonStorey52_t1766801020 * get_U3CU3Ef__refU2482_1() const { return ___U3CU3Ef__refU2482_1; }
	inline U3CLoadUsersU3Ec__AnonStorey52_t1766801020 ** get_address_of_U3CU3Ef__refU2482_1() { return &___U3CU3Ef__refU2482_1; }
	inline void set_U3CU3Ef__refU2482_1(U3CLoadUsersU3Ec__AnonStorey52_t1766801020 * value)
	{
		___U3CU3Ef__refU2482_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2482_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
