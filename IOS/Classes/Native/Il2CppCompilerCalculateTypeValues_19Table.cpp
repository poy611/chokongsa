﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Trac4029697686.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Trac4185102918.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_CommentHandli2319298658.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_LineInfoHandl1889929367.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JConstructor3493545088.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JContainer_JT3005068644.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject_U3CGe1105132578.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty_JPr4118229972.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty_JPr3465727442.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JPropertyKeye1970079725.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JRaw225059758.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonLoadSetti1368013569.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken_LineIn3303106648.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken_U3CGet2219391906.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenReader3546959010.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenWriter3702364242.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JValue3413677367.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_BinaryC3850399473.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_BsonObj2170026696.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_KeyValu3986497414.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_RegexCo1616016887.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinaryType407343707.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinaryWri2157484308.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonObjectId1082490490.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonToken455725415.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonObject2743735103.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonArray438274503.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonValue457156831.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonString2875117585.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinary2378136577.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonRegex453576629.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonProperty4293821333.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonType2455132538.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonWriter2987529331.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E_973660384.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E_973660386.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E_973660423.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E_973660510.h"
#include "UnityEngine_U3CModuleU3E86524790.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest_Co561206607.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "UnityEngine_UnityEngine_Cursor2745727898.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_Flare4197217604.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumen4047764288.h"
#include "UnityEngine_UnityEngine_Graphics3672240399.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_GL2267613321.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent2820176033.h"
#include "UnityEngine_UnityEngine_CullingGroup1868862003.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2578300556.h"
#include "UnityEngine_UnityEngine_Gradient3661184436.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Interna705488572.h"
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode3302654991.h"
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode4213044537.h"
#include "UnityEngine_UnityEngine_Handheld3573483176.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType2604324130.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard1858258760.h"
#include "UnityEngine_UnityEngine_Gizmos2849394813.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (TraceJsonReader_t4029697686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[3] = 
{
	TraceJsonReader_t4029697686::get_offset_of__innerReader_15(),
	TraceJsonReader_t4029697686::get_offset_of__textWriter_16(),
	TraceJsonReader_t4029697686::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (TraceJsonWriter_t4185102918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[3] = 
{
	TraceJsonWriter_t4185102918::get_offset_of__innerWriter_13(),
	TraceJsonWriter_t4185102918::get_offset_of__textWriter_14(),
	TraceJsonWriter_t4185102918::get_offset_of__sw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (CommentHandling_t2319298658)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[3] = 
{
	CommentHandling_t2319298658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (LineInfoHandling_t1889929367)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	LineInfoHandling_t1889929367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (JArray_t3394795039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[1] = 
{
	JArray_t3394795039::get_offset_of__values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (JConstructor_t3493545088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	JConstructor_t3493545088::get_offset_of__name_15(),
	JConstructor_t3493545088::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (JContainer_t3364442311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	JContainer_t3364442311::get_offset_of__syncRoot_13(),
	JContainer_t3364442311::get_offset_of__busy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (JTokenReferenceEqualityComparer_t3005068644), -1, sizeof(JTokenReferenceEqualityComparer_t3005068644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1908[1] = 
{
	JTokenReferenceEqualityComparer_t3005068644_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (JObject_t1798544199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[2] = 
{
	JObject_t1798544199::get_offset_of__properties_15(),
	JObject_t1798544199::get_offset_of_PropertyChanged_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (U3CGetEnumeratorU3Ed__54_t1105132578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[4] = 
{
	U3CGetEnumeratorU3Ed__54_t1105132578::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__54_t1105132578::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__54_t1105132578::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__54_t1105132578::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (JProperty_t2616415645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[2] = 
{
	JProperty_t2616415645::get_offset_of__content_15(),
	JProperty_t2616415645::get_offset_of__name_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (JPropertyList_t4118229972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[1] = 
{
	JPropertyList_t4118229972::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (U3CGetEnumeratorU3Ed__1_t3465727442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[3] = 
{
	U3CGetEnumeratorU3Ed__1_t3465727442::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t3465727442::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t3465727442::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (JPropertyKeyedCollection_t1970079725), -1, sizeof(JPropertyKeyedCollection_t1970079725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1915[2] = 
{
	JPropertyKeyedCollection_t1970079725_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_t1970079725::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (JRaw_t225059758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (JsonLoadSettings_t1368013569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[2] = 
{
	JsonLoadSettings_t1368013569::get_offset_of__commentHandling_0(),
	JsonLoadSettings_t1368013569::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (JToken_t3412245951), -1, sizeof(JToken_t3412245951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1918[13] = 
{
	JToken_t3412245951::get_offset_of__parent_0(),
	JToken_t3412245951::get_offset_of__previous_1(),
	JToken_t3412245951::get_offset_of__next_2(),
	JToken_t3412245951::get_offset_of__annotations_3(),
	JToken_t3412245951_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_t3412245951_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_t3412245951_StaticFields::get_offset_of_StringTypes_6(),
	JToken_t3412245951_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_t3412245951_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_t3412245951_StaticFields::get_offset_of_UriTypes_9(),
	JToken_t3412245951_StaticFields::get_offset_of_CharTypes_10(),
	JToken_t3412245951_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_t3412245951_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (LineInfoAnnotation_t3303106648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[2] = 
{
	LineInfoAnnotation_t3303106648::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t3303106648::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (U3CGetAncestorsU3Ed__41_t2219391906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[7] = 
{
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CU3E1__state_0(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CU3E2__current_1(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_self_3(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CU3E3__self_4(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CU3E4__this_5(),
	U3CGetAncestorsU3Ed__41_t2219391906::get_offset_of_U3CcurrentU3E5__1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (JTokenReader_t3546959010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[4] = 
{
	JTokenReader_t3546959010::get_offset_of__initialPath_15(),
	JTokenReader_t3546959010::get_offset_of__root_16(),
	JTokenReader_t3546959010::get_offset_of__parent_17(),
	JTokenReader_t3546959010::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (JTokenType_t3916897561)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[19] = 
{
	JTokenType_t3916897561::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (JTokenWriter_t3702364242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[4] = 
{
	JTokenWriter_t3702364242::get_offset_of__token_13(),
	JTokenWriter_t3702364242::get_offset_of__parent_14(),
	JTokenWriter_t3702364242::get_offset_of__value_15(),
	JTokenWriter_t3702364242::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (JValue_t3413677367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[2] = 
{
	JValue_t3413677367::get_offset_of__valueType_13(),
	JValue_t3413677367::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (BinaryConverter_t3850399473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[1] = 
{
	BinaryConverter_t3850399473::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (BsonObjectIdConverter_t2170026696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (KeyValuePairConverter_t3986497414), -1, sizeof(KeyValuePairConverter_t3986497414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1927[1] = 
{
	KeyValuePairConverter_t3986497414_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (RegexConverter_t1616016887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (BsonBinaryType_t407343707)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[8] = 
{
	BsonBinaryType_t407343707::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (BsonBinaryWriter_t2157484308), -1, sizeof(BsonBinaryWriter_t2157484308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	BsonBinaryWriter_t2157484308_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t2157484308::get_offset_of__writer_1(),
	BsonBinaryWriter_t2157484308::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t2157484308::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (BsonObjectId_t1082490490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[1] = 
{
	BsonObjectId_t1082490490::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (BsonToken_t455725415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[2] = 
{
	BsonToken_t455725415::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t455725415::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (BsonObject_t2743735103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[1] = 
{
	BsonObject_t2743735103::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (BsonArray_t438274503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	BsonArray_t438274503::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (BsonValue_t457156831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[2] = 
{
	BsonValue_t457156831::get_offset_of__value_2(),
	BsonValue_t457156831::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (BsonString_t2875117585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[2] = 
{
	BsonString_t2875117585::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t2875117585::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (BsonBinary_t2378136577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	BsonBinary_t2378136577::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (BsonRegex_t453576629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[2] = 
{
	BsonRegex_t453576629::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t453576629::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (BsonProperty_t4293821333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[2] = 
{
	BsonProperty_t4293821333::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t4293821333::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (BsonType_t2455132538)+ sizeof (Il2CppObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1940[21] = 
{
	BsonType_t2455132538::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (BsonWriter_t2987529331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[4] = 
{
	BsonWriter_t2987529331::get_offset_of__writer_13(),
	BsonWriter_t2987529331::get_offset_of__root_14(),
	BsonWriter_t2987529331::get_offset_of__parent_15(),
	BsonWriter_t2987529331::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238938), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (__StaticArrayInitTypeSizeU3D10_t973660384)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D10_t973660384_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (__StaticArrayInitTypeSizeU3D12_t973660386)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D12_t973660386_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (__StaticArrayInitTypeSizeU3D28_t973660423)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D28_t973660423_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (__StaticArrayInitTypeSizeU3D52_t973660510)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D52_t973660510_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (AssetBundleCreateRequest_t1416890373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CompatibilityCheck_t561206607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[6] = 
{
	CompatibilityCheck_t561206607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (AssetBundleRequest_t2154290273), sizeof(AssetBundleRequest_t2154290273_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (AssetBundle_t2070959688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (SendMessageOptions_t3856946179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1952[3] = 
{
	SendMessageOptions_t3856946179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (Space_t4209342076)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1953[3] = 
{
	Space_t4209342076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (RuntimePlatform_t3050318497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[32] = 
{
	RuntimePlatform_t3050318497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (LogType_t4286006228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[6] = 
{
	LogType_t4286006228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (SystemInfo_t3820892225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (WaitForSeconds_t1615819279), sizeof(WaitForSeconds_t1615819279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1957[1] = 
{
	WaitForSeconds_t1615819279::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (WaitForFixedUpdate_t2130080621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (WaitForEndOfFrame_t2372756133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (CustomYieldInstruction_t2666549910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (Coroutine_t3621161934), sizeof(Coroutine_t3621161934_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	Coroutine_t3621161934::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (ScriptableObject_t2970544072), sizeof(ScriptableObject_t2970544072_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (UnhandledExceptionHandler_t1700300692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (CursorLockMode_t1155278888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[4] = 
{
	CursorLockMode_t1155278888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (Cursor_t2745727898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (GameCenterPlatform_t3570684786), -1, sizeof(GameCenterPlatform_t3570684786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1966[15] = 
{
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AuthenticateCallback_0(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_FriendsCallback_1(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementDescriptionLoaderCallback_2(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementLoaderCallback_3(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ProgressCallback_4(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreCallback_5(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreLoaderCallback_6(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_LeaderboardCallback_7(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_UsersCallback_8(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_adCache_9(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_friends_10(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_users_11(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ResetAchievements_12(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_LocalUser_13(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_GcBoards_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (GcLeaderboard_t1820874799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	GcLeaderboard_t1820874799::get_offset_of_m_InternalLeaderboard_0(),
	GcLeaderboard_t1820874799::get_offset_of_m_GenericLeaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (RenderSettings_t425127197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (MeshFilter_t3839065225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (Flare_t4197217604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (Renderer_t3076687687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (InternalDrawTextureArguments_t4047764288)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[9] = 
{
	InternalDrawTextureArguments_t4047764288::get_offset_of_screenRect_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_texture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_sourceRect_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_leftBorder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_rightBorder_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_topBorder_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_bottomBorder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_color_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_mat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (Graphics_t3672240399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (Screen_t3187157168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (GL_t2267613321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (MeshRenderer_t2804666580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (RectOffset_t3056157787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[2] = 
{
	RectOffset_t3056157787::get_offset_of_m_Ptr_0(),
	RectOffset_t3056157787::get_offset_of_m_SourceStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (GUIElement_t3775428101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (GUITexture_t4020448292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (GUILayer_t2983897946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (Texture_t2526458961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (Texture2D_t3884108195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (RenderTexture_t1963041563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (CullingGroupEvent_t2820176033)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t2820176033_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[3] = 
{
	CullingGroupEvent_t2820176033::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (CullingGroup_t1868862003), sizeof(CullingGroup_t1868862003_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[2] = 
{
	CullingGroup_t1868862003::get_offset_of_m_Ptr_0(),
	CullingGroup_t1868862003::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (StateChanged_t2578300556), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (Gradient_t3661184436), sizeof(Gradient_t3661184436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[1] = 
{
	Gradient_t3661184436::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572)+ sizeof (Il2CppObject), sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[5] = 
{
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_keyboardType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_autocorrection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_multiline_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_secure_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_alert_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (FullScreenMovieControlMode_t3302654991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[5] = 
{
	FullScreenMovieControlMode_t3302654991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (FullScreenMovieScalingMode_t4213044537)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1990[5] = 
{
	FullScreenMovieScalingMode_t4213044537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (Handheld_t3573483176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (TouchScreenKeyboardType_t2604324130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[10] = 
{
	TouchScreenKeyboardType_t2604324130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (TouchScreenKeyboard_t1858258760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[1] = 
{
	TouchScreenKeyboard_t1858258760::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (Gizmos_t2849394813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (LayerMask_t3236759763)+ sizeof (Il2CppObject), sizeof(LayerMask_t3236759763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1995[1] = 
{
	LayerMask_t3236759763::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (Vector2_t4282066565)+ sizeof (Il2CppObject), sizeof(Vector2_t4282066565_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1996[3] = 
{
	0,
	Vector2_t4282066565::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t4282066565::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (Vector3_t4282066566)+ sizeof (Il2CppObject), sizeof(Vector3_t4282066566_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[4] = 
{
	0,
	Vector3_t4282066566::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (Color32_t598853688)+ sizeof (Il2CppObject), sizeof(Color32_t598853688_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1998[4] = 
{
	Color32_t598853688::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (Quaternion_t1553702882)+ sizeof (Il2CppObject), sizeof(Quaternion_t1553702882_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1999[5] = 
{
	0,
	Quaternion_t1553702882::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
