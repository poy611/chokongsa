﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorFloat
struct GetAnimatorFloat_t948332455;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::.ctor()
extern "C"  void GetAnimatorFloat__ctor_m4271842975 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::Reset()
extern "C"  void GetAnimatorFloat_Reset_m1918275916 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnEnter()
extern "C"  void GetAnimatorFloat_OnEnter_m2939059894 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnActionUpdate()
extern "C"  void GetAnimatorFloat_OnActionUpdate_m422670435 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::GetParameter()
extern "C"  void GetAnimatorFloat_GetParameter_m2334025464 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
