﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider
struct  GUILayoutVerticalSlider_t1513124266  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::topValue
	FsmFloat_t2134102846 * ___topValue_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::bottomValue
	FsmFloat_t2134102846 * ___bottomValue_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_16;

public:
	inline static int32_t get_offset_of_floatVariable_13() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___floatVariable_13)); }
	inline FsmFloat_t2134102846 * get_floatVariable_13() const { return ___floatVariable_13; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_13() { return &___floatVariable_13; }
	inline void set_floatVariable_13(FsmFloat_t2134102846 * value)
	{
		___floatVariable_13 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_13, value);
	}

	inline static int32_t get_offset_of_topValue_14() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___topValue_14)); }
	inline FsmFloat_t2134102846 * get_topValue_14() const { return ___topValue_14; }
	inline FsmFloat_t2134102846 ** get_address_of_topValue_14() { return &___topValue_14; }
	inline void set_topValue_14(FsmFloat_t2134102846 * value)
	{
		___topValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___topValue_14, value);
	}

	inline static int32_t get_offset_of_bottomValue_15() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___bottomValue_15)); }
	inline FsmFloat_t2134102846 * get_bottomValue_15() const { return ___bottomValue_15; }
	inline FsmFloat_t2134102846 ** get_address_of_bottomValue_15() { return &___bottomValue_15; }
	inline void set_bottomValue_15(FsmFloat_t2134102846 * value)
	{
		___bottomValue_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomValue_15, value);
	}

	inline static int32_t get_offset_of_changedEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1513124266, ___changedEvent_16)); }
	inline FsmEvent_t2133468028 * get_changedEvent_16() const { return ___changedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_16() { return &___changedEvent_16; }
	inline void set_changedEvent_16(FsmEvent_t2133468028 * value)
	{
		___changedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
