﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkConnect
struct  NetworkConnect_t2329652676  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkConnect::remoteIP
	FsmString_t952858651 * ___remoteIP_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkConnect::remotePort
	FsmInt_t1596138449 * ___remotePort_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkConnect::password
	FsmString_t952858651 * ___password_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkConnect::errorEvent
	FsmEvent_t2133468028 * ___errorEvent_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkConnect::errorString
	FsmString_t952858651 * ___errorString_15;

public:
	inline static int32_t get_offset_of_remoteIP_11() { return static_cast<int32_t>(offsetof(NetworkConnect_t2329652676, ___remoteIP_11)); }
	inline FsmString_t952858651 * get_remoteIP_11() const { return ___remoteIP_11; }
	inline FsmString_t952858651 ** get_address_of_remoteIP_11() { return &___remoteIP_11; }
	inline void set_remoteIP_11(FsmString_t952858651 * value)
	{
		___remoteIP_11 = value;
		Il2CppCodeGenWriteBarrier(&___remoteIP_11, value);
	}

	inline static int32_t get_offset_of_remotePort_12() { return static_cast<int32_t>(offsetof(NetworkConnect_t2329652676, ___remotePort_12)); }
	inline FsmInt_t1596138449 * get_remotePort_12() const { return ___remotePort_12; }
	inline FsmInt_t1596138449 ** get_address_of_remotePort_12() { return &___remotePort_12; }
	inline void set_remotePort_12(FsmInt_t1596138449 * value)
	{
		___remotePort_12 = value;
		Il2CppCodeGenWriteBarrier(&___remotePort_12, value);
	}

	inline static int32_t get_offset_of_password_13() { return static_cast<int32_t>(offsetof(NetworkConnect_t2329652676, ___password_13)); }
	inline FsmString_t952858651 * get_password_13() const { return ___password_13; }
	inline FsmString_t952858651 ** get_address_of_password_13() { return &___password_13; }
	inline void set_password_13(FsmString_t952858651 * value)
	{
		___password_13 = value;
		Il2CppCodeGenWriteBarrier(&___password_13, value);
	}

	inline static int32_t get_offset_of_errorEvent_14() { return static_cast<int32_t>(offsetof(NetworkConnect_t2329652676, ___errorEvent_14)); }
	inline FsmEvent_t2133468028 * get_errorEvent_14() const { return ___errorEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_errorEvent_14() { return &___errorEvent_14; }
	inline void set_errorEvent_14(FsmEvent_t2133468028 * value)
	{
		___errorEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___errorEvent_14, value);
	}

	inline static int32_t get_offset_of_errorString_15() { return static_cast<int32_t>(offsetof(NetworkConnect_t2329652676, ___errorString_15)); }
	inline FsmString_t952858651 * get_errorString_15() const { return ___errorString_15; }
	inline FsmString_t952858651 ** get_address_of_errorString_15() { return &___errorString_15; }
	inline void set_errorString_15(FsmString_t952858651 * value)
	{
		___errorString_15 = value;
		Il2CppCodeGenWriteBarrier(&___errorString_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
