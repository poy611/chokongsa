﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnumSwitch
struct EnumSwitch_t1347272957;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::.ctor()
extern "C"  void EnumSwitch__ctor_m3118129161 (EnumSwitch_t1347272957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::Reset()
extern "C"  void EnumSwitch_Reset_m764562102 (EnumSwitch_t1347272957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::OnEnter()
extern "C"  void EnumSwitch_OnEnter_m2321647008 (EnumSwitch_t1347272957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::OnUpdate()
extern "C"  void EnumSwitch_OnUpdate_m2385139875 (EnumSwitch_t1347272957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::DoEnumSwitch()
extern "C"  void EnumSwitch_DoEnumSwitch_m3536353435 (EnumSwitch_t1347272957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
