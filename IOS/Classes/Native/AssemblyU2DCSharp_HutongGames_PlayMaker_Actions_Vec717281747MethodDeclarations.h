﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3ClampMagnitude
struct Vector3ClampMagnitude_t717281747;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::.ctor()
extern "C"  void Vector3ClampMagnitude__ctor_m4150095043 (Vector3ClampMagnitude_t717281747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::Reset()
extern "C"  void Vector3ClampMagnitude_Reset_m1796527984 (Vector3ClampMagnitude_t717281747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::OnEnter()
extern "C"  void Vector3ClampMagnitude_OnEnter_m1903414234 (Vector3ClampMagnitude_t717281747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::OnUpdate()
extern "C"  void Vector3ClampMagnitude_OnUpdate_m2304825769 (Vector3ClampMagnitude_t717281747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::DoVector3ClampMagnitude()
extern "C"  void Vector3ClampMagnitude_DoVector3ClampMagnitude_m4026400411 (Vector3ClampMagnitude_t717281747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
