﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.Sentinels::Sentinels_AutomatchingParticipant()
extern "C"  IntPtr_t Sentinels_Sentinels_AutomatchingParticipant_m2704397095 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
