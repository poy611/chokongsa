﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3F_1__ctor_m3926717341_gshared (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey3F_1__ctor_m3926717341(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey3F_1__ctor_m3926717341_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>::<>m__13()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3F_1_U3CU3Em__13_m607154376_gshared (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey3F_1_U3CU3Em__13_m607154376(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey3F_1_U3CU3Em__13_m607154376_gshared)(__this, method)
