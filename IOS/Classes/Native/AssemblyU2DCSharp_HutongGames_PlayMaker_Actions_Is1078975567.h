﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com719239868.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsSleeping2d
struct  IsSleeping2d_t1078975567  : public ComponentAction_1_t719239868
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsSleeping2d::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping2d::trueEvent
	FsmEvent_t2133468028 * ___trueEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping2d::falseEvent
	FsmEvent_t2133468028 * ___falseEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsSleeping2d::store
	FsmBool_t1075959796 * ___store_16;
	// System.Boolean HutongGames.PlayMaker.Actions.IsSleeping2d::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(IsSleeping2d_t1078975567, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_trueEvent_14() { return static_cast<int32_t>(offsetof(IsSleeping2d_t1078975567, ___trueEvent_14)); }
	inline FsmEvent_t2133468028 * get_trueEvent_14() const { return ___trueEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_trueEvent_14() { return &___trueEvent_14; }
	inline void set_trueEvent_14(FsmEvent_t2133468028 * value)
	{
		___trueEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___trueEvent_14, value);
	}

	inline static int32_t get_offset_of_falseEvent_15() { return static_cast<int32_t>(offsetof(IsSleeping2d_t1078975567, ___falseEvent_15)); }
	inline FsmEvent_t2133468028 * get_falseEvent_15() const { return ___falseEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_falseEvent_15() { return &___falseEvent_15; }
	inline void set_falseEvent_15(FsmEvent_t2133468028 * value)
	{
		___falseEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___falseEvent_15, value);
	}

	inline static int32_t get_offset_of_store_16() { return static_cast<int32_t>(offsetof(IsSleeping2d_t1078975567, ___store_16)); }
	inline FsmBool_t1075959796 * get_store_16() const { return ___store_16; }
	inline FsmBool_t1075959796 ** get_address_of_store_16() { return &___store_16; }
	inline void set_store_16(FsmBool_t1075959796 * value)
	{
		___store_16 = value;
		Il2CppCodeGenWriteBarrier(&___store_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(IsSleeping2d_t1078975567, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
