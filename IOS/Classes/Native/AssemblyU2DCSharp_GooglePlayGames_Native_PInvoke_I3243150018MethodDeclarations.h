﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
struct IosPlatformConfiguration_t3243150018;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::.ctor(System.IntPtr)
extern "C"  void IosPlatformConfiguration__ctor_m3174265378 (IosPlatformConfiguration_t3243150018 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::SetClientId(System.String)
extern "C"  void IosPlatformConfiguration_SetClientId_m3823282666 (IosPlatformConfiguration_t3243150018 * __this, String_t* ___clientId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void IosPlatformConfiguration_CallDispose_m2107156430 (IosPlatformConfiguration_t3243150018 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration GooglePlayGames.Native.PInvoke.IosPlatformConfiguration::Create()
extern "C"  IosPlatformConfiguration_t3243150018 * IosPlatformConfiguration_Create_m4079578740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
