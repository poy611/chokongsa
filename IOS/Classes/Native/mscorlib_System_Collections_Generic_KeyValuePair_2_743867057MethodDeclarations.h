﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1881125267(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t743867057 *, String_t*, uint32_t, const MethodInfo*))KeyValuePair_2__ctor_m1597091749_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::get_Key()
#define KeyValuePair_2_get_Key_m592785525(__this, method) ((  String_t* (*) (KeyValuePair_2_t743867057 *, const MethodInfo*))KeyValuePair_2_get_Key_m1874048547_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4086675638(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t743867057 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1019066212_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::get_Value()
#define KeyValuePair_2_get_Value_m3893063641(__this, method) ((  uint32_t (*) (KeyValuePair_2_t743867057 *, const MethodInfo*))KeyValuePair_2_get_Value_m2531213831_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1079624118(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t743867057 *, uint32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2733029732_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>::ToString()
#define KeyValuePair_2_ToString_m343834130(__this, method) ((  String_t* (*) (KeyValuePair_2_t743867057 *, const MethodInfo*))KeyValuePair_2_ToString_m1408282148_gshared)(__this, method)
