﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuList
struct  MenuList_t3755595453  : public MonoBehaviour_t667441552
{
public:
	// System.String MenuList::esspresso
	String_t* ___esspresso_2;
	// System.String MenuList::americano
	String_t* ___americano_3;
	// System.String MenuList::ice_americano
	String_t* ___ice_americano_4;
	// System.String MenuList::cafelatte
	String_t* ___cafelatte_5;
	// System.String MenuList::ice_cafelatte
	String_t* ___ice_cafelatte_6;
	// System.String MenuList::vanillalatte
	String_t* ___vanillalatte_7;
	// System.String MenuList::ice_vanillalatte
	String_t* ___ice_vanillalatte_8;
	// System.String MenuList::hazelnut
	String_t* ___hazelnut_9;
	// System.String MenuList::ice_hazelnut
	String_t* ___ice_hazelnut_10;
	// System.String MenuList::cafemocca
	String_t* ___cafemocca_11;
	// System.String MenuList::ice_cafemoca
	String_t* ___ice_cafemoca_12;
	// System.String MenuList::caramel
	String_t* ___caramel_13;
	// System.String MenuList::ice_caramel
	String_t* ___ice_caramel_14;
	// System.String MenuList::caramelfrappuccino
	String_t* ___caramelfrappuccino_15;
	// System.String MenuList::mochafrappuccino
	String_t* ___mochafrappuccino_16;
	// System.String MenuList::hazelnutfrappuccino
	String_t* ___hazelnutfrappuccino_17;

public:
	inline static int32_t get_offset_of_esspresso_2() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___esspresso_2)); }
	inline String_t* get_esspresso_2() const { return ___esspresso_2; }
	inline String_t** get_address_of_esspresso_2() { return &___esspresso_2; }
	inline void set_esspresso_2(String_t* value)
	{
		___esspresso_2 = value;
		Il2CppCodeGenWriteBarrier(&___esspresso_2, value);
	}

	inline static int32_t get_offset_of_americano_3() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___americano_3)); }
	inline String_t* get_americano_3() const { return ___americano_3; }
	inline String_t** get_address_of_americano_3() { return &___americano_3; }
	inline void set_americano_3(String_t* value)
	{
		___americano_3 = value;
		Il2CppCodeGenWriteBarrier(&___americano_3, value);
	}

	inline static int32_t get_offset_of_ice_americano_4() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_americano_4)); }
	inline String_t* get_ice_americano_4() const { return ___ice_americano_4; }
	inline String_t** get_address_of_ice_americano_4() { return &___ice_americano_4; }
	inline void set_ice_americano_4(String_t* value)
	{
		___ice_americano_4 = value;
		Il2CppCodeGenWriteBarrier(&___ice_americano_4, value);
	}

	inline static int32_t get_offset_of_cafelatte_5() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___cafelatte_5)); }
	inline String_t* get_cafelatte_5() const { return ___cafelatte_5; }
	inline String_t** get_address_of_cafelatte_5() { return &___cafelatte_5; }
	inline void set_cafelatte_5(String_t* value)
	{
		___cafelatte_5 = value;
		Il2CppCodeGenWriteBarrier(&___cafelatte_5, value);
	}

	inline static int32_t get_offset_of_ice_cafelatte_6() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_cafelatte_6)); }
	inline String_t* get_ice_cafelatte_6() const { return ___ice_cafelatte_6; }
	inline String_t** get_address_of_ice_cafelatte_6() { return &___ice_cafelatte_6; }
	inline void set_ice_cafelatte_6(String_t* value)
	{
		___ice_cafelatte_6 = value;
		Il2CppCodeGenWriteBarrier(&___ice_cafelatte_6, value);
	}

	inline static int32_t get_offset_of_vanillalatte_7() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___vanillalatte_7)); }
	inline String_t* get_vanillalatte_7() const { return ___vanillalatte_7; }
	inline String_t** get_address_of_vanillalatte_7() { return &___vanillalatte_7; }
	inline void set_vanillalatte_7(String_t* value)
	{
		___vanillalatte_7 = value;
		Il2CppCodeGenWriteBarrier(&___vanillalatte_7, value);
	}

	inline static int32_t get_offset_of_ice_vanillalatte_8() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_vanillalatte_8)); }
	inline String_t* get_ice_vanillalatte_8() const { return ___ice_vanillalatte_8; }
	inline String_t** get_address_of_ice_vanillalatte_8() { return &___ice_vanillalatte_8; }
	inline void set_ice_vanillalatte_8(String_t* value)
	{
		___ice_vanillalatte_8 = value;
		Il2CppCodeGenWriteBarrier(&___ice_vanillalatte_8, value);
	}

	inline static int32_t get_offset_of_hazelnut_9() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___hazelnut_9)); }
	inline String_t* get_hazelnut_9() const { return ___hazelnut_9; }
	inline String_t** get_address_of_hazelnut_9() { return &___hazelnut_9; }
	inline void set_hazelnut_9(String_t* value)
	{
		___hazelnut_9 = value;
		Il2CppCodeGenWriteBarrier(&___hazelnut_9, value);
	}

	inline static int32_t get_offset_of_ice_hazelnut_10() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_hazelnut_10)); }
	inline String_t* get_ice_hazelnut_10() const { return ___ice_hazelnut_10; }
	inline String_t** get_address_of_ice_hazelnut_10() { return &___ice_hazelnut_10; }
	inline void set_ice_hazelnut_10(String_t* value)
	{
		___ice_hazelnut_10 = value;
		Il2CppCodeGenWriteBarrier(&___ice_hazelnut_10, value);
	}

	inline static int32_t get_offset_of_cafemocca_11() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___cafemocca_11)); }
	inline String_t* get_cafemocca_11() const { return ___cafemocca_11; }
	inline String_t** get_address_of_cafemocca_11() { return &___cafemocca_11; }
	inline void set_cafemocca_11(String_t* value)
	{
		___cafemocca_11 = value;
		Il2CppCodeGenWriteBarrier(&___cafemocca_11, value);
	}

	inline static int32_t get_offset_of_ice_cafemoca_12() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_cafemoca_12)); }
	inline String_t* get_ice_cafemoca_12() const { return ___ice_cafemoca_12; }
	inline String_t** get_address_of_ice_cafemoca_12() { return &___ice_cafemoca_12; }
	inline void set_ice_cafemoca_12(String_t* value)
	{
		___ice_cafemoca_12 = value;
		Il2CppCodeGenWriteBarrier(&___ice_cafemoca_12, value);
	}

	inline static int32_t get_offset_of_caramel_13() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___caramel_13)); }
	inline String_t* get_caramel_13() const { return ___caramel_13; }
	inline String_t** get_address_of_caramel_13() { return &___caramel_13; }
	inline void set_caramel_13(String_t* value)
	{
		___caramel_13 = value;
		Il2CppCodeGenWriteBarrier(&___caramel_13, value);
	}

	inline static int32_t get_offset_of_ice_caramel_14() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___ice_caramel_14)); }
	inline String_t* get_ice_caramel_14() const { return ___ice_caramel_14; }
	inline String_t** get_address_of_ice_caramel_14() { return &___ice_caramel_14; }
	inline void set_ice_caramel_14(String_t* value)
	{
		___ice_caramel_14 = value;
		Il2CppCodeGenWriteBarrier(&___ice_caramel_14, value);
	}

	inline static int32_t get_offset_of_caramelfrappuccino_15() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___caramelfrappuccino_15)); }
	inline String_t* get_caramelfrappuccino_15() const { return ___caramelfrappuccino_15; }
	inline String_t** get_address_of_caramelfrappuccino_15() { return &___caramelfrappuccino_15; }
	inline void set_caramelfrappuccino_15(String_t* value)
	{
		___caramelfrappuccino_15 = value;
		Il2CppCodeGenWriteBarrier(&___caramelfrappuccino_15, value);
	}

	inline static int32_t get_offset_of_mochafrappuccino_16() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___mochafrappuccino_16)); }
	inline String_t* get_mochafrappuccino_16() const { return ___mochafrappuccino_16; }
	inline String_t** get_address_of_mochafrappuccino_16() { return &___mochafrappuccino_16; }
	inline void set_mochafrappuccino_16(String_t* value)
	{
		___mochafrappuccino_16 = value;
		Il2CppCodeGenWriteBarrier(&___mochafrappuccino_16, value);
	}

	inline static int32_t get_offset_of_hazelnutfrappuccino_17() { return static_cast<int32_t>(offsetof(MenuList_t3755595453, ___hazelnutfrappuccino_17)); }
	inline String_t* get_hazelnutfrappuccino_17() const { return ___hazelnutfrappuccino_17; }
	inline String_t** get_address_of_hazelnutfrappuccino_17() { return &___hazelnutfrappuccino_17; }
	inline void set_hazelnutfrappuccino_17(String_t* value)
	{
		___hazelnutfrappuccino_17 = value;
		Il2CppCodeGenWriteBarrier(&___hazelnutfrappuccino_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
