﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey66
struct U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey66::.ctor()
extern "C"  void U3CCreateQuickGameU3Ec__AnonStorey66__ctor_m4182304530 (U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
