﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey7C
struct U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey7C_t3360561201;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t1088765775;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t3582269991;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey7C::.ctor()
extern "C"  void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey7C__ctor_m2952604762 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey7C_t3360561201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey7C::<>m__67(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C"  void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey7C_U3CU3Em__67_m1391775430 (U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey7C_t3360561201 * __this, Il2CppObject * ___resolver0, Il2CppObject * ___original1, ByteU5BU5D_t4260760469* ___originalData2, Il2CppObject * ___unmerged3, ByteU5BU5D_t4260760469* ___unmergedData4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
