﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73
struct U3CAcceptInvitationU3Ec__AnonStorey73_t412159721;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73::.ctor()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey73__ctor_m668725202 (U3CAcceptInvitationU3Ec__AnonStorey73_t412159721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73::<>m__4E()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey73_U3CU3Em__4E_m707974252 (U3CAcceptInvitationU3Ec__AnonStorey73_t412159721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
