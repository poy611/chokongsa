﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo>
struct IEnumerable_1_t1105416852;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "YoutubeUnity_YoutubeExtractor_AdaptiveType3319469144.h"
#include "YoutubeUnity_YoutubeExtractor_AudioType955051966.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeExtractor.VideoInfo
struct  VideoInfo_t2099471191  : public Il2CppObject
{
public:
	// YoutubeExtractor.AdaptiveType YoutubeExtractor.VideoInfo::<AdaptiveType>k__BackingField
	int32_t ___U3CAdaptiveTypeU3Ek__BackingField_1;
	// System.Int32 YoutubeExtractor.VideoInfo::<AudioBitrate>k__BackingField
	int32_t ___U3CAudioBitrateU3Ek__BackingField_2;
	// System.Int32 YoutubeExtractor.VideoInfo::<FrameRate>k__BackingField
	int32_t ___U3CFrameRateU3Ek__BackingField_3;
	// YoutubeExtractor.AudioType YoutubeExtractor.VideoInfo::<AudioType>k__BackingField
	int32_t ___U3CAudioTypeU3Ek__BackingField_4;
	// System.String YoutubeExtractor.VideoInfo::<DownloadUrl>k__BackingField
	String_t* ___U3CDownloadUrlU3Ek__BackingField_5;
	// System.Int32 YoutubeExtractor.VideoInfo::<FormatCode>k__BackingField
	int32_t ___U3CFormatCodeU3Ek__BackingField_6;
	// System.Boolean YoutubeExtractor.VideoInfo::<Is3D>k__BackingField
	bool ___U3CIs3DU3Ek__BackingField_7;
	// System.Boolean YoutubeExtractor.VideoInfo::<RequiresDecryption>k__BackingField
	bool ___U3CRequiresDecryptionU3Ek__BackingField_8;
	// System.Int64 YoutubeExtractor.VideoInfo::<FileSize>k__BackingField
	int64_t ___U3CFileSizeU3Ek__BackingField_9;
	// System.Int32 YoutubeExtractor.VideoInfo::<Resolution>k__BackingField
	int32_t ___U3CResolutionU3Ek__BackingField_10;
	// System.String YoutubeExtractor.VideoInfo::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_11;
	// YoutubeExtractor.VideoType YoutubeExtractor.VideoInfo::<VideoType>k__BackingField
	int32_t ___U3CVideoTypeU3Ek__BackingField_12;
	// System.String YoutubeExtractor.VideoInfo::<HtmlPlayerVersion>k__BackingField
	String_t* ___U3CHtmlPlayerVersionU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CAdaptiveTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CAdaptiveTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CAdaptiveTypeU3Ek__BackingField_1() const { return ___U3CAdaptiveTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CAdaptiveTypeU3Ek__BackingField_1() { return &___U3CAdaptiveTypeU3Ek__BackingField_1; }
	inline void set_U3CAdaptiveTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CAdaptiveTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CAudioBitrateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CAudioBitrateU3Ek__BackingField_2)); }
	inline int32_t get_U3CAudioBitrateU3Ek__BackingField_2() const { return ___U3CAudioBitrateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CAudioBitrateU3Ek__BackingField_2() { return &___U3CAudioBitrateU3Ek__BackingField_2; }
	inline void set_U3CAudioBitrateU3Ek__BackingField_2(int32_t value)
	{
		___U3CAudioBitrateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFrameRateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CFrameRateU3Ek__BackingField_3)); }
	inline int32_t get_U3CFrameRateU3Ek__BackingField_3() const { return ___U3CFrameRateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CFrameRateU3Ek__BackingField_3() { return &___U3CFrameRateU3Ek__BackingField_3; }
	inline void set_U3CFrameRateU3Ek__BackingField_3(int32_t value)
	{
		___U3CFrameRateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CAudioTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CAudioTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CAudioTypeU3Ek__BackingField_4() const { return ___U3CAudioTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CAudioTypeU3Ek__BackingField_4() { return &___U3CAudioTypeU3Ek__BackingField_4; }
	inline void set_U3CAudioTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CAudioTypeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CDownloadUrlU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CDownloadUrlU3Ek__BackingField_5)); }
	inline String_t* get_U3CDownloadUrlU3Ek__BackingField_5() const { return ___U3CDownloadUrlU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CDownloadUrlU3Ek__BackingField_5() { return &___U3CDownloadUrlU3Ek__BackingField_5; }
	inline void set_U3CDownloadUrlU3Ek__BackingField_5(String_t* value)
	{
		___U3CDownloadUrlU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDownloadUrlU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CFormatCodeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CFormatCodeU3Ek__BackingField_6)); }
	inline int32_t get_U3CFormatCodeU3Ek__BackingField_6() const { return ___U3CFormatCodeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CFormatCodeU3Ek__BackingField_6() { return &___U3CFormatCodeU3Ek__BackingField_6; }
	inline void set_U3CFormatCodeU3Ek__BackingField_6(int32_t value)
	{
		___U3CFormatCodeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIs3DU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CIs3DU3Ek__BackingField_7)); }
	inline bool get_U3CIs3DU3Ek__BackingField_7() const { return ___U3CIs3DU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIs3DU3Ek__BackingField_7() { return &___U3CIs3DU3Ek__BackingField_7; }
	inline void set_U3CIs3DU3Ek__BackingField_7(bool value)
	{
		___U3CIs3DU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRequiresDecryptionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CRequiresDecryptionU3Ek__BackingField_8)); }
	inline bool get_U3CRequiresDecryptionU3Ek__BackingField_8() const { return ___U3CRequiresDecryptionU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CRequiresDecryptionU3Ek__BackingField_8() { return &___U3CRequiresDecryptionU3Ek__BackingField_8; }
	inline void set_U3CRequiresDecryptionU3Ek__BackingField_8(bool value)
	{
		___U3CRequiresDecryptionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFileSizeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CFileSizeU3Ek__BackingField_9)); }
	inline int64_t get_U3CFileSizeU3Ek__BackingField_9() const { return ___U3CFileSizeU3Ek__BackingField_9; }
	inline int64_t* get_address_of_U3CFileSizeU3Ek__BackingField_9() { return &___U3CFileSizeU3Ek__BackingField_9; }
	inline void set_U3CFileSizeU3Ek__BackingField_9(int64_t value)
	{
		___U3CFileSizeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CResolutionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CResolutionU3Ek__BackingField_10)); }
	inline int32_t get_U3CResolutionU3Ek__BackingField_10() const { return ___U3CResolutionU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CResolutionU3Ek__BackingField_10() { return &___U3CResolutionU3Ek__BackingField_10; }
	inline void set_U3CResolutionU3Ek__BackingField_10(int32_t value)
	{
		___U3CResolutionU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CTitleU3Ek__BackingField_11)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_11() const { return ___U3CTitleU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_11() { return &___U3CTitleU3Ek__BackingField_11; }
	inline void set_U3CTitleU3Ek__BackingField_11(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CVideoTypeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CVideoTypeU3Ek__BackingField_12)); }
	inline int32_t get_U3CVideoTypeU3Ek__BackingField_12() const { return ___U3CVideoTypeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CVideoTypeU3Ek__BackingField_12() { return &___U3CVideoTypeU3Ek__BackingField_12; }
	inline void set_U3CVideoTypeU3Ek__BackingField_12(int32_t value)
	{
		___U3CVideoTypeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CHtmlPlayerVersionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191, ___U3CHtmlPlayerVersionU3Ek__BackingField_13)); }
	inline String_t* get_U3CHtmlPlayerVersionU3Ek__BackingField_13() const { return ___U3CHtmlPlayerVersionU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CHtmlPlayerVersionU3Ek__BackingField_13() { return &___U3CHtmlPlayerVersionU3Ek__BackingField_13; }
	inline void set_U3CHtmlPlayerVersionU3Ek__BackingField_13(String_t* value)
	{
		___U3CHtmlPlayerVersionU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHtmlPlayerVersionU3Ek__BackingField_13, value);
	}
};

struct VideoInfo_t2099471191_StaticFields
{
public:
	// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo> YoutubeExtractor.VideoInfo::Defaults
	Il2CppObject* ___Defaults_0;

public:
	inline static int32_t get_offset_of_Defaults_0() { return static_cast<int32_t>(offsetof(VideoInfo_t2099471191_StaticFields, ___Defaults_0)); }
	inline Il2CppObject* get_Defaults_0() const { return ___Defaults_0; }
	inline Il2CppObject** get_address_of_Defaults_0() { return &___Defaults_0; }
	inline void set_Defaults_0(Il2CppObject* value)
	{
		___Defaults_0 = value;
		Il2CppCodeGenWriteBarrier(&___Defaults_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
