﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion
struct GetQuaternionMultipliedByQuaternion_t1710736874;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::.ctor()
extern "C"  void GetQuaternionMultipliedByQuaternion__ctor_m3808425100 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::Reset()
extern "C"  void GetQuaternionMultipliedByQuaternion_Reset_m1454858041 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnEnter()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnEnter_m4271080803 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnUpdate_m2688045376 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnLateUpdate_m4018108166 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnFixedUpdate_m883246888 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
