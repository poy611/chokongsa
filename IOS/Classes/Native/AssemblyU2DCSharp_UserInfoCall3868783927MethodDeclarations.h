﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserInfoCall
struct UserInfoCall_t3868783927;

#include "codegen/il2cpp-codegen.h"

// System.Void UserInfoCall::.ctor()
extern "C"  void UserInfoCall__ctor_m1362750276 (UserInfoCall_t3868783927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
