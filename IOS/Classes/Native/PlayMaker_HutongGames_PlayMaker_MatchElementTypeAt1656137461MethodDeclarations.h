﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.MatchElementTypeAttribute
struct MatchElementTypeAttribute_t1656137461;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.MatchElementTypeAttribute::get_FieldName()
extern "C"  String_t* MatchElementTypeAttribute_get_FieldName_m392966087 (MatchElementTypeAttribute_t1656137461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.MatchElementTypeAttribute::.ctor(System.String)
extern "C"  void MatchElementTypeAttribute__ctor_m3261878840 (MatchElementTypeAttribute_t1656137461 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
