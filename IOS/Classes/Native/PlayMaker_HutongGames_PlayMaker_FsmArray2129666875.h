﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Array
struct Il2CppArray;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmArray
struct  FsmArray_t2129666875  : public NamedVariable_t3211770239
{
public:
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::type
	int32_t ___type_5;
	// System.String HutongGames.PlayMaker.FsmArray::objectTypeName
	String_t* ___objectTypeName_6;
	// System.Type HutongGames.PlayMaker.FsmArray::objectType
	Type_t * ___objectType_7;
	// System.Single[] HutongGames.PlayMaker.FsmArray::floatValues
	SingleU5BU5D_t2316563989* ___floatValues_8;
	// System.Int32[] HutongGames.PlayMaker.FsmArray::intValues
	Int32U5BU5D_t3230847821* ___intValues_9;
	// System.Boolean[] HutongGames.PlayMaker.FsmArray::boolValues
	BooleanU5BU5D_t3456302923* ___boolValues_10;
	// System.String[] HutongGames.PlayMaker.FsmArray::stringValues
	StringU5BU5D_t4054002952* ___stringValues_11;
	// UnityEngine.Vector4[] HutongGames.PlayMaker.FsmArray::vector4Values
	Vector4U5BU5D_t701588350* ___vector4Values_12;
	// UnityEngine.Object[] HutongGames.PlayMaker.FsmArray::objectReferences
	ObjectU5BU5D_t1015136018* ___objectReferences_13;
	// System.Array HutongGames.PlayMaker.FsmArray::sourceArray
	Il2CppArray * ___sourceArray_14;
	// System.Object[] HutongGames.PlayMaker.FsmArray::values
	ObjectU5BU5D_t1108656482* ___values_15;

public:
	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_objectTypeName_6() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___objectTypeName_6)); }
	inline String_t* get_objectTypeName_6() const { return ___objectTypeName_6; }
	inline String_t** get_address_of_objectTypeName_6() { return &___objectTypeName_6; }
	inline void set_objectTypeName_6(String_t* value)
	{
		___objectTypeName_6 = value;
		Il2CppCodeGenWriteBarrier(&___objectTypeName_6, value);
	}

	inline static int32_t get_offset_of_objectType_7() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___objectType_7)); }
	inline Type_t * get_objectType_7() const { return ___objectType_7; }
	inline Type_t ** get_address_of_objectType_7() { return &___objectType_7; }
	inline void set_objectType_7(Type_t * value)
	{
		___objectType_7 = value;
		Il2CppCodeGenWriteBarrier(&___objectType_7, value);
	}

	inline static int32_t get_offset_of_floatValues_8() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___floatValues_8)); }
	inline SingleU5BU5D_t2316563989* get_floatValues_8() const { return ___floatValues_8; }
	inline SingleU5BU5D_t2316563989** get_address_of_floatValues_8() { return &___floatValues_8; }
	inline void set_floatValues_8(SingleU5BU5D_t2316563989* value)
	{
		___floatValues_8 = value;
		Il2CppCodeGenWriteBarrier(&___floatValues_8, value);
	}

	inline static int32_t get_offset_of_intValues_9() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___intValues_9)); }
	inline Int32U5BU5D_t3230847821* get_intValues_9() const { return ___intValues_9; }
	inline Int32U5BU5D_t3230847821** get_address_of_intValues_9() { return &___intValues_9; }
	inline void set_intValues_9(Int32U5BU5D_t3230847821* value)
	{
		___intValues_9 = value;
		Il2CppCodeGenWriteBarrier(&___intValues_9, value);
	}

	inline static int32_t get_offset_of_boolValues_10() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___boolValues_10)); }
	inline BooleanU5BU5D_t3456302923* get_boolValues_10() const { return ___boolValues_10; }
	inline BooleanU5BU5D_t3456302923** get_address_of_boolValues_10() { return &___boolValues_10; }
	inline void set_boolValues_10(BooleanU5BU5D_t3456302923* value)
	{
		___boolValues_10 = value;
		Il2CppCodeGenWriteBarrier(&___boolValues_10, value);
	}

	inline static int32_t get_offset_of_stringValues_11() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___stringValues_11)); }
	inline StringU5BU5D_t4054002952* get_stringValues_11() const { return ___stringValues_11; }
	inline StringU5BU5D_t4054002952** get_address_of_stringValues_11() { return &___stringValues_11; }
	inline void set_stringValues_11(StringU5BU5D_t4054002952* value)
	{
		___stringValues_11 = value;
		Il2CppCodeGenWriteBarrier(&___stringValues_11, value);
	}

	inline static int32_t get_offset_of_vector4Values_12() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___vector4Values_12)); }
	inline Vector4U5BU5D_t701588350* get_vector4Values_12() const { return ___vector4Values_12; }
	inline Vector4U5BU5D_t701588350** get_address_of_vector4Values_12() { return &___vector4Values_12; }
	inline void set_vector4Values_12(Vector4U5BU5D_t701588350* value)
	{
		___vector4Values_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector4Values_12, value);
	}

	inline static int32_t get_offset_of_objectReferences_13() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___objectReferences_13)); }
	inline ObjectU5BU5D_t1015136018* get_objectReferences_13() const { return ___objectReferences_13; }
	inline ObjectU5BU5D_t1015136018** get_address_of_objectReferences_13() { return &___objectReferences_13; }
	inline void set_objectReferences_13(ObjectU5BU5D_t1015136018* value)
	{
		___objectReferences_13 = value;
		Il2CppCodeGenWriteBarrier(&___objectReferences_13, value);
	}

	inline static int32_t get_offset_of_sourceArray_14() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___sourceArray_14)); }
	inline Il2CppArray * get_sourceArray_14() const { return ___sourceArray_14; }
	inline Il2CppArray ** get_address_of_sourceArray_14() { return &___sourceArray_14; }
	inline void set_sourceArray_14(Il2CppArray * value)
	{
		___sourceArray_14 = value;
		Il2CppCodeGenWriteBarrier(&___sourceArray_14, value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(FsmArray_t2129666875, ___values_15)); }
	inline ObjectU5BU5D_t1108656482* get_values_15() const { return ___values_15; }
	inline ObjectU5BU5D_t1108656482** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(ObjectU5BU5D_t1108656482* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier(&___values_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
