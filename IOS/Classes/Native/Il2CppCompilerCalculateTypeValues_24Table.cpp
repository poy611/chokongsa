﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl1823032379.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride3235106805.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption_Layou2090144509.h"
#include "PlayMaker_HutongGames_PlayMaker_DebugUtils257587552.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1938906778.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmDebugUtility965175459.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmExecutionStack2677439066.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve2685995989.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "PlayMaker_HutongGames_PlayMaker_None3231562618.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData3958426178.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData_Context1489737484.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm_EditorFlags1538621599.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog1596141350.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry2614866584.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType537852544.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust2484246752.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust4157095950.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility81143630.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility_BitConv1958851146.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeNicifie195153339.h"
#include "PlayMaker_HutongGames_PlayMaker_ArrayVariableTypesN105039977.h"
#include "PlayMaker_HutongGames_PlayMaker_Actions_MissingActi834633994.h"
#include "PlayMaker_PlayMakerPrefs941311808.h"
#include "PlayMaker_HutongGames_PlayMaker_ReflectionUtils1202855664.h"
#include "PlayMaker_PlayMakerProxyBase3469687535.h"
#include "PlayMaker_PlayMakerAnimatorIK2388642745.h"
#include "PlayMaker_PlayMakerAnimatorMove1973299400.h"
#include "PlayMaker_PlayMakerApplicationEvents1615127321.h"
#include "PlayMaker_PlayMakerCollisionEnter4046453814.h"
#include "PlayMaker_PlayMakerCollisionEnter2D1696713992.h"
#include "PlayMaker_PlayMakerCollisionExit3871318016.h"
#include "PlayMaker_PlayMakerCollisionExit2D894936658.h"
#include "PlayMaker_PlayMakerCollisionStay3871731003.h"
#include "PlayMaker_PlayMakerCollisionStay2D1291817165.h"
#include "PlayMaker_PlayMakerControllerColliderHit1717485139.h"
#include "PlayMaker_PlayMakerFixedUpdate997170669.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_PlayMakerFSM_U3CDoCoroutineU3Ed__13331551771.h"
#include "PlayMaker_PlayMakerGlobals3097244096.h"
#include "PlayMaker_PlayMakerGUI3799848395.h"
#include "PlayMaker_PlayMakerMouseEvents316289710.h"
#include "PlayMaker_PlayMakerOnGUI940239724.h"
#include "PlayMaker_PlayMakerTriggerEnter977448304.h"
#include "PlayMaker_PlayMakerTriggerEnter2D3024951234.h"
#include "PlayMaker_PlayMakerTriggerExit3495223174.h"
#include "PlayMaker_PlayMakerTriggerExit2D245046360.h"
#include "PlayMaker_PlayMakerTriggerStay3495636161.h"
#include "PlayMaker_PlayMakerTriggerStay2D641926867.h"
#include "PlayMaker_PlayMakerParticleCollision2428451804.h"
#include "PlayMaker_PlayMakerJointBreak197925893.h"
#include "PlayMaker_PlayMakerJointBreak2D1228223767.h"
#include "UnityEngine_UI_U3CModuleU3E86524790.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandl1938870832.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (FsmTemplateControl_t2786508133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[4] = 
{
	FsmTemplateControl_t2786508133::get_offset_of_fsmTemplate_0(),
	FsmTemplateControl_t2786508133::get_offset_of_fsmVarOverrides_1(),
	FsmTemplateControl_t2786508133::get_offset_of_runFsm_2(),
	FsmTemplateControl_t2786508133::get_offset_of_U3CIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (U3CU3Ec__DisplayClass2_t1823032379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[1] = 
{
	U3CU3Ec__DisplayClass2_t1823032379::get_offset_of_namedVariable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (FsmEventTarget_t1823904941), -1, sizeof(FsmEventTarget_t1823904941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2402[7] = 
{
	FsmEventTarget_t1823904941_StaticFields::get_offset_of_self_0(),
	FsmEventTarget_t1823904941::get_offset_of_target_1(),
	FsmEventTarget_t1823904941::get_offset_of_excludeSelf_2(),
	FsmEventTarget_t1823904941::get_offset_of_gameObject_3(),
	FsmEventTarget_t1823904941::get_offset_of_fsmName_4(),
	FsmEventTarget_t1823904941::get_offset_of_sendToChildren_5(),
	FsmEventTarget_t1823904941::get_offset_of_fsmComponent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (EventTarget_t3998278035)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[8] = 
{
	EventTarget_t3998278035::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (FsmVarOverride_t3235106805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[3] = 
{
	FsmVarOverride_t3235106805::get_offset_of_variable_0(),
	FsmVarOverride_t3235106805::get_offset_of_fsmVar_1(),
	FsmVarOverride_t3235106805::get_offset_of_isEdited_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (FunctionCall_t3279845016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[17] = 
{
	FunctionCall_t3279845016::get_offset_of_FunctionName_0(),
	FunctionCall_t3279845016::get_offset_of_parameterType_1(),
	FunctionCall_t3279845016::get_offset_of_BoolParameter_2(),
	FunctionCall_t3279845016::get_offset_of_FloatParameter_3(),
	FunctionCall_t3279845016::get_offset_of_IntParameter_4(),
	FunctionCall_t3279845016::get_offset_of_GameObjectParameter_5(),
	FunctionCall_t3279845016::get_offset_of_ObjectParameter_6(),
	FunctionCall_t3279845016::get_offset_of_StringParameter_7(),
	FunctionCall_t3279845016::get_offset_of_Vector2Parameter_8(),
	FunctionCall_t3279845016::get_offset_of_Vector3Parameter_9(),
	FunctionCall_t3279845016::get_offset_of_RectParamater_10(),
	FunctionCall_t3279845016::get_offset_of_QuaternionParameter_11(),
	FunctionCall_t3279845016::get_offset_of_MaterialParameter_12(),
	FunctionCall_t3279845016::get_offset_of_TextureParameter_13(),
	FunctionCall_t3279845016::get_offset_of_ColorParameter_14(),
	FunctionCall_t3279845016::get_offset_of_EnumParameter_15(),
	FunctionCall_t3279845016::get_offset_of_ArrayParameter_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (LayoutOption_t964995201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[3] = 
{
	LayoutOption_t964995201::get_offset_of_option_0(),
	LayoutOption_t964995201::get_offset_of_floatParam_1(),
	LayoutOption_t964995201::get_offset_of_boolParam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (LayoutOptionType_t2090144509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2407[9] = 
{
	LayoutOptionType_t2090144509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (DebugUtils_t257587552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (DelayedEvent_t1938906778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[6] = 
{
	DelayedEvent_t1938906778::get_offset_of_fsm_0(),
	DelayedEvent_t1938906778::get_offset_of_fsmEvent_1(),
	DelayedEvent_t1938906778::get_offset_of_eventTarget_2(),
	DelayedEvent_t1938906778::get_offset_of_eventData_3(),
	DelayedEvent_t1938906778::get_offset_of_timer_4(),
	DelayedEvent_t1938906778::get_offset_of_eventFired_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (FsmDebugUtility_t965175459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (FsmEventData_t1076900934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[21] = 
{
	FsmEventData_t1076900934::get_offset_of_SentByFsm_0(),
	FsmEventData_t1076900934::get_offset_of_SentByState_1(),
	FsmEventData_t1076900934::get_offset_of_SentByAction_2(),
	FsmEventData_t1076900934::get_offset_of_BoolData_3(),
	FsmEventData_t1076900934::get_offset_of_IntData_4(),
	FsmEventData_t1076900934::get_offset_of_FloatData_5(),
	FsmEventData_t1076900934::get_offset_of_Vector2Data_6(),
	FsmEventData_t1076900934::get_offset_of_Vector3Data_7(),
	FsmEventData_t1076900934::get_offset_of_StringData_8(),
	FsmEventData_t1076900934::get_offset_of_QuaternionData_9(),
	FsmEventData_t1076900934::get_offset_of_RectData_10(),
	FsmEventData_t1076900934::get_offset_of_ColorData_11(),
	FsmEventData_t1076900934::get_offset_of_ObjectData_12(),
	FsmEventData_t1076900934::get_offset_of_GameObjectData_13(),
	FsmEventData_t1076900934::get_offset_of_MaterialData_14(),
	FsmEventData_t1076900934::get_offset_of_TextureData_15(),
	FsmEventData_t1076900934::get_offset_of_Player_16(),
	FsmEventData_t1076900934::get_offset_of_DisconnectionInfo_17(),
	FsmEventData_t1076900934::get_offset_of_ConnectionError_18(),
	FsmEventData_t1076900934::get_offset_of_NetworkMessageInfo_19(),
	FsmEventData_t1076900934::get_offset_of_MasterServerEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (FsmExecutionStack_t2677439066), -1, sizeof(FsmExecutionStack_t2677439066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2412[2] = 
{
	FsmExecutionStack_t2677439066_StaticFields::get_offset_of_fsmExecutionStack_0(),
	FsmExecutionStack_t2677439066_StaticFields::get_offset_of_U3CMaxStackCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (FsmProperty_t3927159007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[24] = 
{
	FsmProperty_t3927159007::get_offset_of_TargetObject_0(),
	FsmProperty_t3927159007::get_offset_of_TargetTypeName_1(),
	FsmProperty_t3927159007::get_offset_of_TargetType_2(),
	FsmProperty_t3927159007::get_offset_of_PropertyName_3(),
	FsmProperty_t3927159007::get_offset_of_PropertyType_4(),
	FsmProperty_t3927159007::get_offset_of_BoolParameter_5(),
	FsmProperty_t3927159007::get_offset_of_FloatParameter_6(),
	FsmProperty_t3927159007::get_offset_of_IntParameter_7(),
	FsmProperty_t3927159007::get_offset_of_GameObjectParameter_8(),
	FsmProperty_t3927159007::get_offset_of_StringParameter_9(),
	FsmProperty_t3927159007::get_offset_of_Vector2Parameter_10(),
	FsmProperty_t3927159007::get_offset_of_Vector3Parameter_11(),
	FsmProperty_t3927159007::get_offset_of_RectParamater_12(),
	FsmProperty_t3927159007::get_offset_of_QuaternionParameter_13(),
	FsmProperty_t3927159007::get_offset_of_ObjectParameter_14(),
	FsmProperty_t3927159007::get_offset_of_MaterialParameter_15(),
	FsmProperty_t3927159007::get_offset_of_TextureParameter_16(),
	FsmProperty_t3927159007::get_offset_of_ColorParameter_17(),
	FsmProperty_t3927159007::get_offset_of_EnumParameter_18(),
	FsmProperty_t3927159007::get_offset_of_ArrayParameter_19(),
	FsmProperty_t3927159007::get_offset_of_setProperty_20(),
	FsmProperty_t3927159007::get_offset_of_initialized_21(),
	FsmProperty_t3927159007::get_offset_of_targetObjectCached_22(),
	FsmProperty_t3927159007::get_offset_of_memberInfo_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (FsmTemplate_t1237263802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[2] = 
{
	FsmTemplate_t1237263802::get_offset_of_category_2(),
	FsmTemplate_t1237263802::get_offset_of_fsm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (FsmTime_t1076490199), -1, sizeof(FsmTime_t1076490199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2415[4] = 
{
	FsmTime_t1076490199_StaticFields::get_offset_of_firstUpdateHasHappened_0(),
	FsmTime_t1076490199_StaticFields::get_offset_of_totalEditorPlayerPausedTime_1(),
	FsmTime_t1076490199_StaticFields::get_offset_of_realtimeLastUpdate_2(),
	FsmTime_t1076490199_StaticFields::get_offset_of_frameCountLastUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (FsmAnimationCurve_t2685995989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	FsmAnimationCurve_t2685995989::get_offset_of_curve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (NamedVariable_t3211770239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[5] = 
{
	NamedVariable_t3211770239::get_offset_of_useVariable_0(),
	NamedVariable_t3211770239::get_offset_of_name_1(),
	NamedVariable_t3211770239::get_offset_of_tooltip_2(),
	NamedVariable_t3211770239::get_offset_of_showInInspector_3(),
	NamedVariable_t3211770239::get_offset_of_networkSync_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (FsmArray_t2129666875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[11] = 
{
	FsmArray_t2129666875::get_offset_of_type_5(),
	FsmArray_t2129666875::get_offset_of_objectTypeName_6(),
	FsmArray_t2129666875::get_offset_of_objectType_7(),
	FsmArray_t2129666875::get_offset_of_floatValues_8(),
	FsmArray_t2129666875::get_offset_of_intValues_9(),
	FsmArray_t2129666875::get_offset_of_boolValues_10(),
	FsmArray_t2129666875::get_offset_of_stringValues_11(),
	FsmArray_t2129666875::get_offset_of_vector4Values_12(),
	FsmArray_t2129666875::get_offset_of_objectReferences_13(),
	FsmArray_t2129666875::get_offset_of_sourceArray_14(),
	FsmArray_t2129666875::get_offset_of_values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (FsmBool_t1075959796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	FsmBool_t1075959796::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (FsmColor_t2131419205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[1] = 
{
	FsmColor_t2131419205::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (FsmEnum_t1076048395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[4] = 
{
	FsmEnum_t1076048395::get_offset_of_enumName_5(),
	FsmEnum_t1076048395::get_offset_of_intValue_6(),
	FsmEnum_t1076048395::get_offset_of_value_7(),
	FsmEnum_t1076048395::get_offset_of_enumType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (None_t3231562618)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2424[2] = 
{
	None_t3231562618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (FsmFloat_t2134102846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[1] = 
{
	FsmFloat_t2134102846::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (FsmGameObject_t1697147867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[1] = 
{
	FsmGameObject_t1697147867::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (FsmInt_t1596138449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[1] = 
{
	FsmInt_t1596138449::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (FsmObject_t821476169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[3] = 
{
	FsmObject_t821476169::get_offset_of_typeName_5(),
	FsmObject_t821476169::get_offset_of_value_6(),
	FsmObject_t821476169::get_offset_of_objectType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (FsmMaterial_t924399665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (FsmOwnerDefault_t251897112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[2] = 
{
	FsmOwnerDefault_t251897112::get_offset_of_ownerOption_0(),
	FsmOwnerDefault_t251897112::get_offset_of_gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (OwnerDefaultOption_t1934292325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2431[3] = 
{
	OwnerDefaultOption_t1934292325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (FsmQuaternion_t3871136040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[1] = 
{
	FsmQuaternion_t3871136040::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (FsmRect_t1076426478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[1] = 
{
	FsmRect_t1076426478::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (FsmString_t952858651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	FsmString_t952858651::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (FsmTexture_t3073272573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (FsmVar_t1596150537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[19] = 
{
	FsmVar_t1596150537::get_offset_of_variableName_0(),
	FsmVar_t1596150537::get_offset_of_objectType_1(),
	FsmVar_t1596150537::get_offset_of_useVariable_2(),
	FsmVar_t1596150537::get_offset_of_namedVar_3(),
	FsmVar_t1596150537::get_offset_of_namedVarType_4(),
	FsmVar_t1596150537::get_offset_of_enumType_5(),
	FsmVar_t1596150537::get_offset_of_enumValue_6(),
	FsmVar_t1596150537::get_offset_of__objectType_7(),
	FsmVar_t1596150537::get_offset_of_type_8(),
	FsmVar_t1596150537::get_offset_of_floatValue_9(),
	FsmVar_t1596150537::get_offset_of_intValue_10(),
	FsmVar_t1596150537::get_offset_of_boolValue_11(),
	FsmVar_t1596150537::get_offset_of_stringValue_12(),
	FsmVar_t1596150537::get_offset_of_vector4Value_13(),
	FsmVar_t1596150537::get_offset_of_objectReference_14(),
	FsmVar_t1596150537::get_offset_of_arrayValue_15(),
	FsmVar_t1596150537::get_offset_of_vector2_16(),
	FsmVar_t1596150537::get_offset_of_vector3_17(),
	FsmVar_t1596150537::get_offset_of_rect_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (FsmVector2_t533912881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[1] = 
{
	FsmVector2_t533912881::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (FsmVector3_t533912882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	FsmVector3_t533912882::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (ActionData_t3958426178), -1, sizeof(ActionData_t3958426178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[48] = 
{
	0,
	0,
	ActionData_t3958426178_StaticFields::get_offset_of_ActionTypeLookup_2(),
	ActionData_t3958426178_StaticFields::get_offset_of_ActionFieldsLookup_3(),
	ActionData_t3958426178_StaticFields::get_offset_of_ActionHashCodeLookup_4(),
	ActionData_t3958426178_StaticFields::get_offset_of_resaveActionData_5(),
	ActionData_t3958426178_StaticFields::get_offset_of_UsedIndices_6(),
	ActionData_t3958426178_StaticFields::get_offset_of_InitFields_7(),
	ActionData_t3958426178::get_offset_of_actionNames_8(),
	ActionData_t3958426178::get_offset_of_customNames_9(),
	ActionData_t3958426178::get_offset_of_actionEnabled_10(),
	ActionData_t3958426178::get_offset_of_actionIsOpen_11(),
	ActionData_t3958426178::get_offset_of_actionStartIndex_12(),
	ActionData_t3958426178::get_offset_of_actionHashCodes_13(),
	ActionData_t3958426178::get_offset_of_unityObjectParams_14(),
	ActionData_t3958426178::get_offset_of_fsmGameObjectParams_15(),
	ActionData_t3958426178::get_offset_of_fsmOwnerDefaultParams_16(),
	ActionData_t3958426178::get_offset_of_animationCurveParams_17(),
	ActionData_t3958426178::get_offset_of_functionCallParams_18(),
	ActionData_t3958426178::get_offset_of_fsmTemplateControlParams_19(),
	ActionData_t3958426178::get_offset_of_fsmEventTargetParams_20(),
	ActionData_t3958426178::get_offset_of_fsmPropertyParams_21(),
	ActionData_t3958426178::get_offset_of_layoutOptionParams_22(),
	ActionData_t3958426178::get_offset_of_fsmStringParams_23(),
	ActionData_t3958426178::get_offset_of_fsmObjectParams_24(),
	ActionData_t3958426178::get_offset_of_fsmVarParams_25(),
	ActionData_t3958426178::get_offset_of_fsmArrayParams_26(),
	ActionData_t3958426178::get_offset_of_fsmEnumParams_27(),
	ActionData_t3958426178::get_offset_of_fsmFloatParams_28(),
	ActionData_t3958426178::get_offset_of_fsmIntParams_29(),
	ActionData_t3958426178::get_offset_of_fsmBoolParams_30(),
	ActionData_t3958426178::get_offset_of_fsmVector2Params_31(),
	ActionData_t3958426178::get_offset_of_fsmVector3Params_32(),
	ActionData_t3958426178::get_offset_of_fsmColorParams_33(),
	ActionData_t3958426178::get_offset_of_fsmRectParams_34(),
	ActionData_t3958426178::get_offset_of_fsmQuaternionParams_35(),
	ActionData_t3958426178::get_offset_of_stringParams_36(),
	ActionData_t3958426178::get_offset_of_byteData_37(),
	ActionData_t3958426178::get_offset_of_byteDataAsArray_38(),
	ActionData_t3958426178::get_offset_of_arrayParamSizes_39(),
	ActionData_t3958426178::get_offset_of_arrayParamTypes_40(),
	ActionData_t3958426178::get_offset_of_customTypeSizes_41(),
	ActionData_t3958426178::get_offset_of_customTypeNames_42(),
	ActionData_t3958426178::get_offset_of_paramDataType_43(),
	ActionData_t3958426178::get_offset_of_paramName_44(),
	ActionData_t3958426178::get_offset_of_paramDataPos_45(),
	ActionData_t3958426178::get_offset_of_paramByteDataSize_46(),
	ActionData_t3958426178::get_offset_of_nextParamIndex_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (Context_t1489737484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[5] = 
{
	Context_t1489737484::get_offset_of_currentFsm_0(),
	Context_t1489737484::get_offset_of_currentState_1(),
	Context_t1489737484::get_offset_of_currentAction_2(),
	Context_t1489737484::get_offset_of_currentActionIndex_3(),
	Context_t1489737484::get_offset_of_currentParameter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (ParamDataType_t2672665179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2441[44] = 
{
	ParamDataType_t2672665179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (Fsm_t1527112426), -1, sizeof(Fsm_t1527112426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2442[101] = 
{
	0,
	0,
	0,
	Fsm_t1527112426::get_offset_of_updateHelperSetDirty_3(),
	Fsm_t1527112426_StaticFields::get_offset_of_EventData_4(),
	Fsm_t1527112426_StaticFields::get_offset_of_debugLookAtColor_5(),
	Fsm_t1527112426_StaticFields::get_offset_of_debugRaycastColor_6(),
	Fsm_t1527112426::get_offset_of_dataVersion_7(),
	Fsm_t1527112426::get_offset_of_owner_8(),
	Fsm_t1527112426::get_offset_of_usedInTemplate_9(),
	Fsm_t1527112426::get_offset_of_name_10(),
	Fsm_t1527112426::get_offset_of_startState_11(),
	Fsm_t1527112426::get_offset_of_states_12(),
	Fsm_t1527112426::get_offset_of_events_13(),
	Fsm_t1527112426::get_offset_of_globalTransitions_14(),
	Fsm_t1527112426::get_offset_of_variables_15(),
	Fsm_t1527112426::get_offset_of_description_16(),
	Fsm_t1527112426::get_offset_of_docUrl_17(),
	Fsm_t1527112426::get_offset_of_showStateLabel_18(),
	Fsm_t1527112426::get_offset_of_maxLoopCount_19(),
	Fsm_t1527112426::get_offset_of_watermark_20(),
	Fsm_t1527112426::get_offset_of_password_21(),
	Fsm_t1527112426::get_offset_of_locked_22(),
	Fsm_t1527112426::get_offset_of_manualUpdate_23(),
	Fsm_t1527112426::get_offset_of_keepDelayedEventsOnStateExit_24(),
	Fsm_t1527112426::get_offset_of_preprocessed_25(),
	Fsm_t1527112426::get_offset_of_host_26(),
	Fsm_t1527112426::get_offset_of_rootFsm_27(),
	Fsm_t1527112426::get_offset_of_subFsmList_28(),
	Fsm_t1527112426::get_offset_of_setDirty_29(),
	Fsm_t1527112426::get_offset_of_activeStateEntered_30(),
	Fsm_t1527112426::get_offset_of_ExposedEvents_31(),
	Fsm_t1527112426::get_offset_of_myLog_32(),
	Fsm_t1527112426::get_offset_of_RestartOnEnable_33(),
	Fsm_t1527112426::get_offset_of_EnableDebugFlow_34(),
	Fsm_t1527112426::get_offset_of_EnableBreakpoints_35(),
	Fsm_t1527112426::get_offset_of_StepFrame_36(),
	Fsm_t1527112426::get_offset_of_delayedEvents_37(),
	Fsm_t1527112426::get_offset_of_updateEvents_38(),
	Fsm_t1527112426::get_offset_of_removeEvents_39(),
	Fsm_t1527112426::get_offset_of_editorFlags_40(),
	Fsm_t1527112426::get_offset_of_initialized_41(),
	Fsm_t1527112426::get_offset_of_activeStateName_42(),
	Fsm_t1527112426::get_offset_of_activeState_43(),
	Fsm_t1527112426::get_offset_of_switchToState_44(),
	Fsm_t1527112426::get_offset_of_previousActiveState_45(),
	Fsm_t1527112426_StaticFields::get_offset_of_StateColors_46(),
	Fsm_t1527112426::get_offset_of_editState_47(),
	Fsm_t1527112426::get_offset_of_mouseEvents_48(),
	Fsm_t1527112426::get_offset_of_handleTriggerEnter2D_49(),
	Fsm_t1527112426::get_offset_of_handleTriggerExit2D_50(),
	Fsm_t1527112426::get_offset_of_handleTriggerStay2D_51(),
	Fsm_t1527112426::get_offset_of_handleCollisionEnter2D_52(),
	Fsm_t1527112426::get_offset_of_handleCollisionExit2D_53(),
	Fsm_t1527112426::get_offset_of_handleCollisionStay2D_54(),
	Fsm_t1527112426::get_offset_of_handleTriggerEnter_55(),
	Fsm_t1527112426::get_offset_of_handleTriggerExit_56(),
	Fsm_t1527112426::get_offset_of_handleTriggerStay_57(),
	Fsm_t1527112426::get_offset_of_handleCollisionEnter_58(),
	Fsm_t1527112426::get_offset_of_handleCollisionExit_59(),
	Fsm_t1527112426::get_offset_of_handleCollisionStay_60(),
	Fsm_t1527112426::get_offset_of_handleParticleCollision_61(),
	Fsm_t1527112426::get_offset_of_handleControllerColliderHit_62(),
	Fsm_t1527112426::get_offset_of_handleJointBreak_63(),
	Fsm_t1527112426::get_offset_of_handleJointBreak2D_64(),
	Fsm_t1527112426::get_offset_of_handleOnGUI_65(),
	Fsm_t1527112426::get_offset_of_handleFixedUpdate_66(),
	Fsm_t1527112426::get_offset_of_handleApplicationEvents_67(),
	Fsm_t1527112426_StaticFields::get_offset_of_lastRaycastHit2DInfoLUT_68(),
	Fsm_t1527112426::get_offset_of_handleAnimatorMove_69(),
	Fsm_t1527112426::get_offset_of_handleAnimatorIK_70(),
	Fsm_t1527112426_StaticFields::get_offset_of_targetSelf_71(),
	Fsm_t1527112426::get_offset_of_U3CStartedU3Ek__BackingField_72(),
	Fsm_t1527112426::get_offset_of_U3CEventTargetU3Ek__BackingField_73(),
	Fsm_t1527112426::get_offset_of_U3CFinishedU3Ek__BackingField_74(),
	Fsm_t1527112426::get_offset_of_U3CLastTransitionU3Ek__BackingField_75(),
	Fsm_t1527112426::get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_76(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CLastClickedObjectU3Ek__BackingField_77(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_78(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CHitBreakpointU3Ek__BackingField_79(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakAtFsmU3Ek__BackingField_80(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CBreakAtStateU3Ek__BackingField_81(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CIsBreakU3Ek__BackingField_82(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CIsErrorBreakU3Ek__BackingField_83(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CLastErrorU3Ek__BackingField_84(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CStepToStateChangeU3Ek__BackingField_85(),
	Fsm_t1527112426_StaticFields::get_offset_of_U3CStepFsmU3Ek__BackingField_86(),
	Fsm_t1527112426::get_offset_of_U3CSwitchedStateU3Ek__BackingField_87(),
	Fsm_t1527112426::get_offset_of_U3CCollisionInfoU3Ek__BackingField_88(),
	Fsm_t1527112426::get_offset_of_U3CTriggerColliderU3Ek__BackingField_89(),
	Fsm_t1527112426::get_offset_of_U3CCollision2DInfoU3Ek__BackingField_90(),
	Fsm_t1527112426::get_offset_of_U3CTriggerCollider2DU3Ek__BackingField_91(),
	Fsm_t1527112426::get_offset_of_U3CJointBreakForceU3Ek__BackingField_92(),
	Fsm_t1527112426::get_offset_of_U3CBrokenJoint2DU3Ek__BackingField_93(),
	Fsm_t1527112426::get_offset_of_U3CParticleCollisionGOU3Ek__BackingField_94(),
	Fsm_t1527112426::get_offset_of_U3CTriggerNameU3Ek__BackingField_95(),
	Fsm_t1527112426::get_offset_of_U3CCollisionNameU3Ek__BackingField_96(),
	Fsm_t1527112426::get_offset_of_U3CTrigger2dNameU3Ek__BackingField_97(),
	Fsm_t1527112426::get_offset_of_U3CCollision2dNameU3Ek__BackingField_98(),
	Fsm_t1527112426::get_offset_of_U3CControllerColliderU3Ek__BackingField_99(),
	Fsm_t1527112426::get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_100(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (EditorFlags_t1538621599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[6] = 
{
	EditorFlags_t1538621599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (FsmEvent_t2133468028), -1, sizeof(FsmEvent_t2133468028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[45] = 
{
	FsmEvent_t2133468028_StaticFields::get_offset_of_eventList_0(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_syncObj_1(),
	FsmEvent_t2133468028::get_offset_of_name_2(),
	FsmEvent_t2133468028::get_offset_of_isSystemEvent_3(),
	FsmEvent_t2133468028::get_offset_of_isGlobal_4(),
	FsmEvent_t2133468028::get_offset_of_U3CPathU3Ek__BackingField_5(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CBecameInvisibleU3Ek__BackingField_6(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CBecameVisibleU3Ek__BackingField_7(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionEnterU3Ek__BackingField_8(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionExitU3Ek__BackingField_9(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionStayU3Ek__BackingField_10(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionEnter2DU3Ek__BackingField_11(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionExit2DU3Ek__BackingField_12(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CCollisionStay2DU3Ek__BackingField_13(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CControllerColliderHitU3Ek__BackingField_14(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFinishedU3Ek__BackingField_15(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CLevelLoadedU3Ek__BackingField_16(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseDownU3Ek__BackingField_17(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseDragU3Ek__BackingField_18(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseEnterU3Ek__BackingField_19(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseExitU3Ek__BackingField_20(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseOverU3Ek__BackingField_21(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseUpU3Ek__BackingField_22(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMouseUpAsButtonU3Ek__BackingField_23(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerEnterU3Ek__BackingField_24(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerExitU3Ek__BackingField_25(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerStayU3Ek__BackingField_26(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerEnter2DU3Ek__BackingField_27(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerExit2DU3Ek__BackingField_28(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CTriggerStay2DU3Ek__BackingField_29(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationFocusU3Ek__BackingField_30(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationPauseU3Ek__BackingField_31(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CApplicationQuitU3Ek__BackingField_32(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CParticleCollisionU3Ek__BackingField_33(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CJointBreakU3Ek__BackingField_34(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CJointBreak2DU3Ek__BackingField_35(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CPlayerConnectedU3Ek__BackingField_36(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CServerInitializedU3Ek__BackingField_37(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CConnectedToServerU3Ek__BackingField_38(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_39(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_40(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFailedToConnectU3Ek__BackingField_41(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_42(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CMasterServerEventU3Ek__BackingField_43(),
	FsmEvent_t2133468028_StaticFields::get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (FsmLog_t1596141350), -1, sizeof(FsmLog_t1596141350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[8] = 
{
	0,
	FsmLog_t1596141350_StaticFields::get_offset_of_Logs_1(),
	FsmLog_t1596141350_StaticFields::get_offset_of_loggingEnabled_2(),
	FsmLog_t1596141350::get_offset_of_entries_3(),
	FsmLog_t1596141350_StaticFields::get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_4(),
	FsmLog_t1596141350_StaticFields::get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_5(),
	FsmLog_t1596141350::get_offset_of_U3CFsmU3Ek__BackingField_6(),
	FsmLog_t1596141350::get_offset_of_U3CResizedU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (FsmLogEntry_t2614866584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[18] = 
{
	FsmLogEntry_t2614866584::get_offset_of_textWithTimecode_0(),
	FsmLogEntry_t2614866584::get_offset_of_U3CLogU3Ek__BackingField_1(),
	FsmLogEntry_t2614866584::get_offset_of_U3CLogTypeU3Ek__BackingField_2(),
	FsmLogEntry_t2614866584::get_offset_of_U3CStateU3Ek__BackingField_3(),
	FsmLogEntry_t2614866584::get_offset_of_U3CSentByStateU3Ek__BackingField_4(),
	FsmLogEntry_t2614866584::get_offset_of_U3CActionU3Ek__BackingField_5(),
	FsmLogEntry_t2614866584::get_offset_of_U3CEventU3Ek__BackingField_6(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTransitionU3Ek__BackingField_7(),
	FsmLogEntry_t2614866584::get_offset_of_U3CEventTargetU3Ek__BackingField_8(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTimeU3Ek__BackingField_9(),
	FsmLogEntry_t2614866584::get_offset_of_U3CTextU3Ek__BackingField_10(),
	FsmLogEntry_t2614866584::get_offset_of_U3CText2U3Ek__BackingField_11(),
	FsmLogEntry_t2614866584::get_offset_of_U3CFrameCountU3Ek__BackingField_12(),
	FsmLogEntry_t2614866584::get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_13(),
	FsmLogEntry_t2614866584::get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_14(),
	FsmLogEntry_t2614866584::get_offset_of_U3CGameObjectU3Ek__BackingField_15(),
	FsmLogEntry_t2614866584::get_offset_of_U3CGameObjectNameU3Ek__BackingField_16(),
	FsmLogEntry_t2614866584::get_offset_of_U3CGameObjectIconU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (FsmLogType_t537852544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[12] = 
{
	FsmLogType_t537852544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (FsmState_t2146334067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[21] = 
{
	FsmState_t2146334067::get_offset_of_active_0(),
	FsmState_t2146334067::get_offset_of_finished_1(),
	FsmState_t2146334067::get_offset_of_activeAction_2(),
	FsmState_t2146334067::get_offset_of_activeActionIndex_3(),
	FsmState_t2146334067::get_offset_of_fsm_4(),
	FsmState_t2146334067::get_offset_of_name_5(),
	FsmState_t2146334067::get_offset_of_description_6(),
	FsmState_t2146334067::get_offset_of_colorIndex_7(),
	FsmState_t2146334067::get_offset_of_position_8(),
	FsmState_t2146334067::get_offset_of_isBreakpoint_9(),
	FsmState_t2146334067::get_offset_of_isSequence_10(),
	FsmState_t2146334067::get_offset_of_hideUnused_11(),
	FsmState_t2146334067::get_offset_of_transitions_12(),
	FsmState_t2146334067::get_offset_of_actions_13(),
	FsmState_t2146334067::get_offset_of_actionData_14(),
	FsmState_t2146334067::get_offset_of_activeActions_15(),
	FsmState_t2146334067::get_offset_of__finishedActions_16(),
	FsmState_t2146334067::get_offset_of_U3CStateTimeU3Ek__BackingField_17(),
	FsmState_t2146334067::get_offset_of_U3CRealStartTimeU3Ek__BackingField_18(),
	FsmState_t2146334067::get_offset_of_U3CloopCountU3Ek__BackingField_19(),
	FsmState_t2146334067::get_offset_of_U3CmaxLoopCountU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (FsmStateAction_t2366529033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[11] = 
{
	FsmStateAction_t2366529033::get_offset_of_name_0(),
	FsmStateAction_t2366529033::get_offset_of_enabled_1(),
	FsmStateAction_t2366529033::get_offset_of_isOpen_2(),
	FsmStateAction_t2366529033::get_offset_of_active_3(),
	FsmStateAction_t2366529033::get_offset_of_finished_4(),
	FsmStateAction_t2366529033::get_offset_of_autoName_5(),
	FsmStateAction_t2366529033::get_offset_of_owner_6(),
	FsmStateAction_t2366529033::get_offset_of_fsmState_7(),
	FsmStateAction_t2366529033::get_offset_of_fsm_8(),
	FsmStateAction_t2366529033::get_offset_of_fsmComponent_9(),
	FsmStateAction_t2366529033::get_offset_of_U3CEnteredU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (FsmTransition_t3771611999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[5] = 
{
	FsmTransition_t3771611999::get_offset_of_fsmEvent_0(),
	FsmTransition_t3771611999::get_offset_of_toState_1(),
	FsmTransition_t3771611999::get_offset_of_linkStyle_2(),
	FsmTransition_t3771611999::get_offset_of_linkConstraint_3(),
	FsmTransition_t3771611999::get_offset_of_colorIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (CustomLinkStyle_t2484246752)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2452[4] = 
{
	CustomLinkStyle_t2484246752::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (CustomLinkConstraint_t4157095950)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2453[4] = 
{
	CustomLinkConstraint_t4157095950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (FsmUtility_t81143630), -1, sizeof(FsmUtility_t81143630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	FsmUtility_t81143630_StaticFields::get_offset_of_encoding_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (BitConverter_t1958851146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (FsmVariables_t963491929), -1, sizeof(FsmVariables_t963491929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[18] = 
{
	FsmVariables_t963491929::get_offset_of_floatVariables_0(),
	FsmVariables_t963491929::get_offset_of_intVariables_1(),
	FsmVariables_t963491929::get_offset_of_boolVariables_2(),
	FsmVariables_t963491929::get_offset_of_stringVariables_3(),
	FsmVariables_t963491929::get_offset_of_vector2Variables_4(),
	FsmVariables_t963491929::get_offset_of_vector3Variables_5(),
	FsmVariables_t963491929::get_offset_of_colorVariables_6(),
	FsmVariables_t963491929::get_offset_of_rectVariables_7(),
	FsmVariables_t963491929::get_offset_of_quaternionVariables_8(),
	FsmVariables_t963491929::get_offset_of_gameObjectVariables_9(),
	FsmVariables_t963491929::get_offset_of_objectVariables_10(),
	FsmVariables_t963491929::get_offset_of_materialVariables_11(),
	FsmVariables_t963491929::get_offset_of_textureVariables_12(),
	FsmVariables_t963491929::get_offset_of_arrayVariables_13(),
	FsmVariables_t963491929::get_offset_of_enumVariables_14(),
	FsmVariables_t963491929::get_offset_of_categories_15(),
	FsmVariables_t963491929::get_offset_of_variableCategoryIDs_16(),
	FsmVariables_t963491929_StaticFields::get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (VariableType_t3118725144)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2457[17] = 
{
	VariableType_t3118725144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (VariableTypeNicified_t195153339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2458[16] = 
{
	VariableTypeNicified_t195153339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (ArrayVariableTypesNicified_t105039977)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2459[15] = 
{
	ArrayVariableTypesNicified_t105039977::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (MissingAction_t834633994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[1] = 
{
	MissingAction_t834633994::get_offset_of_actionName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (PlayMakerPrefs_t941311808), -1, sizeof(PlayMakerPrefs_t941311808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2461[6] = 
{
	PlayMakerPrefs_t941311808_StaticFields::get_offset_of_instance_2(),
	PlayMakerPrefs_t941311808_StaticFields::get_offset_of_defaultColors_3(),
	PlayMakerPrefs_t941311808_StaticFields::get_offset_of_defaultColorNames_4(),
	PlayMakerPrefs_t941311808::get_offset_of_colors_5(),
	PlayMakerPrefs_t941311808::get_offset_of_colorNames_6(),
	PlayMakerPrefs_t941311808_StaticFields::get_offset_of_minimapColors_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (ReflectionUtils_t1202855664), -1, sizeof(ReflectionUtils_t1202855664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[3] = 
{
	ReflectionUtils_t1202855664_StaticFields::get_offset_of_assemblyNames_0(),
	ReflectionUtils_t1202855664_StaticFields::get_offset_of_loadedAssemblies_1(),
	ReflectionUtils_t1202855664_StaticFields::get_offset_of_typeLookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (PlayMakerProxyBase_t3469687535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[1] = 
{
	PlayMakerProxyBase_t3469687535::get_offset_of_playMakerFSMs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (PlayMakerAnimatorIK_t2388642745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (PlayMakerAnimatorMove_t1973299400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (PlayMakerApplicationEvents_t1615127321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (PlayMakerCollisionEnter_t4046453814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (PlayMakerCollisionEnter2D_t1696713992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (PlayMakerCollisionExit_t3871318016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (PlayMakerCollisionExit2D_t894936658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (PlayMakerCollisionStay_t3871731003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (PlayMakerCollisionStay2D_t1291817165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (PlayMakerControllerColliderHit_t1717485139), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (PlayMakerFixedUpdate_t997170669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (PlayMakerFSM_t3799847376), -1, sizeof(PlayMakerFSM_t3799847376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2475[10] = 
{
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_fsmList_2(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_MaximizeFileCompatibility_3(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_ApplicationIsQuitting_4(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_NotMainThread_5(),
	PlayMakerFSM_t3799847376::get_offset_of_fsm_6(),
	PlayMakerFSM_t3799847376::get_offset_of_fsmTemplate_7(),
	PlayMakerFSM_t3799847376::get_offset_of_eventHandlerComponentsAdded_8(),
	PlayMakerFSM_t3799847376::get_offset_of_U3CGuiTextureU3Ek__BackingField_9(),
	PlayMakerFSM_t3799847376::get_offset_of_U3CGuiTextU3Ek__BackingField_10(),
	PlayMakerFSM_t3799847376_StaticFields::get_offset_of_U3CDrawGizmosU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (U3CDoCoroutineU3Ed__1_t3331551771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[4] = 
{
	U3CDoCoroutineU3Ed__1_t3331551771::get_offset_of_U3CU3E2__current_0(),
	U3CDoCoroutineU3Ed__1_t3331551771::get_offset_of_U3CU3E1__state_1(),
	U3CDoCoroutineU3Ed__1_t3331551771::get_offset_of_U3CU3E4__this_2(),
	U3CDoCoroutineU3Ed__1_t3331551771::get_offset_of_routine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (PlayMakerGlobals_t3097244096), -1, sizeof(PlayMakerGlobals_t3097244096_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2477[8] = 
{
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_instance_2(),
	PlayMakerGlobals_t3097244096::get_offset_of_variables_3(),
	PlayMakerGlobals_t3097244096::get_offset_of_events_4(),
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_U3CInitializedU3Ek__BackingField_5(),
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_U3CIsPlayingInEditorU3Ek__BackingField_6(),
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_U3CIsPlayingU3Ek__BackingField_7(),
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_U3CIsEditorU3Ek__BackingField_8(),
	PlayMakerGlobals_t3097244096_StaticFields::get_offset_of_U3CIsBuildingU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (PlayMakerGUI_t3799848395), -1, sizeof(PlayMakerGUI_t3799848395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2478[27] = 
{
	0,
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_fsmList_3(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_SelectedFSM_4(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_labelContent_5(),
	PlayMakerGUI_t3799848395::get_offset_of_previewOnGUI_6(),
	PlayMakerGUI_t3799848395::get_offset_of_enableGUILayout_7(),
	PlayMakerGUI_t3799848395::get_offset_of_drawStateLabels_8(),
	PlayMakerGUI_t3799848395::get_offset_of_GUITextureStateLabels_9(),
	PlayMakerGUI_t3799848395::get_offset_of_GUITextStateLabels_10(),
	PlayMakerGUI_t3799848395::get_offset_of_filterLabelsWithDistance_11(),
	PlayMakerGUI_t3799848395::get_offset_of_maxLabelDistance_12(),
	PlayMakerGUI_t3799848395::get_offset_of_controlMouseCursor_13(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_SortedFsmList_14(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_labelGameObject_15(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_fsmLabelIndex_16(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_instance_17(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiSkin_18(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiColor_19(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiBackgroundColor_20(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiContentColor_21(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_guiMatrix_22(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_stateLabelStyle_23(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_stateLabelBackground_24(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CMouseCursorU3Ek__BackingField_25(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CLockCursorU3Ek__BackingField_26(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_U3CHideCursorU3Ek__BackingField_27(),
	PlayMakerGUI_t3799848395_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (PlayMakerMouseEvents_t316289710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (PlayMakerOnGUI_t940239724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	PlayMakerOnGUI_t940239724::get_offset_of_playMakerFSM_2(),
	PlayMakerOnGUI_t940239724::get_offset_of_previewInEditMode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (PlayMakerTriggerEnter_t977448304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (PlayMakerTriggerEnter2D_t3024951234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (PlayMakerTriggerExit_t3495223174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (PlayMakerTriggerExit2D_t245046360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (PlayMakerTriggerStay_t3495636161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (PlayMakerTriggerStay2D_t641926867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (PlayMakerParticleCollision_t2428451804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (PlayMakerJointBreak_t197925893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (PlayMakerJointBreak2D_t1228223767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (U3CModuleU3E_t86524800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (EventHandle_t1938870832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2491[3] = 
{
	EventHandle_t1938870832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
