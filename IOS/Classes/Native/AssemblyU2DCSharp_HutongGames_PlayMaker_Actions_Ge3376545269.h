﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct  GetAnimatorBool_t3376545269  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBool::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorBool::parameter
	FsmString_t952858651 * ___parameter_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorBool::result
	FsmBool_t1075959796 * ___result_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBool::_animator
	Animator_t2776330603 * ____animator_17;
	// System.Int32 HutongGames.PlayMaker.Actions.GetAnimatorBool::_paramID
	int32_t ____paramID_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_parameter_15() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___parameter_15)); }
	inline FsmString_t952858651 * get_parameter_15() const { return ___parameter_15; }
	inline FsmString_t952858651 ** get_address_of_parameter_15() { return &___parameter_15; }
	inline void set_parameter_15(FsmString_t952858651 * value)
	{
		___parameter_15 = value;
		Il2CppCodeGenWriteBarrier(&___parameter_15, value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ___result_16)); }
	inline FsmBool_t1075959796 * get_result_16() const { return ___result_16; }
	inline FsmBool_t1075959796 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmBool_t1075959796 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier(&___result_16, value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ____animator_17)); }
	inline Animator_t2776330603 * get__animator_17() const { return ____animator_17; }
	inline Animator_t2776330603 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t2776330603 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier(&____animator_17, value);
	}

	inline static int32_t get_offset_of__paramID_18() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3376545269, ____paramID_18)); }
	inline int32_t get__paramID_18() const { return ____paramID_18; }
	inline int32_t* get_address_of__paramID_18() { return &____paramID_18; }
	inline void set__paramID_18(int32_t value)
	{
		____paramID_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
