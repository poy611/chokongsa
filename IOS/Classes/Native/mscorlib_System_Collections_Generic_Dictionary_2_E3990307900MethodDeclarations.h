﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3950172068(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3990307900 *, Dictionary_2_t2672984508 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m935329853(__this, method) ((  Il2CppObject * (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1674492561(__this, method) ((  void (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2520469402(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3492502361(__this, method) ((  Il2CppObject * (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2973747115(__this, method) ((  Il2CppObject * (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::MoveNext()
#define Enumerator_MoveNext_m4284706429(__this, method) ((  bool (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::get_Current()
#define Enumerator_get_Current_m3394421011(__this, method) ((  KeyValuePair_2_t2571765214  (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2657242378(__this, method) ((  Type_t * (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m572280750(__this, method) ((  FieldInfoU5BU5D_t2567562023* (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::Reset()
#define Enumerator_Reset_m1531722742(__this, method) ((  void (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::VerifyState()
#define Enumerator_VerifyState_m3376692095(__this, method) ((  void (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m750699303(__this, method) ((  void (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Reflection.FieldInfo[]>::Dispose()
#define Enumerator_Dispose_m1335201606(__this, method) ((  void (*) (Enumerator_t3990307900 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
