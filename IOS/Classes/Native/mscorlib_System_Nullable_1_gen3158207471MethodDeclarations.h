﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3158207471.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatParseHandling3074080948.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4073717711_gshared (Nullable_1_t3158207471 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4073717711(__this, ___value0, method) ((  void (*) (Nullable_1_t3158207471 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4073717711_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2464786364_gshared (Nullable_1_t3158207471 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2464786364(__this, method) ((  bool (*) (Nullable_1_t3158207471 *, const MethodInfo*))Nullable_1_get_HasValue_m2464786364_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m110206263_gshared (Nullable_1_t3158207471 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m110206263(__this, method) ((  int32_t (*) (Nullable_1_t3158207471 *, const MethodInfo*))Nullable_1_get_Value_m110206263_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2379666885_gshared (Nullable_1_t3158207471 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2379666885(__this, ___other0, method) ((  bool (*) (Nullable_1_t3158207471 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2379666885_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m4085818842_gshared (Nullable_1_t3158207471 * __this, Nullable_1_t3158207471  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4085818842(__this, ___other0, method) ((  bool (*) (Nullable_1_t3158207471 *, Nullable_1_t3158207471 , const MethodInfo*))Nullable_1_Equals_m4085818842_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1958667421_gshared (Nullable_1_t3158207471 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1958667421(__this, method) ((  int32_t (*) (Nullable_1_t3158207471 *, const MethodInfo*))Nullable_1_GetHashCode_m1958667421_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m639602947_gshared (Nullable_1_t3158207471 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m639602947(__this, method) ((  int32_t (*) (Nullable_1_t3158207471 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m639602947_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m4280895756_gshared (Nullable_1_t3158207471 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m4280895756(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3158207471 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m4280895756_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.FloatParseHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m663241499_gshared (Nullable_1_t3158207471 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m663241499(__this, method) ((  String_t* (*) (Nullable_1_t3158207471 *, const MethodInfo*))Nullable_1_ToString_m663241499_gshared)(__this, method)
