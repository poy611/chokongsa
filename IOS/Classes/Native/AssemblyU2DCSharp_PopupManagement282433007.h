﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// UnityEngine.Object
struct Object_t3071478659;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupManagement
struct  PopupManagement_t282433007  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 PopupManagement::reverse
	int32_t ___reverse_3;
	// UnityEngine.Object[] PopupManagement::ad
	ObjectU5BU5D_t1015136018* ___ad_4;
	// UnityEngine.Object PopupManagement::pm
	Object_t3071478659 * ___pm_5;

public:
	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(PopupManagement_t282433007, ___reverse_3)); }
	inline int32_t get_reverse_3() const { return ___reverse_3; }
	inline int32_t* get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(int32_t value)
	{
		___reverse_3 = value;
	}

	inline static int32_t get_offset_of_ad_4() { return static_cast<int32_t>(offsetof(PopupManagement_t282433007, ___ad_4)); }
	inline ObjectU5BU5D_t1015136018* get_ad_4() const { return ___ad_4; }
	inline ObjectU5BU5D_t1015136018** get_address_of_ad_4() { return &___ad_4; }
	inline void set_ad_4(ObjectU5BU5D_t1015136018* value)
	{
		___ad_4 = value;
		Il2CppCodeGenWriteBarrier(&___ad_4, value);
	}

	inline static int32_t get_offset_of_pm_5() { return static_cast<int32_t>(offsetof(PopupManagement_t282433007, ___pm_5)); }
	inline Object_t3071478659 * get_pm_5() const { return ___pm_5; }
	inline Object_t3071478659 ** get_address_of_pm_5() { return &___pm_5; }
	inline void set_pm_5(Object_t3071478659 * value)
	{
		___pm_5 = value;
		Il2CppCodeGenWriteBarrier(&___pm_5, value);
	}
};

struct PopupManagement_t282433007_StaticFields
{
public:
	// PopupManagement PopupManagement::p
	PopupManagement_t282433007 * ___p_2;

public:
	inline static int32_t get_offset_of_p_2() { return static_cast<int32_t>(offsetof(PopupManagement_t282433007_StaticFields, ___p_2)); }
	inline PopupManagement_t282433007 * get_p_2() const { return ___p_2; }
	inline PopupManagement_t282433007 ** get_address_of_p_2() { return &___p_2; }
	inline void set_p_2(PopupManagement_t282433007 * value)
	{
		___p_2 = value;
		Il2CppCodeGenWriteBarrier(&___p_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
