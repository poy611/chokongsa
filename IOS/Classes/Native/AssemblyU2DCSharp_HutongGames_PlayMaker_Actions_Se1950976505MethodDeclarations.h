﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralVector3
struct SetProceduralVector3_t1950976505;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::.ctor()
extern "C"  void SetProceduralVector3__ctor_m1393079437 (SetProceduralVector3_t1950976505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::Reset()
extern "C"  void SetProceduralVector3_Reset_m3334479674 (SetProceduralVector3_t1950976505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::OnEnter()
extern "C"  void SetProceduralVector3_OnEnter_m2406238500 (SetProceduralVector3_t1950976505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::OnUpdate()
extern "C"  void SetProceduralVector3_OnUpdate_m712508831 (SetProceduralVector3_t1950976505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::DoSetProceduralVector()
extern "C"  void SetProceduralVector3_DoSetProceduralVector_m2923597058 (SetProceduralVector3_t1950976505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
