﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlaySound
struct  PlaySound_t2516791689  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlaySound::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PlaySound::position
	FsmVector3_t533912882 * ___position_12;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.PlaySound::clip
	FsmObject_t821476169 * ___clip_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlaySound::volume
	FsmFloat_t2134102846 * ___volume_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___position_12)); }
	inline FsmVector3_t533912882 * get_position_12() const { return ___position_12; }
	inline FsmVector3_t533912882 ** get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(FsmVector3_t533912882 * value)
	{
		___position_12 = value;
		Il2CppCodeGenWriteBarrier(&___position_12, value);
	}

	inline static int32_t get_offset_of_clip_13() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___clip_13)); }
	inline FsmObject_t821476169 * get_clip_13() const { return ___clip_13; }
	inline FsmObject_t821476169 ** get_address_of_clip_13() { return &___clip_13; }
	inline void set_clip_13(FsmObject_t821476169 * value)
	{
		___clip_13 = value;
		Il2CppCodeGenWriteBarrier(&___clip_13, value);
	}

	inline static int32_t get_offset_of_volume_14() { return static_cast<int32_t>(offsetof(PlaySound_t2516791689, ___volume_14)); }
	inline FsmFloat_t2134102846 * get_volume_14() const { return ___volume_14; }
	inline FsmFloat_t2134102846 ** get_address_of_volume_14() { return &___volume_14; }
	inline void set_volume_14(FsmFloat_t2134102846 * value)
	{
		___volume_14 = value;
		Il2CppCodeGenWriteBarrier(&___volume_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
