﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1
struct U3CU3Ec__DisplayClass13_1_t1733456159;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass13_1__ctor_m1787293543 (U3CU3Ec__DisplayClass13_1_t1733456159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::<Create>b__1(System.Object)
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass13_1_U3CCreateU3Eb__1_m1330085709 (U3CU3Ec__DisplayClass13_1_t1733456159 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
