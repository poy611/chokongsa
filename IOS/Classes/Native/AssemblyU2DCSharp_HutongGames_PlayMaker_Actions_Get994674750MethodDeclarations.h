﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
struct GetAnimatorCullingMode_t994674750;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::.ctor()
extern "C"  void GetAnimatorCullingMode__ctor_m1074548200 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::Reset()
extern "C"  void GetAnimatorCullingMode_Reset_m3015948437 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::OnEnter()
extern "C"  void GetAnimatorCullingMode_OnEnter_m1240397759 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::DoCheckCulling()
extern "C"  void GetAnimatorCullingMode_DoCheckCulling_m2645242639 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
