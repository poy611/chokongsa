﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Joint2D2513613714.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnchoredJoint2D
struct  AnchoredJoint2D_t1716788702  : public Joint2D_t2513613714
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
