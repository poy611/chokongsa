﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Serving/<okNet>c__Iterator2B
struct U3CokNetU3Ec__Iterator2B_t599730315;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Serving/<okNet>c__Iterator2B::.ctor()
extern "C"  void U3CokNetU3Ec__Iterator2B__ctor_m3561948784 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<okNet>c__Iterator2B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator2B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2761915372 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<okNet>c__Iterator2B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator2B_System_Collections_IEnumerator_get_Current_m993974144 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Serving/<okNet>c__Iterator2B::MoveNext()
extern "C"  bool U3CokNetU3Ec__Iterator2B_MoveNext_m3027829612 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<okNet>c__Iterator2B::Dispose()
extern "C"  void U3CokNetU3Ec__Iterator2B_Dispose_m4137096877 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<okNet>c__Iterator2B::Reset()
extern "C"  void U3CokNetU3Ec__Iterator2B_Reset_m1208381725 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
