﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>
struct DefaultComparer_t1180910956;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void DefaultComparer__ctor_m128211465_gshared (DefaultComparer_t1180910956 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m128211465(__this, method) ((  void (*) (DefaultComparer_t1180910956 *, const MethodInfo*))DefaultComparer__ctor_m128211465_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m808039110_gshared (DefaultComparer_t1180910956 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m808039110(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1180910956 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m808039110_gshared)(__this, ___x0, ___y1, method)
