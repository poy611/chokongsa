﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943444146.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m447238051_gshared (InternalEnumerator_1_t3943444146 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m447238051(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3943444146 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m447238051_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813(__this, method) ((  void (*) (InternalEnumerator_1_t3943444146 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3943444146 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2608036154_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2608036154(__this, method) ((  void (*) (InternalEnumerator_1_t3943444146 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2608036154_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2535341517_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2535341517(__this, method) ((  bool (*) (InternalEnumerator_1_t3943444146 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2535341517_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::get_Current()
extern "C"  TypeConvertKey_t866134174  InternalEnumerator_1_get_Current_m624396236_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m624396236(__this, method) ((  TypeConvertKey_t866134174  (*) (InternalEnumerator_1_t3943444146 *, const MethodInfo*))InternalEnumerator_1_get_Current_m624396236_gshared)(__this, method)
