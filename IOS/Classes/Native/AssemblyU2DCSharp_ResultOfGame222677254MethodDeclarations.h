﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultOfGame
struct ResultOfGame_t222677254;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ResultData
struct ResultData_t1421128071;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultData1421128071.h"

// System.Void ResultOfGame::.ctor()
extern "C"  void ResultOfGame__ctor_m4222414933 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::Start()
extern "C"  void ResultOfGame_Start_m3169552725 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::OnSave()
extern "C"  void ResultOfGame_OnSave_m3939102667 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::toMainScene()
extern "C"  void ResultOfGame_toMainScene_m161045547 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ResultOfGame::okNet()
extern "C"  Il2CppObject * ResultOfGame_okNet_m1318551084 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::csSwitch(System.Single)
extern "C"  void ResultOfGame_csSwitch_m2456559256 (ResultOfGame_t222677254 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::OnAllResult(ResultData)
extern "C"  void ResultOfGame_OnAllResult_m3642452555 (ResultOfGame_t222677254 * __this, ResultData_t1421128071 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::OnAllResultFail()
extern "C"  void ResultOfGame_OnAllResultFail_m290035056 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::ShowData(System.Int32)
extern "C"  void ResultOfGame_ShowData_m1298837031 (ResultOfGame_t222677254 * __this, int32_t ____countOfGame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ResultOfGame::GetGameName(System.Int32)
extern "C"  String_t* ResultOfGame_GetGameName_m3765493524 (ResultOfGame_t222677254 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame::OnDestroy()
extern "C"  void ResultOfGame_OnDestroy_m271880910 (ResultOfGame_t222677254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
