﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SwipeGestureEvent
struct SwipeGestureEvent_t2968962937;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"

// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::.ctor()
extern "C"  void SwipeGestureEvent__ctor_m2119904733 (SwipeGestureEvent_t2968962937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::Reset()
extern "C"  void SwipeGestureEvent_Reset_m4061304970 (SwipeGestureEvent_t2968962937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::OnEnter()
extern "C"  void SwipeGestureEvent_OnEnter_m805678708 (SwipeGestureEvent_t2968962937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::OnUpdate()
extern "C"  void SwipeGestureEvent_OnUpdate_m2634762831 (SwipeGestureEvent_t2968962937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::TestForSwipeGesture(UnityEngine.Touch)
extern "C"  void SwipeGestureEvent_TestForSwipeGesture_m1737440969 (SwipeGestureEvent_t2968962937 * __this, Touch_t4210255029  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
