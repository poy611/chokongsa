﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmArrayItem
struct GetFsmArrayItem_t2453534512;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::.ctor()
extern "C"  void GetFsmArrayItem__ctor_m1289967110 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::Reset()
extern "C"  void GetFsmArrayItem_Reset_m3231367347 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::OnEnter()
extern "C"  void GetFsmArrayItem_OnEnter_m2099540061 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::DoGetFsmArray()
extern "C"  void GetFsmArrayItem_DoGetFsmArray_m1586405384 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::OnUpdate()
extern "C"  void GetFsmArrayItem_OnUpdate_m4089759110 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
