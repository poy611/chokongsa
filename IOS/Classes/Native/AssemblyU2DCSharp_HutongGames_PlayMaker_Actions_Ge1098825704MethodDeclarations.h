﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d
struct GetNextOverlapCircle2d_t1098825704;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::.ctor()
extern "C"  void GetNextOverlapCircle2d__ctor_m1703548286 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::Reset()
extern "C"  void GetNextOverlapCircle2d_Reset_m3644948523 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::OnEnter()
extern "C"  void GetNextOverlapCircle2d_OnEnter_m119091669 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::DoGetNextCollider()
extern "C"  void GetNextOverlapCircle2d_DoGetNextCollider_m3130863630 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::GetOverlapCircleAll()
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapCircle2d_GetOverlapCircleAll_m1663548798 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
