﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempingPointer/<TempingSlideDown>c__Iterator31
struct U3CTempingSlideDownU3Ec__Iterator31_t3382326578;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::.ctor()
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31__ctor_m1655238569 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<TempingSlideDown>c__Iterator31::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideDownU3Ec__Iterator31_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1746407699 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<TempingSlideDown>c__Iterator31::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideDownU3Ec__Iterator31_System_Collections_IEnumerator_get_Current_m3938228903 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TempingPointer/<TempingSlideDown>c__Iterator31::MoveNext()
extern "C"  bool U3CTempingSlideDownU3Ec__Iterator31_MoveNext_m1457168019 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::Dispose()
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31_Dispose_m1444648358 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::Reset()
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31_Reset_m3596638806 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
