﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t2614866584;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t1596141350;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Texture
struct Texture_t2526458961;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog1596141350.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType537852544.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.FsmLogEntry::get_Log()
extern "C"  FsmLog_t1596141350 * FsmLogEntry_get_Log_m4222680721 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Log(HutongGames.PlayMaker.FsmLog)
extern "C"  void FsmLogEntry_set_Log_m1497111964 (FsmLogEntry_t2614866584 * __this, FsmLog_t1596141350 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLogType HutongGames.PlayMaker.FsmLogEntry::get_LogType()
extern "C"  int32_t FsmLogEntry_get_LogType_m741444561 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_LogType(HutongGames.PlayMaker.FsmLogType)
extern "C"  void FsmLogEntry_set_LogType_m867500892 (FsmLogEntry_t2614866584 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLogEntry::get_Fsm()
extern "C"  Fsm_t1527112426 * FsmLogEntry_get_Fsm_m1646077211 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::get_State()
extern "C"  FsmState_t2146334067 * FsmLogEntry_get_State_m4097769969 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_State(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLogEntry_set_State_m334904252 (FsmLogEntry_t2614866584 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::get_SentByState()
extern "C"  FsmState_t2146334067 * FsmLogEntry_get_SentByState_m1783985570 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_SentByState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLogEntry_set_SentByState_m1675580845 (FsmLogEntry_t2614866584 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmLogEntry::get_Action()
extern "C"  FsmStateAction_t2366529033 * FsmLogEntry_get_Action_m423245518 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Action(HutongGames.PlayMaker.FsmStateAction)
extern "C"  void FsmLogEntry_set_Action_m3998130845 (FsmLogEntry_t2614866584 * __this, FsmStateAction_t2366529033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmLogEntry::get_Event()
extern "C"  FsmEvent_t2133468028 * FsmLogEntry_get_Event_m1154585745 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmLogEntry_set_Event_m1573492956 (FsmLogEntry_t2614866584 * __this, FsmEvent_t2133468028 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmLogEntry::get_Transition()
extern "C"  FsmTransition_t3771611999 * FsmLogEntry_get_Transition_m2725474353 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Transition(HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmLogEntry_set_Transition_m4033039130 (FsmLogEntry_t2614866584 * __this, FsmTransition_t3771611999 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmLogEntry::get_EventTarget()
extern "C"  FsmEventTarget_t1823904941 * FsmLogEntry_get_EventTarget_m978602033 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_EventTarget(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmLogEntry_set_EventTarget_m2302808892 (FsmLogEntry_t2614866584 * __this, FsmEventTarget_t1823904941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmLogEntry::get_Time()
extern "C"  float FsmLogEntry_get_Time_m367739367 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Time(System.Single)
extern "C"  void FsmLogEntry_set_Time_m1212637156 (FsmLogEntry_t2614866584 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_Text()
extern "C"  String_t* FsmLogEntry_get_Text_m552976464 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Text(System.String)
extern "C"  void FsmLogEntry_set_Text_m4158062299 (FsmLogEntry_t2614866584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_Text2()
extern "C"  String_t* FsmLogEntry_get_Text2_m4257378116 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Text2(System.String)
extern "C"  void FsmLogEntry_set_Text2_m2247410997 (FsmLogEntry_t2614866584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmLogEntry::get_FrameCount()
extern "C"  int32_t FsmLogEntry_get_FrameCount_m3805210742 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_FrameCount(System.Int32)
extern "C"  void FsmLogEntry_set_FrameCount_m44195149 (FsmLogEntry_t2614866584 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::get_FsmVariablesCopy()
extern "C"  FsmVariables_t963491929 * FsmLogEntry_get_FsmVariablesCopy_m3242140436 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_FsmVariablesCopy(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmLogEntry_set_FsmVariablesCopy_m6319043 (FsmLogEntry_t2614866584 * __this, FsmVariables_t963491929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::get_GlobalVariablesCopy()
extern "C"  FsmVariables_t963491929 * FsmLogEntry_get_GlobalVariablesCopy_m576731939 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GlobalVariablesCopy(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmLogEntry_set_GlobalVariablesCopy_m2363236206 (FsmLogEntry_t2614866584 * __this, FsmVariables_t963491929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmLogEntry::get_GameObject()
extern "C"  GameObject_t3674682005 * FsmLogEntry_get_GameObject_m15808862 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObject(UnityEngine.GameObject)
extern "C"  void FsmLogEntry_set_GameObject_m2079881635 (FsmLogEntry_t2614866584 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_GameObjectName()
extern "C"  String_t* FsmLogEntry_get_GameObjectName_m1570925791 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObjectName(System.String)
extern "C"  void FsmLogEntry_set_GameObjectName_m2711130988 (FsmLogEntry_t2614866584 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture HutongGames.PlayMaker.FsmLogEntry::get_GameObjectIcon()
extern "C"  Texture_t2526458961 * FsmLogEntry_get_GameObjectIcon_m1992511841 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObjectIcon(UnityEngine.Texture)
extern "C"  void FsmLogEntry_set_GameObjectIcon_m974796586 (FsmLogEntry_t2614866584 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_TextWithTimecode()
extern "C"  String_t* FsmLogEntry_get_TextWithTimecode_m4198762256 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmLogEntry::GetIndex()
extern "C"  int32_t FsmLogEntry_GetIndex_m4159633063 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::DebugLog()
extern "C"  void FsmLogEntry_DebugLog_m770604750 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::.ctor()
extern "C"  void FsmLogEntry__ctor_m276108103 (FsmLogEntry_t2614866584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
