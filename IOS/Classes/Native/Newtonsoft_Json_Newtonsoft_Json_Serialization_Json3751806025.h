﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t3751806025  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0::property
	JsonProperty_t902655177 * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t3751806025, ___property_0)); }
	inline JsonProperty_t902655177 * get_property_0() const { return ___property_0; }
	inline JsonProperty_t902655177 ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_t902655177 * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier(&___property_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
