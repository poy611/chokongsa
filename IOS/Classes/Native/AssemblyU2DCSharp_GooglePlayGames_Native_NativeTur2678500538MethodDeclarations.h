﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey8F
struct U3CFindInvitationWithIdU3Ec__AnonStorey8F_t2678500538;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t3460122515;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3460122515.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey8F::.ctor()
extern "C"  void U3CFindInvitationWithIdU3Ec__AnonStorey8F__ctor_m2948027889 (U3CFindInvitationWithIdU3Ec__AnonStorey8F_t2678500538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey8F::<>m__7E(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse)
extern "C"  void U3CFindInvitationWithIdU3Ec__AnonStorey8F_U3CU3Em__7E_m4236808117 (U3CFindInvitationWithIdU3Ec__AnonStorey8F_t2678500538 * __this, TurnBasedMatchesResponse_t3460122515 * ___allMatches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
