﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>
struct Collection_1_t1858123337;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t3909450202;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t289562932;
// System.Collections.Generic.IList`1<HutongGames.PlayMaker.ParamDataType>
struct IList_1_t1072345086;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void Collection_1__ctor_m315169576_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1__ctor_m315169576(__this, method) ((  void (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1__ctor_m315169576_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1689079891_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1689079891(__this, method) ((  bool (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1689079891_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m656408348_gshared (Collection_1_t1858123337 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m656408348(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1858123337 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m656408348_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3389868823_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3389868823(__this, method) ((  Il2CppObject * (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3389868823_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m4274939194_gshared (Collection_1_t1858123337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m4274939194(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1858123337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m4274939194_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m4045160146_gshared (Collection_1_t1858123337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m4045160146(__this, ___value0, method) ((  bool (*) (Collection_1_t1858123337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m4045160146_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m696610450_gshared (Collection_1_t1858123337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m696610450(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1858123337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m696610450_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m472056765_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m472056765(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m472056765_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2739164747_gshared (Collection_1_t1858123337 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2739164747(__this, ___value0, method) ((  void (*) (Collection_1_t1858123337 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2739164747_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1168494410_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1168494410(__this, method) ((  bool (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1168494410_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2846363830_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2846363830(__this, method) ((  Il2CppObject * (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2846363830_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2376594369_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2376594369(__this, method) ((  bool (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2376594369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m807767832_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m807767832(__this, method) ((  bool (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m807767832_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m527084733_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m527084733(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m527084733_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3288937556_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3288937556(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3288937556_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Add(T)
extern "C"  void Collection_1_Add_m2494160983_gshared (Collection_1_t1858123337 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2494160983(__this, ___item0, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_Add_m2494160983_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Clear()
extern "C"  void Collection_1_Clear_m2016270163_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2016270163(__this, method) ((  void (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_Clear_m2016270163_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1659790735_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1659790735(__this, method) ((  void (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_ClearItems_m1659790735_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool Collection_1_Contains_m664615681_gshared (Collection_1_t1858123337 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m664615681(__this, ___item0, method) ((  bool (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_Contains_m664615681_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3825139847_gshared (Collection_1_t1858123337 * __this, ParamDataTypeU5BU5D_t3909450202* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3825139847(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1858123337 *, ParamDataTypeU5BU5D_t3909450202*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3825139847_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m949524708_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m949524708(__this, method) ((  Il2CppObject* (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_GetEnumerator_m949524708_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m300254795_gshared (Collection_1_t1858123337 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m300254795(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m300254795_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1822526654_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1822526654(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1822526654_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1428696049_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1428696049(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1428696049_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m3724490343_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m3724490343(__this, method) ((  Il2CppObject* (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_get_Items_m3724490343_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Remove(T)
extern "C"  bool Collection_1_Remove_m2033633084_gshared (Collection_1_t1858123337 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2033633084(__this, ___item0, method) ((  bool (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_Remove_m2033633084_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3991346820_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3991346820(__this, ___index0, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3991346820_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3178123684_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3178123684(__this, ___index0, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3178123684_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m535271568_gshared (Collection_1_t1858123337 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m535271568(__this, method) ((  int32_t (*) (Collection_1_t1858123337 *, const MethodInfo*))Collection_1_get_Count_m535271568_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2081729544_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2081729544(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1858123337 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2081729544_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m911054357_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m911054357(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m911054357_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1980341188_gshared (Collection_1_t1858123337 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1980341188(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1858123337 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1980341188_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1035449611_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1035449611(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1035449611_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m495523559_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m495523559(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m495523559_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1681689031_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1681689031(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1681689031_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2453749465_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2453749465(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2453749465_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m564403814_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m564403814(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m564403814_gshared)(__this /* static, unused */, ___list0, method)
