﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LookAt2d
struct LookAt2d_t1999723980;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LookAt2d::.ctor()
extern "C"  void LookAt2d__ctor_m2052579866 (LookAt2d_t1999723980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2d::Reset()
extern "C"  void LookAt2d_Reset_m3993980103 (LookAt2d_t1999723980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2d::OnEnter()
extern "C"  void LookAt2d_OnEnter_m530990961 (LookAt2d_t1999723980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2d::OnUpdate()
extern "C"  void LookAt2d_OnUpdate_m2709377266 (LookAt2d_t1999723980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2d::DoLookAt()
extern "C"  void LookAt2d_DoLookAt_m3993294791 (LookAt2d_t1999723980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
