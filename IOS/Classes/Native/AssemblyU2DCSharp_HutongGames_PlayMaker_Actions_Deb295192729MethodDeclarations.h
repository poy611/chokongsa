﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugLog
struct DebugLog_t295192729;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugLog::.ctor()
extern "C"  void DebugLog__ctor_m4075277805 (DebugLog_t295192729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugLog::Reset()
extern "C"  void DebugLog_Reset_m1721710746 (DebugLog_t295192729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugLog::OnEnter()
extern "C"  void DebugLog_OnEnter_m3018492548 (DebugLog_t295192729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
