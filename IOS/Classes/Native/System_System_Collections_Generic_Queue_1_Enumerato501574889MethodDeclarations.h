﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>
struct Queue_1_t3507456673;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato501574889.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m2320345811_gshared (Enumerator_t501574889 * __this, Queue_1_t3507456673 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m2320345811(__this, ___q0, method) ((  void (*) (Enumerator_t501574889 *, Queue_1_t3507456673 *, const MethodInfo*))Enumerator__ctor_m2320345811_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1634881230_gshared (Enumerator_t501574889 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1634881230(__this, method) ((  void (*) (Enumerator_t501574889 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1634881230_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1268686906_gshared (Enumerator_t501574889 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1268686906(__this, method) ((  Il2CppObject * (*) (Enumerator_t501574889 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1268686906_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::Dispose()
extern "C"  void Enumerator_Dispose_m643858921_gshared (Enumerator_t501574889 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m643858921(__this, method) ((  void (*) (Enumerator_t501574889 *, const MethodInfo*))Enumerator_Dispose_m643858921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m704363814_gshared (Enumerator_t501574889 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m704363814(__this, method) ((  bool (*) (Enumerator_t501574889 *, const MethodInfo*))Enumerator_MoveNext_m704363814_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::get_Current()
extern "C"  Point_t1271214244  Enumerator_get_Current_m160354393_gshared (Enumerator_t501574889 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m160354393(__this, method) ((  Point_t1271214244  (*) (Enumerator_t501574889 *, const MethodInfo*))Enumerator_get_Current_m160354393_gshared)(__this, method)
