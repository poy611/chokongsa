﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct GUILayoutBeginScrollView_t818578494;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::.ctor()
extern "C"  void GUILayoutBeginScrollView__ctor_m4157778920 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::Reset()
extern "C"  void GUILayoutBeginScrollView_Reset_m1804211861 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::OnGUI()
extern "C"  void GUILayoutBeginScrollView_OnGUI_m3653177570 (GUILayoutBeginScrollView_t818578494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
