﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey9A
struct U3CRematchU3Ec__AnonStorey9A_t3539402195;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey9A::.ctor()
extern "C"  void U3CRematchU3Ec__AnonStorey9A__ctor_m494290472 (U3CRematchU3Ec__AnonStorey9A_t3539402195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey9A::<>m__89(System.Boolean)
extern "C"  void U3CRematchU3Ec__AnonStorey9A_U3CU3Em__89_m4281821097 (U3CRematchU3Ec__AnonStorey9A_t3539402195 * __this, bool ___failed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey9A::<>m__8A(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CRematchU3Ec__AnonStorey9A_U3CU3Em__8A_m4168654735 (U3CRematchU3Ec__AnonStorey9A_t3539402195 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey9A::<>m__93(GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch)
extern "C"  void U3CRematchU3Ec__AnonStorey9A_U3CU3Em__93_m214331866 (U3CRematchU3Ec__AnonStorey9A_t3539402195 * __this, int32_t ___status0, TurnBasedMatch_t3573041681 * ___m1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
