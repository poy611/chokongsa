﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringSwitch
struct  StringSwitch_t3661243597  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringSwitch::stringVariable
	FsmString_t952858651 * ___stringVariable_11;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.StringSwitch::compareTo
	FsmStringU5BU5D_t2523845914* ___compareTo_12;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.StringSwitch::sendEvent
	FsmEventU5BU5D_t2862142229* ___sendEvent_13;
	// System.Boolean HutongGames.PlayMaker.Actions.StringSwitch::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_stringVariable_11() { return static_cast<int32_t>(offsetof(StringSwitch_t3661243597, ___stringVariable_11)); }
	inline FsmString_t952858651 * get_stringVariable_11() const { return ___stringVariable_11; }
	inline FsmString_t952858651 ** get_address_of_stringVariable_11() { return &___stringVariable_11; }
	inline void set_stringVariable_11(FsmString_t952858651 * value)
	{
		___stringVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariable_11, value);
	}

	inline static int32_t get_offset_of_compareTo_12() { return static_cast<int32_t>(offsetof(StringSwitch_t3661243597, ___compareTo_12)); }
	inline FsmStringU5BU5D_t2523845914* get_compareTo_12() const { return ___compareTo_12; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_compareTo_12() { return &___compareTo_12; }
	inline void set_compareTo_12(FsmStringU5BU5D_t2523845914* value)
	{
		___compareTo_12 = value;
		Il2CppCodeGenWriteBarrier(&___compareTo_12, value);
	}

	inline static int32_t get_offset_of_sendEvent_13() { return static_cast<int32_t>(offsetof(StringSwitch_t3661243597, ___sendEvent_13)); }
	inline FsmEventU5BU5D_t2862142229* get_sendEvent_13() const { return ___sendEvent_13; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_sendEvent_13() { return &___sendEvent_13; }
	inline void set_sendEvent_13(FsmEventU5BU5D_t2862142229* value)
	{
		___sendEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(StringSwitch_t3661243597, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
