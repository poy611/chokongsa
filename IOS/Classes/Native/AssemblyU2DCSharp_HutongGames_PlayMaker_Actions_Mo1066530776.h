﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas595986710.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MoveObject
struct  MoveObject_t1066530776  : public EaseFsmAction_t595986710
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MoveObject::objectToMove
	FsmOwnerDefault_t251897112 * ___objectToMove_32;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MoveObject::destination
	FsmGameObject_t1697147867 * ___destination_33;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::fromValue
	FsmVector3_t533912882 * ___fromValue_34;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::toVector
	FsmVector3_t533912882 * ___toVector_35;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::fromVector
	FsmVector3_t533912882 * ___fromVector_36;
	// System.Boolean HutongGames.PlayMaker.Actions.MoveObject::finishInNextStep
	bool ___finishInNextStep_37;

public:
	inline static int32_t get_offset_of_objectToMove_32() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___objectToMove_32)); }
	inline FsmOwnerDefault_t251897112 * get_objectToMove_32() const { return ___objectToMove_32; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_objectToMove_32() { return &___objectToMove_32; }
	inline void set_objectToMove_32(FsmOwnerDefault_t251897112 * value)
	{
		___objectToMove_32 = value;
		Il2CppCodeGenWriteBarrier(&___objectToMove_32, value);
	}

	inline static int32_t get_offset_of_destination_33() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___destination_33)); }
	inline FsmGameObject_t1697147867 * get_destination_33() const { return ___destination_33; }
	inline FsmGameObject_t1697147867 ** get_address_of_destination_33() { return &___destination_33; }
	inline void set_destination_33(FsmGameObject_t1697147867 * value)
	{
		___destination_33 = value;
		Il2CppCodeGenWriteBarrier(&___destination_33, value);
	}

	inline static int32_t get_offset_of_fromValue_34() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___fromValue_34)); }
	inline FsmVector3_t533912882 * get_fromValue_34() const { return ___fromValue_34; }
	inline FsmVector3_t533912882 ** get_address_of_fromValue_34() { return &___fromValue_34; }
	inline void set_fromValue_34(FsmVector3_t533912882 * value)
	{
		___fromValue_34 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_34, value);
	}

	inline static int32_t get_offset_of_toVector_35() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___toVector_35)); }
	inline FsmVector3_t533912882 * get_toVector_35() const { return ___toVector_35; }
	inline FsmVector3_t533912882 ** get_address_of_toVector_35() { return &___toVector_35; }
	inline void set_toVector_35(FsmVector3_t533912882 * value)
	{
		___toVector_35 = value;
		Il2CppCodeGenWriteBarrier(&___toVector_35, value);
	}

	inline static int32_t get_offset_of_fromVector_36() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___fromVector_36)); }
	inline FsmVector3_t533912882 * get_fromVector_36() const { return ___fromVector_36; }
	inline FsmVector3_t533912882 ** get_address_of_fromVector_36() { return &___fromVector_36; }
	inline void set_fromVector_36(FsmVector3_t533912882 * value)
	{
		___fromVector_36 = value;
		Il2CppCodeGenWriteBarrier(&___fromVector_36, value);
	}

	inline static int32_t get_offset_of_finishInNextStep_37() { return static_cast<int32_t>(offsetof(MoveObject_t1066530776, ___finishInNextStep_37)); }
	inline bool get_finishInNextStep_37() const { return ___finishInNextStep_37; }
	inline bool* get_address_of_finishInNextStep_37() { return &___finishInNextStep_37; }
	inline void set_finishInNextStep_37(bool value)
	{
		___finishInNextStep_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
