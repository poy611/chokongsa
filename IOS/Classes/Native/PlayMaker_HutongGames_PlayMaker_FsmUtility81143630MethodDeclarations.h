﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.UTF8Encoding
struct UTF8Encoding_t2817869802;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t3757199647;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.IO.Stream
struct Stream_t1561764144;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"

// System.Text.UTF8Encoding HutongGames.PlayMaker.FsmUtility::get_Encoding()
extern "C"  UTF8Encoding_t2817869802 * FsmUtility_get_Encoding_m414813462 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmUtility::GetVariableType(HutongGames.PlayMaker.INamedVariable)
extern "C"  int32_t FsmUtility_GetVariableType_m1423105976 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmUtility::GetVariableRealType(HutongGames.PlayMaker.VariableType)
extern "C"  Type_t * FsmUtility_GetVariableRealType_m1008140785 (Il2CppObject * __this /* static, unused */, int32_t ___variableType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmUtility::GetEnum(System.Type,System.Int32)
extern "C"  Il2CppObject * FsmUtility_GetEnum_m3697689283 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, int32_t ___enumValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmEventToByteArray(HutongGames.PlayMaker.FsmEvent)
extern "C"  Il2CppObject* FsmUtility_FsmEventToByteArray_m2731710352 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmFloatToByteArray(HutongGames.PlayMaker.FsmFloat)
extern "C"  Il2CppObject* FsmUtility_FsmFloatToByteArray_m2877664012 (Il2CppObject * __this /* static, unused */, FsmFloat_t2134102846 * ___fsmFloat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmIntToByteArray(HutongGames.PlayMaker.FsmInt)
extern "C"  Il2CppObject* FsmUtility_FsmIntToByteArray_m3166919078 (Il2CppObject * __this /* static, unused */, FsmInt_t1596138449 * ___fsmInt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmBoolToByteArray(HutongGames.PlayMaker.FsmBool)
extern "C"  Il2CppObject* FsmUtility_FsmBoolToByteArray_m616055424 (Il2CppObject * __this /* static, unused */, FsmBool_t1075959796 * ___fsmBool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmVector2ToByteArray(HutongGames.PlayMaker.FsmVector2)
extern "C"  Il2CppObject* FsmUtility_FsmVector2ToByteArray_m2561394790 (Il2CppObject * __this /* static, unused */, FsmVector2_t533912881 * ___fsmVector20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmVector3ToByteArray(HutongGames.PlayMaker.FsmVector3)
extern "C"  Il2CppObject* FsmUtility_FsmVector3ToByteArray_m1378047524 (Il2CppObject * __this /* static, unused */, FsmVector3_t533912882 * ___fsmVector30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmRectToByteArray(HutongGames.PlayMaker.FsmRect)
extern "C"  Il2CppObject* FsmUtility_FsmRectToByteArray_m3116157248 (Il2CppObject * __this /* static, unused */, FsmRect_t1076426478 * ___fsmRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmQuaternionToByteArray(HutongGames.PlayMaker.FsmQuaternion)
extern "C"  Il2CppObject* FsmUtility_FsmQuaternionToByteArray_m2495944064 (Il2CppObject * __this /* static, unused */, FsmQuaternion_t3871136040 * ___fsmQuaternion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmColorToByteArray(HutongGames.PlayMaker.FsmColor)
extern "C"  Il2CppObject* FsmUtility_FsmColorToByteArray_m472511358 (Il2CppObject * __this /* static, unused */, FsmColor_t2131419205 * ___fsmColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::ColorToByteArray(UnityEngine.Color)
extern "C"  Il2CppObject* FsmUtility_ColorToByteArray_m48088331 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector2ToByteArray(UnityEngine.Vector2)
extern "C"  Il2CppObject* FsmUtility_Vector2ToByteArray_m2482590475 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___vector20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector3ToByteArray(UnityEngine.Vector3)
extern "C"  Il2CppObject* FsmUtility_Vector3ToByteArray_m298913579 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___vector30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector4ToByteArray(UnityEngine.Vector4)
extern "C"  Il2CppObject* FsmUtility_Vector4ToByteArray_m2410203979 (Il2CppObject * __this /* static, unused */, Vector4_t4282066567  ___vector40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::RectToByteArray(UnityEngine.Rect)
extern "C"  Il2CppObject* FsmUtility_RectToByteArray_m4168738609 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::QuaternionToByteArray(UnityEngine.Quaternion)
extern "C"  Il2CppObject* FsmUtility_QuaternionToByteArray_m3779338749 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___quaternion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility::StringToByteArray(System.String)
extern "C"  ByteU5BU5D_t4260760469* FsmUtility_StringToByteArray_m1930364708 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::ByteArrayToString(System.Byte[])
extern "C"  String_t* FsmUtility_ByteArrayToString_m4287253324 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::ByteArrayToString(System.Byte[],System.Int32,System.Int32)
extern "C"  String_t* FsmUtility_ByteArrayToString_m4013250988 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmEvent(System.Byte[],System.Int32,System.Int32)
extern "C"  FsmEvent_t2133468028 * FsmUtility_ByteArrayToFsmEvent_m379087355 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmFloat(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmFloat_t2134102846 * FsmUtility_ByteArrayToFsmFloat_m3240375087 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmInt(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmInt_t1596138449 * FsmUtility_ByteArrayToFsmInt_m2592711497 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmBool(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmBool_t1075959796 * FsmUtility_ByteArrayToFsmBool_m3911052367 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.FsmUtility::ByteArrayToColor(System.Byte[],System.Int32)
extern "C"  Color_t4194546905  FsmUtility_ByteArrayToColor_m4067128843 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector2(System.Byte[],System.Int32)
extern "C"  Vector2_t4282066565  FsmUtility_ByteArrayToVector2_m244982603 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmVector2(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmVector2_t533912881 * FsmUtility_ByteArrayToFsmVector2_m1019730697 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector3(System.Byte[],System.Int32)
extern "C"  Vector3_t4282066566  FsmUtility_ByteArrayToVector3_m1721463275 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmVector3(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmVector3_t533912882 * FsmUtility_ByteArrayToFsmVector3_m4117703367 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmRect(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmRect_t1076426478 * FsmUtility_ByteArrayToFsmRect_m2333318159 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmQuaternion(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmQuaternion_t3871136040 * FsmUtility_ByteArrayToFsmQuaternion_m1339542991 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmColor(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmColor_t2131419205 * FsmUtility_ByteArrayToFsmColor_m656267681 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, ByteU5BU5D_t4260760469* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector4(System.Byte[],System.Int32)
extern "C"  Vector4_t4282066567  FsmUtility_ByteArrayToVector4_m3197943947 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmUtility::ByteArrayToRect(System.Byte[],System.Int32)
extern "C"  Rect_t4241904616  FsmUtility_ByteArrayToRect_m3136172123 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HutongGames.PlayMaker.FsmUtility::ByteArrayToQuaternion(System.Byte[],System.Int32)
extern "C"  Quaternion_t1553702882  FsmUtility_ByteArrayToQuaternion_m3143652559 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility::ReadToEnd(System.IO.Stream)
extern "C"  ByteU5BU5D_t4260760469* FsmUtility_ReadToEnd_m2350292794 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::StripNamespace(System.String)
extern "C"  String_t* FsmUtility_StripNamespace_m1772136317 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState)
extern "C"  String_t* FsmUtility_GetPath_m804257822 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction)
extern "C"  String_t* FsmUtility_GetPath_m735465141 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, FsmStateAction_t2366529033 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.String)
extern "C"  String_t* FsmUtility_GetPath_m810620465 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, FsmStateAction_t2366529033 * ___action1, String_t* ___parameter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* FsmUtility_GetFullFsmLabel_m175618885 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(PlayMakerFSM)
extern "C"  String_t* FsmUtility_GetFullFsmLabel_m4153503881 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* FsmUtility_GetFsmLabel_m3730159060 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.FsmUtility::GetOwner(HutongGames.PlayMaker.Fsm)
extern "C"  Object_t3071478659 * FsmUtility_GetOwner_m1165120503 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
