﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey97
struct U3CLeaveU3Ec__AnonStorey97_t2983955214;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey97::.ctor()
extern "C"  void U3CLeaveU3Ec__AnonStorey97__ctor_m618533197 (U3CLeaveU3Ec__AnonStorey97_t2983955214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey97::<>m__86(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CLeaveU3Ec__AnonStorey97_U3CU3Em__86_m3081015477 (U3CLeaveU3Ec__AnonStorey97_t2983955214 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Leave>c__AnonStorey97::<>m__90(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C"  void U3CLeaveU3Ec__AnonStorey97_U3CU3Em__90_m531660602 (U3CLeaveU3Ec__AnonStorey97_t2983955214 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
