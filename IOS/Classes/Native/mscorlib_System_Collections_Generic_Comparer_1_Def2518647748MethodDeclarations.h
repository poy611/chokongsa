﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>
struct DefaultComparer_t2518647748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>::.ctor()
extern "C"  void DefaultComparer__ctor_m2365152386_gshared (DefaultComparer_t2518647748 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2365152386(__this, method) ((  void (*) (DefaultComparer_t2518647748 *, const MethodInfo*))DefaultComparer__ctor_m2365152386_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m624120045_gshared (DefaultComparer_t2518647748 * __this, IntPtr_t ___x0, IntPtr_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m624120045(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2518647748 *, IntPtr_t, IntPtr_t, const MethodInfo*))DefaultComparer_Compare_m624120045_gshared)(__this, ___x0, ___y1, method)
