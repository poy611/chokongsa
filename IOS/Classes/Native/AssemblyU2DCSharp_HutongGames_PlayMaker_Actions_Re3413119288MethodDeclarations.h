﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectOverlaps
struct RectOverlaps_t3413119288;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::.ctor()
extern "C"  void RectOverlaps__ctor_m2258795566 (RectOverlaps_t3413119288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::Reset()
extern "C"  void RectOverlaps_Reset_m4200195803 (RectOverlaps_t3413119288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::OnEnter()
extern "C"  void RectOverlaps_OnEnter_m1135783045 (RectOverlaps_t3413119288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::OnUpdate()
extern "C"  void RectOverlaps_OnUpdate_m4278062686 (RectOverlaps_t3413119288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::DoRectOverlap()
extern "C"  void RectOverlaps_DoRectOverlap_m997423236 (RectOverlaps_t3413119288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.RectOverlaps::Intersect(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectOverlaps_Intersect_m3737812495 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___a0, Rect_t4241904616  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::FlipNegative(UnityEngine.Rect&)
extern "C"  void RectOverlaps_FlipNegative_m4017097743 (Il2CppObject * __this /* static, unused */, Rect_t4241904616 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
