﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DeviceShakeEvent
struct  DeviceShakeEvent_t2461145554  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DeviceShakeEvent::shakeThreshold
	FsmFloat_t2134102846 * ___shakeThreshold_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DeviceShakeEvent::sendEvent
	FsmEvent_t2133468028 * ___sendEvent_12;

public:
	inline static int32_t get_offset_of_shakeThreshold_11() { return static_cast<int32_t>(offsetof(DeviceShakeEvent_t2461145554, ___shakeThreshold_11)); }
	inline FsmFloat_t2134102846 * get_shakeThreshold_11() const { return ___shakeThreshold_11; }
	inline FsmFloat_t2134102846 ** get_address_of_shakeThreshold_11() { return &___shakeThreshold_11; }
	inline void set_shakeThreshold_11(FsmFloat_t2134102846 * value)
	{
		___shakeThreshold_11 = value;
		Il2CppCodeGenWriteBarrier(&___shakeThreshold_11, value);
	}

	inline static int32_t get_offset_of_sendEvent_12() { return static_cast<int32_t>(offsetof(DeviceShakeEvent_t2461145554, ___sendEvent_12)); }
	inline FsmEvent_t2133468028 * get_sendEvent_12() const { return ___sendEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_sendEvent_12() { return &___sendEvent_12; }
	inline void set_sendEvent_12(FsmEvent_t2133468028 * value)
	{
		___sendEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
