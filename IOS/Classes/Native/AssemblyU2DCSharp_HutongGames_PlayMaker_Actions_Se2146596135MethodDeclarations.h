﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SequenceEvent
struct SequenceEvent_t2146596135;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::.ctor()
extern "C"  void SequenceEvent__ctor_m1783674863 (SequenceEvent_t2146596135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::Reset()
extern "C"  void SequenceEvent_Reset_m3725075100 (SequenceEvent_t2146596135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::OnEnter()
extern "C"  void SequenceEvent_OnEnter_m4106288134 (SequenceEvent_t2146596135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::OnUpdate()
extern "C"  void SequenceEvent_OnUpdate_m1874439933 (SequenceEvent_t2146596135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
