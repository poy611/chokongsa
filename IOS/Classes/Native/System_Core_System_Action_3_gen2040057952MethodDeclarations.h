﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen708612388MethodDeclarations.h"

// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m833060884(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2040057952 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2714700114_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2218944014(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2040057952 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m3247580302_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m430916735(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2040057952 *, int32_t, Il2CppObject *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2235337925_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m105258020(__this, ___result0, method) ((  void (*) (Action_3_t2040057952 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m1806693474_gshared)(__this, ___result0, method)
