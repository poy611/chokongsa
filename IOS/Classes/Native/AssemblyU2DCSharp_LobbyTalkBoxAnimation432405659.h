﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// ScoreManager_Lobby
struct ScoreManager_Lobby_t3853030226;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyTalkBoxAnimation
struct  LobbyTalkBoxAnimation_t432405659  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Image LobbyTalkBoxAnimation::ShowmetheCoffee
	Image_t538875265 * ___ShowmetheCoffee_2;
	// ScoreManager_Lobby LobbyTalkBoxAnimation::scoreMng
	ScoreManager_Lobby_t3853030226 * ___scoreMng_3;

public:
	inline static int32_t get_offset_of_ShowmetheCoffee_2() { return static_cast<int32_t>(offsetof(LobbyTalkBoxAnimation_t432405659, ___ShowmetheCoffee_2)); }
	inline Image_t538875265 * get_ShowmetheCoffee_2() const { return ___ShowmetheCoffee_2; }
	inline Image_t538875265 ** get_address_of_ShowmetheCoffee_2() { return &___ShowmetheCoffee_2; }
	inline void set_ShowmetheCoffee_2(Image_t538875265 * value)
	{
		___ShowmetheCoffee_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShowmetheCoffee_2, value);
	}

	inline static int32_t get_offset_of_scoreMng_3() { return static_cast<int32_t>(offsetof(LobbyTalkBoxAnimation_t432405659, ___scoreMng_3)); }
	inline ScoreManager_Lobby_t3853030226 * get_scoreMng_3() const { return ___scoreMng_3; }
	inline ScoreManager_Lobby_t3853030226 ** get_address_of_scoreMng_3() { return &___scoreMng_3; }
	inline void set_scoreMng_3(ScoreManager_Lobby_t3853030226 * value)
	{
		___scoreMng_3 = value;
		Il2CppCodeGenWriteBarrier(&___scoreMng_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
