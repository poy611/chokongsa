﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen24149625MethodDeclarations.h"

// System.Void System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m996634046(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2753664159 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3549080524_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::Invoke(T)
#define Func_2_Invoke_m4246327124(__this, ___arg10, method) ((  IntPtr_t (*) (Func_2_t2753664159 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))Func_2_Invoke_m849983126_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2012515715(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2753664159 *, MultiplayerParticipant_t3337232325 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3190407365_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3777171712(__this, ___result0, method) ((  IntPtr_t (*) (Func_2_t2753664159 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1691592446_gshared)(__this, ___result0, method)
