﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerGetProperties
struct  MasterServerGetProperties_t3166189106  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetProperties::ipAddress
	FsmString_t952858651 * ___ipAddress_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetProperties::port
	FsmInt_t1596138449 * ___port_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetProperties::updateRate
	FsmInt_t1596138449 * ___updateRate_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetProperties::dedicatedServer
	FsmBool_t1075959796 * ___dedicatedServer_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetProperties::isDedicatedServerEvent
	FsmEvent_t2133468028 * ___isDedicatedServerEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MasterServerGetProperties::isNotDedicatedServerEvent
	FsmEvent_t2133468028 * ___isNotDedicatedServerEvent_16;

public:
	inline static int32_t get_offset_of_ipAddress_11() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___ipAddress_11)); }
	inline FsmString_t952858651 * get_ipAddress_11() const { return ___ipAddress_11; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_11() { return &___ipAddress_11; }
	inline void set_ipAddress_11(FsmString_t952858651 * value)
	{
		___ipAddress_11 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_11, value);
	}

	inline static int32_t get_offset_of_port_12() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___port_12)); }
	inline FsmInt_t1596138449 * get_port_12() const { return ___port_12; }
	inline FsmInt_t1596138449 ** get_address_of_port_12() { return &___port_12; }
	inline void set_port_12(FsmInt_t1596138449 * value)
	{
		___port_12 = value;
		Il2CppCodeGenWriteBarrier(&___port_12, value);
	}

	inline static int32_t get_offset_of_updateRate_13() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___updateRate_13)); }
	inline FsmInt_t1596138449 * get_updateRate_13() const { return ___updateRate_13; }
	inline FsmInt_t1596138449 ** get_address_of_updateRate_13() { return &___updateRate_13; }
	inline void set_updateRate_13(FsmInt_t1596138449 * value)
	{
		___updateRate_13 = value;
		Il2CppCodeGenWriteBarrier(&___updateRate_13, value);
	}

	inline static int32_t get_offset_of_dedicatedServer_14() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___dedicatedServer_14)); }
	inline FsmBool_t1075959796 * get_dedicatedServer_14() const { return ___dedicatedServer_14; }
	inline FsmBool_t1075959796 ** get_address_of_dedicatedServer_14() { return &___dedicatedServer_14; }
	inline void set_dedicatedServer_14(FsmBool_t1075959796 * value)
	{
		___dedicatedServer_14 = value;
		Il2CppCodeGenWriteBarrier(&___dedicatedServer_14, value);
	}

	inline static int32_t get_offset_of_isDedicatedServerEvent_15() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___isDedicatedServerEvent_15)); }
	inline FsmEvent_t2133468028 * get_isDedicatedServerEvent_15() const { return ___isDedicatedServerEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_isDedicatedServerEvent_15() { return &___isDedicatedServerEvent_15; }
	inline void set_isDedicatedServerEvent_15(FsmEvent_t2133468028 * value)
	{
		___isDedicatedServerEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___isDedicatedServerEvent_15, value);
	}

	inline static int32_t get_offset_of_isNotDedicatedServerEvent_16() { return static_cast<int32_t>(offsetof(MasterServerGetProperties_t3166189106, ___isNotDedicatedServerEvent_16)); }
	inline FsmEvent_t2133468028 * get_isNotDedicatedServerEvent_16() const { return ___isNotDedicatedServerEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotDedicatedServerEvent_16() { return &___isNotDedicatedServerEvent_16; }
	inline void set_isNotDedicatedServerEvent_16(FsmEvent_t2133468028 * value)
	{
		___isNotDedicatedServerEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___isNotDedicatedServerEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
