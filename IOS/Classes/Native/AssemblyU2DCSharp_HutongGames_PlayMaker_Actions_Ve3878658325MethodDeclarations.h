﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3LowPassFilter
struct Vector3LowPassFilter_t3878658325;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3LowPassFilter::.ctor()
extern "C"  void Vector3LowPassFilter__ctor_m336911601 (Vector3LowPassFilter_t3878658325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3LowPassFilter::Reset()
extern "C"  void Vector3LowPassFilter_Reset_m2278311838 (Vector3LowPassFilter_t3878658325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3LowPassFilter::OnEnter()
extern "C"  void Vector3LowPassFilter_OnEnter_m1041229960 (Vector3LowPassFilter_t3878658325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3LowPassFilter::OnUpdate()
extern "C"  void Vector3LowPassFilter_OnUpdate_m1346917051 (Vector3LowPassFilter_t3878658325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
