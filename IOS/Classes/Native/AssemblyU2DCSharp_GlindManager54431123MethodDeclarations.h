﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlindManager
struct GlindManager_t54431123;

#include "codegen/il2cpp-codegen.h"

// System.Void GlindManager::.ctor()
extern "C"  void GlindManager__ctor_m63175272 (GlindManager_t54431123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlindManager::Start()
extern "C"  void GlindManager_Start_m3305280360 (GlindManager_t54431123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlindManager::Update()
extern "C"  void GlindManager_Update_m3685295525 (GlindManager_t54431123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
