﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl2761661122.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4215973110_gshared (Nullable_1_t2845787645 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4215973110(__this, ___value0, method) ((  void (*) (Nullable_1_t2845787645 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4215973110_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2331388434_gshared (Nullable_1_t2845787645 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2331388434(__this, method) ((  bool (*) (Nullable_1_t2845787645 *, const MethodInfo*))Nullable_1_get_HasValue_m2331388434_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3748828655_gshared (Nullable_1_t2845787645 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3748828655(__this, method) ((  int32_t (*) (Nullable_1_t2845787645 *, const MethodInfo*))Nullable_1_get_Value_m3748828655_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1933655575_gshared (Nullable_1_t2845787645 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1933655575(__this, ___other0, method) ((  bool (*) (Nullable_1_t2845787645 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1933655575_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3275332936_gshared (Nullable_1_t2845787645 * __this, Nullable_1_t2845787645  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3275332936(__this, ___other0, method) ((  bool (*) (Nullable_1_t2845787645 *, Nullable_1_t2845787645 , const MethodInfo*))Nullable_1_Equals_m3275332936_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1790767227_gshared (Nullable_1_t2845787645 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1790767227(__this, method) ((  int32_t (*) (Nullable_1_t2845787645 *, const MethodInfo*))Nullable_1_GetHashCode_m1790767227_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m755395385_gshared (Nullable_1_t2845787645 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m755395385(__this, method) ((  int32_t (*) (Nullable_1_t2845787645 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m755395385_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m85485802_gshared (Nullable_1_t2845787645 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m85485802(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2845787645 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m85485802_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m192155435_gshared (Nullable_1_t2845787645 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m192155435(__this, method) ((  String_t* (*) (Nullable_1_t2845787645 *, const MethodInfo*))Nullable_1_ToString_m192155435_gshared)(__this, method)
