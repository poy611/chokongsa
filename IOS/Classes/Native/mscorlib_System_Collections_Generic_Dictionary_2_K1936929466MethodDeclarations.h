﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.ReadType>
struct Dictionary_2_t1321993412;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1936929466.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1569088848_gshared (Enumerator_t1936929466 * __this, Dictionary_2_t1321993412 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1569088848(__this, ___host0, method) ((  void (*) (Enumerator_t1936929466 *, Dictionary_2_t1321993412 *, const MethodInfo*))Enumerator__ctor_m1569088848_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1398055515_gshared (Enumerator_t1936929466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1398055515(__this, method) ((  Il2CppObject * (*) (Enumerator_t1936929466 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1398055515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m454850917_gshared (Enumerator_t1936929466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m454850917(__this, method) ((  void (*) (Enumerator_t1936929466 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m454850917_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::Dispose()
extern "C"  void Enumerator_Dispose_m3845464050_gshared (Enumerator_t1936929466 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3845464050(__this, method) ((  void (*) (Enumerator_t1936929466 *, const MethodInfo*))Enumerator_Dispose_m3845464050_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m356067861_gshared (Enumerator_t1936929466 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m356067861(__this, method) ((  bool (*) (Enumerator_t1936929466 *, const MethodInfo*))Enumerator_MoveNext_m356067861_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2827460067_gshared (Enumerator_t1936929466 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2827460067(__this, method) ((  Il2CppObject * (*) (Enumerator_t1936929466 *, const MethodInfo*))Enumerator_get_Current_m2827460067_gshared)(__this, method)
