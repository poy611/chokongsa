﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct  GameObjectIsNull_t2162669226  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsNull::gameObject
	FsmGameObject_t1697147867 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNull
	FsmEvent_t2133468028 * ___isNull_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNotNull
	FsmEvent_t2133468028 * ___isNotNull_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsNull::storeResult
	FsmBool_t1075959796 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsNull::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___gameObject_11)); }
	inline FsmGameObject_t1697147867 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t1697147867 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isNull_12() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___isNull_12)); }
	inline FsmEvent_t2133468028 * get_isNull_12() const { return ___isNull_12; }
	inline FsmEvent_t2133468028 ** get_address_of_isNull_12() { return &___isNull_12; }
	inline void set_isNull_12(FsmEvent_t2133468028 * value)
	{
		___isNull_12 = value;
		Il2CppCodeGenWriteBarrier(&___isNull_12, value);
	}

	inline static int32_t get_offset_of_isNotNull_13() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___isNotNull_13)); }
	inline FsmEvent_t2133468028 * get_isNotNull_13() const { return ___isNotNull_13; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotNull_13() { return &___isNotNull_13; }
	inline void set_isNotNull_13(FsmEvent_t2133468028 * value)
	{
		___isNotNull_13 = value;
		Il2CppCodeGenWriteBarrier(&___isNotNull_13, value);
	}

	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___storeResult_14)); }
	inline FsmBool_t1075959796 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmBool_t1075959796 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t2162669226, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
