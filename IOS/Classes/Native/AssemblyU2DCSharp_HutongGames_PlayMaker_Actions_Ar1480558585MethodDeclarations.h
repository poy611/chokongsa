﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayForEach
struct ArrayForEach_t1480558585;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::.ctor()
extern "C"  void ArrayForEach__ctor_m4164292237 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Reset()
extern "C"  void ArrayForEach_Reset_m1810725178 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Awake()
extern "C"  void ArrayForEach_Awake_m106930160 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnEnter()
extern "C"  void ArrayForEach_OnEnter_m2662015780 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnUpdate()
extern "C"  void ArrayForEach_OnUpdate_m51669919 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnFixedUpdate()
extern "C"  void ArrayForEach_OnFixedUpdate_m996528169 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnLateUpdate()
extern "C"  void ArrayForEach_OnLateUpdate_m4160309733 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartNextFsm()
extern "C"  void ArrayForEach_StartNextFsm_m2285137634 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartFsm()
extern "C"  void ArrayForEach_StartFsm_m2952795157 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::DoStartFsm()
extern "C"  void ArrayForEach_DoStartFsm_m2189303168 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::CheckIfFinished()
extern "C"  void ArrayForEach_CheckIfFinished_m172419394 (ArrayForEach_t1480558585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
