﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Object
struct Il2CppObject;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t926437290;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t3848485703;
// System.Collections.IList
struct IList_t1751339649;
// System.Array
struct Il2CppArray;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"

// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsDictionaryType(System.Type)
extern "C"  bool CollectionUtils_IsDictionaryType_m2934797035 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type)
extern "C"  ConstructorInfo_t4136801618 * CollectionUtils_ResolveEnumerableCollectionConstructor_m2141954997 (Il2CppObject * __this /* static, unused */, Type_t * ___collectionType0, Type_t * ___collectionItemType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::Contains(System.Collections.IEnumerable,System.Object,System.Collections.IEqualityComparer)
extern "C"  bool CollectionUtils_Contains_m2108405094 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, Il2CppObject * ___value1, Il2CppObject * ___comparer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Int32> Newtonsoft.Json.Utilities.CollectionUtils::GetDimensions(System.Collections.IList,System.Int32)
extern "C"  Il2CppObject* CollectionUtils_GetDimensions_m2027594952 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___values0, int32_t ___dimensionsCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.CollectionUtils::CopyFromJaggedToMultidimensionalArray(System.Collections.IList,System.Array,System.Int32[])
extern "C"  void CollectionUtils_CopyFromJaggedToMultidimensionalArray_m13756687 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___values0, Il2CppArray * ___multidimensionalArray1, Int32U5BU5D_t3230847821* ___indices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.CollectionUtils::JaggedArrayGetValue(System.Collections.IList,System.Int32[])
extern "C"  Il2CppObject * CollectionUtils_JaggedArrayGetValue_m251698829 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___values0, Int32U5BU5D_t3230847821* ___indices1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array Newtonsoft.Json.Utilities.CollectionUtils::ToMultidimensionalArray(System.Collections.IList,System.Type,System.Int32)
extern "C"  Il2CppArray * CollectionUtils_ToMultidimensionalArray_m96124944 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___values0, Type_t * ___type1, int32_t ___rank2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
