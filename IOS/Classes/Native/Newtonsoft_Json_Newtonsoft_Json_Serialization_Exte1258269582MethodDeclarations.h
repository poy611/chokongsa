﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_t1258269582;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::.ctor(System.Object,System.IntPtr)
extern "C"  void ExtensionDataSetter__ctor_m1651215415 (ExtensionDataSetter_t1258269582 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::Invoke(System.Object,System.String,System.Object)
extern "C"  void ExtensionDataSetter_Invoke_m1197004141 (ExtensionDataSetter_t1258269582 * __this, Il2CppObject * ___o0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataSetter::BeginInvoke(System.Object,System.String,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExtensionDataSetter_BeginInvoke_m1457854530 (ExtensionDataSetter_t1258269582 * __this, Il2CppObject * ___o0, String_t* ___key1, Il2CppObject * ___value2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::EndInvoke(System.IAsyncResult)
extern "C"  void ExtensionDataSetter_EndInvoke_m2430841287 (ExtensionDataSetter_t1258269582 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
