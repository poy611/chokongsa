﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3124613658;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>
struct IDictionary_2_t3468504184;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Reflection.MethodBase
struct MethodBase_t318515428;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"

// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::get_Creator()
extern "C"  ObjectConstructor_1_t2948332186 * ReflectionObject_get_Creator_m2911534260 (ReflectionObject_t3124613658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionObject::set_Creator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern "C"  void ReflectionObject_set_Creator_m1602307373 (ReflectionObject_t3124613658 * __this, ObjectConstructor_1_t2948332186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::get_Members()
extern "C"  Il2CppObject* ReflectionObject_get_Members_m1028611368 (ReflectionObject_t3124613658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionObject::set_Members(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>)
extern "C"  void ReflectionObject_set_Members_m1877907763 (ReflectionObject_t3124613658 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionObject::.ctor()
extern "C"  void ReflectionObject__ctor_m511425059 (ReflectionObject_t3124613658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionObject::GetValue(System.Object,System.String)
extern "C"  Il2CppObject * ReflectionObject_GetValue_m1998683839 (ReflectionObject_t3124613658 * __this, Il2CppObject * ___target0, String_t* ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionObject::GetType(System.String)
extern "C"  Type_t * ReflectionObject_GetType_m3167460427 (ReflectionObject_t3124613658 * __this, String_t* ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.String[])
extern "C"  ReflectionObject_t3124613658 * ReflectionObject_Create_m3355985926 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, StringU5BU5D_t4054002952* ___memberNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.Reflection.MethodBase,System.String[])
extern "C"  ReflectionObject_t3124613658 * ReflectionObject_Create_m1865659230 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, MethodBase_t318515428 * ___creator1, StringU5BU5D_t4054002952* ___memberNames2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
