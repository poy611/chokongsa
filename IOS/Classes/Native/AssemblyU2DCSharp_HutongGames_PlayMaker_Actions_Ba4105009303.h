﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseLogAction
struct  BaseLogAction_t4105009303  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.BaseLogAction::sendToUnityLog
	bool ___sendToUnityLog_11;

public:
	inline static int32_t get_offset_of_sendToUnityLog_11() { return static_cast<int32_t>(offsetof(BaseLogAction_t4105009303, ___sendToUnityLog_11)); }
	inline bool get_sendToUnityLog_11() const { return ___sendToUnityLog_11; }
	inline bool* get_address_of_sendToUnityLog_11() { return &___sendToUnityLog_11; }
	inline void set_sendToUnityLog_11(bool value)
	{
		___sendToUnityLog_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
