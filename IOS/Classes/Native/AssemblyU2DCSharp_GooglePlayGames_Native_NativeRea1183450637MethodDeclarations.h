﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6A
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6A::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey6A__ctor_m1557382446 (U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
