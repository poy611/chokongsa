﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResponseInfo
struct ResponseInfo_t1603221679;

#include "codegen/il2cpp-codegen.h"

// System.Void ResponseInfo::.ctor()
extern "C"  void ResponseInfo__ctor_m1208749772 (ResponseInfo_t1603221679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
