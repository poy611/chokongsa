﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// System.Func`2<System.String,System.String>
struct Func_2_t3261292977;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// Newtonsoft.Json.Utilities.IWrappedDictionary
struct IWrappedDictionary_t1790279202;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionary
struct IDictionary_t537317817;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "mscorlib_System_Object4170816371.h"

// System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyResolver()
extern "C"  Func_2_t3261292977 * JsonDictionaryContract_get_DictionaryKeyResolver_m3050650853 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyResolver(System.Func`2<System.String,System.String>)
extern "C"  void JsonDictionaryContract_set_DictionaryKeyResolver_m706810050 (JsonDictionaryContract_t989352188 * __this, Func_2_t3261292977 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyType()
extern "C"  Type_t * JsonDictionaryContract_get_DictionaryKeyType_m947193249 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyType(System.Type)
extern "C"  void JsonDictionaryContract_set_DictionaryKeyType_m503944082 (JsonDictionaryContract_t989352188 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryValueType()
extern "C"  Type_t * JsonDictionaryContract_get_DictionaryValueType_m268195187 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryValueType(System.Type)
extern "C"  void JsonDictionaryContract_set_DictionaryValueType_m2396511744 (JsonDictionaryContract_t989352188 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::get_KeyContract()
extern "C"  JsonContract_t1328848902 * JsonDictionaryContract_get_KeyContract_m1521558723 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_KeyContract(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonDictionaryContract_set_KeyContract_m2394589672 (JsonDictionaryContract_t989352188 * __this, JsonContract_t1328848902 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ShouldCreateWrapper()
extern "C"  bool JsonDictionaryContract_get_ShouldCreateWrapper_m3773024786 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_ShouldCreateWrapper(System.Boolean)
extern "C"  void JsonDictionaryContract_set_ShouldCreateWrapper_m747415615 (JsonDictionaryContract_t989352188 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ParameterizedCreator()
extern "C"  ObjectConstructor_1_t2948332186 * JsonDictionaryContract_get_ParameterizedCreator_m3033285805 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_OverrideCreator()
extern "C"  ObjectConstructor_1_t2948332186 * JsonDictionaryContract_get_OverrideCreator_m3581038824 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern "C"  void JsonDictionaryContract_set_OverrideCreator_m928991585 (JsonDictionaryContract_t989352188 * __this, ObjectConstructor_1_t2948332186 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreator()
extern "C"  bool JsonDictionaryContract_get_HasParameterizedCreator_m555121467 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_HasParameterizedCreator(System.Boolean)
extern "C"  void JsonDictionaryContract_set_HasParameterizedCreator_m507155112 (JsonDictionaryContract_t989352188 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreatorInternal()
extern "C"  bool JsonDictionaryContract_get_HasParameterizedCreatorInternal_m3854338264 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::.ctor(System.Type)
extern "C"  void JsonDictionaryContract__ctor_m1986205654 (JsonDictionaryContract_t989352188 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.IWrappedDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateWrapper(System.Object)
extern "C"  Il2CppObject * JsonDictionaryContract_CreateWrapper_m1350256996 (JsonDictionaryContract_t989352188 * __this, Il2CppObject * ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateTemporaryDictionary()
extern "C"  Il2CppObject * JsonDictionaryContract_CreateTemporaryDictionary_m2219945568 (JsonDictionaryContract_t989352188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
