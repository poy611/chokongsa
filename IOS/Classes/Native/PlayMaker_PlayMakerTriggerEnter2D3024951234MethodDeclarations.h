﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerEnter2D
struct PlayMakerTriggerEnter2D_t3024951234;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void PlayMakerTriggerEnter2D::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerEnter2D_OnTriggerEnter2D_m2751469681 (PlayMakerTriggerEnter2D_t3024951234 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerEnter2D::.ctor()
extern "C"  void PlayMakerTriggerEnter2D__ctor_m239525863 (PlayMakerTriggerEnter2D_t3024951234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
