﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"

// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse_m3393489795 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetVariant_m4181722918 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___variant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_AddPlayerToInvite_m2963966479 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___player_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Construct()
extern "C"  IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Construct_m2904048914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetExclusiveBitMask_m1450326527 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___exclusive_bit_mask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers_m30375497 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___maximum_automatching_players1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Create_m3121774112 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers_m1511535515 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_automatching_players1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Dispose_m1368534604 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
