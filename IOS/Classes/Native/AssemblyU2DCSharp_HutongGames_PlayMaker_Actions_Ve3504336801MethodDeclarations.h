﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Multiply
struct Vector2Multiply_t3504336801;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::.ctor()
extern "C"  void Vector2Multiply__ctor_m3982707893 (Vector2Multiply_t3504336801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::Reset()
extern "C"  void Vector2Multiply_Reset_m1629140834 (Vector2Multiply_t3504336801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::OnEnter()
extern "C"  void Vector2Multiply_OnEnter_m4253120332 (Vector2Multiply_t3504336801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::OnUpdate()
extern "C"  void Vector2Multiply_OnUpdate_m2131270775 (Vector2Multiply_t3504336801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
