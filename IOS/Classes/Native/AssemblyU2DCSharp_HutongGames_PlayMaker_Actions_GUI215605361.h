﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEmailField
struct  GUILayoutEmailField_t215605361  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutEmailField::text
	FsmString_t952858651 * ___text_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutEmailField::maxLength
	FsmInt_t1596138449 * ___maxLength_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutEmailField::style
	FsmString_t952858651 * ___style_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutEmailField::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutEmailField::valid
	FsmBool_t1075959796 * ___valid_17;

public:
	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t215605361, ___text_13)); }
	inline FsmString_t952858651 * get_text_13() const { return ___text_13; }
	inline FsmString_t952858651 ** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(FsmString_t952858651 * value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier(&___text_13, value);
	}

	inline static int32_t get_offset_of_maxLength_14() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t215605361, ___maxLength_14)); }
	inline FsmInt_t1596138449 * get_maxLength_14() const { return ___maxLength_14; }
	inline FsmInt_t1596138449 ** get_address_of_maxLength_14() { return &___maxLength_14; }
	inline void set_maxLength_14(FsmInt_t1596138449 * value)
	{
		___maxLength_14 = value;
		Il2CppCodeGenWriteBarrier(&___maxLength_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t215605361, ___style_15)); }
	inline FsmString_t952858651 * get_style_15() const { return ___style_15; }
	inline FsmString_t952858651 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t952858651 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_changedEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t215605361, ___changedEvent_16)); }
	inline FsmEvent_t2133468028 * get_changedEvent_16() const { return ___changedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_16() { return &___changedEvent_16; }
	inline void set_changedEvent_16(FsmEvent_t2133468028 * value)
	{
		___changedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_16, value);
	}

	inline static int32_t get_offset_of_valid_17() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t215605361, ___valid_17)); }
	inline FsmBool_t1075959796 * get_valid_17() const { return ___valid_17; }
	inline FsmBool_t1075959796 ** get_address_of_valid_17() { return &___valid_17; }
	inline void set_valid_17(FsmBool_t1075959796 * value)
	{
		___valid_17 = value;
		Il2CppCodeGenWriteBarrier(&___valid_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
