﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerParticleCollision
struct PlayMakerParticleCollision_t2428451804;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void PlayMakerParticleCollision::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void PlayMakerParticleCollision_OnParticleCollision_m1753553028 (PlayMakerParticleCollision_t2428451804 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerParticleCollision::.ctor()
extern "C"  void PlayMakerParticleCollision__ctor_m3278577025 (PlayMakerParticleCollision_t2428451804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
