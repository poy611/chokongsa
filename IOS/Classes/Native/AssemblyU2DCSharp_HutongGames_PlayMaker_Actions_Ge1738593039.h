﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetName
struct  GetName_t1738593039  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetName::gameObject
	FsmGameObject_t1697147867 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetName::storeName
	FsmString_t952858651 * ___storeName_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetName::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GetName_t1738593039, ___gameObject_11)); }
	inline FsmGameObject_t1697147867 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t1697147867 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_storeName_12() { return static_cast<int32_t>(offsetof(GetName_t1738593039, ___storeName_12)); }
	inline FsmString_t952858651 * get_storeName_12() const { return ___storeName_12; }
	inline FsmString_t952858651 ** get_address_of_storeName_12() { return &___storeName_12; }
	inline void set_storeName_12(FsmString_t952858651 * value)
	{
		___storeName_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeName_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetName_t1738593039, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
