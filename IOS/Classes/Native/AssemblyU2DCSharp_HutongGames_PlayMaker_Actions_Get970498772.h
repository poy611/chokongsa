﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTouchCount
struct  GetTouchCount_t970498772  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchCount::storeCount
	FsmInt_t1596138449 * ___storeCount_11;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTouchCount::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_storeCount_11() { return static_cast<int32_t>(offsetof(GetTouchCount_t970498772, ___storeCount_11)); }
	inline FsmInt_t1596138449 * get_storeCount_11() const { return ___storeCount_11; }
	inline FsmInt_t1596138449 ** get_address_of_storeCount_11() { return &___storeCount_11; }
	inline void set_storeCount_11(FsmInt_t1596138449 * value)
	{
		___storeCount_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeCount_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(GetTouchCount_t970498772, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
