﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37
struct U3CLoadScoresU3Ec__AnonStorey37_t2259335463;
// GooglePlayGames.BasicApi.LeaderboardScoreData
struct LeaderboardScoreData_t4006482697;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb4006482697.h"

// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37::.ctor()
extern "C"  void U3CLoadScoresU3Ec__AnonStorey37__ctor_m3185808420 (U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37::<>m__6(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern "C"  void U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384 (U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * __this, LeaderboardScoreData_t4006482697 * ___scoreData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
