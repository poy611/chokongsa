﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight
struct SetAnimatorLayerWeight_t351678440;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::.ctor()
extern "C"  void SetAnimatorLayerWeight__ctor_m2842731902 (SetAnimatorLayerWeight_t351678440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::Reset()
extern "C"  void SetAnimatorLayerWeight_Reset_m489164843 (SetAnimatorLayerWeight_t351678440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::OnEnter()
extern "C"  void SetAnimatorLayerWeight_OnEnter_m3952853461 (SetAnimatorLayerWeight_t351678440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::OnUpdate()
extern "C"  void SetAnimatorLayerWeight_OnUpdate_m1412932366 (SetAnimatorLayerWeight_t351678440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::DoLayerWeight()
extern "C"  void SetAnimatorLayerWeight_DoLayerWeight_m1650735546 (SetAnimatorLayerWeight_t351678440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
