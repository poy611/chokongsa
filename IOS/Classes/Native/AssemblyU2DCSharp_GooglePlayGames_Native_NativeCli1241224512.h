﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A
struct U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B
struct  U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.CommonStatusCodes GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B::responseCode
	int32_t ___responseCode_0;
	// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B::<>f__ref$74
	U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * ___U3CU3Ef__refU2474_1;

public:
	inline static int32_t get_offset_of_responseCode_0() { return static_cast<int32_t>(offsetof(U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512, ___responseCode_0)); }
	inline int32_t get_responseCode_0() const { return ___responseCode_0; }
	inline int32_t* get_address_of_responseCode_0() { return &___responseCode_0; }
	inline void set_responseCode_0(int32_t value)
	{
		___responseCode_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2474_1() { return static_cast<int32_t>(offsetof(U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512, ___U3CU3Ef__refU2474_1)); }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * get_U3CU3Ef__refU2474_1() const { return ___U3CU3Ef__refU2474_1; }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 ** get_address_of_U3CU3Ef__refU2474_1() { return &___U3CU3Ef__refU2474_1; }
	inline void set_U3CU3Ef__refU2474_1(U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * value)
	{
		___U3CU3Ef__refU2474_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2474_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
