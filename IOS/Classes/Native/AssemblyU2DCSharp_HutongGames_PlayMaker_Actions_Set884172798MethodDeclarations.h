﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUISkin
struct SetGUISkin_t884172798;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::.ctor()
extern "C"  void SetGUISkin__ctor_m3559557672 (SetGUISkin_t884172798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::Reset()
extern "C"  void SetGUISkin_Reset_m1205990613 (SetGUISkin_t884172798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::OnGUI()
extern "C"  void SetGUISkin_OnGUI_m3054956322 (SetGUISkin_t884172798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
