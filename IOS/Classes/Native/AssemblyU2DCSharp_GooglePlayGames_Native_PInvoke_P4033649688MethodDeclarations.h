﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponseCollector
struct FetchResponseCollector_t4033649688;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponseCollector::.ctor()
extern "C"  void FetchResponseCollector__ctor_m2193302099 (FetchResponseCollector_t4033649688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
