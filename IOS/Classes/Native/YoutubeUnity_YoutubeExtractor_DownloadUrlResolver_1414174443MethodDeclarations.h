﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1
struct U3CExtractDownloadUrlsU3Ed__1_t1414174443;
// System.Collections.Generic.IEnumerator`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>
struct IEnumerator_1_t2837303389;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo
struct ExtractionInfo_t925438340;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IEnumerator`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo> YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.Generic.IEnumerable<YoutubeExtractor.DownloadUrlResolver.ExtractionInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumerableU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_GetEnumerator_m2632622343 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerable_GetEnumerator_m2559629573 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::MoveNext()
extern "C"  bool U3CExtractDownloadUrlsU3Ed__1_MoveNext_m1534962416 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.Generic.IEnumerator<YoutubeExtractor.DownloadUrlResolver.ExtractionInfo>.get_Current()
extern "C"  ExtractionInfo_t925438340 * U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumeratorU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_get_Current_m4284446846 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerator.Reset()
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_Reset_m1672672582 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.IDisposable.Dispose()
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_System_IDisposable_Dispose_m2005597931 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_get_Current_m1829989116 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::.ctor(System.Int32)
extern "C"  void U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>m__Finally8()
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_U3CU3Em__Finally8_m1123724274 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
