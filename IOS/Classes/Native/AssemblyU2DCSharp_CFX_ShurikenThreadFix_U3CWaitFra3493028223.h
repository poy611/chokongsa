﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// System.Object
struct Il2CppObject;
// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t2457384100;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9
struct  U3CWaitFrameU3Ec__Iterator9_t3493028223  : public Il2CppObject
{
public:
	// UnityEngine.ParticleSystem[] CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::<$s_97>__0
	ParticleSystemU5BU5D_t1536434148* ___U3CU24s_97U3E__0_0;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::<$s_98>__1
	int32_t ___U3CU24s_98U3E__1_1;
	// UnityEngine.ParticleSystem CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::<ps>__2
	ParticleSystem_t381473177 * ___U3CpsU3E__2_2;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::$PC
	int32_t ___U24PC_3;
	// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::$current
	Il2CppObject * ___U24current_4;
	// CFX_ShurikenThreadFix CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::<>f__this
	CFX_ShurikenThreadFix_t2457384100 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CU24s_97U3E__0_0() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U3CU24s_97U3E__0_0)); }
	inline ParticleSystemU5BU5D_t1536434148* get_U3CU24s_97U3E__0_0() const { return ___U3CU24s_97U3E__0_0; }
	inline ParticleSystemU5BU5D_t1536434148** get_address_of_U3CU24s_97U3E__0_0() { return &___U3CU24s_97U3E__0_0; }
	inline void set_U3CU24s_97U3E__0_0(ParticleSystemU5BU5D_t1536434148* value)
	{
		___U3CU24s_97U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_97U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_98U3E__1_1() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U3CU24s_98U3E__1_1)); }
	inline int32_t get_U3CU24s_98U3E__1_1() const { return ___U3CU24s_98U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_98U3E__1_1() { return &___U3CU24s_98U3E__1_1; }
	inline void set_U3CU24s_98U3E__1_1(int32_t value)
	{
		___U3CU24s_98U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CpsU3E__2_2() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U3CpsU3E__2_2)); }
	inline ParticleSystem_t381473177 * get_U3CpsU3E__2_2() const { return ___U3CpsU3E__2_2; }
	inline ParticleSystem_t381473177 ** get_address_of_U3CpsU3E__2_2() { return &___U3CpsU3E__2_2; }
	inline void set_U3CpsU3E__2_2(ParticleSystem_t381473177 * value)
	{
		___U3CpsU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpsU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator9_t3493028223, ___U3CU3Ef__this_5)); }
	inline CFX_ShurikenThreadFix_t2457384100 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline CFX_ShurikenThreadFix_t2457384100 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(CFX_ShurikenThreadFix_t2457384100 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
