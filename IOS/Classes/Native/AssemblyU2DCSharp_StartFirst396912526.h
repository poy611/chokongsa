﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartFirst
struct  StartFirst_t396912526  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject StartFirst::Blink
	GameObject_t3674682005 * ___Blink_2;
	// System.Single StartFirst::speed
	float ___speed_3;

public:
	inline static int32_t get_offset_of_Blink_2() { return static_cast<int32_t>(offsetof(StartFirst_t396912526, ___Blink_2)); }
	inline GameObject_t3674682005 * get_Blink_2() const { return ___Blink_2; }
	inline GameObject_t3674682005 ** get_address_of_Blink_2() { return &___Blink_2; }
	inline void set_Blink_2(GameObject_t3674682005 * value)
	{
		___Blink_2 = value;
		Il2CppCodeGenWriteBarrier(&___Blink_2, value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(StartFirst_t396912526, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
