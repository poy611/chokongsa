﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2975001167.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveColor
struct  CurveColor_t904628508  : public CurveFsmAction_t2975001167
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::colorVariable
	FsmColor_t2131419205 * ___colorVariable_35;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::fromValue
	FsmColor_t2131419205 * ___fromValue_36;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::toValue
	FsmColor_t2131419205 * ___toValue_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveR
	FsmAnimationCurve_t2685995989 * ___curveR_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationR
	int32_t ___calculationR_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveG
	FsmAnimationCurve_t2685995989 * ___curveG_40;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationG
	int32_t ___calculationG_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveB
	FsmAnimationCurve_t2685995989 * ___curveB_42;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationB
	int32_t ___calculationB_43;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveA
	FsmAnimationCurve_t2685995989 * ___curveA_44;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationA
	int32_t ___calculationA_45;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.CurveColor::clr
	Color_t4194546905  ___clr_46;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveColor::finishInNextStep
	bool ___finishInNextStep_47;

public:
	inline static int32_t get_offset_of_colorVariable_35() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___colorVariable_35)); }
	inline FsmColor_t2131419205 * get_colorVariable_35() const { return ___colorVariable_35; }
	inline FsmColor_t2131419205 ** get_address_of_colorVariable_35() { return &___colorVariable_35; }
	inline void set_colorVariable_35(FsmColor_t2131419205 * value)
	{
		___colorVariable_35 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariable_35, value);
	}

	inline static int32_t get_offset_of_fromValue_36() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___fromValue_36)); }
	inline FsmColor_t2131419205 * get_fromValue_36() const { return ___fromValue_36; }
	inline FsmColor_t2131419205 ** get_address_of_fromValue_36() { return &___fromValue_36; }
	inline void set_fromValue_36(FsmColor_t2131419205 * value)
	{
		___fromValue_36 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_36, value);
	}

	inline static int32_t get_offset_of_toValue_37() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___toValue_37)); }
	inline FsmColor_t2131419205 * get_toValue_37() const { return ___toValue_37; }
	inline FsmColor_t2131419205 ** get_address_of_toValue_37() { return &___toValue_37; }
	inline void set_toValue_37(FsmColor_t2131419205 * value)
	{
		___toValue_37 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_37, value);
	}

	inline static int32_t get_offset_of_curveR_38() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___curveR_38)); }
	inline FsmAnimationCurve_t2685995989 * get_curveR_38() const { return ___curveR_38; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveR_38() { return &___curveR_38; }
	inline void set_curveR_38(FsmAnimationCurve_t2685995989 * value)
	{
		___curveR_38 = value;
		Il2CppCodeGenWriteBarrier(&___curveR_38, value);
	}

	inline static int32_t get_offset_of_calculationR_39() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___calculationR_39)); }
	inline int32_t get_calculationR_39() const { return ___calculationR_39; }
	inline int32_t* get_address_of_calculationR_39() { return &___calculationR_39; }
	inline void set_calculationR_39(int32_t value)
	{
		___calculationR_39 = value;
	}

	inline static int32_t get_offset_of_curveG_40() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___curveG_40)); }
	inline FsmAnimationCurve_t2685995989 * get_curveG_40() const { return ___curveG_40; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveG_40() { return &___curveG_40; }
	inline void set_curveG_40(FsmAnimationCurve_t2685995989 * value)
	{
		___curveG_40 = value;
		Il2CppCodeGenWriteBarrier(&___curveG_40, value);
	}

	inline static int32_t get_offset_of_calculationG_41() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___calculationG_41)); }
	inline int32_t get_calculationG_41() const { return ___calculationG_41; }
	inline int32_t* get_address_of_calculationG_41() { return &___calculationG_41; }
	inline void set_calculationG_41(int32_t value)
	{
		___calculationG_41 = value;
	}

	inline static int32_t get_offset_of_curveB_42() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___curveB_42)); }
	inline FsmAnimationCurve_t2685995989 * get_curveB_42() const { return ___curveB_42; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveB_42() { return &___curveB_42; }
	inline void set_curveB_42(FsmAnimationCurve_t2685995989 * value)
	{
		___curveB_42 = value;
		Il2CppCodeGenWriteBarrier(&___curveB_42, value);
	}

	inline static int32_t get_offset_of_calculationB_43() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___calculationB_43)); }
	inline int32_t get_calculationB_43() const { return ___calculationB_43; }
	inline int32_t* get_address_of_calculationB_43() { return &___calculationB_43; }
	inline void set_calculationB_43(int32_t value)
	{
		___calculationB_43 = value;
	}

	inline static int32_t get_offset_of_curveA_44() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___curveA_44)); }
	inline FsmAnimationCurve_t2685995989 * get_curveA_44() const { return ___curveA_44; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveA_44() { return &___curveA_44; }
	inline void set_curveA_44(FsmAnimationCurve_t2685995989 * value)
	{
		___curveA_44 = value;
		Il2CppCodeGenWriteBarrier(&___curveA_44, value);
	}

	inline static int32_t get_offset_of_calculationA_45() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___calculationA_45)); }
	inline int32_t get_calculationA_45() const { return ___calculationA_45; }
	inline int32_t* get_address_of_calculationA_45() { return &___calculationA_45; }
	inline void set_calculationA_45(int32_t value)
	{
		___calculationA_45 = value;
	}

	inline static int32_t get_offset_of_clr_46() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___clr_46)); }
	inline Color_t4194546905  get_clr_46() const { return ___clr_46; }
	inline Color_t4194546905 * get_address_of_clr_46() { return &___clr_46; }
	inline void set_clr_46(Color_t4194546905  value)
	{
		___clr_46 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_47() { return static_cast<int32_t>(offsetof(CurveColor_t904628508, ___finishInNextStep_47)); }
	inline bool get_finishInNextStep_47() const { return ___finishInNextStep_47; }
	inline bool* get_address_of_finishInNextStep_47() { return &___finishInNextStep_47; }
	inline void set_finishInNextStep_47(bool value)
	{
		___finishInNextStep_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
