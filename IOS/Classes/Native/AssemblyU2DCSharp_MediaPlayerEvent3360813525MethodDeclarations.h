﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerEvent
struct MediaPlayerEvent_t3360813525;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_ERRO1475309647.h"

// System.Void MediaPlayerEvent::.ctor()
extern "C"  void MediaPlayerEvent__ctor_m1337816166 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::Start()
extern "C"  void MediaPlayerEvent_Start_m284953958 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::Update()
extern "C"  void MediaPlayerEvent_Update_m249490279 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::OnReady()
extern "C"  void MediaPlayerEvent_OnReady_m3510223560 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::OnFirstFrameReady()
extern "C"  void MediaPlayerEvent_OnFirstFrameReady_m4125261419 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::OnEnd()
extern "C"  void MediaPlayerEvent_OnEnd_m832138496 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::OnResize()
extern "C"  void MediaPlayerEvent_OnResize_m1962778705 (MediaPlayerEvent_t3360813525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerEvent::OnError(MediaPlayerCtrl/MEDIAPLAYER_ERROR,MediaPlayerCtrl/MEDIAPLAYER_ERROR)
extern "C"  void MediaPlayerEvent_OnError_m2791663603 (MediaPlayerEvent_t3360813525 * __this, int32_t ___errorCode0, int32_t ___errorCodeExtra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
