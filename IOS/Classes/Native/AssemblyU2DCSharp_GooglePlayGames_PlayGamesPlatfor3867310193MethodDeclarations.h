﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39
struct U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193;
// GooglePlayGames.BasicApi.LeaderboardScoreData
struct LeaderboardScoreData_t4006482697;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb4006482697.h"

// System.Void GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39::.ctor()
extern "C"  void U3CHandleLoadingScoresU3Ec__AnonStorey39__ctor_m3465157450 (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39::<>m__8(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern "C"  void U3CHandleLoadingScoresU3Ec__AnonStorey39_U3CU3Em__8_m3584706148 (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * __this, LeaderboardScoreData_t4006482697 * ___nextScoreData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
