﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecipeList
struct RecipeList_t1641733996;

#include "codegen/il2cpp-codegen.h"

// System.Void RecipeList::.ctor()
extern "C"  void RecipeList__ctor_m3082244783 (RecipeList_t1641733996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeList::.cctor()
extern "C"  void RecipeList__cctor_m578211550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RecipeList RecipeList::GetInstance()
extern "C"  RecipeList_t1641733996 * RecipeList_GetInstance_m3683561895 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
