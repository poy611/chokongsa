﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ForwardEvent
struct ForwardEvent_t3275232509;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"

// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::.ctor()
extern "C"  void ForwardEvent__ctor_m2830231049 (ForwardEvent_t3275232509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::Reset()
extern "C"  void ForwardEvent_Reset_m476663990 (ForwardEvent_t3275232509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ForwardEvent::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardEvent_Event_m2780265479 (ForwardEvent_t3275232509 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
