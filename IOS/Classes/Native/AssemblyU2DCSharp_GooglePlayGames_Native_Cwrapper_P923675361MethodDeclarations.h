﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4163934672.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_CurrentLevel(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Player_Player_CurrentLevel_m1264167784 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Name_m332392472 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Player::Player_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Player_Player_Dispose_m2925546665 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_AvatarUrl_m901019241 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___resolution1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_LastLevelUpTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Player_Player_LastLevelUpTime_m996014492 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Title(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Title_m1522031299 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_CurrentXP(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Player_Player_CurrentXP_m17556439 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Player_Player_Valid_m170236154 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_HasLevelInfo(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Player_Player_HasLevelInfo_m3132517014 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_NextLevel(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Player_Player_NextLevel_m3927512976 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Id_m605911176 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
