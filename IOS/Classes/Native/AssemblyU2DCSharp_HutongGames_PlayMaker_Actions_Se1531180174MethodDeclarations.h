﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRotation
struct SetRotation_t1531180174;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRotation::.ctor()
extern "C"  void SetRotation__ctor_m3889313384 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::Reset()
extern "C"  void SetRotation_Reset_m1535746325 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnEnter()
extern "C"  void SetRotation_OnEnter_m400343103 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnUpdate()
extern "C"  void SetRotation_OnUpdate_m2954260964 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnLateUpdate()
extern "C"  void SetRotation_OnLateUpdate_m2891228586 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::DoSetRotation()
extern "C"  void SetRotation_DoSetRotation_m2520027579 (SetRotation_t1531180174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
