﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_ResultOfGame_ResultState1900874443.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultOfGame
struct  ResultOfGame_t222677254  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ResultOfGame::reverse
	int32_t ___reverse_2;
	// System.Single ResultOfGame::totalScore
	float ___totalScore_3;
	// System.Single[] ResultOfGame::tempArray
	SingleU5BU5D_t2316563989* ___tempArray_4;
	// System.Single[] ResultOfGame::countArray
	SingleU5BU5D_t2316563989* ___countArray_5;
	// UnityEngine.Sprite[] ResultOfGame::_gameImg
	SpriteU5BU5D_t2761310900* ____gameImg_6;
	// System.Boolean ResultOfGame::okClick
	bool ___okClick_7;
	// PopupManagement ResultOfGame::pop
	PopupManagement_t282433007 * ___pop_8;
	// UnityEngine.GameObject ResultOfGame::resultPref
	GameObject_t3674682005 * ___resultPref_9;
	// ResultOfGame/ResultState ResultOfGame::state
	int32_t ___state_10;

public:
	inline static int32_t get_offset_of_reverse_2() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___reverse_2)); }
	inline int32_t get_reverse_2() const { return ___reverse_2; }
	inline int32_t* get_address_of_reverse_2() { return &___reverse_2; }
	inline void set_reverse_2(int32_t value)
	{
		___reverse_2 = value;
	}

	inline static int32_t get_offset_of_totalScore_3() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___totalScore_3)); }
	inline float get_totalScore_3() const { return ___totalScore_3; }
	inline float* get_address_of_totalScore_3() { return &___totalScore_3; }
	inline void set_totalScore_3(float value)
	{
		___totalScore_3 = value;
	}

	inline static int32_t get_offset_of_tempArray_4() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___tempArray_4)); }
	inline SingleU5BU5D_t2316563989* get_tempArray_4() const { return ___tempArray_4; }
	inline SingleU5BU5D_t2316563989** get_address_of_tempArray_4() { return &___tempArray_4; }
	inline void set_tempArray_4(SingleU5BU5D_t2316563989* value)
	{
		___tempArray_4 = value;
		Il2CppCodeGenWriteBarrier(&___tempArray_4, value);
	}

	inline static int32_t get_offset_of_countArray_5() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___countArray_5)); }
	inline SingleU5BU5D_t2316563989* get_countArray_5() const { return ___countArray_5; }
	inline SingleU5BU5D_t2316563989** get_address_of_countArray_5() { return &___countArray_5; }
	inline void set_countArray_5(SingleU5BU5D_t2316563989* value)
	{
		___countArray_5 = value;
		Il2CppCodeGenWriteBarrier(&___countArray_5, value);
	}

	inline static int32_t get_offset_of__gameImg_6() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ____gameImg_6)); }
	inline SpriteU5BU5D_t2761310900* get__gameImg_6() const { return ____gameImg_6; }
	inline SpriteU5BU5D_t2761310900** get_address_of__gameImg_6() { return &____gameImg_6; }
	inline void set__gameImg_6(SpriteU5BU5D_t2761310900* value)
	{
		____gameImg_6 = value;
		Il2CppCodeGenWriteBarrier(&____gameImg_6, value);
	}

	inline static int32_t get_offset_of_okClick_7() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___okClick_7)); }
	inline bool get_okClick_7() const { return ___okClick_7; }
	inline bool* get_address_of_okClick_7() { return &___okClick_7; }
	inline void set_okClick_7(bool value)
	{
		___okClick_7 = value;
	}

	inline static int32_t get_offset_of_pop_8() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___pop_8)); }
	inline PopupManagement_t282433007 * get_pop_8() const { return ___pop_8; }
	inline PopupManagement_t282433007 ** get_address_of_pop_8() { return &___pop_8; }
	inline void set_pop_8(PopupManagement_t282433007 * value)
	{
		___pop_8 = value;
		Il2CppCodeGenWriteBarrier(&___pop_8, value);
	}

	inline static int32_t get_offset_of_resultPref_9() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___resultPref_9)); }
	inline GameObject_t3674682005 * get_resultPref_9() const { return ___resultPref_9; }
	inline GameObject_t3674682005 ** get_address_of_resultPref_9() { return &___resultPref_9; }
	inline void set_resultPref_9(GameObject_t3674682005 * value)
	{
		___resultPref_9 = value;
		Il2CppCodeGenWriteBarrier(&___resultPref_9, value);
	}

	inline static int32_t get_offset_of_state_10() { return static_cast<int32_t>(offsetof(ResultOfGame_t222677254, ___state_10)); }
	inline int32_t get_state_10() const { return ___state_10; }
	inline int32_t* get_address_of_state_10() { return &___state_10; }
	inline void set_state_10(int32_t value)
	{
		___state_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
