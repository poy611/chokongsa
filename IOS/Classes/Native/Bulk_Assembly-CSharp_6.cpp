﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// HutongGames.PlayMaker.Actions.FinishFSM
struct FinishFSM_t701881083;
// HutongGames.PlayMaker.Actions.Flicker
struct Flicker_t1041969670;
// HutongGames.PlayMaker.Actions.FloatAbs
struct FloatAbs_t1757489438;
// HutongGames.PlayMaker.Actions.FloatAdd
struct FloatAdd_t1757489485;
// HutongGames.PlayMaker.Actions.FloatAddMultiple
struct FloatAddMultiple_t2785538429;
// HutongGames.PlayMaker.Actions.FloatChanged
struct FloatChanged_t1905781472;
// HutongGames.PlayMaker.Actions.FloatClamp
struct FloatClamp_t1735441703;
// HutongGames.PlayMaker.Actions.FloatCompare
struct FloatCompare_t2117322001;
// System.String
struct String_t;
// HutongGames.PlayMaker.Actions.FloatDivide
struct FloatDivide_t1636605059;
// HutongGames.PlayMaker.Actions.FloatInterpolate
struct FloatInterpolate_t1095399597;
// HutongGames.PlayMaker.Actions.FloatMultiply
struct FloatMultiply_t1817102958;
// HutongGames.PlayMaker.Actions.FloatOperator
struct FloatOperator_t662719726;
// HutongGames.PlayMaker.Actions.FloatRound
struct FloatRound_t1749403130;
// HutongGames.PlayMaker.Actions.FloatSignTest
struct FloatSignTest_t1474597945;
// HutongGames.PlayMaker.Actions.FloatSubtract
struct FloatSubtract_t3397992286;
// HutongGames.PlayMaker.Actions.FloatSwitch
struct FloatSwitch_t2078594878;
// HutongGames.PlayMaker.Actions.FormatString
struct FormatString_t1206542608;
// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct ForwardAllEvents_t4134244477;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.Actions.ForwardEvent
struct ForwardEvent_t3275232509;
// HutongGames.PlayMaker.Actions.FsmArraySet
struct FsmArraySet_t3475177271;
// HutongGames.PlayMaker.Actions.FsmEventOptions
struct FsmEventOptions_t3746547986;
// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase
struct FsmStateActionAnimatorBase_t2852864039;
// HutongGames.PlayMaker.Actions.FsmStateSwitch
struct FsmStateSwitch_t3425333421;
// HutongGames.PlayMaker.Actions.FsmStateTest
struct FsmStateTest_t3341384075;
// HutongGames.PlayMaker.Actions.GameObjectChanged
struct GameObjectChanged_t641634289;
// HutongGames.PlayMaker.Actions.GameObjectCompare
struct GameObjectCompare_t853174818;
// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct GameObjectCompareTag_t3437146702;
// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct GameObjectHasChildren_t3474747126;
// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct GameObjectIsChildOf_t1849565254;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct GameObjectIsNull_t2162669226;
// HutongGames.PlayMaker.Actions.GameObjectIsVisible
struct GameObjectIsVisible_t1570039973;
// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct GameObjectTagSwitch_t3433592875;
// HutongGames.PlayMaker.Actions.GetACosine
struct GetACosine_t1733000684;
// HutongGames.PlayMaker.Actions.GetAngleToTarget
struct GetAngleToTarget_t1034773969;
// HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
struct GetAnimatorApplyRootMotion_t212715153;
// UnityEngine.Animator
struct Animator_t2776330603;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct GetAnimatorBody_t3376544941;
// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct GetAnimatorBoneGameObject_t14629568;
// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct GetAnimatorBool_t3376545269;
// HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
struct GetAnimatorCullingMode_t994674750;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo
struct GetAnimatorCurrentStateInfo_t2823805553;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName
struct GetAnimatorCurrentStateInfoIsName_t2852409574;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct GetAnimatorCurrentStateInfoIsTag_t2329710485;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct GetAnimatorCurrentTransitionInfo_t114989767;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
struct GetAnimatorCurrentTransitionInfoIsName_t3939956924;
// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
struct GetAnimatorCurrentTransitionInfoIsUserName_t1580140583;
// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct GetAnimatorDelta_t946274563;
// HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive
struct GetAnimatorFeetPivotActive_t705651877;
// HutongGames.PlayMaker.Actions.GetAnimatorFloat
struct GetAnimatorFloat_t948332455;
// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct GetAnimatorGravityWeight_t3107738545;
// HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
struct GetAnimatorHumanScale_t3915188008;
// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct GetAnimatorIKGoal_t3033774656;
// HutongGames.PlayMaker.Actions.GetAnimatorInt
struct GetAnimatorInt_t3593545018;
// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct GetAnimatorIsHuman_t55053166;
// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct GetAnimatorIsLayerInTransition_t3666075020;
// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct GetAnimatorIsMatchingTarget_t2980401859;
// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct GetAnimatorIsParameterControlledByCurve_t1394563734;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
struct GetAnimatorLayerCount_t3712536905;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct GetAnimatorLayerName_t4020332743;
// HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
struct GetAnimatorLayersAffectMassCenter_t2244244939;
// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct GetAnimatorLayerWeight_t3333517428;
// HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
struct GetAnimatorLeftFootBottomHeight_t4090212338;
// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct GetAnimatorNextStateInfo_t3502000247;
// HutongGames.PlayMaker.Actions.GetAnimatorPivot
struct GetAnimatorPivot_t957485453;
// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed
struct GetAnimatorPlayBackSpeed_t331618327;
// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime
struct GetAnimatorPlayBackTime_t2941148851;
// HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight
struct GetAnimatorRightFootBottomHeight_t1347553863;
// HutongGames.PlayMaker.Actions.GetAnimatorRoot
struct GetAnimatorRoot_t3377021933;
// HutongGames.PlayMaker.Actions.GetAnimatorSpeed
struct GetAnimatorSpeed_t960447890;
// HutongGames.PlayMaker.Actions.GetAnimatorTarget
struct GetAnimatorTarget_t3370286236;
// HutongGames.PlayMaker.Actions.GetASine
struct GetASine_t1695390080;
// HutongGames.PlayMaker.Actions.GetAtan
struct GetAtan_t1738223652;
// HutongGames.PlayMaker.Actions.GetAtan2
struct GetAtan2_t1696365444;
// HutongGames.PlayMaker.Actions.GetAtan2FromVector2
struct GetAtan2FromVector2_t4248152919;
// HutongGames.PlayMaker.Actions.GetAtan2FromVector3
struct GetAtan2FromVector3_t4248152920;
// HutongGames.PlayMaker.Actions.GetAxis
struct GetAxis_t1738227749;
// HutongGames.PlayMaker.Actions.GetAxisVector
struct GetAxisVector_t2714053128;
// HutongGames.PlayMaker.Actions.GetButton
struct GetButton_t428887414;
// HutongGames.PlayMaker.Actions.GetButtonDown
struct GetButtonDown_t2951628984;
// HutongGames.PlayMaker.Actions.GetButtonUp
struct GetButtonUp_t552055153;
// HutongGames.PlayMaker.Actions.GetChild
struct GetChild_t1697862670;
// HutongGames.PlayMaker.Actions.GetChildCount
struct GetChildCount_t3922702103;
// HutongGames.PlayMaker.Actions.GetChildNum
struct GetChildNum_t3319553230;
// HutongGames.PlayMaker.Actions.GetCollision2dInfo
struct GetCollision2dInfo_t2518958628;
// HutongGames.PlayMaker.Actions.GetCollisionInfo
struct GetCollisionInfo_t3783581426;
// HutongGames.PlayMaker.Actions.GetColorRGBA
struct GetColorRGBA_t1254146697;
// HutongGames.PlayMaker.Actions.GetComponent
struct GetComponent_t2168373359;
// HutongGames.PlayMaker.Actions.GetControllerCollisionFlags
struct GetControllerCollisionFlags_t722913077;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// HutongGames.PlayMaker.Actions.GetControllerHitInfo
struct GetControllerHitInfo_t3761505303;
// HutongGames.PlayMaker.Actions.GetCosine
struct GetCosine_t451935037;
// HutongGames.PlayMaker.Actions.GetDeviceAcceleration
struct GetDeviceAcceleration_t262192826;
// HutongGames.PlayMaker.Actions.GetDeviceRoll
struct GetDeviceRoll_t3374672311;
// HutongGames.PlayMaker.Actions.GetDistance
struct GetDistance_t1948520441;
// HutongGames.PlayMaker.Actions.GetEventInfo
struct GetEventInfo_t3599372762;
// HutongGames.PlayMaker.Actions.GetFsmArray
struct GetFsmArray_t1205911805;
// HutongGames.PlayMaker.Actions.GetFsmArrayItem
struct GetFsmArrayItem_t2453534512;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin701881083.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fin701881083MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1041969670.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1041969670MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Boolean476798718.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489438.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489438MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489485.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1757489485MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2785538429.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2785538429MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1905781472.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1905781472MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1735441703.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1735441703MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2117322001.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2117322001MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1636605059.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1636605059MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1095399597.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1095399597MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1817102958.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1817102958MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo662719726.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo662719726MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1085110523.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1085110523MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1749403130.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1749403130MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1474597945.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl1474597945MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl3397992286.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl3397992286MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2078594878.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl2078594878MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo1206542608.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo1206542608MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "mscorlib_System_FormatException3455918062.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo4134244477.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo4134244477MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo3275232509.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fo3275232509MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3475177271.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3475177271MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3746547986.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3746547986MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1093022331.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1093022331MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3425333421.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3425333421MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3341384075.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs3341384075MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam641634289.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam641634289MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam853174818.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Gam853174818MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3437146702.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3437146702MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3474747126.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3474747126MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1849565254.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1849565254MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga2162669226.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga2162669226MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1570039973.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga1570039973MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3433592875.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ga3433592875MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1733000684.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1733000684MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1034773969.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1034773969MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get212715153.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get212715153MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376544941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376544941MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA14629568.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA14629568MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395MethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "mscorlib_System_Enum2862688501.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376545269.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3376545269MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get994674750.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get994674750MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2823805553.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2823805553MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2852409574.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2852409574MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2329710485.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2329710485MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get114989767.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get114989767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3939956924.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3939956924MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1580140583.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1580140583MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get946274563.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get946274563MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get705651877.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get705651877MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get948332455.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get948332455MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3107738545.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3107738545MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3915188008.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3915188008MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3033774656.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3033774656MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3593545018.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3593545018MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA55053166.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetA55053166MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3666075020.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3666075020MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2980401859.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2980401859MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1394563734.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1394563734MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3712536905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3712536905MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4020332743.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4020332743MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2244244939.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2244244939MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3333517428.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3333517428MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4090212338.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4090212338MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3502000247.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3502000247MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get957485453.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get957485453MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get331618327.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get331618327MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2941148851.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2941148851MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1347553863.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1347553863MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3377021933.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3377021933MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get960447890.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get960447890MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3370286236.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3370286236MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1695390080.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1695390080MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738223652.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738223652MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1696365444.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1696365444MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4248152919.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4248152919MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4248152920.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4248152920MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1456148914.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1456148914MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738227749.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738227749MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2714053128.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2714053128MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get992085865.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get992085865MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get428887414.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get428887414MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2951628984.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2951628984MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get552055153.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get552055153MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1697862670.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1697862670MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3922702103.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3922702103MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3319553230.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3319553230MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2518958628.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2518958628MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3783581426.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3783581426MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2494107688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1254146697.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1254146697MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2168373359.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2168373359MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get722913077.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get722913077MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3761505303.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3761505303MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get451935037.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get451935037MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get262192826.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get262192826MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3374672311.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3374672311MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3972655516.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3972655516MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1948520441.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1948520441MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3599372762.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3599372762MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1205911805.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1205911805MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2224198959MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2224198959.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2453534512.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2453534512MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2226508741MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2226508741.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(__this, method) ((  Animator_t2776330603 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.CharacterController>()
#define GameObject_GetComponent_TisCharacterController_t1618060635_m3112787219(__this, method) ((  CharacterController_t1618060635 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.Actions.FinishFSM::.ctor()
extern "C"  void FinishFSM__ctor_m1366185115 (FinishFSM_t701881083 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FinishFSM::OnEnter()
extern "C"  void FinishFSM_OnEnter_m2330598834 (FinishFSM_t701881083 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Stop_m177462513(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t Flicker__ctor_m3158791152_MetadataUsageId;
extern "C"  void Flicker__ctor_m3158791152 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Flicker__ctor_m3158791152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::Reset()
extern "C"  void Flicker_Reset_m805224093 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.1f), /*hidden argument*/NULL);
		__this->set_frequency_14(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.5f), /*hidden argument*/NULL);
		__this->set_amountOn_15(L_1);
		__this->set_rendererOnly_16((bool)1);
		__this->set_realTime_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnEnter()
extern "C"  void Flicker_OnEnter_m2743114695 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_18(L_0);
		__this->set_timer_19((0.0f));
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t Flicker_OnUpdate_m2565736284_MetadataUsageId;
extern "C"  void Flicker_OnUpdate_m2565736284 (Flicker_t1041969670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Flicker_OnUpdate_m2565736284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		bool L_5 = __this->get_realTime_17();
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		float L_6 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_startTime_18();
		__this->set_timer_19(((float)((float)L_6-(float)L_7)));
		goto IL_0053;
	}

IL_0041:
	{
		float L_8 = __this->get_timer_19();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_19(((float)((float)L_8+(float)L_9)));
	}

IL_0053:
	{
		float L_10 = __this->get_timer_19();
		FsmFloat_t2134102846 * L_11 = __this->get_frequency_14();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) > ((float)L_12))))
		{
			goto IL_00cc;
		}
	}
	{
		float L_13 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_amountOn_15();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_13) < ((float)L_15))? 1 : 0);
		bool L_16 = __this->get_rendererOnly_16();
		if (!L_16)
		{
			goto IL_00ae;
		}
	}
	{
		GameObject_t3674682005 * L_17 = V_0;
		bool L_18 = ComponentAction_1_UpdateCache_m3595067967(__this, L_17, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (!L_18)
		{
			goto IL_00a9;
		}
	}
	{
		Renderer_t3076687687 * L_19 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		bool L_20 = V_1;
		NullCheck(L_19);
		Renderer_set_enabled_m2514140131(L_19, L_20, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		goto IL_00b5;
	}

IL_00ae:
	{
		GameObject_t3674682005 * L_21 = V_0;
		bool L_22 = V_1;
		NullCheck(L_21);
		GameObject_SetActive_m3538205401(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		float L_23 = __this->get_timer_19();
		__this->set_startTime_18(L_23);
		__this->set_timer_19((0.0f));
	}

IL_00cc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::.ctor()
extern "C"  void FloatAbs__ctor_m1440920328 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::Reset()
extern "C"  void FloatAbs_Reset_m3382320565 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnEnter()
extern "C"  void FloatAbs_OnEnter_m1136694495 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FloatAbs_DoFloatAbs_m2648064733(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnUpdate()
extern "C"  void FloatAbs_OnUpdate_m11350340 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	{
		FloatAbs_DoFloatAbs_m2648064733(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::DoFloatAbs()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatAbs_DoFloatAbs_m2648064733_MetadataUsageId;
extern "C"  void FloatAbs_DoFloatAbs_m2648064733 (FloatAbs_t1757489438 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatAbs_DoFloatAbs_m2648064733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::.ctor()
extern "C"  void FloatAdd__ctor_m794720185 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::Reset()
extern "C"  void FloatAdd_Reset_m2736120422 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_add_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		__this->set_perSecond_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnEnter()
extern "C"  void FloatAdd_OnEnter_m2908614992 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FloatAdd_DoFloatAdd_m488600891(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnUpdate()
extern "C"  void FloatAdd_OnUpdate_m3401278195 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		FloatAdd_DoFloatAdd_m488600891(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::DoFloatAdd()
extern "C"  void FloatAdd_DoFloatAdd_m488600891 (FloatAdd_t1757489485 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_perSecond_14();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_2 = L_1;
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_add_12();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_7 = L_6;
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_add_12();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, ((float)((float)L_8+(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::.ctor()
extern "C"  void FloatAddMultiple__ctor_m3039579529 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::Reset()
extern "C"  void FloatAddMultiple_Reset_m686012470 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariables_11((FsmFloatU5BU5D_t2945380875*)NULL);
		__this->set_addTo_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnEnter()
extern "C"  void FloatAddMultiple_OnEnter_m4144861984 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method)
{
	{
		FloatAddMultiple_DoFloatAdd_m178176363(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnUpdate()
extern "C"  void FloatAddMultiple_OnUpdate_m3070229283 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method)
{
	{
		FloatAddMultiple_DoFloatAdd_m178176363(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::DoFloatAdd()
extern "C"  void FloatAddMultiple_DoFloatAdd_m178176363 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0007:
	{
		FsmFloat_t2134102846 * L_0 = __this->get_addTo_12();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloatU5BU5D_t2945380875* L_3 = __this->get_floatVariables_11();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FsmFloat_t2134102846 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2+(float)L_7)), /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		FsmFloatU5BU5D_t2945380875* L_10 = __this->get_floatVariables_11();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::.ctor()
extern "C"  void FloatChanged__ctor_m1836121990 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::Reset()
extern "C"  void FloatChanged_Reset_m3777522227 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_changedEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnEnter()
extern "C"  void FloatChanged_OnEnter_m2968369629 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_14(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatChanged::OnUpdate()
extern "C"  void FloatChanged_OnUpdate_m958704646 (FloatChanged_t1905781472 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_13();
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_previousValue_14();
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_0050;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_floatVariable_11();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		__this->set_previousValue_14(L_5);
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_13();
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_changedEvent_12();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::.ctor()
extern "C"  void FloatClamp__ctor_m3898602271 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::Reset()
extern "C"  void FloatClamp_Reset_m1545035212 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_minValue_12((FsmFloat_t2134102846 *)NULL);
		__this->set_maxValue_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnEnter()
extern "C"  void FloatClamp_OnEnter_m737028918 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FloatClamp_DoClamp_m3332393005(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnUpdate()
extern "C"  void FloatClamp_OnUpdate_m506619341 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	{
		FloatClamp_DoClamp_m3332393005(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::DoClamp()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatClamp_DoClamp_m3332393005_MetadataUsageId;
extern "C"  void FloatClamp_DoClamp_m3332393005 (FloatClamp_t1735441703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatClamp_DoClamp_m3332393005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_minValue_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_maxValue_13();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_2, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::.ctor()
extern "C"  void FloatCompare__ctor_m2878934133 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::Reset()
extern "C"  void FloatCompare_Reset_m525367074 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_float1_11(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_float2_12(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_tolerance_13(L_2);
		__this->set_equal_14((FsmEvent_t2133468028 *)NULL);
		__this->set_lessThan_15((FsmEvent_t2133468028 *)NULL);
		__this->set_greaterThan_16((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnEnter()
extern "C"  void FloatCompare_OnEnter_m88491788 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FloatCompare_DoCompare_m978022381(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnUpdate()
extern "C"  void FloatCompare_OnUpdate_m1876804791 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	{
		FloatCompare_DoCompare_m978022381(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::DoCompare()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatCompare_DoCompare_m978022381_MetadataUsageId;
extern "C"  void FloatCompare_DoCompare_m978022381 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatCompare_DoCompare_m978022381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmFloat_t2134102846 * L_0 = __this->get_float1_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_float2_12();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = fabsf(((float)((float)L_1-(float)L_3)));
		FsmFloat_t2134102846 * L_5 = __this->get_tolerance_13();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		if ((!(((float)L_4) <= ((float)L_6))))
		{
			goto IL_003e;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_equal_14();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		return;
	}

IL_003e:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_float1_11();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = __this->get_float2_12();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) < ((float)L_12))))
		{
			goto IL_006b;
		}
	}
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_lessThan_15();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
		return;
	}

IL_006b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_float1_11();
		NullCheck(L_15);
		float L_16 = FsmFloat_get_Value_m4137923823(L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_17 = __this->get_float2_12();
		NullCheck(L_17);
		float L_18 = FsmFloat_get_Value_m4137923823(L_17, /*hidden argument*/NULL);
		if ((!(((float)L_16) > ((float)L_18))))
		{
			goto IL_0097;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = __this->get_greaterThan_16();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.FloatCompare::ErrorCheck()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226149032;
extern const uint32_t FloatCompare_ErrorCheck_m3586345138_MetadataUsageId;
extern "C"  String_t* FloatCompare_ErrorCheck_m3586345138 (FloatCompare_t2117322001 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatCompare_ErrorCheck_m3586345138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmEvent_t2133468028 * L_0 = __this->get_equal_14();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		bool L_1 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_2 = __this->get_lessThan_15();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		bool L_3 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_greaterThan_16();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		bool L_5 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		return _stringLiteral1226149032;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_6;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::.ctor()
extern "C"  void FloatDivide__ctor_m631157267 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::Reset()
extern "C"  void FloatDivide_Reset_m2572557504 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_divideBy_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnEnter()
extern "C"  void FloatDivide_OnEnter_m343473450 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_divideBy_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2/(float)L_4)), /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_13();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnUpdate()
extern "C"  void FloatDivide_OnUpdate_m1191301721 (FloatDivide_t1636605059 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_divideBy_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2/(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::.ctor()
extern "C"  void FloatInterpolate__ctor_m748084313 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::Reset()
extern "C"  void FloatInterpolate_Reset_m2689484550 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		__this->set_mode_11(0);
		__this->set_fromFloat_12((FsmFloat_t2134102846 *)NULL);
		__this->set_toFloat_13((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_time_14(L_0);
		__this->set_storeResult_15((FsmFloat_t2134102846 *)NULL);
		__this->set_finishEvent_16((FsmEvent_t2133468028 *)NULL);
		__this->set_realTime_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnEnter()
extern "C"  void FloatInterpolate_OnEnter_m1041214960 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_18(L_0);
		__this->set_currentTime_19((0.0f));
		FsmFloat_t2134102846 * L_1 = __this->get_storeResult_15();
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_002c:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeResult_15();
		FsmFloat_t2134102846 * L_3 = __this->get_fromFloat_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnUpdate()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatInterpolate_OnUpdate_m1346452051_MetadataUsageId;
extern "C"  void FloatInterpolate_OnUpdate_m1346452051 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatInterpolate_OnUpdate_m1346452051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_realTime_17();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_18();
		__this->set_currentTime_19(((float)((float)L_1-(float)L_2)));
		goto IL_0034;
	}

IL_0022:
	{
		float L_3 = __this->get_currentTime_19();
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentTime_19(((float)((float)L_3+(float)L_4)));
	}

IL_0034:
	{
		float L_5 = __this->get_currentTime_19();
		FsmFloat_t2134102846 * L_6 = __this->get_time_14();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_5/(float)L_7));
		int32_t L_8 = __this->get_mode_11();
		V_1 = L_8;
		int32_t L_9 = V_1;
		if (!L_9)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_00b8;
	}

IL_0060:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_storeResult_15();
		FsmFloat_t2134102846 * L_12 = __this->get_fromFloat_12();
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_toFloat_13();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		float L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_13, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmFloat_set_Value_m1568963140(L_11, L_17, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_008c:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_storeResult_15();
		FsmFloat_t2134102846 * L_19 = __this->get_fromFloat_12();
		NullCheck(L_19);
		float L_20 = FsmFloat_get_Value_m4137923823(L_19, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_21 = __this->get_toFloat_13();
		NullCheck(L_21);
		float L_22 = FsmFloat_get_Value_m4137923823(L_21, /*hidden argument*/NULL);
		float L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = Mathf_SmoothStep_m1876640478(NULL /*static, unused*/, L_20, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_24, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_00b8:
	{
		float L_25 = V_0;
		if ((!(((float)L_25) > ((float)(1.0f)))))
		{
			goto IL_00e5;
		}
	}
	{
		FsmEvent_t2133468028 * L_26 = __this->get_finishEvent_16();
		if (!L_26)
		{
			goto IL_00df;
		}
	}
	{
		Fsm_t1527112426 * L_27 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_28 = __this->get_finishEvent_16();
		NullCheck(L_27);
		Fsm_Event_m625948263(L_27, L_28, /*hidden argument*/NULL);
	}

IL_00df:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::.ctor()
extern "C"  void FloatMultiply__ctor_m3361890952 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::Reset()
extern "C"  void FloatMultiply_Reset_m1008323893 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_multiplyBy_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnEnter()
extern "C"  void FloatMultiply_OnEnter_m353526879 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_multiplyBy_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_13();
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnUpdate()
extern "C"  void FloatMultiply_OnUpdate_m1502958020 (FloatMultiply_t1817102958 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = FsmFloat_get_Value_m4137923823(L_1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_3 = __this->get_multiplyBy_12();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmFloat_set_Value_m1568963140(L_1, ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::.ctor()
extern "C"  void FloatOperator__ctor_m2222698504 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::Reset()
extern "C"  void FloatOperator_Reset_m4164098741 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		__this->set_float1_11((FsmFloat_t2134102846 *)NULL);
		__this->set_float2_12((FsmFloat_t2134102846 *)NULL);
		__this->set_operation_13(0);
		__this->set_storeResult_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnEnter()
extern "C"  void FloatOperator_OnEnter_m806244831 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FloatOperator_DoFloatOperator_m4708859(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnUpdate()
extern "C"  void FloatOperator_OnUpdate_m2652312644 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	{
		FloatOperator_DoFloatOperator_m4708859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::DoFloatOperator()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatOperator_DoFloatOperator_m4708859_MetadataUsageId;
extern "C"  void FloatOperator_DoFloatOperator_m4708859 (FloatOperator_t662719726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatOperator_DoFloatOperator_m4708859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_float1_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmFloat_t2134102846 * L_2 = __this->get_float2_12();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = __this->get_operation_13();
		V_2 = L_4;
		int32_t L_5 = V_2;
		if (L_5 == 0)
		{
			goto IL_0042;
		}
		if (L_5 == 1)
		{
			goto IL_0055;
		}
		if (L_5 == 2)
		{
			goto IL_0068;
		}
		if (L_5 == 3)
		{
			goto IL_007b;
		}
		if (L_5 == 4)
		{
			goto IL_008e;
		}
		if (L_5 == 5)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00bc;
	}

IL_0042:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeResult_14();
		float L_7 = V_0;
		float L_8 = V_1;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, ((float)((float)L_7+(float)L_8)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0055:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_storeResult_14();
		float L_10 = V_0;
		float L_11 = V_1;
		NullCheck(L_9);
		FsmFloat_set_Value_m1568963140(L_9, ((float)((float)L_10-(float)L_11)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0068:
	{
		FsmFloat_t2134102846 * L_12 = __this->get_storeResult_14();
		float L_13 = V_0;
		float L_14 = V_1;
		NullCheck(L_12);
		FsmFloat_set_Value_m1568963140(L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_007b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_storeResult_14();
		float L_16 = V_0;
		float L_17 = V_1;
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, ((float)((float)L_16/(float)L_17)), /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_008e:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_storeResult_14();
		float L_19 = V_0;
		float L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_21, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00a5:
	{
		FsmFloat_t2134102846 * L_22 = __this->get_storeResult_14();
		float L_23 = V_0;
		float L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmFloat_set_Value_m1568963140(L_22, L_25, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00bc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatRound::.ctor()
extern "C"  void FloatRound__ctor_m2872846252 (FloatRound_t1749403130 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatRound::Reset()
extern "C"  void FloatRound_Reset_m519279193 (FloatRound_t1749403130 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_resultAsInt_12((FsmInt_t1596138449 *)NULL);
		__this->set_resultAsFloat_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatRound::OnEnter()
extern "C"  void FloatRound_OnEnter_m2827972739 (FloatRound_t1749403130 * __this, const MethodInfo* method)
{
	{
		FloatRound_DoFloatRound_m1411620181(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatRound::OnUpdate()
extern "C"  void FloatRound_OnUpdate_m901368352 (FloatRound_t1749403130 * __this, const MethodInfo* method)
{
	{
		FloatRound_DoFloatRound_m1411620181(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatRound::DoFloatRound()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t FloatRound_DoFloatRound_m1411620181_MetadataUsageId;
extern "C"  void FloatRound_DoFloatRound_m1411620181 (FloatRound_t1749403130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatRound_DoFloatRound_m1411620181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = __this->get_resultAsInt_12();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_resultAsInt_12();
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_11();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_resultAsFloat_13();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0056;
		}
	}
	{
		FsmFloat_t2134102846 * L_8 = __this->get_resultAsFloat_13();
		FsmFloat_t2134102846 * L_9 = __this->get_floatVariable_11();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = bankers_roundf(L_10);
		NullCheck(L_8);
		FsmFloat_set_Value_m1568963140(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::.ctor()
extern "C"  void FloatSignTest__ctor_m1954245917 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::Reset()
extern "C"  void FloatSignTest_Reset_m3895646154 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_floatValue_11(L_0);
		__this->set_isPositive_12((FsmEvent_t2133468028 *)NULL);
		__this->set_isNegative_13((FsmEvent_t2133468028 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnEnter()
extern "C"  void FloatSignTest_OnEnter_m521346484 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FloatSignTest_DoSignTest_m1316262689(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::OnUpdate()
extern "C"  void FloatSignTest_OnUpdate_m2410398479 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	{
		FloatSignTest_DoSignTest_m1316262689(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSignTest::DoSignTest()
extern "C"  void FloatSignTest_DoSignTest_m1316262689 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatValue_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_floatValue_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			G_B4_0 = L_1;
			goto IL_0032;
		}
	}
	{
		FsmEvent_t2133468028 * L_4 = __this->get_isNegative_13();
		G_B5_0 = L_4;
		G_B5_1 = G_B3_0;
		goto IL_0038;
	}

IL_0032:
	{
		FsmEvent_t2133468028 * L_5 = __this->get_isPositive_12();
		G_B5_0 = L_5;
		G_B5_1 = G_B4_0;
	}

IL_0038:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.FloatSignTest::ErrorCheck()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1226149032;
extern const uint32_t FloatSignTest_ErrorCheck_m2356136740_MetadataUsageId;
extern "C"  String_t* FloatSignTest_ErrorCheck_m2356136740 (FloatSignTest_t1474597945 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatSignTest_ErrorCheck_m2356136740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmEvent_t2133468028 * L_0 = __this->get_isPositive_12();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		bool L_1 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		FsmEvent_t2133468028 * L_2 = __this->get_isNegative_13();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		bool L_3 = FsmEvent_IsNullOrEmpty_m3021350928(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		return _stringLiteral1226149032;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::.ctor()
extern "C"  void FloatSubtract__ctor_m394558360 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::Reset()
extern "C"  void FloatSubtract_Reset_m2335958597 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_subtract_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		__this->set_perSecond_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnEnter()
extern "C"  void FloatSubtract_OnEnter_m605190511 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FloatSubtract_DoFloatSubtract_m2286520315(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnUpdate()
extern "C"  void FloatSubtract_OnUpdate_m714596020 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		FloatSubtract_DoFloatSubtract_m2286520315(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::DoFloatSubtract()
extern "C"  void FloatSubtract_DoFloatSubtract_m2286520315 (FloatSubtract_t3397992286 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_perSecond_14();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_2 = L_1;
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_4 = __this->get_subtract_12();
		NullCheck(L_4);
		float L_5 = FsmFloat_get_Value_m4137923823(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, ((float)((float)L_3-(float)L_5)), /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_floatVariable_11();
		FsmFloat_t2134102846 * L_7 = L_6;
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_subtract_12();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, ((float)((float)L_8-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::.ctor()
extern "C"  void FloatSwitch__ctor_m1133337016 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::Reset()
extern Il2CppClass* FsmFloatU5BU5D_t2945380875_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t FloatSwitch_Reset_m3074737253_MetadataUsageId;
extern "C"  void FloatSwitch_Reset_m3074737253 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FloatSwitch_Reset_m3074737253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_floatVariable_11((FsmFloat_t2134102846 *)NULL);
		__this->set_lessThan_12(((FsmFloatU5BU5D_t2945380875*)SZArrayNew(FsmFloatU5BU5D_t2945380875_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_13(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnEnter()
extern "C"  void FloatSwitch_OnEnter_m1901875087 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FloatSwitch_DoFloatSwitch_m863132603(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnUpdate()
extern "C"  void FloatSwitch_OnUpdate_m2257112212 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	{
		FloatSwitch_DoFloatSwitch_m863132603(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::DoFloatSwitch()
extern "C"  void FloatSwitch_DoFloatSwitch_m863132603 (FloatSwitch_t2078594878 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = 0;
		goto IL_004d;
	}

IL_0018:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_floatVariable_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		FsmFloatU5BU5D_t2945380875* L_4 = __this->get_lessThan_12();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		FsmFloat_t2134102846 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		if ((!(((float)L_3) < ((float)L_8))))
		{
			goto IL_0049;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_10 = __this->get_sendEvent_13();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		FsmEvent_t2133468028 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_15 = V_0;
		FsmFloatU5BU5D_t2945380875* L_16 = __this->get_lessThan_12();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::.ctor()
extern "C"  void FormatString__ctor_m994606934 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::Reset()
extern "C"  void FormatString_Reset_m2936007171 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		__this->set_format_11((FsmString_t952858651 *)NULL);
		__this->set_variables_12((FsmVarU5BU5D_t3498949300*)NULL);
		__this->set_storeResult_13((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnEnter()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t FormatString_OnEnter_m1726252461_MetadataUsageId;
extern "C"  void FormatString_OnEnter_m1726252461 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FormatString_OnEnter_m1726252461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_variables_12();
		NullCheck(L_0);
		__this->set_objectArray_15(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		FormatString_DoFormatString_m2819192129(__this, /*hidden argument*/NULL);
		bool L_1 = __this->get_everyFrame_14();
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnUpdate()
extern "C"  void FormatString_OnUpdate_m1107778102 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	{
		FormatString_DoFormatString_m2819192129(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FormatString::DoFormatString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t3455918062_il2cpp_TypeInfo_var;
extern const uint32_t FormatString_DoFormatString_m2819192129_MetadataUsageId;
extern "C"  void FormatString_DoFormatString_m2819192129 (FormatString_t1206542608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FormatString_DoFormatString_m2819192129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	FormatException_t3455918062 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0007:
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_variables_12();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		FsmVar_t1596150537 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		FsmVar_UpdateValue_m1579759472(L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = __this->get_objectArray_15();
		int32_t L_5 = V_0;
		FsmVarU5BU5D_t3498949300* L_6 = __this->get_variables_12();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FsmVar_t1596150537 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		Il2CppObject * L_10 = FsmVar_GetValue_m2309870922(L_9, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_10);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_10);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_12 = V_0;
		FsmVarU5BU5D_t3498949300* L_13 = __this->get_variables_12();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		FsmString_t952858651 * L_14 = __this->get_storeResult_13();
		FsmString_t952858651 * L_15 = __this->get_format_11();
		NullCheck(L_15);
		String_t* L_16 = FsmString_get_Value_m872383149(L_15, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_17 = __this->get_objectArray_15();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m4050103162(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmString_set_Value_m829393196(L_14, L_18, /*hidden argument*/NULL);
		goto IL_0079;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t3455918062_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0061;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.FormatException)
		V_1 = ((FormatException_t3455918062 *)__exception_local);
		FormatException_t3455918062 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_19);
		FsmStateAction_LogError_m3478223492(__this, L_20, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0079;
	} // end catch (depth: 1)

IL_0079:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::.ctor()
extern "C"  void ForwardAllEvents__ctor_m3245051529 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::Reset()
extern Il2CppClass* FsmEventTarget_t1823904941_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t ForwardAllEvents_Reset_m891484470_MetadataUsageId;
extern "C"  void ForwardAllEvents_Reset_m891484470 (ForwardAllEvents_t4134244477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForwardAllEvents_Reset_m891484470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmEventTarget_t1823904941 * V_0 = NULL;
	{
		FsmEventTarget_t1823904941 * L_0 = (FsmEventTarget_t1823904941 *)il2cpp_codegen_object_new(FsmEventTarget_t1823904941_il2cpp_TypeInfo_var);
		FsmEventTarget__ctor_m3123387462(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmEventTarget_t1823904941 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_target_1(3);
		FsmEventTarget_t1823904941 * L_2 = V_0;
		__this->set_forwardTo_11(L_2);
		FsmEventU5BU5D_t2862142229* L_3 = ((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_4 = FsmEvent_get_Finished_m374313731(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (FsmEvent_t2133468028 *)L_4);
		__this->set_exceptThese_12(L_3);
		__this->set_eatEvents_13((bool)1);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardAllEvents_Event_m4176117383 (ForwardAllEvents_t4134244477 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	FsmEvent_t2133468028 * V_0 = NULL;
	FsmEventU5BU5D_t2862142229* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmEventU5BU5D_t2862142229* L_0 = __this->get_exceptThese_12();
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		FsmEventU5BU5D_t2862142229* L_1 = __this->get_exceptThese_12();
		V_1 = L_1;
		V_2 = 0;
		goto IL_002a;
	}

IL_0019:
	{
		FsmEventU5BU5D_t2862142229* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		FsmEvent_t2133468028 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		FsmEvent_t2133468028 * L_6 = V_0;
		FsmEvent_t2133468028 * L_7 = ___fsmEvent0;
		if ((!(((Il2CppObject*)(FsmEvent_t2133468028 *)L_6) == ((Il2CppObject*)(FsmEvent_t2133468028 *)L_7))))
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_2;
		FsmEventU5BU5D_t2862142229* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_0033:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventTarget_t1823904941 * L_12 = __this->get_forwardTo_11();
		FsmEvent_t2133468028 * L_13 = ___fsmEvent0;
		NullCheck(L_11);
		Fsm_Event_m1295831978(L_11, L_12, L_13, /*hidden argument*/NULL);
		bool L_14 = __this->get_eatEvents_13();
		return L_14;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::.ctor()
extern "C"  void ForwardEvent__ctor_m2830231049 (ForwardEvent_t3275232509 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::Reset()
extern Il2CppClass* FsmEventTarget_t1823904941_il2cpp_TypeInfo_var;
extern const uint32_t ForwardEvent_Reset_m476663990_MetadataUsageId;
extern "C"  void ForwardEvent_Reset_m476663990 (ForwardEvent_t3275232509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ForwardEvent_Reset_m476663990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmEventTarget_t1823904941 * V_0 = NULL;
	{
		FsmEventTarget_t1823904941 * L_0 = (FsmEventTarget_t1823904941 *)il2cpp_codegen_object_new(FsmEventTarget_t1823904941_il2cpp_TypeInfo_var);
		FsmEventTarget__ctor_m3123387462(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmEventTarget_t1823904941 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_target_1(3);
		FsmEventTarget_t1823904941 * L_2 = V_0;
		__this->set_forwardTo_11(L_2);
		__this->set_eventsToForward_12((FsmEventU5BU5D_t2862142229*)NULL);
		__this->set_eatEvents_13((bool)1);
		return;
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.ForwardEvent::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardEvent_Event_m2780265479 (ForwardEvent_t3275232509 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	FsmEvent_t2133468028 * V_0 = NULL;
	FsmEventU5BU5D_t2862142229* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmEventU5BU5D_t2862142229* L_0 = __this->get_eventsToForward_12();
		if (!L_0)
		{
			goto IL_004a;
		}
	}
	{
		FsmEventU5BU5D_t2862142229* L_1 = __this->get_eventsToForward_12();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0041;
	}

IL_0019:
	{
		FsmEventU5BU5D_t2862142229* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		FsmEvent_t2133468028 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		FsmEvent_t2133468028 * L_6 = V_0;
		FsmEvent_t2133468028 * L_7 = ___fsmEvent0;
		if ((!(((Il2CppObject*)(FsmEvent_t2133468028 *)L_6) == ((Il2CppObject*)(FsmEvent_t2133468028 *)L_7))))
		{
			goto IL_003d;
		}
	}
	{
		Fsm_t1527112426 * L_8 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventTarget_t1823904941 * L_9 = __this->get_forwardTo_11();
		FsmEvent_t2133468028 * L_10 = ___fsmEvent0;
		NullCheck(L_8);
		Fsm_Event_m1295831978(L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = __this->get_eatEvents_13();
		return L_11;
	}

IL_003d:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_13 = V_2;
		FsmEventU5BU5D_t2862142229* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_004a:
	{
		return (bool)0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::.ctor()
extern "C"  void FsmArraySet__ctor_m2176457183 (FsmArraySet_t3475177271 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmArraySet_Reset_m4117857420_MetadataUsageId;
extern "C"  void FsmArraySet_Reset_m4117857420 (FsmArraySet_t3475177271 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmArraySet_Reset_m4117857420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_setValue_14((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnEnter()
extern "C"  void FsmArraySet_OnEnter_m3612975606 (FsmArraySet_t3475177271 * __this, const MethodInfo* method)
{
	{
		FsmArraySet_DoSetFsmString_m2702089055(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::DoSetFsmString()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1087150059;
extern Il2CppCodeGenString* _stringLiteral3008553501;
extern const uint32_t FsmArraySet_DoSetFsmString_m2702089055_MetadataUsageId;
extern "C"  void FsmArraySet_DoSetFsmString_m2702089055 (FsmArraySet_t3475177271 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmArraySet_DoSetFsmString_m2702089055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmString_t952858651 * V_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_setValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0087;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_fsmName_12();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1087150059, L_17, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_18, /*hidden argument*/NULL);
		return;
	}

IL_0087:
	{
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_17();
		NullCheck(L_19);
		FsmVariables_t963491929 * L_20 = PlayMakerFSM_get_FsmVariables_m1986570741(L_19, /*hidden argument*/NULL);
		FsmString_t952858651 * L_21 = __this->get_variableName_13();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmString_t952858651 * L_23 = FsmVariables_GetFsmString_m4055225775(L_20, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmString_t952858651 * L_24 = V_1;
		if (!L_24)
		{
			goto IL_00bf;
		}
	}
	{
		FsmString_t952858651 * L_25 = V_1;
		FsmString_t952858651 * L_26 = __this->get_setValue_14();
		NullCheck(L_26);
		String_t* L_27 = FsmString_get_Value_m872383149(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmString_set_Value_m829393196(L_25, L_27, /*hidden argument*/NULL);
		goto IL_00da;
	}

IL_00bf:
	{
		FsmString_t952858651 * L_28 = __this->get_variableName_13();
		NullCheck(L_28);
		String_t* L_29 = FsmString_get_Value_m872383149(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3008553501, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_30, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnUpdate()
extern "C"  void FsmArraySet_OnUpdate_m3761620749 (FsmArraySet_t3475177271 * __this, const MethodInfo* method)
{
	{
		FsmArraySet_DoSetFsmString_m2702089055(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::.ctor()
extern "C"  void FsmEventOptions__ctor_m3240264292 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmEventOptions_Reset_m886697233_MetadataUsageId;
extern "C"  void FsmEventOptions_Reset_m886697233 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmEventOptions_Reset_m886697233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_sendToFsmComponent_11((PlayMakerFSM_t3799847376 *)NULL);
		__this->set_sendToGameObject_12((FsmGameObject_t1697147867 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_13(L_1);
		FsmBool_t1075959796 * L_2 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_sendToChildren_14(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_broadcastToAll_15(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::OnUpdate()
extern "C"  void FsmEventOptions_OnUpdate_m3075527784 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::.ctor()
extern "C"  void FsmStateActionAnimatorBase__ctor_m1231659039 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::Reset()
extern "C"  void FsmStateActionAnimatorBase_Reset_m3173059276 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method)
{
	{
		__this->set_everyFrame_11((bool)0);
		__this->set_everyFrameOption_12(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnPreprocess()
extern "C"  void FsmStateActionAnimatorBase_OnPreprocess_m3740592176 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_set_HandleAnimatorMove_m2293111747(L_1, (bool)1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		int32_t L_2 = __this->get_everyFrameOption_12();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0030;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Fsm_set_HandleAnimatorIK_m368996724(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnUpdate()
extern "C"  void FsmStateActionAnimatorBase_OnUpdate_m2200803533 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_everyFrameOption_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(60 /* System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnActionUpdate() */, __this);
	}

IL_0011:
	{
		bool L_1 = __this->get_everyFrame_11();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::DoAnimatorMove()
extern "C"  void FsmStateActionAnimatorBase_DoAnimatorMove_m29928200 (FsmStateActionAnimatorBase_t2852864039 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(60 /* System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnActionUpdate() */, __this);
	}

IL_0012:
	{
		bool L_1 = __this->get_everyFrame_11();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::DoAnimatorIK(System.Int32)
extern "C"  void FsmStateActionAnimatorBase_DoAnimatorIK_m4212416842 (FsmStateActionAnimatorBase_t2852864039 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___layerIndex0;
		__this->set_IklayerIndex_13(L_0);
		int32_t L_1 = __this->get_everyFrameOption_12();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0019;
		}
	}
	{
		VirtActionInvoker0::Invoke(60 /* System.Void HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::OnActionUpdate() */, __this);
	}

IL_0019:
	{
		bool L_2 = __this->get_everyFrame_11();
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::.ctor()
extern "C"  void FsmStateSwitch__ctor_m1908935257 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::Reset()
extern Il2CppClass* FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateSwitch_Reset_m3850335494_MetadataUsageId;
extern "C"  void FsmStateSwitch_Reset_m3850335494 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateSwitch_Reset_m3850335494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_fsmName_12((FsmString_t952858651 *)NULL);
		__this->set_compareTo_13(((FsmStringU5BU5D_t2523845914*)SZArrayNew(FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_14(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnEnter()
extern "C"  void FsmStateSwitch_OnEnter_m4222442480 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateSwitch_DoFsmStateSwitch_m3415497723(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnUpdate()
extern "C"  void FsmStateSwitch_OnUpdate_m1180257363 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	{
		FsmStateSwitch_DoFsmStateSwitch_m3415497723(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::DoFsmStateSwitch()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateSwitch_DoFsmStateSwitch_m3415497723_MetadataUsageId;
extern "C"  void FsmStateSwitch_DoFsmStateSwitch_m3415497723 (FsmStateSwitch_t3425333421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateSwitch_DoFsmStateSwitch_m3415497723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GameObject_t3674682005 * L_4 = V_0;
		GameObject_t3674682005 * L_5 = __this->get_previousGo_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmString_t952858651 * L_8 = __this->get_fsmName_12();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_10 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		__this->set_fsm_17(L_10);
		GameObject_t3674682005 * L_11 = V_0;
		__this->set_previousGo_16(L_11);
	}

IL_0048:
	{
		PlayMakerFSM_t3799847376 * L_12 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		NullCheck(L_14);
		String_t* L_15 = PlayMakerFSM_get_ActiveStateName_m1548898773(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		V_2 = 0;
		goto IL_009d;
	}

IL_006d:
	{
		String_t* L_16 = V_1;
		FsmStringU5BU5D_t2523845914* L_17 = __this->get_compareTo_13();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		FsmString_t952858651 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_16, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0099;
		}
	}
	{
		Fsm_t1527112426 * L_23 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_24 = __this->get_sendEvent_14();
		int32_t L_25 = V_2;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		FsmEvent_t2133468028 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_23);
		Fsm_Event_m625948263(L_23, L_27, /*hidden argument*/NULL);
		return;
	}

IL_0099:
	{
		int32_t L_28 = V_2;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_009d:
	{
		int32_t L_29 = V_2;
		FsmStringU5BU5D_t2523845914* L_30 = __this->get_compareTo_13();
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_006d;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::.ctor()
extern "C"  void FsmStateTest__ctor_m3033383227 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::Reset()
extern "C"  void FsmStateTest_Reset_m679816168 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_fsmName_12((FsmString_t952858651 *)NULL);
		__this->set_stateName_13((FsmString_t952858651 *)NULL);
		__this->set_trueEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_16((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnEnter()
extern "C"  void FsmStateTest_OnEnter_m2485183058 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateTest_DoFsmStateTest_m3949619895(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnUpdate()
extern "C"  void FsmStateTest_OnUpdate_m3159790129 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	{
		FsmStateTest_DoFsmStateTest_m3949619895(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::DoFsmStateTest()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t FsmStateTest_DoFsmStateTest_m3949619895_MetadataUsageId;
extern "C"  void FsmStateTest_DoFsmStateTest_m3949619895 (FsmStateTest_t3341384075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FsmStateTest_DoFsmStateTest_m3949619895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GameObject_t3674682005 * L_4 = V_0;
		GameObject_t3674682005 * L_5 = __this->get_previousGo_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmString_t952858651 * L_8 = __this->get_fsmName_12();
		NullCheck(L_8);
		String_t* L_9 = FsmString_get_Value_m872383149(L_8, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_10 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		__this->set_fsm_19(L_10);
		GameObject_t3674682005 * L_11 = V_0;
		__this->set_previousGo_18(L_11);
	}

IL_0048:
	{
		PlayMakerFSM_t3799847376 * L_12 = __this->get_fsm_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		V_1 = (bool)0;
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_19();
		NullCheck(L_14);
		String_t* L_15 = PlayMakerFSM_get_ActiveStateName_m1548898773(L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_stateName_13();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0094;
		}
	}
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = __this->get_trueEvent_14();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
		V_1 = (bool)1;
		goto IL_00a5;
	}

IL_0094:
	{
		Fsm_t1527112426 * L_21 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_22 = __this->get_falseEvent_15();
		NullCheck(L_21);
		Fsm_Event_m625948263(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		FsmBool_t1075959796 * L_23 = __this->get_storeResult_16();
		bool L_24 = V_1;
		NullCheck(L_23);
		FsmBool_set_Value_m1126216340(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::.ctor()
extern "C"  void GameObjectChanged__ctor_m3955321957 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::Reset()
extern "C"  void GameObjectChanged_Reset_m1601754898 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectVariable_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_changedEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnEnter()
extern "C"  void GameObjectChanged_OnEnter_m3705039612 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObjectVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObjectVariable_11();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = FsmGameObject_get_Value_m673294275(L_2, /*hidden argument*/NULL);
		__this->set_previousValue_14(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectChanged_OnUpdate_m2320637639_MetadataUsageId;
extern "C"  void GameObjectChanged_OnUpdate_m2320637639 (GameObjectChanged_t641634289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectChanged_OnUpdate_m2320637639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_13();
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)0, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_1 = __this->get_gameObjectVariable_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = FsmGameObject_get_Value_m673294275(L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_previousValue_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_13();
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_changedEvent_12();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::.ctor()
extern "C"  void GameObjectCompare__ctor_m703166804 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::Reset()
extern "C"  void GameObjectCompare_Reset_m2644567041 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectVariable_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_compareTo_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_equalEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_notEqualEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_15((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnEnter()
extern "C"  void GameObjectCompare_OnEnter_m825161771 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		GameObjectCompare_DoGameObjectCompare_m2271525627(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnUpdate()
extern "C"  void GameObjectCompare_OnUpdate_m3238737784 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	{
		GameObjectCompare_DoGameObjectCompare_m2271525627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::DoGameObjectCompare()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectCompare_DoGameObjectCompare_m2271525627_MetadataUsageId;
extern "C"  void GameObjectCompare_DoGameObjectCompare_m2271525627 (GameObjectCompare_t853174818 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectCompare_DoGameObjectCompare_m2271525627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObjectVariable_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_3 = __this->get_compareTo_12();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = FsmGameObject_get_Value_m673294275(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_15();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0055;
		}
	}
	{
		FsmEvent_t2133468028 * L_9 = __this->get_equalEvent_13();
		if (!L_9)
		{
			goto IL_0055;
		}
	}
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_equalEvent_13();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0077;
	}

IL_0055:
	{
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_0077;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_notEqualEvent_14();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_notEqualEvent_14();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::.ctor()
extern "C"  void GameObjectCompareTag__ctor_m1747131352 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GameObjectCompareTag_Reset_m3688531589_MetadataUsageId;
extern "C"  void GameObjectCompareTag_Reset_m3688531589 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectCompareTag_Reset_m3688531589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_tag_12(L_0);
		__this->set_trueEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_15((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnEnter()
extern "C"  void GameObjectCompareTag_OnEnter_m3347712431 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		GameObjectCompareTag_DoCompareTag_m623402700(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnUpdate()
extern "C"  void GameObjectCompareTag_OnUpdate_m4128396916 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	{
		GameObjectCompareTag_DoCompareTag_m623402700(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::DoCompareTag()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectCompareTag_DoCompareTag_m623402700_MetadataUsageId;
extern "C"  void GameObjectCompareTag_DoCompareTag_m623402700 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectCompareTag_DoCompareTag_m623402700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		V_0 = (bool)0;
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		FsmGameObject_t1697147867 * L_3 = __this->get_gameObject_11();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = FsmGameObject_get_Value_m673294275(L_3, /*hidden argument*/NULL);
		FsmString_t952858651 * L_5 = __this->get_tag_12();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_7 = GameObject_CompareTag_m3153977471(L_4, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0034:
	{
		FsmBool_t1075959796 * L_8 = __this->get_storeResult_15();
		bool L_9 = V_0;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_11 = V_0;
		G_B3_0 = L_10;
		if (!L_11)
		{
			G_B4_0 = L_10;
			goto IL_0057;
		}
	}
	{
		FsmEvent_t2133468028 * L_12 = __this->get_trueEvent_13();
		G_B5_0 = L_12;
		G_B5_1 = G_B3_0;
		goto IL_005d;
	}

IL_0057:
	{
		FsmEvent_t2133468028 * L_13 = __this->get_falseEvent_14();
		G_B5_0 = L_13;
		G_B5_1 = G_B4_0;
	}

IL_005d:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::.ctor()
extern "C"  void GameObjectHasChildren__ctor_m776204032 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::Reset()
extern "C"  void GameObjectHasChildren_Reset_m2717604269 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnEnter()
extern "C"  void GameObjectHasChildren_OnEnter_m2294461143 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		GameObjectHasChildren_DoHasChildren_m1996182732(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnUpdate()
extern "C"  void GameObjectHasChildren_OnUpdate_m1542378060 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	{
		GameObjectHasChildren_DoHasChildren_m1996182732(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::DoHasChildren()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectHasChildren_DoHasChildren_m1996182732_MetadataUsageId;
extern "C"  void GameObjectHasChildren_DoHasChildren_m1996182732 (GameObjectHasChildren_t3474747126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectHasChildren_DoHasChildren_m1996182732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m2107810675(L_6, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_7) > ((int32_t)0))? 1 : 0);
		FsmBool_t1075959796 * L_8 = __this->get_storeResult_14();
		bool L_9 = V_1;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_11 = V_1;
		G_B3_0 = L_10;
		if (!L_11)
		{
			G_B4_0 = L_10;
			goto IL_0051;
		}
	}
	{
		FsmEvent_t2133468028 * L_12 = __this->get_trueEvent_12();
		G_B5_0 = L_12;
		G_B5_1 = G_B3_0;
		goto IL_0057;
	}

IL_0051:
	{
		FsmEvent_t2133468028 * L_13 = __this->get_falseEvent_13();
		G_B5_0 = L_13;
		G_B5_1 = G_B4_0;
	}

IL_0057:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::.ctor()
extern "C"  void GameObjectIsChildOf__ctor_m3409596848 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::Reset()
extern "C"  void GameObjectIsChildOf_Reset_m1056029789 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isChildOf_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_trueEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_15((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::OnEnter()
extern "C"  void GameObjectIsChildOf_OnEnter_m3249219975 (GameObjectIsChildOf_t1849565254 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		GameObjectIsChildOf_DoIsChildOf_m970755908(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::DoIsChildOf(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectIsChildOf_DoIsChildOf_m970755908_MetadataUsageId;
extern "C"  void GameObjectIsChildOf_DoIsChildOf_m970755908 (GameObjectIsChildOf_t1849565254 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsChildOf_DoIsChildOf_m970755908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Fsm_t1527112426 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B4_0 = NULL;
	FsmEvent_t2133468028 * G_B6_0 = NULL;
	Fsm_t1527112426 * G_B6_1 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_isChildOf_12();
		if (L_2)
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		return;
	}

IL_0018:
	{
		GameObject_t3674682005 * L_3 = ___go0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_5 = __this->get_isChildOf_12();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_8 = Transform_IsChildOf_m3321648579(L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		FsmBool_t1075959796 * L_9 = __this->get_storeResult_15();
		bool L_10 = V_0;
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, L_10, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_12 = V_0;
		G_B4_0 = L_11;
		if (!L_12)
		{
			G_B5_0 = L_11;
			goto IL_0057;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_trueEvent_13();
		G_B6_0 = L_13;
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0057:
	{
		FsmEvent_t2133468028 * L_14 = __this->get_falseEvent_14();
		G_B6_0 = L_14;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		Fsm_Event_m625948263(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::.ctor()
extern "C"  void GameObjectIsNull__ctor_m438504188 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::Reset()
extern "C"  void GameObjectIsNull_Reset_m2379904425 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_isNull_12((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotNull_13((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_14((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnEnter()
extern "C"  void GameObjectIsNull_OnEnter_m4182425555 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		GameObjectIsNull_DoIsGameObjectNull_m1826385077(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnUpdate()
extern "C"  void GameObjectIsNull_OnUpdate_m4234699984 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	{
		GameObjectIsNull_DoIsGameObjectNull_m1826385077(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::DoIsGameObjectNull()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectIsNull_DoIsGameObjectNull_m1826385077_MetadataUsageId;
extern "C"  void GameObjectIsNull_DoIsGameObjectNull_m1826385077 (GameObjectIsNull_t2162669226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsNull_DoIsGameObjectNull_m1826385077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Fsm_t1527112426 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B3_0 = NULL;
	FsmEvent_t2133468028 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B5_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmBool_t1075959796 * L_3 = __this->get_storeResult_14();
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		FsmBool_t1075959796 * L_4 = __this->get_storeResult_14();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_7 = V_0;
		G_B3_0 = L_6;
		if (!L_7)
		{
			G_B4_0 = L_6;
			goto IL_0040;
		}
	}
	{
		FsmEvent_t2133468028 * L_8 = __this->get_isNull_12();
		G_B5_0 = L_8;
		G_B5_1 = G_B3_0;
		goto IL_0046;
	}

IL_0040:
	{
		FsmEvent_t2133468028 * L_9 = __this->get_isNotNull_13();
		G_B5_0 = L_9;
		G_B5_1 = G_B4_0;
	}

IL_0046:
	{
		NullCheck(G_B5_1);
		Fsm_Event_m625948263(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t GameObjectIsVisible__ctor_m3292890161_MetadataUsageId;
extern "C"  void GameObjectIsVisible__ctor_m3292890161 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsVisible__ctor_m3292890161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::Reset()
extern "C"  void GameObjectIsVisible_Reset_m939323102 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_trueEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_falseEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_16((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnEnter()
extern "C"  void GameObjectIsVisible_OnEnter_m2763243464 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		GameObjectIsVisible_DoIsVisible_m1769454380(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnUpdate()
extern "C"  void GameObjectIsVisible_OnUpdate_m3189728123 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	{
		GameObjectIsVisible_DoIsVisible_m1769454380(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::DoIsVisible()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t GameObjectIsVisible_DoIsVisible_m1769454380_MetadataUsageId;
extern "C"  void GameObjectIsVisible_DoIsVisible_m1769454380 (GameObjectIsVisible_t1570039973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectIsVisible_DoIsVisible_m1769454380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	bool V_1 = false;
	Fsm_t1527112426 * G_B3_0 = NULL;
	Fsm_t1527112426 * G_B2_0 = NULL;
	FsmEvent_t2133468028 * G_B4_0 = NULL;
	Fsm_t1527112426 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		Renderer_t3076687687 * L_5 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_5);
		bool L_6 = Renderer_get_isVisible_m1011967393(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_storeResult_16();
		bool L_8 = V_1;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		bool L_10 = V_1;
		G_B2_0 = L_9;
		if (!L_10)
		{
			G_B3_0 = L_9;
			goto IL_004d;
		}
	}
	{
		FsmEvent_t2133468028 * L_11 = __this->get_trueEvent_14();
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_0053;
	}

IL_004d:
	{
		FsmEvent_t2133468028 * L_12 = __this->get_falseEvent_15();
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0053:
	{
		NullCheck(G_B4_1);
		Fsm_Event_m625948263(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::.ctor()
extern "C"  void GameObjectTagSwitch__ctor_m2134117227 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::Reset()
extern Il2CppClass* FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectTagSwitch_Reset_m4075517464_MetadataUsageId;
extern "C"  void GameObjectTagSwitch_Reset_m4075517464 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectTagSwitch_Reset_m4075517464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_compareTo_12(((FsmStringU5BU5D_t2523845914*)SZArrayNew(FsmStringU5BU5D_t2523845914_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_sendEvent_13(((FsmEventU5BU5D_t2862142229*)SZArrayNew(FsmEventU5BU5D_t2862142229_il2cpp_TypeInfo_var, (uint32_t)1)));
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnEnter()
extern "C"  void GameObjectTagSwitch_OnEnter_m1578983554 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		GameObjectTagSwitch_DoTagSwitch_m398210284(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnUpdate()
extern "C"  void GameObjectTagSwitch_OnUpdate_m837409281 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	{
		GameObjectTagSwitch_DoTagSwitch_m398210284(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::DoTagSwitch()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectTagSwitch_DoTagSwitch_m398210284_MetadataUsageId;
extern "C"  void GameObjectTagSwitch_DoTagSwitch_m398210284 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObjectTagSwitch_DoTagSwitch_m398210284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t3674682005 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_0055;
	}

IL_0020:
	{
		GameObject_t3674682005 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = GameObject_get_tag_m211612200(L_4, /*hidden argument*/NULL);
		FsmStringU5BU5D_t2523845914* L_6 = __this->get_compareTo_12();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FsmString_t952858651 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0051;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEventU5BU5D_t2862142229* L_13 = __this->get_sendEvent_13();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		FsmEvent_t2133468028 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0051:
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_18 = V_1;
		FsmStringU5BU5D_t2523845914* L_19 = __this->get_compareTo_12();
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetACosine::.ctor()
extern "C"  void GetACosine__ctor_m790708218 (GetACosine_t1733000684 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetACosine::Reset()
extern "C"  void GetACosine_Reset_m2732108455 (GetACosine_t1733000684 * __this, const MethodInfo* method)
{
	{
		__this->set_angle_12((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_13(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_Value_11((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetACosine::OnEnter()
extern "C"  void GetACosine_OnEnter_m3348082001 (GetACosine_t1733000684 * __this, const MethodInfo* method)
{
	{
		GetACosine_DoACosine_m4226920327(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetACosine::OnUpdate()
extern "C"  void GetACosine_OnUpdate_m4139853586 (GetACosine_t1733000684 * __this, const MethodInfo* method)
{
	{
		GetACosine_DoACosine_m4226920327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetACosine::DoACosine()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetACosine_DoACosine_m4226920327_MetadataUsageId;
extern "C"  void GetACosine_DoACosine_m4226920327 (GetACosine_t1733000684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetACosine_DoACosine_m4226920327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_Value_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = acosf(L_1);
		V_0 = L_2;
		FsmBool_t1075959796 * L_3 = __this->get_RadToDeg_13();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		float L_5 = V_0;
		V_0 = ((float)((float)L_5*(float)(57.29578f)));
	}

IL_0029:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_angle_12();
		float L_7 = V_0;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::.ctor()
extern "C"  void GetAngleToTarget__ctor_m1451390901 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t GetAngleToTarget_Reset_m3392791138_MetadataUsageId;
extern "C"  void GetAngleToTarget_Reset_m3392791138 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAngleToTarget_Reset_m3392791138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetObject_12((FsmGameObject_t1697147867 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetPosition_13(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_ignoreHeight_14(L_3);
		__this->set_storeAngle_15((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::OnLateUpdate()
extern "C"  void GetAngleToTarget_OnLateUpdate_m3267609533 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	{
		GetAngleToTarget_DoGetAngleToTarget_m677929539(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::DoGetAngleToTarget()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAngleToTarget_DoGetAngleToTarget_m677929539_MetadataUsageId;
extern "C"  void GetAngleToTarget_DoGetAngleToTarget_m677929539 (GetAngleToTarget_t1034773969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAngleToTarget_DoGetAngleToTarget_m677929539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_targetObject_12();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t3674682005 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_targetPosition_13();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		return;
	}

IL_0048:
	{
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0090;
		}
	}
	{
		FsmVector3_t533912882 * L_13 = __this->get_targetPosition_13();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t3674682005 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_17 = __this->get_targetPosition_13();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = FsmVector3_get_Value_m2779135117(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_19 = Transform_TransformPoint_m437395512(L_16, L_18, /*hidden argument*/NULL);
		G_B9_0 = L_19;
		goto IL_008a;
	}

IL_007f:
	{
		GameObject_t3674682005 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = GameObject_get_transform_m1278640159(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t4282066566  L_22 = Transform_get_position_m2211398607(L_21, /*hidden argument*/NULL);
		G_B9_0 = L_22;
	}

IL_008a:
	{
		V_2 = G_B9_0;
		goto IL_009c;
	}

IL_0090:
	{
		FsmVector3_t533912882 * L_23 = __this->get_targetPosition_13();
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
	}

IL_009c:
	{
		FsmBool_t1075959796 * L_25 = __this->get_ignoreHeight_14();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00c7;
		}
	}
	{
		GameObject_t3674682005 * L_27 = V_0;
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		float L_30 = (&V_4)->get_y_2();
		(&V_2)->set_y_2(L_30);
	}

IL_00c7:
	{
		Vector3_t4282066566  L_31 = V_2;
		GameObject_t3674682005 * L_32 = V_0;
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t4282066566  L_34 = Transform_get_position_m2211398607(L_33, /*hidden argument*/NULL);
		Vector3_t4282066566  L_35 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		FsmFloat_t2134102846 * L_36 = __this->get_storeAngle_15();
		Vector3_t4282066566  L_37 = V_3;
		GameObject_t3674682005 * L_38 = V_0;
		NullCheck(L_38);
		Transform_t1659122786 * L_39 = GameObject_get_transform_m1278640159(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t4282066566  L_40 = Transform_get_forward_m877665793(L_39, /*hidden argument*/NULL);
		float L_41 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_37, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		FsmFloat_set_Value_m1568963140(L_36, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::.ctor()
extern "C"  void GetAnimatorApplyRootMotion__ctor_m1420210421 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::Reset()
extern "C"  void GetAnimatorApplyRootMotion_Reset_m3361610658 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_rootMotionApplied_12((FsmBool_t1075959796 *)NULL);
		__this->set_rootMotionIsAppliedEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_rootMotionIsNotAppliedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorApplyRootMotion_OnEnter_m2709310348_MetadataUsageId;
extern "C"  void GetAnimatorApplyRootMotion_OnEnter_m2709310348 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorApplyRootMotion_OnEnter_m2709310348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::GetApplyMotionRoot()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815_MetadataUsageId;
extern "C"  void GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815 (GetAnimatorApplyRootMotion_t212715153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorApplyRootMotion_GetApplyMotionRoot_m1558234815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_15();
		NullCheck(L_2);
		bool L_3 = Animator_get_applyRootMotion_m2388146907(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_rootMotionApplied_12();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_rootMotionIsAppliedEvent_13();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0045:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_rootMotionIsNotAppliedEvent_14();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::.ctor()
extern "C"  void GetAnimatorBody__ctor_m1903242537 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::Reset()
extern "C"  void GetAnimatorBody_Reset_m3844642774 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_bodyPosition_15((FsmVector3_t533912882 *)NULL);
		__this->set_bodyRotation_16((FsmQuaternion_t3871136040 *)NULL);
		__this->set_bodyGameObject_17((FsmGameObject_t1697147867 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorBody_OnEnter_m3046705856_MetadataUsageId;
extern "C"  void GetAnimatorBody_OnEnter_m3046705856 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBody_OnEnter_m3046705856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_18(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_bodyGameObject_17();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_19(L_14);
	}

IL_006d:
	{
		GetAnimatorBody_DoGetBodyPosition_m4155442973(__this, /*hidden argument*/NULL);
		bool L_15 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnActionUpdate()
extern "C"  void GetAnimatorBody_OnActionUpdate_m450700569 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	{
		GetAnimatorBody_DoGetBodyPosition_m4155442973(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::DoGetBodyPosition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBody_DoGetBodyPosition_m4155442973_MetadataUsageId;
extern "C"  void GetAnimatorBody_DoGetBodyPosition_m4155442973 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBody_DoGetBodyPosition_m4155442973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_bodyPosition_15();
		Animator_t2776330603 * L_3 = __this->get__animator_18();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_bodyPosition_m2404872492(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_bodyRotation_16();
		Animator_t2776330603 * L_6 = __this->get__animator_18();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_bodyRotation_m994115073(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		Transform_t1659122786 * L_8 = __this->get__transform_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		Transform_t1659122786 * L_10 = __this->get__transform_19();
		Animator_t2776330603 * L_11 = __this->get__animator_18();
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Animator_get_bodyPosition_m2404872492(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m3111394108(L_10, L_12, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = __this->get__transform_19();
		Animator_t2776330603 * L_14 = __this->get__animator_18();
		NullCheck(L_14);
		Quaternion_t1553702882  L_15 = Animator_get_bodyRotation_m994115073(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m1525803229(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::.ctor()
extern "C"  void GetAnimatorBoneGameObject__ctor_m891884150 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::Reset()
extern Il2CppClass* HumanBodyBones_t1606609988_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_Reset_m2833284387_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_Reset_m2833284387 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_Reset_m2833284387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		int32_t L_0 = ((int32_t)0);
		Il2CppObject * L_1 = Box(HumanBodyBones_t1606609988_il2cpp_TypeInfo_var, &L_0);
		FsmEnum_t1076048395 * L_2 = FsmEnum_op_Implicit_m4146730367(NULL /*static, unused*/, (Enum_t2862688501 *)L_1, /*hidden argument*/NULL);
		__this->set_bone_12(L_2);
		__this->set_boneGameObject_13((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_OnEnter_m1793904845_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_OnEnter_m1793904845 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_OnEnter_m1793904845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorBoneGameObject_GetBoneTransform_m2228430176(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::GetBoneTransform()
extern Il2CppClass* HumanBodyBones_t1606609988_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBoneGameObject_GetBoneTransform_m2228430176_MetadataUsageId;
extern "C"  void GetAnimatorBoneGameObject_GetBoneTransform_m2228430176 (GetAnimatorBoneGameObject_t14629568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBoneGameObject_GetBoneTransform_m2228430176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_boneGameObject_13();
		Animator_t2776330603 * L_1 = __this->get__animator_14();
		FsmEnum_t1076048395 * L_2 = __this->get_bone_12();
		NullCheck(L_2);
		Enum_t2862688501 * L_3 = FsmEnum_get_Value_m4041924749(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_4 = Animator_GetBoneTransform_m3449809847(L_1, ((*(int32_t*)((int32_t*)UnBox (L_3, HumanBodyBones_t1606609988_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::.ctor()
extern "C"  void GetAnimatorBool__ctor_m1871322337 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::Reset()
extern "C"  void GetAnimatorBool_Reset_m3812722574 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_15((FsmString_t952858651 *)NULL);
		__this->set_result_16((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorBool_OnEnter_m2436164728_MetadataUsageId;
extern "C"  void GetAnimatorBool_OnEnter_m2436164728 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBool_OnEnter_m2436164728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmString_t952858651 * L_9 = __this->get_parameter_15();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		int32_t L_11 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set__paramID_18(L_11);
		GetAnimatorBool_GetParameter_m1210073718(__this, /*hidden argument*/NULL);
		bool L_12 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_12)
		{
			goto IL_0076;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnActionUpdate()
extern "C"  void GetAnimatorBool_OnActionUpdate_m2636801121 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	{
		GetAnimatorBool_GetParameter_m1210073718(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::GetParameter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorBool_GetParameter_m1210073718_MetadataUsageId;
extern "C"  void GetAnimatorBool_GetParameter_m1210073718 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorBool_GetParameter_m1210073718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmBool_t1075959796 * L_2 = __this->get_result_16();
		Animator_t2776330603 * L_3 = __this->get__animator_17();
		int32_t L_4 = __this->get__paramID_18();
		NullCheck(L_3);
		bool L_5 = Animator_GetBool_m1246282447(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmBool_set_Value_m1126216340(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::.ctor()
extern "C"  void GetAnimatorCullingMode__ctor_m1074548200 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::Reset()
extern "C"  void GetAnimatorCullingMode_Reset_m3015948437 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_alwaysAnimate_12((FsmBool_t1075959796 *)NULL);
		__this->set_alwaysAnimateEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_basedOnRenderersEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCullingMode_OnEnter_m1240397759_MetadataUsageId;
extern "C"  void GetAnimatorCullingMode_OnEnter_m1240397759 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCullingMode_OnEnter_m1240397759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCullingMode_DoCheckCulling_m2645242639(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::DoCheckCulling()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCullingMode_DoCheckCulling_m2645242639_MetadataUsageId;
extern "C"  void GetAnimatorCullingMode_DoCheckCulling_m2645242639 (GetAnimatorCullingMode_t994674750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCullingMode_DoCheckCulling_m2645242639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_15();
		NullCheck(L_2);
		int32_t L_3 = Animator_get_cullingMode_m1008819440(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_0029;
	}

IL_0028:
	{
		G_B5_0 = 0;
	}

IL_0029:
	{
		V_0 = (bool)G_B5_0;
		FsmBool_t1075959796 * L_4 = __this->get_alwaysAnimate_12();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_alwaysAnimateEvent_13();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0052:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_basedOnRenderersEvent_14();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::.ctor()
extern "C"  void GetAnimatorCurrentStateInfo__ctor_m3574624229 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::Reset()
extern "C"  void GetAnimatorCurrentStateInfo_Reset_m1221057170 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_name_16((FsmString_t952858651 *)NULL);
		__this->set_nameHash_17((FsmInt_t1596138449 *)NULL);
		__this->set_fullPathHash_18((FsmInt_t1596138449 *)NULL);
		__this->set_shortPathHash_19((FsmInt_t1596138449 *)NULL);
		__this->set_tagHash_20((FsmInt_t1596138449 *)NULL);
		__this->set_length_22((FsmFloat_t2134102846 *)NULL);
		__this->set_normalizedTime_23((FsmFloat_t2134102846 *)NULL);
		__this->set_isStateLooping_21((FsmBool_t1075959796 *)NULL);
		__this->set_loopCount_24((FsmInt_t1596138449 *)NULL);
		__this->set_currentLoopProgress_25((FsmFloat_t2134102846 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfo_OnEnter_m2926743164_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfo_OnEnter_m2926743164 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfo_OnEnter_m2926743164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_26(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfo_OnActionUpdate_m2939631069 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::GetLayerInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032 (GetAnimatorCurrentStateInfo_t2823805553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfo_GetLayerInfo_m2468317032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_019f;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_26();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmInt_t1596138449 * L_6 = __this->get_fullPathHash_18();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		FsmInt_t1596138449 * L_8 = __this->get_fullPathHash_18();
		int32_t L_9 = AnimatorStateInfo_get_fullPathHash_m3257074542((&V_0), /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmInt_set_Value_m2087583461(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004a:
	{
		FsmInt_t1596138449 * L_10 = __this->get_shortPathHash_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006c;
		}
	}
	{
		FsmInt_t1596138449 * L_12 = __this->get_shortPathHash_19();
		int32_t L_13 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmInt_set_Value_m2087583461(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006c:
	{
		FsmInt_t1596138449 * L_14 = __this->get_nameHash_17();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008e;
		}
	}
	{
		FsmInt_t1596138449 * L_16 = __this->get_nameHash_17();
		int32_t L_17 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmInt_set_Value_m2087583461(L_16, L_17, /*hidden argument*/NULL);
	}

IL_008e:
	{
		FsmString_t952858651 * L_18 = __this->get_name_16();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00bf;
		}
	}
	{
		FsmString_t952858651 * L_20 = __this->get_name_16();
		Animator_t2776330603 * L_21 = __this->get__animator_26();
		FsmInt_t1596138449 * L_22 = __this->get_layerIndex_15();
		NullCheck(L_22);
		int32_t L_23 = FsmInt_get_Value_m27059446(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_24 = Animator_GetLayerName_m3480300056(L_21, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmString_set_Value_m829393196(L_20, L_24, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		FsmInt_t1596138449 * L_25 = __this->get_tagHash_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00e1;
		}
	}
	{
		FsmInt_t1596138449 * L_27 = __this->get_tagHash_20();
		int32_t L_28 = AnimatorStateInfo_get_tagHash_m3543262078((&V_0), /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmInt_set_Value_m2087583461(L_27, L_28, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		FsmFloat_t2134102846 * L_29 = __this->get_length_22();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_0103;
		}
	}
	{
		FsmFloat_t2134102846 * L_31 = __this->get_length_22();
		float L_32 = AnimatorStateInfo_get_length_m3147284742((&V_0), /*hidden argument*/NULL);
		NullCheck(L_31);
		FsmFloat_set_Value_m1568963140(L_31, L_32, /*hidden argument*/NULL);
	}

IL_0103:
	{
		FsmBool_t1075959796 * L_33 = __this->get_isStateLooping_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0125;
		}
	}
	{
		FsmBool_t1075959796 * L_35 = __this->get_isStateLooping_21();
		bool L_36 = AnimatorStateInfo_get_loop_m1495892586((&V_0), /*hidden argument*/NULL);
		NullCheck(L_35);
		FsmBool_set_Value_m1126216340(L_35, L_36, /*hidden argument*/NULL);
	}

IL_0125:
	{
		FsmFloat_t2134102846 * L_37 = __this->get_normalizedTime_23();
		NullCheck(L_37);
		bool L_38 = NamedVariable_get_IsNone_m281035543(L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_0147;
		}
	}
	{
		FsmFloat_t2134102846 * L_39 = __this->get_normalizedTime_23();
		float L_40 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		NullCheck(L_39);
		FsmFloat_set_Value_m1568963140(L_39, L_40, /*hidden argument*/NULL);
	}

IL_0147:
	{
		FsmInt_t1596138449 * L_41 = __this->get_loopCount_24();
		NullCheck(L_41);
		bool L_42 = NamedVariable_get_IsNone_m281035543(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0167;
		}
	}
	{
		FsmFloat_t2134102846 * L_43 = __this->get_currentLoopProgress_25();
		NullCheck(L_43);
		bool L_44 = NamedVariable_get_IsNone_m281035543(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_019f;
		}
	}

IL_0167:
	{
		FsmInt_t1596138449 * L_45 = __this->get_loopCount_24();
		float L_46 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		double L_47 = Math_Truncate_m534017384(NULL /*static, unused*/, (((double)((double)L_46))), /*hidden argument*/NULL);
		NullCheck(L_45);
		FsmInt_set_Value_m2087583461(L_45, (((int32_t)((int32_t)L_47))), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_48 = __this->get_currentLoopProgress_25();
		float L_49 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_50 = __this->get_loopCount_24();
		NullCheck(L_50);
		int32_t L_51 = FsmInt_get_Value_m27059446(L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		FsmFloat_set_Value_m1568963140(L_48, ((float)((float)L_49-(float)(((float)((float)L_51))))), /*hidden argument*/NULL);
	}

IL_019f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsName__ctor_m934420240 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsName_Reset_m2875820477 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_name_16((FsmString_t952858651 *)NULL);
		__this->set_nameMatchEvent_18((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_19((FsmEvent_t2133468028 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsName_OnEnter_m4016381671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_20(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentStateInfoIsName_IsName_m3222692105(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnActionUpdate_m3999295250 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentStateInfoIsName_IsName_m3222692105(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::IsName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsName_IsName_m3222692105_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsName_IsName_m3222692105 (GetAnimatorCurrentStateInfoIsName_t2852409574 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsName_IsName_m3222692105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_20();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_isMatching_17();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0055;
		}
	}
	{
		FsmBool_t1075959796 * L_8 = __this->get_isMatching_17();
		FsmString_t952858651 * L_9 = __this->get_name_16();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		bool L_11 = AnimatorStateInfo_IsName_m1653922768((&V_0), L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0055:
	{
		FsmString_t952858651 * L_12 = __this->get_name_16();
		NullCheck(L_12);
		String_t* L_13 = FsmString_get_Value_m872383149(L_12, /*hidden argument*/NULL);
		bool L_14 = AnimatorStateInfo_IsName_m1653922768((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_16 = __this->get_nameMatchEvent_18();
		NullCheck(L_15);
		Fsm_Event_m625948263(L_15, L_16, /*hidden argument*/NULL);
		goto IL_0093;
	}

IL_0082:
	{
		Fsm_t1527112426 * L_17 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_18 = __this->get_nameDoNotMatchEvent_19();
		NullCheck(L_17);
		Fsm_Event_m625948263(L_17, L_18, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsTag__ctor_m3945847409 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_Reset_m1592280350 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_tag_16((FsmString_t952858651 *)NULL);
		__this->set_tagMatch_17((FsmBool_t1075959796 *)NULL);
		__this->set_tagMatchEvent_18((FsmEvent_t2133468028 *)NULL);
		__this->set_tagDoNotMatchEvent_19((FsmEvent_t2133468028 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_20(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnActionUpdate_m2115496145 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::IsTag()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119_MetadataUsageId;
extern "C"  void GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_20();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_tag_16();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorStateInfo_IsTag_m119936877((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_tagMatch_17();
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_tagMatchEvent_18();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_12 = __this->get_tagMatch_17();
		NullCheck(L_12);
		FsmBool_set_Value_m1126216340(L_12, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_tagDoNotMatchEvent_19();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfo__ctor_m3945219967 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfo_Reset_m1591652908 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_name_16((FsmString_t952858651 *)NULL);
		__this->set_nameHash_17((FsmInt_t1596138449 *)NULL);
		__this->set_userNameHash_18((FsmInt_t1596138449 *)NULL);
		__this->set_normalizedTime_19((FsmFloat_t2134102846 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfo_OnEnter_m2586961814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_20(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnActionUpdate_m3020966787 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::GetTransitionInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558 (GetAnimatorCurrentTransitionInfo_t114989767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m3403661558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_20();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_name_16();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0059;
		}
	}
	{
		FsmString_t952858651 * L_8 = __this->get_name_16();
		Animator_t2776330603 * L_9 = __this->get__animator_20();
		FsmInt_t1596138449 * L_10 = __this->get_layerIndex_15();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_12 = Animator_GetLayerName_m3480300056(L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmString_set_Value_m829393196(L_8, L_12, /*hidden argument*/NULL);
	}

IL_0059:
	{
		FsmInt_t1596138449 * L_13 = __this->get_nameHash_17();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_007b;
		}
	}
	{
		FsmInt_t1596138449 * L_15 = __this->get_nameHash_17();
		int32_t L_16 = AnimatorTransitionInfo_get_nameHash_m2102867203((&V_0), /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmInt_set_Value_m2087583461(L_15, L_16, /*hidden argument*/NULL);
	}

IL_007b:
	{
		FsmInt_t1596138449 * L_17 = __this->get_userNameHash_18();
		NullCheck(L_17);
		bool L_18 = NamedVariable_get_IsNone_m281035543(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_009d;
		}
	}
	{
		FsmInt_t1596138449 * L_19 = __this->get_userNameHash_18();
		int32_t L_20 = AnimatorTransitionInfo_get_userNameHash_m249220782((&V_0), /*hidden argument*/NULL);
		NullCheck(L_19);
		FsmInt_set_Value_m2087583461(L_19, L_20, /*hidden argument*/NULL);
	}

IL_009d:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_normalizedTime_19();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00bf;
		}
	}
	{
		FsmFloat_t2134102846 * L_23 = __this->get_normalizedTime_19();
		float L_24 = AnimatorTransitionInfo_get_normalizedTime_m3258684986((&V_0), /*hidden argument*/NULL);
		NullCheck(L_23);
		FsmFloat_set_Value_m1568963140(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName__ctor_m327899946 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_Reset_m2269300183 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_name_16((FsmString_t952858651 *)NULL);
		__this->set_nameMatch_17((FsmBool_t1075959796 *)NULL);
		__this->set_nameMatchEvent_18((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_19((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsName_OnEnter_m970964097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_20(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnActionUpdate_m1553999928 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::IsName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175 (GetAnimatorCurrentTransitionInfoIsName_t3939956924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsName_IsName_m1600432175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_20();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_name_16();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorTransitionInfo_IsName_m1283663078((&V_0), L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_nameMatch_17();
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_nameMatchEvent_18();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0061:
	{
		FsmBool_t1075959796 * L_12 = __this->get_nameMatch_17();
		NullCheck(L_12);
		FsmBool_set_Value_m1126216340(L_12, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_nameDoNotMatchEvent_19();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName__ctor_m3001526303 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_Reset_m647959244 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_userName_16((FsmString_t952858651 *)NULL);
		__this->set_nameMatch_17((FsmBool_t1075959796 *)NULL);
		__this->set_nameMatchEvent_18((FsmEvent_t2133468028 *)NULL);
		__this->set_nameDoNotMatchEvent_19((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m1935450166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_20(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnActionUpdate_m3701645027 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	{
		GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::IsName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618_MetadataUsageId;
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618 (GetAnimatorCurrentTransitionInfoIsUserName_t1580140583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorCurrentTransitionInfoIsUserName_IsName_m2878470618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorTransitionInfo_t2817229998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_20();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorTransitionInfo_t2817229998  L_5 = Animator_GetAnimatorTransitionInfo_m3858104711(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmString_t952858651 * L_6 = __this->get_userName_16();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		bool L_8 = AnimatorTransitionInfo_IsUserName_m2732197659((&V_0), L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		FsmBool_t1075959796 * L_9 = __this->get_nameMatch_17();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0057;
		}
	}
	{
		FsmBool_t1075959796 * L_11 = __this->get_nameMatch_17();
		bool L_12 = V_1;
		NullCheck(L_11);
		FsmBool_set_Value_m1126216340(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0057:
	{
		bool L_13 = V_1;
		if (!L_13)
		{
			goto IL_0073;
		}
	}
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_nameMatchEvent_18();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_0073:
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_nameDoNotMatchEvent_19();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::.ctor()
extern "C"  void GetAnimatorDelta__ctor_m2311017667 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::Reset()
extern "C"  void GetAnimatorDelta_Reset_m4252417904 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_deltaPosition_15((FsmVector3_t533912882 *)NULL);
		__this->set_deltaRotation_16((FsmQuaternion_t3871136040 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorDelta_OnEnter_m4076581850_MetadataUsageId;
extern "C"  void GetAnimatorDelta_OnEnter_m4076581850 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorDelta_OnEnter_m4076581850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_18(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorDelta_DoGetDeltaPosition_m84291223(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnActionUpdate()
extern "C"  void GetAnimatorDelta_OnActionUpdate_m2662918847 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	{
		GetAnimatorDelta_DoGetDeltaPosition_m84291223(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::DoGetDeltaPosition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorDelta_DoGetDeltaPosition_m84291223_MetadataUsageId;
extern "C"  void GetAnimatorDelta_DoGetDeltaPosition_m84291223 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorDelta_DoGetDeltaPosition_m84291223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_deltaPosition_15();
		Animator_t2776330603 * L_3 = __this->get__animator_18();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_deltaPosition_m1658225602(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_deltaRotation_16();
		Animator_t2776330603 * L_6 = __this->get__animator_18();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_deltaRotation_m1583110423(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::.ctor()
extern "C"  void GetAnimatorFeetPivotActive__ctor_m502507873 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::Reset()
extern "C"  void GetAnimatorFeetPivotActive_Reset_m2443908110 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_feetPivotActive_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorFeetPivotActive_OnEnter_m1265457400_MetadataUsageId;
extern "C"  void GetAnimatorFeetPivotActive_OnEnter_m1265457400 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFeetPivotActive_OnEnter_m1265457400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::DoGetFeetPivotActive()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530_MetadataUsageId;
extern "C"  void GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530 (GetAnimatorFeetPivotActive_t705651877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m3179232530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_feetPivotActive_12();
		Animator_t2776330603 * L_3 = __this->get__animator_13();
		NullCheck(L_3);
		float L_4 = Animator_get_feetPivotActive_m4087239689(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::.ctor()
extern "C"  void GetAnimatorFloat__ctor_m4271842975 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::Reset()
extern "C"  void GetAnimatorFloat_Reset_m1918275916 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_15((FsmString_t952858651 *)NULL);
		__this->set_result_16((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorFloat_OnEnter_m2939059894_MetadataUsageId;
extern "C"  void GetAnimatorFloat_OnEnter_m2939059894 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFloat_OnEnter_m2939059894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmString_t952858651 * L_9 = __this->get_parameter_15();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		int32_t L_11 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set__paramID_18(L_11);
		GetAnimatorFloat_GetParameter_m2334025464(__this, /*hidden argument*/NULL);
		bool L_12 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_12)
		{
			goto IL_0076;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnActionUpdate()
extern "C"  void GetAnimatorFloat_OnActionUpdate_m422670435 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	{
		GetAnimatorFloat_GetParameter_m2334025464(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::GetParameter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorFloat_GetParameter_m2334025464_MetadataUsageId;
extern "C"  void GetAnimatorFloat_GetParameter_m2334025464 (GetAnimatorFloat_t948332455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorFloat_GetParameter_m2334025464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_result_16();
		Animator_t2776330603 * L_3 = __this->get__animator_17();
		int32_t L_4 = __this->get__paramID_18();
		NullCheck(L_3);
		float L_5 = Animator_GetFloat_m2965884705(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::.ctor()
extern "C"  void GetAnimatorGravityWeight__ctor_m2113375701 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::Reset()
extern "C"  void GetAnimatorGravityWeight_Reset_m4054775938 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_gravityWeight_15((FsmFloat_t2134102846 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorGravityWeight_OnEnter_m3121213548_MetadataUsageId;
extern "C"  void GetAnimatorGravityWeight_OnEnter_m3121213548 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorGravityWeight_OnEnter_m3121213548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnActionUpdate()
extern "C"  void GetAnimatorGravityWeight_OnActionUpdate_m1893385709 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	{
		GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::DoGetGravityWeight()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922_MetadataUsageId;
extern "C"  void GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922 (GetAnimatorGravityWeight_t3107738545 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorGravityWeight_DoGetGravityWeight_m3454642922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_gravityWeight_15();
		Animator_t2776330603 * L_3 = __this->get__animator_16();
		NullCheck(L_3);
		float L_4 = Animator_get_gravityWeight_m1393695637(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::.ctor()
extern "C"  void GetAnimatorHumanScale__ctor_m3985010446 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::Reset()
extern "C"  void GetAnimatorHumanScale_Reset_m1631443387 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_humanScale_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorHumanScale_OnEnter_m2170906469_MetadataUsageId;
extern "C"  void GetAnimatorHumanScale_OnEnter_m2170906469 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorHumanScale_OnEnter_m2170906469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorHumanScale_DoGetHumanScale_m527444404(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::DoGetHumanScale()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorHumanScale_DoGetHumanScale_m527444404_MetadataUsageId;
extern "C"  void GetAnimatorHumanScale_DoGetHumanScale_m527444404 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorHumanScale_DoGetHumanScale_m527444404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_humanScale_12();
		Animator_t2776330603 * L_3 = __this->get__animator_13();
		NullCheck(L_3);
		float L_4 = Animator_get_humanScale_m13697776(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::.ctor()
extern "C"  void GetAnimatorIKGoal__ctor_m1678076662 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::Reset()
extern "C"  void GetAnimatorIKGoal_Reset_m3619476899 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_iKGoal_15((FsmEnum_t1076048395 *)NULL);
		__this->set_goal_16((FsmGameObject_t1697147867 *)NULL);
		__this->set_position_17((FsmVector3_t533912882 *)NULL);
		__this->set_rotation_18((FsmQuaternion_t3871136040 *)NULL);
		__this->set_positionWeight_19((FsmFloat_t2134102846 *)NULL);
		__this->set_rotationWeight_20((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIKGoal_OnEnter_m1410664781_MetadataUsageId;
extern "C"  void GetAnimatorIKGoal_OnEnter_m1410664781 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIKGoal_OnEnter_m1410664781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_21(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_goal_16();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_22(L_14);
	}

IL_006d:
	{
		GetAnimatorIKGoal_DoGetIKGoal_m3978612660(__this, /*hidden argument*/NULL);
		bool L_15 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnActionUpdate()
extern "C"  void GetAnimatorIKGoal_OnActionUpdate_m3985448428 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	{
		GetAnimatorIKGoal_DoGetIKGoal_m3978612660(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::DoGetIKGoal()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* AvatarIKGoal_t2036631794_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorIKGoal_DoGetIKGoal_m3978612660_MetadataUsageId;
extern "C"  void GetAnimatorIKGoal_DoGetIKGoal_m3978612660 (GetAnimatorIKGoal_t3033774656 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIKGoal_DoGetIKGoal_m3978612660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmEnum_t1076048395 * L_2 = __this->get_iKGoal_15();
		NullCheck(L_2);
		Enum_t2862688501 * L_3 = FsmEnum_get_Value_m4041924749(L_2, /*hidden argument*/NULL);
		__this->set__iKGoal_23(((*(int32_t*)((int32_t*)UnBox (L_3, AvatarIKGoal_t2036631794_il2cpp_TypeInfo_var)))));
		Transform_t1659122786 * L_4 = __this->get__transform_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		Transform_t1659122786 * L_6 = __this->get__transform_22();
		Animator_t2776330603 * L_7 = __this->get__animator_21();
		int32_t L_8 = __this->get__iKGoal_23();
		NullCheck(L_7);
		Vector3_t4282066566  L_9 = Animator_GetIKPosition_m385128518(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_m3111394108(L_6, L_9, /*hidden argument*/NULL);
		Transform_t1659122786 * L_10 = __this->get__transform_22();
		Animator_t2776330603 * L_11 = __this->get__animator_21();
		int32_t L_12 = __this->get__iKGoal_23();
		NullCheck(L_11);
		Quaternion_t1553702882  L_13 = Animator_GetIKRotation_m3297713819(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m1525803229(L_10, L_13, /*hidden argument*/NULL);
	}

IL_0071:
	{
		FsmVector3_t533912882 * L_14 = __this->get_position_17();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_009d;
		}
	}
	{
		FsmVector3_t533912882 * L_16 = __this->get_position_17();
		Animator_t2776330603 * L_17 = __this->get__animator_21();
		int32_t L_18 = __this->get__iKGoal_23();
		NullCheck(L_17);
		Vector3_t4282066566  L_19 = Animator_GetIKPosition_m385128518(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmVector3_set_Value_m716982822(L_16, L_19, /*hidden argument*/NULL);
	}

IL_009d:
	{
		FsmQuaternion_t3871136040 * L_20 = __this->get_rotation_18();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00c9;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_22 = __this->get_rotation_18();
		Animator_t2776330603 * L_23 = __this->get__animator_21();
		int32_t L_24 = __this->get__iKGoal_23();
		NullCheck(L_23);
		Quaternion_t1553702882  L_25 = Animator_GetIKRotation_m3297713819(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmQuaternion_set_Value_m446581172(L_22, L_25, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		FsmFloat_t2134102846 * L_26 = __this->get_positionWeight_19();
		NullCheck(L_26);
		bool L_27 = NamedVariable_get_IsNone_m281035543(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00f5;
		}
	}
	{
		FsmFloat_t2134102846 * L_28 = __this->get_positionWeight_19();
		Animator_t2776330603 * L_29 = __this->get__animator_21();
		int32_t L_30 = __this->get__iKGoal_23();
		NullCheck(L_29);
		float L_31 = Animator_GetIKPositionWeight_m2079943564(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		FsmFloat_set_Value_m1568963140(L_28, L_31, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		FsmFloat_t2134102846 * L_32 = __this->get_rotationWeight_20();
		NullCheck(L_32);
		bool L_33 = NamedVariable_get_IsNone_m281035543(L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_0121;
		}
	}
	{
		FsmFloat_t2134102846 * L_34 = __this->get_rotationWeight_20();
		Animator_t2776330603 * L_35 = __this->get__animator_21();
		int32_t L_36 = __this->get__iKGoal_23();
		NullCheck(L_35);
		float L_37 = Animator_GetIKRotationWeight_m3997781473(L_35, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		FsmFloat_set_Value_m1568963140(L_34, L_37, /*hidden argument*/NULL);
	}

IL_0121:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::.ctor()
extern "C"  void GetAnimatorInt__ctor_m2122261612 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::Reset()
extern "C"  void GetAnimatorInt_Reset_m4063661849 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameter_15((FsmString_t952858651 *)NULL);
		__this->set_result_16((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorInt_OnEnter_m3070639427_MetadataUsageId;
extern "C"  void GetAnimatorInt_OnEnter_m3070639427 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorInt_OnEnter_m3070639427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmString_t952858651 * L_9 = __this->get_parameter_15();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		int32_t L_11 = Animator_StringToHash_m4020897098(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set__paramID_18(L_11);
		GetAnimatorInt_GetParameter_m4211652747(__this, /*hidden argument*/NULL);
		bool L_12 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_12)
		{
			goto IL_0076;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnActionUpdate()
extern "C"  void GetAnimatorInt_OnActionUpdate_m936225078 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	{
		GetAnimatorInt_GetParameter_m4211652747(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::GetParameter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorInt_GetParameter_m4211652747_MetadataUsageId;
extern "C"  void GetAnimatorInt_GetParameter_m4211652747 (GetAnimatorInt_t3593545018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorInt_GetParameter_m4211652747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_result_16();
		Animator_t2776330603 * L_3 = __this->get__animator_17();
		int32_t L_4 = __this->get__paramID_18();
		NullCheck(L_3);
		int32_t L_5 = Animator_GetInteger_m3944178743(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_5, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::.ctor()
extern "C"  void GetAnimatorIsHuman__ctor_m57879736 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::Reset()
extern "C"  void GetAnimatorIsHuman_Reset_m1999279973 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isHuman_12((FsmBool_t1075959796 *)NULL);
		__this->set_isHumanEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_isGenericEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsHuman_OnEnter_m3474547343_MetadataUsageId;
extern "C"  void GetAnimatorIsHuman_OnEnter_m3474547343 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsHuman_OnEnter_m3474547343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsHuman_DoCheckIsHuman_m3251639922(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::DoCheckIsHuman()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorIsHuman_DoCheckIsHuman_m3251639922_MetadataUsageId;
extern "C"  void GetAnimatorIsHuman_DoCheckIsHuman_m3251639922 (GetAnimatorIsHuman_t55053166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsHuman_DoCheckIsHuman_m3251639922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_15();
		NullCheck(L_2);
		bool L_3 = Animator_get_isHuman_m4030001272(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_isHuman_12();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003a;
		}
	}
	{
		FsmBool_t1075959796 * L_6 = __this->get_isHuman_12();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003a:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_isHumanEvent_13();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0067;
	}

IL_0056:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isGenericEvent_14();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::.ctor()
extern "C"  void GetAnimatorIsLayerInTransition__ctor_m3017439322 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::Reset()
extern "C"  void GetAnimatorIsLayerInTransition_Reset_m663872263 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isInTransition_16((FsmBool_t1075959796 *)NULL);
		__this->set_isInTransitionEvent_17((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotInTransitionEvent_18((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsLayerInTransition_OnEnter_m47992241_MetadataUsageId;
extern "C"  void GetAnimatorIsLayerInTransition_OnEnter_m47992241 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsLayerInTransition_OnEnter_m47992241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_19(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnActionUpdate()
extern "C"  void GetAnimatorIsLayerInTransition_OnActionUpdate_m3301368072 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	{
		GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::DoCheckIsInTransition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465_MetadataUsageId;
extern "C"  void GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_19();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Animator_IsInTransition_m2609196857(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_isInTransition_16();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		FsmBool_t1075959796 * L_8 = __this->get_isInTransition_16();
		bool L_9 = V_0;
		NullCheck(L_8);
		FsmBool_set_Value_m1126216340(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0045:
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0061;
		}
	}
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isInTransitionEvent_17();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0061:
	{
		Fsm_t1527112426 * L_13 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_14 = __this->get_isNotInTransitionEvent_18();
		NullCheck(L_13);
		Fsm_Event_m625948263(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::.ctor()
extern "C"  void GetAnimatorIsMatchingTarget__ctor_m669529043 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::Reset()
extern "C"  void GetAnimatorIsMatchingTarget_Reset_m2610929280 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isMatchingActive_15((FsmBool_t1075959796 *)NULL);
		__this->set_matchingActivatedEvent_16((FsmEvent_t2133468028 *)NULL);
		__this->set_matchingDeactivedEvent_17((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsMatchingTarget_OnEnter_m2859011818_MetadataUsageId;
extern "C"  void GetAnimatorIsMatchingTarget_OnEnter_m2859011818 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsMatchingTarget_OnEnter_m2859011818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_18(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnActionUpdate()
extern "C"  void GetAnimatorIsMatchingTarget_OnActionUpdate_m2880568751 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	{
		GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::DoCheckIsMatchingActive()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091_MetadataUsageId;
extern "C"  void GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_18();
		NullCheck(L_2);
		bool L_3 = Animator_get_isMatchingTarget_m3696235301(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_isMatchingActive_15();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_matchingActivatedEvent_16();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_matchingDeactivedEvent_17();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::.ctor()
extern "C"  void GetAnimatorIsParameterControlledByCurve__ctor_m1539452256 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::Reset()
extern "C"  void GetAnimatorIsParameterControlledByCurve_Reset_m3480852493 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_parameterName_12((FsmString_t952858651 *)NULL);
		__this->set_isControlledByCurve_13((FsmBool_t1075959796 *)NULL);
		__this->set_isControlledByCurveEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotControlledByCurveEvent_15((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791_MetadataUsageId;
extern "C"  void GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsParameterControlledByCurve_OnEnter_m1336596791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::DoCheckIsParameterControlledByCurve()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670_MetadataUsageId;
extern "C"  void GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670 (GetAnimatorIsParameterControlledByCurve_t1394563734 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m4108133670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_16();
		FsmString_t952858651 * L_3 = __this->get_parameterName_12();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Animator_IsParameterControlledByCurve_m3328157587(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmBool_t1075959796 * L_6 = __this->get_isControlledByCurve_13();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_isControlledByCurveEvent_14();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0051:
	{
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_12 = __this->get_isNotControlledByCurveEvent_15();
		NullCheck(L_11);
		Fsm_Event_m625948263(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::.ctor()
extern "C"  void GetAnimatorLayerCount__ctor_m2729802253 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::Reset()
extern "C"  void GetAnimatorLayerCount_Reset_m376235194 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerCount_12((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayerCount_OnEnter_m2801643172_MetadataUsageId;
extern "C"  void GetAnimatorLayerCount_OnEnter_m2801643172 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerCount_OnEnter_m2801643172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_13(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayerCount_DoGetLayerCount_m1288341460(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::DoGetLayerCount()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorLayerCount_DoGetLayerCount_m1288341460_MetadataUsageId;
extern "C"  void GetAnimatorLayerCount_DoGetLayerCount_m1288341460 (GetAnimatorLayerCount_t3712536905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerCount_DoGetLayerCount_m1288341460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmInt_t1596138449 * L_2 = __this->get_layerCount_12();
		Animator_t2776330603 * L_3 = __this->get__animator_13();
		NullCheck(L_3);
		int32_t L_4 = Animator_get_layerCount_m3326924613(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::.ctor()
extern "C"  void GetAnimatorLayerName__ctor_m3972242815 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::Reset()
extern "C"  void GetAnimatorLayerName_Reset_m1618675756 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_12((FsmInt_t1596138449 *)NULL);
		__this->set_layerName_13((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayerName_OnEnter_m2786114966_MetadataUsageId;
extern "C"  void GetAnimatorLayerName_OnEnter_m2786114966 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerName_OnEnter_m2786114966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayerName_DoGetLayerName_m639249302(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::DoGetLayerName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorLayerName_DoGetLayerName_m639249302_MetadataUsageId;
extern "C"  void GetAnimatorLayerName_DoGetLayerName_m639249302 (GetAnimatorLayerName_t4020332743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerName_DoGetLayerName_m639249302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmString_t952858651 * L_2 = __this->get_layerName_13();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		FsmInt_t1596138449 * L_4 = __this->get_layerIndex_12();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_6 = Animator_GetLayerName_m3480300056(L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::.ctor()
extern "C"  void GetAnimatorLayersAffectMassCenter__ctor_m1837935563 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::Reset()
extern "C"  void GetAnimatorLayersAffectMassCenter_Reset_m3779335800 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_affectMassCenter_12((FsmBool_t1075959796 *)NULL);
		__this->set_affectMassCenterEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_doNotAffectMassCenterEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayersAffectMassCenter_OnEnter_m416245986_MetadataUsageId;
extern "C"  void GetAnimatorLayersAffectMassCenter_OnEnter_m416245986 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayersAffectMassCenter_OnEnter_m416245986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_15(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::CheckAffectMassCenter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295_MetadataUsageId;
extern "C"  void GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295 (GetAnimatorLayersAffectMassCenter_t2244244939 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayersAffectMassCenter_CheckAffectMassCenter_m3496781295_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t2776330603 * L_2 = __this->get__animator_15();
		NullCheck(L_2);
		bool L_3 = Animator_get_layersAffectMassCenter_m3409296173(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmBool_t1075959796 * L_4 = __this->get_affectMassCenter_12();
		bool L_5 = V_0;
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = __this->get_affectMassCenterEvent_13();
		NullCheck(L_7);
		Fsm_Event_m625948263(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		Fsm_t1527112426 * L_9 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_10 = __this->get_doNotAffectMassCenterEvent_14();
		NullCheck(L_9);
		Fsm_Event_m625948263(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::.ctor()
extern "C"  void GetAnimatorLayerWeight__ctor_m2928099954 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::Reset()
extern "C"  void GetAnimatorLayerWeight_Reset_m574532895 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_layerWeight_16((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLayerWeight_OnEnter_m92205513_MetadataUsageId;
extern "C"  void GetAnimatorLayerWeight_OnEnter_m92205513 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerWeight_OnEnter_m92205513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLayerWeight_GetLayerWeight_m1458072709(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnActionUpdate()
extern "C"  void GetAnimatorLayerWeight_OnActionUpdate_m3854561776 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	{
		GetAnimatorLayerWeight_GetLayerWeight_m1458072709(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::GetLayerWeight()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorLayerWeight_GetLayerWeight_m1458072709_MetadataUsageId;
extern "C"  void GetAnimatorLayerWeight_GetLayerWeight_m1458072709 (GetAnimatorLayerWeight_t3333517428 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLayerWeight_GetLayerWeight_m1458072709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_layerWeight_16();
		Animator_t2776330603 * L_3 = __this->get__animator_17();
		FsmInt_t1596138449 * L_4 = __this->get_layerIndex_15();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		float L_6 = Animator_GetLayerWeight_m3878421230(L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::.ctor()
extern "C"  void GetAnimatorLeftFootBottomHeight__ctor_m1634568068 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::Reset()
extern "C"  void GetAnimatorLeftFootBottomHeight_Reset_m3575968305 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_leftFootHeight_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907_MetadataUsageId;
extern "C"  void GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_13();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnLateUpdate_m3660389646 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	{
		GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::_getLeftFootBottonHeight()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871_MetadataUsageId;
extern "C"  void GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_leftFootHeight_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		NullCheck(L_3);
		float L_4 = Animator_get_leftFeetBottomHeight_m4166530042(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::.ctor()
extern "C"  void GetAnimatorNextStateInfo__ctor_m1442289615 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::Reset()
extern "C"  void GetAnimatorNextStateInfo_Reset_m3383689852 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_layerIndex_15((FsmInt_t1596138449 *)NULL);
		__this->set_name_16((FsmString_t952858651 *)NULL);
		__this->set_nameHash_17((FsmInt_t1596138449 *)NULL);
		__this->set_fullPathHash_18((FsmInt_t1596138449 *)NULL);
		__this->set_shortPathHash_19((FsmInt_t1596138449 *)NULL);
		__this->set_tagHash_20((FsmInt_t1596138449 *)NULL);
		__this->set_length_22((FsmFloat_t2134102846 *)NULL);
		__this->set_normalizedTime_23((FsmFloat_t2134102846 *)NULL);
		__this->set_isStateLooping_21((FsmBool_t1075959796 *)NULL);
		__this->set_loopCount_24((FsmInt_t1596138449 *)NULL);
		__this->set_currentLoopProgress_25((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorNextStateInfo_OnEnter_m2452579302_MetadataUsageId;
extern "C"  void GetAnimatorNextStateInfo_OnEnter_m2452579302 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorNextStateInfo_OnEnter_m2452579302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_26(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorNextStateInfo_GetLayerInfo_m3523314750(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnActionUpdate()
extern "C"  void GetAnimatorNextStateInfo_OnActionUpdate_m3180156211 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	{
		GetAnimatorNextStateInfo_GetLayerInfo_m3523314750(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::GetLayerInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorNextStateInfo_GetLayerInfo_m3523314750_MetadataUsageId;
extern "C"  void GetAnimatorNextStateInfo_GetLayerInfo_m3523314750 (GetAnimatorNextStateInfo_t3502000247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorNextStateInfo_GetLayerInfo_m3523314750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorStateInfo_t323110318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_019f;
		}
	}
	{
		Animator_t2776330603 * L_2 = __this->get__animator_26();
		FsmInt_t1596138449 * L_3 = __this->get_layerIndex_15();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimatorStateInfo_t323110318  L_5 = Animator_GetNextAnimatorStateInfo_m791156688(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmInt_t1596138449 * L_6 = __this->get_fullPathHash_18();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		FsmInt_t1596138449 * L_8 = __this->get_fullPathHash_18();
		int32_t L_9 = AnimatorStateInfo_get_fullPathHash_m3257074542((&V_0), /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmInt_set_Value_m2087583461(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004a:
	{
		FsmInt_t1596138449 * L_10 = __this->get_shortPathHash_19();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006c;
		}
	}
	{
		FsmInt_t1596138449 * L_12 = __this->get_shortPathHash_19();
		int32_t L_13 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmInt_set_Value_m2087583461(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006c:
	{
		FsmInt_t1596138449 * L_14 = __this->get_nameHash_17();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008e;
		}
	}
	{
		FsmInt_t1596138449 * L_16 = __this->get_nameHash_17();
		int32_t L_17 = AnimatorStateInfo_get_shortNameHash_m994885515((&V_0), /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmInt_set_Value_m2087583461(L_16, L_17, /*hidden argument*/NULL);
	}

IL_008e:
	{
		FsmString_t952858651 * L_18 = __this->get_name_16();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00bf;
		}
	}
	{
		FsmString_t952858651 * L_20 = __this->get_name_16();
		Animator_t2776330603 * L_21 = __this->get__animator_26();
		FsmInt_t1596138449 * L_22 = __this->get_layerIndex_15();
		NullCheck(L_22);
		int32_t L_23 = FsmInt_get_Value_m27059446(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_24 = Animator_GetLayerName_m3480300056(L_21, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmString_set_Value_m829393196(L_20, L_24, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		FsmInt_t1596138449 * L_25 = __this->get_tagHash_20();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00e1;
		}
	}
	{
		FsmInt_t1596138449 * L_27 = __this->get_tagHash_20();
		int32_t L_28 = AnimatorStateInfo_get_tagHash_m3543262078((&V_0), /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmInt_set_Value_m2087583461(L_27, L_28, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		FsmFloat_t2134102846 * L_29 = __this->get_length_22();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_0103;
		}
	}
	{
		FsmFloat_t2134102846 * L_31 = __this->get_length_22();
		float L_32 = AnimatorStateInfo_get_length_m3147284742((&V_0), /*hidden argument*/NULL);
		NullCheck(L_31);
		FsmFloat_set_Value_m1568963140(L_31, L_32, /*hidden argument*/NULL);
	}

IL_0103:
	{
		FsmBool_t1075959796 * L_33 = __this->get_isStateLooping_21();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0125;
		}
	}
	{
		FsmBool_t1075959796 * L_35 = __this->get_isStateLooping_21();
		bool L_36 = AnimatorStateInfo_get_loop_m1495892586((&V_0), /*hidden argument*/NULL);
		NullCheck(L_35);
		FsmBool_set_Value_m1126216340(L_35, L_36, /*hidden argument*/NULL);
	}

IL_0125:
	{
		FsmFloat_t2134102846 * L_37 = __this->get_normalizedTime_23();
		NullCheck(L_37);
		bool L_38 = NamedVariable_get_IsNone_m281035543(L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_0147;
		}
	}
	{
		FsmFloat_t2134102846 * L_39 = __this->get_normalizedTime_23();
		float L_40 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		NullCheck(L_39);
		FsmFloat_set_Value_m1568963140(L_39, L_40, /*hidden argument*/NULL);
	}

IL_0147:
	{
		FsmInt_t1596138449 * L_41 = __this->get_loopCount_24();
		NullCheck(L_41);
		bool L_42 = NamedVariable_get_IsNone_m281035543(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0167;
		}
	}
	{
		FsmFloat_t2134102846 * L_43 = __this->get_currentLoopProgress_25();
		NullCheck(L_43);
		bool L_44 = NamedVariable_get_IsNone_m281035543(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_019f;
		}
	}

IL_0167:
	{
		FsmInt_t1596138449 * L_45 = __this->get_loopCount_24();
		float L_46 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		double L_47 = Math_Truncate_m534017384(NULL /*static, unused*/, (((double)((double)L_46))), /*hidden argument*/NULL);
		NullCheck(L_45);
		FsmInt_set_Value_m2087583461(L_45, (((int32_t)((int32_t)L_47))), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_48 = __this->get_currentLoopProgress_25();
		float L_49 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_50 = __this->get_loopCount_24();
		NullCheck(L_50);
		int32_t L_51 = FsmInt_get_Value_m27059446(L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		FsmFloat_set_Value_m1568963140(L_48, ((float)((float)L_49-(float)(((float)((float)L_51))))), /*hidden argument*/NULL);
	}

IL_019f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::.ctor()
extern "C"  void GetAnimatorPivot__ctor_m1612529529 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::Reset()
extern "C"  void GetAnimatorPivot_Reset_m3553929766 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_pivotWeight_15((FsmFloat_t2134102846 *)NULL);
		__this->set_pivotPosition_16((FsmVector3_t533912882 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorPivot_OnEnter_m2844379408_MetadataUsageId;
extern "C"  void GetAnimatorPivot_OnEnter_m2844379408 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPivot_OnEnter_m2844379408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_17(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorPivot_DoCheckPivot_m312014448(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnActionUpdate()
extern "C"  void GetAnimatorPivot_OnActionUpdate_m1855499465 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	{
		GetAnimatorPivot_DoCheckPivot_m312014448(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::DoCheckPivot()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorPivot_DoCheckPivot_m312014448_MetadataUsageId;
extern "C"  void GetAnimatorPivot_DoCheckPivot_m312014448 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPivot_DoCheckPivot_m312014448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_pivotWeight_15();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		FsmFloat_t2134102846 * L_4 = __this->get_pivotWeight_15();
		Animator_t2776330603 * L_5 = __this->get__animator_17();
		NullCheck(L_5);
		float L_6 = Animator_get_pivotWeight_m1500566793(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmFloat_set_Value_m1568963140(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		FsmVector3_t533912882 * L_7 = __this->get_pivotPosition_16();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005e;
		}
	}
	{
		FsmVector3_t533912882 * L_9 = __this->get_pivotPosition_16();
		Animator_t2776330603 * L_10 = __this->get__animator_17();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Animator_get_pivotPosition_m1479929804(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmVector3_set_Value_m716982822(L_9, L_11, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::.ctor()
extern "C"  void GetAnimatorPlayBackSpeed__ctor_m2603572271 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::Reset()
extern "C"  void GetAnimatorPlayBackSpeed_Reset_m250005212 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_playBackSpeed_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorPlayBackSpeed_OnEnter_m1753714758_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackSpeed_OnEnter_m1753714758 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackSpeed_OnEnter_m1753714758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_13();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnUpdate()
extern "C"  void GetAnimatorPlayBackSpeed_OnUpdate_m1959109309 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	{
		GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::GetPlayBackSpeed()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587 (GetAnimatorPlayBackSpeed_t331618327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m3957072587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_playBackSpeed_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		NullCheck(L_3);
		float L_4 = Animator_get_speed_m1893369654(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::.ctor()
extern "C"  void GetAnimatorPlayBackTime__ctor_m876104163 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::Reset()
extern "C"  void GetAnimatorPlayBackTime_Reset_m2817504400 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_playBackTime_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorPlayBackTime_OnEnter_m3809206522_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackTime_OnEnter_m3809206522 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackTime_OnEnter_m3809206522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_13();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnUpdate()
extern "C"  void GetAnimatorPlayBackTime_OnUpdate_m1254844553 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	{
		GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::GetPlayBackTime()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711_MetadataUsageId;
extern "C"  void GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711 (GetAnimatorPlayBackTime_t2941148851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorPlayBackTime_GetPlayBackTime_m2202807711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_playBackTime_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		NullCheck(L_3);
		float L_4 = Animator_get_playbackTime_m3871048475(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::.ctor()
extern "C"  void GetAnimatorRightFootBottomHeight__ctor_m1414875135 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::Reset()
extern "C"  void GetAnimatorRightFootBottomHeight_Reset_m3356275372 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method)
{
	{
		FsmStateAction_Reset_m4087448855(__this, /*hidden argument*/NULL);
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_rightFootHeight_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorRightFootBottomHeight_OnEnter_m1877067798_MetadataUsageId;
extern "C"  void GetAnimatorRightFootBottomHeight_OnEnter_m1877067798 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorRightFootBottomHeight_OnEnter_m1877067798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_14(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651(__this, /*hidden argument*/NULL);
		bool L_9 = __this->get_everyFrame_13();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorRightFootBottomHeight_OnLateUpdate_m4030590003 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method)
{
	{
		GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::_getRightFootBottonHeight()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651_MetadataUsageId;
extern "C"  void GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651 (GetAnimatorRightFootBottomHeight_t1347553863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2162975651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_rightFootHeight_12();
		Animator_t2776330603 * L_3 = __this->get__animator_14();
		NullCheck(L_3);
		float L_4 = Animator_get_rightFeetBottomHeight_m3839016939(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::.ctor()
extern "C"  void GetAnimatorRoot__ctor_m4194700777 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::Reset()
extern "C"  void GetAnimatorRoot_Reset_m1841133718 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_rootPosition_15((FsmVector3_t533912882 *)NULL);
		__this->set_rootRotation_16((FsmQuaternion_t3871136040 *)NULL);
		__this->set_bodyGameObject_17((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorRoot_OnEnter_m1819851648_MetadataUsageId;
extern "C"  void GetAnimatorRoot_OnEnter_m1819851648 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorRoot_OnEnter_m1819851648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_18(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_bodyGameObject_17();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_19(L_14);
	}

IL_006d:
	{
		GetAnimatorRoot_DoGetBodyPosition_m3392718813(__this, /*hidden argument*/NULL);
		bool L_15 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnActionUpdate()
extern "C"  void GetAnimatorRoot_OnActionUpdate_m1926831193 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method)
{
	{
		GetAnimatorRoot_DoGetBodyPosition_m3392718813(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::DoGetBodyPosition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorRoot_DoGetBodyPosition_m3392718813_MetadataUsageId;
extern "C"  void GetAnimatorRoot_DoGetBodyPosition_m3392718813 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorRoot_DoGetBodyPosition_m3392718813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_rootPosition_15();
		Animator_t2776330603 * L_3 = __this->get__animator_18();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_rootPosition_m425633900(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_rootRotation_16();
		Animator_t2776330603 * L_6 = __this->get__animator_18();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_rootRotation_m3309843777(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		Transform_t1659122786 * L_8 = __this->get__transform_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		Transform_t1659122786 * L_10 = __this->get__transform_19();
		Animator_t2776330603 * L_11 = __this->get__animator_18();
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Animator_get_rootPosition_m425633900(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m3111394108(L_10, L_12, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = __this->get__transform_19();
		Animator_t2776330603 * L_14 = __this->get__animator_18();
		NullCheck(L_14);
		Quaternion_t1553702882  L_15 = Animator_get_rootRotation_m3309843777(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m1525803229(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::.ctor()
extern "C"  void GetAnimatorSpeed__ctor_m4076454164 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::Reset()
extern "C"  void GetAnimatorSpeed_Reset_m1722887105 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_speed_15((FsmFloat_t2134102846 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorSpeed_OnEnter_m4148973547_MetadataUsageId;
extern "C"  void GetAnimatorSpeed_OnEnter_m4148973547 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorSpeed_OnEnter_m4148973547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		GetAnimatorSpeed_GetPlaybackSpeed_m2498397414(__this, /*hidden argument*/NULL);
		bool L_9 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_9)
		{
			goto IL_0060;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnActionUpdate()
extern "C"  void GetAnimatorSpeed_OnActionUpdate_m1035258254 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method)
{
	{
		GetAnimatorSpeed_GetPlaybackSpeed_m2498397414(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::GetPlaybackSpeed()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorSpeed_GetPlaybackSpeed_m2498397414_MetadataUsageId;
extern "C"  void GetAnimatorSpeed_GetPlaybackSpeed_m2498397414 (GetAnimatorSpeed_t960447890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorSpeed_GetPlaybackSpeed_m2498397414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_speed_15();
		Animator_t2776330603 * L_3 = __this->get__animator_16();
		NullCheck(L_3);
		float L_4 = Animator_get_speed_m1893369654(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::.ctor()
extern "C"  void GetAnimatorTarget__ctor_m1909821466 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase__ctor_m1231659039(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::Reset()
extern "C"  void GetAnimatorTarget_Reset_m3851221703 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method)
{
	{
		FsmStateActionAnimatorBase_Reset_m3173059276(__this, /*hidden argument*/NULL);
		__this->set_gameObject_14((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_targetPosition_15((FsmVector3_t533912882 *)NULL);
		__this->set_targetRotation_16((FsmQuaternion_t3871136040 *)NULL);
		__this->set_targetGameObject_17((FsmGameObject_t1697147867 *)NULL);
		((FsmStateActionAnimatorBase_t2852864039 *)__this)->set_everyFrame_11((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t GetAnimatorTarget_OnEnter_m779122033_MetadataUsageId;
extern "C"  void GetAnimatorTarget_OnEnter_m779122033 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorTarget_OnEnter_m779122033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_14();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_18(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_targetGameObject_17();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_19(L_14);
	}

IL_006d:
	{
		GetAnimatorTarget_DoGetTarget_m1632445748(__this, /*hidden argument*/NULL);
		bool L_15 = ((FsmStateActionAnimatorBase_t2852864039 *)__this)->get_everyFrame_11();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnActionUpdate()
extern "C"  void GetAnimatorTarget_OnActionUpdate_m1634369864 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method)
{
	{
		GetAnimatorTarget_DoGetTarget_m1632445748(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::DoGetTarget()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetAnimatorTarget_DoGetTarget_m1632445748_MetadataUsageId;
extern "C"  void GetAnimatorTarget_DoGetTarget_m1632445748 (GetAnimatorTarget_t3370286236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAnimatorTarget_DoGetTarget_m1632445748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2776330603 * L_0 = __this->get__animator_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FsmVector3_t533912882 * L_2 = __this->get_targetPosition_15();
		Animator_t2776330603 * L_3 = __this->get__animator_18();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Animator_get_targetPosition_m3699919451(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_4, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = __this->get_targetRotation_16();
		Animator_t2776330603 * L_6 = __this->get__animator_18();
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Animator_get_targetRotation_m2080040752(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmQuaternion_set_Value_m446581172(L_5, L_7, /*hidden argument*/NULL);
		Transform_t1659122786 * L_8 = __this->get__transform_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		Transform_t1659122786 * L_10 = __this->get__transform_19();
		Animator_t2776330603 * L_11 = __this->get__animator_18();
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Animator_get_targetPosition_m3699919451(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m3111394108(L_10, L_12, /*hidden argument*/NULL);
		Transform_t1659122786 * L_13 = __this->get__transform_19();
		Animator_t2776330603 * L_14 = __this->get__animator_18();
		NullCheck(L_14);
		Quaternion_t1553702882  L_15 = Animator_get_targetRotation_m2080040752(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m1525803229(L_13, L_15, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetASine::.ctor()
extern "C"  void GetASine__ctor_m347181286 (GetASine_t1695390080 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetASine::Reset()
extern "C"  void GetASine_Reset_m2288581523 (GetASine_t1695390080 * __this, const MethodInfo* method)
{
	{
		__this->set_angle_12((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_13(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_Value_11((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnEnter()
extern "C"  void GetASine_OnEnter_m2320462653 (GetASine_t1695390080 * __this, const MethodInfo* method)
{
	{
		GetASine_DoASine_m2432497991(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnUpdate()
extern "C"  void GetASine_OnUpdate_m2348424870 (GetASine_t1695390080 * __this, const MethodInfo* method)
{
	{
		GetASine_DoASine_m2432497991(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetASine::DoASine()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetASine_DoASine_m2432497991_MetadataUsageId;
extern "C"  void GetASine_DoASine_m2432497991 (GetASine_t1695390080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetASine_DoASine_m2432497991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_Value_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = asinf(L_1);
		V_0 = L_2;
		FsmBool_t1075959796 * L_3 = __this->get_RadToDeg_13();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		float L_5 = V_0;
		V_0 = ((float)((float)L_5*(float)(57.29578f)));
	}

IL_0029:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_angle_12();
		float L_7 = V_0;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan::.ctor()
extern "C"  void GetAtan__ctor_m1350588306 (GetAtan_t1738223652 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan::Reset()
extern "C"  void GetAtan_Reset_m3291988543 (GetAtan_t1738223652 * __this, const MethodInfo* method)
{
	{
		__this->set_Value_11((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_13(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_angle_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnEnter()
extern "C"  void GetAtan_OnEnter_m226967273 (GetAtan_t1738223652 * __this, const MethodInfo* method)
{
	{
		GetAtan_DoATan_m3475302173(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnUpdate()
extern "C"  void GetAtan_OnUpdate_m1874577530 (GetAtan_t1738223652 * __this, const MethodInfo* method)
{
	{
		GetAtan_DoATan_m3475302173(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan::DoATan()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetAtan_DoATan_m3475302173_MetadataUsageId;
extern "C"  void GetAtan_DoATan_m3475302173 (GetAtan_t1738223652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAtan_DoATan_m3475302173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_Value_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = atanf(L_1);
		V_0 = L_2;
		FsmBool_t1075959796 * L_3 = __this->get_RadToDeg_13();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		float L_5 = V_0;
		V_0 = ((float)((float)L_5*(float)(57.29578f)));
	}

IL_0029:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_angle_12();
		float L_7 = V_0;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::.ctor()
extern "C"  void GetAtan2__ctor_m3949376354 (GetAtan2_t1696365444 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::Reset()
extern "C"  void GetAtan2_Reset_m1595809295 (GetAtan2_t1696365444 * __this, const MethodInfo* method)
{
	{
		__this->set_xValue_11((FsmFloat_t2134102846 *)NULL);
		__this->set_yValue_12((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_14(L_0);
		__this->set_everyFrame_15((bool)0);
		__this->set_angle_13((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnEnter()
extern "C"  void GetAtan2_OnEnter_m2286282425 (GetAtan2_t1696365444 * __this, const MethodInfo* method)
{
	{
		GetAtan2_DoATan_m2433353037(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnUpdate()
extern "C"  void GetAtan2_OnUpdate_m1288837802 (GetAtan2_t1696365444 * __this, const MethodInfo* method)
{
	{
		GetAtan2_DoATan_m2433353037(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::DoATan()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetAtan2_DoATan_m2433353037_MetadataUsageId;
extern "C"  void GetAtan2_DoATan_m2433353037 (GetAtan2_t1696365444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAtan2_DoATan_m2433353037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_yValue_12();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = __this->get_xValue_11();
		NullCheck(L_2);
		float L_3 = FsmFloat_get_Value_m4137923823(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = atan2f(L_1, L_3);
		V_0 = L_4;
		FsmBool_t1075959796 * L_5 = __this->get_RadToDeg_14();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		float L_7 = V_0;
		V_0 = ((float)((float)L_7*(float)(57.29578f)));
	}

IL_0034:
	{
		FsmFloat_t2134102846 * L_8 = __this->get_angle_13();
		float L_9 = V_0;
		NullCheck(L_8);
		FsmFloat_set_Value_m1568963140(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::.ctor()
extern "C"  void GetAtan2FromVector2__ctor_m997173695 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::Reset()
extern "C"  void GetAtan2FromVector2_Reset_m2938573932 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method)
{
	{
		__this->set_vector2_11((FsmVector2_t533912881 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_13(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_angle_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnEnter()
extern "C"  void GetAtan2FromVector2_OnEnter_m4192909782 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method)
{
	{
		GetAtan2FromVector2_DoATan_m1109383824(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnUpdate()
extern "C"  void GetAtan2FromVector2_OnUpdate_m264743725 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method)
{
	{
		GetAtan2FromVector2_DoATan_m1109383824(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::DoATan()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetAtan2FromVector2_DoATan_m1109383824_MetadataUsageId;
extern "C"  void GetAtan2FromVector2_DoATan_m1109383824 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAtan2FromVector2_DoATan_m1109383824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		FsmVector2_t533912881 * L_0 = __this->get_vector2_11();
		NullCheck(L_0);
		Vector2_t4282066565  L_1 = FsmVector2_get_Value_m1313754285(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_2();
		FsmVector2_t533912881 * L_3 = __this->get_vector2_11();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_6 = atan2f(L_2, L_5);
		V_0 = L_6;
		FsmBool_t1075959796 * L_7 = __this->get_RadToDeg_13();
		NullCheck(L_7);
		bool L_8 = FsmBool_get_Value_m3101329097(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		float L_9 = V_0;
		V_0 = ((float)((float)L_9*(float)(57.29578f)));
	}

IL_0044:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_angle_12();
		float L_11 = V_0;
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::.ctor()
extern "C"  void GetAtan2FromVector3__ctor_m800660190 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::Reset()
extern "C"  void GetAtan2FromVector3_Reset_m2742060427 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method)
{
	{
		__this->set_vector3_11((FsmVector3_t533912882 *)NULL);
		__this->set_xAxis_12(0);
		__this->set_yAxis_13(1);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_RadToDeg_15(L_0);
		__this->set_everyFrame_16((bool)0);
		__this->set_angle_14((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnEnter()
extern "C"  void GetAtan2FromVector3_OnEnter_m27025205 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method)
{
	{
		GetAtan2FromVector3_DoATan_m3607399761(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnUpdate()
extern "C"  void GetAtan2FromVector3_OnUpdate_m4266308014 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method)
{
	{
		GetAtan2FromVector3_DoATan_m3607399761(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::DoATan()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetAtan2FromVector3_DoATan_m3607399761_MetadataUsageId;
extern "C"  void GetAtan2FromVector3_DoATan_m3607399761 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAtan2FromVector3_DoATan_m3607399761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3_11();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = FsmVector3_get_Value_m2779135117(L_0, /*hidden argument*/NULL);
		V_3 = L_1;
		float L_2 = (&V_3)->get_x_1();
		V_0 = L_2;
		int32_t L_3 = __this->get_xAxis_12();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_003a;
		}
	}
	{
		FsmVector3_t533912882 * L_4 = __this->get_vector3_11();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = FsmVector3_get_Value_m2779135117(L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		float L_6 = (&V_4)->get_y_2();
		V_0 = L_6;
		goto IL_005b;
	}

IL_003a:
	{
		int32_t L_7 = __this->get_xAxis_12();
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_005b;
		}
	}
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector3_11();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		V_0 = L_10;
	}

IL_005b:
	{
		FsmVector3_t533912882 * L_11 = __this->get_vector3_11();
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = FsmVector3_get_Value_m2779135117(L_11, /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = (&V_6)->get_y_2();
		V_1 = L_13;
		int32_t L_14 = __this->get_yAxis_13();
		if (L_14)
		{
			goto IL_0095;
		}
	}
	{
		FsmVector3_t533912882 * L_15 = __this->get_vector3_11();
		NullCheck(L_15);
		Vector3_t4282066566  L_16 = FsmVector3_get_Value_m2779135117(L_15, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = (&V_7)->get_x_1();
		V_1 = L_17;
		goto IL_00b6;
	}

IL_0095:
	{
		int32_t L_18 = __this->get_yAxis_13();
		if ((!(((uint32_t)L_18) == ((uint32_t)2))))
		{
			goto IL_00b6;
		}
	}
	{
		FsmVector3_t533912882 * L_19 = __this->get_vector3_11();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = FsmVector3_get_Value_m2779135117(L_19, /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = (&V_8)->get_z_3();
		V_1 = L_21;
	}

IL_00b6:
	{
		float L_22 = V_1;
		float L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = atan2f(L_22, L_23);
		V_2 = L_24;
		FsmBool_t1075959796 * L_25 = __this->get_RadToDeg_15();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00d6;
		}
	}
	{
		float L_27 = V_2;
		V_2 = ((float)((float)L_27*(float)(57.29578f)));
	}

IL_00d6:
	{
		FsmFloat_t2134102846 * L_28 = __this->get_angle_14();
		float L_29 = V_2;
		NullCheck(L_28);
		FsmFloat_set_Value_m1568963140(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxis::.ctor()
extern "C"  void GetAxis__ctor_m3688609969 (GetAxis_t1738227749 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxis::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetAxis_Reset_m1335042910_MetadataUsageId;
extern "C"  void GetAxis_Reset_m1335042910 (GetAxis_t1738227749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAxis_Reset_m1335042910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_axisName_11(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_multiplier_12(L_2);
		__this->set_store_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnEnter()
extern "C"  void GetAxis_OnEnter_m797889608 (GetAxis_t1738227749 * __this, const MethodInfo* method)
{
	{
		GetAxis_DoGetAxis_m1514384603(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnUpdate()
extern "C"  void GetAxis_OnUpdate_m2393300731 (GetAxis_t1738227749 * __this, const MethodInfo* method)
{
	{
		GetAxis_DoGetAxis_m1514384603(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxis::DoGetAxis()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetAxis_DoGetAxis_m1514384603_MetadataUsageId;
extern "C"  void GetAxis_DoGetAxis_m1514384603 (GetAxis_t1738227749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAxis_DoGetAxis_m1514384603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmString_t952858651 * L_0 = __this->get_axisName_11();
		bool L_1 = FsmString_IsNullOrEmpty_m2181469370(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmString_t952858651 * L_2 = __this->get_axisName_11();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m2027668530(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmFloat_t2134102846 * L_5 = __this->get_multiplier_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0040;
		}
	}
	{
		float L_7 = V_0;
		FsmFloat_t2134102846 * L_8 = __this->get_multiplier_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_7*(float)L_9));
	}

IL_0040:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_store_13();
		float L_11 = V_0;
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::.ctor()
extern "C"  void GetAxisVector__ctor_m2362765358 (GetAxisVector_t2714053128 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::Reset()
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern const uint32_t GetAxisVector_Reset_m9198299_MetadataUsageId;
extern "C"  void GetAxisVector_Reset_m9198299 (GetAxisVector_t2714053128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAxisVector_Reset_m9198299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		__this->set_horizontalAxis_11(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		__this->set_verticalAxis_12(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_multiplier_13(L_2);
		__this->set_mapToPlane_14(0);
		__this->set_storeVector_16((FsmVector3_t533912882 *)NULL);
		__this->set_storeMagnitude_17((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::OnUpdate()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetAxisVector_OnUpdate_m675748446_MetadataUsageId;
extern "C"  void GetAxisVector_OnUpdate_m675748446 (GetAxisVector_t2714053128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetAxisVector_OnUpdate_m675748446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t1659122786 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	float G_B15_0 = 0.0f;
	float G_B19_0 = 0.0f;
	{
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_1));
		FsmGameObject_t1697147867 * L_0 = __this->get_relativeTo_15();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_3 = __this->get_mapToPlane_14();
		V_6 = L_3;
		int32_t L_4 = V_6;
		if (L_4 == 0)
		{
			goto IL_0046;
		}
		if (L_4 == 1)
		{
			goto IL_0057;
		}
		if (L_4 == 2)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_0079;
	}

IL_0046:
	{
		Vector3_t4282066566  L_5 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_t4282066566  L_6 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0079;
	}

IL_0057:
	{
		Vector3_t4282066566  L_7 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t4282066566  L_8 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_0079;
	}

IL_0068:
	{
		Vector3_t4282066566  L_9 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t4282066566  L_10 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_10;
		goto IL_0079;
	}

IL_0079:
	{
		goto IL_011a;
	}

IL_007e:
	{
		FsmGameObject_t1697147867 * L_11 = __this->get_relativeTo_15();
		NullCheck(L_11);
		GameObject_t3674682005 * L_12 = FsmGameObject_get_Value_m673294275(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = __this->get_mapToPlane_14();
		V_6 = L_14;
		int32_t L_15 = V_6;
		if (L_15 == 0)
		{
			goto IL_00af;
		}
		if (L_15 == 1)
		{
			goto IL_00ef;
		}
		if (L_15 == 2)
		{
			goto IL_00ef;
		}
	}
	{
		goto IL_011a;
	}

IL_00af:
	{
		Transform_t1659122786 * L_16 = V_2;
		Vector3_t4282066566  L_17 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_18 = Transform_TransformDirection_m83001769(L_16, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		(&V_0)->set_y_2((0.0f));
		Vector3_t4282066566  L_19 = Vector3_get_normalized_m2650940353((&V_0), /*hidden argument*/NULL);
		V_0 = L_19;
		float L_20 = (&V_0)->get_z_3();
		float L_21 = (&V_0)->get_x_1();
		Vector3__ctor_m2926210380((&V_1), L_20, (0.0f), ((-L_21)), /*hidden argument*/NULL);
		goto IL_011a;
	}

IL_00ef:
	{
		Vector3_t4282066566  L_22 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_22;
		(&V_0)->set_z_3((0.0f));
		Vector3_t4282066566  L_23 = Vector3_get_normalized_m2650940353((&V_0), /*hidden argument*/NULL);
		V_0 = L_23;
		Transform_t1659122786 * L_24 = V_2;
		Vector3_t4282066566  L_25 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_26 = Transform_TransformDirection_m83001769(L_24, L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		goto IL_011a;
	}

IL_011a:
	{
		FsmString_t952858651 * L_27 = __this->get_horizontalAxis_11();
		NullCheck(L_27);
		bool L_28 = NamedVariable_get_IsNone_m281035543(L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_013f;
		}
	}
	{
		FsmString_t952858651 * L_29 = __this->get_horizontalAxis_11();
		NullCheck(L_29);
		String_t* L_30 = FsmString_get_Value_m872383149(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0149;
		}
	}

IL_013f:
	{
		G_B15_0 = (0.0f);
		goto IL_0159;
	}

IL_0149:
	{
		FsmString_t952858651 * L_32 = __this->get_horizontalAxis_11();
		NullCheck(L_32);
		String_t* L_33 = FsmString_get_Value_m872383149(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_34 = Input_GetAxis_m2027668530(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		G_B15_0 = L_34;
	}

IL_0159:
	{
		V_3 = G_B15_0;
		FsmString_t952858651 * L_35 = __this->get_verticalAxis_12();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_017f;
		}
	}
	{
		FsmString_t952858651 * L_37 = __this->get_verticalAxis_12();
		NullCheck(L_37);
		String_t* L_38 = FsmString_get_Value_m872383149(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0189;
		}
	}

IL_017f:
	{
		G_B19_0 = (0.0f);
		goto IL_0199;
	}

IL_0189:
	{
		FsmString_t952858651 * L_40 = __this->get_verticalAxis_12();
		NullCheck(L_40);
		String_t* L_41 = FsmString_get_Value_m872383149(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_42 = Input_GetAxis_m2027668530(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		G_B19_0 = L_42;
	}

IL_0199:
	{
		V_4 = G_B19_0;
		float L_43 = V_3;
		Vector3_t4282066566  L_44 = V_1;
		Vector3_t4282066566  L_45 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		float L_46 = V_4;
		Vector3_t4282066566  L_47 = V_0;
		Vector3_t4282066566  L_48 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		Vector3_t4282066566  L_49 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_45, L_48, /*hidden argument*/NULL);
		V_5 = L_49;
		Vector3_t4282066566  L_50 = V_5;
		FsmFloat_t2134102846 * L_51 = __this->get_multiplier_13();
		NullCheck(L_51);
		float L_52 = FsmFloat_get_Value_m4137923823(L_51, /*hidden argument*/NULL);
		Vector3_t4282066566  L_53 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		V_5 = L_53;
		FsmVector3_t533912882 * L_54 = __this->get_storeVector_16();
		Vector3_t4282066566  L_55 = V_5;
		NullCheck(L_54);
		FsmVector3_set_Value_m716982822(L_54, L_55, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_56 = __this->get_storeMagnitude_17();
		NullCheck(L_56);
		bool L_57 = NamedVariable_get_IsNone_m281035543(L_56, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_01f4;
		}
	}
	{
		FsmFloat_t2134102846 * L_58 = __this->get_storeMagnitude_17();
		float L_59 = Vector3_get_magnitude_m989985786((&V_5), /*hidden argument*/NULL);
		NullCheck(L_58);
		FsmFloat_set_Value_m1568963140(L_58, L_59, /*hidden argument*/NULL);
	}

IL_01f4:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButton::.ctor()
extern "C"  void GetButton__ctor_m2258467968 (GetButton_t428887414 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButton::Reset()
extern Il2CppCodeGenString* _stringLiteral67887259;
extern const uint32_t GetButton_Reset_m4199868205_MetadataUsageId;
extern "C"  void GetButton_Reset_m4199868205 (GetButton_t428887414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButton_Reset_m4199868205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral67887259, /*hidden argument*/NULL);
		__this->set_buttonName_11(L_0);
		__this->set_storeResult_12((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_13((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnEnter()
extern "C"  void GetButton_OnEnter_m820961367 (GetButton_t428887414 * __this, const MethodInfo* method)
{
	{
		GetButton_DoGetButton_m2944171643(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnUpdate()
extern "C"  void GetButton_OnUpdate_m3108525260 (GetButton_t428887414 * __this, const MethodInfo* method)
{
	{
		GetButton_DoGetButton_m2944171643(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButton::DoGetButton()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetButton_DoGetButton_m2944171643_MetadataUsageId;
extern "C"  void GetButton_DoGetButton_m2944171643 (GetButton_t428887414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButton_DoGetButton_m2944171643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_12();
		FsmString_t952858651 * L_1 = __this->get_buttonName_11();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetButton_m4226175975(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::.ctor()
extern "C"  void GetButtonDown__ctor_m3935000446 (GetButtonDown_t2951628984 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::Reset()
extern Il2CppCodeGenString* _stringLiteral67887259;
extern const uint32_t GetButtonDown_Reset_m1581433387_MetadataUsageId;
extern "C"  void GetButtonDown_Reset_m1581433387 (GetButtonDown_t2951628984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButtonDown_Reset_m1581433387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral67887259, /*hidden argument*/NULL);
		__this->set_buttonName_11(L_0);
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetButtonDown_OnUpdate_m2512892174_MetadataUsageId;
extern "C"  void GetButtonDown_OnUpdate_m2512892174 (GetButtonDown_t2951628984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButtonDown_OnUpdate_m2512892174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_buttonName_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetButtonDown_m1879002085(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_5 = __this->get_sendEvent_12();
		NullCheck(L_4);
		Fsm_Event_m625948263(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0028:
	{
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_13();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::.ctor()
extern "C"  void GetButtonUp__ctor_m1438730469 (GetButtonUp_t552055153 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::Reset()
extern Il2CppCodeGenString* _stringLiteral67887259;
extern const uint32_t GetButtonUp_Reset_m3380130706_MetadataUsageId;
extern "C"  void GetButtonUp_Reset_m3380130706 (GetButtonUp_t552055153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButtonUp_Reset_m3380130706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral67887259, /*hidden argument*/NULL);
		__this->set_buttonName_11(L_0);
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetButtonUp_OnUpdate_m3492737607_MetadataUsageId;
extern "C"  void GetButtonUp_OnUpdate_m3492737607 (GetButtonUp_t552055153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetButtonUp_OnUpdate_m3492737607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		FsmString_t952858651 * L_0 = __this->get_buttonName_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetButtonUp_m2712347212(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_5 = __this->get_sendEvent_12();
		NullCheck(L_4);
		Fsm_Event_m625948263(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0028:
	{
		FsmBool_t1075959796 * L_6 = __this->get_storeResult_13();
		bool L_7 = V_0;
		NullCheck(L_6);
		FsmBool_set_Value_m1126216340(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChild::.ctor()
extern "C"  void GetChild__ctor_m1259984408 (GetChild_t1697862670 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChild::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GetChild_Reset_m3201384645_MetadataUsageId;
extern "C"  void GetChild_Reset_m3201384645 (GetChild_t1697862670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetChild_Reset_m3201384645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_childName_12(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_withTag_13(L_2);
		__this->set_storeResult_14((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChild::OnEnter()
extern "C"  void GetChild_OnEnter_m3350934511 (GetChild_t1697862670 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_storeResult_14();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_childName_12();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		FsmString_t952858651 * L_6 = __this->get_withTag_13();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_8 = GetChild_DoGetChildByName_m4062192452(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_8, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChild::DoGetChildByName(UnityEngine.GameObject,System.String,System.String)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t1659122786_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t GetChild_DoGetChildByName_m4062192452_MetadataUsageId;
extern "C"  GameObject_t3674682005 * GetChild_DoGetChildByName_m4062192452 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___root0, String_t* ___name1, String_t* ___tag2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetChild_DoGetChildByName_m4062192452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	GameObject_t3674682005 * V_2 = NULL;
	GameObject_t3674682005 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t3674682005 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (GameObject_t3674682005 *)NULL;
	}

IL_000e:
	{
		GameObject_t3674682005 * L_2 = ___root0;
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject * L_4 = Transform_GetEnumerator_m688365631(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ce;
		}

IL_001f:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_5);
			V_0 = ((Transform_t1659122786 *)CastclassClass(L_6, Transform_t1659122786_il2cpp_TypeInfo_var));
			String_t* L_7 = ___name1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0085;
			}
		}

IL_0036:
		{
			Transform_t1659122786 * L_9 = V_0;
			NullCheck(L_9);
			String_t* L_10 = Object_get_name_m3709440845(L_9, /*hidden argument*/NULL);
			String_t* L_11 = ___name1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0080;
			}
		}

IL_0047:
		{
			String_t* L_13 = ___tag2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_14 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			if (L_14)
			{
				goto IL_0074;
			}
		}

IL_0052:
		{
			Transform_t1659122786 * L_15 = V_0;
			NullCheck(L_15);
			String_t* L_16 = Component_get_tag_m217485006(L_15, /*hidden argument*/NULL);
			String_t* L_17 = ___tag2;
			NullCheck(L_16);
			bool L_18 = String_Equals_m3541721061(L_16, L_17, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_006f;
			}
		}

IL_0063:
		{
			Transform_t1659122786 * L_19 = V_0;
			NullCheck(L_19);
			GameObject_t3674682005 * L_20 = Component_get_gameObject_m1170635899(L_19, /*hidden argument*/NULL);
			V_3 = L_20;
			IL2CPP_LEAVE(0xF5, FINALLY_00de);
		}

IL_006f:
		{
			goto IL_0080;
		}

IL_0074:
		{
			Transform_t1659122786 * L_21 = V_0;
			NullCheck(L_21);
			GameObject_t3674682005 * L_22 = Component_get_gameObject_m1170635899(L_21, /*hidden argument*/NULL);
			V_3 = L_22;
			IL2CPP_LEAVE(0xF5, FINALLY_00de);
		}

IL_0080:
		{
			goto IL_00ad;
		}

IL_0085:
		{
			String_t* L_23 = ___tag2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_24 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			if (L_24)
			{
				goto IL_00ad;
			}
		}

IL_0090:
		{
			Transform_t1659122786 * L_25 = V_0;
			NullCheck(L_25);
			String_t* L_26 = Component_get_tag_m217485006(L_25, /*hidden argument*/NULL);
			String_t* L_27 = ___tag2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_28 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
			if (!L_28)
			{
				goto IL_00ad;
			}
		}

IL_00a1:
		{
			Transform_t1659122786 * L_29 = V_0;
			NullCheck(L_29);
			GameObject_t3674682005 * L_30 = Component_get_gameObject_m1170635899(L_29, /*hidden argument*/NULL);
			V_3 = L_30;
			IL2CPP_LEAVE(0xF5, FINALLY_00de);
		}

IL_00ad:
		{
			Transform_t1659122786 * L_31 = V_0;
			NullCheck(L_31);
			GameObject_t3674682005 * L_32 = Component_get_gameObject_m1170635899(L_31, /*hidden argument*/NULL);
			String_t* L_33 = ___name1;
			String_t* L_34 = ___tag2;
			GameObject_t3674682005 * L_35 = GetChild_DoGetChildByName_m4062192452(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
			V_2 = L_35;
			GameObject_t3674682005 * L_36 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
			bool L_37 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_36, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
			if (!L_37)
			{
				goto IL_00ce;
			}
		}

IL_00c7:
		{
			GameObject_t3674682005 * L_38 = V_2;
			V_3 = L_38;
			IL2CPP_LEAVE(0xF5, FINALLY_00de);
		}

IL_00ce:
		{
			Il2CppObject * L_39 = V_1;
			NullCheck(L_39);
			bool L_40 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_39);
			if (L_40)
			{
				goto IL_001f;
			}
		}

IL_00d9:
		{
			IL2CPP_LEAVE(0xF3, FINALLY_00de);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00de;
	}

FINALLY_00de:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_41 = V_1;
			V_4 = ((Il2CppObject *)IsInst(L_41, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_42 = V_4;
			if (L_42)
			{
				goto IL_00eb;
			}
		}

IL_00ea:
		{
			IL2CPP_END_FINALLY(222)
		}

IL_00eb:
		{
			Il2CppObject * L_43 = V_4;
			NullCheck(L_43);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_43);
			IL2CPP_END_FINALLY(222)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(222)
	{
		IL2CPP_JUMP_TBL(0xF5, IL_00f5)
		IL2CPP_JUMP_TBL(0xF3, IL_00f3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00f3:
	{
		return (GameObject_t3674682005 *)NULL;
	}

IL_00f5:
	{
		GameObject_t3674682005 * L_44 = V_3;
		return L_44;
	}
}
// System.String HutongGames.PlayMaker.Actions.GetChild::ErrorCheck()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3180584572;
extern const uint32_t GetChild_ErrorCheck_m4013168239_MetadataUsageId;
extern "C"  String_t* GetChild_ErrorCheck_m4013168239 (GetChild_t1697862670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetChild_ErrorCheck_m4013168239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_childName_12();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_withTag_13();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		return _stringLiteral3180584572;
	}

IL_0030:
	{
		return (String_t*)NULL;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::.ctor()
extern "C"  void GetChildCount__ctor_m673033215 (GetChildCount_t3922702103 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::Reset()
extern "C"  void GetChildCount_Reset_m2614433452 (GetChildCount_t3922702103 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_12((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::OnEnter()
extern "C"  void GetChildCount_OnEnter_m1931553814 (GetChildCount_t3922702103 * __this, const MethodInfo* method)
{
	{
		GetChildCount_DoGetChildCount_m2660501787(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::DoGetChildCount()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetChildCount_DoGetChildCount_m2660501787_MetadataUsageId;
extern "C"  void GetChildCount_DoGetChildCount_m2660501787 (GetChildCount_t3922702103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetChildCount_DoGetChildCount_m2660501787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmInt_t1596138449 * L_5 = __this->get_storeResult_12();
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Transform_get_childCount_m2107810675(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmInt_set_Value_m2087583461(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::.ctor()
extern "C"  void GetChildNum__ctor_m3498776104 (GetChildNum_t3319553230 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::Reset()
extern "C"  void GetChildNum_Reset_m1145209045 (GetChildNum_t3319553230 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_childIndex_12(L_0);
		__this->set_store_13((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::OnEnter()
extern "C"  void GetChildNum_OnEnter_m3051139071 (GetChildNum_t3319553230 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_store_13();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = GetChildNum_DoGetChildNum_m1852380072(__this, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_4, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChildNum::DoGetChildNum(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetChildNum_DoGetChildNum_m1852380072_MetadataUsageId;
extern "C"  GameObject_t3674682005 * GetChildNum_DoGetChildNum_m1852380072 (GetChildNum_t3319553230 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetChildNum_DoGetChildNum_m1852380072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((GameObject_t3674682005 *)(NULL));
		goto IL_0039;
	}

IL_0012:
	{
		GameObject_t3674682005 * L_2 = ___go0;
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_4 = __this->get_childIndex_12();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = ___go0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Transform_get_childCount_m2107810675(L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_9 = Transform_GetChild_m4040462992(L_3, ((int32_t)((int32_t)L_5%(int32_t)L_8)), /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0039:
	{
		return G_B3_0;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::.ctor()
extern "C"  void GetCollision2dInfo__ctor_m304751298 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::Reset()
extern "C"  void GetCollision2dInfo_Reset_m2246151535 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_relativeVelocity_12((FsmVector3_t533912882 *)NULL);
		__this->set_relativeSpeed_13((FsmFloat_t2134102846 *)NULL);
		__this->set_contactPoint_14((FsmVector3_t533912882 *)NULL);
		__this->set_contactNormal_15((FsmVector3_t533912882 *)NULL);
		__this->set_shapeCount_16((FsmInt_t1596138449 *)NULL);
		__this->set_physics2dMaterialName_17((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::StoreCollisionInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetCollision2dInfo_StoreCollisionInfo_m2896075873_MetadataUsageId;
extern "C"  void GetCollision2dInfo_StoreCollisionInfo_m2896075873 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetCollision2dInfo_StoreCollisionInfo_m2896075873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	FsmString_t952858651 * G_B4_0 = NULL;
	FsmString_t952858651 * G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	FsmString_t952858651 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Collision2D_t2859305914 * L_1 = Fsm_get_Collision2DInfo_m225936834(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Collision2D_t2859305914 * L_4 = Fsm_get_Collision2DInfo_m225936834(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Collision2D_get_gameObject_m718845954(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmGameObject_set_Value_m297051598(L_2, L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = __this->get_relativeSpeed_13();
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Collision2D_t2859305914 * L_8 = Fsm_get_Collision2DInfo_m225936834(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t4282066565  L_9 = Collision2D_get_relativeVelocity_m2960747782(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = Vector2_get_magnitude_m1987058139((&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_10, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_11 = __this->get_relativeVelocity_12();
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Collision2D_t2859305914 * L_13 = Fsm_get_Collision2DInfo_m225936834(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector2_t4282066565  L_14 = Collision2D_get_relativeVelocity_m2960747782(L_13, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmVector3_set_Value_m716982822(L_11, L_15, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_physics2dMaterialName_17();
		Fsm_t1527112426 * L_17 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Collision2D_t2859305914 * L_18 = Fsm_get_Collision2DInfo_m225936834(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Collider2D_t1552025098 * L_19 = Collision2D_get_collider_m4116040666(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		PhysicsMaterial2D_t1327932246 * L_20 = Collider2D_get_sharedMaterial_m2954504316(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_20, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = L_16;
		if (!L_21)
		{
			G_B4_0 = L_16;
			goto IL_00b4;
		}
	}
	{
		Fsm_t1527112426 * L_22 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Collision2D_t2859305914 * L_23 = Fsm_get_Collision2DInfo_m225936834(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Collider2D_t1552025098 * L_24 = Collision2D_get_collider_m4116040666(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		PhysicsMaterial2D_t1327932246 * L_25 = Collider2D_get_sharedMaterial_m2954504316(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		String_t* L_26 = Object_get_name_m3709440845(L_25, /*hidden argument*/NULL);
		G_B5_0 = L_26;
		G_B5_1 = G_B3_0;
		goto IL_00b9;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B5_0 = L_27;
		G_B5_1 = G_B4_0;
	}

IL_00b9:
	{
		NullCheck(G_B5_1);
		FsmString_set_Value_m829393196(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_28 = __this->get_shapeCount_16();
		Fsm_t1527112426 * L_29 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Collision2D_t2859305914 * L_30 = Fsm_get_Collision2DInfo_m225936834(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Collider2D_t1552025098 * L_31 = Collision2D_get_collider_m4116040666(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = Collider2D_get_shapeCount_m2388185620(L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		FsmInt_set_Value_m2087583461(L_28, L_32, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_33 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Collision2D_t2859305914 * L_34 = Fsm_get_Collision2DInfo_m225936834(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		ContactPoint2DU5BU5D_t3916425411* L_35 = Collision2D_get_contacts_m4248754423(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0161;
		}
	}
	{
		Fsm_t1527112426 * L_36 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Collision2D_t2859305914 * L_37 = Fsm_get_Collision2DInfo_m225936834(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		ContactPoint2DU5BU5D_t3916425411* L_38 = Collision2D_get_contacts_m4248754423(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0161;
		}
	}
	{
		FsmVector3_t533912882 * L_39 = __this->get_contactPoint_14();
		Fsm_t1527112426 * L_40 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Collision2D_t2859305914 * L_41 = Fsm_get_Collision2DInfo_m225936834(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		ContactPoint2DU5BU5D_t3916425411* L_42 = Collision2D_get_contacts_m4248754423(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		Vector2_t4282066565  L_43 = ContactPoint2D_get_point_m875769013(((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_44 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		NullCheck(L_39);
		FsmVector3_set_Value_m716982822(L_39, L_44, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_45 = __this->get_contactNormal_15();
		Fsm_t1527112426 * L_46 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Collision2D_t2859305914 * L_47 = Fsm_get_Collision2DInfo_m225936834(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		ContactPoint2DU5BU5D_t3916425411* L_48 = Collision2D_get_contacts_m4248754423(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 0);
		Vector2_t4282066565  L_49 = ContactPoint2D_get_normal_m2444620420(((L_48)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_50 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		NullCheck(L_45);
		FsmVector3_set_Value_m716982822(L_45, L_50, /*hidden argument*/NULL);
	}

IL_0161:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::OnEnter()
extern "C"  void GetCollision2dInfo_OnEnter_m199949849 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method)
{
	{
		GetCollision2dInfo_StoreCollisionInfo_m2896075873(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::.ctor()
extern "C"  void GetCollisionInfo__ctor_m1216000948 (GetCollisionInfo_t3783581426 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::Reset()
extern "C"  void GetCollisionInfo_Reset_m3157401185 (GetCollisionInfo_t3783581426 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_relativeVelocity_12((FsmVector3_t533912882 *)NULL);
		__this->set_relativeSpeed_13((FsmFloat_t2134102846 *)NULL);
		__this->set_contactPoint_14((FsmVector3_t533912882 *)NULL);
		__this->set_contactNormal_15((FsmVector3_t533912882 *)NULL);
		__this->set_physicsMaterialName_16((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::StoreCollisionInfo()
extern "C"  void GetCollisionInfo_StoreCollisionInfo_m2103485615 (GetCollisionInfo_t3783581426 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Collision_t2494107688 * L_1 = Fsm_get_CollisionInfo_m2736371010(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Collision_t2494107688 * L_4 = Fsm_get_CollisionInfo_m2736371010(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Collision_get_gameObject_m4245316464(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmGameObject_set_Value_m297051598(L_2, L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = __this->get_relativeSpeed_13();
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Collision_t2494107688 * L_8 = Fsm_get_CollisionInfo_m2736371010(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Collision_get_relativeVelocity_m1067992885(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_10, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_11 = __this->get_relativeVelocity_12();
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Collision_t2494107688 * L_13 = Fsm_get_CollisionInfo_m2736371010(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = Collision_get_relativeVelocity_m1067992885(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmVector3_set_Value_m716982822(L_11, L_14, /*hidden argument*/NULL);
		FsmString_t952858651 * L_15 = __this->get_physicsMaterialName_16();
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Collision_t2494107688 * L_17 = Fsm_get_CollisionInfo_m2736371010(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Collider_t2939674232 * L_18 = Collision_get_collider_m1325344374(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		PhysicMaterial_t211873335 * L_19 = Collider_get_material_m2158909696(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m3709440845(L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmString_set_Value_m829393196(L_15, L_20, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_21 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Collision_t2494107688 * L_22 = Fsm_get_CollisionInfo_m2736371010(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		ContactPointU5BU5D_t715040733* L_23 = Collision_get_contacts_m658316947(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0108;
		}
	}
	{
		Fsm_t1527112426 * L_24 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Collision_t2494107688 * L_25 = Fsm_get_CollisionInfo_m2736371010(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		ContactPointU5BU5D_t715040733* L_26 = Collision_get_contacts_m658316947(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0108;
		}
	}
	{
		FsmVector3_t533912882 * L_27 = __this->get_contactPoint_14();
		Fsm_t1527112426 * L_28 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Collision_t2494107688 * L_29 = Fsm_get_CollisionInfo_m2736371010(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		ContactPointU5BU5D_t715040733* L_30 = Collision_get_contacts_m658316947(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		Vector3_t4282066566  L_31 = ContactPoint_get_point_m1387782344(((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmVector3_set_Value_m716982822(L_27, L_31, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_32 = __this->get_contactNormal_15();
		Fsm_t1527112426 * L_33 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Collision_t2494107688 * L_34 = Fsm_get_CollisionInfo_m2736371010(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		ContactPointU5BU5D_t715040733* L_35 = Collision_get_contacts_m658316947(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		Vector3_t4282066566  L_36 = ContactPoint_get_normal_m1137164497(((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		NullCheck(L_32);
		FsmVector3_set_Value_m716982822(L_32, L_36, /*hidden argument*/NULL);
	}

IL_0108:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::OnEnter()
extern "C"  void GetCollisionInfo_OnEnter_m4032502411 (GetCollisionInfo_t3783581426 * __this, const MethodInfo* method)
{
	{
		GetCollisionInfo_StoreCollisionInfo_m2103485615(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::.ctor()
extern "C"  void GetColorRGBA__ctor_m1405466109 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::Reset()
extern "C"  void GetColorRGBA_Reset_m3346866346 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method)
{
	{
		__this->set_color_11((FsmColor_t2131419205 *)NULL);
		__this->set_storeRed_12((FsmFloat_t2134102846 *)NULL);
		__this->set_storeGreen_13((FsmFloat_t2134102846 *)NULL);
		__this->set_storeBlue_14((FsmFloat_t2134102846 *)NULL);
		__this->set_storeAlpha_15((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnEnter()
extern "C"  void GetColorRGBA_OnEnter_m1424928404 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method)
{
	{
		GetColorRGBA_DoGetColorRGBA_m1286975923(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnUpdate()
extern "C"  void GetColorRGBA_OnUpdate_m356666927 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method)
{
	{
		GetColorRGBA_DoGetColorRGBA_m1286975923(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::DoGetColorRGBA()
extern "C"  void GetColorRGBA_DoGetColorRGBA_m1286975923 (GetColorRGBA_t1254146697 * __this, const MethodInfo* method)
{
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		FsmColor_t2131419205 * L_0 = __this->get_color_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeRed_12();
		FsmColor_t2131419205 * L_3 = __this->get_color_11();
		NullCheck(L_3);
		Color_t4194546905  L_4 = FsmColor_get_Value_m1679829997(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_r_0();
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = __this->get_storeGreen_13();
		FsmColor_t2131419205 * L_7 = __this->get_color_11();
		NullCheck(L_7);
		Color_t4194546905  L_8 = FsmColor_get_Value_m1679829997(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (&V_1)->get_g_1();
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = __this->get_storeBlue_14();
		FsmColor_t2131419205 * L_11 = __this->get_color_11();
		NullCheck(L_11);
		Color_t4194546905  L_12 = FsmColor_get_Value_m1679829997(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_b_2();
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_storeAlpha_15();
		FsmColor_t2131419205 * L_15 = __this->get_color_11();
		NullCheck(L_15);
		Color_t4194546905  L_16 = FsmColor_get_Value_m1679829997(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_a_3();
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetComponent::.ctor()
extern "C"  void GetComponent__ctor_m682621655 (GetComponent_t2168373359 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetComponent::Reset()
extern "C"  void GetComponent_Reset_m2624021892 (GetComponent_t2168373359 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeComponent_12((FsmObject_t821476169 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnEnter()
extern "C"  void GetComponent_OnEnter_m2556110062 (GetComponent_t2168373359 * __this, const MethodInfo* method)
{
	{
		GetComponent_DoGetComponent_m1989697151(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnUpdate()
extern "C"  void GetComponent_OnUpdate_m1063559957 (GetComponent_t2168373359 * __this, const MethodInfo* method)
{
	{
		GetComponent_DoGetComponent_m1989697151(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetComponent::DoGetComponent()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetComponent_DoGetComponent_m1989697151_MetadataUsageId;
extern "C"  void GetComponent_DoGetComponent_m1989697151 (GetComponent_t2168373359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetComponent_DoGetComponent_m1989697151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		FsmObject_t821476169 * L_0 = __this->get_storeComponent_12();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		FsmObject_t821476169 * L_6 = __this->get_storeComponent_12();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		return;
	}

IL_003c:
	{
		FsmObject_t821476169 * L_8 = __this->get_storeComponent_12();
		GameObject_t3674682005 * L_9 = V_0;
		FsmObject_t821476169 * L_10 = __this->get_storeComponent_12();
		NullCheck(L_10);
		Type_t * L_11 = VirtFuncInvoker0< Type_t * >::Invoke(23 /* System.Type HutongGames.PlayMaker.FsmObject::get_ObjectType() */, L_10);
		NullCheck(L_9);
		Component_t3501516275 * L_12 = GameObject_GetComponent_m1004814461(L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmObject_set_Value_m867520242(L_8, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::.ctor()
extern "C"  void GetControllerCollisionFlags__ctor_m3448610721 (GetControllerCollisionFlags_t722913077 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::Reset()
extern "C"  void GetControllerCollisionFlags_Reset_m1095043662 (GetControllerCollisionFlags_t722913077 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_isGrounded_12((FsmBool_t1075959796 *)NULL);
		__this->set_none_13((FsmBool_t1075959796 *)NULL);
		__this->set_sides_14((FsmBool_t1075959796 *)NULL);
		__this->set_above_15((FsmBool_t1075959796 *)NULL);
		__this->set_below_16((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::OnUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCharacterController_t1618060635_m3112787219_MethodInfo_var;
extern const uint32_t GetControllerCollisionFlags_OnUpdate_m3696251403_MetadataUsageId;
extern "C"  void GetControllerCollisionFlags_OnUpdate_m3696251403 (GetControllerCollisionFlags_t722913077 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetControllerCollisionFlags_OnUpdate_m3696251403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_previousGo_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		NullCheck(L_8);
		CharacterController_t1618060635 * L_9 = GameObject_GetComponent_TisCharacterController_t1618060635_m3112787219(L_8, /*hidden argument*/GameObject_GetComponent_TisCharacterController_t1618060635_m3112787219_MethodInfo_var);
		__this->set_controller_18(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		__this->set_previousGo_17(L_10);
	}

IL_0043:
	{
		CharacterController_t1618060635 * L_11 = __this->get_controller_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00dc;
		}
	}
	{
		FsmBool_t1075959796 * L_13 = __this->get_isGrounded_12();
		CharacterController_t1618060635 * L_14 = __this->get_controller_18();
		NullCheck(L_14);
		bool L_15 = CharacterController_get_isGrounded_m1739295843(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmBool_set_Value_m1126216340(L_13, L_15, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_16 = __this->get_none_13();
		CharacterController_t1618060635 * L_17 = __this->get_controller_18();
		NullCheck(L_17);
		CharacterController_get_collisionFlags_m3620825515(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmBool_set_Value_m1126216340(L_16, (bool)0, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_18 = __this->get_sides_14();
		CharacterController_t1618060635 * L_19 = __this->get_controller_18();
		NullCheck(L_19);
		int32_t L_20 = CharacterController_get_collisionFlags_m3620825515(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmBool_set_Value_m1126216340(L_18, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_20&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_21 = __this->get_above_15();
		CharacterController_t1618060635 * L_22 = __this->get_controller_18();
		NullCheck(L_22);
		int32_t L_23 = CharacterController_get_collisionFlags_m3620825515(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		FsmBool_set_Value_m1126216340(L_21, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_23&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_24 = __this->get_below_16();
		CharacterController_t1618060635 * L_25 = __this->get_controller_18();
		NullCheck(L_25);
		int32_t L_26 = CharacterController_get_collisionFlags_m3620825515(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		FsmBool_set_Value_m1126216340(L_24, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_26&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::.ctor()
extern "C"  void GetControllerHitInfo__ctor_m1323482159 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::Reset()
extern "C"  void GetControllerHitInfo_Reset_m3264882396 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_contactPoint_12((FsmVector3_t533912882 *)NULL);
		__this->set_contactNormal_13((FsmVector3_t533912882 *)NULL);
		__this->set_moveDirection_14((FsmVector3_t533912882 *)NULL);
		__this->set_moveLength_15((FsmFloat_t2134102846 *)NULL);
		__this->set_physicsMaterialName_16((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::OnPreprocess()
extern "C"  void GetControllerHitInfo_OnPreprocess_m2388072992 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleControllerColliderHit_m1230002728(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::StoreTriggerInfo()
extern "C"  void GetControllerHitInfo_StoreTriggerInfo_m16578266 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ControllerColliderHit_t2416790841 * L_1 = Fsm_get_ControllerCollider_m1596430369(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		ControllerColliderHit_t2416790841 * L_4 = Fsm_get_ControllerCollider_m1596430369(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = ControllerColliderHit_get_gameObject_m3516749633(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmGameObject_set_Value_m297051598(L_2, L_5, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_6 = __this->get_contactPoint_12();
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		ControllerColliderHit_t2416790841 * L_8 = Fsm_get_ControllerCollider_m1596430369(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = ControllerColliderHit_get_point_m1436351445(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmVector3_set_Value_m716982822(L_6, L_9, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_10 = __this->get_contactNormal_13();
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		ControllerColliderHit_t2416790841 * L_12 = Fsm_get_ControllerCollider_m1596430369(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = ControllerColliderHit_get_normal_m2642806628(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmVector3_set_Value_m716982822(L_10, L_13, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_14 = __this->get_moveDirection_14();
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		ControllerColliderHit_t2416790841 * L_16 = Fsm_get_ControllerCollider_m1596430369(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = ControllerColliderHit_get_moveDirection_m3645766003(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmVector3_set_Value_m716982822(L_14, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_moveLength_15();
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		ControllerColliderHit_t2416790841 * L_20 = Fsm_get_ControllerCollider_m1596430369(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		float L_21 = ControllerColliderHit_get_moveLength_m3314200770(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_21, /*hidden argument*/NULL);
		FsmString_t952858651 * L_22 = __this->get_physicsMaterialName_16();
		Fsm_t1527112426 * L_23 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		ControllerColliderHit_t2416790841 * L_24 = Fsm_get_ControllerCollider_m1596430369(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Collider_t2939674232 * L_25 = ControllerColliderHit_get_collider_m1024441863(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		PhysicMaterial_t211873335 * L_26 = Collider_get_material_m2158909696(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = Object_get_name_m3709440845(L_26, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmString_set_Value_m829393196(L_22, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::OnEnter()
extern "C"  void GetControllerHitInfo_OnEnter_m4242731078 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		GetControllerHitInfo_StoreTriggerInfo_m16578266(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String HutongGames.PlayMaker.Actions.GetControllerHitInfo::ErrorCheck()
extern "C"  String_t* GetControllerHitInfo_ErrorCheck_m489327160 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		String_t* L_1 = ActionHelpers_CheckOwnerPhysicsSetup_m1952038644(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCosine::.ctor()
extern "C"  void GetCosine__ctor_m648502937 (GetCosine_t451935037 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCosine::Reset()
extern "C"  void GetCosine_Reset_m2589903174 (GetCosine_t451935037 * __this, const MethodInfo* method)
{
	{
		__this->set_angle_11((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_DegToRad_12(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_result_13((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnEnter()
extern "C"  void GetCosine_OnEnter_m4127760432 (GetCosine_t451935037 * __this, const MethodInfo* method)
{
	{
		GetCosine_DoCosine_m1137603311(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnUpdate()
extern "C"  void GetCosine_OnUpdate_m2540081171 (GetCosine_t451935037 * __this, const MethodInfo* method)
{
	{
		GetCosine_DoCosine_m1137603311(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetCosine::DoCosine()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetCosine_DoCosine_m1137603311_MetadataUsageId;
extern "C"  void GetCosine_DoCosine_m1137603311 (GetCosine_t451935037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetCosine_DoCosine_m1137603311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_angle_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmBool_t1075959796 * L_2 = __this->get_DegToRad_12();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4*(float)(0.0174532924f)));
	}

IL_0024:
	{
		FsmFloat_t2134102846 * L_5 = __this->get_result_13();
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = cosf(L_6);
		NullCheck(L_5);
		FsmFloat_set_Value_m1568963140(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::.ctor()
extern "C"  void GetDeviceAcceleration__ctor_m4100135868 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::Reset()
extern "C"  void GetDeviceAcceleration_Reset_m1746568809 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method)
{
	{
		__this->set_storeVector_11((FsmVector3_t533912882 *)NULL);
		__this->set_storeX_12((FsmFloat_t2134102846 *)NULL);
		__this->set_storeY_13((FsmFloat_t2134102846 *)NULL);
		__this->set_storeZ_14((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_multiplier_15(L_0);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnEnter()
extern "C"  void GetDeviceAcceleration_OnEnter_m1137287315 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method)
{
	{
		GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnUpdate()
extern "C"  void GetDeviceAcceleration_OnUpdate_m29727760 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method)
{
	{
		GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::DoGetDeviceAcceleration()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627_MetadataUsageId;
extern "C"  void GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		float L_1 = (&V_1)->get_x_1();
		Vector3_t4282066566  L_2 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_y_2();
		Vector3_t4282066566  L_4 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_4;
		float L_5 = (&V_3)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_1, L_3, L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = __this->get_multiplier_15();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		Vector3_t4282066566  L_8 = V_0;
		FsmFloat_t2134102846 * L_9 = __this->get_multiplier_15();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_0050:
	{
		FsmVector3_t533912882 * L_12 = __this->get_storeVector_11();
		Vector3_t4282066566  L_13 = V_0;
		NullCheck(L_12);
		FsmVector3_set_Value_m716982822(L_12, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_storeX_12();
		float L_15 = (&V_0)->get_x_1();
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_storeY_13();
		float L_17 = (&V_0)->get_y_2();
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_storeZ_14();
		float L_19 = (&V_0)->get_z_3();
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::.ctor()
extern "C"  void GetDeviceRoll__ctor_m3263161695 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t GetDeviceRoll_Reset_m909594636_MetadataUsageId;
extern "C"  void GetDeviceRoll_Reset_m909594636 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetDeviceRoll_Reset_m909594636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_baseOrientation_11(1);
		__this->set_storeAngle_12((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_limitAngle_13(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		__this->set_smoothing_14(L_3);
		__this->set_everyFrame_15((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnEnter()
extern "C"  void GetDeviceRoll_OnEnter_m4258958710 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method)
{
	{
		GetDeviceRoll_DoGetDeviceRoll_m1222090011(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnUpdate()
extern "C"  void GetDeviceRoll_OnUpdate_m2312260493 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method)
{
	{
		GetDeviceRoll_DoGetDeviceRoll_m1222090011(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::DoGetDeviceRoll()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetDeviceRoll_DoGetDeviceRoll_m1222090011_MetadataUsageId;
extern "C"  void GetDeviceRoll_DoGetDeviceRoll_m1222090011 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetDeviceRoll_DoGetDeviceRoll_m1222090011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_0;
		float L_1 = (&V_3)->get_x_1();
		V_0 = L_1;
		Vector3_t4282066566  L_2 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_2;
		float L_3 = (&V_4)->get_y_2();
		V_1 = L_3;
		V_2 = (0.0f);
		int32_t L_4 = __this->get_baseOrientation_11();
		V_5 = L_4;
		int32_t L_5 = V_5;
		if (L_5 == 0)
		{
			goto IL_0043;
		}
		if (L_5 == 1)
		{
			goto IL_0052;
		}
		if (L_5 == 2)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_006e;
	}

IL_0043:
	{
		float L_6 = V_0;
		float L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = atan2f(L_6, ((-L_7)));
		V_2 = ((-L_8));
		goto IL_006e;
	}

IL_0052:
	{
		float L_9 = V_1;
		float L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_11 = atan2f(L_9, ((-L_10)));
		V_2 = L_11;
		goto IL_006e;
	}

IL_0060:
	{
		float L_12 = V_1;
		float L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_14 = atan2f(L_12, L_13);
		V_2 = ((-L_14));
		goto IL_006e;
	}

IL_006e:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_limitAngle_13();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a2;
		}
	}
	{
		float L_17 = V_2;
		FsmFloat_t2134102846 * L_18 = __this->get_limitAngle_13();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_limitAngle_13();
		NullCheck(L_20);
		float L_21 = FsmFloat_get_Value_m4137923823(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, ((float)((float)(57.29578f)*(float)L_17)), ((-L_19)), L_21, /*hidden argument*/NULL);
		V_2 = L_22;
	}

IL_00a2:
	{
		FsmFloat_t2134102846 * L_23 = __this->get_smoothing_14();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		if ((!(((float)L_24) > ((float)(0.0f)))))
		{
			goto IL_00d5;
		}
	}
	{
		float L_25 = __this->get_lastZAngle_16();
		float L_26 = V_2;
		FsmFloat_t2134102846 * L_27 = __this->get_smoothing_14();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		float L_29 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_30 = Mathf_LerpAngle_m1852538964(NULL /*static, unused*/, L_25, L_26, ((float)((float)L_28*(float)L_29)), /*hidden argument*/NULL);
		V_2 = L_30;
	}

IL_00d5:
	{
		float L_31 = V_2;
		__this->set_lastZAngle_16(L_31);
		FsmFloat_t2134102846 * L_32 = __this->get_storeAngle_12();
		float L_33 = V_2;
		NullCheck(L_32);
		FsmFloat_set_Value_m1568963140(L_32, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDistance::.ctor()
extern "C"  void GetDistance__ctor_m2761081693 (GetDistance_t1948520441 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDistance::Reset()
extern "C"  void GetDistance_Reset_m407514634 (GetDistance_t1948520441 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_target_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeResult_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDistance::OnEnter()
extern "C"  void GetDistance_OnEnter_m2796413940 (GetDistance_t1948520441 * __this, const MethodInfo* method)
{
	{
		GetDistance_DoGetDistance_m1181632091(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDistance::OnUpdate()
extern "C"  void GetDistance_OnUpdate_m4218012879 (GetDistance_t1948520441 * __this, const MethodInfo* method)
{
	{
		GetDistance_DoGetDistance_m1181632091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetDistance::DoGetDistance()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetDistance_DoGetDistance_m1181632091_MetadataUsageId;
extern "C"  void GetDistance_DoGetDistance_m1181632091 (GetDistance_t1948520441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetDistance_DoGetDistance_m1181632091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_target_12();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		FsmFloat_t2134102846 * L_8 = __this->get_storeResult_13();
		if (L_8)
		{
			goto IL_0040;
		}
	}

IL_003f:
	{
		return;
	}

IL_0040:
	{
		FsmFloat_t2134102846 * L_9 = __this->get_storeResult_13();
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Transform_get_position_m2211398607(L_11, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_13 = __this->get_target_12();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = FsmGameObject_get_Value_m673294275(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = GameObject_get_transform_m1278640159(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t4282066566  L_16 = Transform_get_position_m2211398607(L_15, /*hidden argument*/NULL);
		float L_17 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_12, L_16, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmFloat_set_Value_m1568963140(L_9, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::.ctor()
extern "C"  void GetEventInfo__ctor_m4014084556 (GetEventInfo_t3599372762 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::Reset()
extern "C"  void GetEventInfo_Reset_m1660517497 (GetEventInfo_t3599372762 * __this, const MethodInfo* method)
{
	{
		__this->set_sentByGameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_fsmName_12((FsmString_t952858651 *)NULL);
		__this->set_getBoolData_13((FsmBool_t1075959796 *)NULL);
		__this->set_getIntData_14((FsmInt_t1596138449 *)NULL);
		__this->set_getFloatData_15((FsmFloat_t2134102846 *)NULL);
		__this->set_getVector2Data_16((FsmVector2_t533912881 *)NULL);
		__this->set_getVector3Data_17((FsmVector3_t533912882 *)NULL);
		__this->set_getStringData_18((FsmString_t952858651 *)NULL);
		__this->set_getGameObjectData_19((FsmGameObject_t1697147867 *)NULL);
		__this->set_getRectData_20((FsmRect_t1076426478 *)NULL);
		__this->set_getQuaternionData_21((FsmQuaternion_t3871136040 *)NULL);
		__this->set_getMaterialData_22((FsmMaterial_t924399665 *)NULL);
		__this->set_getTextureData_23((FsmTexture_t3073272573 *)NULL);
		__this->set_getColorData_24((FsmColor_t2131419205 *)NULL);
		__this->set_getObjectData_25((FsmObject_t821476169 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::OnEnter()
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetEventInfo_OnEnter_m46355107_MetadataUsageId;
extern "C"  void GetEventInfo_OnEnter_m46355107 (GetEventInfo_t3599372762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetEventInfo_OnEnter_m46355107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_0);
		Fsm_t1527112426 * L_1 = L_0->get_SentByFsm_0();
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		FsmGameObject_t1697147867 * L_2 = __this->get_sentByGameObject_11();
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_3 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_3);
		Fsm_t1527112426 * L_4 = L_3->get_SentByFsm_0();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Fsm_get_GameObject_m993266224(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmGameObject_set_Value_m297051598(L_2, L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_6 = __this->get_fsmName_12();
		FsmEventData_t1076900934 * L_7 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = L_7->get_SentByFsm_0();
		NullCheck(L_8);
		String_t* L_9 = Fsm_get_Name_m2749457024(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmString_set_Value_m829393196(L_6, L_9, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_0048:
	{
		FsmGameObject_t1697147867 * L_10 = __this->get_sentByGameObject_11();
		NullCheck(L_10);
		FsmGameObject_set_Value_m297051598(L_10, (GameObject_t3674682005 *)NULL, /*hidden argument*/NULL);
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_11);
		FsmString_set_Value_m829393196(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0064:
	{
		FsmBool_t1075959796 * L_13 = __this->get_getBoolData_13();
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_14 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_14);
		bool L_15 = L_14->get_BoolData_3();
		NullCheck(L_13);
		FsmBool_set_Value_m1126216340(L_13, L_15, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_16 = __this->get_getIntData_14();
		FsmEventData_t1076900934 * L_17 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_IntData_4();
		NullCheck(L_16);
		FsmInt_set_Value_m2087583461(L_16, L_18, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_19 = __this->get_getFloatData_15();
		FsmEventData_t1076900934 * L_20 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_20);
		float L_21 = L_20->get_FloatData_5();
		NullCheck(L_19);
		FsmFloat_set_Value_m1568963140(L_19, L_21, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_22 = __this->get_getVector2Data_16();
		FsmEventData_t1076900934 * L_23 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_23);
		Vector2_t4282066565  L_24 = L_23->get_Vector2Data_6();
		NullCheck(L_22);
		FsmVector2_set_Value_m2900659718(L_22, L_24, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_25 = __this->get_getVector3Data_17();
		FsmEventData_t1076900934 * L_26 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_26);
		Vector3_t4282066566  L_27 = L_26->get_Vector3Data_7();
		NullCheck(L_25);
		FsmVector3_set_Value_m716982822(L_25, L_27, /*hidden argument*/NULL);
		FsmString_t952858651 * L_28 = __this->get_getStringData_18();
		FsmEventData_t1076900934 * L_29 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_29);
		String_t* L_30 = L_29->get_StringData_8();
		NullCheck(L_28);
		FsmString_set_Value_m829393196(L_28, L_30, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_31 = __this->get_getGameObjectData_19();
		FsmEventData_t1076900934 * L_32 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_32);
		GameObject_t3674682005 * L_33 = L_32->get_GameObjectData_13();
		NullCheck(L_31);
		FsmGameObject_set_Value_m297051598(L_31, L_33, /*hidden argument*/NULL);
		FsmRect_t1076426478 * L_34 = __this->get_getRectData_20();
		FsmEventData_t1076900934 * L_35 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_35);
		Rect_t4241904616  L_36 = L_35->get_RectData_10();
		NullCheck(L_34);
		FsmRect_set_Value_m1159518952(L_34, L_36, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_37 = __this->get_getQuaternionData_21();
		FsmEventData_t1076900934 * L_38 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_38);
		Quaternion_t1553702882  L_39 = L_38->get_QuaternionData_9();
		NullCheck(L_37);
		FsmQuaternion_set_Value_m446581172(L_37, L_39, /*hidden argument*/NULL);
		FsmMaterial_t924399665 * L_40 = __this->get_getMaterialData_22();
		FsmEventData_t1076900934 * L_41 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_41);
		Material_t3870600107 * L_42 = L_41->get_MaterialData_14();
		NullCheck(L_40);
		FsmMaterial_set_Value_m3341549218(L_40, L_42, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_43 = __this->get_getTextureData_23();
		FsmEventData_t1076900934 * L_44 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_44);
		Texture_t2526458961 * L_45 = L_44->get_TextureData_15();
		NullCheck(L_43);
		FsmTexture_set_Value_m2261522310(L_43, L_45, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_46 = __this->get_getColorData_24();
		FsmEventData_t1076900934 * L_47 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_47);
		Color_t4194546905  L_48 = L_47->get_ColorData_11();
		NullCheck(L_46);
		FsmColor_set_Value_m1684002054(L_46, L_48, /*hidden argument*/NULL);
		FsmObject_t821476169 * L_49 = __this->get_getObjectData_25();
		FsmEventData_t1076900934 * L_50 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NullCheck(L_50);
		Object_t3071478659 * L_51 = L_50->get_ObjectData_12();
		NullCheck(L_49);
		FsmObject_set_Value_m867520242(L_49, L_51, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::.ctor()
extern "C"  void GetFsmArray__ctor_m3702716121 (GetFsmArray_t1205911805 * __this, const MethodInfo* method)
{
	{
		BaseFsmVariableAction__ctor_m2293414119(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmArray_Reset_m1349149062_MetadataUsageId;
extern "C"  void GetFsmArray_Reset_m1349149062 (GetFsmArray_t1205911805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmArray_Reset_m1349149062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_16((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_17(L_1);
		__this->set_variableName_18((FsmString_t952858651 *)NULL);
		__this->set_storeValue_19((FsmArray_t2129666875 *)NULL);
		__this->set_copyValues_20((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::OnEnter()
extern "C"  void GetFsmArray_OnEnter_m1468999792 (GetFsmArray_t1205911805 * __this, const MethodInfo* method)
{
	{
		GetFsmArray_DoSetFsmArrayCopy_m4230331196(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::DoSetFsmArrayCopy()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* VariableType_t3118725144_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3708554905;
extern Il2CppCodeGenString* _stringLiteral199741905;
extern Il2CppCodeGenString* _stringLiteral62;
extern const uint32_t GetFsmArray_DoSetFsmArrayCopy_m4230331196_MetadataUsageId;
extern "C"  void GetFsmArray_DoSetFsmArrayCopy_m4230331196 (GetFsmArray_t1205911805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmArray_DoSetFsmArrayCopy_m4230331196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmArray_t2129666875 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_16();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		FsmString_t952858651 * L_4 = __this->get_fsmName_17();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		bool L_6 = BaseFsmVariableAction_UpdateCache_m3373753190(__this, L_3, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		PlayMakerFSM_t3799847376 * L_7 = ((BaseFsmVariableAction_t2224198959 *)__this)->get_fsm_15();
		NullCheck(L_7);
		FsmVariables_t963491929 * L_8 = PlayMakerFSM_get_FsmVariables_m1986570741(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_variableName_18();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmArray_t2129666875 * L_11 = FsmVariables_GetFsmArray_m457345943(L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		FsmArray_t2129666875 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_00fa;
		}
	}
	{
		FsmArray_t2129666875 * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = FsmArray_get_ElementType_m310141636(L_13, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_15 = __this->get_storeValue_19();
		NullCheck(L_15);
		int32_t L_16 = FsmArray_get_ElementType_m310141636(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)L_16)))
		{
			goto IL_00ad;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_17 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, _stringLiteral3708554905);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3708554905);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		FsmArray_t2129666875 * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = FsmArray_get_ElementType_m310141636(L_19, /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(VariableType_t3118725144_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral199741905);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral199741905);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		FsmArray_t2129666875 * L_25 = __this->get_storeValue_19();
		NullCheck(L_25);
		int32_t L_26 = FsmArray_get_ElementType_m310141636(L_25, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(VariableType_t3118725144_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_28);
		ObjectU5BU5D_t1108656482* L_29 = L_24;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 4);
		ArrayElementTypeCheck (L_29, _stringLiteral62);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m3016520001(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_30, /*hidden argument*/NULL);
		return;
	}

IL_00ad:
	{
		FsmArray_t2129666875 * L_31 = __this->get_storeValue_19();
		NullCheck(L_31);
		FsmArray_Resize_m3367576465(L_31, 0, /*hidden argument*/NULL);
		bool L_32 = __this->get_copyValues_20();
		if (!L_32)
		{
			goto IL_00e4;
		}
	}
	{
		FsmArray_t2129666875 * L_33 = __this->get_storeValue_19();
		FsmArray_t2129666875 * L_34 = V_1;
		NullCheck(L_34);
		ObjectU5BU5D_t1108656482* L_35 = FsmArray_get_Values_m1362031434(L_34, /*hidden argument*/NULL);
		NullCheck((Il2CppArray *)(Il2CppArray *)L_35);
		Il2CppObject * L_36 = Array_Clone_m1369268734((Il2CppArray *)(Il2CppArray *)L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		FsmArray_set_Values_m2547392359(L_33, ((ObjectU5BU5D_t1108656482*)IsInst(L_36, ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00f5;
	}

IL_00e4:
	{
		FsmArray_t2129666875 * L_37 = __this->get_storeValue_19();
		FsmArray_t2129666875 * L_38 = V_1;
		NullCheck(L_38);
		ObjectU5BU5D_t1108656482* L_39 = FsmArray_get_Values_m1362031434(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		FsmArray_set_Values_m2547392359(L_37, L_39, /*hidden argument*/NULL);
	}

IL_00f5:
	{
		goto IL_010b;
	}

IL_00fa:
	{
		FsmString_t952858651 * L_40 = __this->get_variableName_18();
		NullCheck(L_40);
		String_t* L_41 = FsmString_get_Value_m872383149(L_40, /*hidden argument*/NULL);
		BaseFsmVariableAction_DoVariableNotFound_m1188175663(__this, L_41, /*hidden argument*/NULL);
	}

IL_010b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::.ctor()
extern "C"  void GetFsmArrayItem__ctor_m1289967110 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method)
{
	{
		BaseFsmVariableIndexAction__ctor_m3209182785(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmArrayItem_Reset_m3231367347_MetadataUsageId;
extern "C"  void GetFsmArrayItem_Reset_m3231367347 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmArrayItem_Reset_m3231367347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_17((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_18(L_1);
		__this->set_storeValue_21((FsmVar_t1596150537 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::OnEnter()
extern "C"  void GetFsmArrayItem_OnEnter_m2099540061 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method)
{
	{
		GetFsmArrayItem_DoGetFsmArray_m1586405384(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_22();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::DoGetFsmArray()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3013508541;
extern const uint32_t GetFsmArrayItem_DoGetFsmArray_m1586405384_MetadataUsageId;
extern "C"  void GetFsmArrayItem_DoGetFsmArray_m1586405384 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmArrayItem_DoGetFsmArray_m1586405384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmArray_t2129666875 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_17();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		FsmString_t952858651 * L_4 = __this->get_fsmName_18();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		bool L_6 = BaseFsmVariableIndexAction_UpdateCache_m1136982360(__this, L_3, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		PlayMakerFSM_t3799847376 * L_7 = ((BaseFsmVariableIndexAction_t2226508741 *)__this)->get_fsm_16();
		NullCheck(L_7);
		FsmVariables_t963491929 * L_8 = PlayMakerFSM_get_FsmVariables_m1986570741(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_variableName_19();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmArray_t2129666875 * L_11 = FsmVariables_GetFsmArray_m457345943(L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		FsmArray_t2129666875 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_00e7;
		}
	}
	{
		FsmInt_t1596138449 * L_13 = __this->get_index_20();
		NullCheck(L_13);
		int32_t L_14 = FsmInt_get_Value_m27059446(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		FsmInt_t1596138449 * L_15 = __this->get_index_20();
		NullCheck(L_15);
		int32_t L_16 = FsmInt_get_Value_m27059446(L_15, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_17 = V_1;
		NullCheck(L_17);
		int32_t L_18 = FsmArray_get_Length_m3299442701(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_008b;
		}
	}

IL_0073:
	{
		Fsm_t1527112426 * L_19 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_20 = ((BaseFsmVariableIndexAction_t2226508741 *)__this)->get_indexOutOfRange_11();
		NullCheck(L_19);
		Fsm_Event_m625948263(L_19, L_20, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_008b:
	{
		FsmArray_t2129666875 * L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = FsmArray_get_ElementType_m310141636(L_21, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_23 = __this->get_storeValue_21();
		NullCheck(L_23);
		NamedVariable_t3211770239 * L_24 = FsmVar_get_NamedVar_m894426299(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(22 /* HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_VariableType() */, L_24);
		if ((!(((uint32_t)L_22) == ((uint32_t)L_25))))
		{
			goto IL_00c7;
		}
	}
	{
		FsmVar_t1596150537 * L_26 = __this->get_storeValue_21();
		FsmArray_t2129666875 * L_27 = V_1;
		FsmInt_t1596138449 * L_28 = __this->get_index_20();
		NullCheck(L_28);
		int32_t L_29 = FsmInt_get_Value_m27059446(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Il2CppObject * L_30 = FsmArray_Get_m160560104(L_27, L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		FsmVar_SetValue_m2004058539(L_26, L_30, /*hidden argument*/NULL);
		goto IL_00e2;
	}

IL_00c7:
	{
		FsmString_t952858651 * L_31 = __this->get_variableName_19();
		NullCheck(L_31);
		String_t* L_32 = FsmString_get_Value_m872383149(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3013508541, L_32, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_33, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		goto IL_00f8;
	}

IL_00e7:
	{
		FsmString_t952858651 * L_34 = __this->get_variableName_19();
		NullCheck(L_34);
		String_t* L_35 = FsmString_get_Value_m872383149(L_34, /*hidden argument*/NULL);
		BaseFsmVariableIndexAction_DoVariableNotFound_m22759433(__this, L_35, /*hidden argument*/NULL);
	}

IL_00f8:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmArrayItem::OnUpdate()
extern "C"  void GetFsmArrayItem_OnUpdate_m4089759110 (GetFsmArrayItem_t2453534512 * __this, const MethodInfo* method)
{
	{
		GetFsmArrayItem_DoGetFsmArray_m1586405384(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
