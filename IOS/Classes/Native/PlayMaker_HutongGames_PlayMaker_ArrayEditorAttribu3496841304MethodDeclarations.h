﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ArrayEditorAttribute
struct ArrayEditorAttribute_t3496841304;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"

// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.ArrayEditorAttribute::get_VariableType()
extern "C"  int32_t ArrayEditorAttribute_get_VariableType_m954147623 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ArrayEditorAttribute::get_ObjectType()
extern "C"  Type_t * ArrayEditorAttribute_get_ObjectType_m118407589 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ArrayEditorAttribute::get_ElementName()
extern "C"  String_t* ArrayEditorAttribute_get_ElementName_m2914357652 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_FixedSize()
extern "C"  int32_t ArrayEditorAttribute_get_FixedSize_m2437574259 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ArrayEditorAttribute::get_Resizable()
extern "C"  bool ArrayEditorAttribute_get_Resizable_m3060649507 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_MinSize()
extern "C"  int32_t ArrayEditorAttribute_get_MinSize_m3722162705 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_MaxSize()
extern "C"  int32_t ArrayEditorAttribute_get_MaxSize_m2949684131 (ArrayEditorAttribute_t3496841304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ArrayEditorAttribute::.ctor(HutongGames.PlayMaker.VariableType,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void ArrayEditorAttribute__ctor_m2557472386 (ArrayEditorAttribute_t3496841304 * __this, int32_t ___variableType0, String_t* ___elementName1, int32_t ___fixedSize2, int32_t ___minSize3, int32_t ___maxSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ArrayEditorAttribute::.ctor(System.Type,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void ArrayEditorAttribute__ctor_m2381363037 (ArrayEditorAttribute_t3496841304 * __this, Type_t * ___objectType0, String_t* ___elementName1, int32_t ___fixedSize2, int32_t ___minSize3, int32_t ___maxSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
