﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c
struct U3CU3Ec_t1701094040;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_t351813185;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct Func_2_t1245559798;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_t1756683401;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32>
struct Func_2_t1391807484;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c
struct  U3CU3Ec_t1701094040  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t1701094040_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9
	U3CU3Ec_t1701094040 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__30_0
	Func_2_t351813185 * ___U3CU3E9__30_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__30_1
	Func_2_t351813185 * ___U3CU3E9__30_1_2;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__33_0
	Func_2_t1245559798 * ___U3CU3E9__33_0_3;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__33_1
	Func_2_t351813185 * ___U3CU3E9__33_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__36_0
	Func_2_t1756683401 * ___U3CU3E9__36_0_5;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__60_0
	Func_2_t1391807484 * ___U3CU3E9__60_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1701094040 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1701094040 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1701094040 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Func_2_t351813185 * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Func_2_t351813185 ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Func_2_t351813185 * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__30_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__30_1_2)); }
	inline Func_2_t351813185 * get_U3CU3E9__30_1_2() const { return ___U3CU3E9__30_1_2; }
	inline Func_2_t351813185 ** get_address_of_U3CU3E9__30_1_2() { return &___U3CU3E9__30_1_2; }
	inline void set_U3CU3E9__30_1_2(Func_2_t351813185 * value)
	{
		___U3CU3E9__30_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__30_1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__33_0_3)); }
	inline Func_2_t1245559798 * get_U3CU3E9__33_0_3() const { return ___U3CU3E9__33_0_3; }
	inline Func_2_t1245559798 ** get_address_of_U3CU3E9__33_0_3() { return &___U3CU3E9__33_0_3; }
	inline void set_U3CU3E9__33_0_3(Func_2_t1245559798 * value)
	{
		___U3CU3E9__33_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__33_0_3, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__33_1_4)); }
	inline Func_2_t351813185 * get_U3CU3E9__33_1_4() const { return ___U3CU3E9__33_1_4; }
	inline Func_2_t351813185 ** get_address_of_U3CU3E9__33_1_4() { return &___U3CU3E9__33_1_4; }
	inline void set_U3CU3E9__33_1_4(Func_2_t351813185 * value)
	{
		___U3CU3E9__33_1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__33_1_4, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__36_0_5)); }
	inline Func_2_t1756683401 * get_U3CU3E9__36_0_5() const { return ___U3CU3E9__36_0_5; }
	inline Func_2_t1756683401 ** get_address_of_U3CU3E9__36_0_5() { return &___U3CU3E9__36_0_5; }
	inline void set_U3CU3E9__36_0_5(Func_2_t1756683401 * value)
	{
		___U3CU3E9__36_0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__36_0_5, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__60_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1701094040_StaticFields, ___U3CU3E9__60_0_6)); }
	inline Func_2_t1391807484 * get_U3CU3E9__60_0_6() const { return ___U3CU3E9__60_0_6; }
	inline Func_2_t1391807484 ** get_address_of_U3CU3E9__60_0_6() { return &___U3CU3E9__60_0_6; }
	inline void set_U3CU3E9__60_0_6(Func_2_t1391807484 * value)
	{
		___U3CU3E9__60_0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__60_0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
