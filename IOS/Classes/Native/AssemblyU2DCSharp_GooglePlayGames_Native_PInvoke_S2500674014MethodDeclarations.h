﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t2500674014;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t3479575958;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::.ctor(System.IntPtr)
extern "C"  void SnapshotSelectUIResponse__ctor_m839008807 (SnapshotSelectUIResponse_t2500674014 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestStatus()
extern "C"  int32_t SnapshotSelectUIResponse_RequestStatus_m4165722402 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestSucceeded()
extern "C"  bool SnapshotSelectUIResponse_RequestSucceeded_m3140975669 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::Data()
extern "C"  NativeSnapshotMetadata_t3479575958 * SnapshotSelectUIResponse_Data_m1876063291 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotSelectUIResponse_CallDispose_m1535290473 (SnapshotSelectUIResponse_t2500674014 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::FromPointer(System.IntPtr)
extern "C"  SnapshotSelectUIResponse_t2500674014 * SnapshotSelectUIResponse_FromPointer_m2303338145 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
