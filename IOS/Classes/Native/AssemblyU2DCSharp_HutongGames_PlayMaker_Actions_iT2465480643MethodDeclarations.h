﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchPosition
struct iTweenPunchPosition_t2465480643;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::.ctor()
extern "C"  void iTweenPunchPosition__ctor_m3218296531 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::Reset()
extern "C"  void iTweenPunchPosition_Reset_m864729472 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnEnter()
extern "C"  void iTweenPunchPosition_OnEnter_m4093209066 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnExit()
extern "C"  void iTweenPunchPosition_OnExit_m2496263278 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::DoiTween()
extern "C"  void iTweenPunchPosition_DoiTween_m3786561694 (iTweenPunchPosition_t2465480643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
