﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>
struct U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::.ctor()
extern "C"  void U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m338042362_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m338042362(__this, method) ((  void (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m338042362_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3997015363_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3997015363(__this, method) ((  Il2CppObject * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3997015363_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m1936491894_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m1936491894(__this, method) ((  Il2CppObject * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m1936491894_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m3231418263_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m3231418263(__this, method) ((  Il2CppObject * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m3231418263_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3463825750_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3463825750(__this, method) ((  Il2CppObject* (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3463825750_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m240226434_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m240226434(__this, method) ((  bool (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m240226434_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::Dispose()
extern "C"  void U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m2634445751_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m2634445751(__this, method) ((  void (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m2634445751_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::Reset()
extern "C"  void U3CCreateExceptIteratorU3Ec__Iterator4_1_Reset_m2279442599_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 * __this, const MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_Reset_m2279442599(__this, method) ((  void (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t2167938059 *, const MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_Reset_m2279442599_gshared)(__this, method)
