﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"


extern const Il2CppMethodPointer g_MethodPointers[];
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[];
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[];
extern const Il2CppMethodPointer g_Il2CppGenericMethodPointers[];
extern const InvokerMethod g_Il2CppInvokerPointers[];
extern const CustomAttributesCacheGenerator g_AttributeGenerators[];
const Il2CppCodeRegistration g_CodeRegistration = 
{
	25690,
	g_MethodPointers,
	47,
	g_ReversePInvokeWrapperPointers,
	104,
	g_DelegateWrappersManagedToNative,
	222,
	g_MarshalingFunctions,
	0,
	NULL,
	9366,
	g_Il2CppGenericMethodPointers,
	2795,
	g_Il2CppInvokerPointers,
	8738,
	g_AttributeGenerators,
	0,
	NULL,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static const Il2CppCodeGenOptions s_Il2CppCodeGenOptions = 
{
	false,
};
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration, &s_Il2CppCodeGenOptions);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
