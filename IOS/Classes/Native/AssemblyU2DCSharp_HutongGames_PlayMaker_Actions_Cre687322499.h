﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateObject
struct  CreateObject_t687322499  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::spawnPoint
	FsmGameObject_t1697147867 * ___spawnPoint_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::position
	FsmVector3_t533912882 * ___position_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::rotation
	FsmVector3_t533912882 * ___rotation_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::storeObject
	FsmGameObject_t1697147867 * ___storeObject_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CreateObject::networkInstantiate
	FsmBool_t1075959796 * ___networkInstantiate_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CreateObject::networkGroup
	FsmInt_t1596138449 * ___networkGroup_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___gameObject_11)); }
	inline FsmGameObject_t1697147867 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t1697147867 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_spawnPoint_12() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___spawnPoint_12)); }
	inline FsmGameObject_t1697147867 * get_spawnPoint_12() const { return ___spawnPoint_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_spawnPoint_12() { return &___spawnPoint_12; }
	inline void set_spawnPoint_12(FsmGameObject_t1697147867 * value)
	{
		___spawnPoint_12 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPoint_12, value);
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___position_13)); }
	inline FsmVector3_t533912882 * get_position_13() const { return ___position_13; }
	inline FsmVector3_t533912882 ** get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(FsmVector3_t533912882 * value)
	{
		___position_13 = value;
		Il2CppCodeGenWriteBarrier(&___position_13, value);
	}

	inline static int32_t get_offset_of_rotation_14() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___rotation_14)); }
	inline FsmVector3_t533912882 * get_rotation_14() const { return ___rotation_14; }
	inline FsmVector3_t533912882 ** get_address_of_rotation_14() { return &___rotation_14; }
	inline void set_rotation_14(FsmVector3_t533912882 * value)
	{
		___rotation_14 = value;
		Il2CppCodeGenWriteBarrier(&___rotation_14, value);
	}

	inline static int32_t get_offset_of_storeObject_15() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___storeObject_15)); }
	inline FsmGameObject_t1697147867 * get_storeObject_15() const { return ___storeObject_15; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeObject_15() { return &___storeObject_15; }
	inline void set_storeObject_15(FsmGameObject_t1697147867 * value)
	{
		___storeObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeObject_15, value);
	}

	inline static int32_t get_offset_of_networkInstantiate_16() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___networkInstantiate_16)); }
	inline FsmBool_t1075959796 * get_networkInstantiate_16() const { return ___networkInstantiate_16; }
	inline FsmBool_t1075959796 ** get_address_of_networkInstantiate_16() { return &___networkInstantiate_16; }
	inline void set_networkInstantiate_16(FsmBool_t1075959796 * value)
	{
		___networkInstantiate_16 = value;
		Il2CppCodeGenWriteBarrier(&___networkInstantiate_16, value);
	}

	inline static int32_t get_offset_of_networkGroup_17() { return static_cast<int32_t>(offsetof(CreateObject_t687322499, ___networkGroup_17)); }
	inline FsmInt_t1596138449 * get_networkGroup_17() const { return ___networkGroup_17; }
	inline FsmInt_t1596138449 ** get_address_of_networkGroup_17() { return &___networkGroup_17; }
	inline void set_networkGroup_17(FsmInt_t1596138449 * value)
	{
		___networkGroup_17 = value;
		Il2CppCodeGenWriteBarrier(&___networkGroup_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
