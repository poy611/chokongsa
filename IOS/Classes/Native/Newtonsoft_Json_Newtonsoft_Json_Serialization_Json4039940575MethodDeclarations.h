﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c
struct U3CU3Ec_t4039940575;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m3360730406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2340719463 (U3CU3Ec_t4039940575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<CreateObjectUsingCreatorWithParameters>b__36_0(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_0_m1051783801 (U3CU3Ec_t4039940575 * __this, JsonProperty_t902655177 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<CreateObjectUsingCreatorWithParameters>b__36_2(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__36_2_m1945270139 (U3CU3Ec_t4039940575 * __this, JsonProperty_t902655177 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<PopulateObject>b__41_0(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonProperty_t902655177 * U3CU3Ec_U3CPopulateObjectU3Eb__41_0_m14734953 (U3CU3Ec_t4039940575 * __this, JsonProperty_t902655177 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<PopulateObject>b__41_1(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  int32_t U3CU3Ec_U3CPopulateObjectU3Eb__41_1_m1402579084 (U3CU3Ec_t4039940575 * __this, JsonProperty_t902655177 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
