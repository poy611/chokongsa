﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseFloat
struct EaseFloat_t1438389852;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseFloat::.ctor()
extern "C"  void EaseFloat__ctor_m2771277402 (EaseFloat_t1438389852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::Reset()
extern "C"  void EaseFloat_Reset_m417710343 (EaseFloat_t1438389852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnEnter()
extern "C"  void EaseFloat_OnEnter_m4004555697 (EaseFloat_t1438389852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnExit()
extern "C"  void EaseFloat_OnExit_m1523572167 (EaseFloat_t1438389852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnUpdate()
extern "C"  void EaseFloat_OnUpdate_m3015701682 (EaseFloat_t1438389852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
