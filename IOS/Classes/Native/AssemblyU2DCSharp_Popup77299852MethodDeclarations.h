﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Popup
struct Popup_t77299852;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Popup::.ctor()
extern "C"  void Popup__ctor_m1920214623 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Start()
extern "C"  void Popup_Start_m867352415 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::judgeOk()
extern "C"  void Popup_judgeOk_m1463042864 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::ok()
extern "C"  void Popup_ok_m3479241409 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::newGame()
extern "C"  void Popup_newGame_m3766727119 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::cancle()
extern "C"  void Popup_cancle_m3908728369 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Retry()
extern "C"  void Popup_Retry_m3862930469 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::RetryLater()
extern "C"  void Popup_RetryLater_m428614601 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Continue()
extern "C"  void Popup_Continue_m2777922540 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::OnSave()
extern "C"  void Popup_OnSave_m1290369793 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::csSwitch(System.Single)
extern "C"  void Popup_csSwitch_m2527723682 (Popup_t77299852 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::juSwitch(System.Single)
extern "C"  void Popup_juSwitch_m2869995175 (Popup_t77299852 * __this, float ___ju0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Popup::stringSwitch(System.Single)
extern "C"  String_t* Popup_stringSwitch_m2795876100 (Popup_t77299852 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::RestartOnThisGame()
extern "C"  void Popup_RestartOnThisGame_m3031943195 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::CalNextQuestion()
extern "C"  void Popup_CalNextQuestion_m2529532324 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::BackLearnPage()
extern "C"  void Popup_BackLearnPage_m1309023817 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::PopCancle()
extern "C"  void Popup_PopCancle_m985968442 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::Quit()
extern "C"  void Popup_Quit_m1218808212 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Popup::OnDestroy()
extern "C"  void Popup_OnDestroy_m3304960984 (Popup_t77299852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
