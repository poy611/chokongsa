﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.DebugUtils
struct DebugUtils_t257587552;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.DebugUtils::Assert(System.Boolean)
extern "C"  void DebugUtils_Assert_m81460206 (Il2CppObject * __this /* static, unused */, bool ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DebugUtils::.ctor()
extern "C"  void DebugUtils__ctor_m2263895539 (DebugUtils_t257587552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
