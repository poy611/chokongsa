﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo
struct ExtractionInfo_t925438340;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1
struct  U3CExtractDownloadUrlsU3Ed__1_t1414174443  : public Il2CppObject
{
public:
	// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>2__current
	ExtractionInfo_t925438340 * ___U3CU3E2__current_0;
	// System.Int32 YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>1__state
	int32_t ___U3CU3E1__state_1;
	// System.Int32 YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JObject YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::json
	JObject_t1798544199 * ___json_3;
	// Newtonsoft.Json.Linq.JObject YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>3__json
	JObject_t1798544199 * ___U3CU3E3__json_4;
	// System.String[] YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<splitByUrls>5__2
	StringU5BU5D_t4054002952* ___U3CsplitByUrlsU3E5__2_5;
	// System.String YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<s>5__3
	String_t* ___U3CsU3E5__3_6;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<queries>5__4
	Il2CppObject* ___U3CqueriesU3E5__4_7;
	// System.String YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<url>5__5
	String_t* ___U3CurlU3E5__5_8;
	// System.Boolean YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<requiresDecryption>5__6
	bool ___U3CrequiresDecryptionU3E5__6_9;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<parameters>5__7
	Il2CppObject* ___U3CparametersU3E5__7_10;
	// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>g__initLocal0
	ExtractionInfo_t925438340 * ___U3CU3Eg__initLocal0_11;
	// System.String[] YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>7__wrap9
	StringU5BU5D_t4054002952* ___U3CU3E7__wrap9_12;
	// System.Int32 YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>7__wrapa
	int32_t ___U3CU3E7__wrapa_13;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3E2__current_0)); }
	inline ExtractionInfo_t925438340 * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline ExtractionInfo_t925438340 ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(ExtractionInfo_t925438340 * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_json_3() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___json_3)); }
	inline JObject_t1798544199 * get_json_3() const { return ___json_3; }
	inline JObject_t1798544199 ** get_address_of_json_3() { return &___json_3; }
	inline void set_json_3(JObject_t1798544199 * value)
	{
		___json_3 = value;
		Il2CppCodeGenWriteBarrier(&___json_3, value);
	}

	inline static int32_t get_offset_of_U3CU3E3__json_4() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3E3__json_4)); }
	inline JObject_t1798544199 * get_U3CU3E3__json_4() const { return ___U3CU3E3__json_4; }
	inline JObject_t1798544199 ** get_address_of_U3CU3E3__json_4() { return &___U3CU3E3__json_4; }
	inline void set_U3CU3E3__json_4(JObject_t1798544199 * value)
	{
		___U3CU3E3__json_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E3__json_4, value);
	}

	inline static int32_t get_offset_of_U3CsplitByUrlsU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CsplitByUrlsU3E5__2_5)); }
	inline StringU5BU5D_t4054002952* get_U3CsplitByUrlsU3E5__2_5() const { return ___U3CsplitByUrlsU3E5__2_5; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CsplitByUrlsU3E5__2_5() { return &___U3CsplitByUrlsU3E5__2_5; }
	inline void set_U3CsplitByUrlsU3E5__2_5(StringU5BU5D_t4054002952* value)
	{
		___U3CsplitByUrlsU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsplitByUrlsU3E5__2_5, value);
	}

	inline static int32_t get_offset_of_U3CsU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CsU3E5__3_6)); }
	inline String_t* get_U3CsU3E5__3_6() const { return ___U3CsU3E5__3_6; }
	inline String_t** get_address_of_U3CsU3E5__3_6() { return &___U3CsU3E5__3_6; }
	inline void set_U3CsU3E5__3_6(String_t* value)
	{
		___U3CsU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E5__3_6, value);
	}

	inline static int32_t get_offset_of_U3CqueriesU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CqueriesU3E5__4_7)); }
	inline Il2CppObject* get_U3CqueriesU3E5__4_7() const { return ___U3CqueriesU3E5__4_7; }
	inline Il2CppObject** get_address_of_U3CqueriesU3E5__4_7() { return &___U3CqueriesU3E5__4_7; }
	inline void set_U3CqueriesU3E5__4_7(Il2CppObject* value)
	{
		___U3CqueriesU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CqueriesU3E5__4_7, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CurlU3E5__5_8)); }
	inline String_t* get_U3CurlU3E5__5_8() const { return ___U3CurlU3E5__5_8; }
	inline String_t** get_address_of_U3CurlU3E5__5_8() { return &___U3CurlU3E5__5_8; }
	inline void set_U3CurlU3E5__5_8(String_t* value)
	{
		___U3CurlU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E5__5_8, value);
	}

	inline static int32_t get_offset_of_U3CrequiresDecryptionU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CrequiresDecryptionU3E5__6_9)); }
	inline bool get_U3CrequiresDecryptionU3E5__6_9() const { return ___U3CrequiresDecryptionU3E5__6_9; }
	inline bool* get_address_of_U3CrequiresDecryptionU3E5__6_9() { return &___U3CrequiresDecryptionU3E5__6_9; }
	inline void set_U3CrequiresDecryptionU3E5__6_9(bool value)
	{
		___U3CrequiresDecryptionU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CparametersU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CparametersU3E5__7_10)); }
	inline Il2CppObject* get_U3CparametersU3E5__7_10() const { return ___U3CparametersU3E5__7_10; }
	inline Il2CppObject** get_address_of_U3CparametersU3E5__7_10() { return &___U3CparametersU3E5__7_10; }
	inline void set_U3CparametersU3E5__7_10(Il2CppObject* value)
	{
		___U3CparametersU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparametersU3E5__7_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Eg__initLocal0_11() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3Eg__initLocal0_11)); }
	inline ExtractionInfo_t925438340 * get_U3CU3Eg__initLocal0_11() const { return ___U3CU3Eg__initLocal0_11; }
	inline ExtractionInfo_t925438340 ** get_address_of_U3CU3Eg__initLocal0_11() { return &___U3CU3Eg__initLocal0_11; }
	inline void set_U3CU3Eg__initLocal0_11(ExtractionInfo_t925438340 * value)
	{
		___U3CU3Eg__initLocal0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Eg__initLocal0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap9_12() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3E7__wrap9_12)); }
	inline StringU5BU5D_t4054002952* get_U3CU3E7__wrap9_12() const { return ___U3CU3E7__wrap9_12; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU3E7__wrap9_12() { return &___U3CU3E7__wrap9_12; }
	inline void set_U3CU3E7__wrap9_12(StringU5BU5D_t4054002952* value)
	{
		___U3CU3E7__wrap9_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E7__wrap9_12, value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrapa_13() { return static_cast<int32_t>(offsetof(U3CExtractDownloadUrlsU3Ed__1_t1414174443, ___U3CU3E7__wrapa_13)); }
	inline int32_t get_U3CU3E7__wrapa_13() const { return ___U3CU3E7__wrapa_13; }
	inline int32_t* get_address_of_U3CU3E7__wrapa_13() { return &___U3CU3E7__wrapa_13; }
	inline void set_U3CU3E7__wrapa_13(int32_t value)
	{
		___U3CU3E7__wrapa_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
