﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3
struct U3CLoadImageU3Ec__Iterator3_t2198666563;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::.ctor()
extern "C"  void U3CLoadImageU3Ec__Iterator3__ctor_m384475320 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2234821284 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1555592760 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::MoveNext()
extern "C"  bool U3CLoadImageU3Ec__Iterator3_MoveNext_m648549412 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::Dispose()
extern "C"  void U3CLoadImageU3Ec__Iterator3_Dispose_m11878133 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::Reset()
extern "C"  void U3CLoadImageU3Ec__Iterator3_Reset_m2325875557 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
