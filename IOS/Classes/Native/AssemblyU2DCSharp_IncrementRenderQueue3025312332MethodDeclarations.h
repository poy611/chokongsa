﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IncrementRenderQueue
struct IncrementRenderQueue_t3025312332;

#include "codegen/il2cpp-codegen.h"

// System.Void IncrementRenderQueue::.ctor()
extern "C"  void IncrementRenderQueue__ctor_m3538982351 (IncrementRenderQueue_t3025312332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementRenderQueue::Start()
extern "C"  void IncrementRenderQueue_Start_m2486120143 (IncrementRenderQueue_t3025312332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IncrementRenderQueue::Update()
extern "C"  void IncrementRenderQueue_Update_m4061132574 (IncrementRenderQueue_t3025312332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
