﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializationException
struct JsonSerializationException_t2886273343;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

// System.Void Newtonsoft.Json.JsonSerializationException::.ctor()
extern "C"  void JsonSerializationException__ctor_m320322250 (JsonSerializationException_t2886273343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String)
extern "C"  void JsonSerializationException__ctor_m506018936 (JsonSerializationException_t2886273343 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String,System.Exception)
extern "C"  void JsonSerializationException__ctor_m1091307454 (JsonSerializationException_t2886273343 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializationException__ctor_m2198567179 (JsonSerializationException_t2886273343 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String)
extern "C"  JsonSerializationException_t2886273343 * JsonSerializationException_Create_m3452296553 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern "C"  JsonSerializationException_t2886273343 * JsonSerializationException_Create_m2006533869 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, String_t* ___message1, Exception_t3991598821 * ___ex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern "C"  JsonSerializationException_t2886273343 * JsonSerializationException_Create_m217825527 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lineInfo0, String_t* ___path1, String_t* ___message2, Exception_t3991598821 * ___ex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
