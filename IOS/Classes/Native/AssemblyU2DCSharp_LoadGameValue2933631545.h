﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadGameValue
struct  LoadGameValue_t2933631545  : public MonoBehaviour_t667441552
{
public:
	// PlayMakerFSM LoadGameValue::_pmTrriger2
	PlayMakerFSM_t3799847376 * ____pmTrriger2_2;
	// PlayMakerFSM LoadGameValue::_pmTrriger3
	PlayMakerFSM_t3799847376 * ____pmTrriger3_3;
	// PlayMakerFSM LoadGameValue::_dataInfo
	PlayMakerFSM_t3799847376 * ____dataInfo_4;
	// UnityEngine.GameObject LoadGameValue::reticle_03
	GameObject_t3674682005 * ___reticle_03_5;
	// HutongGames.PlayMaker.FsmFloat LoadGameValue::_value2
	FsmFloat_t2134102846 * ____value2_6;
	// HutongGames.PlayMaker.FsmFloat LoadGameValue::_value3
	FsmFloat_t2134102846 * ____value3_7;
	// HutongGames.PlayMaker.FsmFloat LoadGameValue::_need
	FsmFloat_t2134102846 * ____need_8;
	// System.Single LoadGameValue::totalValue
	float ___totalValue_9;
	// System.Single LoadGameValue::totalNeed
	float ___totalNeed_10;

public:
	inline static int32_t get_offset_of__pmTrriger2_2() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____pmTrriger2_2)); }
	inline PlayMakerFSM_t3799847376 * get__pmTrriger2_2() const { return ____pmTrriger2_2; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__pmTrriger2_2() { return &____pmTrriger2_2; }
	inline void set__pmTrriger2_2(PlayMakerFSM_t3799847376 * value)
	{
		____pmTrriger2_2 = value;
		Il2CppCodeGenWriteBarrier(&____pmTrriger2_2, value);
	}

	inline static int32_t get_offset_of__pmTrriger3_3() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____pmTrriger3_3)); }
	inline PlayMakerFSM_t3799847376 * get__pmTrriger3_3() const { return ____pmTrriger3_3; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__pmTrriger3_3() { return &____pmTrriger3_3; }
	inline void set__pmTrriger3_3(PlayMakerFSM_t3799847376 * value)
	{
		____pmTrriger3_3 = value;
		Il2CppCodeGenWriteBarrier(&____pmTrriger3_3, value);
	}

	inline static int32_t get_offset_of__dataInfo_4() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____dataInfo_4)); }
	inline PlayMakerFSM_t3799847376 * get__dataInfo_4() const { return ____dataInfo_4; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__dataInfo_4() { return &____dataInfo_4; }
	inline void set__dataInfo_4(PlayMakerFSM_t3799847376 * value)
	{
		____dataInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&____dataInfo_4, value);
	}

	inline static int32_t get_offset_of_reticle_03_5() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ___reticle_03_5)); }
	inline GameObject_t3674682005 * get_reticle_03_5() const { return ___reticle_03_5; }
	inline GameObject_t3674682005 ** get_address_of_reticle_03_5() { return &___reticle_03_5; }
	inline void set_reticle_03_5(GameObject_t3674682005 * value)
	{
		___reticle_03_5 = value;
		Il2CppCodeGenWriteBarrier(&___reticle_03_5, value);
	}

	inline static int32_t get_offset_of__value2_6() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____value2_6)); }
	inline FsmFloat_t2134102846 * get__value2_6() const { return ____value2_6; }
	inline FsmFloat_t2134102846 ** get_address_of__value2_6() { return &____value2_6; }
	inline void set__value2_6(FsmFloat_t2134102846 * value)
	{
		____value2_6 = value;
		Il2CppCodeGenWriteBarrier(&____value2_6, value);
	}

	inline static int32_t get_offset_of__value3_7() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____value3_7)); }
	inline FsmFloat_t2134102846 * get__value3_7() const { return ____value3_7; }
	inline FsmFloat_t2134102846 ** get_address_of__value3_7() { return &____value3_7; }
	inline void set__value3_7(FsmFloat_t2134102846 * value)
	{
		____value3_7 = value;
		Il2CppCodeGenWriteBarrier(&____value3_7, value);
	}

	inline static int32_t get_offset_of__need_8() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ____need_8)); }
	inline FsmFloat_t2134102846 * get__need_8() const { return ____need_8; }
	inline FsmFloat_t2134102846 ** get_address_of__need_8() { return &____need_8; }
	inline void set__need_8(FsmFloat_t2134102846 * value)
	{
		____need_8 = value;
		Il2CppCodeGenWriteBarrier(&____need_8, value);
	}

	inline static int32_t get_offset_of_totalValue_9() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ___totalValue_9)); }
	inline float get_totalValue_9() const { return ___totalValue_9; }
	inline float* get_address_of_totalValue_9() { return &___totalValue_9; }
	inline void set_totalValue_9(float value)
	{
		___totalValue_9 = value;
	}

	inline static int32_t get_offset_of_totalNeed_10() { return static_cast<int32_t>(offsetof(LoadGameValue_t2933631545, ___totalNeed_10)); }
	inline float get_totalNeed_10() const { return ___totalNeed_10; }
	inline float* get_address_of_totalNeed_10() { return &___totalNeed_10; }
	inline void set_totalNeed_10(float value)
	{
		___totalNeed_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
