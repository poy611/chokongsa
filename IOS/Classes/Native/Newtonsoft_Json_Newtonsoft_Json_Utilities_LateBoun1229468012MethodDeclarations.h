﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>
struct U3CU3Ec__DisplayClass5_0_1_t1229468012;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0_1__ctor_m1419602846_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass5_0_1__ctor_m1419602846(__this, method) ((  void (*) (U3CU3Ec__DisplayClass5_0_1_t1229468012 *, const MethodInfo*))U3CU3Ec__DisplayClass5_0_1__ctor_m1419602846_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::<CreateDefaultConstructor>b__0()
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__0_m1122221088_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__0_m1122221088(__this, method) ((  Il2CppObject * (*) (U3CU3Ec__DisplayClass5_0_1_t1229468012 *, const MethodInfo*))U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__0_m1122221088_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::<CreateDefaultConstructor>b__1()
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__1_m1122222049_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__1_m1122222049(__this, method) ((  Il2CppObject * (*) (U3CU3Ec__DisplayClass5_0_1_t1229468012 *, const MethodInfo*))U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__1_m1122222049_gshared)(__this, method)
