﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetButton
struct  GetButton_t428887414  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetButton::buttonName
	FsmString_t952858651 * ___buttonName_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetButton::storeResult
	FsmBool_t1075959796 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.GetButton::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_buttonName_11() { return static_cast<int32_t>(offsetof(GetButton_t428887414, ___buttonName_11)); }
	inline FsmString_t952858651 * get_buttonName_11() const { return ___buttonName_11; }
	inline FsmString_t952858651 ** get_address_of_buttonName_11() { return &___buttonName_11; }
	inline void set_buttonName_11(FsmString_t952858651 * value)
	{
		___buttonName_11 = value;
		Il2CppCodeGenWriteBarrier(&___buttonName_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(GetButton_t428887414, ___storeResult_12)); }
	inline FsmBool_t1075959796 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmBool_t1075959796 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(GetButton_t428887414, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
