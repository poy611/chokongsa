﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.EnumUtils/<>c
struct U3CU3Ec_t2239738249;
// System.String
struct String_t;
// System.Runtime.Serialization.EnumMemberAttribute
struct EnumMemberAttribute_t1202205191;
// System.Reflection.FieldInfo
struct FieldInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Runtime_Serialization_System_Runtime_Serial1202205191.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"

// System.Void Newtonsoft.Json.Utilities.EnumUtils/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m750775286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.EnumUtils/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2949264023 (U3CU3Ec_t2239738249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.EnumUtils/<>c::<InitializeEnumType>b__1_0(System.Runtime.Serialization.EnumMemberAttribute)
extern "C"  String_t* U3CU3Ec_U3CInitializeEnumTypeU3Eb__1_0_m1000112691 (U3CU3Ec_t2239738249 * __this, EnumMemberAttribute_t1202205191 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.EnumUtils/<>c::<GetValues>b__5_0(System.Reflection.FieldInfo)
extern "C"  bool U3CU3Ec_U3CGetValuesU3Eb__5_0_m3929079017 (U3CU3Ec_t2239738249 * __this, FieldInfo_t * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
