﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.PlayerStats
struct PlayerStats_t60064856;
// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F
struct U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607;
// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50
struct U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51
struct  U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.PlayerStats GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51::stats
	PlayerStats_t60064856 * ___stats_0;
	// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51::<>f__ref$79
	U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * ___U3CU3Ef__refU2479_1;
	// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50 GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51::<>f__ref$80
	U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 * ___U3CU3Ef__refU2480_2;

public:
	inline static int32_t get_offset_of_stats_0() { return static_cast<int32_t>(offsetof(U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025, ___stats_0)); }
	inline PlayerStats_t60064856 * get_stats_0() const { return ___stats_0; }
	inline PlayerStats_t60064856 ** get_address_of_stats_0() { return &___stats_0; }
	inline void set_stats_0(PlayerStats_t60064856 * value)
	{
		___stats_0 = value;
		Il2CppCodeGenWriteBarrier(&___stats_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2479_1() { return static_cast<int32_t>(offsetof(U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025, ___U3CU3Ef__refU2479_1)); }
	inline U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * get_U3CU3Ef__refU2479_1() const { return ___U3CU3Ef__refU2479_1; }
	inline U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 ** get_address_of_U3CU3Ef__refU2479_1() { return &___U3CU3Ef__refU2479_1; }
	inline void set_U3CU3Ef__refU2479_1(U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * value)
	{
		___U3CU3Ef__refU2479_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2479_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2480_2() { return static_cast<int32_t>(offsetof(U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025, ___U3CU3Ef__refU2480_2)); }
	inline U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 * get_U3CU3Ef__refU2480_2() const { return ___U3CU3Ef__refU2480_2; }
	inline U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 ** get_address_of_U3CU3Ef__refU2480_2() { return &___U3CU3Ef__refU2480_2; }
	inline void set_U3CU3Ef__refU2480_2(U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 * value)
	{
		___U3CU3Ef__refU2480_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2480_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
