﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralVector2
struct SetProceduralVector2_t1950976504;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::.ctor()
extern "C"  void SetProceduralVector2__ctor_m1589592942 (SetProceduralVector2_t1950976504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::Reset()
extern "C"  void SetProceduralVector2_Reset_m3530993179 (SetProceduralVector2_t1950976504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::OnEnter()
extern "C"  void SetProceduralVector2_OnEnter_m2277155781 (SetProceduralVector2_t1950976504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::OnUpdate()
extern "C"  void SetProceduralVector2_OnUpdate_m1005911838 (SetProceduralVector2_t1950976504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::DoSetProceduralVector()
extern "C"  void SetProceduralVector2_DoSetProceduralVector_m2892577251 (SetProceduralVector2_t1950976504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
