﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutToolbar
struct GUILayoutToolbar_t3141290782;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::.ctor()
extern "C"  void GUILayoutToolbar__ctor_m1694251272 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::get_Contents()
extern "C"  GUIContentU5BU5D_t3588725815* GUILayoutToolbar_get_Contents_m1153165093 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::SetButtonsContent()
extern "C"  void GUILayoutToolbar_SetButtonsContent_m1877690080 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::Reset()
extern "C"  void GUILayoutToolbar_Reset_m3635651509 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::OnEnter()
extern "C"  void GUILayoutToolbar_OnEnter_m4069563103 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToolbar::OnGUI()
extern "C"  void GUILayoutToolbar_OnGUI_m1189649922 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.GUILayoutToolbar::ErrorCheck()
extern "C"  String_t* GUILayoutToolbar_ErrorCheck_m1656665727 (GUILayoutToolbar_t3141290782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
