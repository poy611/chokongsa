﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>
struct ShimEnumerator_t117377792;
// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3595697099_gshared (ShimEnumerator_t117377792 * __this, Dictionary_2_t401599765 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3595697099(__this, ___host0, method) ((  void (*) (ShimEnumerator_t117377792 *, Dictionary_2_t401599765 *, const MethodInfo*))ShimEnumerator__ctor_m3595697099_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m4009168058_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m4009168058(__this, method) ((  bool (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_MoveNext_m4009168058_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3051821360_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3051821360(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_get_Entry_m3051821360_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2592008303_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2592008303(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_get_Key_m2592008303_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m887383873_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m887383873(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_get_Value_m887383873_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3797068169_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3797068169(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_get_Current_m3797068169_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Runtime.InteropServices.HandleRef,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m636229021_gshared (ShimEnumerator_t117377792 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m636229021(__this, method) ((  void (*) (ShimEnumerator_t117377792 *, const MethodInfo*))ShimEnumerator_Reset_m636229021_gshared)(__this, method)
