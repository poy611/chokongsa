﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive
struct  SetAnimatorFeetPivotActive_t1077889561  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::feetPivotActive
	FsmFloat_t2134102846 * ___feetPivotActive_12;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::_animator
	Animator_t2776330603 * ____animator_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t1077889561, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_feetPivotActive_12() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t1077889561, ___feetPivotActive_12)); }
	inline FsmFloat_t2134102846 * get_feetPivotActive_12() const { return ___feetPivotActive_12; }
	inline FsmFloat_t2134102846 ** get_address_of_feetPivotActive_12() { return &___feetPivotActive_12; }
	inline void set_feetPivotActive_12(FsmFloat_t2134102846 * value)
	{
		___feetPivotActive_12 = value;
		Il2CppCodeGenWriteBarrier(&___feetPivotActive_12, value);
	}

	inline static int32_t get_offset_of__animator_13() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t1077889561, ____animator_13)); }
	inline Animator_t2776330603 * get__animator_13() const { return ____animator_13; }
	inline Animator_t2776330603 ** get_address_of__animator_13() { return &____animator_13; }
	inline void set__animator_13(Animator_t2776330603 * value)
	{
		____animator_13 = value;
		Il2CppCodeGenWriteBarrier(&____animator_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
