﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1211634336.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m708140506_gshared (InternalEnumerator_1_t1211634336 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m708140506(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1211634336 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m708140506_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854(__this, method) ((  void (*) (InternalEnumerator_1_t1211634336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1211634336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2767949873_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2767949873(__this, method) ((  void (*) (InternalEnumerator_1_t1211634336 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2767949873_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1301430326_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1301430326(__this, method) ((  bool (*) (InternalEnumerator_1_t1211634336 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1301430326_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3936493699_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3936493699(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1211634336 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3936493699_gshared)(__this, method)
