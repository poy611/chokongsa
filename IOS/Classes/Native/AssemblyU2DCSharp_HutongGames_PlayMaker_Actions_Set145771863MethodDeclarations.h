﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmString
struct SetFsmString_t145771863;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmString::.ctor()
extern "C"  void SetFsmString__ctor_m4241126127 (SetFsmString_t145771863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::Reset()
extern "C"  void SetFsmString_Reset_m1887559068 (SetFsmString_t145771863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnEnter()
extern "C"  void SetFsmString_OnEnter_m3484940038 (SetFsmString_t145771863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::DoSetFsmString()
extern "C"  void SetFsmString_DoSetFsmString_m1693021263 (SetFsmString_t145771863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnUpdate()
extern "C"  void SetFsmString_OnUpdate_m4087485437 (SetFsmString_t145771863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
