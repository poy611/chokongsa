﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3551110977.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m343115129_gshared (InternalEnumerator_1_t3551110977 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m343115129(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3551110977 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m343115129_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079(__this, method) ((  void (*) (InternalEnumerator_1_t3551110977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3551110977 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4204389008_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4204389008(__this, method) ((  void (*) (InternalEnumerator_1_t3551110977 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4204389008_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1197756471_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1197756471(__this, method) ((  bool (*) (InternalEnumerator_1_t3551110977 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1197756471_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::get_Current()
extern "C"  ResolverContractKey_t473801005  InternalEnumerator_1_get_Current_m3388255906_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3388255906(__this, method) ((  ResolverContractKey_t473801005  (*) (InternalEnumerator_1_t3551110977 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3388255906_gshared)(__this, method)
