﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
struct BeforeRoomCreateStartedState_t89186511;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t1352686482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRea1352686482.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C"  void BeforeRoomCreateStartedState__ctor_m4282376554 (BeforeRoomCreateStartedState_t89186511 * __this, RoomSession_t1352686482 * ___session0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::LeaveRoom()
extern "C"  void BeforeRoomCreateStartedState_LeaveRoom_m4253231052 (BeforeRoomCreateStartedState_t89186511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
