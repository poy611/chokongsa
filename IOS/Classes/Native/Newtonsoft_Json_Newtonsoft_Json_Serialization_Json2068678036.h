﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t18794611;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t25693564;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t1492900827;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t2068678036  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t18794611 * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_t25693564 * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t251850770 * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	Il2CppObject * ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t3893567258 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2068678036, ____currentErrorContext_0)); }
	inline ErrorContext_t18794611 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t18794611 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t18794611 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier(&____currentErrorContext_0, value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2068678036, ____mappings_1)); }
	inline BidirectionalDictionary_2_t25693564 * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_t25693564 ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_t25693564 * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier(&____mappings_1, value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2068678036, ___Serializer_2)); }
	inline JsonSerializer_t251850770 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t251850770 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t251850770 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier(&___Serializer_2, value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2068678036, ___TraceWriter_3)); }
	inline Il2CppObject * get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline Il2CppObject ** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(Il2CppObject * value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier(&___TraceWriter_3, value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t2068678036, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t3893567258 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t3893567258 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t3893567258 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier(&___InternalSerializer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
