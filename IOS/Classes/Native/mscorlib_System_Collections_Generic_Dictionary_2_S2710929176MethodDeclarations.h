﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct ShimEnumerator_t2710929176;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct Dictionary_2_t2995151149;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1479146512_gshared (ShimEnumerator_t2710929176 * __this, Dictionary_2_t2995151149 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1479146512(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2710929176 *, Dictionary_2_t2995151149 *, const MethodInfo*))ShimEnumerator__ctor_m1479146512_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m781341457_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m781341457(__this, method) ((  bool (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_MoveNext_m781341457_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m495070307_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m495070307(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_get_Entry_m495070307_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2602884606_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2602884606(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_get_Key_m2602884606_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2749576464_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2749576464(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_get_Value_m2749576464_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2362785688_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2362785688(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_get_Current_m2362785688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1476230754_gshared (ShimEnumerator_t2710929176 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1476230754(__this, method) ((  void (*) (ShimEnumerator_t2710929176 *, const MethodInfo*))ShimEnumerator_Reset_m1476230754_gshared)(__this, method)
