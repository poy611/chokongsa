﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Icing/<TimeEnd>c__Iterator1D
struct U3CTimeEndU3Ec__Iterator1D_t664746339;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator1D__ctor_m2437225320 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<TimeEnd>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1383866154 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<TimeEnd>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m3549804222 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Icing/<TimeEnd>c__Iterator1D::MoveNext()
extern "C"  bool U3CTimeEndU3Ec__Iterator1D_MoveNext_m301783692 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator1D_Dispose_m1314639269 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::Reset()
extern "C"  void U3CTimeEndU3Ec__Iterator1D_Reset_m83658261 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
