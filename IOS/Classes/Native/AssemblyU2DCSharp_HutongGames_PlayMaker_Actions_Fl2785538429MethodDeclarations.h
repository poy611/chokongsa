﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAddMultiple
struct FloatAddMultiple_t2785538429;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::.ctor()
extern "C"  void FloatAddMultiple__ctor_m3039579529 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::Reset()
extern "C"  void FloatAddMultiple_Reset_m686012470 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnEnter()
extern "C"  void FloatAddMultiple_OnEnter_m4144861984 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnUpdate()
extern "C"  void FloatAddMultiple_OnUpdate_m3070229283 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::DoFloatAdd()
extern "C"  void FloatAddMultiple_DoFloatAdd_m178176363 (FloatAddMultiple_t2785538429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
