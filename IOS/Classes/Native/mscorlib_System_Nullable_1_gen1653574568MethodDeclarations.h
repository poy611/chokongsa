﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3129462792_gshared (Nullable_1_t1653574568 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3129462792(__this, ___value0, method) ((  void (*) (Nullable_1_t1653574568 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3129462792_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m700003747_gshared (Nullable_1_t1653574568 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m700003747(__this, method) ((  bool (*) (Nullable_1_t1653574568 *, const MethodInfo*))Nullable_1_get_HasValue_m700003747_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1038537328_gshared (Nullable_1_t1653574568 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1038537328(__this, method) ((  int32_t (*) (Nullable_1_t1653574568 *, const MethodInfo*))Nullable_1_get_Value_m1038537328_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2901324542_gshared (Nullable_1_t1653574568 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2901324542(__this, ___other0, method) ((  bool (*) (Nullable_1_t1653574568 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2901324542_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2171676289_gshared (Nullable_1_t1653574568 * __this, Nullable_1_t1653574568  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2171676289(__this, ___other0, method) ((  bool (*) (Nullable_1_t1653574568 *, Nullable_1_t1653574568 , const MethodInfo*))Nullable_1_Equals_m2171676289_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3721161814_gshared (Nullable_1_t1653574568 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3721161814(__this, method) ((  int32_t (*) (Nullable_1_t1653574568 *, const MethodInfo*))Nullable_1_GetHashCode_m3721161814_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m888513788_gshared (Nullable_1_t1653574568 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m888513788(__this, method) ((  int32_t (*) (Nullable_1_t1653574568 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m888513788_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3458219501_gshared (Nullable_1_t1653574568 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3458219501(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t1653574568 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m3458219501_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m72457154_gshared (Nullable_1_t1653574568 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m72457154(__this, method) ((  String_t* (*) (Nullable_1_t1653574568 *, const MethodInfo*))Nullable_1_ToString_m72457154_gshared)(__this, method)
