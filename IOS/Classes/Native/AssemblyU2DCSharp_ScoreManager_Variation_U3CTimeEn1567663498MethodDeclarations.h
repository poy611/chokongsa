﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Variation/<TimeEnd>c__Iterator27
struct U3CTimeEndU3Ec__Iterator27_t1567663498;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator27__ctor_m3270293281 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<TimeEnd>c__Iterator27::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4252178321 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<TimeEnd>c__Iterator27::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m2017484069 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Variation/<TimeEnd>c__Iterator27::MoveNext()
extern "C"  bool U3CTimeEndU3Ec__Iterator27_MoveNext_m4184191539 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator27_Dispose_m3029032734 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::Reset()
extern "C"  void U3CTimeEndU3Ec__Iterator27_Reset_m916726222 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
