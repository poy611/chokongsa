﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ProjectLocationToMap
struct ProjectLocationToMap_t969841851;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::.ctor()
extern "C"  void ProjectLocationToMap__ctor_m1019190283 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::Reset()
extern "C"  void ProjectLocationToMap_Reset_m2960590520 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::OnEnter()
extern "C"  void ProjectLocationToMap_OnEnter_m3876014370 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::OnUpdate()
extern "C"  void ProjectLocationToMap_OnUpdate_m3325887841 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoProjectGPSLocation()
extern "C"  void ProjectLocationToMap_DoProjectGPSLocation_m2048383274 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoEquidistantCylindrical()
extern "C"  void ProjectLocationToMap_DoEquidistantCylindrical_m3761440177 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoMercatorProjection()
extern "C"  void ProjectLocationToMap_DoMercatorProjection_m1149793234 (ProjectLocationToMap_t969841851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::LatitudeToMercator(System.Single)
extern "C"  float ProjectLocationToMap_LatitudeToMercator_m1227397792 (Il2CppObject * __this /* static, unused */, float ___latitudeInDegrees0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
