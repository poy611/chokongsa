﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIntFromFloat
struct SetIntFromFloat_t4253369395;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::.ctor()
extern "C"  void SetIntFromFloat__ctor_m2506030179 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::Reset()
extern "C"  void SetIntFromFloat_Reset_m152463120 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::OnEnter()
extern "C"  void SetIntFromFloat_OnEnter_m2505044858 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntFromFloat::OnUpdate()
extern "C"  void SetIntFromFloat_OnUpdate_m3775505929 (SetIntFromFloat_t4253369395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
