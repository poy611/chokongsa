﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Variation/<CheckIngredient>c__Iterator25
struct U3CCheckIngredientU3Ec__Iterator25_t55506035;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::.ctor()
extern "C"  void U3CCheckIngredientU3Ec__Iterator25__ctor_m1382725208 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<CheckIngredient>c__Iterator25::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckIngredientU3Ec__Iterator25_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1049711034 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<CheckIngredient>c__Iterator25::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckIngredientU3Ec__Iterator25_System_Collections_IEnumerator_get_Current_m1325533518 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Variation/<CheckIngredient>c__Iterator25::MoveNext()
extern "C"  bool U3CCheckIngredientU3Ec__Iterator25_MoveNext_m3381774236 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::Dispose()
extern "C"  void U3CCheckIngredientU3Ec__Iterator25_Dispose_m1552313493 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::Reset()
extern "C"  void U3CCheckIngredientU3Ec__Iterator25_Reset_m3324125445 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
