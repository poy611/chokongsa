﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey56
struct U3CRevealAchievementU3Ec__AnonStorey56_t2760527584;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177.h"

// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey56::.ctor()
extern "C"  void U3CRevealAchievementU3Ec__AnonStorey56__ctor_m952832443 (U3CRevealAchievementU3Ec__AnonStorey56_t2760527584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey56::<>m__2C(GooglePlayGames.BasicApi.Achievement)
extern "C"  void U3CRevealAchievementU3Ec__AnonStorey56_U3CU3Em__2C_m1015385934 (U3CRevealAchievementU3Ec__AnonStorey56_t2760527584 * __this, Achievement_t1261647177 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
