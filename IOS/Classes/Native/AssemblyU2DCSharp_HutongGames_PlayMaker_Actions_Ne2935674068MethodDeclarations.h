﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkDestroy
struct NetworkDestroy_t2935674068;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::.ctor()
extern "C"  void NetworkDestroy__ctor_m2690146834 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::Reset()
extern "C"  void NetworkDestroy_Reset_m336579775 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::OnEnter()
extern "C"  void NetworkDestroy_OnEnter_m3347491177 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::DoDestroy()
extern "C"  void NetworkDestroy_DoDestroy_m949788895 (NetworkDestroy_t2935674068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
