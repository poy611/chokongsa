﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecipeUpDown/<GoUP>c__Iterator17
struct U3CGoUPU3Ec__Iterator17_t3652911668;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RecipeUpDown/<GoUP>c__Iterator17::.ctor()
extern "C"  void U3CGoUPU3Ec__Iterator17__ctor_m1438133991 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecipeUpDown/<GoUP>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoUPU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3081891157 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecipeUpDown/<GoUP>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoUPU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m533412585 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RecipeUpDown/<GoUP>c__Iterator17::MoveNext()
extern "C"  bool U3CGoUPU3Ec__Iterator17_MoveNext_m93878677 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown/<GoUP>c__Iterator17::Dispose()
extern "C"  void U3CGoUPU3Ec__Iterator17_Dispose_m3260546404 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown/<GoUP>c__Iterator17::Reset()
extern "C"  void U3CGoUPU3Ec__Iterator17_Reset_m3379534228 (U3CGoUPU3Ec__Iterator17_t3652911668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
