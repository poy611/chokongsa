﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>
struct ValueCollection_t2245389293;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit2D>
struct IEnumerator_1_t3286609433;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1476616988.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m440413687_gshared (ValueCollection_t2245389293 * __this, Dictionary_2_t3544783580 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m440413687(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2245389293 *, Dictionary_2_t3544783580 *, const MethodInfo*))ValueCollection__ctor_m440413687_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m160756571_gshared (ValueCollection_t2245389293 * __this, RaycastHit2D_t1374744384  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m160756571(__this, ___item0, method) ((  void (*) (ValueCollection_t2245389293 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m160756571_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1596507556_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1596507556(__this, method) ((  void (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1596507556_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2005047919_gshared (ValueCollection_t2245389293 * __this, RaycastHit2D_t1374744384  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2005047919(__this, ___item0, method) ((  bool (*) (ValueCollection_t2245389293 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2005047919_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3989668564_gshared (ValueCollection_t2245389293 * __this, RaycastHit2D_t1374744384  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3989668564(__this, ___item0, method) ((  bool (*) (ValueCollection_t2245389293 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3989668564_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3156224740_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3156224740(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3156224740_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m963619240_gshared (ValueCollection_t2245389293 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m963619240(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2245389293 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m963619240_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3107242167_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3107242167(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3107242167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m280223778_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m280223778(__this, method) ((  bool (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m280223778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3439315202_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3439315202(__this, method) ((  bool (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3439315202_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1840810100_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1840810100(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1840810100_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m183626878_gshared (ValueCollection_t2245389293 * __this, RaycastHit2DU5BU5D_t889400257* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m183626878(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2245389293 *, RaycastHit2DU5BU5D_t889400257*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m183626878_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t1476616988  ValueCollection_GetEnumerator_m1027386599_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1027386599(__this, method) ((  Enumerator_t1476616988  (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_GetEnumerator_m1027386599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4040439612_gshared (ValueCollection_t2245389293 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4040439612(__this, method) ((  int32_t (*) (ValueCollection_t2245389293 *, const MethodInfo*))ValueCollection_get_Count_m4040439612_gshared)(__this, method)
