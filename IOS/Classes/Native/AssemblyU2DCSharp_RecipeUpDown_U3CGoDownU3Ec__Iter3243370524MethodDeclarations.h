﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecipeUpDown/<GoDown>c__Iterator18
struct U3CGoDownU3Ec__Iterator18_t3243370524;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RecipeUpDown/<GoDown>c__Iterator18::.ctor()
extern "C"  void U3CGoDownU3Ec__Iterator18__ctor_m2726184959 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecipeUpDown/<GoDown>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoDownU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3027413501 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecipeUpDown/<GoDown>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoDownU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3567929233 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RecipeUpDown/<GoDown>c__Iterator18::MoveNext()
extern "C"  bool U3CGoDownU3Ec__Iterator18_MoveNext_m2043759741 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown/<GoDown>c__Iterator18::Dispose()
extern "C"  void U3CGoDownU3Ec__Iterator18_Dispose_m4126945404 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown/<GoDown>c__Iterator18::Reset()
extern "C"  void U3CGoDownU3Ec__Iterator18_Reset_m372617900 (U3CGoDownU3Ec__Iterator18_t3243370524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
