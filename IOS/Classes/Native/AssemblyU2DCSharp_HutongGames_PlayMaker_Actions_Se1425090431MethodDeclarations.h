﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTextureScale
struct SetTextureScale_t1425090431;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::.ctor()
extern "C"  void SetTextureScale__ctor_m191893143 (SetTextureScale_t1425090431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::Reset()
extern "C"  void SetTextureScale_Reset_m2133293380 (SetTextureScale_t1425090431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnEnter()
extern "C"  void SetTextureScale_OnEnter_m3412412590 (SetTextureScale_t1425090431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnUpdate()
extern "C"  void SetTextureScale_OnUpdate_m1839134549 (SetTextureScale_t1425090431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::DoSetTextureScale()
extern "C"  void SetTextureScale_DoSetTextureScale_m3882316443 (SetTextureScale_t1425090431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
