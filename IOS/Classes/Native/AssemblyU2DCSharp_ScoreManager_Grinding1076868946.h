﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// PopupManagement
struct PopupManagement_t282433007;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Grinding
struct  ScoreManager_Grinding_t1076868946  : public MonoBehaviour_t667441552
{
public:
	// StructforMinigame ScoreManager_Grinding::stuff
	StructforMinigame_t1286533533 * ___stuff_2;
	// ConstforMinigame ScoreManager_Grinding::cuff
	ConstforMinigame_t2789090703 * ___cuff_3;
	// PopupManagement ScoreManager_Grinding::popup
	PopupManagement_t282433007 * ___popup_4;
	// System.Int32 ScoreManager_Grinding::nowNum
	int32_t ___nowNum_5;

public:
	inline static int32_t get_offset_of_stuff_2() { return static_cast<int32_t>(offsetof(ScoreManager_Grinding_t1076868946, ___stuff_2)); }
	inline StructforMinigame_t1286533533 * get_stuff_2() const { return ___stuff_2; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_2() { return &___stuff_2; }
	inline void set_stuff_2(StructforMinigame_t1286533533 * value)
	{
		___stuff_2 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_2, value);
	}

	inline static int32_t get_offset_of_cuff_3() { return static_cast<int32_t>(offsetof(ScoreManager_Grinding_t1076868946, ___cuff_3)); }
	inline ConstforMinigame_t2789090703 * get_cuff_3() const { return ___cuff_3; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_3() { return &___cuff_3; }
	inline void set_cuff_3(ConstforMinigame_t2789090703 * value)
	{
		___cuff_3 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_3, value);
	}

	inline static int32_t get_offset_of_popup_4() { return static_cast<int32_t>(offsetof(ScoreManager_Grinding_t1076868946, ___popup_4)); }
	inline PopupManagement_t282433007 * get_popup_4() const { return ___popup_4; }
	inline PopupManagement_t282433007 ** get_address_of_popup_4() { return &___popup_4; }
	inline void set_popup_4(PopupManagement_t282433007 * value)
	{
		___popup_4 = value;
		Il2CppCodeGenWriteBarrier(&___popup_4, value);
	}

	inline static int32_t get_offset_of_nowNum_5() { return static_cast<int32_t>(offsetof(ScoreManager_Grinding_t1076868946, ___nowNum_5)); }
	inline int32_t get_nowNum_5() const { return ___nowNum_5; }
	inline int32_t* get_address_of_nowNum_5() { return &___nowNum_5; }
	inline void set_nowNum_5(int32_t value)
	{
		___nowNum_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
