﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButtonDown
struct GetButtonDown_t2951628984;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::.ctor()
extern "C"  void GetButtonDown__ctor_m3935000446 (GetButtonDown_t2951628984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::Reset()
extern "C"  void GetButtonDown_Reset_m1581433387 (GetButtonDown_t2951628984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::OnUpdate()
extern "C"  void GetButtonDown_OnUpdate_m2512892174 (GetButtonDown_t2951628984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
