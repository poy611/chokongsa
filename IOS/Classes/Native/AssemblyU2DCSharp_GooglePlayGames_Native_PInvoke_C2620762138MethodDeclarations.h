﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m4046309363_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m4046309363(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m4046309363_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>::<>m__9D(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 * __this, bool ___result0, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479(__this, ___result0, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 *, bool, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479_gshared)(__this, ___result0, method)
