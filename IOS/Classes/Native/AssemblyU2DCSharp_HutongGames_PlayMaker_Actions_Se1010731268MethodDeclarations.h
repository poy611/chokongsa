﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties
struct SetWheelJoint2dProperties_t1010731268;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::.ctor()
extern "C"  void SetWheelJoint2dProperties__ctor_m2560856242 (SetWheelJoint2dProperties_t1010731268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::Reset()
extern "C"  void SetWheelJoint2dProperties_Reset_m207289183 (SetWheelJoint2dProperties_t1010731268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::OnEnter()
extern "C"  void SetWheelJoint2dProperties_OnEnter_m3653283849 (SetWheelJoint2dProperties_t1010731268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::OnUpdate()
extern "C"  void SetWheelJoint2dProperties_OnUpdate_m716208986 (SetWheelJoint2dProperties_t1010731268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::SetProperties()
extern "C"  void SetWheelJoint2dProperties_SetProperties_m3812053189 (SetWheelJoint2dProperties_t1010731268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
