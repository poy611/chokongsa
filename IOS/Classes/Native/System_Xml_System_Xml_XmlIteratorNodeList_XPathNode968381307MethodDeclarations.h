﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlIteratorNodeList/XPathNodeIteratorNodeListIterator
struct XPathNodeIteratorNodeListIterator_t968381307;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t1383168931;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931.h"

// System.Void System.Xml.XmlIteratorNodeList/XPathNodeIteratorNodeListIterator::.ctor(System.Xml.XPath.XPathNodeIterator)
extern "C"  void XPathNodeIteratorNodeListIterator__ctor_m2937753508 (XPathNodeIteratorNodeListIterator_t968381307 * __this, XPathNodeIterator_t1383168931 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlIteratorNodeList/XPathNodeIteratorNodeListIterator::MoveNext()
extern "C"  bool XPathNodeIteratorNodeListIterator_MoveNext_m3068324134 (XPathNodeIteratorNodeListIterator_t968381307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XmlIteratorNodeList/XPathNodeIteratorNodeListIterator::get_Current()
extern "C"  Il2CppObject * XPathNodeIteratorNodeListIterator_get_Current_m3890733923 (XPathNodeIteratorNodeListIterator_t968381307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlIteratorNodeList/XPathNodeIteratorNodeListIterator::Reset()
extern "C"  void XPathNodeIteratorNodeListIterator_Reset_m2829420663 (XPathNodeIteratorNodeListIterator_t968381307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
