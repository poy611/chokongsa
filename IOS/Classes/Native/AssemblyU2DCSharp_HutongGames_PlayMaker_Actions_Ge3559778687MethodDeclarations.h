﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmTexture
struct GetFsmTexture_t3559778687;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::.ctor()
extern "C"  void GetFsmTexture__ctor_m3908478615 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::Reset()
extern "C"  void GetFsmTexture_Reset_m1554911556 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnEnter()
extern "C"  void GetFsmTexture_OnEnter_m1638260910 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnUpdate()
extern "C"  void GetFsmTexture_OnUpdate_m2675007317 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::DoGetFsmVariable()
extern "C"  void GetFsmTexture_DoGetFsmVariable_m1943280030 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
