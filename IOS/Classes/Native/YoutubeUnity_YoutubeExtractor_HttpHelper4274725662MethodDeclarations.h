﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String YoutubeExtractor.HttpHelper::DownloadString(System.String)
extern "C"  String_t* HttpHelper_DownloadString_m189277324 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.String> YoutubeExtractor.HttpHelper::ParseQueryString(System.String)
extern "C"  Il2CppObject* HttpHelper_ParseQueryString_m2936859467 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.HttpHelper::UrlDecode(System.String)
extern "C"  String_t* HttpHelper_UrlDecode_m2955848320 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
