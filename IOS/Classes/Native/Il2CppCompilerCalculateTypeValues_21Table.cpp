﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri1378537957.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_Physics3358180733.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Joint4201008640.h"
#include "UnityEngine_UnityEngine_SpringJoint558455091.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_Physics2D9846735.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_JointAngleLimits2D2258250679.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"
#include "UnityEngine_UnityEngine_JointSuspension2D939016335.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"
#include "UnityEngine_UnityEngine_AnchoredJoint2D1716788702.h"
#include "UnityEngine_UnityEngine_HingeJoint2D2650814389.h"
#include "UnityEngine_UnityEngine_WheelJoint2D2492372869.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_AudioSettings3774206607.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu1377657005.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback83861602.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa4244274966.h"
#include "UnityEngine_UnityEngine_AudioListener3685735200.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_AnimationEventSource2152433973.h"
#include "UnityEngine_UnityEngine_AnimationEvent3669457594.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator1374492422.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo2746035113.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_HumanLimit3300934482.h"
#include "UnityEngine_UnityEngine_HumanBone194476679.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3906681469.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1390812184.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Custo503624423.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1923005356.h"
#include "UnityEngine_UnityEngine_TextGenerator538854556.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2918974229.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode1147493927.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "UnityEngine_UnityEngine_Font4241557075.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal4168056797.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases4247149838.h"
#include "UnityEngine_UnityEngine_CanvasGroup3702418109.h"
#include "UnityEngine_UnityEngine_CanvasRenderer3950887807.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_EventModifiers4195406918.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_GUI3134605553.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState2687015188.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction2749288659.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUILayout3864601915.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility87000299.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (ScriptPlayable_t1378537957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (LoadSceneMode_t3067001883)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[3] = 
{
	LoadSceneMode_t3067001883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (SceneManager_t2940962239), -1, sizeof(SceneManager_t2940962239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2102[3] = 
{
	SceneManager_t2940962239_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t2940962239_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t2940962239_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (Scene_t1080795294)+ sizeof (Il2CppObject), sizeof(Scene_t1080795294_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	Scene_t1080795294::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (ParticleSystem_t381473177), -1, sizeof(ParticleSystem_t381473177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2104[5] = 
{
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
	ParticleSystem_t381473177_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (IteratorDelegate_t4269758102), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (ForceMode_t2134283300)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2106[5] = 
{
	ForceMode_t2134283300::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (ControllerColliderHit_t2416790841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[7] = 
{
	ControllerColliderHit_t2416790841::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Point_2(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t2416790841::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t2416790841::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (Collision_t2494107688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	Collision_t2494107688::get_offset_of_m_Impulse_0(),
	Collision_t2494107688::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2494107688::get_offset_of_m_Rigidbody_2(),
	Collision_t2494107688::get_offset_of_m_Collider_3(),
	Collision_t2494107688::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (CollisionFlags_t490137529)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[8] = 
{
	CollisionFlags_t490137529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (QueryTriggerInteraction_t577895768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[4] = 
{
	QueryTriggerInteraction_t577895768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Physics_t3358180733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (ContactPoint_t243083348)+ sizeof (Il2CppObject), sizeof(ContactPoint_t243083348_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	ContactPoint_t243083348::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t243083348::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Rigidbody_t3346577219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (Joint_t4201008640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (SpringJoint_t558455091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (Collider_t2939674232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (RaycastHit_t4003175726)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[6] = 
{
	RaycastHit_t4003175726::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t4003175726::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (PhysicMaterial_t211873335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (CharacterController_t1618060635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (Physics2D_t9846735), -1, sizeof(Physics2D_t9846735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	Physics2D_t9846735_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (RaycastHit2D_t1374744384)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[6] = 
{
	RaycastHit2D_t1374744384::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t1374744384::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (ForceMode2D_t665452726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[3] = 
{
	ForceMode2D_t665452726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (RigidbodyConstraints2D_t774977535)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2123[7] = 
{
	RigidbodyConstraints2D_t774977535::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (Rigidbody2D_t1743771669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (Collider2D_t1552025098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (ContactPoint2D_t4288432358)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[4] = 
{
	ContactPoint2D_t4288432358::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_Collider_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t4288432358::get_offset_of_m_OtherCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (Collision2D_t2859305914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[5] = 
{
	Collision2D_t2859305914::get_offset_of_m_Rigidbody_0(),
	Collision2D_t2859305914::get_offset_of_m_Collider_1(),
	Collision2D_t2859305914::get_offset_of_m_Contacts_2(),
	Collision2D_t2859305914::get_offset_of_m_RelativeVelocity_3(),
	Collision2D_t2859305914::get_offset_of_m_Enabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (JointAngleLimits2D_t2258250679)+ sizeof (Il2CppObject), sizeof(JointAngleLimits2D_t2258250679_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	JointAngleLimits2D_t2258250679::get_offset_of_m_LowerAngle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointAngleLimits2D_t2258250679::get_offset_of_m_UpperAngle_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (JointMotor2D_t682576033)+ sizeof (Il2CppObject), sizeof(JointMotor2D_t682576033_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	JointMotor2D_t682576033::get_offset_of_m_MotorSpeed_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointMotor2D_t682576033::get_offset_of_m_MaximumMotorTorque_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (JointSuspension2D_t939016335)+ sizeof (Il2CppObject), sizeof(JointSuspension2D_t939016335_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2130[3] = 
{
	JointSuspension2D_t939016335::get_offset_of_m_DampingRatio_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointSuspension2D_t939016335::get_offset_of_m_Frequency_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JointSuspension2D_t939016335::get_offset_of_m_Angle_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Joint2D_t2513613714), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (AnchoredJoint2D_t1716788702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (HingeJoint2D_t2650814389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (WheelJoint2D_t2492372869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (PhysicsMaterial2D_t1327932246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (NavMeshAgent_t588466745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (AudioSettings_t3774206607), -1, sizeof(AudioSettings_t3774206607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	AudioSettings_t3774206607_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (AudioConfigurationChangeHandler_t1377657005), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (AudioType_t794660134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2139[14] = 
{
	AudioType_t794660134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (AudioClip_t794140988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	AudioClip_t794140988::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t794140988::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (PCMReaderCallback_t83861602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (PCMSetPositionCallback_t4244274966), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (AudioListener_t3685735200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (AudioSource_t1740077639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (WrapMode_t1491636113)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2145[7] = 
{
	WrapMode_t1491636113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (AnimationEventSource_t2152433973)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2146[4] = 
{
	AnimationEventSource_t2152433973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (AnimationEvent_t3669457594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[11] = 
{
	AnimationEvent_t3669457594::get_offset_of_m_Time_0(),
	AnimationEvent_t3669457594::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t3669457594::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t3669457594::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t3669457594::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t3669457594::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t3669457594::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t3669457594::get_offset_of_m_Source_7(),
	AnimationEvent_t3669457594::get_offset_of_m_StateSender_8(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t3669457594::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (AnimationClip_t2007702890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (PlayMode_t1155122555)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	PlayMode_t1155122555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (AnimationBlendMode_t23503924)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[3] = 
{
	AnimationBlendMode_t23503924::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (Animation_t1724966010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (Enumerator_t1374492422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[2] = 
{
	Enumerator_t1374492422::get_offset_of_m_Outer_0(),
	Enumerator_t1374492422::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (AnimationState_t3682323633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (AvatarTarget_t2373143374)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2154[7] = 
{
	AvatarTarget_t2373143374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (AvatarIKGoal_t2036631794)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[5] = 
{
	AvatarIKGoal_t2036631794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (AnimatorClipInfo_t2746035113)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t2746035113_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	AnimatorClipInfo_t2746035113::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t2746035113::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (AnimatorCullingMode_t3217251138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[4] = 
{
	AnimatorCullingMode_t3217251138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (AnimatorStateInfo_t323110318)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t323110318_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[9] = 
{
	AnimatorStateInfo_t323110318::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t323110318::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (AnimatorTransitionInfo_t2817229998)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2817229998_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[6] = 
{
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2817229998::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (MatchTargetWeightMask_t258413904)+ sizeof (Il2CppObject), sizeof(MatchTargetWeightMask_t258413904_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	MatchTargetWeightMask_t258413904::get_offset_of_m_PositionXYZWeight_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MatchTargetWeightMask_t258413904::get_offset_of_m_RotationWeight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (Animator_t2776330603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (SkeletonBone_t421858229)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t421858229_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[5] = 
{
	SkeletonBone_t421858229::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_scale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t421858229::get_offset_of_transformModified_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (HumanLimit_t3300934482)+ sizeof (Il2CppObject), sizeof(HumanLimit_t3300934482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2163[5] = 
{
	HumanLimit_t3300934482::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t3300934482::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (HumanBone_t194476679)+ sizeof (Il2CppObject), sizeof(HumanBone_t194476679_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2164[3] = 
{
	HumanBone_t194476679::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t194476679::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (RuntimeAnimatorController_t274649809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (HumanBodyBones_t1606609988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2166[56] = 
{
	HumanBodyBones_t1606609988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (AnimatorControllerPlayable_t3906681469)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t3906681469_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	AnimatorControllerPlayable_t3906681469::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (AnimationPlayable_t1390812184)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1390812184_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[1] = 
{
	AnimationPlayable_t1390812184::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (CustomAnimationPlayable_t503624423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[1] = 
{
	CustomAnimationPlayable_t503624423::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (TextGenerationSettings_t1923005356)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[18] = 
{
	TextGenerationSettings_t1923005356::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1923005356::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (TextGenerator_t538854556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[11] = 
{
	TextGenerator_t538854556::get_offset_of_m_Ptr_0(),
	TextGenerator_t538854556::get_offset_of_m_LastString_1(),
	TextGenerator_t538854556::get_offset_of_m_LastSettings_2(),
	TextGenerator_t538854556::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t538854556::get_offset_of_m_LastValid_4(),
	TextGenerator_t538854556::get_offset_of_m_Verts_5(),
	TextGenerator_t538854556::get_offset_of_m_Characters_6(),
	TextGenerator_t538854556::get_offset_of_m_Lines_7(),
	TextGenerator_t538854556::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t538854556::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t538854556::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (TextAnchor_t213922566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2172[10] = 
{
	TextAnchor_t213922566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (HorizontalWrapMode_t2918974229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	HorizontalWrapMode_t2918974229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (VerticalWrapMode_t1147493927)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[3] = 
{
	VerticalWrapMode_t1147493927::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (GUIText_t3371372606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (TextMesh_t2567681854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (Font_t4241557075), -1, sizeof(Font_t4241557075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2177[2] = 
{
	Font_t4241557075_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4241557075::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (FontTextureRebuildCallback_t4168056797), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (UICharInfo_t65807484)+ sizeof (Il2CppObject), sizeof(UICharInfo_t65807484_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	UICharInfo_t65807484::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t65807484::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (UILineInfo_t4113875482)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4113875482_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2180[3] = 
{
	UILineInfo_t4113875482::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4113875482::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (UIVertex_t4244065212)+ sizeof (Il2CppObject), sizeof(UIVertex_t4244065212_marshaled_pinvoke), sizeof(UIVertex_t4244065212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2181[9] = 
{
	UIVertex_t4244065212::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t4244065212_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t4244065212_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (RenderMode_t77252893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2182[4] = 
{
	RenderMode_t77252893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (Canvas_t2727140764), -1, sizeof(Canvas_t2727140764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2183[1] = 
{
	Canvas_t2727140764_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (WillRenderCanvases_t4247149838), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (CanvasGroup_t3702418109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (CanvasRenderer_t3950887807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (RectTransformUtility_t3025555048), -1, sizeof(RectTransformUtility_t3025555048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	RectTransformUtility_t3025555048_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (Event_t4196595728), sizeof(Event_t4196595728_marshaled_pinvoke), sizeof(Event_t4196595728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2189[4] = 
{
	Event_t4196595728::get_offset_of_m_Ptr_0(),
	Event_t4196595728_StaticFields::get_offset_of_s_Current_1(),
	Event_t4196595728_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t4196595728_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (KeyCode_t3128317986)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2190[322] = 
{
	KeyCode_t3128317986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (EventType_t637886954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[31] = 
{
	EventType_t637886954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (EventModifiers_t4195406918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2192[9] = 
{
	EventModifiers_t4195406918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (ScaleMode_t3023293187)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[4] = 
{
	ScaleMode_t3023293187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (GUI_t3134605553), -1, sizeof(GUI_t3134605553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[14] = 
{
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollControlId_1(),
	GUI_t3134605553_StaticFields::get_offset_of_s_HotTextField_2(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BoxHash_3(),
	GUI_t3134605553_StaticFields::get_offset_of_s_RepeatButtonHash_4(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ToggleHash_5(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ButtonGridHash_6(),
	GUI_t3134605553_StaticFields::get_offset_of_s_SliderHash_7(),
	GUI_t3134605553_StaticFields::get_offset_of_s_BeginGroupHash_8(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollviewHash_9(),
	GUI_t3134605553_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t3134605553_StaticFields::get_offset_of_s_ScrollViewStates_11(),
	GUI_t3134605553_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_12(),
	GUI_t3134605553_StaticFields::get_offset_of_U3CscrollTroughSideU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (ScrollViewState_t2687015188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[5] = 
{
	ScrollViewState_t2687015188::get_offset_of_position_0(),
	ScrollViewState_t2687015188::get_offset_of_visibleRect_1(),
	ScrollViewState_t2687015188::get_offset_of_viewRect_2(),
	ScrollViewState_t2687015188::get_offset_of_scrollPosition_3(),
	ScrollViewState_t2687015188::get_offset_of_apply_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (WindowFunction_t2749288659), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (GUIContent_t2094828418), -1, sizeof(GUIContent_t2094828418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2197[7] = 
{
	GUIContent_t2094828418::get_offset_of_m_Text_0(),
	GUIContent_t2094828418::get_offset_of_m_Image_1(),
	GUIContent_t2094828418::get_offset_of_m_Tooltip_2(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t2094828418_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t2094828418_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (GUILayout_t3864601915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (GUILayoutUtility_t87000299), -1, sizeof(GUILayoutUtility_t87000299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2199[5] = 
{
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t87000299_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
