﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// CFX_Demo
struct CFX_Demo_t1114149805;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo/<RandomSpawnsCoroutine>c__Iterator6
struct  U3CRandomSpawnsCoroutineU3Ec__Iterator6_t1269513600  : public Il2CppObject
{
public:
	// UnityEngine.GameObject CFX_Demo/<RandomSpawnsCoroutine>c__Iterator6::<particles>__0
	GameObject_t3674682005 * ___U3CparticlesU3E__0_0;
	// System.Int32 CFX_Demo/<RandomSpawnsCoroutine>c__Iterator6::$PC
	int32_t ___U24PC_1;
	// System.Object CFX_Demo/<RandomSpawnsCoroutine>c__Iterator6::$current
	Il2CppObject * ___U24current_2;
	// CFX_Demo CFX_Demo/<RandomSpawnsCoroutine>c__Iterator6::<>f__this
	CFX_Demo_t1114149805 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CparticlesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ec__Iterator6_t1269513600, ___U3CparticlesU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CparticlesU3E__0_0() const { return ___U3CparticlesU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CparticlesU3E__0_0() { return &___U3CparticlesU3E__0_0; }
	inline void set_U3CparticlesU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CparticlesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparticlesU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ec__Iterator6_t1269513600, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ec__Iterator6_t1269513600, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ec__Iterator6_t1269513600, ___U3CU3Ef__this_3)); }
	inline CFX_Demo_t1114149805 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline CFX_Demo_t1114149805 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(CFX_Demo_t1114149805 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
