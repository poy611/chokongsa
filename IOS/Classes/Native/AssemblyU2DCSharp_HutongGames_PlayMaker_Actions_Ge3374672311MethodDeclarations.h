﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetDeviceRoll
struct GetDeviceRoll_t3374672311;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::.ctor()
extern "C"  void GetDeviceRoll__ctor_m3263161695 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::Reset()
extern "C"  void GetDeviceRoll_Reset_m909594636 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnEnter()
extern "C"  void GetDeviceRoll_OnEnter_m4258958710 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnUpdate()
extern "C"  void GetDeviceRoll_OnUpdate_m2312260493 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::DoGetDeviceRoll()
extern "C"  void GetDeviceRoll_DoGetDeviceRoll_m1222090011 (GetDeviceRoll_t3374672311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
