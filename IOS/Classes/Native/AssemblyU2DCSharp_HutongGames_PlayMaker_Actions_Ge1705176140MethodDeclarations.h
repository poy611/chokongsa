﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKeyUp
struct GetKeyUp_t1705176140;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::.ctor()
extern "C"  void GetKeyUp__ctor_m478061466 (GetKeyUp_t1705176140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::Reset()
extern "C"  void GetKeyUp_Reset_m2419461703 (GetKeyUp_t1705176140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::OnUpdate()
extern "C"  void GetKeyUp_OnUpdate_m1569562482 (GetKeyUp_t1705176140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
