﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerPrefs
struct PlayMakerPrefs_t941311808;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C"  PlayMakerPrefs_t941311808 * PlayMakerPrefs_get_Instance_m1461573414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C"  ColorU5BU5D_t2441545636* PlayMakerPrefs_get_Colors_m2303259045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::set_Colors(UnityEngine.Color[])
extern "C"  void PlayMakerPrefs_set_Colors_m71542544 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t2441545636* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PlayMakerPrefs::get_ColorNames()
extern "C"  StringU5BU5D_t4054002952* PlayMakerPrefs_get_ColorNames_m2268906614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::set_ColorNames(System.String[])
extern "C"  void PlayMakerPrefs_set_ColorNames_m2640639191 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::ResetDefaultColors()
extern "C"  void PlayMakerPrefs_ResetDefaultColors_m4260521737 (PlayMakerPrefs_t941311808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayMakerPrefs::get_MinimapColors()
extern "C"  ColorU5BU5D_t2441545636* PlayMakerPrefs_get_MinimapColors_m2755696002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::SaveChanges()
extern "C"  void PlayMakerPrefs_SaveChanges_m270435393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C"  void PlayMakerPrefs_UpdateMinimapColors_m170820455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::.ctor()
extern "C"  void PlayMakerPrefs__ctor_m1517509021 (PlayMakerPrefs_t941311808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::.cctor()
extern "C"  void PlayMakerPrefs__cctor_m3611010480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
