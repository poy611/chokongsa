﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t2895297978;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// FsmTemplate
struct FsmTemplate_t1237263802;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent>
struct List_1_t3307092330;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t2644459362;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Object
struct Object_t3071478659;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t1596141350;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Joint2D
struct Joint2D_t2513613714;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1076900934;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "mscorlib_System_Object4170816371.h"

// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_FsmList()
extern "C"  List_1_t2895297978 * Fsm_get_FsmList_m1323126625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SortedFsmList()
extern "C"  List_1_t2895297978 * Fsm_get_SortedFsmList_m2906795940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo HutongGames.PlayMaker.Fsm::get_UpdateHelperSetDirty()
extern "C"  MethodInfo_t * Fsm_get_UpdateHelperSetDirty_m1650485777 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ManualUpdate()
extern "C"  bool Fsm_get_ManualUpdate_m4192751 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ManualUpdate(System.Boolean)
extern "C"  void Fsm_set_ManualUpdate_m1366758546 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_KeepDelayedEventsOnStateExit()
extern "C"  bool Fsm_get_KeepDelayedEventsOnStateExit_m511680442 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_KeepDelayedEventsOnStateExit(System.Boolean)
extern "C"  void Fsm_set_KeepDelayedEventsOnStateExit_m1767039773 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Preprocessed()
extern "C"  bool Fsm_get_Preprocessed_m3347034827 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Preprocessed(System.Boolean)
extern "C"  void Fsm_set_Preprocessed_m3949206638 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_Host()
extern "C"  Fsm_t1527112426 * Fsm_get_Host_m3425712481 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Host(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_Host_m1917051754 (Fsm_t1527112426 * __this, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Password()
extern "C"  String_t* Fsm_get_Password_m4273726608 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Locked()
extern "C"  bool Fsm_get_Locked_m2344439114 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Lock(System.String)
extern "C"  void Fsm_Lock_m1735146856 (Fsm_t1527112426 * __this, String_t* ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Unlock(System.String)
extern "C"  void Fsm_Unlock_m466698671 (Fsm_t1527112426 * __this, String_t* ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate HutongGames.PlayMaker.Fsm::get_Template()
extern "C"  FsmTemplate_t1237263802 * Fsm_get_Template_m4115422457 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsSubFsm()
extern "C"  bool Fsm_get_IsSubFsm_m2451431210 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_RootFsm()
extern "C"  Fsm_t1527112426 * Fsm_get_RootFsm_m2562894535 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SubFsmList()
extern "C"  List_1_t2895297978 * Fsm_get_SubFsmList_m2986382845 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Started()
extern "C"  bool Fsm_get_Started_m2754310499 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Started(System.Boolean)
extern "C"  void Fsm_set_Started_m2295010990 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::get_DelayedEvents()
extern "C"  List_1_t3307092330 * Fsm_get_DelayedEvents_m3700898684 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::KillDelayedEvents()
extern "C"  void Fsm_KillDelayedEvents_m3981231728 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_DataVersion()
extern "C"  int32_t Fsm_get_DataVersion_m2041022410 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DataVersion(System.Int32)
extern "C"  void Fsm_set_DataVersion_m603798613 (Fsm_t1527112426 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Fsm::get_Owner()
extern "C"  MonoBehaviour_t667441552 * Fsm_get_Owner_m3536043597 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Owner(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_set_Owner_m1411892998 (Fsm_t1527112426 * __this, MonoBehaviour_t667441552 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_NameIsExpanded()
extern "C"  bool Fsm_get_NameIsExpanded_m2469542702 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_NameIsExpanded(System.Boolean)
extern "C"  void Fsm_set_NameIsExpanded_m1874038929 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ControlsIsExpanded()
extern "C"  bool Fsm_get_ControlsIsExpanded_m1135060825 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ControlsIsExpanded(System.Boolean)
extern "C"  void Fsm_set_ControlsIsExpanded_m2288303996 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_DebugIsExpanded()
extern "C"  bool Fsm_get_DebugIsExpanded_m1417478488 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugIsExpanded(System.Boolean)
extern "C"  void Fsm_set_DebugIsExpanded_m2858598883 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ExperimentalIsExpanded()
extern "C"  bool Fsm_get_ExperimentalIsExpanded_m1241959179 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ExperimentalIsExpanded(System.Boolean)
extern "C"  void Fsm_set_ExperimentalIsExpanded_m434335150 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Name()
extern "C"  String_t* Fsm_get_Name_m2749457024 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Name(System.String)
extern "C"  void Fsm_set_Name_m2790512555 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate HutongGames.PlayMaker.Fsm::get_UsedInTemplate()
extern "C"  FsmTemplate_t1237263802 * Fsm_get_UsedInTemplate_m4002378203 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_UsedInTemplate(FsmTemplate)
extern "C"  void Fsm_set_UsedInTemplate_m2612368368 (Fsm_t1527112426 * __this, FsmTemplate_t1237263802 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_StartState()
extern "C"  String_t* Fsm_get_StartState_m2760224068 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StartState(System.String)
extern "C"  void Fsm_set_StartState_m2182178983 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::get_States()
extern "C"  FsmStateU5BU5D_t2644459362* Fsm_get_States_m3319335032 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_States(HutongGames.PlayMaker.FsmState[])
extern "C"  void Fsm_set_States_m2030862255 (Fsm_t1527112426 * __this, FsmStateU5BU5D_t2644459362* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::get_Events()
extern "C"  FsmEventU5BU5D_t2862142229* Fsm_get_Events_m3588867224 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Events(HutongGames.PlayMaker.FsmEvent[])
extern "C"  void Fsm_set_Events_m3900487261 (Fsm_t1527112426 * __this, FsmEventU5BU5D_t2862142229* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::get_GlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t818210886* Fsm_get_GlobalTransitions_m2969873965 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_GlobalTransitions(HutongGames.PlayMaker.FsmTransition[])
extern "C"  void Fsm_set_GlobalTransitions_m1888859542 (Fsm_t1527112426 * __this, FsmTransitionU5BU5D_t818210886* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::get_Variables()
extern "C"  FsmVariables_t963491929 * Fsm_get_Variables_m2281949087 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm_set_Variables_m1137150058 (Fsm_t1527112426 * __this, FsmVariables_t963491929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::get_EventTarget()
extern "C"  FsmEventTarget_t1823904941 * Fsm_get_EventTarget_m2687079071 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_EventTarget(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void Fsm_set_EventTarget_m3950818474 (Fsm_t1527112426 * __this, FsmEventTarget_t1823904941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Initialized()
extern "C"  bool Fsm_get_Initialized_m20616822 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Active()
extern "C"  bool Fsm_get_Active_m3073608262 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Finished()
extern "C"  bool Fsm_get_Finished_m2204445938 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Finished(System.Boolean)
extern "C"  void Fsm_set_Finished_m1611914325 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsSwitchingState()
extern "C"  bool Fsm_get_IsSwitchingState_m681506701 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_ActiveState()
extern "C"  FsmState_t2146334067 * Fsm_get_ActiveState_m580729145 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ActiveState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_ActiveState_m3273954052 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_ActiveStateName()
extern "C"  String_t* Fsm_get_ActiveStateName_m1896234915 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_PreviousActiveState()
extern "C"  FsmState_t2146334067 * Fsm_get_PreviousActiveState_m1798855298 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_PreviousActiveState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_PreviousActiveState_m964994701 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.Fsm::get_LastTransition()
extern "C"  FsmTransition_t3771611999 * Fsm_get_LastTransition_m3649861433 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastTransition(HutongGames.PlayMaker.FsmTransition)
extern "C"  void Fsm_set_LastTransition_m1415015442 (Fsm_t1527112426 * __this, FsmTransition_t3771611999 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_MaxLoopCount()
extern "C"  int32_t Fsm_get_MaxLoopCount_m2308382189 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_MaxLoopCountOverride()
extern "C"  int32_t Fsm_get_MaxLoopCountOverride_m690398937 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_MaxLoopCountOverride(System.Int32)
extern "C"  void Fsm_set_MaxLoopCountOverride_m1972494768 (Fsm_t1527112426 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_OwnerName()
extern "C"  String_t* Fsm_get_OwnerName_m1825270827 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_OwnerDebugName()
extern "C"  String_t* Fsm_get_OwnerDebugName_m3615957248 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_GameObject()
extern "C"  GameObject_t3674682005 * Fsm_get_GameObject_m993266224 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_GameObjectName()
extern "C"  String_t* Fsm_get_GameObjectName_m2044394161 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.Fsm::get_OwnerObject()
extern "C"  Object_t3071478659 * Fsm_get_OwnerObject_m1330565575 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.Fsm::get_FsmComponent()
extern "C"  PlayMakerFSM_t3799847376 * Fsm_get_FsmComponent_m1588515396 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.Fsm::get_MyLog()
extern "C"  FsmLog_t1596141350 * Fsm_get_MyLog_m4006974771 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsModifiedPrefabInstance()
extern "C"  bool Fsm_get_IsModifiedPrefabInstance_m3180955436 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsModifiedPrefabInstance(System.Boolean)
extern "C"  void Fsm_set_IsModifiedPrefabInstance_m701526287 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Description()
extern "C"  String_t* Fsm_get_Description_m3085929929 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Description(System.String)
extern "C"  void Fsm_set_Description_m124830864 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Watermark()
extern "C"  String_t* Fsm_get_Watermark_m3290725169 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Watermark(System.String)
extern "C"  void Fsm_set_Watermark_m3806765544 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ShowStateLabel()
extern "C"  bool Fsm_get_ShowStateLabel_m1575907712 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ShowStateLabel(System.Boolean)
extern "C"  void Fsm_set_ShowStateLabel_m3525398371 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.Fsm::get_DebugLookAtColor()
extern "C"  Color_t4194546905  Fsm_get_DebugLookAtColor_m3795516815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugLookAtColor(UnityEngine.Color)
extern "C"  void Fsm_set_DebugLookAtColor_m3949841276 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.Fsm::get_DebugRaycastColor()
extern "C"  Color_t4194546905  Fsm_get_DebugRaycastColor_m257755934 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugRaycastColor(UnityEngine.Color)
extern "C"  void Fsm_set_DebugRaycastColor_m3999789155 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_GuiLabel()
extern "C"  String_t* Fsm_get_GuiLabel_m3109631214 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_DocUrl()
extern "C"  String_t* Fsm_get_DocUrl_m4111398156 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DocUrl(System.String)
extern "C"  void Fsm_set_DocUrl_m3738673759 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_EditState()
extern "C"  FsmState_t2146334067 * Fsm_get_EditState_m3234409717 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_EditState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_EditState_m3714559936 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_LastClickedObject()
extern "C"  GameObject_t3674682005 * Fsm_get_LastClickedObject_m61592307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastClickedObject(UnityEngine.GameObject)
extern "C"  void Fsm_set_LastClickedObject_m1114637630 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_BreakpointsEnabled()
extern "C"  bool Fsm_get_BreakpointsEnabled_m2348288191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakpointsEnabled(System.Boolean)
extern "C"  void Fsm_set_BreakpointsEnabled_m4116952674 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HitBreakpoint()
extern "C"  bool Fsm_get_HitBreakpoint_m388538182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HitBreakpoint(System.Boolean)
extern "C"  void Fsm_set_HitBreakpoint_m3450510417 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_BreakAtFsm()
extern "C"  Fsm_t1527112426 * Fsm_get_BreakAtFsm_m1502641799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakAtFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_BreakAtFsm_m397672260 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_BreakAtState()
extern "C"  FsmState_t2146334067 * Fsm_get_BreakAtState_m1258326835 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakAtState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_BreakAtState_m1969794222 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsBreak()
extern "C"  bool Fsm_get_IsBreak_m149400023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsBreak(System.Boolean)
extern "C"  void Fsm_set_IsBreak_m2872144930 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsErrorBreak()
extern "C"  bool Fsm_get_IsErrorBreak_m2495782369 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsErrorBreak(System.Boolean)
extern "C"  void Fsm_set_IsErrorBreak_m2156587268 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_LastError()
extern "C"  String_t* Fsm_get_LastError_m2637962335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastError(System.String)
extern "C"  void Fsm_set_LastError_m3637959546 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_StepToStateChange()
extern "C"  bool Fsm_get_StepToStateChange_m3591817244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StepToStateChange(System.Boolean)
extern "C"  void Fsm_set_StepToStateChange_m862612647 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_StepFsm()
extern "C"  Fsm_t1527112426 * Fsm_get_StepFsm_m479938333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StepFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_StepFsm_m450545076 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_SwitchedState()
extern "C"  bool Fsm_get_SwitchedState_m908911200 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_SwitchedState(System.Boolean)
extern "C"  void Fsm_set_SwitchedState_m3804654059 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_MouseEvents()
extern "C"  bool Fsm_get_MouseEvents_m624787264 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_MouseEvents(System.Boolean)
extern "C"  void Fsm_set_MouseEvents_m1246917579 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter2D()
extern "C"  bool Fsm_get_HandleTriggerEnter2D_m3537267354 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerEnter2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerEnter2D_m483742205 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit2D()
extern "C"  bool Fsm_get_HandleTriggerExit2D_m2035912226 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerExit2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerExit2D_m3706420013 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay2D()
extern "C"  bool Fsm_get_HandleTriggerStay2D_m1185990109 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerStay2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerStay2D_m3441233832 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter2D()
extern "C"  bool Fsm_get_HandleCollisionEnter2D_m102673568 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionEnter2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionEnter2D_m2261935747 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit2D()
extern "C"  bool Fsm_get_HandleCollisionExit2D_m4003328860 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionExit2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionExit2D_m3763781095 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay2D()
extern "C"  bool Fsm_get_HandleCollisionStay2D_m3153406743 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionStay2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionStay2D_m3498594914 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter()
extern "C"  bool Fsm_get_HandleTriggerEnter_m204797576 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerEnter(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerEnter_m189026411 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit()
extern "C"  bool Fsm_get_HandleTriggerExit_m985357328 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerExit(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerExit_m1278412187 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay()
extern "C"  bool Fsm_get_HandleTriggerStay_m1382237835 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerStay(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerStay_m790985942 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter()
extern "C"  bool Fsm_get_HandleCollisionEnter_m1653735950 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionEnter(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionEnter_m745066097 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit()
extern "C"  bool Fsm_get_HandleCollisionExit_m3664496586 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionExit(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionExit_m3374558933 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay()
extern "C"  bool Fsm_get_HandleCollisionStay_m4061377093 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionStay(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionStay_m2887132688 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleParticleCollision()
extern "C"  bool Fsm_get_HandleParticleCollision_m1126387750 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleParticleCollision(System.Boolean)
extern "C"  void Fsm_set_HandleParticleCollision_m1186708529 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleControllerColliderHit()
extern "C"  bool Fsm_get_HandleControllerColliderHit_m456508509 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleControllerColliderHit(System.Boolean)
extern "C"  void Fsm_set_HandleControllerColliderHit_m1230002728 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak()
extern "C"  bool Fsm_get_HandleJointBreak_m2499030429 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleJointBreak(System.Boolean)
extern "C"  void Fsm_set_HandleJointBreak_m1127786944 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak2D()
extern "C"  bool Fsm_get_HandleJointBreak2D_m681848943 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleJointBreak2D(System.Boolean)
extern "C"  void Fsm_set_HandleJointBreak2D_m689482258 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleOnGUI()
extern "C"  bool Fsm_get_HandleOnGUI_m3061956918 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleOnGUI(System.Boolean)
extern "C"  void Fsm_set_HandleOnGUI_m1667829569 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleFixedUpdate()
extern "C"  bool Fsm_get_HandleFixedUpdate_m1243618487 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleFixedUpdate(System.Boolean)
extern "C"  void Fsm_set_HandleFixedUpdate_m3395950082 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleApplicationEvents()
extern "C"  bool Fsm_get_HandleApplicationEvents_m1205607459 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleApplicationEvents(System.Boolean)
extern "C"  void Fsm_set_HandleApplicationEvents_m1955416430 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ResetEventHandlerFlags()
extern "C"  void Fsm_ResetEventHandlerFlags_m2811882359 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision HutongGames.PlayMaker.Fsm::get_CollisionInfo()
extern "C"  Collision_t2494107688 * Fsm_get_CollisionInfo_m2736371010 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_CollisionInfo(UnityEngine.Collision)
extern "C"  void Fsm_set_CollisionInfo_m1284185473 (Fsm_t1527112426 * __this, Collision_t2494107688 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider HutongGames.PlayMaker.Fsm::get_TriggerCollider()
extern "C"  Collider_t2939674232 * Fsm_get_TriggerCollider_m2008788780 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerCollider(UnityEngine.Collider)
extern "C"  void Fsm_set_TriggerCollider_m3121237943 (Fsm_t1527112426 * __this, Collider_t2939674232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision2D HutongGames.PlayMaker.Fsm::get_Collision2DInfo()
extern "C"  Collision2D_t2859305914 * Fsm_get_Collision2DInfo_m225936834 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Collision2DInfo(UnityEngine.Collision2D)
extern "C"  void Fsm_set_Collision2DInfo_m1305609245 (Fsm_t1527112426 * __this, Collision2D_t2859305914 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D HutongGames.PlayMaker.Fsm::get_TriggerCollider2D()
extern "C"  Collider2D_t1552025098 * Fsm_get_TriggerCollider2D_m1137780652 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerCollider2D(UnityEngine.Collider2D)
extern "C"  void Fsm_set_TriggerCollider2D_m2954447927 (Fsm_t1527112426 * __this, Collider2D_t1552025098 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Fsm::get_JointBreakForce()
extern "C"  float Fsm_get_JointBreakForce_m1141664588 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_JointBreakForce(System.Single)
extern "C"  void Fsm_set_JointBreakForce_m4230075679 (Fsm_t1527112426 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Joint2D HutongGames.PlayMaker.Fsm::get_BrokenJoint2D()
extern "C"  Joint2D_t2513613714 * Fsm_get_BrokenJoint2D_m3680999647 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BrokenJoint2D(UnityEngine.Joint2D)
extern "C"  void Fsm_set_BrokenJoint2D_m3302948528 (Fsm_t1527112426 * __this, Joint2D_t2513613714 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_ParticleCollisionGO()
extern "C"  GameObject_t3674682005 * Fsm_get_ParticleCollisionGO_m27653975 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ParticleCollisionGO(UnityEngine.GameObject)
extern "C"  void Fsm_set_ParticleCollisionGO_m1548783074 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_CollisionGO()
extern "C"  GameObject_t3674682005 * Fsm_get_CollisionGO_m3350081085 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_Collision2dGO()
extern "C"  GameObject_t3674682005 * Fsm_get_Collision2dGO_m1916561071 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_TriggerGO()
extern "C"  GameObject_t3674682005 * Fsm_get_TriggerGO_m1559505475 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_Trigger2dGO()
extern "C"  GameObject_t3674682005 * Fsm_get_Trigger2dGO_m3455285557 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_TriggerName()
extern "C"  String_t* Fsm_get_TriggerName_m778646640 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerName(System.String)
extern "C"  void Fsm_set_TriggerName_m3322111305 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_CollisionName()
extern "C"  String_t* Fsm_get_CollisionName_m3152207722 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_CollisionName(System.String)
extern "C"  void Fsm_set_CollisionName_m3710291663 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Trigger2dName()
extern "C"  String_t* Fsm_get_Trigger2dName_m1174490210 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Trigger2dName(System.String)
extern "C"  void Fsm_set_Trigger2dName_m2477978839 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Collision2dName()
extern "C"  String_t* Fsm_get_Collision2dName_m1539055836 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Collision2dName(System.String)
extern "C"  void Fsm_set_Collision2dName_m1857148125 (Fsm_t1527112426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ControllerColliderHit HutongGames.PlayMaker.Fsm::get_ControllerCollider()
extern "C"  ControllerColliderHit_t2416790841 * Fsm_get_ControllerCollider_m1596430369 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ControllerCollider(UnityEngine.ControllerColliderHit)
extern "C"  void Fsm_set_ControllerCollider_m2431967722 (Fsm_t1527112426 * __this, ControllerColliderHit_t2416790841 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit HutongGames.PlayMaker.Fsm::get_RaycastHitInfo()
extern "C"  RaycastHit_t4003175726  Fsm_get_RaycastHitInfo_m576422000 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_RaycastHitInfo(UnityEngine.RaycastHit)
extern "C"  void Fsm_set_RaycastHitInfo_m638262883 (Fsm_t1527112426 * __this, RaycastHit_t4003175726  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::RecordLastRaycastHit2DInfo(HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D)
extern "C"  void Fsm_RecordLastRaycastHit2DInfo_m3252930453 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, RaycastHit2D_t1374744384  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D HutongGames.PlayMaker.Fsm::GetLastRaycastHit2DInfo(HutongGames.PlayMaker.Fsm)
extern "C"  RaycastHit2D_t1374744384  Fsm_GetLastRaycastHit2DInfo_m382528231 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorMove()
extern "C"  bool Fsm_get_HandleAnimatorMove_m3734961120 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleAnimatorMove(System.Boolean)
extern "C"  void Fsm_set_HandleAnimatorMove_m2293111747 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorIK()
extern "C"  bool Fsm_get_HandleAnimatorIK_m3243950161 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleAnimatorIK(System.Boolean)
extern "C"  void Fsm_set_HandleAnimatorIK_m368996724 (Fsm_t1527112426 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::NewTempFsm()
extern "C"  Fsm_t1527112426 * Fsm_NewTempFsm_m2446050556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.ctor()
extern "C"  void Fsm__ctor_m2246436149 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm__ctor_m2998734970 (Fsm_t1527112426 * __this, Fsm_t1527112426 * ___source0, FsmVariables_t963491929 * ___overrideVariables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::CreateSubFsm(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  Fsm_t1527112426 * Fsm_CreateSubFsm_m1210636381 (Fsm_t1527112426 * __this, FsmTemplateControl_t2786508133 * ___templateControl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::GetRootFsm()
extern "C"  Fsm_t1527112426 * Fsm_GetRootFsm_m3589711800 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::CheckIfDirty()
extern "C"  void Fsm_CheckIfDirty_m2770721468 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Reset(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Reset_m1527928237 (Fsm_t1527112426 * __this, MonoBehaviour_t667441552 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateDataVersion()
extern "C"  void Fsm_UpdateDataVersion_m517315416 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SaveActions()
extern "C"  void Fsm_SaveActions_m2114207123 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Clear(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Clear_m1648954863 (Fsm_t1527112426 * __this, MonoBehaviour_t667441552 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixDataVersion()
extern "C"  void Fsm_FixDataVersion_m2384707624 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::DeduceDataVersion()
extern "C"  int32_t Fsm_DeduceDataVersion_m916276127 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Preprocess(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Preprocess_m1676843348 (Fsm_t1527112426 * __this, MonoBehaviour_t667441552 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Preprocess()
extern "C"  void Fsm_Preprocess_m2566919771 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Awake()
extern "C"  void Fsm_Awake_m2484041368 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Init(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Init_m4020318640 (Fsm_t1527112426 * __this, MonoBehaviour_t667441552 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Reinitialize()
extern "C"  void Fsm_Reinitialize_m3548119218 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::InitData()
extern "C"  void Fsm_InitData_m1165351113 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::CheckFsmEventsForEventHandlers()
extern "C"  void Fsm_CheckFsmEventsForEventHandlers_m258198138 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnEnable()
extern "C"  void Fsm_OnEnable_m1470487089 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Start()
extern "C"  void Fsm_Start_m1193573941 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Update()
extern "C"  void Fsm_Update_m2646905976 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateDelayedEvents()
extern "C"  void Fsm_UpdateDelayedEvents_m118462757 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ClearDelayedEvents()
extern "C"  void Fsm_ClearDelayedEvents_m2042348861 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdate()
extern "C"  void Fsm_FixedUpdate_m2195343088 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::LateUpdate()
extern "C"  void Fsm_LateUpdate_m4198981182 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDisable()
extern "C"  void Fsm_OnDisable_m3076364060 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Stop()
extern "C"  void Fsm_Stop_m177462513 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::StopAndReset()
extern "C"  void Fsm_StopAndReset_m898923721 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::HasEvent(System.String)
extern "C"  bool Fsm_HasEvent_m4292283595 (Fsm_t1527112426 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ProcessEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData)
extern "C"  void Fsm_ProcessEvent_m713583330 (Fsm_t1527112426 * __this, FsmEvent_t2133468028 * ___fsmEvent0, FsmEventData_t1076900934 * ___eventData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetEventDataSentByInfo()
extern "C"  void Fsm_SetEventDataSentByInfo_m138334798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetEventDataSentByInfo(HutongGames.PlayMaker.FsmEventData)
extern "C"  void Fsm_SetEventDataSentByInfo_m1855395966 (Il2CppObject * __this /* static, unused */, FsmEventData_t1076900934 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.Fsm::GetEventDataSentByInfo()
extern "C"  FsmEventData_t1076900934 * Fsm_GetEventDataSentByInfo_m616629769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEventTarget,System.String)
extern "C"  void Fsm_Event_m3379708434 (Fsm_t1527112426 * __this, FsmEventTarget_t1823904941 * ___eventTarget0, String_t* ___fsmEventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_Event_m1295831978 (Fsm_t1527112426 * __this, FsmEventTarget_t1823904941 * ___eventTarget0, FsmEvent_t2133468028 * ___fsmEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(System.String)
extern "C"  void Fsm_Event_m4127177141 (Fsm_t1527112426 * __this, String_t* ___fsmEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_Event_m625948263 (Fsm_t1527112426 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Fsm::DelayedEvent(HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  DelayedEvent_t1938906778 * Fsm_DelayedEvent_m1399601449 (Fsm_t1527112426 * __this, FsmEvent_t2133468028 * ___fsmEvent0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Fsm::DelayedEvent(HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  DelayedEvent_t1938906778 * Fsm_DelayedEvent_m2987143954 (Fsm_t1527112426 * __this, FsmEventTarget_t1823904941 * ___eventTarget0, FsmEvent_t2133468028 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEvent(System.String,System.Boolean)
extern "C"  void Fsm_BroadcastEvent_m344787011 (Fsm_t1527112426 * __this, String_t* ___fsmEventName0, bool ___excludeSelf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEvent(HutongGames.PlayMaker.FsmEvent,System.Boolean)
extern "C"  void Fsm_BroadcastEvent_m4264668283 (Fsm_t1527112426 * __this, FsmEvent_t2133468028 * ___fsmEvent0, bool ___excludeSelf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEventToGameObject(UnityEngine.GameObject,System.String,System.Boolean,System.Boolean)
extern "C"  void Fsm_BroadcastEventToGameObject_m2269921640 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___go0, String_t* ___fsmEventName1, bool ___sendToChildren2, bool ___excludeSelf3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEventToGameObject(UnityEngine.GameObject,HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData,System.Boolean,System.Boolean)
extern "C"  void Fsm_BroadcastEventToGameObject_m532693922 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___go0, FsmEvent_t2133468028 * ___fsmEvent1, FsmEventData_t1076900934 * ___eventData2, bool ___sendToChildren3, bool ___excludeSelf4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SendEventToFsmOnGameObject(UnityEngine.GameObject,System.String,System.String)
extern "C"  void Fsm_SendEventToFsmOnGameObject_m671707458 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___gameObject0, String_t* ___fsmName1, String_t* ___fsmEventName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SendEventToFsmOnGameObject(UnityEngine.GameObject,System.String,HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_SendEventToFsmOnGameObject_m3009669754 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___gameObject0, String_t* ___fsmName1, FsmEvent_t2133468028 * ___fsmEvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetState(System.String)
extern "C"  void Fsm_SetState_m1081751492 (Fsm_t1527112426 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateStateChanges()
extern "C"  void Fsm_UpdateStateChanges_m112169610 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::DoTransition(HutongGames.PlayMaker.FsmTransition,System.Boolean)
extern "C"  bool Fsm_DoTransition_m3030009067 (Fsm_t1527112426 * __this, FsmTransition_t3771611999 * ___transition0, bool ___isGlobal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SwitchState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_SwitchState_m3617438963 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___toState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::GotoPreviousState()
extern "C"  void Fsm_GotoPreviousState_m3818426570 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::EnterState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_EnterState_m949242091 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_FixedUpdateState_m2059042566 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_UpdateState_m1830487166 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::LateUpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_LateUpdateState_m3179361528 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ExitState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_ExitState_m1252159945 (Fsm_t1527112426 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::GetSubFsm(System.String)
extern "C"  Fsm_t1527112426 * Fsm_GetSubFsm_m1685570682 (Fsm_t1527112426 * __this, String_t* ___subFsmName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* Fsm_GetFullFsmLabel_m3992658159 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::GetOwnerDefaultTarget(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  GameObject_t3674682005 * Fsm_GetOwnerDefaultTarget_m846013999 (Fsm_t1527112426 * __this, FsmOwnerDefault_t251897112 * ___ownerDefault0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::GetState(System.String)
extern "C"  FsmState_t2146334067 * Fsm_GetState_m816012764 (Fsm_t1527112426 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Fsm::GetEvent(System.String)
extern "C"  FsmEvent_t2133468028 * Fsm_GetEvent_m2439363594 (Fsm_t1527112426 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::CompareTo(System.Object)
extern "C"  int32_t Fsm_CompareTo_m3248853455 (Fsm_t1527112426 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Fsm::GetFsmObject(System.String)
extern "C"  FsmObject_t821476169 * Fsm_GetFsmObject_m2905355466 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Fsm::GetFsmMaterial(System.String)
extern "C"  FsmMaterial_t924399665 * Fsm_GetFsmMaterial_m1156815290 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Fsm::GetFsmTexture(System.String)
extern "C"  FsmTexture_t3073272573 * Fsm_GetFsmTexture_m119778404 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Fsm::GetFsmFloat(System.String)
extern "C"  FsmFloat_t2134102846 * Fsm_GetFsmFloat_m618220612 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Fsm::GetFsmInt(System.String)
extern "C"  FsmInt_t1596138449 * Fsm_GetFsmInt_m3993502052 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Fsm::GetFsmBool(System.String)
extern "C"  FsmBool_t1075959796 * Fsm_GetFsmBool_m1829953076 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Fsm::GetFsmString(System.String)
extern "C"  FsmString_t952858651 * Fsm_GetFsmString_m1216854182 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Fsm::GetFsmVector2(System.String)
extern "C"  FsmVector2_t533912881 * Fsm_GetFsmVector2_m1126584804 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Fsm::GetFsmVector3(System.String)
extern "C"  FsmVector3_t533912882 * Fsm_GetFsmVector3_m1246509252 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Fsm::GetFsmRect(System.String)
extern "C"  FsmRect_t1076426478 * Fsm_GetFsmRect_m212333888 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Fsm::GetFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t3871136040 * Fsm_GetFsmQuaternion_m1434961228 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Fsm::GetFsmColor(System.String)
extern "C"  FsmColor_t2131419205 * Fsm_GetFsmColor_m4053619236 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Fsm::GetFsmGameObject(System.String)
extern "C"  FsmGameObject_t1697147867 * Fsm_GetFsmGameObject_m3734796198 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Fsm::GetFsmArray(System.String)
extern "C"  FsmArray_t2129666875 * Fsm_GetFsmArray_m1604367076 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Fsm::GetFsmEnum(System.String)
extern "C"  FsmEnum_t1076048395 * Fsm_GetFsmEnum_m1487436166 (Fsm_t1527112426 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmos()
extern "C"  void Fsm_OnDrawGizmos_m3578189387 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmosSelected()
extern "C"  void Fsm_OnDrawGizmosSelected_m4258496582 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionEnter_m2124714755 (Fsm_t1527112426 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionStay_m3472074680 (Fsm_t1527112426 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionExit_m3915687763 (Fsm_t1527112426 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerEnter_m3743133283 (Fsm_t1527112426 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerStay_m1973116378 (Fsm_t1527112426 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerExit_m3649994463 (Fsm_t1527112426 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void Fsm_OnParticleCollision_m1030297144 (Fsm_t1527112426 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionEnter2D_m3950287647 (Fsm_t1527112426 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionStay2D_m2685422932 (Fsm_t1527112426 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionExit2D_m343017327 (Fsm_t1527112426 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerEnter2D_m449017059 (Fsm_t1527112426 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerStay2D_m39882970 (Fsm_t1527112426 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerExit2D_m102868831 (Fsm_t1527112426 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void Fsm_OnControllerColliderHit_m23910575 (Fsm_t1527112426 * __this, ControllerColliderHit_t2416790841 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak(System.Single)
extern "C"  void Fsm_OnJointBreak_m1675142728 (Fsm_t1527112426 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void Fsm_OnJointBreak2D_m1427593368 (Fsm_t1527112426 * __this, Joint2D_t2513613714 * ___brokenJoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorMove()
extern "C"  void Fsm_OnAnimatorMove_m839048454 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorIK(System.Int32)
extern "C"  void Fsm_OnAnimatorIK_m737062344 (Fsm_t1527112426 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnGUI()
extern "C"  void Fsm_OnGUI_m1741834799 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreakpoint()
extern "C"  void Fsm_DoBreakpoint_m236647243 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreakError(System.String)
extern "C"  void Fsm_DoBreakError_m2891634239 (Fsm_t1527112426 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreak()
extern "C"  void Fsm_DoBreak_m4026252295 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Continue()
extern "C"  void Fsm_Continue_m1732412758 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDestroy()
extern "C"  void Fsm_OnDestroy_m958928814 (Fsm_t1527112426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.cctor()
extern "C"  void Fsm__cctor_m437947672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
