﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
struct OnP2PConnectedCallback_t507387838;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnP2PConnectedCallback__ctor_m2704862053 (OnP2PConnectedCallback_t507387838 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void OnP2PConnectedCallback_Invoke_m496854993 (OnP2PConnectedCallback_t507387838 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnP2PConnectedCallback_BeginInvoke_m2750434106 (OnP2PConnectedCallback_t507387838 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnP2PConnectedCallback_EndInvoke_m1504434165 (OnP2PConnectedCallback_t507387838 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
