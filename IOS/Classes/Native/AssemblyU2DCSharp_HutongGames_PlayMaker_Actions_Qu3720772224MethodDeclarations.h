﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionInverse
struct QuaternionInverse_t3720772224;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::.ctor()
extern "C"  void QuaternionInverse__ctor_m1384575670 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::Reset()
extern "C"  void QuaternionInverse_Reset_m3325975907 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnEnter()
extern "C"  void QuaternionInverse_OnEnter_m2824053005 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnUpdate()
extern "C"  void QuaternionInverse_OnUpdate_m779856598 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnLateUpdate()
extern "C"  void QuaternionInverse_OnLateUpdate_m2461013404 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnFixedUpdate()
extern "C"  void QuaternionInverse_OnFixedUpdate_m4152916818 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::DoQuatInverse()
extern "C"  void QuaternionInverse_DoQuatInverse_m1249792418 (QuaternionInverse_t3720772224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
