﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m2272897223_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m2272897223(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m2272897223_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::<>m__10(T1,T2)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110(__this, ___val10, ___val21, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 *, int32_t, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110_gshared)(__this, ___val10, ___val21, method)
