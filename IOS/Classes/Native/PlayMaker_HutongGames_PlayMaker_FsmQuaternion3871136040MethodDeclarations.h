﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// UnityEngine.Quaternion HutongGames.PlayMaker.FsmQuaternion::get_Value()
extern "C"  Quaternion_t1553702882  FsmQuaternion_get_Value_m3393858025 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::set_Value(UnityEngine.Quaternion)
extern "C"  void FsmQuaternion_set_Value_m446581172 (FsmQuaternion_t3871136040 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmQuaternion::get_RawValue()
extern "C"  Il2CppObject * FsmQuaternion_get_RawValue_m3425276490 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::set_RawValue(System.Object)
extern "C"  void FsmQuaternion_set_RawValue_m4017247073 (FsmQuaternion_t3871136040 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor()
extern "C"  void FsmQuaternion__ctor_m44600631 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor(System.String)
extern "C"  void FsmQuaternion__ctor_m325878571 (FsmQuaternion_t3871136040 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor(HutongGames.PlayMaker.FsmQuaternion)
extern "C"  void FsmQuaternion__ctor_m1612345129 (FsmQuaternion_t3871136040 * __this, FsmQuaternion_t3871136040 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmQuaternion::Clone()
extern "C"  NamedVariable_t3211770239 * FsmQuaternion_Clone_m3190499164 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmQuaternion::get_VariableType()
extern "C"  int32_t FsmQuaternion_get_VariableType_m1499843085 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmQuaternion::ToString()
extern "C"  String_t* FsmQuaternion_ToString_m2275749270 (FsmQuaternion_t3871136040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmQuaternion::op_Implicit(UnityEngine.Quaternion)
extern "C"  FsmQuaternion_t3871136040 * FsmQuaternion_op_Implicit_m3700973956 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
