﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// System.Type
struct Type_t;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// System.Type HutongGames.PlayMaker.FsmObject::get_ObjectType()
extern "C"  Type_t * FsmObject_get_ObjectType_m958343766 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_ObjectType(System.Type)
extern "C"  void FsmObject_set_ObjectType_m1796843605 (FsmObject_t821476169 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmObject::get_TypeName()
extern "C"  String_t* FsmObject_get_TypeName_m1942994297 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.FsmObject::get_Value()
extern "C"  Object_t3071478659 * FsmObject_get_Value_m188501991 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_Value(UnityEngine.Object)
extern "C"  void FsmObject_set_Value_m867520242 (FsmObject_t821476169 * __this, Object_t3071478659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmObject::get_RawValue()
extern "C"  Il2CppObject * FsmObject_get_RawValue_m1203428587 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_RawValue(System.Object)
extern "C"  void FsmObject_set_RawValue_m2761700320 (FsmObject_t821476169 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor()
extern "C"  void FsmObject__ctor_m3316802358 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor(System.String)
extern "C"  void FsmObject__ctor_m3417351052 (FsmObject_t821476169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmObject__ctor_m175629385 (FsmObject_t821476169 * __this, FsmObject_t821476169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmObject::Clone()
extern "C"  NamedVariable_t3211770239 * FsmObject_Clone_m2500526683 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmObject::get_VariableType()
extern "C"  int32_t FsmObject_get_VariableType_m136640302 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmObject::ToString()
extern "C"  String_t* FsmObject_ToString_m1589389751 (FsmObject_t821476169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmObject::op_Implicit(UnityEngine.Object)
extern "C"  FsmObject_t821476169 * FsmObject_op_Implicit_m31306241 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmObject::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmObject_TestTypeConstraint_m671650128 (FsmObject_t821476169 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
