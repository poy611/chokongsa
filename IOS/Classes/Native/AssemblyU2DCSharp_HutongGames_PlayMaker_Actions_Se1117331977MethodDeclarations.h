﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmArray
struct SetFsmArray_t1117331977;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmArray::.ctor()
extern "C"  void SetFsmArray__ctor_m3091792717 (SetFsmArray_t1117331977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArray::Reset()
extern "C"  void SetFsmArray_Reset_m738225658 (SetFsmArray_t1117331977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArray::OnEnter()
extern "C"  void SetFsmArray_OnEnter_m2782128100 (SetFsmArray_t1117331977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArray::DoSetFsmArrayCopy()
extern "C"  void SetFsmArray_DoSetFsmArrayCopy_m1583921584 (SetFsmArray_t1117331977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
