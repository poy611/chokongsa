﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LobbyTalkBoxAnimation
struct LobbyTalkBoxAnimation_t432405659;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void LobbyTalkBoxAnimation::.ctor()
extern "C"  void LobbyTalkBoxAnimation__ctor_m2333342512 (LobbyTalkBoxAnimation_t432405659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyTalkBoxAnimation::ShowmetheCoffeSizeUp()
extern "C"  void LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661 (LobbyTalkBoxAnimation_t432405659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LobbyTalkBoxAnimation::ShowmetheCoffeSizeUpCo()
extern "C"  Il2CppObject * LobbyTalkBoxAnimation_ShowmetheCoffeSizeUpCo_m3317454353 (LobbyTalkBoxAnimation_t432405659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
