﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutPasswordField
struct GUILayoutPasswordField_t886514882;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::.ctor()
extern "C"  void GUILayoutPasswordField__ctor_m1810662884 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::Reset()
extern "C"  void GUILayoutPasswordField_Reset_m3752063121 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutPasswordField::OnGUI()
extern "C"  void GUILayoutPasswordField_OnGUI_m1306061534 (GUILayoutPasswordField_t886514882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
