﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3928821330;
// System.Collections.Generic.IDictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct IDictionary_2_t2755903203;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct ICollection_1_t4032376913;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>[]
struct KeyValuePair_2U5BU5D_t3030736685;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>
struct IEnumerator_1_t693708317;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct KeyCollection_t509822013;
// System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct ValueCollection_t1878635571;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En200385954.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor()
extern "C"  void Dictionary_2__ctor_m4060116765_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4060116765(__this, method) ((  void (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2__ctor_m4060116765_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3941886109_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3941886109(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3941886109_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1955110962_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1955110962(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1955110962_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2520294775_gshared (Dictionary_2_t3178029858 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2520294775(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2520294775_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1525029643_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1525029643(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1525029643_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2048415462_gshared (Dictionary_2_t3178029858 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2048415462(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2048415462_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2488628583_gshared (Dictionary_2_t3178029858 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2488628583(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3178029858 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2488628583_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m884898930_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m884898930(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m884898930_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3893549028_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3893549028(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3893549028_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4188352866_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4188352866(__this, method) ((  bool (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m4188352866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2227554328_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2227554328(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2227554328_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3444675015_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3444675015(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3444675015_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2375314474_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2375314474(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2375314474_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3493418312_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3493418312(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3493418312_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m131496453_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m131496453(__this, ___key0, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m131496453_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1872862988_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1872862988(__this, method) ((  bool (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1872862988_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2678637694_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2678637694(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2678637694_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3992350800_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3992350800(__this, method) ((  bool (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3992350800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4162166811_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2_t3076810564  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4162166811(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3178029858 *, KeyValuePair_2_t3076810564 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4162166811_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3153170635_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2_t3076810564  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3153170635(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3178029858 *, KeyValuePair_2_t3076810564 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3153170635_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4160937791_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2U5BU5D_t3030736685* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4160937791(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3178029858 *, KeyValuePair_2U5BU5D_t3030736685*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4160937791_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m598961840_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2_t3076810564  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m598961840(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3178029858 *, KeyValuePair_2_t3076810564 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m598961840_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1416049374_gshared (Dictionary_2_t3178029858 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1416049374(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1416049374_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4077033069_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4077033069(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m4077033069_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m287939940_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m287939940(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m287939940_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3487214385_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3487214385(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3487214385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3310219846_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3310219846(__this, method) ((  int32_t (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_get_Count_m3310219846_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2687478631_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2687478631(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2687478631_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m547599462_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m547599462(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m547599462_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1073619102_gshared (Dictionary_2_t3178029858 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1073619102(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1073619102_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m217928409_gshared (Dictionary_2_t3178029858 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m217928409(__this, ___size0, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m217928409_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1288027925_gshared (Dictionary_2_t3178029858 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1288027925(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1288027925_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3076810564  Dictionary_2_make_pair_m1693764009_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1693764009(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3076810564  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m1693764009_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1353434101_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1353434101(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1353434101_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m2594214929_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2594214929(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m2594214929_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m94369562_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2U5BU5D_t3030736685* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m94369562(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3178029858 *, KeyValuePair_2U5BU5D_t3030736685*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m94369562_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Resize()
extern "C"  void Dictionary_2_Resize_m3195769810_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3195769810(__this, method) ((  void (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_Resize_m3195769810_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m103415785_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m103415785(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3178029858 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m103415785_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Clear()
extern "C"  void Dictionary_2_Clear_m3741988177_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3741988177(__this, method) ((  void (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_Clear_m3741988177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m157578171_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m157578171(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m157578171_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m177828411_gshared (Dictionary_2_t3178029858 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m177828411(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m177828411_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3722077636_gshared (Dictionary_2_t3178029858 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3722077636(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3178029858 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3722077636_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3565870880_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3565870880(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3565870880_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m397089877_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m397089877(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3178029858 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m397089877_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2160779540_gshared (Dictionary_2_t3178029858 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2160779540(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3178029858 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m2160779540_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Keys()
extern "C"  KeyCollection_t509822013 * Dictionary_2_get_Keys_m1062231303_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1062231303(__this, method) ((  KeyCollection_t509822013 * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_get_Keys_m1062231303_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Values()
extern "C"  ValueCollection_t1878635571 * Dictionary_2_get_Values_m1331992163_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1331992163(__this, method) ((  ValueCollection_t1878635571 * (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_get_Values_m1331992163_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m803293008_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m803293008(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m803293008_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m906529068_gshared (Dictionary_2_t3178029858 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m906529068(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t3178029858 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m906529068_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1132405304_gshared (Dictionary_2_t3178029858 * __this, KeyValuePair_2_t3076810564  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1132405304(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3178029858 *, KeyValuePair_2_t3076810564 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1132405304_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
extern "C"  Enumerator_t200385954  Dictionary_2_GetEnumerator_m2704674865_gshared (Dictionary_2_t3178029858 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2704674865(__this, method) ((  Enumerator_t200385954  (*) (Dictionary_2_t3178029858 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2704674865_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m3578013800_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3578013800(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3578013800_gshared)(__this /* static, unused */, ___key0, ___value1, method)
