﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultSaveData
struct ResultSaveData_t3126241828;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultSaveData::.ctor()
extern "C"  void ResultSaveData__ctor_m200428279 (ResultSaveData_t3126241828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
