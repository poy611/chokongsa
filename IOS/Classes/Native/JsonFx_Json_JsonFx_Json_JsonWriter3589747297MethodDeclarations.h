﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonWriter
struct JsonWriter_t3589747297;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// JsonFx.Json.JsonWriterSettings
struct JsonWriterSettings_t323204516;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Enum
struct Enum_t2862688501;
// System.Uri
struct Uri_t1116831938;
// System.Version
struct Version_t763695022;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Enum[]
struct EnumU5BU5D_t3205174168;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriterSettings323204516.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_Version763695022.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"

// System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder)
extern "C"  void JsonWriter__ctor_m2321787662 (JsonWriter_t3589747297 * __this, StringBuilder_t243639308 * ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::.ctor(System.Text.StringBuilder,JsonFx.Json.JsonWriterSettings)
extern "C"  void JsonWriter__ctor_m780966760 (JsonWriter_t3589747297 * __this, StringBuilder_t243639308 * ___output0, JsonWriterSettings_t323204516 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonFx.Json.JsonWriter::Serialize(System.Object)
extern "C"  String_t* JsonWriter_Serialize_m2877040541 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Object)
extern "C"  void JsonWriter_Write_m2686199963 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Object,System.Boolean)
extern "C"  void JsonWriter_Write_m228622722 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, bool ___isProperty1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.DateTime)
extern "C"  void JsonWriter_Write_m1393129343 (JsonWriter_t3589747297 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Guid)
extern "C"  void JsonWriter_Write_m3030605521 (JsonWriter_t3589747297 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Enum)
extern "C"  void JsonWriter_Write_m3028561753 (JsonWriter_t3589747297 * __this, Enum_t2862688501 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.String)
extern "C"  void JsonWriter_Write_m2464089609 (JsonWriter_t3589747297 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Boolean)
extern "C"  void JsonWriter_Write_m3583389520 (JsonWriter_t3589747297 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Byte)
extern "C"  void JsonWriter_Write_m3026117682 (JsonWriter_t3589747297 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.SByte)
extern "C"  void JsonWriter_Write_m4051402109 (JsonWriter_t3589747297 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Int16)
extern "C"  void JsonWriter_Write_m3805530724 (JsonWriter_t3589747297 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt16)
extern "C"  void JsonWriter_Write_m3004616955 (JsonWriter_t3589747297 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Int32)
extern "C"  void JsonWriter_Write_m3805532522 (JsonWriter_t3589747297 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt32)
extern "C"  void JsonWriter_Write_m3004618753 (JsonWriter_t3589747297 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Int64)
extern "C"  void JsonWriter_Write_m3805535467 (JsonWriter_t3589747297 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.UInt64)
extern "C"  void JsonWriter_Write_m3004621698 (JsonWriter_t3589747297 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Single)
extern "C"  void JsonWriter_Write_m2145413298 (JsonWriter_t3589747297 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Double)
extern "C"  void JsonWriter_Write_m1895850569 (JsonWriter_t3589747297 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Decimal)
extern "C"  void JsonWriter_Write_m2142858567 (JsonWriter_t3589747297 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Char)
extern "C"  void JsonWriter_Write_m3026516900 (JsonWriter_t3589747297 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.TimeSpan)
extern "C"  void JsonWriter_Write_m1677925091 (JsonWriter_t3589747297 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Uri)
extern "C"  void JsonWriter_Write_m1068006924 (JsonWriter_t3589747297 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Version)
extern "C"  void JsonWriter_Write_m3887240352 (JsonWriter_t3589747297 * __this, Version_t763695022 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::Write(System.Xml.XmlNode)
extern "C"  void JsonWriter_Write_m2726874998 (JsonWriter_t3589747297 * __this, XmlNode_t856910923 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern "C"  void JsonWriter_WriteArray_m890754409 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteArrayItem(System.Object)
extern "C"  void JsonWriter_WriteArrayItem_m3627136607 (JsonWriter_t3589747297 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObject(System.Collections.IDictionary)
extern "C"  void JsonWriter_WriteObject_m2746452913 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern "C"  void JsonWriter_WriteDictionary_m4272317462 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern "C"  void JsonWriter_WriteObjectProperty_m2713404739 (JsonWriter_t3589747297 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyName(System.String)
extern "C"  void JsonWriter_WriteObjectPropertyName_m1558042410 (JsonWriter_t3589747297 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyValue(System.Object)
extern "C"  void JsonWriter_WriteObjectPropertyValue_m1110441390 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObject(System.Object,System.Type)
extern "C"  void JsonWriter_WriteObject_m3861055119 (JsonWriter_t3589747297 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteArrayItemDelim()
extern "C"  void JsonWriter_WriteArrayItemDelim_m2976019388 (JsonWriter_t3589747297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteObjectPropertyDelim()
extern "C"  void JsonWriter_WriteObjectPropertyDelim_m1543176996 (JsonWriter_t3589747297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::WriteLine()
extern "C"  void JsonWriter_WriteLine_m2362711213 (JsonWriter_t3589747297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern "C"  bool JsonWriter_IsIgnored_m1964658177 (JsonWriter_t3589747297 * __this, Type_t * ___objType0, MemberInfo_t * ___member1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern "C"  bool JsonWriter_IsDefaultValue_m135619988 (JsonWriter_t3589747297 * __this, MemberInfo_t * ___member0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum[] JsonFx.Json.JsonWriter::GetFlagList(System.Type,System.Object)
extern "C"  EnumU5BU5D_t3205174168* JsonWriter_GetFlagList_m4094172542 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonWriter::InvalidIeee754(System.Decimal)
extern "C"  bool JsonWriter_InvalidIeee754_m1092164553 (JsonWriter_t3589747297 * __this, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m2408474819 (JsonWriter_t3589747297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
