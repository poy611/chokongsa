﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2413175471(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4056431401 *, String_t*, MultiplayerParticipant_t3337232325 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Key()
#define KeyValuePair_2_get_Key_m2544832473(__this, method) ((  String_t* (*) (KeyValuePair_2_t4056431401 *, const MethodInfo*))KeyValuePair_2_get_Key_m2940899609_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m426509850(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4056431401 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Value()
#define KeyValuePair_2_get_Value_m2201750589(__this, method) ((  MultiplayerParticipant_t3337232325 * (*) (KeyValuePair_2_t4056431401 *, const MethodInfo*))KeyValuePair_2_get_Value_m4250204908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3452091674(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4056431401 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ToString()
#define KeyValuePair_2_ToString_m2278992942(__this, method) ((  String_t* (*) (KeyValuePair_2_t4056431401 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
