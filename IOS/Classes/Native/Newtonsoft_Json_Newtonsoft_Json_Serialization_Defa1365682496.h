﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Reflection.MemberInfo
struct MemberInfo_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t1365682496  : public Il2CppObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0::getExtensionDataDictionary
	Func_2_t184564025 * ___getExtensionDataDictionary_0;
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0::member
	MemberInfo_t * ___member_1;

public:
	inline static int32_t get_offset_of_getExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t1365682496, ___getExtensionDataDictionary_0)); }
	inline Func_2_t184564025 * get_getExtensionDataDictionary_0() const { return ___getExtensionDataDictionary_0; }
	inline Func_2_t184564025 ** get_address_of_getExtensionDataDictionary_0() { return &___getExtensionDataDictionary_0; }
	inline void set_getExtensionDataDictionary_0(Func_2_t184564025 * value)
	{
		___getExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&___getExtensionDataDictionary_0, value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t1365682496, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier(&___member_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
