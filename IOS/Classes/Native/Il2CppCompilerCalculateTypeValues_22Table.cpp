﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCach879908455.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry1336615025.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup1338576510.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup1392439483.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer1645739142.h"
#include "UnityEngine_UnityEngine_GUILayoutOption331591504.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type957706982.h"
#include "UnityEngine_UnityEngine_GUISettings4075193652.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat4002872878.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "UnityEngine_UnityEngine_ImagePosition2091083802.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_TextClipping3924524051.h"
#include "UnityEngine_UnityEngine_ExitGUIException2922874134.h"
#include "UnityEngine_UnityEngine_FocusType2235102504.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349.h"
#include "UnityEngine_UnityEngine_GUIClip3370872417.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute2337826580.h"
#include "UnityEngine_UnityEngine_TextEditor319394238.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1841135060.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType1769114053.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments1587375252.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec4294668057.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest1890284502.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3819726735.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest_947498394.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils4281919736.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler4062689071.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRaw16481323.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4125766536.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4019763368.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2552687013.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandlerA946683804.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1243312272.h"
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute1825429660.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2494346367.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttri3576128422.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe3251508852.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine4212589506.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent62111112.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_AddComponentMenu813859935.h"
#include "UnityEngine_UnityEngine_ContextMenu1199215204.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3132250205.h"
#include "UnityEngine_UnityEngine_HideInInspector2952493798.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_WritableAttribute2171443922.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1696890055.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "UnityEngine_UnityEngine_LightType1292142182.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_FilterMode1625068031.h"
#include "UnityEngine_UnityEngine_TextureWrapMode1899634046.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy3609427819.h"
#include "UnityEngine_UnityEngine_GUIStateObjects1371753044.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Social3197796273.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_ActivePlat2714626623.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085.h"
#include "UnityEngine_UnityEngine_TooltipAttribute1877437789.h"
#include "UnityEngine_UnityEngine_SpaceAttribute1515135354.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute641061976.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987.h"
#include "UnityEngine_UnityEngine_StackTraceUtility4217621253.h"
#include "UnityEngine_UnityEngine_UnityException3473321374.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (LayoutCache_t879908455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[3] = 
{
	LayoutCache_t879908455::get_offset_of_topLevel_0(),
	LayoutCache_t879908455::get_offset_of_layoutGroups_1(),
	LayoutCache_t879908455::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (GUILayoutEntry_t1336615025), -1, sizeof(GUILayoutEntry_t1336615025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[10] = 
{
	GUILayoutEntry_t1336615025::get_offset_of_minWidth_0(),
	GUILayoutEntry_t1336615025::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t1336615025::get_offset_of_minHeight_2(),
	GUILayoutEntry_t1336615025::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t1336615025::get_offset_of_rect_4(),
	GUILayoutEntry_t1336615025::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t1336615025::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t1336615025::get_offset_of_m_Style_7(),
	GUILayoutEntry_t1336615025_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t1336615025_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (GUILayoutGroup_t1338576510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[17] = 
{
	GUILayoutGroup_t1338576510::get_offset_of_entries_10(),
	GUILayoutGroup_t1338576510::get_offset_of_isVertical_11(),
	GUILayoutGroup_t1338576510::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t1338576510::get_offset_of_spacing_13(),
	GUILayoutGroup_t1338576510::get_offset_of_sameSize_14(),
	GUILayoutGroup_t1338576510::get_offset_of_isWindow_15(),
	GUILayoutGroup_t1338576510::get_offset_of_windowID_16(),
	GUILayoutGroup_t1338576510::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t1338576510::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t1338576510::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t1338576510::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t1338576510::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t1338576510::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t1338576510::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (GUIScrollGroup_t1392439483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[12] = 
{
	GUIScrollGroup_t1392439483::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t1392439483::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t1392439483::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t1392439483::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t1392439483::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t1392439483::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t1392439483::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t1392439483::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t1392439483::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t1392439483::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (GUIWordWrapSizer_t1645739142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[3] = 
{
	GUIWordWrapSizer_t1645739142::get_offset_of_m_Content_10(),
	GUIWordWrapSizer_t1645739142::get_offset_of_m_ForcedMinHeight_11(),
	GUIWordWrapSizer_t1645739142::get_offset_of_m_ForcedMaxHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (GUILayoutOption_t331591504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[2] = 
{
	GUILayoutOption_t331591504::get_offset_of_type_0(),
	GUILayoutOption_t331591504::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (Type_t957706982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2206[15] = 
{
	Type_t957706982::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (GUISettings_t4075193652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[5] = 
{
	GUISettings_t4075193652::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t4075193652::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t4075193652::get_offset_of_m_CursorColor_2(),
	GUISettings_t4075193652::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t4075193652::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (GUISkin_t3371348110), -1, sizeof(GUISkin_t3371348110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2208[27] = 
{
	GUISkin_t3371348110::get_offset_of_m_Font_2(),
	GUISkin_t3371348110::get_offset_of_m_box_3(),
	GUISkin_t3371348110::get_offset_of_m_button_4(),
	GUISkin_t3371348110::get_offset_of_m_toggle_5(),
	GUISkin_t3371348110::get_offset_of_m_label_6(),
	GUISkin_t3371348110::get_offset_of_m_textField_7(),
	GUISkin_t3371348110::get_offset_of_m_textArea_8(),
	GUISkin_t3371348110::get_offset_of_m_window_9(),
	GUISkin_t3371348110::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t3371348110::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t3371348110::get_offset_of_m_verticalSlider_12(),
	GUISkin_t3371348110::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t3371348110::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t3371348110::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t3371348110::get_offset_of_m_ScrollView_22(),
	GUISkin_t3371348110::get_offset_of_m_CustomStyles_23(),
	GUISkin_t3371348110::get_offset_of_m_Settings_24(),
	GUISkin_t3371348110_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t3371348110::get_offset_of_m_Styles_26(),
	GUISkin_t3371348110_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t3371348110_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (SkinChangedDelegate_t4002872878), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (GUIStyleState_t1997423985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	GUIStyleState_t1997423985::get_offset_of_m_Ptr_0(),
	GUIStyleState_t1997423985::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t1997423985::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (FontStyle_t3350479768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	FontStyle_t3350479768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (ImagePosition_t2091083802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	ImagePosition_t2091083802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (GUIStyle_t2990928826), -1, sizeof(GUIStyle_t2990928826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2213[16] = 
{
	GUIStyle_t2990928826::get_offset_of_m_Ptr_0(),
	GUIStyle_t2990928826::get_offset_of_m_Normal_1(),
	GUIStyle_t2990928826::get_offset_of_m_Hover_2(),
	GUIStyle_t2990928826::get_offset_of_m_Active_3(),
	GUIStyle_t2990928826::get_offset_of_m_Focused_4(),
	GUIStyle_t2990928826::get_offset_of_m_OnNormal_5(),
	GUIStyle_t2990928826::get_offset_of_m_OnHover_6(),
	GUIStyle_t2990928826::get_offset_of_m_OnActive_7(),
	GUIStyle_t2990928826::get_offset_of_m_OnFocused_8(),
	GUIStyle_t2990928826::get_offset_of_m_Border_9(),
	GUIStyle_t2990928826::get_offset_of_m_Padding_10(),
	GUIStyle_t2990928826::get_offset_of_m_Margin_11(),
	GUIStyle_t2990928826::get_offset_of_m_Overflow_12(),
	GUIStyle_t2990928826::get_offset_of_m_FontInternal_13(),
	GUIStyle_t2990928826_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t2990928826_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (TextClipping_t3924524051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	TextClipping_t3924524051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (ExitGUIException_t2922874134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (FocusType_t2235102504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[4] = 
{
	FocusType_t2235102504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (GUIUtility_t1028319349), -1, sizeof(GUIUtility_t1028319349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2217[5] = 
{
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_EditorScreenPointOffset_2(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_s_HasKeyboardFocus_3(),
	GUIUtility_t1028319349_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (GUIClip_t3370872417), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (SliderState_t1233388262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	SliderState_t1233388262::get_offset_of_dragStartPos_0(),
	SliderState_t1233388262::get_offset_of_dragStartValue_1(),
	SliderState_t1233388262::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (SliderHandler_t783692703)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[9] = 
{
	SliderHandler_t783692703::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_currentValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_start_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_end_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_slider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_thumb_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_horiz_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SliderHandler_t783692703::get_offset_of_id_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (GUITargetAttribute_t2337826580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[1] = 
{
	GUITargetAttribute_t2337826580::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (TextEditor_t319394238), -1, sizeof(TextEditor_t319394238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2222[24] = 
{
	TextEditor_t319394238::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t319394238::get_offset_of_controlID_1(),
	TextEditor_t319394238::get_offset_of_style_2(),
	TextEditor_t319394238::get_offset_of_multiline_3(),
	TextEditor_t319394238::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t319394238::get_offset_of_isPasswordField_5(),
	TextEditor_t319394238::get_offset_of_m_HasFocus_6(),
	TextEditor_t319394238::get_offset_of_scrollOffset_7(),
	TextEditor_t319394238::get_offset_of_m_Content_8(),
	TextEditor_t319394238::get_offset_of_m_Position_9(),
	TextEditor_t319394238::get_offset_of_m_CursorIndex_10(),
	TextEditor_t319394238::get_offset_of_m_SelectIndex_11(),
	TextEditor_t319394238::get_offset_of_m_RevealCursor_12(),
	TextEditor_t319394238::get_offset_of_graphicalCursorPos_13(),
	TextEditor_t319394238::get_offset_of_graphicalSelectCursorPos_14(),
	TextEditor_t319394238::get_offset_of_m_MouseDragSelectsWholeWords_15(),
	TextEditor_t319394238::get_offset_of_m_DblClickInitPos_16(),
	TextEditor_t319394238::get_offset_of_m_DblClickSnap_17(),
	TextEditor_t319394238::get_offset_of_m_bJustSelected_18(),
	TextEditor_t319394238::get_offset_of_m_iAltCursorPos_19(),
	TextEditor_t319394238::get_offset_of_oldText_20(),
	TextEditor_t319394238::get_offset_of_oldPos_21(),
	TextEditor_t319394238::get_offset_of_oldSelectPos_22(),
	TextEditor_t319394238_StaticFields::get_offset_of_s_Keyactions_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (DblClickSnapping_t1841135060)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2223[3] = 
{
	DblClickSnapping_t1841135060::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (CharacterType_t1769114053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[5] = 
{
	CharacterType_t1769114053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (TextEditOp_t4145961110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[51] = 
{
	TextEditOp_t4145961110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (Internal_DrawArguments_t1587375252)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t1587375252_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[6] = 
{
	Internal_DrawArguments_t1587375252::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t1587375252::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (Internal_DrawWithTextSelectionArguments_t4294668057)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t4294668057_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[11] = 
{
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t4294668057::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (UnityWebRequest_t1890284502), sizeof(UnityWebRequest_t1890284502_marshaled_pinvoke), sizeof(UnityWebRequest_t1890284502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2228[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	UnityWebRequest_t1890284502::get_offset_of_m_Ptr_6(),
	UnityWebRequest_t1890284502_StaticFields::get_offset_of_domainRegex_7(),
	UnityWebRequest_t1890284502_StaticFields::get_offset_of_forbiddenHeaderKeys_8(),
	UnityWebRequest_t1890284502::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(),
	UnityWebRequest_t1890284502::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(),
	UnityWebRequest_t1890284502_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (UnityWebRequestMethod_t3819726735)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[6] = 
{
	UnityWebRequestMethod_t3819726735::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (UnityWebRequestError_t947498394)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2230[30] = 
{
	UnityWebRequestError_t947498394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (WebRequestUtils_t4281919736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (UploadHandler_t4062689071), sizeof(UploadHandler_t4062689071_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[1] = 
{
	UploadHandler_t4062689071::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (UploadHandlerRaw_t16481323), sizeof(UploadHandlerRaw_t16481323_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (DownloadHandler_t4125766536), sizeof(DownloadHandler_t4125766536_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[1] = 
{
	DownloadHandler_t4125766536::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (DownloadHandlerBuffer_t4019763368), sizeof(DownloadHandlerBuffer_t4019763368_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (DownloadHandlerTexture_t2552687013), sizeof(DownloadHandlerTexture_t2552687013_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (DownloadHandlerAssetBundle_t946683804), sizeof(DownloadHandlerAssetBundle_t946683804_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (DownloadHandlerAudioClip_t1243312272), sizeof(DownloadHandlerAudioClip_t1243312272_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (MonoPInvokeCallbackAttribute_t1825429660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (WrapperlessIcall_t2494346367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (IL2CPPStructAlignmentAttribute_t3576128422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[1] = 
{
	IL2CPPStructAlignmentAttribute_t3576128422::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ThreadAndSerializationSafe_t3251508852), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (AttributeHelperEngine_t4212589506), -1, sizeof(AttributeHelperEngine_t4212589506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2244[3] = 
{
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t4212589506_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (DisallowMultipleComponent_t62111112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (RequireComponent_t1687166108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	RequireComponent_t1687166108::get_offset_of_m_Type0_0(),
	RequireComponent_t1687166108::get_offset_of_m_Type1_1(),
	RequireComponent_t1687166108::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (AddComponentMenu_t813859935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[2] = 
{
	AddComponentMenu_t813859935::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t813859935::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (ContextMenu_t1199215204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[1] = 
{
	ContextMenu_t1199215204::get_offset_of_m_ItemName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (ExecuteInEditMode_t3132250205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (HideInInspector_t2952493798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (Color_t4194546905)+ sizeof (Il2CppObject), sizeof(Color_t4194546905_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	Color_t4194546905::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t4194546905::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (SetupCoroutine_t1459791391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (WritableAttribute_t2171443922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (AssemblyIsEditorAssembly_t1696890055), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (GcUserProfileData_t657441114)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[4] = 
{
	GcUserProfileData_t657441114::get_offset_of_userName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_userID_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_isFriend_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcUserProfileData_t657441114::get_offset_of_image_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (GcAchievementDescriptionData_t2242891083)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[7] = 
{
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Title_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Image_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_AchievedDescription_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_UnachievedDescription_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Hidden_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementDescriptionData_t2242891083::get_offset_of_m_Points_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GcAchievementData_t3481375915)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t3481375915_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[5] = 
{
	GcAchievementData_t3481375915::get_offset_of_m_Identifier_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_PercentCompleted_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_Completed_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_Hidden_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcAchievementData_t3481375915::get_offset_of_m_LastReportedDate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (GcScoreData_t2181296590)+ sizeof (Il2CppObject), sizeof(GcScoreData_t2181296590_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2259[7] = 
{
	GcScoreData_t2181296590::get_offset_of_m_Category_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_ValueLow_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_ValueHigh_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_Date_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_FormattedValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_PlayerID_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GcScoreData_t2181296590::get_offset_of_m_Rank_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (LightType_t1292142182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[5] = 
{
	LightType_t1292142182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (CameraClearFlags_t2093155523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[6] = 
{
	CameraClearFlags_t2093155523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (FilterMode_t1625068031)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2262[4] = 
{
	FilterMode_t1625068031::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (TextureWrapMode_t1899634046)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	TextureWrapMode_t1899634046::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (TextureFormat_t4189619560)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2264[47] = 
{
	TextureFormat_t4189619560::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (CompareFunction_t2661816155)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[10] = 
{
	CompareFunction_t2661816155::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ColorWriteMask_t3214369688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2266[6] = 
{
	ColorWriteMask_t3214369688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (StencilOp_t3324967291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2267[9] = 
{
	StencilOp_t3324967291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (GraphicsDeviceType_t3609427819)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2268[17] = 
{
	GraphicsDeviceType_t3609427819::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (GUIStateObjects_t1371753044), -1, sizeof(GUIStateObjects_t1371753044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2269[1] = 
{
	GUIStateObjects_t1371753044_StaticFields::get_offset_of_s_StateCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (LocalUser_t1307362368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	LocalUser_t1307362368::get_offset_of_m_Friends_5(),
	LocalUser_t1307362368::get_offset_of_m_Authenticated_6(),
	LocalUser_t1307362368::get_offset_of_m_Underage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (UserProfile_t2280656072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[5] = 
{
	UserProfile_t2280656072::get_offset_of_m_UserName_0(),
	UserProfile_t2280656072::get_offset_of_m_ID_1(),
	UserProfile_t2280656072::get_offset_of_m_IsFriend_2(),
	UserProfile_t2280656072::get_offset_of_m_State_3(),
	UserProfile_t2280656072::get_offset_of_m_Image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (Achievement_t344600729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[5] = 
{
	Achievement_t344600729::get_offset_of_m_Completed_0(),
	Achievement_t344600729::get_offset_of_m_Hidden_1(),
	Achievement_t344600729::get_offset_of_m_LastReportedDate_2(),
	Achievement_t344600729::get_offset_of_U3CidU3Ek__BackingField_3(),
	Achievement_t344600729::get_offset_of_U3CpercentCompletedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (AchievementDescription_t2116066607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[7] = 
{
	AchievementDescription_t2116066607::get_offset_of_m_Title_0(),
	AchievementDescription_t2116066607::get_offset_of_m_Image_1(),
	AchievementDescription_t2116066607::get_offset_of_m_AchievedDescription_2(),
	AchievementDescription_t2116066607::get_offset_of_m_UnachievedDescription_3(),
	AchievementDescription_t2116066607::get_offset_of_m_Hidden_4(),
	AchievementDescription_t2116066607::get_offset_of_m_Points_5(),
	AchievementDescription_t2116066607::get_offset_of_U3CidU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (Score_t3396031228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[6] = 
{
	Score_t3396031228::get_offset_of_m_Date_0(),
	Score_t3396031228::get_offset_of_m_FormattedValue_1(),
	Score_t3396031228::get_offset_of_m_UserID_2(),
	Score_t3396031228::get_offset_of_m_Rank_3(),
	Score_t3396031228::get_offset_of_U3CleaderboardIDU3Ek__BackingField_4(),
	Score_t3396031228::get_offset_of_U3CvalueU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (Leaderboard_t1185876199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[10] = 
{
	Leaderboard_t1185876199::get_offset_of_m_Loading_0(),
	Leaderboard_t1185876199::get_offset_of_m_LocalUserScore_1(),
	Leaderboard_t1185876199::get_offset_of_m_MaxRange_2(),
	Leaderboard_t1185876199::get_offset_of_m_Scores_3(),
	Leaderboard_t1185876199::get_offset_of_m_Title_4(),
	Leaderboard_t1185876199::get_offset_of_m_UserIDs_5(),
	Leaderboard_t1185876199::get_offset_of_U3CidU3Ek__BackingField_6(),
	Leaderboard_t1185876199::get_offset_of_U3CuserScopeU3Ek__BackingField_7(),
	Leaderboard_t1185876199::get_offset_of_U3CrangeU3Ek__BackingField_8(),
	Leaderboard_t1185876199::get_offset_of_U3CtimeScopeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (SendMouseEvents_t3965481900), -1, sizeof(SendMouseEvents_t3965481900_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2276[5] = 
{
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_s_MouseUsed_0(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_LastHit_1(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_MouseDownHit_2(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_CurrentHit_3(),
	SendMouseEvents_t3965481900_StaticFields::get_offset_of_m_Cameras_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (HitInfo_t3209134097)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[2] = 
{
	HitInfo_t3209134097::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitInfo_t3209134097::get_offset_of_camera_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (Social_t3197796273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (ActivePlatform_t2714626623), -1, sizeof(ActivePlatform_t2714626623_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2279[1] = 
{
	ActivePlatform_t2714626623_StaticFields::get_offset_of__active_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (UserState_t1609153288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2282[6] = 
{
	UserState_t1609153288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (UserScope_t1608660171)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[3] = 
{
	UserScope_t1608660171::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (TimeScope_t1305796361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2288[4] = 
{
	TimeScope_t1305796361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (Range_t1533311935)+ sizeof (Il2CppObject), sizeof(Range_t1533311935_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[2] = 
{
	Range_t1533311935::get_offset_of_from_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Range_t1533311935::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (PropertyAttribute_t3531521085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (TooltipAttribute_t1877437789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[1] = 
{
	TooltipAttribute_t1877437789::get_offset_of_tooltip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (SpaceAttribute_t1515135354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[1] = 
{
	SpaceAttribute_t1515135354::get_offset_of_height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (RangeAttribute_t912008995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[2] = 
{
	RangeAttribute_t912008995::get_offset_of_min_0(),
	RangeAttribute_t912008995::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (TextAreaAttribute_t641061976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	TextAreaAttribute_t641061976::get_offset_of_minLines_0(),
	TextAreaAttribute_t641061976::get_offset_of_maxLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (SelectionBaseAttribute_t204085987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (StackTraceUtility_t4217621253), -1, sizeof(StackTraceUtility_t4217621253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2297[1] = 
{
	StackTraceUtility_t4217621253_StaticFields::get_offset_of_projectFolder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (UnityException_t3473321374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[2] = 
{
	0,
	UnityException_t3473321374::get_offset_of_unityStackTrace_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (SharedBetweenAnimatorsAttribute_t1486273289), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
