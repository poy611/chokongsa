﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct BaseReferenceHolder_t2237584300;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::.ctor(System.IntPtr)
extern "C"  void BaseReferenceHolder__ctor_m2809683036 (BaseReferenceHolder_t2237584300 * __this, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::.cctor()
extern "C"  void BaseReferenceHolder__cctor_m2916274101 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.BaseReferenceHolder::IsDisposed()
extern "C"  bool BaseReferenceHolder_IsDisposed_m4043367279 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.BaseReferenceHolder::SelfPtr()
extern "C"  HandleRef_t1780819301  BaseReferenceHolder_SelfPtr_m1052503806 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Finalize()
extern "C"  void BaseReferenceHolder_Finalize_m1761231466 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose()
extern "C"  void BaseReferenceHolder_Dispose_m2168547637 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.BaseReferenceHolder::AsPointer()
extern "C"  IntPtr_t BaseReferenceHolder_AsPointer_m1925325420 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose(System.Boolean)
extern "C"  void BaseReferenceHolder_Dispose_m1458134508 (BaseReferenceHolder_t2237584300 * __this, bool ___fromFinalizer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::ReferToMe()
extern "C"  void BaseReferenceHolder_ReferToMe_m4130110185 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::ForgetMe()
extern "C"  void BaseReferenceHolder_ForgetMe_m3021182193 (BaseReferenceHolder_t2237584300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
