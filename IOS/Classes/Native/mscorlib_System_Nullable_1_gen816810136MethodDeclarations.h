﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen816810136.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting732683613.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.Formatting>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2713022200_gshared (Nullable_1_t816810136 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2713022200(__this, ___value0, method) ((  void (*) (Nullable_1_t816810136 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2713022200_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Formatting>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1693548595_gshared (Nullable_1_t816810136 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1693548595(__this, method) ((  bool (*) (Nullable_1_t816810136 *, const MethodInfo*))Nullable_1_get_HasValue_m1693548595_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Formatting>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4253124896_gshared (Nullable_1_t816810136 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4253124896(__this, method) ((  int32_t (*) (Nullable_1_t816810136 *, const MethodInfo*))Nullable_1_get_Value_m4253124896_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Formatting>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1918707054_gshared (Nullable_1_t816810136 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1918707054(__this, ___other0, method) ((  bool (*) (Nullable_1_t816810136 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1918707054_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Formatting>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m482481681_gshared (Nullable_1_t816810136 * __this, Nullable_1_t816810136  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m482481681(__this, ___other0, method) ((  bool (*) (Nullable_1_t816810136 *, Nullable_1_t816810136 , const MethodInfo*))Nullable_1_Equals_m482481681_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.Formatting>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3814106950_gshared (Nullable_1_t816810136 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3814106950(__this, method) ((  int32_t (*) (Nullable_1_t816810136 *, const MethodInfo*))Nullable_1_GetHashCode_m3814106950_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Formatting>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3350955244_gshared (Nullable_1_t816810136 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3350955244(__this, method) ((  int32_t (*) (Nullable_1_t816810136 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3350955244_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Formatting>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m650752835_gshared (Nullable_1_t816810136 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m650752835(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t816810136 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m650752835_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.Formatting>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2440417810_gshared (Nullable_1_t816810136 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2440417810(__this, method) ((  String_t* (*) (Nullable_1_t816810136 *, const MethodInfo*))Nullable_1_ToString_m2440417810_gshared)(__this, method)
