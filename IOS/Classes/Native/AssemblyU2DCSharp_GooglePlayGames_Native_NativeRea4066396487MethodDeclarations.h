﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E
struct U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E::.ctor()
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey6E__ctor_m1558619444 (U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
