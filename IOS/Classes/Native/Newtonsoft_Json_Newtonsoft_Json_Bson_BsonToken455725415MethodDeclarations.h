﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonToken455725415.h"

// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::get_Parent()
extern "C"  BsonToken_t455725415 * BsonToken_get_Parent_m3782528132 (BsonToken_t455725415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonToken::set_Parent(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonToken_set_Parent_m1584327877 (BsonToken_t455725415 * __this, BsonToken_t455725415 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Bson.BsonToken::get_CalculatedSize()
extern "C"  int32_t BsonToken_get_CalculatedSize_m3860150762 (BsonToken_t455725415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonToken::set_CalculatedSize(System.Int32)
extern "C"  void BsonToken_set_CalculatedSize_m518806315 (BsonToken_t455725415 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonToken::.ctor()
extern "C"  void BsonToken__ctor_m1208023622 (BsonToken_t455725415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
