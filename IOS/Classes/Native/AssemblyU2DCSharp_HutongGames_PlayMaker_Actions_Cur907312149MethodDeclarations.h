﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveFloat
struct CurveFloat_t907312149;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveFloat::.ctor()
extern "C"  void CurveFloat__ctor_m2136547313 (CurveFloat_t907312149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::Reset()
extern "C"  void CurveFloat_Reset_m4077947550 (CurveFloat_t907312149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnEnter()
extern "C"  void CurveFloat_OnEnter_m3914296200 (CurveFloat_t907312149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnExit()
extern "C"  void CurveFloat_OnExit_m3321775888 (CurveFloat_t907312149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnUpdate()
extern "C"  void CurveFloat_OnUpdate_m217657275 (CurveFloat_t907312149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
