﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qua508608383.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionBaseAction
struct  QuaternionBaseAction_t1884049229  : public FsmStateAction_t2366529033
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.QuaternionBaseAction::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.Actions.QuaternionBaseAction/everyFrameOptions HutongGames.PlayMaker.Actions.QuaternionBaseAction::everyFrameOption
	int32_t ___everyFrameOption_12;

public:
	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(QuaternionBaseAction_t1884049229, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_everyFrameOption_12() { return static_cast<int32_t>(offsetof(QuaternionBaseAction_t1884049229, ___everyFrameOption_12)); }
	inline int32_t get_everyFrameOption_12() const { return ___everyFrameOption_12; }
	inline int32_t* get_address_of_everyFrameOption_12() { return &___everyFrameOption_12; }
	inline void set_everyFrameOption_12(int32_t value)
	{
		___everyFrameOption_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
