﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePick
struct MousePick_t696801716;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePick::.ctor()
extern "C"  void MousePick__ctor_m2294044162 (MousePick_t696801716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::Reset()
extern "C"  void MousePick_Reset_m4235444399 (MousePick_t696801716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnEnter()
extern "C"  void MousePick_OnEnter_m649945433 (MousePick_t696801716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnUpdate()
extern "C"  void MousePick_OnUpdate_m2101998602 (MousePick_t696801716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::DoMousePick()
extern "C"  void MousePick_DoMousePick_m442803003 (MousePick_t696801716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
