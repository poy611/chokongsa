﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3280454238.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4101758999_gshared (InternalEnumerator_1_t3280454238 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4101758999(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3280454238 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4101758999_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089(__this, method) ((  void (*) (InternalEnumerator_1_t3280454238 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3280454238 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m796347054_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m796347054(__this, method) ((  void (*) (InternalEnumerator_1_t3280454238 *, const MethodInfo*))InternalEnumerator_1_Dispose_m796347054_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3714593429_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3714593429(__this, method) ((  bool (*) (InternalEnumerator_1_t3280454238 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3714593429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::get_Current()
extern "C"  KeyValuePair_2_t203144266  InternalEnumerator_1_get_Current_m170531934_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m170531934(__this, method) ((  KeyValuePair_2_t203144266  (*) (InternalEnumerator_1_t3280454238 *, const MethodInfo*))InternalEnumerator_1_get_Current_m170531934_gshared)(__this, method)
