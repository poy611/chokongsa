﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MoveObject
struct MoveObject_t1066530776;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MoveObject::.ctor()
extern "C"  void MoveObject__ctor_m801582990 (MoveObject_t1066530776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::Reset()
extern "C"  void MoveObject_Reset_m2742983227 (MoveObject_t1066530776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::OnEnter()
extern "C"  void MoveObject_OnEnter_m913836005 (MoveObject_t1066530776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::OnUpdate()
extern "C"  void MoveObject_OnUpdate_m1692671742 (MoveObject_t1066530776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
