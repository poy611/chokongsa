﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetSkybox
struct  SetSkybox_t691862362  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetSkybox::skybox
	FsmMaterial_t924399665 * ___skybox_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetSkybox::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_skybox_11() { return static_cast<int32_t>(offsetof(SetSkybox_t691862362, ___skybox_11)); }
	inline FsmMaterial_t924399665 * get_skybox_11() const { return ___skybox_11; }
	inline FsmMaterial_t924399665 ** get_address_of_skybox_11() { return &___skybox_11; }
	inline void set_skybox_11(FsmMaterial_t924399665 * value)
	{
		___skybox_11 = value;
		Il2CppCodeGenWriteBarrier(&___skybox_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetSkybox_t691862362, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
