﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyObjects
struct DestroyObjects_t613471906;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::.ctor()
extern "C"  void DestroyObjects__ctor_m4027353092 (DestroyObjects_t613471906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::Reset()
extern "C"  void DestroyObjects_Reset_m1673786033 (DestroyObjects_t613471906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::OnEnter()
extern "C"  void DestroyObjects_OnEnter_m4207483611 (DestroyObjects_t613471906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
