﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenRotateTo
struct  iTweenRotateTo_t1141197216  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenRotateTo::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenRotateTo::id
	FsmString_t952858651 * ___id_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenRotateTo::transformRotation
	FsmGameObject_t1697147867 * ___transformRotation_21;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenRotateTo::vectorRotation
	FsmVector3_t533912882 * ___vectorRotation_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateTo::time
	FsmFloat_t2134102846 * ___time_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateTo::delay
	FsmFloat_t2134102846 * ___delay_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateTo::speed
	FsmFloat_t2134102846 * ___speed_25;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenRotateTo::easeType
	int32_t ___easeType_26;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenRotateTo::loopType
	int32_t ___loopType_27;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenRotateTo::space
	int32_t ___space_28;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___gameObject_19)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_19, value);
	}

	inline static int32_t get_offset_of_id_20() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___id_20)); }
	inline FsmString_t952858651 * get_id_20() const { return ___id_20; }
	inline FsmString_t952858651 ** get_address_of_id_20() { return &___id_20; }
	inline void set_id_20(FsmString_t952858651 * value)
	{
		___id_20 = value;
		Il2CppCodeGenWriteBarrier(&___id_20, value);
	}

	inline static int32_t get_offset_of_transformRotation_21() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___transformRotation_21)); }
	inline FsmGameObject_t1697147867 * get_transformRotation_21() const { return ___transformRotation_21; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformRotation_21() { return &___transformRotation_21; }
	inline void set_transformRotation_21(FsmGameObject_t1697147867 * value)
	{
		___transformRotation_21 = value;
		Il2CppCodeGenWriteBarrier(&___transformRotation_21, value);
	}

	inline static int32_t get_offset_of_vectorRotation_22() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___vectorRotation_22)); }
	inline FsmVector3_t533912882 * get_vectorRotation_22() const { return ___vectorRotation_22; }
	inline FsmVector3_t533912882 ** get_address_of_vectorRotation_22() { return &___vectorRotation_22; }
	inline void set_vectorRotation_22(FsmVector3_t533912882 * value)
	{
		___vectorRotation_22 = value;
		Il2CppCodeGenWriteBarrier(&___vectorRotation_22, value);
	}

	inline static int32_t get_offset_of_time_23() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___time_23)); }
	inline FsmFloat_t2134102846 * get_time_23() const { return ___time_23; }
	inline FsmFloat_t2134102846 ** get_address_of_time_23() { return &___time_23; }
	inline void set_time_23(FsmFloat_t2134102846 * value)
	{
		___time_23 = value;
		Il2CppCodeGenWriteBarrier(&___time_23, value);
	}

	inline static int32_t get_offset_of_delay_24() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___delay_24)); }
	inline FsmFloat_t2134102846 * get_delay_24() const { return ___delay_24; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_24() { return &___delay_24; }
	inline void set_delay_24(FsmFloat_t2134102846 * value)
	{
		___delay_24 = value;
		Il2CppCodeGenWriteBarrier(&___delay_24, value);
	}

	inline static int32_t get_offset_of_speed_25() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___speed_25)); }
	inline FsmFloat_t2134102846 * get_speed_25() const { return ___speed_25; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_25() { return &___speed_25; }
	inline void set_speed_25(FsmFloat_t2134102846 * value)
	{
		___speed_25 = value;
		Il2CppCodeGenWriteBarrier(&___speed_25, value);
	}

	inline static int32_t get_offset_of_easeType_26() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___easeType_26)); }
	inline int32_t get_easeType_26() const { return ___easeType_26; }
	inline int32_t* get_address_of_easeType_26() { return &___easeType_26; }
	inline void set_easeType_26(int32_t value)
	{
		___easeType_26 = value;
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}

	inline static int32_t get_offset_of_space_28() { return static_cast<int32_t>(offsetof(iTweenRotateTo_t1141197216, ___space_28)); }
	inline int32_t get_space_28() const { return ___space_28; }
	inline int32_t* get_address_of_space_28() { return &___space_28; }
	inline void set_space_28(int32_t value)
	{
		___space_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
