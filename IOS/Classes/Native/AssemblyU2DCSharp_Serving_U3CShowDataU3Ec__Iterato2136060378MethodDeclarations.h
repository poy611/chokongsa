﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Serving/<ShowData>c__Iterator29
struct U3CShowDataU3Ec__Iterator29_t2136060378;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Serving/<ShowData>c__Iterator29::.ctor()
extern "C"  void U3CShowDataU3Ec__Iterator29__ctor_m4016880337 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<ShowData>c__Iterator29::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowDataU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m106551585 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<ShowData>c__Iterator29::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowDataU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m2877046453 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Serving/<ShowData>c__Iterator29::MoveNext()
extern "C"  bool U3CShowDataU3Ec__Iterator29_MoveNext_m1926243715 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<ShowData>c__Iterator29::Dispose()
extern "C"  void U3CShowDataU3Ec__Iterator29_Dispose_m3239655118 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<ShowData>c__Iterator29::Reset()
extern "C"  void U3CShowDataU3Ec__Iterator29_Reset_m1663313278 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
