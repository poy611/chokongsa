﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Variation
struct ScoreManager_Variation_t1665153935;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"

// System.Void ScoreManager_Variation::.ctor()
extern "C"  void ScoreManager_Variation__ctor_m1171556844 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::Start()
extern "C"  void ScoreManager_Variation_Start_m118694636 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Variation::CheckIngredient()
extern "C"  Il2CppObject * ScoreManager_Variation_CheckIngredient_m4221245755 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::buttonOkClickSound()
extern "C"  void ScoreManager_Variation_buttonOkClickSound_m2844652525 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::startVariation()
extern "C"  void ScoreManager_Variation_startVariation_m258681993 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::RestartThisGame()
extern "C"  void ScoreManager_Variation_RestartThisGame_m551128809 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::ButtonClickEvent(PlayMakerFSM)
extern "C"  void ScoreManager_Variation_ButtonClickEvent_m3233447116 (ScoreManager_Variation_t1665153935 * __this, PlayMakerFSM_t3799847376 * ____myFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::isRightSelect(System.Int32)
extern "C"  void ScoreManager_Variation_isRightSelect_m275584681 (ScoreManager_Variation_t1665153935 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::TimeCheckerStart()
extern "C"  void ScoreManager_Variation_TimeCheckerStart_m1950168562 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Variation::TimerCheck()
extern "C"  Il2CppObject * ScoreManager_Variation_TimerCheck_m557678307 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Variation::TimeEnd()
extern "C"  Il2CppObject * ScoreManager_Variation_TimeEnd_m2239639248 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation::conFirm()
extern "C"  void ScoreManager_Variation_conFirm_m802853834 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
