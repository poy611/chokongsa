﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkCloseConnection
struct NetworkCloseConnection_t686438608;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::.ctor()
extern "C"  void NetworkCloseConnection__ctor_m1981030806 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::Reset()
extern "C"  void NetworkCloseConnection_Reset_m3922431043 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkCloseConnection::OnEnter()
extern "C"  void NetworkCloseConnection_OnEnter_m491821037 (NetworkCloseConnection_t686438608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.NetworkCloseConnection::getIndexFromGUID(System.String,System.Int32&)
extern "C"  bool NetworkCloseConnection_getIndexFromGUID_m3293212166 (NetworkCloseConnection_t686438608 * __this, String_t* ___guid0, int32_t* ___guidIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
