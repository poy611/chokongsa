﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Collision2dEvent
struct Collision2dEvent_t149109630;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::.ctor()
extern "C"  void Collision2dEvent__ctor_m2276308136 (Collision2dEvent_t149109630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::Reset()
extern "C"  void Collision2dEvent_Reset_m4217708373 (Collision2dEvent_t149109630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::OnPreprocess()
extern "C"  void Collision2dEvent_OnPreprocess_m4214445959 (Collision2dEvent_t149109630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::StoreCollisionInfo(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_StoreCollisionInfo_m2717712906 (Collision2dEvent_t149109630 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionEnter2D_m2344247270 (Collision2dEvent_t149109630 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionStay2D_m1663783853 (Collision2dEvent_t149109630 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionExit2D_m3616345544 (Collision2dEvent_t149109630 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void Collision2dEvent_DoParticleCollision_m3719224063 (Collision2dEvent_t149109630 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.Collision2dEvent::ErrorCheck()
extern "C"  String_t* Collision2dEvent_ErrorCheck_m1003060959 (Collision2dEvent_t149109630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
