﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetColorRGBA
struct SetColorRGBA_t2803139325;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::.ctor()
extern "C"  void SetColorRGBA__ctor_m3941677065 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::Reset()
extern "C"  void SetColorRGBA_Reset_m1588110006 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnEnter()
extern "C"  void SetColorRGBA_OnEnter_m3477200288 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnUpdate()
extern "C"  void SetColorRGBA_OnUpdate_m3847553187 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::DoSetColorRGBA()
extern "C"  void SetColorRGBA_DoSetColorRGBA_m3946000539 (SetColorRGBA_t2803139325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
