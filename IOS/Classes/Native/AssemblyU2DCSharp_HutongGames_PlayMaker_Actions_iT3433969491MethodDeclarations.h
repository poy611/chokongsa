﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookFrom
struct iTweenLookFrom_t3433969491;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::.ctor()
extern "C"  void iTweenLookFrom__ctor_m2311216755 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::Reset()
extern "C"  void iTweenLookFrom_Reset_m4252616992 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnEnter()
extern "C"  void iTweenLookFrom_OnEnter_m4267905418 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnExit()
extern "C"  void iTweenLookFrom_OnExit_m146593998 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::DoiTween()
extern "C"  void iTweenLookFrom_DoiTween_m612214014 (iTweenLookFrom_t3433969491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
