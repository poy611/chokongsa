﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetVideo
struct GetVideo_t2032672485;

#include "codegen/il2cpp-codegen.h"

// System.Void GetVideo::.ctor()
extern "C"  void GetVideo__ctor_m2102169942 (GetVideo_t2032672485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetVideo::OnGUI()
extern "C"  void GetVideo_OnGUI_m1597568592 (GetVideo_t2032672485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
