﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolChanged
struct  BoolChanged_t2120993528  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::boolVariable
	FsmBool_t1075959796 * ___boolVariable_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolChanged::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::storeResult
	FsmBool_t1075959796 * ___storeResult_13;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolChanged::previousValue
	bool ___previousValue_14;

public:
	inline static int32_t get_offset_of_boolVariable_11() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___boolVariable_11)); }
	inline FsmBool_t1075959796 * get_boolVariable_11() const { return ___boolVariable_11; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_11() { return &___boolVariable_11; }
	inline void set_boolVariable_11(FsmBool_t1075959796 * value)
	{
		___boolVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_11, value);
	}

	inline static int32_t get_offset_of_changedEvent_12() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___changedEvent_12)); }
	inline FsmEvent_t2133468028 * get_changedEvent_12() const { return ___changedEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_12() { return &___changedEvent_12; }
	inline void set_changedEvent_12(FsmEvent_t2133468028 * value)
	{
		___changedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_12, value);
	}

	inline static int32_t get_offset_of_storeResult_13() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___storeResult_13)); }
	inline FsmBool_t1075959796 * get_storeResult_13() const { return ___storeResult_13; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_13() { return &___storeResult_13; }
	inline void set_storeResult_13(FsmBool_t1075959796 * value)
	{
		___storeResult_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_13, value);
	}

	inline static int32_t get_offset_of_previousValue_14() { return static_cast<int32_t>(offsetof(BoolChanged_t2120993528, ___previousValue_14)); }
	inline bool get_previousValue_14() const { return ___previousValue_14; }
	inline bool* get_address_of_previousValue_14() { return &___previousValue_14; }
	inline void set_previousValue_14(bool value)
	{
		___previousValue_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
