﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct DefaultSerializationBinder_t3048163941;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_String7231557.h"

// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern "C"  Type_t * DefaultSerializationBinder_GetTypeFromTypeNameKey_m1116398445 (Il2CppObject * __this /* static, unused */, TypeNameKey_t2971844791  ___typeNameKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern "C"  Type_t * DefaultSerializationBinder_BindToType_m3348569000 (DefaultSerializationBinder_t3048163941 * __this, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern "C"  void DefaultSerializationBinder__ctor_m2912715514 (DefaultSerializationBinder_t3048163941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern "C"  void DefaultSerializationBinder__cctor_m3912738803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
