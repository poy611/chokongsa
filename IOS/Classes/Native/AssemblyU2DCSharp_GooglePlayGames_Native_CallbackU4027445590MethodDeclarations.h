﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m1876344714_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m1876344714(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m1876344714_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>::<>m__10(T1,T2)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539(__this, ___val10, ___val21, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539_gshared)(__this, ___val10, ___val21, method)
