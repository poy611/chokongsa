﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<GameStepManager>::.ctor()
#define Singleton_1__ctor_m1499818604(__this, method) ((  void (*) (Singleton_1_t2764559344 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// System.Void Singleton`1<GameStepManager>::.cctor()
#define Singleton_1__cctor_m3062607553(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m1977804114_gshared)(__this /* static, unused */, method)
// T Singleton`1<GameStepManager>::get_GetInstance()
#define Singleton_1_get_GetInstance_m3657169165(__this /* static, unused */, method) ((  GameStepManager_t2511743951 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_GetInstance_m102549980_gshared)(__this /* static, unused */, method)
