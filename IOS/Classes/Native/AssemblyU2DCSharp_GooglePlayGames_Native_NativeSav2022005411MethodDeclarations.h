﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey84
struct U3CFetchAllSavedGamesU3Ec__AnonStorey84_t2022005411;
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t1240414705;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1240414705.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey84::.ctor()
extern "C"  void U3CFetchAllSavedGamesU3Ec__AnonStorey84__ctor_m3086701400 (U3CFetchAllSavedGamesU3Ec__AnonStorey84_t2022005411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<FetchAllSavedGames>c__AnonStorey84::<>m__6D(GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse)
extern "C"  void U3CFetchAllSavedGamesU3Ec__AnonStorey84_U3CU3Em__6D_m3854065746 (U3CFetchAllSavedGamesU3Ec__AnonStorey84_t2022005411 * __this, FetchAllResponse_t1240414705 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
