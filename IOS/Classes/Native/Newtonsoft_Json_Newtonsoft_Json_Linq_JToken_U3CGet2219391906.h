﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41
struct  U3CGetAncestorsU3Ed__41_t2219391906  : public Il2CppObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>2__current
	JToken_t3412245951 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>4__this
	JToken_t3412245951 * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<current>5__1
	JToken_t3412245951 * ___U3CcurrentU3E5__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CU3E2__current_1)); }
	inline JToken_t3412245951 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t3412245951 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t3412245951 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CU3E4__this_5)); }
	inline JToken_t3412245951 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_t3412245951 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_t3412245951 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_5, value);
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2219391906, ___U3CcurrentU3E5__1_6)); }
	inline JToken_t3412245951 * get_U3CcurrentU3E5__1_6() const { return ___U3CcurrentU3E5__1_6; }
	inline JToken_t3412245951 ** get_address_of_U3CcurrentU3E5__1_6() { return &___U3CcurrentU3E5__1_6; }
	inline void set_U3CcurrentU3E5__1_6(JToken_t3412245951 * value)
	{
		___U3CcurrentU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentU3E5__1_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
