﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHandli56081595.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3780370405_gshared (Nullable_1_t140208118 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3780370405(__this, ___value0, method) ((  void (*) (Nullable_1_t140208118 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3780370405_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1816069013_gshared (Nullable_1_t140208118 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1816069013(__this, method) ((  bool (*) (Nullable_1_t140208118 *, const MethodInfo*))Nullable_1_get_HasValue_m1816069013_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1250184062_gshared (Nullable_1_t140208118 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1250184062(__this, method) ((  int32_t (*) (Nullable_1_t140208118 *, const MethodInfo*))Nullable_1_get_Value_m1250184062_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m864419276_gshared (Nullable_1_t140208118 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m864419276(__this, ___other0, method) ((  bool (*) (Nullable_1_t140208118 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m864419276_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1629796339_gshared (Nullable_1_t140208118 * __this, Nullable_1_t140208118  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1629796339(__this, ___other0, method) ((  bool (*) (Nullable_1_t140208118 *, Nullable_1_t140208118 , const MethodInfo*))Nullable_1_Equals_m1629796339_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m568616612_gshared (Nullable_1_t140208118 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m568616612(__this, method) ((  int32_t (*) (Nullable_1_t140208118 *, const MethodInfo*))Nullable_1_GetHashCode_m568616612_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m694577098_gshared (Nullable_1_t140208118 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m694577098(__this, method) ((  int32_t (*) (Nullable_1_t140208118 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m694577098_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1768654139_gshared (Nullable_1_t140208118 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1768654139(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t140208118 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1768654139_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m557720052_gshared (Nullable_1_t140208118 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m557720052(__this, method) ((  String_t* (*) (Nullable_1_t140208118 *, const MethodInfo*))Nullable_1_ToString_m557720052_gshared)(__this, method)
