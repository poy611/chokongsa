﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeEasyMovieTexture
struct YoutubeEasyMovieTexture_t1195908624;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void YoutubeEasyMovieTexture::.ctor()
extern "C"  void YoutubeEasyMovieTexture__ctor_m4288615259 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::Start()
extern "C"  void YoutubeEasyMovieTexture_Start_m3235753051 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::BackButtonClick()
extern "C"  void YoutubeEasyMovieTexture_BackButtonClick_m2711418056 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::ButtonClickYoutube(System.String)
extern "C"  void YoutubeEasyMovieTexture_ButtonClickYoutube_m692045996 (YoutubeEasyMovieTexture_t1195908624 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::StopButton()
extern "C"  void YoutubeEasyMovieTexture_StopButton_m3077138781 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::PlayOrPause()
extern "C"  void YoutubeEasyMovieTexture_PlayOrPause_m1579295416 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::MouseDown()
extern "C"  void YoutubeEasyMovieTexture_MouseDown_m1542773152 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::MouseUp()
extern "C"  void YoutubeEasyMovieTexture_MouseUp_m2719425689 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator YoutubeEasyMovieTexture::CheckPlay()
extern "C"  Il2CppObject * YoutubeEasyMovieTexture_CheckPlay_m3316543517 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeEasyMovieTexture::FixedUpdate()
extern "C"  void YoutubeEasyMovieTexture_FixedUpdate_m1185267862 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
