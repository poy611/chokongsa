﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector3Value
struct SetVector3Value_t2514376337;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::.ctor()
extern "C"  void SetVector3Value__ctor_m772912069 (SetVector3Value_t2514376337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::Reset()
extern "C"  void SetVector3Value_Reset_m2714312306 (SetVector3Value_t2514376337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnEnter()
extern "C"  void SetVector3Value_OnEnter_m3425851996 (SetVector3Value_t2514376337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnUpdate()
extern "C"  void SetVector3Value_OnUpdate_m2255756135 (SetVector3Value_t2514376337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
