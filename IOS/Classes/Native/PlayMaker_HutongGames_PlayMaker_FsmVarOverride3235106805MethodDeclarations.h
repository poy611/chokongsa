﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVarOverride
struct FsmVarOverride_t3235106805;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride3235106805.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"

// System.Void HutongGames.PlayMaker.FsmVarOverride::.ctor(HutongGames.PlayMaker.FsmVarOverride)
extern "C"  void FsmVarOverride__ctor_m2736747807 (FsmVarOverride_t3235106805 * __this, FsmVarOverride_t3235106805 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVarOverride::.ctor(HutongGames.PlayMaker.NamedVariable)
extern "C"  void FsmVarOverride__ctor_m769288075 (FsmVarOverride_t3235106805 * __this, NamedVariable_t3211770239 * ___namedVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVarOverride::Apply(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVarOverride_Apply_m3442142343 (FsmVarOverride_t3235106805 * __this, FsmVariables_t963491929 * ___variables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
