﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m56845681_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m56845681(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m56845681_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::<>m__18()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257_gshared)(__this, method)
