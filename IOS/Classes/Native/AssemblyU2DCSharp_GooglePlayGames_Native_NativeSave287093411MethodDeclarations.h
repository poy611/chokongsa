﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E
struct U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey7E__ctor_m3393939288 (U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::<>m__6F()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey7E_U3CU3Em__6F_m4003688177 (U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
