﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeVideo
struct YoutubeVideo_t2077346616;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"

// System.Void YoutubeVideo::.ctor()
extern "C"  void YoutubeVideo__ctor_m1943750243 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeVideo::Awake()
extern "C"  void YoutubeVideo_Awake_m2181355462 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeVideo::RequestVideo(System.String,System.Int32)
extern "C"  String_t* YoutubeVideo_RequestVideo_m2780138341 (YoutubeVideo_t2077346616 * __this, String_t* ___urlOrId0, int32_t ___quality1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeVideo::MyRemoteCertificateValidationCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603 (YoutubeVideo_t2077346616 * __this, Il2CppObject * ___sender0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeVideo::OnGUI()
extern "C"  void YoutubeVideo_OnGUI_m1439148893 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
