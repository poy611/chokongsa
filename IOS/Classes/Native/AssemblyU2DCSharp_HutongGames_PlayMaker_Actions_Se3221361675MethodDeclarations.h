﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIsFixedAngle2d
struct SetIsFixedAngle2d_t3221361675;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::.ctor()
extern "C"  void SetIsFixedAngle2d__ctor_m1260905867 (SetIsFixedAngle2d_t3221361675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::Reset()
extern "C"  void SetIsFixedAngle2d_Reset_m3202306104 (SetIsFixedAngle2d_t3221361675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::OnEnter()
extern "C"  void SetIsFixedAngle2d_OnEnter_m4236456610 (SetIsFixedAngle2d_t3221361675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::OnUpdate()
extern "C"  void SetIsFixedAngle2d_OnUpdate_m1614695393 (SetIsFixedAngle2d_t3221361675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::DoSetIsFixedAngle()
extern "C"  void SetIsFixedAngle2d_DoSetIsFixedAngle_m3782764201 (SetIsFixedAngle2d_t3221361675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
