﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t3760582229;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76
struct  U3CRoomConnectedU3Ec__AnonStorey76_t3218131764  : public Il2CppObject
{
public:
	// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76::success
	bool ___success_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76::<>f__this
	OnGameThreadForwardingListener_t3760582229 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CRoomConnectedU3Ec__AnonStorey76_t3218131764, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CRoomConnectedU3Ec__AnonStorey76_t3218131764, ___U3CU3Ef__this_1)); }
	inline OnGameThreadForwardingListener_t3760582229 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline OnGameThreadForwardingListener_t3760582229 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(OnGameThreadForwardingListener_t3760582229 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
