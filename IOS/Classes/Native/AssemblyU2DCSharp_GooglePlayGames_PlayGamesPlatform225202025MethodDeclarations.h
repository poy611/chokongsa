﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36
struct U3CLoadAchievementsU3Ec__AnonStorey36_t225202025;
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3251685236;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36::.ctor()
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey36__ctor_m972134946 (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36::<>m__5(GooglePlayGames.BasicApi.Achievement[])
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995 (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * __this, AchievementU5BU5D_t3251685236* ___ach0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
