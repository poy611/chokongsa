﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>
struct ComponentAction_1_t3146284570;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GUIText
struct GUIText_t3371372606;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.Light
struct Light_t4202674828;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::.ctor()
extern "C"  void ComponentAction_1__ctor_m1981820550_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1__ctor_m1981820550(__this, method) ((  void (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1__ctor_m1981820550_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody()
extern "C"  Rigidbody_t3346577219 * ComponentAction_1_get_rigidbody_m3654614315_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_rigidbody_m3654614315(__this, method) ((  Rigidbody_t3346577219 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3654614315_gshared)(__this, method)
// UnityEngine.Rigidbody2D HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody2d()
extern "C"  Rigidbody2D_t1743771669 * ComponentAction_1_get_rigidbody2d_m3969478575_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_rigidbody2d_m3969478575(__this, method) ((  Rigidbody2D_t1743771669 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_rigidbody2d_m3969478575_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_renderer()
extern "C"  Renderer_t3076687687 * ComponentAction_1_get_renderer_m2707840429_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_renderer_m2707840429(__this, method) ((  Renderer_t3076687687 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_renderer_m2707840429_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_animation()
extern "C"  Animation_t1724966010 * ComponentAction_1_get_animation_m3244187609_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_animation_m3244187609(__this, method) ((  Animation_t1724966010 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_animation_m3244187609_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_audio()
extern "C"  AudioSource_t1740077639 * ComponentAction_1_get_audio_m1198830264_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_audio_m1198830264(__this, method) ((  AudioSource_t1740077639 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_audio_m1198830264_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_camera()
extern "C"  Camera_t2727095145 * ComponentAction_1_get_camera_m2580411949_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_camera_m2580411949(__this, method) ((  Camera_t2727095145 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_camera_m2580411949_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiText()
extern "C"  GUIText_t3371372606 * ComponentAction_1_get_guiText_m995647329_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_guiText_m995647329(__this, method) ((  GUIText_t3371372606 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_guiText_m995647329_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiTexture()
extern "C"  GUITexture_t4020448292 * ComponentAction_1_get_guiTexture_m3296952909_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_guiTexture_m3296952909(__this, method) ((  GUITexture_t4020448292 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m3296952909_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_light()
extern "C"  Light_t4202674828 * ComponentAction_1_get_light_m3399183485_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_light_m3399183485(__this, method) ((  Light_t4202674828 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_light_m3399183485_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_networkView()
extern "C"  NetworkView_t3656680617 * ComponentAction_1_get_networkView_m1393581751_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method);
#define ComponentAction_1_get_networkView_m1393581751(__this, method) ((  NetworkView_t3656680617 * (*) (ComponentAction_1_t3146284570 *, const MethodInfo*))ComponentAction_1_get_networkView_m1393581751_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::UpdateCache(UnityEngine.GameObject)
extern "C"  bool ComponentAction_1_UpdateCache_m1764863969_gshared (ComponentAction_1_t3146284570 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method);
#define ComponentAction_1_UpdateCache_m1764863969(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t3146284570 *, GameObject_t3674682005 *, const MethodInfo*))ComponentAction_1_UpdateCache_m1764863969_gshared)(__this, ___go0, method)
