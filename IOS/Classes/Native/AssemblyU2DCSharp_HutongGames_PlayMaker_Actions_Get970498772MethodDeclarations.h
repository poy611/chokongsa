﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTouchCount
struct GetTouchCount_t970498772;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::.ctor()
extern "C"  void GetTouchCount__ctor_m1907268834 (GetTouchCount_t970498772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::Reset()
extern "C"  void GetTouchCount_Reset_m3848669071 (GetTouchCount_t970498772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnEnter()
extern "C"  void GetTouchCount_OnEnter_m2621009977 (GetTouchCount_t970498772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnUpdate()
extern "C"  void GetTouchCount_OnUpdate_m3075457322 (GetTouchCount_t970498772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::DoGetTouchCount()
extern "C"  void GetTouchCount_DoGetTouchCount_m337539771 (GetTouchCount_t970498772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
