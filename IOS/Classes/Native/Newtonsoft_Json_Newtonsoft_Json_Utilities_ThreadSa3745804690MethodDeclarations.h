﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct ThreadSafeStore_2_t3745804690;
// System.Func`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Func_2_t1055798018;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern "C"  void ThreadSafeStore_2__ctor_m72384408_gshared (ThreadSafeStore_2_t3745804690 * __this, Func_2_t1055798018 * ___creator0, const MethodInfo* method);
#define ThreadSafeStore_2__ctor_m72384408(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t3745804690 *, Func_2_t1055798018 *, const MethodInfo*))ThreadSafeStore_2__ctor_m72384408_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m2316141778_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method);
#define ThreadSafeStore_2_Get_m2316141778(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t3745804690 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_Get_m2316141778_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m1276670208_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method);
#define ThreadSafeStore_2_AddValue_m1276670208(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t3745804690 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_AddValue_m1276670208_gshared)(__this, ___key0, method)
