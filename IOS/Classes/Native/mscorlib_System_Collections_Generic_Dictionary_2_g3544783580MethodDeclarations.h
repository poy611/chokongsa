﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct IDictionary_2_t3122656925;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>[]
struct KeyValuePair_2U5BU5D_t2263924555;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>
struct IEnumerator_1_t1060462039;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>
struct KeyCollection_t876575735;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>
struct ValueCollection_t2245389293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En567139676.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void Dictionary_2__ctor_m344206812_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m344206812(__this, method) ((  void (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2__ctor_m344206812_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m526077523_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m526077523(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m526077523_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3439227196_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3439227196(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3439227196_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2244999469_gshared (Dictionary_2_t3544783580 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2244999469(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3544783580 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2244999469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1493425217_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1493425217(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1493425217_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m343599344_gshared (Dictionary_2_t3544783580 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m343599344(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3544783580 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m343599344_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m722595101_gshared (Dictionary_2_t3544783580 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m722595101(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3544783580 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m722595101_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3207321468_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3207321468(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3207321468_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3244004634_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3244004634(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3244004634_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3033734424_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3033734424(__this, method) ((  bool (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3033734424_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m630105122_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m630105122(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m630105122_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2316482769_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2316482769(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2316482769_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3288696544_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3288696544(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3288696544_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m649261266_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m649261266(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m649261266_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1749742863_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1749742863(__this, ___key0, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1749742863_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3320233922_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3320233922(__this, method) ((  bool (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3320233922_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2006471988_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2006471988(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2006471988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2396201350_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2396201350(__this, method) ((  bool (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2396201350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1984007717_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2_t3443564286  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1984007717(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3544783580 *, KeyValuePair_2_t3443564286 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1984007717_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3209608705_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2_t3443564286  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3209608705(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3544783580 *, KeyValuePair_2_t3443564286 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3209608705_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3505856201_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2U5BU5D_t2263924555* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3505856201(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3544783580 *, KeyValuePair_2U5BU5D_t2263924555*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3505856201_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754324326_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2_t3443564286  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754324326(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3544783580 *, KeyValuePair_2_t3443564286 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754324326_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3961089768_gshared (Dictionary_2_t3544783580 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3961089768(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3961089768_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1648093687_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1648093687(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1648093687_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1104149486_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1104149486(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1104149486_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m434357691_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m434357691(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m434357691_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1783390716_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1783390716(__this, method) ((  int32_t (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_get_Count_m1783390716_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Item(TKey)
extern "C"  RaycastHit2D_t1374744384  Dictionary_2_get_Item_m1157420299_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1157420299(__this, ___key0, method) ((  RaycastHit2D_t1374744384  (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1157420299_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2276782748_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2276782748(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_set_Item_m2276782748_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3512477012_gshared (Dictionary_2_t3544783580 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3512477012(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3544783580 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3512477012_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m479453411_gshared (Dictionary_2_t3544783580 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m479453411(__this, ___size0, method) ((  void (*) (Dictionary_2_t3544783580 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m479453411_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2505962399_gshared (Dictionary_2_t3544783580 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2505962399(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2505962399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3443564286  Dictionary_2_make_pair_m3593325619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3593325619(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3443564286  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_make_pair_m3593325619_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m697410091_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m697410091(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_pick_key_m697410091_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::pick_value(TKey,TValue)
extern "C"  RaycastHit2D_t1374744384  Dictionary_2_pick_value_m1218482119_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1218482119(__this /* static, unused */, ___key0, ___value1, method) ((  RaycastHit2D_t1374744384  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_pick_value_m1218482119_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3440974672_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2U5BU5D_t2263924555* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3440974672(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3544783580 *, KeyValuePair_2U5BU5D_t2263924555*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3440974672_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Resize()
extern "C"  void Dictionary_2_Resize_m2138273244_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2138273244(__this, method) ((  void (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_Resize_m2138273244_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m67296857_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m67296857(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_Add_m67296857_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Clear()
extern "C"  void Dictionary_2_Clear_m2045307399_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2045307399(__this, method) ((  void (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_Clear_m2045307399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3246170609_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3246170609(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m3246170609_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2179175793_gshared (Dictionary_2_t3544783580 * __this, RaycastHit2D_t1374744384  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2179175793(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3544783580 *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_ContainsValue_m2179175793_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m819382138_gshared (Dictionary_2_t3544783580 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m819382138(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3544783580 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m819382138_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2925511466_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2925511466(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2925511466_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m682648671_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m682648671(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m682648671_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m860125514_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m860125514(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3544783580 *, Il2CppObject *, RaycastHit2D_t1374744384 *, const MethodInfo*))Dictionary_2_TryGetValue_m860125514_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Keys()
extern "C"  KeyCollection_t876575735 * Dictionary_2_get_Keys_m4086370961_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4086370961(__this, method) ((  KeyCollection_t876575735 * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_get_Keys_m4086370961_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Values()
extern "C"  ValueCollection_t2245389293 * Dictionary_2_get_Values_m1704125549_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1704125549(__this, method) ((  ValueCollection_t2245389293 * (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_get_Values_m1704125549_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m147268998_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m147268998(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m147268998_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ToTValue(System.Object)
extern "C"  RaycastHit2D_t1374744384  Dictionary_2_ToTValue_m3825763554_gshared (Dictionary_2_t3544783580 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3825763554(__this, ___value0, method) ((  RaycastHit2D_t1374744384  (*) (Dictionary_2_t3544783580 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3825763554_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2411665986_gshared (Dictionary_2_t3544783580 * __this, KeyValuePair_2_t3443564286  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2411665986(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3544783580 *, KeyValuePair_2_t3443564286 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2411665986_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t567139676  Dictionary_2_GetEnumerator_m2082037351_gshared (Dictionary_2_t3544783580 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2082037351(__this, method) ((  Enumerator_t567139676  (*) (Dictionary_2_t3544783580 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2082037351_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1079278878_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1079278878(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1079278878_gshared)(__this /* static, unused */, ___key0, ___value1, method)
