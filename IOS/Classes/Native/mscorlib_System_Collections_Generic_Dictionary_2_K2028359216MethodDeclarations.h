﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>
struct KeyCollection_t2028359216;
// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.HandleRef>
struct IEnumerator_1_t3692684350;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Runtime.InteropServices.HandleRef[]
struct HandleRefU5BU5D_t3759335272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1016535819.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m4115427069_gshared (KeyCollection_t2028359216 * __this, Dictionary_2_t401599765 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m4115427069(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2028359216 *, Dictionary_2_t401599765 *, const MethodInfo*))KeyCollection__ctor_m4115427069_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3701784633_gshared (KeyCollection_t2028359216 * __this, HandleRef_t1780819301  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3701784633(__this, ___item0, method) ((  void (*) (KeyCollection_t2028359216 *, HandleRef_t1780819301 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3701784633_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3042942960_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3042942960(__this, method) ((  void (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3042942960_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3910380085_gshared (KeyCollection_t2028359216 * __this, HandleRef_t1780819301  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3910380085(__this, ___item0, method) ((  bool (*) (KeyCollection_t2028359216 *, HandleRef_t1780819301 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3910380085_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1564419290_gshared (KeyCollection_t2028359216 * __this, HandleRef_t1780819301  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1564419290(__this, ___item0, method) ((  bool (*) (KeyCollection_t2028359216 *, HandleRef_t1780819301 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1564419290_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1510320834_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1510320834(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1510320834_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1528896610_gshared (KeyCollection_t2028359216 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1528896610(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2028359216 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1528896610_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m157932657_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m157932657(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m157932657_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2659936342_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2659936342(__this, method) ((  bool (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2659936342_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m432915592_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m432915592(__this, method) ((  bool (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m432915592_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m164612922_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m164612922(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m164612922_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3020753330_gshared (KeyCollection_t2028359216 * __this, HandleRefU5BU5D_t3759335272* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3020753330(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2028359216 *, HandleRefU5BU5D_t3759335272*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3020753330_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1016535819  KeyCollection_GetEnumerator_m3523836287_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3523836287(__this, method) ((  Enumerator_t1016535819  (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_GetEnumerator_m3523836287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1366883138_gshared (KeyCollection_t2028359216 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1366883138(__this, method) ((  int32_t (*) (KeyCollection_t2028359216 *, const MethodInfo*))KeyCollection_get_Count_m1366883138_gshared)(__this, method)
