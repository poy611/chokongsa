﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerEnter
struct PlayMakerTriggerEnter_t977448304;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void PlayMakerTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerEnter_OnTriggerEnter_m2450496159 (PlayMakerTriggerEnter_t977448304 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerEnter::.ctor()
extern "C"  void PlayMakerTriggerEnter__ctor_m3125792633 (PlayMakerTriggerEnter_t977448304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
