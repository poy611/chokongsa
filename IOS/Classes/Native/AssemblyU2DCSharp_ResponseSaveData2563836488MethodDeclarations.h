﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResponseSaveData
struct ResponseSaveData_t2563836488;

#include "codegen/il2cpp-codegen.h"

// System.Void ResponseSaveData::.ctor()
extern "C"  void ResponseSaveData__ctor_m1035414867 (ResponseSaveData_t2563836488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
