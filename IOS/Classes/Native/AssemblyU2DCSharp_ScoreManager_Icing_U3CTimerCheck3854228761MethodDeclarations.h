﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Icing/<TimerCheck>c__Iterator1C
struct U3CTimerCheckU3Ec__Iterator1C_t3854228761;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator1C__ctor_m557015458 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<TimerCheck>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2896638458 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<TimerCheck>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m1533045134 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Icing/<TimerCheck>c__Iterator1C::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator1C_MoveNext_m2019924730 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator1C_Dispose_m2614193503 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::Reset()
extern "C"  void U3CTimerCheckU3Ec__Iterator1C_Reset_m2498415695 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
