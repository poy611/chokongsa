﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Subtract
struct Vector3Subtract_t3277771666;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::.ctor()
extern "C"  void Vector3Subtract__ctor_m18302948 (Vector3Subtract_t3277771666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::Reset()
extern "C"  void Vector3Subtract_Reset_m1959703185 (Vector3Subtract_t3277771666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::OnEnter()
extern "C"  void Vector3Subtract_OnEnter_m4095959739 (Vector3Subtract_t3277771666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::OnUpdate()
extern "C"  void Vector3Subtract_OnUpdate_m1554259688 (Vector3Subtract_t3277771666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
