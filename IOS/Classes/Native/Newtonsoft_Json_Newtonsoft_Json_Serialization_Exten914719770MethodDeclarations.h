﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_t914719770;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t950614638;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Serialization.ExtensionDataGetter::.ctor(System.Object,System.IntPtr)
extern "C"  void ExtensionDataGetter__ctor_m162336451 (ExtensionDataGetter_t914719770 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::Invoke(System.Object)
extern "C"  Il2CppObject* ExtensionDataGetter_Invoke_m3210258322 (ExtensionDataGetter_t914719770 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataGetter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExtensionDataGetter_BeginInvoke_m2763992960 (ExtensionDataGetter_t914719770 * __this, Il2CppObject * ___o0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject* ExtensionDataGetter_EndInvoke_m2550783224 (ExtensionDataGetter_t914719770 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
