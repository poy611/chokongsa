﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertSecondsToString
struct ConvertSecondsToString_t2628471232;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::.ctor()
extern "C"  void ConvertSecondsToString__ctor_m1197416102 (ConvertSecondsToString_t2628471232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::Reset()
extern "C"  void ConvertSecondsToString_Reset_m3138816339 (ConvertSecondsToString_t2628471232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::OnEnter()
extern "C"  void ConvertSecondsToString_OnEnter_m3352334589 (ConvertSecondsToString_t2628471232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::OnUpdate()
extern "C"  void ConvertSecondsToString_OnUpdate_m4271683814 (ConvertSecondsToString_t2628471232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::DoConvertSecondsToString()
extern "C"  void ConvertSecondsToString_DoConvertSecondsToString_m4077955809 (ConvertSecondsToString_t2628471232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
