﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct GetAnimatorBool_t3376545269;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::.ctor()
extern "C"  void GetAnimatorBool__ctor_m1871322337 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::Reset()
extern "C"  void GetAnimatorBool_Reset_m3812722574 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnEnter()
extern "C"  void GetAnimatorBool_OnEnter_m2436164728 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnActionUpdate()
extern "C"  void GetAnimatorBool_OnActionUpdate_m2636801121 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::GetParameter()
extern "C"  void GetAnimatorBool_GetParameter_m1210073718 (GetAnimatorBool_t3376545269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
