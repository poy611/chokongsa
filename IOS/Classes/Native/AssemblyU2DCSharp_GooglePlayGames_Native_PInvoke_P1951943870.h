﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponseCollector
struct FetchResponseCollector_t4033649688;
// GooglePlayGames.Native.PInvoke.PlayerManager
struct PlayerManager_t3380828466;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4
struct  U3CFetchListU3Ec__AnonStoreyA4_t1951943870  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponseCollector GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4::coll
	FetchResponseCollector_t4033649688 * ___coll_0;
	// GooglePlayGames.Native.PInvoke.PlayerManager GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4::<>f__this
	PlayerManager_t3380828466 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_coll_0() { return static_cast<int32_t>(offsetof(U3CFetchListU3Ec__AnonStoreyA4_t1951943870, ___coll_0)); }
	inline FetchResponseCollector_t4033649688 * get_coll_0() const { return ___coll_0; }
	inline FetchResponseCollector_t4033649688 ** get_address_of_coll_0() { return &___coll_0; }
	inline void set_coll_0(FetchResponseCollector_t4033649688 * value)
	{
		___coll_0 = value;
		Il2CppCodeGenWriteBarrier(&___coll_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CFetchListU3Ec__AnonStoreyA4_t1951943870, ___U3CU3Ef__this_1)); }
	inline PlayerManager_t3380828466 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline PlayerManager_t3380828466 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(PlayerManager_t3380828466 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
