﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorRoot
struct GetAnimatorRoot_t3377021933;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::.ctor()
extern "C"  void GetAnimatorRoot__ctor_m4194700777 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::Reset()
extern "C"  void GetAnimatorRoot_Reset_m1841133718 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnEnter()
extern "C"  void GetAnimatorRoot_OnEnter_m1819851648 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnActionUpdate()
extern "C"  void GetAnimatorRoot_OnActionUpdate_m1926831193 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::DoGetBodyPosition()
extern "C"  void GetAnimatorRoot_DoGetBodyPosition_m3392718813 (GetAnimatorRoot_t3377021933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
