﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2710165404.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHa2626038881.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m110173055_gshared (Nullable_1_t2710165404 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m110173055(__this, ___value0, method) ((  void (*) (Nullable_1_t2710165404 *, int32_t, const MethodInfo*))Nullable_1__ctor_m110173055_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m618740015_gshared (Nullable_1_t2710165404 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m618740015(__this, method) ((  bool (*) (Nullable_1_t2710165404 *, const MethodInfo*))Nullable_1_get_HasValue_m618740015_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3210300900_gshared (Nullable_1_t2710165404 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3210300900(__this, method) ((  int32_t (*) (Nullable_1_t2710165404 *, const MethodInfo*))Nullable_1_get_Value_m3210300900_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3124048882_gshared (Nullable_1_t2710165404 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3124048882(__this, ___other0, method) ((  bool (*) (Nullable_1_t2710165404 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3124048882_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2994301709_gshared (Nullable_1_t2710165404 * __this, Nullable_1_t2710165404  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2994301709(__this, ___other0, method) ((  bool (*) (Nullable_1_t2710165404 *, Nullable_1_t2710165404 , const MethodInfo*))Nullable_1_Equals_m2994301709_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m864470090_gshared (Nullable_1_t2710165404 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m864470090(__this, method) ((  int32_t (*) (Nullable_1_t2710165404 *, const MethodInfo*))Nullable_1_GetHashCode_m864470090_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1196223472_gshared (Nullable_1_t2710165404 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1196223472(__this, method) ((  int32_t (*) (Nullable_1_t2710165404 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1196223472_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3383298815_gshared (Nullable_1_t2710165404 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3383298815(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2710165404 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m3383298815_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3606991822_gshared (Nullable_1_t2710165404 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3606991822(__this, method) ((  String_t* (*) (Nullable_1_t2710165404 *, const MethodInfo*))Nullable_1_ToString_m3606991822_gshared)(__this, method)
