﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SaveData
struct SaveData_t2286519015;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// NetworkMng
struct NetworkMng_t1515215352;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng/<SaveGameDataCouroutine>c__Iterator13
struct  U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974  : public Il2CppObject
{
public:
	// SaveData NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<save>__0
	SaveData_t2286519015 * ___U3CsaveU3E__0_0;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::trialTh
	int32_t ___trialTh_1;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::grade
	int32_t ___grade_2;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::gameId
	int32_t ___gameId_3;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::guestTh
	int32_t ___guestTh_4;
	// System.String NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<str>__1
	String_t* ___U3CstrU3E__1_5;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::$PC
	int32_t ___U24PC_6;
	// System.Object NetworkMng/<SaveGameDataCouroutine>c__Iterator13::$current
	Il2CppObject * ___U24current_7;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<$>trialTh
	int32_t ___U3CU24U3EtrialTh_8;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<$>grade
	int32_t ___U3CU24U3Egrade_9;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<$>gameId
	int32_t ___U3CU24U3EgameId_10;
	// System.Int32 NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<$>guestTh
	int32_t ___U3CU24U3EguestTh_11;
	// NetworkMng NetworkMng/<SaveGameDataCouroutine>c__Iterator13::<>f__this
	NetworkMng_t1515215352 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_U3CsaveU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CsaveU3E__0_0)); }
	inline SaveData_t2286519015 * get_U3CsaveU3E__0_0() const { return ___U3CsaveU3E__0_0; }
	inline SaveData_t2286519015 ** get_address_of_U3CsaveU3E__0_0() { return &___U3CsaveU3E__0_0; }
	inline void set_U3CsaveU3E__0_0(SaveData_t2286519015 * value)
	{
		___U3CsaveU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsaveU3E__0_0, value);
	}

	inline static int32_t get_offset_of_trialTh_1() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___trialTh_1)); }
	inline int32_t get_trialTh_1() const { return ___trialTh_1; }
	inline int32_t* get_address_of_trialTh_1() { return &___trialTh_1; }
	inline void set_trialTh_1(int32_t value)
	{
		___trialTh_1 = value;
	}

	inline static int32_t get_offset_of_grade_2() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___grade_2)); }
	inline int32_t get_grade_2() const { return ___grade_2; }
	inline int32_t* get_address_of_grade_2() { return &___grade_2; }
	inline void set_grade_2(int32_t value)
	{
		___grade_2 = value;
	}

	inline static int32_t get_offset_of_gameId_3() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___gameId_3)); }
	inline int32_t get_gameId_3() const { return ___gameId_3; }
	inline int32_t* get_address_of_gameId_3() { return &___gameId_3; }
	inline void set_gameId_3(int32_t value)
	{
		___gameId_3 = value;
	}

	inline static int32_t get_offset_of_guestTh_4() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___guestTh_4)); }
	inline int32_t get_guestTh_4() const { return ___guestTh_4; }
	inline int32_t* get_address_of_guestTh_4() { return &___guestTh_4; }
	inline void set_guestTh_4(int32_t value)
	{
		___guestTh_4 = value;
	}

	inline static int32_t get_offset_of_U3CstrU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CstrU3E__1_5)); }
	inline String_t* get_U3CstrU3E__1_5() const { return ___U3CstrU3E__1_5; }
	inline String_t** get_address_of_U3CstrU3E__1_5() { return &___U3CstrU3E__1_5; }
	inline void set_U3CstrU3E__1_5(String_t* value)
	{
		___U3CstrU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EtrialTh_8() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CU24U3EtrialTh_8)); }
	inline int32_t get_U3CU24U3EtrialTh_8() const { return ___U3CU24U3EtrialTh_8; }
	inline int32_t* get_address_of_U3CU24U3EtrialTh_8() { return &___U3CU24U3EtrialTh_8; }
	inline void set_U3CU24U3EtrialTh_8(int32_t value)
	{
		___U3CU24U3EtrialTh_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Egrade_9() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CU24U3Egrade_9)); }
	inline int32_t get_U3CU24U3Egrade_9() const { return ___U3CU24U3Egrade_9; }
	inline int32_t* get_address_of_U3CU24U3Egrade_9() { return &___U3CU24U3Egrade_9; }
	inline void set_U3CU24U3Egrade_9(int32_t value)
	{
		___U3CU24U3Egrade_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EgameId_10() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CU24U3EgameId_10)); }
	inline int32_t get_U3CU24U3EgameId_10() const { return ___U3CU24U3EgameId_10; }
	inline int32_t* get_address_of_U3CU24U3EgameId_10() { return &___U3CU24U3EgameId_10; }
	inline void set_U3CU24U3EgameId_10(int32_t value)
	{
		___U3CU24U3EgameId_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EguestTh_11() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CU24U3EguestTh_11)); }
	inline int32_t get_U3CU24U3EguestTh_11() const { return ___U3CU24U3EguestTh_11; }
	inline int32_t* get_address_of_U3CU24U3EguestTh_11() { return &___U3CU24U3EguestTh_11; }
	inline void set_U3CU24U3EguestTh_11(int32_t value)
	{
		___U3CU24U3EguestTh_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974, ___U3CU3Ef__this_12)); }
	inline NetworkMng_t1515215352 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline NetworkMng_t1515215352 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(NetworkMng_t1515215352 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
