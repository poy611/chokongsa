﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1524908924MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1135972061(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2211148282 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2746623473_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(T1,T2)
#define Action_2_Invoke_m3922543966(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2211148282 *, int32_t, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m4075160986_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m743557077(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2211148282 *, int32_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1730471097_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m758445933(__this, ___result0, method) ((  void (*) (Action_2_t2211148282 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3349428353_gshared)(__this, ___result0, method)
