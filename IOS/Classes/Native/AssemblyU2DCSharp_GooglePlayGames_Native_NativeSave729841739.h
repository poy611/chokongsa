﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct Action_2_t2661426558;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739  : public Il2CppObject
{
public:
	// System.Action`2<T1,T2> GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2::toConvert
	Action_2_t2661426558 * ___toConvert_0;

public:
	inline static int32_t get_offset_of_toConvert_0() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739, ___toConvert_0)); }
	inline Action_2_t2661426558 * get_toConvert_0() const { return ___toConvert_0; }
	inline Action_2_t2661426558 ** get_address_of_toConvert_0() { return &___toConvert_0; }
	inline void set_toConvert_0(Action_2_t2661426558 * value)
	{
		___toConvert_0 = value;
		Il2CppCodeGenWriteBarrier(&___toConvert_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
