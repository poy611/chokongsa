﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct  GetAnimatorNextStateInfo_t3502000247  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::layerIndex
	FsmInt_t1596138449 * ___layerIndex_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::name
	FsmString_t952858651 * ___name_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::nameHash
	FsmInt_t1596138449 * ___nameHash_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::fullPathHash
	FsmInt_t1596138449 * ___fullPathHash_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::shortPathHash
	FsmInt_t1596138449 * ___shortPathHash_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::tagHash
	FsmInt_t1596138449 * ___tagHash_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::isStateLooping
	FsmBool_t1075959796 * ___isStateLooping_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::length
	FsmFloat_t2134102846 * ___length_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::normalizedTime
	FsmFloat_t2134102846 * ___normalizedTime_23;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::loopCount
	FsmInt_t1596138449 * ___loopCount_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::currentLoopProgress
	FsmFloat_t2134102846 * ___currentLoopProgress_25;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::_animator
	Animator_t2776330603 * ____animator_26;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_layerIndex_15() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___layerIndex_15)); }
	inline FsmInt_t1596138449 * get_layerIndex_15() const { return ___layerIndex_15; }
	inline FsmInt_t1596138449 ** get_address_of_layerIndex_15() { return &___layerIndex_15; }
	inline void set_layerIndex_15(FsmInt_t1596138449 * value)
	{
		___layerIndex_15 = value;
		Il2CppCodeGenWriteBarrier(&___layerIndex_15, value);
	}

	inline static int32_t get_offset_of_name_16() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___name_16)); }
	inline FsmString_t952858651 * get_name_16() const { return ___name_16; }
	inline FsmString_t952858651 ** get_address_of_name_16() { return &___name_16; }
	inline void set_name_16(FsmString_t952858651 * value)
	{
		___name_16 = value;
		Il2CppCodeGenWriteBarrier(&___name_16, value);
	}

	inline static int32_t get_offset_of_nameHash_17() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___nameHash_17)); }
	inline FsmInt_t1596138449 * get_nameHash_17() const { return ___nameHash_17; }
	inline FsmInt_t1596138449 ** get_address_of_nameHash_17() { return &___nameHash_17; }
	inline void set_nameHash_17(FsmInt_t1596138449 * value)
	{
		___nameHash_17 = value;
		Il2CppCodeGenWriteBarrier(&___nameHash_17, value);
	}

	inline static int32_t get_offset_of_fullPathHash_18() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___fullPathHash_18)); }
	inline FsmInt_t1596138449 * get_fullPathHash_18() const { return ___fullPathHash_18; }
	inline FsmInt_t1596138449 ** get_address_of_fullPathHash_18() { return &___fullPathHash_18; }
	inline void set_fullPathHash_18(FsmInt_t1596138449 * value)
	{
		___fullPathHash_18 = value;
		Il2CppCodeGenWriteBarrier(&___fullPathHash_18, value);
	}

	inline static int32_t get_offset_of_shortPathHash_19() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___shortPathHash_19)); }
	inline FsmInt_t1596138449 * get_shortPathHash_19() const { return ___shortPathHash_19; }
	inline FsmInt_t1596138449 ** get_address_of_shortPathHash_19() { return &___shortPathHash_19; }
	inline void set_shortPathHash_19(FsmInt_t1596138449 * value)
	{
		___shortPathHash_19 = value;
		Il2CppCodeGenWriteBarrier(&___shortPathHash_19, value);
	}

	inline static int32_t get_offset_of_tagHash_20() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___tagHash_20)); }
	inline FsmInt_t1596138449 * get_tagHash_20() const { return ___tagHash_20; }
	inline FsmInt_t1596138449 ** get_address_of_tagHash_20() { return &___tagHash_20; }
	inline void set_tagHash_20(FsmInt_t1596138449 * value)
	{
		___tagHash_20 = value;
		Il2CppCodeGenWriteBarrier(&___tagHash_20, value);
	}

	inline static int32_t get_offset_of_isStateLooping_21() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___isStateLooping_21)); }
	inline FsmBool_t1075959796 * get_isStateLooping_21() const { return ___isStateLooping_21; }
	inline FsmBool_t1075959796 ** get_address_of_isStateLooping_21() { return &___isStateLooping_21; }
	inline void set_isStateLooping_21(FsmBool_t1075959796 * value)
	{
		___isStateLooping_21 = value;
		Il2CppCodeGenWriteBarrier(&___isStateLooping_21, value);
	}

	inline static int32_t get_offset_of_length_22() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___length_22)); }
	inline FsmFloat_t2134102846 * get_length_22() const { return ___length_22; }
	inline FsmFloat_t2134102846 ** get_address_of_length_22() { return &___length_22; }
	inline void set_length_22(FsmFloat_t2134102846 * value)
	{
		___length_22 = value;
		Il2CppCodeGenWriteBarrier(&___length_22, value);
	}

	inline static int32_t get_offset_of_normalizedTime_23() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___normalizedTime_23)); }
	inline FsmFloat_t2134102846 * get_normalizedTime_23() const { return ___normalizedTime_23; }
	inline FsmFloat_t2134102846 ** get_address_of_normalizedTime_23() { return &___normalizedTime_23; }
	inline void set_normalizedTime_23(FsmFloat_t2134102846 * value)
	{
		___normalizedTime_23 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedTime_23, value);
	}

	inline static int32_t get_offset_of_loopCount_24() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___loopCount_24)); }
	inline FsmInt_t1596138449 * get_loopCount_24() const { return ___loopCount_24; }
	inline FsmInt_t1596138449 ** get_address_of_loopCount_24() { return &___loopCount_24; }
	inline void set_loopCount_24(FsmInt_t1596138449 * value)
	{
		___loopCount_24 = value;
		Il2CppCodeGenWriteBarrier(&___loopCount_24, value);
	}

	inline static int32_t get_offset_of_currentLoopProgress_25() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ___currentLoopProgress_25)); }
	inline FsmFloat_t2134102846 * get_currentLoopProgress_25() const { return ___currentLoopProgress_25; }
	inline FsmFloat_t2134102846 ** get_address_of_currentLoopProgress_25() { return &___currentLoopProgress_25; }
	inline void set_currentLoopProgress_25(FsmFloat_t2134102846 * value)
	{
		___currentLoopProgress_25 = value;
		Il2CppCodeGenWriteBarrier(&___currentLoopProgress_25, value);
	}

	inline static int32_t get_offset_of__animator_26() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t3502000247, ____animator_26)); }
	inline Animator_t2776330603 * get__animator_26() const { return ____animator_26; }
	inline Animator_t2776330603 ** get_address_of__animator_26() { return &____animator_26; }
	inline void set__animator_26(Animator_t2776330603 * value)
	{
		____animator_26 = value;
		Il2CppCodeGenWriteBarrier(&____animator_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
