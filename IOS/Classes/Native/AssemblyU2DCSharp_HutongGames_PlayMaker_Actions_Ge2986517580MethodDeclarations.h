﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTag
struct GetTag_t2986517580;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTag::.ctor()
extern "C"  void GetTag__ctor_m3949859226 (GetTag_t2986517580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::Reset()
extern "C"  void GetTag_Reset_m1596292167 (GetTag_t2986517580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnEnter()
extern "C"  void GetTag_OnEnter_m2750322417 (GetTag_t2986517580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnUpdate()
extern "C"  void GetTag_OnUpdate_m2789175666 (GetTag_t2986517580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::DoGetTag()
extern "C"  void GetTag_DoGetTag_m3786719481 (GetTag_t2986517580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
