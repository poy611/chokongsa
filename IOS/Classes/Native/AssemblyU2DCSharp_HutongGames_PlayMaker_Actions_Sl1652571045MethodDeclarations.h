﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Sleep
struct Sleep_t1652571045;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Sleep::.ctor()
extern "C"  void Sleep__ctor_m1499252529 (Sleep_t1652571045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::Reset()
extern "C"  void Sleep_Reset_m3440652766 (Sleep_t1652571045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::OnEnter()
extern "C"  void Sleep_OnEnter_m1359364808 (Sleep_t1652571045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::DoSleep()
extern "C"  void Sleep_DoSleep_m978374363 (Sleep_t1652571045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
