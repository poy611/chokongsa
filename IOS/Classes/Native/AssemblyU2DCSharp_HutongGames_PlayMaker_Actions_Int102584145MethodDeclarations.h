﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntSwitch
struct IntSwitch_t102584145;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntSwitch::.ctor()
extern "C"  void IntSwitch__ctor_m2901762309 (IntSwitch_t102584145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::Reset()
extern "C"  void IntSwitch_Reset_m548195250 (IntSwitch_t102584145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnEnter()
extern "C"  void IntSwitch_OnEnter_m551532444 (IntSwitch_t102584145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnUpdate()
extern "C"  void IntSwitch_OnUpdate_m3346163239 (IntSwitch_t102584145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::DoIntSwitch()
extern "C"  void IntSwitch_DoIntSwitch_m4266271323 (IntSwitch_t102584145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
