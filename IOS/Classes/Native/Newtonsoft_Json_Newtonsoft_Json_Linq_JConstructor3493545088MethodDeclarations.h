﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JConstructor
struct JConstructor_t3493545088;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Linq.JsonLoadSettings
struct JsonLoadSettings_t1368013569;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JConstructor3493545088.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonLoadSetti1368013569.h"

// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::get_ChildrenTokens()
extern "C"  Il2CppObject* JConstructor_get_ChildrenTokens_m2540197 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JConstructor::get_Name()
extern "C"  String_t* JConstructor_get_Name_m1975332574 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JConstructor::get_Type()
extern "C"  int32_t JConstructor_get_Type_m732633023 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(Newtonsoft.Json.Linq.JConstructor)
extern "C"  void JConstructor__ctor_m2384701778 (JConstructor_t3493545088 * __this, JConstructor_t3493545088 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String)
extern "C"  void JConstructor__ctor_m1422594571 (JConstructor_t3493545088 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::CloneToken()
extern "C"  JToken_t3412245951 * JConstructor_CloneToken_m1784051582 (JConstructor_t3493545088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JConstructor::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JConstructor_WriteTo_m2853107302 (JConstructor_t3493545088 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JConstructor_get_Item_m2026403542 (JConstructor_t3493545088 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JConstructor Newtonsoft.Json.Linq.JConstructor::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern "C"  JConstructor_t3493545088 * JConstructor_Load_m4079017163 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, JsonLoadSettings_t1368013569 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
