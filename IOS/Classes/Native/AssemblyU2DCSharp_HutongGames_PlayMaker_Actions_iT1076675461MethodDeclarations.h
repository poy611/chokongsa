﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveFrom
struct iTweenMoveFrom_t1076675461;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::.ctor()
extern "C"  void iTweenMoveFrom__ctor_m70871681 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::Reset()
extern "C"  void iTweenMoveFrom_Reset_m2012271918 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnEnter()
extern "C"  void iTweenMoveFrom_OnEnter_m3074904600 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnExit()
extern "C"  void iTweenMoveFrom_OnExit_m3710340736 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::DoiTween()
extern "C"  void iTweenMoveFrom_DoiTween_m2283894320 (iTweenMoveFrom_t1076675461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
