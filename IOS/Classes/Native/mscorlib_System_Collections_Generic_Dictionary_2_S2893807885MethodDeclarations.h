﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct ShimEnumerator_t2893807885;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1909708961_gshared (ShimEnumerator_t2893807885 * __this, Dictionary_2_t3178029858 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1909708961(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2893807885 *, Dictionary_2_t3178029858 *, const MethodInfo*))ShimEnumerator__ctor_m1909708961_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1793087524_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1793087524(__this, method) ((  bool (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_MoveNext_m1793087524_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2859957638_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2859957638(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_get_Entry_m2859957638_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4283294021_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4283294021(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_get_Key_m4283294021_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2715320983_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2715320983(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_get_Value_m2715320983_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3803006815_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3803006815(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_get_Current_m3803006815_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Reset()
extern "C"  void ShimEnumerator_Reset_m173347187_gshared (ShimEnumerator_t2893807885 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m173347187(__this, method) ((  void (*) (ShimEnumerator_t2893807885 *, const MethodInfo*))ShimEnumerator_Reset_m173347187_gshared)(__this, method)
