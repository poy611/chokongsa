﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCollision2dInfo
struct GetCollision2dInfo_t2518958628;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::.ctor()
extern "C"  void GetCollision2dInfo__ctor_m304751298 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::Reset()
extern "C"  void GetCollision2dInfo_Reset_m2246151535 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::StoreCollisionInfo()
extern "C"  void GetCollision2dInfo_StoreCollisionInfo_m2896075873 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::OnEnter()
extern "C"  void GetCollision2dInfo_OnEnter_m199949849 (GetCollision2dInfo_t2518958628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
