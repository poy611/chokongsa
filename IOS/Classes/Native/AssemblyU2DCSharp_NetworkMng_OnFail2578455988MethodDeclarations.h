﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnFail
struct OnFail_t2578455988;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnFail::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFail__ctor_m3870144395 (OnFail_t2578455988 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnFail::Invoke()
extern "C"  void OnFail_Invoke_m2003516325 (OnFail_t2578455988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnFail::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFail_BeginInvoke_m3478421318 (OnFail_t2578455988 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnFail::EndInvoke(System.IAsyncResult)
extern "C"  void OnFail_EndInvoke_m3322455835 (OnFail_t2578455988 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
