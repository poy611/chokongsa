﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>
struct KeyCollection_t2948752863;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.ReadType>
struct Dictionary_2_t1321993412;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1936929466.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2184009115_gshared (KeyCollection_t2948752863 * __this, Dictionary_2_t1321993412 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2184009115(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2948752863 *, Dictionary_2_t1321993412 *, const MethodInfo*))KeyCollection__ctor_m2184009115_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1310306011_gshared (KeyCollection_t2948752863 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1310306011(__this, ___item0, method) ((  void (*) (KeyCollection_t2948752863 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1310306011_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2267270930_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2267270930(__this, method) ((  void (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2267270930_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2599539923_gshared (KeyCollection_t2948752863 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2599539923(__this, ___item0, method) ((  bool (*) (KeyCollection_t2948752863 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2599539923_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2135121656_gshared (KeyCollection_t2948752863 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2135121656(__this, ___item0, method) ((  bool (*) (KeyCollection_t2948752863 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2135121656_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1967189476_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1967189476(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1967189476_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1652402308_gshared (KeyCollection_t2948752863 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1652402308(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2948752863 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1652402308_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2114461971_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2114461971(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2114461971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1367958388_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1367958388(__this, method) ((  bool (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1367958388_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m966225190_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m966225190(__this, method) ((  bool (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m966225190_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4121561176_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4121561176(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4121561176_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3368020048_gshared (KeyCollection_t2948752863 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3368020048(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2948752863 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3368020048_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::GetEnumerator()
extern "C"  Enumerator_t1936929466  KeyCollection_GetEnumerator_m3284107037_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3284107037(__this, method) ((  Enumerator_t1936929466  (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_GetEnumerator_m3284107037_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.ReadType>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m4169567712_gshared (KeyCollection_t2948752863 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m4169567712(__this, method) ((  int32_t (*) (KeyCollection_t2948752863 *, const MethodInfo*))KeyCollection_get_Count_m4169567712_gshared)(__this, method)
