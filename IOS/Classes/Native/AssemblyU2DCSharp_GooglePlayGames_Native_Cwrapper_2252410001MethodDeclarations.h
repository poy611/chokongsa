﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_Length_m353120500 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_GetElement_m3736469840 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_Variant_m4247830218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t TurnBasedMatchConfig_TurnBasedMatchConfig_ExclusiveBitMask_m3723863422 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatchConfig_TurnBasedMatchConfig_Valid_m3020583066 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MaximumAutomatchingPlayers_m3651177341 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MinimumAutomatchingPlayers_m2446399567 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfig_TurnBasedMatchConfig_Dispose_m2127172169 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
