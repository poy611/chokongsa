﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct KeyCollection_t1931123011;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919299614.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m745756159_gshared (KeyCollection_t1931123011 * __this, Dictionary_2_t304363560 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m745756159(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1931123011 *, Dictionary_2_t304363560 *, const MethodInfo*))KeyCollection__ctor_m745756159_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2745565175_gshared (KeyCollection_t1931123011 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2745565175(__this, ___item0, method) ((  void (*) (KeyCollection_t1931123011 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2745565175_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m512341806_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m512341806(__this, method) ((  void (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m512341806_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1486582003_gshared (KeyCollection_t1931123011 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1486582003(__this, ___item0, method) ((  bool (*) (KeyCollection_t1931123011 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1486582003_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m484803352_gshared (KeyCollection_t1931123011 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m484803352(__this, ___item0, method) ((  bool (*) (KeyCollection_t1931123011 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m484803352_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m557882410_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m557882410(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m557882410_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3734783648_gshared (KeyCollection_t1931123011 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3734783648(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1931123011 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3734783648_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2496615323_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2496615323(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2496615323_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1262253972_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1262253972(__this, method) ((  bool (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1262253972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3022989638_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3022989638(__this, method) ((  bool (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3022989638_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3177161650_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3177161650(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3177161650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4043339956_gshared (KeyCollection_t1931123011 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4043339956(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1931123011 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4043339956_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::GetEnumerator()
extern "C"  Enumerator_t919299614  KeyCollection_GetEnumerator_m211759959_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m211759959(__this, method) ((  Enumerator_t919299614  (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_GetEnumerator_m211759959_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3174139532_gshared (KeyCollection_t1931123011 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3174139532(__this, method) ((  int32_t (*) (KeyCollection_t1931123011 *, const MethodInfo*))KeyCollection_get_Count_m3174139532_gshared)(__this, method)
