﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3693433134_gshared (KeyValuePair_2_t3443564286 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3693433134(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3443564286 *, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))KeyValuePair_2__ctor_m3693433134_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2490056442_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2490056442(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3443564286 *, const MethodInfo*))KeyValuePair_2_get_Key_m2490056442_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4016800827_gshared (KeyValuePair_2_t3443564286 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4016800827(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3443564286 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m4016800827_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Value()
extern "C"  RaycastHit2D_t1374744384  KeyValuePair_2_get_Value_m1935801786_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1935801786(__this, method) ((  RaycastHit2D_t1374744384  (*) (KeyValuePair_2_t3443564286 *, const MethodInfo*))KeyValuePair_2_get_Value_m1935801786_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2107916987_gshared (KeyValuePair_2_t3443564286 * __this, RaycastHit2D_t1374744384  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2107916987(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3443564286 *, RaycastHit2D_t1374744384 , const MethodInfo*))KeyValuePair_2_set_Value_m2107916987_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3008316039_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3008316039(__this, method) ((  String_t* (*) (KeyValuePair_2_t3443564286 *, const MethodInfo*))KeyValuePair_2_ToString_m3008316039_gshared)(__this, method)
