﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ApplicationQuit
struct ApplicationQuit_t3450523853;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::.ctor()
extern "C"  void ApplicationQuit__ctor_m1021639945 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::Reset()
extern "C"  void ApplicationQuit_Reset_m2963040182 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::OnEnter()
extern "C"  void ApplicationQuit_OnEnter_m1935172256 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
