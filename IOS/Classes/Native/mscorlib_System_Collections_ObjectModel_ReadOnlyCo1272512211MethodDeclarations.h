﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>
struct ReadOnlyCollection_1_t1272512211;
// System.Collections.Generic.IList`1<System.IntPtr>
struct IList_1_t2410081878;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t1627299724;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2102343570_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2102343570(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2102343570_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1120489596_gshared (ReadOnlyCollection_1_t1272512211 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1120489596(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1120489596_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2382130126_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2382130126(__this, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2382130126_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1403545699_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, IntPtr_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1403545699(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1403545699_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3880337399_gshared (ReadOnlyCollection_1_t1272512211 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3880337399(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3880337399_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3572365865_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3572365865(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3572365865_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  IntPtr_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1441767021_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1441767021(__this, ___index0, method) ((  IntPtr_t (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1441767021_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1997282426_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, IntPtr_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1997282426(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1997282426_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m366733752_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m366733752(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m366733752_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3893071169_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3893071169(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3893071169_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1648142076_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1648142076(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1648142076_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3730909429_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3730909429(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3730909429_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m741728335_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m741728335(__this, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m741728335_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3610468983_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3610468983(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3610468983_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3058293965_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3058293965(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3058293965_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2370204280_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2370204280(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2370204280_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m816616368_gshared (ReadOnlyCollection_1_t1272512211 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m816616368(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m816616368_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1130659336_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1130659336(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1130659336_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m610454917_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m610454917(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m610454917_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2249964017_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2249964017(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2249964017_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2386032422_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2386032422(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2386032422_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2609187603_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2609187603(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2609187603_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2912482104_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2912482104(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2912482104_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2047598671_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2047598671(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2047598671_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3051966524_gshared (ReadOnlyCollection_1_t1272512211 * __this, IntPtr_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3051966524(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1272512211 *, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3051966524_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m723998636_gshared (ReadOnlyCollection_1_t1272512211 * __this, IntPtrU5BU5D_t3228729122* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m723998636(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1272512211 *, IntPtrU5BU5D_t3228729122*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m723998636_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3248452383_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3248452383(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3248452383_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1863496496_gshared (ReadOnlyCollection_1_t1272512211 * __this, IntPtr_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1863496496(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1272512211 *, IntPtr_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1863496496_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1751124043_gshared (ReadOnlyCollection_1_t1272512211 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1751124043(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1272512211 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1751124043_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::get_Item(System.Int32)
extern "C"  IntPtr_t ReadOnlyCollection_1_get_Item_m3248714413_gshared (ReadOnlyCollection_1_t1272512211 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3248714413(__this, ___index0, method) ((  IntPtr_t (*) (ReadOnlyCollection_1_t1272512211 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3248714413_gshared)(__this, ___index0, method)
