﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleFrom
struct iTweenScaleFrom_t4034512864;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::.ctor()
extern "C"  void iTweenScaleFrom__ctor_m1601129302 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::Reset()
extern "C"  void iTweenScaleFrom_Reset_m3542529539 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnEnter()
extern "C"  void iTweenScaleFrom_OnEnter_m478695853 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnExit()
extern "C"  void iTweenScaleFrom_OnExit_m3903686731 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::DoiTween()
extern "C"  void iTweenScaleFrom_DoiTween_m3405801787 (iTweenScaleFrom_t4034512864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
