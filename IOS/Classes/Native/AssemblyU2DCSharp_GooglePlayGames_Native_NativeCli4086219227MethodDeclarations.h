﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m617187386_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m617187386(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m617187386_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m2704394581_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 * __this, bool ___result0, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m2704394581(__this, ___result0, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 *, bool, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m2704394581_gshared)(__this, ___result0, method)
