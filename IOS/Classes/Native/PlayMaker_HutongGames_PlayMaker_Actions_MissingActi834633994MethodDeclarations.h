﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MissingAction
struct MissingAction_t834633994;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MissingAction::.ctor()
extern "C"  void MissingAction__ctor_m1679191658 (MissingAction_t834633994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
