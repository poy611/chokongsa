﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4
struct U3CFetchListU3Ec__AnonStoreyA4_t1951943870;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse
struct FetchResponse_t3310613493;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3310613493.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4::.ctor()
extern "C"  void U3CFetchListU3Ec__AnonStoreyA4__ctor_m1782388845 (U3CFetchListU3Ec__AnonStoreyA4_t1951943870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/<FetchList>c__AnonStoreyA4::<>m__C9(GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse)
extern "C"  void U3CFetchListU3Ec__AnonStoreyA4_U3CU3Em__C9_m3310571447 (U3CFetchListU3Ec__AnonStoreyA4_t1951943870 * __this, FetchResponse_t3310613493 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
