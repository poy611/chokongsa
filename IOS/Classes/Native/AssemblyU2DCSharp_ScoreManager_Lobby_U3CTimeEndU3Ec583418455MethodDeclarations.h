﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Lobby/<TimeEnd>c__Iterator1F
struct U3CTimeEndU3Ec__Iterator1F_t583418455;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator1F__ctor_m1401349364 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<TimeEnd>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561960734 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<TimeEnd>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m739980466 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Lobby/<TimeEnd>c__Iterator1F::MoveNext()
extern "C"  bool U3CTimeEndU3Ec__Iterator1F_MoveNext_m4156167552 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator1F_Dispose_m2270258225 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::Reset()
extern "C"  void U3CTimeEndU3Ec__Iterator1F_Reset_m3342749601 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
