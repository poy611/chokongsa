﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PopupManager_popupIndex3504285908.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupManager
struct  PopupManager_t2711269761  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Object[] PopupManager::ad
	ObjectU5BU5D_t1015136018* ___ad_3;
	// PopupManager/popupIndex PopupManager::state
	int32_t ___state_4;
	// System.Int32 PopupManager::nowNum
	int32_t ___nowNum_5;
	// StructforMinigame[] PopupManager::stuff
	StructforMinigameU5BU5D_t1843399504* ___stuff_6;
	// UnityEngine.UI.Text PopupManager::tx
	Text_t9039225 * ___tx_7;

public:
	inline static int32_t get_offset_of_ad_3() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___ad_3)); }
	inline ObjectU5BU5D_t1015136018* get_ad_3() const { return ___ad_3; }
	inline ObjectU5BU5D_t1015136018** get_address_of_ad_3() { return &___ad_3; }
	inline void set_ad_3(ObjectU5BU5D_t1015136018* value)
	{
		___ad_3 = value;
		Il2CppCodeGenWriteBarrier(&___ad_3, value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_nowNum_5() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___nowNum_5)); }
	inline int32_t get_nowNum_5() const { return ___nowNum_5; }
	inline int32_t* get_address_of_nowNum_5() { return &___nowNum_5; }
	inline void set_nowNum_5(int32_t value)
	{
		___nowNum_5 = value;
	}

	inline static int32_t get_offset_of_stuff_6() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___stuff_6)); }
	inline StructforMinigameU5BU5D_t1843399504* get_stuff_6() const { return ___stuff_6; }
	inline StructforMinigameU5BU5D_t1843399504** get_address_of_stuff_6() { return &___stuff_6; }
	inline void set_stuff_6(StructforMinigameU5BU5D_t1843399504* value)
	{
		___stuff_6 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_6, value);
	}

	inline static int32_t get_offset_of_tx_7() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761, ___tx_7)); }
	inline Text_t9039225 * get_tx_7() const { return ___tx_7; }
	inline Text_t9039225 ** get_address_of_tx_7() { return &___tx_7; }
	inline void set_tx_7(Text_t9039225 * value)
	{
		___tx_7 = value;
		Il2CppCodeGenWriteBarrier(&___tx_7, value);
	}
};

struct PopupManager_t2711269761_StaticFields
{
public:
	// UnityEngine.Object PopupManager::pm
	Object_t3071478659 * ___pm_2;

public:
	inline static int32_t get_offset_of_pm_2() { return static_cast<int32_t>(offsetof(PopupManager_t2711269761_StaticFields, ___pm_2)); }
	inline Object_t3071478659 * get_pm_2() const { return ___pm_2; }
	inline Object_t3071478659 ** get_address_of_pm_2() { return &___pm_2; }
	inline void set_pm_2(Object_t3071478659 * value)
	{
		___pm_2 = value;
		Il2CppCodeGenWriteBarrier(&___pm_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
