﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t3507996764;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct  GUILayoutAction_t2615417833  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.LayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::layoutOptions
	LayoutOptionU5BU5D_t3507996764* ___layoutOptions_11;
	// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::options
	GUILayoutOptionU5BU5D_t2977405297* ___options_12;

public:
	inline static int32_t get_offset_of_layoutOptions_11() { return static_cast<int32_t>(offsetof(GUILayoutAction_t2615417833, ___layoutOptions_11)); }
	inline LayoutOptionU5BU5D_t3507996764* get_layoutOptions_11() const { return ___layoutOptions_11; }
	inline LayoutOptionU5BU5D_t3507996764** get_address_of_layoutOptions_11() { return &___layoutOptions_11; }
	inline void set_layoutOptions_11(LayoutOptionU5BU5D_t3507996764* value)
	{
		___layoutOptions_11 = value;
		Il2CppCodeGenWriteBarrier(&___layoutOptions_11, value);
	}

	inline static int32_t get_offset_of_options_12() { return static_cast<int32_t>(offsetof(GUILayoutAction_t2615417833, ___options_12)); }
	inline GUILayoutOptionU5BU5D_t2977405297* get_options_12() const { return ___options_12; }
	inline GUILayoutOptionU5BU5D_t2977405297** get_address_of_options_12() { return &___options_12; }
	inline void set_options_12(GUILayoutOptionU5BU5D_t2977405297* value)
	{
		___options_12 = value;
		Il2CppCodeGenWriteBarrier(&___options_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
