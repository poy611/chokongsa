﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayDeleteAt
struct ArrayDeleteAt_t2681271941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::.ctor()
extern "C"  void ArrayDeleteAt__ctor_m4180336721 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::Reset()
extern "C"  void ArrayDeleteAt_Reset_m1826769662 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::OnEnter()
extern "C"  void ArrayDeleteAt_OnEnter_m900895720 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::DoDeleteAt()
extern "C"  void ArrayDeleteAt_DoDeleteAt_m3047409660 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
