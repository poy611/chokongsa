﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextRayCast2d
struct GetNextRayCast2d_t3555256570;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::.ctor()
extern "C"  void GetNextRayCast2d__ctor_m1196257964 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::Reset()
extern "C"  void GetNextRayCast2d_Reset_m3137658201 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::OnEnter()
extern "C"  void GetNextRayCast2d_OnEnter_m2239363971 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::DoGetNextCollider()
extern "C"  void GetNextRayCast2d_DoGetNextCollider_m2227714108 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextRayCast2d::GetRayCastAll()
extern "C"  RaycastHit2DU5BU5D_t889400257* GetNextRayCast2d_GetRayCastAll_m3590188560 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
