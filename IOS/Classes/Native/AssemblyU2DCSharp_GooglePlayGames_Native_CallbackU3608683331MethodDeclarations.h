﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_Q935666006.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m2174576724_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m2174576724(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m2174576724_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 * __this, int32_t ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264(__this, ___val10, ___val21, ___val32, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264_gshared)(__this, ___val10, ___val21, ___val32, method)
