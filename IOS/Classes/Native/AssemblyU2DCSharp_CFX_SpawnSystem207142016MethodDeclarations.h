﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_SpawnSystem
struct CFX_SpawnSystem_t207142016;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void CFX_SpawnSystem::.ctor()
extern "C"  void CFX_SpawnSystem__ctor_m1016797931 (CFX_SpawnSystem_t207142016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_SpawnSystem::GetNextObject(UnityEngine.GameObject,System.Boolean)
extern "C"  GameObject_t3674682005 * CFX_SpawnSystem_GetNextObject_m450460671 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___sourceObj0, bool ___activateObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::PreloadObject(UnityEngine.GameObject,System.Int32)
extern "C"  void CFX_SpawnSystem_PreloadObject_m4259383822 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___sourceObj0, int32_t ___poolSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::UnloadObjects(UnityEngine.GameObject)
extern "C"  void CFX_SpawnSystem_UnloadObjects_m1825541334 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___sourceObj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_SpawnSystem::get_AllObjectsLoaded()
extern "C"  bool CFX_SpawnSystem_get_AllObjectsLoaded_m2308961126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::addObjectToPool(UnityEngine.GameObject,System.Int32)
extern "C"  void CFX_SpawnSystem_addObjectToPool_m3769595743 (CFX_SpawnSystem_t207142016 * __this, GameObject_t3674682005 * ___sourceObject0, int32_t ___number1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::removeObjectsFromPool(UnityEngine.GameObject)
extern "C"  void CFX_SpawnSystem_removeObjectsFromPool_m4061907927 (CFX_SpawnSystem_t207142016 * __this, GameObject_t3674682005 * ___sourceObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::increasePoolCursor(System.Int32)
extern "C"  void CFX_SpawnSystem_increasePoolCursor_m2831376926 (CFX_SpawnSystem_t207142016 * __this, int32_t ___uniqueId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Awake()
extern "C"  void CFX_SpawnSystem_Awake_m1254403150 (CFX_SpawnSystem_t207142016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_SpawnSystem::Start()
extern "C"  void CFX_SpawnSystem_Start_m4258903019 (CFX_SpawnSystem_t207142016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
