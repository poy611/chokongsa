﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// SoundManage
struct SoundManage_t3403985716;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManage
struct  SoundManage_t3403985716  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AudioSource SoundManage::efxSource
	AudioSource_t1740077639 * ___efxSource_2;
	// UnityEngine.AudioSource SoundManage::musicSource
	AudioSource_t1740077639 * ___musicSource_3;
	// UnityEngine.AudioClip SoundManage::_bgmTitle
	AudioClip_t794140988 * ____bgmTitle_5;
	// UnityEngine.AudioClip SoundManage::_bgmLobby
	AudioClip_t794140988 * ____bgmLobby_6;
	// UnityEngine.AudioClip SoundManage::_bgmGrinding
	AudioClip_t794140988 * ____bgmGrinding_7;
	// UnityEngine.AudioClip SoundManage::_bgmVariaion
	AudioClip_t794140988 * ____bgmVariaion_8;
	// UnityEngine.AudioClip SoundManage::_bgmTemping
	AudioClip_t794140988 * ____bgmTemping_9;
	// UnityEngine.AudioClip SoundManage::_bgmIcing
	AudioClip_t794140988 * ____bgmIcing_10;
	// UnityEngine.AudioClip SoundManage::_bgmLearn
	AudioClip_t794140988 * ____bgmLearn_11;
	// UnityEngine.AudioClip SoundManage::_bgmMinigame
	AudioClip_t794140988 * ____bgmMinigame_12;
	// UnityEngine.AudioClip SoundManage::_bgm
	AudioClip_t794140988 * ____bgm_13;
	// UnityEngine.AudioClip SoundManage::_efxBtn
	AudioClip_t794140988 * ____efxBtn_14;
	// UnityEngine.AudioClip SoundManage::_efxOk
	AudioClip_t794140988 * ____efxOk_15;
	// UnityEngine.AudioClip SoundManage::_efxBad
	AudioClip_t794140988 * ____efxBad_16;
	// System.Boolean SoundManage::isPlaying
	bool ___isPlaying_17;

public:
	inline static int32_t get_offset_of_efxSource_2() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ___efxSource_2)); }
	inline AudioSource_t1740077639 * get_efxSource_2() const { return ___efxSource_2; }
	inline AudioSource_t1740077639 ** get_address_of_efxSource_2() { return &___efxSource_2; }
	inline void set_efxSource_2(AudioSource_t1740077639 * value)
	{
		___efxSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___efxSource_2, value);
	}

	inline static int32_t get_offset_of_musicSource_3() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ___musicSource_3)); }
	inline AudioSource_t1740077639 * get_musicSource_3() const { return ___musicSource_3; }
	inline AudioSource_t1740077639 ** get_address_of_musicSource_3() { return &___musicSource_3; }
	inline void set_musicSource_3(AudioSource_t1740077639 * value)
	{
		___musicSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___musicSource_3, value);
	}

	inline static int32_t get_offset_of__bgmTitle_5() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmTitle_5)); }
	inline AudioClip_t794140988 * get__bgmTitle_5() const { return ____bgmTitle_5; }
	inline AudioClip_t794140988 ** get_address_of__bgmTitle_5() { return &____bgmTitle_5; }
	inline void set__bgmTitle_5(AudioClip_t794140988 * value)
	{
		____bgmTitle_5 = value;
		Il2CppCodeGenWriteBarrier(&____bgmTitle_5, value);
	}

	inline static int32_t get_offset_of__bgmLobby_6() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmLobby_6)); }
	inline AudioClip_t794140988 * get__bgmLobby_6() const { return ____bgmLobby_6; }
	inline AudioClip_t794140988 ** get_address_of__bgmLobby_6() { return &____bgmLobby_6; }
	inline void set__bgmLobby_6(AudioClip_t794140988 * value)
	{
		____bgmLobby_6 = value;
		Il2CppCodeGenWriteBarrier(&____bgmLobby_6, value);
	}

	inline static int32_t get_offset_of__bgmGrinding_7() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmGrinding_7)); }
	inline AudioClip_t794140988 * get__bgmGrinding_7() const { return ____bgmGrinding_7; }
	inline AudioClip_t794140988 ** get_address_of__bgmGrinding_7() { return &____bgmGrinding_7; }
	inline void set__bgmGrinding_7(AudioClip_t794140988 * value)
	{
		____bgmGrinding_7 = value;
		Il2CppCodeGenWriteBarrier(&____bgmGrinding_7, value);
	}

	inline static int32_t get_offset_of__bgmVariaion_8() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmVariaion_8)); }
	inline AudioClip_t794140988 * get__bgmVariaion_8() const { return ____bgmVariaion_8; }
	inline AudioClip_t794140988 ** get_address_of__bgmVariaion_8() { return &____bgmVariaion_8; }
	inline void set__bgmVariaion_8(AudioClip_t794140988 * value)
	{
		____bgmVariaion_8 = value;
		Il2CppCodeGenWriteBarrier(&____bgmVariaion_8, value);
	}

	inline static int32_t get_offset_of__bgmTemping_9() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmTemping_9)); }
	inline AudioClip_t794140988 * get__bgmTemping_9() const { return ____bgmTemping_9; }
	inline AudioClip_t794140988 ** get_address_of__bgmTemping_9() { return &____bgmTemping_9; }
	inline void set__bgmTemping_9(AudioClip_t794140988 * value)
	{
		____bgmTemping_9 = value;
		Il2CppCodeGenWriteBarrier(&____bgmTemping_9, value);
	}

	inline static int32_t get_offset_of__bgmIcing_10() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmIcing_10)); }
	inline AudioClip_t794140988 * get__bgmIcing_10() const { return ____bgmIcing_10; }
	inline AudioClip_t794140988 ** get_address_of__bgmIcing_10() { return &____bgmIcing_10; }
	inline void set__bgmIcing_10(AudioClip_t794140988 * value)
	{
		____bgmIcing_10 = value;
		Il2CppCodeGenWriteBarrier(&____bgmIcing_10, value);
	}

	inline static int32_t get_offset_of__bgmLearn_11() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmLearn_11)); }
	inline AudioClip_t794140988 * get__bgmLearn_11() const { return ____bgmLearn_11; }
	inline AudioClip_t794140988 ** get_address_of__bgmLearn_11() { return &____bgmLearn_11; }
	inline void set__bgmLearn_11(AudioClip_t794140988 * value)
	{
		____bgmLearn_11 = value;
		Il2CppCodeGenWriteBarrier(&____bgmLearn_11, value);
	}

	inline static int32_t get_offset_of__bgmMinigame_12() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgmMinigame_12)); }
	inline AudioClip_t794140988 * get__bgmMinigame_12() const { return ____bgmMinigame_12; }
	inline AudioClip_t794140988 ** get_address_of__bgmMinigame_12() { return &____bgmMinigame_12; }
	inline void set__bgmMinigame_12(AudioClip_t794140988 * value)
	{
		____bgmMinigame_12 = value;
		Il2CppCodeGenWriteBarrier(&____bgmMinigame_12, value);
	}

	inline static int32_t get_offset_of__bgm_13() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____bgm_13)); }
	inline AudioClip_t794140988 * get__bgm_13() const { return ____bgm_13; }
	inline AudioClip_t794140988 ** get_address_of__bgm_13() { return &____bgm_13; }
	inline void set__bgm_13(AudioClip_t794140988 * value)
	{
		____bgm_13 = value;
		Il2CppCodeGenWriteBarrier(&____bgm_13, value);
	}

	inline static int32_t get_offset_of__efxBtn_14() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____efxBtn_14)); }
	inline AudioClip_t794140988 * get__efxBtn_14() const { return ____efxBtn_14; }
	inline AudioClip_t794140988 ** get_address_of__efxBtn_14() { return &____efxBtn_14; }
	inline void set__efxBtn_14(AudioClip_t794140988 * value)
	{
		____efxBtn_14 = value;
		Il2CppCodeGenWriteBarrier(&____efxBtn_14, value);
	}

	inline static int32_t get_offset_of__efxOk_15() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____efxOk_15)); }
	inline AudioClip_t794140988 * get__efxOk_15() const { return ____efxOk_15; }
	inline AudioClip_t794140988 ** get_address_of__efxOk_15() { return &____efxOk_15; }
	inline void set__efxOk_15(AudioClip_t794140988 * value)
	{
		____efxOk_15 = value;
		Il2CppCodeGenWriteBarrier(&____efxOk_15, value);
	}

	inline static int32_t get_offset_of__efxBad_16() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ____efxBad_16)); }
	inline AudioClip_t794140988 * get__efxBad_16() const { return ____efxBad_16; }
	inline AudioClip_t794140988 ** get_address_of__efxBad_16() { return &____efxBad_16; }
	inline void set__efxBad_16(AudioClip_t794140988 * value)
	{
		____efxBad_16 = value;
		Il2CppCodeGenWriteBarrier(&____efxBad_16, value);
	}

	inline static int32_t get_offset_of_isPlaying_17() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716, ___isPlaying_17)); }
	inline bool get_isPlaying_17() const { return ___isPlaying_17; }
	inline bool* get_address_of_isPlaying_17() { return &___isPlaying_17; }
	inline void set_isPlaying_17(bool value)
	{
		___isPlaying_17 = value;
	}
};

struct SoundManage_t3403985716_StaticFields
{
public:
	// SoundManage SoundManage::instance
	SoundManage_t3403985716 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(SoundManage_t3403985716_StaticFields, ___instance_4)); }
	inline SoundManage_t3403985716 * get_instance_4() const { return ___instance_4; }
	inline SoundManage_t3403985716 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SoundManage_t3403985716 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier(&___instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
