﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey95
struct U3CFinishU3Ec__AnonStorey95_t3948912648;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey95::.ctor()
extern "C"  void U3CFinishU3Ec__AnonStorey95__ctor_m926707811 (U3CFinishU3Ec__AnonStorey95_t3948912648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey95::<>m__84(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CFinishU3Ec__AnonStorey95_U3CU3Em__84_m2720626209 (U3CFinishU3Ec__AnonStorey95_t3948912648 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey95::<>m__8E(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void U3CFinishU3Ec__AnonStorey95_U3CU3Em__8E_m3545308404 (U3CFinishU3Ec__AnonStorey95_t3948912648 * __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
