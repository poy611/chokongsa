﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Serving/<onStart>c__Iterator28
struct U3ConStartU3Ec__Iterator28_t3665343203;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Serving/<onStart>c__Iterator28::.ctor()
extern "C"  void U3ConStartU3Ec__Iterator28__ctor_m2034858264 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<onStart>c__Iterator28::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ConStartU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2816612612 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<onStart>c__Iterator28::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ConStartU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2781783192 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Serving/<onStart>c__Iterator28::MoveNext()
extern "C"  bool U3ConStartU3Ec__Iterator28_MoveNext_m3212694212 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<onStart>c__Iterator28::Dispose()
extern "C"  void U3ConStartU3Ec__Iterator28_Dispose_m1186955093 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<onStart>c__Iterator28::Reset()
extern "C"  void U3ConStartU3Ec__Iterator28_Reset_m3976258501 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
