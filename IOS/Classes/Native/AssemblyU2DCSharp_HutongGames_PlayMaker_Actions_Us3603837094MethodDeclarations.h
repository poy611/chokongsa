﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.UseGUILayout
struct UseGUILayout_t3603837094;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.UseGUILayout::.ctor()
extern "C"  void UseGUILayout__ctor_m1095096448 (UseGUILayout_t3603837094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGUILayout::Reset()
extern "C"  void UseGUILayout_Reset_m3036496685 (UseGUILayout_t3603837094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGUILayout::OnEnter()
extern "C"  void UseGUILayout_OnEnter_m3807394903 (UseGUILayout_t3603837094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
