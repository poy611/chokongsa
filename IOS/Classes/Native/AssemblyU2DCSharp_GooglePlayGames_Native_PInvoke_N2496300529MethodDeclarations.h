﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone
struct IQuestMilestone_t3485030629;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3806932551.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeQuest::.ctor(System.IntPtr)
extern "C"  void NativeQuest__ctor_m1629635809 (NativeQuest_t2496300529 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::get_Id()
extern "C"  String_t* NativeQuest_get_Id_m3068462296 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::get_Name()
extern "C"  String_t* NativeQuest_get_Name_m2587191112 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::get_Description()
extern "C"  String_t* NativeQuest_get_Description_m1740249089 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::get_BannerUrl()
extern "C"  String_t* NativeQuest_get_BannerUrl_m431465960 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::get_IconUrl()
extern "C"  String_t* NativeQuest_get_IconUrl_m3536789947 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.PInvoke.NativeQuest::get_StartTime()
extern "C"  DateTime_t4283661327  NativeQuest_get_StartTime_m3221226014 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.PInvoke.NativeQuest::get_ExpirationTime()
extern "C"  DateTime_t4283661327  NativeQuest_get_ExpirationTime_m1260695119 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> GooglePlayGames.Native.PInvoke.NativeQuest::get_AcceptedTime()
extern "C"  Nullable_1_t72820554  NativeQuest_get_AcceptedTime_m1946170684 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone GooglePlayGames.Native.PInvoke.NativeQuest::get_Milestone()
extern "C"  Il2CppObject * NativeQuest_get_Milestone_m46119047 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.QuestState GooglePlayGames.Native.PInvoke.NativeQuest::get_State()
extern "C"  int32_t NativeQuest_get_State_m1583722784 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativeQuest::Valid()
extern "C"  bool NativeQuest_Valid_m172343609 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeQuest::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeQuest_CallDispose_m956262639 (NativeQuest_t2496300529 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuest::ToString()
extern "C"  String_t* NativeQuest_ToString_m1948916192 (NativeQuest_t2496300529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.NativeQuest::FromPointer(System.IntPtr)
extern "C"  NativeQuest_t2496300529 * NativeQuest_FromPointer_m2241315429 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuest::<get_Id>m__B2(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuest_U3Cget_IdU3Em__B2_m3124906223 (NativeQuest_t2496300529 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuest::<get_Name>m__B3(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuest_U3Cget_NameU3Em__B3_m1337581024 (NativeQuest_t2496300529 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuest::<get_Description>m__B4(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuest_U3Cget_DescriptionU3Em__B4_m1370947352 (NativeQuest_t2496300529 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuest::<get_BannerUrl>m__B5(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuest_U3Cget_BannerUrlU3Em__B5_m298090304 (NativeQuest_t2496300529 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuest::<get_IconUrl>m__B6(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuest_U3Cget_IconUrlU3Em__B6_m752705620 (NativeQuest_t2496300529 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
