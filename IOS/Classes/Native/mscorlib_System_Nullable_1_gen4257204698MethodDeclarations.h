﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen4257204698.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonToken4173078175.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.JsonToken>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3314382634_gshared (Nullable_1_t4257204698 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3314382634(__this, ___value0, method) ((  void (*) (Nullable_1_t4257204698 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3314382634_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1858475733_gshared (Nullable_1_t4257204698 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1858475733(__this, method) ((  bool (*) (Nullable_1_t4257204698 *, const MethodInfo*))Nullable_1_get_HasValue_m1858475733_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1763091084_gshared (Nullable_1_t4257204698 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1763091084(__this, method) ((  int32_t (*) (Nullable_1_t4257204698 *, const MethodInfo*))Nullable_1_get_Value_m1763091084_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2418757108_gshared (Nullable_1_t4257204698 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2418757108(__this, ___other0, method) ((  bool (*) (Nullable_1_t4257204698 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2418757108_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonToken>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m169110219_gshared (Nullable_1_t4257204698 * __this, Nullable_1_t4257204698  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m169110219(__this, ___other0, method) ((  bool (*) (Nullable_1_t4257204698 *, Nullable_1_t4257204698 , const MethodInfo*))Nullable_1_Equals_m169110219_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.JsonToken>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3350722136_gshared (Nullable_1_t4257204698 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3350722136(__this, method) ((  int32_t (*) (Nullable_1_t4257204698 *, const MethodInfo*))Nullable_1_GetHashCode_m3350722136_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1227209046_gshared (Nullable_1_t4257204698 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1227209046(__this, method) ((  int32_t (*) (Nullable_1_t4257204698 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1227209046_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonToken>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m769422167_gshared (Nullable_1_t4257204698 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m769422167(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t4257204698 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m769422167_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.JsonToken>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1611403502_gshared (Nullable_1_t4257204698 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1611403502(__this, method) ((  String_t* (*) (Nullable_1_t4257204698 *, const MethodInfo*))Nullable_1_ToString_m1611403502_gshared)(__this, method)
