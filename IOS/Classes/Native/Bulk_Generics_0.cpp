﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366;
// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504;
// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct Action_1_t3953224079;
// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403;
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178;
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Boolean,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<GooglePlayGames.BasicApi.UIStatus,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t3522153312;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Boolean,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t317199770;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625;
// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>
struct U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301;
// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>
struct U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator5_1_t3982122310;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>
struct OutMethod_1_t2402168711;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>
struct OutMethod_1_t3549961022;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>
struct OutMethod_1_t3710375422;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>
struct ComponentAction_1_t3146284570;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GUIText
struct GUIText_t3371372606;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.Light
struct Light_t4202674828;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// JsonFx.Json.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t3884844371;
// JsonFx.Json.JsonWriter
struct JsonWriter_t3589747297;
// JsonFx.Json.WriteDelegate`1<System.Object>
struct WriteDelegate_1_t3771999415;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>
struct BidirectionalDictionary_2_t1375314390;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.String
struct String_t;
// Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>
struct CollectionWrapper_1_t3035453308;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Array
struct Il2CppArray;
// Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>
struct U3CU3Ec_t1205074931;
// Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>
struct DictionaryWrapper_2_t3980492226;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;
// System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>
struct IEnumerable_1_t757552275;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t950614638;
// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Func_2_t1623933992;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.ICollection
struct ICollection_t2643922881;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>
struct U3CU3Ec__DisplayClass4_0_1_t3696729747;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>
struct U3CU3Ec__DisplayClass5_0_1_t1229468012;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass6_0`1<System.Object>
struct U3CU3Ec__DisplayClass6_0_1_t3057173573;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>
struct U3CU3Ec__DisplayClass7_0_1_t589911838;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass8_0`1<System.Object>
struct U3CU3Ec__DisplayClass8_0_1_t2417617399;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>
struct U3CU3Ec__DisplayClass9_0_1_t4245322960;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1809280638;
// Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>
struct U3CU3Ec__DisplayClass15_0_1_t118279205;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ThreadSafeStore_2_t648438005;
// System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Func_2_t2253398629;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct ThreadSafeStore_2_t3745804690;
// System.Func`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Func_2_t1055798018;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>
struct ThreadSafeStore_2_t2874570697;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// Singleton`1<System.Object>
struct Singleton_1_t128664468;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
struct Action_1_t464558559;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>
struct Action_1_t260648783;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
struct Action_1_t380876779;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t914279838;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t823521528;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
struct Action_1_t3071188627;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>
struct Action_1_t150760668;
// System.Action`1<System.IntPtr>
struct Action_1_t111250811;
// System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.Object>
struct Action_2_t1729022911;
// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct Action_2_t1524908924;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct Action_2_t3406652893;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct Action_2_t2661426558;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>
struct Action_2_t1740094460;
// System.Action`2<GooglePlayGames.BasicApi.UIStatus,System.Object>
struct Action_2_t1255729854;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2345743608;
// System.Action`2<System.Object,System.Boolean>
struct Action_2_t599046810;
// System.Action`2<System.Object,System.IntPtr>
struct Action_2_t4132650063;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>
struct Action_3_t708612388;
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>
struct Action_3_t178818711;
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.Object,System.Object>
struct Action_3_t4239861309;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t2968268857;
// System.Action`4<System.Object,System.Object,System.Object,System.Boolean>
struct Action_4_t2331760108;
// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t1730810465;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU2768147670.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU2768147670MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1866455366.h"
#include "mscorlib_System_Action_1_gen271665211.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Action_1_gen271665211MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1866455366MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame727662960MethodDeclarations.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUti13892788.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUti13892788MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1259290051.h"
#include "System_Core_System_Action_2_gen1524908924.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3857037258.h"
#include "System_Core_System_Action_2_gen1524908924MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1895636757.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1895636757MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3141034020.h"
#include "System_Core_System_Action_2_gen3406652893.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"
#include "System_Core_System_Action_2_gen3406652893MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU2782048327.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU2782048327MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU4027445590.h"
#include "System_Core_System_Action_2_gen4293064463.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1259290051MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3141034020MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU4027445590MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1154936577.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1154936577MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3608683331.h"
#include "System_Core_System_Action_3_gen708612388.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_Q935666006.h"
#include "System_Core_System_Action_3_gen708612388MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUt625142900.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUt625142900MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3078889654.h"
#include "System_Core_System_Action_3_gen178818711.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3187252993.h"
#include "System_Core_System_Action_3_gen178818711MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3414593046.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3414593046MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1573372504.h"
#include "System_Core_System_Action_3_gen2968268857.h"
#include "System_Core_System_Action_3_gen2968268857MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3608683331MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU3078889654MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackU1573372504MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli2871861156.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli2871861156MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Action_1_gen3953224079.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3798002602MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3798002602.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli4086219227.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli4086219227MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3485269584.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3485269584MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3940624403.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3940624403MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger984534948MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Action_1_gen3953224079MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClie860015178.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClie860015178MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClie259065535.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClie259065535MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSave147574175.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSave147574175MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSave729841739.h"
#include "System_Core_System_Action_2_gen2661426558.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa3786215536.h"
#include "System_Core_System_Action_2_gen2661426558MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav3521209373.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav3521209373MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav4103476937.h"
#include "System_Core_System_Action_2_gen1740094460.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4210182474.h"
#include "System_Core_System_Action_2_gen1740094460MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav1779212080.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav1779212080MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav2361479644.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSave729841739MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav4103476937MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSav2361479644MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3939258807.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3939258807MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2620762138.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3338309164.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3338309164MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2019812495.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2620762138MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2019812495MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C4039120987.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C4039120987MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3522153312.h"
#include "System_Core_System_Action_2_gen1255729854.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "System_Core_System_Action_2_gen1255729854MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Ca834167445.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Ca834167445MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Ca317199770.h"
#include "System_Core_System_Action_2_gen2345743608.h"
#include "System_Core_System_Action_2_gen2345743608MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3382437943.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3382437943MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2865470268.h"
#include "System_Core_System_Action_2_gen599046810.h"
#include "System_Core_System_Action_2_gen599046810MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2781488300.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2781488300MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2264520625.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3522153312MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Ca317199770MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2865470268MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C2264520625MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C4197075301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C4197075301MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2304636665.h"
#include "System_Core_System_Func_2_gen2304636665MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3071445088.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3071445088MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3982122310.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3982122310MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "System_Core_System_Func_2_gen2301125574.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_UIntPtr3365854250MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2301125574MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2402168711.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2402168711MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3549961022.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3549961022MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3710375422.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3710375422MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3146284570.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3146284570MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_Light4202674828.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_Utility_Arrays_1_gen792694677.h"
#include "PlayMaker_HutongGames_Utility_Arrays_1_gen792694677MethodDeclarations.h"
#include "PlayMaker_HutongGames_Utility_Lists_1_gen4287567192.h"
#include "PlayMaker_HutongGames_Utility_Lists_1_gen4287567192MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3884844371.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3884844371MethodDeclarations.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriter3589747297.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3771999415.h"
#include "JsonFx_Json_JsonFx_Json_WriteDelegate_1_gen3771999415MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JEnumerable_1_971832625.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JEnumerable_1_971832625MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Validati3410775990MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Cach1288273172.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Cach1288273172MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4005035940.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4005035940MethodDeclarations.h"
#include "System_Core_System_Func_2_gen184564025.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau362240250.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau362240250MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Obje2948332186.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Obje2948332186MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Bidirect1375314390.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Bidirect1375314390MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2045888271MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringUt3446198942MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Collecti3035453308.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Collecti3035453308MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeExten889461758MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3905956676MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona1205074931.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona1205074931MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona4062924206.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona4062924206MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona3980492226.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona3980492226MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1623933992.h"
#include "System_Core_System_Func_2_gen1623933992MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun3696729747.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun3696729747MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun1229468012.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun1229468012MethodDeclarations.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun3057173573.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun3057173573MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBound589911838.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBound589911838MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun2417617399.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun2417617399MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun4245322960.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun4245322960MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_MethodCa1809280638.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_MethodCa1809280638MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringUti118279205.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringUti118279205MethodDeclarations.h"
#include "System_Core_System_Func_2_gen315946507.h"
#include "System_Core_System_Func_2_gen315946507MethodDeclarations.h"
#include "mscorlib_System_StringComparison4173268078.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSaf648438005.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSaf648438005MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2253398629.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4114722875.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4114722875MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Threading_Monitor2734674376MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread1973216770MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2253398629MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa3745804690.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa3745804690MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1055798018.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2917122264.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2917122264MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "System_Core_System_Func_2_gen1055798018MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen464558559.h"
#include "mscorlib_System_Action_1_gen464558559MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Ad68742423.h"
#include "mscorlib_System_Action_1_gen260648783.h"
#include "mscorlib_System_Action_1_gen260648783MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_4159799943.h"
#include "mscorlib_System_Action_1_gen380876779.h"
#include "mscorlib_System_Action_1_gen380876779MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_4280027939.h"
#include "mscorlib_System_Action_1_gen914279838.h"
#include "mscorlib_System_Action_1_gen914279838MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_I518463702.h"
#include "mscorlib_System_Action_1_gen823521528.h"
#include "mscorlib_System_Action_1_gen823521528MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3071188627.h"
#include "mscorlib_System_Action_1_gen3071188627MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "mscorlib_System_Action_1_gen150760668.h"
#include "mscorlib_System_Action_1_gen150760668MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Action_1_gen111250811.h"
#include "mscorlib_System_Action_1_gen111250811MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1729022911.h"
#include "System_Core_System_Action_2_gen1729022911MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "System_Core_System_Action_2_gen4132650063.h"
#include "System_Core_System_Action_2_gen4132650063MethodDeclarations.h"
#include "System_Core_System_Action_3_gen4239861309.h"
#include "System_Core_System_Action_3_gen4239861309MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2613789975.h"
#include "System_Core_System_Action_4_gen2331760108.h"
#include "System_Core_System_Action_4_gen2331760108MethodDeclarations.h"
#include "System_Core_System_Action_4_gen1730810465.h"
#include "System_Core_System_Action_4_gen1730810465MethodDeclarations.h"

// System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>(System.Action`1<!!0>,!!0)
extern "C"  void NativeClient_InvokeCallbackOnGameThread_TisUIStatus_t3557407943_m582631075_gshared (Il2CppObject * __this /* static, unused */, Action_1_t3953224079 * p0, int32_t p1, const MethodInfo* method);
#define NativeClient_InvokeCallbackOnGameThread_TisUIStatus_t3557407943_m582631075(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Action_1_t3953224079 *, int32_t, const MethodInfo*))NativeClient_InvokeCallbackOnGameThread_TisUIStatus_t3557407943_m582631075_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Boolean>(System.Action`1<!!0>,!!0)
extern "C"  void NativeClient_InvokeCallbackOnGameThread_TisBoolean_t476798718_m2549550913_gshared (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * p0, bool p1, const MethodInfo* method);
#define NativeClient_InvokeCallbackOnGameThread_TisBoolean_t476798718_m2549550913(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Action_1_t872614854 *, bool, const MethodInfo*))NativeClient_InvokeCallbackOnGameThread_TisBoolean_t476798718_m2549550913_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Object>(System.Action`1<!!0>,!!0)
extern "C"  void NativeClient_InvokeCallbackOnGameThread_TisIl2CppObject_m1753332582_gshared (Il2CppObject * __this /* static, unused */, Action_1_t271665211 * p0, Il2CppObject * p1, const MethodInfo* method);
#define NativeClient_InvokeCallbackOnGameThread_TisIl2CppObject_m1753332582(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))NativeClient_InvokeCallbackOnGameThread_TisIl2CppObject_m1753332582_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
extern "C"  Il2CppObject* Enumerable_Empty_TisIl2CppObject_m2029158076_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerable_Empty_TisIl2CppObject_m2029158076(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m2029158076_gshared)(__this /* static, unused */, method)
// T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute<System.Object>(System.Object)
extern "C"  Il2CppObject * JsonTypeReflector_GetAttribute_TisIl2CppObject_m4177715532_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___provider0, const MethodInfo* method);
#define JsonTypeReflector_GetAttribute_TisIl2CppObject_m4177715532(__this /* static, unused */, ___provider0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))JsonTypeReflector_GetAttribute_TisIl2CppObject_m4177715532_gshared)(__this /* static, unused */, ___provider0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisIl2CppObject_m1853624565_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisIl2CppObject_m1853624565(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m1853624565_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1244034627 * Enumerable_ToList_TisIl2CppObject_m3795766066_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3795766066(__this /* static, unused */, p0, method) ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3795766066_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Collections.DictionaryEntry>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisDictionaryEntry_t1751606614_TisKeyValuePair_2_t1944668977_m1353638537_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1623933992 * p1, const MethodInfo* method);
#define Enumerable_Select_TisDictionaryEntry_t1751606614_TisKeyValuePair_2_t1944668977_m1353638537(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1623933992 *, const MethodInfo*))Enumerable_Select_TisDictionaryEntry_t1751606614_TisKeyValuePair_2_t1944668977_m1353638537_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3F_1__ctor_m3926717341_gshared (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1/<ToOnGameThread>c__AnonStorey3F`1<System.Object>::<>m__13()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3F_1_U3CU3Em__13_m607154376_gshared (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 *)__this->get_U3CU3Ef__refU2462_1();
		NullCheck(L_0);
		Action_1_t271665211 * L_1 = (Action_1_t271665211 *)L_0->get_toConvert_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_val_0();
		NullCheck((Action_1_t271665211 *)L_1);
		((  void (*) (Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t271665211 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3E_1__ctor_m2917705613_gshared (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>::<>m__E(T)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277_gshared (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2462_1(__this);
		U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * L_2 = V_0;
		Il2CppObject * L_3 = ___val0;
		NullCheck(L_2);
		L_2->set_val_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey3F_1_t2768147670 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_6 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m4133136942_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::<>m__14()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m1407896282_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 *)__this->get_U3CU3Ef__refU2464_2();
		NullCheck(L_0);
		Action_2_t1524908924 * L_1 = (Action_2_t1524908924 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t1524908924 *)L_1);
		((  void (*) (Action_2_t1524908924 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t1524908924 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m3213873058_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::<>m__14()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2758566734_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 *)__this->get_U3CU3Ef__refU2464_2();
		NullCheck(L_0);
		Action_2_t3406652893 * L_1 = (Action_2_t3406652893 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t3406652893 *)L_1);
		((  void (*) (Action_2_t3406652893 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t3406652893 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m706101797_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<System.Object,System.Object>::<>m__14()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2267037969_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 *)__this->get_U3CU3Ef__refU2464_2();
		NullCheck(L_0);
		Action_2_t4293064463 * L_1 = (Action_2_t4293064463 *)L_0->get_toConvert_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t4293064463 *)L_1);
		((  void (*) (Action_2_t4293064463 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t4293064463 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m261560595_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::<>m__10(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1524650922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2464_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m2272897223_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::<>m__10(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t3141034020 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1931637110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2464_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2__ctor_m1876344714_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<System.Object,System.Object>::<>m__10(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539_gshared (U3CToOnGameThreadU3Ec__AnonStorey40_2_t4027445590 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey40_2_U3CU3Em__10_m1384499539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2464_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * L_2 = V_0;
		Il2CppObject * L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey41_2_t2782048327 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m2492884466_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::<>m__15()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m1378265439_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 *)__this->get_U3CU3Ef__refU2466_3();
		NullCheck(L_0);
		Action_3_t708612388 * L_1 = (Action_3_t708612388 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_val3_2();
		NullCheck((Action_3_t708612388 *)L_1);
		((  void (*) (Action_3_t708612388 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_3_t708612388 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m2275597405_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::<>m__15()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m3018797322_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 *)__this->get_U3CU3Ef__refU2466_3();
		NullCheck(L_0);
		Action_3_t178818711 * L_1 = (Action_3_t178818711 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_val3_2();
		NullCheck((Action_3_t178818711 *)L_1);
		((  void (*) (Action_3_t178818711 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_3_t178818711 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m3400352285_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>::<>m__15()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m1576478410_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 *)__this->get_U3CU3Ef__refU2466_3();
		NullCheck(L_0);
		Action_3_t2968268857 * L_1 = (Action_3_t2968268857 *)L_0->get_toConvert_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_val3_2();
		NullCheck((Action_3_t2968268857 *)L_1);
		((  void (*) (Action_3_t2968268857 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_3_t2968268857 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m2174576724_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3608683331 * __this, int32_t ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m213191264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2466_3(__this);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_6 = V_0;
		Il2CppObject * L_7 = ___val32;
		NullCheck(L_6);
		L_6->set_val3_2(L_7);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t1154936577 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_10 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m3294683583_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 * __this, int32_t ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2466_3(__this);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_6 = V_0;
		Il2CppObject * L_7 = ___val32;
		NullCheck(L_6);
		L_6->set_val3_2(L_7);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_10 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m1447645055_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<System.Object,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t1573372504 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m2807445451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2466_3(__this);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_2 = V_0;
		Il2CppObject * L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_6 = V_0;
		Il2CppObject * L_7 = ___val32;
		NullCheck(L_6);
		L_6->set_val3_2(L_7);
		U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_10 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m998904344_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m1360890551_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t2871861156 * __this, int32_t ___result0, const MethodInfo* method)
{
	{
		Action_1_t3953224079 * L_0 = (Action_1_t3953224079 *)__this->get_callback_0();
		int32_t L_1 = ___result0;
		((  void (*) (Il2CppObject * /* static, unused */, Action_1_t3953224079 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t3953224079 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m617187386_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Boolean>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m2704394581_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t4086219227 * __this, bool ___result0, const MethodInfo* method)
{
	{
		Action_1_t872614854 * L_0 = (Action_1_t872614854 *)__this->get_callback_0();
		bool L_1 = ___result0;
		((  void (*) (Il2CppObject * /* static, unused */, Action_1_t872614854 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t872614854 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m695785687_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m3469203352_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	{
		Action_1_t271665211 * L_0 = (Action_1_t271665211 *)__this->get_callback_0();
		Il2CppObject * L_1 = ___result0;
		((  void (*) (Il2CppObject * /* static, unused */, Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t271665211 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m56845681_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::<>m__18()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1005472264;
extern const uint32_t U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257_MetadataUsageId;
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t3940624403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m1102172257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, (String_t*)_stringLiteral1005472264, /*hidden argument*/NULL);
		Action_1_t3953224079 * L_0 = (Action_1_t3953224079 *)__this->get_callback_0();
		int32_t L_1 = (int32_t)__this->get_data_1();
		NullCheck((Action_1_t3953224079 *)L_0);
		((  void (*) (Action_1_t3953224079 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t3953224079 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m1698430227_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>::<>m__18()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1005472264;
extern const uint32_t U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331_MetadataUsageId;
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, (String_t*)_stringLiteral1005472264, /*hidden argument*/NULL);
		Action_1_t872614854 * L_0 = (Action_1_t872614854 *)__this->get_callback_0();
		bool L_1 = (bool)__this->get_data_1();
		NullCheck((Action_1_t872614854 *)L_0);
		((  void (*) (Action_1_t872614854 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t872614854 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m2531779806_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Object>::<>m__18()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1005472264;
extern const uint32_t U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398_MetadataUsageId;
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t259065535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m101984398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, (String_t*)_stringLiteral1005472264, /*hidden argument*/NULL);
		Action_1_t271665211 * L_0 = (Action_1_t271665211 *)__this->get_callback_0();
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_data_1();
		NullCheck((Action_1_t271665211 *)L_0);
		((  void (*) (Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t271665211 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m3379422511_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::<>m__72()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m2937977939_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739 *)__this->get_U3CU3Ef__refU24133_2();
		NullCheck(L_0);
		Action_2_t2661426558 * L_1 = (Action_2_t2661426558 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t2661426558 *)L_1);
		((  void (*) (Action_2_t2661426558 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t2661426558 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m651080457_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::<>m__72()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m931314605_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 *)__this->get_U3CU3Ef__refU24133_2();
		NullCheck(L_0);
		Action_2_t1740094460 * L_1 = (Action_2_t1740094460 *)L_0->get_toConvert_0();
		int32_t L_2 = (int32_t)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t1740094460 *)L_1);
		((  void (*) (Action_2_t1740094460 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t1740094460 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m5751912_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<System.Object,System.Object>::<>m__72()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m3540840780_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * __this, const MethodInfo* method)
{
	{
		U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 *)__this->get_U3CU3Ef__refU24133_2();
		NullCheck(L_0);
		Action_2_t4293064463 * L_1 = (Action_2_t4293064463 *)L_0->get_toConvert_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_val1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_val2_1();
		NullCheck((Action_2_t4293064463 *)L_1);
		((  void (*) (Action_2_t4293064463 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t4293064463 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m1719796115_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::<>m__6E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3032855674_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3032855674_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t729841739 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3032855674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24133_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m501707629_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::<>m__6E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24133_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * L_2 = V_0;
		int32_t L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t3521209373 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m1028318924_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>::<>m__6E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393_MetadataUsageId;
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * V_0 = NULL;
	{
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * L_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 *)L_0;
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24133_2(__this);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * L_2 = V_0;
		Il2CppObject * L_3 = ___val10;
		NullCheck(L_2);
		L_2->set_val1_0(L_3);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * L_4 = V_0;
		Il2CppObject * L_5 = ___val21;
		NullCheck(L_4);
		L_4->set_val2_1(L_5);
		U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_8 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1__ctor_m1536677294_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>::<>m__9F()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_U3CU3Em__9F_m1586429156_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 *)__this->get_U3CU3Ef__refU24157_1();
		NullCheck(L_0);
		Action_1_t872614854 * L_1 = (Action_1_t872614854 *)L_0->get_toInvokeOnGameThread_0();
		bool L_2 = (bool)__this->get_result_0();
		NullCheck((Action_1_t872614854 *)L_1);
		((  void (*) (Action_1_t872614854 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t872614854 *)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1__ctor_m4189129955_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Object>::<>m__9F()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_U3CU3Em__9F_m3677829849_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 *)__this->get_U3CU3Ef__refU24157_1();
		NullCheck(L_0);
		Action_1_t271665211 * L_1 = (Action_1_t271665211 *)L_0->get_toInvokeOnGameThread_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_result_0();
		NullCheck((Action_1_t271665211 *)L_1);
		((  void (*) (Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_1_t271665211 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m4046309363_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Boolean>::<>m__9D(T)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2620762138 * __this, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m3887851479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24157_1(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * L_2 = V_0;
		bool L_3 = ___result0;
		NullCheck(L_2);
		L_2->set_result_0(L_3);
		Action_1_t872614854 * L_4 = (Action_1_t872614854 *)__this->get_toInvokeOnGameThread_0();
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		return;
	}

IL_0020:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_7 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1__ctor_m1914781182_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1<System.Object>::<>m__9D(T)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t2019812495 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_U3CU3Em__9D_m328315372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24157_1(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * L_2 = V_0;
		Il2CppObject * L_3 = ___result0;
		NullCheck(L_2);
		L_2->set_result_0(L_3);
		Action_1_t271665211 * L_4 = (Action_1_t271665211 *)__this->get_toInvokeOnGameThread_0();
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		return;
	}

IL_0020:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3338309164 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_7 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m4016861356_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m1336680644_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t3522153312 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t3522153312 *)__this->get_U3CU3Ef__refU24159_2();
		NullCheck(L_0);
		Action_2_t1255729854 * L_1 = (Action_2_t1255729854 *)L_0->get_toInvokeOnGameThread_0();
		int32_t L_2 = (int32_t)__this->get_result1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_result2_1();
		NullCheck((Action_2_t1255729854 *)L_1);
		((  void (*) (Action_2_t1255729854 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t1255729854 *)L_1, (int32_t)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Boolean,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m1592765657_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Boolean,System.Object>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m3947955633_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t317199770 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t317199770 *)__this->get_U3CU3Ef__refU24159_2();
		NullCheck(L_0);
		Action_2_t2345743608 * L_1 = (Action_2_t2345743608 *)L_0->get_toInvokeOnGameThread_0();
		bool L_2 = (bool)__this->get_result1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_result2_1();
		NullCheck((Action_2_t2345743608 *)L_1);
		((  void (*) (Action_2_t2345743608 *, bool, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t2345743608 *)L_1, (bool)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2892245493_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Boolean>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2912594893_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 *)__this->get_U3CU3Ef__refU24159_2();
		NullCheck(L_0);
		Action_2_t599046810 * L_1 = (Action_2_t599046810 *)L_0->get_toInvokeOnGameThread_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_result1_0();
		bool L_3 = (bool)__this->get_result2_1();
		NullCheck((Action_2_t599046810 *)L_1);
		((  void (*) (Action_2_t599046810 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t599046810 *)L_1, (Il2CppObject *)L_2, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2708837308_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2750988244_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * __this, const MethodInfo* method)
{
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 *)__this->get_U3CU3Ef__refU24159_2();
		NullCheck(L_0);
		Action_2_t4293064463 * L_1 = (Action_2_t4293064463 *)L_0->get_toInvokeOnGameThread_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_result1_0();
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_result2_1();
		NullCheck((Action_2_t4293064463 *)L_1);
		((  void (*) (Action_2_t4293064463 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Action_2_t4293064463 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m3761397667_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t3522153312 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::<>m__9E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3731861869_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3731861869_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t3522153312 * __this, int32_t ___result10, Il2CppObject * ___result21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3731861869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24159_2(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * L_2 = V_0;
		int32_t L_3 = ___result10;
		NullCheck(L_2);
		L_2->set_result1_0(L_3);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * L_4 = V_0;
		Il2CppObject * L_5 = ___result21;
		NullCheck(L_4);
		L_4->set_result2_1(L_5);
		Action_2_t1255729854 * L_6 = (Action_2_t1255729854 *)__this->get_toInvokeOnGameThread_0();
		if (L_6)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t4039120987 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_9 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Boolean,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m1874193218_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t317199770 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Boolean,System.Object>::<>m__9E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m767861998_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m767861998_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t317199770 * __this, bool ___result10, Il2CppObject * ___result21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m767861998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24159_2(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * L_2 = V_0;
		bool L_3 = ___result10;
		NullCheck(L_2);
		L_2->set_result1_0(L_3);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * L_4 = V_0;
		Il2CppObject * L_5 = ___result21;
		NullCheck(L_4);
		L_4->set_result2_1(L_5);
		Action_2_t2345743608 * L_6 = (Action_2_t2345743608 *)__this->get_toInvokeOnGameThread_0();
		if (L_6)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t834167445 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_9 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m3173673054_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Boolean>::<>m__9E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2865470268 * __this, Il2CppObject * ___result10, bool ___result21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m3847909458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24159_2(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * L_2 = V_0;
		Il2CppObject * L_3 = ___result10;
		NullCheck(L_2);
		L_2->set_result1_0(L_3);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * L_4 = V_0;
		bool L_5 = ___result21;
		NullCheck(L_4);
		L_4->set_result2_1(L_5);
		Action_2_t599046810 * L_6 = (Action_2_t599046810 *)__this->get_toInvokeOnGameThread_0();
		if (L_6)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3382437943 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_9 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2__ctor_m1193894963_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2<System.Object,System.Object>::<>m__9E(T1,T2)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813_MetadataUsageId;
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t2264520625 * __this, Il2CppObject * ___result10, Il2CppObject * ___result21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_U3CU3Em__9E_m4143278813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * V_0 = NULL;
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * L_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 *)L_0;
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24159_2(__this);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * L_2 = V_0;
		Il2CppObject * L_3 = ___result10;
		NullCheck(L_2);
		L_2->set_result1_0(L_3);
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * L_4 = V_0;
		Il2CppObject * L_5 = ___result21;
		NullCheck(L_4);
		L_4->set_result2_1(L_5);
		Action_2_t4293064463 * L_6 = (Action_2_t4293064463 *)__this->get_toInvokeOnGameThread_0();
		if (L_6)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Action_t3771233898 * L_9 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_9, (Il2CppObject *)L_7, (IntPtr_t)L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, (Action_t3771233898 *)L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>::.ctor()
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9B_1__ctor_m1610901700_gshared (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>::<>m__9B(System.IntPtr)
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110_MetadataUsageId;
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110_gshared (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 * __this, IntPtr_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2304636665 * L_0 = (Func_2_t2304636665 *)__this->get_conversionFunction_0();
		IntPtr_t L_1 = ___result0;
		NullCheck((Func_2_t2304636665 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2304636665 *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t2304636665 *)L_0, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Action_1_t271665211 * L_3 = (Action_1_t271665211 *)__this->get_callback_1();
			if (!L_3)
			{
				goto IL_0024;
			}
		}

IL_0018:
		{
			Action_1_t271665211 * L_4 = (Action_1_t271665211 *)__this->get_callback_1();
			Il2CppObject * L_5 = V_0;
			NullCheck((Action_1_t271665211 *)L_4);
			((  void (*) (Action_1_t271665211 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Action_1_t271665211 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		}

IL_0024:
		{
			IL2CPP_LEAVE(0x3D, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_6 = V_0;
			if (!L_6)
			{
				goto IL_003c;
			}
		}

IL_002f:
		{
			NullCheck((Il2CppObject *)(*(&V_0)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(41)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9C_2__ctor_m2655981944_gshared (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>::<>m__9C(T,System.IntPtr)
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793_MetadataUsageId;
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793_gshared (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 * __this, Il2CppObject * ___param10, IntPtr_t ___param21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2304636665 * L_0 = (Func_2_t2304636665 *)__this->get_conversionFunction_0();
		IntPtr_t L_1 = ___param21;
		NullCheck((Func_2_t2304636665 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2304636665 *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t2304636665 *)L_0, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Action_2_t4293064463 * L_3 = (Action_2_t4293064463 *)__this->get_callback_1();
			if (!L_3)
			{
				goto IL_0025;
			}
		}

IL_0018:
		{
			Action_2_t4293064463 * L_4 = (Action_2_t4293064463 *)__this->get_callback_1();
			Il2CppObject * L_5 = ___param10;
			Il2CppObject * L_6 = V_0;
			NullCheck((Action_2_t4293064463 *)L_4);
			((  void (*) (Action_2_t4293064463 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Action_2_t4293064463 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_7 = V_0;
			if (!L_7)
			{
				goto IL_003d;
			}
		}

IL_0030:
		{
			NullCheck((Il2CppObject *)(*(&V_0)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)(*(&V_0)));
		}

IL_003d:
		{
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003e:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1__ctor_m3214966157_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2450095124_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m3069925113_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator5_1_System_Collections_IEnumerable_GetEnumerator_m3327423796_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator5_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4217469015_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * L_2 = (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 *)L_2;
		U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * L_3 = V_0;
		UIntPtr_t  L_4 = (UIntPtr_t )__this->get_U3CU24U3Esize_5();
		NullCheck(L_3);
		L_3->set_size_1(L_4);
		U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * L_5 = V_0;
		Func_2_t2301125574 * L_6 = (Func_2_t2301125574 *)__this->get_U3CU24U3EgetElement_6();
		NullCheck(L_5);
		L_5->set_getElement_2(L_6);
		U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator5_1_MoveNext_m3635576647_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0((((int64_t)((int64_t)0))));
		goto IL_0065;
	}

IL_002e:
	{
		Func_2_t2301125574 * L_2 = (Func_2_t2301125574 *)__this->get_getElement_2();
		uint64_t L_3 = (uint64_t)__this->get_U3CiU3E__0_0();
		UIntPtr_t  L_4;
		memset(&L_4, 0, sizeof(L_4));
		UIntPtr__ctor_m3952300446(&L_4, (uint64_t)L_3, /*hidden argument*/NULL);
		NullCheck((Func_2_t2301125574 *)L_2);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t2301125574 *, UIntPtr_t , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2301125574 *)L_2, (UIntPtr_t )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_U24current_4(L_5);
		__this->set_U24PC_3(1);
		goto IL_0084;
	}

IL_0056:
	{
		uint64_t L_6 = (uint64_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_0065:
	{
		uint64_t L_7 = (uint64_t)__this->get_U3CiU3E__0_0();
		UIntPtr_t * L_8 = (UIntPtr_t *)__this->get_address_of_size_1();
		uint64_t L_9 = UIntPtr_ToUInt64_m958857238((UIntPtr_t *)L_8, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_7) >= ((uint64_t)L_9))))
		{
			goto IL_002e;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1_Dispose_m1399274122_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098_MetadataUsageId;
extern "C"  void U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098_gshared (U3CToEnumerableU3Ec__Iterator5_1_t3982122310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator5_1_Reset_m861399098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void OutMethod_1__ctor_m2423716964_gshared (OutMethod_1_t2402168711 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>::Invoke(T[],System.UIntPtr)
extern "C"  UIntPtr_t  OutMethod_1_Invoke_m2153845915_gshared (OutMethod_1_t2402168711 * __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OutMethod_1_Invoke_m2153845915((OutMethod_1_t2402168711 *)__this->get_prev_9(),___out_bytes0, ___out_size1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OutMethod_1_BeginInvoke_m1214319812_MetadataUsageId;
extern "C"  Il2CppObject * OutMethod_1_BeginInvoke_m1214319812_gshared (OutMethod_1_t2402168711 * __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutMethod_1_BeginInvoke_m1214319812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___out_bytes0;
	__d_args[1] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___out_size1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  UIntPtr_t  OutMethod_1_EndInvoke_m1043361660_gshared (OutMethod_1_t2402168711 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(UIntPtr_t *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void OutMethod_1__ctor_m2700608397_gshared (OutMethod_1_t3549961022 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::Invoke(T[],System.UIntPtr)
extern "C"  UIntPtr_t  OutMethod_1_Invoke_m817607364_gshared (OutMethod_1_t3549961022 * __this, IntPtrU5BU5D_t3228729122* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OutMethod_1_Invoke_m817607364((OutMethod_1_t3549961022 *)__this->get_prev_9(),___out_bytes0, ___out_size1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtrU5BU5D_t3228729122* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, IntPtrU5BU5D_t3228729122* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OutMethod_1_BeginInvoke_m2321473261_MetadataUsageId;
extern "C"  Il2CppObject * OutMethod_1_BeginInvoke_m2321473261_gshared (OutMethod_1_t3549961022 * __this, IntPtrU5BU5D_t3228729122* ___out_bytes0, UIntPtr_t  ___out_size1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutMethod_1_BeginInvoke_m2321473261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___out_bytes0;
	__d_args[1] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___out_size1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  UIntPtr_t  OutMethod_1_EndInvoke_m3863859493_gshared (OutMethod_1_t3549961022 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(UIntPtr_t *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void OutMethod_1__ctor_m3096051917_gshared (OutMethod_1_t3710375422 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::Invoke(T[],System.UIntPtr)
extern "C"  UIntPtr_t  OutMethod_1_Invoke_m2612981764_gshared (OutMethod_1_t3710375422 * __this, ObjectU5BU5D_t1108656482* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OutMethod_1_Invoke_m2612981764((OutMethod_1_t3710375422 *)__this->get_prev_9(),___out_bytes0, ___out_size1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Il2CppObject *, void* __this, ObjectU5BU5D_t1108656482* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, ObjectU5BU5D_t1108656482* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OutMethod_1_BeginInvoke_m3366706221_MetadataUsageId;
extern "C"  Il2CppObject * OutMethod_1_BeginInvoke_m3366706221_gshared (OutMethod_1_t3710375422 * __this, ObjectU5BU5D_t1108656482* ___out_bytes0, UIntPtr_t  ___out_size1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutMethod_1_BeginInvoke_m3366706221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___out_bytes0;
	__d_args[1] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___out_size1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  UIntPtr_t  OutMethod_1_EndInvoke_m1995468389_gshared (OutMethod_1_t3710375422 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(UIntPtr_t *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::.ctor()
extern "C"  void ComponentAction_1__ctor_m1981820550_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	{
		NullCheck((FsmStateAction_t2366529033 *)__this);
		FsmStateAction__ctor_m2146048618((FsmStateAction_t2366529033 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody()
extern Il2CppClass* Rigidbody_t3346577219_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_rigidbody_m3654614315_MetadataUsageId;
extern "C"  Rigidbody_t3346577219 * ComponentAction_1_get_rigidbody_m3654614315_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_rigidbody_m3654614315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Rigidbody_t3346577219 *)IsInst(L_0, Rigidbody_t3346577219_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Rigidbody2D HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody2d()
extern Il2CppClass* Rigidbody2D_t1743771669_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_rigidbody2d_m3969478575_MetadataUsageId;
extern "C"  Rigidbody2D_t1743771669 * ComponentAction_1_get_rigidbody2d_m3969478575_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_rigidbody2d_m3969478575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Rigidbody2D_t1743771669 *)IsInst(L_0, Rigidbody2D_t1743771669_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_renderer()
extern Il2CppClass* Renderer_t3076687687_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_renderer_m2707840429_MetadataUsageId;
extern "C"  Renderer_t3076687687 * ComponentAction_1_get_renderer_m2707840429_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_renderer_m2707840429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Renderer_t3076687687 *)IsInst(L_0, Renderer_t3076687687_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_animation()
extern Il2CppClass* Animation_t1724966010_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_animation_m3244187609_MetadataUsageId;
extern "C"  Animation_t1724966010 * ComponentAction_1_get_animation_m3244187609_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_animation_m3244187609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Animation_t1724966010 *)IsInst(L_0, Animation_t1724966010_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_audio()
extern Il2CppClass* AudioSource_t1740077639_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_audio_m1198830264_MetadataUsageId;
extern "C"  AudioSource_t1740077639 * ComponentAction_1_get_audio_m1198830264_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_audio_m1198830264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((AudioSource_t1740077639 *)IsInst(L_0, AudioSource_t1740077639_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_camera()
extern Il2CppClass* Camera_t2727095145_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_camera_m2580411949_MetadataUsageId;
extern "C"  Camera_t2727095145 * ComponentAction_1_get_camera_m2580411949_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_camera_m2580411949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Camera_t2727095145 *)IsInst(L_0, Camera_t2727095145_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiText()
extern Il2CppClass* GUIText_t3371372606_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_guiText_m995647329_MetadataUsageId;
extern "C"  GUIText_t3371372606 * ComponentAction_1_get_guiText_m995647329_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_guiText_m995647329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((GUIText_t3371372606 *)IsInst(L_0, GUIText_t3371372606_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiTexture()
extern Il2CppClass* GUITexture_t4020448292_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_guiTexture_m3296952909_MetadataUsageId;
extern "C"  GUITexture_t4020448292 * ComponentAction_1_get_guiTexture_m3296952909_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_guiTexture_m3296952909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((GUITexture_t4020448292 *)IsInst(L_0, GUITexture_t4020448292_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_light()
extern Il2CppClass* Light_t4202674828_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_light_m3399183485_MetadataUsageId;
extern "C"  Light_t4202674828 * ComponentAction_1_get_light_m3399183485_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_light_m3399183485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((Light_t4202674828 *)IsInst(L_0, Light_t4202674828_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_networkView()
extern Il2CppClass* NetworkView_t3656680617_il2cpp_TypeInfo_var;
extern const uint32_t ComponentAction_1_get_networkView_m1393581751_MetadataUsageId;
extern "C"  NetworkView_t3656680617 * ComponentAction_1_get_networkView_m1393581751_gshared (ComponentAction_1_t3146284570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_get_networkView_m1393581751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_component_12();
		return ((NetworkView_t3656680617 *)IsInst(L_0, NetworkView_t3656680617_il2cpp_TypeInfo_var));
	}
}
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::UpdateCache(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2807948553;
extern Il2CppCodeGenString* _stringLiteral32967013;
extern const uint32_t ComponentAction_1_UpdateCache_m1764863969_MetadataUsageId;
extern "C"  bool ComponentAction_1_UpdateCache_m1764863969_gshared (ComponentAction_1_t3146284570 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ComponentAction_1_UpdateCache_m1764863969_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_component_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		GameObject_t3674682005 * L_4 = (GameObject_t3674682005 *)__this->get_cachedGameObject_11();
		GameObject_t3674682005 * L_5 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)L_4, (Object_t3071478659 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0088;
		}
	}

IL_0035:
	{
		GameObject_t3674682005 * L_7 = ___go0;
		NullCheck((GameObject_t3674682005 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((GameObject_t3674682005 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_component_12(L_8);
		GameObject_t3674682005 * L_9 = ___go0;
		__this->set_cachedGameObject_11(L_9);
		Il2CppObject * L_10 = (Il2CppObject *)__this->get_component_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_10, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0088;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_12);
		GameObject_t3674682005 * L_14 = ___go0;
		NullCheck((Object_t3071478659 *)L_14);
		String_t* L_15 = Object_get_name_m3709440845((Object_t3071478659 *)L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2933632197(NULL /*static, unused*/, (String_t*)_stringLiteral2807948553, (String_t*)L_13, (String_t*)_stringLiteral32967013, (String_t*)L_15, /*hidden argument*/NULL);
		NullCheck((FsmStateAction_t2366529033 *)__this);
		FsmStateAction_LogWarning_m567309232((FsmStateAction_t2366529033 *)__this, (String_t*)L_16, /*hidden argument*/NULL);
	}

IL_0088:
	{
		Il2CppObject * L_17 = (Il2CppObject *)__this->get_component_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3071478659 *)L_17, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Void HutongGames.Utility.Arrays`1<System.Object>::.cctor()
extern "C"  void Arrays_1__cctor_m1926254853_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((Arrays_1_t792694677_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_Empty_0(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (uint32_t)0)));
		return;
	}
}
// System.Void HutongGames.Utility.Lists`1<System.Object>::.cctor()
extern "C"  void Lists_1__cctor_m3087565302_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((Lists_1_t4287567192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Empty_0(L_0);
		return;
	}
}
// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m2253674582_gshared (WriteDelegate_1_t3884844371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::Invoke(JsonFx.Json.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m3947399451_gshared (WriteDelegate_1_t3884844371 * __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m3947399451((WriteDelegate_1_t3884844371 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTime_t4283661327  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JsonFx.Json.WriteDelegate`1<System.DateTime>::BeginInvoke(JsonFx.Json.JsonWriter,T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t WriteDelegate_1_BeginInvoke_m3657410720_MetadataUsageId;
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m3657410720_gshared (WriteDelegate_1_t3884844371 * __this, JsonWriter_t3589747297 * ___writer0, DateTime_t4283661327  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WriteDelegate_1_BeginInvoke_m3657410720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void JsonFx.Json.WriteDelegate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m371025254_gshared (WriteDelegate_1_t3884844371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void JsonFx.Json.WriteDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m1559236658_gshared (WriteDelegate_1_t3771999415 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void JsonFx.Json.WriteDelegate`1<System.Object>::Invoke(JsonFx.Json.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m3924330487_gshared (WriteDelegate_1_t3771999415 * __this, JsonWriter_t3589747297 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m3924330487((WriteDelegate_1_t3771999415 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t3589747297 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t3589747297 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult JsonFx.Json.WriteDelegate`1<System.Object>::BeginInvoke(JsonFx.Json.JsonWriter,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m3023137916_gshared (WriteDelegate_1_t3771999415 * __this, JsonWriter_t3589747297 * ___writer0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void JsonFx.Json.WriteDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m791112514_gshared (WriteDelegate_1_t3771999415 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppCodeGenString* _stringLiteral3374289864;
extern const uint32_t JEnumerable_1__ctor_m313850865_MetadataUsageId;
extern "C"  void JEnumerable_1__ctor_m313850865_gshared (JEnumerable_1_t971832625 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JEnumerable_1__ctor_m313850865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___enumerable0;
		ValidationUtils_ArgumentNotNull_m2954428052(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3374289864, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___enumerable0;
		__this->set__enumerable_1(L_1);
		return;
	}
}
extern "C"  void JEnumerable_1__ctor_m313850865_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	JEnumerable_1__ctor_m313850865(_thisAdjusted, ___enumerable0, method);
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m3571439426_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method)
{
	JEnumerable_1_t971832625  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		JEnumerable_1_t971832625  L_1 = ((JEnumerable_1_t971832625_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Empty_0();
		V_0 = (JEnumerable_1_t971832625 )L_1;
		Il2CppObject* L_2 = JEnumerable_1_GetEnumerator_m3571439426((JEnumerable_1_t971832625 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}

IL_0016:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__enumerable_1();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m3571439426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	return JEnumerable_1_GetEnumerator_m3571439426(_thisAdjusted, method);
}
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = JEnumerable_1_GetEnumerator_m3571439426((JEnumerable_1_t971832625 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	return JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165(_thisAdjusted, method);
}
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(Newtonsoft.Json.Linq.JEnumerable`1<T>)
extern "C"  bool JEnumerable_1_Equals_m1430828747_gshared (JEnumerable_1_t971832625 * __this, JEnumerable_1_t971832625  ___other0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		JEnumerable_1_t971832625  L_1 = ___other0;
		Il2CppObject* L_2 = (Il2CppObject*)L_1.get__enumerable_1();
		bool L_3 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_0, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool JEnumerable_1_Equals_m1430828747_AdjustorThunk (Il2CppObject * __this, JEnumerable_1_t971832625  ___other0, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	return JEnumerable_1_Equals_m1430828747(_thisAdjusted, ___other0, method);
}
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(System.Object)
extern "C"  bool JEnumerable_1_Equals_m2597819561_gshared (JEnumerable_1_t971832625 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = JEnumerable_1_Equals_m1430828747((JEnumerable_1_t971832625 *)__this, (JEnumerable_1_t971832625 )((*(JEnumerable_1_t971832625 *)((JEnumerable_1_t971832625 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_2;
	}

IL_0015:
	{
		return (bool)0;
	}
}
extern "C"  bool JEnumerable_1_Equals_m2597819561_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	return JEnumerable_1_Equals_m2597819561(_thisAdjusted, ___obj0, method);
}
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetHashCode()
extern "C"  int32_t JEnumerable_1_GetHashCode_m2089431425_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__enumerable_1();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__enumerable_1();
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_1);
		return L_2;
	}
}
extern "C"  int32_t JEnumerable_1_GetHashCode_m2089431425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	JEnumerable_1_t971832625 * _thisAdjusted = reinterpret_cast<JEnumerable_1_t971832625 *>(__this + 1);
	return JEnumerable_1_GetHashCode_m2089431425(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.cctor()
extern "C"  void JEnumerable_1__cctor_m3840698783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		JEnumerable_1_t971832625  L_1;
		memset(&L_1, 0, sizeof(L_1));
		JEnumerable_1__ctor_m313850865(&L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((JEnumerable_1_t971832625_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Empty_0(L_1);
		return;
	}
}
// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::GetAttribute(System.Object)
extern "C"  Il2CppObject * CachedAttributeGetter_1_GetAttribute_m3559125336_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___type0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ThreadSafeStore_2_t2874570697 * L_0 = ((CachedAttributeGetter_1_t1288273172_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_TypeAttributeCache_0();
		Il2CppObject * L_1 = ___type0;
		NullCheck((ThreadSafeStore_2_t2874570697 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t2874570697 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ThreadSafeStore_2_t2874570697 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Object>::.cctor()
extern "C"  void CachedAttributeGetter_1__cctor_m3626143424_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Func_2_t184564025 * L_1 = (Func_2_t184564025 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Func_2_t184564025 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		ThreadSafeStore_2_t2874570697 * L_2 = (ThreadSafeStore_2_t2874570697 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (ThreadSafeStore_2_t2874570697 *, Func_2_t184564025 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_2, (Func_2_t184564025 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((CachedAttributeGetter_1_t1288273172_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_TypeAttributeCache_0(L_2);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_MoveNext_m3181297203_MetadataUsageId;
extern "C"  bool DictionaryEnumerator_2_MoveNext_m3181297203_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_MoveNext_m3181297203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
extern "C"  bool DictionaryEnumerator_2_MoveNext_m3181297203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	return DictionaryEnumerator_2_MoveNext_m3181297203(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::Reset()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_Reset_m3656227480_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2_Reset_m3656227480_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_Reset_m3656227480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2_Reset_m3656227480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	DictionaryEnumerator_2_Reset_m3656227480(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::get_Current()
extern const MethodInfo* KeyValuePair_2__ctor_m4168265535_MethodInfo_var;
extern const uint32_t DictionaryEnumerator_2_get_Current_m739694543_MetadataUsageId;
extern "C"  KeyValuePair_2_t1944668977  DictionaryEnumerator_2_get_Current_m739694543_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_get_Current_m739694543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1944668977  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_0);
		KeyValuePair_2_t1944668977  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t1944668977  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		V_0 = (KeyValuePair_2_t1944668977 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_3);
		KeyValuePair_2_t1944668977  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t1944668977  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_3);
		V_0 = (KeyValuePair_2_t1944668977 )L_4;
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m4250204908((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1944668977  L_6;
		memset(&L_6, 0, sizeof(L_6));
		KeyValuePair_2__ctor_m4168265535(&L_6, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/KeyValuePair_2__ctor_m4168265535_MethodInfo_var);
		return L_6;
	}
}
extern "C"  KeyValuePair_2_t1944668977  DictionaryEnumerator_2_get_Current_m739694543_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	return DictionaryEnumerator_2_get_Current_m739694543(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_Dispose_m2874789224_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2_Dispose_m2874789224_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_Dispose_m2874789224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2_Dispose_m2874789224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	DictionaryEnumerator_2_Dispose_m2874789224(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeyValuePair_2_t1944668977  L_0 = DictionaryEnumerator_2_get_Current_m739694543((DictionaryEnumerator_2_t362240250 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		KeyValuePair_2_t1944668977  L_1 = L_0;
		Il2CppObject * L_2 = Box(KeyValuePair_2_t1944668977_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	return DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243(_thisAdjusted, method);
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* DictionaryEnumerator_2_GetEnumerator_m2641693378_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	{
		DictionaryEnumerator_2_t362240250  L_0 = (*(DictionaryEnumerator_2_t362240250 *)__this);
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_0);
		return (Il2CppObject*)L_1;
	}
}
extern "C"  Il2CppObject* DictionaryEnumerator_2_GetEnumerator_m2641693378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	return DictionaryEnumerator_2_GetEnumerator_m2641693378(_thisAdjusted, method);
}
// System.Collections.IEnumerator Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method)
{
	{
		DictionaryEnumerator_2_t362240250  L_0 = (*(DictionaryEnumerator_2_t362240250 *)__this);
		Il2CppObject * L_1 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_0);
		return (Il2CppObject *)L_1;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t362240250 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t362240250 *>(__this + 1);
	return DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ObjectConstructor_1__ctor_m4092255551_gshared (ObjectConstructor_1_t2948332186 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::Invoke(System.Object[])
extern "C"  Il2CppObject * ObjectConstructor_1_Invoke_m1102952036_gshared (ObjectConstructor_1_t2948332186 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ObjectConstructor_1_Invoke_m1102952036((ObjectConstructor_1_t2948332186 *)__this->get_prev_9(),___args0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ObjectConstructor_1_BeginInvoke_m3616522142_gshared (ObjectConstructor_1_t2948332186 * __this, ObjectU5BU5D_t1108656482* ___args0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ObjectConstructor_1_EndInvoke_m2364446276_gshared (ObjectConstructor_1_t2948332186 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
extern Il2CppCodeGenString* _stringLiteral3642591715;
extern const uint32_t BidirectionalDictionary_2__ctor_m2565085922_MetadataUsageId;
extern "C"  void BidirectionalDictionary_2__ctor_m2565085922_gshared (BidirectionalDictionary_2_t1375314390 * __this, Il2CppObject* ___firstEqualityComparer0, Il2CppObject* ___secondEqualityComparer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BidirectionalDictionary_2__ctor_m2565085922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___firstEqualityComparer0;
		Il2CppObject* L_1 = ___secondEqualityComparer1;
		NullCheck((BidirectionalDictionary_2_t1375314390 *)__this);
		((  void (*) (BidirectionalDictionary_2_t1375314390 *, Il2CppObject*, Il2CppObject*, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((BidirectionalDictionary_2_t1375314390 *)__this, (Il2CppObject*)L_0, (Il2CppObject*)L_1, (String_t*)_stringLiteral3642591715, (String_t*)_stringLiteral3642591715, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
extern "C"  void BidirectionalDictionary_2__ctor_m1641679706_gshared (BidirectionalDictionary_2_t1375314390 * __this, Il2CppObject* ___firstEqualityComparer0, Il2CppObject* ___secondEqualityComparer1, String_t* ___duplicateFirstErrorMessage2, String_t* ___duplicateSecondErrorMessage3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___firstEqualityComparer0;
		Dictionary_2_t2045888271 * L_1 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__firstToSecond_0(L_1);
		Il2CppObject* L_2 = ___secondEqualityComparer1;
		Dictionary_2_t2045888271 * L_3 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set__secondToFirst_1(L_3);
		String_t* L_4 = ___duplicateFirstErrorMessage2;
		__this->set__duplicateFirstErrorMessage_2(L_4);
		String_t* L_5 = ___duplicateSecondErrorMessage3;
		__this->set__duplicateSecondErrorMessage_3(L_5);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::Set(TFirst,TSecond)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t BidirectionalDictionary_2_Set_m2800109206_MetadataUsageId;
extern "C"  void BidirectionalDictionary_2_Set_m2800109206_gshared (BidirectionalDictionary_2_t1375314390 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BidirectionalDictionary_2_Set_m2800109206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_1 = ___first0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)(&V_1));
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_3 = ___second1;
		NullCheck((Il2CppObject *)(*(&V_1)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_1)), (Il2CppObject *)L_3);
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_5 = (String_t*)__this->get__duplicateFirstErrorMessage_2();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_7 = ___first0;
		String_t* L_8 = StringUtils_FormatWith_m692745541(NULL /*static, unused*/, (String_t*)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_9 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_9, (String_t*)L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0041:
	{
		Il2CppObject* L_10 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_11 = ___second1;
		NullCheck((Il2CppObject*)L_10);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_0));
		if (!L_12)
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject * L_13 = ___first0;
		NullCheck((Il2CppObject *)(*(&V_0)));
		bool L_14 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_0)), (Il2CppObject *)L_13);
		if (L_14)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_15 = (String_t*)__this->get__duplicateSecondErrorMessage_3();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_16 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_17 = ___second1;
		String_t* L_18 = StringUtils_FormatWith_m692745541(NULL /*static, unused*/, (String_t*)L_15, (Il2CppObject *)L_16, (Il2CppObject *)L_17, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_19 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0082:
	{
		Il2CppObject* L_20 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_21 = ___first0;
		Il2CppObject * L_22 = ___second1;
		NullCheck((Il2CppObject*)L_20);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_20, (Il2CppObject *)L_21, (Il2CppObject *)L_22);
		Il2CppObject* L_23 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_24 = ___second1;
		Il2CppObject * L_25 = ___first0;
		NullCheck((Il2CppObject*)L_23);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_23, (Il2CppObject *)L_24, (Il2CppObject *)L_25);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::TryGetByFirst(TFirst,TSecond&)
extern "C"  bool BidirectionalDictionary_2_TryGetByFirst_m3250057860_gshared (BidirectionalDictionary_2_t1375314390 * __this, Il2CppObject * ___first0, Il2CppObject ** ___second1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__firstToSecond_0();
		Il2CppObject * L_1 = ___first0;
		Il2CppObject ** L_2 = ___second1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.Object,System.Object>::TryGetBySecond(TSecond,TFirst&)
extern "C"  bool BidirectionalDictionary_2_TryGetBySecond_m1619327686_gshared (BidirectionalDictionary_2_t1375314390 * __this, Il2CppObject * ___second0, Il2CppObject ** ___first1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__secondToFirst_1();
		Il2CppObject * L_1 = ___second0;
		Il2CppObject ** L_2 = ___first1;
		NullCheck((Il2CppObject*)L_0);
		bool L_3 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject **)L_2);
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Add_m1347557672_MetadataUsageId;
extern "C"  void CollectionWrapper_1_Add_m1347557672_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Add_m1347557672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return;
	}

IL_0015:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Clear()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Clear_m831305890_MetadataUsageId;
extern "C"  void CollectionWrapper_1_Clear_m831305890_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Clear_m831305890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return;
	}

IL_0014:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IList::Clear() */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Contains_m687940200_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_Contains_m687940200_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Contains_m687940200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0015:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___item0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_CopyTo_m857933592_MetadataUsageId;
extern "C"  void CollectionWrapper_1_CopyTo_m857933592_gshared (CollectionWrapper_1_t3035453308 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_CopyTo_m857933592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		ObjectU5BU5D_t1108656482* L_2 = ___array0;
		int32_t L_3 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (ObjectU5BU5D_t1108656482*)L_2, (int32_t)L_3);
		return;
	}

IL_0016:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		ObjectU5BU5D_t1108656482* L_5 = ___array0;
		int32_t L_6 = ___arrayIndex1;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6);
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_get_Count_m1049256439_MetadataUsageId;
extern "C"  int32_t CollectionWrapper_1_get_Count_m1049256439_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_get_Count_m1049256439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_IsReadOnly()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_get_IsReadOnly_m4049681356_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_get_IsReadOnly_m4049681356_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_get_IsReadOnly_m4049681356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IList::get_IsReadOnly() */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_Remove_m3043712099_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_Remove_m3043712099_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_Remove_m3043712099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B4_0 = false;
	bool G_B3_0 = false;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		Il2CppObject * L_2 = ___item0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0015:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___item0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		bool L_7 = (bool)L_6;
		G_B3_0 = L_7;
		if (!L_7)
		{
			G_B4_0 = L_7;
			goto IL_003a;
		}
	}
	{
		Il2CppObject * L_8 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_9 = ___item0;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		G_B4_0 = G_B3_0;
	}

IL_003a:
	{
		return G_B4_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* CollectionWrapper_1_GetEnumerator_m1532560691_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4);
		return L_5;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_Add_m433705633_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_1 = ___value0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T) */, (CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count() */, (CollectionWrapper_1_t3035453308 *)__this);
		return ((int32_t)((int32_t)L_2-(int32_t)1));
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool CollectionWrapper_1_System_Collections_IList_Contains_m3576282059_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(31 /* System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T) */, (CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return L_3;
	}

IL_0015:
	{
		return (bool)0;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3553969220;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737_MetadataUsageId;
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3553969220, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___value0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__list_0();
		Il2CppObject * L_5 = ___value0;
		NullCheck((Il2CppObject *)L_4);
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return L_6;
	}

IL_0032:
	{
		return (-1);
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral593916376;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral593916376, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		int32_t L_3 = ___index0;
		NullCheck((Il2CppObject *)L_2);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3453628278;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_Insert_m2912955276_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_Insert_m2912955276_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_Insert_m2912955276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3453628278, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___value1;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___value1;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		NullCheck((Il2CppObject*)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		NullCheck((Il2CppObject *)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		return L_4;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Remove_m1863821852_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(35 /* System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T) */, (CollectionWrapper_1_t3035453308 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
	}

IL_0015:
	{
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3489346606;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3489346606, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		int32_t L_3 = ___index0;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_2, (int32_t)L_3);
		return L_4;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3489346606;
extern const uint32_t CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011_MetadataUsageId;
extern "C"  void CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3489346606, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___value1;
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__list_0();
		int32_t L_4 = ___index0;
		Il2CppObject * L_5 = ___value1;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.IList::set_Item(System.Int32,System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (int32_t)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_ICollection_CopyTo_m3925762221_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		int32_t L_1 = ___arrayIndex1;
		NullCheck((CollectionWrapper_1_t3035453308 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(32 /* System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32) */, (CollectionWrapper_1_t3035453308 *)__this, (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11))), (int32_t)L_1);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m55051185_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029_MetadataUsageId;
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__syncRoot_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of__syncRoot_2();
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		Interlocked_CompareExchange_m1450605286(NULL /*static, unused*/, (Il2CppObject **)L_1, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
	}

IL_001a:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__syncRoot_2();
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::VerifyValueType(System.Object)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3919549799;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t CollectionWrapper_1_VerifyValueType_m753172749_MetadataUsageId;
extern "C"  void CollectionWrapper_1_VerifyValueType_m753172749_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_VerifyValueType_m753172749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_2 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		String_t* L_5 = StringUtils_FormatWith_m3174277971(NULL /*static, unused*/, (String_t*)_stringLiteral3919549799, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, (String_t*)L_5, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002d:
	{
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsCompatibleObject(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3905956676_il2cpp_TypeInfo_var;
extern const uint32_t CollectionWrapper_1_IsCompatibleObject_m2245613824_MetadataUsageId;
extern "C"  bool CollectionWrapper_1_IsCompatibleObject_m2245613824_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionWrapper_1_IsCompatibleObject_m2245613824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		bool L_3 = TypeExtensions_IsValueType_m2619215877(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3905956676_il2cpp_TypeInfo_var);
		bool L_5 = ReflectionUtils_IsNullableType_m1302721165(NULL /*static, unused*/, (Type_t *)L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002f;
		}
	}

IL_002d:
	{
		return (bool)0;
	}

IL_002f:
	{
		return (bool)1;
	}
}
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_UnderlyingCollection()
extern "C"  Il2CppObject * CollectionWrapper_1_get_UnderlyingCollection_m2146542214_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericCollection_1();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericCollection_1();
		return L_1;
	}

IL_000f:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__list_0();
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::.cctor()
extern "C"  void U3CU3Ec__cctor_m3523957673_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		U3CU3Ec_t1205074931 * L_0 = (U3CU3Ec_t1205074931 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CU3Ec_t1205074931 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((U3CU3Ec_t1205074931_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::.ctor()
extern "C"  void U3CU3Ec__ctor_m3731458180_gshared (U3CU3Ec_t1205074931 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>::<GetEnumerator>b__25_0(System.Collections.DictionaryEntry)
extern "C"  KeyValuePair_2_t1944668977  U3CU3Ec_U3CGetEnumeratorU3Eb__25_0_m329333814_gshared (U3CU3Ec_t1205074931 * __this, DictionaryEntry_t1751606614  ___de0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = DictionaryEntry_get_Key_m3516209325((DictionaryEntry_t1751606614 *)(&___de0), /*hidden argument*/NULL);
		Il2CppObject * L_1 = DictionaryEntry_get_Value_m4281303039((DictionaryEntry_t1751606614 *)(&___de0), /*hidden argument*/NULL);
		KeyValuePair_2_t1944668977  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m4168265535(&L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
extern Il2CppCodeGenString* _stringLiteral101;
extern const uint32_t DictionaryEnumerator_2__ctor_m3301157341_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2__ctor_m3301157341_gshared (DictionaryEnumerator_2_t4062924206 * __this, Il2CppObject* ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2__ctor_m3301157341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___e0;
		ValidationUtils_ArgumentNotNull_m2954428052(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral101, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___e0;
		__this->set__e_0(L_1);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2__ctor_m3301157341_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___e0, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	DictionaryEnumerator_2__ctor_m3301157341(_thisAdjusted, ___e0, method);
}
// System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Entry()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_get_Entry_m2420324981_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  DictionaryEnumerator_2_get_Entry_m2420324981_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_get_Entry_m2420324981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = DictionaryEnumerator_2_get_Current_m2488022850((DictionaryEnumerator_2_t4062924206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return ((*(DictionaryEntry_t1751606614 *)((DictionaryEntry_t1751606614 *)UnBox (L_0, DictionaryEntry_t1751606614_il2cpp_TypeInfo_var))));
	}
}
extern "C"  DictionaryEntry_t1751606614  DictionaryEnumerator_2_get_Entry_m2420324981_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	return DictionaryEnumerator_2_get_Entry_m2420324981(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m3310238376_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	DictionaryEntry_t1751606614  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DictionaryEntry_t1751606614  L_0 = DictionaryEnumerator_2_get_Entry_m2420324981((DictionaryEnumerator_2_t4062924206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (DictionaryEntry_t1751606614 )L_0;
		Il2CppObject * L_1 = DictionaryEntry_get_Key_m3516209325((DictionaryEntry_t1751606614 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m3310238376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	return DictionaryEnumerator_2_get_Key_m3310238376(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m3911716666_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	DictionaryEntry_t1751606614  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DictionaryEntry_t1751606614  L_0 = DictionaryEnumerator_2_get_Entry_m2420324981((DictionaryEnumerator_2_t4062924206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (DictionaryEntry_t1751606614 )L_0;
		Il2CppObject * L_1 = DictionaryEntry_get_Value_m4281303039((DictionaryEntry_t1751606614 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m3911716666_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	return DictionaryEnumerator_2_get_Value_m3911716666(_thisAdjusted, method);
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_get_Current_m2488022850_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m2488022850_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_get_Current_m2488022850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1944668977  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_0);
		KeyValuePair_2_t1944668977  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t1944668977  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (KeyValuePair_2_t1944668977 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject*)L_3);
		KeyValuePair_2_t1944668977  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t1944668977  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_3);
		V_0 = (KeyValuePair_2_t1944668977 )L_4;
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m4250204908((KeyValuePair_2_t1944668977 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t1751606614  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2600671860(&L_6, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		DictionaryEntry_t1751606614  L_7 = L_6;
		Il2CppObject * L_8 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m2488022850_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	return DictionaryEnumerator_2_get_Current_m2488022850(_thisAdjusted, method);
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_MoveNext_m1748601087_MetadataUsageId;
extern "C"  bool DictionaryEnumerator_2_MoveNext_m1748601087_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_MoveNext_m1748601087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
extern "C"  bool DictionaryEnumerator_2_MoveNext_m1748601087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	return DictionaryEnumerator_2_MoveNext_m1748601087(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::Reset()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryEnumerator_2_Reset_m1189624908_MetadataUsageId;
extern "C"  void DictionaryEnumerator_2_Reset_m1189624908_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryEnumerator_2_Reset_m1189624908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__e_0();
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
extern "C"  void DictionaryEnumerator_2_Reset_m1189624908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DictionaryEnumerator_2_t4062924206 * _thisAdjusted = reinterpret_cast<DictionaryEnumerator_2_t4062924206 *>(__this + 1);
	DictionaryEnumerator_2_Reset_m1189624908(_thisAdjusted, method);
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Add_m169742595_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Add_m169742595_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Add_m169742595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		return;
	}

IL_0020:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_6 = ___key0;
		Il2CppObject * L_7 = ___value1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		return;
	}

IL_0036:
	{
		NotSupportedException_t1732551818 * L_8 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::ContainsKey(TKey)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_ContainsKey_m575413467_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_ContainsKey_m575413467_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_ContainsKey_m575413467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_001a:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject*)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Collections.Generic.ICollection`1<TKey> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Keys()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Keys_m2676182786_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Keys_m2676182786_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Keys_m2676182786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		List_1_t1244034627 * L_4 = ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_4;
	}

IL_001e:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		return L_6;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m2795913013_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Remove_m2795913013_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m2795913013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return (bool)1;
	}

IL_002e:
	{
		return (bool)0;
	}

IL_0030:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_7 = ___key0;
		NullCheck((Il2CppObject*)L_6);
		bool L_8 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_6, (Il2CppObject *)L_7);
		return L_8;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_TryGetValue_m2022140468_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_TryGetValue_m2022140468_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_TryGetValue_m2022140468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject ** L_4 = ___value1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, L_4);
		return (bool)0;
	}

IL_0024:
	{
		Il2CppObject ** L_5 = ___value1;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_7 = ___key0;
		NullCheck((Il2CppObject *)L_6);
		Il2CppObject * L_8 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		(*(Il2CppObject **)L_5) = ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_5, ((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return (bool)1;
	}

IL_0042:
	{
		Il2CppObject* L_9 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_10 = ___key0;
		Il2CppObject ** L_11 = ___value1;
		NullCheck((Il2CppObject*)L_9);
		bool L_12 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_9, (Il2CppObject *)L_10, (Il2CppObject **)L_11);
		return L_12;
	}
}
// TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Item(TKey)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Item_m1244011961_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_get_Item_m1244011961_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Item_m1244011961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		return ((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
	}

IL_001f:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_set_Item_m2397100594_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_set_Item_m2397100594_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_set_Item_m2397100594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		return;
	}

IL_0020:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Add_m758248834_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Add_m758248834_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Add_m758248834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		KeyValuePair_2_t1944668977  L_2 = ___item0;
		KeyValuePair_2_t1944668977  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_1, IList_t1751339649_il2cpp_TypeInfo_var)));
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IList_t1751339649_il2cpp_TypeInfo_var)), (Il2CppObject *)L_4);
		return;
	}

IL_0020:
	{
		Il2CppObject* L_5 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t1944668977  L_7 = ___item0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker1< KeyValuePair_2_t1944668977  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_6, (KeyValuePair_2_t1944668977 )L_7);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Clear()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Clear_m1785475101_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Clear_m1785475101_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Clear_m1785475101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IDictionary::Clear() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return;
	}

IL_0014:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_2);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Contains_m482039724_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Contains_m482039724_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Contains_m482039724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		KeyValuePair_2_t1944668977  L_2 = ___item0;
		KeyValuePair_2_t1944668977  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_1, IList_t1751339649_il2cpp_TypeInfo_var)));
		bool L_5 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IList_t1751339649_il2cpp_TypeInfo_var)), (Il2CppObject *)L_4);
		return L_5;
	}

IL_001f:
	{
		Il2CppObject* L_6 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t1944668977  L_7 = ___item0;
		NullCheck((Il2CppObject*)L_6);
		bool L_8 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1944668977  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_6, (KeyValuePair_2_t1944668977 )L_7);
		return L_8;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_CopyTo_m4028713190_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_CopyTo_m4028713190_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_CopyTo_m4028713190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	DictionaryEntry_t1751606614  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		V_0 = (Il2CppObject *)L_2;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004b;
		}

IL_0016:
		{
			Il2CppObject * L_3 = V_0;
			NullCheck((Il2CppObject *)L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			V_1 = (DictionaryEntry_t1751606614 )((*(DictionaryEntry_t1751606614 *)((DictionaryEntry_t1751606614 *)UnBox (L_4, DictionaryEntry_t1751606614_il2cpp_TypeInfo_var))));
			KeyValuePair_2U5BU5D_t2483180780* L_5 = ___array0;
			int32_t L_6 = ___arrayIndex1;
			int32_t L_7 = (int32_t)L_6;
			___arrayIndex1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
			Il2CppObject * L_8 = DictionaryEntry_get_Key_m3516209325((DictionaryEntry_t1751606614 *)(&V_1), /*hidden argument*/NULL);
			Il2CppObject * L_9 = DictionaryEntry_get_Value_m4281303039((DictionaryEntry_t1751606614 *)(&V_1), /*hidden argument*/NULL);
			KeyValuePair_2_t1944668977  L_10;
			memset(&L_10, 0, sizeof(L_10));
			KeyValuePair_2__ctor_m4168265535(&L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (Il2CppObject *)((Il2CppObject *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_7);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (KeyValuePair_2_t1944668977 )L_10);
		}

IL_004b:
		{
			Il2CppObject * L_11 = V_0;
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0016;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_13 = V_0;
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_13, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_14 = V_2;
			if (!L_14)
			{
				goto IL_0065;
			}
		}

IL_005f:
		{
			Il2CppObject * L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(85)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0066:
	{
		Il2CppObject* L_16 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2U5BU5D_t2483180780* L_17 = ___array0;
		int32_t L_18 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_16);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2483180780*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_16, (KeyValuePair_2U5BU5D_t2483180780*)L_17, (int32_t)L_18);
	}

IL_0073:
	{
		return;
	}
}
// System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Count()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_Count_m451164914_MetadataUsageId;
extern "C"  int32_t DictionaryWrapper_2_get_Count_m451164914_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_Count_m451164914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_IsReadOnly()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_get_IsReadOnly_m1339546929_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_get_IsReadOnly_m1339546929_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_get_IsReadOnly_m1339546929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IDictionary::get_IsReadOnly() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m743668177_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_Remove_m743668177_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m743668177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_0066;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		Il2CppObject * L_7 = KeyValuePair_2_get_Value_m4250204908((KeyValuePair_2_t1944668977 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		bool L_8 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0064;
		}
	}
	{
		Il2CppObject * L_9 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_10 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)(&___item0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_9, (Il2CppObject *)L_10);
		return (bool)1;
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}

IL_0068:
	{
		Il2CppObject* L_11 = (Il2CppObject*)__this->get__genericDictionary_1();
		KeyValuePair_2_t1944668977  L_12 = ___item0;
		NullCheck((Il2CppObject*)L_11);
		bool L_13 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1944668977  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_11, (KeyValuePair_2_t1944668977 )L_12);
		return L_13;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::GetEnumerator()
extern const MethodInfo* Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881_MethodInfo_var;
extern const uint32_t DictionaryWrapper_2_GetEnumerator_m2402075769_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryWrapper_2_GetEnumerator_m2402075769_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_GetEnumerator_m2402075769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Func_2_t1623933992 * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	Func_2_t1623933992 * G_B2_0 = NULL;
	Il2CppObject* G_B2_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject* L_2 = Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/Enumerable_Cast_TisDictionaryEntry_t1751606614_m659260881_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10));
		Func_2_t1623933992 * L_3 = ((U3CU3Ec_t1205074931_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)->static_fields)->get_U3CU3E9__25_0_1();
		Func_2_t1623933992 * L_4 = (Func_2_t1623933992 *)L_3;
		G_B2_0 = L_4;
		G_B2_1 = L_2;
		if (L_4)
		{
			G_B3_0 = L_4;
			G_B3_1 = L_2;
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10));
		U3CU3Ec_t1205074931 * L_5 = ((U3CU3Ec_t1205074931_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Func_2_t1623933992 * L_7 = (Func_2_t1623933992 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (Func_2_t1623933992 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		Func_2_t1623933992 * L_8 = (Func_2_t1623933992 *)L_7;
		((U3CU3Ec_t1205074931_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10)->static_fields)->set_U3CU3E9__25_0_1(L_8);
		G_B3_0 = L_8;
		G_B3_1 = G_B2_1;
	}

IL_0032:
	{
		Il2CppObject* L_9 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1623933992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)G_B3_1, (Func_2_t1623933992 *)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 15), (Il2CppObject*)L_9);
		return L_10;
	}

IL_003d:
	{
		Il2CppObject* L_11 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject* L_12 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 15), (Il2CppObject*)L_11);
		return L_12;
	}
}
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2289560565_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	{
		NullCheck((DictionaryWrapper_2_t3980492226 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((DictionaryWrapper_2_t3980492226 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return L_0;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		return;
	}

IL_0016:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Add(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		return L_3;
	}

IL_0015:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		return L_6;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3);
		return;
	}

IL_0016:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_5 = ___key0;
		Il2CppObject * L_6 = ___value1;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}

IL_0014:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 15), (Il2CppObject*)L_3);
		DictionaryEnumerator_2_t4062924206  L_5;
		memset(&L_5, 0, sizeof(L_5));
		DictionaryEnumerator_2__ctor_m3301157341(&L_5, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		DictionaryEnumerator_2_t4062924206  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 17), &L_6);
		return (Il2CppObject *)L_7;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		bool L_3 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::ContainsKey(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		return L_3;
	}

IL_001a:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_5 = ___key0;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		return L_6;
	}
}
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__genericDictionary_1();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__genericDictionary_1();
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_1);
		List_1_t1244034627 * L_3 = ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_3;
	}

IL_0019:
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		return L_5;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_Remove_m1555833598_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_Remove_m1555833598_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_Remove_m1555833598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		return;
	}

IL_0015:
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Il2CppObject*)L_3);
		InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Object,System.Object>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_3, (Il2CppObject *)((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506_MetadataUsageId;
extern "C"  void DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		Il2CppArray * L_2 = ___array0;
		int32_t L_3 = ___index1;
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_1, (Il2CppArray *)L_2, (int32_t)L_3);
		return;
	}

IL_0016:
	{
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__genericDictionary_1();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2483180780*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_4, (KeyValuePair_2U5BU5D_t2483180780*)((KeyValuePair_2U5BU5D_t2483180780*)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19))), (int32_t)L_6);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476_MetadataUsageId;
extern "C"  bool DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		return L_2;
	}

IL_0014:
	{
		return (bool)0;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176_MetadataUsageId;
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__syncRoot_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of__syncRoot_2();
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		Interlocked_CompareExchange_m1450605286(NULL /*static, unused*/, (Il2CppObject **)L_1, (Il2CppObject *)L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
	}

IL_001a:
	{
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__syncRoot_2();
		return L_3;
	}
}
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_UnderlyingDictionary()
extern "C"  Il2CppObject * DictionaryWrapper_2_get_UnderlyingDictionary_m150371939_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__dictionary_0();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__dictionary_0();
		return L_1;
	}

IL_000f:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get__genericDictionary_1();
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0_1__ctor_m726501149_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::<CreateMethodCall>b__0(T,System.Object[])
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__0_m923796313_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method)
{
	{
		ConstructorInfo_t4136801618 * L_0 = (ConstructorInfo_t4136801618 *)__this->get_c_0();
		ObjectU5BU5D_t1108656482* L_1 = ___a1;
		NullCheck((ConstructorInfo_t4136801618 *)L_0);
		Il2CppObject * L_2 = ConstructorInfo_Invoke_m759007899((ConstructorInfo_t4136801618 *)L_0, (ObjectU5BU5D_t1108656482*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::<CreateMethodCall>b__1(T,System.Object[])
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__1_m514972088_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method)
{
	{
		MethodBase_t318515428 * L_0 = (MethodBase_t318515428 *)__this->get_method_1();
		Il2CppObject * L_1 = ___o0;
		ObjectU5BU5D_t1108656482* L_2 = ___a1;
		NullCheck((MethodBase_t318515428 *)L_0);
		Il2CppObject * L_3 = MethodBase_Invoke_m3435166155((MethodBase_t318515428 *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t1108656482*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0_1__ctor_m1419602846_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::<CreateDefaultConstructor>b__0()
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__0_m1122221088_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = (Type_t *)__this->get_type_0();
		Il2CppObject * L_1 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1<System.Object>::<CreateDefaultConstructor>b__1()
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass5_0_1_U3CCreateDefaultConstructorU3Eb__1_m1122222049_gshared (U3CU3Ec__DisplayClass5_0_1_t1229468012 * __this, const MethodInfo* method)
{
	{
		ConstructorInfo_t4136801618 * L_0 = (ConstructorInfo_t4136801618 *)__this->get_constructorInfo_1();
		NullCheck((ConstructorInfo_t4136801618 *)L_0);
		Il2CppObject * L_1 = ConstructorInfo_Invoke_m759007899((ConstructorInfo_t4136801618 *)L_0, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass6_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass6_0_1__ctor_m2112704543_gshared (U3CU3Ec__DisplayClass6_0_1_t3057173573 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass6_0`1<System.Object>::<CreateGet>b__0(T)
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass6_0_1_U3CCreateGetU3Eb__0_m347927752_gshared (U3CU3Ec__DisplayClass6_0_1_t3057173573 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((PropertyInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(25 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0_1__ctor_m2805806240_gshared (U3CU3Ec__DisplayClass7_0_1_t589911838 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>::<CreateGet>b__0(T)
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass7_0_1_U3CCreateGetU3Eb__0_m1809507751_gshared (U3CU3Ec__DisplayClass7_0_1_t589911838 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		NullCheck((FieldInfo_t *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass8_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0_1__ctor_m3498907937_gshared (U3CU3Ec__DisplayClass8_0_1_t2417617399 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass8_0`1<System.Object>::<CreateSet>b__0(T,System.Object)
extern "C"  void U3CU3Ec__DisplayClass8_0_1_U3CCreateSetU3Eb__0_m2868634091_gshared (U3CU3Ec__DisplayClass8_0_1_t2417617399 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (FieldInfo_t *)__this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((FieldInfo_t *)L_0);
		FieldInfo_SetValue_m1669444927((FieldInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0_1__ctor_m4192009634_gshared (U3CU3Ec__DisplayClass9_0_1_t4245322960 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>::<CreateSet>b__0(T,System.Object)
extern "C"  void U3CU3Ec__DisplayClass9_0_1_U3CCreateSetU3Eb__0_m3306643594_gshared (U3CU3Ec__DisplayClass9_0_1_t4245322960 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = (PropertyInfo_t *)__this->get_propertyInfo_0();
		Il2CppObject * L_1 = ___o0;
		Il2CppObject * L_2 = ___v1;
		NullCheck((PropertyInfo_t *)L_0);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(27 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, (PropertyInfo_t *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		return;
	}
}
// System.Void Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void MethodCall_2__ctor_m4173381250_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::Invoke(T,System.Object[])
extern "C"  Il2CppObject * MethodCall_2_Invoke_m1327760016_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MethodCall_2_Invoke_m1327760016((MethodCall_2_t1809280638 *)__this->get_prev_9(),___target0, ___args1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___target0, ___args1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MethodCall_2_BeginInvoke_m4137169899_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___target0;
	__d_args[1] = ___args1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * MethodCall_2_EndInvoke_m2253508144_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15_0_1__ctor_m2755217215_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::<ForgivingCaseSensitiveFind>b__0(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652_MetadataUsageId;
extern "C"  bool U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, Il2CppObject * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t315946507 * L_0 = (Func_2_t315946507 *)__this->get_valueSelector_0();
		Il2CppObject * L_1 = ___s0;
		NullCheck((Func_2_t315946507 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t315946507 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t315946507 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_Equals_m2001218004(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)5, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::<ForgivingCaseSensitiveFind>b__1(TSource)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147_MetadataUsageId;
extern "C"  bool U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, Il2CppObject * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_2_t315946507 * L_0 = (Func_2_t315946507 *)__this->get_valueSelector_0();
		Il2CppObject * L_1 = ___s0;
		NullCheck((Func_2_t315946507 *)L_0);
		String_t* L_2 = ((  String_t* (*) (Func_2_t315946507 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Func_2_t315946507 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		String_t* L_3 = (String_t*)__this->get_testValue_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_Equals_m2001218004(NULL /*static, unused*/, (String_t*)L_2, (String_t*)L_3, (int32_t)4, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m713611997_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m713611997_gshared (ThreadSafeStore_2_t648438005 * __this, Func_2_t2253398629 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m713611997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t2253398629 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		Func_2_t2253398629 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		Dictionary_2_t4114722875 * L_4 = (Dictionary_2_t4114722875 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__store_1(L_4);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m2486152451_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t4114722875 * L_0 = (Dictionary_2_t4114722875 *)__this->get__store_1();
		TypeNameKey_t2971844791  L_1 = ___key0;
		NullCheck((Dictionary_2_t4114722875 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t4114722875 *)L_0, (TypeNameKey_t2971844791 )L_1, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		TypeNameKey_t2971844791  L_3 = ___key0;
		NullCheck((ThreadSafeStore_2_t648438005 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t648438005 *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ThreadSafeStore_2_t648438005 *)__this, (TypeNameKey_t2971844791 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}

IL_0018:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::AddValue(TKey)
extern Il2CppClass* Thread_t1973216770_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeStore_2_AddValue_m3702308719_MetadataUsageId;
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m3702308719_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2_AddValue_m3702308719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t4114722875 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t2253398629 * L_0 = (Func_2_t2253398629 *)__this->get__creator_2();
		TypeNameKey_t2971844791  L_1 = ___key0;
		NullCheck((Func_2_t2253398629 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t2253398629 *, TypeNameKey_t2971844791 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t2253398629 *)L_0, (TypeNameKey_t2971844791 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t4114722875 * L_5 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_003c;
			}
		}

IL_0022:
		{
			Dictionary_2_t4114722875 * L_6 = (Dictionary_2_t4114722875 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t4114722875 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			__this->set__store_1(L_6);
			Dictionary_2_t4114722875 * L_7 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			TypeNameKey_t2971844791  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t4114722875 *)L_7);
			((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4114722875 *)L_7, (TypeNameKey_t2971844791 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0071;
		}

IL_003c:
		{
			Dictionary_2_t4114722875 * L_10 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			TypeNameKey_t2971844791  L_11 = ___key0;
			NullCheck((Dictionary_2_t4114722875 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t4114722875 *)L_10, (TypeNameKey_t2971844791 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			if (!L_12)
			{
				goto IL_0051;
			}
		}

IL_004c:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}

IL_0051:
		{
			Dictionary_2_t4114722875 * L_14 = (Dictionary_2_t4114722875 *)__this->get__store_1();
			Dictionary_2_t4114722875 * L_15 = (Dictionary_2_t4114722875 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t4114722875 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t4114722875 *)L_15;
			Dictionary_2_t4114722875 * L_16 = V_3;
			TypeNameKey_t2971844791  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t4114722875 *)L_16);
			((  void (*) (Dictionary_2_t4114722875 *, TypeNameKey_t2971844791 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4114722875 *)L_16, (TypeNameKey_t2971844791 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t1973216770_il2cpp_TypeInfo_var);
			Thread_MemoryBarrier_m232301001(NULL /*static, unused*/, /*hidden argument*/NULL);
			Dictionary_2_t4114722875 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0071:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_007d:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m72384408_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m72384408_gshared (ThreadSafeStore_2_t3745804690 * __this, Func_2_t1055798018 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m72384408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t1055798018 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		Func_2_t1055798018 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		Dictionary_2_t2917122264 * L_4 = (Dictionary_2_t2917122264 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2917122264 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__store_1(L_4);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m2316141778_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2917122264 * L_0 = (Dictionary_2_t2917122264 *)__this->get__store_1();
		TypeConvertKey_t866134174  L_1 = ___key0;
		NullCheck((Dictionary_2_t2917122264 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2917122264 *)L_0, (TypeConvertKey_t866134174 )L_1, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		TypeConvertKey_t866134174  L_3 = ___key0;
		NullCheck((ThreadSafeStore_2_t3745804690 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t3745804690 *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ThreadSafeStore_2_t3745804690 *)__this, (TypeConvertKey_t866134174 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}

IL_0018:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::AddValue(TKey)
extern Il2CppClass* Thread_t1973216770_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeStore_2_AddValue_m1276670208_MetadataUsageId;
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m1276670208_gshared (ThreadSafeStore_2_t3745804690 * __this, TypeConvertKey_t866134174  ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2_AddValue_m1276670208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2917122264 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t1055798018 * L_0 = (Func_2_t1055798018 *)__this->get__creator_2();
		TypeConvertKey_t866134174  L_1 = ___key0;
		NullCheck((Func_2_t1055798018 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t1055798018 *, TypeConvertKey_t866134174 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t1055798018 *)L_0, (TypeConvertKey_t866134174 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2917122264 * L_5 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_003c;
			}
		}

IL_0022:
		{
			Dictionary_2_t2917122264 * L_6 = (Dictionary_2_t2917122264 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t2917122264 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			__this->set__store_1(L_6);
			Dictionary_2_t2917122264 * L_7 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			TypeConvertKey_t866134174  L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2917122264 *)L_7);
			((  void (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2917122264 *)L_7, (TypeConvertKey_t866134174 )L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0071;
		}

IL_003c:
		{
			Dictionary_2_t2917122264 * L_10 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			TypeConvertKey_t866134174  L_11 = ___key0;
			NullCheck((Dictionary_2_t2917122264 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2917122264 *)L_10, (TypeConvertKey_t866134174 )L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			if (!L_12)
			{
				goto IL_0051;
			}
		}

IL_004c:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}

IL_0051:
		{
			Dictionary_2_t2917122264 * L_14 = (Dictionary_2_t2917122264 *)__this->get__store_1();
			Dictionary_2_t2917122264 * L_15 = (Dictionary_2_t2917122264 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t2917122264 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2917122264 *)L_15;
			Dictionary_2_t2917122264 * L_16 = V_3;
			TypeConvertKey_t866134174  L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2917122264 *)L_16);
			((  void (*) (Dictionary_2_t2917122264 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2917122264 *)L_16, (TypeConvertKey_t866134174 )L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t1973216770_il2cpp_TypeInfo_var);
			Thread_MemoryBarrier_m232301001(NULL /*static, unused*/, /*hidden argument*/NULL);
			Dictionary_2_t2917122264 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0071:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_007d:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1028554796;
extern const uint32_t ThreadSafeStore_2__ctor_m2250270936_MetadataUsageId;
extern "C"  void ThreadSafeStore_2__ctor_m2250270936_gshared (ThreadSafeStore_2_t2874570697 * __this, Func_2_t184564025 * ___creator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2__ctor_m2250270936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Func_2_t184564025 * L_1 = ___creator0;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral1028554796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001f:
	{
		Func_2_t184564025 * L_3 = ___creator0;
		__this->set__creator_2(L_3);
		Dictionary_2_t2045888271 * L_4 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2045888271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__store_1(L_4);
		return;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m4035272722_gshared (ThreadSafeStore_2_t2874570697 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2045888271 * L_0 = (Dictionary_2_t2045888271 *)__this->get__store_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2045888271 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2045888271 *)L_0, (Il2CppObject *)L_1, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_3 = ___key0;
		NullCheck((ThreadSafeStore_2_t2874570697 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (ThreadSafeStore_2_t2874570697 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ThreadSafeStore_2_t2874570697 *)__this, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_4;
	}

IL_0018:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Object>::AddValue(TKey)
extern Il2CppClass* Thread_t1973216770_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeStore_2_AddValue_m2055708096_MetadataUsageId;
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m2055708096_gshared (ThreadSafeStore_2_t2874570697 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeStore_2_AddValue_m2055708096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2045888271 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Func_2_t184564025 * L_0 = (Func_2_t184564025 *)__this->get__creator_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Func_2_t184564025 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Func_2_t184564025 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Func_2_t184564025 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2045888271 * L_5 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			if (L_5)
			{
				goto IL_003c;
			}
		}

IL_0022:
		{
			Dictionary_2_t2045888271 * L_6 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t2045888271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			__this->set__store_1(L_6);
			Dictionary_2_t2045888271 * L_7 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2045888271 *)L_7);
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2045888271 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0071;
		}

IL_003c:
		{
			Dictionary_2_t2045888271 * L_10 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2045888271 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2045888271 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			if (!L_12)
			{
				goto IL_0051;
			}
		}

IL_004c:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}

IL_0051:
		{
			Dictionary_2_t2045888271 * L_14 = (Dictionary_2_t2045888271 *)__this->get__store_1();
			Dictionary_2_t2045888271 * L_15 = (Dictionary_2_t2045888271 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_3 = (Dictionary_2_t2045888271 *)L_15;
			Dictionary_2_t2045888271 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2045888271 *)L_16);
			((  void (*) (Dictionary_2_t2045888271 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2045888271 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t1973216770_il2cpp_TypeInfo_var);
			Thread_MemoryBarrier_m232301001(NULL /*static, unused*/, /*hidden argument*/NULL);
			Dictionary_2_t2045888271 * L_19 = V_3;
			__this->set__store_1(L_19);
		}

IL_0071:
		{
			Il2CppObject * L_20 = V_0;
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_007d:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m3958676923_gshared (Singleton_1_t128664468 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t667441552 *)__this);
		MonoBehaviour__ctor_m2022291967((MonoBehaviour_t667441552 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton`1<System.Object>::.cctor()
extern "C"  void Singleton_1__cctor_m1977804114_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// T Singleton`1<System.Object>::get_GetInstance()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3793977261;
extern const uint32_t Singleton_1_get_GetInstance_m102549980_MetadataUsageId;
extern "C"  Il2CppObject * Singleton_1_get_GetInstance_m102549980_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Singleton_1_get_GetInstance_m102549980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		Il2CppObject * L_4 = ((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_instance_2();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3071478659 *)L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Object_t3071478659 *)(*(((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_instance_2())));
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, (Object_t3071478659 *)(*(((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_address_of_instance_2())));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, (String_t*)_stringLiteral3793977261, (String_t*)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_8 = ((Singleton_1_t128664468_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_instance_2();
		return L_8;
	}
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1976743594_gshared (Action_1_t464558559 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m1737599660_gshared (Action_1_t464558559 * __this, AdvertisingResult_t68742423  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1737599660((Action_1_t464558559 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, AdvertisingResult_t68742423  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, AdvertisingResult_t68742423  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AdvertisingResult_t68742423_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4261843751_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4261843751_gshared (Action_1_t464558559 * __this, AdvertisingResult_t68742423  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4261843751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AdvertisingResult_t68742423_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1250954682_gshared (Action_1_t464558559 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m843564858_gshared (Action_1_t260648783 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::Invoke(T)
extern "C"  void Action_1_Invoke_m1153105866_gshared (Action_1_t260648783 * __this, ConnectionRequest_t4159799943  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1153105866((Action_1_t260648783 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ConnectionRequest_t4159799943  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ConnectionRequest_t4159799943  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ConnectionRequest_t4159799943_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m685527191_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m685527191_gshared (Action_1_t260648783 * __this, ConnectionRequest_t4159799943  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m685527191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConnectionRequest_t4159799943_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1824777802_gshared (Action_1_t260648783 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2084931440_gshared (Action_1_t380876779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::Invoke(T)
extern "C"  void Action_1_Invoke_m1137087460_gshared (Action_1_t380876779 * __this, ConnectionResponse_t4280027939  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1137087460((Action_1_t380876779 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ConnectionResponse_t4280027939  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ConnectionResponse_t4280027939  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ConnectionResponse_t4280027939_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2383279401_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2383279401_gshared (Action_1_t380876779 * __this, ConnectionResponse_t4280027939  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2383279401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConnectionResponse_t4280027939_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4091949440_gshared (Action_1_t380876779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2262397789_gshared (Action_1_t914279838 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m2110401095_gshared (Action_1_t914279838 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2110401095((Action_1_t914279838 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitializationStatus_t518463702_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1004290204_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1004290204_gshared (Action_1_t914279838 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1004290204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationStatus_t518463702_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1660885997_gshared (Action_1_t914279838 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1165386806_gshared (Action_1_t823521528 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m2641070648_gshared (Action_1_t823521528 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2641070648((Action_1_t823521528 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.UIStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t427705392_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3585610779_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3585610779_gshared (Action_1_t823521528 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3585610779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIStatus_t427705392_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m685793606_gshared (Action_1_t823521528 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m776178832_gshared (Action_1_t3071188627 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m1122706011_gshared (Action_1_t3071188627 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1122706011((Action_1_t3071188627 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2949719512_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2949719512_gshared (Action_1_t3071188627 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2949719512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2871752617_gshared (Action_1_t3071188627 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m4187092907_gshared (Action_1_t150760668 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m3024449142_gshared (Action_1_t150760668 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3024449142((Action_1_t150760668 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ResponseStatus_t4049911828_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1198763927_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1198763927_gshared (Action_1_t150760668 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1198763927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ResponseStatus_t4049911828_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m453118674_gshared (Action_1_t150760668 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3778664536_gshared (Action_1_t3953224079 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m1050560035_gshared (Action_1_t3953224079 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1050560035((Action_1_t3953224079 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t3557407943_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3200757002_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3200757002_gshared (Action_1_t3953224079 * __this, int32_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3200757002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIStatus_t3557407943_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4278683455_gshared (Action_1_t3953224079 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m377969142_gshared (Action_1_t872614854 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3594021162_gshared (Action_1_t872614854 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3594021162((Action_1_t872614854 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t872614854 * __this, bool ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t872614854 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m356506525_gshared (Action_1_t111250811 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.IntPtr>::Invoke(T)
extern "C"  void Action_1_Invoke_m2730854504_gshared (Action_1_t111250811 * __this, IntPtr_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2730854504((Action_1_t111250811 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.IntPtr>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4121364331_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4121364331_gshared (Action_1_t111250811 * __this, IntPtr_t ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4121364331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1135551990_gshared (Action_1_t111250811 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m881151526_gshared (Action_1_t271665211 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m663971678_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m663971678((Action_1_t271665211 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t271665211 * __this, Il2CppObject * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t271665211 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m907218493_gshared (Action_2_t1729022911 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m970130894_gshared (Action_2_t1729022911 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m970130894((Action_2_t1729022911 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* CommonStatusCodes_t671203459_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1465736309_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1465736309_gshared (Action_2_t1729022911 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1465736309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CommonStatusCodes_t671203459_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2247698125_gshared (Action_2_t1729022911 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2746623473_gshared (Action_2_t1524908924 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m4075160986_gshared (Action_2_t1524908924 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m4075160986((Action_2_t1524908924 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* QuestAcceptStatus_t3857037258_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1730471097_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1730471097_gshared (Action_2_t1524908924 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1730471097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(QuestAcceptStatus_t3857037258_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3349428353_gshared (Action_2_t1524908924 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m884641853_gshared (Action_2_t3406652893 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3208528846_gshared (Action_2_t3406652893 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3208528846((Action_2_t3406652893 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* ResponseStatus_t419677757_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3751373293_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3751373293_gshared (Action_2_t3406652893 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3751373293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ResponseStatus_t419677757_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2024909005_gshared (Action_2_t3406652893 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1546088691_gshared (Action_2_t2661426558 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2057936216_gshared (Action_2_t2661426558 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2057936216((Action_2_t2661426558 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* SavedGameRequestStatus_t3786215536_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m2131733111_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2131733111_gshared (Action_2_t2661426558 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2131733111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SavedGameRequestStatus_t3786215536_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1323783299_gshared (Action_2_t2661426558 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2564041433_gshared (Action_2_t1740094460 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3217670066_gshared (Action_2_t1740094460 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3217670066((Action_2_t1740094460 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* SelectUIStatus_t4210182474_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3813320401_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3813320401_gshared (Action_2_t1740094460 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3813320401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SelectUIStatus_t4210182474_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1763242345_gshared (Action_2_t1740094460 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m4251406634_gshared (Action_2_t1255729854 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2069375169_gshared (Action_2_t1255729854 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2069375169((Action_2_t1255729854 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t427705392_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1417546976_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1417546976_gshared (Action_2_t1255729854 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1417546976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t427705392_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3583371322_gshared (Action_2_t1255729854 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1942110965_gshared (Action_2_t2345743608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2625457686_gshared (Action_2_t2345743608 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2625457686((Action_2_t2345743608 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m516909757_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m516909757_gshared (Action_2_t2345743608 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m516909757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3710875525_gshared (Action_2_t2345743608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3309597785_gshared (Action_2_t599046810 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m923698738_gshared (Action_2_t599046810 * __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m923698738((Action_2_t599046810 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m2135774937_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2135774937_gshared (Action_2_t599046810 * __this, Il2CppObject * ___arg10, bool ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2135774937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2751209_gshared (Action_2_t599046810 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3776211162_gshared (Action_2_t4132650063 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.IntPtr>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2324013841_gshared (Action_2_t4132650063 * __this, Il2CppObject * ___arg10, IntPtr_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2324013841((Action_2_t4132650063 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, IntPtr_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, IntPtr_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.IntPtr>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m4250389808_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m4250389808_gshared (Action_2_t4132650063 * __this, Il2CppObject * ___arg10, IntPtr_t ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m4250389808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1499617258_gshared (Action_2_t4132650063 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m2309492639_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m172731500_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m172731500((Action_2_t4293064463 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3413657584_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3926193450_gshared (Action_2_t4293064463 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2714700114_gshared (Action_3_t708612388 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3247580302_gshared (Action_3_t708612388 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3247580302((Action_3_t708612388 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* QuestClaimMilestoneStatus_t935666006_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m2235337925_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m2235337925_gshared (Action_3_t708612388 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m2235337925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(QuestClaimMilestoneStatus_t935666006_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m1806693474_gshared (Action_3_t708612388 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2603739911_gshared (Action_3_t178818711 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3469900163_gshared (Action_3_t178818711 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3469900163((Action_3_t178818711 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* QuestUiResult_t3187252993_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1158295226_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1158295226_gshared (Action_3_t178818711 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1158295226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(QuestUiResult_t3187252993_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m1187641495_gshared (Action_3_t178818711 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m756414230_gshared (Action_3_t4239861309 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3126836050_gshared (Action_3_t4239861309 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3126836050((Action_3_t4239861309 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern Il2CppClass* MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m107478785_MetadataUsageId;
extern "C"  Il2CppObject * Action_3_BeginInvoke_m107478785_gshared (Action_3_t4239861309 * __this, int32_t ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m107478785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3590087718_gshared (Action_3_t4239861309 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m22279495_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3949892547_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3949892547((Action_3_t2968268857 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1386617082_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3792380631_gshared (Action_3_t2968268857 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m593249855_gshared (Action_4_t2331760108 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Boolean>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m3761155659_gshared (Action_4_t2331760108 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, bool ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m3761155659((Action_4_t2331760108 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, bool ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, bool ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, bool ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Object,System.Object,System.Object,System.Boolean>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Action_4_BeginInvoke_m1301136724_MetadataUsageId;
extern "C"  Il2CppObject * Action_4_BeginInvoke_m1301136724_gshared (Action_4_t2331760108 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, bool ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_4_BeginInvoke_m1301136724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m487763919_gshared (Action_4_t2331760108 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_4__ctor_m620347252_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  void Action_4_Invoke_m157716918_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_4_Invoke_m157716918((Action_4_t1730810465 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_4_BeginInvoke_m1414752311_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void System.Action`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_4_EndInvoke_m2556365700_gshared (Action_4_t1730810465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
