﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayLength
struct ArrayLength_t92600621;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayLength::.ctor()
extern "C"  void ArrayLength__ctor_m370341033 (ArrayLength_t92600621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::Reset()
extern "C"  void ArrayLength_Reset_m2311741270 (ArrayLength_t92600621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnEnter()
extern "C"  void ArrayLength_OnEnter_m3102143040 (ArrayLength_t92600621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnUpdate()
extern "C"  void ArrayLength_OnUpdate_m810713091 (ArrayLength_t92600621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
