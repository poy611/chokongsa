﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoStopLoopedEffect
struct CFX_AutoStopLoopedEffect_t3902828655;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoStopLoopedEffect::.ctor()
extern "C"  void CFX_AutoStopLoopedEffect__ctor_m1509370636 (CFX_AutoStopLoopedEffect_t3902828655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoStopLoopedEffect::OnEnable()
extern "C"  void CFX_AutoStopLoopedEffect_OnEnable_m3719573754 (CFX_AutoStopLoopedEffect_t3902828655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoStopLoopedEffect::Update()
extern "C"  void CFX_AutoStopLoopedEffect_Update_m1272711553 (CFX_AutoStopLoopedEffect_t3902828655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
