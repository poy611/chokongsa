﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GotoPreviousState
struct GotoPreviousState_t1461179269;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::.ctor()
extern "C"  void GotoPreviousState__ctor_m303624017 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::Reset()
extern "C"  void GotoPreviousState_Reset_m2245024254 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::OnEnter()
extern "C"  void GotoPreviousState_OnEnter_m3411600104 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
