﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionStay2D
struct PlayMakerCollisionStay2D_t1291817165;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"

// System.Void PlayMakerCollisionStay2D::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionStay2D_OnCollisionStay2D_m2197627513 (PlayMakerCollisionStay2D_t1291817165 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionStay2D::.ctor()
extern "C"  void PlayMakerCollisionStay2D__ctor_m654267952 (PlayMakerCollisionStay2D_t1291817165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
