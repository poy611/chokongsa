﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ingredient
struct  Ingredient_t1787055601  : public MonoBehaviour_t667441552
{
public:
	// System.String Ingredient::BEAN_ORGN
	String_t* ___BEAN_ORGN_2;
	// System.String Ingredient::BEAN_FLOOR
	String_t* ___BEAN_FLOOR_3;
	// System.String Ingredient::BEAN_ESPRESSO
	String_t* ___BEAN_ESPRESSO_4;
	// System.String Ingredient::WATER_ORGN
	String_t* ___WATER_ORGN_5;
	// System.String Ingredient::WATER_HOT
	String_t* ___WATER_HOT_6;
	// System.String Ingredient::WATER_COLD
	String_t* ___WATER_COLD_7;
	// System.String Ingredient::WATER_ICE
	String_t* ___WATER_ICE_8;
	// System.String Ingredient::MILK_ORGN
	String_t* ___MILK_ORGN_9;
	// System.String Ingredient::MILK_STEAM
	String_t* ___MILK_STEAM_10;
	// System.String Ingredient::MILK_COLD
	String_t* ___MILK_COLD_11;
	// System.String Ingredient::SYRUP_CARAMEL
	String_t* ___SYRUP_CARAMEL_12;
	// System.String Ingredient::SYRUP_CHOCO
	String_t* ___SYRUP_CHOCO_13;
	// System.String Ingredient::SYRUP_HAZELNUT
	String_t* ___SYRUP_HAZELNUT_14;
	// System.String Ingredient::SYRUP_VANILLA
	String_t* ___SYRUP_VANILLA_15;
	// System.String Ingredient::HCREAM_ORGN
	String_t* ___HCREAM_ORGN_16;
	// System.String Ingredient::HCREAM_MACCHIATO
	String_t* ___HCREAM_MACCHIATO_17;
	// System.String Ingredient::HCREAM_CHOCO
	String_t* ___HCREAM_CHOCO_18;

public:
	inline static int32_t get_offset_of_BEAN_ORGN_2() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___BEAN_ORGN_2)); }
	inline String_t* get_BEAN_ORGN_2() const { return ___BEAN_ORGN_2; }
	inline String_t** get_address_of_BEAN_ORGN_2() { return &___BEAN_ORGN_2; }
	inline void set_BEAN_ORGN_2(String_t* value)
	{
		___BEAN_ORGN_2 = value;
		Il2CppCodeGenWriteBarrier(&___BEAN_ORGN_2, value);
	}

	inline static int32_t get_offset_of_BEAN_FLOOR_3() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___BEAN_FLOOR_3)); }
	inline String_t* get_BEAN_FLOOR_3() const { return ___BEAN_FLOOR_3; }
	inline String_t** get_address_of_BEAN_FLOOR_3() { return &___BEAN_FLOOR_3; }
	inline void set_BEAN_FLOOR_3(String_t* value)
	{
		___BEAN_FLOOR_3 = value;
		Il2CppCodeGenWriteBarrier(&___BEAN_FLOOR_3, value);
	}

	inline static int32_t get_offset_of_BEAN_ESPRESSO_4() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___BEAN_ESPRESSO_4)); }
	inline String_t* get_BEAN_ESPRESSO_4() const { return ___BEAN_ESPRESSO_4; }
	inline String_t** get_address_of_BEAN_ESPRESSO_4() { return &___BEAN_ESPRESSO_4; }
	inline void set_BEAN_ESPRESSO_4(String_t* value)
	{
		___BEAN_ESPRESSO_4 = value;
		Il2CppCodeGenWriteBarrier(&___BEAN_ESPRESSO_4, value);
	}

	inline static int32_t get_offset_of_WATER_ORGN_5() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___WATER_ORGN_5)); }
	inline String_t* get_WATER_ORGN_5() const { return ___WATER_ORGN_5; }
	inline String_t** get_address_of_WATER_ORGN_5() { return &___WATER_ORGN_5; }
	inline void set_WATER_ORGN_5(String_t* value)
	{
		___WATER_ORGN_5 = value;
		Il2CppCodeGenWriteBarrier(&___WATER_ORGN_5, value);
	}

	inline static int32_t get_offset_of_WATER_HOT_6() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___WATER_HOT_6)); }
	inline String_t* get_WATER_HOT_6() const { return ___WATER_HOT_6; }
	inline String_t** get_address_of_WATER_HOT_6() { return &___WATER_HOT_6; }
	inline void set_WATER_HOT_6(String_t* value)
	{
		___WATER_HOT_6 = value;
		Il2CppCodeGenWriteBarrier(&___WATER_HOT_6, value);
	}

	inline static int32_t get_offset_of_WATER_COLD_7() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___WATER_COLD_7)); }
	inline String_t* get_WATER_COLD_7() const { return ___WATER_COLD_7; }
	inline String_t** get_address_of_WATER_COLD_7() { return &___WATER_COLD_7; }
	inline void set_WATER_COLD_7(String_t* value)
	{
		___WATER_COLD_7 = value;
		Il2CppCodeGenWriteBarrier(&___WATER_COLD_7, value);
	}

	inline static int32_t get_offset_of_WATER_ICE_8() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___WATER_ICE_8)); }
	inline String_t* get_WATER_ICE_8() const { return ___WATER_ICE_8; }
	inline String_t** get_address_of_WATER_ICE_8() { return &___WATER_ICE_8; }
	inline void set_WATER_ICE_8(String_t* value)
	{
		___WATER_ICE_8 = value;
		Il2CppCodeGenWriteBarrier(&___WATER_ICE_8, value);
	}

	inline static int32_t get_offset_of_MILK_ORGN_9() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___MILK_ORGN_9)); }
	inline String_t* get_MILK_ORGN_9() const { return ___MILK_ORGN_9; }
	inline String_t** get_address_of_MILK_ORGN_9() { return &___MILK_ORGN_9; }
	inline void set_MILK_ORGN_9(String_t* value)
	{
		___MILK_ORGN_9 = value;
		Il2CppCodeGenWriteBarrier(&___MILK_ORGN_9, value);
	}

	inline static int32_t get_offset_of_MILK_STEAM_10() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___MILK_STEAM_10)); }
	inline String_t* get_MILK_STEAM_10() const { return ___MILK_STEAM_10; }
	inline String_t** get_address_of_MILK_STEAM_10() { return &___MILK_STEAM_10; }
	inline void set_MILK_STEAM_10(String_t* value)
	{
		___MILK_STEAM_10 = value;
		Il2CppCodeGenWriteBarrier(&___MILK_STEAM_10, value);
	}

	inline static int32_t get_offset_of_MILK_COLD_11() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___MILK_COLD_11)); }
	inline String_t* get_MILK_COLD_11() const { return ___MILK_COLD_11; }
	inline String_t** get_address_of_MILK_COLD_11() { return &___MILK_COLD_11; }
	inline void set_MILK_COLD_11(String_t* value)
	{
		___MILK_COLD_11 = value;
		Il2CppCodeGenWriteBarrier(&___MILK_COLD_11, value);
	}

	inline static int32_t get_offset_of_SYRUP_CARAMEL_12() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___SYRUP_CARAMEL_12)); }
	inline String_t* get_SYRUP_CARAMEL_12() const { return ___SYRUP_CARAMEL_12; }
	inline String_t** get_address_of_SYRUP_CARAMEL_12() { return &___SYRUP_CARAMEL_12; }
	inline void set_SYRUP_CARAMEL_12(String_t* value)
	{
		___SYRUP_CARAMEL_12 = value;
		Il2CppCodeGenWriteBarrier(&___SYRUP_CARAMEL_12, value);
	}

	inline static int32_t get_offset_of_SYRUP_CHOCO_13() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___SYRUP_CHOCO_13)); }
	inline String_t* get_SYRUP_CHOCO_13() const { return ___SYRUP_CHOCO_13; }
	inline String_t** get_address_of_SYRUP_CHOCO_13() { return &___SYRUP_CHOCO_13; }
	inline void set_SYRUP_CHOCO_13(String_t* value)
	{
		___SYRUP_CHOCO_13 = value;
		Il2CppCodeGenWriteBarrier(&___SYRUP_CHOCO_13, value);
	}

	inline static int32_t get_offset_of_SYRUP_HAZELNUT_14() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___SYRUP_HAZELNUT_14)); }
	inline String_t* get_SYRUP_HAZELNUT_14() const { return ___SYRUP_HAZELNUT_14; }
	inline String_t** get_address_of_SYRUP_HAZELNUT_14() { return &___SYRUP_HAZELNUT_14; }
	inline void set_SYRUP_HAZELNUT_14(String_t* value)
	{
		___SYRUP_HAZELNUT_14 = value;
		Il2CppCodeGenWriteBarrier(&___SYRUP_HAZELNUT_14, value);
	}

	inline static int32_t get_offset_of_SYRUP_VANILLA_15() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___SYRUP_VANILLA_15)); }
	inline String_t* get_SYRUP_VANILLA_15() const { return ___SYRUP_VANILLA_15; }
	inline String_t** get_address_of_SYRUP_VANILLA_15() { return &___SYRUP_VANILLA_15; }
	inline void set_SYRUP_VANILLA_15(String_t* value)
	{
		___SYRUP_VANILLA_15 = value;
		Il2CppCodeGenWriteBarrier(&___SYRUP_VANILLA_15, value);
	}

	inline static int32_t get_offset_of_HCREAM_ORGN_16() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___HCREAM_ORGN_16)); }
	inline String_t* get_HCREAM_ORGN_16() const { return ___HCREAM_ORGN_16; }
	inline String_t** get_address_of_HCREAM_ORGN_16() { return &___HCREAM_ORGN_16; }
	inline void set_HCREAM_ORGN_16(String_t* value)
	{
		___HCREAM_ORGN_16 = value;
		Il2CppCodeGenWriteBarrier(&___HCREAM_ORGN_16, value);
	}

	inline static int32_t get_offset_of_HCREAM_MACCHIATO_17() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___HCREAM_MACCHIATO_17)); }
	inline String_t* get_HCREAM_MACCHIATO_17() const { return ___HCREAM_MACCHIATO_17; }
	inline String_t** get_address_of_HCREAM_MACCHIATO_17() { return &___HCREAM_MACCHIATO_17; }
	inline void set_HCREAM_MACCHIATO_17(String_t* value)
	{
		___HCREAM_MACCHIATO_17 = value;
		Il2CppCodeGenWriteBarrier(&___HCREAM_MACCHIATO_17, value);
	}

	inline static int32_t get_offset_of_HCREAM_CHOCO_18() { return static_cast<int32_t>(offsetof(Ingredient_t1787055601, ___HCREAM_CHOCO_18)); }
	inline String_t* get_HCREAM_CHOCO_18() const { return ___HCREAM_CHOCO_18; }
	inline String_t** get_address_of_HCREAM_CHOCO_18() { return &___HCREAM_CHOCO_18; }
	inline void set_HCREAM_CHOCO_18(String_t* value)
	{
		___HCREAM_CHOCO_18 = value;
		Il2CppCodeGenWriteBarrier(&___HCREAM_CHOCO_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
