﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct KeyCollection_t248914419;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>
struct Dictionary_2_t2917122264;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct IEnumerator_1_t2777999223;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey[]
struct TypeConvertKeyU5BU5D_t1979890475;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3532058318.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m615471304_gshared (KeyCollection_t248914419 * __this, Dictionary_2_t2917122264 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m615471304(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t248914419 *, Dictionary_2_t2917122264 *, const MethodInfo*))KeyCollection__ctor_m615471304_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3106120590_gshared (KeyCollection_t248914419 * __this, TypeConvertKey_t866134174  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3106120590(__this, ___item0, method) ((  void (*) (KeyCollection_t248914419 *, TypeConvertKey_t866134174 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3106120590_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m830035077_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m830035077(__this, method) ((  void (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m830035077_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4183801404_gshared (KeyCollection_t248914419 * __this, TypeConvertKey_t866134174  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4183801404(__this, ___item0, method) ((  bool (*) (KeyCollection_t248914419 *, TypeConvertKey_t866134174 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4183801404_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m264146593_gshared (KeyCollection_t248914419 * __this, TypeConvertKey_t866134174  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m264146593(__this, ___item0, method) ((  bool (*) (KeyCollection_t248914419 *, TypeConvertKey_t866134174 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m264146593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1908115521_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1908115521(__this, method) ((  Il2CppObject* (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1908115521_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1821350263_gshared (KeyCollection_t248914419 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1821350263(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t248914419 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1821350263_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1911044786_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1911044786(__this, method) ((  Il2CppObject * (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1911044786_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3424818845_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3424818845(__this, method) ((  bool (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3424818845_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1400229775_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1400229775(__this, method) ((  bool (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1400229775_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m748150907_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m748150907(__this, method) ((  Il2CppObject * (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m748150907_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m703717245_gshared (KeyCollection_t248914419 * __this, TypeConvertKeyU5BU5D_t1979890475* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m703717245(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t248914419 *, TypeConvertKeyU5BU5D_t1979890475*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m703717245_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3532058318  KeyCollection_GetEnumerator_m2373790176_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2373790176(__this, method) ((  Enumerator_t3532058318  (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_GetEnumerator_m2373790176_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m946221653_gshared (KeyCollection_t248914419 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m946221653(__this, method) ((  int32_t (*) (KeyCollection_t248914419 *, const MethodInfo*))KeyCollection_get_Count_m946221653_gshared)(__this, method)
