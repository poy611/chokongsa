﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmTexture
struct SetFsmTexture_t38942603;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::.ctor()
extern "C"  void SetFsmTexture__ctor_m926639627 (SetFsmTexture_t38942603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::Reset()
extern "C"  void SetFsmTexture_Reset_m2868039864 (SetFsmTexture_t38942603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnEnter()
extern "C"  void SetFsmTexture_OnEnter_m834179874 (SetFsmTexture_t38942603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::DoSetFsmBool()
extern "C"  void SetFsmTexture_DoSetFsmBool_m2214022540 (SetFsmTexture_t38942603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnUpdate()
extern "C"  void SetFsmTexture_OnUpdate_m3518298977 (SetFsmTexture_t38942603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
