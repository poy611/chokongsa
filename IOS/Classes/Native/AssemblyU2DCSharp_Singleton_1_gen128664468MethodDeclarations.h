﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Singleton`1<System.Object>
struct Singleton_1_t128664468;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Singleton`1<System.Object>::.ctor()
extern "C"  void Singleton_1__ctor_m3958676923_gshared (Singleton_1_t128664468 * __this, const MethodInfo* method);
#define Singleton_1__ctor_m3958676923(__this, method) ((  void (*) (Singleton_1_t128664468 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// System.Void Singleton`1<System.Object>::.cctor()
extern "C"  void Singleton_1__cctor_m1977804114_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1__cctor_m1977804114(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m1977804114_gshared)(__this /* static, unused */, method)
// T Singleton`1<System.Object>::get_GetInstance()
extern "C"  Il2CppObject * Singleton_1_get_GetInstance_m102549980_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Singleton_1_get_GetInstance_m102549980(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_GetInstance_m102549980_gshared)(__this /* static, unused */, method)
