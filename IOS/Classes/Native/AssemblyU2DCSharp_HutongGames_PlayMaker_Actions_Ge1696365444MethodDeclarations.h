﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2
struct GetAtan2_t1696365444;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2::.ctor()
extern "C"  void GetAtan2__ctor_m3949376354 (GetAtan2_t1696365444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::Reset()
extern "C"  void GetAtan2_Reset_m1595809295 (GetAtan2_t1696365444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnEnter()
extern "C"  void GetAtan2_OnEnter_m2286282425 (GetAtan2_t1696365444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnUpdate()
extern "C"  void GetAtan2_OnUpdate_m1288837802 (GetAtan2_t1696365444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::DoATan()
extern "C"  void GetAtan2_DoATan_m2433353037 (GetAtan2_t1696365444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
