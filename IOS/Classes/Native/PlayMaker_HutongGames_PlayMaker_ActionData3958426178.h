﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t3683564144;
// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>
struct Dictionary_2_t2672984508;
// System.Collections.Generic.Dictionary`2<System.Type,System.Int32>
struct Dictionary_2_t1259260985;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t1046271522;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t144696915;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t3065333419;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t1620082664;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t4054181541;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t353063272;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t4154693685;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t3192090493;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t1000377263;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t2333180753;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t2321044203;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t2189661721;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t2964336089;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray>
struct List_1_t3497852427;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>
struct List_1_t2444233947;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat>
struct List_1_t3502288398;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt>
struct List_1_t2964324001;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool>
struct List_1_t2444145348;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2>
struct List_1_t1902098433;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3>
struct List_1_t1902098434;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor>
struct List_1_t3499604757;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect>
struct List_1_t2444612030;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion>
struct List_1_t944354296;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t4230795212;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t4040850731;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionData
struct  ActionData_t3958426178  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::actionNames
	List_1_t1375417109 * ___actionNames_8;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customNames
	List_1_t1375417109 * ___customNames_9;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionEnabled
	List_1_t1844984270 * ___actionEnabled_10;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionIsOpen
	List_1_t1844984270 * ___actionIsOpen_11;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionStartIndex
	List_1_t2522024052 * ___actionStartIndex_12;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionHashCodes
	List_1_t2522024052 * ___actionHashCodes_13;
	// System.Collections.Generic.List`1<UnityEngine.Object> HutongGames.PlayMaker.ActionData::unityObjectParams
	List_1_t144696915 * ___unityObjectParams_14;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::fsmGameObjectParams
	List_1_t3065333419 * ___fsmGameObjectParams_15;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::fsmOwnerDefaultParams
	List_1_t1620082664 * ___fsmOwnerDefaultParams_16;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::animationCurveParams
	List_1_t4054181541 * ___animationCurveParams_17;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::functionCallParams
	List_1_t353063272 * ___functionCallParams_18;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::fsmTemplateControlParams
	List_1_t4154693685 * ___fsmTemplateControlParams_19;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::fsmEventTargetParams
	List_1_t3192090493 * ___fsmEventTargetParams_20;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::fsmPropertyParams
	List_1_t1000377263 * ___fsmPropertyParams_21;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::layoutOptionParams
	List_1_t2333180753 * ___layoutOptionParams_22;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::fsmStringParams
	List_1_t2321044203 * ___fsmStringParams_23;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::fsmObjectParams
	List_1_t2189661721 * ___fsmObjectParams_24;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::fsmVarParams
	List_1_t2964336089 * ___fsmVarParams_25;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray> HutongGames.PlayMaker.ActionData::fsmArrayParams
	List_1_t3497852427 * ___fsmArrayParams_26;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum> HutongGames.PlayMaker.ActionData::fsmEnumParams
	List_1_t2444233947 * ___fsmEnumParams_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat> HutongGames.PlayMaker.ActionData::fsmFloatParams
	List_1_t3502288398 * ___fsmFloatParams_28;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt> HutongGames.PlayMaker.ActionData::fsmIntParams
	List_1_t2964324001 * ___fsmIntParams_29;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool> HutongGames.PlayMaker.ActionData::fsmBoolParams
	List_1_t2444145348 * ___fsmBoolParams_30;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2> HutongGames.PlayMaker.ActionData::fsmVector2Params
	List_1_t1902098433 * ___fsmVector2Params_31;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3> HutongGames.PlayMaker.ActionData::fsmVector3Params
	List_1_t1902098434 * ___fsmVector3Params_32;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor> HutongGames.PlayMaker.ActionData::fsmColorParams
	List_1_t3499604757 * ___fsmColorParams_33;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect> HutongGames.PlayMaker.ActionData::fsmRectParams
	List_1_t2444612030 * ___fsmRectParams_34;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion> HutongGames.PlayMaker.ActionData::fsmQuaternionParams
	List_1_t944354296 * ___fsmQuaternionParams_35;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::stringParams
	List_1_t1375417109 * ___stringParams_36;
	// System.Collections.Generic.List`1<System.Byte> HutongGames.PlayMaker.ActionData::byteData
	List_1_t4230795212 * ___byteData_37;
	// System.Byte[] HutongGames.PlayMaker.ActionData::byteDataAsArray
	ByteU5BU5D_t4260760469* ___byteDataAsArray_38;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::arrayParamSizes
	List_1_t2522024052 * ___arrayParamSizes_39;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::arrayParamTypes
	List_1_t1375417109 * ___arrayParamTypes_40;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::customTypeSizes
	List_1_t2522024052 * ___customTypeSizes_41;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customTypeNames
	List_1_t1375417109 * ___customTypeNames_42;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType> HutongGames.PlayMaker.ActionData::paramDataType
	List_1_t4040850731 * ___paramDataType_43;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::paramName
	List_1_t1375417109 * ___paramName_44;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramDataPos
	List_1_t2522024052 * ___paramDataPos_45;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramByteDataSize
	List_1_t2522024052 * ___paramByteDataSize_46;
	// System.Int32 HutongGames.PlayMaker.ActionData::nextParamIndex
	int32_t ___nextParamIndex_47;

public:
	inline static int32_t get_offset_of_actionNames_8() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionNames_8)); }
	inline List_1_t1375417109 * get_actionNames_8() const { return ___actionNames_8; }
	inline List_1_t1375417109 ** get_address_of_actionNames_8() { return &___actionNames_8; }
	inline void set_actionNames_8(List_1_t1375417109 * value)
	{
		___actionNames_8 = value;
		Il2CppCodeGenWriteBarrier(&___actionNames_8, value);
	}

	inline static int32_t get_offset_of_customNames_9() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customNames_9)); }
	inline List_1_t1375417109 * get_customNames_9() const { return ___customNames_9; }
	inline List_1_t1375417109 ** get_address_of_customNames_9() { return &___customNames_9; }
	inline void set_customNames_9(List_1_t1375417109 * value)
	{
		___customNames_9 = value;
		Il2CppCodeGenWriteBarrier(&___customNames_9, value);
	}

	inline static int32_t get_offset_of_actionEnabled_10() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionEnabled_10)); }
	inline List_1_t1844984270 * get_actionEnabled_10() const { return ___actionEnabled_10; }
	inline List_1_t1844984270 ** get_address_of_actionEnabled_10() { return &___actionEnabled_10; }
	inline void set_actionEnabled_10(List_1_t1844984270 * value)
	{
		___actionEnabled_10 = value;
		Il2CppCodeGenWriteBarrier(&___actionEnabled_10, value);
	}

	inline static int32_t get_offset_of_actionIsOpen_11() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionIsOpen_11)); }
	inline List_1_t1844984270 * get_actionIsOpen_11() const { return ___actionIsOpen_11; }
	inline List_1_t1844984270 ** get_address_of_actionIsOpen_11() { return &___actionIsOpen_11; }
	inline void set_actionIsOpen_11(List_1_t1844984270 * value)
	{
		___actionIsOpen_11 = value;
		Il2CppCodeGenWriteBarrier(&___actionIsOpen_11, value);
	}

	inline static int32_t get_offset_of_actionStartIndex_12() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionStartIndex_12)); }
	inline List_1_t2522024052 * get_actionStartIndex_12() const { return ___actionStartIndex_12; }
	inline List_1_t2522024052 ** get_address_of_actionStartIndex_12() { return &___actionStartIndex_12; }
	inline void set_actionStartIndex_12(List_1_t2522024052 * value)
	{
		___actionStartIndex_12 = value;
		Il2CppCodeGenWriteBarrier(&___actionStartIndex_12, value);
	}

	inline static int32_t get_offset_of_actionHashCodes_13() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___actionHashCodes_13)); }
	inline List_1_t2522024052 * get_actionHashCodes_13() const { return ___actionHashCodes_13; }
	inline List_1_t2522024052 ** get_address_of_actionHashCodes_13() { return &___actionHashCodes_13; }
	inline void set_actionHashCodes_13(List_1_t2522024052 * value)
	{
		___actionHashCodes_13 = value;
		Il2CppCodeGenWriteBarrier(&___actionHashCodes_13, value);
	}

	inline static int32_t get_offset_of_unityObjectParams_14() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___unityObjectParams_14)); }
	inline List_1_t144696915 * get_unityObjectParams_14() const { return ___unityObjectParams_14; }
	inline List_1_t144696915 ** get_address_of_unityObjectParams_14() { return &___unityObjectParams_14; }
	inline void set_unityObjectParams_14(List_1_t144696915 * value)
	{
		___unityObjectParams_14 = value;
		Il2CppCodeGenWriteBarrier(&___unityObjectParams_14, value);
	}

	inline static int32_t get_offset_of_fsmGameObjectParams_15() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmGameObjectParams_15)); }
	inline List_1_t3065333419 * get_fsmGameObjectParams_15() const { return ___fsmGameObjectParams_15; }
	inline List_1_t3065333419 ** get_address_of_fsmGameObjectParams_15() { return &___fsmGameObjectParams_15; }
	inline void set_fsmGameObjectParams_15(List_1_t3065333419 * value)
	{
		___fsmGameObjectParams_15 = value;
		Il2CppCodeGenWriteBarrier(&___fsmGameObjectParams_15, value);
	}

	inline static int32_t get_offset_of_fsmOwnerDefaultParams_16() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmOwnerDefaultParams_16)); }
	inline List_1_t1620082664 * get_fsmOwnerDefaultParams_16() const { return ___fsmOwnerDefaultParams_16; }
	inline List_1_t1620082664 ** get_address_of_fsmOwnerDefaultParams_16() { return &___fsmOwnerDefaultParams_16; }
	inline void set_fsmOwnerDefaultParams_16(List_1_t1620082664 * value)
	{
		___fsmOwnerDefaultParams_16 = value;
		Il2CppCodeGenWriteBarrier(&___fsmOwnerDefaultParams_16, value);
	}

	inline static int32_t get_offset_of_animationCurveParams_17() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___animationCurveParams_17)); }
	inline List_1_t4054181541 * get_animationCurveParams_17() const { return ___animationCurveParams_17; }
	inline List_1_t4054181541 ** get_address_of_animationCurveParams_17() { return &___animationCurveParams_17; }
	inline void set_animationCurveParams_17(List_1_t4054181541 * value)
	{
		___animationCurveParams_17 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurveParams_17, value);
	}

	inline static int32_t get_offset_of_functionCallParams_18() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___functionCallParams_18)); }
	inline List_1_t353063272 * get_functionCallParams_18() const { return ___functionCallParams_18; }
	inline List_1_t353063272 ** get_address_of_functionCallParams_18() { return &___functionCallParams_18; }
	inline void set_functionCallParams_18(List_1_t353063272 * value)
	{
		___functionCallParams_18 = value;
		Il2CppCodeGenWriteBarrier(&___functionCallParams_18, value);
	}

	inline static int32_t get_offset_of_fsmTemplateControlParams_19() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmTemplateControlParams_19)); }
	inline List_1_t4154693685 * get_fsmTemplateControlParams_19() const { return ___fsmTemplateControlParams_19; }
	inline List_1_t4154693685 ** get_address_of_fsmTemplateControlParams_19() { return &___fsmTemplateControlParams_19; }
	inline void set_fsmTemplateControlParams_19(List_1_t4154693685 * value)
	{
		___fsmTemplateControlParams_19 = value;
		Il2CppCodeGenWriteBarrier(&___fsmTemplateControlParams_19, value);
	}

	inline static int32_t get_offset_of_fsmEventTargetParams_20() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmEventTargetParams_20)); }
	inline List_1_t3192090493 * get_fsmEventTargetParams_20() const { return ___fsmEventTargetParams_20; }
	inline List_1_t3192090493 ** get_address_of_fsmEventTargetParams_20() { return &___fsmEventTargetParams_20; }
	inline void set_fsmEventTargetParams_20(List_1_t3192090493 * value)
	{
		___fsmEventTargetParams_20 = value;
		Il2CppCodeGenWriteBarrier(&___fsmEventTargetParams_20, value);
	}

	inline static int32_t get_offset_of_fsmPropertyParams_21() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmPropertyParams_21)); }
	inline List_1_t1000377263 * get_fsmPropertyParams_21() const { return ___fsmPropertyParams_21; }
	inline List_1_t1000377263 ** get_address_of_fsmPropertyParams_21() { return &___fsmPropertyParams_21; }
	inline void set_fsmPropertyParams_21(List_1_t1000377263 * value)
	{
		___fsmPropertyParams_21 = value;
		Il2CppCodeGenWriteBarrier(&___fsmPropertyParams_21, value);
	}

	inline static int32_t get_offset_of_layoutOptionParams_22() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___layoutOptionParams_22)); }
	inline List_1_t2333180753 * get_layoutOptionParams_22() const { return ___layoutOptionParams_22; }
	inline List_1_t2333180753 ** get_address_of_layoutOptionParams_22() { return &___layoutOptionParams_22; }
	inline void set_layoutOptionParams_22(List_1_t2333180753 * value)
	{
		___layoutOptionParams_22 = value;
		Il2CppCodeGenWriteBarrier(&___layoutOptionParams_22, value);
	}

	inline static int32_t get_offset_of_fsmStringParams_23() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmStringParams_23)); }
	inline List_1_t2321044203 * get_fsmStringParams_23() const { return ___fsmStringParams_23; }
	inline List_1_t2321044203 ** get_address_of_fsmStringParams_23() { return &___fsmStringParams_23; }
	inline void set_fsmStringParams_23(List_1_t2321044203 * value)
	{
		___fsmStringParams_23 = value;
		Il2CppCodeGenWriteBarrier(&___fsmStringParams_23, value);
	}

	inline static int32_t get_offset_of_fsmObjectParams_24() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmObjectParams_24)); }
	inline List_1_t2189661721 * get_fsmObjectParams_24() const { return ___fsmObjectParams_24; }
	inline List_1_t2189661721 ** get_address_of_fsmObjectParams_24() { return &___fsmObjectParams_24; }
	inline void set_fsmObjectParams_24(List_1_t2189661721 * value)
	{
		___fsmObjectParams_24 = value;
		Il2CppCodeGenWriteBarrier(&___fsmObjectParams_24, value);
	}

	inline static int32_t get_offset_of_fsmVarParams_25() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmVarParams_25)); }
	inline List_1_t2964336089 * get_fsmVarParams_25() const { return ___fsmVarParams_25; }
	inline List_1_t2964336089 ** get_address_of_fsmVarParams_25() { return &___fsmVarParams_25; }
	inline void set_fsmVarParams_25(List_1_t2964336089 * value)
	{
		___fsmVarParams_25 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVarParams_25, value);
	}

	inline static int32_t get_offset_of_fsmArrayParams_26() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmArrayParams_26)); }
	inline List_1_t3497852427 * get_fsmArrayParams_26() const { return ___fsmArrayParams_26; }
	inline List_1_t3497852427 ** get_address_of_fsmArrayParams_26() { return &___fsmArrayParams_26; }
	inline void set_fsmArrayParams_26(List_1_t3497852427 * value)
	{
		___fsmArrayParams_26 = value;
		Il2CppCodeGenWriteBarrier(&___fsmArrayParams_26, value);
	}

	inline static int32_t get_offset_of_fsmEnumParams_27() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmEnumParams_27)); }
	inline List_1_t2444233947 * get_fsmEnumParams_27() const { return ___fsmEnumParams_27; }
	inline List_1_t2444233947 ** get_address_of_fsmEnumParams_27() { return &___fsmEnumParams_27; }
	inline void set_fsmEnumParams_27(List_1_t2444233947 * value)
	{
		___fsmEnumParams_27 = value;
		Il2CppCodeGenWriteBarrier(&___fsmEnumParams_27, value);
	}

	inline static int32_t get_offset_of_fsmFloatParams_28() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmFloatParams_28)); }
	inline List_1_t3502288398 * get_fsmFloatParams_28() const { return ___fsmFloatParams_28; }
	inline List_1_t3502288398 ** get_address_of_fsmFloatParams_28() { return &___fsmFloatParams_28; }
	inline void set_fsmFloatParams_28(List_1_t3502288398 * value)
	{
		___fsmFloatParams_28 = value;
		Il2CppCodeGenWriteBarrier(&___fsmFloatParams_28, value);
	}

	inline static int32_t get_offset_of_fsmIntParams_29() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmIntParams_29)); }
	inline List_1_t2964324001 * get_fsmIntParams_29() const { return ___fsmIntParams_29; }
	inline List_1_t2964324001 ** get_address_of_fsmIntParams_29() { return &___fsmIntParams_29; }
	inline void set_fsmIntParams_29(List_1_t2964324001 * value)
	{
		___fsmIntParams_29 = value;
		Il2CppCodeGenWriteBarrier(&___fsmIntParams_29, value);
	}

	inline static int32_t get_offset_of_fsmBoolParams_30() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmBoolParams_30)); }
	inline List_1_t2444145348 * get_fsmBoolParams_30() const { return ___fsmBoolParams_30; }
	inline List_1_t2444145348 ** get_address_of_fsmBoolParams_30() { return &___fsmBoolParams_30; }
	inline void set_fsmBoolParams_30(List_1_t2444145348 * value)
	{
		___fsmBoolParams_30 = value;
		Il2CppCodeGenWriteBarrier(&___fsmBoolParams_30, value);
	}

	inline static int32_t get_offset_of_fsmVector2Params_31() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmVector2Params_31)); }
	inline List_1_t1902098433 * get_fsmVector2Params_31() const { return ___fsmVector2Params_31; }
	inline List_1_t1902098433 ** get_address_of_fsmVector2Params_31() { return &___fsmVector2Params_31; }
	inline void set_fsmVector2Params_31(List_1_t1902098433 * value)
	{
		___fsmVector2Params_31 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVector2Params_31, value);
	}

	inline static int32_t get_offset_of_fsmVector3Params_32() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmVector3Params_32)); }
	inline List_1_t1902098434 * get_fsmVector3Params_32() const { return ___fsmVector3Params_32; }
	inline List_1_t1902098434 ** get_address_of_fsmVector3Params_32() { return &___fsmVector3Params_32; }
	inline void set_fsmVector3Params_32(List_1_t1902098434 * value)
	{
		___fsmVector3Params_32 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVector3Params_32, value);
	}

	inline static int32_t get_offset_of_fsmColorParams_33() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmColorParams_33)); }
	inline List_1_t3499604757 * get_fsmColorParams_33() const { return ___fsmColorParams_33; }
	inline List_1_t3499604757 ** get_address_of_fsmColorParams_33() { return &___fsmColorParams_33; }
	inline void set_fsmColorParams_33(List_1_t3499604757 * value)
	{
		___fsmColorParams_33 = value;
		Il2CppCodeGenWriteBarrier(&___fsmColorParams_33, value);
	}

	inline static int32_t get_offset_of_fsmRectParams_34() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmRectParams_34)); }
	inline List_1_t2444612030 * get_fsmRectParams_34() const { return ___fsmRectParams_34; }
	inline List_1_t2444612030 ** get_address_of_fsmRectParams_34() { return &___fsmRectParams_34; }
	inline void set_fsmRectParams_34(List_1_t2444612030 * value)
	{
		___fsmRectParams_34 = value;
		Il2CppCodeGenWriteBarrier(&___fsmRectParams_34, value);
	}

	inline static int32_t get_offset_of_fsmQuaternionParams_35() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___fsmQuaternionParams_35)); }
	inline List_1_t944354296 * get_fsmQuaternionParams_35() const { return ___fsmQuaternionParams_35; }
	inline List_1_t944354296 ** get_address_of_fsmQuaternionParams_35() { return &___fsmQuaternionParams_35; }
	inline void set_fsmQuaternionParams_35(List_1_t944354296 * value)
	{
		___fsmQuaternionParams_35 = value;
		Il2CppCodeGenWriteBarrier(&___fsmQuaternionParams_35, value);
	}

	inline static int32_t get_offset_of_stringParams_36() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___stringParams_36)); }
	inline List_1_t1375417109 * get_stringParams_36() const { return ___stringParams_36; }
	inline List_1_t1375417109 ** get_address_of_stringParams_36() { return &___stringParams_36; }
	inline void set_stringParams_36(List_1_t1375417109 * value)
	{
		___stringParams_36 = value;
		Il2CppCodeGenWriteBarrier(&___stringParams_36, value);
	}

	inline static int32_t get_offset_of_byteData_37() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___byteData_37)); }
	inline List_1_t4230795212 * get_byteData_37() const { return ___byteData_37; }
	inline List_1_t4230795212 ** get_address_of_byteData_37() { return &___byteData_37; }
	inline void set_byteData_37(List_1_t4230795212 * value)
	{
		___byteData_37 = value;
		Il2CppCodeGenWriteBarrier(&___byteData_37, value);
	}

	inline static int32_t get_offset_of_byteDataAsArray_38() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___byteDataAsArray_38)); }
	inline ByteU5BU5D_t4260760469* get_byteDataAsArray_38() const { return ___byteDataAsArray_38; }
	inline ByteU5BU5D_t4260760469** get_address_of_byteDataAsArray_38() { return &___byteDataAsArray_38; }
	inline void set_byteDataAsArray_38(ByteU5BU5D_t4260760469* value)
	{
		___byteDataAsArray_38 = value;
		Il2CppCodeGenWriteBarrier(&___byteDataAsArray_38, value);
	}

	inline static int32_t get_offset_of_arrayParamSizes_39() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___arrayParamSizes_39)); }
	inline List_1_t2522024052 * get_arrayParamSizes_39() const { return ___arrayParamSizes_39; }
	inline List_1_t2522024052 ** get_address_of_arrayParamSizes_39() { return &___arrayParamSizes_39; }
	inline void set_arrayParamSizes_39(List_1_t2522024052 * value)
	{
		___arrayParamSizes_39 = value;
		Il2CppCodeGenWriteBarrier(&___arrayParamSizes_39, value);
	}

	inline static int32_t get_offset_of_arrayParamTypes_40() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___arrayParamTypes_40)); }
	inline List_1_t1375417109 * get_arrayParamTypes_40() const { return ___arrayParamTypes_40; }
	inline List_1_t1375417109 ** get_address_of_arrayParamTypes_40() { return &___arrayParamTypes_40; }
	inline void set_arrayParamTypes_40(List_1_t1375417109 * value)
	{
		___arrayParamTypes_40 = value;
		Il2CppCodeGenWriteBarrier(&___arrayParamTypes_40, value);
	}

	inline static int32_t get_offset_of_customTypeSizes_41() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customTypeSizes_41)); }
	inline List_1_t2522024052 * get_customTypeSizes_41() const { return ___customTypeSizes_41; }
	inline List_1_t2522024052 ** get_address_of_customTypeSizes_41() { return &___customTypeSizes_41; }
	inline void set_customTypeSizes_41(List_1_t2522024052 * value)
	{
		___customTypeSizes_41 = value;
		Il2CppCodeGenWriteBarrier(&___customTypeSizes_41, value);
	}

	inline static int32_t get_offset_of_customTypeNames_42() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___customTypeNames_42)); }
	inline List_1_t1375417109 * get_customTypeNames_42() const { return ___customTypeNames_42; }
	inline List_1_t1375417109 ** get_address_of_customTypeNames_42() { return &___customTypeNames_42; }
	inline void set_customTypeNames_42(List_1_t1375417109 * value)
	{
		___customTypeNames_42 = value;
		Il2CppCodeGenWriteBarrier(&___customTypeNames_42, value);
	}

	inline static int32_t get_offset_of_paramDataType_43() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramDataType_43)); }
	inline List_1_t4040850731 * get_paramDataType_43() const { return ___paramDataType_43; }
	inline List_1_t4040850731 ** get_address_of_paramDataType_43() { return &___paramDataType_43; }
	inline void set_paramDataType_43(List_1_t4040850731 * value)
	{
		___paramDataType_43 = value;
		Il2CppCodeGenWriteBarrier(&___paramDataType_43, value);
	}

	inline static int32_t get_offset_of_paramName_44() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramName_44)); }
	inline List_1_t1375417109 * get_paramName_44() const { return ___paramName_44; }
	inline List_1_t1375417109 ** get_address_of_paramName_44() { return &___paramName_44; }
	inline void set_paramName_44(List_1_t1375417109 * value)
	{
		___paramName_44 = value;
		Il2CppCodeGenWriteBarrier(&___paramName_44, value);
	}

	inline static int32_t get_offset_of_paramDataPos_45() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramDataPos_45)); }
	inline List_1_t2522024052 * get_paramDataPos_45() const { return ___paramDataPos_45; }
	inline List_1_t2522024052 ** get_address_of_paramDataPos_45() { return &___paramDataPos_45; }
	inline void set_paramDataPos_45(List_1_t2522024052 * value)
	{
		___paramDataPos_45 = value;
		Il2CppCodeGenWriteBarrier(&___paramDataPos_45, value);
	}

	inline static int32_t get_offset_of_paramByteDataSize_46() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___paramByteDataSize_46)); }
	inline List_1_t2522024052 * get_paramByteDataSize_46() const { return ___paramByteDataSize_46; }
	inline List_1_t2522024052 ** get_address_of_paramByteDataSize_46() { return &___paramByteDataSize_46; }
	inline void set_paramByteDataSize_46(List_1_t2522024052 * value)
	{
		___paramByteDataSize_46 = value;
		Il2CppCodeGenWriteBarrier(&___paramByteDataSize_46, value);
	}

	inline static int32_t get_offset_of_nextParamIndex_47() { return static_cast<int32_t>(offsetof(ActionData_t3958426178, ___nextParamIndex_47)); }
	inline int32_t get_nextParamIndex_47() const { return ___nextParamIndex_47; }
	inline int32_t* get_address_of_nextParamIndex_47() { return &___nextParamIndex_47; }
	inline void set_nextParamIndex_47(int32_t value)
	{
		___nextParamIndex_47 = value;
	}
};

struct ActionData_t3958426178_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HutongGames.PlayMaker.ActionData::ActionTypeLookup
	Dictionary_2_t3683564144 * ___ActionTypeLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]> HutongGames.PlayMaker.ActionData::ActionFieldsLookup
	Dictionary_2_t2672984508 * ___ActionFieldsLookup_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> HutongGames.PlayMaker.ActionData::ActionHashCodeLookup
	Dictionary_2_t1259260985 * ___ActionHashCodeLookup_4;
	// System.Boolean HutongGames.PlayMaker.ActionData::resaveActionData
	bool ___resaveActionData_5;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::UsedIndices
	List_1_t2522024052 * ___UsedIndices_6;
	// System.Collections.Generic.List`1<System.Reflection.FieldInfo> HutongGames.PlayMaker.ActionData::InitFields
	List_1_t1046271522 * ___InitFields_7;

public:
	inline static int32_t get_offset_of_ActionTypeLookup_2() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionTypeLookup_2)); }
	inline Dictionary_2_t3683564144 * get_ActionTypeLookup_2() const { return ___ActionTypeLookup_2; }
	inline Dictionary_2_t3683564144 ** get_address_of_ActionTypeLookup_2() { return &___ActionTypeLookup_2; }
	inline void set_ActionTypeLookup_2(Dictionary_2_t3683564144 * value)
	{
		___ActionTypeLookup_2 = value;
		Il2CppCodeGenWriteBarrier(&___ActionTypeLookup_2, value);
	}

	inline static int32_t get_offset_of_ActionFieldsLookup_3() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionFieldsLookup_3)); }
	inline Dictionary_2_t2672984508 * get_ActionFieldsLookup_3() const { return ___ActionFieldsLookup_3; }
	inline Dictionary_2_t2672984508 ** get_address_of_ActionFieldsLookup_3() { return &___ActionFieldsLookup_3; }
	inline void set_ActionFieldsLookup_3(Dictionary_2_t2672984508 * value)
	{
		___ActionFieldsLookup_3 = value;
		Il2CppCodeGenWriteBarrier(&___ActionFieldsLookup_3, value);
	}

	inline static int32_t get_offset_of_ActionHashCodeLookup_4() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___ActionHashCodeLookup_4)); }
	inline Dictionary_2_t1259260985 * get_ActionHashCodeLookup_4() const { return ___ActionHashCodeLookup_4; }
	inline Dictionary_2_t1259260985 ** get_address_of_ActionHashCodeLookup_4() { return &___ActionHashCodeLookup_4; }
	inline void set_ActionHashCodeLookup_4(Dictionary_2_t1259260985 * value)
	{
		___ActionHashCodeLookup_4 = value;
		Il2CppCodeGenWriteBarrier(&___ActionHashCodeLookup_4, value);
	}

	inline static int32_t get_offset_of_resaveActionData_5() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___resaveActionData_5)); }
	inline bool get_resaveActionData_5() const { return ___resaveActionData_5; }
	inline bool* get_address_of_resaveActionData_5() { return &___resaveActionData_5; }
	inline void set_resaveActionData_5(bool value)
	{
		___resaveActionData_5 = value;
	}

	inline static int32_t get_offset_of_UsedIndices_6() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___UsedIndices_6)); }
	inline List_1_t2522024052 * get_UsedIndices_6() const { return ___UsedIndices_6; }
	inline List_1_t2522024052 ** get_address_of_UsedIndices_6() { return &___UsedIndices_6; }
	inline void set_UsedIndices_6(List_1_t2522024052 * value)
	{
		___UsedIndices_6 = value;
		Il2CppCodeGenWriteBarrier(&___UsedIndices_6, value);
	}

	inline static int32_t get_offset_of_InitFields_7() { return static_cast<int32_t>(offsetof(ActionData_t3958426178_StaticFields, ___InitFields_7)); }
	inline List_1_t1046271522 * get_InitFields_7() const { return ___InitFields_7; }
	inline List_1_t1046271522 ** get_address_of_InitFields_7() { return &___InitFields_7; }
	inline void set_InitFields_7(List_1_t1046271522 * value)
	{
		___InitFields_7 = value;
		Il2CppCodeGenWriteBarrier(&___InitFields_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
