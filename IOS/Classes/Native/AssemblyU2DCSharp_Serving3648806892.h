﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[,]
struct StringU5BU2CU5D_t4054002953;
// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Serving
struct  Serving_t3648806892  : public MonoBehaviour_t667441552
{
public:
	// System.String[,] Serving::rightText
	StringU5BU2CU5D_t4054002953* ___rightText_2;
	// PopupManagement Serving::popup
	PopupManagement_t282433007 * ___popup_3;
	// UnityEngine.UI.Text Serving::MayItakeYourOrder
	Text_t9039225 * ___MayItakeYourOrder_4;
	// UnityEngine.GameObject Serving::ShowmetheCoffee
	GameObject_t3674682005 * ___ShowmetheCoffee_5;
	// UnityEngine.UI.Image Serving::desk
	Image_t538875265 * ___desk_6;
	// UnityEngine.UI.Image Serving::CoffeeImage1
	Image_t538875265 * ___CoffeeImage1_7;
	// UnityEngine.UI.Image Serving::CoffeeImage2
	Image_t538875265 * ___CoffeeImage2_8;
	// UnityEngine.GameObject Serving::CreamImg1
	GameObject_t3674682005 * ___CreamImg1_9;
	// UnityEngine.GameObject Serving::CreamImg2
	GameObject_t3674682005 * ___CreamImg2_10;
	// System.Single Serving::sum
	float ___sum_11;
	// System.Single Serving::cnt
	float ___cnt_12;
	// UnityEngine.Transform Serving::imgTrans
	Transform_t1659122786 * ___imgTrans_13;
	// UnityEngine.Transform Serving::pos2
	Transform_t1659122786 * ___pos2_14;
	// UnityEngine.GameObject Serving::popServing
	GameObject_t3674682005 * ___popServing_15;
	// UnityEngine.UI.Image Serving::_teacher
	Image_t538875265 * ____teacher_16;
	// UnityEngine.UI.Image Serving::_scoreAlphabet
	Image_t538875265 * ____scoreAlphabet_17;
	// UnityEngine.UI.Text Serving::_greatText
	Text_t9039225 * ____greatText_18;
	// UnityEngine.UI.Text Serving::coffeeText
	Text_t9039225 * ___coffeeText_19;
	// System.Int32 Serving::nowNum
	int32_t ___nowNum_20;
	// System.Int32 Serving::reverse
	int32_t ___reverse_21;
	// System.Boolean Serving::okClick
	bool ___okClick_22;

public:
	inline static int32_t get_offset_of_rightText_2() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___rightText_2)); }
	inline StringU5BU2CU5D_t4054002953* get_rightText_2() const { return ___rightText_2; }
	inline StringU5BU2CU5D_t4054002953** get_address_of_rightText_2() { return &___rightText_2; }
	inline void set_rightText_2(StringU5BU2CU5D_t4054002953* value)
	{
		___rightText_2 = value;
		Il2CppCodeGenWriteBarrier(&___rightText_2, value);
	}

	inline static int32_t get_offset_of_popup_3() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___popup_3)); }
	inline PopupManagement_t282433007 * get_popup_3() const { return ___popup_3; }
	inline PopupManagement_t282433007 ** get_address_of_popup_3() { return &___popup_3; }
	inline void set_popup_3(PopupManagement_t282433007 * value)
	{
		___popup_3 = value;
		Il2CppCodeGenWriteBarrier(&___popup_3, value);
	}

	inline static int32_t get_offset_of_MayItakeYourOrder_4() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___MayItakeYourOrder_4)); }
	inline Text_t9039225 * get_MayItakeYourOrder_4() const { return ___MayItakeYourOrder_4; }
	inline Text_t9039225 ** get_address_of_MayItakeYourOrder_4() { return &___MayItakeYourOrder_4; }
	inline void set_MayItakeYourOrder_4(Text_t9039225 * value)
	{
		___MayItakeYourOrder_4 = value;
		Il2CppCodeGenWriteBarrier(&___MayItakeYourOrder_4, value);
	}

	inline static int32_t get_offset_of_ShowmetheCoffee_5() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___ShowmetheCoffee_5)); }
	inline GameObject_t3674682005 * get_ShowmetheCoffee_5() const { return ___ShowmetheCoffee_5; }
	inline GameObject_t3674682005 ** get_address_of_ShowmetheCoffee_5() { return &___ShowmetheCoffee_5; }
	inline void set_ShowmetheCoffee_5(GameObject_t3674682005 * value)
	{
		___ShowmetheCoffee_5 = value;
		Il2CppCodeGenWriteBarrier(&___ShowmetheCoffee_5, value);
	}

	inline static int32_t get_offset_of_desk_6() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___desk_6)); }
	inline Image_t538875265 * get_desk_6() const { return ___desk_6; }
	inline Image_t538875265 ** get_address_of_desk_6() { return &___desk_6; }
	inline void set_desk_6(Image_t538875265 * value)
	{
		___desk_6 = value;
		Il2CppCodeGenWriteBarrier(&___desk_6, value);
	}

	inline static int32_t get_offset_of_CoffeeImage1_7() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___CoffeeImage1_7)); }
	inline Image_t538875265 * get_CoffeeImage1_7() const { return ___CoffeeImage1_7; }
	inline Image_t538875265 ** get_address_of_CoffeeImage1_7() { return &___CoffeeImage1_7; }
	inline void set_CoffeeImage1_7(Image_t538875265 * value)
	{
		___CoffeeImage1_7 = value;
		Il2CppCodeGenWriteBarrier(&___CoffeeImage1_7, value);
	}

	inline static int32_t get_offset_of_CoffeeImage2_8() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___CoffeeImage2_8)); }
	inline Image_t538875265 * get_CoffeeImage2_8() const { return ___CoffeeImage2_8; }
	inline Image_t538875265 ** get_address_of_CoffeeImage2_8() { return &___CoffeeImage2_8; }
	inline void set_CoffeeImage2_8(Image_t538875265 * value)
	{
		___CoffeeImage2_8 = value;
		Il2CppCodeGenWriteBarrier(&___CoffeeImage2_8, value);
	}

	inline static int32_t get_offset_of_CreamImg1_9() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___CreamImg1_9)); }
	inline GameObject_t3674682005 * get_CreamImg1_9() const { return ___CreamImg1_9; }
	inline GameObject_t3674682005 ** get_address_of_CreamImg1_9() { return &___CreamImg1_9; }
	inline void set_CreamImg1_9(GameObject_t3674682005 * value)
	{
		___CreamImg1_9 = value;
		Il2CppCodeGenWriteBarrier(&___CreamImg1_9, value);
	}

	inline static int32_t get_offset_of_CreamImg2_10() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___CreamImg2_10)); }
	inline GameObject_t3674682005 * get_CreamImg2_10() const { return ___CreamImg2_10; }
	inline GameObject_t3674682005 ** get_address_of_CreamImg2_10() { return &___CreamImg2_10; }
	inline void set_CreamImg2_10(GameObject_t3674682005 * value)
	{
		___CreamImg2_10 = value;
		Il2CppCodeGenWriteBarrier(&___CreamImg2_10, value);
	}

	inline static int32_t get_offset_of_sum_11() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___sum_11)); }
	inline float get_sum_11() const { return ___sum_11; }
	inline float* get_address_of_sum_11() { return &___sum_11; }
	inline void set_sum_11(float value)
	{
		___sum_11 = value;
	}

	inline static int32_t get_offset_of_cnt_12() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___cnt_12)); }
	inline float get_cnt_12() const { return ___cnt_12; }
	inline float* get_address_of_cnt_12() { return &___cnt_12; }
	inline void set_cnt_12(float value)
	{
		___cnt_12 = value;
	}

	inline static int32_t get_offset_of_imgTrans_13() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___imgTrans_13)); }
	inline Transform_t1659122786 * get_imgTrans_13() const { return ___imgTrans_13; }
	inline Transform_t1659122786 ** get_address_of_imgTrans_13() { return &___imgTrans_13; }
	inline void set_imgTrans_13(Transform_t1659122786 * value)
	{
		___imgTrans_13 = value;
		Il2CppCodeGenWriteBarrier(&___imgTrans_13, value);
	}

	inline static int32_t get_offset_of_pos2_14() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___pos2_14)); }
	inline Transform_t1659122786 * get_pos2_14() const { return ___pos2_14; }
	inline Transform_t1659122786 ** get_address_of_pos2_14() { return &___pos2_14; }
	inline void set_pos2_14(Transform_t1659122786 * value)
	{
		___pos2_14 = value;
		Il2CppCodeGenWriteBarrier(&___pos2_14, value);
	}

	inline static int32_t get_offset_of_popServing_15() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___popServing_15)); }
	inline GameObject_t3674682005 * get_popServing_15() const { return ___popServing_15; }
	inline GameObject_t3674682005 ** get_address_of_popServing_15() { return &___popServing_15; }
	inline void set_popServing_15(GameObject_t3674682005 * value)
	{
		___popServing_15 = value;
		Il2CppCodeGenWriteBarrier(&___popServing_15, value);
	}

	inline static int32_t get_offset_of__teacher_16() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ____teacher_16)); }
	inline Image_t538875265 * get__teacher_16() const { return ____teacher_16; }
	inline Image_t538875265 ** get_address_of__teacher_16() { return &____teacher_16; }
	inline void set__teacher_16(Image_t538875265 * value)
	{
		____teacher_16 = value;
		Il2CppCodeGenWriteBarrier(&____teacher_16, value);
	}

	inline static int32_t get_offset_of__scoreAlphabet_17() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ____scoreAlphabet_17)); }
	inline Image_t538875265 * get__scoreAlphabet_17() const { return ____scoreAlphabet_17; }
	inline Image_t538875265 ** get_address_of__scoreAlphabet_17() { return &____scoreAlphabet_17; }
	inline void set__scoreAlphabet_17(Image_t538875265 * value)
	{
		____scoreAlphabet_17 = value;
		Il2CppCodeGenWriteBarrier(&____scoreAlphabet_17, value);
	}

	inline static int32_t get_offset_of__greatText_18() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ____greatText_18)); }
	inline Text_t9039225 * get__greatText_18() const { return ____greatText_18; }
	inline Text_t9039225 ** get_address_of__greatText_18() { return &____greatText_18; }
	inline void set__greatText_18(Text_t9039225 * value)
	{
		____greatText_18 = value;
		Il2CppCodeGenWriteBarrier(&____greatText_18, value);
	}

	inline static int32_t get_offset_of_coffeeText_19() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___coffeeText_19)); }
	inline Text_t9039225 * get_coffeeText_19() const { return ___coffeeText_19; }
	inline Text_t9039225 ** get_address_of_coffeeText_19() { return &___coffeeText_19; }
	inline void set_coffeeText_19(Text_t9039225 * value)
	{
		___coffeeText_19 = value;
		Il2CppCodeGenWriteBarrier(&___coffeeText_19, value);
	}

	inline static int32_t get_offset_of_nowNum_20() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___nowNum_20)); }
	inline int32_t get_nowNum_20() const { return ___nowNum_20; }
	inline int32_t* get_address_of_nowNum_20() { return &___nowNum_20; }
	inline void set_nowNum_20(int32_t value)
	{
		___nowNum_20 = value;
	}

	inline static int32_t get_offset_of_reverse_21() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___reverse_21)); }
	inline int32_t get_reverse_21() const { return ___reverse_21; }
	inline int32_t* get_address_of_reverse_21() { return &___reverse_21; }
	inline void set_reverse_21(int32_t value)
	{
		___reverse_21 = value;
	}

	inline static int32_t get_offset_of_okClick_22() { return static_cast<int32_t>(offsetof(Serving_t3648806892, ___okClick_22)); }
	inline bool get_okClick_22() const { return ___okClick_22; }
	inline bool* get_address_of_okClick_22() { return &___okClick_22; }
	inline void set_okClick_22(bool value)
	{
		___okClick_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
