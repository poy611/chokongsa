﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableFog
struct EnableFog_t3773849481;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableFog::.ctor()
extern "C"  void EnableFog__ctor_m1453658061 (EnableFog_t3773849481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::Reset()
extern "C"  void EnableFog_Reset_m3395058298 (EnableFog_t3773849481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnEnter()
extern "C"  void EnableFog_OnEnter_m492754020 (EnableFog_t3773849481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnUpdate()
extern "C"  void EnableFog_OnUpdate_m1524032095 (EnableFog_t3773849481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
