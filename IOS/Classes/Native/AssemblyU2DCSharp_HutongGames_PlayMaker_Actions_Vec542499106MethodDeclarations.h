﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Operator
struct Vector3Operator_t542499106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::.ctor()
extern "C"  void Vector3Operator__ctor_m1846443092 (Vector3Operator_t542499106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::Reset()
extern "C"  void Vector3Operator_Reset_m3787843329 (Vector3Operator_t542499106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::OnEnter()
extern "C"  void Vector3Operator_OnEnter_m2046763 (Vector3Operator_t542499106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::OnUpdate()
extern "C"  void Vector3Operator_OnUpdate_m3491976312 (Vector3Operator_t542499106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::DoVector3Operator()
extern "C"  void Vector3Operator_DoVector3Operator_m2997029307 (Vector3Operator_t542499106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
