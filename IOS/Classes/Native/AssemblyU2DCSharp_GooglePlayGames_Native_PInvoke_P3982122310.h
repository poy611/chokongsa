﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.UIntPtr,System.Object>
struct Func_2_t2301125574;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_UIntPtr3365854250.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1<System.Object>
struct  U3CToEnumerableU3Ec__Iterator5_1_t3982122310  : public Il2CppObject
{
public:
	// System.UInt64 GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::<i>__0
	uint64_t ___U3CiU3E__0_0;
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::size
	UIntPtr_t  ___size_1;
	// System.Func`2<System.UIntPtr,T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::getElement
	Func_2_t2301125574 * ___getElement_2;
	// System.Int32 GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::$PC
	int32_t ___U24PC_3;
	// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::$current
	Il2CppObject * ___U24current_4;
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::<$>size
	UIntPtr_t  ___U3CU24U3Esize_5;
	// System.Func`2<System.UIntPtr,T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator5`1::<$>getElement
	Func_2_t2301125574 * ___U3CU24U3EgetElement_6;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___U3CiU3E__0_0)); }
	inline uint64_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline uint64_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(uint64_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___size_1)); }
	inline UIntPtr_t  get_size_1() const { return ___size_1; }
	inline UIntPtr_t * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(UIntPtr_t  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_getElement_2() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___getElement_2)); }
	inline Func_2_t2301125574 * get_getElement_2() const { return ___getElement_2; }
	inline Func_2_t2301125574 ** get_address_of_getElement_2() { return &___getElement_2; }
	inline void set_getElement_2(Func_2_t2301125574 * value)
	{
		___getElement_2 = value;
		Il2CppCodeGenWriteBarrier(&___getElement_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esize_5() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___U3CU24U3Esize_5)); }
	inline UIntPtr_t  get_U3CU24U3Esize_5() const { return ___U3CU24U3Esize_5; }
	inline UIntPtr_t * get_address_of_U3CU24U3Esize_5() { return &___U3CU24U3Esize_5; }
	inline void set_U3CU24U3Esize_5(UIntPtr_t  value)
	{
		___U3CU24U3Esize_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EgetElement_6() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator5_1_t3982122310, ___U3CU24U3EgetElement_6)); }
	inline Func_2_t2301125574 * get_U3CU24U3EgetElement_6() const { return ___U3CU24U3EgetElement_6; }
	inline Func_2_t2301125574 ** get_address_of_U3CU24U3EgetElement_6() { return &___U3CU24U3EgetElement_6; }
	inline void set_U3CU24U3EgetElement_6(Func_2_t2301125574 * value)
	{
		___U3CU24U3EgetElement_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EgetElement_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
