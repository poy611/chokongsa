﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Cach1288273172MethodDeclarations.h"

// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataContractAttribute>::GetAttribute(System.Object)
#define CachedAttributeGetter_1_GetAttribute_m2189174929(__this /* static, unused */, ___type0, method) ((  DataContractAttribute_t2462274566 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m3559125336_gshared)(__this /* static, unused */, ___type0, method)
// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataContractAttribute>::.cctor()
#define CachedAttributeGetter_1__cctor_m2396513221(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m3626143424_gshared)(__this /* static, unused */, method)
