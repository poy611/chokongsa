﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t691583313 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t4166107989 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1428404869 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2583486074 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t651537830 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1474775431 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t124146534 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t1873379884 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t3250749483 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3891738222 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1637408689 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t742231849 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1292950321 ();
extern "C" void DelegatePInvokeWrapper_Action_t3771233898 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t4120438118 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t2984951347 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t83861602 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4247149838 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2578300556 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t594794173 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t2749288659 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878 ();
extern "C" void DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594 ();
extern "C" void DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818 ();
extern "C" void DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210 ();
extern "C" void DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3952708057 ();
extern "C" void DelegatePInvokeWrapper_FetchAllCallback_t3534128988 ();
extern "C" void DelegatePInvokeWrapper_FetchCallback_t1950512559 ();
extern "C" void DelegatePInvokeWrapper_ShowAllUICallback_t766686189 ();
extern "C" void DelegatePInvokeWrapper_OnAuthActionFinishedCallback_t1172336457 ();
extern "C" void DelegatePInvokeWrapper_OnAuthActionStartedCallback_t2378408244 ();
extern "C" void DelegatePInvokeWrapper_OnLogCallback_t508427221 ();
extern "C" void DelegatePInvokeWrapper_OnMultiplayerInvitationEventCallback_t3993607552 ();
extern "C" void DelegatePInvokeWrapper_OnQuestCompletedCallback_t2055592770 ();
extern "C" void DelegatePInvokeWrapper_OnTurnBasedMatchEventCallback_t2064424764 ();
extern "C" void DelegatePInvokeWrapper_FetchAllCallback_t213806343 ();
extern "C" void DelegatePInvokeWrapper_FetchCallback_t1493526500 ();
extern "C" void DelegatePInvokeWrapper_FetchServerAuthCodeCallback_t1082452761 ();
extern "C" void DelegatePInvokeWrapper_FlushCallback_t3126222379 ();
extern "C" void DelegatePInvokeWrapper_FetchAllCallback_t3766146474 ();
extern "C" void DelegatePInvokeWrapper_FetchAllScoreSummariesCallback_t1010253660 ();
extern "C" void DelegatePInvokeWrapper_FetchCallback_t2463332897 ();
extern "C" void DelegatePInvokeWrapper_FetchScorePageCallback_t1423453514 ();
extern "C" void DelegatePInvokeWrapper_FetchScoreSummaryCallback_t1723193589 ();
extern "C" void DelegatePInvokeWrapper_ShowAllUICallback_t3664260959 ();
extern "C" void DelegatePInvokeWrapper_ShowUICallback_t1832555764 ();
extern "C" void DelegatePInvokeWrapper_FetchCallback_t1233224091 ();
extern "C" void DelegatePInvokeWrapper_FetchListCallback_t2067692889 ();
extern "C" void DelegatePInvokeWrapper_FetchSelfCallback_t2479296007 ();
extern "C" void DelegatePInvokeWrapper_AcceptCallback_t1408209808 ();
extern "C" void DelegatePInvokeWrapper_ClaimMilestoneCallback_t1411568668 ();
extern "C" void DelegatePInvokeWrapper_FetchCallback_t1576861340 ();
extern "C" void DelegatePInvokeWrapper_FetchListCallback_t3150025178 ();
extern "C" void DelegatePInvokeWrapper_QuestUICallback_t3097297816 ();
extern "C" void DelegatePInvokeWrapper_OnDataReceivedCallback_t176853902 ();
extern "C" void DelegatePInvokeWrapper_OnP2PConnectedCallback_t507387838 ();
extern "C" void DelegatePInvokeWrapper_OnP2PDisconnectedCallback_t401589328 ();
extern "C" void DelegatePInvokeWrapper_OnParticipantStatusChangedCallback_t2709967058 ();
extern "C" void DelegatePInvokeWrapper_OnRoomConnectedSetChangedCallback_t3651478055 ();
extern "C" void DelegatePInvokeWrapper_OnRoomStatusChangedCallback_t1130665710 ();
extern "C" void DelegatePInvokeWrapper_FetchInvitationsCallback_t1518378837 ();
extern "C" void DelegatePInvokeWrapper_LeaveRoomCallback_t3947593351 ();
extern "C" void DelegatePInvokeWrapper_PlayerSelectUICallback_t2633669638 ();
extern "C" void DelegatePInvokeWrapper_RealTimeRoomCallback_t3285480827 ();
extern "C" void DelegatePInvokeWrapper_RoomInboxUICallback_t3659854740 ();
extern "C" void DelegatePInvokeWrapper_SendReliableMessageCallback_t2145488650 ();
extern "C" void DelegatePInvokeWrapper_WaitingRoomUICallback_t206291377 ();
extern "C" void DelegatePInvokeWrapper_CommitCallback_t1623255043 ();
extern "C" void DelegatePInvokeWrapper_FetchAllCallback_t677738899 ();
extern "C" void DelegatePInvokeWrapper_OpenCallback_t1210339286 ();
extern "C" void DelegatePInvokeWrapper_ReadCallback_t1569945378 ();
extern "C" void DelegatePInvokeWrapper_SnapshotSelectUICallback_t2531066752 ();
extern "C" void DelegatePInvokeWrapper_FetchForPlayerCallback_t1995639285 ();
extern "C" void DelegatePInvokeWrapper_MatchInboxUICallback_t3070391457 ();
extern "C" void DelegatePInvokeWrapper_MultiplayerStatusCallback_t994271114 ();
extern "C" void DelegatePInvokeWrapper_PlayerSelectUICallback_t4277124957 ();
extern "C" void DelegatePInvokeWrapper_TurnBasedMatchCallback_t3800683771 ();
extern "C" void DelegatePInvokeWrapper_TurnBasedMatchesCallback_t1856996905 ();
extern "C" void DelegatePInvokeWrapper_ShowUICallbackInternal_t1108446455 ();
extern "C" void DelegatePInvokeWrapper_AuthFinishedCallback_t563494438 ();
extern "C" void DelegatePInvokeWrapper_AuthStartedCallback_t2497315511 ();
extern "C" void DelegatePInvokeWrapper_OutStringMethod_t1062451542 ();
extern "C" void DelegatePInvokeWrapper_ReportProgress_t3967815895 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t524263911 ();
extern "C" void DelegatePInvokeWrapper_ApplyTween_t882368618 ();
extern "C" void DelegatePInvokeWrapper_EasingFunction_t1323017328 ();
extern "C" void DelegatePInvokeWrapper_VideoEnd_t3177907679 ();
extern "C" void DelegatePInvokeWrapper_VideoError_t247668236 ();
extern "C" void DelegatePInvokeWrapper_VideoFirstFrameReady_t2520860170 ();
extern "C" void DelegatePInvokeWrapper_VideoReady_t259270055 ();
extern "C" void DelegatePInvokeWrapper_VideoResize_t3742945584 ();
extern "C" void DelegatePInvokeWrapper_OnAllResultFail_t306118534 ();
extern "C" void DelegatePInvokeWrapper_OnAllResultLoadFail_t435151244 ();
extern "C" void DelegatePInvokeWrapper_OnFail_t2578455988 ();
extern "C" void DelegatePInvokeWrapper_OnNoInfo_t4236056357 ();
extern "C" void DelegatePInvokeWrapper_OnSave_t2578843667 ();
extern "C" void DelegatePInvokeWrapper_OnSuccess_t2806737325 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[104] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t691583313,
	DelegatePInvokeWrapper_Swapper_t4166107989,
	DelegatePInvokeWrapper_ReadDelegate_t1428404869,
	DelegatePInvokeWrapper_WriteDelegate_t2583486074,
	DelegatePInvokeWrapper_CrossContextDelegate_t651537830,
	DelegatePInvokeWrapper_CallbackHandler_t1474775431,
	DelegatePInvokeWrapper_ThreadStart_t124146534,
	DelegatePInvokeWrapper_ReadMethod_t1873379884,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t2055733333,
	DelegatePInvokeWrapper_WriteMethod_t3250749483,
	DelegatePInvokeWrapper_ReadDelegate_t3891738222,
	DelegatePInvokeWrapper_WriteDelegate_t1637408689,
	DelegatePInvokeWrapper_SocketAsyncCall_t742231849,
	DelegatePInvokeWrapper_CostDelegate_t1292950321,
	DelegatePInvokeWrapper_Action_t3771233898,
	DelegatePInvokeWrapper_CharGetter_t4120438118,
	DelegatePInvokeWrapper_LogCallback_t2984951347,
	DelegatePInvokeWrapper_PCMReaderCallback_t83861602,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t4244274966,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1377657005,
	DelegatePInvokeWrapper_WillRenderCanvases_t4247149838,
	DelegatePInvokeWrapper_StateChanged_t2578300556,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t581305515,
	DelegatePInvokeWrapper_UnityAction_t594794173,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t4168056797,
	DelegatePInvokeWrapper_WindowFunction_t2749288659,
	DelegatePInvokeWrapper_SkinChangedDelegate_t4002872878,
	DelegatePInvokeWrapper_BannerFailedToLoadDelegate_t2244784594,
	DelegatePInvokeWrapper_BannerWasClickedDelegate_t3510840818,
	DelegatePInvokeWrapper_BannerWasLoadedDelegate_t3604098244,
	DelegatePInvokeWrapper_InterstitialWasLoadedDelegate_t3068494210,
	DelegatePInvokeWrapper_InterstitialWasViewedDelegate_t507319425,
	DelegatePInvokeWrapper_OnValidateInput_t3952708057,
	DelegatePInvokeWrapper_FetchAllCallback_t3534128988,
	DelegatePInvokeWrapper_FetchCallback_t1950512559,
	DelegatePInvokeWrapper_ShowAllUICallback_t766686189,
	DelegatePInvokeWrapper_OnAuthActionFinishedCallback_t1172336457,
	DelegatePInvokeWrapper_OnAuthActionStartedCallback_t2378408244,
	DelegatePInvokeWrapper_OnLogCallback_t508427221,
	DelegatePInvokeWrapper_OnMultiplayerInvitationEventCallback_t3993607552,
	DelegatePInvokeWrapper_OnQuestCompletedCallback_t2055592770,
	DelegatePInvokeWrapper_OnTurnBasedMatchEventCallback_t2064424764,
	DelegatePInvokeWrapper_FetchAllCallback_t213806343,
	DelegatePInvokeWrapper_FetchCallback_t1493526500,
	DelegatePInvokeWrapper_FetchServerAuthCodeCallback_t1082452761,
	DelegatePInvokeWrapper_FlushCallback_t3126222379,
	DelegatePInvokeWrapper_FetchAllCallback_t3766146474,
	DelegatePInvokeWrapper_FetchAllScoreSummariesCallback_t1010253660,
	DelegatePInvokeWrapper_FetchCallback_t2463332897,
	DelegatePInvokeWrapper_FetchScorePageCallback_t1423453514,
	DelegatePInvokeWrapper_FetchScoreSummaryCallback_t1723193589,
	DelegatePInvokeWrapper_ShowAllUICallback_t3664260959,
	DelegatePInvokeWrapper_ShowUICallback_t1832555764,
	DelegatePInvokeWrapper_FetchCallback_t1233224091,
	DelegatePInvokeWrapper_FetchListCallback_t2067692889,
	DelegatePInvokeWrapper_FetchSelfCallback_t2479296007,
	DelegatePInvokeWrapper_AcceptCallback_t1408209808,
	DelegatePInvokeWrapper_ClaimMilestoneCallback_t1411568668,
	DelegatePInvokeWrapper_FetchCallback_t1576861340,
	DelegatePInvokeWrapper_FetchListCallback_t3150025178,
	DelegatePInvokeWrapper_QuestUICallback_t3097297816,
	DelegatePInvokeWrapper_OnDataReceivedCallback_t176853902,
	DelegatePInvokeWrapper_OnP2PConnectedCallback_t507387838,
	DelegatePInvokeWrapper_OnP2PDisconnectedCallback_t401589328,
	DelegatePInvokeWrapper_OnParticipantStatusChangedCallback_t2709967058,
	DelegatePInvokeWrapper_OnRoomConnectedSetChangedCallback_t3651478055,
	DelegatePInvokeWrapper_OnRoomStatusChangedCallback_t1130665710,
	DelegatePInvokeWrapper_FetchInvitationsCallback_t1518378837,
	DelegatePInvokeWrapper_LeaveRoomCallback_t3947593351,
	DelegatePInvokeWrapper_PlayerSelectUICallback_t2633669638,
	DelegatePInvokeWrapper_RealTimeRoomCallback_t3285480827,
	DelegatePInvokeWrapper_RoomInboxUICallback_t3659854740,
	DelegatePInvokeWrapper_SendReliableMessageCallback_t2145488650,
	DelegatePInvokeWrapper_WaitingRoomUICallback_t206291377,
	DelegatePInvokeWrapper_CommitCallback_t1623255043,
	DelegatePInvokeWrapper_FetchAllCallback_t677738899,
	DelegatePInvokeWrapper_OpenCallback_t1210339286,
	DelegatePInvokeWrapper_ReadCallback_t1569945378,
	DelegatePInvokeWrapper_SnapshotSelectUICallback_t2531066752,
	DelegatePInvokeWrapper_FetchForPlayerCallback_t1995639285,
	DelegatePInvokeWrapper_MatchInboxUICallback_t3070391457,
	DelegatePInvokeWrapper_MultiplayerStatusCallback_t994271114,
	DelegatePInvokeWrapper_PlayerSelectUICallback_t4277124957,
	DelegatePInvokeWrapper_TurnBasedMatchCallback_t3800683771,
	DelegatePInvokeWrapper_TurnBasedMatchesCallback_t1856996905,
	DelegatePInvokeWrapper_ShowUICallbackInternal_t1108446455,
	DelegatePInvokeWrapper_AuthFinishedCallback_t563494438,
	DelegatePInvokeWrapper_AuthStartedCallback_t2497315511,
	DelegatePInvokeWrapper_OutStringMethod_t1062451542,
	DelegatePInvokeWrapper_ReportProgress_t3967815895,
	DelegatePInvokeWrapper_EasingFunction_t524263911,
	DelegatePInvokeWrapper_ApplyTween_t882368618,
	DelegatePInvokeWrapper_EasingFunction_t1323017328,
	DelegatePInvokeWrapper_VideoEnd_t3177907679,
	DelegatePInvokeWrapper_VideoError_t247668236,
	DelegatePInvokeWrapper_VideoFirstFrameReady_t2520860170,
	DelegatePInvokeWrapper_VideoReady_t259270055,
	DelegatePInvokeWrapper_VideoResize_t3742945584,
	DelegatePInvokeWrapper_OnAllResultFail_t306118534,
	DelegatePInvokeWrapper_OnAllResultLoadFail_t435151244,
	DelegatePInvokeWrapper_OnFail_t2578455988,
	DelegatePInvokeWrapper_OnNoInfo_t4236056357,
	DelegatePInvokeWrapper_OnSave_t2578843667,
	DelegatePInvokeWrapper_OnSuccess_t2806737325,
};
