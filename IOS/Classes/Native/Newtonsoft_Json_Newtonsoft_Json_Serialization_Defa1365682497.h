﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// System.Func`1<System.Object>
struct Func_1_t1001010649;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1809280638;
// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t1365682496;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1
struct  U3CU3Ec__DisplayClass34_1_t1365682497  : public Il2CppObject
{
public:
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::setExtensionDataDictionary
	Action_2_t4293064463 * ___setExtensionDataDictionary_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::createExtensionDataDictionary
	Func_1_t1001010649 * ___createExtensionDataDictionary_1;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::setExtensionDataDictionaryValue
	MethodCall_2_t1809280638 * ___setExtensionDataDictionaryValue_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0 Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass34_0_t1365682496 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_setExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_t1365682497, ___setExtensionDataDictionary_0)); }
	inline Action_2_t4293064463 * get_setExtensionDataDictionary_0() const { return ___setExtensionDataDictionary_0; }
	inline Action_2_t4293064463 ** get_address_of_setExtensionDataDictionary_0() { return &___setExtensionDataDictionary_0; }
	inline void set_setExtensionDataDictionary_0(Action_2_t4293064463 * value)
	{
		___setExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&___setExtensionDataDictionary_0, value);
	}

	inline static int32_t get_offset_of_createExtensionDataDictionary_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_t1365682497, ___createExtensionDataDictionary_1)); }
	inline Func_1_t1001010649 * get_createExtensionDataDictionary_1() const { return ___createExtensionDataDictionary_1; }
	inline Func_1_t1001010649 ** get_address_of_createExtensionDataDictionary_1() { return &___createExtensionDataDictionary_1; }
	inline void set_createExtensionDataDictionary_1(Func_1_t1001010649 * value)
	{
		___createExtensionDataDictionary_1 = value;
		Il2CppCodeGenWriteBarrier(&___createExtensionDataDictionary_1, value);
	}

	inline static int32_t get_offset_of_setExtensionDataDictionaryValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_t1365682497, ___setExtensionDataDictionaryValue_2)); }
	inline MethodCall_2_t1809280638 * get_setExtensionDataDictionaryValue_2() const { return ___setExtensionDataDictionaryValue_2; }
	inline MethodCall_2_t1809280638 ** get_address_of_setExtensionDataDictionaryValue_2() { return &___setExtensionDataDictionaryValue_2; }
	inline void set_setExtensionDataDictionaryValue_2(MethodCall_2_t1809280638 * value)
	{
		___setExtensionDataDictionaryValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___setExtensionDataDictionaryValue_2, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_1_t1365682497, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass34_0_t1365682496 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass34_0_t1365682496 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass34_0_t1365682496 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E8__locals1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
