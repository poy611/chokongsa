﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
extern "C"  bool Misc_BuffersAreIdentical_m5238390 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___a0, ByteU5BU5D_t4260760469* ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Misc_GetSubsetBytes_m1859181228 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
