﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>
struct ReadOnlyCollection_1_t1127056649;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonPosition>
struct IList_1_t2264626316;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonPosition[]
struct JsonPositionU5BU5D_t3412818772;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.JsonPosition>
struct IEnumerator_1_t1481844162;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m4016921322_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m4016921322(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m4016921322_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1744177108_gshared (ReadOnlyCollection_1_t1127056649 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1744177108(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1744177108_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m241606518_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m241606518(__this, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m241606518_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2621034939_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, JsonPosition_t3864946409  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2621034939(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2621034939_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3373541475_gshared (ReadOnlyCollection_1_t1127056649 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3373541475(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3373541475_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m494887809_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m494887809(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m494887809_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  JsonPosition_t3864946409  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1211190503_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1211190503(__this, ___index0, method) ((  JsonPosition_t3864946409  (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1211190503_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3773337554_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, JsonPosition_t3864946409  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3773337554(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3773337554_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1618669516_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1618669516(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1618669516_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1565430937_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1565430937(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1565430937_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3679602536_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3679602536(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3679602536_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m587724117_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m587724117(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m587724117_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3937159079_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3937159079(__this, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3937159079_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m472592779_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m472592779(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m472592779_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2012444973_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2012444973(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2012444973_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1457665056_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1457665056(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1457665056_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2479833352_gshared (ReadOnlyCollection_1_t1127056649 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2479833352(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2479833352_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1150778288_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1150778288(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1150778288_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m103658993_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m103658993(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m103658993_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3736082915_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3736082915(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3736082915_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2695734586_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2695734586(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2695734586_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2203535999_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2203535999(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2203535999_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m848633514_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m848633514(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m848633514_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1270732791_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1270732791(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1270732791_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3744863400_gshared (ReadOnlyCollection_1_t1127056649 * __this, JsonPosition_t3864946409  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3744863400(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1127056649 *, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3744863400_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m700880644_gshared (ReadOnlyCollection_1_t1127056649 * __this, JsonPositionU5BU5D_t3412818772* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m700880644(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1127056649 *, JsonPositionU5BU5D_t3412818772*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m700880644_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1723897215_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1723897215(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1723897215_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1001872080_gshared (ReadOnlyCollection_1_t1127056649 * __this, JsonPosition_t3864946409  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1001872080(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1127056649 *, JsonPosition_t3864946409 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1001872080_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m810570923_gshared (ReadOnlyCollection_1_t1127056649 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m810570923(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1127056649 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m810570923_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.JsonPosition>::get_Item(System.Int32)
extern "C"  JsonPosition_t3864946409  ReadOnlyCollection_1_get_Item_m2099238439_gshared (ReadOnlyCollection_1_t1127056649 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2099238439(__this, ___index0, method) ((  JsonPosition_t3864946409  (*) (ReadOnlyCollection_1_t1127056649 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2099238439_gshared)(__this, ___index0, method)
