﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen2331760108MethodDeclarations.h"

// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m441300794(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t2045751174 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m593249855_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m1907677488(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t2045751174 *, NativeRealTimeRoom_t3104490121 *, MultiplayerParticipant_t3337232325 *, ByteU5BU5D_t4260760469*, bool, const MethodInfo*))Action_4_Invoke_m3761155659_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m2577823689(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t2045751174 *, NativeRealTimeRoom_t3104490121 *, MultiplayerParticipant_t3337232325 *, ByteU5BU5D_t4260760469*, bool, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m1301136724_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m486789946(__this, ___result0, method) ((  void (*) (Action_4_t2045751174 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m487763919_gshared)(__this, ___result0, method)
