﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4273503902(__this, ___l0, method) ((  void (*) (Enumerator_t2352853523 *, List_1_t2333180753 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m523070196(__this, method) ((  void (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m170294186(__this, method) ((  Il2CppObject * (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::Dispose()
#define Enumerator_Dispose_m2188383235(__this, method) ((  void (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::VerifyState()
#define Enumerator_VerifyState_m1302600124(__this, method) ((  void (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::MoveNext()
#define Enumerator_MoveNext_m537705743(__this, method) ((  bool (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.LayoutOption>::get_Current()
#define Enumerator_get_Current_m711648749(__this, method) ((  LayoutOption_t964995201 * (*) (Enumerator_t2352853523 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
