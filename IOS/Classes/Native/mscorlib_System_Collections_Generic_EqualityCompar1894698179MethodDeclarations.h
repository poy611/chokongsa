﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.ReadType>
struct DefaultComparer_t1894698179;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.ReadType>::.ctor()
extern "C"  void DefaultComparer__ctor_m304348465_gshared (DefaultComparer_t1894698179 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m304348465(__this, method) ((  void (*) (DefaultComparer_t1894698179 *, const MethodInfo*))DefaultComparer__ctor_m304348465_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.ReadType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4118072482_gshared (DefaultComparer_t1894698179 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4118072482(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1894698179 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m4118072482_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.ReadType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m419595974_gshared (DefaultComparer_t1894698179 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m419595974(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1894698179 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m419595974_gshared)(__this, ___x0, ___y1, method)
