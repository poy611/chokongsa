﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<UserExperience>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3715916902(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3572965032 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2849855637_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UserExperience>::Invoke(T)
#define Predicate_1_Invoke_m364045660(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3572965032 *, UserExperience_t3961908149 *, const MethodInfo*))Predicate_1_Invoke_m428283958_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UserExperience>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m758851051(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3572965032 *, UserExperience_t3961908149 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UserExperience>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m73502712(__this, ___result0, method) ((  bool (*) (Predicate_1_t3572965032 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
