﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationSpeed
struct SetAnimationSpeed_t2927029363;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::.ctor()
extern "C"  void SetAnimationSpeed__ctor_m2491154979 (SetAnimationSpeed_t2927029363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::Reset()
extern "C"  void SetAnimationSpeed_Reset_m137587920 (SetAnimationSpeed_t2927029363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::OnEnter()
extern "C"  void SetAnimationSpeed_OnEnter_m1094879546 (SetAnimationSpeed_t2927029363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::OnUpdate()
extern "C"  void SetAnimationSpeed_OnUpdate_m3010054217 (SetAnimationSpeed_t2927029363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::DoSetAnimationSpeed(UnityEngine.GameObject)
extern "C"  void SetAnimationSpeed_DoSetAnimationSpeed_m1795578067 (SetAnimationSpeed_t2927029363 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
