﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51
struct U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51::.ctor()
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey51__ctor_m4003055682 (U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey51::<>m__37()
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey51_U3CU3Em__37_m953919471 (U3CGetPlayerStatsU3Ec__AnonStorey51_t4053278025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
