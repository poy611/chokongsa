﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3690162753(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3046416558 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3368609491_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Key()
#define KeyValuePair_2_get_Key_m1149570055(__this, method) ((  String_t* (*) (KeyValuePair_2_t3046416558 *, const MethodInfo*))KeyValuePair_2_get_Key_m3321873205_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m587254600(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3046416558 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1825586038_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Value()
#define KeyValuePair_2_get_Value_m2143104363(__this, method) ((  int32_t (*) (KeyValuePair_2_t3046416558 *, const MethodInfo*))KeyValuePair_2_get_Value_m2372325657_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3354737480(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3046416558 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m3205817462_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToString()
#define KeyValuePair_2_ToString_m2457185408(__this, method) ((  String_t* (*) (KeyValuePair_2_t3046416558 *, const MethodInfo*))KeyValuePair_2_ToString_m1079106322_gshared)(__this, method)
