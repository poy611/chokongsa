﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TransformPoint
struct TransformPoint_t1395025004;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TransformPoint::.ctor()
extern "C"  void TransformPoint__ctor_m2388188538 (TransformPoint_t1395025004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::Reset()
extern "C"  void TransformPoint_Reset_m34621479 (TransformPoint_t1395025004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::OnEnter()
extern "C"  void TransformPoint_OnEnter_m928377553 (TransformPoint_t1395025004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::OnUpdate()
extern "C"  void TransformPoint_OnUpdate_m2143459730 (TransformPoint_t1395025004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::DoTransformPoint()
extern "C"  void TransformPoint_DoTransformPoint_m1443232313 (TransformPoint_t1395025004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
