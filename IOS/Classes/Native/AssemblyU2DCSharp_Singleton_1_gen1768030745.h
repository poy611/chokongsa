﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// NetworkMng
struct NetworkMng_t1515215352;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<NetworkMng>
struct  Singleton_1_t1768030745  : public MonoBehaviour_t667441552
{
public:

public:
};

struct Singleton_1_t1768030745_StaticFields
{
public:
	// T Singleton`1::instance
	NetworkMng_t1515215352 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t1768030745_StaticFields, ___instance_2)); }
	inline NetworkMng_t1515215352 * get_instance_2() const { return ___instance_2; }
	inline NetworkMng_t1515215352 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(NetworkMng_t1515215352 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
