﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.ReflectionAttributeProvider
struct ReflectionAttributeProvider_t1962998760;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.ReflectionAttributeProvider::.ctor(System.Object)
extern "C"  void ReflectionAttributeProvider__ctor_m1447716931 (ReflectionAttributeProvider_t1962998760 * __this, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
