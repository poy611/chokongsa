﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen875830559.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1666096596_gshared (InternalEnumerator_1_t875830559 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1666096596(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t875830559 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1666096596_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076(__this, method) ((  void (*) (InternalEnumerator_1_t875830559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t875830559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m166437035_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m166437035(__this, method) ((  void (*) (InternalEnumerator_1_t875830559 *, const MethodInfo*))InternalEnumerator_1_Dispose_m166437035_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3463093432_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3463093432(__this, method) ((  bool (*) (InternalEnumerator_1_t875830559 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3463093432_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::get_Current()
extern "C"  KeyValuePair_2_t2093487883  InternalEnumerator_1_get_Current_m482313243_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m482313243(__this, method) ((  KeyValuePair_2_t2093487883  (*) (InternalEnumerator_1_t875830559 *, const MethodInfo*))InternalEnumerator_1_get_Current_m482313243_gshared)(__this, method)
