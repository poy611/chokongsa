﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugBool
struct DebugBool_t4206749483;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugBool::.ctor()
extern "C"  void DebugBool__ctor_m1150877291 (DebugBool_t4206749483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugBool::Reset()
extern "C"  void DebugBool_Reset_m3092277528 (DebugBool_t4206749483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugBool::OnEnter()
extern "C"  void DebugBool_OnEnter_m1578210178 (DebugBool_t4206749483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
