﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey75
struct U3CRoomSetupProgressU3Ec__AnonStorey75_t1368477812;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey75::.ctor()
extern "C"  void U3CRoomSetupProgressU3Ec__AnonStorey75__ctor_m2010538151 (U3CRoomSetupProgressU3Ec__AnonStorey75_t1368477812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey75::<>m__4F()
extern "C"  void U3CRoomSetupProgressU3Ec__AnonStorey75_U3CU3Em__4F_m1700030402 (U3CRoomSetupProgressU3Ec__AnonStorey75_t1368477812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
