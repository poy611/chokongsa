﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGravity2dScale
struct SetGravity2dScale_t647932218;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGravity2dScale::.ctor()
extern "C"  void SetGravity2dScale__ctor_m744404284 (SetGravity2dScale_t647932218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2dScale::Reset()
extern "C"  void SetGravity2dScale_Reset_m2685804521 (SetGravity2dScale_t647932218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2dScale::OnEnter()
extern "C"  void SetGravity2dScale_OnEnter_m1799674387 (SetGravity2dScale_t647932218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2dScale::DoSetGravityScale()
extern "C"  void SetGravity2dScale_DoSetGravityScale_m3018259757 (SetGravity2dScale_t647932218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
