﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomBool
struct RandomBool_t2893514677;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomBool::.ctor()
extern "C"  void RandomBool__ctor_m1578707025 (RandomBool_t2893514677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomBool::Reset()
extern "C"  void RandomBool_Reset_m3520107262 (RandomBool_t2893514677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomBool::OnEnter()
extern "C"  void RandomBool_OnEnter_m405724136 (RandomBool_t2893514677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
