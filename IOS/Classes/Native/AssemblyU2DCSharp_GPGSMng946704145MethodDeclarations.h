﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPGSMng
struct GPGSMng_t946704145;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GPGSMng::.ctor()
extern "C"  void GPGSMng__ctor_m3815572346 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPGSMng::get_bLogin()
extern "C"  bool GPGSMng_get_bLogin_m2039479974 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSMng::set_bLogin(System.Boolean)
extern "C"  void GPGSMng_set_bLogin_m302910021 (GPGSMng_t946704145 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSMng::InitializeGPGS()
extern "C"  void GPGSMng_InitializeGPGS_m137004335 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSMng::LoginGPGS()
extern "C"  void GPGSMng_LoginGPGS_m3908873238 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPGSMng::CheckLoginState()
extern "C"  bool GPGSMng_CheckLoginState_m222383964 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSMng::LoginCallBackGPGS(System.Boolean)
extern "C"  void GPGSMng_LoginCallBackGPGS_m2686207762 (GPGSMng_t946704145 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSMng::LogoutGPGS()
extern "C"  void GPGSMng_LogoutGPGS_m3304224809 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GPGSMng::GetImageGPGS()
extern "C"  Texture2D_t3884108195 * GPGSMng_GetImageGPGS_m130434023 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPGSMng::GetNameGPGS()
extern "C"  String_t* GPGSMng_GetNameGPGS_m3486585297 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GPGSMng::GetIDGPGS()
extern "C"  String_t* GPGSMng_GetIDGPGS_m601054273 (GPGSMng_t946704145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
