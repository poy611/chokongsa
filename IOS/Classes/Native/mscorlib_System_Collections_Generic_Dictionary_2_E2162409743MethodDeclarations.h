﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3512030569MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1883623117(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2162409743 *, Dictionary_2_t845086351 *, const MethodInfo*))Enumerator__ctor_m3626454267_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1016562548(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4273680134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3819535304(__this, method) ((  void (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1183956186_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1156199505(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2033855971_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3314243920(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m374378338_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3466077154(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m48766324_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::MoveNext()
#define Enumerator_MoveNext_m3542269108(__this, method) ((  bool (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_MoveNext_m311749830_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_Current()
#define Enumerator_get_Current_m2753636604(__this, method) ((  KeyValuePair_2_t743867057  (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_get_Current_m3980994474_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3832963457(__this, method) ((  String_t* (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_get_CurrentKey_m699710483_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2600610277(__this, method) ((  uint32_t (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_get_CurrentValue_m2316576759_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::Reset()
#define Enumerator_Reset_m2529330591(__this, method) ((  void (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_Reset_m3804405453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::VerifyState()
#define Enumerator_VerifyState_m2803688040(__this, method) ((  void (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_VerifyState_m4031045910_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4144583632(__this, method) ((  void (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_VerifyCurrent_m2519490302_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::Dispose()
#define Enumerator_Dispose_m2258637487(__this, method) ((  void (*) (Enumerator_t2162409743 *, const MethodInfo*))Enumerator_Dispose_m3539900509_gshared)(__this, method)
