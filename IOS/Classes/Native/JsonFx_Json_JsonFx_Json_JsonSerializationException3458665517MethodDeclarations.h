﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonSerializationException
struct JsonSerializationException_t3458665517;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JsonFx.Json.JsonSerializationException::.ctor(System.String)
extern "C"  void JsonSerializationException__ctor_m2046370034 (JsonSerializationException_t3458665517 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
