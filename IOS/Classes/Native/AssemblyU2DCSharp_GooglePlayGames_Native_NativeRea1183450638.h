﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t3078724631;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6A
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B
struct  U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B::helper
	RealTimeEventListenerHelper_t3078724631 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B::<>f__ref$104
	U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 * ___U3CU3Ef__refU24104_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6A GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68/<CreateWithInvitationScreen>c__AnonStorey6B::<>f__ref$106
	U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637 * ___U3CU3Ef__refU24106_2;

public:
	inline static int32_t get_offset_of_helper_0() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638, ___helper_0)); }
	inline RealTimeEventListenerHelper_t3078724631 * get_helper_0() const { return ___helper_0; }
	inline RealTimeEventListenerHelper_t3078724631 ** get_address_of_helper_0() { return &___helper_0; }
	inline void set_helper_0(RealTimeEventListenerHelper_t3078724631 * value)
	{
		___helper_0 = value;
		Il2CppCodeGenWriteBarrier(&___helper_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24104_1() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638, ___U3CU3Ef__refU24104_1)); }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 * get_U3CU3Ef__refU24104_1() const { return ___U3CU3Ef__refU24104_1; }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 ** get_address_of_U3CU3Ef__refU24104_1() { return &___U3CU3Ef__refU24104_1; }
	inline void set_U3CU3Ef__refU24104_1(U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 * value)
	{
		___U3CU3Ef__refU24104_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24104_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24106_2() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey6B_t1183450638, ___U3CU3Ef__refU24106_2)); }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637 * get_U3CU3Ef__refU24106_2() const { return ___U3CU3Ef__refU24106_2; }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637 ** get_address_of_U3CU3Ef__refU24106_2() { return &___U3CU3Ef__refU24106_2; }
	inline void set_U3CU3Ef__refU24106_2(U3CCreateWithInvitationScreenU3Ec__AnonStorey6A_t1183450637 * value)
	{
		___U3CU3Ef__refU24106_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24106_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
