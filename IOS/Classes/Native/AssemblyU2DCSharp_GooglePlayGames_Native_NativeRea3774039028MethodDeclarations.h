﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey78
struct U3CPeersDisconnectedU3Ec__AnonStorey78_t3774039028;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey78::.ctor()
extern "C"  void U3CPeersDisconnectedU3Ec__AnonStorey78__ctor_m2158910759 (U3CPeersDisconnectedU3Ec__AnonStorey78_t3774039028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey78::<>m__53()
extern "C"  void U3CPeersDisconnectedU3Ec__AnonStorey78_U3CU3Em__53_m2552197454 (U3CPeersDisconnectedU3Ec__AnonStorey78_t3774039028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
