﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Lerp
struct Vector2Lerp_t121398260;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::.ctor()
extern "C"  void Vector2Lerp__ctor_m1613347266 (Vector2Lerp_t121398260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::Reset()
extern "C"  void Vector2Lerp_Reset_m3554747503 (Vector2Lerp_t121398260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::OnEnter()
extern "C"  void Vector2Lerp_OnEnter_m3630224665 (Vector2Lerp_t121398260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::OnUpdate()
extern "C"  void Vector2Lerp_OnUpdate_m1374282 (Vector2Lerp_t121398260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::DoVector2Lerp()
extern "C"  void Vector2Lerp_DoVector2Lerp_m2080524539 (Vector2Lerp_t121398260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
