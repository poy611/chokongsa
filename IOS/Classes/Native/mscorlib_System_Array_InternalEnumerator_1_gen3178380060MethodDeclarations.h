﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3178380060.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2858706882_gshared (InternalEnumerator_1_t3178380060 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2858706882(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3178380060 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2858706882_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462(__this, method) ((  void (*) (InternalEnumerator_1_t3178380060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3178380060 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1571445785_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1571445785(__this, method) ((  void (*) (InternalEnumerator_1_t3178380060 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1571445785_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3159979146_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3159979146(__this, method) ((  bool (*) (InternalEnumerator_1_t3178380060 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3159979146_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::get_Current()
extern "C"  KeyValuePair_2_t101070088  InternalEnumerator_1_get_Current_m3580605257_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3580605257(__this, method) ((  KeyValuePair_2_t101070088  (*) (InternalEnumerator_1_t3178380060 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3580605257_gshared)(__this, method)
