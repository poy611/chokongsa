﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1__ctor_m1536677294_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1__ctor_m1536677294(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1__ctor_m1536677294_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9D`1/<AsOnGameThreadCallback>c__AnonStorey9E`1<System.Boolean>::<>m__9F()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_U3CU3Em__9F_m1586429156_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_U3CU3Em__9F_m1586429156(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3939258807 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_U3CU3Em__9F_m1586429156_gshared)(__this, method)
