﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2975001167.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveVector3
struct  CurveVector3_t1860957577  : public CurveFsmAction_t2975001167
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::vectorVariable
	FsmVector3_t533912882 * ___vectorVariable_35;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::fromValue
	FsmVector3_t533912882 * ___fromValue_36;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::toValue
	FsmVector3_t533912882 * ___toValue_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveX
	FsmAnimationCurve_t2685995989 * ___curveX_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationX
	int32_t ___calculationX_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveY
	FsmAnimationCurve_t2685995989 * ___curveY_40;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationY
	int32_t ___calculationY_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveZ
	FsmAnimationCurve_t2685995989 * ___curveZ_42;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationZ
	int32_t ___calculationZ_43;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.CurveVector3::vct
	Vector3_t4282066566  ___vct_44;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveVector3::finishInNextStep
	bool ___finishInNextStep_45;

public:
	inline static int32_t get_offset_of_vectorVariable_35() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___vectorVariable_35)); }
	inline FsmVector3_t533912882 * get_vectorVariable_35() const { return ___vectorVariable_35; }
	inline FsmVector3_t533912882 ** get_address_of_vectorVariable_35() { return &___vectorVariable_35; }
	inline void set_vectorVariable_35(FsmVector3_t533912882 * value)
	{
		___vectorVariable_35 = value;
		Il2CppCodeGenWriteBarrier(&___vectorVariable_35, value);
	}

	inline static int32_t get_offset_of_fromValue_36() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___fromValue_36)); }
	inline FsmVector3_t533912882 * get_fromValue_36() const { return ___fromValue_36; }
	inline FsmVector3_t533912882 ** get_address_of_fromValue_36() { return &___fromValue_36; }
	inline void set_fromValue_36(FsmVector3_t533912882 * value)
	{
		___fromValue_36 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_36, value);
	}

	inline static int32_t get_offset_of_toValue_37() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___toValue_37)); }
	inline FsmVector3_t533912882 * get_toValue_37() const { return ___toValue_37; }
	inline FsmVector3_t533912882 ** get_address_of_toValue_37() { return &___toValue_37; }
	inline void set_toValue_37(FsmVector3_t533912882 * value)
	{
		___toValue_37 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_37, value);
	}

	inline static int32_t get_offset_of_curveX_38() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveX_38)); }
	inline FsmAnimationCurve_t2685995989 * get_curveX_38() const { return ___curveX_38; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveX_38() { return &___curveX_38; }
	inline void set_curveX_38(FsmAnimationCurve_t2685995989 * value)
	{
		___curveX_38 = value;
		Il2CppCodeGenWriteBarrier(&___curveX_38, value);
	}

	inline static int32_t get_offset_of_calculationX_39() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationX_39)); }
	inline int32_t get_calculationX_39() const { return ___calculationX_39; }
	inline int32_t* get_address_of_calculationX_39() { return &___calculationX_39; }
	inline void set_calculationX_39(int32_t value)
	{
		___calculationX_39 = value;
	}

	inline static int32_t get_offset_of_curveY_40() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveY_40)); }
	inline FsmAnimationCurve_t2685995989 * get_curveY_40() const { return ___curveY_40; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveY_40() { return &___curveY_40; }
	inline void set_curveY_40(FsmAnimationCurve_t2685995989 * value)
	{
		___curveY_40 = value;
		Il2CppCodeGenWriteBarrier(&___curveY_40, value);
	}

	inline static int32_t get_offset_of_calculationY_41() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationY_41)); }
	inline int32_t get_calculationY_41() const { return ___calculationY_41; }
	inline int32_t* get_address_of_calculationY_41() { return &___calculationY_41; }
	inline void set_calculationY_41(int32_t value)
	{
		___calculationY_41 = value;
	}

	inline static int32_t get_offset_of_curveZ_42() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___curveZ_42)); }
	inline FsmAnimationCurve_t2685995989 * get_curveZ_42() const { return ___curveZ_42; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_curveZ_42() { return &___curveZ_42; }
	inline void set_curveZ_42(FsmAnimationCurve_t2685995989 * value)
	{
		___curveZ_42 = value;
		Il2CppCodeGenWriteBarrier(&___curveZ_42, value);
	}

	inline static int32_t get_offset_of_calculationZ_43() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___calculationZ_43)); }
	inline int32_t get_calculationZ_43() const { return ___calculationZ_43; }
	inline int32_t* get_address_of_calculationZ_43() { return &___calculationZ_43; }
	inline void set_calculationZ_43(int32_t value)
	{
		___calculationZ_43 = value;
	}

	inline static int32_t get_offset_of_vct_44() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___vct_44)); }
	inline Vector3_t4282066566  get_vct_44() const { return ___vct_44; }
	inline Vector3_t4282066566 * get_address_of_vct_44() { return &___vct_44; }
	inline void set_vct_44(Vector3_t4282066566  value)
	{
		___vct_44 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_45() { return static_cast<int32_t>(offsetof(CurveVector3_t1860957577, ___finishInNextStep_45)); }
	inline bool get_finishInNextStep_45() const { return ___finishInNextStep_45; }
	inline bool* get_address_of_finishInNextStep_45() { return &___finishInNextStep_45; }
	inline void set_finishInNextStep_45(bool value)
	{
		___finishInNextStep_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
