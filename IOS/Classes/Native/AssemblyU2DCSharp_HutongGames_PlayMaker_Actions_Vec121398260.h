﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Lerp
struct  Vector2Lerp_t121398260  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::fromVector
	FsmVector2_t533912881 * ___fromVector_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::toVector
	FsmVector2_t533912881 * ___toVector_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2Lerp::amount
	FsmFloat_t2134102846 * ___amount_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::storeResult
	FsmVector2_t533912881 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Lerp::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_fromVector_11() { return static_cast<int32_t>(offsetof(Vector2Lerp_t121398260, ___fromVector_11)); }
	inline FsmVector2_t533912881 * get_fromVector_11() const { return ___fromVector_11; }
	inline FsmVector2_t533912881 ** get_address_of_fromVector_11() { return &___fromVector_11; }
	inline void set_fromVector_11(FsmVector2_t533912881 * value)
	{
		___fromVector_11 = value;
		Il2CppCodeGenWriteBarrier(&___fromVector_11, value);
	}

	inline static int32_t get_offset_of_toVector_12() { return static_cast<int32_t>(offsetof(Vector2Lerp_t121398260, ___toVector_12)); }
	inline FsmVector2_t533912881 * get_toVector_12() const { return ___toVector_12; }
	inline FsmVector2_t533912881 ** get_address_of_toVector_12() { return &___toVector_12; }
	inline void set_toVector_12(FsmVector2_t533912881 * value)
	{
		___toVector_12 = value;
		Il2CppCodeGenWriteBarrier(&___toVector_12, value);
	}

	inline static int32_t get_offset_of_amount_13() { return static_cast<int32_t>(offsetof(Vector2Lerp_t121398260, ___amount_13)); }
	inline FsmFloat_t2134102846 * get_amount_13() const { return ___amount_13; }
	inline FsmFloat_t2134102846 ** get_address_of_amount_13() { return &___amount_13; }
	inline void set_amount_13(FsmFloat_t2134102846 * value)
	{
		___amount_13 = value;
		Il2CppCodeGenWriteBarrier(&___amount_13, value);
	}

	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(Vector2Lerp_t121398260, ___storeResult_14)); }
	inline FsmVector2_t533912881 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmVector2_t533912881 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmVector2_t533912881 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector2Lerp_t121398260, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
