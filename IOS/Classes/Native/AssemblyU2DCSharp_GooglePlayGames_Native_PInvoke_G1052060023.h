﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_B2237584300.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse
struct  FetchServerAuthCodeResponse_t1052060023  : public BaseReferenceHolder_t2237584300
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
