﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey5A
struct U3CShowAchievementsUIU3Ec__AnonStorey5A_t1277145270;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"

// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey5A::.ctor()
extern "C"  void U3CShowAchievementsUIU3Ec__AnonStorey5A__ctor_m1882893173 (U3CShowAchievementsUIU3Ec__AnonStorey5A_t1277145270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey5A::<>m__30(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C"  void U3CShowAchievementsUIU3Ec__AnonStorey5A_U3CU3Em__30_m1050745268 (U3CShowAchievementsUIU3Ec__AnonStorey5A_t1277145270 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
