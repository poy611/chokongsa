﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector3Value
struct  SetVector3Value_t2514376337  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3Value::vector3Variable
	FsmVector3_t533912882 * ___vector3Variable_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3Value::vector3Value
	FsmVector3_t533912882 * ___vector3Value_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector3Value::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_vector3Variable_11() { return static_cast<int32_t>(offsetof(SetVector3Value_t2514376337, ___vector3Variable_11)); }
	inline FsmVector3_t533912882 * get_vector3Variable_11() const { return ___vector3Variable_11; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Variable_11() { return &___vector3Variable_11; }
	inline void set_vector3Variable_11(FsmVector3_t533912882 * value)
	{
		___vector3Variable_11 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_11, value);
	}

	inline static int32_t get_offset_of_vector3Value_12() { return static_cast<int32_t>(offsetof(SetVector3Value_t2514376337, ___vector3Value_12)); }
	inline FsmVector3_t533912882 * get_vector3Value_12() const { return ___vector3Value_12; }
	inline FsmVector3_t533912882 ** get_address_of_vector3Value_12() { return &___vector3Value_12; }
	inline void set_vector3Value_12(FsmVector3_t533912882 * value)
	{
		___vector3Value_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Value_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetVector3Value_t2514376337, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
