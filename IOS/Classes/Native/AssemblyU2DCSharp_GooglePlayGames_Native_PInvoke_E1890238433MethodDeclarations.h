﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct FetchAllResponse_t1890238433;
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.NativeEvent>
struct List_1_t3853433465;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// GooglePlayGames.Native.PInvoke.NativeEvent
struct NativeEvent_t2485247913;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C"  void FetchAllResponse__ctor_m815154874 (FetchAllResponse_t1890238433 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::ResponseStatus()
extern "C"  int32_t FetchAllResponse_ResponseStatus_m4007712294 (FetchAllResponse_t1890238433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.NativeEvent> GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::Data()
extern "C"  List_1_t3853433465 * FetchAllResponse_Data_m1425741927 (FetchAllResponse_t1890238433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::RequestSucceeded()
extern "C"  bool FetchAllResponse_RequestSucceeded_m2566508400 (FetchAllResponse_t1890238433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchAllResponse_CallDispose_m398247478 (FetchAllResponse_t1890238433 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C"  FetchAllResponse_t1890238433 * FetchAllResponse_FromPointer_m2184471685 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__A1(System.IntPtr[],System.UIntPtr)
extern "C"  UIntPtr_t  FetchAllResponse_U3CDataU3Em__A1_m1873077460 (FetchAllResponse_t1890238433 * __this, IntPtrU5BU5D_t3228729122* ___out_arg0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__A2(System.IntPtr)
extern "C"  NativeEvent_t2485247913 * FetchAllResponse_U3CDataU3Em__A2_m553714771 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
