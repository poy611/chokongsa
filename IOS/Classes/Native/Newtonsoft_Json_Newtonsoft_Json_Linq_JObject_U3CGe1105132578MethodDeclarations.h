﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54
struct U3CGetEnumeratorU3Ed__54_t1105132578;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24131445027.h"

// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::.ctor(System.Int32)
extern "C"  void U3CGetEnumeratorU3Ed__54__ctor_m2818155567 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::System.IDisposable.Dispose()
extern "C"  void U3CGetEnumeratorU3Ed__54_System_IDisposable_Dispose_m2702817921 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ed__54_MoveNext_m2120692576 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::<>m__Finally1()
extern "C"  void U3CGetEnumeratorU3Ed__54_U3CU3Em__Finally1_m1953806933 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.get_Current()
extern "C"  KeyValuePair_2_t4131445027  U3CGetEnumeratorU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_m422356387 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::System.Collections.IEnumerator.Reset()
extern "C"  void U3CGetEnumeratorU3Ed__54_System_Collections_IEnumerator_Reset_m3226774876 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__54::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ed__54_System_Collections_IEnumerator_get_Current_m430921288 (U3CGetEnumeratorU3Ed__54_t1105132578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
