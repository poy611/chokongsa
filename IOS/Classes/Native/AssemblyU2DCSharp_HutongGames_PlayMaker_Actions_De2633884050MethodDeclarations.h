﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugGameObject
struct DebugGameObject_t2633884050;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::.ctor()
extern "C"  void DebugGameObject__ctor_m2391276004 (DebugGameObject_t2633884050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::Reset()
extern "C"  void DebugGameObject_Reset_m37708945 (DebugGameObject_t2633884050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugGameObject::OnEnter()
extern "C"  void DebugGameObject_OnEnter_m3895432379 (DebugGameObject_t2633884050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
