﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12
struct U3CU3Ec__DisplayClass12_t2936911561;
// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;

#include "codegen/il2cpp-codegen.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"

// System.Void YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12::.ctor()
extern "C"  void U3CU3Ec__DisplayClass12__ctor_m1949650966 (U3CU3Ec__DisplayClass12_t2936911561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12::<GetSingleVideoInfo>b__11(YoutubeExtractor.VideoInfo)
extern "C"  bool U3CU3Ec__DisplayClass12_U3CGetSingleVideoInfoU3Eb__11_m780004071 (U3CU3Ec__DisplayClass12_t2936911561 * __this, VideoInfo_t2099471191 * ___videoInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
