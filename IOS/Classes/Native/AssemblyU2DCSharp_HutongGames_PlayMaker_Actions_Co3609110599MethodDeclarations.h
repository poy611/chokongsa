﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerSettings
struct ControllerSettings_t3609110599;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::.ctor()
extern "C"  void ControllerSettings__ctor_m4086100991 (ControllerSettings_t3609110599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::Reset()
extern "C"  void ControllerSettings_Reset_m1732533932 (ControllerSettings_t3609110599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::OnEnter()
extern "C"  void ControllerSettings_OnEnter_m534672406 (ControllerSettings_t3609110599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::OnUpdate()
extern "C"  void ControllerSettings_OnUpdate_m2823502061 (ControllerSettings_t3609110599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::DoControllerSettings()
extern "C"  void ControllerSettings_DoControllerSettings_m3636965039 (ControllerSettings_t3609110599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
