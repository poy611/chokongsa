﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RecipeList
struct RecipeList_t1641733996;
// System.String[]
struct StringU5BU5D_t4054002952;
// Ingredient
struct Ingredient_t1787055601;
// MenuList
struct MenuList_t3755595453;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecipeList
struct  RecipeList_t1641733996  : public MonoBehaviour_t667441552
{
public:
	// System.String[] RecipeList::coffeeImgList
	StringU5BU5D_t4054002952* ___coffeeImgList_3;
	// Ingredient RecipeList::ingredient
	Ingredient_t1787055601 * ___ingredient_4;
	// MenuList RecipeList::recipeList
	MenuList_t3755595453 * ___recipeList_5;
	// System.Int32[][] RecipeList::lobbyRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___lobbyRecipe_6;
	// System.Int32[][] RecipeList::coffeeBeanRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___coffeeBeanRecipe_7;
	// System.Int32[][] RecipeList::extractionRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___extractionRecipe_8;
	// System.Int32[][] RecipeList::steamMilkRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___steamMilkRecipe_9;
	// System.Int32[][] RecipeList::iceMixerRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___iceMixerRecipe_10;
	// System.Int32[][] RecipeList::variationRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___variationRecipe_11;
	// System.Int32[][] RecipeList::randomValueByRecipe
	Int32U5BU5DU5BU5D_t1820556512* ___randomValueByRecipe_12;

public:
	inline static int32_t get_offset_of_coffeeImgList_3() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___coffeeImgList_3)); }
	inline StringU5BU5D_t4054002952* get_coffeeImgList_3() const { return ___coffeeImgList_3; }
	inline StringU5BU5D_t4054002952** get_address_of_coffeeImgList_3() { return &___coffeeImgList_3; }
	inline void set_coffeeImgList_3(StringU5BU5D_t4054002952* value)
	{
		___coffeeImgList_3 = value;
		Il2CppCodeGenWriteBarrier(&___coffeeImgList_3, value);
	}

	inline static int32_t get_offset_of_ingredient_4() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___ingredient_4)); }
	inline Ingredient_t1787055601 * get_ingredient_4() const { return ___ingredient_4; }
	inline Ingredient_t1787055601 ** get_address_of_ingredient_4() { return &___ingredient_4; }
	inline void set_ingredient_4(Ingredient_t1787055601 * value)
	{
		___ingredient_4 = value;
		Il2CppCodeGenWriteBarrier(&___ingredient_4, value);
	}

	inline static int32_t get_offset_of_recipeList_5() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___recipeList_5)); }
	inline MenuList_t3755595453 * get_recipeList_5() const { return ___recipeList_5; }
	inline MenuList_t3755595453 ** get_address_of_recipeList_5() { return &___recipeList_5; }
	inline void set_recipeList_5(MenuList_t3755595453 * value)
	{
		___recipeList_5 = value;
		Il2CppCodeGenWriteBarrier(&___recipeList_5, value);
	}

	inline static int32_t get_offset_of_lobbyRecipe_6() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___lobbyRecipe_6)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_lobbyRecipe_6() const { return ___lobbyRecipe_6; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_lobbyRecipe_6() { return &___lobbyRecipe_6; }
	inline void set_lobbyRecipe_6(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___lobbyRecipe_6 = value;
		Il2CppCodeGenWriteBarrier(&___lobbyRecipe_6, value);
	}

	inline static int32_t get_offset_of_coffeeBeanRecipe_7() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___coffeeBeanRecipe_7)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_coffeeBeanRecipe_7() const { return ___coffeeBeanRecipe_7; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_coffeeBeanRecipe_7() { return &___coffeeBeanRecipe_7; }
	inline void set_coffeeBeanRecipe_7(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___coffeeBeanRecipe_7 = value;
		Il2CppCodeGenWriteBarrier(&___coffeeBeanRecipe_7, value);
	}

	inline static int32_t get_offset_of_extractionRecipe_8() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___extractionRecipe_8)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_extractionRecipe_8() const { return ___extractionRecipe_8; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_extractionRecipe_8() { return &___extractionRecipe_8; }
	inline void set_extractionRecipe_8(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___extractionRecipe_8 = value;
		Il2CppCodeGenWriteBarrier(&___extractionRecipe_8, value);
	}

	inline static int32_t get_offset_of_steamMilkRecipe_9() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___steamMilkRecipe_9)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_steamMilkRecipe_9() const { return ___steamMilkRecipe_9; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_steamMilkRecipe_9() { return &___steamMilkRecipe_9; }
	inline void set_steamMilkRecipe_9(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___steamMilkRecipe_9 = value;
		Il2CppCodeGenWriteBarrier(&___steamMilkRecipe_9, value);
	}

	inline static int32_t get_offset_of_iceMixerRecipe_10() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___iceMixerRecipe_10)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_iceMixerRecipe_10() const { return ___iceMixerRecipe_10; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_iceMixerRecipe_10() { return &___iceMixerRecipe_10; }
	inline void set_iceMixerRecipe_10(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___iceMixerRecipe_10 = value;
		Il2CppCodeGenWriteBarrier(&___iceMixerRecipe_10, value);
	}

	inline static int32_t get_offset_of_variationRecipe_11() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___variationRecipe_11)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_variationRecipe_11() const { return ___variationRecipe_11; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_variationRecipe_11() { return &___variationRecipe_11; }
	inline void set_variationRecipe_11(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___variationRecipe_11 = value;
		Il2CppCodeGenWriteBarrier(&___variationRecipe_11, value);
	}

	inline static int32_t get_offset_of_randomValueByRecipe_12() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996, ___randomValueByRecipe_12)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_randomValueByRecipe_12() const { return ___randomValueByRecipe_12; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_randomValueByRecipe_12() { return &___randomValueByRecipe_12; }
	inline void set_randomValueByRecipe_12(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___randomValueByRecipe_12 = value;
		Il2CppCodeGenWriteBarrier(&___randomValueByRecipe_12, value);
	}
};

struct RecipeList_t1641733996_StaticFields
{
public:
	// RecipeList RecipeList::instance
	RecipeList_t1641733996 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(RecipeList_t1641733996_StaticFields, ___instance_2)); }
	inline RecipeList_t1641733996 * get_instance_2() const { return ___instance_2; }
	inline RecipeList_t1641733996 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(RecipeList_t1641733996 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
