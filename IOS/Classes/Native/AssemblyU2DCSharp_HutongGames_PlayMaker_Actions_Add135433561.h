﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddTorque
struct  AddTorque_t135433561  : public ComponentAction_1_t2322045418
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddTorque::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddTorque::vector
	FsmVector3_t533912882 * ___vector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::x
	FsmFloat_t2134102846 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::y
	FsmFloat_t2134102846 * ___y_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::z
	FsmFloat_t2134102846 * ___z_17;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.AddTorque::space
	int32_t ___space_18;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddTorque::forceMode
	int32_t ___forceMode_19;
	// System.Boolean HutongGames.PlayMaker.Actions.AddTorque::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_vector_14() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___vector_14)); }
	inline FsmVector3_t533912882 * get_vector_14() const { return ___vector_14; }
	inline FsmVector3_t533912882 ** get_address_of_vector_14() { return &___vector_14; }
	inline void set_vector_14(FsmVector3_t533912882 * value)
	{
		___vector_14 = value;
		Il2CppCodeGenWriteBarrier(&___vector_14, value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___x_15)); }
	inline FsmFloat_t2134102846 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t2134102846 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t2134102846 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier(&___x_15, value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___y_16)); }
	inline FsmFloat_t2134102846 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t2134102846 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t2134102846 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier(&___y_16, value);
	}

	inline static int32_t get_offset_of_z_17() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___z_17)); }
	inline FsmFloat_t2134102846 * get_z_17() const { return ___z_17; }
	inline FsmFloat_t2134102846 ** get_address_of_z_17() { return &___z_17; }
	inline void set_z_17(FsmFloat_t2134102846 * value)
	{
		___z_17 = value;
		Il2CppCodeGenWriteBarrier(&___z_17, value);
	}

	inline static int32_t get_offset_of_space_18() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___space_18)); }
	inline int32_t get_space_18() const { return ___space_18; }
	inline int32_t* get_address_of_space_18() { return &___space_18; }
	inline void set_space_18(int32_t value)
	{
		___space_18 = value;
	}

	inline static int32_t get_offset_of_forceMode_19() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___forceMode_19)); }
	inline int32_t get_forceMode_19() const { return ___forceMode_19; }
	inline int32_t* get_address_of_forceMode_19() { return &___forceMode_19; }
	inline void set_forceMode_19(int32_t value)
	{
		___forceMode_19 = value;
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(AddTorque_t135433561, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
