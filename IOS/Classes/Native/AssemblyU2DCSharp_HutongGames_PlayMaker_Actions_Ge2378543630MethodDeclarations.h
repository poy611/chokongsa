﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo
struct GetRayCastHit2dInfo_t2378543630;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::.ctor()
extern "C"  void GetRayCastHit2dInfo__ctor_m2785427688 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::Reset()
extern "C"  void GetRayCastHit2dInfo_Reset_m431860629 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnEnter()
extern "C"  void GetRayCastHit2dInfo_OnEnter_m423111359 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnUpdate()
extern "C"  void GetRayCastHit2dInfo_OnUpdate_m3660076900 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::StoreRaycastInfo()
extern "C"  void GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
