﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct GameObjectCompareTag_t3437146702;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::.ctor()
extern "C"  void GameObjectCompareTag__ctor_m1747131352 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::Reset()
extern "C"  void GameObjectCompareTag_Reset_m3688531589 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnEnter()
extern "C"  void GameObjectCompareTag_OnEnter_m3347712431 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnUpdate()
extern "C"  void GameObjectCompareTag_OnUpdate_m4128396916 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::DoCompareTag()
extern "C"  void GameObjectCompareTag_DoCompareTag_m623402700 (GameObjectCompareTag_t3437146702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
