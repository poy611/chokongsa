﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoReady
struct VideoReady_t259270055;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void MediaPlayerCtrl/VideoReady::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoReady__ctor_m3604465870 (VideoReady_t259270055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoReady::Invoke()
extern "C"  void VideoReady_Invoke_m3338328744 (VideoReady_t259270055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoReady::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoReady_BeginInvoke_m1017391707 (VideoReady_t259270055 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoReady::EndInvoke(System.IAsyncResult)
extern "C"  void VideoReady_EndInvoke_m488362462 (VideoReady_t259270055 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
