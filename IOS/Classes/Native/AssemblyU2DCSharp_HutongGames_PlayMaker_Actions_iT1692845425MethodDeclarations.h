﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleTo
struct iTweenScaleTo_t1692845425;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::.ctor()
extern "C"  void iTweenScaleTo__ctor_m1752242917 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::Reset()
extern "C"  void iTweenScaleTo_Reset_m3693643154 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnEnter()
extern "C"  void iTweenScaleTo_OnEnter_m3964959100 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnExit()
extern "C"  void iTweenScaleTo_OnExit_m4293241500 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::DoiTween()
extern "C"  void iTweenScaleTo_DoiTween_m4105780044 (iTweenScaleTo_t1692845425 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
