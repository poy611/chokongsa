﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// System.Boolean HutongGames.PlayMaker.FsmBool::get_Value()
extern "C"  bool FsmBool_get_Value_m3101329097 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::set_Value(System.Boolean)
extern "C"  void FsmBool_set_Value_m1126216340 (FsmBool_t1075959796 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmBool::get_RawValue()
extern "C"  Il2CppObject * FsmBool_get_RawValue_m3525943510 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::set_RawValue(System.Object)
extern "C"  void FsmBool_set_RawValue_m1524628629 (FsmBool_t1075959796 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor()
extern "C"  void FsmBool__ctor_m1553455211 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor(System.String)
extern "C"  void FsmBool__ctor_m1895566455 (FsmBool_t1075959796 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor(HutongGames.PlayMaker.FsmBool)
extern "C"  void FsmBool__ctor_m2241284265 (FsmBool_t1075959796 * __this, FsmBool_t1075959796 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmBool::Clone()
extern "C"  NamedVariable_t3211770239 * FsmBool_Clone_m70478352 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmBool::get_VariableType()
extern "C"  int32_t FsmBool_get_VariableType_m2025515801 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmBool::ToString()
extern "C"  String_t* FsmBool_ToString_m330557346 (FsmBool_t1075959796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmBool::op_Implicit(System.Boolean)
extern "C"  FsmBool_t1075959796 * FsmBool_op_Implicit_m2730611352 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
