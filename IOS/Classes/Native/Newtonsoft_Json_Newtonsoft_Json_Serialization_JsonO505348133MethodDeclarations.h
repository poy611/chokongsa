﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t505348133;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_t1258269582;
// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_t914719770;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerializatio1550301796.h"
#include "mscorlib_System_Nullable_1_gen4005432850.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP717767559.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exte1258269582.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exten914719770.h"
#include "mscorlib_System_Type2863145774.h"

// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::get_MemberSerialization()
extern "C"  int32_t JsonObjectContract_get_MemberSerialization_m2599966771 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern "C"  void JsonObjectContract_set_MemberSerialization_m1405608000 (JsonObjectContract_t505348133 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemRequired()
extern "C"  Nullable_1_t4005432850  JsonObjectContract_get_ItemRequired_m4259853945 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemRequired(System.Nullable`1<Newtonsoft.Json.Required>)
extern "C"  void JsonObjectContract_set_ItemRequired_m551153274 (JsonObjectContract_t505348133 * __this, Nullable_1_t4005432850  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_Properties()
extern "C"  JsonPropertyCollection_t717767559 * JsonObjectContract_get_Properties_m4162890027 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_Properties(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  void JsonObjectContract_set_Properties_m1714053804 (JsonObjectContract_t505348133 * __this, JsonPropertyCollection_t717767559 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_CreatorParameters()
extern "C"  JsonPropertyCollection_t717767559 * JsonObjectContract_get_CreatorParameters_m3961226016 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_OverrideConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JsonObjectContract_set_OverrideConstructor_m4184917829 (JsonObjectContract_t505348133 * __this, ConstructorInfo_t4136801618 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ParametrizedConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JsonObjectContract_set_ParametrizedConstructor_m3315061631 (JsonObjectContract_t505348133 * __this, ConstructorInfo_t4136801618 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_OverrideCreator()
extern "C"  ObjectConstructor_1_t2948332186 * JsonObjectContract_get_OverrideCreator_m1458095391 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_ParameterizedCreator()
extern "C"  ObjectConstructor_1_t2948332186 * JsonObjectContract_get_ParameterizedCreator_m4246264534 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataSetter()
extern "C"  ExtensionDataSetter_t1258269582 * JsonObjectContract_get_ExtensionDataSetter_m3966675257 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataSetter(Newtonsoft.Json.Serialization.ExtensionDataSetter)
extern "C"  void JsonObjectContract_set_ExtensionDataSetter_m1192787962 (JsonObjectContract_t505348133 * __this, ExtensionDataSetter_t1258269582 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataGetter()
extern "C"  ExtensionDataGetter_t914719770 * JsonObjectContract_get_ExtensionDataGetter_m3717174609 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataGetter(Newtonsoft.Json.Serialization.ExtensionDataGetter)
extern "C"  void JsonObjectContract_set_ExtensionDataGetter_m157109474 (JsonObjectContract_t505348133 * __this, ExtensionDataGetter_t914719770 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataValueType(System.Type)
extern "C"  void JsonObjectContract_set_ExtensionDataValueType_m4036984912 (JsonObjectContract_t505348133 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::get_HasRequiredOrDefaultValueProperties()
extern "C"  bool JsonObjectContract_get_HasRequiredOrDefaultValueProperties_m1132850756 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonObjectContract::.ctor(System.Type)
extern "C"  void JsonObjectContract__ctor_m594960127 (JsonObjectContract_t505348133 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonObjectContract::GetUninitializedObject()
extern "C"  Il2CppObject * JsonObjectContract_GetUninitializedObject_m3142984771 (JsonObjectContract_t505348133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
