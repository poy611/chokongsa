﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.MatchFieldTypeAttribute
struct MatchFieldTypeAttribute_t3836515319;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.MatchFieldTypeAttribute::get_FieldName()
extern "C"  String_t* MatchFieldTypeAttribute_get_FieldName_m3097752901 (MatchFieldTypeAttribute_t3836515319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.MatchFieldTypeAttribute::.ctor(System.String)
extern "C"  void MatchFieldTypeAttribute__ctor_m3048513594 (MatchFieldTypeAttribute_t3836515319 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
