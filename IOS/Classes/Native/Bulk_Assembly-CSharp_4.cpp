﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder
struct RealtimeRoomConfigBuilder_t200318681;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t922401854;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t294375316;
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t2359319983;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t1862808700;
// System.Object
struct Il2CppObject;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
struct Action_1_t1636230841;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
struct Func_2_t3669202295;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// System.Func`2<System.IntPtr,System.Object>
struct Func_2_t2304636665;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
struct Action_1_t2896490150;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
struct Func_2_t634494308;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>
struct Action_1_t2328837628;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>
struct Func_2_t66841786;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t3479575958;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t2885515174;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
struct Action_1_t3196978601;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
struct Func_2_t934982759;
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>
struct Action_1_t2688443720;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>
struct Func_2_t426447878;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t2801162465;
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t1240414705;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>
struct IEnumerable_1_t2485521619;
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>
struct Func_2_t1609885161;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Func`2<System.UIntPtr,System.Object>
struct Func_2_t2301125574;
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
struct OpenResponse_t1933021492;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t2292627584;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Byte>
struct OutMethod_1_t2402168711;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t2500674014;
// GooglePlayGames.Native.PInvoke.StatsManager
struct StatsManager_t4261398554;
// System.Action`1<GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse>
struct Action_1_t1663036183;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse>
struct Func_2_t3696007637;
// GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse
struct FetchForPlayerResponse_t1267220047;
// GooglePlayGames.Native.PInvoke.NativePlayerStats
struct NativePlayerStats_t1419601613;
// GooglePlayGames.Native.PInvoke.TurnBasedManager
struct TurnBasedManager_t3476156963;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
struct Action_1_t356501165;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t3069007197;
// System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
struct Action_1_t1318217990;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
struct Func_2_t3351189444;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>
struct Action_1_t3855938651;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>
struct Func_2_t1593942809;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
struct Action_1_t1855179219;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
struct Func_2_t3888150673;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
struct Action_1_t3071188627;
// GooglePlayGames.Native.PInvoke.ParticipantResults
struct ParticipantResults_t1948656911;
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
struct Func_2_t2389472619;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t1459363083;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback
struct TurnBasedMatchCallback_t128787945;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t3460122515;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct IEnumerable_1_t2417134198;
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Func_2_t1541497740;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct IEnumerable_1_t3603766383;
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Func_2_t2728129925;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;
// System.Func`2<System.UIntPtr,System.String>
struct Func_2_t2432508056;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8
struct U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder
struct TurnBasedMatchConfigBuilder_t1401664752;
// GooglePlayGames.Native.UnsupportedSavedGamesClient
struct UnsupportedSavedGamesClient_t1204855368;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t2072880178;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t942269343;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t3582269991;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>
struct Action_2_t2751370656;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t1151548080;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
struct Action_2_t3441065730;
// GooglePlayGames.OurUtils.Logger
struct Logger_t984534948;
// GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A
struct U3CdU3Ec__AnonStorey3A_t139427695;
// GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C
struct U3CeU3Ec__AnonStorey3C_t1492737394;
// GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B
struct U3CwU3Ec__AnonStorey3B_t82508163;
// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct PlayGamesHelperObject_t727662960;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Action
struct Action_t3771233898;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D
struct U3CRunCoroutineU3Ec__AnonStorey3D_t724587615;
// GooglePlayGames.PlayGamesAchievement
struct PlayGamesAchievement_t2357341912;
// GooglePlayGames.ReportProgress
struct ReportProgress_t3967815895;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// GooglePlayGames.PlayGamesClientFactory
struct PlayGamesClientFactory_t1822448424;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t2528289561;
// GooglePlayGames.PlayGamesLeaderboard
struct PlayGamesLeaderboard_t3198617382;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726;
// GooglePlayGames.BasicApi.LeaderboardScoreData
struct LeaderboardScoreData_t4006482697;
// GooglePlayGames.PlayGamesScore
struct PlayGamesScore_t486124539;
// GooglePlayGames.PlayGamesLocalUser
struct PlayGamesLocalUser_t4064039103;
// GooglePlayGames.PlayGamesPlatform
struct PlayGamesPlatform_t3624292130;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>
struct Action_2_t1913238692;
// GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34
struct U3CGetStatsU3Ec__AnonStorey34_t1599266691;
// GooglePlayGames.BasicApi.PlayerStats
struct PlayerStats_t60064856;
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient
struct INearbyConnectionClient_t1732200103;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient
struct IRealTimeMultiplayerClient_t3917639923;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient
struct ITurnBasedMultiplayerClient_t661201890;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
struct ISavedGameClient_t3735673347;
// GooglePlayGames.BasicApi.Events.IEventsClient
struct IEventsClient_t3804513627;
// GooglePlayGames.BasicApi.Quests.IQuestsClient
struct IQuestsClient_t519086643;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2654168339;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>
struct Action_1_t2128016239;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3814920354;
// System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>
struct Action_2_t1860405393;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t229750097;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2349069933;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t645920862;
// System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>
struct Action_1_t107331537;
// GooglePlayGames.BasicApi.ScorePageToken
struct ScorePageToken_t1995225314;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3799088250;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t823521528;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t2409308905;
// GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39
struct U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193;
// GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35
struct U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576;
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3251685236;
// GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36
struct U3CLoadAchievementsU3Ec__AnonStorey36_t225202025;
// GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37
struct U3CLoadScoresU3Ec__AnonStorey37_t2259335463;
// GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey38
struct U3CLoadScoresU3Ec__AnonStorey38_t2259335464;
// GooglePlayGames.PlayGamesUserProfile
struct PlayGamesUserProfile_t4293397255;
// GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3
struct U3CLoadImageU3Ec__Iterator3_t2198666563;
// GooglePlayGames.PluginVersion
struct PluginVersion_t3642060999;
// GPGSBtn
struct GPGSBtn_t946693767;
// GPGSBtn/<LoginChecker>c__IteratorD
struct U3CLoginCheckerU3Ec__IteratorD_t4246608817;
// GPGSBtn/<LoginCheckerNet>c__IteratorE
struct U3CLoginCheckerNetU3Ec__IteratorE_t3774081813;
// GPGSMng
struct GPGSMng_t946704145;
// GradeData
struct GradeData_t483491841;
// GradeDataInfo
struct GradeDataInfo_t1480749135;
// HutongGames.PlayMaker.Actions.ActivateGameObject
struct ActivateGameObject_t1848802188;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.AddAnimationClip
struct AddAnimationClip_t922763643;
// UnityEngine.Animation
struct Animation_t1724966010;
// HutongGames.PlayMaker.Actions.AddComponent
struct AddComponent_t2040077188;
// HutongGames.PlayMaker.Actions.AddExplosionForce
struct AddExplosionForce_t3285275685;
// HutongGames.PlayMaker.Actions.AddForce
struct AddForce_t2783319922;
// HutongGames.PlayMaker.Actions.AddForce2d
struct AddForce2d_t4008896548;
// HutongGames.PlayMaker.Actions.AddMixingTransform
struct AddMixingTransform_t436139917;
// HutongGames.PlayMaker.Actions.AddScript
struct AddScript_t95714330;
// HutongGames.PlayMaker.Actions.AddTorque
struct AddTorque_t135433561;
// HutongGames.PlayMaker.Actions.AddTorque2d
struct AddTorque2d_t2010742859;
// HutongGames.PlayMaker.Actions.AnimateColor
struct AnimateColor_t301720362;
// HutongGames.PlayMaker.Actions.AnimateFloat
struct AnimateFloat_t304404003;
// HutongGames.PlayMaker.Actions.AnimateFloatV2
struct AnimateFloatV2_t1182581439;
// HutongGames.PlayMaker.Actions.AnimateFsmAction
struct AnimateFsmAction_t4201352541;
// HutongGames.PlayMaker.Actions.AnimateRect
struct AnimateRect_t723840755;
// HutongGames.PlayMaker.Actions.AnimateVector3
struct AnimateVector3_t2286814231;
// HutongGames.PlayMaker.Actions.AnimationSettings
struct AnimationSettings_t3134957589;
// HutongGames.PlayMaker.Actions.AnimatorCrossFade
struct AnimatorCrossFade_t3506781187;
// UnityEngine.Animator
struct Animator_t2776330603;
// HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget
struct AnimatorInterruptMatchTarget_t1680292610;
// HutongGames.PlayMaker.Actions.AnimatorMatchTarget
struct AnimatorMatchTarget_t2687678941;
// HutongGames.PlayMaker.Actions.AnimatorPlay
struct AnimatorPlay_t630493411;
// HutongGames.PlayMaker.Actions.AnimatorStartPlayback
struct AnimatorStartPlayback_t1925519300;
// HutongGames.PlayMaker.Actions.AnimatorStartRecording
struct AnimatorStartRecording_t1781471326;
// HutongGames.PlayMaker.Actions.AnimatorStopPlayback
struct AnimatorStopPlayback_t1971151340;
// HutongGames.PlayMaker.Actions.AnimatorStopRecording
struct AnimatorStopRecording_t3196064566;
// HutongGames.PlayMaker.Actions.AnyKey
struct AnyKey_t2823194811;
// HutongGames.PlayMaker.Actions.ApplicationQuit
struct ApplicationQuit_t3450523853;
// HutongGames.PlayMaker.Actions.ApplicationRunInBackground
struct ApplicationRunInBackground_t3684091478;
// HutongGames.PlayMaker.Actions.ArrayAdd
struct ArrayAdd_t2749463600;
// HutongGames.PlayMaker.Actions.ArrayAddRange
struct ArrayAddRange_t613168227;
// HutongGames.PlayMaker.Actions.ArrayClear
struct ArrayClear_t1539829980;
// HutongGames.PlayMaker.Actions.ArrayCompare
struct ArrayCompare_t3109678708;
// HutongGames.PlayMaker.Actions.ArrayContains
struct ArrayContains_t1294114150;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// HutongGames.PlayMaker.Actions.ArrayDeleteAt
struct ArrayDeleteAt_t2681271941;
// HutongGames.PlayMaker.Actions.ArrayForEach
struct ArrayForEach_t1480558585;
// HutongGames.PlayMaker.Actions.ArrayGet
struct ArrayGet_t2749469413;
// HutongGames.PlayMaker.Actions.ArrayGetNext
struct ArrayGetNext_t2083890424;
// HutongGames.PlayMaker.Actions.ArrayGetRandom
struct ArrayGetRandom_t1982558280;
// HutongGames.PlayMaker.Actions.ArrayLength
struct ArrayLength_t92600621;
// HutongGames.PlayMaker.Actions.ArrayResize
struct ArrayResize_t264526587;
// HutongGames.PlayMaker.Actions.ArrayReverse
struct ArrayReverse_t3259040881;
// HutongGames.PlayMaker.Actions.ArraySet
struct ArraySet_t2749480945;
// HutongGames.PlayMaker.Actions.ArrayShuffle
struct ArrayShuffle_t4231526536;
// HutongGames.PlayMaker.Actions.ArraySort
struct ArraySort_t2980580069;
// HutongGames.PlayMaker.Actions.ArrayTransferValue
struct ArrayTransferValue_t3288840917;
// HutongGames.PlayMaker.Actions.AudioMute
struct AudioMute_t298551901;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// HutongGames.PlayMaker.Actions.AudioPause
struct AudioPause_t18390312;
// HutongGames.PlayMaker.Actions.AudioPlay
struct AudioPlay_t298632056;
// HutongGames.PlayMaker.Actions.AudioStop
struct AudioStop_t298729542;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re200318681.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re200318681MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Void2863195528.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_B2237584300MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Pl922401854.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1368061809MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_UInt6424668076.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re294375316.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re294375316MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2359319983.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2359319983MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_G1862808700.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc1723690688MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc1723690688.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3670871388.h"
#include "mscorlib_System_Action_1_gen1636230841.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_G1862808700MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S677738899MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3669202295MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3420130036MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2815248615MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S677738899.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1240414705.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1240414705MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3669202295.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3420130036.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_C3660831390.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Action_1_gen2896490150.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2531066752MethodDeclarations.h"
#include "System_Core_System_Func_2_gen634494308MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2531066752.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2500674014.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2500674014MethodDeclarations.h"
#include "System_Core_System_Func_2_gen634494308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3763226879.h"
#include "mscorlib_System_Action_1_gen2328837628.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1210339286MethodDeclarations.h"
#include "System_Core_System_Func_2_gen66841786MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1210339286.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1933021492.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1933021492MethodDeclarations.h"
#include "System_Core_System_Func_2_gen66841786.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N3479575958.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N2885515174.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Action_1_gen3196978601.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1623255043MethodDeclarations.h"
#include "System_Core_System_Func_2_gen934982759MethodDeclarations.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_UIntPtr3365854250MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1623255043.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2801162465.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2801162465MethodDeclarations.h"
#include "System_Core_System_Func_2_gen934982759.h"
#include "mscorlib_System_Action_1_gen2688443720.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1569945378MethodDeclarations.h"
#include "System_Core_System_Func_2_gen426447878MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1569945378.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2292627584.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2292627584MethodDeclarations.h"
#include "System_Core_System_Func_2_gen426447878.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N3479575958MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1609885161MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2826698606MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1609885161.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2826698606.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2173406145.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P1062451542MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P1062451542.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2402168711MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P2402168711.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S4261398554.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S4261398554MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1663036183.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1995639285MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3696007637MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3013934414MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1995639285.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1267220047.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1267220047MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3696007637.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N1419601613.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N1419601613MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3476156963.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3476156963MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen356501165.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3800683771MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3921466855MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3800683771.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3069007197.h"
#include "mscorlib_System_Action_1_gen1318217990.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4277124957MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3351189444MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4277124957.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Pl922401854MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3351189444.h"
#include "mscorlib_System_Action_1_gen3855938651.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1856996905MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1593942809MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1856996905.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3460122515.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3460122515MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1593942809.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3411188537.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger984534948MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3337232325.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P1948656911.h"
#include "mscorlib_System_Action_1_gen1855179219.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3070391457MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3888150673MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3070391457.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1459363083.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1459363083MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3888150673.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "mscorlib_System_Action_1_gen3071188627MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3071188627.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T994271114MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T994271114.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "System_Core_System_Func_2_gen2389472619MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2389472619.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Tu128787945.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Tu128787945MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "System_Core_System_Func_2_gen1541497740MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1541497740.h"
#include "mscorlib_System_Int321153838500.h"
#include "System_Core_System_Func_2_gen2728129925MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2728129925.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3411188537MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3069007197MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1404240610MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1404240610.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2252410001MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2432508056MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2432508056.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1401664752.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1401664752MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T506248488MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Unsupport1204855368.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Unsupport1204855368MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSou3931761423.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2415333593.h"
#include "System_Core_System_Action_2_gen2072880178.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGam942269343.h"
#include "System_Core_System_Action_2_gen2751370656.h"
#include "System_Core_System_Action_2_gen1151548080.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4052853983.h"
#include "System_Core_System_Action_2_gen3441065730.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger984534948.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U139427695MethodDeclarations.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame727662960MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U139427695.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U382508163MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U382508163.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_1492737394MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_1492737394.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame727662960.h"
#include "mscorlib_System_Collections_Generic_List_1_gen844452154MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen844452154.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2240800406MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2240800406.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame724587615MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGame724587615.h"
#include "mscorlib_System_Threading_Monitor2734674376MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2260473176.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2260473176MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesAchieve2357341912.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesAchieve2357341912MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3624292130MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress3967815895MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3624292130.h"
#include "mscorlib_System_Double3868226565.h"
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress3967815895.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_WWW3134621005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesClientF1822448424.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesClientF1822448424MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam4135364200.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DummyCl2268884141MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_IOS_IOSClient3522634302MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3798002602MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DummyCl2268884141.h"
#include "AssemblyU2DCSharp_GooglePlayGames_IOS_IOSClient3522634302.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeCli3798002602.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLeaderb3198617382.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLeaderb3198617382MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1854310091MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1854310091.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_ArrayTypes.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScore486124539.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb4006482697.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb4006482697MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScore486124539MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs4064039103.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs4064039103MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro4293397255MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayerSta60064856.h"
#include "mscorlib_System_Action_1_gen403047693.h"
#include "mscorlib_System_Action_1_gen403047693MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro4293397255.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "System_Core_System_Action_2_gen1913238692.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs1599266691MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayerSta60064856MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1913238692MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesLocalUs1599266691.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam4135364200MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2128016239.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_2104311078MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2128016239MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_2104311078.h"
#include "UnityEngine_UnityEngine_Social3197796273MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354.h"
#include "mscorlib_System_Action_1_gen3814920354MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1860405393.h"
#include "AssemblyU2DCSharp_GooglePlayGames_GameInfo3799103638MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1860405393MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor1592139576MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3647501372MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor1592139576.h"
#include "mscorlib_System_Action_1_gen3647501372.h"
#include "mscorlib_System_Action_1_gen2349069933.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatform225202025MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2349069933MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatform225202025.h"
#include "mscorlib_System_Action_1_gen645920862.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335463MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen107331537MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335463.h"
#include "mscorlib_System_Action_1_gen107331537.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2572053391.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb3617536533.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2320727150.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ScorePa1995225314.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ScorePa1995225314MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen823521528.h"
#include "mscorlib_System_Action_1_gen823521528MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335464MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor2259335464.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Invitat2409308905.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3867310193MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesPlatfor3867310193.h"
#include "mscorlib_System_Action_1_gen645920862MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro2198666563MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro2198666563.h"
#include "mscorlib_System_StringComparer4230573202MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PluginVersion3642060999.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PluginVersion3642060999MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGmanager385716591.h"
#include "AssemblyU2DCSharp_GPGmanager385716591MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSBtn946693767.h"
#include "AssemblyU2DCSharp_GPGSBtn946693767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1199519538MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSMng946704145MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSMng946704145.h"
#include "AssemblyU2DCSharp_GPGSBtn_U3CLoginCheckerU3Ec__Ite4246608817MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSBtn_U3CLoginCheckerU3Ec__Ite4246608817.h"
#include "AssemblyU2DCSharp_GPGSBtn_U3CLoginCheckerNetU3Ec__3774081813MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSBtn_U3CLoginCheckerNetU3Ec__3774081813.h"
#include "AssemblyU2DCSharp_SoundManage3403985716MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManage3403985716.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage821930207MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage821930207.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "mscorlib_System_Single4291918972.h"
#include "AssemblyU2DCSharp_Singleton_1_gen2778891315MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1768030745MethodDeclarations.h"
#include "AssemblyU2DCSharp_NetworkMng1515215352MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainUserInfo2526075922.h"
#include "AssemblyU2DCSharp_NetworkMng1515215352.h"
#include "AssemblyU2DCSharp_GradeData483491841.h"
#include "AssemblyU2DCSharp_GradeData483491841MethodDeclarations.h"
#include "AssemblyU2DCSharp_GradeDataInfo1480749135.h"
#include "AssemblyU2DCSharp_GradeDataInfo1480749135MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ac1848802188.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ac1848802188MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add922763643.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add922763643MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation1724966010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2040077188.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2040077188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "PlayMaker_HutongGames_PlayMaker_ReflectionUtils1202855664MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad3285275685.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad3285275685MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2783319922.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2783319922MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad4008896548.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad4008896548MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com719239868MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add436139917.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add436139917MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2825067351MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com700434209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_AddS95714330.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_AddS95714330MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add135433561.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Add135433561MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2010742859.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ad2010742859MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani301720362.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani301720362MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An4201352541MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An4201352541.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve2685995989.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani304404003.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani304404003MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1182581439.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1182581439MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani723840755.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani723840755MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2286814231.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2286814231MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3134957589.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3134957589MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3506781187.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3506781187MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1680292610.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1680292610MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2687678941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2687678941MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani630493411.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani630493411MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1925519300.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1925519300MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1781471326.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1781471326MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1971151340.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An1971151340MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3196064566.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An3196064566MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2823194811.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2823194811MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ap3450523853.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ap3450523853MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ap3684091478.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ap3684091478MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749463600.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749463600MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr613168227.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr613168227MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1539829980.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1539829980MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3109678708.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3109678708MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1294114150.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1294114150MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2681271941.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2681271941MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1480558585.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1480558585MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ru3707651955MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ru3707651955.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749469413.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749469413MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2083890424.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2083890424MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1982558280.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1982558280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arra92600621.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arra92600621MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr264526587.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr264526587MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3259040881.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3259040881MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749480945.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2749480945MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4231526536.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4231526536MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2980580069.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2980580069MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3288840917.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3288840917MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2700806265.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "mscorlib_System_Enum2862688501.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3581903417.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3581903417MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2700806265MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298551901.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298551901MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Audi18390312.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Audi18390312MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298632056.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298632056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298729542.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Aud298729542MethodDeclarations.h"

// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
extern "C"  Il2CppObject * Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Misc_CheckNotNull_TisIl2CppObject_m3632510375(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.GameServices>(!!0)
#define Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540(__this /* static, unused */, p0, method) ((  GameServices_t1862808700 * (*) (Il2CppObject * /* static, unused */, GameServices_t1862808700 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<System.Object>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
extern "C"  IntPtr_t Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared (Il2CppObject * __this /* static, unused */, Action_1_t271665211 * p0, Func_2_t2304636665 * p1, const MethodInfo* method);
#define Callbacks_ToIntPtr_TisIl2CppObject_m4197446764(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t271665211 *, Func_2_t2304636665 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchAllResponse_t1240414705_m2748838911(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t1636230841 *, Func_2_t3669202295 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisSnapshotSelectUIResponse_t2500674014_m1113705196(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t2896490150 *, Func_2_t634494308 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
#define Misc_CheckNotNull_TisString_t_m699927801(__this /* static, unused */, p0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t2328837628_m2095484061(__this /* static, unused */, p0, method) ((  Action_1_t2328837628 * (*) (Il2CppObject * /* static, unused */, Action_1_t2328837628 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisOpenResponse_t1933021492_m2333321538(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t2328837628 *, Func_2_t66841786 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>(!!0)
#define Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878(__this /* static, unused */, p0, method) ((  NativeSnapshotMetadata_t3479575958 * (*) (Il2CppObject * /* static, unused */, NativeSnapshotMetadata_t3479575958 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange>(!!0)
#define Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094(__this /* static, unused */, p0, method) ((  NativeSnapshotMetadataChange_t2885515174 * (*) (Il2CppObject * /* static, unused */, NativeSnapshotMetadataChange_t2885515174 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t3196978601 *, Func_2_t934982759 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t2688443720_m1632032721(__this /* static, unused */, p0, method) ((  Action_1_t2688443720 * (*) (Il2CppObject * /* static, unused */, Action_1_t2688443720 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisReadResponse_t2292627584_m1020558478(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t2688443720 *, Func_2_t426447878 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
extern "C"  Il2CppObject* PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186_gshared (Il2CppObject * __this /* static, unused */, UIntPtr_t  p0, Func_2_t2301125574 * p1, const MethodInfo* method);
#define PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t2301125574 *, const MethodInfo*))PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerable_TisNativeSnapshotMetadata_t3479575958_m659440259(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t1609885161 *, const MethodInfo*))PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToArray<System.Byte>(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<!!0>)
extern "C"  ByteU5BU5D_t4260760469* PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802_gshared (Il2CppObject * __this /* static, unused */, OutMethod_1_t2402168711 * p0, const MethodInfo* method);
#define PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802(__this /* static, unused */, p0, method) ((  ByteU5BU5D_t4260760469* (*) (Il2CppObject * /* static, unused */, OutMethod_1_t2402168711 *, const MethodInfo*))PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802_gshared)(__this /* static, unused */, p0, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`1<GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse>>(!!0)
#define Misc_CheckNotNull_TisAction_1_t1663036183_m3554545026(__this /* static, unused */, p0, method) ((  Action_1_t1663036183 * (*) (Il2CppObject * /* static, unused */, Action_1_t1663036183 *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisFetchForPlayerResponse_t1267220047_m1308064849(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t1663036183 *, Func_2_t3696007637 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisPlayerSelectUIResponse_t922401854_m3315070755(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t1318217990 *, Func_2_t3351189444 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisTurnBasedMatchesResponse_t3460122515_m954953237(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t3855938651 *, Func_2_t1593942809 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisMatchInboxUIResponse_t1459363083_m4000897165(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t1855179219 *, Func_2_t3888150673 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Object>(System.IntPtr)
extern "C"  Il2CppObject * Callbacks_IntPtrToTempCallback_TisIl2CppObject_m312714400_gshared (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method);
#define Callbacks_IntPtrToTempCallback_TisIl2CppObject_m312714400(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, IntPtr_t, const MethodInfo*))Callbacks_IntPtrToTempCallback_TisIl2CppObject_m312714400_gshared)(__this /* static, unused */, p0, method)
// !!0 GooglePlayGames.Native.PInvoke.Callbacks::IntPtrToTempCallback<System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>>(System.IntPtr)
#define Callbacks_IntPtrToTempCallback_TisAction_1_t3071188627_m1604055793(__this /* static, unused */, p0, method) ((  Action_1_t3071188627 * (*) (Il2CppObject * /* static, unused */, IntPtr_t, const MethodInfo*))Callbacks_IntPtrToTempCallback_TisIl2CppObject_m312714400_gshared)(__this /* static, unused */, p0, method)
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>(System.Action`1<!!0>,System.Func`2<System.IntPtr,!!0>)
#define Callbacks_ToIntPtr_TisTurnBasedMatchResponse_t4255652325_m2749825255(__this /* static, unused */, p0, p1, method) ((  IntPtr_t (*) (Il2CppObject * /* static, unused */, Action_1_t356501165 *, Func_2_t2389472619 *, const MethodInfo*))Callbacks_ToIntPtr_TisIl2CppObject_m4197446764_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerable_TisMultiplayerInvitation_t3411188537_m3316655632(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t1541497740 *, const MethodInfo*))PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerable<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t2728129925 *, const MethodInfo*))PInvokeUtilities_ToEnumerable_TisIl2CppObject_m2205811186_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.Object>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
extern "C"  Il2CppObject* PInvokeUtilities_ToEnumerator_TisIl2CppObject_m3818697842_gshared (Il2CppObject * __this /* static, unused */, UIntPtr_t  p0, Func_2_t2301125574 * p1, const MethodInfo* method);
#define PInvokeUtilities_ToEnumerator_TisIl2CppObject_m3818697842(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t2301125574 *, const MethodInfo*))PInvokeUtilities_ToEnumerator_TisIl2CppObject_m3818697842_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerator`1<!!0> GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToEnumerator<System.String>(System.UIntPtr,System.Func`2<System.UIntPtr,!!0>)
#define PInvokeUtilities_ToEnumerator_TisString_t_m5577312(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UIntPtr_t , Func_2_t2432508056 *, const MethodInfo*))PInvokeUtilities_ToEnumerator_TisIl2CppObject_m3818697842_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m816128272_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m816128272(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m816128272_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<GooglePlayGames.OurUtils.PlayGamesHelperObject>()
#define GameObject_AddComponent_TisPlayGamesHelperObject_t727662960_m2233263623(__this, method) ((  PlayGamesHelperObject_t727662960 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m816128272_gshared)(__this, method)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.IPlayGamesClient>(!!0)
#define Misc_CheckNotNull_TisIPlayGamesClient_t2528289561_m747065152(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Misc_CheckNotNull_TisIl2CppObject_m3632510375_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t1724966010_m1707361494(__this, method) ((  Animation_t1724966010 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(__this, method) ((  Animator_t2776330603 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable::SequenceEqual<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_SequenceEqual_TisIl2CppObject_m1941331891_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Enumerable_SequenceEqual_TisIl2CppObject_m1941331891(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_SequenceEqual_TisIl2CppObject_m1941331891_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(__this, method) ((  AudioSource_t1740077639 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t RealtimeRoomConfigBuilder__ctor_m299567561_MetadataUsageId;
extern "C"  void RealtimeRoomConfigBuilder__ctor_m299567561 (RealtimeRoomConfigBuilder_t200318681 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RealtimeRoomConfigBuilder__ctor_m299567561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::PopulateFromUIResponse(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_PopulateFromUIResponse_m181648622 (RealtimeRoomConfigBuilder_t200318681 * __this, PlayerSelectUIResponse_t922401854 * ___response0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		PlayerSelectUIResponse_t922401854 * L_1 = ___response0;
		NullCheck(L_1);
		IntPtr_t L_2 = BaseReferenceHolder_AsPointer_m1925325420(L_1, /*hidden argument*/NULL);
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse_m1108133859(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetVariant(System.UInt32)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_SetVariant_m2361628241 (RealtimeRoomConfigBuilder_t200318681 * __this, uint32_t ___variantValue0, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	uint32_t G_B3_0 = 0;
	{
		uint32_t L_0 = ___variantValue0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = ((uint32_t)((-1)));
		goto IL_000d;
	}

IL_000c:
	{
		uint32_t L_1 = ___variantValue0;
		G_B3_0 = L_1;
	}

IL_000d:
	{
		V_0 = G_B3_0;
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_3 = V_0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetVariant_m3493815174(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::AddInvitedPlayer(System.String)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_AddInvitedPlayer_m675683521 (RealtimeRoomConfigBuilder_t200318681 * __this, String_t* ___playerId0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___playerId0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_AddPlayerToInvite_m2908520879(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetExclusiveBitMask(System.UInt64)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_SetExclusiveBitMask_m1200729954 (RealtimeRoomConfigBuilder_t200318681 * __this, uint64_t ___bitmask0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint64_t L_1 = ___bitmask0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetExclusiveBitMask_m4001679775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetMinimumAutomatchingPlayers(System.UInt32)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_SetMinimumAutomatchingPlayers_m2639983908 (RealtimeRoomConfigBuilder_t200318681 * __this, uint32_t ___minimum0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ___minimum0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers_m1383316283(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetMaximumAutomatchingPlayers(System.UInt32)
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_SetMaximumAutomatchingPlayers_m3195344566 (RealtimeRoomConfigBuilder_t200318681 * __this, uint32_t ___maximum0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ___maximum0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers_m4197123561(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::Build()
extern Il2CppClass* RealtimeRoomConfig_t294375316_il2cpp_TypeInfo_var;
extern const uint32_t RealtimeRoomConfigBuilder_Build_m430414255_MetadataUsageId;
extern "C"  RealtimeRoomConfig_t294375316 * RealtimeRoomConfigBuilder_Build_m430414255 (RealtimeRoomConfigBuilder_t200318681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RealtimeRoomConfigBuilder_Build_m430414255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Create_m1280644352(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RealtimeRoomConfig_t294375316 * L_2 = (RealtimeRoomConfig_t294375316 *)il2cpp_codegen_object_new(RealtimeRoomConfig_t294375316_il2cpp_TypeInfo_var);
		RealtimeRoomConfig__ctor_m3264667508(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealtimeRoomConfigBuilder_CallDispose_m909697287 (RealtimeRoomConfigBuilder_t200318681 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Dispose_m3244885484(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::Create()
extern Il2CppClass* RealtimeRoomConfigBuilder_t200318681_il2cpp_TypeInfo_var;
extern const uint32_t RealtimeRoomConfigBuilder_Create_m2260369602_MetadataUsageId;
extern "C"  RealtimeRoomConfigBuilder_t200318681 * RealtimeRoomConfigBuilder_Create_m2260369602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RealtimeRoomConfigBuilder_Create_m2260369602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Construct_m2185716274(NULL /*static, unused*/, /*hidden argument*/NULL);
		RealtimeRoomConfigBuilder_t200318681 * L_1 = (RealtimeRoomConfigBuilder_t200318681 *)il2cpp_codegen_object_new(RealtimeRoomConfigBuilder_t200318681_il2cpp_TypeInfo_var);
		RealtimeRoomConfigBuilder__ctor_m299567561(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern const MethodInfo* Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540_MethodInfo_var;
extern const uint32_t SnapshotManager__ctor_m3022785802_MetadataUsageId;
extern "C"  void SnapshotManager__ctor_m3022785802 (SnapshotManager_t2359319983 * __this, GameServices_t1862808700 * ___services0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager__ctor_m3022785802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GameServices_t1862808700 * L_0 = ___services0;
		GameServices_t1862808700 * L_1 = Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540_MethodInfo_var);
		__this->set_mServices_0(L_1);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::FetchAll(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>)
extern Il2CppClass* FetchAllCallback_t677738899_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3669202295_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* SnapshotManager_InternalFetchAllCallback_m2667701258_MethodInfo_var;
extern const MethodInfo* FetchAllResponse_FromPointer_m1786032583_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3306991433_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisFetchAllResponse_t1240414705_m2748838911_MethodInfo_var;
extern const uint32_t SnapshotManager_FetchAll_m1498170345_MetadataUsageId;
extern "C"  void SnapshotManager_FetchAll_m1498170345 (SnapshotManager_t2359319983 * __this, int32_t ___source0, Action_1_t1636230841 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_FetchAll_m1498170345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___source0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SnapshotManager_InternalFetchAllCallback_m2667701258_MethodInfo_var);
		FetchAllCallback_t677738899 * L_4 = (FetchAllCallback_t677738899 *)il2cpp_codegen_object_new(FetchAllCallback_t677738899_il2cpp_TypeInfo_var);
		FetchAllCallback__ctor_m3335956666(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t1636230841 * L_5 = ___callback1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)FetchAllResponse_FromPointer_m1786032583_MethodInfo_var);
		Func_2_t3669202295 * L_7 = (Func_2_t3669202295 *)il2cpp_codegen_object_new(Func_2_t3669202295_il2cpp_TypeInfo_var);
		Func_2__ctor_m3306991433(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3306991433_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_8 = Callbacks_ToIntPtr_TisFetchAllResponse_t1240414705_m2748838911(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/Callbacks_ToIntPtr_TisFetchAllResponse_t1240414705_m2748838911_MethodInfo_var);
		SnapshotManager_SnapshotManager_FetchAll_m854890322(NULL /*static, unused*/, L_1, L_2, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1065289254;
extern const uint32_t SnapshotManager_InternalFetchAllCallback_m2667701258_MetadataUsageId;
extern "C"  void SnapshotManager_InternalFetchAllCallback_m2667701258 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_InternalFetchAllCallback_m2667701258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral1065289254, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SnapshotManager_InternalFetchAllCallback_m2667701258(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	SnapshotManager_InternalFetchAllCallback_m2667701258(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::SnapshotSelectUI(System.Boolean,System.Boolean,System.UInt32,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>)
extern Il2CppClass* SnapshotSelectUICallback_t2531066752_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t634494308_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* SnapshotManager_InternalSnapshotSelectUICallback_m3686048061_MethodInfo_var;
extern const MethodInfo* SnapshotSelectUIResponse_FromPointer_m2303338145_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m243735164_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisSnapshotSelectUIResponse_t2500674014_m1113705196_MethodInfo_var;
extern const uint32_t SnapshotManager_SnapshotSelectUI_m2357835197_MetadataUsageId;
extern "C"  void SnapshotManager_SnapshotSelectUI_m2357835197 (SnapshotManager_t2359319983 * __this, bool ___allowCreate0, bool ___allowDelete1, uint32_t ___maxSnapshots2, String_t* ___uiTitle3, Action_1_t2896490150 * ___callback4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_SnapshotSelectUI_m2357835197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		bool L_2 = ___allowCreate0;
		bool L_3 = ___allowDelete1;
		uint32_t L_4 = ___maxSnapshots2;
		String_t* L_5 = ___uiTitle3;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)SnapshotManager_InternalSnapshotSelectUICallback_m3686048061_MethodInfo_var);
		SnapshotSelectUICallback_t2531066752 * L_7 = (SnapshotSelectUICallback_t2531066752 *)il2cpp_codegen_object_new(SnapshotSelectUICallback_t2531066752_il2cpp_TypeInfo_var);
		SnapshotSelectUICallback__ctor_m777984039(L_7, NULL, L_6, /*hidden argument*/NULL);
		Action_1_t2896490150 * L_8 = ___callback4;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SnapshotSelectUIResponse_FromPointer_m2303338145_MethodInfo_var);
		Func_2_t634494308 * L_10 = (Func_2_t634494308 *)il2cpp_codegen_object_new(Func_2_t634494308_il2cpp_TypeInfo_var);
		Func_2__ctor_m243735164(L_10, NULL, L_9, /*hidden argument*/Func_2__ctor_m243735164_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_11 = Callbacks_ToIntPtr_TisSnapshotSelectUIResponse_t2500674014_m1113705196(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/Callbacks_ToIntPtr_TisSnapshotSelectUIResponse_t2500674014_m1113705196_MethodInfo_var);
		SnapshotManager_SnapshotManager_ShowSelectUIOperation_m596028194(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalSnapshotSelectUICallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral351685907;
extern const uint32_t SnapshotManager_InternalSnapshotSelectUICallback_m3686048061_MetadataUsageId;
extern "C"  void SnapshotManager_InternalSnapshotSelectUICallback_m3686048061 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_InternalSnapshotSelectUICallback_m3686048061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral351685907, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SnapshotManager_InternalSnapshotSelectUICallback_m3686048061(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	SnapshotManager_InternalSnapshotSelectUICallback_m3686048061(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Open(System.String,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse>)
extern Il2CppClass* OpenCallback_t1210339286_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t66841786_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisAction_1_t2328837628_m2095484061_MethodInfo_var;
extern const MethodInfo* SnapshotManager_InternalOpenCallback_m2466174631_MethodInfo_var;
extern const MethodInfo* OpenResponse_FromPointer_m1911824973_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1735527526_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisOpenResponse_t1933021492_m2333321538_MethodInfo_var;
extern const uint32_t SnapshotManager_Open_m1538848608_MetadataUsageId;
extern "C"  void SnapshotManager_Open_m1538848608 (SnapshotManager_t2359319983 * __this, String_t* ___fileName0, int32_t ___source1, int32_t ___conflictPolicy2, Action_1_t2328837628 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_Open_m1538848608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fileName0;
		Misc_CheckNotNull_TisString_t_m699927801(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var);
		Action_1_t2328837628 * L_1 = ___callback3;
		Misc_CheckNotNull_TisAction_1_t2328837628_m2095484061(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t2328837628_m2095484061_MethodInfo_var);
		GameServices_t1862808700 * L_2 = __this->get_mServices_0();
		NullCheck(L_2);
		HandleRef_t1780819301  L_3 = GameServices_AsHandle_m4151855468(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___source1;
		String_t* L_5 = ___fileName0;
		int32_t L_6 = ___conflictPolicy2;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)SnapshotManager_InternalOpenCallback_m2466174631_MethodInfo_var);
		OpenCallback_t1210339286 * L_8 = (OpenCallback_t1210339286 *)il2cpp_codegen_object_new(OpenCallback_t1210339286_il2cpp_TypeInfo_var);
		OpenCallback__ctor_m2093256061(L_8, NULL, L_7, /*hidden argument*/NULL);
		Action_1_t2328837628 * L_9 = ___callback3;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)OpenResponse_FromPointer_m1911824973_MethodInfo_var);
		Func_2_t66841786 * L_11 = (Func_2_t66841786 *)il2cpp_codegen_object_new(Func_2_t66841786_il2cpp_TypeInfo_var);
		Func_2__ctor_m1735527526(L_11, NULL, L_10, /*hidden argument*/Func_2__ctor_m1735527526_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_12 = Callbacks_ToIntPtr_TisOpenResponse_t1933021492_m2333321538(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/Callbacks_ToIntPtr_TisOpenResponse_t1933021492_m2333321538_MethodInfo_var);
		SnapshotManager_SnapshotManager_Open_m2153573951(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_8, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalOpenCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral961093097;
extern const uint32_t SnapshotManager_InternalOpenCallback_m2466174631_MetadataUsageId;
extern "C"  void SnapshotManager_InternalOpenCallback_m2466174631 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_InternalOpenCallback_m2466174631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral961093097, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SnapshotManager_InternalOpenCallback_m2466174631(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	SnapshotManager_InternalOpenCallback_m2466174631(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Commit(GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange,System.Byte[],System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>)
extern Il2CppClass* CommitCallback_t1623255043_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t934982759_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094_MethodInfo_var;
extern const MethodInfo* SnapshotManager_InternalCommitCallback_m3500800666_MethodInfo_var;
extern const MethodInfo* CommitResponse_FromPointer_m1676551783_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1644784025_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039_MethodInfo_var;
extern const uint32_t SnapshotManager_Commit_m2640084546_MetadataUsageId;
extern "C"  void SnapshotManager_Commit_m2640084546 (SnapshotManager_t2359319983 * __this, NativeSnapshotMetadata_t3479575958 * ___metadata0, NativeSnapshotMetadataChange_t2885515174 * ___metadataChange1, ByteU5BU5D_t4260760469* ___updatedData2, Action_1_t3196978601 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_Commit_m2640084546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeSnapshotMetadata_t3479575958 * L_0 = ___metadata0;
		Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var);
		NativeSnapshotMetadataChange_t2885515174 * L_1 = ___metadataChange1;
		Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094_MethodInfo_var);
		GameServices_t1862808700 * L_2 = __this->get_mServices_0();
		NullCheck(L_2);
		HandleRef_t1780819301  L_3 = GameServices_AsHandle_m4151855468(L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = ___metadata0;
		NullCheck(L_4);
		IntPtr_t L_5 = BaseReferenceHolder_AsPointer_m1925325420(L_4, /*hidden argument*/NULL);
		NativeSnapshotMetadataChange_t2885515174 * L_6 = ___metadataChange1;
		NullCheck(L_6);
		IntPtr_t L_7 = BaseReferenceHolder_AsPointer_m1925325420(L_6, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_8 = ___updatedData2;
		ByteU5BU5D_t4260760469* L_9 = ___updatedData2;
		NullCheck(L_9);
		UIntPtr_t  L_10;
		memset(&L_10, 0, sizeof(L_10));
		UIntPtr__ctor_m3952300446(&L_10, (((int64_t)((int64_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))))), /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)SnapshotManager_InternalCommitCallback_m3500800666_MethodInfo_var);
		CommitCallback_t1623255043 * L_12 = (CommitCallback_t1623255043 *)il2cpp_codegen_object_new(CommitCallback_t1623255043_il2cpp_TypeInfo_var);
		CommitCallback__ctor_m1437284650(L_12, NULL, L_11, /*hidden argument*/NULL);
		Action_1_t3196978601 * L_13 = ___callback3;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)CommitResponse_FromPointer_m1676551783_MethodInfo_var);
		Func_2_t934982759 * L_15 = (Func_2_t934982759 *)il2cpp_codegen_object_new(Func_2_t934982759_il2cpp_TypeInfo_var);
		Func_2__ctor_m1644784025(L_15, NULL, L_14, /*hidden argument*/Func_2__ctor_m1644784025_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_16 = Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039_MethodInfo_var);
		SnapshotManager_SnapshotManager_Commit_m1395112938(NULL /*static, unused*/, L_3, L_5, L_7, L_8, L_10, L_12, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Resolve(GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadataChange,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>)
extern Il2CppClass* CommitCallback_t1623255043_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t934982759_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var;
extern const MethodInfo* SnapshotManager_InternalCommitCallback_m3500800666_MethodInfo_var;
extern const MethodInfo* CommitResponse_FromPointer_m1676551783_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1644784025_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039_MethodInfo_var;
extern const uint32_t SnapshotManager_Resolve_m1211847618_MetadataUsageId;
extern "C"  void SnapshotManager_Resolve_m1211847618 (SnapshotManager_t2359319983 * __this, NativeSnapshotMetadata_t3479575958 * ___metadata0, NativeSnapshotMetadataChange_t2885515174 * ___metadataChange1, String_t* ___conflictId2, Action_1_t3196978601 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_Resolve_m1211847618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeSnapshotMetadata_t3479575958 * L_0 = ___metadata0;
		Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var);
		NativeSnapshotMetadataChange_t2885515174 * L_1 = ___metadataChange1;
		Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadataChange_t2885515174_m3261945094_MethodInfo_var);
		String_t* L_2 = ___conflictId2;
		Misc_CheckNotNull_TisString_t_m699927801(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var);
		GameServices_t1862808700 * L_3 = __this->get_mServices_0();
		NullCheck(L_3);
		HandleRef_t1780819301  L_4 = GameServices_AsHandle_m4151855468(L_3, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_5 = ___metadata0;
		NullCheck(L_5);
		IntPtr_t L_6 = BaseReferenceHolder_AsPointer_m1925325420(L_5, /*hidden argument*/NULL);
		NativeSnapshotMetadataChange_t2885515174 * L_7 = ___metadataChange1;
		NullCheck(L_7);
		IntPtr_t L_8 = BaseReferenceHolder_AsPointer_m1925325420(L_7, /*hidden argument*/NULL);
		String_t* L_9 = ___conflictId2;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)SnapshotManager_InternalCommitCallback_m3500800666_MethodInfo_var);
		CommitCallback_t1623255043 * L_11 = (CommitCallback_t1623255043 *)il2cpp_codegen_object_new(CommitCallback_t1623255043_il2cpp_TypeInfo_var);
		CommitCallback__ctor_m1437284650(L_11, NULL, L_10, /*hidden argument*/NULL);
		Action_1_t3196978601 * L_12 = ___callback3;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)CommitResponse_FromPointer_m1676551783_MethodInfo_var);
		Func_2_t934982759 * L_14 = (Func_2_t934982759 *)il2cpp_codegen_object_new(Func_2_t934982759_il2cpp_TypeInfo_var);
		Func_2__ctor_m1644784025(L_14, NULL, L_13, /*hidden argument*/Func_2__ctor_m1644784025_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_15 = Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/Callbacks_ToIntPtr_TisCommitResponse_t2801162465_m1776637039_MethodInfo_var);
		SnapshotManager_SnapshotManager_ResolveConflict_m1052197715(NULL /*static, unused*/, L_4, L_6, L_8, L_9, L_11, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalCommitCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2615835990;
extern const uint32_t SnapshotManager_InternalCommitCallback_m3500800666_MetadataUsageId;
extern "C"  void SnapshotManager_InternalCommitCallback_m3500800666 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_InternalCommitCallback_m3500800666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral2615835990, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SnapshotManager_InternalCommitCallback_m3500800666(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	SnapshotManager_InternalCommitCallback_m3500800666(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Delete(GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata)
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var;
extern const uint32_t SnapshotManager_Delete_m2176805355_MetadataUsageId;
extern "C"  void SnapshotManager_Delete_m2176805355 (SnapshotManager_t2359319983 * __this, NativeSnapshotMetadata_t3479575958 * ___metadata0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_Delete_m2176805355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeSnapshotMetadata_t3479575958 * L_0 = ___metadata0;
		Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var);
		GameServices_t1862808700 * L_1 = __this->get_mServices_0();
		NullCheck(L_1);
		HandleRef_t1780819301  L_2 = GameServices_AsHandle_m4151855468(L_1, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_3 = ___metadata0;
		NullCheck(L_3);
		IntPtr_t L_4 = BaseReferenceHolder_AsPointer_m1925325420(L_3, /*hidden argument*/NULL);
		SnapshotManager_SnapshotManager_Delete_m1855694989(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::Read(GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata,System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>)
extern Il2CppClass* ReadCallback_t1569945378_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t426447878_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisAction_1_t2688443720_m1632032721_MethodInfo_var;
extern const MethodInfo* SnapshotManager_InternalReadCallback_m2304246491_MethodInfo_var;
extern const MethodInfo* ReadResponse_FromPointer_m1747665637_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3586507930_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisReadResponse_t2292627584_m1020558478_MethodInfo_var;
extern const uint32_t SnapshotManager_Read_m1399765778_MetadataUsageId;
extern "C"  void SnapshotManager_Read_m1399765778 (SnapshotManager_t2359319983 * __this, NativeSnapshotMetadata_t3479575958 * ___metadata0, Action_1_t2688443720 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_Read_m1399765778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NativeSnapshotMetadata_t3479575958 * L_0 = ___metadata0;
		Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisNativeSnapshotMetadata_t3479575958_m2369749878_MethodInfo_var);
		Action_1_t2688443720 * L_1 = ___callback1;
		Misc_CheckNotNull_TisAction_1_t2688443720_m1632032721(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t2688443720_m1632032721_MethodInfo_var);
		GameServices_t1862808700 * L_2 = __this->get_mServices_0();
		NullCheck(L_2);
		HandleRef_t1780819301  L_3 = GameServices_AsHandle_m4151855468(L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = ___metadata0;
		NullCheck(L_4);
		IntPtr_t L_5 = BaseReferenceHolder_AsPointer_m1925325420(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)SnapshotManager_InternalReadCallback_m2304246491_MethodInfo_var);
		ReadCallback_t1569945378 * L_7 = (ReadCallback_t1569945378 *)il2cpp_codegen_object_new(ReadCallback_t1569945378_il2cpp_TypeInfo_var);
		ReadCallback__ctor_m767491785(L_7, NULL, L_6, /*hidden argument*/NULL);
		Action_1_t2688443720 * L_8 = ___callback1;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)ReadResponse_FromPointer_m1747665637_MethodInfo_var);
		Func_2_t426447878 * L_10 = (Func_2_t426447878 *)il2cpp_codegen_object_new(Func_2_t426447878_il2cpp_TypeInfo_var);
		Func_2__ctor_m3586507930(L_10, NULL, L_9, /*hidden argument*/Func_2__ctor_m3586507930_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_11 = Callbacks_ToIntPtr_TisReadResponse_t2292627584_m1020558478(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/Callbacks_ToIntPtr_TisReadResponse_t2292627584_m1020558478_MethodInfo_var);
		SnapshotManager_SnapshotManager_Read_m3479849658(NULL /*static, unused*/, L_3, L_5, L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager::InternalReadCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1320699189;
extern const uint32_t SnapshotManager_InternalReadCallback_m2304246491_MetadataUsageId;
extern "C"  void SnapshotManager_InternalReadCallback_m2304246491 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotManager_InternalReadCallback_m2304246491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral1320699189, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SnapshotManager_InternalReadCallback_m2304246491(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	SnapshotManager_InternalReadCallback_m2304246491(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t CommitResponse__ctor_m2654479082_MetadataUsageId;
extern "C"  void CommitResponse__ctor_m2654479082 (CommitResponse_t2801162465 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CommitResponse__ctor_m2654479082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::ResponseStatus()
extern "C"  int32_t CommitResponse_ResponseStatus_m741189284 (CommitResponse_t2801162465 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = SnapshotManager_SnapshotManager_CommitResponse_GetStatus_m3492871084(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::RequestSucceeded()
extern "C"  bool CommitResponse_RequestSucceeded_m564461752 (CommitResponse_t2801162465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = CommitResponse_ResponseStatus_m741189284(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::Data()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4112577571;
extern const uint32_t CommitResponse_Data_m3067336382_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * CommitResponse_Data_m3067336382 (CommitResponse_t2801162465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CommitResponse_Data_m3067336382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = CommitResponse_RequestSucceeded_m564461752(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral4112577571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = SnapshotManager_SnapshotManager_CommitResponse_GetData_m901309080(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void CommitResponse_CallDispose_m1689989126 (CommitResponse_t2801162465 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		SnapshotManager_SnapshotManager_CommitResponse_Dispose_m173916844(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CommitResponse_t2801162465_il2cpp_TypeInfo_var;
extern const uint32_t CommitResponse_FromPointer_m1676551783_MetadataUsageId;
extern "C"  CommitResponse_t2801162465 * CommitResponse_FromPointer_m1676551783 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CommitResponse_FromPointer_m1676551783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (CommitResponse_t2801162465 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		CommitResponse_t2801162465 * L_5 = (CommitResponse_t2801162465 *)il2cpp_codegen_object_new(CommitResponse_t2801162465_il2cpp_TypeInfo_var);
		CommitResponse__ctor_m2654479082(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllResponse__ctor_m3458740730_MetadataUsageId;
extern "C"  void FetchAllResponse__ctor_m3458740730 (FetchAllResponse_t1240414705 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllResponse__ctor_m3458740730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::ResponseStatus()
extern "C"  int32_t FetchAllResponse_ResponseStatus_m115698676 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = SnapshotManager_SnapshotManager_FetchAllResponse_GetStatus_m279620540(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::RequestSucceeded()
extern "C"  bool FetchAllResponse_RequestSucceeded_m93376712 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = FetchAllResponse_ResponseStatus_m115698676(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata> GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::Data()
extern Il2CppClass* Func_2_t1609885161_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* FetchAllResponse_U3CDataU3Em__D2_m621860908_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3120573176_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerable_TisNativeSnapshotMetadata_t3479575958_m659440259_MethodInfo_var;
extern const uint32_t FetchAllResponse_Data_m3870451963_MetadataUsageId;
extern "C"  Il2CppObject* FetchAllResponse_Data_m3870451963 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllResponse_Data_m3870451963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = SnapshotManager_SnapshotManager_FetchAllResponse_GetData_Length_m2754352268(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)FetchAllResponse_U3CDataU3Em__D2_m621860908_MethodInfo_var);
		Func_2_t1609885161 * L_3 = (Func_2_t1609885161 *)il2cpp_codegen_object_new(Func_2_t1609885161_il2cpp_TypeInfo_var);
		Func_2__ctor_m3120573176(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3120573176_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerable_TisNativeSnapshotMetadata_t3479575958_m659440259(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisNativeSnapshotMetadata_t3479575958_m659440259_MethodInfo_var);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchAllResponse_CallDispose_m2705670902 (FetchAllResponse_t1240414705 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		SnapshotManager_SnapshotManager_FetchAllResponse_Dispose_m3323893884(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FetchAllResponse_t1240414705_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllResponse_FromPointer_m1786032583_MetadataUsageId;
extern "C"  FetchAllResponse_t1240414705 * FetchAllResponse_FromPointer_m1786032583 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllResponse_FromPointer_m1786032583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchAllResponse_t1240414705 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		FetchAllResponse_t1240414705 * L_5 = (FetchAllResponse_t1240414705 *)il2cpp_codegen_object_new(FetchAllResponse_t1240414705_il2cpp_TypeInfo_var);
		FetchAllResponse__ctor_m3458740730(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::<Data>m__D2(System.UIntPtr)
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllResponse_U3CDataU3Em__D2_m621860908_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * FetchAllResponse_U3CDataU3Em__D2_m621860908 (FetchAllResponse_t1240414705 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllResponse_U3CDataU3Em__D2_m621860908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index0;
		IntPtr_t L_2 = SnapshotManager_SnapshotManager_FetchAllResponse_GetData_GetElement_m2092587160(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_3 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t OpenResponse__ctor_m3956879613_MetadataUsageId;
extern "C"  void OpenResponse__ctor_m3956879613 (OpenResponse_t1933021492 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse__ctor_m3956879613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::RequestSucceeded()
extern "C"  bool OpenResponse_RequestSucceeded_m3967416971 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = OpenResponse_ResponseStatus_m3968836452(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ResponseStatus()
extern "C"  int32_t OpenResponse_ResponseStatus_m3968836452 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = SnapshotManager_SnapshotManager_OpenResponse_GetStatus_m356190316(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictId()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* OutStringMethod_t1062451542_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* OpenResponse_U3CConflictIdU3Em__D1_m3387186761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651567014;
extern const uint32_t OpenResponse_ConflictId_m3156091703_MetadataUsageId;
extern "C"  String_t* OpenResponse_ConflictId_m3156091703 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse_ConflictId_m3156091703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = OpenResponse_ResponseStatus_m3968836452(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3651567014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)OpenResponse_U3CConflictIdU3Em__D1_m3387186761_MethodInfo_var);
		OutStringMethod_t1062451542 * L_3 = (OutStringMethod_t1062451542 *)il2cpp_codegen_object_new(OutStringMethod_t1062451542_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2730868781(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		String_t* L_4 = PInvokeUtilities_OutParamsToString_m1562371957(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::Data()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2489149787;
extern const uint32_t OpenResponse_Data_m812595345_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * OpenResponse_Data_m812595345 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse_Data_m812595345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = OpenResponse_ResponseStatus_m3968836452(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral2489149787, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = SnapshotManager_SnapshotManager_OpenResponse_GetData_m2569209387(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictOriginal()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651567014;
extern const uint32_t OpenResponse_ConflictOriginal_m4155532650_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * OpenResponse_ConflictOriginal_m4155532650 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse_ConflictOriginal_m4155532650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = OpenResponse_ResponseStatus_m3968836452(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3651567014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = SnapshotManager_SnapshotManager_OpenResponse_GetConflictOriginal_m661418628(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::ConflictUnmerged()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651567014;
extern const uint32_t OpenResponse_ConflictUnmerged_m2768910750_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * OpenResponse_ConflictUnmerged_m2768910750 (OpenResponse_t1933021492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse_ConflictUnmerged_m2768910750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = OpenResponse_ResponseStatus_m3968836452(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3651567014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = SnapshotManager_SnapshotManager_OpenResponse_GetConflictUnmerged_m4226482872(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void OpenResponse_CallDispose_m1431269459 (OpenResponse_t1933021492 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		SnapshotManager_SnapshotManager_OpenResponse_Dispose_m2186024831(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OpenResponse_t1933021492_il2cpp_TypeInfo_var;
extern const uint32_t OpenResponse_FromPointer_m1911824973_MetadataUsageId;
extern "C"  OpenResponse_t1933021492 * OpenResponse_FromPointer_m1911824973 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenResponse_FromPointer_m1911824973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (OpenResponse_t1933021492 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		OpenResponse_t1933021492 * L_5 = (OpenResponse_t1933021492 *)il2cpp_codegen_object_new(OpenResponse_t1933021492_il2cpp_TypeInfo_var);
		OpenResponse__ctor_m3956879613(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse::<ConflictId>m__D1(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  OpenResponse_U3CConflictIdU3Em__D1_m3387186761 (OpenResponse_t1933021492 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_1 = ___out_string0;
		UIntPtr_t  L_2 = ___out_size1;
		UIntPtr_t  L_3 = SnapshotManager_SnapshotManager_OpenResponse_GetConflictId_m3386434098(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t ReadResponse__ctor_m1226479433_MetadataUsageId;
extern "C"  void ReadResponse__ctor_m1226479433 (ReadResponse_t2292627584 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadResponse__ctor_m1226479433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::ResponseStatus()
extern "C"  int32_t ReadResponse_ResponseStatus_m3911377987 (ReadResponse_t2292627584 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = SnapshotManager_SnapshotManager_CommitResponse_GetStatus_m3492871084(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::RequestSucceeded()
extern "C"  bool ReadResponse_RequestSucceeded_m4281893847 (ReadResponse_t2292627584 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ReadResponse_ResponseStatus_m3911377987(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// System.Byte[] GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::Data()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* OutMethod_1_t2402168711_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* ReadResponse_U3CDataU3Em__D3_m909704157_MethodInfo_var;
extern const MethodInfo* OutMethod_1__ctor_m2423716964_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4112577571;
extern const uint32_t ReadResponse_Data_m1888371449_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* ReadResponse_Data_m1888371449 (ReadResponse_t2292627584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadResponse_Data_m1888371449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ReadResponse_RequestSucceeded_m4281893847(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral4112577571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ReadResponse_U3CDataU3Em__D3_m909704157_MethodInfo_var);
		OutMethod_1_t2402168711 * L_3 = (OutMethod_1_t2402168711 *)il2cpp_codegen_object_new(OutMethod_1_t2402168711_il2cpp_TypeInfo_var);
		OutMethod_1__ctor_m2423716964(L_3, __this, L_2, /*hidden argument*/OutMethod_1__ctor_m2423716964_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_4 = PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802(NULL /*static, unused*/, L_3, /*hidden argument*/PInvokeUtilities_OutParamsToArray_TisByte_t2862609660_m2719423802_MethodInfo_var);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ReadResponse_CallDispose_m834732935 (ReadResponse_t2292627584 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		SnapshotManager_SnapshotManager_ReadResponse_Dispose_m2094027723(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadResponse_t2292627584_il2cpp_TypeInfo_var;
extern const uint32_t ReadResponse_FromPointer_m1747665637_MetadataUsageId;
extern "C"  ReadResponse_t2292627584 * ReadResponse_FromPointer_m1747665637 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadResponse_FromPointer_m1747665637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (ReadResponse_t2292627584 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		ReadResponse_t2292627584 * L_5 = (ReadResponse_t2292627584 *)il2cpp_codegen_object_new(ReadResponse_t2292627584_il2cpp_TypeInfo_var);
		ReadResponse__ctor_m1226479433(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse::<Data>m__D3(System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  ReadResponse_U3CDataU3Em__D3_m909704157 (ReadResponse_t2292627584 * __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_1 = ___out_bytes0;
		UIntPtr_t  L_2 = ___out_size1;
		UIntPtr_t  L_3 = SnapshotManager_SnapshotManager_ReadResponse_GetData_m2577370730(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t SnapshotSelectUIResponse__ctor_m839008807_MetadataUsageId;
extern "C"  void SnapshotSelectUIResponse__ctor_m839008807 (SnapshotSelectUIResponse_t2500674014 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotSelectUIResponse__ctor_m839008807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestStatus()
extern "C"  int32_t SnapshotSelectUIResponse_RequestStatus_m4165722402 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetStatus_m1545112284(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestSucceeded()
extern "C"  bool SnapshotSelectUIResponse_RequestSucceeded_m3140975669 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = SnapshotSelectUIResponse_RequestStatus_m4165722402(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::Data()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4112577571;
extern const uint32_t SnapshotSelectUIResponse_Data_m1876063291_MetadataUsageId;
extern "C"  NativeSnapshotMetadata_t3479575958 * SnapshotSelectUIResponse_Data_m1876063291 (SnapshotSelectUIResponse_t2500674014 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotSelectUIResponse_Data_m1876063291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SnapshotSelectUIResponse_RequestSucceeded_m3140975669(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral4112577571, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		HandleRef_t1780819301  L_2 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_3 = SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetData_m2615366741(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NativeSnapshotMetadata_t3479575958 * L_4 = (NativeSnapshotMetadata_t3479575958 *)il2cpp_codegen_object_new(NativeSnapshotMetadata_t3479575958_il2cpp_TypeInfo_var);
		NativeSnapshotMetadata__ctor_m4150137846(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotSelectUIResponse_CallDispose_m1535290473 (SnapshotSelectUIResponse_t2500674014 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_Dispose_m3717262889(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SnapshotSelectUIResponse_t2500674014_il2cpp_TypeInfo_var;
extern const uint32_t SnapshotSelectUIResponse_FromPointer_m2303338145_MetadataUsageId;
extern "C"  SnapshotSelectUIResponse_t2500674014 * SnapshotSelectUIResponse_FromPointer_m2303338145 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotSelectUIResponse_FromPointer_m2303338145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (SnapshotSelectUIResponse_t2500674014 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		SnapshotSelectUIResponse_t2500674014 * L_5 = (SnapshotSelectUIResponse_t2500674014 *)il2cpp_codegen_object_new(SnapshotSelectUIResponse_t2500674014_il2cpp_TypeInfo_var);
		SnapshotSelectUIResponse__ctor_m839008807(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.StatsManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern const MethodInfo* Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540_MethodInfo_var;
extern const uint32_t StatsManager__ctor_m4169733925_MetadataUsageId;
extern "C"  void StatsManager__ctor_m4169733925 (StatsManager_t4261398554 * __this, GameServices_t1862808700 * ___services0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StatsManager__ctor_m4169733925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GameServices_t1862808700 * L_0 = ___services0;
		GameServices_t1862808700 * L_1 = Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisGameServices_t1862808700_m1798792540_MethodInfo_var);
		__this->set_mServices_0(L_1);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.StatsManager::FetchForPlayer(System.Action`1<GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse>)
extern Il2CppClass* FetchForPlayerCallback_t1995639285_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3696007637_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisAction_1_t1663036183_m3554545026_MethodInfo_var;
extern const MethodInfo* StatsManager_InternalFetchForPlayerCallback_m548634438_MethodInfo_var;
extern const MethodInfo* FetchForPlayerResponse_FromPointer_m897028869_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3765806417_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisFetchForPlayerResponse_t1267220047_m1308064849_MethodInfo_var;
extern const uint32_t StatsManager_FetchForPlayer_m2924525119_MetadataUsageId;
extern "C"  void StatsManager_FetchForPlayer_m2924525119 (StatsManager_t4261398554 * __this, Action_1_t1663036183 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StatsManager_FetchForPlayer_m2924525119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1663036183 * L_0 = ___callback0;
		Misc_CheckNotNull_TisAction_1_t1663036183_m3554545026(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_1_t1663036183_m3554545026_MethodInfo_var);
		GameServices_t1862808700 * L_1 = __this->get_mServices_0();
		NullCheck(L_1);
		HandleRef_t1780819301  L_2 = GameServices_AsHandle_m4151855468(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)StatsManager_InternalFetchForPlayerCallback_m548634438_MethodInfo_var);
		FetchForPlayerCallback_t1995639285 * L_4 = (FetchForPlayerCallback_t1995639285 *)il2cpp_codegen_object_new(FetchForPlayerCallback_t1995639285_il2cpp_TypeInfo_var);
		FetchForPlayerCallback__ctor_m2851840332(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t1663036183 * L_5 = ___callback0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)FetchForPlayerResponse_FromPointer_m897028869_MethodInfo_var);
		Func_2_t3696007637 * L_7 = (Func_2_t3696007637 *)il2cpp_codegen_object_new(Func_2_t3696007637_il2cpp_TypeInfo_var);
		Func_2__ctor_m3765806417(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3765806417_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_8 = Callbacks_ToIntPtr_TisFetchForPlayerResponse_t1267220047_m1308064849(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/Callbacks_ToIntPtr_TisFetchForPlayerResponse_t1267220047_m1308064849_MethodInfo_var);
		StatsManager_StatsManager_FetchForPlayer_m163136903(NULL /*static, unused*/, L_2, 1, L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.StatsManager::InternalFetchForPlayerCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral265871367;
extern const uint32_t StatsManager_InternalFetchForPlayerCallback_m548634438_MetadataUsageId;
extern "C"  void StatsManager_InternalFetchForPlayerCallback_m548634438 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StatsManager_InternalFetchForPlayerCallback_m548634438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral265871367, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_StatsManager_InternalFetchForPlayerCallback_m548634438(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	StatsManager_InternalFetchForPlayerCallback_m548634438(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t FetchForPlayerResponse__ctor_m955109544_MetadataUsageId;
extern "C"  void FetchForPlayerResponse__ctor_m955109544 (FetchForPlayerResponse_t1267220047 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchForPlayerResponse__ctor_m955109544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::Status()
extern "C"  int32_t FetchForPlayerResponse_Status_m89763059 (FetchForPlayerResponse_t1267220047 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = StatsManager_StatsManager_FetchForPlayerResponse_GetStatus_m333399849(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.NativePlayerStats GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::PlayerStats()
extern Il2CppClass* NativePlayerStats_t1419601613_il2cpp_TypeInfo_var;
extern const uint32_t FetchForPlayerResponse_PlayerStats_m3492990287_MetadataUsageId;
extern "C"  NativePlayerStats_t1419601613 * FetchForPlayerResponse_PlayerStats_m3492990287 (FetchForPlayerResponse_t1267220047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchForPlayerResponse_PlayerStats_m3492990287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = StatsManager_StatsManager_FetchForPlayerResponse_GetData_m3796184149(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		NativePlayerStats_t1419601613 * L_3 = (NativePlayerStats_t1419601613 *)il2cpp_codegen_object_new(NativePlayerStats_t1419601613_il2cpp_TypeInfo_var);
		NativePlayerStats__ctor_m248646333(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchForPlayerResponse_CallDispose_m2311238152 (FetchForPlayerResponse_t1267220047 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		StatsManager_StatsManager_FetchForPlayerResponse_Dispose_m2997693225(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FetchForPlayerResponse_t1267220047_il2cpp_TypeInfo_var;
extern const uint32_t FetchForPlayerResponse_FromPointer_m897028869_MetadataUsageId;
extern "C"  FetchForPlayerResponse_t1267220047 * FetchForPlayerResponse_FromPointer_m897028869 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchForPlayerResponse_FromPointer_m897028869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (FetchForPlayerResponse_t1267220047 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		FetchForPlayerResponse_t1267220047 * L_5 = (FetchForPlayerResponse_t1267220047 *)il2cpp_codegen_object_new(FetchForPlayerResponse_t1267220047_il2cpp_TypeInfo_var);
		FetchForPlayerResponse__ctor_m955109544(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C"  void TurnBasedManager__ctor_m3655774894 (TurnBasedManager_t3476156963 * __this, GameServices_t1862808700 * ___services0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GameServices_t1862808700 * L_0 = ___services0;
		__this->set_mGameServices_0(L_0);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::GetMatch(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_GetMatch_m1680948713_MetadataUsageId;
extern "C"  void TurnBasedManager_GetMatch_m1680948713 (TurnBasedManager_t3476156963 * __this, String_t* ___matchId0, Action_1_t356501165 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_GetMatch_m1680948713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___matchId0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_4 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_4, NULL, L_3, /*hidden argument*/NULL);
		Action_1_t356501165 * L_5 = ___callback1;
		IntPtr_t L_6 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatch_m2638862366(NULL /*static, unused*/, L_1, L_2, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalTurnBasedMatchCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3597167389;
extern const uint32_t TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MetadataUsageId;
extern "C"  void TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral3597167389, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::CreateMatch(GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_CreateMatch_m135186159_MetadataUsageId;
extern "C"  void TurnBasedManager_CreateMatch_m135186159 (TurnBasedManager_t3476156963 * __this, TurnBasedMatchConfig_t3069007197 * ___config0, Action_1_t356501165 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_CreateMatch_m135186159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		TurnBasedMatchConfig_t3069007197 * L_2 = ___config0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_5 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t356501165 * L_6 = ___callback1;
		IntPtr_t L_7 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CreateTurnBasedMatch_m4112807276(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ShowPlayerSelectUI(System.UInt32,System.UInt32,System.Boolean,System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>)
extern Il2CppClass* PlayerSelectUICallback_t4277124957_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3351189444_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalPlayerSelectUIcallback_m776827772_MethodInfo_var;
extern const MethodInfo* PlayerSelectUIResponse_FromPointer_m2059843535_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1475767909_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisPlayerSelectUIResponse_t922401854_m3315070755_MethodInfo_var;
extern const uint32_t TurnBasedManager_ShowPlayerSelectUI_m2349326231_MetadataUsageId;
extern "C"  void TurnBasedManager_ShowPlayerSelectUI_m2349326231 (TurnBasedManager_t3476156963 * __this, uint32_t ___minimumPlayers0, uint32_t ___maxiumPlayers1, bool ___allowAutomatching2, Action_1_t1318217990 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_ShowPlayerSelectUI_m2349326231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		uint32_t L_2 = ___minimumPlayers0;
		uint32_t L_3 = ___maxiumPlayers1;
		bool L_4 = ___allowAutomatching2;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TurnBasedManager_InternalPlayerSelectUIcallback_m776827772_MethodInfo_var);
		PlayerSelectUICallback_t4277124957 * L_6 = (PlayerSelectUICallback_t4277124957 *)il2cpp_codegen_object_new(PlayerSelectUICallback_t4277124957_il2cpp_TypeInfo_var);
		PlayerSelectUICallback__ctor_m788752260(L_6, NULL, L_5, /*hidden argument*/NULL);
		Action_1_t1318217990 * L_7 = ___callback3;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)PlayerSelectUIResponse_FromPointer_m2059843535_MethodInfo_var);
		Func_2_t3351189444 * L_9 = (Func_2_t3351189444 *)il2cpp_codegen_object_new(Func_2_t3351189444_il2cpp_TypeInfo_var);
		Func_2__ctor_m1475767909(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m1475767909_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_10 = Callbacks_ToIntPtr_TisPlayerSelectUIResponse_t922401854_m3315070755(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/Callbacks_ToIntPtr_TisPlayerSelectUIResponse_t922401854_m3315070755_MethodInfo_var);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowPlayerSelectUI_m3419203364(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_6, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalPlayerSelectUIcallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral583625314;
extern const uint32_t TurnBasedManager_InternalPlayerSelectUIcallback_m776827772_MetadataUsageId;
extern "C"  void TurnBasedManager_InternalPlayerSelectUIcallback_m776827772 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_InternalPlayerSelectUIcallback_m776827772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral583625314, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TurnBasedManager_InternalPlayerSelectUIcallback_m776827772(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	TurnBasedManager_InternalPlayerSelectUIcallback_m776827772(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::GetAllTurnbasedMatches(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>)
extern Il2CppClass* TurnBasedMatchesCallback_t1856996905_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1593942809_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432_MethodInfo_var;
extern const MethodInfo* TurnBasedMatchesResponse_FromPointer_m2393966405_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m972888141_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisTurnBasedMatchesResponse_t3460122515_m954953237_MethodInfo_var;
extern const uint32_t TurnBasedManager_GetAllTurnbasedMatches_m1814050818_MetadataUsageId;
extern "C"  void TurnBasedManager_GetAllTurnbasedMatches_m1814050818 (TurnBasedManager_t3476156963 * __this, Action_1_t3855938651 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_GetAllTurnbasedMatches_m1814050818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432_MethodInfo_var);
		TurnBasedMatchesCallback_t1856996905 * L_3 = (TurnBasedMatchesCallback_t1856996905 *)il2cpp_codegen_object_new(TurnBasedMatchesCallback_t1856996905_il2cpp_TypeInfo_var);
		TurnBasedMatchesCallback__ctor_m1891245648(L_3, NULL, L_2, /*hidden argument*/NULL);
		Action_1_t3855938651 * L_4 = ___callback0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TurnBasedMatchesResponse_FromPointer_m2393966405_MethodInfo_var);
		Func_2_t1593942809 * L_6 = (Func_2_t1593942809 *)il2cpp_codegen_object_new(Func_2_t1593942809_il2cpp_TypeInfo_var);
		Func_2__ctor_m972888141(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m972888141_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_TisTurnBasedMatchesResponse_t3460122515_m954953237(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/Callbacks_ToIntPtr_TisTurnBasedMatchesResponse_t3460122515_m954953237_MethodInfo_var);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatches_m3205772926(NULL /*static, unused*/, L_1, L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalTurnBasedMatchesCallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral46826478;
extern const uint32_t TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432_MetadataUsageId;
extern "C"  void TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral46826478, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	TurnBasedManager_InternalTurnBasedMatchesCallback_m302844432(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::AcceptInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2460869925;
extern const uint32_t TurnBasedManager_AcceptInvitation_m870982733_MetadataUsageId;
extern "C"  void TurnBasedManager_AcceptInvitation_m870982733 (TurnBasedManager_t3476156963 * __this, MultiplayerInvitation_t3411188537 * ___invitation0, Action_1_t356501165 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_AcceptInvitation_m870982733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MultiplayerInvitation_t3411188537 * L_0 = ___invitation0;
		NullCheck(L_0);
		IntPtr_t L_1 = BaseReferenceHolder_AsPointer_m1925325420(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int64_t L_2 = IntPtr_ToInt64_m2695254659((&V_0), /*hidden argument*/NULL);
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2460869925, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		GameServices_t1862808700 * L_6 = __this->get_mGameServices_0();
		NullCheck(L_6);
		HandleRef_t1780819301  L_7 = GameServices_AsHandle_m4151855468(L_6, /*hidden argument*/NULL);
		MultiplayerInvitation_t3411188537 * L_8 = ___invitation0;
		NullCheck(L_8);
		IntPtr_t L_9 = BaseReferenceHolder_AsPointer_m1925325420(L_8, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_11 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_11, NULL, L_10, /*hidden argument*/NULL);
		Action_1_t356501165 * L_12 = ___callback1;
		IntPtr_t L_13 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_AcceptInvitation_m3355169462(NULL /*static, unused*/, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::DeclineInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C"  void TurnBasedManager_DeclineInvitation_m2056111534 (TurnBasedManager_t3476156963 * __this, MultiplayerInvitation_t3411188537 * ___invitation0, const MethodInfo* method)
{
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		MultiplayerInvitation_t3411188537 * L_2 = ___invitation0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DeclineInvitation_m2784931045(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::TakeTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Byte[],GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_TakeTurn_m2577773198_MetadataUsageId;
extern "C"  void TurnBasedManager_TakeTurn_m2577773198 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, ByteU5BU5D_t4260760469* ___data1, MultiplayerParticipant_t3337232325 * ___nextParticipant2, Action_1_t356501165 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_TakeTurn_m2577773198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_4 = ___data1;
		ByteU5BU5D_t4260760469* L_5 = ___data1;
		NullCheck(L_5);
		UIntPtr_t  L_6;
		memset(&L_6, 0, sizeof(L_6));
		UIntPtr__ctor_m3952297501(&L_6, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_7 = ___match0;
		NullCheck(L_7);
		ParticipantResults_t1948656911 * L_8 = NativeTurnBasedMatch_Results_m3731659715(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		IntPtr_t L_9 = BaseReferenceHolder_AsPointer_m1925325420(L_8, /*hidden argument*/NULL);
		MultiplayerParticipant_t3337232325 * L_10 = ___nextParticipant2;
		NullCheck(L_10);
		IntPtr_t L_11 = BaseReferenceHolder_AsPointer_m1925325420(L_10, /*hidden argument*/NULL);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_13 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_13, NULL, L_12, /*hidden argument*/NULL);
		Action_1_t356501165 * L_14 = ___callback3;
		IntPtr_t L_15 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TakeMyTurn_m1018634945(NULL /*static, unused*/, L_1, L_3, L_4, L_6, L_9, L_11, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalMatchInboxUICallback(System.IntPtr,System.IntPtr)
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3370458342;
extern const uint32_t TurnBasedManager_InternalMatchInboxUICallback_m3282582168_MetadataUsageId;
extern "C"  void TurnBasedManager_InternalMatchInboxUICallback_m3282582168 (Il2CppObject * __this /* static, unused */, IntPtr_t ___response0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_InternalMatchInboxUICallback_m3282582168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___response0;
		IntPtr_t L_1 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Callbacks_PerformInternalCallback_m2585278875(NULL /*static, unused*/, _stringLiteral3370458342, 1, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TurnBasedManager_InternalMatchInboxUICallback_m3282582168(intptr_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg00' to managed representation
	IntPtr_t ____arg00_unmarshaled;
	____arg00_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg00));

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	TurnBasedManager_InternalMatchInboxUICallback_m3282582168(NULL, ____arg00_unmarshaled, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ShowInboxUI(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>)
extern Il2CppClass* MatchInboxUICallback_t3070391457_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3888150673_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalMatchInboxUICallback_m3282582168_MethodInfo_var;
extern const MethodInfo* MatchInboxUIResponse_FromPointer_m3118728645_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1702741845_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisMatchInboxUIResponse_t1459363083_m4000897165_MethodInfo_var;
extern const uint32_t TurnBasedManager_ShowInboxUI_m496073385_MetadataUsageId;
extern "C"  void TurnBasedManager_ShowInboxUI_m496073385 (TurnBasedManager_t3476156963 * __this, Action_1_t1855179219 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_ShowInboxUI_m496073385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedManager_InternalMatchInboxUICallback_m3282582168_MethodInfo_var);
		MatchInboxUICallback_t3070391457 * L_3 = (MatchInboxUICallback_t3070391457 *)il2cpp_codegen_object_new(MatchInboxUICallback_t3070391457_il2cpp_TypeInfo_var);
		MatchInboxUICallback__ctor_m2733434056(L_3, NULL, L_2, /*hidden argument*/NULL);
		Action_1_t1855179219 * L_4 = ___callback0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)MatchInboxUIResponse_FromPointer_m3118728645_MethodInfo_var);
		Func_2_t3888150673 * L_6 = (Func_2_t3888150673 *)il2cpp_codegen_object_new(Func_2_t3888150673_il2cpp_TypeInfo_var);
		Func_2__ctor_m1702741845(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m1702741845_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_TisMatchInboxUIResponse_t1459363083_m4000897165(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/Callbacks_ToIntPtr_TisMatchInboxUIResponse_t1459363083_m4000897165_MethodInfo_var);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowMatchInboxUI_m2424059789(NULL /*static, unused*/, L_1, L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalMultiplayerStatusCallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern Il2CppClass* MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Callbacks_IntPtrToTempCallback_TisAction_1_t3071188627_m1604055793_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m1122706011_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral534453274;
extern Il2CppCodeGenString* _stringLiteral322640687;
extern const uint32_t TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MetadataUsageId;
extern "C"  void TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482 (Il2CppObject * __this /* static, unused */, int32_t ___status0, IntPtr_t ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t3071188627 * V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___status0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral534453274, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ___data1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		Action_1_t3071188627 * L_5 = Callbacks_IntPtrToTempCallback_TisAction_1_t3071188627_m1604055793(NULL /*static, unused*/, L_4, /*hidden argument*/Callbacks_IntPtrToTempCallback_TisAction_1_t3071188627_m1604055793_MethodInfo_var);
		V_0 = L_5;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		Action_1_t3071188627 * L_6 = V_0;
		int32_t L_7 = ___status0;
		NullCheck(L_6);
		Action_1_Invoke_m1122706011(L_6, L_7, /*hidden argument*/Action_1_Invoke_m1122706011_MethodInfo_var);
		goto IL_003e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0028;
		throw e;
	}

CATCH_0028:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t3991598821 *)__exception_local);
		Exception_t3991598821 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral322640687, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_003e;
	} // end catch (depth: 1)

IL_003e:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482(int32_t ___arg00, intptr_t ___arg11)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___arg11' to managed representation
	IntPtr_t ____arg11_unmarshaled;
	____arg11_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)___arg11));

	// Managed method invocation
	TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482(NULL, ___arg00, ____arg11_unmarshaled, NULL);

}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::LeaveDuringMyTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern Il2CppClass* MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var;
extern const uint32_t TurnBasedManager_LeaveDuringMyTurn_m1467675284_MetadataUsageId;
extern "C"  void TurnBasedManager_LeaveDuringMyTurn_m1467675284 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, MultiplayerParticipant_t3337232325 * ___nextParticipant1, Action_1_t3071188627 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_LeaveDuringMyTurn_m1467675284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		MultiplayerParticipant_t3337232325 * L_4 = ___nextParticipant1;
		NullCheck(L_4);
		IntPtr_t L_5 = BaseReferenceHolder_AsPointer_m1925325420(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var);
		MultiplayerStatusCallback_t994271114 * L_7 = (MultiplayerStatusCallback_t994271114 *)il2cpp_codegen_object_new(MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var);
		MultiplayerStatusCallback__ctor_m2373958241(L_7, NULL, L_6, /*hidden argument*/NULL);
		Action_1_t3071188627 * L_8 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_9 = Callbacks_ToIntPtr_m2396808395(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn_m2337901432(NULL /*static, unused*/, L_1, L_3, L_5, L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::FinishMatchDuringMyTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Byte[],GooglePlayGames.Native.PInvoke.ParticipantResults,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_FinishMatchDuringMyTurn_m1438843344_MetadataUsageId;
extern "C"  void TurnBasedManager_FinishMatchDuringMyTurn_m1438843344 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, ByteU5BU5D_t4260760469* ___data1, ParticipantResults_t1948656911 * ___results2, Action_1_t356501165 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_FinishMatchDuringMyTurn_m1438843344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_4 = ___data1;
		ByteU5BU5D_t4260760469* L_5 = ___data1;
		NullCheck(L_5);
		UIntPtr_t  L_6;
		memset(&L_6, 0, sizeof(L_6));
		UIntPtr__ctor_m3952297501(&L_6, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		ParticipantResults_t1948656911 * L_7 = ___results2;
		NullCheck(L_7);
		IntPtr_t L_8 = BaseReferenceHolder_AsPointer_m1925325420(L_7, /*hidden argument*/NULL);
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_10 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_10, NULL, L_9, /*hidden argument*/NULL);
		Action_1_t356501165 * L_11 = ___callback3;
		IntPtr_t L_12 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FinishMatchDuringMyTurn_m2572900911(NULL /*static, unused*/, L_1, L_3, L_4, L_6, L_8, L_10, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ConfirmPendingCompletion(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_ConfirmPendingCompletion_m138577292_MetadataUsageId;
extern "C"  void TurnBasedManager_ConfirmPendingCompletion_m138577292 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, Action_1_t356501165 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_ConfirmPendingCompletion_m138577292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_5 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t356501165 * L_6 = ___callback1;
		IntPtr_t L_7 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ConfirmPendingCompletion_m3075365316(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::LeaveMatchDuringTheirTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern Il2CppClass* MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var;
extern const uint32_t TurnBasedManager_LeaveMatchDuringTheirTurn_m2950730155_MetadataUsageId;
extern "C"  void TurnBasedManager_LeaveMatchDuringTheirTurn_m2950730155 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, Action_1_t3071188627 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_LeaveMatchDuringTheirTurn_m2950730155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var);
		MultiplayerStatusCallback_t994271114 * L_5 = (MultiplayerStatusCallback_t994271114 *)il2cpp_codegen_object_new(MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var);
		MultiplayerStatusCallback__ctor_m2373958241(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t3071188627 * L_6 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_m2396808395(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn_m1374199278(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::CancelMatch(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern Il2CppClass* MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var;
extern const uint32_t TurnBasedManager_CancelMatch_m3451460942_MetadataUsageId;
extern "C"  void TurnBasedManager_CancelMatch_m3451460942 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, Action_1_t3071188627 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_CancelMatch_m3451460942_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TurnBasedManager_InternalMultiplayerStatusCallback_m4279382482_MethodInfo_var);
		MultiplayerStatusCallback_t994271114 * L_5 = (MultiplayerStatusCallback_t994271114 *)il2cpp_codegen_object_new(MultiplayerStatusCallback_t994271114_il2cpp_TypeInfo_var);
		MultiplayerStatusCallback__ctor_m2373958241(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t3071188627 * L_6 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Callbacks_ToIntPtr_m2396808395(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CancelMatch_m3919581137(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::Rematch(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var;
extern const uint32_t TurnBasedManager_Rematch_m3969723153_MetadataUsageId;
extern "C"  void TurnBasedManager_Rematch_m3969723153 (TurnBasedManager_t3476156963 * __this, NativeTurnBasedMatch_t302853426 * ___match0, Action_1_t356501165 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_Rematch_m3969723153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameServices_t1862808700 * L_0 = __this->get_mGameServices_0();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = GameServices_AsHandle_m4151855468(L_0, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_2 = ___match0;
		NullCheck(L_2);
		IntPtr_t L_3 = BaseReferenceHolder_AsPointer_m1925325420(L_2, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TurnBasedManager_InternalTurnBasedMatchCallback_m2292120190_MethodInfo_var);
		TurnBasedMatchCallback_t3800683771 * L_5 = (TurnBasedMatchCallback_t3800683771 *)il2cpp_codegen_object_new(TurnBasedMatchCallback_t3800683771_il2cpp_TypeInfo_var);
		TurnBasedMatchCallback__ctor_m3012265506(L_5, NULL, L_4, /*hidden argument*/NULL);
		Action_1_t356501165 * L_6 = ___callback1;
		IntPtr_t L_7 = TurnBasedManager_ToCallbackPointer_m2878361038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_Rematch_m1408642129(NULL /*static, unused*/, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr GooglePlayGames.Native.PInvoke.TurnBasedManager::ToCallbackPointer(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern Il2CppClass* Func_2_t2389472619_il2cpp_TypeInfo_var;
extern Il2CppClass* Callbacks_t3420130036_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchResponse_FromPointer_m863377605_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4189394427_MethodInfo_var;
extern const MethodInfo* Callbacks_ToIntPtr_TisTurnBasedMatchResponse_t4255652325_m2749825255_MethodInfo_var;
extern const uint32_t TurnBasedManager_ToCallbackPointer_m2878361038_MetadataUsageId;
extern "C"  IntPtr_t TurnBasedManager_ToCallbackPointer_m2878361038 (Il2CppObject * __this /* static, unused */, Action_1_t356501165 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedManager_ToCallbackPointer_m2878361038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t356501165 * L_0 = ___callback0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)TurnBasedMatchResponse_FromPointer_m863377605_MethodInfo_var);
		Func_2_t2389472619 * L_2 = (Func_2_t2389472619 *)il2cpp_codegen_object_new(Func_2_t2389472619_il2cpp_TypeInfo_var);
		Func_2__ctor_m4189394427(L_2, NULL, L_1, /*hidden argument*/Func_2__ctor_m4189394427_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t3420130036_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Callbacks_ToIntPtr_TisTurnBasedMatchResponse_t4255652325_m2749825255(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/Callbacks_ToIntPtr_TisTurnBasedMatchResponse_t4255652325_m2749825255_MethodInfo_var);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t MatchInboxUIResponse__ctor_m1324327012_MetadataUsageId;
extern "C"  void MatchInboxUIResponse__ctor_m1324327012 (MatchInboxUIResponse_t1459363083 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MatchInboxUIResponse__ctor_m1324327012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::UiStatus()
extern "C"  int32_t MatchInboxUIResponse_UiStatus_m4059168656 (MatchInboxUIResponse_t1459363083 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus_m1413420669(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::Match()
extern Il2CppClass* NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var;
extern const uint32_t MatchInboxUIResponse_Match_m1963052201_MetadataUsageId;
extern "C"  NativeTurnBasedMatch_t302853426 * MatchInboxUIResponse_Match_m1963052201 (MatchInboxUIResponse_t1459363083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MatchInboxUIResponse_Match_m1963052201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = MatchInboxUIResponse_UiStatus_m4059168656(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		return (NativeTurnBasedMatch_t302853426 *)NULL;
	}

IL_000e:
	{
		HandleRef_t1780819301  L_1 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch_m2844274293(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_3 = (NativeTurnBasedMatch_t302853426 *)il2cpp_codegen_object_new(NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2483317906(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MatchInboxUIResponse_CallDispose_m2328545740 (MatchInboxUIResponse_t1459363083 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose_m4276676362(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MatchInboxUIResponse_t1459363083_il2cpp_TypeInfo_var;
extern const uint32_t MatchInboxUIResponse_FromPointer_m3118728645_MetadataUsageId;
extern "C"  MatchInboxUIResponse_t1459363083 * MatchInboxUIResponse_FromPointer_m3118728645 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MatchInboxUIResponse_FromPointer_m3118728645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (MatchInboxUIResponse_t1459363083 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		MatchInboxUIResponse_t1459363083 * L_5 = (MatchInboxUIResponse_t1459363083 *)il2cpp_codegen_object_new(MatchInboxUIResponse_t1459363083_il2cpp_TypeInfo_var);
		MatchInboxUIResponse__ctor_m1324327012(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void TurnBasedMatchCallback__ctor_m2458752016 (TurnBasedMatchCallback_t128787945 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::Invoke(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void TurnBasedMatchCallback_Invoke_m202103397 (TurnBasedMatchCallback_t128787945 * __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TurnBasedMatchCallback_Invoke_m202103397((TurnBasedMatchCallback_t128787945 *)__this->get_prev_9(),___response0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::BeginInvoke(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TurnBasedMatchCallback_BeginInvoke_m721848842 (TurnBasedMatchCallback_t128787945 * __this, TurnBasedMatchResponse_t4255652325 * ___response0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___response0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::EndInvoke(System.IAsyncResult)
extern "C"  void TurnBasedMatchCallback_EndInvoke_m2355990048 (TurnBasedMatchCallback_t128787945 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse__ctor_m1747058924_MetadataUsageId;
extern "C"  void TurnBasedMatchesResponse__ctor_m1747058924 (TurnBasedMatchesResponse_t3460122515 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse__ctor_m1747058924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchesResponse_CallDispose_m1111792196 (TurnBasedMatchesResponse_t3460122515 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose_m4225394450(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Status()
extern "C"  int32_t TurnBasedMatchesResponse_Status_m2039759030 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus_m3228912179(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Invitations()
extern Il2CppClass* Func_2_t1541497740_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m474110117_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerable_TisMultiplayerInvitation_t3411188537_m3316655632_MethodInfo_var;
extern const uint32_t TurnBasedMatchesResponse_Invitations_m4255789128_MetadataUsageId;
extern "C"  Il2CppObject* TurnBasedMatchesResponse_Invitations_m4255789128 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_Invitations_m4255789128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length_m1819430028(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873_MethodInfo_var);
		Func_2_t1541497740 * L_3 = (Func_2_t1541497740 *)il2cpp_codegen_object_new(Func_2_t1541497740_il2cpp_TypeInfo_var);
		Func_2__ctor_m474110117(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m474110117_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerable_TisMultiplayerInvitation_t3411188537_m3316655632(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisMultiplayerInvitation_t3411188537_m3316655632_MethodInfo_var);
		return L_4;
	}
}
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::InvitationCount()
extern "C"  int32_t TurnBasedMatchesResponse_InvitationCount_m1396940782 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	UIntPtr_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length_m1819430028(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3683901238((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::MyTurnMatches()
extern Il2CppClass* Func_2_t2728129925_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2867199708_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var;
extern const uint32_t TurnBasedMatchesResponse_MyTurnMatches_m3551672251_MetadataUsageId;
extern "C"  Il2CppObject* TurnBasedMatchesResponse_MyTurnMatches_m3551672251 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_MyTurnMatches_m3551672251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length_m3099920668(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111_MethodInfo_var);
		Func_2_t2728129925 * L_3 = (Func_2_t2728129925 *)il2cpp_codegen_object_new(Func_2_t2728129925_il2cpp_TypeInfo_var);
		Func_2__ctor_m2867199708(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m2867199708_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var);
		return L_4;
	}
}
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::MyTurnMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_MyTurnMatchesCount_m706481007 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	UIntPtr_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length_m3099920668(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3683901238((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::TheirTurnMatches()
extern Il2CppClass* Func_2_t2728129925_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2867199708_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var;
extern const uint32_t TurnBasedMatchesResponse_TheirTurnMatches_m2855471021_MetadataUsageId;
extern "C"  Il2CppObject* TurnBasedMatchesResponse_TheirTurnMatches_m2855471021 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_TheirTurnMatches_m2855471021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length_m3060513636(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584_MethodInfo_var);
		Func_2_t2728129925 * L_3 = (Func_2_t2728129925 *)il2cpp_codegen_object_new(Func_2_t2728129925_il2cpp_TypeInfo_var);
		Func_2__ctor_m2867199708(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m2867199708_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var);
		return L_4;
	}
}
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::TheirTurnMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_TheirTurnMatchesCount_m4021892267 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	UIntPtr_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length_m3060513636(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3683901238((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CompletedMatches()
extern Il2CppClass* Func_2_t2728129925_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2867199708_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var;
extern const uint32_t TurnBasedMatchesResponse_CompletedMatches_m3154803929_MetadataUsageId;
extern "C"  Il2CppObject* TurnBasedMatchesResponse_CompletedMatches_m3154803929 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_CompletedMatches_m3154803929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length_m42170552(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541_MethodInfo_var);
		Func_2_t2728129925 * L_3 = (Func_2_t2728129925 *)il2cpp_codegen_object_new(Func_2_t2728129925_il2cpp_TypeInfo_var);
		Func_2__ctor_m2867199708(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m2867199708_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerable_TisNativeTurnBasedMatch_t302853426_m554296167_MethodInfo_var);
		return L_4;
	}
}
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CompletedMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_CompletedMatchesCount_m1582832383 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method)
{
	UIntPtr_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length_m42170552(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		uint32_t L_2 = UIntPtr_ToUInt32_m3683901238((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::FromPointer(System.IntPtr)
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern Il2CppClass* TurnBasedMatchesResponse_t3460122515_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse_FromPointer_m2393966405_MetadataUsageId;
extern "C"  TurnBasedMatchesResponse_t3460122515 * TurnBasedMatchesResponse_FromPointer_m2393966405 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_FromPointer_m2393966405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pointer0;
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		bool L_1 = PInvokeUtilities_IsNull_m516106713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (TurnBasedMatchesResponse_t3460122515 *)NULL;
	}

IL_000d:
	{
		IntPtr_t L_2 = ___pointer0;
		TurnBasedMatchesResponse_t3460122515 * L_3 = (TurnBasedMatchesResponse_t3460122515 *)il2cpp_codegen_object_new(TurnBasedMatchesResponse_t3460122515_il2cpp_TypeInfo_var);
		TurnBasedMatchesResponse__ctor_m1747058924(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<Invitations>m__D4(System.UIntPtr)
extern Il2CppClass* MultiplayerInvitation_t3411188537_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873_MetadataUsageId;
extern "C"  MultiplayerInvitation_t3411188537 * TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index0;
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement_m1846925266(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		MultiplayerInvitation_t3411188537 * L_3 = (MultiplayerInvitation_t3411188537 *)il2cpp_codegen_object_new(MultiplayerInvitation_t3411188537_il2cpp_TypeInfo_var);
		MultiplayerInvitation__ctor_m1420177961(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<MyTurnMatches>m__D5(System.UIntPtr)
extern Il2CppClass* NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111_MetadataUsageId;
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index0;
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement_m149252610(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_3 = (NativeTurnBasedMatch_t302853426 *)il2cpp_codegen_object_new(NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2483317906(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<TheirTurnMatches>m__D6(System.UIntPtr)
extern Il2CppClass* NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584_MetadataUsageId;
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index0;
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement_m979554880(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_3 = (NativeTurnBasedMatch_t302853426 *)il2cpp_codegen_object_new(NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2483317906(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<CompletedMatches>m__D7(System.UIntPtr)
extern Il2CppClass* NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541_MetadataUsageId;
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = ___index0;
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement_m759843948(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_3 = (NativeTurnBasedMatch_t302853426 *)il2cpp_codegen_object_new(NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2483317906(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchResponse__ctor_m3633152958_MetadataUsageId;
extern "C"  void TurnBasedMatchResponse__ctor_m3633152958 (TurnBasedMatchResponse_t4255652325 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchResponse__ctor_m3633152958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::ResponseStatus()
extern "C"  int32_t TurnBasedMatchResponse_ResponseStatus_m2592316137 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int32_t L_1 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus_m2848141765(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::RequestSucceeded()
extern "C"  bool TurnBasedMatchResponse_RequestSucceeded_m627747956 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TurnBasedMatchResponse_ResponseStatus_m2592316137(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
	}
}
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::Match()
extern Il2CppClass* NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchResponse_Match_m2858914191_MetadataUsageId;
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchResponse_Match_m2858914191 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchResponse_Match_m2858914191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = TurnBasedMatchResponse_RequestSucceeded_m627747956(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (NativeTurnBasedMatch_t302853426 *)NULL;
	}

IL_000d:
	{
		HandleRef_t1780819301  L_1 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch_m557914715(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NativeTurnBasedMatch_t302853426 * L_3 = (NativeTurnBasedMatch_t302853426 *)il2cpp_codegen_object_new(NativeTurnBasedMatch_t302853426_il2cpp_TypeInfo_var);
		NativeTurnBasedMatch__ctor_m2483317906(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchResponse_CallDispose_m283130290 (TurnBasedMatchResponse_t4255652325 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose_m1721808164(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::FromPointer(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TurnBasedMatchResponse_t4255652325_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchResponse_FromPointer_m863377605_MetadataUsageId;
extern "C"  TurnBasedMatchResponse_t4255652325 * TurnBasedMatchResponse_FromPointer_m863377605 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchResponse_FromPointer_m863377605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IntPtr_t_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = IntPtr_Equals_m3488505417((&___pointer0), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (TurnBasedMatchResponse_t4255652325 *)NULL;
	}

IL_0018:
	{
		IntPtr_t L_4 = ___pointer0;
		TurnBasedMatchResponse_t4255652325 * L_5 = (TurnBasedMatchResponse_t4255652325 *)il2cpp_codegen_object_new(TurnBasedMatchResponse_t4255652325_il2cpp_TypeInfo_var);
		TurnBasedMatchResponse__ctor_m3633152958(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchConfig__ctor_m2267763325_MetadataUsageId;
extern "C"  void TurnBasedMatchConfig__ctor_m2267763325 (TurnBasedMatchConfig_t3069007197 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfig__ctor_m2267763325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdAtIndex(System.UIntPtr)
extern Il2CppClass* U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610_il2cpp_TypeInfo_var;
extern Il2CppClass* OutStringMethod_t1062451542_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_U3CU3Em__D8_m12626581_MethodInfo_var;
extern const uint32_t TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406_MetadataUsageId;
extern "C"  String_t* TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406 (TurnBasedMatchConfig_t3069007197 * __this, UIntPtr_t  ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * V_0 = NULL;
	{
		U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * L_0 = (U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 *)il2cpp_codegen_object_new(U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610_il2cpp_TypeInfo_var);
		U3CPlayerIdAtIndexU3Ec__AnonStoreyA8__ctor_m1794160121(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * L_1 = V_0;
		UIntPtr_t  L_2 = ___index0;
		NullCheck(L_1);
		L_1->set_index_0(L_2);
		U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_U3CU3Em__D8_m12626581_MethodInfo_var);
		OutStringMethod_t1062451542 * L_6 = (OutStringMethod_t1062451542 *)il2cpp_codegen_object_new(OutStringMethod_t1062451542_il2cpp_TypeInfo_var);
		OutStringMethod__ctor_m2730868781(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		String_t* L_7 = PInvokeUtilities_OutParamsToString_m1562371957(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdsToInvite()
extern Il2CppClass* Func_2_t2432508056_il2cpp_TypeInfo_var;
extern Il2CppClass* PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var;
extern const MethodInfo* TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2920535829_MethodInfo_var;
extern const MethodInfo* PInvokeUtilities_ToEnumerator_TisString_t_m5577312_MethodInfo_var;
extern const uint32_t TurnBasedMatchConfig_PlayerIdsToInvite_m1157952936_MetadataUsageId;
extern "C"  Il2CppObject* TurnBasedMatchConfig_PlayerIdsToInvite_m1157952936 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfig_PlayerIdsToInvite_m1157952936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		UIntPtr_t  L_1 = TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_Length_m353120500(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TurnBasedMatchConfig_PlayerIdAtIndex_m2905042406_MethodInfo_var);
		Func_2_t2432508056 * L_3 = (Func_2_t2432508056 *)il2cpp_codegen_object_new(Func_2_t2432508056_il2cpp_TypeInfo_var);
		Func_2__ctor_m2920535829(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m2920535829_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PInvokeUtilities_t2826698606_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = PInvokeUtilities_ToEnumerator_TisString_t_m5577312(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/PInvokeUtilities_ToEnumerator_TisString_t_m5577312_MethodInfo_var);
		return L_4;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::Variant()
extern "C"  uint32_t TurnBasedMatchConfig_Variant_m4237649269 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatchConfig_TurnBasedMatchConfig_Variant_m4247830218(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::ExclusiveBitMask()
extern "C"  int64_t TurnBasedMatchConfig_ExclusiveBitMask_m1648745241 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		int64_t L_1 = TurnBasedMatchConfig_TurnBasedMatchConfig_ExclusiveBitMask_m3723863422(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MinimumAutomatchingPlayers()
extern "C"  uint32_t TurnBasedMatchConfig_MinimumAutomatchingPlayers_m442887146 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatchConfig_TurnBasedMatchConfig_MinimumAutomatchingPlayers_m2446399567(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MaximumAutomatchingPlayers()
extern "C"  uint32_t TurnBasedMatchConfig_MaximumAutomatchingPlayers_m1650107672 (TurnBasedMatchConfig_t3069007197 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = TurnBasedMatchConfig_TurnBasedMatchConfig_MaximumAutomatchingPlayers_m3651177341(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfig_CallDispose_m2489810643 (TurnBasedMatchConfig_t3069007197 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		TurnBasedMatchConfig_TurnBasedMatchConfig_Dispose_m2127172169(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8::.ctor()
extern "C"  void U3CPlayerIdAtIndexU3Ec__AnonStoreyA8__ctor_m1794160121 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8::<>m__D8(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_U3CU3Em__D8_m12626581 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___size1, const MethodInfo* method)
{
	{
		TurnBasedMatchConfig_t3069007197 * L_0 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		HandleRef_t1780819301  L_1 = BaseReferenceHolder_SelfPtr_m1052503806(L_0, /*hidden argument*/NULL);
		UIntPtr_t  L_2 = __this->get_index_0();
		StringBuilder_t243639308 * L_3 = ___out_string0;
		UIntPtr_t  L_4 = ___size1;
		UIntPtr_t  L_5 = TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_GetElement_m3736469840(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::.ctor(System.IntPtr)
extern Il2CppClass* BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchConfigBuilder__ctor_m2363083424_MetadataUsageId;
extern "C"  void TurnBasedMatchConfigBuilder__ctor_m2363083424 (TurnBasedMatchConfigBuilder_t1401664752 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfigBuilder__ctor_m2363083424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___selfPointer0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseReferenceHolder_t2237584300_il2cpp_TypeInfo_var);
		BaseReferenceHolder__ctor_m2809683036(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::PopulateFromUIResponse(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_PopulateFromUIResponse_m1524931022 (TurnBasedMatchConfigBuilder_t1401664752 * __this, PlayerSelectUIResponse_t922401854 * ___response0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		PlayerSelectUIResponse_t922401854 * L_1 = ___response0;
		NullCheck(L_1);
		IntPtr_t L_2 = BaseReferenceHolder_AsPointer_m1925325420(L_1, /*hidden argument*/NULL);
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse_m3393489795(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetVariant(System.UInt32)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_SetVariant_m1810309937 (TurnBasedMatchConfigBuilder_t1401664752 * __this, uint32_t ___variant0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ___variant0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetVariant_m4181722918(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::AddInvitedPlayer(System.String)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_AddInvitedPlayer_m319062433 (TurnBasedMatchConfigBuilder_t1401664752 * __this, String_t* ___playerId0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___playerId0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_AddPlayerToInvite_m2963966479(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetExclusiveBitMask(System.UInt64)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_SetExclusiveBitMask_m2850987650 (TurnBasedMatchConfigBuilder_t1401664752 * __this, uint64_t ___bitmask0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint64_t L_1 = ___bitmask0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetExclusiveBitMask_m1450326527(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetMinimumAutomatchingPlayers(System.UInt32)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_SetMinimumAutomatchingPlayers_m3694099524 (TurnBasedMatchConfigBuilder_t1401664752 * __this, uint32_t ___minimum0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ___minimum0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers_m1511535515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::SetMaximumAutomatchingPlayers(System.UInt32)
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_SetMaximumAutomatchingPlayers_m4249460182 (TurnBasedMatchConfigBuilder_t1401664752 * __this, uint32_t ___maximum0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ___maximum0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers_m30375497(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::Build()
extern Il2CppClass* TurnBasedMatchConfig_t3069007197_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchConfigBuilder_Build_m2253168001_MetadataUsageId;
extern "C"  TurnBasedMatchConfig_t3069007197 * TurnBasedMatchConfigBuilder_Build_m2253168001 (TurnBasedMatchConfigBuilder_t1401664752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfigBuilder_Build_m2253168001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HandleRef_t1780819301  L_0 = BaseReferenceHolder_SelfPtr_m1052503806(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Create_m3121774112(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		TurnBasedMatchConfig_t3069007197 * L_2 = (TurnBasedMatchConfig_t3069007197 *)il2cpp_codegen_object_new(TurnBasedMatchConfig_t3069007197_il2cpp_TypeInfo_var);
		TurnBasedMatchConfig__ctor_m2267763325(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfigBuilder_CallDispose_m3149911824 (TurnBasedMatchConfigBuilder_t1401664752 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___selfPointer0;
		TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Dispose_m1368534604(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder GooglePlayGames.Native.PInvoke.TurnBasedMatchConfigBuilder::Create()
extern Il2CppClass* TurnBasedMatchConfigBuilder_t1401664752_il2cpp_TypeInfo_var;
extern const uint32_t TurnBasedMatchConfigBuilder_Create_m3948156898_MetadataUsageId;
extern "C"  TurnBasedMatchConfigBuilder_t1401664752 * TurnBasedMatchConfigBuilder_Create_m3948156898 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TurnBasedMatchConfigBuilder_Create_m3948156898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Construct_m2904048914(NULL /*static, unused*/, /*hidden argument*/NULL);
		TurnBasedMatchConfigBuilder_t1401664752 * L_1 = (TurnBasedMatchConfigBuilder_t1401664752 *)il2cpp_codegen_object_new(TurnBasedMatchConfigBuilder_t1401664752_il2cpp_TypeInfo_var);
		TurnBasedMatchConfigBuilder__ctor_m2363083424(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::.ctor(System.String)
extern const MethodInfo* Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var;
extern const uint32_t UnsupportedSavedGamesClient__ctor_m3943147780_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient__ctor_m3943147780 (UnsupportedSavedGamesClient_t1204855368 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient__ctor_m3943147780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___message0;
		String_t* L_1 = Misc_CheckNotNull_TisString_t_m699927801(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m699927801_MethodInfo_var);
		__this->set_mMessage_0(L_1);
		return;
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::OpenWithAutomaticConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_OpenWithAutomaticConflictResolution_m86433630_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_OpenWithAutomaticConflictResolution_m86433630 (UnsupportedSavedGamesClient_t1204855368 * __this, String_t* ___filename0, int32_t ___source1, int32_t ___resolutionStrategy2, Action_2_t2072880178 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_OpenWithAutomaticConflictResolution_m86433630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_OpenWithManualConflictResolution_m1396368090_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_OpenWithManualConflictResolution_m1396368090 (UnsupportedSavedGamesClient_t1204855368 * __this, String_t* ___filename0, int32_t ___source1, bool ___prefetchDataOnConflict2, ConflictCallback_t942269343 * ___conflictCallback3, Action_2_t2072880178 * ___completedCallback4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_OpenWithManualConflictResolution_m1396368090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::ReadBinaryData(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_ReadBinaryData_m3700419110_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_ReadBinaryData_m3700419110 (UnsupportedSavedGamesClient_t1204855368 * __this, Il2CppObject * ___metadata0, Action_2_t2751370656 * ___completedCallback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_ReadBinaryData_m3700419110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::ShowSelectSavedGameUI(System.String,System.UInt32,System.Boolean,System.Boolean,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_ShowSelectSavedGameUI_m3905940860_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_ShowSelectSavedGameUI_m3905940860 (UnsupportedSavedGamesClient_t1204855368 * __this, String_t* ___uiTitle0, uint32_t ___maxDisplayedSavedGames1, bool ___showCreateSaveUI2, bool ___showDeleteSaveUI3, Action_2_t1151548080 * ___callback4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_ShowSelectSavedGameUI_m3905940860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::CommitUpdate(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[],System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_CommitUpdate_m1724690339_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_CommitUpdate_m1724690339 (UnsupportedSavedGamesClient_t1204855368 * __this, Il2CppObject * ___metadata0, SavedGameMetadataUpdate_t4052853983  ___updateForMetadata1, ByteU5BU5D_t4260760469* ___updatedBinaryData2, Action_2_t2072880178 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_CommitUpdate_m1724690339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::FetchAllSavedGames(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_FetchAllSavedGames_m4108889032_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_FetchAllSavedGames_m4108889032 (UnsupportedSavedGamesClient_t1204855368 * __this, int32_t ___source0, Action_2_t3441065730 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_FetchAllSavedGames_m4108889032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::Delete(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const uint32_t UnsupportedSavedGamesClient_Delete_m861289605_MetadataUsageId;
extern "C"  void UnsupportedSavedGamesClient_Delete_m861289605 (UnsupportedSavedGamesClient_t1204855368 * __this, Il2CppObject * ___metadata0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnsupportedSavedGamesClient_Delete_m861289605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mMessage_0();
		NotImplementedException_t1912495542 * L_1 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::.ctor()
extern "C"  void Logger__ctor_m939653068 (Logger_t984534948 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::.cctor()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t Logger__cctor_m2877345121_MetadataUsageId;
extern "C"  void Logger__cctor_m2877345121 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger__cctor_m2877345121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->set_warningLogEnabled_1((bool)1);
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Logger::get_DebugLogEnabled()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t Logger_get_DebugLogEnabled_m3753664069_MetadataUsageId;
extern "C"  bool Logger_get_DebugLogEnabled_m3753664069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_get_DebugLogEnabled_m3753664069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->get_debugLogEnabled_0();
		return L_0;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_DebugLogEnabled(System.Boolean)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t Logger_set_DebugLogEnabled_m2155831124_MetadataUsageId;
extern "C"  void Logger_set_DebugLogEnabled_m2155831124 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_set_DebugLogEnabled_m2155831124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->set_debugLogEnabled_0(L_0);
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Logger::get_WarningLogEnabled()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t Logger_get_WarningLogEnabled_m2123552014_MetadataUsageId;
extern "C"  bool Logger_get_WarningLogEnabled_m2123552014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_get_WarningLogEnabled_m2123552014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->get_warningLogEnabled_1();
		return L_0;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::set_WarningLogEnabled(System.Boolean)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t Logger_set_WarningLogEnabled_m1751977053_MetadataUsageId;
extern "C"  void Logger_set_WarningLogEnabled_m1751977053 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_set_WarningLogEnabled_m1751977053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->set_warningLogEnabled_1(L_0);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
extern Il2CppClass* U3CdU3Ec__AnonStorey3A_t139427695_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302_MethodInfo_var;
extern const uint32_t Logger_d_m234514644_MetadataUsageId;
extern "C"  void Logger_d_m234514644 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_d_m234514644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CdU3Ec__AnonStorey3A_t139427695 * V_0 = NULL;
	{
		U3CdU3Ec__AnonStorey3A_t139427695 * L_0 = (U3CdU3Ec__AnonStorey3A_t139427695 *)il2cpp_codegen_object_new(U3CdU3Ec__AnonStorey3A_t139427695_il2cpp_TypeInfo_var);
		U3CdU3Ec__AnonStorey3A__ctor_m1409748492(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CdU3Ec__AnonStorey3A_t139427695 * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->get_debugLogEnabled_0();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		U3CdU3Ec__AnonStorey3A_t139427695 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302_MethodInfo_var);
		Action_t3771233898 * L_6 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
extern Il2CppClass* U3CwU3Ec__AnonStorey3B_t82508163_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066_MethodInfo_var;
extern const uint32_t Logger_w_m3419267169_MetadataUsageId;
extern "C"  void Logger_w_m3419267169 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_w_m3419267169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CwU3Ec__AnonStorey3B_t82508163 * V_0 = NULL;
	{
		U3CwU3Ec__AnonStorey3B_t82508163 * L_0 = (U3CwU3Ec__AnonStorey3B_t82508163 *)il2cpp_codegen_object_new(U3CwU3Ec__AnonStorey3B_t82508163_il2cpp_TypeInfo_var);
		U3CwU3Ec__AnonStorey3B__ctor_m1802611320(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CwU3Ec__AnonStorey3B_t82508163 * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->get_warningLogEnabled_1();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		U3CwU3Ec__AnonStorey3B_t82508163 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066_MethodInfo_var);
		Action_t3771233898 * L_6 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
extern Il2CppClass* U3CeU3Ec__AnonStorey3C_t1492737394_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546_MethodInfo_var;
extern const uint32_t Logger_e_m4018947763_MetadataUsageId;
extern "C"  void Logger_e_m4018947763 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_e_m4018947763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CeU3Ec__AnonStorey3C_t1492737394 * V_0 = NULL;
	{
		U3CeU3Ec__AnonStorey3C_t1492737394 * L_0 = (U3CeU3Ec__AnonStorey3C_t1492737394 *)il2cpp_codegen_object_new(U3CeU3Ec__AnonStorey3C_t1492737394_il2cpp_TypeInfo_var);
		U3CeU3Ec__AnonStorey3C__ctor_m1047741289(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CeU3Ec__AnonStorey3C_t1492737394 * L_1 = V_0;
		String_t* L_2 = ___msg0;
		NullCheck(L_1);
		L_1->set_msg_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_3 = ((Logger_t984534948_StaticFields*)Logger_t984534948_il2cpp_TypeInfo_var->static_fields)->get_warningLogEnabled_1();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		U3CeU3Ec__AnonStorey3C_t1492737394 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546_MethodInfo_var);
		Action_t3771233898 * L_6 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::describe(System.Byte[])
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1250346074;
extern Il2CppCodeGenString* _stringLiteral94224467;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t Logger_describe_m3732005225_MetadataUsageId;
extern "C"  String_t* Logger_describe_m3732005225 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___b0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_describe_m3732005225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = ___b0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral1250346074;
		goto IL_0027;
	}

IL_0010:
	{
		ByteU5BU5D_t4260760469* L_1 = ___b0;
		NullCheck(L_1);
		int32_t L_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))));
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral94224467, L_3, _stringLiteral93, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0027:
	{
		return G_B3_0;
	}
}
// System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1570599348;
extern Il2CppCodeGenString* _stringLiteral2366589858;
extern const uint32_t Logger_ToLogMessage_m938433255_MetadataUsageId;
extern "C"  String_t* Logger_ToLogMessage_m938433255 (Il2CppObject * __this /* static, unused */, String_t* ___prefix0, String_t* ___logType1, String_t* ___msg2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_ToLogMessage_m938433255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_1 = ___prefix0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_3 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = DateTime_ToString_m3415116655((&V_0), _stringLiteral2366589858, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_2;
		String_t* L_6 = ___logType1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_5;
		String_t* L_8 = ___msg2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m4050103162(NULL /*static, unused*/, _stringLiteral1570599348, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A::.ctor()
extern "C"  void U3CdU3Ec__AnonStorey3A__ctor_m1409748492 (U3CdU3Ec__AnonStorey3A_t139427695 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<d>c__AnonStorey3A::<>m__9()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral64921139;
extern const uint32_t U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302_MetadataUsageId;
extern "C"  void U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302 (U3CdU3Ec__AnonStorey3A_t139427695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdU3Ec__AnonStorey3A_U3CU3Em__9_m688443302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		String_t* L_2 = Logger_ToLogMessage_m938433255(NULL /*static, unused*/, L_0, _stringLiteral64921139, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C::.ctor()
extern "C"  void U3CeU3Ec__AnonStorey3C__ctor_m1047741289 (U3CeU3Ec__AnonStorey3C_t1492737394 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C::<>m__B()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral41706;
extern Il2CppCodeGenString* _stringLiteral66247144;
extern const uint32_t U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546_MetadataUsageId;
extern "C"  void U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546 (U3CeU3Ec__AnonStorey3C_t1492737394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		String_t* L_1 = Logger_ToLogMessage_m938433255(NULL /*static, unused*/, _stringLiteral41706, _stringLiteral66247144, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B::.ctor()
extern "C"  void U3CwU3Ec__AnonStorey3B__ctor_m1802611320 (U3CwU3Ec__AnonStorey3B_t82508163 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B::<>m__A()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32769;
extern Il2CppCodeGenString* _stringLiteral1842428796;
extern const uint32_t U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066_MetadataUsageId;
extern "C"  void U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066 (U3CwU3Ec__AnonStorey3B_t82508163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_msg_0();
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		String_t* L_1 = Logger_ToLogMessage_m938433255(NULL /*static, unused*/, _stringLiteral32769, _stringLiteral1842428796, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
extern "C"  bool Misc_BuffersAreIdentical_m5238390 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___a0, ByteU5BU5D_t4260760469* ___b1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___a0;
		ByteU5BU5D_t4260760469* L_1 = ___b1;
		if ((!(((Il2CppObject*)(ByteU5BU5D_t4260760469*)L_0) == ((Il2CppObject*)(ByteU5BU5D_t4260760469*)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		ByteU5BU5D_t4260760469* L_2 = ___a0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_3 = ___b1;
		if (L_3)
		{
			goto IL_0017;
		}
	}

IL_0015:
	{
		return (bool)0;
	}

IL_0017:
	{
		ByteU5BU5D_t4260760469* L_4 = ___a0;
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_5 = ___b1;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_002b:
	{
		ByteU5BU5D_t4260760469* L_6 = ___a0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		ByteU5BU5D_t4260760469* L_10 = ___b1;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		uint8_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		if ((((int32_t)L_9) == ((int32_t)L_13)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0038:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_15 = V_0;
		ByteU5BU5D_t4260760469* L_16 = ___a0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)1;
	}
}
// System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern Il2CppCodeGenString* _stringLiteral3188603622;
extern const uint32_t Misc_GetSubsetBytes_m1859181228_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* Misc_GetSubsetBytes_m1859181228 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___array0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Misc_GetSubsetBytes_m1859181228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = ___offset1;
		ByteU5BU5D_t4260760469* L_4 = ___array0;
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, _stringLiteral3275187347, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		int32_t L_6 = ___length2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_7 = ___array0;
		NullCheck(L_7);
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___length2;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_0049;
		}
	}

IL_003e:
	{
		ArgumentOutOfRangeException_t3816648464 * L_10 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_10, _stringLiteral3188603622, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0049:
	{
		int32_t L_11 = ___offset1;
		if (L_11)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_12 = ___length2;
		ByteU5BU5D_t4260760469* L_13 = ___array0;
		NullCheck(L_13);
		if ((!(((uint32_t)L_12) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))))))
		{
			goto IL_005a;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_14 = ___array0;
		return L_14;
	}

IL_005a:
	{
		int32_t L_15 = ___length2;
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_15));
		ByteU5BU5D_t4260760469* L_16 = ___array0;
		int32_t L_17 = ___offset1;
		ByteU5BU5D_t4260760469* L_18 = V_0;
		int32_t L_19 = ___length2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, L_17, (Il2CppArray *)(Il2CppArray *)L_18, 0, L_19, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_20 = V_0;
		return L_20;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.ctor()
extern Il2CppClass* List_1_t844452154_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2445803700_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject__ctor_m1093222576_MetadataUsageId;
extern "C"  void PlayGamesHelperObject__ctor_m1093222576 (PlayGamesHelperObject_t727662960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject__ctor_m1093222576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t844452154 * L_0 = (List_1_t844452154 *)il2cpp_codegen_object_new(List_1_t844452154_il2cpp_TypeInfo_var);
		List_1__ctor_m2445803700(L_0, /*hidden argument*/List_1__ctor_m2445803700_MethodInfo_var);
		__this->set_localQueue_5(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::.cctor()
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t844452154_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2240800406_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2445803700_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3809301118_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject__cctor_m3343032573_MetadataUsageId;
extern "C"  void PlayGamesHelperObject__cctor_m3343032573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject__cctor_m3343032573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayGamesHelperObject_t727662960 *)NULL);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sIsDummy_3((bool)0);
		List_1_t844452154 * L_0 = (List_1_t844452154 *)il2cpp_codegen_object_new(List_1_t844452154_il2cpp_TypeInfo_var);
		List_1__ctor_m2445803700(L_0, /*hidden argument*/List_1__ctor_m2445803700_MethodInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sQueue_4(L_0);
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sQueueEmpty_6(1);
		List_1_t2240800406 * L_1 = (List_1_t2240800406 *)il2cpp_codegen_object_new(List_1_t2240800406_il2cpp_TypeInfo_var);
		List_1__ctor_m3809301118(L_1, /*hidden argument*/List_1__ctor_m3809301118_MethodInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sPauseCallbackList_7(L_1);
		List_1_t2240800406 * L_2 = (List_1_t2240800406 *)il2cpp_codegen_object_new(List_1_t2240800406_il2cpp_TypeInfo_var);
		List_1__ctor_m3809301118(L_2, /*hidden argument*/List_1__ctor_m3809301118_MethodInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sFocusCallbackList_8(L_2);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::CreateObject()
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayGamesHelperObject_t727662960_m2233263623_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3773466127;
extern const uint32_t PlayGamesHelperObject_CreateObject_m281177135_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_CreateObject_m281177135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_CreateObject_m281177135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t727662960 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		bool L_2 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		GameObject_t3674682005 * L_3 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_3, _stringLiteral3773466127, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		PlayGamesHelperObject_t727662960 * L_6 = GameObject_AddComponent_TisPlayGamesHelperObject_t727662960_m2233263623(L_5, /*hidden argument*/GameObject_AddComponent_TisPlayGamesHelperObject_t727662960_m2233263623_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_6);
		goto IL_004c;
	}

IL_003c:
	{
		PlayGamesHelperObject_t727662960 * L_7 = (PlayGamesHelperObject_t727662960 *)il2cpp_codegen_object_new(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject__ctor_m1093222576(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_7);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sIsDummy_3((bool)1);
	}

IL_004c:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Awake()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesHelperObject_Awake_m1330827795_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_Awake_m1330827795 (PlayGamesHelperObject_t727662960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_Awake_m1330827795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnDisable()
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesHelperObject_OnDisable_m574668055_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_OnDisable_m574668055 (PlayGamesHelperObject_t727662960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnDisable_m574668055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t727662960 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayGamesHelperObject_t727662960 *)NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunCoroutine(System.Collections.IEnumerator)
extern Il2CppClass* U3CRunCoroutineU3Ec__AnonStorey3D_t724587615_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_RunCoroutine_m3444677164_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_RunCoroutine_m3444677164 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RunCoroutine_m3444677164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * V_0 = NULL;
	{
		U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * L_0 = (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 *)il2cpp_codegen_object_new(U3CRunCoroutineU3Ec__AnonStorey3D_t724587615_il2cpp_TypeInfo_var);
		U3CRunCoroutineU3Ec__AnonStorey3D__ctor_m3701102876(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * L_1 = V_0;
		Il2CppObject * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t727662960 * L_3 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080_MethodInfo_var);
		Action_t3771233898 * L_7 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunOnGameThread_m1608083433(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::RunOnGameThread(System.Action)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m2140164516_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2872016438;
extern const uint32_t PlayGamesHelperObject_RunOnGameThread_m1608083433_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_RunOnGameThread_m1608083433 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RunOnGameThread_m1608083433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t844452154 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Action_t3771233898 * L_0 = ___action0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2872016438, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		bool L_2 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sIsDummy_3();
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t844452154 * L_3 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueue_4();
		V_0 = L_3;
		List_1_t844452154 * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t844452154 * L_5 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueue_4();
		Action_t3771233898 * L_6 = ___action0;
		NullCheck(L_5);
		List_1_Add_m2140164516(L_5, L_6, /*hidden argument*/List_1_Add_m2140164516_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sQueueEmpty_6(0);
		IL2CPP_LEAVE(0x47, FINALLY_0040);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		List_1_t844452154 * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::Update()
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m4146904287_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3373845830_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m330663275_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m628918122_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_Update_m1257023581_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_Update_m1257023581 (PlayGamesHelperObject_t727662960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_Update_m1257023581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t844452154 * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sIsDummy_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueueEmpty_6();
		il2cpp_codegen_memory_barrier();
		if (!L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		List_1_t844452154 * L_2 = __this->get_localQueue_5();
		NullCheck(L_2);
		List_1_Clear_m4146904287(L_2, /*hidden argument*/List_1_Clear_m4146904287_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t844452154 * L_3 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueue_4();
		V_0 = L_3;
		List_1_t844452154 * L_4 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		List_1_t844452154 * L_5 = __this->get_localQueue_5();
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t844452154 * L_6 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueue_4();
		NullCheck(L_5);
		List_1_AddRange_m3373845830(L_5, L_6, /*hidden argument*/List_1_AddRange_m3373845830_MethodInfo_var);
		List_1_t844452154 * L_7 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sQueue_4();
		NullCheck(L_7);
		List_1_Clear_m4146904287(L_7, /*hidden argument*/List_1_Clear_m4146904287_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->set_sQueueEmpty_6(1);
		IL2CPP_LEAVE(0x5C, FINALLY_0055);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		List_1_t844452154 * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005c:
	{
		V_1 = 0;
		goto IL_0078;
	}

IL_0063:
	{
		List_1_t844452154 * L_9 = __this->get_localQueue_5();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Action_t3771233898 * L_11 = List_1_get_Item_m330663275(L_9, L_10, /*hidden argument*/List_1_get_Item_m330663275_MethodInfo_var);
		NullCheck(L_11);
		Action_Invoke_m1445970038(L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_13 = V_1;
		List_1_t844452154 * L_14 = __this->get_localQueue_5();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m628918122(L_14, /*hidden argument*/List_1_get_Count_m628918122_MethodInfo_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0063;
		}
	}
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationFocus(System.Boolean)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2260473176_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2944382337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3527387137_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m550731055_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1105123113;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t PlayGamesHelperObject_OnApplicationFocus_m3270632242_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_OnApplicationFocus_m3270632242 (PlayGamesHelperObject_t727662960 * __this, bool ___focused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnApplicationFocus_m3270632242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * V_0 = NULL;
	Enumerator_t2260473176  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sFocusCallbackList_8();
		NullCheck(L_0);
		Enumerator_t2260473176  L_1 = List_1_GetEnumerator_m2944382337(L_0, /*hidden argument*/List_1_GetEnumerator_m2944382337_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0010:
		{
			Action_1_t872614854 * L_2 = Enumerator_get_Current_m3527387137((&V_1), /*hidden argument*/Enumerator_get_Current_m3527387137_MethodInfo_var);
			V_0 = L_2;
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Action_1_t872614854 * L_3 = V_0;
			bool L_4 = ___focused0;
			NullCheck(L_3);
			Action_1_Invoke_m3594021162(L_3, L_4, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
			goto IL_004a;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0024;
			throw e;
		}

CATCH_0024:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t3991598821 *)__exception_local);
			Exception_t3991598821 * L_5 = V_2;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_5);
			Exception_t3991598821 * L_7 = V_2;
			NullCheck(L_7);
			String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_9 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral1105123113, L_6, _stringLiteral10, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			goto IL_004a;
		} // end catch (depth: 2)

IL_004a:
		{
			bool L_10 = Enumerator_MoveNext_m550731055((&V_1), /*hidden argument*/Enumerator_MoveNext_m550731055_MethodInfo_var);
			if (L_10)
			{
				goto IL_0010;
			}
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x67, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Enumerator_t2260473176  L_11 = V_1;
		Enumerator_t2260473176  L_12 = L_11;
		Il2CppObject * L_13 = Box(Enumerator_t2260473176_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Il2CppObject *)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::OnApplicationPause(System.Boolean)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2260473176_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2944382337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3527387137_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m550731055_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1379019211;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t PlayGamesHelperObject_OnApplicationPause_m429832272_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_OnApplicationPause_m429832272 (PlayGamesHelperObject_t727662960 * __this, bool ___paused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_OnApplicationPause_m429832272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * V_0 = NULL;
	Enumerator_t2260473176  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sPauseCallbackList_7();
		NullCheck(L_0);
		Enumerator_t2260473176  L_1 = List_1_GetEnumerator_m2944382337(L_0, /*hidden argument*/List_1_GetEnumerator_m2944382337_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0010:
		{
			Action_1_t872614854 * L_2 = Enumerator_get_Current_m3527387137((&V_1), /*hidden argument*/Enumerator_get_Current_m3527387137_MethodInfo_var);
			V_0 = L_2;
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			Action_1_t872614854 * L_3 = V_0;
			bool L_4 = ___paused0;
			NullCheck(L_3);
			Action_1_Invoke_m3594021162(L_3, L_4, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
			goto IL_004a;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0024;
			throw e;
		}

CATCH_0024:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t3991598821 *)__exception_local);
			Exception_t3991598821 * L_5 = V_2;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_5);
			Exception_t3991598821 * L_7 = V_2;
			NullCheck(L_7);
			String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_9 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral1379019211, L_6, _stringLiteral10, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			goto IL_004a;
		} // end catch (depth: 2)

IL_004a:
		{
			bool L_10 = Enumerator_MoveNext_m550731055((&V_1), /*hidden argument*/Enumerator_MoveNext_m550731055_MethodInfo_var);
			if (L_10)
			{
				goto IL_0010;
			}
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x67, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Enumerator_t2260473176  L_11 = V_1;
		Enumerator_t2260473176  L_12 = L_11;
		Il2CppObject * L_13 = Box(Enumerator_t2260473176_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Il2CppObject *)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0067:
	{
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddFocusCallback(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Contains_m855514186_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3503661934_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_AddFocusCallback_m1120382595_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_AddFocusCallback_m1120382595 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_AddFocusCallback_m1120382595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sFocusCallbackList_8();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m855514186(L_0, L_1, /*hidden argument*/List_1_Contains_m855514186_MethodInfo_var);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_3 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sFocusCallbackList_8();
		Action_1_t872614854 * L_4 = ___callback0;
		NullCheck(L_3);
		List_1_Add_m3503661934(L_3, L_4, /*hidden argument*/List_1_Add_m3503661934_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemoveFocusCallback(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m3326004847_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_RemoveFocusCallback_m2199410080_MetadataUsageId;
extern "C"  bool PlayGamesHelperObject_RemoveFocusCallback_m2199410080 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RemoveFocusCallback_m2199410080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sFocusCallbackList_8();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Remove_m3326004847(L_0, L_1, /*hidden argument*/List_1_Remove_m3326004847_MethodInfo_var);
		return L_2;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject::AddPauseCallback(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Contains_m855514186_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3503661934_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_AddPauseCallback_m2403088165_MetadataUsageId;
extern "C"  void PlayGamesHelperObject_AddPauseCallback_m2403088165 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_AddPauseCallback_m2403088165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sPauseCallbackList_7();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m855514186(L_0, L_1, /*hidden argument*/List_1_Contains_m855514186_MethodInfo_var);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_3 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sPauseCallbackList_7();
		Action_1_t872614854 * L_4 = ___callback0;
		NullCheck(L_3);
		List_1_Add_m3503661934(L_3, L_4, /*hidden argument*/List_1_Add_m3503661934_MethodInfo_var);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::RemovePauseCallback(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m3326004847_MethodInfo_var;
extern const uint32_t PlayGamesHelperObject_RemovePauseCallback_m3482115650_MetadataUsageId;
extern "C"  bool PlayGamesHelperObject_RemovePauseCallback_m3482115650 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesHelperObject_RemovePauseCallback_m3482115650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		List_1_t2240800406 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_sPauseCallbackList_7();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		bool L_2 = List_1_Remove_m3326004847(L_0, L_1, /*hidden argument*/List_1_Remove_m3326004847_MethodInfo_var);
		return L_2;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D::.ctor()
extern "C"  void U3CRunCoroutineU3Ec__AnonStorey3D__ctor_m3701102876 (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.OurUtils.PlayGamesHelperObject/<RunCoroutine>c__AnonStorey3D::<>m__C()
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern const uint32_t U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080_MetadataUsageId;
extern "C"  void U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080 (U3CRunCoroutineU3Ec__AnonStorey3D_t724587615 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRunCoroutineU3Ec__AnonStorey3D_U3CU3Em__C_m3000962080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_t727662960 * L_0 = ((PlayGamesHelperObject_t727662960_StaticFields*)PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		Il2CppObject * L_1 = __this->get_action_0();
		NullCheck(L_0);
		MonoBehaviour_StartCoroutine_m2135303124(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor()
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern Il2CppClass* ReportProgress_t3967815895_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesAchievement__ctor_m3384221919_MetadataUsageId;
extern "C"  void PlayGamesAchievement__ctor_m3384221919 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesAchievement__ctor_m3384221919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = PlayGamesPlatform_get_Instance_m247033584(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayGamesPlatform_t3624292130 * L_1 = L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_1, 9));
		ReportProgress_t3967815895 * L_3 = (ReportProgress_t3967815895 *)il2cpp_codegen_object_new(ReportProgress_t3967815895_il2cpp_TypeInfo_var);
		ReportProgress__ctor_m230147170(L_3, L_1, L_2, /*hidden argument*/NULL);
		PlayGamesAchievement__ctor_m1026446980(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor(GooglePlayGames.ReportProgress)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesAchievement__ctor_m1026446980_MetadataUsageId;
extern "C"  void PlayGamesAchievement__ctor_m1026446980 (PlayGamesAchievement_t2357341912 * __this, ReportProgress_t3967815895 * ___progressCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesAchievement__ctor_m1026446980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mId_1(L_0);
		DateTime_t4283661327  L_1;
		memset(&L_1, 0, sizeof(L_1));
		DateTime__ctor_m1594789867(&L_1, ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		__this->set_mLastModifiedTime_8(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mTitle_9(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mRevealedImageUrl_10(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mUnlockedImageUrl_11(L_4);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mDescription_14(L_5);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ReportProgress_t3967815895 * L_6 = ___progressCallback0;
		__this->set_mProgressCallback_0(L_6);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::.ctor(GooglePlayGames.BasicApi.Achievement)
extern "C"  void PlayGamesAchievement__ctor_m1510359896 (PlayGamesAchievement_t2357341912 * __this, Achievement_t1261647177 * ___ach0, const MethodInfo* method)
{
	PlayGamesAchievement_t2357341912 * G_B7_0 = NULL;
	PlayGamesAchievement_t2357341912 * G_B6_0 = NULL;
	double G_B8_0 = 0.0;
	PlayGamesAchievement_t2357341912 * G_B8_1 = NULL;
	{
		PlayGamesAchievement__ctor_m3384221919(__this, /*hidden argument*/NULL);
		Achievement_t1261647177 * L_0 = ___ach0;
		NullCheck(L_0);
		String_t* L_1 = Achievement_get_Id_m502868599(L_0, /*hidden argument*/NULL);
		__this->set_mId_1(L_1);
		Achievement_t1261647177 * L_2 = ___ach0;
		NullCheck(L_2);
		bool L_3 = Achievement_get_IsIncremental_m3147909093(L_2, /*hidden argument*/NULL);
		__this->set_mIsIncremental_2(L_3);
		Achievement_t1261647177 * L_4 = ___ach0;
		NullCheck(L_4);
		int32_t L_5 = Achievement_get_CurrentSteps_m2632110837(L_4, /*hidden argument*/NULL);
		__this->set_mCurrentSteps_3(L_5);
		Achievement_t1261647177 * L_6 = ___ach0;
		NullCheck(L_6);
		int32_t L_7 = Achievement_get_TotalSteps_m951649354(L_6, /*hidden argument*/NULL);
		__this->set_mTotalSteps_4(L_7);
		Achievement_t1261647177 * L_8 = ___ach0;
		NullCheck(L_8);
		bool L_9 = Achievement_get_IsIncremental_m3147909093(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0085;
		}
	}
	{
		Achievement_t1261647177 * L_10 = ___ach0;
		NullCheck(L_10);
		int32_t L_11 = Achievement_get_TotalSteps_m951649354(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		Achievement_t1261647177 * L_12 = ___ach0;
		NullCheck(L_12);
		int32_t L_13 = Achievement_get_CurrentSteps_m2632110837(L_12, /*hidden argument*/NULL);
		Achievement_t1261647177 * L_14 = ___ach0;
		NullCheck(L_14);
		int32_t L_15 = Achievement_get_TotalSteps_m951649354(L_14, /*hidden argument*/NULL);
		__this->set_mPercentComplete_5(((double)((double)((double)((double)(((double)((double)L_13)))/(double)(((double)((double)L_15)))))*(double)(100.0))));
		goto IL_0080;
	}

IL_0071:
	{
		__this->set_mPercentComplete_5((0.0));
	}

IL_0080:
	{
		goto IL_00ad;
	}

IL_0085:
	{
		Achievement_t1261647177 * L_16 = ___ach0;
		NullCheck(L_16);
		bool L_17 = Achievement_get_IsUnlocked_m3028755930(L_16, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if (!L_17)
		{
			G_B7_0 = __this;
			goto IL_009f;
		}
	}
	{
		G_B8_0 = (100.0);
		G_B8_1 = G_B6_0;
		goto IL_00a8;
	}

IL_009f:
	{
		G_B8_0 = (0.0);
		G_B8_1 = G_B7_0;
	}

IL_00a8:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mPercentComplete_5(G_B8_0);
	}

IL_00ad:
	{
		Achievement_t1261647177 * L_18 = ___ach0;
		NullCheck(L_18);
		bool L_19 = Achievement_get_IsUnlocked_m3028755930(L_18, /*hidden argument*/NULL);
		__this->set_mCompleted_6(L_19);
		Achievement_t1261647177 * L_20 = ___ach0;
		NullCheck(L_20);
		bool L_21 = Achievement_get_IsRevealed_m2612314595(L_20, /*hidden argument*/NULL);
		__this->set_mHidden_7((bool)((((int32_t)L_21) == ((int32_t)0))? 1 : 0));
		Achievement_t1261647177 * L_22 = ___ach0;
		NullCheck(L_22);
		DateTime_t4283661327  L_23 = Achievement_get_LastModifiedTime_m2988362366(L_22, /*hidden argument*/NULL);
		__this->set_mLastModifiedTime_8(L_23);
		Achievement_t1261647177 * L_24 = ___ach0;
		NullCheck(L_24);
		String_t* L_25 = Achievement_get_Name_m2362876199(L_24, /*hidden argument*/NULL);
		__this->set_mTitle_9(L_25);
		Achievement_t1261647177 * L_26 = ___ach0;
		NullCheck(L_26);
		String_t* L_27 = Achievement_get_Description_m236770626(L_26, /*hidden argument*/NULL);
		__this->set_mDescription_14(L_27);
		Achievement_t1261647177 * L_28 = ___ach0;
		NullCheck(L_28);
		uint64_t L_29 = Achievement_get_Points_m3921891320(L_28, /*hidden argument*/NULL);
		__this->set_mPoints_15(L_29);
		Achievement_t1261647177 * L_30 = ___ach0;
		NullCheck(L_30);
		String_t* L_31 = Achievement_get_RevealedImageUrl_m89171004(L_30, /*hidden argument*/NULL);
		__this->set_mRevealedImageUrl_10(L_31);
		Achievement_t1261647177 * L_32 = ___ach0;
		NullCheck(L_32);
		String_t* L_33 = Achievement_get_UnlockedImageUrl_m2971083059(L_32, /*hidden argument*/NULL);
		__this->set_mUnlockedImageUrl_11(L_33);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::ReportProgress(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesAchievement_ReportProgress_m4053774893 (PlayGamesAchievement_t2357341912 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	{
		ReportProgress_t3967815895 * L_0 = __this->get_mProgressCallback_0();
		String_t* L_1 = __this->get_mId_1();
		double L_2 = __this->get_mPercentComplete_5();
		Action_1_t872614854 * L_3 = ___callback0;
		NullCheck(L_0);
		ReportProgress_Invoke_m485975983(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesAchievement::LoadImage()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesAchievement_LoadImage_m298890549_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * PlayGamesAchievement_LoadImage_m298890549 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesAchievement_LoadImage_m298890549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		bool L_0 = PlayGamesAchievement_get_hidden_m1598629132(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (Texture2D_t3884108195 *)NULL;
	}

IL_000d:
	{
		bool L_1 = PlayGamesAchievement_get_completed_m3462469643(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		String_t* L_2 = __this->get_mUnlockedImageUrl_11();
		G_B5_0 = L_2;
		goto IL_0029;
	}

IL_0023:
	{
		String_t* L_3 = __this->get_mRevealedImageUrl_10();
		G_B5_0 = L_3;
	}

IL_0029:
	{
		V_0 = G_B5_0;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00a9;
		}
	}
	{
		WWW_t3134621005 * L_6 = __this->get_mImageFetcher_12();
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		WWW_t3134621005 * L_7 = __this->get_mImageFetcher_12();
		NullCheck(L_7);
		String_t* L_8 = WWW_get_url_m4155171145(L_7, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0069;
		}
	}

IL_0056:
	{
		String_t* L_11 = V_0;
		WWW_t3134621005 * L_12 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_12, L_11, /*hidden argument*/NULL);
		__this->set_mImageFetcher_12(L_12);
		__this->set_mImage_13((Texture2D_t3884108195 *)NULL);
	}

IL_0069:
	{
		Texture2D_t3884108195 * L_13 = __this->get_mImage_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0081;
		}
	}
	{
		Texture2D_t3884108195 * L_15 = __this->get_mImage_13();
		return L_15;
	}

IL_0081:
	{
		WWW_t3134621005 * L_16 = __this->get_mImageFetcher_12();
		NullCheck(L_16);
		bool L_17 = WWW_get_isDone_m634060017(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a9;
		}
	}
	{
		WWW_t3134621005 * L_18 = __this->get_mImageFetcher_12();
		NullCheck(L_18);
		Texture2D_t3884108195 * L_19 = WWW_get_texture_m2854732303(L_18, /*hidden argument*/NULL);
		__this->set_mImage_13(L_19);
		Texture2D_t3884108195 * L_20 = __this->get_mImage_13();
		return L_20;
	}

IL_00a9:
	{
		return (Texture2D_t3884108195 *)NULL;
	}
}
// System.String GooglePlayGames.PlayGamesAchievement::get_id()
extern "C"  String_t* PlayGamesAchievement_get_id_m197866988 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mId_1();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::set_id(System.String)
extern "C"  void PlayGamesAchievement_set_id_m1300436645 (PlayGamesAchievement_t2357341912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mId_1(L_0);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_isIncremental()
extern "C"  bool PlayGamesAchievement_get_isIncremental_m2094723408 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mIsIncremental_2();
		return L_0;
	}
}
// System.Int32 GooglePlayGames.PlayGamesAchievement::get_currentSteps()
extern "C"  int32_t PlayGamesAchievement_get_currentSteps_m3429421098 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mCurrentSteps_3();
		return L_0;
	}
}
// System.Int32 GooglePlayGames.PlayGamesAchievement::get_totalSteps()
extern "C"  int32_t PlayGamesAchievement_get_totalSteps_m3902196415 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mTotalSteps_4();
		return L_0;
	}
}
// System.Double GooglePlayGames.PlayGamesAchievement::get_percentCompleted()
extern "C"  double PlayGamesAchievement_get_percentCompleted_m2462165463 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_mPercentComplete_5();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesAchievement::set_percentCompleted(System.Double)
extern "C"  void PlayGamesAchievement_set_percentCompleted_m870793946 (PlayGamesAchievement_t2357341912 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_mPercentComplete_5(L_0);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_completed()
extern "C"  bool PlayGamesAchievement_get_completed_m3462469643 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mCompleted_6();
		return L_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesAchievement::get_hidden()
extern "C"  bool PlayGamesAchievement_get_hidden_m1598629132 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mHidden_7();
		return L_0;
	}
}
// System.DateTime GooglePlayGames.PlayGamesAchievement::get_lastReportedDate()
extern "C"  DateTime_t4283661327  PlayGamesAchievement_get_lastReportedDate_m802603102 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_mLastModifiedTime_8();
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesAchievement::get_title()
extern "C"  String_t* PlayGamesAchievement_get_title_m3348342985 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mTitle_9();
		return L_0;
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesAchievement::get_image()
extern "C"  Texture2D_t3884108195 * PlayGamesAchievement_get_image_m3515279666 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = PlayGamesAchievement_LoadImage_m298890549(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesAchievement::get_achievedDescription()
extern "C"  String_t* PlayGamesAchievement_get_achievedDescription_m478251002 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mDescription_14();
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesAchievement::get_unachievedDescription()
extern "C"  String_t* PlayGamesAchievement_get_unachievedDescription_m439592705 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mDescription_14();
		return L_0;
	}
}
// System.Int32 GooglePlayGames.PlayGamesAchievement::get_points()
extern "C"  int32_t PlayGamesAchievement_get_points_m2562384607 (PlayGamesAchievement_t2357341912 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = __this->get_mPoints_15();
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Void GooglePlayGames.PlayGamesClientFactory::.ctor()
extern "C"  void PlayGamesClientFactory__ctor_m1251846287 (PlayGamesClientFactory_t1822448424 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.IPlayGamesClient GooglePlayGames.PlayGamesClientFactory::GetPlatformPlayGamesClient(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* DummyClient_t2268884141_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSClient_t3522634302_il2cpp_TypeInfo_var;
extern Il2CppClass* NativeClient_t3798002602_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1867057609;
extern Il2CppCodeGenString* _stringLiteral1892596041;
extern const uint32_t PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676 (Il2CppObject * __this /* static, unused */, PlayGamesClientConfiguration_t4135364200  ___config0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m1279348309(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral1867057609, /*hidden argument*/NULL);
		DummyClient_t2268884141 * L_1 = (DummyClient_t2268884141 *)il2cpp_codegen_object_new(DummyClient_t2268884141_il2cpp_TypeInfo_var);
		DummyClient__ctor_m320356400(L_1, /*hidden argument*/NULL);
		return L_1;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral1892596041, /*hidden argument*/NULL);
		PlayGamesClientConfiguration_t4135364200  L_2 = ___config0;
		IOSClient_t3522634302 * L_3 = (IOSClient_t3522634302 *)il2cpp_codegen_object_new(IOSClient_t3522634302_il2cpp_TypeInfo_var);
		IOSClient__ctor_m1866621838(L_3, /*hidden argument*/NULL);
		NativeClient_t3798002602 * L_4 = (NativeClient_t3798002602 *)il2cpp_codegen_object_new(NativeClient_t3798002602_il2cpp_TypeInfo_var);
		NativeClient__ctor_m3414341266(L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::.ctor(System.String)
extern Il2CppClass* List_1_t1854310091_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2512918218_MethodInfo_var;
extern const uint32_t PlayGamesLeaderboard__ctor_m4211124817_MetadataUsageId;
extern "C"  void PlayGamesLeaderboard__ctor_m4211124817 (PlayGamesLeaderboard_t3198617382 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard__ctor_m4211124817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1854310091 * L_0 = (List_1_t1854310091 *)il2cpp_codegen_object_new(List_1_t1854310091_il2cpp_TypeInfo_var);
		List_1__ctor_m2512918218(L_0, /*hidden argument*/List_1__ctor_m2512918218_MethodInfo_var);
		__this->set_mScoreList_8(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		__this->set_mId_0(L_1);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::SetUserFilter(System.String[])
extern "C"  void PlayGamesLeaderboard_SetUserFilter_m3491369932 (PlayGamesLeaderboard_t3198617382 * __this, StringU5BU5D_t4054002952* ___userIDs0, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = ___userIDs0;
		__this->set_mFilteredUserIds_4(L_0);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::LoadScores(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLeaderboard_LoadScores_m2462425657_MetadataUsageId;
extern "C"  void PlayGamesLeaderboard_LoadScores_m2462425657 (PlayGamesLeaderboard_t3198617382 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_LoadScores_m2462425657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = PlayGamesPlatform_get_Instance_m247033584(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		PlayGamesPlatform_LoadScores_m1968437709(L_0, __this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLeaderboard::get_loading()
extern "C"  bool PlayGamesLeaderboard_get_loading_m985440366 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mLoading_5();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::set_loading(System.Boolean)
extern "C"  void PlayGamesLeaderboard_set_loading_m1714962085 (PlayGamesLeaderboard_t3198617382 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_mLoading_5(L_0);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesLeaderboard::get_id()
extern "C"  String_t* PlayGamesLeaderboard_get_id_m896566458 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mId_0();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::set_id(System.String)
extern "C"  void PlayGamesLeaderboard_set_id_m199542423 (PlayGamesLeaderboard_t3198617382 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mId_0(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope GooglePlayGames.PlayGamesLeaderboard::get_userScope()
extern "C"  int32_t PlayGamesLeaderboard_get_userScope_m118555411 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mUserScope_1();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void PlayGamesLeaderboard_set_userScope_m485564192 (PlayGamesLeaderboard_t3198617382 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mUserScope_1(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range GooglePlayGames.PlayGamesLeaderboard::get_range()
extern "C"  Range_t1533311935  PlayGamesLeaderboard_get_range_m4196876027 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = __this->get_mRange_2();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void PlayGamesLeaderboard_set_range_m265156792 (PlayGamesLeaderboard_t3198617382 * __this, Range_t1533311935  ___value0, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = ___value0;
		__this->set_mRange_2(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope GooglePlayGames.PlayGamesLeaderboard::get_timeScope()
extern "C"  int32_t PlayGamesLeaderboard_get_timeScope_m176022927 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mTimeScope_3();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void PlayGamesLeaderboard_set_timeScope_m240527524 (PlayGamesLeaderboard_t3198617382 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mTimeScope_3(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.IScore GooglePlayGames.PlayGamesLeaderboard::get_localUserScore()
extern "C"  Il2CppObject * PlayGamesLeaderboard_get_localUserScore_m158861082 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_mLocalUserScore_6();
		return L_0;
	}
}
// System.UInt32 GooglePlayGames.PlayGamesLeaderboard::get_maxRange()
extern "C"  uint32_t PlayGamesLeaderboard_get_maxRange_m891048400 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_mMaxRange_7();
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.IScore[] GooglePlayGames.PlayGamesLeaderboard::get_scores()
extern Il2CppClass* PlayGamesScoreU5BU5D_t797504954_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m3836308316_MethodInfo_var;
extern const MethodInfo* List_1_CopyTo_m3221531033_MethodInfo_var;
extern const uint32_t PlayGamesLeaderboard_get_scores_m604031869_MetadataUsageId;
extern "C"  IScoreU5BU5D_t250104726* PlayGamesLeaderboard_get_scores_m604031869 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_get_scores_m604031869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayGamesScoreU5BU5D_t797504954* V_0 = NULL;
	{
		List_1_t1854310091 * L_0 = __this->get_mScoreList_8();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3836308316(L_0, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		V_0 = ((PlayGamesScoreU5BU5D_t797504954*)SZArrayNew(PlayGamesScoreU5BU5D_t797504954_il2cpp_TypeInfo_var, (uint32_t)L_1));
		List_1_t1854310091 * L_2 = __this->get_mScoreList_8();
		PlayGamesScoreU5BU5D_t797504954* L_3 = V_0;
		NullCheck(L_2);
		List_1_CopyTo_m3221531033(L_2, L_3, /*hidden argument*/List_1_CopyTo_m3221531033_MethodInfo_var);
		PlayGamesScoreU5BU5D_t797504954* L_4 = V_0;
		return (IScoreU5BU5D_t250104726*)L_4;
	}
}
// System.String GooglePlayGames.PlayGamesLeaderboard::get_title()
extern "C"  String_t* PlayGamesLeaderboard_get_title_m597770043 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mTitle_9();
		return L_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLeaderboard::SetFromData(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesScore_t486124539_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2949704259;
extern const uint32_t PlayGamesLeaderboard_SetFromData_m3542296026_MetadataUsageId;
extern "C"  bool PlayGamesLeaderboard_SetFromData_m3542296026 (PlayGamesLeaderboard_t3198617382 * __this, LeaderboardScoreData_t4006482697 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_SetFromData_m3542296026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	IScoreU5BU5D_t250104726* V_1 = NULL;
	int32_t V_2 = 0;
	PlayGamesLeaderboard_t3198617382 * G_B6_0 = NULL;
	PlayGamesLeaderboard_t3198617382 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	PlayGamesLeaderboard_t3198617382 * G_B7_1 = NULL;
	{
		LeaderboardScoreData_t4006482697 * L_0 = ___data0;
		NullCheck(L_0);
		bool L_1 = LeaderboardScoreData_get_Valid_m1568951177(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		LeaderboardScoreData_t4006482697 * L_2 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2949704259, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		LeaderboardScoreData_t4006482697 * L_4 = ___data0;
		NullCheck(L_4);
		uint64_t L_5 = LeaderboardScoreData_get_ApproximateCount_m1040175016(L_4, /*hidden argument*/NULL);
		PlayGamesLeaderboard_SetMaxRange_m3112923089(__this, L_5, /*hidden argument*/NULL);
		LeaderboardScoreData_t4006482697 * L_6 = ___data0;
		NullCheck(L_6);
		String_t* L_7 = LeaderboardScoreData_get_Title_m2388953492(L_6, /*hidden argument*/NULL);
		PlayGamesLeaderboard_SetTitle_m1221383737(__this, L_7, /*hidden argument*/NULL);
		LeaderboardScoreData_t4006482697 * L_8 = ___data0;
		NullCheck(L_8);
		Il2CppObject * L_9 = LeaderboardScoreData_get_PlayerScore_m1767200780(L_8, /*hidden argument*/NULL);
		PlayGamesLeaderboard_SetLocalUserScore_m2088895182(__this, ((PlayGamesScore_t486124539 *)CastclassClass(L_9, PlayGamesScore_t486124539_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		LeaderboardScoreData_t4006482697 * L_10 = ___data0;
		NullCheck(L_10);
		IScoreU5BU5D_t250104726* L_11 = LeaderboardScoreData_get_Scores_m1316040810(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		V_2 = 0;
		goto IL_0067;
	}

IL_0052:
	{
		IScoreU5BU5D_t250104726* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_0 = L_15;
		Il2CppObject * L_16 = V_0;
		PlayGamesLeaderboard_AddScore_m4205433299(__this, ((PlayGamesScore_t486124539 *)CastclassClass(L_16, PlayGamesScore_t486124539_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_18 = V_2;
		IScoreU5BU5D_t250104726* L_19 = V_1;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		LeaderboardScoreData_t4006482697 * L_20 = ___data0;
		NullCheck(L_20);
		IScoreU5BU5D_t250104726* L_21 = LeaderboardScoreData_get_Scores_m1316040810(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		G_B5_0 = __this;
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))
		{
			G_B6_0 = __this;
			goto IL_0086;
		}
	}
	{
		bool L_22 = PlayGamesLeaderboard_HasAllScores_m4175623983(__this, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_22));
		G_B7_1 = G_B5_0;
		goto IL_0087;
	}

IL_0086:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_0087:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_mLoading_5((bool)G_B7_0);
	}

IL_008c:
	{
		LeaderboardScoreData_t4006482697 * L_23 = ___data0;
		NullCheck(L_23);
		bool L_24 = LeaderboardScoreData_get_Valid_m1568951177(L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::SetMaxRange(System.UInt64)
extern "C"  void PlayGamesLeaderboard_SetMaxRange_m3112923089 (PlayGamesLeaderboard_t3198617382 * __this, uint64_t ___val0, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___val0;
		__this->set_mMaxRange_7((((int32_t)((uint32_t)L_0))));
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::SetTitle(System.String)
extern "C"  void PlayGamesLeaderboard_SetTitle_m1221383737 (PlayGamesLeaderboard_t3198617382 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mTitle_9(L_0);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLeaderboard::SetLocalUserScore(GooglePlayGames.PlayGamesScore)
extern "C"  void PlayGamesLeaderboard_SetLocalUserScore_m2088895182 (PlayGamesLeaderboard_t3198617382 * __this, PlayGamesScore_t486124539 * ___score0, const MethodInfo* method)
{
	{
		PlayGamesScore_t486124539 * L_0 = ___score0;
		__this->set_mLocalUserScore_6(L_0);
		return;
	}
}
// System.Int32 GooglePlayGames.PlayGamesLeaderboard::AddScore(GooglePlayGames.PlayGamesScore)
extern const MethodInfo* List_1_Add_m2207279034_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3836308316_MethodInfo_var;
extern const uint32_t PlayGamesLeaderboard_AddScore_m4205433299_MetadataUsageId;
extern "C"  int32_t PlayGamesLeaderboard_AddScore_m4205433299 (PlayGamesLeaderboard_t3198617382 * __this, PlayGamesScore_t486124539 * ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_AddScore_m4205433299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	int32_t V_2 = 0;
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_mFilteredUserIds_4();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		StringU5BU5D_t4054002952* L_1 = __this->get_mFilteredUserIds_4();
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_0029;
		}
	}

IL_0018:
	{
		List_1_t1854310091 * L_2 = __this->get_mScoreList_8();
		PlayGamesScore_t486124539 * L_3 = ___score0;
		NullCheck(L_2);
		List_1_Add_m2207279034(L_2, L_3, /*hidden argument*/List_1_Add_m2207279034_MethodInfo_var);
		goto IL_0071;
	}

IL_0029:
	{
		StringU5BU5D_t4054002952* L_4 = __this->get_mFilteredUserIds_4();
		V_1 = L_4;
		V_2 = 0;
		goto IL_005c;
	}

IL_0037:
	{
		StringU5BU5D_t4054002952* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = L_8;
		String_t* L_9 = V_0;
		PlayGamesScore_t486124539 * L_10 = ___score0;
		NullCheck(L_10);
		String_t* L_11 = PlayGamesScore_get_userID_m608145498(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_12 = String_Equals_m3541721061(L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		List_1_t1854310091 * L_13 = __this->get_mScoreList_8();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m3836308316(L_13, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		return L_14;
	}

IL_0058:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_2;
		StringU5BU5D_t4054002952* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1854310091 * L_18 = __this->get_mScoreList_8();
		PlayGamesScore_t486124539 * L_19 = ___score0;
		NullCheck(L_18);
		List_1_Add_m2207279034(L_18, L_19, /*hidden argument*/List_1_Add_m2207279034_MethodInfo_var);
	}

IL_0071:
	{
		List_1_t1854310091 * L_20 = __this->get_mScoreList_8();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m3836308316(L_20, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		return L_21;
	}
}
// System.Int32 GooglePlayGames.PlayGamesLeaderboard::get_ScoreCount()
extern const MethodInfo* List_1_get_Count_m3836308316_MethodInfo_var;
extern const uint32_t PlayGamesLeaderboard_get_ScoreCount_m3010022535_MetadataUsageId;
extern "C"  int32_t PlayGamesLeaderboard_get_ScoreCount_m3010022535 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_get_ScoreCount_m3010022535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1854310091 * L_0 = __this->get_mScoreList_8();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3836308316(L_0, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLeaderboard::HasAllScores()
extern const MethodInfo* List_1_get_Count_m3836308316_MethodInfo_var;
extern const uint32_t PlayGamesLeaderboard_HasAllScores_m4175623983_MetadataUsageId;
extern "C"  bool PlayGamesLeaderboard_HasAllScores_m4175623983 (PlayGamesLeaderboard_t3198617382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLeaderboard_HasAllScores_m4175623983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		List_1_t1854310091 * L_0 = __this->get_mScoreList_8();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3836308316(L_0, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		Range_t1533311935 * L_2 = __this->get_address_of_mRange_2();
		int32_t L_3 = L_2->get_count_1();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0035;
		}
	}
	{
		List_1_t1854310091 * L_4 = __this->get_mScoreList_8();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m3836308316(L_4, /*hidden argument*/List_1_get_Count_m3836308316_MethodInfo_var);
		uint32_t L_6 = PlayGamesLeaderboard_get_maxRange_m891048400(__this, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int64_t)(((int64_t)((int64_t)L_5)))) < ((int64_t)(((int64_t)((uint64_t)L_6)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 1;
	}

IL_0036:
	{
		return (bool)G_B3_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::.ctor(GooglePlayGames.PlayGamesPlatform)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3089797590;
extern const uint32_t PlayGamesLocalUser__ctor_m3952843924_MetadataUsageId;
extern "C"  void PlayGamesLocalUser__ctor_m3952843924 (PlayGamesLocalUser_t4064039103 * __this, PlayGamesPlatform_t3624292130 * ___plaf0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser__ctor_m3952843924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		PlayGamesUserProfile__ctor_m3655178218(__this, _stringLiteral3089797590, L_0, L_1, /*hidden argument*/NULL);
		PlayGamesPlatform_t3624292130 * L_2 = ___plaf0;
		__this->set_mPlatform_5(L_2);
		__this->set_emailAddress_6((String_t*)NULL);
		__this->set_mStats_7((PlayerStats_t60064856 *)NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesLocalUser_Authenticate_m3099382162 (PlayGamesLocalUser_t4064039103 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_mPlatform_5();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		PlayGamesPlatform_Authenticate_m193306015(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C"  void PlayGamesLocalUser_Authenticate_m4267174827 (PlayGamesLocalUser_t4064039103 * __this, Action_1_t872614854 * ___callback0, bool ___silent1, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_mPlatform_5();
		Action_1_t872614854 * L_1 = ___callback0;
		bool L_2 = ___silent1;
		NullCheck(L_0);
		PlayGamesPlatform_Authenticate_m1942348798(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::LoadFriends(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesLocalUser_LoadFriends_m1928733902 (PlayGamesLocalUser_t4064039103 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_mPlatform_5();
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		PlayGamesPlatform_LoadFriends_m3849896064(L_0, __this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.PlayGamesLocalUser::get_friends()
extern "C"  IUserProfileU5BU5D_t3419104218* PlayGamesLocalUser_get_friends_m728743214 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_mPlatform_5();
		NullCheck(L_0);
		IUserProfileU5BU5D_t3419104218* L_1 = PlayGamesPlatform_GetFriends_m3329218634(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::GetIdToken(System.Action`1<System.String>)
extern const MethodInfo* Action_1_Invoke_m3709300246_MethodInfo_var;
extern const uint32_t PlayGamesLocalUser_GetIdToken_m4167317808_MetadataUsageId;
extern "C"  void PlayGamesLocalUser_GetIdToken_m4167317808 (PlayGamesLocalUser_t4064039103 * __this, Action_1_t403047693 * ___idTokenCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_GetIdToken_m4167317808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_1 = __this->get_mPlatform_5();
		Action_1_t403047693 * L_2 = ___idTokenCallback0;
		NullCheck(L_1);
		PlayGamesPlatform_GetIdToken_m1555869763(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_001c:
	{
		Action_1_t403047693 * L_3 = ___idTokenCallback0;
		NullCheck(L_3);
		Action_1_Invoke_m3709300246(L_3, (String_t*)NULL, /*hidden argument*/Action_1_Invoke_m3709300246_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_authenticated()
extern "C"  bool PlayGamesLocalUser_get_authenticated_m4184744680 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_mPlatform_5();
		NullCheck(L_0);
		bool L_1 = PlayGamesPlatform_IsAuthenticated_m2538886108(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_underage()
extern "C"  bool PlayGamesLocalUser_get_underage_m2889300464 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_userName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLocalUser_get_userName_m1023461806_MetadataUsageId;
extern "C"  String_t* PlayGamesLocalUser_get_userName_m1023461806 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_get_userName_m1023461806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		bool L_1 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_2 = __this->get_mPlatform_5();
		NullCheck(L_2);
		String_t* L_3 = PlayGamesPlatform_GetUserDisplayName_m3773014120(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = PlayGamesUserProfile_get_userName_m2379353398(__this, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = String_Equals_m3541721061(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_7 = V_0;
		PlayGamesPlatform_t3624292130 * L_8 = __this->get_mPlatform_5();
		NullCheck(L_8);
		String_t* L_9 = PlayGamesPlatform_GetUserId_m3348234722(L_8, /*hidden argument*/NULL);
		PlayGamesPlatform_t3624292130 * L_10 = __this->get_mPlatform_5();
		NullCheck(L_10);
		String_t* L_11 = PlayGamesPlatform_GetUserImageUrl_m3289874459(L_10, /*hidden argument*/NULL);
		PlayGamesUserProfile_ResetIdentity_m1865931839(__this, L_7, L_9, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_id()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLocalUser_get_id_m2596166099_MetadataUsageId;
extern "C"  String_t* PlayGamesLocalUser_get_id_m2596166099 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_get_id_m2596166099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		bool L_1 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_2 = __this->get_mPlatform_5();
		NullCheck(L_2);
		String_t* L_3 = PlayGamesPlatform_GetUserId_m3348234722(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = PlayGamesUserProfile_get_id_m613081947(__this, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = String_Equals_m3541721061(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_7 = __this->get_mPlatform_5();
		NullCheck(L_7);
		String_t* L_8 = PlayGamesPlatform_GetUserDisplayName_m3773014120(L_7, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		PlayGamesPlatform_t3624292130 * L_10 = __this->get_mPlatform_5();
		NullCheck(L_10);
		String_t* L_11 = PlayGamesPlatform_GetUserImageUrl_m3289874459(L_10, /*hidden argument*/NULL);
		PlayGamesUserProfile_ResetIdentity_m1865931839(__this, L_8, L_9, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_accessToken()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLocalUser_get_accessToken_m4125991455_MetadataUsageId;
extern "C"  String_t* PlayGamesLocalUser_get_accessToken_m4125991455 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_get_accessToken_m4125991455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_1 = __this->get_mPlatform_5();
		NullCheck(L_1);
		String_t* L_2 = PlayGamesPlatform_GetAccessToken_m2849472379(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0020;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
	}

IL_0020:
	{
		return G_B3_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_isFriend()
extern "C"  bool PlayGamesLocalUser_get_isFriend_m2454307569 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesLocalUser::get_state()
extern "C"  int32_t PlayGamesLocalUser_get_state_m570677023 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_AvatarURL()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLocalUser_get_AvatarURL_m2297398880_MetadataUsageId;
extern "C"  String_t* PlayGamesLocalUser_get_AvatarURL_m2297398880 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_get_AvatarURL_m2297398880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		bool L_1 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_2 = __this->get_mPlatform_5();
		NullCheck(L_2);
		String_t* L_3 = PlayGamesPlatform_GetUserImageUrl_m3289874459(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = PlayGamesUserProfile_get_id_m613081947(__this, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = String_Equals_m3541721061(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_7 = __this->get_mPlatform_5();
		NullCheck(L_7);
		String_t* L_8 = PlayGamesPlatform_GetUserDisplayName_m3773014120(L_7, /*hidden argument*/NULL);
		PlayGamesPlatform_t3624292130 * L_9 = __this->get_mPlatform_5();
		NullCheck(L_9);
		String_t* L_10 = PlayGamesPlatform_GetUserId_m3348234722(L_9, /*hidden argument*/NULL);
		String_t* L_11 = V_0;
		PlayGamesUserProfile_ResetIdentity_m1865931839(__this, L_8, L_10, L_11, /*hidden argument*/NULL);
	}

IL_004b:
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.String GooglePlayGames.PlayGamesLocalUser::get_Email()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesLocalUser_get_Email_m1335109542_MetadataUsageId;
extern "C"  String_t* PlayGamesLocalUser_get_Email_m1335109542 (PlayGamesLocalUser_t4064039103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_get_Email_m1335109542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = NULL;
	PlayGamesLocalUser_t4064039103 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	PlayGamesLocalUser_t4064039103 * G_B3_1 = NULL;
	String_t* G_B8_0 = NULL;
	{
		bool L_0 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_1 = __this->get_emailAddress_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		PlayGamesPlatform_t3624292130 * L_3 = __this->get_mPlatform_5();
		NullCheck(L_3);
		String_t* L_4 = PlayGamesPlatform_GetUserEmail_m1997388119(L_3, /*hidden argument*/NULL);
		__this->set_emailAddress_6(L_4);
		String_t* L_5 = __this->get_emailAddress_6();
		String_t* L_6 = L_5;
		G_B3_0 = L_6;
		G_B3_1 = __this;
		if (L_6)
		{
			G_B4_0 = L_6;
			G_B4_1 = __this;
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_7;
		G_B4_1 = G_B3_1;
	}

IL_003f:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_emailAddress_6(G_B4_0);
	}

IL_0044:
	{
		bool L_8 = PlayGamesLocalUser_get_authenticated_m4184744680(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_9 = __this->get_emailAddress_6();
		G_B8_0 = L_9;
		goto IL_005f;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_10;
	}

IL_005f:
	{
		return G_B8_0;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser::GetStats(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>)
extern Il2CppClass* U3CGetStatsU3Ec__AnonStorey34_t1599266691_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1913238692_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1274431257_MethodInfo_var;
extern const MethodInfo* Action_2_Invoke_m514882738_MethodInfo_var;
extern const uint32_t PlayGamesLocalUser_GetStats_m2172969827_MetadataUsageId;
extern "C"  void PlayGamesLocalUser_GetStats_m2172969827 (PlayGamesLocalUser_t4064039103 * __this, Action_2_t1913238692 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesLocalUser_GetStats_m2172969827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetStatsU3Ec__AnonStorey34_t1599266691 * V_0 = NULL;
	{
		U3CGetStatsU3Ec__AnonStorey34_t1599266691 * L_0 = (U3CGetStatsU3Ec__AnonStorey34_t1599266691 *)il2cpp_codegen_object_new(U3CGetStatsU3Ec__AnonStorey34_t1599266691_il2cpp_TypeInfo_var);
		U3CGetStatsU3Ec__AnonStorey34__ctor_m545212024(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetStatsU3Ec__AnonStorey34_t1599266691 * L_1 = V_0;
		Action_2_t1913238692 * L_2 = ___callback0;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		U3CGetStatsU3Ec__AnonStorey34_t1599266691 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		PlayerStats_t60064856 * L_4 = __this->get_mStats_7();
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		PlayerStats_t60064856 * L_5 = __this->get_mStats_7();
		NullCheck(L_5);
		bool L_6 = PlayerStats_get_Valid_m4027792866(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}

IL_002f:
	{
		PlayGamesPlatform_t3624292130 * L_7 = __this->get_mPlatform_5();
		U3CGetStatsU3Ec__AnonStorey34_t1599266691 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733_MethodInfo_var);
		Action_2_t1913238692 * L_10 = (Action_2_t1913238692 *)il2cpp_codegen_object_new(Action_2_t1913238692_il2cpp_TypeInfo_var);
		Action_2__ctor_m1274431257(L_10, L_8, L_9, /*hidden argument*/Action_2__ctor_m1274431257_MethodInfo_var);
		NullCheck(L_7);
		PlayGamesPlatform_GetPlayerStats_m3541911125(L_7, L_10, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_004b:
	{
		U3CGetStatsU3Ec__AnonStorey34_t1599266691 * L_11 = V_0;
		NullCheck(L_11);
		Action_2_t1913238692 * L_12 = L_11->get_callback_0();
		PlayerStats_t60064856 * L_13 = __this->get_mStats_7();
		NullCheck(L_12);
		Action_2_Invoke_m514882738(L_12, 0, L_13, /*hidden argument*/Action_2_Invoke_m514882738_MethodInfo_var);
	}

IL_005d:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34::.ctor()
extern "C"  void U3CGetStatsU3Ec__AnonStorey34__ctor_m545212024 (U3CGetStatsU3Ec__AnonStorey34_t1599266691 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34::<>m__3(GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats)
extern const MethodInfo* Action_2_Invoke_m514882738_MethodInfo_var;
extern const uint32_t U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733_MetadataUsageId;
extern "C"  void U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733 (U3CGetStatsU3Ec__AnonStorey34_t1599266691 * __this, int32_t ___rc0, PlayerStats_t60064856 * ___stats1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayGamesLocalUser_t4064039103 * L_0 = __this->get_U3CU3Ef__this_1();
		PlayerStats_t60064856 * L_1 = ___stats1;
		NullCheck(L_0);
		L_0->set_mStats_7(L_1);
		Action_2_t1913238692 * L_2 = __this->get_callback_0();
		int32_t L_3 = ___rc0;
		PlayerStats_t60064856 * L_4 = ___stats1;
		NullCheck(L_2);
		Action_2_Invoke_m514882738(L_2, L_3, L_4, /*hidden argument*/Action_2_Invoke_m514882738_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.IPlayGamesClient)
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Misc_CheckNotNull_TisIPlayGamesClient_t2528289561_m747065152_MethodInfo_var;
extern const uint32_t PlayGamesPlatform__ctor_m2037393110_MetadataUsageId;
extern "C"  void PlayGamesPlatform__ctor_m2037393110 (PlayGamesPlatform_t3624292130 * __this, Il2CppObject * ___client0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform__ctor_m2037393110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t827649927 * L_0 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		__this->set_mIdMap_7(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___client0;
		Il2CppObject * L_2 = Misc_CheckNotNull_TisIPlayGamesClient_t2528289561_m747065152(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisIPlayGamesClient_t2528289561_m747065152_MethodInfo_var);
		__this->set_mClient_5(L_2);
		PlayGamesLocalUser_t4064039103 * L_3 = (PlayGamesLocalUser_t4064039103 *)il2cpp_codegen_object_new(PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var);
		PlayGamesLocalUser__ctor_m3952843924(L_3, __this, /*hidden argument*/NULL);
		__this->set_mLocalUser_4(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var);
		PlayGamesClientConfiguration_t4135364200  L_4 = ((PlayGamesClientConfiguration_t4135364200_StaticFields*)PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var->static_fields)->get_DefaultConfiguration_0();
		__this->set_mConfiguration_3(L_4);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t PlayGamesPlatform__ctor_m3086690151_MetadataUsageId;
extern "C"  void PlayGamesPlatform__ctor_m3086690151 (PlayGamesPlatform_t3624292130 * __this, PlayGamesClientConfiguration_t4135364200  ___configuration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform__ctor_m3086690151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t827649927 * L_0 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		__this->set_mIdMap_7(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		PlayGamesLocalUser_t4064039103 * L_1 = (PlayGamesLocalUser_t4064039103 *)il2cpp_codegen_object_new(PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var);
		PlayGamesLocalUser__ctor_m3952843924(L_1, __this, /*hidden argument*/NULL);
		__this->set_mLocalUser_4(L_1);
		PlayGamesClientConfiguration_t4135364200  L_2 = ___configuration0;
		__this->set_mConfiguration_3(L_2);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::.cctor()
extern "C"  void PlayGamesPlatform__cctor_m2735946152 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::get_DebugLogEnabled()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_DebugLogEnabled_m2560835678_MetadataUsageId;
extern "C"  bool PlayGamesPlatform_get_DebugLogEnabled_m2560835678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_DebugLogEnabled_m2560835678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		bool L_0 = Logger_get_DebugLogEnabled_m3753664069(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::set_DebugLogEnabled(System.Boolean)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_set_DebugLogEnabled_m1455586989_MetadataUsageId;
extern "C"  void PlayGamesPlatform_set_DebugLogEnabled_m1455586989 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_set_DebugLogEnabled_m1455586989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_set_DebugLogEnabled_m2155831124(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::get_Instance()
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2470289260;
extern const uint32_t PlayGamesPlatform_get_Instance_m247033584_MetadataUsageId;
extern "C"  PlayGamesPlatform_t3624292130 * PlayGamesPlatform_get_Instance_m247033584 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_Instance_m247033584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sInstance_0();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral2470289260, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var);
		PlayGamesClientConfiguration_t4135364200  L_1 = ((PlayGamesClientConfiguration_t4135364200_StaticFields*)PlayGamesClientConfiguration_t4135364200_il2cpp_TypeInfo_var->static_fields)->get_DefaultConfiguration_0();
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_InitializeInstance_m2009168616(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_2 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sInstance_0();
		il2cpp_codegen_memory_barrier();
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient GooglePlayGames.PlayGamesPlatform::get_Nearby()
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_Nearby_m3819147390_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_Nearby_m3819147390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_Nearby_m3819147390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyConnectionClient_2();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		bool L_1 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyInitializePending_1();
		il2cpp_codegen_memory_barrier();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->set_sNearbyInitializePending_1(1);
		PlayGamesPlatform_InitializeNearby_m2278615940(NULL /*static, unused*/, (Action_1_t2128016239 *)NULL, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyConnectionClient_2();
		il2cpp_codegen_memory_barrier();
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_RealTime()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_RealTime_m2455404397_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_RealTime_m2455404397 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_RealTime_m2455404397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(28 /* GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.BasicApi.IPlayGamesClient::GetRtmpClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_TurnBased()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_TurnBased_m1184567569_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_TurnBased_m1184567569 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_TurnBased_m1184567569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(29 /* GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.BasicApi.IPlayGamesClient::GetTbmpClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.PlayGamesPlatform::get_SavedGame()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_SavedGame_m2856314446_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_SavedGame_m2856314446 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_SavedGame_m2856314446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(30 /* GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.BasicApi.IPlayGamesClient::GetSavedGameClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.PlayGamesPlatform::get_Events()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_Events_m971873230_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_Events_m971873230 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_Events_m971873230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(31 /* GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.BasicApi.IPlayGamesClient::GetEventsClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.PlayGamesPlatform::get_Quests()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_get_Quests_m3579546102_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_get_Quests_m3579546102 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_get_Quests_m3579546102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(32 /* GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.BasicApi.IPlayGamesClient::GetQuestsClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser GooglePlayGames.PlayGamesPlatform::get_localUser()
extern "C"  Il2CppObject * PlayGamesPlatform_get_localUser_m2126881742 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	{
		PlayGamesLocalUser_t4064039103 * L_0 = __this->get_mLocalUser_4();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeInstance(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3808032353;
extern const uint32_t PlayGamesPlatform_InitializeInstance_m2009168616_MetadataUsageId;
extern "C"  void PlayGamesPlatform_InitializeInstance_m2009168616 (Il2CppObject * __this /* static, unused */, PlayGamesClientConfiguration_t4135364200  ___configuration0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_InitializeInstance_m2009168616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sInstance_0();
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_w_m3419267169(NULL /*static, unused*/, _stringLiteral3808032353, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		PlayGamesClientConfiguration_t4135364200  L_1 = ___configuration0;
		PlayGamesPlatform_t3624292130 * L_2 = (PlayGamesPlatform_t3624292130 *)il2cpp_codegen_object_new(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform__ctor_m3086690151(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->set_sInstance_0(L_2);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeNearby(System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern Il2CppClass* DummyNearbyConnectionClient_t2104311078_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2835899548_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2087068470;
extern Il2CppCodeGenString* _stringLiteral538637604;
extern Il2CppCodeGenString* _stringLiteral4162937835;
extern const uint32_t PlayGamesPlatform_InitializeNearby_m2278615940_MetadataUsageId;
extern "C"  void PlayGamesPlatform_InitializeNearby_m2278615940 (Il2CppObject * __this /* static, unused */, Action_1_t2128016239 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_InitializeNearby_m2278615940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2087068470, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyConnectionClient_2();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		DummyNearbyConnectionClient_t2104311078 * L_1 = (DummyNearbyConnectionClient_t2104311078 *)il2cpp_codegen_object_new(DummyNearbyConnectionClient_t2104311078_il2cpp_TypeInfo_var);
		DummyNearbyConnectionClient__ctor_m2255000610(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		il2cpp_codegen_memory_barrier();
		((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->set_sNearbyConnectionClient_2(L_1);
		Action_1_t2128016239 * L_2 = ___callback0;
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		Action_1_t2128016239 * L_3 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyConnectionClient_2();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_3);
		Action_1_Invoke_m2835899548(L_3, L_4, /*hidden argument*/Action_1_Invoke_m2835899548_MethodInfo_var);
	}

IL_0035:
	{
		goto IL_0066;
	}

IL_003a:
	{
		Action_1_t2128016239 * L_5 = ___callback0;
		if (!L_5)
		{
			goto IL_005c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral538637604, /*hidden argument*/NULL);
		Action_1_t2128016239 * L_6 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		Il2CppObject * L_7 = ((PlayGamesPlatform_t3624292130_StaticFields*)PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var->static_fields)->get_sNearbyConnectionClient_2();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_6);
		Action_1_Invoke_m2835899548(L_6, L_7, /*hidden argument*/Action_1_Invoke_m2835899548_MethodInfo_var);
		goto IL_0066;
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral4162937835, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::Activate()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral200189406;
extern Il2CppCodeGenString* _stringLiteral1787077623;
extern const uint32_t PlayGamesPlatform_Activate_m3016211397_MetadataUsageId;
extern "C"  PlayGamesPlatform_t3624292130 * PlayGamesPlatform_Activate_m3016211397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_Activate_m3016211397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral200189406, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = PlayGamesPlatform_get_Instance_m247033584(NULL /*static, unused*/, /*hidden argument*/NULL);
		Social_set_Active_m3574014644(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = Social_get_Active_m590102927(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1787077623, L_1, /*hidden argument*/NULL);
		Logger_d_m234514644(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		PlayGamesPlatform_t3624292130 * L_3 = PlayGamesPlatform_get_Instance_m247033584(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.IntPtr GooglePlayGames.PlayGamesPlatform::GetApiClient()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetApiClient_m2667095353_MetadataUsageId;
extern "C"  IntPtr_t PlayGamesPlatform_GetApiClient_m2667095353 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetApiClient_m2667095353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		IntPtr_t L_1 = InterfaceFuncInvoker0< IntPtr_t >::Invoke(35 /* System.IntPtr GooglePlayGames.BasicApi.IPlayGamesClient::GetApiClient() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::AddIdMapping(System.String,System.String)
extern const MethodInfo* Dictionary_2_set_Item_m439360998_MethodInfo_var;
extern const uint32_t PlayGamesPlatform_AddIdMapping_m3155954189_MetadataUsageId;
extern "C"  void PlayGamesPlatform_AddIdMapping_m3155954189 (PlayGamesPlatform_t3624292130 * __this, String_t* ___fromId0, String_t* ___toId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_AddIdMapping_m3155954189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t827649927 * L_0 = __this->get_mIdMap_7();
		String_t* L_1 = ___fromId0;
		String_t* L_2 = ___toId1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m439360998(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m439360998_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>)
extern "C"  void PlayGamesPlatform_Authenticate_m193306015 (PlayGamesPlatform_t3624292130 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	{
		Action_1_t872614854 * L_0 = ___callback0;
		PlayGamesPlatform_Authenticate_m1942348798(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3627912115;
extern const uint32_t PlayGamesPlatform_Authenticate_m1942348798_MetadataUsageId;
extern "C"  void PlayGamesPlatform_Authenticate_m1942348798 (PlayGamesPlatform_t3624292130 * __this, Action_1_t872614854 * ___callback0, bool ___silent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_Authenticate_m1942348798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral3627912115, /*hidden argument*/NULL);
		PlayGamesClientConfiguration_t4135364200  L_1 = __this->get_mConfiguration_3();
		Il2CppObject * L_2 = PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_mClient_5(L_2);
	}

IL_0026:
	{
		Il2CppObject * L_3 = __this->get_mClient_5();
		Action_1_t872614854 * L_4 = ___callback0;
		bool L_5 = ___silent1;
		NullCheck(L_3);
		InterfaceActionInvoker2< Action_1_t872614854 *, bool >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_3, L_4, L_5);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern "C"  void PlayGamesPlatform_Authenticate_m193434690 (PlayGamesPlatform_t3624292130 * __this, Il2CppObject * ___unused0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	{
		Action_1_t872614854 * L_0 = ___callback1;
		PlayGamesPlatform_Authenticate_m1942348798(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::IsAuthenticated()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_IsAuthenticated_m2538886108_MetadataUsageId;
extern "C"  bool PlayGamesPlatform_IsAuthenticated_m2538886108 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_IsAuthenticated_m2538886108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean GooglePlayGames.BasicApi.IPlayGamesClient::IsAuthenticated() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::SignOut()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_SignOut_m1679456084_MetadataUsageId;
extern "C"  void PlayGamesPlatform_SignOut_m1679456084 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_SignOut_m1679456084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(2 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SignOut() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		PlayGamesLocalUser_t4064039103 * L_2 = (PlayGamesLocalUser_t4064039103 *)il2cpp_codegen_object_new(PlayGamesLocalUser_t4064039103_il2cpp_TypeInfo_var);
		PlayGamesLocalUser__ctor_m3952843924(L_2, __this, /*hidden argument*/NULL);
		__this->set_mLocalUser_4(L_2);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IUserProfileU5BU5D_t3419104218_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2263799883;
extern const uint32_t PlayGamesPlatform_LoadUsers_m2176466248_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadUsers_m2176466248 (PlayGamesPlatform_t3624292130 * __this, StringU5BU5D_t4054002952* ___userIds0, Action_1_t3814920354 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadUsers_m2176466248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral2263799883, /*hidden argument*/NULL);
		Action_1_t3814920354 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, ((IUserProfileU5BU5D_t3419104218*)SZArrayNew(IUserProfileU5BU5D_t3419104218_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
		return;
	}

IL_0022:
	{
		Il2CppObject * L_2 = __this->get_mClient_5();
		StringU5BU5D_t4054002952* L_3 = ___userIds0;
		Action_1_t3814920354 * L_4 = ___callback1;
		NullCheck(L_2);
		InterfaceActionInvoker2< StringU5BU5D_t4054002952*, Action_1_t3814920354 * >::Invoke(14 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserId()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2263799883;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t PlayGamesPlatform_GetUserId_m3348234722_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetUserId_m3348234722 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetUserId_m3348234722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral2263799883, /*hidden argument*/NULL);
		return _stringLiteral48;
	}

IL_001b:
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserId() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::GetIdToken(System.Action`1<System.String>)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3709300246_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2789444483;
extern const uint32_t PlayGamesPlatform_GetIdToken_m1555869763_MetadataUsageId;
extern "C"  void PlayGamesPlatform_GetIdToken_m1555869763 (PlayGamesPlatform_t3624292130 * __this, Action_1_t403047693 * ___idTokenCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetIdToken_m1555869763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		Action_1_t403047693 * L_2 = ___idTokenCallback0;
		NullCheck(L_1);
		InterfaceActionInvoker1< Action_1_t403047693 * >::Invoke(7 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::GetIdToken(System.Action`1<System.String>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1, L_2);
		goto IL_002d;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral2789444483, /*hidden argument*/NULL);
		Action_1_t403047693 * L_3 = ___idTokenCallback0;
		NullCheck(L_3);
		Action_1_Invoke_m3709300246(L_3, (String_t*)NULL, /*hidden argument*/Action_1_Invoke_m3709300246_MethodInfo_var);
	}

IL_002d:
	{
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetAccessToken()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetAccessToken_m2849472379_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetAccessToken_m2849472379 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetAccessToken_m2849472379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(8 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetAccessToken() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}

IL_0017:
	{
		return (String_t*)NULL;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::GetServerAuthCode(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m1185907408_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3792450056;
extern Il2CppCodeGenString* _stringLiteral2719625363;
extern Il2CppCodeGenString* _stringLiteral1933467034;
extern const uint32_t PlayGamesPlatform_GetServerAuthCode_m2525601181_MetadataUsageId;
extern "C"  void PlayGamesPlatform_GetServerAuthCode_m2525601181 (PlayGamesPlatform_t3624292130 * __this, Action_2_t1860405393 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetServerAuthCode_m2525601181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_0057;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean GooglePlayGames.BasicApi.IPlayGamesClient::IsAuthenticated() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		if (!L_2)
		{
			goto IL_0057;
		}
	}
	{
		bool L_3 = GameInfo_WebClientIdInitialized_m2118387633(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_4 = __this->get_mClient_5();
		Action_2_t1860405393 * L_5 = ___callback0;
		NullCheck(L_4);
		InterfaceActionInvoker2< String_t*, Action_2_t1860405393 * >::Invoke(9 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::GetServerAuthCode(System.String,System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_4, _stringLiteral3792450056, L_5);
		goto IL_0052;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral2719625363, /*hidden argument*/NULL);
		Action_2_t1860405393 * L_6 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_6);
		Action_2_Invoke_m1185907408(L_6, ((int32_t)10), L_7, /*hidden argument*/Action_2_Invoke_m1185907408_MethodInfo_var);
	}

IL_0052:
	{
		goto IL_006d;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1933467034, /*hidden argument*/NULL);
		Action_2_t1860405393 * L_8 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_8);
		Action_2_Invoke_m1185907408(L_8, 4, L_9, /*hidden argument*/Action_2_Invoke_m1185907408_MethodInfo_var);
	}

IL_006d:
	{
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserEmail()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetUserEmail_m1997388119_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetUserEmail_m1997388119 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetUserEmail_m1997388119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(10 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserEmail() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}

IL_0017:
	{
		return (String_t*)NULL;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::GetUserEmail(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetUserEmail_m3529081076_MetadataUsageId;
extern "C"  void PlayGamesPlatform_GetUserEmail_m3529081076 (PlayGamesPlatform_t3624292130 * __this, Action_2_t1860405393 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetUserEmail_m3529081076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		Action_2_t1860405393 * L_1 = ___callback0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Action_2_t1860405393 * >::Invoke(11 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::GetUserEmail(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::GetPlayerStats(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerStats_t60064856_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m514882738_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4185919136;
extern const uint32_t PlayGamesPlatform_GetPlayerStats_m3541911125_MetadataUsageId;
extern "C"  void PlayGamesPlatform_GetPlayerStats_m3541911125 (PlayGamesPlatform_t3624292130 * __this, Action_2_t1913238692 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetPlayerStats_m3541911125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean GooglePlayGames.BasicApi.IPlayGamesClient::IsAuthenticated() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = __this->get_mClient_5();
		Action_2_t1913238692 * L_4 = ___callback0;
		NullCheck(L_3);
		InterfaceActionInvoker1< Action_2_t1913238692 * >::Invoke(13 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::GetPlayerStats(System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_3, L_4);
		goto IL_0042;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral4185919136, /*hidden argument*/NULL);
		Action_2_t1913238692 * L_5 = ___callback0;
		PlayerStats_t60064856 * L_6 = (PlayerStats_t60064856 *)il2cpp_codegen_object_new(PlayerStats_t60064856_il2cpp_TypeInfo_var);
		PlayerStats__ctor_m3220740325(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Action_2_Invoke_m514882738(L_5, 4, L_6, /*hidden argument*/Action_2_Invoke_m514882738_MethodInfo_var);
	}

IL_0042:
	{
		return;
	}
}
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.PlayGamesPlatform::GetAchievement(System.String)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3865993423;
extern const uint32_t PlayGamesPlatform_GetAchievement_m1603747456_MetadataUsageId;
extern "C"  Achievement_t1261647177 * PlayGamesPlatform_GetAchievement_m1603747456 (PlayGamesPlatform_t3624292130 * __this, String_t* ___achievementId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetAchievement_m1603747456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral3865993423, /*hidden argument*/NULL);
		return (Achievement_t1261647177 *)NULL;
	}

IL_0017:
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		String_t* L_2 = ___achievementId0;
		NullCheck(L_1);
		Achievement_t1261647177 * L_3 = InterfaceFuncInvoker1< Achievement_t1261647177 *, String_t* >::Invoke(15 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.IPlayGamesClient::GetAchievement(System.String) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1, L_2);
		return L_3;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserDisplayName()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1676188860;
extern const uint32_t PlayGamesPlatform_GetUserDisplayName_m3773014120_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetUserDisplayName_m3773014120 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetUserDisplayName_m3773014120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1676188860, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_001b:
	{
		Il2CppObject * L_2 = __this->get_mClient_5();
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(6 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserDisplayName() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetUserImageUrl()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1895978259;
extern const uint32_t PlayGamesPlatform_GetUserImageUrl_m3289874459_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetUserImageUrl_m3289874459 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetUserImageUrl_m3289874459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1895978259, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_0017:
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(12 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetUserImageUrl() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2913044551;
extern Il2CppCodeGenString* _stringLiteral1306358997;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral3521753496;
extern Il2CppCodeGenString* _stringLiteral1596265191;
extern Il2CppCodeGenString* _stringLiteral1017031286;
extern Il2CppCodeGenString* _stringLiteral3917757349;
extern Il2CppCodeGenString* _stringLiteral3238333370;
extern Il2CppCodeGenString* _stringLiteral2095255229;
extern Il2CppCodeGenString* _stringLiteral3528602822;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral1035314035;
extern Il2CppCodeGenString* _stringLiteral1055423392;
extern Il2CppCodeGenString* _stringLiteral3363482478;
extern Il2CppCodeGenString* _stringLiteral3973014814;
extern Il2CppCodeGenString* _stringLiteral3567858759;
extern Il2CppCodeGenString* _stringLiteral2897568489;
extern Il2CppCodeGenString* _stringLiteral14957008;
extern Il2CppCodeGenString* _stringLiteral2544523935;
extern const uint32_t PlayGamesPlatform_ReportProgress_m1748764843_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ReportProgress_m1748764843 (PlayGamesPlatform_t3624292130 * __this, String_t* ___achievementID0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ReportProgress_m1748764843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Achievement_t1261647177 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* G_B10_0 = NULL;
	String_t* G_B9_0 = NULL;
	String_t* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral2913044551, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral1306358997);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1306358997);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = ___achievementID0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1396);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		double L_8 = ___progress1;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___achievementID0;
		String_t* L_13 = PlayGamesPlatform_MapId_m1357306405(__this, L_12, /*hidden argument*/NULL);
		___achievementID0 = L_13;
		double L_14 = ___progress1;
		if ((!(((double)L_14) < ((double)(1.0E-06)))))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral3521753496, /*hidden argument*/NULL);
		Il2CppObject * L_15 = __this->get_mClient_5();
		String_t* L_16 = ___achievementID0;
		Action_1_t872614854 * L_17 = ___callback2;
		NullCheck(L_15);
		InterfaceActionInvoker2< String_t*, Action_1_t872614854 * >::Invoke(18 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::RevealAchievement(System.String,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_15, L_16, L_17);
		return;
	}

IL_0080:
	{
		V_0 = (bool)0;
		V_1 = 0;
		V_2 = 0;
		Il2CppObject * L_18 = __this->get_mClient_5();
		String_t* L_19 = ___achievementID0;
		NullCheck(L_18);
		Achievement_t1261647177 * L_20 = InterfaceFuncInvoker1< Achievement_t1261647177 *, String_t* >::Invoke(15 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.IPlayGamesClient::GetAchievement(System.String) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_18, L_19);
		V_3 = L_20;
		Achievement_t1261647177 * L_21 = V_3;
		if (L_21)
		{
			goto IL_00ba;
		}
	}
	{
		String_t* L_22 = ___achievementID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1596265191, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_w_m3419267169(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Logger_w_m3419267169(NULL /*static, unused*/, _stringLiteral1017031286, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_012b;
	}

IL_00ba:
	{
		Achievement_t1261647177 * L_24 = V_3;
		NullCheck(L_24);
		bool L_25 = Achievement_get_IsIncremental_m3147909093(L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		Achievement_t1261647177 * L_26 = V_3;
		NullCheck(L_26);
		int32_t L_27 = Achievement_get_CurrentSteps_m2632110837(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		Achievement_t1261647177 * L_28 = V_3;
		NullCheck(L_28);
		int32_t L_29 = Achievement_get_TotalSteps_m951649354(L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		bool L_30 = V_0;
		G_B9_0 = _stringLiteral3917757349;
		if (!L_30)
		{
			G_B10_0 = _stringLiteral3917757349;
			goto IL_00e4;
		}
	}
	{
		G_B11_0 = _stringLiteral3238333370;
		G_B11_1 = G_B9_0;
		goto IL_00e9;
	}

IL_00e4:
	{
		G_B11_0 = _stringLiteral2095255229;
		G_B11_1 = G_B10_0;
	}

IL_00e9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m138640077(NULL /*static, unused*/, G_B11_1, G_B11_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = V_0;
		if (!L_32)
		{
			goto IL_012b;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_33 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
		ArrayElementTypeCheck (L_33, _stringLiteral3528602822);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3528602822);
		ObjectU5BU5D_t1108656482* L_34 = L_33;
		int32_t L_35 = V_1;
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 1);
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
		ArrayElementTypeCheck (L_38, _stringLiteral47);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral47);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		int32_t L_40 = V_2;
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_42);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m3016520001(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
	}

IL_012b:
	{
		bool L_44 = V_0;
		if (!L_44)
		{
			goto IL_0204;
		}
	}
	{
		double L_45 = ___progress1;
		double L_46 = L_45;
		Il2CppObject * L_47 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1035314035, L_47, _stringLiteral1055423392, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		double L_49 = ___progress1;
		if ((!(((double)L_49) >= ((double)(0.0)))))
		{
			goto IL_0183;
		}
	}
	{
		double L_50 = ___progress1;
		if ((!(((double)L_50) <= ((double)(1.0)))))
		{
			goto IL_0183;
		}
	}
	{
		double L_51 = ___progress1;
		double L_52 = L_51;
		Il2CppObject * L_53 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1035314035, L_53, _stringLiteral3363482478, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_w_m3419267169(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
	}

IL_0183:
	{
		double L_55 = ___progress1;
		int32_t L_56 = V_2;
		double L_57 = bankers_round(((double)((double)((double)((double)L_55/(double)(100.0)))*(double)(((double)((double)L_56))))));
		V_4 = (((int32_t)((int32_t)L_57)));
		int32_t L_58 = V_4;
		int32_t L_59 = V_1;
		V_5 = ((int32_t)((int32_t)L_58-(int32_t)L_59));
		ObjectU5BU5D_t1108656482* L_60 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 0);
		ArrayElementTypeCheck (L_60, _stringLiteral3973014814);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3973014814);
		ObjectU5BU5D_t1108656482* L_61 = L_60;
		int32_t L_62 = V_4;
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_63);
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 1);
		ArrayElementTypeCheck (L_61, L_64);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_64);
		ObjectU5BU5D_t1108656482* L_65 = L_61;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 2);
		ArrayElementTypeCheck (L_65, _stringLiteral3567858759);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3567858759);
		ObjectU5BU5D_t1108656482* L_66 = L_65;
		int32_t L_67 = V_1;
		int32_t L_68 = L_67;
		Il2CppObject * L_69 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 3);
		ArrayElementTypeCheck (L_66, L_69);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_69);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_70 = String_Concat_m3016520001(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		int32_t L_71 = V_5;
		int32_t L_72 = L_71;
		Il2CppObject * L_73 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_72);
		String_t* L_74 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2897568489, L_73, /*hidden argument*/NULL);
		Logger_d_m234514644(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		int32_t L_75 = V_5;
		if ((((int32_t)L_75) < ((int32_t)0)))
		{
			goto IL_01ff;
		}
	}
	{
		Il2CppObject * L_76 = __this->get_mClient_5();
		String_t* L_77 = ___achievementID0;
		int32_t L_78 = V_5;
		Action_1_t872614854 * L_79 = ___callback2;
		NullCheck(L_76);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t872614854 * >::Invoke(19 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_76, L_77, L_78, L_79);
	}

IL_01ff:
	{
		goto IL_0259;
	}

IL_0204:
	{
		double L_80 = ___progress1;
		if ((!(((double)L_80) >= ((double)(100.0)))))
		{
			goto IL_023f;
		}
	}
	{
		double L_81 = ___progress1;
		double L_82 = L_81;
		Il2CppObject * L_83 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_82);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1035314035, L_83, _stringLiteral14957008, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		Il2CppObject * L_85 = __this->get_mClient_5();
		String_t* L_86 = ___achievementID0;
		Action_1_t872614854 * L_87 = ___callback2;
		NullCheck(L_85);
		InterfaceActionInvoker2< String_t*, Action_1_t872614854 * >::Invoke(17 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_85, L_86, L_87);
		goto IL_0259;
	}

IL_023f:
	{
		double L_88 = ___progress1;
		double L_89 = L_88;
		Il2CppObject * L_90 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_89);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1035314035, L_90, _stringLiteral2544523935, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
	}

IL_0259:
	{
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4192160360;
extern Il2CppCodeGenString* _stringLiteral4293469062;
extern Il2CppCodeGenString* _stringLiteral1112191053;
extern const uint32_t PlayGamesPlatform_IncrementAchievement_m1794692865_MetadataUsageId;
extern "C"  void PlayGamesPlatform_IncrementAchievement_m1794692865 (PlayGamesPlatform_t3624292130 * __this, String_t* ___achievementID0, int32_t ___steps1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_IncrementAchievement_m1794692865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral4192160360, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral4293469062);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4293469062);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = ___achievementID0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1112191053);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1112191053);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		int32_t L_8 = ___steps1;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___achievementID0;
		String_t* L_13 = PlayGamesPlatform_MapId_m1357306405(__this, L_12, /*hidden argument*/NULL);
		___achievementID0 = L_13;
		Il2CppObject * L_14 = __this->get_mClient_5();
		String_t* L_15 = ___achievementID0;
		int32_t L_16 = ___steps1;
		Action_1_t872614854 * L_17 = ___callback2;
		NullCheck(L_14);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t872614854 * >::Invoke(19 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::SetStepsAtLeast(System.String,System.Int32,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral424838071;
extern Il2CppCodeGenString* _stringLiteral857585943;
extern Il2CppCodeGenString* _stringLiteral1112191053;
extern const uint32_t PlayGamesPlatform_SetStepsAtLeast_m1284277206_MetadataUsageId;
extern "C"  void PlayGamesPlatform_SetStepsAtLeast_m1284277206 (PlayGamesPlatform_t3624292130 * __this, String_t* ___achievementID0, int32_t ___steps1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_SetStepsAtLeast_m1284277206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral424838071, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral857585943);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral857585943);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = ___achievementID0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1112191053);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1112191053);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		int32_t L_8 = ___steps1;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___achievementID0;
		String_t* L_13 = PlayGamesPlatform_MapId_m1357306405(__this, L_12, /*hidden argument*/NULL);
		___achievementID0 = L_13;
		Il2CppObject * L_14 = __this->get_mClient_5();
		String_t* L_15 = ___achievementID0;
		int32_t L_16 = ___steps1;
		Action_1_t872614854 * L_17 = ___callback2;
		NullCheck(L_14);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t872614854 * >::Invoke(20 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SetStepsAtLeast(System.String,System.Int32,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3647501372_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const MethodInfo* U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3272996438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3549865448;
extern const uint32_t PlayGamesPlatform_LoadAchievementDescriptions_m2564331573_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadAchievementDescriptions_m2564331573 (PlayGamesPlatform_t3624292130 * __this, Action_1_t229750097 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadAchievementDescriptions_m2564331573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * V_0 = NULL;
	{
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * L_0 = (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 *)il2cpp_codegen_object_new(U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576_il2cpp_TypeInfo_var);
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35__ctor_m210144355(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * L_1 = V_0;
		Action_1_t229750097 * L_2 = ___callback0;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		bool L_3 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral3549865448, /*hidden argument*/NULL);
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * L_4 = V_0;
		NullCheck(L_4);
		Action_1_t229750097 * L_5 = L_4->get_callback_0();
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * L_6 = V_0;
		NullCheck(L_6);
		Action_1_t229750097 * L_7 = L_6->get_callback_0();
		NullCheck(L_7);
		Action_1_Invoke_m2766540343(L_7, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)NULL, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		Il2CppObject * L_8 = __this->get_mClient_5();
		U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617_MethodInfo_var);
		Action_1_t3647501372 * L_11 = (Action_1_t3647501372 *)il2cpp_codegen_object_new(Action_1_t3647501372_il2cpp_TypeInfo_var);
		Action_1__ctor_m3272996438(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m3272996438_MethodInfo_var);
		NullCheck(L_8);
		InterfaceActionInvoker1< Action_1_t3647501372 * >::Invoke(16 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadAchievements(System.Action`1<GooglePlayGames.BasicApi.Achievement[]>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_8, L_11);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* U3CLoadAchievementsU3Ec__AnonStorey36_t225202025_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3647501372_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const MethodInfo* U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3272996438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral422822590;
extern const uint32_t PlayGamesPlatform_LoadAchievements_m861189147_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadAchievements_m861189147 (PlayGamesPlatform_t3624292130 * __this, Action_1_t2349069933 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadAchievements_m861189147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * V_0 = NULL;
	{
		U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * L_0 = (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 *)il2cpp_codegen_object_new(U3CLoadAchievementsU3Ec__AnonStorey36_t225202025_il2cpp_TypeInfo_var);
		U3CLoadAchievementsU3Ec__AnonStorey36__ctor_m972134946(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * L_1 = V_0;
		Action_1_t2349069933 * L_2 = ___callback0;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		bool L_3 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral422822590, /*hidden argument*/NULL);
		U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * L_4 = V_0;
		NullCheck(L_4);
		Action_1_t2349069933 * L_5 = L_4->get_callback_0();
		NullCheck(L_5);
		Action_1_Invoke_m222677977(L_5, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)NULL, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}

IL_002f:
	{
		Il2CppObject * L_6 = __this->get_mClient_5();
		U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995_MethodInfo_var);
		Action_1_t3647501372 * L_9 = (Action_1_t3647501372 *)il2cpp_codegen_object_new(Action_1_t3647501372_il2cpp_TypeInfo_var);
		Action_1__ctor_m3272996438(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m3272996438_MethodInfo_var);
		NullCheck(L_6);
		InterfaceActionInvoker1< Action_1_t3647501372 * >::Invoke(16 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadAchievements(System.Action`1<GooglePlayGames.BasicApi.Achievement[]>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_6, L_9);
		return;
	}
}
// UnityEngine.SocialPlatforms.IAchievement GooglePlayGames.PlayGamesPlatform::CreateAchievement()
extern Il2CppClass* PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_CreateAchievement_m3087416859_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_CreateAchievement_m3087416859 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_CreateAchievement_m3087416859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayGamesAchievement_t2357341912 * L_0 = (PlayGamesAchievement_t2357341912 *)il2cpp_codegen_object_new(PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var);
		PlayGamesAchievement__ctor_m3384221919(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4103256810;
extern Il2CppCodeGenString* _stringLiteral4154616367;
extern Il2CppCodeGenString* _stringLiteral620760203;
extern const uint32_t PlayGamesPlatform_ReportScore_m947609404_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ReportScore_m947609404 (PlayGamesPlatform_t3624292130 * __this, int64_t ___score0, String_t* ___board1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ReportScore_m947609404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral4103256810, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral4154616367);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4154616367);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		int64_t L_5 = ___score0;
		int64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral620760203);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral620760203);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = ___board1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3016520001(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_12 = ___board1;
		String_t* L_13 = PlayGamesPlatform_MapId_m1357306405(__this, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		Il2CppObject * L_14 = __this->get_mClient_5();
		String_t* L_15 = V_0;
		int64_t L_16 = ___score0;
		Action_1_t872614854 * L_17 = ___callback2;
		NullCheck(L_14);
		InterfaceActionInvoker3< String_t*, int64_t, Action_1_t872614854 * >::Invoke(26 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ReportScore(System.Int64,System.String,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4103256810;
extern Il2CppCodeGenString* _stringLiteral4154616367;
extern Il2CppCodeGenString* _stringLiteral620760203;
extern Il2CppCodeGenString* _stringLiteral1236274894;
extern const uint32_t PlayGamesPlatform_ReportScore_m615121208_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ReportScore_m615121208 (PlayGamesPlatform_t3624292130 * __this, int64_t ___score0, String_t* ___board1, String_t* ___metadata2, Action_1_t872614854 * ___callback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ReportScore_m615121208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral4103256810, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback3;
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback3;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0024:
	{
		return;
	}

IL_0025:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral4154616367);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4154616367);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		int64_t L_5 = ___score0;
		int64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral620760203);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral620760203);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = ___board1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, _stringLiteral1236274894);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1236274894);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		String_t* L_13 = ___metadata2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		String_t* L_15 = ___board1;
		String_t* L_16 = PlayGamesPlatform_MapId_m1357306405(__this, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		Il2CppObject * L_17 = __this->get_mClient_5();
		String_t* L_18 = V_0;
		int64_t L_19 = ___score0;
		String_t* L_20 = ___metadata2;
		Action_1_t872614854 * L_21 = ___callback3;
		NullCheck(L_17);
		InterfaceActionInvoker4< String_t*, int64_t, String_t*, Action_1_t872614854 * >::Invoke(27 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::SubmitScore(System.String,System.Int64,System.String,System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_17, L_18, L_19, L_20, L_21);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* U3CLoadScoresU3Ec__AnonStorey37_t2259335463_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t107331537_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m115807540_MethodInfo_var;
extern const uint32_t PlayGamesPlatform_LoadScores_m649875421_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadScores_m649875421 (PlayGamesPlatform_t3624292130 * __this, String_t* ___leaderboardId0, Action_1_t645920862 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadScores_m649875421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * V_0 = NULL;
	{
		U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * L_0 = (U3CLoadScoresU3Ec__AnonStorey37_t2259335463 *)il2cpp_codegen_object_new(U3CLoadScoresU3Ec__AnonStorey37_t2259335463_il2cpp_TypeInfo_var);
		U3CLoadScoresU3Ec__AnonStorey37__ctor_m3185808420(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * L_1 = V_0;
		Action_1_t645920862 * L_2 = ___callback1;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		String_t* L_3 = ___leaderboardId0;
		Il2CppObject * L_4 = __this->get_mClient_5();
		NullCheck(L_4);
		int32_t L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(25 /* System.Int32 GooglePlayGames.BasicApi.IPlayGamesClient::LeaderboardMaxResults() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_4);
		U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384_MethodInfo_var);
		Action_1_t107331537 * L_8 = (Action_1_t107331537 *)il2cpp_codegen_object_new(Action_1_t107331537_il2cpp_TypeInfo_var);
		Action_1__ctor_m115807540(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m115807540_MethodInfo_var);
		PlayGamesPlatform_LoadScores_m1161796118(__this, L_3, 2, L_5, 1, 3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(System.String,GooglePlayGames.BasicApi.LeaderboardStart,System.Int32,GooglePlayGames.BasicApi.LeaderboardCollection,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* LeaderboardScoreData_t4006482697_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1727616767_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1452605921;
extern const uint32_t PlayGamesPlatform_LoadScores_m1161796118_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadScores_m1161796118 (PlayGamesPlatform_t3624292130 * __this, String_t* ___leaderboardId0, int32_t ___start1, int32_t ___rowCount2, int32_t ___collection3, int32_t ___timeSpan4, Action_1_t107331537 * ___callback5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadScores_m1161796118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1452605921, /*hidden argument*/NULL);
		Action_1_t107331537 * L_1 = ___callback5;
		String_t* L_2 = ___leaderboardId0;
		LeaderboardScoreData_t4006482697 * L_3 = (LeaderboardScoreData_t4006482697 *)il2cpp_codegen_object_new(LeaderboardScoreData_t4006482697_il2cpp_TypeInfo_var);
		LeaderboardScoreData__ctor_m2065891215(L_3, L_2, ((int32_t)-3), /*hidden argument*/NULL);
		NullCheck(L_1);
		Action_1_Invoke_m1727616767(L_1, L_3, /*hidden argument*/Action_1_Invoke_m1727616767_MethodInfo_var);
		return;
	}

IL_0025:
	{
		Il2CppObject * L_4 = __this->get_mClient_5();
		String_t* L_5 = ___leaderboardId0;
		int32_t L_6 = ___start1;
		int32_t L_7 = ___rowCount2;
		int32_t L_8 = ___collection3;
		int32_t L_9 = ___timeSpan4;
		Action_1_t107331537 * L_10 = ___callback5;
		NullCheck(L_4);
		InterfaceActionInvoker6< String_t*, int32_t, int32_t, int32_t, int32_t, Action_1_t107331537 * >::Invoke(23 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadScores(System.String,GooglePlayGames.BasicApi.LeaderboardStart,System.Int32,GooglePlayGames.BasicApi.LeaderboardCollection,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8, L_9, L_10);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadMoreScores(GooglePlayGames.BasicApi.ScorePageToken,System.Int32,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* LeaderboardScoreData_t4006482697_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1727616767_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3470573772;
extern const uint32_t PlayGamesPlatform_LoadMoreScores_m462535769_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadMoreScores_m462535769 (PlayGamesPlatform_t3624292130 * __this, ScorePageToken_t1995225314 * ___token0, int32_t ___rowCount1, Action_1_t107331537 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadMoreScores_m462535769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral3470573772, /*hidden argument*/NULL);
		Action_1_t107331537 * L_1 = ___callback2;
		ScorePageToken_t1995225314 * L_2 = ___token0;
		NullCheck(L_2);
		String_t* L_3 = ScorePageToken_get_LeaderboardId_m3952163259(L_2, /*hidden argument*/NULL);
		LeaderboardScoreData_t4006482697 * L_4 = (LeaderboardScoreData_t4006482697 *)il2cpp_codegen_object_new(LeaderboardScoreData_t4006482697_il2cpp_TypeInfo_var);
		LeaderboardScoreData__ctor_m2065891215(L_4, L_3, ((int32_t)-3), /*hidden argument*/NULL);
		NullCheck(L_1);
		Action_1_Invoke_m1727616767(L_1, L_4, /*hidden argument*/Action_1_Invoke_m1727616767_MethodInfo_var);
		return;
	}

IL_0029:
	{
		Il2CppObject * L_5 = __this->get_mClient_5();
		ScorePageToken_t1995225314 * L_6 = ___token0;
		int32_t L_7 = ___rowCount1;
		Action_1_t107331537 * L_8 = ___callback2;
		NullCheck(L_5);
		InterfaceActionInvoker3< ScorePageToken_t1995225314 *, int32_t, Action_1_t107331537 * >::Invoke(24 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadMoreScores(GooglePlayGames.BasicApi.ScorePageToken,System.Int32,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_5, L_6, L_7, L_8);
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard GooglePlayGames.PlayGamesPlatform::CreateLeaderboard()
extern Il2CppClass* PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_CreateLeaderboard_m1579918043_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesPlatform_CreateLeaderboard_m1579918043 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_CreateLeaderboard_m1579918043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mDefaultLbUi_6();
		PlayGamesLeaderboard_t3198617382 * L_1 = (PlayGamesLeaderboard_t3198617382 *)il2cpp_codegen_object_new(PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var);
		PlayGamesLeaderboard__ctor_m4211124817(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI()
extern "C"  void PlayGamesPlatform_ShowAchievementsUI_m4102105428 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	{
		PlayGamesPlatform_ShowAchievementsUI_m2534030222(__this, (Action_1_t823521528 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1419453747;
extern Il2CppCodeGenString* _stringLiteral640695014;
extern const uint32_t PlayGamesPlatform_ShowAchievementsUI_m2534030222_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ShowAchievementsUI_m2534030222 (PlayGamesPlatform_t3624292130 * __this, Action_1_t823521528 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ShowAchievementsUI_m2534030222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1419453747, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		Action_1_t823521528 * L_1 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral640695014, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = __this->get_mClient_5();
		Action_1_t823521528 * L_4 = ___callback0;
		NullCheck(L_3);
		InterfaceActionInvoker1< Action_1_t823521528 * >::Invoke(21 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_3, L_4);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1853651336;
extern const uint32_t PlayGamesPlatform_ShowLeaderboardUI_m2711990935_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ShowLeaderboardUI_m2711990935 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ShowLeaderboardUI_m2711990935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral1853651336, /*hidden argument*/NULL);
		String_t* L_0 = __this->get_mDefaultLbUi_6();
		String_t* L_1 = PlayGamesPlatform_MapId_m1357306405(__this, L_0, /*hidden argument*/NULL);
		PlayGamesPlatform_ShowLeaderboardUI_m1355272853(__this, L_1, (Action_1_t823521528 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_ShowLeaderboardUI_m954145227_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ShowLeaderboardUI_m954145227 (PlayGamesPlatform_t3624292130 * __this, String_t* ___leaderboardId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ShowLeaderboardUI_m954145227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardId0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = ___leaderboardId0;
		String_t* L_2 = PlayGamesPlatform_MapId_m1357306405(__this, L_1, /*hidden argument*/NULL);
		___leaderboardId0 = L_2;
	}

IL_000f:
	{
		Il2CppObject * L_3 = __this->get_mClient_5();
		String_t* L_4 = ___leaderboardId0;
		NullCheck(L_3);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t823521528 * >::Invoke(22 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowLeaderboardUI(System.String,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_3, L_4, 3, (Action_1_t823521528 *)NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C"  void PlayGamesPlatform_ShowLeaderboardUI_m1355272853 (PlayGamesPlatform_t3624292130 * __this, String_t* ___leaderboardId0, Action_1_t823521528 * ___callback1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___leaderboardId0;
		Action_1_t823521528 * L_1 = ___callback1;
		PlayGamesPlatform_ShowLeaderboardUI_m3010809297(__this, L_0, 3, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2641070648_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1264914068;
extern Il2CppCodeGenString* _stringLiteral2762934020;
extern Il2CppCodeGenString* _stringLiteral1323919867;
extern const uint32_t PlayGamesPlatform_ShowLeaderboardUI_m3010809297_MetadataUsageId;
extern "C"  void PlayGamesPlatform_ShowLeaderboardUI_m3010809297 (PlayGamesPlatform_t3624292130 * __this, String_t* ___leaderboardId0, int32_t ___span1, Action_1_t823521528 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_ShowLeaderboardUI_m3010809297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1264914068, /*hidden argument*/NULL);
		Action_1_t823521528 * L_1 = ___callback2;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Action_1_t823521528 * L_2 = ___callback2;
		NullCheck(L_2);
		Action_1_Invoke_m2641070648(L_2, ((int32_t)-3), /*hidden argument*/Action_1_Invoke_m2641070648_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}

IL_0024:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral2762934020);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2762934020);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = ___leaderboardId0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1323919867);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1323919867);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		Action_1_t823521528 * L_8 = ___callback2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3016520001(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Il2CppObject * L_10 = __this->get_mClient_5();
		String_t* L_11 = ___leaderboardId0;
		int32_t L_12 = ___span1;
		Action_1_t823521528 * L_13 = ___callback2;
		NullCheck(L_10);
		InterfaceActionInvoker3< String_t*, int32_t, Action_1_t823521528 * >::Invoke(22 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::ShowLeaderboardUI(System.String,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.UIStatus>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_10, L_11, L_12, L_13);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::SetDefaultLeaderboardForUI(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1038083269;
extern const uint32_t PlayGamesPlatform_SetDefaultLeaderboardForUI_m3695507140_MetadataUsageId;
extern "C"  void PlayGamesPlatform_SetDefaultLeaderboardForUI_m3695507140 (PlayGamesPlatform_t3624292130 * __this, String_t* ___lbid0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_SetDefaultLeaderboardForUI_m3695507140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___lbid0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1038083269, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___lbid0;
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_3 = ___lbid0;
		String_t* L_4 = PlayGamesPlatform_MapId_m1357306405(__this, L_3, /*hidden argument*/NULL);
		___lbid0 = L_4;
	}

IL_001f:
	{
		String_t* L_5 = ___lbid0;
		__this->set_mDefaultLbUi_6(L_5);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1452605921;
extern const uint32_t PlayGamesPlatform_LoadFriends_m3849896064_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadFriends_m3849896064 (PlayGamesPlatform_t3624292130 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadFriends_m3849896064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1452605921, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t872614854 * L_2 = ___callback1;
		NullCheck(L_2);
		Action_1_Invoke_m3594021162(L_2, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		Il2CppObject * L_3 = __this->get_mClient_5();
		Action_1_t872614854 * L_4 = ___callback1;
		NullCheck(L_3);
		InterfaceActionInvoker1< Action_1_t872614854 * >::Invoke(5 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadFriends(System.Action`1<System.Boolean>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_3, L_4);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CLoadScoresU3Ec__AnonStorey38_t2259335464_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t107331537_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* U3CLoadScoresU3Ec__AnonStorey38_U3CU3Em__7_m3835822782_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m115807540_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1452605921;
extern Il2CppCodeGenString* _stringLiteral588952146;
extern Il2CppCodeGenString* _stringLiteral1323919867;
extern const uint32_t PlayGamesPlatform_LoadScores_m1968437709_MetadataUsageId;
extern "C"  void PlayGamesPlatform_LoadScores_m1968437709 (PlayGamesPlatform_t3624292130 * __this, Il2CppObject * ___board0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_LoadScores_m1968437709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * V_1 = NULL;
	int32_t V_2 = 0;
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Range_t1533311935  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B12_0 = 0;
	String_t* G_B12_1 = NULL;
	Il2CppObject * G_B12_2 = NULL;
	int32_t G_B11_0 = 0;
	String_t* G_B11_1 = NULL;
	Il2CppObject * G_B11_2 = NULL;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	String_t* G_B13_2 = NULL;
	Il2CppObject * G_B13_3 = NULL;
	int32_t G_B15_0 = 0;
	int32_t G_B15_1 = 0;
	String_t* G_B15_2 = NULL;
	Il2CppObject * G_B15_3 = NULL;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	String_t* G_B14_2 = NULL;
	Il2CppObject * G_B14_3 = NULL;
	int32_t G_B16_0 = 0;
	int32_t G_B16_1 = 0;
	int32_t G_B16_2 = 0;
	String_t* G_B16_3 = NULL;
	Il2CppObject * G_B16_4 = NULL;
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_0 = (U3CLoadScoresU3Ec__AnonStorey38_t2259335464 *)il2cpp_codegen_object_new(U3CLoadScoresU3Ec__AnonStorey38_t2259335464_il2cpp_TypeInfo_var);
		U3CLoadScoresU3Ec__AnonStorey38__ctor_m2989294915(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_1 = V_1;
		Il2CppObject * L_2 = ___board0;
		NullCheck(L_1);
		L_1->set_board_0(L_2);
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_3 = V_1;
		Action_1_t872614854 * L_4 = ___callback1;
		NullCheck(L_3);
		L_3->set_callback_1(L_4);
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_5 = V_1;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_2(__this);
		bool L_6 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_e_m4018947763(NULL /*static, unused*/, _stringLiteral1452605921, /*hidden argument*/NULL);
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_7 = V_1;
		NullCheck(L_7);
		Action_1_t872614854 * L_8 = L_7->get_callback_1();
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_9 = V_1;
		NullCheck(L_9);
		Action_1_t872614854 * L_10 = L_9->get_callback_1();
		NullCheck(L_10);
		Action_1_Invoke_m3594021162(L_10, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0047:
	{
		return;
	}

IL_0048:
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_11 = V_1;
		NullCheck(L_11);
		Il2CppObject * L_12 = L_11->get_board_0();
		NullCheck(L_12);
		int32_t L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_12);
		V_2 = L_13;
		int32_t L_14 = V_2;
		if (L_14 == 0)
		{
			goto IL_0079;
		}
		if (L_14 == 1)
		{
			goto IL_0072;
		}
		if (L_14 == 2)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_0080;
	}

IL_006b:
	{
		V_0 = 3;
		goto IL_0087;
	}

IL_0072:
	{
		V_0 = 2;
		goto IL_0087;
	}

IL_0079:
	{
		V_0 = 1;
		goto IL_0087;
	}

IL_0080:
	{
		V_0 = 3;
		goto IL_0087;
	}

IL_0087:
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_15 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_16 = L_15->get_board_0();
		NullCheck(((PlayGamesLeaderboard_t3198617382 *)CastclassClass(L_16, PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var)));
		PlayGamesLeaderboard_set_loading_m1714962085(((PlayGamesLeaderboard_t3198617382 *)CastclassClass(L_16, PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var)), (bool)1, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_17 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, _stringLiteral588952146);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral588952146);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_19 = V_1;
		NullCheck(L_19);
		Il2CppObject * L_20 = L_19->get_board_0();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_20);
		ObjectU5BU5D_t1108656482* L_21 = L_18;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 2);
		ArrayElementTypeCheck (L_21, _stringLiteral1323919867);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1323919867);
		ObjectU5BU5D_t1108656482* L_22 = L_21;
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_23 = V_1;
		NullCheck(L_23);
		Action_1_t872614854 * L_24 = L_23->get_callback_1();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 3);
		ArrayElementTypeCheck (L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m3016520001(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Il2CppObject * L_26 = __this->get_mClient_5();
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_27 = V_1;
		NullCheck(L_27);
		Il2CppObject * L_28 = L_27->get_board_0();
		NullCheck(L_28);
		String_t* L_29 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_28);
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_30 = V_1;
		NullCheck(L_30);
		Il2CppObject * L_31 = L_30->get_board_0();
		NullCheck(L_31);
		Range_t1533311935  L_32 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(3 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_31);
		V_3 = L_32;
		int32_t L_33 = (&V_3)->get_count_1();
		G_B11_0 = 2;
		G_B11_1 = L_29;
		G_B11_2 = L_26;
		if ((((int32_t)L_33) <= ((int32_t)0)))
		{
			G_B12_0 = 2;
			G_B12_1 = L_29;
			G_B12_2 = L_26;
			goto IL_010e;
		}
	}
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_34 = V_1;
		NullCheck(L_34);
		Il2CppObject * L_35 = L_34->get_board_0();
		NullCheck(L_35);
		Range_t1533311935  L_36 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(3 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_35);
		V_4 = L_36;
		int32_t L_37 = (&V_4)->get_count_1();
		G_B13_0 = L_37;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		G_B13_3 = G_B11_2;
		goto IL_0119;
	}

IL_010e:
	{
		Il2CppObject * L_38 = __this->get_mClient_5();
		NullCheck(L_38);
		int32_t L_39 = InterfaceFuncInvoker0< int32_t >::Invoke(25 /* System.Int32 GooglePlayGames.BasicApi.IPlayGamesClient::LeaderboardMaxResults() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_38);
		G_B13_0 = L_39;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
		G_B13_3 = G_B12_2;
	}

IL_0119:
	{
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_40 = V_1;
		NullCheck(L_40);
		Il2CppObject * L_41 = L_40->get_board_0();
		NullCheck(L_41);
		int32_t L_42 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_41);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
		G_B14_2 = G_B13_2;
		G_B14_3 = G_B13_3;
		if ((!(((uint32_t)L_42) == ((uint32_t)1))))
		{
			G_B15_0 = G_B13_0;
			G_B15_1 = G_B13_1;
			G_B15_2 = G_B13_2;
			G_B15_3 = G_B13_3;
			goto IL_0130;
		}
	}
	{
		G_B16_0 = 2;
		G_B16_1 = G_B14_0;
		G_B16_2 = G_B14_1;
		G_B16_3 = G_B14_2;
		G_B16_4 = G_B14_3;
		goto IL_0131;
	}

IL_0130:
	{
		G_B16_0 = 1;
		G_B16_1 = G_B15_0;
		G_B16_2 = G_B15_1;
		G_B16_3 = G_B15_2;
		G_B16_4 = G_B15_3;
	}

IL_0131:
	{
		int32_t L_43 = V_0;
		U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * L_44 = V_1;
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)U3CLoadScoresU3Ec__AnonStorey38_U3CU3Em__7_m3835822782_MethodInfo_var);
		Action_1_t107331537 * L_46 = (Action_1_t107331537 *)il2cpp_codegen_object_new(Action_1_t107331537_il2cpp_TypeInfo_var);
		Action_1__ctor_m115807540(L_46, L_44, L_45, /*hidden argument*/Action_1__ctor_m115807540_MethodInfo_var);
		NullCheck(G_B16_4);
		InterfaceActionInvoker6< String_t*, int32_t, int32_t, int32_t, int32_t, Action_1_t107331537 * >::Invoke(23 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadScores(System.String,GooglePlayGames.BasicApi.LeaderboardStart,System.Int32,GooglePlayGames.BasicApi.LeaderboardCollection,GooglePlayGames.BasicApi.LeaderboardTimeSpan,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, G_B16_4, G_B16_3, G_B16_2, G_B16_1, G_B16_0, L_43, L_46);
		return;
	}
}
// System.Boolean GooglePlayGames.PlayGamesPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetLoading_m1681232331_MetadataUsageId;
extern "C"  bool PlayGamesPlatform_GetLoading_m1681232331 (PlayGamesPlatform_t3624292130 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetLoading_m1681232331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___board0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		Il2CppObject * L_1 = ___board0;
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILeaderboard::get_loading() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		return (bool)G_B3_0;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_RegisterInvitationDelegate_m3195177803_MetadataUsageId;
extern "C"  void PlayGamesPlatform_RegisterInvitationDelegate_m3195177803 (PlayGamesPlatform_t3624292130 * __this, InvitationReceivedDelegate_t2409308905 * ___deleg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_RegisterInvitationDelegate_m3195177803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		InvitationReceivedDelegate_t2409308905 * L_1 = ___deleg0;
		NullCheck(L_0);
		InterfaceActionInvoker1< InvitationReceivedDelegate_t2409308905 * >::Invoke(33 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::GetToken()
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesPlatform_GetToken_m4098878783_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_GetToken_m4098878783 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetToken_m4098878783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mClient_5();
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String GooglePlayGames.BasicApi.IPlayGamesClient::GetToken() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform::HandleLoadingScores(GooglePlayGames.PlayGamesLeaderboard,GooglePlayGames.BasicApi.LeaderboardScoreData,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t107331537_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CHandleLoadingScoresU3Ec__AnonStorey39_U3CU3Em__8_m3584706148_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m115807540_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t PlayGamesPlatform_HandleLoadingScores_m1326934272_MetadataUsageId;
extern "C"  void PlayGamesPlatform_HandleLoadingScores_m1326934272 (PlayGamesPlatform_t3624292130 * __this, PlayGamesLeaderboard_t3198617382 * ___board0, LeaderboardScoreData_t4006482697 * ___scoreData1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_HandleLoadingScores_m1326934272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * V_2 = NULL;
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_0 = (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 *)il2cpp_codegen_object_new(U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193_il2cpp_TypeInfo_var);
		U3CHandleLoadingScoresU3Ec__AnonStorey39__ctor_m3465157450(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_1 = V_2;
		PlayGamesLeaderboard_t3198617382 * L_2 = ___board0;
		NullCheck(L_1);
		L_1->set_board_0(L_2);
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_3 = V_2;
		Action_1_t872614854 * L_4 = ___callback2;
		NullCheck(L_3);
		L_3->set_callback_1(L_4);
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_5 = V_2;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_2(__this);
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_6 = V_2;
		NullCheck(L_6);
		PlayGamesLeaderboard_t3198617382 * L_7 = L_6->get_board_0();
		LeaderboardScoreData_t4006482697 * L_8 = ___scoreData1;
		NullCheck(L_7);
		bool L_9 = PlayGamesLeaderboard_SetFromData_m3542296026(L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_008c;
		}
	}
	{
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_11 = V_2;
		NullCheck(L_11);
		PlayGamesLeaderboard_t3198617382 * L_12 = L_11->get_board_0();
		NullCheck(L_12);
		bool L_13 = PlayGamesLeaderboard_HasAllScores_m4175623983(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008c;
		}
	}
	{
		LeaderboardScoreData_t4006482697 * L_14 = ___scoreData1;
		NullCheck(L_14);
		ScorePageToken_t1995225314 * L_15 = LeaderboardScoreData_get_NextPageToken_m1251517283(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008c;
		}
	}
	{
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_16 = V_2;
		NullCheck(L_16);
		PlayGamesLeaderboard_t3198617382 * L_17 = L_16->get_board_0();
		NullCheck(L_17);
		Range_t1533311935  L_18 = PlayGamesLeaderboard_get_range_m4196876027(L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		int32_t L_19 = (&V_3)->get_count_1();
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_20 = V_2;
		NullCheck(L_20);
		PlayGamesLeaderboard_t3198617382 * L_21 = L_20->get_board_0();
		NullCheck(L_21);
		int32_t L_22 = PlayGamesLeaderboard_get_ScoreCount_m3010022535(L_21, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_19-(int32_t)L_22));
		Il2CppObject * L_23 = __this->get_mClient_5();
		LeaderboardScoreData_t4006482697 * L_24 = ___scoreData1;
		NullCheck(L_24);
		ScorePageToken_t1995225314 * L_25 = LeaderboardScoreData_get_NextPageToken_m1251517283(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_27 = V_2;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)U3CHandleLoadingScoresU3Ec__AnonStorey39_U3CU3Em__8_m3584706148_MethodInfo_var);
		Action_1_t107331537 * L_29 = (Action_1_t107331537 *)il2cpp_codegen_object_new(Action_1_t107331537_il2cpp_TypeInfo_var);
		Action_1__ctor_m115807540(L_29, L_27, L_28, /*hidden argument*/Action_1__ctor_m115807540_MethodInfo_var);
		NullCheck(L_23);
		InterfaceActionInvoker3< ScorePageToken_t1995225314 *, int32_t, Action_1_t107331537 * >::Invoke(24 /* System.Void GooglePlayGames.BasicApi.IPlayGamesClient::LoadMoreScores(GooglePlayGames.BasicApi.ScorePageToken,System.Int32,System.Action`1<GooglePlayGames.BasicApi.LeaderboardScoreData>) */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_23, L_25, L_26, L_29);
		goto IL_0098;
	}

IL_008c:
	{
		U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * L_30 = V_2;
		NullCheck(L_30);
		Action_1_t872614854 * L_31 = L_30->get_callback_1();
		bool L_32 = V_0;
		NullCheck(L_31);
		Action_1_Invoke_m3594021162(L_31, L_32, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0098:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.PlayGamesPlatform::GetFriends()
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern Il2CppClass* IUserProfileU5BU5D_t3419104218_il2cpp_TypeInfo_var;
extern Il2CppClass* IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1481945747;
extern const uint32_t PlayGamesPlatform_GetFriends_m3329218634_MetadataUsageId;
extern "C"  IUserProfileU5BU5D_t3419104218* PlayGamesPlatform_GetFriends_m3329218634 (PlayGamesPlatform_t3624292130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_GetFriends_m3329218634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayGamesPlatform_IsAuthenticated_m2538886108(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, _stringLiteral1481945747, /*hidden argument*/NULL);
		return ((IUserProfileU5BU5D_t3419104218*)SZArrayNew(IUserProfileU5BU5D_t3419104218_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_001c:
	{
		Il2CppObject * L_1 = __this->get_mClient_5();
		NullCheck(L_1);
		IUserProfileU5BU5D_t3419104218* L_2 = InterfaceFuncInvoker0< IUserProfileU5BU5D_t3419104218* >::Invoke(34 /* UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.BasicApi.IPlayGamesClient::GetFriends() */, IPlayGamesClient_t2528289561_il2cpp_TypeInfo_var, L_1);
		return L_2;
	}
}
// System.String GooglePlayGames.PlayGamesPlatform::MapId(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t984534948_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2892345767_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3979518802_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3434244994;
extern Il2CppCodeGenString* _stringLiteral1759864672;
extern const uint32_t PlayGamesPlatform_MapId_m1357306405_MetadataUsageId;
extern "C"  String_t* PlayGamesPlatform_MapId_m1357306405 (PlayGamesPlatform_t3624292130 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesPlatform_MapId_m1357306405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___id0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t827649927 * L_1 = __this->get_mIdMap_7();
		String_t* L_2 = ___id0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_m2892345767(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m2892345767_MethodInfo_var);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		Dictionary_2_t827649927 * L_4 = __this->get_mIdMap_7();
		String_t* L_5 = ___id0;
		NullCheck(L_4);
		String_t* L_6 = Dictionary_2_get_Item_m3979518802(L_4, L_5, /*hidden argument*/Dictionary_2_get_Item_m3979518802_MethodInfo_var);
		V_0 = L_6;
		String_t* L_7 = ___id0;
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral3434244994, L_7, _stringLiteral1759864672, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t984534948_il2cpp_TypeInfo_var);
		Logger_d_m234514644(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		return L_10;
	}

IL_003e:
	{
		String_t* L_11 = ___id0;
		return L_11;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39::.ctor()
extern "C"  void U3CHandleLoadingScoresU3Ec__AnonStorey39__ctor_m3465157450 (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<HandleLoadingScores>c__AnonStorey39::<>m__8(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern "C"  void U3CHandleLoadingScoresU3Ec__AnonStorey39_U3CU3Em__8_m3584706148 (U3CHandleLoadingScoresU3Ec__AnonStorey39_t3867310193 * __this, LeaderboardScoreData_t4006482697 * ___nextScoreData0, const MethodInfo* method)
{
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_U3CU3Ef__this_2();
		PlayGamesLeaderboard_t3198617382 * L_1 = __this->get_board_0();
		LeaderboardScoreData_t4006482697 * L_2 = ___nextScoreData0;
		Action_1_t872614854 * L_3 = __this->get_callback_1();
		NullCheck(L_0);
		PlayGamesPlatform_HandleLoadingScores_m1326934272(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35::.ctor()
extern "C"  void U3CLoadAchievementDescriptionsU3Ec__AnonStorey35__ctor_m210144355 (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35::<>m__4(GooglePlayGames.BasicApi.Achievement[])
extern Il2CppClass* IAchievementDescriptionU5BU5D_t4128901257_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const uint32_t U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617_MetadataUsageId;
extern "C"  void U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617 (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * __this, AchievementU5BU5D_t3251685236* ___ach0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IAchievementDescriptionU5BU5D_t4128901257* V_0 = NULL;
	int32_t V_1 = 0;
	{
		AchievementU5BU5D_t3251685236* L_0 = ___ach0;
		NullCheck(L_0);
		V_0 = ((IAchievementDescriptionU5BU5D_t4128901257*)SZArrayNew(IAchievementDescriptionU5BU5D_t4128901257_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = 0;
		goto IL_001f;
	}

IL_0010:
	{
		IAchievementDescriptionU5BU5D_t4128901257* L_1 = V_0;
		int32_t L_2 = V_1;
		AchievementU5BU5D_t3251685236* L_3 = ___ach0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Achievement_t1261647177 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		PlayGamesAchievement_t2357341912 * L_7 = (PlayGamesAchievement_t2357341912 *)il2cpp_codegen_object_new(PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var);
		PlayGamesAchievement__ctor_m1510359896(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_9 = V_1;
		IAchievementDescriptionU5BU5D_t4128901257* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		Action_1_t229750097 * L_11 = __this->get_callback_0();
		IAchievementDescriptionU5BU5D_t4128901257* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m2766540343(L_11, L_12, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36::.ctor()
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey36__ctor_m972134946 (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievements>c__AnonStorey36::<>m__5(GooglePlayGames.BasicApi.Achievement[])
extern Il2CppClass* IAchievementU5BU5D_t1953253797_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const uint32_t U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995_MetadataUsageId;
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995 (U3CLoadAchievementsU3Ec__AnonStorey36_t225202025 * __this, AchievementU5BU5D_t3251685236* ___ach0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadAchievementsU3Ec__AnonStorey36_U3CU3Em__5_m2223304995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IAchievementU5BU5D_t1953253797* V_0 = NULL;
	int32_t V_1 = 0;
	{
		AchievementU5BU5D_t3251685236* L_0 = ___ach0;
		NullCheck(L_0);
		V_0 = ((IAchievementU5BU5D_t1953253797*)SZArrayNew(IAchievementU5BU5D_t1953253797_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = 0;
		goto IL_001f;
	}

IL_0010:
	{
		IAchievementU5BU5D_t1953253797* L_1 = V_0;
		int32_t L_2 = V_1;
		AchievementU5BU5D_t3251685236* L_3 = ___ach0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Achievement_t1261647177 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		PlayGamesAchievement_t2357341912 * L_7 = (PlayGamesAchievement_t2357341912 *)il2cpp_codegen_object_new(PlayGamesAchievement_t2357341912_il2cpp_TypeInfo_var);
		PlayGamesAchievement__ctor_m1510359896(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_9 = V_1;
		IAchievementU5BU5D_t1953253797* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		Action_1_t2349069933 * L_11 = __this->get_callback_0();
		IAchievementU5BU5D_t1953253797* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m222677977(L_11, L_12, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37::.ctor()
extern "C"  void U3CLoadScoresU3Ec__AnonStorey37__ctor_m3185808420 (U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey37::<>m__6(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384_MetadataUsageId;
extern "C"  void U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384 (U3CLoadScoresU3Ec__AnonStorey37_t2259335463 * __this, LeaderboardScoreData_t4006482697 * ___scoreData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadScoresU3Ec__AnonStorey37_U3CU3Em__6_m2845904384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t645920862 * L_0 = __this->get_callback_0();
		LeaderboardScoreData_t4006482697 * L_1 = ___scoreData0;
		NullCheck(L_1);
		IScoreU5BU5D_t250104726* L_2 = LeaderboardScoreData_get_Scores_m1316040810(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Action_1_Invoke_m50340758(L_0, L_2, /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey38::.ctor()
extern "C"  void U3CLoadScoresU3Ec__AnonStorey38__ctor_m2989294915 (U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadScores>c__AnonStorey38::<>m__7(GooglePlayGames.BasicApi.LeaderboardScoreData)
extern Il2CppClass* PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadScoresU3Ec__AnonStorey38_U3CU3Em__7_m3835822782_MetadataUsageId;
extern "C"  void U3CLoadScoresU3Ec__AnonStorey38_U3CU3Em__7_m3835822782 (U3CLoadScoresU3Ec__AnonStorey38_t2259335464 * __this, LeaderboardScoreData_t4006482697 * ___scoreData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadScoresU3Ec__AnonStorey38_U3CU3Em__7_m3835822782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayGamesPlatform_t3624292130 * L_0 = __this->get_U3CU3Ef__this_2();
		Il2CppObject * L_1 = __this->get_board_0();
		LeaderboardScoreData_t4006482697 * L_2 = ___scoreData0;
		Action_1_t872614854 * L_3 = __this->get_callback_1();
		NullCheck(L_0);
		PlayGamesPlatform_HandleLoadingScores_m1326934272(L_0, ((PlayGamesLeaderboard_t3198617382 *)CastclassClass(L_1, PlayGamesLeaderboard_t3198617382_il2cpp_TypeInfo_var)), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::.ctor(System.DateTime,System.String,System.UInt64,System.String,System.UInt64,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesScore__ctor_m2435400634_MetadataUsageId;
extern "C"  void PlayGamesScore__ctor_m2435400634 (PlayGamesScore_t486124539 * __this, DateTime_t4283661327  ___date0, String_t* ___leaderboardId1, uint64_t ___rank2, String_t* ___playerId3, uint64_t ___value4, String_t* ___metadata5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesScore__ctor_m2435400634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mPlayerId_3(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mMetadata_4(L_1);
		DateTime_t4283661327  L_2;
		memset(&L_2, 0, sizeof(L_2));
		DateTime__ctor_m580066412(&L_2, ((int32_t)1970), 1, 1, 0, 0, 0, /*hidden argument*/NULL);
		__this->set_mDate_5(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DateTime_t4283661327  L_3 = ___date0;
		__this->set_mDate_5(L_3);
		String_t* L_4 = PlayGamesScore_get_leaderboardID_m3982419526(__this, /*hidden argument*/NULL);
		__this->set_mLbId_0(L_4);
		uint64_t L_5 = ___rank2;
		__this->set_mRank_2(L_5);
		String_t* L_6 = ___playerId3;
		__this->set_mPlayerId_3(L_6);
		uint64_t L_7 = ___value4;
		__this->set_mValue_1(L_7);
		String_t* L_8 = ___metadata5;
		__this->set_mMetadata_4(L_8);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::ReportScore(System.Action`1<System.Boolean>)
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesScore_ReportScore_m3681302779_MetadataUsageId;
extern "C"  void PlayGamesScore_ReportScore_m3681302779 (PlayGamesScore_t486124539 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesScore_ReportScore_m3681302779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_t3624292130 * L_0 = PlayGamesPlatform_get_Instance_m247033584(NULL /*static, unused*/, /*hidden argument*/NULL);
		int64_t L_1 = __this->get_mValue_1();
		String_t* L_2 = __this->get_mLbId_0();
		String_t* L_3 = __this->get_mMetadata_4();
		Action_1_t872614854 * L_4 = ___callback0;
		NullCheck(L_0);
		PlayGamesPlatform_ReportScore_m615121208(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_leaderboardID()
extern "C"  String_t* PlayGamesScore_get_leaderboardID_m3982419526 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mLbId_0();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::set_leaderboardID(System.String)
extern "C"  void PlayGamesScore_set_leaderboardID_m2979886797 (PlayGamesScore_t486124539 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mLbId_0(L_0);
		return;
	}
}
// System.Int64 GooglePlayGames.PlayGamesScore::get_value()
extern "C"  int64_t PlayGamesScore_get_value_m96316979 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_mValue_1();
		return L_0;
	}
}
// System.Void GooglePlayGames.PlayGamesScore::set_value(System.Int64)
extern "C"  void PlayGamesScore_set_value_m2736614400 (PlayGamesScore_t486124539 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_mValue_1(L_0);
		return;
	}
}
// System.DateTime GooglePlayGames.PlayGamesScore::get_date()
extern "C"  DateTime_t4283661327  PlayGamesScore_get_date_m3407034328 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_mDate_5();
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_formattedValue()
extern "C"  String_t* PlayGamesScore_get_formattedValue_m852870537 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		int64_t* L_0 = __this->get_address_of_mValue_1();
		String_t* L_1 = Int64_ToString_m3478011791(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String GooglePlayGames.PlayGamesScore::get_userID()
extern "C"  String_t* PlayGamesScore_get_userID_m608145498 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mPlayerId_3();
		return L_0;
	}
}
// System.Int32 GooglePlayGames.PlayGamesScore::get_rank()
extern "C"  int32_t PlayGamesScore_get_rank_m2745280459 (PlayGamesScore_t486124539 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = __this->get_mRank_2();
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Void GooglePlayGames.PlayGamesUserProfile::.ctor(System.String,System.String,System.String)
extern "C"  void PlayGamesUserProfile__ctor_m3655178218 (PlayGamesUserProfile_t4293397255 * __this, String_t* ___displayName0, String_t* ___playerId1, String_t* ___avatarUrl2, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___displayName0;
		__this->set_mDisplayName_0(L_0);
		String_t* L_1 = ___playerId1;
		__this->set_mPlayerId_1(L_1);
		String_t* L_2 = ___avatarUrl2;
		__this->set_mAvatarUrl_2(L_2);
		il2cpp_codegen_memory_barrier();
		__this->set_mImageLoading_3(0);
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesUserProfile::ResetIdentity(System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesUserProfile_ResetIdentity_m1865931839_MetadataUsageId;
extern "C"  void PlayGamesUserProfile_ResetIdentity_m1865931839 (PlayGamesUserProfile_t4293397255 * __this, String_t* ___displayName0, String_t* ___playerId1, String_t* ___avatarUrl2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_ResetIdentity_m1865931839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___displayName0;
		__this->set_mDisplayName_0(L_0);
		String_t* L_1 = ___playerId1;
		__this->set_mPlayerId_1(L_1);
		String_t* L_2 = __this->get_mAvatarUrl_2();
		String_t* L_3 = ___avatarUrl2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		__this->set_mImage_4((Texture2D_t3884108195 *)NULL);
		String_t* L_5 = ___avatarUrl2;
		__this->set_mAvatarUrl_2(L_5);
	}

IL_002d:
	{
		il2cpp_codegen_memory_barrier();
		__this->set_mImageLoading_3(0);
		return;
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::get_userName()
extern "C"  String_t* PlayGamesUserProfile_get_userName_m2379353398 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mDisplayName_0();
		return L_0;
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::get_id()
extern "C"  String_t* PlayGamesUserProfile_get_id_m613081947 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mPlayerId_1();
		return L_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesUserProfile::get_isFriend()
extern "C"  bool PlayGamesUserProfile_get_isFriend_m43038265 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesUserProfile::get_state()
extern "C"  int32_t PlayGamesUserProfile_get_state_m361572247 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// UnityEngine.Texture2D GooglePlayGames.PlayGamesUserProfile::get_image()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2197520972;
extern const uint32_t PlayGamesUserProfile_get_image_m3678310755_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * PlayGamesUserProfile_get_image_m3678310755 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_get_image_m3678310755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_mImageLoading_3();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0057;
		}
	}
	{
		Texture2D_t3884108195 * L_1 = __this->get_mImage_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_3 = PlayGamesUserProfile_get_AvatarURL_m1380365272(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_5 = PlayGamesUserProfile_get_AvatarURL_m1380365272(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2197520972, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set_mImageLoading_3(1);
		Il2CppObject * L_7 = PlayGamesUserProfile_LoadImage_m280276411(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t727662960_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_RunCoroutine_m3444677164(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0057:
	{
		Texture2D_t3884108195 * L_8 = __this->get_mImage_4();
		return L_8;
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::get_AvatarURL()
extern "C"  String_t* PlayGamesUserProfile_get_AvatarURL_m1380365272 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mAvatarUrl_2();
		return L_0;
	}
}
// System.Collections.IEnumerator GooglePlayGames.PlayGamesUserProfile::LoadImage()
extern Il2CppClass* U3CLoadImageU3Ec__Iterator3_t2198666563_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesUserProfile_LoadImage_m280276411_MetadataUsageId;
extern "C"  Il2CppObject * PlayGamesUserProfile_LoadImage_m280276411 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_LoadImage_m280276411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadImageU3Ec__Iterator3_t2198666563 * V_0 = NULL;
	{
		U3CLoadImageU3Ec__Iterator3_t2198666563 * L_0 = (U3CLoadImageU3Ec__Iterator3_t2198666563 *)il2cpp_codegen_object_new(U3CLoadImageU3Ec__Iterator3_t2198666563_il2cpp_TypeInfo_var);
		U3CLoadImageU3Ec__Iterator3__ctor_m384475320(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadImageU3Ec__Iterator3_t2198666563 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CLoadImageU3Ec__Iterator3_t2198666563 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean GooglePlayGames.PlayGamesUserProfile::Equals(System.Object)
extern Il2CppClass* PlayGamesUserProfile_t4293397255_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesUserProfile_Equals_m2359957581_MetadataUsageId;
extern "C"  bool PlayGamesUserProfile_Equals_m2359957581 (PlayGamesUserProfile_t4293397255 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_Equals_m2359957581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayGamesUserProfile_t4293397255 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_3 = ___obj0;
		V_0 = ((PlayGamesUserProfile_t4293397255 *)IsInstClass(L_3, PlayGamesUserProfile_t4293397255_il2cpp_TypeInfo_var));
		PlayGamesUserProfile_t4293397255 * L_4 = V_0;
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		return (bool)0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_5 = StringComparer_get_Ordinal_m2543279027(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = __this->get_mPlayerId_1();
		PlayGamesUserProfile_t4293397255 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_mPlayerId_1();
		NullCheck(L_5);
		bool L_9 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(11 /* System.Boolean System.StringComparer::Equals(System.String,System.String) */, L_5, L_6, L_8);
		return L_9;
	}
}
// System.Int32 GooglePlayGames.PlayGamesUserProfile::GetHashCode()
extern const Il2CppType* PlayGamesUserProfile_t4293397255_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayGamesUserProfile_GetHashCode_m2802127217_MetadataUsageId;
extern "C"  int32_t PlayGamesUserProfile_GetHashCode_m2802127217 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_GetHashCode_m2802127217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PlayGamesUserProfile_t4293397255_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Type::GetHashCode() */, L_0);
		String_t* L_2 = __this->get_mPlayerId_1();
		NullCheck(L_2);
		int32_t L_3 = String_GetHashCode_m471729487(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)L_3));
	}
}
// System.String GooglePlayGames.PlayGamesUserProfile::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2174277442;
extern const uint32_t PlayGamesUserProfile_ToString_m3644280771_MetadataUsageId;
extern "C"  String_t* PlayGamesUserProfile_ToString_m3644280771 (PlayGamesUserProfile_t4293397255 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesUserProfile_ToString_m3644280771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mDisplayName_0();
		String_t* L_1 = __this->get_mPlayerId_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2174277442, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::.ctor()
extern "C"  void U3CLoadImageU3Ec__Iterator3__ctor_m384475320 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2234821284 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1555592760 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3134621005_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral492352451;
extern Il2CppCodeGenString* _stringLiteral1624848508;
extern const uint32_t U3CLoadImageU3Ec__Iterator3_MoveNext_m648549412_MetadataUsageId;
extern "C"  bool U3CLoadImageU3Ec__Iterator3_MoveNext_m648549412 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageU3Ec__Iterator3_MoveNext_m648549412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_010b;
	}

IL_0021:
	{
		PlayGamesUserProfile_t4293397255 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		String_t* L_3 = PlayGamesUserProfile_get_AvatarURL_m1380365272(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00dc;
		}
	}
	{
		PlayGamesUserProfile_t4293397255 * L_5 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_5);
		String_t* L_6 = PlayGamesUserProfile_get_AvatarURL_m1380365272(L_5, /*hidden argument*/NULL);
		WWW_t3134621005 * L_7 = (WWW_t3134621005 *)il2cpp_codegen_object_new(WWW_t3134621005_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_7, L_6, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_7);
		goto IL_0064;
	}

IL_0051:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_010d;
	}

IL_0064:
	{
		WWW_t3134621005 * L_8 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_8);
		bool L_9 = WWW_get_isDone_m634060017(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0051;
		}
	}
	{
		WWW_t3134621005 * L_10 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_10);
		String_t* L_11 = WWW_get_error_m1787423074(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_009f;
		}
	}
	{
		PlayGamesUserProfile_t4293397255 * L_12 = __this->get_U3CU3Ef__this_3();
		WWW_t3134621005 * L_13 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_13);
		Texture2D_t3884108195 * L_14 = WWW_get_texture_m2854732303(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_mImage_4(L_14);
		goto IL_00c9;
	}

IL_009f:
	{
		PlayGamesUserProfile_t4293397255 * L_15 = __this->get_U3CU3Ef__this_3();
		Texture2D_t3884108195 * L_16 = Texture2D_get_blackTexture_m338990496(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_mImage_4(L_16);
		WWW_t3134621005 * L_17 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_error_m1787423074(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral492352451, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		PlayGamesUserProfile_t4293397255 * L_20 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_20);
		il2cpp_codegen_memory_barrier();
		L_20->set_mImageLoading_3(0);
		goto IL_0104;
	}

IL_00dc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1624848508, /*hidden argument*/NULL);
		PlayGamesUserProfile_t4293397255 * L_21 = __this->get_U3CU3Ef__this_3();
		Texture2D_t3884108195 * L_22 = Texture2D_get_blackTexture_m338990496(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_mImage_4(L_22);
		PlayGamesUserProfile_t4293397255 * L_23 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_23);
		il2cpp_codegen_memory_barrier();
		L_23->set_mImageLoading_3(0);
	}

IL_0104:
	{
		__this->set_U24PC_1((-1));
	}

IL_010b:
	{
		return (bool)0;
	}

IL_010d:
	{
		return (bool)1;
	}
	// Dead block : IL_010f: ldloc.1
}
// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::Dispose()
extern "C"  void U3CLoadImageU3Ec__Iterator3_Dispose_m11878133 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void GooglePlayGames.PlayGamesUserProfile/<LoadImage>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadImageU3Ec__Iterator3_Reset_m2325875557_MetadataUsageId;
extern "C"  void U3CLoadImageU3Ec__Iterator3_Reset_m2325875557 (U3CLoadImageU3Ec__Iterator3_t2198666563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageU3Ec__Iterator3_Reset_m2325875557_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GooglePlayGames.PluginVersion::.ctor()
extern "C"  void PluginVersion__ctor_m2576876192 (PluginVersion_t3642060999 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.ReportProgress::.ctor(System.Object,System.IntPtr)
extern "C"  void ReportProgress__ctor_m230147170 (ReportProgress_t3967815895 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>)
extern "C"  void ReportProgress_Invoke_m485975983 (ReportProgress_t3967815895 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReportProgress_Invoke_m485975983((ReportProgress_t3967815895 *)__this->get_prev_9(),___id0, ___progress1, ___callback2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___id0, ___progress1, ___callback2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___id0, ___progress1, ___callback2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___id0, ___progress1, ___callback2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ReportProgress_t3967815895 (ReportProgress_t3967815895 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, double, Il2CppMethodPointer);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___id0' to native representation
	char* ____id0_marshaled = NULL;
	____id0_marshaled = il2cpp_codegen_marshal_string(___id0);

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	il2cppPInvokeFunc(____id0_marshaled, ___progress1, ____callback2_marshaled);

	// Marshaling cleanup of parameter '___id0' native representation
	il2cpp_codegen_marshal_free(____id0_marshaled);
	____id0_marshaled = NULL;

}
// System.IAsyncResult GooglePlayGames.ReportProgress::BeginInvoke(System.String,System.Double,System.Action`1<System.Boolean>,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t ReportProgress_BeginInvoke_m1945770370_MetadataUsageId;
extern "C"  Il2CppObject * ReportProgress_BeginInvoke_m1945770370 (ReportProgress_t3967815895 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, AsyncCallback_t1369114871 * ____callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReportProgress_BeginInvoke_m1945770370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___id0;
	__d_args[1] = Box(Double_t3868226565_il2cpp_TypeInfo_var, &___progress1);
	__d_args[2] = ___callback2;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)____callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.ReportProgress::EndInvoke(System.IAsyncResult)
extern "C"  void ReportProgress_EndInvoke_m2865805170 (ReportProgress_t3967815895 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GPGSBtn::.ctor()
extern "C"  void GPGSBtn__ctor_m3123261636 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GPGSBtn::Awake()
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2805399355;
extern Il2CppCodeGenString* _stringLiteral3307638489;
extern Il2CppCodeGenString* _stringLiteral720524429;
extern const uint32_t GPGSBtn_Awake_m3360866855_MetadataUsageId;
extern "C"  void GPGSBtn_Awake_m3360866855 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_Awake_m3360866855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayerPrefs_HasKey_m2032560073(NULL /*static, unused*/, _stringLiteral2805399355, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral2805399355, 0, /*hidden argument*/NULL);
	}

IL_001a:
	{
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral3307638489, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_2 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_2);
		GPGSMng_InitializeGPGS_m137004335(L_2, /*hidden argument*/NULL);
		GPGSBtn_SetOnInit_m118166451(__this, /*hidden argument*/NULL);
	}

IL_003a:
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral3307638489, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral720524429, /*hidden argument*/NULL);
		Il2CppObject * L_3 = GPGSBtn_LoginChecker_m4107403972(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GPGSBtn::SetOnInit()
extern Il2CppCodeGenString* _stringLiteral229116950;
extern const uint32_t GPGSBtn_SetOnInit_m118166451_MetadataUsageId;
extern "C"  void GPGSBtn_SetOnInit_m118166451 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_SetOnInit_m118166451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral229116950, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0016;
		}
	}
	{
		GPGSBtn_ClickEvent_m2313098802(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Collections.IEnumerator GPGSBtn::LoginChecker()
extern Il2CppClass* U3CLoginCheckerU3Ec__IteratorD_t4246608817_il2cpp_TypeInfo_var;
extern const uint32_t GPGSBtn_LoginChecker_m4107403972_MetadataUsageId;
extern "C"  Il2CppObject * GPGSBtn_LoginChecker_m4107403972 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_LoginChecker_m4107403972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoginCheckerU3Ec__IteratorD_t4246608817 * V_0 = NULL;
	{
		U3CLoginCheckerU3Ec__IteratorD_t4246608817 * L_0 = (U3CLoginCheckerU3Ec__IteratorD_t4246608817 *)il2cpp_codegen_object_new(U3CLoginCheckerU3Ec__IteratorD_t4246608817_il2cpp_TypeInfo_var);
		U3CLoginCheckerU3Ec__IteratorD__ctor_m3162131466(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoginCheckerU3Ec__IteratorD_t4246608817 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CLoginCheckerU3Ec__IteratorD_t4246608817 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GPGSBtn::LoginCheckerNet()
extern Il2CppClass* U3CLoginCheckerNetU3Ec__IteratorE_t3774081813_il2cpp_TypeInfo_var;
extern const uint32_t GPGSBtn_LoginCheckerNet_m90460827_MetadataUsageId;
extern "C"  Il2CppObject * GPGSBtn_LoginCheckerNet_m90460827 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_LoginCheckerNet_m90460827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * V_0 = NULL;
	{
		U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * L_0 = (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 *)il2cpp_codegen_object_new(U3CLoginCheckerNetU3Ec__IteratorE_t3774081813_il2cpp_TypeInfo_var);
		U3CLoginCheckerNetU3Ec__IteratorE__ctor_m734193142(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * L_1 = V_0;
		return L_1;
	}
}
// System.Void GPGSBtn::ClickEvent()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral229116950;
extern const uint32_t GPGSBtn_ClickEvent_m2313098802_MetadataUsageId;
extern "C"  void GPGSBtn_ClickEvent_m2313098802 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_ClickEvent_m2313098802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_1 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_1);
		bool L_2 = GPGSMng_get_bLogin_m2039479974(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_3 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_3);
		GPGSMng_LoginGPGS_m3908873238(L_3, /*hidden argument*/NULL);
		Il2CppObject * L_4 = GPGSBtn_LoginCheckerNet_m90460827(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_4, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_5 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_5);
		GPGSMng_LogoutGPGS_m3304224809(L_5, /*hidden argument*/NULL);
		GPGSMng_t946704145 * L_6 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_6);
		GPGSMng_set_bLogin_m302910021(L_6, (bool)0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral229116950, 0, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void GPGSBtn::SettingUser()
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern const uint32_t GPGSBtn_SettingUser_m1225378973_MetadataUsageId;
extern "C"  void GPGSBtn_SettingUser_m1225378973 (GPGSBtn_t946693767 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSBtn_SettingUser_m1225378973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_User_Label_3();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_1 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_1);
		String_t* L_2 = GPGSMng_GetNameGPGS_m3486585297(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		RawImage_t821930207 * L_3 = __this->get_User_Texture_4();
		GPGSMng_t946704145 * L_4 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_4);
		Texture2D_t3884108195 * L_5 = GPGSMng_GetImageGPGS_m130434023(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RawImage_set_texture_m153141914(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GPGSBtn/<LoginChecker>c__IteratorD::.ctor()
extern "C"  void U3CLoginCheckerU3Ec__IteratorD__ctor_m3162131466 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GPGSBtn/<LoginChecker>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1830786770 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GPGSBtn/<LoginChecker>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m19758694 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean GPGSBtn/<LoginChecker>c__IteratorD::MoveNext()
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral73596745;
extern Il2CppCodeGenString* _stringLiteral69156280;
extern Il2CppCodeGenString* _stringLiteral2281505194;
extern const uint32_t U3CLoginCheckerU3Ec__IteratorD_MoveNext_m1345046674_MetadataUsageId;
extern "C"  bool U3CLoginCheckerU3Ec__IteratorD_MoveNext_m1345046674 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckerU3Ec__IteratorD_MoveNext_m1345046674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00e9;
		}
	}
	{
		goto IL_00f5;
	}

IL_0021:
	{
		GPGSBtn_t946693767 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_bt_8();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		GPGSBtn_t946693767 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = L_4->get_loginBt_9();
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		GPGSBtn_t946693767 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = L_6->get_exitBt_10();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_8 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_8);
		bool L_9 = GPGSMng_get_bLogin_m2039479974(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00ad;
		}
	}
	{
		GPGSBtn_t946693767 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		Text_t9039225 * L_11 = L_10->get_Login_Label_2();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral73596745);
		GPGSBtn_t946693767 * L_12 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_12);
		Text_t9039225 * L_13 = L_12->get_User_Label_3();
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, _stringLiteral69156280);
		GPGSBtn_t946693767 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		RawImage_t821930207 * L_15 = L_14->get_User_Texture_4();
		GPGSBtn_t946693767 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		Texture2D_t3884108195 * L_17 = L_16->get__img_5();
		NullCheck(L_15);
		RawImage_set_texture_m153141914(L_15, L_17, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00ad:
	{
		GPGSBtn_t946693767 * L_18 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_18);
		Text_t9039225 * L_19 = L_18->get_Login_Label_2();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, _stringLiteral2281505194);
		GPGSBtn_t946693767 * L_20 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_20);
		GPGSBtn_SettingUser_m1225378973(L_20, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		WaitForSeconds_t1615819279 * L_21 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_21, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_21);
		__this->set_U24PC_0(1);
		goto IL_00f7;
	}

IL_00e9:
	{
		goto IL_0021;
	}
	// Dead block : IL_00ee: ldarg.0

IL_00f5:
	{
		return (bool)0;
	}

IL_00f7:
	{
		return (bool)1;
	}
}
// System.Void GPGSBtn/<LoginChecker>c__IteratorD::Dispose()
extern "C"  void U3CLoginCheckerU3Ec__IteratorD_Dispose_m2164743623 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void GPGSBtn/<LoginChecker>c__IteratorD::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoginCheckerU3Ec__IteratorD_Reset_m808564407_MetadataUsageId;
extern "C"  void U3CLoginCheckerU3Ec__IteratorD_Reset_m808564407 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckerU3Ec__IteratorD_Reset_m808564407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::.ctor()
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE__ctor_m734193142 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GPGSBtn/<LoginCheckerNet>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerNetU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3205914332 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GPGSBtn/<LoginCheckerNet>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerNetU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m809081456 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean GPGSBtn/<LoginCheckerNet>c__IteratorE::MoveNext()
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1768030745_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m207825188_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral229116950;
extern Il2CppCodeGenString* _stringLiteral570880527;
extern Il2CppCodeGenString* _stringLiteral1632924745;
extern Il2CppCodeGenString* _stringLiteral1102060698;
extern Il2CppCodeGenString* _stringLiteral2387020765;
extern Il2CppCodeGenString* _stringLiteral3069746827;
extern Il2CppCodeGenString* _stringLiteral157937801;
extern Il2CppCodeGenString* _stringLiteral2805399355;
extern const uint32_t U3CLoginCheckerNetU3Ec__IteratorE_MoveNext_m4003472190_MetadataUsageId;
extern "C"  bool U3CLoginCheckerNetU3Ec__IteratorE_MoveNext_m4003472190 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckerNetU3Ec__IteratorE_MoveNext_m4003472190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0130;
		}
	}
	{
		goto IL_013c;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_2 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = GPGSMng_get_bLogin_m2039479974(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0114;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_4 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_5 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_5);
		String_t* L_6 = GPGSMng_GetIDGPGS_m601054273(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_userId_3(L_6);
		MainUserInfo_t2526075922 * L_7 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		GPGSMng_t946704145 * L_8 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_8);
		String_t* L_9 = GPGSMng_GetNameGPGS_m3486585297(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_userName_4(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_10 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		GPGSMng_t946704145 * L_11 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_11);
		String_t* L_12 = GPGSMng_GetIDGPGS_m601054273(L_11, /*hidden argument*/NULL);
		GPGSMng_t946704145 * L_13 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_13);
		String_t* L_14 = GPGSMng_GetNameGPGS_m3486585297(L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		NetworkMng_RequestLogin_m3564897283(L_10, L_12, L_14, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral229116950, 1, /*hidden argument*/NULL);
		GPGSMng_t946704145 * L_15 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_15);
		GPGSMng_set_bLogin_m302910021(L_15, (bool)1, /*hidden argument*/NULL);
		MainUserInfo_t2526075922 * L_16 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_16);
		String_t* L_17 = L_16->get_userId_3();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral570880527, L_17, /*hidden argument*/NULL);
		int32_t L_18 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1632924745, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00d2;
		}
	}
	{
		int32_t L_19 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1102060698, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00d2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2387020765, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_00d2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_20 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		int32_t L_21 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1102060698, /*hidden argument*/NULL);
		int32_t L_22 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral3069746827, /*hidden argument*/NULL);
		int32_t L_23 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1632924745, /*hidden argument*/NULL);
		int32_t L_24 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral157937801, /*hidden argument*/NULL);
		NullCheck(L_20);
		NetworkMng_RequestSaveGameData_m2757540381(L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
	}

IL_0104:
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral2805399355, 1, /*hidden argument*/NULL);
		goto IL_013c;
	}

IL_0114:
	{
		WaitForSeconds_t1615819279 * L_25 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_25, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_25);
		__this->set_U24PC_0(1);
		goto IL_013e;
	}

IL_0130:
	{
		goto IL_0021;
	}
	// Dead block : IL_0135: ldarg.0

IL_013c:
	{
		return (bool)0;
	}

IL_013e:
	{
		return (bool)1;
	}
}
// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::Dispose()
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE_Dispose_m1083255987 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void GPGSBtn/<LoginCheckerNet>c__IteratorE::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoginCheckerNetU3Ec__IteratorE_Reset_m2675593379_MetadataUsageId;
extern "C"  void U3CLoginCheckerNetU3Ec__IteratorE_Reset_m2675593379 (U3CLoginCheckerNetU3Ec__IteratorE_t3774081813 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoginCheckerNetU3Ec__IteratorE_Reset_m2675593379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GPGSMng::.ctor()
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1__ctor_m2612788014_MethodInfo_var;
extern const uint32_t GPGSMng__ctor_m3815572346_MetadataUsageId;
extern "C"  void GPGSMng__ctor_m3815572346 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng__ctor_m3815572346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		Singleton_1__ctor_m2612788014(__this, /*hidden argument*/Singleton_1__ctor_m2612788014_MethodInfo_var);
		return;
	}
}
// System.Boolean GPGSMng::get_bLogin()
extern "C"  bool GPGSMng_get_bLogin_m2039479974 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CbLoginU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void GPGSMng::set_bLogin(System.Boolean)
extern "C"  void GPGSMng_set_bLogin_m302910021 (GPGSMng_t946704145 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CbLoginU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void GPGSMng::InitializeGPGS()
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_InitializeGPGS_m137004335_MetadataUsageId;
extern "C"  void GPGSMng_InitializeGPGS_m137004335 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_InitializeGPGS_m137004335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GPGSMng_set_bLogin_m302910021(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var);
		PlayGamesPlatform_Activate_m3016211397(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GPGSMng::LoginGPGS()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t872614854_il2cpp_TypeInfo_var;
extern const MethodInfo* GPGSMng_LoginCallBackGPGS_m2686207762_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m377969142_MethodInfo_var;
extern const uint32_t GPGSMng_LoginGPGS_m3908873238_MetadataUsageId;
extern "C"  void GPGSMng_LoginGPGS_m3908873238 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_LoginGPGS_m3908873238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_2 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GPGSMng_LoginCallBackGPGS_m2686207762_MethodInfo_var);
		Action_1_t872614854 * L_4 = (Action_1_t872614854 *)il2cpp_codegen_object_new(Action_1_t872614854_il2cpp_TypeInfo_var);
		Action_1__ctor_m377969142(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m377969142_MethodInfo_var);
		NullCheck(L_2);
		InterfaceActionInvoker1< Action_1_t872614854 * >::Invoke(0 /* System.Void UnityEngine.SocialPlatforms.ILocalUser::Authenticate(System.Action`1<System.Boolean>) */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_2, L_4);
	}

IL_0025:
	{
		return;
	}
}
// System.Boolean GPGSMng::CheckLoginState()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_CheckLoginState_m222383964_MetadataUsageId;
extern "C"  bool GPGSMng_CheckLoginState_m222383964 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_CheckLoginState_m222383964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		return (bool)1;
	}
}
// System.Void GPGSMng::LoginCallBackGPGS(System.Boolean)
extern "C"  void GPGSMng_LoginCallBackGPGS_m2686207762 (GPGSMng_t946704145 * __this, bool ___result0, const MethodInfo* method)
{
	{
		bool L_0 = ___result0;
		GPGSMng_set_bLogin_m302910021(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GPGSMng::LogoutGPGS()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_LogoutGPGS_m3304224809_MetadataUsageId;
extern "C"  void GPGSMng_LogoutGPGS_m3304224809 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_LogoutGPGS_m3304224809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject * L_2 = Social_get_Active_m590102927(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((PlayGamesPlatform_t3624292130 *)CastclassClass(L_2, PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var)));
		PlayGamesPlatform_SignOut_m1679456084(((PlayGamesPlatform_t3624292130 *)CastclassClass(L_2, PlayGamesPlatform_t3624292130_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GPGSMng_set_bLogin_m302910021(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// UnityEngine.Texture2D GPGSMng::GetImageGPGS()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* IUserProfile_t598900827_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_GetImageGPGS_m130434023_MetadataUsageId;
extern "C"  Texture2D_t3884108195 * GPGSMng_GetImageGPGS_m130434023 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_GetImageGPGS_m130434023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Texture2D_t3884108195 * L_3 = InterfaceFuncInvoker0< Texture2D_t3884108195 * >::Invoke(2 /* UnityEngine.Texture2D UnityEngine.SocialPlatforms.IUserProfile::get_image() */, IUserProfile_t598900827_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}

IL_001a:
	{
		return (Texture2D_t3884108195 *)NULL;
	}
}
// System.String GPGSMng::GetNameGPGS()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* IUserProfile_t598900827_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_GetNameGPGS_m3486585297_MetadataUsageId;
extern "C"  String_t* GPGSMng_GetNameGPGS_m3486585297 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_GetNameGPGS_m3486585297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.IUserProfile::get_userName() */, IUserProfile_t598900827_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}

IL_001a:
	{
		return (String_t*)NULL;
	}
}
// System.String GPGSMng::GetIDGPGS()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* IUserProfile_t598900827_il2cpp_TypeInfo_var;
extern const uint32_t GPGSMng_GetIDGPGS_m601054273_MetadataUsageId;
extern "C"  String_t* GPGSMng_GetIDGPGS_m601054273 (GPGSMng_t946704145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GPGSMng_GetIDGPGS_m601054273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = Social_get_localUser_m2966161563(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String UnityEngine.SocialPlatforms.IUserProfile::get_id() */, IUserProfile_t598900827_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}

IL_001a:
	{
		return (String_t*)NULL;
	}
}
// System.Void GradeData::.ctor()
extern "C"  void GradeData__ctor_m3896706698 (GradeData_t483491841 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GradeDataInfo::.ctor()
extern Il2CppClass* GradeDataU5BU5D_t1462969052_il2cpp_TypeInfo_var;
extern Il2CppClass* GradeData_t483491841_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral51991588;
extern Il2CppCodeGenString* _stringLiteral602269412;
extern Il2CppCodeGenString* _stringLiteral50897784;
extern Il2CppCodeGenString* _stringLiteral602269409;
extern Il2CppCodeGenString* _stringLiteral1587503;
extern Il2CppCodeGenString* _stringLiteral602269414;
extern Il2CppCodeGenString* _stringLiteral1737688;
extern Il2CppCodeGenString* _stringLiteral602269411;
extern const uint32_t GradeDataInfo__ctor_m1021303292_MetadataUsageId;
extern "C"  void GradeDataInfo__ctor_m1021303292 (GradeDataInfo_t1480749135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GradeDataInfo__ctor_m1021303292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_grade_data_0(((GradeDataU5BU5D_t1462969052*)SZArrayNew(GradeDataU5BU5D_t1462969052_il2cpp_TypeInfo_var, (uint32_t)4)));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_015f;
	}

IL_0019:
	{
		GradeDataU5BU5D_t1462969052* L_0 = __this->get_grade_data_0();
		int32_t L_1 = V_0;
		GradeData_t483491841 * L_2 = (GradeData_t483491841 *)il2cpp_codegen_object_new(GradeData_t483491841_il2cpp_TypeInfo_var);
		GradeData__ctor_m3896706698(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (GradeData_t483491841 *)L_2);
		int32_t L_3 = V_0;
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_0043;
		}
		if (L_4 == 1)
		{
			goto IL_0089;
		}
		if (L_4 == 2)
		{
			goto IL_00cf;
		}
		if (L_4 == 3)
		{
			goto IL_0115;
		}
	}
	{
		goto IL_015b;
	}

IL_0043:
	{
		GradeDataU5BU5D_t1462969052* L_5 = __this->get_grade_data_0();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		GradeData_t483491841 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		L_8->set_grand_up_0(((int32_t)70));
		GradeDataU5BU5D_t1462969052* L_9 = __this->get_grade_data_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		GradeData_t483491841 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		L_12->set_name_1(_stringLiteral51991588);
		GradeDataU5BU5D_t1462969052* L_13 = __this->get_grade_data_0();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		GradeData_t483491841 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		L_16->set_teacher_name_2(_stringLiteral602269412);
		GradeDataU5BU5D_t1462969052* L_17 = __this->get_grade_data_0();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		GradeData_t483491841 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		L_20->set_guest_cnt_3(2);
		goto IL_015b;
	}

IL_0089:
	{
		GradeDataU5BU5D_t1462969052* L_21 = __this->get_grade_data_0();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		GradeData_t483491841 * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		L_24->set_grand_up_0(((int32_t)78));
		GradeDataU5BU5D_t1462969052* L_25 = __this->get_grade_data_0();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		GradeData_t483491841 * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		L_28->set_name_1(_stringLiteral50897784);
		GradeDataU5BU5D_t1462969052* L_29 = __this->get_grade_data_0();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		GradeData_t483491841 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		L_32->set_teacher_name_2(_stringLiteral602269409);
		GradeDataU5BU5D_t1462969052* L_33 = __this->get_grade_data_0();
		int32_t L_34 = V_0;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		GradeData_t483491841 * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		L_36->set_guest_cnt_3(2);
		goto IL_015b;
	}

IL_00cf:
	{
		GradeDataU5BU5D_t1462969052* L_37 = __this->get_grade_data_0();
		int32_t L_38 = V_0;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		GradeData_t483491841 * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		L_40->set_grand_up_0(((int32_t)83));
		GradeDataU5BU5D_t1462969052* L_41 = __this->get_grade_data_0();
		int32_t L_42 = V_0;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = L_42;
		GradeData_t483491841 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		L_44->set_name_1(_stringLiteral1587503);
		GradeDataU5BU5D_t1462969052* L_45 = __this->get_grade_data_0();
		int32_t L_46 = V_0;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		GradeData_t483491841 * L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_48);
		L_48->set_teacher_name_2(_stringLiteral602269414);
		GradeDataU5BU5D_t1462969052* L_49 = __this->get_grade_data_0();
		int32_t L_50 = V_0;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		GradeData_t483491841 * L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		NullCheck(L_52);
		L_52->set_guest_cnt_3(2);
		goto IL_015b;
	}

IL_0115:
	{
		GradeDataU5BU5D_t1462969052* L_53 = __this->get_grade_data_0();
		int32_t L_54 = V_0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		GradeData_t483491841 * L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_56);
		L_56->set_grand_up_0(((int32_t)87));
		GradeDataU5BU5D_t1462969052* L_57 = __this->get_grade_data_0();
		int32_t L_58 = V_0;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		int32_t L_59 = L_58;
		GradeData_t483491841 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		NullCheck(L_60);
		L_60->set_name_1(_stringLiteral1737688);
		GradeDataU5BU5D_t1462969052* L_61 = __this->get_grade_data_0();
		int32_t L_62 = V_0;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = L_62;
		GradeData_t483491841 * L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_64);
		L_64->set_teacher_name_2(_stringLiteral602269411);
		GradeDataU5BU5D_t1462969052* L_65 = __this->get_grade_data_0();
		int32_t L_66 = V_0;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_66);
		int32_t L_67 = L_66;
		GradeData_t483491841 * L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_68);
		L_68->set_guest_cnt_3(2);
		goto IL_015b;
	}

IL_015b:
	{
		int32_t L_69 = V_0;
		V_0 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_70 = V_0;
		if ((((int32_t)L_70) < ((int32_t)4)))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}
}
// System.Void GradeDataInfo::.cctor()
extern Il2CppClass* GradeDataInfo_t1480749135_il2cpp_TypeInfo_var;
extern const uint32_t GradeDataInfo__cctor_m1113534769_MetadataUsageId;
extern "C"  void GradeDataInfo__cctor_m1113534769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GradeDataInfo__cctor_m1113534769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GradeDataInfo_t1480749135 * L_0 = (GradeDataInfo_t1480749135 *)il2cpp_codegen_object_new(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		GradeDataInfo__ctor_m1021303292(L_0, /*hidden argument*/NULL);
		((GradeDataInfo_t1480749135_StaticFields*)GradeDataInfo_t1480749135_il2cpp_TypeInfo_var->static_fields)->set_instance_1(L_0);
		return;
	}
}
// GradeDataInfo GradeDataInfo::GetInstance()
extern Il2CppClass* GradeDataInfo_t1480749135_il2cpp_TypeInfo_var;
extern const uint32_t GradeDataInfo_GetInstance_m2964107273_MetadataUsageId;
extern "C"  GradeDataInfo_t1480749135 * GradeDataInfo_GetInstance_m2964107273 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GradeDataInfo_GetInstance_m2964107273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		GradeDataInfo_t1480749135 * L_0 = ((GradeDataInfo_t1480749135_StaticFields*)GradeDataInfo_t1480749135_il2cpp_TypeInfo_var->static_fields)->get_instance_1();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GradeDataInfo_t1480749135 * L_1 = (GradeDataInfo_t1480749135 *)il2cpp_codegen_object_new(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		GradeDataInfo__ctor_m1021303292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		((GradeDataInfo_t1480749135_StaticFields*)GradeDataInfo_t1480749135_il2cpp_TypeInfo_var->static_fields)->set_instance_1(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		GradeDataInfo_t1480749135 * L_2 = ((GradeDataInfo_t1480749135_StaticFields*)GradeDataInfo_t1480749135_il2cpp_TypeInfo_var->static_fields)->get_instance_1();
		return L_2;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::.ctor()
extern "C"  void ActivateGameObject__ctor_m31587930 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::Reset()
extern "C"  void ActivateGameObject_Reset_m1972988167 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_activate_12(L_0);
		FsmBool_t1075959796 * L_1 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_recursive_13(L_1);
		__this->set_resetOnExit_14((bool)0);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnEnter()
extern "C"  void ActivateGameObject_OnEnter_m3977925553 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	{
		ActivateGameObject_DoActivateGameObject_m3099810041(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnUpdate()
extern "C"  void ActivateGameObject_OnUpdate_m2190167218 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	{
		ActivateGameObject_DoActivateGameObject_m3099810041(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnExit()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t ActivateGameObject_OnExit_m2492544455_MetadataUsageId;
extern "C"  void ActivateGameObject_OnExit_m2492544455 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivateGameObject_OnExit_m2492544455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_activatedGameObject_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_resetOnExit_14();
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		FsmBool_t1075959796 * L_3 = __this->get_recursive_13();
		NullCheck(L_3);
		bool L_4 = FsmBool_get_Value_m3101329097(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		GameObject_t3674682005 * L_5 = __this->get_activatedGameObject_16();
		FsmBool_t1075959796 * L_6 = __this->get_activate_12();
		NullCheck(L_6);
		bool L_7 = FsmBool_get_Value_m3101329097(L_6, /*hidden argument*/NULL);
		ActivateGameObject_SetActiveRecursively_m1268422564(__this, L_5, (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		goto IL_0065;
	}

IL_004c:
	{
		GameObject_t3674682005 * L_8 = __this->get_activatedGameObject_16();
		FsmBool_t1075959796 * L_9 = __this->get_activate_12();
		NullCheck(L_9);
		bool L_10 = FsmBool_get_Value_m3101329097(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::DoActivateGameObject()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t ActivateGameObject_DoActivateGameObject_m3099810041_MetadataUsageId;
extern "C"  void ActivateGameObject_DoActivateGameObject_m3099810041 (ActivateGameObject_t1848802188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivateGameObject_DoActivateGameObject_m3099810041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmBool_t1075959796 * L_5 = __this->get_recursive_13();
		NullCheck(L_5);
		bool L_6 = FsmBool_get_Value_m3101329097(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		FsmBool_t1075959796 * L_8 = __this->get_activate_12();
		NullCheck(L_8);
		bool L_9 = FsmBool_get_Value_m3101329097(L_8, /*hidden argument*/NULL);
		ActivateGameObject_SetActiveRecursively_m1268422564(__this, L_7, L_9, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0046:
	{
		GameObject_t3674682005 * L_10 = V_0;
		FsmBool_t1075959796 * L_11 = __this->get_activate_12();
		NullCheck(L_11);
		bool L_12 = FsmBool_get_Value_m3101329097(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, L_12, /*hidden argument*/NULL);
	}

IL_0057:
	{
		GameObject_t3674682005 * L_13 = V_0;
		__this->set_activatedGameObject_16(L_13);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::SetActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t1659122786_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t ActivateGameObject_SetActiveRecursively_m1268422564_MetadataUsageId;
extern "C"  void ActivateGameObject_SetActiveRecursively_m1268422564 (ActivateGameObject_t1848802188 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivateGameObject_SetActiveRecursively_m1268422564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t3674682005 * L_0 = ___go0;
		bool L_1 = ___state1;
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, L_1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = ___go0;
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = GameObject_get_transform_m1278640159(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject * L_4 = Transform_GetEnumerator_m688365631(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0018:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_5);
			V_0 = ((Transform_t1659122786 *)CastclassClass(L_6, Transform_t1659122786_il2cpp_TypeInfo_var));
			Transform_t1659122786 * L_7 = V_0;
			NullCheck(L_7);
			GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
			bool L_9 = ___state1;
			ActivateGameObject_SetActiveRecursively_m1268422564(__this, L_8, L_9, /*hidden argument*/NULL);
		}

IL_0031:
		{
			Il2CppObject * L_10 = V_1;
			NullCheck(L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0018;
			}
		}

IL_003c:
		{
			IL2CPP_LEAVE(0x53, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_12 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_12, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_13 = V_2;
			if (L_13)
			{
				goto IL_004c;
			}
		}

IL_004b:
		{
			IL2CPP_END_FINALLY(65)
		}

IL_004c:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_14);
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0053:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::.ctor()
extern "C"  void AddAnimationClip__ctor_m3321568075 (AddAnimationClip_t922763643 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AddAnimationClip_Reset_m968001016_MetadataUsageId;
extern "C"  void AddAnimationClip_Reset_m968001016 (AddAnimationClip_t922763643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddAnimationClip_Reset_m968001016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_animationClip_12((FsmObject_t821476169 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_animationName_13(L_1);
		FsmInt_t1596138449 * L_2 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_firstFrame_14(L_2);
		FsmInt_t1596138449 * L_3 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_lastFrame_15(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_addLoopFrame_16(L_4);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::OnEnter()
extern "C"  void AddAnimationClip_OnEnter_m257947746 (AddAnimationClip_t922763643 * __this, const MethodInfo* method)
{
	{
		AddAnimationClip_DoAddAnimationClip_m458185623(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddAnimationClip::DoAddAnimationClip()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationClip_t2007702890_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimation_t1724966010_m1707361494_MethodInfo_var;
extern const uint32_t AddAnimationClip_DoAddAnimationClip_m458185623_MetadataUsageId;
extern "C"  void AddAnimationClip_DoAddAnimationClip_m458185623 (AddAnimationClip_t922763643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddAnimationClip_DoAddAnimationClip_m458185623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AnimationClip_t2007702890 * V_1 = NULL;
	Animation_t1724966010 * V_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmObject_t821476169 * L_5 = __this->get_animationClip_12();
		NullCheck(L_5);
		Object_t3071478659 * L_6 = FsmObject_get_Value_m188501991(L_5, /*hidden argument*/NULL);
		V_1 = ((AnimationClip_t2007702890 *)IsInstSealed(L_6, AnimationClip_t2007702890_il2cpp_TypeInfo_var));
		AnimationClip_t2007702890 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Animation_t1724966010 * L_10 = GameObject_GetComponent_TisAnimation_t1724966010_m1707361494(L_9, /*hidden argument*/GameObject_GetComponent_TisAnimation_t1724966010_m1707361494_MethodInfo_var);
		V_2 = L_10;
		FsmInt_t1596138449 * L_11 = __this->get_firstFrame_14();
		NullCheck(L_11);
		int32_t L_12 = FsmInt_get_Value_m27059446(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_007b;
		}
	}
	{
		FsmInt_t1596138449 * L_13 = __this->get_lastFrame_15();
		NullCheck(L_13);
		int32_t L_14 = FsmInt_get_Value_m27059446(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_007b;
		}
	}
	{
		Animation_t1724966010 * L_15 = V_2;
		AnimationClip_t2007702890 * L_16 = V_1;
		FsmString_t952858651 * L_17 = __this->get_animationName_13();
		NullCheck(L_17);
		String_t* L_18 = FsmString_get_Value_m872383149(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Animation_AddClip_m3358255085(L_15, L_16, L_18, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_007b:
	{
		Animation_t1724966010 * L_19 = V_2;
		AnimationClip_t2007702890 * L_20 = V_1;
		FsmString_t952858651 * L_21 = __this->get_animationName_13();
		NullCheck(L_21);
		String_t* L_22 = FsmString_get_Value_m872383149(L_21, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_23 = __this->get_firstFrame_14();
		NullCheck(L_23);
		int32_t L_24 = FsmInt_get_Value_m27059446(L_23, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_25 = __this->get_lastFrame_15();
		NullCheck(L_25);
		int32_t L_26 = FsmInt_get_Value_m27059446(L_25, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_27 = __this->get_addLoopFrame_16();
		NullCheck(L_27);
		bool L_28 = FsmBool_get_Value_m3101329097(L_27, /*hidden argument*/NULL);
		NullCheck(L_19);
		Animation_AddClip_m770980048(L_19, L_20, L_22, L_24, L_26, L_28, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddComponent::.ctor()
extern "C"  void AddComponent__ctor_m449988450 (AddComponent_t2040077188 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddComponent::Reset()
extern "C"  void AddComponent_Reset_m2391388687 (AddComponent_t2040077188 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_component_12((FsmString_t952858651 *)NULL);
		__this->set_storeComponent_13((FsmObject_t821476169 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnEnter()
extern "C"  void AddComponent_OnEnter_m2333899449 (AddComponent_t2040077188 * __this, const MethodInfo* method)
{
	{
		AddComponent_DoAddComponent_m2270947369(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnExit()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AddComponent_OnExit_m2578058687_MetadataUsageId;
extern "C"  void AddComponent_OnExit_m2578058687 (AddComponent_t2040077188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddComponent_OnExit_m2578058687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_removeOnExit_14();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_2 = __this->get_addedComponent_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_4 = __this->get_addedComponent_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddComponent::DoAddComponent()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t1202855664_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1315133089;
extern const uint32_t AddComponent_DoAddComponent_m2270947369_MetadataUsageId;
extern "C"  void AddComponent_DoAddComponent_m2270947369 (AddComponent_t2040077188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddComponent_DoAddComponent_m2270947369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		FsmString_t952858651 * L_6 = __this->get_component_12();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t1202855664_il2cpp_TypeInfo_var);
		Type_t * L_8 = ReflectionUtils_GetGlobalType_m3454401768(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3501516275 * L_9 = GameObject_AddComponent_m2208780168(L_5, L_8, /*hidden argument*/NULL);
		__this->set_addedComponent_15(L_9);
		FsmObject_t821476169 * L_10 = __this->get_storeComponent_13();
		Component_t3501516275 * L_11 = __this->get_addedComponent_15();
		NullCheck(L_10);
		FsmObject_set_Value_m867520242(L_10, L_11, /*hidden argument*/NULL);
		Component_t3501516275 * L_12 = __this->get_addedComponent_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		FsmString_t952858651 * L_14 = __this->get_component_12();
		NullCheck(L_14);
		String_t* L_15 = FsmString_get_Value_m872383149(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1315133089, L_15, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_16, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t AddExplosionForce__ctor_m1274421937_MetadataUsageId;
extern "C"  void AddExplosionForce__ctor_m1274421937 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddExplosionForce__ctor_m1274421937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t AddExplosionForce_Reset_m3215822174_MetadataUsageId;
extern "C"  void AddExplosionForce_Reset_m3215822174 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddExplosionForce_Reset_m3215822174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_center_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_upwardsModifier_17(L_3);
		__this->set_forceMode_18(0);
		__this->set_everyFrame_19((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnPreprocess()
extern "C"  void AddExplosionForce_OnPreprocess_m3189620062 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnEnter()
extern "C"  void AddExplosionForce_OnEnter_m45530696 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	{
		AddExplosionForce_DoAddExplosionForce_m3509564123(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_19();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnFixedUpdate()
extern "C"  void AddExplosionForce_OnFixedUpdate_m24215629 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	{
		AddExplosionForce_DoAddExplosionForce_m3509564123(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::DoAddExplosionForce()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t AddExplosionForce_DoAddExplosionForce_m3509564123_MetadataUsageId;
extern "C"  void AddExplosionForce_DoAddExplosionForce_m3509564123 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddExplosionForce_DoAddExplosionForce_m3509564123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_13();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_002b;
	}

IL_001b:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_13();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		V_0 = G_B3_0;
		FsmVector3_t533912882 * L_6 = __this->get_center_14();
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t3674682005 * L_7 = V_0;
		bool L_8 = ComponentAction_1_UpdateCache_m864821753(__this, L_7, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_8)
		{
			goto IL_0044;
		}
	}

IL_0043:
	{
		return;
	}

IL_0044:
	{
		Rigidbody_t3346577219 * L_9 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		FsmFloat_t2134102846 * L_10 = __this->get_force_15();
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_12 = __this->get_center_14();
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = FsmVector3_get_Value_m2779135117(L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_radius_16();
		NullCheck(L_14);
		float L_15 = FsmFloat_get_Value_m4137923823(L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_upwardsModifier_17();
		NullCheck(L_16);
		float L_17 = FsmFloat_get_Value_m4137923823(L_16, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_forceMode_18();
		NullCheck(L_9);
		Rigidbody_AddExplosionForce_m196999228(L_9, L_11, L_13, L_15, L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t AddForce__ctor_m276996916_MetadataUsageId;
extern "C"  void AddForce__ctor_m276996916 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce__ctor_m276996916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AddForce_Reset_m2218397153_MetadataUsageId;
extern "C"  void AddForce_Reset_m2218397153 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce_Reset_m2218397153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_atPosition_14(L_2);
		__this->set_vector_15((FsmVector3_t533912882 *)NULL);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmFloat_t2134102846 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_1;
		__this->set_x_16(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmFloat_t2134102846 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_1;
		__this->set_y_17(L_8);
		FsmFloat_t2134102846 * L_9 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmFloat_t2134102846 * L_10 = V_1;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = V_1;
		__this->set_z_18(L_11);
		__this->set_space_19(0);
		__this->set_forceMode_20(0);
		__this->set_everyFrame_21((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnPreprocess()
extern "C"  void AddForce_OnPreprocess_m3267048315 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnEnter()
extern "C"  void AddForce_OnEnter_m3592759819 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	{
		AddForce_DoAddForce_m207894405(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_21();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnFixedUpdate()
extern "C"  void AddForce_OnFixedUpdate_m2424491472 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	{
		AddForce_DoAddForce_m207894405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce::DoAddForce()
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t AddForce_DoAddForce_m207894405_MetadataUsageId;
extern "C"  void AddForce_DoAddForce_m207894405 (AddForce_t2783319922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce_DoAddForce_m207894405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector_15();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t4282066566  L_7 = V_2;
		G_B5_0 = L_7;
		goto IL_0048;
	}

IL_003d:
	{
		FsmVector3_t533912882 * L_8 = __this->get_vector_15();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0048:
	{
		V_1 = G_B5_0;
		FsmFloat_t2134102846 * L_10 = __this->get_x_16();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006b;
		}
	}
	{
		FsmFloat_t2134102846 * L_12 = __this->get_x_16();
		NullCheck(L_12);
		float L_13 = FsmFloat_get_Value_m4137923823(L_12, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_13);
	}

IL_006b:
	{
		FsmFloat_t2134102846 * L_14 = __this->get_y_17();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_008d;
		}
	}
	{
		FsmFloat_t2134102846 * L_16 = __this->get_y_17();
		NullCheck(L_16);
		float L_17 = FsmFloat_get_Value_m4137923823(L_16, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_17);
	}

IL_008d:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_z_18();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00af;
		}
	}
	{
		FsmFloat_t2134102846 * L_20 = __this->get_z_18();
		NullCheck(L_20);
		float L_21 = FsmFloat_get_Value_m4137923823(L_20, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_21);
	}

IL_00af:
	{
		int32_t L_22 = __this->get_space_19();
		if (L_22)
		{
			goto IL_0103;
		}
	}
	{
		FsmVector3_t533912882 * L_23 = __this->get_atPosition_14();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00ec;
		}
	}
	{
		Rigidbody_t3346577219 * L_25 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		Vector3_t4282066566  L_26 = V_1;
		FsmVector3_t533912882 * L_27 = __this->get_atPosition_14();
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = FsmVector3_get_Value_m2779135117(L_27, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_forceMode_20();
		NullCheck(L_25);
		Rigidbody_AddForceAtPosition_m1266619363(L_25, L_26, L_28, L_29, /*hidden argument*/NULL);
		goto IL_00fe;
	}

IL_00ec:
	{
		Rigidbody_t3346577219 * L_30 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		Vector3_t4282066566  L_31 = V_1;
		int32_t L_32 = __this->get_forceMode_20();
		NullCheck(L_30);
		Rigidbody_AddForce_m557267180(L_30, L_31, L_32, /*hidden argument*/NULL);
	}

IL_00fe:
	{
		goto IL_0115;
	}

IL_0103:
	{
		Rigidbody_t3346577219 * L_33 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		Vector3_t4282066566  L_34 = V_1;
		int32_t L_35 = __this->get_forceMode_20();
		NullCheck(L_33);
		Rigidbody_AddRelativeForce_m2803598808(L_33, L_34, L_35, /*hidden argument*/NULL);
	}

IL_0115:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2621392432_MethodInfo_var;
extern const uint32_t AddForce2d__ctor_m742511810_MetadataUsageId;
extern "C"  void AddForce2d__ctor_m742511810 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce2d__ctor_m742511810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2621392432(__this, /*hidden argument*/ComponentAction_1__ctor_m2621392432_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AddForce2d_Reset_m2683912047_MetadataUsageId;
extern "C"  void AddForce2d_Reset_m2683912047 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce2d_Reset_m2683912047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	FsmFloat_t2134102846 * V_2 = NULL;
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_atPosition_15(L_2);
		__this->set_forceMode_14(0);
		__this->set_vector_16((FsmVector2_t533912881 *)NULL);
		FsmVector3_t533912882 * L_3 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmVector3_t533912882 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_5 = V_1;
		__this->set_vector3_19(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		FsmFloat_t2134102846 * L_7 = V_2;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_2;
		__this->set_x_17(L_8);
		FsmFloat_t2134102846 * L_9 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_9, /*hidden argument*/NULL);
		V_2 = L_9;
		FsmFloat_t2134102846 * L_10 = V_2;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = V_2;
		__this->set_y_18(L_11);
		__this->set_everyFrame_20((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnPreprocess()
extern "C"  void AddForce2d_OnPreprocess_m2056898093 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnEnter()
extern "C"  void AddForce2d_OnEnter_m4275974169 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	{
		AddForce2d_DoAddForce_m4291546807(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_20();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnFixedUpdate()
extern "C"  void AddForce2d_OnFixedUpdate_m3564540254 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	{
		AddForce2d_DoAddForce_m4291546807(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::DoAddForce()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var;
extern const uint32_t AddForce2d_DoAddForce_m4291546807_MetadataUsageId;
extern "C"  void AddForce2d_DoAddForce_m4291546807 (AddForce2d_t4008896548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddForce2d_DoAddForce_m4291546807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2944103819(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector2_t533912881 * L_5 = __this->get_vector_16();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		FsmFloat_t2134102846 * L_7 = __this->get_x_17();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_y_18();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		Vector2_t4282066565  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m1517109030(&L_11, L_8, L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
		goto IL_005a;
	}

IL_004f:
	{
		FsmVector2_t533912881 * L_12 = __this->get_vector_16();
		NullCheck(L_12);
		Vector2_t4282066565  L_13 = FsmVector2_get_Value_m1313754285(L_12, /*hidden argument*/NULL);
		G_B5_0 = L_13;
	}

IL_005a:
	{
		V_1 = G_B5_0;
		FsmVector3_t533912882 * L_14 = __this->get_vector3_19();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_009f;
		}
	}
	{
		FsmVector3_t533912882 * L_16 = __this->get_vector3_19();
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = FsmVector3_get_Value_m2779135117(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = (&V_2)->get_x_1();
		(&V_1)->set_x_1(L_18);
		FsmVector3_t533912882 * L_19 = __this->get_vector3_19();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = FsmVector3_get_Value_m2779135117(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = (&V_3)->get_y_2();
		(&V_1)->set_y_2(L_21);
	}

IL_009f:
	{
		FsmFloat_t2134102846 * L_22 = __this->get_x_17();
		NullCheck(L_22);
		bool L_23 = NamedVariable_get_IsNone_m281035543(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00c1;
		}
	}
	{
		FsmFloat_t2134102846 * L_24 = __this->get_x_17();
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_25);
	}

IL_00c1:
	{
		FsmFloat_t2134102846 * L_26 = __this->get_y_18();
		NullCheck(L_26);
		bool L_27 = NamedVariable_get_IsNone_m281035543(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00e3;
		}
	}
	{
		FsmFloat_t2134102846 * L_28 = __this->get_y_18();
		NullCheck(L_28);
		float L_29 = FsmFloat_get_Value_m4137923823(L_28, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_29);
	}

IL_00e3:
	{
		FsmVector2_t533912881 * L_30 = __this->get_atPosition_15();
		NullCheck(L_30);
		bool L_31 = NamedVariable_get_IsNone_m281035543(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0115;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_32 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		Vector2_t4282066565  L_33 = V_1;
		FsmVector2_t533912881 * L_34 = __this->get_atPosition_15();
		NullCheck(L_34);
		Vector2_t4282066565  L_35 = FsmVector2_get_Value_m1313754285(L_34, /*hidden argument*/NULL);
		int32_t L_36 = __this->get_forceMode_14();
		NullCheck(L_32);
		Rigidbody2D_AddForceAtPosition_m174512961(L_32, L_33, L_35, L_36, /*hidden argument*/NULL);
		goto IL_0127;
	}

IL_0115:
	{
		Rigidbody2D_t1743771669 * L_37 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		Vector2_t4282066565  L_38 = V_1;
		int32_t L_39 = __this->get_forceMode_14();
		NullCheck(L_37);
		Rigidbody2D_AddForce_m4161385513(L_37, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0127:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::.ctor()
extern "C"  void AddMixingTransform__ctor_m1655337849 (AddMixingTransform_t436139917 * __this, const MethodInfo* method)
{
	{
		BaseAnimationAction__ctor_m4099946943(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AddMixingTransform_Reset_m3596738086_MetadataUsageId;
extern "C"  void AddMixingTransform_Reset_m3596738086 (AddMixingTransform_t436139917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddMixingTransform_Reset_m3596738086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_animationName_14(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_transform_15(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_recursive_16(L_4);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::OnEnter()
extern "C"  void AddMixingTransform_OnEnter_m1033501968 (AddMixingTransform_t436139917 * __this, const MethodInfo* method)
{
	{
		AddMixingTransform_DoAddMixingTransform_m2494144955(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::DoAddMixingTransform()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_animation_m4211453288_MethodInfo_var;
extern const uint32_t AddMixingTransform_DoAddMixingTransform_m2494144955_MetadataUsageId;
extern "C"  void AddMixingTransform_DoAddMixingTransform_m2494144955 (AddMixingTransform_t436139917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddMixingTransform_DoAddMixingTransform_m2494144955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AnimationState_t3682323633 * V_1 = NULL;
	Transform_t1659122786 * V_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3952061936(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Animation_t1724966010 * L_5 = ComponentAction_1_get_animation_m4211453288(__this, /*hidden argument*/ComponentAction_1_get_animation_m4211453288_MethodInfo_var);
		FsmString_t952858651 * L_6 = __this->get_animationName_14();
		NullCheck(L_6);
		String_t* L_7 = FsmString_get_Value_m872383149(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationState_t3682323633 * L_8 = Animation_get_Item_m2669576386(L_5, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		AnimationState_t3682323633 * L_9 = V_1;
		bool L_10 = TrackedReference_op_Equality_m4125598506(NULL /*static, unused*/, L_9, (TrackedReference_t2089686725 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		return;
	}

IL_0043:
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		FsmString_t952858651 * L_13 = __this->get_transform_15();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_15 = Transform_Find_m3950449392(L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		AnimationState_t3682323633 * L_16 = V_1;
		Transform_t1659122786 * L_17 = V_2;
		FsmBool_t1075959796 * L_18 = __this->get_recursive_16();
		NullCheck(L_18);
		bool L_19 = FsmBool_get_Value_m3101329097(L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		AnimationState_AddMixingTransform_m4158051049(L_16, L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddScript::.ctor()
extern "C"  void AddScript__ctor_m3025765980 (AddScript_t95714330 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddScript::Reset()
extern "C"  void AddScript_Reset_m672198921 (AddScript_t95714330 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_script_12((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddScript::OnEnter()
extern "C"  void AddScript_OnEnter_m3754943283 (AddScript_t95714330 * __this, const MethodInfo* method)
{
	AddScript_t95714330 * G_B2_0 = NULL;
	AddScript_t95714330 * G_B1_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	AddScript_t95714330 * G_B3_1 = NULL;
	{
		FsmOwnerDefault_t251897112 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		int32_t L_1 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_001c;
		}
	}
	{
		GameObject_t3674682005 * L_2 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_001c:
	{
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_11();
		NullCheck(L_3);
		FsmGameObject_t1697147867 * L_4 = FsmOwnerDefault_get_GameObject_m3249227945(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		AddScript_DoAddComponent_m3760538791(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddScript::OnExit()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AddScript_OnExit_m822783493_MetadataUsageId;
extern "C"  void AddScript_OnExit_m822783493 (AddScript_t95714330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddScript_OnExit_m822783493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_removeOnExit_13();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_2 = __this->get_addedComponent_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Component_t3501516275 * L_4 = __this->get_addedComponent_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddScript::DoAddComponent(UnityEngine.GameObject)
extern Il2CppClass* ReflectionUtils_t1202855664_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3105774643;
extern const uint32_t AddScript_DoAddComponent_m3760538791_MetadataUsageId;
extern "C"  void AddScript_DoAddComponent_m3760538791 (AddScript_t95714330 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddScript_DoAddComponent_m3760538791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___go0;
		FsmString_t952858651 * L_1 = __this->get_script_12();
		NullCheck(L_1);
		String_t* L_2 = FsmString_get_Value_m872383149(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t1202855664_il2cpp_TypeInfo_var);
		Type_t * L_3 = ReflectionUtils_GetGlobalType_m3454401768(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3501516275 * L_4 = GameObject_AddComponent_m2208780168(L_0, L_3, /*hidden argument*/NULL);
		__this->set_addedComponent_14(L_4);
		Component_t3501516275 * L_5 = __this->get_addedComponent_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		FsmString_t952858651 * L_7 = __this->get_script_12();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3105774643, L_8, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_9, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t AddTorque__ctor_m2052156413_MetadataUsageId;
extern "C"  void AddTorque__ctor_m2052156413 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddTorque__ctor_m2052156413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AddTorque_Reset_m3993556650_MetadataUsageId;
extern "C"  void AddTorque_Reset_m3993556650 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddTorque_Reset_m3993556650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_x_15(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_y_16(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_z_17(L_8);
		__this->set_space_18(0);
		__this->set_forceMode_19(0);
		__this->set_everyFrame_20((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnPreprocess()
extern "C"  void AddTorque_OnPreprocess_m3490162322 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnEnter()
extern "C"  void AddTorque_OnEnter_m124052628 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	{
		AddTorque_DoAddTorque_m3986550619(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_20();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnFixedUpdate()
extern "C"  void AddTorque_OnFixedUpdate_m751091097 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	{
		AddTorque_DoAddTorque_m3986550619(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque::DoAddTorque()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t AddTorque_DoAddTorque_m3986550619_MetadataUsageId;
extern "C"  void AddTorque_DoAddTorque_m3986550619 (AddTorque_t135433561 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddTorque_DoAddTorque_m3986550619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmVector3_t533912882 * L_5 = __this->get_vector_14();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		FsmFloat_t2134102846 * L_7 = __this->get_x_15();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_9 = __this->get_y_16();
		NullCheck(L_9);
		float L_10 = FsmFloat_get_Value_m4137923823(L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_11 = __this->get_z_17();
		NullCheck(L_11);
		float L_12 = FsmFloat_get_Value_m4137923823(L_11, /*hidden argument*/NULL);
		Vector3_t4282066566  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2926210380(&L_13, L_8, L_10, L_12, /*hidden argument*/NULL);
		G_B5_0 = L_13;
		goto IL_0065;
	}

IL_005a:
	{
		FsmVector3_t533912882 * L_14 = __this->get_vector_14();
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = FsmVector3_get_Value_m2779135117(L_14, /*hidden argument*/NULL);
		G_B5_0 = L_15;
	}

IL_0065:
	{
		V_1 = G_B5_0;
		FsmFloat_t2134102846 * L_16 = __this->get_x_15();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0088;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_x_15();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_19);
	}

IL_0088:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_y_16();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00aa;
		}
	}
	{
		FsmFloat_t2134102846 * L_22 = __this->get_y_16();
		NullCheck(L_22);
		float L_23 = FsmFloat_get_Value_m4137923823(L_22, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_23);
	}

IL_00aa:
	{
		FsmFloat_t2134102846 * L_24 = __this->get_z_17();
		NullCheck(L_24);
		bool L_25 = NamedVariable_get_IsNone_m281035543(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00cc;
		}
	}
	{
		FsmFloat_t2134102846 * L_26 = __this->get_z_17();
		NullCheck(L_26);
		float L_27 = FsmFloat_get_Value_m4137923823(L_26, /*hidden argument*/NULL);
		(&V_1)->set_z_3(L_27);
	}

IL_00cc:
	{
		int32_t L_28 = __this->get_space_18();
		if (L_28)
		{
			goto IL_00ee;
		}
	}
	{
		Rigidbody_t3346577219 * L_29 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		Vector3_t4282066566  L_30 = V_1;
		int32_t L_31 = __this->get_forceMode_19();
		NullCheck(L_29);
		Rigidbody_AddTorque_m3009708185(L_29, L_30, L_31, /*hidden argument*/NULL);
		goto IL_0100;
	}

IL_00ee:
	{
		Rigidbody_t3346577219 * L_32 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		Vector3_t4282066566  L_33 = V_1;
		int32_t L_34 = __this->get_forceMode_19();
		NullCheck(L_32);
		Rigidbody_AddRelativeTorque_m3926511917(L_32, L_33, L_34, /*hidden argument*/NULL);
	}

IL_0100:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2621392432_MethodInfo_var;
extern const uint32_t AddTorque2d__ctor_m1568771915_MetadataUsageId;
extern "C"  void AddTorque2d__ctor_m1568771915 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddTorque2d__ctor_m1568771915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2621392432(__this, /*hidden argument*/ComponentAction_1__ctor_m2621392432_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnPreprocess()
extern "C"  void AddTorque2d_OnPreprocess_m1721094020 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_set_HandleFixedUpdate_m3395950082(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::Reset()
extern "C"  void AddTorque2d_Reset_m3510172152 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_torque_15((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnEnter()
extern "C"  void AddTorque2d_OnEnter_m3742985314 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	{
		AddTorque2d_DoAddTorque_m1158537257(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnFixedUpdate()
extern "C"  void AddTorque2d_OnFixedUpdate_m1744548583 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	{
		AddTorque2d_DoAddTorque_m1158537257(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::DoAddTorque()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var;
extern const uint32_t AddTorque2d_DoAddTorque_m1158537257_MetadataUsageId;
extern "C"  void AddTorque2d_DoAddTorque_m1158537257 (AddTorque2d_t2010742859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AddTorque2d_DoAddTorque_m1158537257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2944103819(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Rigidbody2D_t1743771669 * L_5 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		FsmFloat_t2134102846 * L_6 = __this->get_torque_15();
		NullCheck(L_6);
		float L_7 = FsmFloat_get_Value_m4137923823(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_forceMode_14();
		NullCheck(L_5);
		Rigidbody2D_AddTorque_m3138315435(L_5, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::.ctor()
extern "C"  void AnimateColor__ctor_m1329511036 (AnimateColor_t301720362 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction__ctor_m3622440361(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::Reset()
extern Il2CppClass* FsmColor_t2131419205_il2cpp_TypeInfo_var;
extern const uint32_t AnimateColor_Reset_m3270911273_MetadataUsageId;
extern "C"  void AnimateColor_Reset_m3270911273 (AnimateColor_t301720362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateColor_Reset_m3270911273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmColor_t2131419205 * V_0 = NULL;
	{
		AnimateFsmAction_Reset_m1268873302(__this, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_0 = (FsmColor_t2131419205 *)il2cpp_codegen_object_new(FsmColor_t2131419205_il2cpp_TypeInfo_var);
		FsmColor__ctor_m4262627118(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmColor_t2131419205 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_2 = V_0;
		__this->set_colorVariable_34(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var;
extern Il2CppClass* CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var;
extern const uint32_t AnimateColor_OnEnter_m1446547283_MetadataUsageId;
extern "C"  void AnimateColor_OnEnter_m1446547283 (AnimateColor_t301720362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateColor_OnEnter_m1446547283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t4194546905  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B2_0 = 0;
	SingleU5BU5D_t2316563989* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	SingleU5BU5D_t2316563989* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	SingleU5BU5D_t2316563989* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	SingleU5BU5D_t2316563989* G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	SingleU5BU5D_t2316563989* G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	int32_t G_B6_1 = 0;
	SingleU5BU5D_t2316563989* G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	SingleU5BU5D_t2316563989* G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	SingleU5BU5D_t2316563989* G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	int32_t G_B9_1 = 0;
	SingleU5BU5D_t2316563989* G_B9_2 = NULL;
	int32_t G_B11_0 = 0;
	SingleU5BU5D_t2316563989* G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	SingleU5BU5D_t2316563989* G_B10_1 = NULL;
	float G_B12_0 = 0.0f;
	int32_t G_B12_1 = 0;
	SingleU5BU5D_t2316563989* G_B12_2 = NULL;
	{
		AnimateFsmAction_OnEnter_m1633405760(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_43((bool)0);
		((AnimateFsmAction_t4201352541 *)__this)->set_resultFloats_26(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		((AnimateFsmAction_t4201352541 *)__this)->set_fromFloats_27(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmColor_t2131419205 * L_1 = __this->get_colorVariable_34();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		G_B1_0 = 0;
		G_B1_1 = L_0;
		if (!L_2)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0059;
	}

IL_0046:
	{
		FsmColor_t2131419205 * L_3 = __this->get_colorVariable_34();
		NullCheck(L_3);
		Color_t4194546905  L_4 = FsmColor_get_Value_m1679829997(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_r_0();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0059:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (float)G_B3_0);
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmColor_t2131419205 * L_7 = __this->get_colorVariable_34();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B4_0 = 1;
		G_B4_1 = L_6;
		if (!L_8)
		{
			G_B5_0 = 1;
			G_B5_1 = L_6;
			goto IL_007b;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_008e;
	}

IL_007b:
	{
		FsmColor_t2131419205 * L_9 = __this->get_colorVariable_34();
		NullCheck(L_9);
		Color_t4194546905  L_10 = FsmColor_get_Value_m1679829997(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_g_1();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_008e:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (float)G_B6_0);
		SingleU5BU5D_t2316563989* L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmColor_t2131419205 * L_13 = __this->get_colorVariable_34();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B7_0 = 2;
		G_B7_1 = L_12;
		if (!L_14)
		{
			G_B8_0 = 2;
			G_B8_1 = L_12;
			goto IL_00b0;
		}
	}
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c3;
	}

IL_00b0:
	{
		FsmColor_t2131419205 * L_15 = __this->get_colorVariable_34();
		NullCheck(L_15);
		Color_t4194546905  L_16 = FsmColor_get_Value_m1679829997(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = (&V_2)->get_b_2();
		G_B9_0 = L_17;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c3:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (float)G_B9_0);
		SingleU5BU5D_t2316563989* L_18 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmColor_t2131419205 * L_19 = __this->get_colorVariable_34();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		G_B10_0 = 3;
		G_B10_1 = L_18;
		if (!L_20)
		{
			G_B11_0 = 3;
			G_B11_1 = L_18;
			goto IL_00e5;
		}
	}
	{
		G_B12_0 = (0.0f);
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00f8;
	}

IL_00e5:
	{
		FsmColor_t2131419205 * L_21 = __this->get_colorVariable_34();
		NullCheck(L_21);
		Color_t4194546905  L_22 = FsmColor_get_Value_m1679829997(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_a_3();
		G_B12_0 = L_23;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00f8:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		(G_B12_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_1), (float)G_B12_0);
		((AnimateFsmAction_t4201352541 *)__this)->set_curves_24(((AnimationCurveU5BU5D_t2600615382*)SZArrayNew(AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var, (uint32_t)4)));
		AnimationCurveU5BU5D_t2600615382* L_24 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_25 = __this->get_curveR_35();
		NullCheck(L_25);
		AnimationCurve_t3667593487 * L_26 = L_25->get_curve_0();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (AnimationCurve_t3667593487 *)L_26);
		AnimationCurveU5BU5D_t2600615382* L_27 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_28 = __this->get_curveG_37();
		NullCheck(L_28);
		AnimationCurve_t3667593487 * L_29 = L_28->get_curve_0();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 1);
		ArrayElementTypeCheck (L_27, L_29);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (AnimationCurve_t3667593487 *)L_29);
		AnimationCurveU5BU5D_t2600615382* L_30 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_31 = __this->get_curveB_39();
		NullCheck(L_31);
		AnimationCurve_t3667593487 * L_32 = L_31->get_curve_0();
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 2);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (AnimationCurve_t3667593487 *)L_32);
		AnimationCurveU5BU5D_t2600615382* L_33 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_34 = __this->get_curveA_41();
		NullCheck(L_34);
		AnimationCurve_t3667593487 * L_35 = L_34->get_curve_0();
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 3);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (AnimationCurve_t3667593487 *)L_35);
		((AnimateFsmAction_t4201352541 *)__this)->set_calculations_25(((CalculationU5BU5D_t3054796293*)SZArrayNew(CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var, (uint32_t)4)));
		CalculationU5BU5D_t3054796293* L_36 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_37 = __this->get_calculationR_36();
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_37);
		CalculationU5BU5D_t3054796293* L_38 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_39 = __this->get_calculationG_38();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_39);
		CalculationU5BU5D_t3054796293* L_40 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_41 = __this->get_calculationB_40();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 2);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_41);
		CalculationU5BU5D_t3054796293* L_42 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_43 = __this->get_calculationA_42();
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 3);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_43);
		AnimateFsmAction_Init_m3532072843(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_44 = ((AnimateFsmAction_t4201352541 *)__this)->get_delay_13();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		float L_46 = fabsf(L_45);
		if ((!(((float)L_46) < ((float)(0.01f)))))
		{
			goto IL_01bb;
		}
	}
	{
		AnimateColor_UpdateVariableValue_m3632598758(__this, /*hidden argument*/NULL);
	}

IL_01bb:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::UpdateVariableValue()
extern "C"  void AnimateColor_UpdateVariableValue_m3632598758 (AnimateColor_t301720362 * __this, const MethodInfo* method)
{
	{
		FsmColor_t2131419205 * L_0 = __this->get_colorVariable_34();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		FsmColor_t2131419205 * L_2 = __this->get_colorVariable_34();
		SingleU5BU5D_t2316563989* L_3 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		float L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		float L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		SingleU5BU5D_t2316563989* L_9 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		int32_t L_10 = 2;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		SingleU5BU5D_t2316563989* L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		int32_t L_13 = 3;
		float L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		Color_t4194546905  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m2252924356(&L_15, L_5, L_8, L_11, L_14, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmColor_set_Value_m1684002054(L_2, L_15, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnUpdate()
extern "C"  void AnimateColor_OnUpdate_m1026852176 (AnimateColor_t301720362 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction_OnUpdate_m2524497667(__this, /*hidden argument*/NULL);
		bool L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_isRunning_30();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AnimateColor_UpdateVariableValue_m3632598758(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		bool L_1 = __this->get_finishInNextStep_43();
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		bool L_2 = ((AnimateFsmAction_t4201352541 *)__this)->get_looping_31();
		if (L_2)
		{
			goto IL_0044;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishEvent_15();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	{
		bool L_5 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishAction_29();
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		bool L_6 = __this->get_finishInNextStep_43();
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		AnimateColor_UpdateVariableValue_m3632598758(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_43((bool)1);
	}

IL_0067:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::.ctor()
extern "C"  void AnimateFloat__ctor_m74780579 (AnimateFloat_t304404003 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::Reset()
extern "C"  void AnimateFloat_Reset_m2016180816 (AnimateFloat_t304404003 * __this, const MethodInfo* method)
{
	{
		__this->set_animCurve_11((FsmAnimationCurve_t2685995989 *)NULL);
		__this->set_floatVariable_12((FsmFloat_t2134102846 *)NULL);
		__this->set_finishEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_realTime_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnEnter()
extern "C"  void AnimateFloat_OnEnter_m2536388282 (AnimateFloat_t304404003 * __this, const MethodInfo* method)
{
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_15(L_0);
		__this->set_currentTime_16((0.0f));
		FsmAnimationCurve_t2685995989 * L_1 = __this->get_animCurve_11();
		if (!L_1)
		{
			goto IL_009b;
		}
	}
	{
		FsmAnimationCurve_t2685995989 * L_2 = __this->get_animCurve_11();
		NullCheck(L_2);
		AnimationCurve_t3667593487 * L_3 = L_2->get_curve_0();
		if (!L_3)
		{
			goto IL_009b;
		}
	}
	{
		FsmAnimationCurve_t2685995989 * L_4 = __this->get_animCurve_11();
		NullCheck(L_4);
		AnimationCurve_t3667593487 * L_5 = L_4->get_curve_0();
		NullCheck(L_5);
		KeyframeU5BU5D_t3589549831* L_6 = AnimationCurve_get_keys_m450535207(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009b;
		}
	}
	{
		FsmAnimationCurve_t2685995989 * L_7 = __this->get_animCurve_11();
		NullCheck(L_7);
		AnimationCurve_t3667593487 * L_8 = L_7->get_curve_0();
		NullCheck(L_8);
		KeyframeU5BU5D_t3589549831* L_9 = AnimationCurve_get_keys_m450535207(L_8, /*hidden argument*/NULL);
		FsmAnimationCurve_t2685995989 * L_10 = __this->get_animCurve_11();
		NullCheck(L_10);
		AnimationCurve_t3667593487 * L_11 = L_10->get_curve_0();
		NullCheck(L_11);
		int32_t L_12 = AnimationCurve_get_length_m3019229777(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_12-(int32_t)1)));
		float L_13 = Keyframe_get_time_m1367974951(((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_12-(int32_t)1))))), /*hidden argument*/NULL);
		__this->set_endTime_17(L_13);
		FsmAnimationCurve_t2685995989 * L_14 = __this->get_animCurve_11();
		NullCheck(L_14);
		AnimationCurve_t3667593487 * L_15 = L_14->get_curve_0();
		NullCheck(L_15);
		int32_t L_16 = AnimationCurve_get_postWrapMode_m2205068323(L_15, /*hidden argument*/NULL);
		bool L_17 = ActionHelpers_IsLoopingWrapMode_m2744633945(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		__this->set_looping_18(L_17);
		goto IL_00a2;
	}

IL_009b:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_00a2:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_floatVariable_12();
		FsmAnimationCurve_t2685995989 * L_19 = __this->get_animCurve_11();
		NullCheck(L_19);
		AnimationCurve_t3667593487 * L_20 = L_19->get_curve_0();
		NullCheck(L_20);
		float L_21 = AnimationCurve_Evaluate_m547727012(L_20, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnUpdate()
extern "C"  void AnimateFloat_OnUpdate_m452184777 (AnimateFloat_t304404003 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_realTime_14();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_15();
		__this->set_currentTime_16(((float)((float)L_1-(float)L_2)));
		goto IL_0034;
	}

IL_0022:
	{
		float L_3 = __this->get_currentTime_16();
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentTime_16(((float)((float)L_3+(float)L_4)));
	}

IL_0034:
	{
		FsmAnimationCurve_t2685995989 * L_5 = __this->get_animCurve_11();
		if (!L_5)
		{
			goto IL_007b;
		}
	}
	{
		FsmAnimationCurve_t2685995989 * L_6 = __this->get_animCurve_11();
		NullCheck(L_6);
		AnimationCurve_t3667593487 * L_7 = L_6->get_curve_0();
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		FsmFloat_t2134102846 * L_8 = __this->get_floatVariable_12();
		if (!L_8)
		{
			goto IL_007b;
		}
	}
	{
		FsmFloat_t2134102846 * L_9 = __this->get_floatVariable_12();
		FsmAnimationCurve_t2685995989 * L_10 = __this->get_animCurve_11();
		NullCheck(L_10);
		AnimationCurve_t3667593487 * L_11 = L_10->get_curve_0();
		float L_12 = __this->get_currentTime_16();
		NullCheck(L_11);
		float L_13 = AnimationCurve_Evaluate_m547727012(L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmFloat_set_Value_m1568963140(L_9, L_13, /*hidden argument*/NULL);
	}

IL_007b:
	{
		float L_14 = __this->get_currentTime_16();
		float L_15 = __this->get_endTime_17();
		if ((!(((float)L_14) >= ((float)L_15))))
		{
			goto IL_00b9;
		}
	}
	{
		bool L_16 = __this->get_looping_18();
		if (L_16)
		{
			goto IL_009d;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_009d:
	{
		FsmEvent_t2133468028 * L_17 = __this->get_finishEvent_13();
		if (!L_17)
		{
			goto IL_00b9;
		}
	}
	{
		Fsm_t1527112426 * L_18 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_19 = __this->get_finishEvent_13();
		NullCheck(L_18);
		Fsm_Event_m625948263(L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::.ctor()
extern "C"  void AnimateFloatV2__ctor_m656141447 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction__ctor_m3622440361(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AnimateFloatV2_Reset_m2597541684_MetadataUsageId;
extern "C"  void AnimateFloatV2_Reset_m2597541684 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateFloatV2_Reset_m2597541684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	{
		AnimateFsmAction_Reset_m1268873302(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_floatVariable_34(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var;
extern const uint32_t AnimateFloatV2_OnEnter_m2878433950_MetadataUsageId;
extern "C"  void AnimateFloatV2_OnEnter_m2878433950 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateFloatV2_OnEnter_m2878433950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	SingleU5BU5D_t2316563989* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	SingleU5BU5D_t2316563989* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	SingleU5BU5D_t2316563989* G_B3_2 = NULL;
	{
		AnimateFsmAction_OnEnter_m1633405760(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_37((bool)0);
		((AnimateFsmAction_t4201352541 *)__this)->set_resultFloats_26(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AnimateFsmAction_t4201352541 *)__this)->set_fromFloats_27(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)1)));
		SingleU5BU5D_t2316563989* L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_34();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		G_B1_0 = 0;
		G_B1_1 = L_0;
		if (!L_2)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0051;
	}

IL_0046:
	{
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_34();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0051:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (float)G_B3_0);
		((AnimateFsmAction_t4201352541 *)__this)->set_calculations_25(((CalculationU5BU5D_t3054796293*)SZArrayNew(CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var, (uint32_t)1)));
		CalculationU5BU5D_t3054796293* L_5 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_6 = __this->get_calculation_36();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_6);
		((AnimateFsmAction_t4201352541 *)__this)->set_curves_24(((AnimationCurveU5BU5D_t2600615382*)SZArrayNew(AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var, (uint32_t)1)));
		AnimationCurveU5BU5D_t2600615382* L_7 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_8 = __this->get_animCurve_35();
		NullCheck(L_8);
		AnimationCurve_t3667593487 * L_9 = L_8->get_curve_0();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (AnimationCurve_t3667593487 *)L_9);
		AnimateFsmAction_Init_m3532072843(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnExit()
extern "C"  void AnimateFloatV2_OnExit_m378867002 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnUpdate()
extern "C"  void AnimateFloatV2_OnUpdate_m2465665893 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction_OnUpdate_m2524497667(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_34();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		bool L_2 = ((AnimateFsmAction_t4201352541 *)__this)->get_isRunning_30();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_34();
		SingleU5BU5D_t2316563989* L_4 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_3);
		FsmFloat_set_Value_m1568963140(L_3, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		bool L_7 = __this->get_finishInNextStep_37();
		if (!L_7)
		{
			goto IL_006c;
		}
	}
	{
		bool L_8 = ((AnimateFsmAction_t4201352541 *)__this)->get_looping_31();
		if (L_8)
		{
			goto IL_006c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_9 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishEvent_15();
		if (!L_9)
		{
			goto IL_006c;
		}
	}
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishEvent_15();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
	}

IL_006c:
	{
		bool L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishAction_29();
		if (!L_12)
		{
			goto IL_00ac;
		}
	}
	{
		bool L_13 = __this->get_finishInNextStep_37();
		if (L_13)
		{
			goto IL_00ac;
		}
	}
	{
		FsmFloat_t2134102846 * L_14 = __this->get_floatVariable_34();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00a5;
		}
	}
	{
		FsmFloat_t2134102846 * L_16 = __this->get_floatVariable_34();
		SingleU5BU5D_t2316563989* L_17 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		float L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_19, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		__this->set_finishInNextStep_37((bool)1);
	}

IL_00ac:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::.ctor()
extern "C"  void AnimateFsmAction__ctor_m3622440361 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Reset()
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmBool_t1075959796_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var;
extern const uint32_t AnimateFsmAction_Reset_m1268873302_MetadataUsageId;
extern "C"  void AnimateFsmAction_Reset_m1268873302 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateFsmAction_Reset_m1268873302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmFloat_t2134102846 * V_0 = NULL;
	FsmBool_t1075959796 * V_1 = NULL;
	{
		__this->set_finishEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_realTime_16((bool)0);
		FsmFloat_t2134102846 * L_0 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmFloat_t2134102846 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_2 = V_0;
		__this->set_time_11(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmFloat_t2134102846 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_0;
		__this->set_speed_12(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmFloat_t2134102846 * L_7 = V_0;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_0;
		__this->set_delay_13(L_8);
		FsmBool_t1075959796 * L_9 = (FsmBool_t1075959796 *)il2cpp_codegen_object_new(FsmBool_t1075959796_il2cpp_TypeInfo_var);
		FsmBool__ctor_m1553455211(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmBool_t1075959796 * L_10 = V_1;
		NullCheck(L_10);
		FsmBool_set_Value_m1126216340(L_10, (bool)1, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_11 = V_1;
		__this->set_ignoreCurveOffset_14(L_11);
		__this->set_resultFloats_26(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_fromFloats_27(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_toFloats_28(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_endTimes_19(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_keyOffsets_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_curves_24(((AnimationCurveU5BU5D_t2600615382*)SZArrayNew(AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_finishAction_29((bool)0);
		__this->set_start_32((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnEnter()
extern "C"  void AnimateFsmAction_OnEnter_m1633405760 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	AnimateFsmAction_t4201352541 * G_B2_0 = NULL;
	AnimateFsmAction_t4201352541 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	AnimateFsmAction_t4201352541 * G_B3_1 = NULL;
	{
		float L_0 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_17(L_0);
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_17();
		__this->set_lastTime_20(((float)((float)L_1-(float)L_2)));
		__this->set_deltaTime_21((0.0f));
		__this->set_currentTime_18((0.0f));
		__this->set_isRunning_30((bool)0);
		__this->set_finishAction_29((bool)0);
		__this->set_looping_31((bool)0);
		FsmFloat_t2134102846 * L_3 = __this->get_delay_13();
		NullCheck(L_3);
		bool L_4 = NamedVariable_get_IsNone_m281035543(L_3, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_4)
		{
			G_B2_0 = __this;
			goto IL_0063;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		goto IL_0077;
	}

IL_0063:
	{
		FsmFloat_t2134102846 * L_5 = __this->get_delay_13();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		float L_7 = L_6;
		V_0 = L_7;
		__this->set_delayTime_22(L_7);
		float L_8 = V_0;
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
	}

IL_0077:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_delayTime_22(G_B3_0);
		__this->set_start_32((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Init()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t AnimateFsmAction_Init_m3532072843_MetadataUsageId;
extern "C"  void AnimateFsmAction_Init_m3532072843 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateFsmAction_Init_m3532072843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B8_0 = 0;
	SingleU5BU5D_t2316563989* G_B8_1 = NULL;
	int32_t G_B4_0 = 0;
	SingleU5BU5D_t2316563989* G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	SingleU5BU5D_t2316563989* G_B6_1 = NULL;
	int32_t G_B5_0 = 0;
	SingleU5BU5D_t2316563989* G_B5_1 = NULL;
	float G_B7_0 = 0.0f;
	int32_t G_B7_1 = 0;
	SingleU5BU5D_t2316563989* G_B7_2 = NULL;
	float G_B9_0 = 0.0f;
	int32_t G_B9_1 = 0;
	SingleU5BU5D_t2316563989* G_B9_2 = NULL;
	AnimateFsmAction_t4201352541 * G_B11_0 = NULL;
	AnimateFsmAction_t4201352541 * G_B10_0 = NULL;
	float G_B14_0 = 0.0f;
	AnimateFsmAction_t4201352541 * G_B14_1 = NULL;
	AnimateFsmAction_t4201352541 * G_B13_0 = NULL;
	AnimateFsmAction_t4201352541 * G_B12_0 = NULL;
	{
		AnimationCurveU5BU5D_t2600615382* L_0 = __this->get_curves_24();
		NullCheck(L_0);
		__this->set_endTimes_19(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		AnimationCurveU5BU5D_t2600615382* L_1 = __this->get_curves_24();
		NullCheck(L_1);
		__this->set_keyOffsets_23(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		__this->set_largestEndTime_33((0.0f));
		V_0 = 0;
		goto IL_01eb;
	}

IL_0038:
	{
		AnimationCurveU5BU5D_t2600615382* L_2 = __this->get_curves_24();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		AnimationCurve_t3667593487 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		if (!L_5)
		{
			goto IL_01da;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_6 = __this->get_curves_24();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		AnimationCurve_t3667593487 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		KeyframeU5BU5D_t3589549831* L_10 = AnimationCurve_get_keys_m450535207(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_01da;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_11 = __this->get_keyOffsets_23();
		int32_t L_12 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_13 = __this->get_curves_24();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		AnimationCurve_t3667593487 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		KeyframeU5BU5D_t3589549831* L_17 = AnimationCurve_get_keys_m450535207(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		G_B4_0 = L_12;
		G_B4_1 = L_11;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) <= ((int32_t)0)))
		{
			G_B8_0 = L_12;
			G_B8_1 = L_11;
			goto IL_00f3;
		}
	}
	{
		FsmFloat_t2134102846 * L_18 = __this->get_time_11();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		if (!L_19)
		{
			G_B6_0 = G_B4_0;
			G_B6_1 = G_B4_1;
			goto IL_00a3;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_20 = __this->get_curves_24();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		AnimationCurve_t3667593487 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		KeyframeU5BU5D_t3589549831* L_24 = AnimationCurve_get_keys_m450535207(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		float L_25 = Keyframe_get_time_m1367974951(((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		G_B7_0 = L_25;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		goto IL_00ee;
	}

IL_00a3:
	{
		FsmFloat_t2134102846 * L_26 = __this->get_time_11();
		NullCheck(L_26);
		float L_27 = FsmFloat_get_Value_m4137923823(L_26, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_28 = __this->get_curves_24();
		int32_t L_29 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		AnimationCurve_t3667593487 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		KeyframeU5BU5D_t3589549831* L_32 = AnimationCurve_get_keys_m450535207(L_31, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_33 = __this->get_curves_24();
		int32_t L_34 = V_0;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		AnimationCurve_t3667593487 * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		int32_t L_37 = AnimationCurve_get_length_m3019229777(L_36, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)((int32_t)L_37-(int32_t)1)));
		float L_38 = Keyframe_get_time_m1367974951(((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_37-(int32_t)1))))), /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_39 = __this->get_curves_24();
		int32_t L_40 = V_0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = L_40;
		AnimationCurve_t3667593487 * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_42);
		KeyframeU5BU5D_t3589549831* L_43 = AnimationCurve_get_keys_m450535207(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		float L_44 = Keyframe_get_time_m1367974951(((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		G_B7_0 = ((float)((float)((float)((float)L_27/(float)L_38))*(float)L_44));
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
	}

IL_00ee:
	{
		G_B9_0 = G_B7_0;
		G_B9_1 = G_B7_1;
		G_B9_2 = G_B7_2;
		goto IL_00f8;
	}

IL_00f3:
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00f8:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (float)G_B9_0);
		FsmBool_t1075959796 * L_45 = __this->get_ignoreCurveOffset_14();
		NullCheck(L_45);
		bool L_46 = NamedVariable_get_IsNone_m281035543(L_45, /*hidden argument*/NULL);
		G_B10_0 = __this;
		if (!L_46)
		{
			G_B11_0 = __this;
			goto IL_0114;
		}
	}
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B10_0;
		goto IL_0136;
	}

IL_0114:
	{
		FsmBool_t1075959796 * L_47 = __this->get_ignoreCurveOffset_14();
		NullCheck(L_47);
		bool L_48 = FsmBool_get_Value_m3101329097(L_47, /*hidden argument*/NULL);
		G_B12_0 = G_B11_0;
		if (!L_48)
		{
			G_B13_0 = G_B11_0;
			goto IL_0131;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_49 = __this->get_keyOffsets_23();
		int32_t L_50 = V_0;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		float L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		G_B14_0 = L_52;
		G_B14_1 = G_B12_0;
		goto IL_0136;
	}

IL_0131:
	{
		G_B14_0 = (0.0f);
		G_B14_1 = G_B13_0;
	}

IL_0136:
	{
		NullCheck(G_B14_1);
		G_B14_1->set_currentTime_18(G_B14_0);
		FsmFloat_t2134102846 * L_53 = __this->get_time_11();
		NullCheck(L_53);
		bool L_54 = NamedVariable_get_IsNone_m281035543(L_53, /*hidden argument*/NULL);
		if (L_54)
		{
			goto IL_0163;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_55 = __this->get_endTimes_19();
		int32_t L_56 = V_0;
		FsmFloat_t2134102846 * L_57 = __this->get_time_11();
		NullCheck(L_57);
		float L_58 = FsmFloat_get_Value_m4137923823(L_57, /*hidden argument*/NULL);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (float)L_58);
		goto IL_0191;
	}

IL_0163:
	{
		SingleU5BU5D_t2316563989* L_59 = __this->get_endTimes_19();
		int32_t L_60 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_61 = __this->get_curves_24();
		int32_t L_62 = V_0;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = L_62;
		AnimationCurve_t3667593487 * L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_64);
		KeyframeU5BU5D_t3589549831* L_65 = AnimationCurve_get_keys_m450535207(L_64, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_66 = __this->get_curves_24();
		int32_t L_67 = V_0;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		int32_t L_68 = L_67;
		AnimationCurve_t3667593487 * L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		NullCheck(L_69);
		int32_t L_70 = AnimationCurve_get_length_m3019229777(L_69, /*hidden argument*/NULL);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)((int32_t)L_70-(int32_t)1)));
		float L_71 = Keyframe_get_time_m1367974951(((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_70-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(L_60), (float)L_71);
	}

IL_0191:
	{
		float L_72 = __this->get_largestEndTime_33();
		SingleU5BU5D_t2316563989* L_73 = __this->get_endTimes_19();
		int32_t L_74 = V_0;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		int32_t L_75 = L_74;
		float L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		if ((!(((float)L_72) < ((float)L_76))))
		{
			goto IL_01b2;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_77 = __this->get_endTimes_19();
		int32_t L_78 = V_0;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, L_78);
		int32_t L_79 = L_78;
		float L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		__this->set_largestEndTime_33(L_80);
	}

IL_01b2:
	{
		bool L_81 = __this->get_looping_31();
		if (L_81)
		{
			goto IL_01d5;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_82 = __this->get_curves_24();
		int32_t L_83 = V_0;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, L_83);
		int32_t L_84 = L_83;
		AnimationCurve_t3667593487 * L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		NullCheck(L_85);
		int32_t L_86 = AnimationCurve_get_postWrapMode_m2205068323(L_85, /*hidden argument*/NULL);
		bool L_87 = ActionHelpers_IsLoopingWrapMode_m2744633945(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		__this->set_looping_31(L_87);
	}

IL_01d5:
	{
		goto IL_01e7;
	}

IL_01da:
	{
		SingleU5BU5D_t2316563989* L_88 = __this->get_endTimes_19();
		int32_t L_89 = V_0;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_89);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (float)(-1.0f));
	}

IL_01e7:
	{
		int32_t L_90 = V_0;
		V_0 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_01eb:
	{
		int32_t L_91 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_92 = __this->get_curves_24();
		NullCheck(L_92);
		if ((((int32_t)L_91) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_92)->max_length)))))))
		{
			goto IL_0038;
		}
	}
	{
		V_1 = 0;
		goto IL_0290;
	}

IL_0200:
	{
		float L_93 = __this->get_largestEndTime_33();
		if ((!(((float)L_93) > ((float)(0.0f)))))
		{
			goto IL_0235;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_94 = __this->get_endTimes_19();
		int32_t L_95 = V_1;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_95);
		int32_t L_96 = L_95;
		float L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		if ((!(((float)L_97) == ((float)(-1.0f)))))
		{
			goto IL_0235;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_98 = __this->get_endTimes_19();
		int32_t L_99 = V_1;
		float L_100 = __this->get_largestEndTime_33();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, L_99);
		(L_98)->SetAt(static_cast<il2cpp_array_size_t>(L_99), (float)L_100);
		goto IL_028c;
	}

IL_0235:
	{
		float L_101 = __this->get_largestEndTime_33();
		if ((!(((float)L_101) == ((float)(0.0f)))))
		{
			goto IL_028c;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_102 = __this->get_endTimes_19();
		int32_t L_103 = V_1;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, L_103);
		int32_t L_104 = L_103;
		float L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		if ((!(((float)L_105) == ((float)(-1.0f)))))
		{
			goto IL_028c;
		}
	}
	{
		FsmFloat_t2134102846 * L_106 = __this->get_time_11();
		NullCheck(L_106);
		bool L_107 = NamedVariable_get_IsNone_m281035543(L_106, /*hidden argument*/NULL);
		if (!L_107)
		{
			goto IL_0279;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_108 = __this->get_endTimes_19();
		int32_t L_109 = V_1;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(L_109), (float)(1.0f));
		goto IL_028c;
	}

IL_0279:
	{
		SingleU5BU5D_t2316563989* L_110 = __this->get_endTimes_19();
		int32_t L_111 = V_1;
		FsmFloat_t2134102846 * L_112 = __this->get_time_11();
		NullCheck(L_112);
		float L_113 = FsmFloat_get_Value_m4137923823(L_112, /*hidden argument*/NULL);
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, L_111);
		(L_110)->SetAt(static_cast<il2cpp_array_size_t>(L_111), (float)L_113);
	}

IL_028c:
	{
		int32_t L_114 = V_1;
		V_1 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0290:
	{
		int32_t L_115 = V_1;
		AnimationCurveU5BU5D_t2600615382* L_116 = __this->get_curves_24();
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_116)->max_length)))))))
		{
			goto IL_0200;
		}
	}
	{
		AnimateFsmAction_UpdateAnimation_m2875131202(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnUpdate()
extern "C"  void AnimateFsmAction_OnUpdate_m2524497667 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction_CheckStart_m2974544053(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_isRunning_30();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		AnimateFsmAction_UpdateTime_m4010459313(__this, /*hidden argument*/NULL);
		AnimateFsmAction_UpdateAnimation_m2875131202(__this, /*hidden argument*/NULL);
		AnimateFsmAction_CheckFinished_m3654930401(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::CheckStart()
extern "C"  void AnimateFsmAction_CheckStart_m2974544053 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isRunning_30();
		if (L_0)
		{
			goto IL_0099;
		}
	}
	{
		bool L_1 = __this->get_start_32();
		if (!L_1)
		{
			goto IL_0099;
		}
	}
	{
		float L_2 = __this->get_delayTime_22();
		if ((!(((float)L_2) >= ((float)(0.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		bool L_3 = __this->get_realTime_16();
		if (!L_3)
		{
			goto IL_0074;
		}
	}
	{
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_17();
		float L_6 = __this->get_lastTime_20();
		__this->set_deltaTime_21(((float)((float)((float)((float)L_4-(float)L_5))-(float)L_6)));
		float L_7 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_startTime_17();
		__this->set_lastTime_20(((float)((float)L_7-(float)L_8)));
		float L_9 = __this->get_delayTime_22();
		float L_10 = __this->get_deltaTime_21();
		__this->set_delayTime_22(((float)((float)L_9-(float)L_10)));
		goto IL_0086;
	}

IL_0074:
	{
		float L_11 = __this->get_delayTime_22();
		float L_12 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_delayTime_22(((float)((float)L_11-(float)L_12)));
	}

IL_0086:
	{
		goto IL_0099;
	}

IL_008b:
	{
		__this->set_isRunning_30((bool)1);
		__this->set_start_32((bool)0);
	}

IL_0099:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::UpdateTime()
extern "C"  void AnimateFsmAction_UpdateTime_m4010459313 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_realTime_16();
		if (!L_0)
		{
			goto IL_0082;
		}
	}
	{
		float L_1 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_startTime_17();
		float L_3 = __this->get_lastTime_20();
		__this->set_deltaTime_21(((float)((float)((float)((float)L_1-(float)L_2))-(float)L_3)));
		float L_4 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_startTime_17();
		__this->set_lastTime_20(((float)((float)L_4-(float)L_5)));
		FsmFloat_t2134102846 * L_6 = __this->get_speed_12();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006a;
		}
	}
	{
		float L_8 = __this->get_currentTime_18();
		float L_9 = __this->get_deltaTime_21();
		FsmFloat_t2134102846 * L_10 = __this->get_speed_12();
		NullCheck(L_10);
		float L_11 = FsmFloat_get_Value_m4137923823(L_10, /*hidden argument*/NULL);
		__this->set_currentTime_18(((float)((float)L_8+(float)((float)((float)L_9*(float)L_11)))));
		goto IL_007d;
	}

IL_006a:
	{
		float L_12 = __this->get_currentTime_18();
		float L_13 = __this->get_deltaTime_21();
		__this->set_currentTime_18(((float)((float)L_12+(float)L_13)));
	}

IL_007d:
	{
		goto IL_00c7;
	}

IL_0082:
	{
		FsmFloat_t2134102846 * L_14 = __this->get_speed_12();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00b5;
		}
	}
	{
		float L_16 = __this->get_currentTime_18();
		float L_17 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_speed_12();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		__this->set_currentTime_18(((float)((float)L_16+(float)((float)((float)L_17*(float)L_19)))));
		goto IL_00c7;
	}

IL_00b5:
	{
		float L_20 = __this->get_currentTime_18();
		float L_21 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_currentTime_18(((float)((float)L_20+(float)L_21)));
	}

IL_00c7:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::UpdateAnimation()
extern "C"  void AnimateFsmAction_UpdateAnimation_m2875131202 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B29_0 = 0;
	SingleU5BU5D_t2316563989* G_B29_1 = NULL;
	int32_t G_B28_0 = 0;
	SingleU5BU5D_t2316563989* G_B28_1 = NULL;
	float G_B30_0 = 0.0f;
	int32_t G_B30_1 = 0;
	SingleU5BU5D_t2316563989* G_B30_2 = NULL;
	int32_t G_B33_0 = 0;
	SingleU5BU5D_t2316563989* G_B33_1 = NULL;
	int32_t G_B32_0 = 0;
	SingleU5BU5D_t2316563989* G_B32_1 = NULL;
	float G_B34_0 = 0.0f;
	int32_t G_B34_1 = 0;
	SingleU5BU5D_t2316563989* G_B34_2 = NULL;
	int32_t G_B39_0 = 0;
	SingleU5BU5D_t2316563989* G_B39_1 = NULL;
	int32_t G_B38_0 = 0;
	SingleU5BU5D_t2316563989* G_B38_1 = NULL;
	float G_B40_0 = 0.0f;
	int32_t G_B40_1 = 0;
	SingleU5BU5D_t2316563989* G_B40_2 = NULL;
	int32_t G_B43_0 = 0;
	SingleU5BU5D_t2316563989* G_B43_1 = NULL;
	int32_t G_B42_0 = 0;
	SingleU5BU5D_t2316563989* G_B42_1 = NULL;
	float G_B44_0 = 0.0f;
	int32_t G_B44_1 = 0;
	SingleU5BU5D_t2316563989* G_B44_2 = NULL;
	{
		V_0 = 0;
		goto IL_0550;
	}

IL_0007:
	{
		AnimationCurveU5BU5D_t2600615382* L_0 = __this->get_curves_24();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		AnimationCurve_t3667593487 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if (!L_3)
		{
			goto IL_053c;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_4 = __this->get_curves_24();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		AnimationCurve_t3667593487 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		KeyframeU5BU5D_t3589549831* L_8 = AnimationCurve_get_keys_m450535207(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_053c;
		}
	}
	{
		CalculationU5BU5D_t3054796293* L_9 = __this->get_calculations_25();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		if (!L_12)
		{
			goto IL_0527;
		}
	}
	{
		CalculationU5BU5D_t3054796293* L_13 = __this->get_calculations_25();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_1 = L_16;
		int32_t L_17 = V_1;
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 0)
		{
			goto IL_0068;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 1)
		{
			goto IL_00eb;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 2)
		{
			goto IL_0180;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 3)
		{
			goto IL_0215;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 4)
		{
			goto IL_02aa;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 5)
		{
			goto IL_033f;
		}
		if (((int32_t)((int32_t)L_17-(int32_t)1)) == 6)
		{
			goto IL_0455;
		}
	}
	{
		goto IL_0522;
	}

IL_0068:
	{
		FsmFloat_t2134102846 * L_18 = __this->get_time_11();
		NullCheck(L_18);
		bool L_19 = NamedVariable_get_IsNone_m281035543(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00cb;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_20 = __this->get_resultFloats_26();
		int32_t L_21 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_22 = __this->get_curves_24();
		int32_t L_23 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		AnimationCurve_t3667593487 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		float L_26 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_27 = __this->get_time_11();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_29 = __this->get_curves_24();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		AnimationCurve_t3667593487 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		KeyframeU5BU5D_t3589549831* L_33 = AnimationCurve_get_keys_m450535207(L_32, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_34 = __this->get_curves_24();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = L_35;
		AnimationCurve_t3667593487 * L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		int32_t L_38 = AnimationCurve_get_length_m3019229777(L_37, /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)((int32_t)L_38-(int32_t)1)));
		float L_39 = Keyframe_get_time_m1367974951(((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_38-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_25);
		float L_40 = AnimationCurve_Evaluate_m547727012(L_25, ((float)((float)((float)((float)L_26/(float)L_28))*(float)L_39)), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (float)L_40);
		goto IL_00e6;
	}

IL_00cb:
	{
		SingleU5BU5D_t2316563989* L_41 = __this->get_resultFloats_26();
		int32_t L_42 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_43 = __this->get_curves_24();
		int32_t L_44 = V_0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		AnimationCurve_t3667593487 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		float L_47 = __this->get_currentTime_18();
		NullCheck(L_46);
		float L_48 = AnimationCurve_Evaluate_m547727012(L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(L_42), (float)L_48);
	}

IL_00e6:
	{
		goto IL_0522;
	}

IL_00eb:
	{
		FsmFloat_t2134102846 * L_49 = __this->get_time_11();
		NullCheck(L_49);
		bool L_50 = NamedVariable_get_IsNone_m281035543(L_49, /*hidden argument*/NULL);
		if (L_50)
		{
			goto IL_0157;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_51 = __this->get_resultFloats_26();
		int32_t L_52 = V_0;
		SingleU5BU5D_t2316563989* L_53 = __this->get_fromFloats_27();
		int32_t L_54 = V_0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		float L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		AnimationCurveU5BU5D_t2600615382* L_57 = __this->get_curves_24();
		int32_t L_58 = V_0;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		int32_t L_59 = L_58;
		AnimationCurve_t3667593487 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		float L_61 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_62 = __this->get_time_11();
		NullCheck(L_62);
		float L_63 = FsmFloat_get_Value_m4137923823(L_62, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_64 = __this->get_curves_24();
		int32_t L_65 = V_0;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		int32_t L_66 = L_65;
		AnimationCurve_t3667593487 * L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		NullCheck(L_67);
		KeyframeU5BU5D_t3589549831* L_68 = AnimationCurve_get_keys_m450535207(L_67, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_69 = __this->get_curves_24();
		int32_t L_70 = V_0;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		int32_t L_71 = L_70;
		AnimationCurve_t3667593487 * L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		NullCheck(L_72);
		int32_t L_73 = AnimationCurve_get_length_m3019229777(L_72, /*hidden argument*/NULL);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)((int32_t)L_73-(int32_t)1)));
		float L_74 = Keyframe_get_time_m1367974951(((L_68)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_73-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_60);
		float L_75 = AnimationCurve_Evaluate_m547727012(L_60, ((float)((float)((float)((float)L_61/(float)L_63))*(float)L_74)), /*hidden argument*/NULL);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, L_52);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(L_52), (float)((float)((float)L_56+(float)L_75)));
		goto IL_017b;
	}

IL_0157:
	{
		SingleU5BU5D_t2316563989* L_76 = __this->get_resultFloats_26();
		int32_t L_77 = V_0;
		SingleU5BU5D_t2316563989* L_78 = __this->get_fromFloats_27();
		int32_t L_79 = V_0;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, L_79);
		int32_t L_80 = L_79;
		float L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		AnimationCurveU5BU5D_t2600615382* L_82 = __this->get_curves_24();
		int32_t L_83 = V_0;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, L_83);
		int32_t L_84 = L_83;
		AnimationCurve_t3667593487 * L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		float L_86 = __this->get_currentTime_18();
		NullCheck(L_85);
		float L_87 = AnimationCurve_Evaluate_m547727012(L_85, L_86, /*hidden argument*/NULL);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, L_77);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(L_77), (float)((float)((float)L_81+(float)L_87)));
	}

IL_017b:
	{
		goto IL_0522;
	}

IL_0180:
	{
		FsmFloat_t2134102846 * L_88 = __this->get_time_11();
		NullCheck(L_88);
		bool L_89 = NamedVariable_get_IsNone_m281035543(L_88, /*hidden argument*/NULL);
		if (L_89)
		{
			goto IL_01ec;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_90 = __this->get_resultFloats_26();
		int32_t L_91 = V_0;
		SingleU5BU5D_t2316563989* L_92 = __this->get_fromFloats_27();
		int32_t L_93 = V_0;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, L_93);
		int32_t L_94 = L_93;
		float L_95 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_94));
		AnimationCurveU5BU5D_t2600615382* L_96 = __this->get_curves_24();
		int32_t L_97 = V_0;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		int32_t L_98 = L_97;
		AnimationCurve_t3667593487 * L_99 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		float L_100 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_101 = __this->get_time_11();
		NullCheck(L_101);
		float L_102 = FsmFloat_get_Value_m4137923823(L_101, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_103 = __this->get_curves_24();
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		AnimationCurve_t3667593487 * L_106 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_106);
		KeyframeU5BU5D_t3589549831* L_107 = AnimationCurve_get_keys_m450535207(L_106, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_108 = __this->get_curves_24();
		int32_t L_109 = V_0;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		int32_t L_110 = L_109;
		AnimationCurve_t3667593487 * L_111 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		NullCheck(L_111);
		int32_t L_112 = AnimationCurve_get_length_m3019229777(L_111, /*hidden argument*/NULL);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, ((int32_t)((int32_t)L_112-(int32_t)1)));
		float L_113 = Keyframe_get_time_m1367974951(((L_107)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_112-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_99);
		float L_114 = AnimationCurve_Evaluate_m547727012(L_99, ((float)((float)((float)((float)L_100/(float)L_102))*(float)L_113)), /*hidden argument*/NULL);
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, L_91);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(L_91), (float)((float)((float)L_95-(float)L_114)));
		goto IL_0210;
	}

IL_01ec:
	{
		SingleU5BU5D_t2316563989* L_115 = __this->get_resultFloats_26();
		int32_t L_116 = V_0;
		SingleU5BU5D_t2316563989* L_117 = __this->get_fromFloats_27();
		int32_t L_118 = V_0;
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, L_118);
		int32_t L_119 = L_118;
		float L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		AnimationCurveU5BU5D_t2600615382* L_121 = __this->get_curves_24();
		int32_t L_122 = V_0;
		NullCheck(L_121);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_121, L_122);
		int32_t L_123 = L_122;
		AnimationCurve_t3667593487 * L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		float L_125 = __this->get_currentTime_18();
		NullCheck(L_124);
		float L_126 = AnimationCurve_Evaluate_m547727012(L_124, L_125, /*hidden argument*/NULL);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, L_116);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(L_116), (float)((float)((float)L_120-(float)L_126)));
	}

IL_0210:
	{
		goto IL_0522;
	}

IL_0215:
	{
		FsmFloat_t2134102846 * L_127 = __this->get_time_11();
		NullCheck(L_127);
		bool L_128 = NamedVariable_get_IsNone_m281035543(L_127, /*hidden argument*/NULL);
		if (L_128)
		{
			goto IL_0281;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_129 = __this->get_resultFloats_26();
		int32_t L_130 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_131 = __this->get_curves_24();
		int32_t L_132 = V_0;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = L_132;
		AnimationCurve_t3667593487 * L_134 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		float L_135 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_136 = __this->get_time_11();
		NullCheck(L_136);
		float L_137 = FsmFloat_get_Value_m4137923823(L_136, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_138 = __this->get_curves_24();
		int32_t L_139 = V_0;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, L_139);
		int32_t L_140 = L_139;
		AnimationCurve_t3667593487 * L_141 = (L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_140));
		NullCheck(L_141);
		KeyframeU5BU5D_t3589549831* L_142 = AnimationCurve_get_keys_m450535207(L_141, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_143 = __this->get_curves_24();
		int32_t L_144 = V_0;
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, L_144);
		int32_t L_145 = L_144;
		AnimationCurve_t3667593487 * L_146 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		NullCheck(L_146);
		int32_t L_147 = AnimationCurve_get_length_m3019229777(L_146, /*hidden argument*/NULL);
		NullCheck(L_142);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_142, ((int32_t)((int32_t)L_147-(int32_t)1)));
		float L_148 = Keyframe_get_time_m1367974951(((L_142)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_147-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_134);
		float L_149 = AnimationCurve_Evaluate_m547727012(L_134, ((float)((float)((float)((float)L_135/(float)L_137))*(float)L_148)), /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_150 = __this->get_fromFloats_27();
		int32_t L_151 = V_0;
		NullCheck(L_150);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_150, L_151);
		int32_t L_152 = L_151;
		float L_153 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_152));
		NullCheck(L_129);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_129, L_130);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(L_130), (float)((float)((float)L_149-(float)L_153)));
		goto IL_02a5;
	}

IL_0281:
	{
		SingleU5BU5D_t2316563989* L_154 = __this->get_resultFloats_26();
		int32_t L_155 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_156 = __this->get_curves_24();
		int32_t L_157 = V_0;
		NullCheck(L_156);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_156, L_157);
		int32_t L_158 = L_157;
		AnimationCurve_t3667593487 * L_159 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		float L_160 = __this->get_currentTime_18();
		NullCheck(L_159);
		float L_161 = AnimationCurve_Evaluate_m547727012(L_159, L_160, /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_162 = __this->get_fromFloats_27();
		int32_t L_163 = V_0;
		NullCheck(L_162);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_162, L_163);
		int32_t L_164 = L_163;
		float L_165 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_164));
		NullCheck(L_154);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_154, L_155);
		(L_154)->SetAt(static_cast<il2cpp_array_size_t>(L_155), (float)((float)((float)L_161-(float)L_165)));
	}

IL_02a5:
	{
		goto IL_0522;
	}

IL_02aa:
	{
		FsmFloat_t2134102846 * L_166 = __this->get_time_11();
		NullCheck(L_166);
		bool L_167 = NamedVariable_get_IsNone_m281035543(L_166, /*hidden argument*/NULL);
		if (L_167)
		{
			goto IL_0316;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_168 = __this->get_resultFloats_26();
		int32_t L_169 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_170 = __this->get_curves_24();
		int32_t L_171 = V_0;
		NullCheck(L_170);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_170, L_171);
		int32_t L_172 = L_171;
		AnimationCurve_t3667593487 * L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		float L_174 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_175 = __this->get_time_11();
		NullCheck(L_175);
		float L_176 = FsmFloat_get_Value_m4137923823(L_175, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_177 = __this->get_curves_24();
		int32_t L_178 = V_0;
		NullCheck(L_177);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_177, L_178);
		int32_t L_179 = L_178;
		AnimationCurve_t3667593487 * L_180 = (L_177)->GetAt(static_cast<il2cpp_array_size_t>(L_179));
		NullCheck(L_180);
		KeyframeU5BU5D_t3589549831* L_181 = AnimationCurve_get_keys_m450535207(L_180, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_182 = __this->get_curves_24();
		int32_t L_183 = V_0;
		NullCheck(L_182);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_182, L_183);
		int32_t L_184 = L_183;
		AnimationCurve_t3667593487 * L_185 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184));
		NullCheck(L_185);
		int32_t L_186 = AnimationCurve_get_length_m3019229777(L_185, /*hidden argument*/NULL);
		NullCheck(L_181);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_181, ((int32_t)((int32_t)L_186-(int32_t)1)));
		float L_187 = Keyframe_get_time_m1367974951(((L_181)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_186-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_173);
		float L_188 = AnimationCurve_Evaluate_m547727012(L_173, ((float)((float)((float)((float)L_174/(float)L_176))*(float)L_187)), /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_189 = __this->get_fromFloats_27();
		int32_t L_190 = V_0;
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, L_190);
		int32_t L_191 = L_190;
		float L_192 = (L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		NullCheck(L_168);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_168, L_169);
		(L_168)->SetAt(static_cast<il2cpp_array_size_t>(L_169), (float)((float)((float)L_188*(float)L_192)));
		goto IL_033a;
	}

IL_0316:
	{
		SingleU5BU5D_t2316563989* L_193 = __this->get_resultFloats_26();
		int32_t L_194 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_195 = __this->get_curves_24();
		int32_t L_196 = V_0;
		NullCheck(L_195);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_195, L_196);
		int32_t L_197 = L_196;
		AnimationCurve_t3667593487 * L_198 = (L_195)->GetAt(static_cast<il2cpp_array_size_t>(L_197));
		float L_199 = __this->get_currentTime_18();
		NullCheck(L_198);
		float L_200 = AnimationCurve_Evaluate_m547727012(L_198, L_199, /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_201 = __this->get_fromFloats_27();
		int32_t L_202 = V_0;
		NullCheck(L_201);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_201, L_202);
		int32_t L_203 = L_202;
		float L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		NullCheck(L_193);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_193, L_194);
		(L_193)->SetAt(static_cast<il2cpp_array_size_t>(L_194), (float)((float)((float)L_200*(float)L_204)));
	}

IL_033a:
	{
		goto IL_0522;
	}

IL_033f:
	{
		FsmFloat_t2134102846 * L_205 = __this->get_time_11();
		NullCheck(L_205);
		bool L_206 = NamedVariable_get_IsNone_m281035543(L_205, /*hidden argument*/NULL);
		if (L_206)
		{
			goto IL_0405;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_207 = __this->get_resultFloats_26();
		int32_t L_208 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_209 = __this->get_curves_24();
		int32_t L_210 = V_0;
		NullCheck(L_209);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_209, L_210);
		int32_t L_211 = L_210;
		AnimationCurve_t3667593487 * L_212 = (L_209)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		float L_213 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_214 = __this->get_time_11();
		NullCheck(L_214);
		float L_215 = FsmFloat_get_Value_m4137923823(L_214, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_216 = __this->get_curves_24();
		int32_t L_217 = V_0;
		NullCheck(L_216);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_216, L_217);
		int32_t L_218 = L_217;
		AnimationCurve_t3667593487 * L_219 = (L_216)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		NullCheck(L_219);
		KeyframeU5BU5D_t3589549831* L_220 = AnimationCurve_get_keys_m450535207(L_219, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_221 = __this->get_curves_24();
		int32_t L_222 = V_0;
		NullCheck(L_221);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_221, L_222);
		int32_t L_223 = L_222;
		AnimationCurve_t3667593487 * L_224 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		NullCheck(L_224);
		int32_t L_225 = AnimationCurve_get_length_m3019229777(L_224, /*hidden argument*/NULL);
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, ((int32_t)((int32_t)L_225-(int32_t)1)));
		float L_226 = Keyframe_get_time_m1367974951(((L_220)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_225-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_212);
		float L_227 = AnimationCurve_Evaluate_m547727012(L_212, ((float)((float)((float)((float)L_213/(float)L_215))*(float)L_226)), /*hidden argument*/NULL);
		G_B28_0 = L_208;
		G_B28_1 = L_207;
		if ((((float)L_227) == ((float)(0.0f))))
		{
			G_B29_0 = L_208;
			G_B29_1 = L_207;
			goto IL_03fa;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_228 = __this->get_fromFloats_27();
		int32_t L_229 = V_0;
		NullCheck(L_228);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_228, L_229);
		int32_t L_230 = L_229;
		float L_231 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230));
		AnimationCurveU5BU5D_t2600615382* L_232 = __this->get_curves_24();
		int32_t L_233 = V_0;
		NullCheck(L_232);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_232, L_233);
		int32_t L_234 = L_233;
		AnimationCurve_t3667593487 * L_235 = (L_232)->GetAt(static_cast<il2cpp_array_size_t>(L_234));
		float L_236 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_237 = __this->get_time_11();
		NullCheck(L_237);
		float L_238 = FsmFloat_get_Value_m4137923823(L_237, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_239 = __this->get_curves_24();
		int32_t L_240 = V_0;
		NullCheck(L_239);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_239, L_240);
		int32_t L_241 = L_240;
		AnimationCurve_t3667593487 * L_242 = (L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		NullCheck(L_242);
		KeyframeU5BU5D_t3589549831* L_243 = AnimationCurve_get_keys_m450535207(L_242, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_244 = __this->get_curves_24();
		int32_t L_245 = V_0;
		NullCheck(L_244);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_244, L_245);
		int32_t L_246 = L_245;
		AnimationCurve_t3667593487 * L_247 = (L_244)->GetAt(static_cast<il2cpp_array_size_t>(L_246));
		NullCheck(L_247);
		int32_t L_248 = AnimationCurve_get_length_m3019229777(L_247, /*hidden argument*/NULL);
		NullCheck(L_243);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_243, ((int32_t)((int32_t)L_248-(int32_t)1)));
		float L_249 = Keyframe_get_time_m1367974951(((L_243)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_248-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_235);
		float L_250 = AnimationCurve_Evaluate_m547727012(L_235, ((float)((float)((float)((float)L_236/(float)L_238))*(float)L_249)), /*hidden argument*/NULL);
		G_B30_0 = ((float)((float)L_231/(float)L_250));
		G_B30_1 = G_B28_0;
		G_B30_2 = G_B28_1;
		goto IL_03ff;
	}

IL_03fa:
	{
		G_B30_0 = (std::numeric_limits<float>::max());
		G_B30_1 = G_B29_0;
		G_B30_2 = G_B29_1;
	}

IL_03ff:
	{
		NullCheck(G_B30_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B30_2, G_B30_1);
		(G_B30_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B30_1), (float)G_B30_0);
		goto IL_0450;
	}

IL_0405:
	{
		SingleU5BU5D_t2316563989* L_251 = __this->get_resultFloats_26();
		int32_t L_252 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_253 = __this->get_curves_24();
		int32_t L_254 = V_0;
		NullCheck(L_253);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_253, L_254);
		int32_t L_255 = L_254;
		AnimationCurve_t3667593487 * L_256 = (L_253)->GetAt(static_cast<il2cpp_array_size_t>(L_255));
		float L_257 = __this->get_currentTime_18();
		NullCheck(L_256);
		float L_258 = AnimationCurve_Evaluate_m547727012(L_256, L_257, /*hidden argument*/NULL);
		G_B32_0 = L_252;
		G_B32_1 = L_251;
		if ((((float)L_258) == ((float)(0.0f))))
		{
			G_B33_0 = L_252;
			G_B33_1 = L_251;
			goto IL_044a;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_259 = __this->get_fromFloats_27();
		int32_t L_260 = V_0;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, L_260);
		int32_t L_261 = L_260;
		float L_262 = (L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261));
		AnimationCurveU5BU5D_t2600615382* L_263 = __this->get_curves_24();
		int32_t L_264 = V_0;
		NullCheck(L_263);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_263, L_264);
		int32_t L_265 = L_264;
		AnimationCurve_t3667593487 * L_266 = (L_263)->GetAt(static_cast<il2cpp_array_size_t>(L_265));
		float L_267 = __this->get_currentTime_18();
		NullCheck(L_266);
		float L_268 = AnimationCurve_Evaluate_m547727012(L_266, L_267, /*hidden argument*/NULL);
		G_B34_0 = ((float)((float)L_262/(float)L_268));
		G_B34_1 = G_B32_0;
		G_B34_2 = G_B32_1;
		goto IL_044f;
	}

IL_044a:
	{
		G_B34_0 = (std::numeric_limits<float>::max());
		G_B34_1 = G_B33_0;
		G_B34_2 = G_B33_1;
	}

IL_044f:
	{
		NullCheck(G_B34_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B34_2, G_B34_1);
		(G_B34_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B34_1), (float)G_B34_0);
	}

IL_0450:
	{
		goto IL_0522;
	}

IL_0455:
	{
		FsmFloat_t2134102846 * L_269 = __this->get_time_11();
		NullCheck(L_269);
		bool L_270 = NamedVariable_get_IsNone_m281035543(L_269, /*hidden argument*/NULL);
		if (L_270)
		{
			goto IL_04dd;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_271 = __this->get_resultFloats_26();
		int32_t L_272 = V_0;
		SingleU5BU5D_t2316563989* L_273 = __this->get_fromFloats_27();
		int32_t L_274 = V_0;
		NullCheck(L_273);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_273, L_274);
		int32_t L_275 = L_274;
		float L_276 = (L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275));
		G_B38_0 = L_272;
		G_B38_1 = L_271;
		if ((((float)L_276) == ((float)(0.0f))))
		{
			G_B39_0 = L_272;
			G_B39_1 = L_271;
			goto IL_04d2;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_277 = __this->get_curves_24();
		int32_t L_278 = V_0;
		NullCheck(L_277);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_277, L_278);
		int32_t L_279 = L_278;
		AnimationCurve_t3667593487 * L_280 = (L_277)->GetAt(static_cast<il2cpp_array_size_t>(L_279));
		float L_281 = __this->get_currentTime_18();
		FsmFloat_t2134102846 * L_282 = __this->get_time_11();
		NullCheck(L_282);
		float L_283 = FsmFloat_get_Value_m4137923823(L_282, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_284 = __this->get_curves_24();
		int32_t L_285 = V_0;
		NullCheck(L_284);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_284, L_285);
		int32_t L_286 = L_285;
		AnimationCurve_t3667593487 * L_287 = (L_284)->GetAt(static_cast<il2cpp_array_size_t>(L_286));
		NullCheck(L_287);
		KeyframeU5BU5D_t3589549831* L_288 = AnimationCurve_get_keys_m450535207(L_287, /*hidden argument*/NULL);
		AnimationCurveU5BU5D_t2600615382* L_289 = __this->get_curves_24();
		int32_t L_290 = V_0;
		NullCheck(L_289);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_289, L_290);
		int32_t L_291 = L_290;
		AnimationCurve_t3667593487 * L_292 = (L_289)->GetAt(static_cast<il2cpp_array_size_t>(L_291));
		NullCheck(L_292);
		int32_t L_293 = AnimationCurve_get_length_m3019229777(L_292, /*hidden argument*/NULL);
		NullCheck(L_288);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_288, ((int32_t)((int32_t)L_293-(int32_t)1)));
		float L_294 = Keyframe_get_time_m1367974951(((L_288)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_293-(int32_t)1))))), /*hidden argument*/NULL);
		NullCheck(L_280);
		float L_295 = AnimationCurve_Evaluate_m547727012(L_280, ((float)((float)((float)((float)L_281/(float)L_283))*(float)L_294)), /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_296 = __this->get_fromFloats_27();
		int32_t L_297 = V_0;
		NullCheck(L_296);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_296, L_297);
		int32_t L_298 = L_297;
		float L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		G_B40_0 = ((float)((float)L_295/(float)L_299));
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		goto IL_04d7;
	}

IL_04d2:
	{
		G_B40_0 = (std::numeric_limits<float>::max());
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
	}

IL_04d7:
	{
		NullCheck(G_B40_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B40_2, G_B40_1);
		(G_B40_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B40_1), (float)G_B40_0);
		goto IL_051d;
	}

IL_04dd:
	{
		SingleU5BU5D_t2316563989* L_300 = __this->get_resultFloats_26();
		int32_t L_301 = V_0;
		SingleU5BU5D_t2316563989* L_302 = __this->get_fromFloats_27();
		int32_t L_303 = V_0;
		NullCheck(L_302);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_302, L_303);
		int32_t L_304 = L_303;
		float L_305 = (L_302)->GetAt(static_cast<il2cpp_array_size_t>(L_304));
		G_B42_0 = L_301;
		G_B42_1 = L_300;
		if ((((float)L_305) == ((float)(0.0f))))
		{
			G_B43_0 = L_301;
			G_B43_1 = L_300;
			goto IL_0517;
		}
	}
	{
		AnimationCurveU5BU5D_t2600615382* L_306 = __this->get_curves_24();
		int32_t L_307 = V_0;
		NullCheck(L_306);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_306, L_307);
		int32_t L_308 = L_307;
		AnimationCurve_t3667593487 * L_309 = (L_306)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		float L_310 = __this->get_currentTime_18();
		NullCheck(L_309);
		float L_311 = AnimationCurve_Evaluate_m547727012(L_309, L_310, /*hidden argument*/NULL);
		SingleU5BU5D_t2316563989* L_312 = __this->get_fromFloats_27();
		int32_t L_313 = V_0;
		NullCheck(L_312);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_312, L_313);
		int32_t L_314 = L_313;
		float L_315 = (L_312)->GetAt(static_cast<il2cpp_array_size_t>(L_314));
		G_B44_0 = ((float)((float)L_311/(float)L_315));
		G_B44_1 = G_B42_0;
		G_B44_2 = G_B42_1;
		goto IL_051c;
	}

IL_0517:
	{
		G_B44_0 = (std::numeric_limits<float>::max());
		G_B44_1 = G_B43_0;
		G_B44_2 = G_B43_1;
	}

IL_051c:
	{
		NullCheck(G_B44_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B44_2, G_B44_1);
		(G_B44_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B44_1), (float)G_B44_0);
	}

IL_051d:
	{
		goto IL_0522;
	}

IL_0522:
	{
		goto IL_0537;
	}

IL_0527:
	{
		SingleU5BU5D_t2316563989* L_316 = __this->get_resultFloats_26();
		int32_t L_317 = V_0;
		SingleU5BU5D_t2316563989* L_318 = __this->get_fromFloats_27();
		int32_t L_319 = V_0;
		NullCheck(L_318);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_318, L_319);
		int32_t L_320 = L_319;
		float L_321 = (L_318)->GetAt(static_cast<il2cpp_array_size_t>(L_320));
		NullCheck(L_316);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_316, L_317);
		(L_316)->SetAt(static_cast<il2cpp_array_size_t>(L_317), (float)L_321);
	}

IL_0537:
	{
		goto IL_054c;
	}

IL_053c:
	{
		SingleU5BU5D_t2316563989* L_322 = __this->get_resultFloats_26();
		int32_t L_323 = V_0;
		SingleU5BU5D_t2316563989* L_324 = __this->get_fromFloats_27();
		int32_t L_325 = V_0;
		NullCheck(L_324);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_324, L_325);
		int32_t L_326 = L_325;
		float L_327 = (L_324)->GetAt(static_cast<il2cpp_array_size_t>(L_326));
		NullCheck(L_322);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_322, L_323);
		(L_322)->SetAt(static_cast<il2cpp_array_size_t>(L_323), (float)L_327);
	}

IL_054c:
	{
		int32_t L_328 = V_0;
		V_0 = ((int32_t)((int32_t)L_328+(int32_t)1));
	}

IL_0550:
	{
		int32_t L_329 = V_0;
		AnimationCurveU5BU5D_t2600615382* L_330 = __this->get_curves_24();
		NullCheck(L_330);
		if ((((int32_t)L_329) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_330)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::CheckFinished()
extern "C"  void AnimateFsmAction_CheckFinished_m3654930401 (AnimateFsmAction_t4201352541 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_isRunning_30();
		if (!L_0)
		{
			goto IL_005f;
		}
	}
	{
		bool L_1 = __this->get_looping_31();
		if (L_1)
		{
			goto IL_005f;
		}
	}
	{
		__this->set_finishAction_29((bool)1);
		V_0 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		float L_2 = __this->get_currentTime_18();
		SingleU5BU5D_t2316563989* L_3 = __this->get_endTimes_19();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if ((!(((float)L_2) < ((float)L_6))))
		{
			goto IL_003e;
		}
	}
	{
		__this->set_finishAction_29((bool)0);
	}

IL_003e:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_8 = V_0;
		SingleU5BU5D_t2316563989* L_9 = __this->get_endTimes_19();
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_10 = __this->get_finishAction_29();
		__this->set_isRunning_30((bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0));
	}

IL_005f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::.ctor()
extern "C"  void AnimateRect__ctor_m224664995 (AnimateRect_t723840755 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction__ctor_m3622440361(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::Reset()
extern Il2CppClass* FsmRect_t1076426478_il2cpp_TypeInfo_var;
extern const uint32_t AnimateRect_Reset_m2166065232_MetadataUsageId;
extern "C"  void AnimateRect_Reset_m2166065232 (AnimateRect_t723840755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateRect_Reset_m2166065232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmRect_t1076426478 * V_0 = NULL;
	{
		AnimateFsmAction_Reset_m1268873302(__this, /*hidden argument*/NULL);
		FsmRect_t1076426478 * L_0 = (FsmRect_t1076426478 *)il2cpp_codegen_object_new(FsmRect_t1076426478_il2cpp_TypeInfo_var);
		FsmRect__ctor_m2674586289(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmRect_t1076426478 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmRect_t1076426478 * L_2 = V_0;
		__this->set_rectVariable_34(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var;
extern Il2CppClass* CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var;
extern const uint32_t AnimateRect_OnEnter_m546423994_MetadataUsageId;
extern "C"  void AnimateRect_OnEnter_m546423994 (AnimateRect_t723840755 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateRect_OnEnter_m546423994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B2_0 = 0;
	SingleU5BU5D_t2316563989* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	SingleU5BU5D_t2316563989* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	SingleU5BU5D_t2316563989* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	SingleU5BU5D_t2316563989* G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	SingleU5BU5D_t2316563989* G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	int32_t G_B6_1 = 0;
	SingleU5BU5D_t2316563989* G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	SingleU5BU5D_t2316563989* G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	SingleU5BU5D_t2316563989* G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	int32_t G_B9_1 = 0;
	SingleU5BU5D_t2316563989* G_B9_2 = NULL;
	int32_t G_B11_0 = 0;
	SingleU5BU5D_t2316563989* G_B11_1 = NULL;
	int32_t G_B10_0 = 0;
	SingleU5BU5D_t2316563989* G_B10_1 = NULL;
	float G_B12_0 = 0.0f;
	int32_t G_B12_1 = 0;
	SingleU5BU5D_t2316563989* G_B12_2 = NULL;
	{
		AnimateFsmAction_OnEnter_m1633405760(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_43((bool)0);
		((AnimateFsmAction_t4201352541 *)__this)->set_resultFloats_26(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		((AnimateFsmAction_t4201352541 *)__this)->set_fromFloats_27(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)4)));
		SingleU5BU5D_t2316563989* L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmRect_t1076426478 * L_1 = __this->get_rectVariable_34();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		G_B1_0 = 0;
		G_B1_1 = L_0;
		if (!L_2)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0059;
	}

IL_0046:
	{
		FsmRect_t1076426478 * L_3 = __this->get_rectVariable_34();
		NullCheck(L_3);
		Rect_t4241904616  L_4 = FsmRect_get_Value_m1002500317(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0059:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (float)G_B3_0);
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmRect_t1076426478 * L_7 = __this->get_rectVariable_34();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B4_0 = 1;
		G_B4_1 = L_6;
		if (!L_8)
		{
			G_B5_0 = 1;
			G_B5_1 = L_6;
			goto IL_007b;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_008e;
	}

IL_007b:
	{
		FsmRect_t1076426478 * L_9 = __this->get_rectVariable_34();
		NullCheck(L_9);
		Rect_t4241904616  L_10 = FsmRect_get_Value_m1002500317(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = Rect_get_y_m982386315((&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_008e:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (float)G_B6_0);
		SingleU5BU5D_t2316563989* L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmRect_t1076426478 * L_13 = __this->get_rectVariable_34();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B7_0 = 2;
		G_B7_1 = L_12;
		if (!L_14)
		{
			G_B8_0 = 2;
			G_B8_1 = L_12;
			goto IL_00b0;
		}
	}
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c3;
	}

IL_00b0:
	{
		FsmRect_t1076426478 * L_15 = __this->get_rectVariable_34();
		NullCheck(L_15);
		Rect_t4241904616  L_16 = FsmRect_get_Value_m1002500317(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = Rect_get_width_m2824209432((&V_2), /*hidden argument*/NULL);
		G_B9_0 = L_17;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c3:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (float)G_B9_0);
		SingleU5BU5D_t2316563989* L_18 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmRect_t1076426478 * L_19 = __this->get_rectVariable_34();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		G_B10_0 = 3;
		G_B10_1 = L_18;
		if (!L_20)
		{
			G_B11_0 = 3;
			G_B11_1 = L_18;
			goto IL_00e5;
		}
	}
	{
		G_B12_0 = (0.0f);
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00f8;
	}

IL_00e5:
	{
		FsmRect_t1076426478 * L_21 = __this->get_rectVariable_34();
		NullCheck(L_21);
		Rect_t4241904616  L_22 = FsmRect_get_Value_m1002500317(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		G_B12_0 = L_23;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00f8:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		(G_B12_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_1), (float)G_B12_0);
		((AnimateFsmAction_t4201352541 *)__this)->set_curves_24(((AnimationCurveU5BU5D_t2600615382*)SZArrayNew(AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var, (uint32_t)4)));
		AnimationCurveU5BU5D_t2600615382* L_24 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_25 = __this->get_curveX_35();
		NullCheck(L_25);
		AnimationCurve_t3667593487 * L_26 = L_25->get_curve_0();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (AnimationCurve_t3667593487 *)L_26);
		AnimationCurveU5BU5D_t2600615382* L_27 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_28 = __this->get_curveY_37();
		NullCheck(L_28);
		AnimationCurve_t3667593487 * L_29 = L_28->get_curve_0();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 1);
		ArrayElementTypeCheck (L_27, L_29);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (AnimationCurve_t3667593487 *)L_29);
		AnimationCurveU5BU5D_t2600615382* L_30 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_31 = __this->get_curveW_39();
		NullCheck(L_31);
		AnimationCurve_t3667593487 * L_32 = L_31->get_curve_0();
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 2);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (AnimationCurve_t3667593487 *)L_32);
		AnimationCurveU5BU5D_t2600615382* L_33 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_34 = __this->get_curveH_41();
		NullCheck(L_34);
		AnimationCurve_t3667593487 * L_35 = L_34->get_curve_0();
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 3);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (AnimationCurve_t3667593487 *)L_35);
		((AnimateFsmAction_t4201352541 *)__this)->set_calculations_25(((CalculationU5BU5D_t3054796293*)SZArrayNew(CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var, (uint32_t)4)));
		CalculationU5BU5D_t3054796293* L_36 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_37 = __this->get_calculationX_36();
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_37);
		CalculationU5BU5D_t3054796293* L_38 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_39 = __this->get_calculationY_38();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_39);
		CalculationU5BU5D_t3054796293* L_40 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_41 = __this->get_calculationW_40();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 2);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_41);
		CalculationU5BU5D_t3054796293* L_42 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_43 = __this->get_calculationH_42();
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 3);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)L_43);
		AnimateFsmAction_Init_m3532072843(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_44 = ((AnimateFsmAction_t4201352541 *)__this)->get_delay_13();
		NullCheck(L_44);
		float L_45 = FsmFloat_get_Value_m4137923823(L_44, /*hidden argument*/NULL);
		float L_46 = fabsf(L_45);
		if ((!(((float)L_46) < ((float)(0.01f)))))
		{
			goto IL_01bb;
		}
	}
	{
		AnimateRect_UpdateVariableValue_m4235092429(__this, /*hidden argument*/NULL);
	}

IL_01bb:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::UpdateVariableValue()
extern "C"  void AnimateRect_UpdateVariableValue_m4235092429 (AnimateRect_t723840755 * __this, const MethodInfo* method)
{
	{
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_34();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		FsmRect_t1076426478 * L_2 = __this->get_rectVariable_34();
		SingleU5BU5D_t2316563989* L_3 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		float L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		float L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		SingleU5BU5D_t2316563989* L_9 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		int32_t L_10 = 2;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		SingleU5BU5D_t2316563989* L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		int32_t L_13 = 3;
		float L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		Rect_t4241904616  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m3291325233(&L_15, L_5, L_8, L_11, L_14, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmRect_set_Value_m1159518952(L_2, L_15, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnUpdate()
extern "C"  void AnimateRect_OnUpdate_m3187801289 (AnimateRect_t723840755 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction_OnUpdate_m2524497667(__this, /*hidden argument*/NULL);
		bool L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_isRunning_30();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AnimateRect_UpdateVariableValue_m4235092429(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		bool L_1 = __this->get_finishInNextStep_43();
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		bool L_2 = ((AnimateFsmAction_t4201352541 *)__this)->get_looping_31();
		if (L_2)
		{
			goto IL_0044;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishEvent_15();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	{
		bool L_5 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishAction_29();
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		bool L_6 = __this->get_finishInNextStep_43();
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		AnimateRect_UpdateVariableValue_m4235092429(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_43((bool)1);
	}

IL_0067:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::.ctor()
extern "C"  void AnimateVector3__ctor_m4284657199 (AnimateVector3_t2286814231 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction__ctor_m3622440361(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern const uint32_t AnimateVector3_Reset_m1931090140_MetadataUsageId;
extern "C"  void AnimateVector3_Reset_m1931090140 (AnimateVector3_t2286814231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateVector3_Reset_m1931090140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	{
		AnimateFsmAction_Reset_m1268873302(__this, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_vectorVariable_34(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnEnter()
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var;
extern Il2CppClass* CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var;
extern const uint32_t AnimateVector3_OnEnter_m2368627270_MetadataUsageId;
extern "C"  void AnimateVector3_OnEnter_m2368627270 (AnimateVector3_t2286814231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimateVector3_OnEnter_m2368627270_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B2_0 = 0;
	SingleU5BU5D_t2316563989* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	SingleU5BU5D_t2316563989* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	SingleU5BU5D_t2316563989* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	SingleU5BU5D_t2316563989* G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	SingleU5BU5D_t2316563989* G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	int32_t G_B6_1 = 0;
	SingleU5BU5D_t2316563989* G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	SingleU5BU5D_t2316563989* G_B8_1 = NULL;
	int32_t G_B7_0 = 0;
	SingleU5BU5D_t2316563989* G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	int32_t G_B9_1 = 0;
	SingleU5BU5D_t2316563989* G_B9_2 = NULL;
	{
		AnimateFsmAction_OnEnter_m1633405760(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_41((bool)0);
		((AnimateFsmAction_t4201352541 *)__this)->set_resultFloats_26(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)3)));
		((AnimateFsmAction_t4201352541 *)__this)->set_fromFloats_27(((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)3)));
		SingleU5BU5D_t2316563989* L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmVector3_t533912882 * L_1 = __this->get_vectorVariable_34();
		NullCheck(L_1);
		bool L_2 = NamedVariable_get_IsNone_m281035543(L_1, /*hidden argument*/NULL);
		G_B1_0 = 0;
		G_B1_1 = L_0;
		if (!L_2)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0059;
	}

IL_0046:
	{
		FsmVector3_t533912882 * L_3 = __this->get_vectorVariable_34();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = FsmVector3_get_Value_m2779135117(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0059:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (float)G_B3_0);
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmVector3_t533912882 * L_7 = __this->get_vectorVariable_34();
		NullCheck(L_7);
		bool L_8 = NamedVariable_get_IsNone_m281035543(L_7, /*hidden argument*/NULL);
		G_B4_0 = 1;
		G_B4_1 = L_6;
		if (!L_8)
		{
			G_B5_0 = 1;
			G_B5_1 = L_6;
			goto IL_007b;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_008e;
	}

IL_007b:
	{
		FsmVector3_t533912882 * L_9 = __this->get_vectorVariable_34();
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = FsmVector3_get_Value_m2779135117(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_y_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_008e:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (float)G_B6_0);
		SingleU5BU5D_t2316563989* L_12 = ((AnimateFsmAction_t4201352541 *)__this)->get_fromFloats_27();
		FsmVector3_t533912882 * L_13 = __this->get_vectorVariable_34();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		G_B7_0 = 2;
		G_B7_1 = L_12;
		if (!L_14)
		{
			G_B8_0 = 2;
			G_B8_1 = L_12;
			goto IL_00b0;
		}
	}
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c3;
	}

IL_00b0:
	{
		FsmVector3_t533912882 * L_15 = __this->get_vectorVariable_34();
		NullCheck(L_15);
		Vector3_t4282066566  L_16 = FsmVector3_get_Value_m2779135117(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = (&V_2)->get_z_3();
		G_B9_0 = L_17;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c3:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (float)G_B9_0);
		((AnimateFsmAction_t4201352541 *)__this)->set_curves_24(((AnimationCurveU5BU5D_t2600615382*)SZArrayNew(AnimationCurveU5BU5D_t2600615382_il2cpp_TypeInfo_var, (uint32_t)3)));
		AnimationCurveU5BU5D_t2600615382* L_18 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_19 = __this->get_curveX_35();
		NullCheck(L_19);
		AnimationCurve_t3667593487 * L_20 = L_19->get_curve_0();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (AnimationCurve_t3667593487 *)L_20);
		AnimationCurveU5BU5D_t2600615382* L_21 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_22 = __this->get_curveY_37();
		NullCheck(L_22);
		AnimationCurve_t3667593487 * L_23 = L_22->get_curve_0();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (AnimationCurve_t3667593487 *)L_23);
		AnimationCurveU5BU5D_t2600615382* L_24 = ((AnimateFsmAction_t4201352541 *)__this)->get_curves_24();
		FsmAnimationCurve_t2685995989 * L_25 = __this->get_curveZ_39();
		NullCheck(L_25);
		AnimationCurve_t3667593487 * L_26 = L_25->get_curve_0();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (AnimationCurve_t3667593487 *)L_26);
		((AnimateFsmAction_t4201352541 *)__this)->set_calculations_25(((CalculationU5BU5D_t3054796293*)SZArrayNew(CalculationU5BU5D_t3054796293_il2cpp_TypeInfo_var, (uint32_t)3)));
		CalculationU5BU5D_t3054796293* L_27 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_28 = __this->get_calculationX_36();
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_28);
		CalculationU5BU5D_t3054796293* L_29 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_30 = __this->get_calculationY_38();
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 1);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_30);
		CalculationU5BU5D_t3054796293* L_31 = ((AnimateFsmAction_t4201352541 *)__this)->get_calculations_25();
		int32_t L_32 = __this->get_calculationZ_40();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 2);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_32);
		AnimateFsmAction_Init_m3532072843(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_33 = ((AnimateFsmAction_t4201352541 *)__this)->get_delay_13();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		float L_35 = fabsf(L_34);
		if ((!(((float)L_35) < ((float)(0.01f)))))
		{
			goto IL_0165;
		}
	}
	{
		AnimateVector3_UpdateVariableValue_m699338073(__this, /*hidden argument*/NULL);
	}

IL_0165:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::UpdateVariableValue()
extern "C"  void AnimateVector3_UpdateVariableValue_m699338073 (AnimateVector3_t2286814231 * __this, const MethodInfo* method)
{
	{
		FsmVector3_t533912882 * L_0 = __this->get_vectorVariable_34();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0038;
		}
	}
	{
		FsmVector3_t533912882 * L_2 = __this->get_vectorVariable_34();
		SingleU5BU5D_t2316563989* L_3 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		float L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		SingleU5BU5D_t2316563989* L_6 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		float L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		SingleU5BU5D_t2316563989* L_9 = ((AnimateFsmAction_t4201352541 *)__this)->get_resultFloats_26();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		int32_t L_10 = 2;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		Vector3_t4282066566  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, L_5, L_8, L_11, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVector3_set_Value_m716982822(L_2, L_12, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnUpdate()
extern "C"  void AnimateVector3_OnUpdate_m3841527997 (AnimateVector3_t2286814231 * __this, const MethodInfo* method)
{
	{
		AnimateFsmAction_OnUpdate_m2524497667(__this, /*hidden argument*/NULL);
		bool L_0 = ((AnimateFsmAction_t4201352541 *)__this)->get_isRunning_30();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AnimateVector3_UpdateVariableValue_m699338073(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		bool L_1 = __this->get_finishInNextStep_41();
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		bool L_2 = ((AnimateFsmAction_t4201352541 *)__this)->get_looping_31();
		if (L_2)
		{
			goto IL_0044;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishEvent_15();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	{
		bool L_5 = ((AnimateFsmAction_t4201352541 *)__this)->get_finishAction_29();
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		bool L_6 = __this->get_finishInNextStep_41();
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		AnimateVector3_UpdateVariableValue_m699338073(__this, /*hidden argument*/NULL);
		__this->set_finishInNextStep_41((bool)1);
	}

IL_0067:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::.ctor()
extern "C"  void AnimationSettings__ctor_m1957903553 (AnimationSettings_t3134957589 * __this, const MethodInfo* method)
{
	{
		BaseAnimationAction__ctor_m4099946943(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::Reset()
extern "C"  void AnimationSettings_Reset_m3899303790 (AnimationSettings_t3134957589 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_animName_14((FsmString_t952858651 *)NULL);
		__this->set_wrapMode_15(2);
		__this->set_blendMode_16(0);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_speed_17(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_layer_18(L_1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::OnEnter()
extern "C"  void AnimationSettings_OnEnter_m4036334680 (AnimationSettings_t3134957589 * __this, const MethodInfo* method)
{
	{
		AnimationSettings_DoAnimationSettings_m2943551707(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::DoAnimationSettings()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_animation_m4211453288_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral645160400;
extern const uint32_t AnimationSettings_DoAnimationSettings_m2943551707_MetadataUsageId;
extern "C"  void AnimationSettings_DoAnimationSettings_m2943551707 (AnimationSettings_t3134957589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationSettings_DoAnimationSettings_m2943551707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AnimationState_t3682323633 * V_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_animName_14();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_4 = __this->get_gameObject_13();
		NullCheck(L_3);
		GameObject_t3674682005 * L_5 = Fsm_GetOwnerDefaultTarget_m846013999(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t3674682005 * L_6 = V_0;
		bool L_7 = ComponentAction_1_UpdateCache_m3952061936(__this, L_6, /*hidden argument*/ComponentAction_1_UpdateCache_m3952061936_MethodInfo_var);
		if (L_7)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		Animation_t1724966010 * L_8 = ComponentAction_1_get_animation_m4211453288(__this, /*hidden argument*/ComponentAction_1_get_animation_m4211453288_MethodInfo_var);
		FsmString_t952858651 * L_9 = __this->get_animName_14();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_t3682323633 * L_11 = Animation_get_Item_m2669576386(L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		AnimationState_t3682323633 * L_12 = V_1;
		bool L_13 = TrackedReference_op_Equality_m4125598506(NULL /*static, unused*/, L_12, (TrackedReference_t2089686725 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		FsmString_t952858651 * L_14 = __this->get_animName_14();
		NullCheck(L_14);
		String_t* L_15 = FsmString_get_Value_m872383149(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral645160400, L_15, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0074:
	{
		AnimationState_t3682323633 * L_17 = V_1;
		int32_t L_18 = __this->get_wrapMode_15();
		NullCheck(L_17);
		AnimationState_set_wrapMode_m3837047690(L_17, L_18, /*hidden argument*/NULL);
		AnimationState_t3682323633 * L_19 = V_1;
		int32_t L_20 = __this->get_blendMode_16();
		NullCheck(L_19);
		AnimationState_set_blendMode_m2249435534(L_19, L_20, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_21 = __this->get_layer_18();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00ad;
		}
	}
	{
		AnimationState_t3682323633 * L_23 = V_1;
		FsmInt_t1596138449 * L_24 = __this->get_layer_18();
		NullCheck(L_24);
		int32_t L_25 = FsmInt_get_Value_m27059446(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		AnimationState_set_layer_m2805758051(L_23, L_25, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		FsmFloat_t2134102846 * L_26 = __this->get_speed_17();
		NullCheck(L_26);
		bool L_27 = NamedVariable_get_IsNone_m281035543(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00ce;
		}
	}
	{
		AnimationState_t3682323633 * L_28 = V_1;
		FsmFloat_t2134102846 * L_29 = __this->get_speed_17();
		NullCheck(L_29);
		float L_30 = FsmFloat_get_Value_m4137923823(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		AnimationState_set_speed_m4187319075(L_28, L_30, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::.ctor()
extern "C"  void AnimatorCrossFade__ctor_m2229679251 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::Reset()
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorCrossFade_Reset_m4171079488_MetadataUsageId;
extern "C"  void AnimatorCrossFade_Reset_m4171079488 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorCrossFade_Reset_m4171079488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmInt_t1596138449 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_stateName_12((FsmString_t952858651 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_transitionDuration_13(L_0);
		FsmInt_t1596138449 * L_1 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmInt_t1596138449 * L_2 = V_0;
		NullCheck(L_2);
		NamedVariable_set_UseVariable_m4266138971(L_2, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_3 = V_0;
		__this->set_layer_14(L_3);
		FsmFloat_t2134102846 * L_4 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		FsmFloat_t2134102846 * L_5 = V_1;
		NullCheck(L_5);
		NamedVariable_set_UseVariable_m4266138971(L_5, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = V_1;
		__this->set_normalizedTime_15(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorCrossFade::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorCrossFade_OnEnter_m3219775402_MetadataUsageId;
extern "C"  void AnimatorCrossFade_OnEnter_m3219775402 (AnimatorCrossFade_t3506781187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorCrossFade_OnEnter_m3219775402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t G_B6_0 = 0;
	float G_B9_0 = 0.0f;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00ad;
		}
	}
	{
		FsmInt_t1596138449 * L_9 = __this->get_layer_14();
		NullCheck(L_9);
		bool L_10 = NamedVariable_get_IsNone_m281035543(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		G_B6_0 = (-1);
		goto IL_0063;
	}

IL_0058:
	{
		FsmInt_t1596138449 * L_11 = __this->get_layer_14();
		NullCheck(L_11);
		int32_t L_12 = FsmInt_get_Value_m27059446(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
	}

IL_0063:
	{
		V_1 = G_B6_0;
		FsmFloat_t2134102846 * L_13 = __this->get_normalizedTime_15();
		NullCheck(L_13);
		bool L_14 = NamedVariable_get_IsNone_m281035543(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007e;
		}
	}
	{
		G_B9_0 = (-std::numeric_limits<float>::infinity());
		goto IL_0089;
	}

IL_007e:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_normalizedTime_15();
		NullCheck(L_15);
		float L_16 = FsmFloat_get_Value_m4137923823(L_15, /*hidden argument*/NULL);
		G_B9_0 = L_16;
	}

IL_0089:
	{
		V_2 = G_B9_0;
		Animator_t2776330603 * L_17 = __this->get__animator_16();
		FsmString_t952858651 * L_18 = __this->get_stateName_12();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_transitionDuration_13();
		NullCheck(L_20);
		float L_21 = FsmFloat_get_Value_m4137923823(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		float L_23 = V_2;
		NullCheck(L_17);
		Animator_CrossFade_m1281797781(L_17, L_19, L_21, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::.ctor()
extern "C"  void AnimatorInterruptMatchTarget__ctor_m3043591588 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::Reset()
extern "C"  void AnimatorInterruptMatchTarget_Reset_m690024529 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_completeMatch_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorInterruptMatchTarget_OnEnter_m3705483387_MetadataUsageId;
extern "C"  void AnimatorInterruptMatchTarget_OnEnter_m3705483387 (AnimatorInterruptMatchTarget_t1680292610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorInterruptMatchTarget_OnEnter_m3705483387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Animator_t2776330603 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		V_1 = L_6;
		Animator_t2776330603 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		Animator_t2776330603 * L_9 = V_1;
		FsmBool_t1075959796 * L_10 = __this->get_completeMatch_12();
		NullCheck(L_10);
		bool L_11 = FsmBool_get_Value_m3101329097(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Animator_InterruptMatchTarget_m3501603944(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0049:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::.ctor()
extern "C"  void AnimatorMatchTarget__ctor_m2276692473 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmQuaternion_t3871136040_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorMatchTarget_Reset_m4218092710_MetadataUsageId;
extern "C"  void AnimatorMatchTarget_Reset_m4218092710 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorMatchTarget_Reset_m4218092710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	FsmQuaternion_t3871136040 * V_1 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_bodyPart_12(0);
		__this->set_target_13((FsmGameObject_t1697147867 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_targetPosition_14(L_2);
		FsmQuaternion_t3871136040 * L_3 = (FsmQuaternion_t3871136040 *)il2cpp_codegen_object_new(FsmQuaternion_t3871136040_il2cpp_TypeInfo_var);
		FsmQuaternion__ctor_m44600631(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmQuaternion_t3871136040 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_5 = V_1;
		__this->set_targetRotation_15(L_5);
		Vector3_t4282066566  L_6 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_7 = FsmVector3_op_Implicit_m3836665052(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_positionWeight_16(L_7);
		FsmFloat_t2134102846 * L_8 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_rotationWeight_17(L_8);
		__this->set_startNormalizedTime_18((FsmFloat_t2134102846 *)NULL);
		__this->set_targetNormalizedTime_19((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_20((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorMatchTarget_OnEnter_m1154841488_MetadataUsageId;
extern "C"  void AnimatorMatchTarget_OnEnter_m1154841488 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorMatchTarget_OnEnter_m1154841488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	GameObject_t3674682005 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_21(L_6);
		Animator_t2776330603 * L_7 = __this->get__animator_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0049:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_target_13();
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = FsmGameObject_get_Value_m673294275(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t3674682005 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t3674682005 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		__this->set__transform_22(L_14);
	}

IL_006d:
	{
		AnimatorMatchTarget_DoMatchTarget_m2294716546(__this, /*hidden argument*/NULL);
		bool L_15 = __this->get_everyFrame_20();
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnUpdate()
extern "C"  void AnimatorMatchTarget_OnUpdate_m573907123 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method)
{
	{
		AnimatorMatchTarget_DoMatchTarget_m2294716546(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::DoMatchTarget()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorMatchTarget_DoMatchTarget_m2294716546_MetadataUsageId;
extern "C"  void AnimatorMatchTarget_DoMatchTarget_m2294716546 (AnimatorMatchTarget_t2687678941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorMatchTarget_DoMatchTarget_m2294716546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	MatchTargetWeightMask_t258413904  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Animator_t2776330603 * L_0 = __this->get__animator_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Vector3_t4282066566  L_2 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		Quaternion_t1553702882  L_3 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t1659122786 * L_4 = __this->get__transform_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		Transform_t1659122786 * L_6 = __this->get__transform_22();
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Transform_t1659122786 * L_8 = __this->get__transform_22();
		NullCheck(L_8);
		Quaternion_t1553702882  L_9 = Transform_get_rotation_m11483428(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0047:
	{
		FsmVector3_t533912882 * L_10 = __this->get_targetPosition_14();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0069;
		}
	}
	{
		Vector3_t4282066566  L_12 = V_0;
		FsmVector3_t533912882 * L_13 = __this->get_targetPosition_14();
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = FsmVector3_get_Value_m2779135117(L_13, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0069:
	{
		FsmQuaternion_t3871136040 * L_16 = __this->get_targetRotation_15();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008b;
		}
	}
	{
		Quaternion_t1553702882  L_18 = V_1;
		FsmQuaternion_t3871136040 * L_19 = __this->get_targetRotation_15();
		NullCheck(L_19);
		Quaternion_t1553702882  L_20 = FsmQuaternion_get_Value_m3393858025(L_19, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_21 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
	}

IL_008b:
	{
		FsmVector3_t533912882 * L_22 = __this->get_positionWeight_16();
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = FsmVector3_get_Value_m2779135117(L_22, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_24 = __this->get_rotationWeight_17();
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		MatchTargetWeightMask__ctor_m2493400991((&V_2), L_23, L_25, /*hidden argument*/NULL);
		Animator_t2776330603 * L_26 = __this->get__animator_21();
		Vector3_t4282066566  L_27 = V_0;
		Quaternion_t1553702882  L_28 = V_1;
		int32_t L_29 = __this->get_bodyPart_12();
		MatchTargetWeightMask_t258413904  L_30 = V_2;
		FsmFloat_t2134102846 * L_31 = __this->get_startNormalizedTime_18();
		NullCheck(L_31);
		float L_32 = FsmFloat_get_Value_m4137923823(L_31, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_33 = __this->get_targetNormalizedTime_19();
		NullCheck(L_33);
		float L_34 = FsmFloat_get_Value_m4137923823(L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		Animator_MatchTarget_m4236652838(L_26, L_27, L_28, L_29, L_30, L_32, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::.ctor()
extern "C"  void AnimatorPlay__ctor_m4046154979 (AnimatorPlay_t630493411 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::Reset()
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorPlay_Reset_m1692587920_MetadataUsageId;
extern "C"  void AnimatorPlay_Reset_m1692587920 (AnimatorPlay_t630493411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorPlay_Reset_m1692587920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmInt_t1596138449 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_stateName_12((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmInt_t1596138449 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = V_0;
		__this->set_layer_13(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmFloat_t2134102846 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_1;
		__this->set_normalizedTime_14(L_5);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorPlay_OnEnter_m801260538_MetadataUsageId;
extern "C"  void AnimatorPlay_OnEnter_m801260538 (AnimatorPlay_t630493411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorPlay_OnEnter_m801260538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		__this->set__animator_16(L_6);
		AnimatorPlay_DoAnimatorPlay_m376569191(__this, /*hidden argument*/NULL);
		bool L_7 = __this->get_everyFrame_15();
		if (L_7)
		{
			goto IL_0048;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnUpdate()
extern "C"  void AnimatorPlay_OnUpdate_m2497799561 (AnimatorPlay_t630493411 * __this, const MethodInfo* method)
{
	{
		AnimatorPlay_DoAnimatorPlay_m376569191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::DoAnimatorPlay()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorPlay_DoAnimatorPlay_m376569191_MetadataUsageId;
extern "C"  void AnimatorPlay_DoAnimatorPlay_m376569191 (AnimatorPlay_t630493411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorPlay_DoAnimatorPlay_m376569191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B7_0 = 0.0f;
	{
		Animator_t2776330603 * L_0 = __this->get__animator_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0071;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_layer_13();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		G_B4_0 = (-1);
		goto IL_0032;
	}

IL_0027:
	{
		FsmInt_t1596138449 * L_4 = __this->get_layer_13();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		G_B4_0 = L_5;
	}

IL_0032:
	{
		V_0 = G_B4_0;
		FsmFloat_t2134102846 * L_6 = __this->get_normalizedTime_14();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		G_B7_0 = (-std::numeric_limits<float>::infinity());
		goto IL_0058;
	}

IL_004d:
	{
		FsmFloat_t2134102846 * L_8 = __this->get_normalizedTime_14();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		G_B7_0 = L_9;
	}

IL_0058:
	{
		V_1 = G_B7_0;
		Animator_t2776330603 * L_10 = __this->get__animator_16();
		FsmString_t952858651 * L_11 = __this->get_stateName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		float L_14 = V_1;
		NullCheck(L_10);
		Animator_Play_m3631644044(L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
	}

IL_0071:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::.ctor()
extern "C"  void AnimatorStartPlayback__ctor_m3975237106 (AnimatorStartPlayback_t1925519300 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::Reset()
extern "C"  void AnimatorStartPlayback_Reset_m1621670047 (AnimatorStartPlayback_t1925519300 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorStartPlayback_OnEnter_m1368661321_MetadataUsageId;
extern "C"  void AnimatorStartPlayback_OnEnter_m1368661321 (AnimatorStartPlayback_t1925519300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorStartPlayback_OnEnter_m1368661321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Animator_t2776330603 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		V_1 = L_6;
		Animator_t2776330603 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		Animator_t2776330603 * L_9 = V_1;
		NullCheck(L_9);
		Animator_StartPlayback_m3009121761(L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::.ctor()
extern "C"  void AnimatorStartRecording__ctor_m3684098504 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::Reset()
extern "C"  void AnimatorStartRecording_Reset_m1330531445 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_frameCount_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorStartRecording_OnEnter_m757339039_MetadataUsageId;
extern "C"  void AnimatorStartRecording_OnEnter_m757339039 (AnimatorStartRecording_t1781471326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorStartRecording_OnEnter_m757339039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Animator_t2776330603 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		V_1 = L_6;
		Animator_t2776330603 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0049;
		}
	}
	{
		Animator_t2776330603 * L_9 = V_1;
		FsmInt_t1596138449 * L_10 = __this->get_frameCount_12();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Animator_StartRecording_m2916226974(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0049:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::.ctor()
extern "C"  void AnimatorStopPlayback__ctor_m2330795514 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::Reset()
extern "C"  void AnimatorStopPlayback_Reset_m4272195751 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorStopPlayback_OnEnter_m1608256337_MetadataUsageId;
extern "C"  void AnimatorStopPlayback_OnEnter_m1608256337 (AnimatorStopPlayback_t1971151340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorStopPlayback_OnEnter_m1608256337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Animator_t2776330603 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		V_1 = L_6;
		Animator_t2776330603 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		Animator_t2776330603 * L_9 = V_1;
		NullCheck(L_9);
		Animator_StopPlayback_m3612462619(L_9, /*hidden argument*/NULL);
	}

IL_003e:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::.ctor()
extern "C"  void AnimatorStopRecording__ctor_m4246016704 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::Reset()
extern "C"  void AnimatorStopRecording_Reset_m1892449645 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_recorderStartTime_12((FsmFloat_t2134102846 *)NULL);
		__this->set_recorderStopTime_13((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopRecording::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var;
extern const uint32_t AnimatorStopRecording_OnEnter_m3889817239_MetadataUsageId;
extern "C"  void AnimatorStopRecording_OnEnter_m3889817239 (AnimatorStopRecording_t3196064566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatorStopRecording_OnEnter_m3889817239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Animator_t2776330603 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Animator_t2776330603 * L_6 = GameObject_GetComponent_TisAnimator_t2776330603_m2581074431(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t2776330603_m2581074431_MethodInfo_var);
		V_1 = L_6;
		Animator_t2776330603 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		Animator_t2776330603 * L_9 = V_1;
		NullCheck(L_9);
		Animator_StopRecording_m4232410323(L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = __this->get_recorderStartTime_12();
		Animator_t2776330603 * L_11 = V_1;
		NullCheck(L_11);
		float L_12 = Animator_get_recorderStartTime_m1936545440(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_12, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_13 = __this->get_recorderStopTime_13();
		Animator_t2776330603 * L_14 = V_1;
		NullCheck(L_14);
		float L_15 = Animator_get_recorderStopTime_m3086287392(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmFloat_set_Value_m1568963140(L_13, L_15, /*hidden argument*/NULL);
	}

IL_0060:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnyKey::.ctor()
extern "C"  void AnyKey__ctor_m2770516491 (AnyKey_t2823194811 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnyKey::Reset()
extern "C"  void AnyKey_Reset_m416949432 (AnyKey_t2823194811 * __this, const MethodInfo* method)
{
	{
		__this->set_sendEvent_11((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AnyKey::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t AnyKey_OnUpdate_m1822238561_MetadataUsageId;
extern "C"  void AnyKey_OnUpdate_m1822238561 (AnyKey_t2823194811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnyKey_OnUpdate_m1822238561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_get_anyKeyDown_m4005646189(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_2 = __this->get_sendEvent_11();
		NullCheck(L_1);
		Fsm_Event_m625948263(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::.ctor()
extern "C"  void ApplicationQuit__ctor_m1021639945 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::Reset()
extern "C"  void ApplicationQuit_Reset_m2963040182 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::OnEnter()
extern "C"  void ApplicationQuit_OnEnter_m1935172256 (ApplicationQuit_t3450523853 * __this, const MethodInfo* method)
{
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::.ctor()
extern "C"  void ApplicationRunInBackground__ctor_m4008746704 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::Reset()
extern "C"  void ApplicationRunInBackground_Reset_m1655179645 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_runInBackground_11(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::OnEnter()
extern "C"  void ApplicationRunInBackground_OnEnter_m3506613927 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method)
{
	{
		FsmBool_t1075959796 * L_0 = __this->get_runInBackground_11();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		Application_set_runInBackground_m2333211263(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::.ctor()
extern "C"  void ArrayAdd__ctor_m2610541622 (ArrayAdd_t2749463600 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::Reset()
extern "C"  void ArrayAdd_Reset_m256974563 (ArrayAdd_t2749463600 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_value_12((FsmVar_t1596150537 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::OnEnter()
extern "C"  void ArrayAdd_OnEnter_m4156293773 (ArrayAdd_t2749463600 * __this, const MethodInfo* method)
{
	{
		ArrayAdd_DoAddValue_m330101961(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::DoAddValue()
extern "C"  void ArrayAdd_DoAddValue_m330101961 (ArrayAdd_t2749463600 * __this, const MethodInfo* method)
{
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		FsmArray_t2129666875 * L_1 = __this->get_array_11();
		NullCheck(L_1);
		int32_t L_2 = FsmArray_get_Length_m3299442701(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmArray_Resize_m3367576465(L_0, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_3 = __this->get_value_12();
		NullCheck(L_3);
		FsmVar_UpdateValue_m1579759472(L_3, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		FsmArray_t2129666875 * L_5 = __this->get_array_11();
		NullCheck(L_5);
		int32_t L_6 = FsmArray_get_Length_m3299442701(L_5, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_7 = __this->get_value_12();
		NullCheck(L_7);
		Il2CppObject * L_8 = FsmVar_GetValue_m2309870922(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmArray_Set_m410178455(L_4, ((int32_t)((int32_t)L_6-(int32_t)1)), L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::.ctor()
extern "C"  void ArrayAddRange__ctor_m4000603187 (ArrayAddRange_t613168227 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::Reset()
extern Il2CppClass* FsmVarU5BU5D_t3498949300_il2cpp_TypeInfo_var;
extern const uint32_t ArrayAddRange_Reset_m1647036128_MetadataUsageId;
extern "C"  void ArrayAddRange_Reset_m1647036128 (ArrayAddRange_t613168227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayAddRange_Reset_m1647036128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_variables_12(((FsmVarU5BU5D_t3498949300*)SZArrayNew(FsmVarU5BU5D_t3498949300_il2cpp_TypeInfo_var, (uint32_t)2)));
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::OnEnter()
extern "C"  void ArrayAddRange_OnEnter_m4270628682 (ArrayAddRange_t613168227 * __this, const MethodInfo* method)
{
	{
		ArrayAddRange_DoAddRange_m1318692792(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::DoAddRange()
extern "C"  void ArrayAddRange_DoAddRange_m1318692792 (ArrayAddRange_t613168227 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	FsmVar_t1596150537 * V_1 = NULL;
	FsmVarU5BU5D_t3498949300* V_2 = NULL;
	int32_t V_3 = 0;
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_variables_12();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		FsmArray_t2129666875 * L_2 = __this->get_array_11();
		FsmArray_t2129666875 * L_3 = __this->get_array_11();
		NullCheck(L_3);
		int32_t L_4 = FsmArray_get_Length_m3299442701(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		NullCheck(L_2);
		FsmArray_Resize_m3367576465(L_2, ((int32_t)((int32_t)L_4+(int32_t)L_5)), /*hidden argument*/NULL);
		FsmVarU5BU5D_t3498949300* L_6 = __this->get_variables_12();
		V_2 = L_6;
		V_3 = 0;
		goto IL_0066;
	}

IL_0036:
	{
		FsmVarU5BU5D_t3498949300* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		FsmVar_t1596150537 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_1 = L_10;
		FsmVar_t1596150537 * L_11 = V_1;
		NullCheck(L_11);
		FsmVar_UpdateValue_m1579759472(L_11, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_12 = __this->get_array_11();
		FsmArray_t2129666875 * L_13 = __this->get_array_11();
		NullCheck(L_13);
		int32_t L_14 = FsmArray_get_Length_m3299442701(L_13, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		FsmVar_t1596150537 * L_16 = V_1;
		NullCheck(L_16);
		Il2CppObject * L_17 = FsmVar_GetValue_m2309870922(L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmArray_Set_m410178455(L_12, ((int32_t)((int32_t)L_14-(int32_t)L_15)), L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18-(int32_t)1));
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_20 = V_3;
		FsmVarU5BU5D_t3498949300* L_21 = V_2;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_006f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayClear::.ctor()
extern "C"  void ArrayClear__ctor_m1063197450 (ArrayClear_t1539829980 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayClear::Reset()
extern Il2CppClass* FsmVar_t1596150537_il2cpp_TypeInfo_var;
extern const uint32_t ArrayClear_Reset_m3004597687_MetadataUsageId;
extern "C"  void ArrayClear_Reset_m3004597687 (ArrayClear_t1539829980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayClear_Reset_m3004597687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVar_t1596150537 * V_0 = NULL;
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		FsmVar_t1596150537 * L_0 = (FsmVar_t1596150537 *)il2cpp_codegen_object_new(FsmVar_t1596150537_il2cpp_TypeInfo_var);
		FsmVar__ctor_m2050768746(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVar_t1596150537 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_useVariable_2((bool)1);
		FsmVar_t1596150537 * L_2 = V_0;
		__this->set_resetValue_12(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayClear::OnEnter()
extern "C"  void ArrayClear_OnEnter_m3217228897 (ArrayClear_t1539829980 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		int32_t L_1 = FsmArray_get_Length_m3299442701(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmArray_t2129666875 * L_2 = __this->get_array_11();
		NullCheck(L_2);
		FsmArray_Reset_m826385317(L_2, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_3 = __this->get_array_11();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		FsmArray_Resize_m3367576465(L_3, L_4, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_5 = __this->get_resetValue_12();
		NullCheck(L_5);
		bool L_6 = FsmVar_get_IsNone_m3892161437(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0069;
		}
	}
	{
		FsmVar_t1596150537 * L_7 = __this->get_resetValue_12();
		NullCheck(L_7);
		FsmVar_UpdateValue_m1579759472(L_7, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_8 = __this->get_resetValue_12();
		NullCheck(L_8);
		Il2CppObject * L_9 = FsmVar_GetValue_m2309870922(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = 0;
		goto IL_0062;
	}

IL_0051:
	{
		FsmArray_t2129666875 * L_10 = __this->get_array_11();
		int32_t L_11 = V_2;
		Il2CppObject * L_12 = V_1;
		NullCheck(L_10);
		FsmArray_Set_m410178455(L_10, L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_0;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0051;
		}
	}

IL_0069:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::.ctor()
extern "C"  void ArrayCompare__ctor_m3602367090 (ArrayCompare_t3109678708 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::Reset()
extern "C"  void ArrayCompare_Reset_m1248800031 (ArrayCompare_t3109678708 * __this, const MethodInfo* method)
{
	{
		__this->set_array1_11((FsmArray_t2129666875 *)NULL);
		__this->set_array2_12((FsmArray_t2129666875 *)NULL);
		__this->set_SequenceEqual_13((FsmEvent_t2133468028 *)NULL);
		__this->set_SequenceNotEqual_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::OnEnter()
extern "C"  void ArrayCompare_OnEnter_m3817828809 (ArrayCompare_t3109678708 * __this, const MethodInfo* method)
{
	{
		ArrayCompare_DoSequenceEqual_m1872995320(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::DoSequenceEqual()
extern const MethodInfo* Enumerable_SequenceEqual_TisIl2CppObject_m1941331891_MethodInfo_var;
extern const uint32_t ArrayCompare_DoSequenceEqual_m1872995320_MetadataUsageId;
extern "C"  void ArrayCompare_DoSequenceEqual_m1872995320 (ArrayCompare_t3109678708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayCompare_DoSequenceEqual_m1872995320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Fsm_t1527112426 * G_B5_0 = NULL;
	Fsm_t1527112426 * G_B4_0 = NULL;
	FsmEvent_t2133468028 * G_B6_0 = NULL;
	Fsm_t1527112426 * G_B6_1 = NULL;
	{
		FsmArray_t2129666875 * L_0 = __this->get_array1_11();
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_1 = FsmArray_get_Values_m1362031434(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		FsmArray_t2129666875 * L_2 = __this->get_array2_12();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = FsmArray_get_Values_m1362031434(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0021;
		}
	}

IL_0020:
	{
		return;
	}

IL_0021:
	{
		FsmBool_t1075959796 * L_4 = __this->get_storeResult_15();
		FsmArray_t2129666875 * L_5 = __this->get_array1_11();
		NullCheck(L_5);
		ObjectU5BU5D_t1108656482* L_6 = FsmArray_get_Values_m1362031434(L_5, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_7 = __this->get_array2_12();
		NullCheck(L_7);
		ObjectU5BU5D_t1108656482* L_8 = FsmArray_get_Values_m1362031434(L_7, /*hidden argument*/NULL);
		bool L_9 = Enumerable_SequenceEqual_TisIl2CppObject_m1941331891(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_6, (Il2CppObject*)(Il2CppObject*)L_8, /*hidden argument*/Enumerable_SequenceEqual_TisIl2CppObject_m1941331891_MethodInfo_var);
		NullCheck(L_4);
		FsmBool_set_Value_m1126216340(L_4, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_11 = __this->get_storeResult_15();
		NullCheck(L_11);
		bool L_12 = FsmBool_get_Value_m3101329097(L_11, /*hidden argument*/NULL);
		G_B4_0 = L_10;
		if (!L_12)
		{
			G_B5_0 = L_10;
			goto IL_0068;
		}
	}
	{
		FsmEvent_t2133468028 * L_13 = __this->get_SequenceEqual_13();
		G_B6_0 = L_13;
		G_B6_1 = G_B4_0;
		goto IL_006e;
	}

IL_0068:
	{
		FsmEvent_t2133468028 * L_14 = __this->get_SequenceNotEqual_14();
		G_B6_0 = L_14;
		G_B6_1 = G_B5_0;
	}

IL_006e:
	{
		NullCheck(G_B6_1);
		Fsm_Event_m625948263(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::.ctor()
extern "C"  void ArrayContains__ctor_m182478480 (ArrayContains_t1294114150 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::Reset()
extern "C"  void ArrayContains_Reset_m2123878717 (ArrayContains_t1294114150 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_value_12((FsmVar_t1596150537 *)NULL);
		__this->set_index_13((FsmInt_t1596138449 *)NULL);
		__this->set_isContained_14((FsmBool_t1075959796 *)NULL);
		__this->set_isContainedEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_isNotContainedEvent_16((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::OnEnter()
extern "C"  void ArrayContains_OnEnter_m2954856039 (ArrayContains_t1294114150 * __this, const MethodInfo* method)
{
	{
		ArrayContains_DoCheckContainsValue_m2813519817(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::DoCheckContainsValue()
extern const MethodInfo* Array_IndexOf_TisIl2CppObject_m2661005505_MethodInfo_var;
extern const uint32_t ArrayContains_DoCheckContainsValue_m2813519817_MetadataUsageId;
extern "C"  void ArrayContains_DoCheckContainsValue_m2813519817 (ArrayContains_t1294114150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayContains_DoCheckContainsValue_m2813519817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		FsmVar_t1596150537 * L_0 = __this->get_value_12();
		NullCheck(L_0);
		FsmVar_UpdateValue_m1579759472(L_0, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_1 = __this->get_array_11();
		NullCheck(L_1);
		ObjectU5BU5D_t1108656482* L_2 = FsmArray_get_Values_m1362031434(L_1, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_3 = __this->get_value_12();
		NullCheck(L_3);
		Il2CppObject * L_4 = FsmVar_GetValue_m2309870922(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Array_IndexOf_TisIl2CppObject_m2661005505(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/Array_IndexOf_TisIl2CppObject_m2661005505_MethodInfo_var);
		V_0 = L_5;
		int32_t L_6 = V_0;
		V_1 = (bool)((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		FsmBool_t1075959796 * L_7 = __this->get_isContained_14();
		bool L_8 = V_1;
		NullCheck(L_7);
		FsmBool_set_Value_m1126216340(L_7, L_8, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_9 = __this->get_index_13();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		FsmInt_set_Value_m2087583461(L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = V_1;
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_isContainedEvent_15();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_0063:
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_isNotContainedEvent_16();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::.ctor()
extern "C"  void ArrayDeleteAt__ctor_m4180336721 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::Reset()
extern "C"  void ArrayDeleteAt_Reset_m1826769662 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_index_12((FsmInt_t1596138449 *)NULL);
		__this->set_indexOutOfRangeEvent_13((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::OnEnter()
extern "C"  void ArrayDeleteAt_OnEnter_m900895720 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method)
{
	{
		ArrayDeleteAt_DoDeleteAt_m3047409660(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::DoDeleteAt()
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1384868247_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m4092449316_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1046025517_MethodInfo_var;
extern const uint32_t ArrayDeleteAt_DoDeleteAt_m3047409660_MetadataUsageId;
extern "C"  void ArrayDeleteAt_DoDeleteAt_m3047409660 (ArrayDeleteAt_t2681271941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayDeleteAt_DoDeleteAt_m3047409660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1244034627 * V_0 = NULL;
	{
		FsmInt_t1596138449 * L_0 = __this->get_index_12();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0064;
		}
	}
	{
		FsmInt_t1596138449 * L_2 = __this->get_index_12();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		NullCheck(L_4);
		int32_t L_5 = FsmArray_get_Length_m3299442701(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_0064;
		}
	}
	{
		FsmArray_t2129666875 * L_6 = __this->get_array_11();
		NullCheck(L_6);
		ObjectU5BU5D_t1108656482* L_7 = FsmArray_get_Values_m1362031434(L_6, /*hidden argument*/NULL);
		List_1_t1244034627 * L_8 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_8, (Il2CppObject*)(Il2CppObject*)L_7, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_0 = L_8;
		List_1_t1244034627 * L_9 = V_0;
		FsmInt_t1596138449 * L_10 = __this->get_index_12();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_RemoveAt_m4092449316(L_9, L_11, /*hidden argument*/List_1_RemoveAt_m4092449316_MethodInfo_var);
		FsmArray_t2129666875 * L_12 = __this->get_array_11();
		List_1_t1244034627 * L_13 = V_0;
		NullCheck(L_13);
		ObjectU5BU5D_t1108656482* L_14 = List_1_ToArray_m1046025517(L_13, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_12);
		FsmArray_set_Values_m2547392359(L_12, L_14, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_0064:
	{
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_16 = __this->get_indexOutOfRangeEvent_13();
		NullCheck(L_15);
		Fsm_Event_m625948263(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::.ctor()
extern Il2CppClass* FsmTemplateControl_t2786508133_il2cpp_TypeInfo_var;
extern const uint32_t ArrayForEach__ctor_m4164292237_MetadataUsageId;
extern "C"  void ArrayForEach__ctor_m4164292237 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayForEach__ctor_m4164292237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmTemplateControl_t2786508133 * L_0 = (FsmTemplateControl_t2786508133 *)il2cpp_codegen_object_new(FsmTemplateControl_t2786508133_il2cpp_TypeInfo_var);
		FsmTemplateControl__ctor_m4040770958(L_0, /*hidden argument*/NULL);
		__this->set_fsmTemplateControl_14(L_0);
		RunFSMAction__ctor_m530454611(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Reset()
extern Il2CppClass* FsmTemplateControl_t2786508133_il2cpp_TypeInfo_var;
extern const uint32_t ArrayForEach_Reset_m1810725178_MetadataUsageId;
extern "C"  void ArrayForEach_Reset_m1810725178 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayForEach_Reset_m1810725178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_array_12((FsmArray_t2129666875 *)NULL);
		FsmTemplateControl_t2786508133 * L_0 = (FsmTemplateControl_t2786508133 *)il2cpp_codegen_object_new(FsmTemplateControl_t2786508133_il2cpp_TypeInfo_var);
		FsmTemplateControl__ctor_m4040770958(L_0, /*hidden argument*/NULL);
		__this->set_fsmTemplateControl_14(L_0);
		((RunFSMAction_t3707651955 *)__this)->set_runFsm_11((Fsm_t1527112426 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Awake()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t ArrayForEach_Awake_m106930160_MetadataUsageId;
extern "C"  void ArrayForEach_Awake_m106930160 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayForEach_Awake_m106930160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_12();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		FsmTemplateControl_t2786508133 * L_1 = __this->get_fsmTemplateControl_14();
		NullCheck(L_1);
		FsmTemplate_t1237263802 * L_2 = L_1->get_fsmTemplate_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		bool L_4 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		Fsm_t1527112426 * L_5 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmTemplateControl_t2786508133 * L_6 = __this->get_fsmTemplateControl_14();
		NullCheck(L_5);
		Fsm_t1527112426 * L_7 = Fsm_CreateSubFsm_m1210636381(L_5, L_6, /*hidden argument*/NULL);
		((RunFSMAction_t3707651955 *)__this)->set_runFsm_11(L_7);
	}

IL_0042:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnEnter()
extern "C"  void ArrayForEach_OnEnter_m2662015780 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_12();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Fsm_t1527112426 * L_1 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		__this->set_currentIndex_16(0);
		ArrayForEach_StartFsm_m2952795157(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnUpdate()
extern "C"  void ArrayForEach_OnUpdate_m51669919 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_0);
		Fsm_Update_m2646905976(L_0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_1 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_1);
		bool L_2 = Fsm_get_Finished_m2204445938(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		ArrayForEach_StartNextFsm_m2285137634(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnFixedUpdate()
extern "C"  void ArrayForEach_OnFixedUpdate_m996528169 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_0);
		Fsm_LateUpdate_m4198981182(L_0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_1 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_1);
		bool L_2 = Fsm_get_Finished_m2204445938(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		ArrayForEach_StartNextFsm_m2285137634(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnLateUpdate()
extern "C"  void ArrayForEach_OnLateUpdate_m4160309733 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_0);
		Fsm_LateUpdate_m4198981182(L_0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_1 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_1);
		bool L_2 = Fsm_get_Finished_m2204445938(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		ArrayForEach_StartNextFsm_m2285137634(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartNextFsm()
extern "C"  void ArrayForEach_StartNextFsm_m2285137634 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentIndex_16();
		__this->set_currentIndex_16(((int32_t)((int32_t)L_0+(int32_t)1)));
		ArrayForEach_StartFsm_m2952795157(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartFsm()
extern "C"  void ArrayForEach_StartFsm_m2952795157 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		goto IL_002a;
	}

IL_0005:
	{
		ArrayForEach_DoStartFsm_m2189303168(__this, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_0 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m2204445938(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		int32_t L_2 = __this->get_currentIndex_16();
		__this->set_currentIndex_16(((int32_t)((int32_t)L_2+(int32_t)1)));
	}

IL_002a:
	{
		int32_t L_3 = __this->get_currentIndex_16();
		FsmArray_t2129666875 * L_4 = __this->get_array_12();
		NullCheck(L_4);
		int32_t L_5 = FsmArray_get_Length_m3299442701(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0005;
		}
	}
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_finishEvent_15();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::DoStartFsm()
extern "C"  void ArrayForEach_DoStartFsm_m2189303168 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		FsmVar_t1596150537 * L_0 = __this->get_storeItem_13();
		FsmArray_t2129666875 * L_1 = __this->get_array_12();
		NullCheck(L_1);
		ObjectU5BU5D_t1108656482* L_2 = FsmArray_get_Values_m1362031434(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentIndex_16();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		FsmVar_SetValue_m2004058539(L_0, L_5, /*hidden argument*/NULL);
		FsmTemplateControl_t2786508133 * L_6 = __this->get_fsmTemplateControl_14();
		NullCheck(L_6);
		FsmTemplateControl_UpdateValues_m2558089601(L_6, /*hidden argument*/NULL);
		FsmTemplateControl_t2786508133 * L_7 = __this->get_fsmTemplateControl_14();
		Fsm_t1527112426 * L_8 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_7);
		FsmTemplateControl_ApplyOverrides_m1401694351(L_7, L_8, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_9 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_9);
		Fsm_OnEnable_m1470487089(L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_10);
		bool L_11 = Fsm_get_Started_m2754310499(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005f;
		}
	}
	{
		Fsm_t1527112426 * L_12 = ((RunFSMAction_t3707651955 *)__this)->get_runFsm_11();
		NullCheck(L_12);
		Fsm_Start_m1193573941(L_12, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::CheckIfFinished()
extern "C"  void ArrayForEach_CheckIfFinished_m172419394 (ArrayForEach_t1480558585 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGet::.ctor()
extern "C"  void ArrayGet__ctor_m2738837793 (ArrayGet_t2749469413 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGet::Reset()
extern "C"  void ArrayGet_Reset_m385270734 (ArrayGet_t2749469413 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_index_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_14((bool)0);
		__this->set_storeValue_13((FsmVar_t1596150537 *)NULL);
		__this->set_indexOutOfRange_15((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGet::OnEnter()
extern "C"  void ArrayGet_OnEnter_m2894862520 (ArrayGet_t2749469413 * __this, const MethodInfo* method)
{
	{
		ArrayGet_DoGetValue_m1124109929(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGet::OnUpdate()
extern "C"  void ArrayGet_OnUpdate_m2974951563 (ArrayGet_t2749469413 * __this, const MethodInfo* method)
{
	{
		ArrayGet_DoGetValue_m1124109929(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGet::DoGetValue()
extern "C"  void ArrayGet_DoGetValue_m1124109929 (ArrayGet_t2749469413 * __this, const MethodInfo* method)
{
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		FsmVar_t1596150537 * L_2 = __this->get_storeValue_13();
		NullCheck(L_2);
		bool L_3 = FsmVar_get_IsNone_m3892161437(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0020:
	{
		return;
	}

IL_0021:
	{
		FsmInt_t1596138449 * L_4 = __this->get_index_12();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		FsmInt_t1596138449 * L_6 = __this->get_index_12();
		NullCheck(L_6);
		int32_t L_7 = FsmInt_get_Value_m27059446(L_6, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_8 = __this->get_array_11();
		NullCheck(L_8);
		int32_t L_9 = FsmArray_get_Length_m3299442701(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_0073;
		}
	}
	{
		FsmVar_t1596150537 * L_10 = __this->get_storeValue_13();
		FsmArray_t2129666875 * L_11 = __this->get_array_11();
		FsmInt_t1596138449 * L_12 = __this->get_index_12();
		NullCheck(L_12);
		int32_t L_13 = FsmInt_get_Value_m27059446(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Il2CppObject * L_14 = FsmArray_Get_m160560104(L_11, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmVar_SetValue_m2004058539(L_10, L_14, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_0073:
	{
		Fsm_t1527112426 * L_15 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_16 = __this->get_indexOutOfRange_15();
		NullCheck(L_15);
		Fsm_Event_m625948263(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::.ctor()
extern "C"  void ArrayGetNext__ctor_m1183260782 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::Reset()
extern "C"  void ArrayGetNext_Reset_m3124661019 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_startIndex_12((FsmInt_t1596138449 *)NULL);
		__this->set_endIndex_13((FsmInt_t1596138449 *)NULL);
		__this->set_currentIndex_17((FsmInt_t1596138449 *)NULL);
		__this->set_loopEvent_14((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_15((FsmEvent_t2133468028 *)NULL);
		__this->set_result_16((FsmVar_t1596150537 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::OnEnter()
extern "C"  void ArrayGetNext_OnEnter_m2633973957 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nextItemIndex_18();
		if (L_0)
		{
			goto IL_002d;
		}
	}
	{
		FsmInt_t1596138449 * L_1 = __this->get_startIndex_12();
		NullCheck(L_1);
		int32_t L_2 = FsmInt_get_Value_m27059446(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		FsmInt_t1596138449 * L_3 = __this->get_startIndex_12();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		__this->set_nextItemIndex_18(L_4);
	}

IL_002d:
	{
		ArrayGetNext_DoGetNextItem_m2178209501(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::DoGetNextItem()
extern "C"  void ArrayGetNext_DoGetNextItem_m2178209501 (ArrayGetNext_t2083890424 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nextItemIndex_18();
		FsmArray_t2129666875 * L_1 = __this->get_array_11();
		NullCheck(L_1);
		int32_t L_2 = FsmArray_get_Length_m3299442701(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0047;
		}
	}
	{
		__this->set_nextItemIndex_18(0);
		FsmInt_t1596138449 * L_3 = __this->get_currentIndex_17();
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		NullCheck(L_4);
		int32_t L_5 = FsmArray_get_Length_m3299442701(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_finishedEvent_15();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
		return;
	}

IL_0047:
	{
		FsmVar_t1596150537 * L_8 = __this->get_result_16();
		FsmArray_t2129666875 * L_9 = __this->get_array_11();
		int32_t L_10 = __this->get_nextItemIndex_18();
		NullCheck(L_9);
		Il2CppObject * L_11 = FsmArray_Get_m160560104(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmVar_SetValue_m2004058539(L_8, L_11, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_nextItemIndex_18();
		FsmArray_t2129666875 * L_13 = __this->get_array_11();
		NullCheck(L_13);
		int32_t L_14 = FsmArray_get_Length_m3299442701(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_00aa;
		}
	}
	{
		__this->set_nextItemIndex_18(0);
		FsmInt_t1596138449 * L_15 = __this->get_currentIndex_17();
		FsmArray_t2129666875 * L_16 = __this->get_array_11();
		NullCheck(L_16);
		int32_t L_17 = FsmArray_get_Length_m3299442701(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmInt_set_Value_m2087583461(L_15, ((int32_t)((int32_t)L_17-(int32_t)1)), /*hidden argument*/NULL);
		Fsm_t1527112426 * L_18 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_19 = __this->get_finishedEvent_15();
		NullCheck(L_18);
		Fsm_Event_m625948263(L_18, L_19, /*hidden argument*/NULL);
		return;
	}

IL_00aa:
	{
		FsmInt_t1596138449 * L_20 = __this->get_endIndex_13();
		NullCheck(L_20);
		int32_t L_21 = FsmInt_get_Value_m27059446(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0100;
		}
	}
	{
		int32_t L_22 = __this->get_nextItemIndex_18();
		FsmInt_t1596138449 * L_23 = __this->get_endIndex_13();
		NullCheck(L_23);
		int32_t L_24 = FsmInt_get_Value_m27059446(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0100;
		}
	}
	{
		__this->set_nextItemIndex_18(0);
		FsmInt_t1596138449 * L_25 = __this->get_currentIndex_17();
		FsmInt_t1596138449 * L_26 = __this->get_endIndex_13();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmInt_set_Value_m2087583461(L_25, L_27, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_28 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_29 = __this->get_finishedEvent_15();
		NullCheck(L_28);
		Fsm_Event_m625948263(L_28, L_29, /*hidden argument*/NULL);
		return;
	}

IL_0100:
	{
		int32_t L_30 = __this->get_nextItemIndex_18();
		__this->set_nextItemIndex_18(((int32_t)((int32_t)L_30+(int32_t)1)));
		FsmInt_t1596138449 * L_31 = __this->get_currentIndex_17();
		int32_t L_32 = __this->get_nextItemIndex_18();
		NullCheck(L_31);
		FsmInt_set_Value_m2087583461(L_31, ((int32_t)((int32_t)L_32-(int32_t)1)), /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_33 = __this->get_loopEvent_14();
		if (!L_33)
		{
			goto IL_013d;
		}
	}
	{
		Fsm_t1527112426 * L_34 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_35 = __this->get_loopEvent_14();
		NullCheck(L_34);
		Fsm_Event_m625948263(L_34, L_35, /*hidden argument*/NULL);
	}

IL_013d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::.ctor()
extern "C"  void ArrayGetRandom__ctor_m401434910 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::Reset()
extern "C"  void ArrayGetRandom_Reset_m2342835147 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_storeValue_12((FsmVar_t1596150537 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::OnEnter()
extern "C"  void ArrayGetRandom_OnEnter_m2918587765 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method)
{
	{
		ArrayGetRandom_DoGetRandomValue_m3649088201(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::OnUpdate()
extern "C"  void ArrayGetRandom_OnUpdate_m3710434158 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method)
{
	{
		ArrayGetRandom_DoGetRandomValue_m3649088201(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::DoGetRandomValue()
extern "C"  void ArrayGetRandom_DoGetRandomValue_m3649088201 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method)
{
	{
		FsmVar_t1596150537 * L_0 = __this->get_storeValue_12();
		NullCheck(L_0);
		bool L_1 = FsmVar_get_IsNone_m3892161437(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmVar_t1596150537 * L_2 = __this->get_storeValue_12();
		FsmArray_t2129666875 * L_3 = __this->get_array_11();
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		NullCheck(L_4);
		int32_t L_5 = FsmArray_get_Length_m3299442701(L_4, /*hidden argument*/NULL);
		int32_t L_6 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject * L_7 = FsmArray_Get_m160560104(L_3, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmVar_SetValue_m2004058539(L_2, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::.ctor()
extern "C"  void ArrayLength__ctor_m370341033 (ArrayLength_t92600621 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::Reset()
extern "C"  void ArrayLength_Reset_m2311741270 (ArrayLength_t92600621 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_length_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnEnter()
extern "C"  void ArrayLength_OnEnter_m3102143040 (ArrayLength_t92600621 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_length_12();
		FsmArray_t2129666875 * L_1 = __this->get_array_11();
		NullCheck(L_1);
		int32_t L_2 = FsmArray_get_Length_m3299442701(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_everyFrame_13();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnUpdate()
extern "C"  void ArrayLength_OnUpdate_m810713091 (ArrayLength_t92600621 * __this, const MethodInfo* method)
{
	{
		FsmInt_t1596138449 * L_0 = __this->get_length_12();
		FsmArray_t2129666875 * L_1 = __this->get_array_11();
		NullCheck(L_1);
		int32_t L_2 = FsmArray_get_Length_m3299442701(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayResize::.ctor()
extern "C"  void ArrayResize__ctor_m2309601947 (ArrayResize_t264526587 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayResize::OnEnter()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral951026123;
extern const uint32_t ArrayResize_OnEnter_m2716074930_MetadataUsageId;
extern "C"  void ArrayResize_OnEnter_m2716074930 (ArrayResize_t264526587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayResize_OnEnter_m2716074930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = __this->get_newSize_12();
		NullCheck(L_0);
		int32_t L_1 = FsmInt_get_Value_m27059446(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		FsmArray_t2129666875 * L_2 = __this->get_array_11();
		FsmInt_t1596138449 * L_3 = __this->get_newSize_12();
		NullCheck(L_3);
		int32_t L_4 = FsmInt_get_Value_m27059446(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmArray_Resize_m3367576465(L_2, L_4, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_002c:
	{
		FsmInt_t1596138449 * L_5 = __this->get_newSize_12();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral951026123, L_8, /*hidden argument*/NULL);
		FsmStateAction_LogError_m3478223492(__this, L_9, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_sizeOutOfRangeEvent_13();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
	}

IL_005d:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::.ctor()
extern "C"  void ArrayReverse__ctor_m1418533141 (ArrayReverse_t3259040881 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::Reset()
extern "C"  void ArrayReverse_Reset_m3359933378 (ArrayReverse_t3259040881 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::OnEnter()
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1384868247_MethodInfo_var;
extern const MethodInfo* List_1_Reverse_m2062055293_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1046025517_MethodInfo_var;
extern const uint32_t ArrayReverse_OnEnter_m1097444268_MetadataUsageId;
extern "C"  void ArrayReverse_OnEnter_m1097444268 (ArrayReverse_t3259040881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReverse_OnEnter_m1097444268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1244034627 * V_0 = NULL;
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_1 = FsmArray_get_Values_m1362031434(L_0, /*hidden argument*/NULL);
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_2, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_0 = L_2;
		List_1_t1244034627 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Reverse_m2062055293(L_3, /*hidden argument*/List_1_Reverse_m2062055293_MethodInfo_var);
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		List_1_t1244034627 * L_5 = V_0;
		NullCheck(L_5);
		ObjectU5BU5D_t1108656482* L_6 = List_1_ToArray_m1046025517(L_5, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_4);
		FsmArray_set_Values_m2547392359(L_4, L_6, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySet::.ctor()
extern "C"  void ArraySet__ctor_m4287830421 (ArraySet_t2749480945 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySet::Reset()
extern "C"  void ArraySet_Reset_m1934263362 (ArraySet_t2749480945 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		__this->set_index_12((FsmInt_t1596138449 *)NULL);
		__this->set_value_13((FsmVar_t1596150537 *)NULL);
		__this->set_everyFrame_14((bool)0);
		__this->set_indexOutOfRange_15((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnEnter()
extern "C"  void ArraySet_OnEnter_m1123126316 (ArraySet_t2749480945 * __this, const MethodInfo* method)
{
	{
		ArraySet_DoGetValue_m183957109(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnUpdate()
extern "C"  void ArraySet_OnUpdate_m3885704087 (ArraySet_t2749480945 * __this, const MethodInfo* method)
{
	{
		ArraySet_DoGetValue_m183957109(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySet::DoGetValue()
extern "C"  void ArraySet_DoGetValue_m183957109 (ArraySet_t2749480945 * __this, const MethodInfo* method)
{
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmInt_t1596138449 * L_2 = __this->get_index_12();
		NullCheck(L_2);
		int32_t L_3 = FsmInt_get_Value_m27059446(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		FsmInt_t1596138449 * L_4 = __this->get_index_12();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_6 = __this->get_array_11();
		NullCheck(L_6);
		int32_t L_7 = FsmArray_get_Length_m3299442701(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)L_7)))
		{
			goto IL_006e;
		}
	}
	{
		FsmVar_t1596150537 * L_8 = __this->get_value_13();
		NullCheck(L_8);
		FsmVar_UpdateValue_m1579759472(L_8, /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_9 = __this->get_array_11();
		FsmInt_t1596138449 * L_10 = __this->get_index_12();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_12 = __this->get_value_13();
		NullCheck(L_12);
		Il2CppObject * L_13 = FsmVar_GetValue_m2309870922(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmArray_Set_m410178455(L_9, L_11, L_13, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_006e:
	{
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_15 = __this->get_indexOutOfRange_15();
		NullCheck(L_14);
		Fsm_Event_m625948263(L_14, L_15, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::.ctor()
extern "C"  void ArrayShuffle__ctor_m3787575006 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::Reset()
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern const uint32_t ArrayShuffle_Reset_m1434007947_MetadataUsageId;
extern "C"  void ArrayShuffle_Reset_m1434007947 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayShuffle_Reset_m1434007947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmInt_t1596138449 * V_0 = NULL;
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		FsmInt_t1596138449 * L_0 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmInt_t1596138449 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = V_0;
		__this->set_startIndex_12(L_2);
		FsmInt_t1596138449 * L_3 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmInt_t1596138449 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = V_0;
		__this->set_shufflingRange_13(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::OnEnter()
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1384868247_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2222238344_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m850128002_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m2510829103_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1046025517_MethodInfo_var;
extern const uint32_t ArrayShuffle_OnEnter_m1414009653_MetadataUsageId;
extern "C"  void ArrayShuffle_OnEnter_m1414009653 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayShuffle_OnEnter_m1414009653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1244034627 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppObject * V_5 = NULL;
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_1 = FsmArray_get_Values_m1362031434(L_0, /*hidden argument*/NULL);
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_2, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_0 = L_2;
		V_1 = 0;
		List_1_t1244034627 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2222238344(L_3, /*hidden argument*/List_1_get_Count_m2222238344_MethodInfo_var);
		V_2 = ((int32_t)((int32_t)L_4-(int32_t)1));
		FsmInt_t1596138449 * L_5 = __this->get_startIndex_12();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		FsmInt_t1596138449 * L_7 = __this->get_startIndex_12();
		NullCheck(L_7);
		int32_t L_8 = FsmInt_get_Value_m27059446(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Min_m2413438171(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_003f:
	{
		FsmInt_t1596138449 * L_11 = __this->get_shufflingRange_13();
		NullCheck(L_11);
		int32_t L_12 = FsmInt_get_Value_m27059446(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_006b;
		}
	}
	{
		List_1_t1244034627 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2222238344(L_13, /*hidden argument*/List_1_get_Count_m2222238344_MethodInfo_var);
		int32_t L_15 = V_1;
		FsmInt_t1596138449 * L_16 = __this->get_shufflingRange_13();
		NullCheck(L_16);
		int32_t L_17 = FsmInt_get_Value_m27059446(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_18 = Mathf_Min_m2413438171(NULL /*static, unused*/, ((int32_t)((int32_t)L_14-(int32_t)1)), ((int32_t)((int32_t)L_15+(int32_t)L_17)), /*hidden argument*/NULL);
		V_2 = L_18;
	}

IL_006b:
	{
		int32_t L_19 = V_2;
		V_3 = L_19;
		goto IL_00a3;
	}

IL_0072:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = V_3;
		int32_t L_22 = Random_Range_m75452833(NULL /*static, unused*/, L_20, ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_22;
		List_1_t1244034627 * L_23 = V_0;
		int32_t L_24 = V_3;
		NullCheck(L_23);
		Il2CppObject * L_25 = List_1_get_Item_m850128002(L_23, L_24, /*hidden argument*/List_1_get_Item_m850128002_MethodInfo_var);
		V_5 = L_25;
		List_1_t1244034627 * L_26 = V_0;
		int32_t L_27 = V_3;
		List_1_t1244034627 * L_28 = V_0;
		int32_t L_29 = V_4;
		NullCheck(L_28);
		Il2CppObject * L_30 = List_1_get_Item_m850128002(L_28, L_29, /*hidden argument*/List_1_get_Item_m850128002_MethodInfo_var);
		NullCheck(L_26);
		List_1_set_Item_m2510829103(L_26, L_27, L_30, /*hidden argument*/List_1_set_Item_m2510829103_MethodInfo_var);
		List_1_t1244034627 * L_31 = V_0;
		int32_t L_32 = V_4;
		Il2CppObject * L_33 = V_5;
		NullCheck(L_31);
		List_1_set_Item_m2510829103(L_31, L_32, L_33, /*hidden argument*/List_1_set_Item_m2510829103_MethodInfo_var);
		int32_t L_34 = V_3;
		V_3 = ((int32_t)((int32_t)L_34-(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_35 = V_3;
		int32_t L_36 = V_1;
		if ((((int32_t)L_35) > ((int32_t)L_36)))
		{
			goto IL_0072;
		}
	}
	{
		FsmArray_t2129666875 * L_37 = __this->get_array_11();
		List_1_t1244034627 * L_38 = V_0;
		NullCheck(L_38);
		ObjectU5BU5D_t1108656482* L_39 = List_1_ToArray_m1046025517(L_38, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_37);
		FsmArray_set_Values_m2547392359(L_37, L_39, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySort::.ctor()
extern "C"  void ArraySort__ctor_m1019374577 (ArraySort_t2980580069 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySort::Reset()
extern "C"  void ArraySort_Reset_m2960774814 (ArraySort_t2980580069 * __this, const MethodInfo* method)
{
	{
		__this->set_array_11((FsmArray_t2129666875 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArraySort::OnEnter()
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1384868247_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m3444130117_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1046025517_MethodInfo_var;
extern const uint32_t ArraySort_OnEnter_m4053120904_MetadataUsageId;
extern "C"  void ArraySort_OnEnter_m4053120904 (ArraySort_t2980580069 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArraySort_OnEnter_m4053120904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1244034627 * V_0 = NULL;
	{
		FsmArray_t2129666875 * L_0 = __this->get_array_11();
		NullCheck(L_0);
		ObjectU5BU5D_t1108656482* L_1 = FsmArray_get_Values_m1362031434(L_0, /*hidden argument*/NULL);
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_2, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_0 = L_2;
		List_1_t1244034627 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Sort_m3444130117(L_3, /*hidden argument*/List_1_Sort_m3444130117_MethodInfo_var);
		FsmArray_t2129666875 * L_4 = __this->get_array_11();
		List_1_t1244034627 * L_5 = V_0;
		NullCheck(L_5);
		ObjectU5BU5D_t1108656482* L_6 = List_1_ToArray_m1046025517(L_5, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_4);
		FsmArray_set_Values_m2547392359(L_4, L_6, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::.ctor()
extern "C"  void ArrayTransferValue__ctor_m661971249 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::Reset()
extern Il2CppClass* ArrayTransferType_t2700806265_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayPasteType_t3581903417_il2cpp_TypeInfo_var;
extern const uint32_t ArrayTransferValue_Reset_m2603371486_MetadataUsageId;
extern "C"  void ArrayTransferValue_Reset_m2603371486 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayTransferValue_Reset_m2603371486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_arraySource_11((FsmArray_t2129666875 *)NULL);
		__this->set_arrayTarget_12((FsmArray_t2129666875 *)NULL);
		__this->set_indexToTransfer_13((FsmInt_t1596138449 *)NULL);
		int32_t L_0 = ((int32_t)0);
		Il2CppObject * L_1 = Box(ArrayTransferType_t2700806265_il2cpp_TypeInfo_var, &L_0);
		FsmEnum_t1076048395 * L_2 = FsmEnum_op_Implicit_m4146730367(NULL /*static, unused*/, (Enum_t2862688501 *)L_1, /*hidden argument*/NULL);
		__this->set_copyType_14(L_2);
		int32_t L_3 = ((int32_t)1);
		Il2CppObject * L_4 = Box(ArrayPasteType_t3581903417_il2cpp_TypeInfo_var, &L_3);
		FsmEnum_t1076048395 * L_5 = FsmEnum_op_Implicit_m4146730367(NULL /*static, unused*/, (Enum_t2862688501 *)L_4, /*hidden argument*/NULL);
		__this->set_pasteType_15(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::OnEnter()
extern "C"  void ArrayTransferValue_OnEnter_m4185906376 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method)
{
	{
		ArrayTransferValue_DoTransferValue_m2826718090(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::DoTransferValue()
extern Il2CppClass* ArrayTransferType_t2700806265_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1244034627_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayPasteType_t3581903417_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1384868247_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m4092449316_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1046025517_MethodInfo_var;
extern const MethodInfo* List_1_Insert_m3135247846_MethodInfo_var;
extern const uint32_t ArrayTransferValue_DoTransferValue_m2826718090_MetadataUsageId;
extern "C"  void ArrayTransferValue_DoTransferValue_m2826718090 (ArrayTransferValue_t3288840917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayTransferValue_DoTransferValue_m2826718090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	List_1_t1244034627 * V_2 = NULL;
	List_1_t1244034627 * V_3 = NULL;
	List_1_t1244034627 * V_4 = NULL;
	{
		FsmArray_t2129666875 * L_0 = __this->get_arraySource_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		FsmArray_t2129666875 * L_2 = __this->get_arrayTarget_12();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}

IL_0020:
	{
		return;
	}

IL_0021:
	{
		FsmInt_t1596138449 * L_4 = __this->get_indexToTransfer_13();
		NullCheck(L_4);
		int32_t L_5 = FsmInt_get_Value_m27059446(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_7 = V_0;
		FsmArray_t2129666875 * L_8 = __this->get_arraySource_11();
		NullCheck(L_8);
		int32_t L_9 = FsmArray_get_Length_m3299442701(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0057;
		}
	}

IL_0045:
	{
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_indexOutOfRange_16();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		FsmArray_t2129666875 * L_12 = __this->get_arraySource_11();
		NullCheck(L_12);
		ObjectU5BU5D_t1108656482* L_13 = FsmArray_get_Values_m1362031434(L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_1 = L_16;
		FsmEnum_t1076048395 * L_17 = __this->get_copyType_14();
		NullCheck(L_17);
		Enum_t2862688501 * L_18 = FsmEnum_get_Value_m4041924749(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)((*(int32_t*)((int32_t*)UnBox (L_18, ArrayTransferType_t2700806265_il2cpp_TypeInfo_var))))) == ((uint32_t)1))))
		{
			goto IL_00a9;
		}
	}
	{
		FsmArray_t2129666875 * L_19 = __this->get_arraySource_11();
		NullCheck(L_19);
		ObjectU5BU5D_t1108656482* L_20 = FsmArray_get_Values_m1362031434(L_19, /*hidden argument*/NULL);
		List_1_t1244034627 * L_21 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_21, (Il2CppObject*)(Il2CppObject*)L_20, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_2 = L_21;
		List_1_t1244034627 * L_22 = V_2;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		List_1_RemoveAt_m4092449316(L_22, L_23, /*hidden argument*/List_1_RemoveAt_m4092449316_MethodInfo_var);
		FsmArray_t2129666875 * L_24 = __this->get_arraySource_11();
		List_1_t1244034627 * L_25 = V_2;
		NullCheck(L_25);
		ObjectU5BU5D_t1108656482* L_26 = List_1_ToArray_m1046025517(L_25, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_24);
		FsmArray_set_Values_m2547392359(L_24, L_26, /*hidden argument*/NULL);
		goto IL_00d1;
	}

IL_00a9:
	{
		FsmEnum_t1076048395 * L_27 = __this->get_copyType_14();
		NullCheck(L_27);
		Enum_t2862688501 * L_28 = FsmEnum_get_Value_m4041924749(L_27, /*hidden argument*/NULL);
		if ((!(((uint32_t)((*(int32_t*)((int32_t*)UnBox (L_28, ArrayTransferType_t2700806265_il2cpp_TypeInfo_var))))) == ((uint32_t)2))))
		{
			goto IL_00d1;
		}
	}
	{
		FsmArray_t2129666875 * L_29 = __this->get_arraySource_11();
		NullCheck(L_29);
		ObjectU5BU5D_t1108656482* L_30 = FsmArray_get_Values_m1362031434(L_29, /*hidden argument*/NULL);
		int32_t L_31 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_30);
		Array_SetValue_m3564402974((Il2CppArray *)(Il2CppArray *)L_30, NULL, L_31, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		FsmEnum_t1076048395 * L_32 = __this->get_pasteType_15();
		NullCheck(L_32);
		Enum_t2862688501 * L_33 = FsmEnum_get_Value_m4041924749(L_32, /*hidden argument*/NULL);
		if (((*(int32_t*)((int32_t*)UnBox (L_33, ArrayPasteType_t3581903417_il2cpp_TypeInfo_var)))))
		{
			goto IL_0115;
		}
	}
	{
		FsmArray_t2129666875 * L_34 = __this->get_arrayTarget_12();
		NullCheck(L_34);
		ObjectU5BU5D_t1108656482* L_35 = FsmArray_get_Values_m1362031434(L_34, /*hidden argument*/NULL);
		List_1_t1244034627 * L_36 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_36, (Il2CppObject*)(Il2CppObject*)L_35, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_3 = L_36;
		List_1_t1244034627 * L_37 = V_3;
		Il2CppObject * L_38 = V_1;
		NullCheck(L_37);
		List_1_Insert_m3135247846(L_37, 0, L_38, /*hidden argument*/List_1_Insert_m3135247846_MethodInfo_var);
		FsmArray_t2129666875 * L_39 = __this->get_arrayTarget_12();
		List_1_t1244034627 * L_40 = V_3;
		NullCheck(L_40);
		ObjectU5BU5D_t1108656482* L_41 = List_1_ToArray_m1046025517(L_40, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_39);
		FsmArray_set_Values_m2547392359(L_39, L_41, /*hidden argument*/NULL);
		goto IL_0215;
	}

IL_0115:
	{
		FsmEnum_t1076048395 * L_42 = __this->get_pasteType_15();
		NullCheck(L_42);
		Enum_t2862688501 * L_43 = FsmEnum_get_Value_m4041924749(L_42, /*hidden argument*/NULL);
		if ((!(((uint32_t)((*(int32_t*)((int32_t*)UnBox (L_43, ArrayPasteType_t3581903417_il2cpp_TypeInfo_var))))) == ((uint32_t)1))))
		{
			goto IL_0161;
		}
	}
	{
		FsmArray_t2129666875 * L_44 = __this->get_arrayTarget_12();
		FsmArray_t2129666875 * L_45 = __this->get_arrayTarget_12();
		NullCheck(L_45);
		int32_t L_46 = FsmArray_get_Length_m3299442701(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		FsmArray_Resize_m3367576465(L_44, ((int32_t)((int32_t)L_46+(int32_t)1)), /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_47 = __this->get_arrayTarget_12();
		FsmArray_t2129666875 * L_48 = __this->get_arrayTarget_12();
		NullCheck(L_48);
		int32_t L_49 = FsmArray_get_Length_m3299442701(L_48, /*hidden argument*/NULL);
		Il2CppObject * L_50 = V_1;
		NullCheck(L_47);
		FsmArray_Set_m410178455(L_47, ((int32_t)((int32_t)L_49-(int32_t)1)), L_50, /*hidden argument*/NULL);
		goto IL_0215;
	}

IL_0161:
	{
		FsmEnum_t1076048395 * L_51 = __this->get_pasteType_15();
		NullCheck(L_51);
		Enum_t2862688501 * L_52 = FsmEnum_get_Value_m4041924749(L_51, /*hidden argument*/NULL);
		if ((!(((uint32_t)((*(int32_t*)((int32_t*)UnBox (L_52, ArrayPasteType_t3581903417_il2cpp_TypeInfo_var))))) == ((uint32_t)2))))
		{
			goto IL_01cb;
		}
	}
	{
		int32_t L_53 = V_0;
		FsmArray_t2129666875 * L_54 = __this->get_arrayTarget_12();
		NullCheck(L_54);
		int32_t L_55 = FsmArray_get_Length_m3299442701(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_53) < ((int32_t)L_55)))
		{
			goto IL_0199;
		}
	}
	{
		Fsm_t1527112426 * L_56 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_57 = __this->get_indexOutOfRange_16();
		NullCheck(L_56);
		Fsm_Event_m625948263(L_56, L_57, /*hidden argument*/NULL);
	}

IL_0199:
	{
		FsmArray_t2129666875 * L_58 = __this->get_arrayTarget_12();
		NullCheck(L_58);
		ObjectU5BU5D_t1108656482* L_59 = FsmArray_get_Values_m1362031434(L_58, /*hidden argument*/NULL);
		List_1_t1244034627 * L_60 = (List_1_t1244034627 *)il2cpp_codegen_object_new(List_1_t1244034627_il2cpp_TypeInfo_var);
		List_1__ctor_m1384868247(L_60, (Il2CppObject*)(Il2CppObject*)L_59, /*hidden argument*/List_1__ctor_m1384868247_MethodInfo_var);
		V_4 = L_60;
		List_1_t1244034627 * L_61 = V_4;
		int32_t L_62 = V_0;
		Il2CppObject * L_63 = V_1;
		NullCheck(L_61);
		List_1_Insert_m3135247846(L_61, L_62, L_63, /*hidden argument*/List_1_Insert_m3135247846_MethodInfo_var);
		FsmArray_t2129666875 * L_64 = __this->get_arrayTarget_12();
		List_1_t1244034627 * L_65 = V_4;
		NullCheck(L_65);
		ObjectU5BU5D_t1108656482* L_66 = List_1_ToArray_m1046025517(L_65, /*hidden argument*/List_1_ToArray_m1046025517_MethodInfo_var);
		NullCheck(L_64);
		FsmArray_set_Values_m2547392359(L_64, L_66, /*hidden argument*/NULL);
		goto IL_0215;
	}

IL_01cb:
	{
		FsmEnum_t1076048395 * L_67 = __this->get_pasteType_15();
		NullCheck(L_67);
		Enum_t2862688501 * L_68 = FsmEnum_get_Value_m4041924749(L_67, /*hidden argument*/NULL);
		if ((!(((uint32_t)((*(int32_t*)((int32_t*)UnBox (L_68, ArrayPasteType_t3581903417_il2cpp_TypeInfo_var))))) == ((uint32_t)3))))
		{
			goto IL_0215;
		}
	}
	{
		int32_t L_69 = V_0;
		FsmArray_t2129666875 * L_70 = __this->get_arrayTarget_12();
		NullCheck(L_70);
		int32_t L_71 = FsmArray_get_Length_m3299442701(L_70, /*hidden argument*/NULL);
		if ((((int32_t)L_69) < ((int32_t)L_71)))
		{
			goto IL_0208;
		}
	}
	{
		Fsm_t1527112426 * L_72 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_73 = __this->get_indexOutOfRange_16();
		NullCheck(L_72);
		Fsm_Event_m625948263(L_72, L_73, /*hidden argument*/NULL);
		goto IL_0215;
	}

IL_0208:
	{
		FsmArray_t2129666875 * L_74 = __this->get_arrayTarget_12();
		int32_t L_75 = V_0;
		Il2CppObject * L_76 = V_1;
		NullCheck(L_74);
		FsmArray_Set_m410178455(L_74, L_75, L_76, /*hidden argument*/NULL);
	}

IL_0215:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioMute::.ctor()
extern "C"  void AudioMute__ctor_m1166726009 (AudioMute_t298551901 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioMute::Reset()
extern "C"  void AudioMute_Reset_m3108126246 (AudioMute_t298551901 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_mute_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioMute::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t AudioMute_OnEnter_m3923926288_MetadataUsageId;
extern "C"  void AudioMute_OnEnter_m3923926288 (AudioMute_t298551901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioMute_OnEnter_m3923926288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AudioSource_t1740077639 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_5, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		V_1 = L_6;
		AudioSource_t1740077639 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		AudioSource_t1740077639 * L_9 = V_1;
		FsmBool_t1075959796 * L_10 = __this->get_mute_12();
		NullCheck(L_10);
		bool L_11 = FsmBool_get_Value_m3101329097(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		AudioSource_set_mute_m4040046601(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0042:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPause::.ctor()
extern "C"  void AudioPause__ctor_m1910342718 (AudioPause_t18390312 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPause::Reset()
extern "C"  void AudioPause_Reset_m3851742955 (AudioPause_t18390312 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPause::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t AudioPause_OnEnter_m1280045205_MetadataUsageId;
extern "C"  void AudioPause_OnEnter_m1280045205 (AudioPause_t18390312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioPause_OnEnter_m1280045205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AudioSource_t1740077639 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_5, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		V_1 = L_6;
		AudioSource_t1740077639 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0037;
		}
	}
	{
		AudioSource_t1740077639 * L_9 = V_1;
		NullCheck(L_9);
		AudioSource_Pause_m3226052732(L_9, /*hidden argument*/NULL);
	}

IL_0037:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::.ctor()
extern "C"  void AudioPlay__ctor_m3566774462 (AudioPlay_t298632056 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::Reset()
extern "C"  void AudioPlay_Reset_m1213207403 (AudioPlay_t298632056 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_volume_12(L_0);
		__this->set_oneShotClip_13((FsmObject_t821476169 *)NULL);
		__this->set_finishedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* AudioClip_t794140988_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t AudioPlay_OnEnter_m3973051669_MetadataUsageId;
extern "C"  void AudioPlay_OnEnter_m3973051669 (AudioPlay_t298632056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioPlay_OnEnter_m3973051669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AudioClip_t794140988 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00c3;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_5, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		__this->set_audio_15(L_6);
		AudioSource_t1740077639 * L_7 = __this->get_audio_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c3;
		}
	}
	{
		FsmObject_t821476169 * L_9 = __this->get_oneShotClip_13();
		NullCheck(L_9);
		Object_t3071478659 * L_10 = FsmObject_get_Value_m188501991(L_9, /*hidden argument*/NULL);
		V_1 = ((AudioClip_t794140988 *)IsInstSealed(L_10, AudioClip_t794140988_il2cpp_TypeInfo_var));
		AudioClip_t794140988 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_11, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008a;
		}
	}
	{
		AudioSource_t1740077639 * L_13 = __this->get_audio_15();
		NullCheck(L_13);
		AudioSource_Play_m1360558992(L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_volume_12();
		NullCheck(L_14);
		bool L_15 = NamedVariable_get_IsNone_m281035543(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0089;
		}
	}
	{
		AudioSource_t1740077639 * L_16 = __this->get_audio_15();
		FsmFloat_t2134102846 * L_17 = __this->get_volume_12();
		NullCheck(L_17);
		float L_18 = FsmFloat_get_Value_m4137923823(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		AudioSource_set_volume_m1410546616(L_16, L_18, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}

IL_008a:
	{
		FsmFloat_t2134102846 * L_19 = __this->get_volume_12();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b6;
		}
	}
	{
		AudioSource_t1740077639 * L_21 = __this->get_audio_15();
		AudioClip_t794140988 * L_22 = V_1;
		FsmFloat_t2134102846 * L_23 = __this->get_volume_12();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		AudioSource_PlayOneShot_m823779350(L_21, L_22, L_24, /*hidden argument*/NULL);
		goto IL_00c2;
	}

IL_00b6:
	{
		AudioSource_t1740077639 * L_25 = __this->get_audio_15();
		AudioClip_t794140988 * L_26 = V_1;
		NullCheck(L_25);
		AudioSource_PlayOneShot_m1217449713(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		return;
	}

IL_00c3:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnUpdate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t AudioPlay_OnUpdate_m2039076814_MetadataUsageId;
extern "C"  void AudioPlay_OnUpdate_m2039076814 (AudioPlay_t298632056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioPlay_OnUpdate_m2039076814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t1740077639 * L_0 = __this->get_audio_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_001c:
	{
		AudioSource_t1740077639 * L_2 = __this->get_audio_15();
		NullCheck(L_2);
		bool L_3 = AudioSource_get_isPlaying_m4213444423(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0048;
		}
	}
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_5 = __this->get_finishedEvent_14();
		NullCheck(L_4);
		Fsm_Event_m625948263(L_4, L_5, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0048:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_volume_12();
		NullCheck(L_6);
		bool L_7 = NamedVariable_get_IsNone_m281035543(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0089;
		}
	}
	{
		FsmFloat_t2134102846 * L_8 = __this->get_volume_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_10 = __this->get_audio_15();
		NullCheck(L_10);
		float L_11 = AudioSource_get_volume_m2334326995(L_10, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)L_11)))
		{
			goto IL_0089;
		}
	}
	{
		AudioSource_t1740077639 * L_12 = __this->get_audio_15();
		FsmFloat_t2134102846 * L_13 = __this->get_volume_12();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		AudioSource_set_volume_m1410546616(L_12, L_14, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioStop::.ctor()
extern "C"  void AudioStop__ctor_m1805366192 (AudioStop_t298729542 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioStop::Reset()
extern "C"  void AudioStop_Reset_m3746766429 (AudioStop_t298729542 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.AudioStop::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var;
extern const uint32_t AudioStop_OnEnter_m3476818823_MetadataUsageId;
extern "C"  void AudioStop_OnEnter_m3476818823 (AudioStop_t298729542 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioStop_OnEnter_m3476818823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	AudioSource_t1740077639 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151(L_5, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1740077639_m1155306151_MethodInfo_var);
		V_1 = L_6;
		AudioSource_t1740077639 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0037;
		}
	}
	{
		AudioSource_t1740077639 * L_9 = V_1;
		NullCheck(L_9);
		AudioSource_Stop_m1454243038(L_9, /*hidden argument*/NULL);
	}

IL_0037:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
