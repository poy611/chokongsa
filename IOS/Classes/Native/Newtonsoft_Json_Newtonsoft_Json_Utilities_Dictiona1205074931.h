﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>
struct U3CU3Ec_t1205074931;
// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Func_2_t1623933992;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<System.Object,System.Object>
struct  U3CU3Ec_t1205074931  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t1205074931_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c::<>9
	U3CU3Ec_t1205074931 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.DictionaryEntry,System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2/<>c::<>9__25_0
	Func_2_t1623933992 * ___U3CU3E9__25_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1205074931_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1205074931 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1205074931 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1205074931 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1205074931_StaticFields, ___U3CU3E9__25_0_1)); }
	inline Func_2_t1623933992 * get_U3CU3E9__25_0_1() const { return ___U3CU3E9__25_0_1; }
	inline Func_2_t1623933992 ** get_address_of_U3CU3E9__25_0_1() { return &___U3CU3E9__25_0_1; }
	inline void set_U3CU3E9__25_0_1(Func_2_t1623933992 * value)
	{
		___U3CU3E9__25_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__25_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
