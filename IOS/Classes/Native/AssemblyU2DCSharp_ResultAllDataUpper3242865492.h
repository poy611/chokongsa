﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ResultAllData>
struct List_1_t346166462;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultAllDataUpper
struct  ResultAllDataUpper_t3242865492  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<ResultAllData> ResultAllDataUpper::prctcInfoList
	List_1_t346166462 * ___prctcInfoList_0;

public:
	inline static int32_t get_offset_of_prctcInfoList_0() { return static_cast<int32_t>(offsetof(ResultAllDataUpper_t3242865492, ___prctcInfoList_0)); }
	inline List_1_t346166462 * get_prctcInfoList_0() const { return ___prctcInfoList_0; }
	inline List_1_t346166462 ** get_address_of_prctcInfoList_0() { return &___prctcInfoList_0; }
	inline void set_prctcInfoList_0(List_1_t346166462 * value)
	{
		___prctcInfoList_0 = value;
		Il2CppCodeGenWriteBarrier(&___prctcInfoList_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
