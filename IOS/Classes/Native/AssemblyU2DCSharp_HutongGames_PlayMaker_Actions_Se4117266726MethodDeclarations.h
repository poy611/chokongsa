﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGravity2d
struct SetGravity2d_t4117266726;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::.ctor()
extern "C"  void SetGravity2d__ctor_m134092800 (SetGravity2d_t4117266726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::Reset()
extern "C"  void SetGravity2d_Reset_m2075493037 (SetGravity2d_t4117266726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::OnEnter()
extern "C"  void SetGravity2d_OnEnter_m3700857815 (SetGravity2d_t4117266726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::OnUpdate()
extern "C"  void SetGravity2d_OnUpdate_m2191001932 (SetGravity2d_t4117266726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::DoSetGravity()
extern "C"  void SetGravity2d_DoSetGravity_m3648901883 (SetGravity2d_t4117266726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
