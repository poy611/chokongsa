﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t2801162465;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t3479575958;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::.ctor(System.IntPtr)
extern "C"  void CommitResponse__ctor_m2654479082 (CommitResponse_t2801162465 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::ResponseStatus()
extern "C"  int32_t CommitResponse_ResponseStatus_m741189284 (CommitResponse_t2801162465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::RequestSucceeded()
extern "C"  bool CommitResponse_RequestSucceeded_m564461752 (CommitResponse_t2801162465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::Data()
extern "C"  NativeSnapshotMetadata_t3479575958 * CommitResponse_Data_m3067336382 (CommitResponse_t2801162465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void CommitResponse_CallDispose_m1689989126 (CommitResponse_t2801162465 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::FromPointer(System.IntPtr)
extern "C"  CommitResponse_t2801162465 * CommitResponse_FromPointer_m1676551783 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
