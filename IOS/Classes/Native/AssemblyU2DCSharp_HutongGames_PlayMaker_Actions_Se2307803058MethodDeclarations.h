﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorCullingMode
struct SetAnimatorCullingMode_t2307803058;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::.ctor()
extern "C"  void SetAnimatorCullingMode__ctor_m989180148 (SetAnimatorCullingMode_t2307803058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::Reset()
extern "C"  void SetAnimatorCullingMode_Reset_m2930580385 (SetAnimatorCullingMode_t2307803058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::OnEnter()
extern "C"  void SetAnimatorCullingMode_OnEnter_m806078411 (SetAnimatorCullingMode_t2307803058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::SetCullingMode()
extern "C"  void SetAnimatorCullingMode_SetCullingMode_m3286819841 (SetAnimatorCullingMode_t2307803058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
