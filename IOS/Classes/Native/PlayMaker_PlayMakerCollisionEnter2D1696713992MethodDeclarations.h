﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionEnter2D
struct PlayMakerCollisionEnter2D_t1696713992;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"

// System.Void PlayMakerCollisionEnter2D::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionEnter2D_OnCollisionEnter2D_m914417611 (PlayMakerCollisionEnter2D_t1696713992 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionEnter2D::.ctor()
extern "C"  void PlayMakerCollisionEnter2D__ctor_m912332769 (PlayMakerCollisionEnter2D_t1696713992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
