﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LookAt2dGameObject
struct  LookAt2dGameObject_t1416135293  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LookAt2dGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::targetObject
	FsmGameObject_t1697147867 * ___targetObject_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LookAt2dGameObject::rotationOffset
	FsmFloat_t2134102846 * ___rotationOffset_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LookAt2dGameObject::debug
	FsmBool_t1075959796 * ___debug_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LookAt2dGameObject::debugLineColor
	FsmColor_t2131419205 * ___debugLineColor_15;
	// System.Boolean HutongGames.PlayMaker.Actions.LookAt2dGameObject::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::go
	GameObject_t3674682005 * ___go_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::goTarget
	GameObject_t3674682005 * ___goTarget_18;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_targetObject_12() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___targetObject_12)); }
	inline FsmGameObject_t1697147867 * get_targetObject_12() const { return ___targetObject_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_targetObject_12() { return &___targetObject_12; }
	inline void set_targetObject_12(FsmGameObject_t1697147867 * value)
	{
		___targetObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_12, value);
	}

	inline static int32_t get_offset_of_rotationOffset_13() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___rotationOffset_13)); }
	inline FsmFloat_t2134102846 * get_rotationOffset_13() const { return ___rotationOffset_13; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationOffset_13() { return &___rotationOffset_13; }
	inline void set_rotationOffset_13(FsmFloat_t2134102846 * value)
	{
		___rotationOffset_13 = value;
		Il2CppCodeGenWriteBarrier(&___rotationOffset_13, value);
	}

	inline static int32_t get_offset_of_debug_14() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___debug_14)); }
	inline FsmBool_t1075959796 * get_debug_14() const { return ___debug_14; }
	inline FsmBool_t1075959796 ** get_address_of_debug_14() { return &___debug_14; }
	inline void set_debug_14(FsmBool_t1075959796 * value)
	{
		___debug_14 = value;
		Il2CppCodeGenWriteBarrier(&___debug_14, value);
	}

	inline static int32_t get_offset_of_debugLineColor_15() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___debugLineColor_15)); }
	inline FsmColor_t2131419205 * get_debugLineColor_15() const { return ___debugLineColor_15; }
	inline FsmColor_t2131419205 ** get_address_of_debugLineColor_15() { return &___debugLineColor_15; }
	inline void set_debugLineColor_15(FsmColor_t2131419205 * value)
	{
		___debugLineColor_15 = value;
		Il2CppCodeGenWriteBarrier(&___debugLineColor_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_go_17() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___go_17)); }
	inline GameObject_t3674682005 * get_go_17() const { return ___go_17; }
	inline GameObject_t3674682005 ** get_address_of_go_17() { return &___go_17; }
	inline void set_go_17(GameObject_t3674682005 * value)
	{
		___go_17 = value;
		Il2CppCodeGenWriteBarrier(&___go_17, value);
	}

	inline static int32_t get_offset_of_goTarget_18() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1416135293, ___goTarget_18)); }
	inline GameObject_t3674682005 * get_goTarget_18() const { return ___goTarget_18; }
	inline GameObject_t3674682005 ** get_address_of_goTarget_18() { return &___goTarget_18; }
	inline void set_goTarget_18(GameObject_t3674682005 * value)
	{
		___goTarget_18 = value;
		Il2CppCodeGenWriteBarrier(&___goTarget_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
