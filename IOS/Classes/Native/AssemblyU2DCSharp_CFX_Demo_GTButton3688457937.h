﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_GTButton
struct  CFX_Demo_GTButton_t3688457937  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Color CFX_Demo_GTButton::NormalColor
	Color_t4194546905  ___NormalColor_2;
	// UnityEngine.Color CFX_Demo_GTButton::HoverColor
	Color_t4194546905  ___HoverColor_3;
	// System.String CFX_Demo_GTButton::Callback
	String_t* ___Callback_4;
	// UnityEngine.GameObject CFX_Demo_GTButton::Receiver
	GameObject_t3674682005 * ___Receiver_5;
	// UnityEngine.Rect CFX_Demo_GTButton::CollisionRect
	Rect_t4241904616  ___CollisionRect_6;
	// System.Boolean CFX_Demo_GTButton::Over
	bool ___Over_7;

public:
	inline static int32_t get_offset_of_NormalColor_2() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___NormalColor_2)); }
	inline Color_t4194546905  get_NormalColor_2() const { return ___NormalColor_2; }
	inline Color_t4194546905 * get_address_of_NormalColor_2() { return &___NormalColor_2; }
	inline void set_NormalColor_2(Color_t4194546905  value)
	{
		___NormalColor_2 = value;
	}

	inline static int32_t get_offset_of_HoverColor_3() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___HoverColor_3)); }
	inline Color_t4194546905  get_HoverColor_3() const { return ___HoverColor_3; }
	inline Color_t4194546905 * get_address_of_HoverColor_3() { return &___HoverColor_3; }
	inline void set_HoverColor_3(Color_t4194546905  value)
	{
		___HoverColor_3 = value;
	}

	inline static int32_t get_offset_of_Callback_4() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___Callback_4)); }
	inline String_t* get_Callback_4() const { return ___Callback_4; }
	inline String_t** get_address_of_Callback_4() { return &___Callback_4; }
	inline void set_Callback_4(String_t* value)
	{
		___Callback_4 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_4, value);
	}

	inline static int32_t get_offset_of_Receiver_5() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___Receiver_5)); }
	inline GameObject_t3674682005 * get_Receiver_5() const { return ___Receiver_5; }
	inline GameObject_t3674682005 ** get_address_of_Receiver_5() { return &___Receiver_5; }
	inline void set_Receiver_5(GameObject_t3674682005 * value)
	{
		___Receiver_5 = value;
		Il2CppCodeGenWriteBarrier(&___Receiver_5, value);
	}

	inline static int32_t get_offset_of_CollisionRect_6() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___CollisionRect_6)); }
	inline Rect_t4241904616  get_CollisionRect_6() const { return ___CollisionRect_6; }
	inline Rect_t4241904616 * get_address_of_CollisionRect_6() { return &___CollisionRect_6; }
	inline void set_CollisionRect_6(Rect_t4241904616  value)
	{
		___CollisionRect_6 = value;
	}

	inline static int32_t get_offset_of_Over_7() { return static_cast<int32_t>(offsetof(CFX_Demo_GTButton_t3688457937, ___Over_7)); }
	inline bool get_Over_7() const { return ___Over_7; }
	inline bool* get_address_of_Over_7() { return &___Over_7; }
	inline void set_Over_7(bool value)
	{
		___Over_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
