﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesClientFactory
struct PlayGamesClientFactory_t1822448424;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t2528289561;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGam4135364200.h"

// System.Void GooglePlayGames.PlayGamesClientFactory::.ctor()
extern "C"  void PlayGamesClientFactory__ctor_m1251846287 (PlayGamesClientFactory_t1822448424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.IPlayGamesClient GooglePlayGames.PlayGamesClientFactory::GetPlatformPlayGamesClient(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C"  Il2CppObject * PlayGamesClientFactory_GetPlatformPlayGamesClient_m189021676 (Il2CppObject * __this /* static, unused */, PlayGamesClientConfiguration_t4135364200  ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
