﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_t1091180957;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils
struct  EnumUtils_t1026533742  : public Il2CppObject
{
public:

public:
};

struct EnumUtils_t1026533742_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Newtonsoft.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_t1091180957 * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_t1026533742_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_t1091180957 * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_t1091180957 ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_t1091180957 * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier(&___EnumMemberNamesPerType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
