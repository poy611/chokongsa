﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c
struct U3CU3Ec_t1701094040;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>
struct IEnumerable_1_t3001461559;
// System.Type
struct Type_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m519697285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m863599912 (U3CU3Ec_t1701094040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetSerializableMembers>b__30_0(System.Reflection.MemberInfo)
extern "C"  bool U3CU3Ec_U3CGetSerializableMembersU3Eb__30_0_m928980790 (U3CU3Ec_t1701094040 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetSerializableMembers>b__30_1(System.Reflection.MemberInfo)
extern "C"  bool U3CU3Ec_U3CGetSerializableMembersU3Eb__30_1_m1279780727 (U3CU3Ec_t1701094040 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetExtensionDataMemberForType>b__33_0(System.Type)
extern "C"  Il2CppObject* U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__33_0_m1997257432 (U3CU3Ec_t1701094040 * __this, Type_t * ___baseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetExtensionDataMemberForType>b__33_1(System.Reflection.MemberInfo)
extern "C"  bool U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__33_1_m1977138806 (U3CU3Ec_t1701094040 * __this, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetAttributeConstructor>b__36_0(System.Reflection.ConstructorInfo)
extern "C"  bool U3CU3Ec_U3CGetAttributeConstructorU3Eb__36_0_m1136569132 (U3CU3Ec_t1701094040 * __this, ConstructorInfo_t4136801618 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<CreateProperties>b__60_0(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  int32_t U3CU3Ec_U3CCreatePropertiesU3Eb__60_0_m3783603188 (U3CU3Ec_t1701094040 * __this, JsonProperty_t902655177 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
