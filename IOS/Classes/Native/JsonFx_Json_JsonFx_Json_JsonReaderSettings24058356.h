﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JsonFx.Json.TypeCoercionUtility
struct TypeCoercionUtility_t700629014;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonFx.Json.JsonReaderSettings
struct  JsonReaderSettings_t24058356  : public Il2CppObject
{
public:
	// JsonFx.Json.TypeCoercionUtility JsonFx.Json.JsonReaderSettings::Coercion
	TypeCoercionUtility_t700629014 * ___Coercion_0;
	// System.Boolean JsonFx.Json.JsonReaderSettings::allowUnquotedObjectKeys
	bool ___allowUnquotedObjectKeys_1;
	// System.String JsonFx.Json.JsonReaderSettings::typeHintName
	String_t* ___typeHintName_2;

public:
	inline static int32_t get_offset_of_Coercion_0() { return static_cast<int32_t>(offsetof(JsonReaderSettings_t24058356, ___Coercion_0)); }
	inline TypeCoercionUtility_t700629014 * get_Coercion_0() const { return ___Coercion_0; }
	inline TypeCoercionUtility_t700629014 ** get_address_of_Coercion_0() { return &___Coercion_0; }
	inline void set_Coercion_0(TypeCoercionUtility_t700629014 * value)
	{
		___Coercion_0 = value;
		Il2CppCodeGenWriteBarrier(&___Coercion_0, value);
	}

	inline static int32_t get_offset_of_allowUnquotedObjectKeys_1() { return static_cast<int32_t>(offsetof(JsonReaderSettings_t24058356, ___allowUnquotedObjectKeys_1)); }
	inline bool get_allowUnquotedObjectKeys_1() const { return ___allowUnquotedObjectKeys_1; }
	inline bool* get_address_of_allowUnquotedObjectKeys_1() { return &___allowUnquotedObjectKeys_1; }
	inline void set_allowUnquotedObjectKeys_1(bool value)
	{
		___allowUnquotedObjectKeys_1 = value;
	}

	inline static int32_t get_offset_of_typeHintName_2() { return static_cast<int32_t>(offsetof(JsonReaderSettings_t24058356, ___typeHintName_2)); }
	inline String_t* get_typeHintName_2() const { return ___typeHintName_2; }
	inline String_t** get_address_of_typeHintName_2() { return &___typeHintName_2; }
	inline void set_typeHintName_2(String_t* value)
	{
		___typeHintName_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeHintName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
