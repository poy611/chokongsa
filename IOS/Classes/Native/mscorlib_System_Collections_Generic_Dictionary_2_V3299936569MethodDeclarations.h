﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct ValueCollection_t3299936569;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct IEnumerator_1_t46189413;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Utilities.PrimitiveTypeCode[]
struct PrimitiveTypeCodeU5BU5D_t2402767813;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2531164264.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4288293585_gshared (ValueCollection_t3299936569 * __this, Dictionary_2_t304363560 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m4288293585(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3299936569 *, Dictionary_2_t304363560 *, const MethodInfo*))ValueCollection__ctor_m4288293585_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m284881729_gshared (ValueCollection_t3299936569 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m284881729(__this, ___item0, method) ((  void (*) (ValueCollection_t3299936569 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m284881729_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1873193098_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1873193098(__this, method) ((  void (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1873193098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4019550469_gshared (ValueCollection_t3299936569 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4019550469(__this, ___item0, method) ((  bool (*) (ValueCollection_t3299936569 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4019550469_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m554897130_gshared (ValueCollection_t3299936569 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m554897130(__this, ___item0, method) ((  bool (*) (ValueCollection_t3299936569 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m554897130_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3366650200_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3366650200(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3366650200_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1022600462_gshared (ValueCollection_t3299936569 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1022600462(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3299936569 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1022600462_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1772962825_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1772962825(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1772962825_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2294726328_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2294726328(__this, method) ((  bool (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2294726328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3156957336_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3156957336(__this, method) ((  bool (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3156957336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m536240452_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m536240452(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m536240452_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4055892696_gshared (ValueCollection_t3299936569 * __this, PrimitiveTypeCodeU5BU5D_t2402767813* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4055892696(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3299936569 *, PrimitiveTypeCodeU5BU5D_t2402767813*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4055892696_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::GetEnumerator()
extern "C"  Enumerator_t2531164264  ValueCollection_GetEnumerator_m1115290875_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1115290875(__this, method) ((  Enumerator_t2531164264  (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_GetEnumerator_m1115290875_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m6241886_gshared (ValueCollection_t3299936569 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m6241886(__this, method) ((  int32_t (*) (ValueCollection_t3299936569 *, const MethodInfo*))ValueCollection_get_Count_m6241886_gshared)(__this, method)
