﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71
struct U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71::.ctor()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey71__ctor_m1904956804 (U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
