﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Xml.XPath.AncestorIterator
struct AncestorIterator_t2080208005;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t1327316739;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t1383168931;
// System.Xml.XPath.AncestorOrSelfIterator
struct AncestorOrSelfIterator_t3368289780;
// System.Xml.XPath.AttributeIterator
struct AttributeIterator_t4254482656;
// System.Xml.XPath.AxisIterator
struct AxisIterator_t953634771;
// System.Xml.XPath.NodeTest
struct NodeTest_t2939071960;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1075073278;
// System.Xml.XPath.AxisSpecifier
struct AxisSpecifier_t3783148883;
// System.String
struct String_t;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3774973253;
// System.Xml.XPath.ChildIterator
struct ChildIterator_t3087618016;
// System.Xml.XPath.CompiledExpression
struct CompiledExpression_t3956813165;
// System.Xml.XPath.Expression
struct Expression_t2556460284;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t1467853467;
// System.Xml.XPath.DescendantIterator
struct DescendantIterator_t928464879;
// System.Xml.XPath.DescendantOrSelfIterator
struct DescendantOrSelfIterator_t1437473246;
// System.Xml.XPath.EqualityExpr
struct EqualityExpr_t1406255795;
// System.Xml.XPath.ExprAND
struct ExprAND_t3747310744;
// System.Xml.XPath.ExprBinary
struct ExprBinary_t1545048250;
// System.Xml.XPath.ExprBoolean
struct ExprBoolean_t513597673;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.ExprDIV
struct ExprDIV_t3747313490;
// System.Xml.XPath.ExprEQ
struct ExprEQ_t2486226757;
// System.Xml.XPath.ExprFilter
struct ExprFilter_t1659523121;
// System.Xml.XPath.ExprFunctionCall
struct ExprFunctionCall_t3990388239;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2133315502;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t2391178772;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t2968841889;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1178308879;
// System.Xml.XPath.ExprGE
struct ExprGE_t2486226807;
// System.Xml.XPath.ExprGT
struct ExprGT_t2486226822;
// System.Xml.XPath.ExprLE
struct ExprLE_t2486226962;
// System.Xml.XPath.ExprLiteral
struct ExprLiteral_t631346544;
// System.Xml.XPath.ExprLT
struct ExprLT_t2486226977;
// System.Xml.XPath.ExprMINUS
struct ExprMINUS_t623244849;
// System.Xml.XPath.ExprMOD
struct ExprMOD_t3747322307;
// System.Xml.XPath.ExprMULT
struct ExprMULT_t4186577097;
// System.Xml.XPath.ExprNE
struct ExprNE_t2486227024;
// System.Xml.XPath.ExprNEG
struct ExprNEG_t3747322961;
// System.Xml.XPath.ExprNumber
struct ExprNumber_t1899651074;
// System.Xml.XPath.ExprNumeric
struct ExprNumeric_t2743439310;
// System.Xml.XPath.ExprOR
struct ExprOR_t2486227068;
// System.Xml.XPath.ExprParens
struct ExprParens_t1938591074;
// System.Xml.XPath.ExprPLUS
struct ExprPLUS_t4186658099;
// System.Xml.XPath.ExprRoot
struct ExprRoot_t4186752155;
// System.Xml.XPath.ExprSLASH
struct ExprSLASH_t628862782;
// System.Xml.XPath.NodeSet
struct NodeSet_t2875795446;
// System.Xml.XPath.ExprSLASH2
struct ExprSLASH2_t2003606286;
// System.Xml.XPath.ExprUNION
struct ExprUNION_t630776976;
// System.Xml.XPath.ExprVariable
struct ExprVariable_t3764672565;
// System.Xml.XPath.FollowingIterator
struct FollowingIterator_t3149710293;
// System.Xml.XPath.FollowingSiblingIterator
struct FollowingSiblingIterator_t116837539;
// System.Collections.ArrayList
struct ArrayList_t3948406897;
// System.Xml.XPath.ListIterator
struct ListIterator_t4080389072;
// System.Collections.IList
struct IList_t1751339649;
// System.Xml.XPath.NamespaceIterator
struct NamespaceIterator_t1295432863;
// System.Xml.XPath.NodeNameTest
struct NodeNameTest_t2799571587;
// System.Xml.XPath.NodeTypeTest
struct NodeTypeTest_t282671026;
// System.Xml.XPath.NullIterator
struct NullIterator_t2905335737;
// System.Xml.XPath.ParensIterator
struct ParensIterator_t3418004251;
// System.Xml.XPath.ParentIterator
struct ParentIterator_t1610549788;
// System.Xml.XPath.PrecedingIterator
struct PrecedingIterator_t1039893511;
// System.Xml.XPath.PrecedingSiblingIterator
struct PrecedingSiblingIterator_t1318768689;
// System.Xml.XPath.PredicateIterator
struct PredicateIterator_t3158177019;
// System.Xml.XPath.RelationalExpr
struct RelationalExpr_t3718551712;
// System.Xml.XPath.SelfIterator
struct SelfIterator_t197024894;
// System.Xml.XPath.SimpleIterator
struct SimpleIterator_t544647972;
// System.Xml.XPath.SimpleSlashIterator
struct SimpleSlashIterator_t1419496431;
// System.Xml.XPath.SlashIterator
struct SlashIterator_t384731969;
// System.Xml.XPath.SortedIterator
struct SortedIterator_t4014821679;
// System.Xml.XPath.UnionIterator
struct UnionIterator_t1391611539;
// System.Xml.XPath.WrapperIterator
struct WrapperIterator_t2849115671;
// System.Xml.XPath.XPathBooleanFunction
struct XPathBooleanFunction_t3865271111;
// System.Xml.XPath.XPathException
struct XPathException_t1803876086;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Exception
struct Exception_t3991598821;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t3588072235;
// System.Xml.XPath.XPathFunction
struct XPathFunction_t3895226859;
// System.Xml.XPath.XPathFunctionBoolean
struct XPathFunctionBoolean_t149146711;
// System.Xml.XPath.XPathFunctionCeil
struct XPathFunctionCeil_t843991056;
// System.Xml.XPath.XPathFunctionConcat
struct XPathFunctionConcat_t2260200095;
// System.Xml.XPath.XPathFunctionContains
struct XPathFunctionContains_t1738767914;
// System.Xml.XPath.XPathFunctionCount
struct XPathFunctionCount_t82957758;
// System.Xml.XPath.XPathFunctionFalse
struct XPathFunctionFalse_t85302738;
// System.Xml.XPath.XPathFunctionFloor
struct XPathFunctionFloor_t85633211;
// System.Xml.XPath.XPathFunctionId
struct XPathFunctionId_t1025767334;
// System.Xml.XPath.XPathFunctionLang
struct XPathFunctionLang_t844255481;
// System.Xml.XPath.XPathFunctionLast
struct XPathFunctionLast_t844255649;
// System.Xml.XPath.XPathFunctionLocalName
struct XPathFunctionLocalName_t1256488005;
// System.Xml.XPath.XPathFunctionName
struct XPathFunctionName_t844315030;
// System.Xml.XPath.XPathFunctionNamespaceUri
struct XPathFunctionNamespaceUri_t4291495708;
// System.Xml.XPath.XPathFunctionNormalizeSpace
struct XPathFunctionNormalizeSpace_t1100613508;
// System.Xml.XPath.XPathFunctionNot
struct XPathFunctionNot_t1422750722;
// System.Xml.XPath.XPathFunctionNumber
struct XPathFunctionNumber_t2580631252;
// System.Xml.XPath.XPathFunctionPosition
struct XPathFunctionPosition_t3054018868;
// System.Xml.XPath.XPathFunctionRound
struct XPathFunctionRound_t96810557;
// System.Xml.XPath.XPathFunctionStartsWith
struct XPathFunctionStartsWith_t1437129026;
// System.Xml.XPath.XPathFunctionString
struct XPathFunctionString_t2723009436;
// System.Xml.XPath.XPathFunctionStringLength
struct XPathFunctionStringLength_t1116358498;
// System.Xml.XPath.XPathFunctionSubstring
struct XPathFunctionSubstring_t2992425472;
// System.Xml.XPath.XPathFunctionSubstringAfter
struct XPathFunctionSubstringAfter_t1726871446;
// System.Xml.XPath.XPathFunctionSubstringBefore
struct XPathFunctionSubstringBefore_t1709434719;
// System.Xml.XPath.XPathFunctionSum
struct XPathFunctionSum_t1422755706;
// System.Xml.XPath.XPathFunctionTranslate
struct XPathFunctionTranslate_t3514715389;
// System.Xml.XPath.XPathFunctionTrue
struct XPathFunctionTrue_t844510361;
// System.Xml.XPath.XPathItem
struct XPathItem_t3597956134;
// System.Xml.XPath.XPathIteratorComparer
struct XPathIteratorComparer_t3727165134;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_Xml_System_Xml_XPath_AncestorIterator2080208005.h"
#include "System_Xml_System_Xml_XPath_AncestorIterator2080208005MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_BaseIterator1327316739.h"
#include "mscorlib_System_Void2863195528.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator544647972MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator544647972.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Collections_ArrayList3948406897MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator3368289780.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator3368289780MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AttributeIterator4254482656.h"
#include "System_Xml_System_Xml_XPath_AttributeIterator4254482656MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_BaseIterator1327316739MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Axes2202774217.h"
#include "System_Xml_System_Xml_XPath_Axes2202774217MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AxisIterator953634771.h"
#include "System_Xml_System_Xml_XPath_AxisIterator953634771MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTest2939071960.h"
#include "System_Xml_System_Xml_XPath_NodeTest2939071960MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier3783148883.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier3783148883MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3456360697.h"
#include "System_Xml_System_Xml_XPath_ChildIterator3087618016MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_DescendantIterator928464879MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat1437473246MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator3149710293MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterato116837539MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator1295432863MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ParentIterator1610549788MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator1039893511MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat1318768689MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SelfIterator197024894MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ChildIterator3087618016.h"
#include "System_Xml_System_Xml_XPath_DescendantIterator928464879.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat1437473246.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator3149710293.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterato116837539.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator1295432863.h"
#include "System_Xml_System_Xml_XPath_ParentIterator1610549788.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator1039893511.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat1318768689.h"
#include "System_Xml_System_Xml_XPath_SelfIterator197024894.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "System_Xml_System_Xml_XPath_XPathItem3597956134MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathItem3597956134.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3956813165.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3956813165MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Expression2556460284.h"
#include "System_Xml_System_Xml_XPath_XPathExpression3588072235MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Expression2556460284MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"
#include "System_Xml_System_Xml_XPath_XPathSorters1807339567MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSorters1807339567.h"
#include "System_Xml_System_Xml_XPath_XPathException1803876086MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException1803876086.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr1406255795.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr1406255795MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean513597673MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBinary1545048250MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprBinary1545048250.h"
#include "System_Xml_System_Xml_XPath_XPathResultType516720010.h"
#include "mscorlib_System_Double3868226565.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions181677634MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprAND3747310744.h"
#include "System_Xml_System_Xml_XPath_ExprAND3747310744MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean513597673.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue844510361MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse85302738MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue844510361.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2391178772.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse85302738.h"
#include "System_Xml_System_Xml_XPath_ExprDIV3747313490.h"
#include "System_Xml_System_Xml_XPath_ExprDIV3747313490MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric2743439310MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprEQ2486226757.h"
#include "System_Xml_System_Xml_XPath_ExprEQ2486226757MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2849115671MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NullIterator2905335737MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2849115671.h"
#include "System_Xml_System_Xml_XPath_NullIterator2905335737.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprFilter1659523121.h"
#include "System_Xml_System_Xml_XPath_ExprFilter1659523121MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeSet2875795446MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator3158177019MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator3158177019.h"
#include "System_Xml_System_Xml_XPath_NodeSet2875795446.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3990388239.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3990388239MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2391178772MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast844255649MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3054018868MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount82957758MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId1025767334MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName1256488005MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace4291495708MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName844315030MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString2723009436MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat2260200095MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit1437129026MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains1738767914MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1709434719MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1726871446MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2992425472MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen1116358498MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize1100613508MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate3514715389MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean149146711MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1422750722MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang844255481MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber2580631252MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum1422755706MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor85633211MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil843991056MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound96810557MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast844255649.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3054018868.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount82957758.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId1025767334.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName1256488005.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace4291495708.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName844315030.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString2723009436.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat2260200095.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit1437129026.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains1738767914.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1709434719.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1726871446.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2992425472.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen1116358498.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize1100613508.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate3514715389.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean149146711.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1422750722.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang844255481.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber2580631252.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum1422755706.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor85633211.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil843991056.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound96810557.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_System_Xml_Xsl_XsltContext894076946.h"
#include "System_Xml_System_Xml_Xsl_XsltContext894076946MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprGE2486226807.h"
#include "System_Xml_System_Xml_XPath_ExprGE2486226807MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr3718551712MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprGT2486226822.h"
#include "System_Xml_System_Xml_XPath_ExprGT2486226822MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLE2486226962.h"
#include "System_Xml_System_Xml_XPath_ExprLE2486226962MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral631346544.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral631346544MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLT2486226977.h"
#include "System_Xml_System_Xml_XPath_ExprLT2486226977MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS623244849.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS623244849MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMOD3747322307.h"
#include "System_Xml_System_Xml_XPath_ExprMOD3747322307MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMULT4186577097.h"
#include "System_Xml_System_Xml_XPath_ExprMULT4186577097MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNE2486227024.h"
#include "System_Xml_System_Xml_XPath_ExprNE2486227024MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNEG3747322961.h"
#include "System_Xml_System_Xml_XPath_ExprNEG3747322961MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumber1899651074MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumber1899651074.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric2743439310.h"
#include "System_Xml_System_Xml_XPath_ExprOR2486227068.h"
#include "System_Xml_System_Xml_XPath_ExprOR2486227068MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprParens1938591074.h"
#include "System_Xml_System_Xml_XPath_ExprParens1938591074MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ParensIterator3418004251MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ParensIterator3418004251.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS4186658099.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS4186658099MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprRoot4186752155.h"
#include "System_Xml_System_Xml_XPath_ExprRoot4186752155MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH628862782.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH628862782MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator1419496431MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SlashIterator384731969MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SortedIterator4014821679MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator1419496431.h"
#include "System_Xml_System_Xml_XPath_SlashIterator384731969.h"
#include "System_Xml_System_Xml_XPath_SortedIterator4014821679.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH22003606286.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH22003606286MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest282671026MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest282671026.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest2799571587MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest2799571587.h"
#include "System_Xml_System_Xml_XPath_ExprUNION630776976.h"
#include "System_Xml_System_Xml_XPath_ExprUNION630776976MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_UnionIterator1391611539MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_UnionIterator1391611539.h"
#include "System_Xml_System_Xml_XPath_ExprVariable3764672565.h"
#include "System_Xml_System_Xml_XPath_ExprVariable3764672565MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ListIterator4080389072.h"
#include "System_Xml_System_Xml_XPath_ListIterator4080389072MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeOrder444538963.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr3718551712.h"
#include "mscorlib_System_Collections_SortedList4117722949.h"
#include "mscorlib_System_Collections_SortedList4117722949MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer3727165134.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer3727165134MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2221114443.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2221114443MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "System_Xml_System_Xml_XPath_XmlDataType545259441.h"
#include "System_Xml_System_Xml_XPath_XmlDataType545259441MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction3865271111.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction3865271111MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathFunction3895226859MethodDeclarations.h"
#include "mscorlib_System_SystemException4206535862MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathExpression3588072235.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser2461941858MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser2461941858.h"
#include "System_Xml_System_Xml_XPath_XPathFunction3895226859.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction670389548MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1593496111MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3988332413.h"
#include "mscorlib_System_RuntimeFieldHandle2347752062.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions181677634.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232.h"
#include "mscorlib_System_OverflowException4020384771.h"
#include "mscorlib_System_FormatException3455918062.h"
#include "System_Xml_System_Xml_XmlChar856576415.h"
#include "System_Xml_System_Xml_XmlChar856576415MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberStyles2609490573.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope1935109964.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope1935109964MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3379220348.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1273927208MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1273927208.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera2530618692MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera2530618692.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XPath.AncestorIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void AncestorIterator__ctor_m494746931 (AncestorIterator_t2080208005 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.AncestorIterator::.ctor(System.Xml.XPath.AncestorIterator)
extern "C"  void AncestorIterator__ctor_m4189982705 (AncestorIterator_t2080208005 * __this, AncestorIterator_t2080208005 * ___other0, const MethodInfo* method)
{
	{
		AncestorIterator_t2080208005 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		AncestorIterator_t2080208005 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		AncestorIterator_t2080208005 * L_3 = ___other0;
		NullCheck(L_3);
		ArrayList_t3948406897 * L_4 = L_3->get_navigators_7();
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		AncestorIterator_t2080208005 * L_5 = ___other0;
		NullCheck(L_5);
		ArrayList_t3948406897 * L_6 = L_5->get_navigators_7();
		__this->set_navigators_7(L_6);
	}

IL_002b:
	{
		AncestorIterator_t2080208005 * L_7 = ___other0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_currentPosition_6();
		__this->set_currentPosition_6(L_8);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AncestorIterator::Clone()
extern Il2CppClass* AncestorIterator_t2080208005_il2cpp_TypeInfo_var;
extern const uint32_t AncestorIterator_Clone_m1848056383_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * AncestorIterator_Clone_m1848056383 (AncestorIterator_t2080208005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorIterator_Clone_m1848056383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AncestorIterator_t2080208005 * L_0 = (AncestorIterator_t2080208005 *)il2cpp_codegen_object_new(AncestorIterator_t2080208005_il2cpp_TypeInfo_var);
		AncestorIterator__ctor_m4189982705(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.AncestorIterator::CollectResults()
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern const uint32_t AncestorIterator_CollectResults_m594795063_MetadataUsageId;
extern "C"  void AncestorIterator_CollectResults_m594795063 (AncestorIterator_t2080208005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorIterator_CollectResults_m594795063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	{
		ArrayList_t3948406897 * L_0 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_navigators_7(L_0);
		XPathNavigator_t1075073278 * L_1 = __this->get_startPosition_8();
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		V_0 = L_2;
		goto IL_002e;
	}

IL_001c:
	{
		ArrayList_t3948406897 * L_3 = __this->get_navigators_7();
		XPathNavigator_t1075073278 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_4);
		NullCheck(L_3);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_3, L_5);
	}

IL_002e:
	{
		XPathNavigator_t1075073278 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_6);
		if (!L_7)
		{
			goto IL_0044;
		}
	}
	{
		XPathNavigator_t1075073278 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_8);
		if (L_9)
		{
			goto IL_001c;
		}
	}

IL_0044:
	{
		ArrayList_t3948406897 * L_10 = __this->get_navigators_7();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_10);
		__this->set_currentPosition_6(L_11);
		return;
	}
}
// System.Boolean System.Xml.XPath.AncestorIterator::MoveNextCore()
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t AncestorIterator_MoveNextCore_m819234470_MetadataUsageId;
extern "C"  bool AncestorIterator_MoveNextCore_m819234470 (AncestorIterator_t2080208005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorIterator_MoveNextCore_m819234470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ArrayList_t3948406897 * L_0 = __this->get_navigators_7();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		AncestorIterator_CollectResults_m594795063(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		int32_t L_1 = __this->get_currentPosition_6();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)0;
	}

IL_001e:
	{
		XPathNavigator_t1075073278 * L_2 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		ArrayList_t3948406897 * L_3 = __this->get_navigators_7();
		int32_t L_4 = __this->get_currentPosition_6();
		int32_t L_5 = ((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = L_5;
		__this->set_currentPosition_6(L_5);
		int32_t L_6 = V_0;
		NullCheck(L_3);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_6);
		NullCheck(L_2);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_2, ((XPathNavigator_t1075073278 *)CastclassClass(L_7, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.AncestorIterator::get_ReverseAxis()
extern "C"  bool AncestorIterator_get_ReverseAxis_m4260779833 (AncestorIterator_t2080208005 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.AncestorIterator::get_Count()
extern "C"  int32_t AncestorIterator_get_Count_m3165807551 (AncestorIterator_t2080208005 * __this, const MethodInfo* method)
{
	{
		ArrayList_t3948406897 * L_0 = __this->get_navigators_7();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		AncestorIterator_CollectResults_m594795063(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		ArrayList_t3948406897 * L_1 = __this->get_navigators_7();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.AncestorOrSelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void AncestorOrSelfIterator__ctor_m3111916002 (AncestorOrSelfIterator_t3368289780 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.AncestorOrSelfIterator::.ctor(System.Xml.XPath.AncestorOrSelfIterator)
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern const uint32_t AncestorOrSelfIterator__ctor_m334673489_MetadataUsageId;
extern "C"  void AncestorOrSelfIterator__ctor_m334673489 (AncestorOrSelfIterator_t3368289780 * __this, AncestorOrSelfIterator_t3368289780 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorOrSelfIterator__ctor_m334673489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AncestorOrSelfIterator_t3368289780 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		AncestorOrSelfIterator_t3368289780 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		AncestorOrSelfIterator_t3368289780 * L_3 = ___other0;
		NullCheck(L_3);
		ArrayList_t3948406897 * L_4 = L_3->get_navigators_7();
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		AncestorOrSelfIterator_t3368289780 * L_5 = ___other0;
		NullCheck(L_5);
		ArrayList_t3948406897 * L_6 = L_5->get_navigators_7();
		NullCheck(L_6);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(49 /* System.Object System.Collections.ArrayList::Clone() */, L_6);
		__this->set_navigators_7(((ArrayList_t3948406897 *)CastclassClass(L_7, ArrayList_t3948406897_il2cpp_TypeInfo_var)));
	}

IL_0035:
	{
		AncestorOrSelfIterator_t3368289780 * L_8 = ___other0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_currentPosition_6();
		__this->set_currentPosition_6(L_9);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AncestorOrSelfIterator::Clone()
extern Il2CppClass* AncestorOrSelfIterator_t3368289780_il2cpp_TypeInfo_var;
extern const uint32_t AncestorOrSelfIterator_Clone_m1287523056_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * AncestorOrSelfIterator_Clone_m1287523056 (AncestorOrSelfIterator_t3368289780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorOrSelfIterator_Clone_m1287523056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AncestorOrSelfIterator_t3368289780 * L_0 = (AncestorOrSelfIterator_t3368289780 *)il2cpp_codegen_object_new(AncestorOrSelfIterator_t3368289780_il2cpp_TypeInfo_var);
		AncestorOrSelfIterator__ctor_m334673489(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.AncestorOrSelfIterator::CollectResults()
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern const uint32_t AncestorOrSelfIterator_CollectResults_m2689768550_MetadataUsageId;
extern "C"  void AncestorOrSelfIterator_CollectResults_m2689768550 (AncestorOrSelfIterator_t3368289780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorOrSelfIterator_CollectResults_m2689768550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	{
		ArrayList_t3948406897 * L_0 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_navigators_7(L_0);
		XPathNavigator_t1075073278 * L_1 = __this->get_startPosition_8();
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		V_0 = L_2;
		XPathNavigator_t1075073278 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0023:
	{
		goto IL_0041;
	}

IL_0028:
	{
		ArrayList_t3948406897 * L_5 = __this->get_navigators_7();
		XPathNavigator_t1075073278 * L_6 = V_0;
		NullCheck(L_6);
		XPathNavigator_t1075073278 * L_7 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		NullCheck(L_5);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_5, L_7);
		XPathNavigator_t1075073278 * L_8 = V_0;
		NullCheck(L_8);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_8);
	}

IL_0041:
	{
		XPathNavigator_t1075073278 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_9);
		if (L_10)
		{
			goto IL_0028;
		}
	}
	{
		ArrayList_t3948406897 * L_11 = __this->get_navigators_7();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		__this->set_currentPosition_6(L_12);
		return;
	}
}
// System.Boolean System.Xml.XPath.AncestorOrSelfIterator::MoveNextCore()
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t AncestorOrSelfIterator_MoveNextCore_m2628316821_MetadataUsageId;
extern "C"  bool AncestorOrSelfIterator_MoveNextCore_m2628316821 (AncestorOrSelfIterator_t3368289780 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AncestorOrSelfIterator_MoveNextCore_m2628316821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ArrayList_t3948406897 * L_0 = __this->get_navigators_7();
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		AncestorOrSelfIterator_CollectResults_m2689768550(__this, /*hidden argument*/NULL);
		XPathNavigator_t1075073278 * L_1 = __this->get_startPosition_8();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		XPathNavigator_t1075073278 * L_3 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_3);
		return (bool)1;
	}

IL_002e:
	{
		int32_t L_4 = __this->get_currentPosition_6();
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_003c;
		}
	}
	{
		return (bool)0;
	}

IL_003c:
	{
		int32_t L_5 = __this->get_currentPosition_6();
		int32_t L_6 = L_5;
		V_0 = L_6;
		__this->set_currentPosition_6(((int32_t)((int32_t)L_6-(int32_t)1)));
		int32_t L_7 = V_0;
		if (L_7)
		{
			goto IL_0066;
		}
	}
	{
		XPathNavigator_t1075073278 * L_8 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		XPathNavigator_t1075073278 * L_9 = __this->get_startPosition_8();
		NullCheck(L_8);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_8, L_9);
		return (bool)1;
	}

IL_0066:
	{
		XPathNavigator_t1075073278 * L_10 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		ArrayList_t3948406897 * L_11 = __this->get_navigators_7();
		int32_t L_12 = __this->get_currentPosition_6();
		NullCheck(L_11);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_11, L_12);
		NullCheck(L_10);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_10, ((XPathNavigator_t1075073278 *)CastclassClass(L_13, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.AncestorOrSelfIterator::get_ReverseAxis()
extern "C"  bool AncestorOrSelfIterator_get_ReverseAxis_m1088500970 (AncestorOrSelfIterator_t3368289780 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.AncestorOrSelfIterator::get_Count()
extern "C"  int32_t AncestorOrSelfIterator_get_Count_m312110768 (AncestorOrSelfIterator_t3368289780 * __this, const MethodInfo* method)
{
	{
		ArrayList_t3948406897 * L_0 = __this->get_navigators_7();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		AncestorOrSelfIterator_CollectResults_m2689768550(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		ArrayList_t3948406897 * L_1 = __this->get_navigators_7();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		return ((int32_t)((int32_t)L_2+(int32_t)1));
	}
}
// System.Void System.Xml.XPath.AttributeIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void AttributeIterator__ctor_m626920394 (AttributeIterator_t4254482656 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.AttributeIterator::.ctor(System.Xml.XPath.AttributeIterator)
extern "C"  void AttributeIterator__ctor_m738536479 (AttributeIterator_t4254482656 * __this, AttributeIterator_t4254482656 * ___other0, const MethodInfo* method)
{
	{
		AttributeIterator_t4254482656 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AttributeIterator::Clone()
extern Il2CppClass* AttributeIterator_t4254482656_il2cpp_TypeInfo_var;
extern const uint32_t AttributeIterator_Clone_m1870449490_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * AttributeIterator_Clone_m1870449490 (AttributeIterator_t4254482656 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeIterator_Clone_m1870449490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AttributeIterator_t4254482656 * L_0 = (AttributeIterator_t4254482656 *)il2cpp_codegen_object_new(AttributeIterator_t4254482656_il2cpp_TypeInfo_var);
		AttributeIterator__ctor_m738536479(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.AttributeIterator::MoveNextCore()
extern "C"  bool AttributeIterator_MoveNextCore_m816034189 (AttributeIterator_t4254482656 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		XPathNavigator_t1075073278 * L_1 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, L_1);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)1;
	}

IL_001d:
	{
		goto IL_0034;
	}

IL_0022:
	{
		XPathNavigator_t1075073278 * L_3 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_3);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)1;
	}

IL_0034:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.AxisIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeTest)
extern "C"  void AxisIterator__ctor_m2307956914 (AxisIterator_t953634771 * __this, BaseIterator_t1327316739 * ___iter0, NodeTest_t2939071960 * ___test1, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		__this->set__iter_3(L_2);
		NodeTest_t2939071960 * L_3 = ___test1;
		__this->set__test_4(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.AxisIterator::.ctor(System.Xml.XPath.AxisIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t AxisIterator__ctor_m967112753_MetadataUsageId;
extern "C"  void AxisIterator__ctor_m967112753 (AxisIterator_t953634771 * __this, AxisIterator_t953634771 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AxisIterator__ctor_m967112753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisIterator_t953634771 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		AxisIterator_t953634771 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t1327316739 * L_2 = L_1->get__iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iter_3(((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		AxisIterator_t953634771 * L_4 = ___other0;
		NullCheck(L_4);
		NodeTest_t2939071960 * L_5 = L_4->get__test_4();
		__this->set__test_4(L_5);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.AxisIterator::Clone()
extern Il2CppClass* AxisIterator_t953634771_il2cpp_TypeInfo_var;
extern const uint32_t AxisIterator_Clone_m2661155633_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * AxisIterator_Clone_m2661155633 (AxisIterator_t953634771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AxisIterator_Clone_m2661155633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisIterator_t953634771 * L_0 = (AxisIterator_t953634771 *)il2cpp_codegen_object_new(AxisIterator_t953634771_il2cpp_TypeInfo_var);
		AxisIterator__ctor_m967112753(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.AxisIterator::MoveNextCore()
extern "C"  bool AxisIterator_MoveNextCore_m314688244 (AxisIterator_t953634771 * __this, const MethodInfo* method)
{
	{
		goto IL_0028;
	}

IL_0005:
	{
		NodeTest_t2939071960 * L_0 = __this->get__test_4();
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(__this, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = __this->get__iter_3();
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_2);
		NullCheck(L_0);
		bool L_4 = VirtFuncInvoker2< bool, Il2CppObject *, XPathNavigator_t1075073278 * >::Invoke(21 /* System.Boolean System.Xml.XPath.NodeTest::Match(System.Xml.IXmlNamespaceResolver,System.Xml.XPath.XPathNavigator) */, L_0, L_1, L_3);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		return (bool)1;
	}

IL_0028:
	{
		BaseIterator_t1327316739 * L_5 = __this->get__iter_3();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_5);
		if (L_6)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.AxisIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * AxisIterator_get_Current_m1435899435 (AxisIterator_t953634771 * __this, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_001c;
	}

IL_0011:
	{
		BaseIterator_t1327316739 * L_1 = __this->get__iter_3();
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.AxisIterator::get_ReverseAxis()
extern "C"  bool AxisIterator_get_ReverseAxis_m1414729771 (AxisIterator_t953634771 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XPath.BaseIterator::get_ReverseAxis() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.AxisSpecifier::.ctor(System.Xml.XPath.Axes)
extern "C"  void AxisSpecifier__ctor_m223823863 (AxisSpecifier_t3783148883 * __this, int32_t ___axis0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___axis0;
		__this->set__axis_0(L_0);
		return;
	}
}
// System.Xml.XPath.XPathNodeType System.Xml.XPath.AxisSpecifier::get_NodeType()
extern "C"  int32_t AxisSpecifier_get_NodeType_m3352025689 (AxisSpecifier_t3783148883 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__axis_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_001e;
	}

IL_001a:
	{
		return (int32_t)(3);
	}

IL_001c:
	{
		return (int32_t)(2);
	}

IL_001e:
	{
		return (int32_t)(1);
	}
}
// System.String System.Xml.XPath.AxisSpecifier::ToString()
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3321137619;
extern Il2CppCodeGenString* _stringLiteral1566835452;
extern Il2CppCodeGenString* _stringLiteral13085340;
extern Il2CppCodeGenString* _stringLiteral94631196;
extern Il2CppCodeGenString* _stringLiteral3178663165;
extern Il2CppCodeGenString* _stringLiteral3547151398;
extern Il2CppCodeGenString* _stringLiteral765915793;
extern Il2CppCodeGenString* _stringLiteral2451430278;
extern Il2CppCodeGenString* _stringLiteral1252218203;
extern Il2CppCodeGenString* _stringLiteral3299543210;
extern Il2CppCodeGenString* _stringLiteral2914649283;
extern Il2CppCodeGenString* _stringLiteral341613496;
extern Il2CppCodeGenString* _stringLiteral3526476;
extern const uint32_t AxisSpecifier_ToString_m2798584292_MetadataUsageId;
extern "C"  String_t* AxisSpecifier_ToString_m2798584292 (AxisSpecifier_t3783148883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AxisSpecifier_ToString_m2798584292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__axis_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0046;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
		if (L_1 == 2)
		{
			goto IL_0052;
		}
		if (L_1 == 3)
		{
			goto IL_0058;
		}
		if (L_1 == 4)
		{
			goto IL_005e;
		}
		if (L_1 == 5)
		{
			goto IL_0064;
		}
		if (L_1 == 6)
		{
			goto IL_006a;
		}
		if (L_1 == 7)
		{
			goto IL_0070;
		}
		if (L_1 == 8)
		{
			goto IL_0076;
		}
		if (L_1 == 9)
		{
			goto IL_007c;
		}
		if (L_1 == 10)
		{
			goto IL_0082;
		}
		if (L_1 == 11)
		{
			goto IL_0088;
		}
		if (L_1 == 12)
		{
			goto IL_008e;
		}
	}
	{
		goto IL_0094;
	}

IL_0046:
	{
		return _stringLiteral3321137619;
	}

IL_004c:
	{
		return _stringLiteral1566835452;
	}

IL_0052:
	{
		return _stringLiteral13085340;
	}

IL_0058:
	{
		return _stringLiteral94631196;
	}

IL_005e:
	{
		return _stringLiteral3178663165;
	}

IL_0064:
	{
		return _stringLiteral3547151398;
	}

IL_006a:
	{
		return _stringLiteral765915793;
	}

IL_0070:
	{
		return _stringLiteral2451430278;
	}

IL_0076:
	{
		return _stringLiteral1252218203;
	}

IL_007c:
	{
		return _stringLiteral3299543210;
	}

IL_0082:
	{
		return _stringLiteral2914649283;
	}

IL_0088:
	{
		return _stringLiteral341613496;
	}

IL_008e:
	{
		return _stringLiteral3526476;
	}

IL_0094:
	{
		IndexOutOfRangeException_t3456360697 * L_2 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.Xml.XPath.Axes System.Xml.XPath.AxisSpecifier::get_Axis()
extern "C"  int32_t AxisSpecifier_get_Axis_m269195078 (AxisSpecifier_t3783148883 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__axis_0();
		return L_0;
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.AxisSpecifier::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* AncestorIterator_t2080208005_il2cpp_TypeInfo_var;
extern Il2CppClass* AncestorOrSelfIterator_t3368289780_il2cpp_TypeInfo_var;
extern Il2CppClass* AttributeIterator_t4254482656_il2cpp_TypeInfo_var;
extern Il2CppClass* ChildIterator_t3087618016_il2cpp_TypeInfo_var;
extern Il2CppClass* DescendantIterator_t928464879_il2cpp_TypeInfo_var;
extern Il2CppClass* DescendantOrSelfIterator_t1437473246_il2cpp_TypeInfo_var;
extern Il2CppClass* FollowingIterator_t3149710293_il2cpp_TypeInfo_var;
extern Il2CppClass* FollowingSiblingIterator_t116837539_il2cpp_TypeInfo_var;
extern Il2CppClass* NamespaceIterator_t1295432863_il2cpp_TypeInfo_var;
extern Il2CppClass* ParentIterator_t1610549788_il2cpp_TypeInfo_var;
extern Il2CppClass* PrecedingIterator_t1039893511_il2cpp_TypeInfo_var;
extern Il2CppClass* PrecedingSiblingIterator_t1318768689_il2cpp_TypeInfo_var;
extern Il2CppClass* SelfIterator_t197024894_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var;
extern const uint32_t AxisSpecifier_Evaluate_m3457862801_MetadataUsageId;
extern "C"  BaseIterator_t1327316739 * AxisSpecifier_Evaluate_m3457862801 (AxisSpecifier_t3783148883 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AxisSpecifier_Evaluate_m3457862801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__axis_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0046;
		}
		if (L_1 == 1)
		{
			goto IL_004d;
		}
		if (L_1 == 2)
		{
			goto IL_0054;
		}
		if (L_1 == 3)
		{
			goto IL_005b;
		}
		if (L_1 == 4)
		{
			goto IL_0062;
		}
		if (L_1 == 5)
		{
			goto IL_0069;
		}
		if (L_1 == 6)
		{
			goto IL_0070;
		}
		if (L_1 == 7)
		{
			goto IL_0077;
		}
		if (L_1 == 8)
		{
			goto IL_007e;
		}
		if (L_1 == 9)
		{
			goto IL_0085;
		}
		if (L_1 == 10)
		{
			goto IL_008c;
		}
		if (L_1 == 11)
		{
			goto IL_0093;
		}
		if (L_1 == 12)
		{
			goto IL_009a;
		}
	}
	{
		goto IL_00a1;
	}

IL_0046:
	{
		BaseIterator_t1327316739 * L_2 = ___iter0;
		AncestorIterator_t2080208005 * L_3 = (AncestorIterator_t2080208005 *)il2cpp_codegen_object_new(AncestorIterator_t2080208005_il2cpp_TypeInfo_var);
		AncestorIterator__ctor_m494746931(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_004d:
	{
		BaseIterator_t1327316739 * L_4 = ___iter0;
		AncestorOrSelfIterator_t3368289780 * L_5 = (AncestorOrSelfIterator_t3368289780 *)il2cpp_codegen_object_new(AncestorOrSelfIterator_t3368289780_il2cpp_TypeInfo_var);
		AncestorOrSelfIterator__ctor_m3111916002(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0054:
	{
		BaseIterator_t1327316739 * L_6 = ___iter0;
		AttributeIterator_t4254482656 * L_7 = (AttributeIterator_t4254482656 *)il2cpp_codegen_object_new(AttributeIterator_t4254482656_il2cpp_TypeInfo_var);
		AttributeIterator__ctor_m626920394(L_7, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_005b:
	{
		BaseIterator_t1327316739 * L_8 = ___iter0;
		ChildIterator_t3087618016 * L_9 = (ChildIterator_t3087618016 *)il2cpp_codegen_object_new(ChildIterator_t3087618016_il2cpp_TypeInfo_var);
		ChildIterator__ctor_m1815034314(L_9, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0062:
	{
		BaseIterator_t1327316739 * L_10 = ___iter0;
		DescendantIterator_t928464879 * L_11 = (DescendantIterator_t928464879 *)il2cpp_codegen_object_new(DescendantIterator_t928464879_il2cpp_TypeInfo_var);
		DescendantIterator__ctor_m2310076573(L_11, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0069:
	{
		BaseIterator_t1327316739 * L_12 = ___iter0;
		DescendantOrSelfIterator_t1437473246 * L_13 = (DescendantOrSelfIterator_t1437473246 *)il2cpp_codegen_object_new(DescendantOrSelfIterator_t1437473246_il2cpp_TypeInfo_var);
		DescendantOrSelfIterator__ctor_m135009740(L_13, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0070:
	{
		BaseIterator_t1327316739 * L_14 = ___iter0;
		FollowingIterator_t3149710293 * L_15 = (FollowingIterator_t3149710293 *)il2cpp_codegen_object_new(FollowingIterator_t3149710293_il2cpp_TypeInfo_var);
		FollowingIterator__ctor_m4068454143(L_15, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0077:
	{
		BaseIterator_t1327316739 * L_16 = ___iter0;
		FollowingSiblingIterator_t116837539 * L_17 = (FollowingSiblingIterator_t116837539 *)il2cpp_codegen_object_new(FollowingSiblingIterator_t116837539_il2cpp_TypeInfo_var);
		FollowingSiblingIterator__ctor_m3707604177(L_17, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_007e:
	{
		BaseIterator_t1327316739 * L_18 = ___iter0;
		NamespaceIterator_t1295432863 * L_19 = (NamespaceIterator_t1295432863 *)il2cpp_codegen_object_new(NamespaceIterator_t1295432863_il2cpp_TypeInfo_var);
		NamespaceIterator__ctor_m729989193(L_19, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_0085:
	{
		BaseIterator_t1327316739 * L_20 = ___iter0;
		ParentIterator_t1610549788 * L_21 = (ParentIterator_t1610549788 *)il2cpp_codegen_object_new(ParentIterator_t1610549788_il2cpp_TypeInfo_var);
		ParentIterator__ctor_m2909223434(L_21, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_008c:
	{
		BaseIterator_t1327316739 * L_22 = ___iter0;
		PrecedingIterator_t1039893511 * L_23 = (PrecedingIterator_t1039893511 *)il2cpp_codegen_object_new(PrecedingIterator_t1039893511_il2cpp_TypeInfo_var);
		PrecedingIterator__ctor_m3521034673(L_23, L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_0093:
	{
		BaseIterator_t1327316739 * L_24 = ___iter0;
		PrecedingSiblingIterator_t1318768689 * L_25 = (PrecedingSiblingIterator_t1318768689 *)il2cpp_codegen_object_new(PrecedingSiblingIterator_t1318768689_il2cpp_TypeInfo_var);
		PrecedingSiblingIterator__ctor_m3406853599(L_25, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_009a:
	{
		BaseIterator_t1327316739 * L_26 = ___iter0;
		SelfIterator_t197024894 * L_27 = (SelfIterator_t197024894 *)il2cpp_codegen_object_new(SelfIterator_t197024894_il2cpp_TypeInfo_var);
		SelfIterator__ctor_m2835358572(L_27, L_26, /*hidden argument*/NULL);
		return L_27;
	}

IL_00a1:
	{
		IndexOutOfRangeException_t3456360697 * L_28 = (IndexOutOfRangeException_t3456360697 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3456360697_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m707236112(L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}
}
// System.Void System.Xml.XPath.BaseIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void BaseIterator__ctor_m3181771313 (BaseIterator_t1327316739 * __this, BaseIterator_t1327316739 * ___other0, const MethodInfo* method)
{
	{
		XPathNodeIterator__ctor_m2186940031(__this, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_0 = ___other0;
		NullCheck(L_0);
		Il2CppObject * L_1 = L_0->get__nsm_1();
		__this->set__nsm_1(L_1);
		BaseIterator_t1327316739 * L_2 = ___other0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_position_2();
		__this->set_position_2(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.BaseIterator::.ctor(System.Xml.IXmlNamespaceResolver)
extern "C"  void BaseIterator__ctor_m3826808382 (BaseIterator_t1327316739 * __this, Il2CppObject * ___nsm0, const MethodInfo* method)
{
	{
		XPathNodeIterator__ctor_m2186940031(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___nsm0;
		__this->set__nsm_1(L_0);
		return;
	}
}
// System.Xml.IXmlNamespaceResolver System.Xml.XPath.BaseIterator::get_NamespaceManager()
extern "C"  Il2CppObject * BaseIterator_get_NamespaceManager_m3653702256 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get__nsm_1();
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.BaseIterator::get_ReverseAxis()
extern "C"  bool BaseIterator_get_ReverseAxis_m3412652795 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 System.Xml.XPath.BaseIterator::get_ComparablePosition()
extern "C"  int32_t BaseIterator_get_ComparablePosition_m1201108051 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XPath.BaseIterator::get_ReverseAxis() */, __this);
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, __this);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2))+(int32_t)1));
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0029;
	}

IL_0028:
	{
		int32_t L_4 = V_0;
		G_B4_0 = L_4;
	}

IL_0029:
	{
		return G_B4_0;
	}

IL_002a:
	{
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		return L_5;
	}
}
// System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition()
extern "C"  int32_t BaseIterator_get_CurrentPosition_m4192865460 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_position_2();
		return L_0;
	}
}
// System.Void System.Xml.XPath.BaseIterator::SetPosition(System.Int32)
extern "C"  void BaseIterator_SetPosition_m2844760437 (BaseIterator_t1327316739 * __this, int32_t ___pos0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___pos0;
		__this->set_position_2(L_0);
		return;
	}
}
// System.Boolean System.Xml.XPath.BaseIterator::MoveNext()
extern "C"  bool BaseIterator_MoveNext_m2848019493 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(13 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNextCore() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_1 = __this->get_position_2();
		__this->set_position_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		return (bool)1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.BaseIterator::PeekNext()
extern "C"  XPathNavigator_t1075073278 * BaseIterator_PeekNext_m2233508421 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	XPathNodeIterator_t1383168931 * V_0 = NULL;
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		XPathNodeIterator_t1383168931 * L_0 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		V_0 = L_0;
		XPathNodeIterator_t1383168931 * L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_1);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_3 = V_0;
		NullCheck(L_3);
		XPathNavigator_t1075073278 * L_4 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_3);
		G_B3_0 = L_4;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.String System.Xml.XPath.BaseIterator::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeType_t3637370479_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral2803145;
extern Il2CppCodeGenString* _stringLiteral32675;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t BaseIterator_ToString_m2981263858_MetadataUsageId;
extern "C"  String_t* BaseIterator_ToString_m2981263858 (BaseIterator_t1327316739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BaseIterator_ToString_m2981263858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathNavigator_t1075073278 * L_0 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, __this);
		if (!L_0)
		{
			goto IL_0071;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)7));
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, __this);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_2);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(XPathNodeType_t3637370479_il2cpp_TypeInfo_var, &L_4);
		NullCheck((Enum_t2862688501 *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_5);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, _stringLiteral91);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral91);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, _stringLiteral2803145);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2803145);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		XPathNavigator_t1075073278 * L_14 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, __this);
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_14);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_13;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, _stringLiteral32675);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32675);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		XPathNavigator_t1075073278 * L_18 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, __this);
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 6);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_20;
	}

IL_0071:
	{
		ObjectU5BU5D_t1108656482* L_21 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		Type_t * L_22 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_22);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 0);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_23);
		ObjectU5BU5D_t1108656482* L_24 = L_21;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		ArrayElementTypeCheck (L_24, _stringLiteral91);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral91);
		ObjectU5BU5D_t1108656482* L_25 = L_24;
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		int32_t L_27 = L_26;
		Il2CppObject * L_28 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 2);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_28);
		ObjectU5BU5D_t1108656482* L_29 = L_25;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		ArrayElementTypeCheck (L_29, _stringLiteral93);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m3016520001(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Void System.Xml.XPath.ChildIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ChildIterator__ctor_m1815034314 (ChildIterator_t3087618016 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	ChildIterator_t3087618016 * G_B2_0 = NULL;
	ChildIterator_t3087618016 * G_B1_0 = NULL;
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	ChildIterator_t3087618016 * G_B3_1 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_2);
		G_B1_0 = __this;
		if (L_3)
		{
			G_B2_0 = __this;
			goto IL_0023;
		}
	}
	{
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = BaseIterator_PeekNext_m2233508421(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0029;
	}

IL_0023:
	{
		BaseIterator_t1327316739 * L_6 = ___iter0;
		NullCheck(L_6);
		XPathNavigator_t1075073278 * L_7 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		G_B3_0 = L_7;
		G_B3_1 = G_B2_0;
	}

IL_0029:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__nav_3(G_B3_0);
		XPathNavigator_t1075073278 * L_8 = __this->get__nav_3();
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		XPathNavigator_t1075073278 * L_9 = __this->get__nav_3();
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren() */, L_9);
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		XPathNavigator_t1075073278 * L_11 = __this->get__nav_3();
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_11);
		__this->set__nav_3(L_12);
		goto IL_0066;
	}

IL_005f:
	{
		__this->set__nav_3((XPathNavigator_t1075073278 *)NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void System.Xml.XPath.ChildIterator::.ctor(System.Xml.XPath.ChildIterator)
extern "C"  void ChildIterator__ctor_m2771727263 (ChildIterator_t3087618016 * __this, ChildIterator_t3087618016 * ___other0, const MethodInfo* method)
{
	ChildIterator_t3087618016 * G_B2_0 = NULL;
	ChildIterator_t3087618016 * G_B1_0 = NULL;
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	ChildIterator_t3087618016 * G_B3_1 = NULL;
	{
		ChildIterator_t3087618016 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		ChildIterator_t3087618016 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get__nav_3();
		G_B1_0 = __this;
		if (L_2)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		G_B3_1 = G_B1_0;
		goto IL_0024;
	}

IL_0019:
	{
		ChildIterator_t3087618016 * L_3 = ___other0;
		NullCheck(L_3);
		XPathNavigator_t1075073278 * L_4 = L_3->get__nav_3();
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__nav_3(G_B3_0);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ChildIterator::Clone()
extern Il2CppClass* ChildIterator_t3087618016_il2cpp_TypeInfo_var;
extern const uint32_t ChildIterator_Clone_m2923315154_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * ChildIterator_Clone_m2923315154 (ChildIterator_t3087618016 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChildIterator_Clone_m2923315154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ChildIterator_t3087618016 * L_0 = (ChildIterator_t3087618016 *)il2cpp_codegen_object_new(ChildIterator_t3087618016_il2cpp_TypeInfo_var);
		ChildIterator__ctor_m2771727263(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ChildIterator::MoveNextCore()
extern "C"  bool ChildIterator_MoveNextCore_m3551044493 (ChildIterator_t3087618016 * __this, const MethodInfo* method)
{
	bool G_B5_0 = false;
	{
		XPathNavigator_t1075073278 * L_0 = __this->get__nav_3();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		XPathNavigator_t1075073278 * L_2 = __this->get__nav_3();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		G_B5_0 = L_3;
		goto IL_0033;
	}

IL_0028:
	{
		XPathNavigator_t1075073278 * L_4 = __this->get__nav_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_4);
		G_B5_0 = L_5;
	}

IL_0033:
	{
		return G_B5_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ChildIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * ChildIterator_get_Current_m3213955934 (ChildIterator_t3087618016 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (XPathNavigator_t1075073278 *)NULL;
	}

IL_000d:
	{
		XPathNavigator_t1075073278 * L_1 = __this->get__nav_3();
		return L_1;
	}
}
// System.Void System.Xml.XPath.CompiledExpression::.ctor(System.String,System.Xml.XPath.Expression)
extern "C"  void CompiledExpression__ctor_m2683771966 (CompiledExpression_t3956813165 * __this, String_t* ___raw0, Expression_t2556460284 * ___expr1, const MethodInfo* method)
{
	{
		XPathExpression__ctor_m883028087(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___expr1;
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set__expr_1(L_1);
		String_t* L_2 = ___raw0;
		__this->set_rawExpression_3(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.XmlNamespaceManager)
extern "C"  void CompiledExpression_SetContext_m1907071021 (CompiledExpression_t3956813165 * __this, XmlNamespaceManager_t1467853467 * ___nsManager0, const MethodInfo* method)
{
	{
		XmlNamespaceManager_t1467853467 * L_0 = ___nsManager0;
		__this->set__nsm_0(L_0);
		return;
	}
}
// System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.IXmlNamespaceResolver)
extern "C"  void CompiledExpression_SetContext_m1282298115 (CompiledExpression_t3956813165 * __this, Il2CppObject * ___nsResolver0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsResolver0;
		__this->set__nsm_0(L_0);
		return;
	}
}
// System.Xml.IXmlNamespaceResolver System.Xml.XPath.CompiledExpression::get_NamespaceManager()
extern "C"  Il2CppObject * CompiledExpression_get_NamespaceManager_m652200026 (CompiledExpression_t3956813165 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get__nsm_0();
		return L_0;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.CompiledExpression::EvaluateNodeSet(System.Xml.XPath.BaseIterator)
extern "C"  XPathNodeIterator_t1383168931 * CompiledExpression_EvaluateNodeSet_m2426588395 (CompiledExpression_t3956813165 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get__expr_1();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		XPathSorters_t1807339567 * L_3 = __this->get__sorters_2();
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		XPathSorters_t1807339567 * L_4 = __this->get__sorters_2();
		BaseIterator_t1327316739 * L_5 = V_0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_6 = XPathSorters_Sort_m2612507910(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0025:
	{
		BaseIterator_t1327316739 * L_7 = V_0;
		return L_7;
	}
}
// System.Void System.Xml.XPath.DescendantIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void DescendantIterator__ctor_m2310076573 (DescendantIterator_t928464879 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.DescendantIterator::.ctor(System.Xml.XPath.DescendantIterator)
extern "C"  void DescendantIterator__ctor_m2374132145 (DescendantIterator_t928464879 * __this, DescendantIterator_t928464879 * ___other0, const MethodInfo* method)
{
	{
		DescendantIterator_t928464879 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		DescendantIterator_t928464879 * L_1 = ___other0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get__depth_6();
		__this->set__depth_6(L_2);
		DescendantIterator_t928464879 * L_3 = ___other0;
		NullCheck(L_3);
		bool L_4 = L_3->get__finished_7();
		__this->set__finished_7(L_4);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.DescendantIterator::Clone()
extern Il2CppClass* DescendantIterator_t928464879_il2cpp_TypeInfo_var;
extern const uint32_t DescendantIterator_Clone_m1069770709_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * DescendantIterator_Clone_m1069770709 (DescendantIterator_t928464879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DescendantIterator_Clone_m1069770709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DescendantIterator_t928464879 * L_0 = (DescendantIterator_t928464879 *)il2cpp_codegen_object_new(DescendantIterator_t928464879_il2cpp_TypeInfo_var);
		DescendantIterator__ctor_m2374132145(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.DescendantIterator::MoveNextCore()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1194623964;
extern const uint32_t DescendantIterator_MoveNextCore_m3508326288_MetadataUsageId;
extern "C"  bool DescendantIterator_MoveNextCore_m3508326288 (DescendantIterator_t928464879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DescendantIterator_MoveNextCore_m3508326288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get__finished_7();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XPathNavigator_t1075073278 * L_1 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_1);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = __this->get__depth_6();
		__this->set__depth_6(((int32_t)((int32_t)L_3+(int32_t)1)));
		return (bool)1;
	}

IL_002d:
	{
		goto IL_007d;
	}

IL_0032:
	{
		XPathNavigator_t1075073278 * L_4 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_4);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		return (bool)1;
	}

IL_0044:
	{
		XPathNavigator_t1075073278 * L_6 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_6);
		if (L_7)
		{
			goto IL_006f;
		}
	}
	{
		XPathNavigator_t1075073278 * L_8 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m2022236990(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1194623964, L_9, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_11 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_006f:
	{
		int32_t L_12 = __this->get__depth_6();
		__this->set__depth_6(((int32_t)((int32_t)L_12-(int32_t)1)));
	}

IL_007d:
	{
		int32_t L_13 = __this->get__depth_6();
		if (L_13)
		{
			goto IL_0032;
		}
	}
	{
		__this->set__finished_7((bool)1);
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.DescendantOrSelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void DescendantOrSelfIterator__ctor_m135009740 (DescendantOrSelfIterator_t1437473246 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.DescendantOrSelfIterator::.ctor(System.Xml.XPath.DescendantOrSelfIterator)
extern "C"  void DescendantOrSelfIterator__ctor_m1458340049 (DescendantOrSelfIterator_t1437473246 * __this, DescendantOrSelfIterator_t1437473246 * ___other0, const MethodInfo* method)
{
	{
		DescendantOrSelfIterator_t1437473246 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		DescendantOrSelfIterator_t1437473246 * L_1 = ___other0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get__depth_6();
		__this->set__depth_6(L_2);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.DescendantOrSelfIterator::Clone()
extern Il2CppClass* DescendantOrSelfIterator_t1437473246_il2cpp_TypeInfo_var;
extern const uint32_t DescendantOrSelfIterator_Clone_m3088273414_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * DescendantOrSelfIterator_Clone_m3088273414 (DescendantOrSelfIterator_t1437473246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DescendantOrSelfIterator_Clone_m3088273414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DescendantOrSelfIterator_t1437473246 * L_0 = (DescendantOrSelfIterator_t1437473246 *)il2cpp_codegen_object_new(DescendantOrSelfIterator_t1437473246_il2cpp_TypeInfo_var);
		DescendantOrSelfIterator__ctor_m1458340049(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.DescendantOrSelfIterator::MoveNextCore()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1194623964;
extern const uint32_t DescendantOrSelfIterator_MoveNextCore_m34430975_MetadataUsageId;
extern "C"  bool DescendantOrSelfIterator_MoveNextCore_m34430975 (DescendantOrSelfIterator_t1437473246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DescendantOrSelfIterator_MoveNextCore_m34430975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get__finished_7();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		XPathNavigator_t1075073278 * L_2 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_4 = __this->get__depth_6();
		__this->set__depth_6(((int32_t)((int32_t)L_4+(int32_t)1)));
		return (bool)1;
	}

IL_003a:
	{
		goto IL_008a;
	}

IL_003f:
	{
		XPathNavigator_t1075073278 * L_5 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_5);
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		return (bool)1;
	}

IL_0051:
	{
		XPathNavigator_t1075073278 * L_7 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_7);
		if (L_8)
		{
			goto IL_007c;
		}
	}
	{
		XPathNavigator_t1075073278 * L_9 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m2022236990(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1194623964, L_10, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_12 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_007c:
	{
		int32_t L_13 = __this->get__depth_6();
		__this->set__depth_6(((int32_t)((int32_t)L_13-(int32_t)1)));
	}

IL_008a:
	{
		int32_t L_14 = __this->get__depth_6();
		if (L_14)
		{
			goto IL_003f;
		}
	}
	{
		__this->set__finished_7((bool)1);
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.EqualityExpr::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression,System.Boolean)
extern "C"  void EqualityExpr__ctor_m2210778824 (EqualityExpr_t1406255795 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, bool ___trueVal2, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___trueVal2;
		__this->set_trueVal_2(L_2);
		return;
	}
}
// System.Boolean System.Xml.XPath.EqualityExpr::get_StaticValueAsBoolean()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EqualityExpr_get_StaticValueAsBoolean_m405219309_MetadataUsageId;
extern "C"  bool EqualityExpr_get_StaticValueAsBoolean_m405219309 (EqualityExpr_t1406255795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityExpr_get_StaticValueAsBoolean_m405219309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)4))))
		{
			goto IL_006e;
		}
	}

IL_002f:
	{
		Expression_t2556460284 * L_5 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_5);
		Expression_t2556460284 * L_7 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_7);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_006e;
		}
	}
	{
		Expression_t2556460284 * L_9 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_9);
		XPathNavigator_t1075073278 * L_10 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(12 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.Expression::get_StaticValueAsNavigator() */, L_9);
		Expression_t2556460284 * L_11 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(12 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.Expression::get_StaticValueAsNavigator() */, L_11);
		NullCheck(L_10);
		bool L_13 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_10, L_12);
		bool L_14 = __this->get_trueVal_2();
		return (bool)((((int32_t)L_13) == ((int32_t)L_14))? 1 : 0);
	}

IL_006e:
	{
		Expression_t2556460284 * L_15 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_15);
		Expression_t2556460284 * L_17 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_17);
		if (!((int32_t)((int32_t)((((int32_t)L_16) == ((int32_t)2))? 1 : 0)|(int32_t)((((int32_t)L_18) == ((int32_t)2))? 1 : 0))))
		{
			goto IL_00b1;
		}
	}
	{
		Expression_t2556460284 * L_19 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_19);
		bool L_20 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_19);
		Expression_t2556460284 * L_21 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_21);
		bool L_22 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_21);
		bool L_23 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((int32_t)L_20) == ((int32_t)L_22))? 1 : 0)) == ((int32_t)L_23))? 1 : 0);
	}

IL_00b1:
	{
		Expression_t2556460284 * L_24 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_24);
		Expression_t2556460284 * L_26 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_26);
		if (!((int32_t)((int32_t)((((int32_t)L_25) == ((int32_t)0))? 1 : 0)|(int32_t)((((int32_t)L_27) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_00f4;
		}
	}
	{
		Expression_t2556460284 * L_28 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_28);
		double L_29 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_28);
		Expression_t2556460284 * L_30 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_30);
		double L_31 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_30);
		bool L_32 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((double)L_29) == ((double)L_31))? 1 : 0)) == ((int32_t)L_32))? 1 : 0);
	}

IL_00f4:
	{
		Expression_t2556460284 * L_33 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_33);
		int32_t L_34 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_33);
		Expression_t2556460284 * L_35 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_35);
		int32_t L_36 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_35);
		if (!((int32_t)((int32_t)((((int32_t)L_34) == ((int32_t)1))? 1 : 0)|(int32_t)((((int32_t)L_36) == ((int32_t)1))? 1 : 0))))
		{
			goto IL_013a;
		}
	}
	{
		Expression_t2556460284 * L_37 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.Expression::get_StaticValueAsString() */, L_37);
		Expression_t2556460284 * L_39 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_39);
		String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.Expression::get_StaticValueAsString() */, L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		bool L_42 = __this->get_trueVal_2();
		return (bool)((((int32_t)L_41) == ((int32_t)L_42))? 1 : 0);
	}

IL_013a:
	{
		Expression_t2556460284 * L_43 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_43);
		Il2CppObject * L_44 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, L_43);
		Expression_t2556460284 * L_45 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_45);
		Il2CppObject * L_46 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, L_45);
		bool L_47 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_44) == ((Il2CppObject*)(Il2CppObject *)L_46))? 1 : 0)) == ((int32_t)L_47))? 1 : 0);
	}
}
// System.Boolean System.Xml.XPath.EqualityExpr::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern const uint32_t EqualityExpr_EvaluateBoolean_m2691361612_MetadataUsageId;
extern "C"  bool EqualityExpr_EvaluateBoolean_m2691361612 (EqualityExpr_t1406255795 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EqualityExpr_EvaluateBoolean_m2691361612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Expression_t2556460284 * V_2 = NULL;
	Expression_t2556460284 * V_3 = NULL;
	int32_t V_4 = 0;
	BaseIterator_t1327316739 * V_5 = NULL;
	double V_6 = 0.0;
	String_t* V_7 = NULL;
	BaseIterator_t1327316739 * V_8 = NULL;
	ArrayList_t3948406897 * V_9 = NULL;
	String_t* V_10 = NULL;
	int32_t V_11 = 0;
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)5))))
		{
			goto IL_0033;
		}
	}
	{
		Expression_t2556460284 * L_7 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_8 = ___iter0;
		NullCheck(L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		int32_t L_10 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0033:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_004c;
		}
	}
	{
		Expression_t2556460284 * L_12 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_13 = ___iter0;
		NullCheck(L_12);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_12, L_13);
		int32_t L_15 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_004c:
	{
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 1;
	}

IL_0055:
	{
		int32_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)4))))
		{
			goto IL_005e;
		}
	}
	{
		V_1 = 1;
	}

IL_005e:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)3)))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_01f7;
		}
	}

IL_006c:
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)3)))
		{
			goto IL_008e;
		}
	}
	{
		Expression_t2556460284 * L_21 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		V_2 = L_21;
		Expression_t2556460284 * L_22 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		V_3 = L_22;
		int32_t L_23 = V_0;
		V_4 = L_23;
		int32_t L_24 = V_1;
		V_0 = L_24;
		int32_t L_25 = V_4;
		V_1 = L_25;
		goto IL_009c;
	}

IL_008e:
	{
		Expression_t2556460284 * L_26 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		V_2 = L_26;
		Expression_t2556460284 * L_27 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		V_3 = L_27;
	}

IL_009c:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_00bc;
		}
	}
	{
		Expression_t2556460284 * L_29 = V_2;
		BaseIterator_t1327316739 * L_30 = ___iter0;
		NullCheck(L_29);
		bool L_31 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_29, L_30);
		Expression_t2556460284 * L_32 = V_3;
		BaseIterator_t1327316739 * L_33 = ___iter0;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_32, L_33);
		bool L_35 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((int32_t)L_31) == ((int32_t)L_34))? 1 : 0)) == ((int32_t)L_35))? 1 : 0);
	}

IL_00bc:
	{
		Expression_t2556460284 * L_36 = V_2;
		BaseIterator_t1327316739 * L_37 = ___iter0;
		NullCheck(L_36);
		BaseIterator_t1327316739 * L_38 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_36, L_37);
		V_5 = L_38;
		int32_t L_39 = V_1;
		if (L_39)
		{
			goto IL_010c;
		}
	}
	{
		Expression_t2556460284 * L_40 = V_3;
		BaseIterator_t1327316739 * L_41 = ___iter0;
		NullCheck(L_40);
		double L_42 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_40, L_41);
		V_6 = L_42;
		goto IL_00fb;
	}

IL_00d9:
	{
		BaseIterator_t1327316739 * L_43 = V_5;
		NullCheck(L_43);
		XPathNavigator_t1075073278 * L_44 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_43);
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_44);
		double L_46 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		double L_47 = V_6;
		bool L_48 = __this->get_trueVal_2();
		if ((!(((uint32_t)((((double)L_46) == ((double)L_47))? 1 : 0)) == ((uint32_t)L_48))))
		{
			goto IL_00fb;
		}
	}
	{
		return (bool)1;
	}

IL_00fb:
	{
		BaseIterator_t1327316739 * L_49 = V_5;
		NullCheck(L_49);
		bool L_50 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_49);
		if (L_50)
		{
			goto IL_00d9;
		}
	}
	{
		goto IL_01f5;
	}

IL_010c:
	{
		int32_t L_51 = V_1;
		if ((!(((uint32_t)L_51) == ((uint32_t)1))))
		{
			goto IL_0152;
		}
	}
	{
		Expression_t2556460284 * L_52 = V_3;
		BaseIterator_t1327316739 * L_53 = ___iter0;
		NullCheck(L_52);
		String_t* L_54 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_52, L_53);
		V_7 = L_54;
		goto IL_0141;
	}

IL_0121:
	{
		BaseIterator_t1327316739 * L_55 = V_5;
		NullCheck(L_55);
		XPathNavigator_t1075073278 * L_56 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_55);
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_56);
		String_t* L_58 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_59 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		bool L_60 = __this->get_trueVal_2();
		if ((!(((uint32_t)L_59) == ((uint32_t)L_60))))
		{
			goto IL_0141;
		}
	}
	{
		return (bool)1;
	}

IL_0141:
	{
		BaseIterator_t1327316739 * L_61 = V_5;
		NullCheck(L_61);
		bool L_62 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_61);
		if (L_62)
		{
			goto IL_0121;
		}
	}
	{
		goto IL_01f5;
	}

IL_0152:
	{
		int32_t L_63 = V_1;
		if ((!(((uint32_t)L_63) == ((uint32_t)3))))
		{
			goto IL_01f5;
		}
	}
	{
		Expression_t2556460284 * L_64 = V_3;
		BaseIterator_t1327316739 * L_65 = ___iter0;
		NullCheck(L_64);
		BaseIterator_t1327316739 * L_66 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_64, L_65);
		V_8 = L_66;
		ArrayList_t3948406897 * L_67 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_67, /*hidden argument*/NULL);
		V_9 = L_67;
		goto IL_0187;
	}

IL_016e:
	{
		ArrayList_t3948406897 * L_68 = V_9;
		BaseIterator_t1327316739 * L_69 = V_5;
		NullCheck(L_69);
		XPathNavigator_t1075073278 * L_70 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_69);
		NullCheck(L_70);
		String_t* L_71 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_70);
		String_t* L_72 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_68, L_72);
	}

IL_0187:
	{
		BaseIterator_t1327316739 * L_73 = V_5;
		NullCheck(L_73);
		bool L_74 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_73);
		if (L_74)
		{
			goto IL_016e;
		}
	}
	{
		goto IL_01e9;
	}

IL_0198:
	{
		BaseIterator_t1327316739 * L_75 = V_8;
		NullCheck(L_75);
		XPathNavigator_t1075073278 * L_76 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_75);
		NullCheck(L_76);
		String_t* L_77 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_76);
		String_t* L_78 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		V_10 = L_78;
		V_11 = 0;
		goto IL_01db;
	}

IL_01b3:
	{
		String_t* L_79 = V_10;
		ArrayList_t3948406897 * L_80 = V_9;
		int32_t L_81 = V_11;
		NullCheck(L_80);
		Il2CppObject * L_82 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_80, L_81);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_83 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_79, ((String_t*)CastclassSealed(L_82, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_84 = __this->get_trueVal_2();
		if ((!(((uint32_t)L_83) == ((uint32_t)L_84))))
		{
			goto IL_01d5;
		}
	}
	{
		return (bool)1;
	}

IL_01d5:
	{
		int32_t L_85 = V_11;
		V_11 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_01db:
	{
		int32_t L_86 = V_11;
		ArrayList_t3948406897 * L_87 = V_9;
		NullCheck(L_87);
		int32_t L_88 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_87);
		if ((((int32_t)L_86) < ((int32_t)L_88)))
		{
			goto IL_01b3;
		}
	}

IL_01e9:
	{
		BaseIterator_t1327316739 * L_89 = V_8;
		NullCheck(L_89);
		bool L_90 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_89);
		if (L_90)
		{
			goto IL_0198;
		}
	}

IL_01f5:
	{
		return (bool)0;
	}

IL_01f7:
	{
		int32_t L_91 = V_0;
		if ((((int32_t)L_91) == ((int32_t)2)))
		{
			goto IL_0205;
		}
	}
	{
		int32_t L_92 = V_1;
		if ((!(((uint32_t)L_92) == ((uint32_t)2))))
		{
			goto IL_0228;
		}
	}

IL_0205:
	{
		Expression_t2556460284 * L_93 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_94 = ___iter0;
		NullCheck(L_93);
		bool L_95 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_93, L_94);
		Expression_t2556460284 * L_96 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_97 = ___iter0;
		NullCheck(L_96);
		bool L_98 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_96, L_97);
		bool L_99 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((int32_t)L_95) == ((int32_t)L_98))? 1 : 0)) == ((int32_t)L_99))? 1 : 0);
	}

IL_0228:
	{
		int32_t L_100 = V_0;
		if (!L_100)
		{
			goto IL_0234;
		}
	}
	{
		int32_t L_101 = V_1;
		if (L_101)
		{
			goto IL_0257;
		}
	}

IL_0234:
	{
		Expression_t2556460284 * L_102 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_103 = ___iter0;
		NullCheck(L_102);
		double L_104 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_102, L_103);
		Expression_t2556460284 * L_105 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_106 = ___iter0;
		NullCheck(L_105);
		double L_107 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_105, L_106);
		bool L_108 = __this->get_trueVal_2();
		return (bool)((((int32_t)((((double)L_104) == ((double)L_107))? 1 : 0)) == ((int32_t)L_108))? 1 : 0);
	}

IL_0257:
	{
		Expression_t2556460284 * L_109 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_110 = ___iter0;
		NullCheck(L_109);
		String_t* L_111 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_109, L_110);
		Expression_t2556460284 * L_112 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_113 = ___iter0;
		NullCheck(L_112);
		String_t* L_114 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_112, L_113);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_115 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_111, L_114, /*hidden argument*/NULL);
		bool L_116 = __this->get_trueVal_2();
		return (bool)((((int32_t)L_115) == ((int32_t)L_116))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprAND::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprAND__ctor_m2970784246 (ExprAND_t3747310744 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprAND::get_Operator()
extern Il2CppCodeGenString* _stringLiteral96727;
extern const uint32_t ExprAND_get_Operator_m1892457770_MetadataUsageId;
extern "C"  String_t* ExprAND_get_Operator_m1892457770 (ExprAND_t3747310744 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprAND_get_Operator_m1892457770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral96727;
	}
}
// System.Boolean System.Xml.XPath.ExprAND::get_StaticValueAsBoolean()
extern "C"  bool ExprAND_get_StaticValueAsBoolean_m122471774 (ExprAND_t3747310744 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_1);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_3);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B4_0 = 0;
	}

IL_0029:
	{
		G_B6_0 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean System.Xml.XPath.ExprAND::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool ExprAND_EvaluateBoolean_m3911476797 (ExprAND_t3747310744 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return L_5;
	}
}
// System.Void System.Xml.XPath.ExprBinary::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprBinary__ctor_m18595484 (ExprBinary_t1545048250 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___left0;
		__this->set__left_0(L_0);
		Expression_t2556460284 * L_1 = ___right1;
		__this->set__right_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprBinary::Optimize()
extern "C"  Expression_t2556460284 * ExprBinary_Optimize_m4062507817 (ExprBinary_t1545048250 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__left_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set__left_0(L_1);
		Expression_t2556460284 * L_2 = __this->get__right_1();
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set__right_1(L_3);
		return __this;
	}
}
// System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue()
extern "C"  bool ExprBinary_get_HasStaticValue_m3219323882 (ExprBinary_t1545048250 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get__left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get__right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.ExprBinary::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ExprBinary_ToString_m1633004777_MetadataUsageId;
extern "C"  String_t* ExprBinary_ToString_m1633004777 (ExprBinary_t1545048250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprBinary_ToString_m1633004777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)5));
		Expression_t2556460284 * L_1 = __this->get__left_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_0;
		Il2CppChar L_4 = ((Il2CppChar)((int32_t)32));
		Il2CppObject * L_5 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_3;
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(20 /* System.String System.Xml.XPath.ExprBinary::get_Operator() */, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_6;
		Il2CppChar L_9 = ((Il2CppChar)((int32_t)32));
		Il2CppObject * L_10 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_8;
		Expression_t2556460284 * L_12 = __this->get__right_1();
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Boolean System.Xml.XPath.ExprBinary::get_Peer()
extern "C"  bool ExprBinary_get_Peer_m1571277411 (ExprBinary_t1545048250 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get__left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get__right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Xml.XPath.ExprBoolean::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprBoolean__ctor_m1871519559 (ExprBoolean_t513597673 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBinary__ctor_m18595484(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprBoolean::Optimize()
extern Il2CppClass* XPathFunctionTrue_t844510361_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionFalse_t85302738_il2cpp_TypeInfo_var;
extern const uint32_t ExprBoolean_Optimize_m1228371132_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprBoolean_Optimize_m1228371132 (ExprBoolean_t513597673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprBoolean_Optimize_m1228371132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ExprBinary_Optimize_m4062507817(__this, /*hidden argument*/NULL);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, __this);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		XPathFunctionTrue_t844510361 * L_2 = (XPathFunctionTrue_t844510361 *)il2cpp_codegen_object_new(XPathFunctionTrue_t844510361_il2cpp_TypeInfo_var);
		XPathFunctionTrue__ctor_m3889883154(L_2, (FunctionArguments_t2391178772 *)NULL, /*hidden argument*/NULL);
		return L_2;
	}

IL_0026:
	{
		XPathFunctionFalse_t85302738 * L_3 = (XPathFunctionFalse_t85302738 *)il2cpp_codegen_object_new(XPathFunctionFalse_t85302738_il2cpp_TypeInfo_var);
		XPathFunctionFalse__ctor_m1496753845(L_3, (FunctionArguments_t2391178772 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprBoolean::get_ReturnType()
extern "C"  int32_t ExprBoolean_get_ReturnType_m1961480482 (ExprBoolean_t513597673 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Object System.Xml.XPath.ExprBoolean::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t ExprBoolean_Evaluate_m1996826835_MetadataUsageId;
extern "C"  Il2CppObject * ExprBoolean_Evaluate_m1996826835 (ExprBoolean_t513597673 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprBoolean_Evaluate_m1996826835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Double System.Xml.XPath.ExprBoolean::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprBoolean_EvaluateNumber_m3660430300 (ExprBoolean_t513597673 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return (((double)((double)G_B3_0)));
	}
}
// System.String System.Xml.XPath.ExprBoolean::EvaluateString(System.Xml.XPath.BaseIterator)
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t ExprBoolean_EvaluateString_m4084867668_MetadataUsageId;
extern "C"  String_t* ExprBoolean_EvaluateString_m4084867668 (ExprBoolean_t513597673 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprBoolean_EvaluateString_m4084867668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = _stringLiteral3569038;
		goto IL_001b;
	}

IL_0016:
	{
		G_B3_0 = _stringLiteral97196323;
	}

IL_001b:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.ExprDIV::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprDIV__ctor_m2833239216 (ExprDIV_t3747313490 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprDIV::get_Operator()
extern Il2CppCodeGenString* _stringLiteral32636367;
extern const uint32_t ExprDIV_get_Operator_m2934174692_MetadataUsageId;
extern "C"  String_t* ExprDIV_get_Operator_m2934174692 (ExprDIV_t3747313490 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprDIV_get_Operator_m2934174692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral32636367;
	}
}
// System.Double System.Xml.XPath.ExprDIV::get_StaticValueAsNumber()
extern "C"  double ExprDIV_get_StaticValueAsNumber_m3283915648 (ExprDIV_t3747313490 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2/(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprDIV::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprDIV_EvaluateNumber_m3529257427 (ExprDIV_t3747313490 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2/(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprEQ::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprEQ__ctor_m3423036967 (ExprEQ_t2486226757 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		EqualityExpr__ctor_m2210778824(__this, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprEQ::get_Operator()
extern Il2CppCodeGenString* _stringLiteral61;
extern const uint32_t ExprEQ_get_Operator_m537955253_MetadataUsageId;
extern "C"  String_t* ExprEQ_get_Operator_m537955253 (ExprEQ_t2486226757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprEQ_get_Operator_m537955253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral61;
	}
}
// System.Void System.Xml.XPath.Expression::.ctor()
extern "C"  void Expression__ctor_m158740418 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator)
extern "C"  int32_t Expression_GetReturnType_m2142158056 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, __this);
		return L_0;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize()
extern "C"  Expression_t2556460284 * Expression_Optimize_m1244892267 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Xml.XPath.Expression::get_HasStaticValue()
extern "C"  bool Expression_get_HasStaticValue_m4030491308 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Xml.XPath.Expression::get_StaticValue()
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t Expression_get_StaticValue_m2075908687_MetadataUsageId;
extern "C"  Il2CppObject * Expression_get_StaticValue_m2075908687 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_get_StaticValue_m2075908687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, __this);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_001e;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_001e:
	{
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.Expression::get_StaticValueAsString() */, __this);
		return L_2;
	}

IL_0025:
	{
		double L_3 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_0031:
	{
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, __this);
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}

IL_003d:
	{
		return NULL;
	}
}
// System.String System.Xml.XPath.Expression::get_StaticValueAsString()
extern "C"  String_t* Expression_get_StaticValueAsString_m1531184512 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, __this);
		String_t* L_2 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber()
extern "C"  double Expression_get_StaticValueAsNumber_m2129608440 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, __this);
		double L_2 = XPathFunctions_ToNumber_m1220011083(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0024;
	}

IL_001b:
	{
		G_B3_0 = (0.0);
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean()
extern "C"  bool Expression_get_StaticValueAsBoolean_m1506253110 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, __this);
		bool L_2 = XPathFunctions_ToBoolean_m2654560037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.Expression::get_StaticValueAsNavigator()
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t Expression_get_StaticValueAsNavigator_m486487163_MetadataUsageId;
extern "C"  XPathNavigator_t1075073278 * Expression_get_StaticValueAsNavigator_m486487163 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_get_StaticValueAsNavigator_m486487163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, __this);
		return ((XPathNavigator_t1075073278 *)IsInstClass(L_0, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2849115671_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* NullIterator_t2905335737_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathResultType_t516720010_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3872709465;
extern const uint32_t Expression_EvaluateNodeSet_m3337318680_MetadataUsageId;
extern "C"  BaseIterator_t1327316739 * Expression_EvaluateNodeSet_m3337318680 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_EvaluateNodeSet_m3337318680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	XPathNodeIterator_t1383168931 * V_2 = NULL;
	BaseIterator_t1327316739 * V_3 = NULL;
	XPathNavigator_t1075073278 * V_4 = NULL;
	XPathNodeIterator_t1383168931 * V_5 = NULL;
	int32_t V_6 = 0;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		int32_t L_1 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, __this, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_6 = L_2;
		int32_t L_3 = V_6;
		if (((int32_t)((int32_t)L_3-(int32_t)3)) == 0)
		{
			goto IL_0025;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)3)) == 1)
		{
			goto IL_0025;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)3)) == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_00b6;
	}

IL_0025:
	{
		BaseIterator_t1327316739 * L_4 = ___iter0;
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, __this, L_4);
		V_1 = L_5;
		Il2CppObject * L_6 = V_1;
		V_2 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_6, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		V_3 = (BaseIterator_t1327316739 *)NULL;
		XPathNodeIterator_t1383168931 * L_7 = V_2;
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_8 = V_2;
		V_3 = ((BaseIterator_t1327316739 *)IsInstClass(L_8, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_9 = V_3;
		if (L_9)
		{
			goto IL_0056;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_10 = V_2;
		BaseIterator_t1327316739 * L_11 = ___iter0;
		NullCheck(L_11);
		Il2CppObject * L_12 = BaseIterator_get_NamespaceManager_m3653702256(L_11, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_13 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_13, L_10, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
	}

IL_0056:
	{
		BaseIterator_t1327316739 * L_14 = V_3;
		return L_14;
	}

IL_0058:
	{
		Il2CppObject * L_15 = V_1;
		V_4 = ((XPathNavigator_t1075073278 *)IsInstClass(L_15, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		XPathNavigator_t1075073278 * L_16 = V_4;
		if (!L_16)
		{
			goto IL_0095;
		}
	}
	{
		XPathNavigator_t1075073278 * L_17 = V_4;
		NullCheck(L_17);
		XPathNodeIterator_t1383168931 * L_18 = VirtFuncInvoker1< XPathNodeIterator_t1383168931 *, int32_t >::Invoke(33 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType) */, L_17, ((int32_t)9));
		V_5 = L_18;
		XPathNodeIterator_t1383168931 * L_19 = V_5;
		V_3 = ((BaseIterator_t1327316739 *)IsInstClass(L_19, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_20 = V_3;
		if (L_20)
		{
			goto IL_0095;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_21 = V_5;
		if (!L_21)
		{
			goto IL_0095;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_22 = V_5;
		BaseIterator_t1327316739 * L_23 = ___iter0;
		NullCheck(L_23);
		Il2CppObject * L_24 = BaseIterator_get_NamespaceManager_m3653702256(L_23, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_25 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_25, L_22, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
	}

IL_0095:
	{
		BaseIterator_t1327316739 * L_26 = V_3;
		if (!L_26)
		{
			goto IL_009d;
		}
	}
	{
		BaseIterator_t1327316739 * L_27 = V_3;
		return L_27;
	}

IL_009d:
	{
		Il2CppObject * L_28 = V_1;
		if (L_28)
		{
			goto IL_00aa;
		}
	}
	{
		BaseIterator_t1327316739 * L_29 = ___iter0;
		NullIterator_t2905335737 * L_30 = (NullIterator_t2905335737 *)il2cpp_codegen_object_new(NullIterator_t2905335737_il2cpp_TypeInfo_var);
		NullIterator__ctor_m2854644327(L_30, L_29, /*hidden argument*/NULL);
		return L_30;
	}

IL_00aa:
	{
		Il2CppObject * L_31 = V_1;
		int32_t L_32 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_00b6;
	}

IL_00b6:
	{
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		int32_t L_34 = V_0;
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(XPathResultType_t516720010_il2cpp_TypeInfo_var, &L_35);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3872709465, L_33, L_36, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_38 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_38, L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1549035285;
extern const uint32_t Expression_GetReturnType_m2715398160_MetadataUsageId;
extern "C"  int32_t Expression_GetReturnType_m2715398160 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_GetReturnType_m2715398160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(1);
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_1, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		return (int32_t)(2);
	}

IL_001a:
	{
		Il2CppObject * L_2 = ___obj0;
		if (!((XPathNodeIterator_t1383168931 *)IsInstClass(L_2, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var)))
		{
			goto IL_0027;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0027:
	{
		Il2CppObject * L_3 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_3, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_003d;
		}
	}
	{
		Il2CppObject * L_4 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t1153838500_il2cpp_TypeInfo_var)))
		{
			goto IL_003f;
		}
	}

IL_003d:
	{
		return (int32_t)(0);
	}

IL_003f:
	{
		Il2CppObject * L_5 = ___obj0;
		if (!((XPathNavigator_t1075073278 *)IsInstClass(L_5, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)))
		{
			goto IL_004c;
		}
	}
	{
		return (int32_t)(4);
	}

IL_004c:
	{
		Il2CppObject * L_6 = ___obj0;
		NullCheck(L_6);
		Type_t * L_7 = Object_GetType_m2022236990(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1549035285, L_8, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_10 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}
}
// System.Boolean System.Xml.XPath.Expression::get_Peer()
extern "C"  bool Expression_get_Peer_m3048629157 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* IConvertible_t2116191568_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2490994607;
extern const uint32_t Expression_EvaluateNumber_m339636555_MetadataUsageId;
extern "C"  double Expression_EvaluateNumber_m339636555 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_EvaluateNumber_m339636555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	double G_B15_0 = 0.0;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		int32_t L_1 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, __this, L_0);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_001e;
		}
	}
	{
		BaseIterator_t1327316739 * L_3 = ___iter0;
		String_t* L_4 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, __this, L_3);
		V_0 = L_4;
		V_1 = 1;
		goto IL_0026;
	}

IL_001e:
	{
		BaseIterator_t1327316739 * L_5 = ___iter0;
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, __this, L_5);
		V_0 = L_6;
	}

IL_0026:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)5))))
		{
			goto IL_0034;
		}
	}
	{
		Il2CppObject * L_8 = V_0;
		int32_t L_9 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0034:
	{
		int32_t L_10 = V_1;
		V_2 = L_10;
		int32_t L_11 = V_2;
		if (L_11 == 0)
		{
			goto IL_0055;
		}
		if (L_11 == 1)
		{
			goto IL_00ba;
		}
		if (L_11 == 2)
		{
			goto IL_008a;
		}
		if (L_11 == 3)
		{
			goto IL_00ad;
		}
		if (L_11 == 4)
		{
			goto IL_00c6;
		}
	}
	{
		goto IL_00d7;
	}

IL_0055:
	{
		Il2CppObject * L_12 = V_0;
		if (!((Il2CppObject *)IsInstSealed(L_12, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0067;
		}
	}
	{
		Il2CppObject * L_13 = V_0;
		return ((*(double*)((double*)UnBox (L_13, Double_t3868226565_il2cpp_TypeInfo_var))));
	}

IL_0067:
	{
		Il2CppObject * L_14 = V_0;
		if (!((Il2CppObject *)IsInst(L_14, IConvertible_t2116191568_il2cpp_TypeInfo_var)))
		{
			goto IL_0083;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_16 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((Il2CppObject *)Castclass(L_15, IConvertible_t2116191568_il2cpp_TypeInfo_var)));
		double L_17 = InterfaceFuncInvoker1< double, Il2CppObject * >::Invoke(6 /* System.Double System.IConvertible::ToDouble(System.IFormatProvider) */, IConvertible_t2116191568_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_15, IConvertible_t2116191568_il2cpp_TypeInfo_var)), L_16);
		return L_17;
	}

IL_0083:
	{
		Il2CppObject * L_18 = V_0;
		return ((*(double*)((double*)UnBox (L_18, Double_t3868226565_il2cpp_TypeInfo_var))));
	}

IL_008a:
	{
		Il2CppObject * L_19 = V_0;
		if (!((*(bool*)((bool*)UnBox (L_19, Boolean_t476798718_il2cpp_TypeInfo_var)))))
		{
			goto IL_00a3;
		}
	}
	{
		G_B15_0 = (1.0);
		goto IL_00ac;
	}

IL_00a3:
	{
		G_B15_0 = (0.0);
	}

IL_00ac:
	{
		return G_B15_0;
	}

IL_00ad:
	{
		BaseIterator_t1327316739 * L_20 = ___iter0;
		String_t* L_21 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, __this, L_20);
		double L_22 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_00ba:
	{
		Il2CppObject * L_23 = V_0;
		double L_24 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_23, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_24;
	}

IL_00c6:
	{
		Il2CppObject * L_25 = V_0;
		NullCheck(((XPathNavigator_t1075073278 *)CastclassClass(L_25, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, ((XPathNavigator_t1075073278 *)CastclassClass(L_25, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		double L_27 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return L_27;
	}

IL_00d7:
	{
		XPathException_t1803876086 * L_28 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_28, _stringLiteral2490994607, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}
}
// System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral2490994607;
extern const uint32_t Expression_EvaluateString_m3743041347_MetadataUsageId;
extern "C"  String_t* Expression_EvaluateString_m3743041347 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_EvaluateString_m3743041347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	double V_2 = 0.0;
	BaseIterator_t1327316739 * V_3 = NULL;
	int32_t V_4 = 0;
	String_t* G_B8_0 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, __this, L_0);
		V_0 = L_1;
		BaseIterator_t1327316739 * L_2 = ___iter0;
		int32_t L_3 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, __this, L_2);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)5))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_5 = V_0;
		int32_t L_6 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_001e:
	{
		int32_t L_7 = V_1;
		V_4 = L_7;
		int32_t L_8 = V_4;
		if (L_8 == 0)
		{
			goto IL_0041;
		}
		if (L_8 == 1)
		{
			goto IL_006a;
		}
		if (L_8 == 2)
		{
			goto IL_004f;
		}
		if (L_8 == 3)
		{
			goto IL_0071;
		}
		if (L_8 == 4)
		{
			goto IL_009b;
		}
	}
	{
		goto IL_00a7;
	}

IL_0041:
	{
		Il2CppObject * L_9 = V_0;
		V_2 = ((*(double*)((double*)UnBox (L_9, Double_t3868226565_il2cpp_TypeInfo_var))));
		double L_10 = V_2;
		String_t* L_11 = XPathFunctions_ToString_m1520227313(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_004f:
	{
		Il2CppObject * L_12 = V_0;
		if (!((*(bool*)((bool*)UnBox (L_12, Boolean_t476798718_il2cpp_TypeInfo_var)))))
		{
			goto IL_0064;
		}
	}
	{
		G_B8_0 = _stringLiteral3569038;
		goto IL_0069;
	}

IL_0064:
	{
		G_B8_0 = _stringLiteral97196323;
	}

IL_0069:
	{
		return G_B8_0;
	}

IL_006a:
	{
		Il2CppObject * L_13 = V_0;
		return ((String_t*)CastclassSealed(L_13, String_t_il2cpp_TypeInfo_var));
	}

IL_0071:
	{
		Il2CppObject * L_14 = V_0;
		V_3 = ((BaseIterator_t1327316739 *)CastclassClass(L_14, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_15 = V_3;
		if (!L_15)
		{
			goto IL_0089;
		}
	}
	{
		BaseIterator_t1327316739 * L_16 = V_3;
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_16);
		if (L_17)
		{
			goto IL_008f;
		}
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_18;
	}

IL_008f:
	{
		BaseIterator_t1327316739 * L_19 = V_3;
		NullCheck(L_19);
		XPathNavigator_t1075073278 * L_20 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_19);
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_20);
		return L_21;
	}

IL_009b:
	{
		Il2CppObject * L_22 = V_0;
		NullCheck(((XPathNavigator_t1075073278 *)CastclassClass(L_22, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, ((XPathNavigator_t1075073278 *)CastclassClass(L_22, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		return L_23;
	}

IL_00a7:
	{
		XPathException_t1803876086 * L_24 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_24, _stringLiteral2490994607, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2490994607;
extern const uint32_t Expression_EvaluateBoolean_m3993427477_MetadataUsageId;
extern "C"  bool Expression_EvaluateBoolean_m3993427477 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_EvaluateBoolean_m3993427477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	double V_2 = 0.0;
	BaseIterator_t1327316739 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B14_0 = 0;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, __this, L_0);
		V_0 = L_1;
		BaseIterator_t1327316739 * L_2 = ___iter0;
		int32_t L_3 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, __this, L_2);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)5))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_5 = V_0;
		int32_t L_6 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_001e:
	{
		int32_t L_7 = V_1;
		V_4 = L_7;
		int32_t L_8 = V_4;
		if (L_8 == 0)
		{
			goto IL_0041;
		}
		if (L_8 == 1)
		{
			goto IL_007a;
		}
		if (L_8 == 2)
		{
			goto IL_0073;
		}
		if (L_8 == 3)
		{
			goto IL_008c;
		}
		if (L_8 == 4)
		{
			goto IL_00a3;
		}
	}
	{
		goto IL_00af;
	}

IL_0041:
	{
		Il2CppObject * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_10 = Convert_ToDouble_m1941295007(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		double L_11 = V_2;
		if ((((double)L_11) == ((double)(0.0))))
		{
			goto IL_0071;
		}
	}
	{
		double L_12 = V_2;
		if ((((double)L_12) == ((double)(0.0))))
		{
			goto IL_0071;
		}
	}
	{
		double L_13 = V_2;
		bool L_14 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
		goto IL_0072;
	}

IL_0071:
	{
		G_B8_0 = 0;
	}

IL_0072:
	{
		return (bool)G_B8_0;
	}

IL_0073:
	{
		Il2CppObject * L_15 = V_0;
		return ((*(bool*)((bool*)UnBox (L_15, Boolean_t476798718_il2cpp_TypeInfo_var))));
	}

IL_007a:
	{
		Il2CppObject * L_16 = V_0;
		NullCheck(((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var)));
		int32_t L_17 = String_get_Length_m2979997331(((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_17) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_008c:
	{
		Il2CppObject * L_18 = V_0;
		V_3 = ((BaseIterator_t1327316739 *)CastclassClass(L_18, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_19 = V_3;
		if (!L_19)
		{
			goto IL_00a1;
		}
	}
	{
		BaseIterator_t1327316739 * L_20 = V_3;
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_20);
		G_B14_0 = ((int32_t)(L_21));
		goto IL_00a2;
	}

IL_00a1:
	{
		G_B14_0 = 0;
	}

IL_00a2:
	{
		return (bool)G_B14_0;
	}

IL_00a3:
	{
		Il2CppObject * L_22 = V_0;
		NullCheck(((XPathNavigator_t1075073278 *)CastclassClass(L_22, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren() */, ((XPathNavigator_t1075073278 *)CastclassClass(L_22, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		return L_23;
	}

IL_00af:
	{
		XPathException_t1803876086 * L_24 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_24, _stringLiteral2490994607, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}
}
// System.Object System.Xml.XPath.Expression::EvaluateAs(System.Xml.XPath.BaseIterator,System.Xml.XPath.XPathResultType)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t Expression_EvaluateAs_m2173512581_MetadataUsageId;
extern "C"  Il2CppObject * Expression_EvaluateAs_m2173512581 (Expression_t2556460284 * __this, BaseIterator_t1327316739 * ___iter0, int32_t ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Expression_EvaluateAs_m2173512581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___type1;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003a;
		}
		if (L_1 == 1)
		{
			goto IL_0032;
		}
		if (L_1 == 2)
		{
			goto IL_001d;
		}
		if (L_1 == 3)
		{
			goto IL_002a;
		}
	}
	{
		goto IL_0047;
	}

IL_001d:
	{
		BaseIterator_t1327316739 * L_2 = ___iter0;
		bool L_3 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_2);
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_002a:
	{
		BaseIterator_t1327316739 * L_6 = ___iter0;
		BaseIterator_t1327316739 * L_7 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, __this, L_6);
		return L_7;
	}

IL_0032:
	{
		BaseIterator_t1327316739 * L_8 = ___iter0;
		String_t* L_9 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, __this, L_8);
		return L_9;
	}

IL_003a:
	{
		BaseIterator_t1327316739 * L_10 = ___iter0;
		double L_11 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, __this, L_10);
		double L_12 = L_11;
		Il2CppObject * L_13 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_12);
		return L_13;
	}

IL_0047:
	{
		BaseIterator_t1327316739 * L_14 = ___iter0;
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, __this, L_14);
		return L_15;
	}
}
// System.Boolean System.Xml.XPath.Expression::get_RequireSorting()
extern "C"  bool Expression_get_RequireSorting_m1499585762 (Expression_t2556460284 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.ExprFilter::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprFilter__ctor_m1987528211 (ExprFilter_t1659523121 * __this, Expression_t2556460284 * ___expr0, Expression_t2556460284 * ___pred1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___expr0;
		__this->set_expr_0(L_0);
		Expression_t2556460284 * L_1 = ___pred1;
		__this->set_pred_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprFilter::Optimize()
extern "C"  Expression_t2556460284 * ExprFilter_Optimize_m4005115936 (ExprFilter_t1659523121 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_expr_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_expr_0(L_1);
		Expression_t2556460284 * L_2 = __this->get_pred_1();
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_pred_1(L_3);
		return __this;
	}
}
// System.String System.Xml.XPath.ExprFilter::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral1362;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t ExprFilter_ToString_m1575612896_MetadataUsageId;
extern "C"  String_t* ExprFilter_ToString_m1575612896 (ExprFilter_t1659523121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_ToString_m1575612896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral40);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral40);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_expr_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral1362);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1362);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_pred_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral93);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Object System.Xml.XPath.ExprFilter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* PredicateIterator_t3158177019_il2cpp_TypeInfo_var;
extern const uint32_t ExprFilter_Evaluate_m1209514577_MetadataUsageId;
extern "C"  Il2CppObject * ExprFilter_Evaluate_m1209514577 (ExprFilter_t1659523121 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_Evaluate_m1209514577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_expr_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		BaseIterator_t1327316739 * L_3 = V_0;
		Expression_t2556460284 * L_4 = __this->get_pred_1();
		PredicateIterator_t3158177019 * L_5 = (PredicateIterator_t3158177019 *)il2cpp_codegen_object_new(PredicateIterator_t3158177019_il2cpp_TypeInfo_var);
		PredicateIterator__ctor_m3004206002(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.ExprFilter::get_Peer()
extern "C"  bool ExprFilter_get_Peer_m1513885530 (ExprFilter_t1659523121 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_pred_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprFilter::get_Subtree()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern const uint32_t ExprFilter_get_Subtree_m1459955336_MetadataUsageId;
extern "C"  bool ExprFilter_get_Subtree_m1459955336 (ExprFilter_t1659523121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFilter_get_Subtree_m1459955336_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t2875795446 * V_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_expr_0();
		V_0 = ((NodeSet_t2875795446 *)IsInstClass(L_0, NodeSet_t2875795446_il2cpp_TypeInfo_var));
		NodeSet_t2875795446 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return (bool)G_B3_0;
	}
}
// System.Void System.Xml.XPath.ExprFunctionCall::.ctor(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var;
extern const uint32_t ExprFunctionCall__ctor_m3125265142_MetadataUsageId;
extern "C"  void ExprFunctionCall__ctor_m3125265142 (ExprFunctionCall_t3990388239 * __this, XmlQualifiedName_t2133315502 * ___name0, FunctionArguments_t2391178772 * ___args1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall__ctor_m3125265142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t3948406897 * L_0 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set__args_2(L_0);
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___ctx2;
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_2 = ___ctx2;
		XmlQualifiedName_t2133315502 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_3);
		NullCheck(L_2);
		XmlQualifiedName_t2133315502 * L_5 = InterfaceFuncInvoker1< XmlQualifiedName_t2133315502 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var, L_2, L_4);
		___name0 = L_5;
		__this->set_resolvedName_1((bool)1);
	}

IL_002c:
	{
		XmlQualifiedName_t2133315502 * L_6 = ___name0;
		__this->set__name_0(L_6);
		FunctionArguments_t2391178772 * L_7 = ___args1;
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		FunctionArguments_t2391178772 * L_8 = ___args1;
		ArrayList_t3948406897 * L_9 = __this->get__args_2();
		NullCheck(L_8);
		FunctionArguments_ToArrayList_m227079145(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprFunctionCall::Factory(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1974256870_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLast_t844255649_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionPosition_t3054018868_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionCount_t82957758_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t1025767334_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLocalName_t1256488005_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNamespaceUri_t4291495708_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionName_t844315030_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionString_t2723009436_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionConcat_t2260200095_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionStartsWith_t1437129026_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionContains_t1738767914_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstringBefore_t1709434719_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstringAfter_t1726871446_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSubstring_t2992425472_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionStringLength_t1116358498_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNormalizeSpace_t1100613508_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionTranslate_t3514715389_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionBoolean_t149146711_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNot_t1422750722_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionTrue_t844510361_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionFalse_t85302738_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionLang_t844255481_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionNumber_t2580631252_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionSum_t1422755706_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionFloor_t85633211_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionCeil_t843991056_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionRound_t96810557_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4235384975_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m337170132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3314326;
extern Il2CppCodeGenString* _stringLiteral747804969;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1257351085;
extern Il2CppCodeGenString* _stringLiteral4094257658;
extern Il2CppCodeGenString* _stringLiteral3373707;
extern Il2CppCodeGenString* _stringLiteral3402981393;
extern Il2CppCodeGenString* _stringLiteral2940172052;
extern Il2CppCodeGenString* _stringLiteral3279525058;
extern Il2CppCodeGenString* _stringLiteral3727521311;
extern Il2CppCodeGenString* _stringLiteral2021305979;
extern Il2CppCodeGenString* _stringLiteral2835269472;
extern Il2CppCodeGenString* _stringLiteral530542161;
extern Il2CppCodeGenString* _stringLiteral2899918882;
extern Il2CppCodeGenString* _stringLiteral94053958;
extern Il2CppCodeGenString* _stringLiteral1052832078;
extern Il2CppCodeGenString* _stringLiteral64711720;
extern Il2CppCodeGenString* _stringLiteral109267;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3314158;
extern Il2CppCodeGenString* _stringLiteral3260603209;
extern Il2CppCodeGenString* _stringLiteral114251;
extern Il2CppCodeGenString* _stringLiteral97526796;
extern Il2CppCodeGenString* _stringLiteral660387005;
extern Il2CppCodeGenString* _stringLiteral108704142;
extern const uint32_t ExprFunctionCall_Factory_m1613910802_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprFunctionCall_Factory_m1613910802 (Il2CppObject * __this /* static, unused */, XmlQualifiedName_t2133315502 * ___name0, FunctionArguments_t2391178772 * ___args1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_Factory_m1613910802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t1974256870 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		XmlQualifiedName_t2133315502 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2987642414(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2987642414(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_6 = ___name0;
		FunctionArguments_t2391178772 * L_7 = ___args1;
		Il2CppObject * L_8 = ___ctx2;
		ExprFunctionCall_t3990388239 * L_9 = (ExprFunctionCall_t3990388239 *)il2cpp_codegen_object_new(ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var);
		ExprFunctionCall__ctor_m3125265142(L_9, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0029:
	{
		XmlQualifiedName_t2133315502 * L_10 = ___name0;
		NullCheck(L_10);
		String_t* L_11 = XmlQualifiedName_get_Name_m607016698(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = V_0;
		if (!L_12)
		{
			goto IL_02ea;
		}
	}
	{
		Dictionary_2_t1974256870 * L_13 = ((ExprFunctionCall_t3990388239_StaticFields*)ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map41_3();
		if (L_13)
		{
			goto IL_01a4;
		}
	}
	{
		Dictionary_2_t1974256870 * L_14 = (Dictionary_2_t1974256870 *)il2cpp_codegen_object_new(Dictionary_2_t1974256870_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_14, ((int32_t)27), /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_14;
		Dictionary_2_t1974256870 * L_15 = V_1;
		NullCheck(L_15);
		Dictionary_2_Add_m4235384975(L_15, _stringLiteral3314326, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_16 = V_1;
		NullCheck(L_16);
		Dictionary_2_Add_m4235384975(L_16, _stringLiteral747804969, 1, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_17 = V_1;
		NullCheck(L_17);
		Dictionary_2_Add_m4235384975(L_17, _stringLiteral94851343, 2, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_18 = V_1;
		NullCheck(L_18);
		Dictionary_2_Add_m4235384975(L_18, _stringLiteral3355, 3, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_19 = V_1;
		NullCheck(L_19);
		Dictionary_2_Add_m4235384975(L_19, _stringLiteral1257351085, 4, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_20 = V_1;
		NullCheck(L_20);
		Dictionary_2_Add_m4235384975(L_20, _stringLiteral4094257658, 5, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_21 = V_1;
		NullCheck(L_21);
		Dictionary_2_Add_m4235384975(L_21, _stringLiteral3373707, 6, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_22 = V_1;
		NullCheck(L_22);
		Dictionary_2_Add_m4235384975(L_22, _stringLiteral3402981393, 7, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_23 = V_1;
		NullCheck(L_23);
		Dictionary_2_Add_m4235384975(L_23, _stringLiteral2940172052, 8, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_24 = V_1;
		NullCheck(L_24);
		Dictionary_2_Add_m4235384975(L_24, _stringLiteral3279525058, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_25 = V_1;
		NullCheck(L_25);
		Dictionary_2_Add_m4235384975(L_25, _stringLiteral3727521311, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_26 = V_1;
		NullCheck(L_26);
		Dictionary_2_Add_m4235384975(L_26, _stringLiteral2021305979, ((int32_t)11), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_27 = V_1;
		NullCheck(L_27);
		Dictionary_2_Add_m4235384975(L_27, _stringLiteral2835269472, ((int32_t)12), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_28 = V_1;
		NullCheck(L_28);
		Dictionary_2_Add_m4235384975(L_28, _stringLiteral530542161, ((int32_t)13), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_29 = V_1;
		NullCheck(L_29);
		Dictionary_2_Add_m4235384975(L_29, _stringLiteral2899918882, ((int32_t)14), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_30 = V_1;
		NullCheck(L_30);
		Dictionary_2_Add_m4235384975(L_30, _stringLiteral94053958, ((int32_t)15), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_31 = V_1;
		NullCheck(L_31);
		Dictionary_2_Add_m4235384975(L_31, _stringLiteral1052832078, ((int32_t)16), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_32 = V_1;
		NullCheck(L_32);
		Dictionary_2_Add_m4235384975(L_32, _stringLiteral64711720, ((int32_t)17), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_33 = V_1;
		NullCheck(L_33);
		Dictionary_2_Add_m4235384975(L_33, _stringLiteral109267, ((int32_t)18), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_34 = V_1;
		NullCheck(L_34);
		Dictionary_2_Add_m4235384975(L_34, _stringLiteral3569038, ((int32_t)19), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_35 = V_1;
		NullCheck(L_35);
		Dictionary_2_Add_m4235384975(L_35, _stringLiteral97196323, ((int32_t)20), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_36 = V_1;
		NullCheck(L_36);
		Dictionary_2_Add_m4235384975(L_36, _stringLiteral3314158, ((int32_t)21), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_37 = V_1;
		NullCheck(L_37);
		Dictionary_2_Add_m4235384975(L_37, _stringLiteral3260603209, ((int32_t)22), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_38 = V_1;
		NullCheck(L_38);
		Dictionary_2_Add_m4235384975(L_38, _stringLiteral114251, ((int32_t)23), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_39 = V_1;
		NullCheck(L_39);
		Dictionary_2_Add_m4235384975(L_39, _stringLiteral97526796, ((int32_t)24), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_40 = V_1;
		NullCheck(L_40);
		Dictionary_2_Add_m4235384975(L_40, _stringLiteral660387005, ((int32_t)25), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_41 = V_1;
		NullCheck(L_41);
		Dictionary_2_Add_m4235384975(L_41, _stringLiteral108704142, ((int32_t)26), /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_42 = V_1;
		((ExprFunctionCall_t3990388239_StaticFields*)ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map41_3(L_42);
	}

IL_01a4:
	{
		Dictionary_2_t1974256870 * L_43 = ((ExprFunctionCall_t3990388239_StaticFields*)ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map41_3();
		String_t* L_44 = V_0;
		NullCheck(L_43);
		bool L_45 = Dictionary_2_TryGetValue_m337170132(L_43, L_44, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m337170132_MethodInfo_var);
		if (!L_45)
		{
			goto IL_02ea;
		}
	}
	{
		int32_t L_46 = V_2;
		if (L_46 == 0)
		{
			goto IL_022d;
		}
		if (L_46 == 1)
		{
			goto IL_0234;
		}
		if (L_46 == 2)
		{
			goto IL_023b;
		}
		if (L_46 == 3)
		{
			goto IL_0242;
		}
		if (L_46 == 4)
		{
			goto IL_0249;
		}
		if (L_46 == 5)
		{
			goto IL_0250;
		}
		if (L_46 == 6)
		{
			goto IL_0257;
		}
		if (L_46 == 7)
		{
			goto IL_025e;
		}
		if (L_46 == 8)
		{
			goto IL_0265;
		}
		if (L_46 == 9)
		{
			goto IL_026c;
		}
		if (L_46 == 10)
		{
			goto IL_0273;
		}
		if (L_46 == 11)
		{
			goto IL_027a;
		}
		if (L_46 == 12)
		{
			goto IL_0281;
		}
		if (L_46 == 13)
		{
			goto IL_0288;
		}
		if (L_46 == 14)
		{
			goto IL_028f;
		}
		if (L_46 == 15)
		{
			goto IL_0296;
		}
		if (L_46 == 16)
		{
			goto IL_029d;
		}
		if (L_46 == 17)
		{
			goto IL_02a4;
		}
		if (L_46 == 18)
		{
			goto IL_02ab;
		}
		if (L_46 == 19)
		{
			goto IL_02b2;
		}
		if (L_46 == 20)
		{
			goto IL_02b9;
		}
		if (L_46 == 21)
		{
			goto IL_02c0;
		}
		if (L_46 == 22)
		{
			goto IL_02c7;
		}
		if (L_46 == 23)
		{
			goto IL_02ce;
		}
		if (L_46 == 24)
		{
			goto IL_02d5;
		}
		if (L_46 == 25)
		{
			goto IL_02dc;
		}
		if (L_46 == 26)
		{
			goto IL_02e3;
		}
	}
	{
		goto IL_02ea;
	}

IL_022d:
	{
		FunctionArguments_t2391178772 * L_47 = ___args1;
		XPathFunctionLast_t844255649 * L_48 = (XPathFunctionLast_t844255649 *)il2cpp_codegen_object_new(XPathFunctionLast_t844255649_il2cpp_TypeInfo_var);
		XPathFunctionLast__ctor_m565310986(L_48, L_47, /*hidden argument*/NULL);
		return L_48;
	}

IL_0234:
	{
		FunctionArguments_t2391178772 * L_49 = ___args1;
		XPathFunctionPosition_t3054018868 * L_50 = (XPathFunctionPosition_t3054018868 *)il2cpp_codegen_object_new(XPathFunctionPosition_t3054018868_il2cpp_TypeInfo_var);
		XPathFunctionPosition__ctor_m545736599(L_50, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_023b:
	{
		FunctionArguments_t2391178772 * L_51 = ___args1;
		XPathFunctionCount_t82957758 * L_52 = (XPathFunctionCount_t82957758 *)il2cpp_codegen_object_new(XPathFunctionCount_t82957758_il2cpp_TypeInfo_var);
		XPathFunctionCount__ctor_m74396489(L_52, L_51, /*hidden argument*/NULL);
		return L_52;
	}

IL_0242:
	{
		FunctionArguments_t2391178772 * L_53 = ___args1;
		XPathFunctionId_t1025767334 * L_54 = (XPathFunctionId_t1025767334 *)il2cpp_codegen_object_new(XPathFunctionId_t1025767334_il2cpp_TypeInfo_var);
		XPathFunctionId__ctor_m3649212261(L_54, L_53, /*hidden argument*/NULL);
		return L_54;
	}

IL_0249:
	{
		FunctionArguments_t2391178772 * L_55 = ___args1;
		XPathFunctionLocalName_t1256488005 * L_56 = (XPathFunctionLocalName_t1256488005 *)il2cpp_codegen_object_new(XPathFunctionLocalName_t1256488005_il2cpp_TypeInfo_var);
		XPathFunctionLocalName__ctor_m2097528674(L_56, L_55, /*hidden argument*/NULL);
		return L_56;
	}

IL_0250:
	{
		FunctionArguments_t2391178772 * L_57 = ___args1;
		XPathFunctionNamespaceUri_t4291495708 * L_58 = (XPathFunctionNamespaceUri_t4291495708 *)il2cpp_codegen_object_new(XPathFunctionNamespaceUri_t4291495708_il2cpp_TypeInfo_var);
		XPathFunctionNamespaceUri__ctor_m503725999(L_58, L_57, /*hidden argument*/NULL);
		return L_58;
	}

IL_0257:
	{
		FunctionArguments_t2391178772 * L_59 = ___args1;
		XPathFunctionName_t844315030 * L_60 = (XPathFunctionName_t844315030 *)il2cpp_codegen_object_new(XPathFunctionName_t844315030_il2cpp_TypeInfo_var);
		XPathFunctionName__ctor_m17270517(L_60, L_59, /*hidden argument*/NULL);
		return L_60;
	}

IL_025e:
	{
		FunctionArguments_t2391178772 * L_61 = ___args1;
		XPathFunctionString_t2723009436 * L_62 = (XPathFunctionString_t2723009436 *)il2cpp_codegen_object_new(XPathFunctionString_t2723009436_il2cpp_TypeInfo_var);
		XPathFunctionString__ctor_m3584382127(L_62, L_61, /*hidden argument*/NULL);
		return L_62;
	}

IL_0265:
	{
		FunctionArguments_t2391178772 * L_63 = ___args1;
		XPathFunctionConcat_t2260200095 * L_64 = (XPathFunctionConcat_t2260200095 *)il2cpp_codegen_object_new(XPathFunctionConcat_t2260200095_il2cpp_TypeInfo_var);
		XPathFunctionConcat__ctor_m2570590668(L_64, L_63, /*hidden argument*/NULL);
		return L_64;
	}

IL_026c:
	{
		FunctionArguments_t2391178772 * L_65 = ___args1;
		XPathFunctionStartsWith_t1437129026 * L_66 = (XPathFunctionStartsWith_t1437129026 *)il2cpp_codegen_object_new(XPathFunctionStartsWith_t1437129026_il2cpp_TypeInfo_var);
		XPathFunctionStartsWith__ctor_m3911729737(L_66, L_65, /*hidden argument*/NULL);
		return L_66;
	}

IL_0273:
	{
		FunctionArguments_t2391178772 * L_67 = ___args1;
		XPathFunctionContains_t1738767914 * L_68 = (XPathFunctionContains_t1738767914 *)il2cpp_codegen_object_new(XPathFunctionContains_t1738767914_il2cpp_TypeInfo_var);
		XPathFunctionContains__ctor_m2892508385(L_68, L_67, /*hidden argument*/NULL);
		return L_68;
	}

IL_027a:
	{
		FunctionArguments_t2391178772 * L_69 = ___args1;
		XPathFunctionSubstringBefore_t1709434719 * L_70 = (XPathFunctionSubstringBefore_t1709434719 *)il2cpp_codegen_object_new(XPathFunctionSubstringBefore_t1709434719_il2cpp_TypeInfo_var);
		XPathFunctionSubstringBefore__ctor_m498616712(L_70, L_69, /*hidden argument*/NULL);
		return L_70;
	}

IL_0281:
	{
		FunctionArguments_t2391178772 * L_71 = ___args1;
		XPathFunctionSubstringAfter_t1726871446 * L_72 = (XPathFunctionSubstringAfter_t1726871446 *)il2cpp_codegen_object_new(XPathFunctionSubstringAfter_t1726871446_il2cpp_TypeInfo_var);
		XPathFunctionSubstringAfter__ctor_m4187633269(L_72, L_71, /*hidden argument*/NULL);
		return L_72;
	}

IL_0288:
	{
		FunctionArguments_t2391178772 * L_73 = ___args1;
		XPathFunctionSubstring_t2992425472 * L_74 = (XPathFunctionSubstring_t2992425472 *)il2cpp_codegen_object_new(XPathFunctionSubstring_t2992425472_il2cpp_TypeInfo_var);
		XPathFunctionSubstring__ctor_m2565884615(L_74, L_73, /*hidden argument*/NULL);
		return L_74;
	}

IL_028f:
	{
		FunctionArguments_t2391178772 * L_75 = ___args1;
		XPathFunctionStringLength_t1116358498 * L_76 = (XPathFunctionStringLength_t1116358498 *)il2cpp_codegen_object_new(XPathFunctionStringLength_t1116358498_il2cpp_TypeInfo_var);
		XPathFunctionStringLength__ctor_m2699284393(L_76, L_75, /*hidden argument*/NULL);
		return L_76;
	}

IL_0296:
	{
		FunctionArguments_t2391178772 * L_77 = ___args1;
		XPathFunctionNormalizeSpace_t1100613508 * L_78 = (XPathFunctionNormalizeSpace_t1100613508 *)il2cpp_codegen_object_new(XPathFunctionNormalizeSpace_t1100613508_il2cpp_TypeInfo_var);
		XPathFunctionNormalizeSpace__ctor_m3177496007(L_78, L_77, /*hidden argument*/NULL);
		return L_78;
	}

IL_029d:
	{
		FunctionArguments_t2391178772 * L_79 = ___args1;
		XPathFunctionTranslate_t3514715389 * L_80 = (XPathFunctionTranslate_t3514715389 *)il2cpp_codegen_object_new(XPathFunctionTranslate_t3514715389_il2cpp_TypeInfo_var);
		XPathFunctionTranslate__ctor_m1616227242(L_80, L_79, /*hidden argument*/NULL);
		return L_80;
	}

IL_02a4:
	{
		FunctionArguments_t2391178772 * L_81 = ___args1;
		XPathFunctionBoolean_t149146711 * L_82 = (XPathFunctionBoolean_t149146711 *)il2cpp_codegen_object_new(XPathFunctionBoolean_t149146711_il2cpp_TypeInfo_var);
		XPathFunctionBoolean__ctor_m2311468944(L_82, L_81, /*hidden argument*/NULL);
		return L_82;
	}

IL_02ab:
	{
		FunctionArguments_t2391178772 * L_83 = ___args1;
		XPathFunctionNot_t1422750722 * L_84 = (XPathFunctionNot_t1422750722 *)il2cpp_codegen_object_new(XPathFunctionNot_t1422750722_il2cpp_TypeInfo_var);
		XPathFunctionNot__ctor_m1396109829(L_84, L_83, /*hidden argument*/NULL);
		return L_84;
	}

IL_02b2:
	{
		FunctionArguments_t2391178772 * L_85 = ___args1;
		XPathFunctionTrue_t844510361 * L_86 = (XPathFunctionTrue_t844510361 *)il2cpp_codegen_object_new(XPathFunctionTrue_t844510361_il2cpp_TypeInfo_var);
		XPathFunctionTrue__ctor_m3889883154(L_86, L_85, /*hidden argument*/NULL);
		return L_86;
	}

IL_02b9:
	{
		FunctionArguments_t2391178772 * L_87 = ___args1;
		XPathFunctionFalse_t85302738 * L_88 = (XPathFunctionFalse_t85302738 *)il2cpp_codegen_object_new(XPathFunctionFalse_t85302738_il2cpp_TypeInfo_var);
		XPathFunctionFalse__ctor_m1496753845(L_88, L_87, /*hidden argument*/NULL);
		return L_88;
	}

IL_02c0:
	{
		FunctionArguments_t2391178772 * L_89 = ___args1;
		XPathFunctionLang_t844255481 * L_90 = (XPathFunctionLang_t844255481 *)il2cpp_codegen_object_new(XPathFunctionLang_t844255481_il2cpp_TypeInfo_var);
		XPathFunctionLang__ctor_m915197874(L_90, L_89, /*hidden argument*/NULL);
		return L_90;
	}

IL_02c7:
	{
		FunctionArguments_t2391178772 * L_91 = ___args1;
		XPathFunctionNumber_t2580631252 * L_92 = (XPathFunctionNumber_t2580631252 *)il2cpp_codegen_object_new(XPathFunctionNumber_t2580631252_il2cpp_TypeInfo_var);
		XPathFunctionNumber__ctor_m2761557623(L_92, L_91, /*hidden argument*/NULL);
		return L_92;
	}

IL_02ce:
	{
		FunctionArguments_t2391178772 * L_93 = ___args1;
		XPathFunctionSum_t1422755706 * L_94 = (XPathFunctionSum_t1422755706 *)il2cpp_codegen_object_new(XPathFunctionSum_t1422755706_il2cpp_TypeInfo_var);
		XPathFunctionSum__ctor_m1037722509(L_94, L_93, /*hidden argument*/NULL);
		return L_94;
	}

IL_02d5:
	{
		FunctionArguments_t2391178772 * L_95 = ___args1;
		XPathFunctionFloor_t85633211 * L_96 = (XPathFunctionFloor_t85633211 *)il2cpp_codegen_object_new(XPathFunctionFloor_t85633211_il2cpp_TypeInfo_var);
		XPathFunctionFloor__ctor_m224085036(L_96, L_95, /*hidden argument*/NULL);
		return L_96;
	}

IL_02dc:
	{
		FunctionArguments_t2391178772 * L_97 = ___args1;
		XPathFunctionCeil_t843991056 * L_98 = (XPathFunctionCeil_t843991056 *)il2cpp_codegen_object_new(XPathFunctionCeil_t843991056_il2cpp_TypeInfo_var);
		XPathFunctionCeil__ctor_m3298422843(L_98, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_02e3:
	{
		FunctionArguments_t2391178772 * L_99 = ___args1;
		XPathFunctionRound_t96810557 * L_100 = (XPathFunctionRound_t96810557 *)il2cpp_codegen_object_new(XPathFunctionRound_t96810557_il2cpp_TypeInfo_var);
		XPathFunctionRound__ctor_m948443498(L_100, L_99, /*hidden argument*/NULL);
		return L_100;
	}

IL_02ea:
	{
		XmlQualifiedName_t2133315502 * L_101 = ___name0;
		FunctionArguments_t2391178772 * L_102 = ___args1;
		Il2CppObject * L_103 = ___ctx2;
		ExprFunctionCall_t3990388239 * L_104 = (ExprFunctionCall_t3990388239 *)il2cpp_codegen_object_new(ExprFunctionCall_t3990388239_il2cpp_TypeInfo_var);
		ExprFunctionCall__ctor_m3125265142(L_104, L_101, L_102, L_103, /*hidden argument*/NULL);
		return L_104;
	}
}
// System.String System.Xml.XPath.ExprFunctionCall::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t2556460284_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t ExprFunctionCall_ToString_m3998080894_MetadataUsageId;
extern "C"  String_t* ExprFunctionCall_ToString_m3998080894 (ExprFunctionCall_t3990388239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_ToString_m3998080894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Expression_t2556460284 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_004c;
	}

IL_000d:
	{
		ArrayList_t3948406897 * L_1 = __this->get__args_2();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, L_2);
		V_2 = ((Expression_t2556460284 *)CastclassClass(L_3, Expression_t2556460284_il2cpp_TypeInfo_var));
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, L_7, _stringLiteral1396, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003b:
	{
		String_t* L_9 = V_0;
		Expression_t2556460284 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_14 = V_1;
		ArrayList_t3948406897 * L_15 = __this->get__args_2();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_17 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		XmlQualifiedName_t2133315502 * L_18 = __this->get__name_0();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		ObjectU5BU5D_t1108656482* L_20 = L_17;
		Il2CppChar L_21 = ((Il2CppChar)((int32_t)40));
		Il2CppObject * L_22 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 1);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_20;
		String_t* L_24 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_24);
		ObjectU5BU5D_t1108656482* L_25 = L_23;
		Il2CppChar L_26 = ((Il2CppChar)((int32_t)41));
		Il2CppObject * L_27 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m3016520001(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprFunctionCall::get_ReturnType()
extern "C"  int32_t ExprFunctionCall_get_ReturnType_m3858789124 (ExprFunctionCall_t3990388239 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprFunctionCall::GetReturnType(System.Xml.XPath.BaseIterator)
extern "C"  int32_t ExprFunctionCall_GetReturnType_m2079911483 (ExprFunctionCall_t3990388239 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType[] System.Xml.XPath.ExprFunctionCall::GetArgTypes(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathResultTypeU5BU5D_t1178308879_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t2556460284_il2cpp_TypeInfo_var;
extern const uint32_t ExprFunctionCall_GetArgTypes_m669661892_MetadataUsageId;
extern "C"  XPathResultTypeU5BU5D_t1178308879* ExprFunctionCall_GetArgTypes_m669661892 (ExprFunctionCall_t3990388239 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_GetArgTypes_m669661892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathResultTypeU5BU5D_t1178308879* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ArrayList_t3948406897 * L_0 = __this->get__args_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = ((XPathResultTypeU5BU5D_t1178308879*)SZArrayNew(XPathResultTypeU5BU5D_t1178308879_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		goto IL_0036;
	}

IL_0018:
	{
		XPathResultTypeU5BU5D_t1178308879* L_2 = V_0;
		int32_t L_3 = V_1;
		ArrayList_t3948406897 * L_4 = __this->get__args_2();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(((Expression_t2556460284 *)CastclassClass(L_6, Expression_t2556460284_il2cpp_TypeInfo_var)));
		int32_t L_8 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, ((Expression_t2556460284 *)CastclassClass(L_6, Expression_t2556460284_il2cpp_TypeInfo_var)), L_7);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int32_t)L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_1;
		ArrayList_t3948406897 * L_11 = __this->get__args_2();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0018;
		}
	}
	{
		XPathResultTypeU5BU5D_t1178308879* L_13 = V_0;
		return L_13;
	}
}
// System.Object System.Xml.XPath.ExprFunctionCall::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XsltContext_t894076946_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* IXsltContextFunction_t1712234343_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t2556460284_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4154394440;
extern Il2CppCodeGenString* _stringLiteral2113392597;
extern const uint32_t ExprFunctionCall_Evaluate_m3216499699_MetadataUsageId;
extern "C"  Il2CppObject * ExprFunctionCall_Evaluate_m3216499699 (ExprFunctionCall_t3990388239 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprFunctionCall_Evaluate_m3216499699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathResultTypeU5BU5D_t1178308879* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	XsltContext_t894076946 * V_2 = NULL;
	ObjectU5BU5D_t1108656482* V_3 = NULL;
	XPathResultTypeU5BU5D_t1178308879* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Expression_t2556460284 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		XPathResultTypeU5BU5D_t1178308879* L_1 = ExprFunctionCall_GetArgTypes_m669661892(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (Il2CppObject *)NULL;
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_2);
		Il2CppObject * L_3 = BaseIterator_get_NamespaceManager_m3653702256(L_2, /*hidden argument*/NULL);
		V_2 = ((XsltContext_t894076946 *)IsInstClass(L_3, XsltContext_t894076946_il2cpp_TypeInfo_var));
		XsltContext_t894076946 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		bool L_5 = __this->get_resolvedName_1();
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		XsltContext_t894076946 * L_6 = V_2;
		XmlQualifiedName_t2133315502 * L_7 = __this->get__name_0();
		XPathResultTypeU5BU5D_t1178308879* L_8 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_9 = VirtFuncInvoker2< Il2CppObject *, XmlQualifiedName_t2133315502 *, XPathResultTypeU5BU5D_t1178308879* >::Invoke(18 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[]) */, L_6, L_7, L_8);
		V_1 = L_9;
		goto IL_0058;
	}

IL_003a:
	{
		XsltContext_t894076946 * L_10 = V_2;
		XmlQualifiedName_t2133315502 * L_11 = __this->get__name_0();
		NullCheck(L_11);
		String_t* L_12 = XmlQualifiedName_get_Namespace_m2987642414(L_11, /*hidden argument*/NULL);
		XmlQualifiedName_t2133315502 * L_13 = __this->get__name_0();
		NullCheck(L_13);
		String_t* L_14 = XmlQualifiedName_get_Name_m607016698(L_13, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t1178308879* L_15 = V_0;
		NullCheck(L_10);
		Il2CppObject * L_16 = VirtFuncInvoker3< Il2CppObject *, String_t*, String_t*, XPathResultTypeU5BU5D_t1178308879* >::Invoke(15 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, L_10, L_12, L_14, L_15);
		V_1 = L_16;
	}

IL_0058:
	{
		Il2CppObject * L_17 = V_1;
		if (L_17)
		{
			goto IL_007e;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_18 = __this->get__name_0();
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4154394440, L_19, _stringLiteral2113392597, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_21 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_007e:
	{
		ArrayList_t3948406897 * L_22 = __this->get__args_2();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_22);
		V_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)L_23));
		Il2CppObject * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Xml.Xsl.IXsltContextFunction::get_Maxargs() */, IXsltContextFunction_t1712234343_il2cpp_TypeInfo_var, L_24);
		if (!L_25)
		{
			goto IL_0119;
		}
	}
	{
		Il2CppObject * L_26 = V_1;
		NullCheck(L_26);
		XPathResultTypeU5BU5D_t1178308879* L_27 = InterfaceFuncInvoker0< XPathResultTypeU5BU5D_t1178308879* >::Invoke(0 /* System.Xml.XPath.XPathResultType[] System.Xml.Xsl.IXsltContextFunction::get_ArgTypes() */, IXsltContextFunction_t1712234343_il2cpp_TypeInfo_var, L_26);
		V_4 = L_27;
		V_5 = 0;
		goto IL_0107;
	}

IL_00aa:
	{
		XPathResultTypeU5BU5D_t1178308879* L_28 = V_4;
		if (L_28)
		{
			goto IL_00b9;
		}
	}
	{
		V_6 = 5;
		goto IL_00db;
	}

IL_00b9:
	{
		int32_t L_29 = V_5;
		XPathResultTypeU5BU5D_t1178308879* L_30 = V_4;
		NullCheck(L_30);
		if ((((int32_t)L_29) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_00d0;
		}
	}
	{
		XPathResultTypeU5BU5D_t1178308879* L_31 = V_4;
		int32_t L_32 = V_5;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		int32_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_6 = L_34;
		goto IL_00db;
	}

IL_00d0:
	{
		XPathResultTypeU5BU5D_t1178308879* L_35 = V_4;
		XPathResultTypeU5BU5D_t1178308879* L_36 = V_4;
		NullCheck(L_36);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length))))-(int32_t)1)));
		int32_t L_37 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length))))-(int32_t)1));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		V_6 = L_38;
	}

IL_00db:
	{
		ArrayList_t3948406897 * L_39 = __this->get__args_2();
		int32_t L_40 = V_5;
		NullCheck(L_39);
		Il2CppObject * L_41 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_39, L_40);
		V_7 = ((Expression_t2556460284 *)CastclassClass(L_41, Expression_t2556460284_il2cpp_TypeInfo_var));
		Expression_t2556460284 * L_42 = V_7;
		BaseIterator_t1327316739 * L_43 = ___iter0;
		int32_t L_44 = V_6;
		NullCheck(L_42);
		Il2CppObject * L_45 = Expression_EvaluateAs_m2173512581(L_42, L_43, L_44, /*hidden argument*/NULL);
		V_8 = L_45;
		ObjectU5BU5D_t1108656482* L_46 = V_3;
		int32_t L_47 = V_5;
		Il2CppObject * L_48 = V_8;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		ArrayElementTypeCheck (L_46, L_48);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(L_47), (Il2CppObject *)L_48);
		int32_t L_49 = V_5;
		V_5 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_0107:
	{
		int32_t L_50 = V_5;
		ArrayList_t3948406897 * L_51 = __this->get__args_2();
		NullCheck(L_51);
		int32_t L_52 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_51);
		if ((((int32_t)L_50) < ((int32_t)L_52)))
		{
			goto IL_00aa;
		}
	}

IL_0119:
	{
		Il2CppObject * L_53 = V_1;
		XsltContext_t894076946 * L_54 = V_2;
		ObjectU5BU5D_t1108656482* L_55 = V_3;
		BaseIterator_t1327316739 * L_56 = ___iter0;
		NullCheck(L_56);
		XPathNavigator_t1075073278 * L_57 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_56);
		NullCheck(L_53);
		Il2CppObject * L_58 = InterfaceFuncInvoker3< Il2CppObject *, XsltContext_t894076946 *, ObjectU5BU5D_t1108656482*, XPathNavigator_t1075073278 * >::Invoke(2 /* System.Object System.Xml.Xsl.IXsltContextFunction::Invoke(System.Xml.Xsl.XsltContext,System.Object[],System.Xml.XPath.XPathNavigator) */, IXsltContextFunction_t1712234343_il2cpp_TypeInfo_var, L_53, L_54, L_55, L_57);
		return L_58;
	}
}
// System.Boolean System.Xml.XPath.ExprFunctionCall::get_Peer()
extern "C"  bool ExprFunctionCall_get_Peer_m569283768 (ExprFunctionCall_t3990388239 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.ExprGE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGE__ctor_m4218213977 (ExprGE_t2486226807 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprGE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1983;
extern const uint32_t ExprGE_get_Operator_m3778930663_MetadataUsageId;
extern "C"  String_t* ExprGE_get_Operator_m3778930663 (ExprGE_t2486226807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprGE_get_Operator_m3778930663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1983;
	}
}
// System.Boolean System.Xml.XPath.ExprGE::Compare(System.Double,System.Double)
extern "C"  bool ExprGE_Compare_m601870988 (ExprGE_t2486226807 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((int32_t)((!(((double)L_0) >= ((double)L_1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprGT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGT__ctor_m2309283432 (ExprGT_t2486226822 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprGT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral62;
extern const uint32_t ExprGT_get_Operator_m2603739638_MetadataUsageId;
extern "C"  String_t* ExprGT_get_Operator_m2603739638 (ExprGT_t2486226822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprGT_get_Operator_m2603739638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral62;
	}
}
// System.Boolean System.Xml.XPath.ExprGT::Compare(System.Double,System.Double)
extern "C"  bool ExprGT_Compare_m3941486427 (ExprGT_t2486226822 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((double)L_0) > ((double)L_1))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprLE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprLE__ctor_m240811764 (ExprLE_t2486226962 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprLE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1922;
extern const uint32_t ExprLE_get_Operator_m3088536194_MetadataUsageId;
extern "C"  String_t* ExprLE_get_Operator_m3088536194 (ExprLE_t2486226962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLE_get_Operator_m3088536194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1922;
	}
}
// System.Boolean System.Xml.XPath.ExprLE::Compare(System.Double,System.Double)
extern "C"  bool ExprLE_Compare_m3614803687 (ExprLE_t2486226962 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((int32_t)((!(((double)L_0) <= ((double)L_1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprLiteral::.ctor(System.String)
extern "C"  void ExprLiteral__ctor_m1598703472 (ExprLiteral_t631346544 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set__value_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprLiteral::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t ExprLiteral_ToString_m242797249_MetadataUsageId;
extern "C"  String_t* ExprLiteral_ToString_m242797249 (ExprLiteral_t631346544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLiteral_ToString_m242797249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get__value_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral39, L_0, _stringLiteral39, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprLiteral::get_ReturnType()
extern "C"  int32_t ExprLiteral_get_ReturnType_m2223182313 (ExprLiteral_t631346544 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.ExprLiteral::get_Peer()
extern "C"  bool ExprLiteral_get_Peer_m3129008549 (ExprLiteral_t631346544 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprLiteral::get_HasStaticValue()
extern "C"  bool ExprLiteral_get_HasStaticValue_m2424400044 (ExprLiteral_t631346544 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.String System.Xml.XPath.ExprLiteral::get_StaticValueAsString()
extern "C"  String_t* ExprLiteral_get_StaticValueAsString_m495911466 (ExprLiteral_t631346544 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Object System.Xml.XPath.ExprLiteral::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * ExprLiteral_Evaluate_m876214316 (ExprLiteral_t631346544 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.String System.Xml.XPath.ExprLiteral::EvaluateString(System.Xml.XPath.BaseIterator)
extern "C"  String_t* ExprLiteral_EvaluateString_m2275560173 (ExprLiteral_t631346544 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ExprLT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprLT__ctor_m2626848515 (ExprLT_t2486226977 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		RelationalExpr__ctor_m506647938(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprLT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral60;
extern const uint32_t ExprLT_get_Operator_m1913345169_MetadataUsageId;
extern "C"  String_t* ExprLT_get_Operator_m1913345169 (ExprLT_t2486226977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprLT_get_Operator_m1913345169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral60;
	}
}
// System.Boolean System.Xml.XPath.ExprLT::Compare(System.Double,System.Double)
extern "C"  bool ExprLT_Compare_m2659451830 (ExprLT_t2486226977 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	{
		double L_0 = ___arg10;
		double L_1 = ___arg21;
		return (bool)((((double)L_0) < ((double)L_1))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.ExprMINUS::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMINUS__ctor_m1636365327 (ExprMINUS_t623244849 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMINUS::get_Operator()
extern Il2CppCodeGenString* _stringLiteral45;
extern const uint32_t ExprMINUS_get_Operator_m3066304643_MetadataUsageId;
extern "C"  String_t* ExprMINUS_get_Operator_m3066304643 (ExprMINUS_t623244849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMINUS_get_Operator_m3066304643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral45;
	}
}
// System.Double System.Xml.XPath.ExprMINUS::get_StaticValueAsNumber()
extern "C"  double ExprMINUS_get_StaticValueAsNumber_m3648238977 (ExprMINUS_t623244849 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2-(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMINUS::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMINUS_EvaluateNumber_m1137133908 (ExprMINUS_t623244849 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2-(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprMOD::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMOD__ctor_m1750329121 (ExprMOD_t3747322307 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMOD::get_Operator()
extern Il2CppCodeGenString* _stringLiteral37;
extern const uint32_t ExprMOD_get_Operator_m3646624853_MetadataUsageId;
extern "C"  String_t* ExprMOD_get_Operator_m3646624853 (ExprMOD_t3747322307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMOD_get_Operator_m3646624853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral37;
	}
}
// System.Double System.Xml.XPath.ExprMOD::get_StaticValueAsNumber()
extern "C"  double ExprMOD_get_StaticValueAsNumber_m3539694191 (ExprMOD_t3747322307 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = (fmod(L_2, L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMOD::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMOD_EvaluateNumber_m1045250882 (ExprMOD_t3747322307 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return (fmod(L_2, L_5));
	}
}
// System.Void System.Xml.XPath.ExprMULT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprMULT__ctor_m2682740523 (ExprMULT_t4186577097 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprMULT::get_Operator()
extern Il2CppCodeGenString* _stringLiteral42;
extern const uint32_t ExprMULT_get_Operator_m1717408121_MetadataUsageId;
extern "C"  String_t* ExprMULT_get_Operator_m1717408121 (ExprMULT_t4186577097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprMULT_get_Operator_m1717408121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral42;
	}
}
// System.Double System.Xml.XPath.ExprMULT::get_StaticValueAsNumber()
extern "C"  double ExprMULT_get_StaticValueAsNumber_m3551270731 (ExprMULT_t4186577097 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2*(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprMULT::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprMULT_EvaluateNumber_m3619086878 (ExprMULT_t4186577097 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2*(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprNE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprNE__ctor_m3803811634 (ExprNE_t2486227024 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		EqualityExpr__ctor_m2210778824(__this, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprNE::get_Operator()
extern Il2CppCodeGenString* _stringLiteral1084;
extern const uint32_t ExprNE_get_Operator_m1094391488_MetadataUsageId;
extern "C"  String_t* ExprNE_get_Operator_m1094391488 (ExprNE_t2486227024 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNE_get_Operator_m1094391488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1084;
	}
}
// System.Void System.Xml.XPath.ExprNEG::.ctor(System.Xml.XPath.Expression)
extern "C"  void ExprNEG__ctor_m3726946082 (ExprNEG_t3747322961 * __this, Expression_t2556460284 * ___expr0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___expr0;
		__this->set__expr_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprNEG::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1427;
extern const uint32_t ExprNEG_ToString_m1964650658_MetadataUsageId;
extern "C"  String_t* ExprNEG_ToString_m1964650658 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_ToString_m1964650658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1427, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNEG::get_ReturnType()
extern "C"  int32_t ExprNEG_get_ReturnType_m3037696906 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprNEG::Optimize()
extern Il2CppClass* ExprNumber_t1899651074_il2cpp_TypeInfo_var;
extern const uint32_t ExprNEG_Optimize_m3524037924_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprNEG_Optimize_m3524037924 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_Optimize_m3524037924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ExprNEG_t3747322961 * G_B3_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set__expr_0(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprNEG::get_HasStaticValue() */, __this);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		G_B3_0 = __this;
		goto IL_002d;
	}

IL_0022:
	{
		double L_3 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.ExprNEG::get_StaticValueAsNumber() */, __this);
		ExprNumber_t1899651074 * L_4 = (ExprNumber_t1899651074 *)il2cpp_codegen_object_new(ExprNumber_t1899651074_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_4, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((ExprNEG_t3747322961 *)(L_4));
	}

IL_002d:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprNEG::get_Peer()
extern "C"  bool ExprNEG_get_Peer_m269107718 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.ExprNEG::get_HasStaticValue()
extern "C"  bool ExprNEG_get_HasStaticValue_m2088403917 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.ExprNEG::get_StaticValueAsNumber()
extern "C"  double ExprNEG_get_StaticValueAsNumber_m150751009 (ExprNEG_t3747322961 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get__expr_0();
		NullCheck(L_2);
		double L_3 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_2);
		G_B3_0 = ((double)((double)(-1.0)*(double)L_3));
		goto IL_0033;
	}

IL_002a:
	{
		G_B3_0 = (0.0);
	}

IL_0033:
	{
		return G_B3_0;
	}
}
// System.Object System.Xml.XPath.ExprNEG::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t ExprNEG_Evaluate_m3230994667_MetadataUsageId;
extern "C"  Il2CppObject * ExprNEG_Evaluate_m3230994667 (ExprNEG_t3747322961 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNEG_Evaluate_m3230994667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = ((-L_2));
		Il2CppObject * L_4 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Double System.Xml.XPath.ExprNEG::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprNEG_EvaluateNumber_m3791047412 (ExprNEG_t3747322961 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		return ((-L_2));
	}
}
// System.Void System.Xml.XPath.ExprNumber::.ctor(System.Double)
extern "C"  void ExprNumber__ctor_m2500538694 (ExprNumber_t1899651074 * __this, double ___value0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		double L_0 = ___value0;
		__this->set__value_0(L_0);
		return;
	}
}
// System.String System.Xml.XPath.ExprNumber::ToString()
extern "C"  String_t* ExprNumber_ToString_m3667120177 (ExprNumber_t1899651074 * __this, const MethodInfo* method)
{
	{
		double* L_0 = __this->get_address_of__value_0();
		String_t* L_1 = Double_ToString_m3380246633(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNumber::get_ReturnType()
extern "C"  int32_t ExprNumber_get_ReturnType_m3482223799 (ExprNumber_t1899651074 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.ExprNumber::get_Peer()
extern "C"  bool ExprNumber_get_Peer_m3605392811 (ExprNumber_t1899651074 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprNumber::get_HasStaticValue()
extern "C"  bool ExprNumber_get_HasStaticValue_m1751360818 (ExprNumber_t1899651074 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Double System.Xml.XPath.ExprNumber::get_StaticValueAsNumber()
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumber_get_StaticValueAsNumber_m3840256050_MetadataUsageId;
extern "C"  double ExprNumber_get_StaticValueAsNumber_m3840256050 (ExprNumber_t1899651074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumber_get_StaticValueAsNumber_m3840256050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = __this->get__value_0();
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_1);
		double L_3 = XPathFunctions_ToNumber_m1220011083(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object System.Xml.XPath.ExprNumber::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumber_Evaluate_m2340743584_MetadataUsageId;
extern "C"  Il2CppObject * ExprNumber_Evaluate_m2340743584 (ExprNumber_t1899651074 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumber_Evaluate_m2340743584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = __this->get__value_0();
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Double System.Xml.XPath.ExprNumber::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprNumber_EvaluateNumber_m2056124805 (ExprNumber_t1899651074 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		double L_0 = __this->get__value_0();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ExprNumeric::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprNumeric__ctor_m1682712108 (ExprNumeric_t2743439310 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBinary__ctor_m18595484(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprNumeric::get_ReturnType()
extern "C"  int32_t ExprNumeric_get_ReturnType_m1470918855 (ExprNumeric_t2743439310 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprNumeric::Optimize()
extern Il2CppClass* ExprNumber_t1899651074_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumeric_Optimize_m465564705_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprNumeric_Optimize_m465564705 (ExprNumeric_t2743439310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumeric_Optimize_m465564705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ExprNumeric_t2743439310 * G_B3_0 = NULL;
	{
		ExprBinary_Optimize_m4062507817(__this, /*hidden argument*/NULL);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		G_B3_0 = __this;
		goto IL_0023;
	}

IL_0018:
	{
		double L_1 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		ExprNumber_t1899651074 * L_2 = (ExprNumber_t1899651074 *)il2cpp_codegen_object_new(ExprNumber_t1899651074_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_2, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((ExprNumeric_t2743439310 *)(L_2));
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.Object System.Xml.XPath.ExprNumeric::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t ExprNumeric_Evaluate_m4274044814_MetadataUsageId;
extern "C"  Il2CppObject * ExprNumeric_Evaluate_m4274044814 (ExprNumeric_t2743439310 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprNumeric_Evaluate_m4274044814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		double L_1 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, __this, L_0);
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Void System.Xml.XPath.ExprOR::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprOR__ctor_m1067593566 (ExprOR_t2486227068 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprOR::get_Operator()
extern Il2CppCodeGenString* _stringLiteral3555;
extern const uint32_t ExprOR_get_Operator_m510476012_MetadataUsageId;
extern "C"  String_t* ExprOR_get_Operator_m510476012 (ExprOR_t2486227068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprOR_get_Operator_m510476012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3555;
	}
}
// System.Boolean System.Xml.XPath.ExprOR::get_StaticValueAsBoolean()
extern "C"  bool ExprOR_get_StaticValueAsBoolean_m1871030710 (ExprOR_t2486227068 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_1);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_3);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B4_0 = 1;
	}

IL_0029:
	{
		G_B6_0 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean System.Xml.XPath.ExprOR::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool ExprOR_EvaluateBoolean_m2492968597 (ExprOR_t2486227068 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return L_5;
	}
}
// System.Void System.Xml.XPath.ExprParens::.ctor(System.Xml.XPath.Expression)
extern "C"  void ExprParens__ctor_m2196271671 (ExprParens_t1938591074 * __this, Expression_t2556460284 * ___expr0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___expr0;
		__this->set__expr_0(L_0);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprParens::Optimize()
extern "C"  Expression_t2556460284 * ExprParens_Optimize_m4227609553 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		return __this;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_HasStaticValue()
extern "C"  bool ExprParens_get_HasStaticValue_m2344585874 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.ExprParens::get_StaticValue()
extern "C"  Il2CppObject * ExprParens_get_StaticValue_m15807529 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* System.Object System.Xml.XPath.Expression::get_StaticValue() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.ExprParens::get_StaticValueAsString()
extern "C"  String_t* ExprParens_get_StaticValueAsString_m3898008922 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.Expression::get_StaticValueAsString() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.ExprParens::get_StaticValueAsNumber()
extern "C"  double ExprParens_get_StaticValueAsNumber_m201465554 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_StaticValueAsBoolean()
extern "C"  bool ExprParens_get_StaticValueAsBoolean_m1863365788 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.ExprParens::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t ExprParens_ToString_m1798106513_MetadataUsageId;
extern "C"  String_t* ExprParens_ToString_m1798106513 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprParens_ToString_m1798106513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral40, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprParens::get_ReturnType()
extern "C"  int32_t ExprParens_get_ReturnType_m3027507735 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::get_ReturnType() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.ExprParens::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2849115671_il2cpp_TypeInfo_var;
extern Il2CppClass* ParensIterator_t3418004251_il2cpp_TypeInfo_var;
extern const uint32_t ExprParens_Evaluate_m1572685888_MetadataUsageId;
extern "C"  Il2CppObject * ExprParens_Evaluate_m1572685888 (ExprParens_t1938591074 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprParens_Evaluate_m1572685888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XPathNodeIterator_t1383168931 * V_1 = NULL;
	BaseIterator_t1327316739 * V_2 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		V_1 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_3, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_4 = V_1;
		V_2 = ((BaseIterator_t1327316739 *)IsInstClass(L_4, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_5 = V_2;
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_7 = V_1;
		BaseIterator_t1327316739 * L_8 = ___iter0;
		NullCheck(L_8);
		Il2CppObject * L_9 = BaseIterator_get_NamespaceManager_m3653702256(L_8, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_10 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_10, L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
	}

IL_0034:
	{
		BaseIterator_t1327316739 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_0041;
		}
	}
	{
		BaseIterator_t1327316739 * L_12 = V_2;
		ParensIterator_t3418004251 * L_13 = (ParensIterator_t3418004251 *)il2cpp_codegen_object_new(ParensIterator_t3418004251_il2cpp_TypeInfo_var);
		ParensIterator__ctor_m938283977(L_13, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0041:
	{
		Il2CppObject * L_14 = V_0;
		return L_14;
	}
}
// System.Boolean System.Xml.XPath.ExprParens::get_Peer()
extern "C"  bool ExprParens_get_Peer_m1736379147 (ExprParens_t1938591074 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__expr_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.ExprPLUS::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprPLUS__ctor_m693128085 (ExprPLUS_t4186658099 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprNumeric__ctor_m1682712108(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprPLUS::get_Operator()
extern Il2CppCodeGenString* _stringLiteral43;
extern const uint32_t ExprPLUS_get_Operator_m2059188707_MetadataUsageId;
extern "C"  String_t* ExprPLUS_get_Operator_m2059188707 (ExprPLUS_t4186658099 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprPLUS_get_Operator_m2059188707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral43;
	}
}
// System.Double System.Xml.XPath.ExprPLUS::get_StaticValueAsNumber()
extern "C"  double ExprPLUS_get_StaticValueAsNumber_m2404542625 (ExprPLUS_t4186658099 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		G_B3_0 = ((double)((double)L_2+(double)L_4));
		goto IL_0030;
	}

IL_0027:
	{
		G_B3_0 = (0.0);
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Double System.Xml.XPath.ExprPLUS::EvaluateNumber(System.Xml.XPath.BaseIterator)
extern "C"  double ExprPLUS_EvaluateNumber_m3849137268 (ExprPLUS_t4186658099 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return ((double)((double)L_2+(double)L_5));
	}
}
// System.Void System.Xml.XPath.ExprRoot::.ctor()
extern "C"  void ExprRoot__ctor_m3406596739 (ExprRoot_t4186752155 * __this, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.ExprRoot::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ExprRoot_ToString_m808174346_MetadataUsageId;
extern "C"  String_t* ExprRoot_ToString_m808174346 (ExprRoot_t4186752155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprRoot_ToString_m808174346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.Object System.Xml.XPath.ExprRoot::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* SelfIterator_t197024894_il2cpp_TypeInfo_var;
extern const uint32_t ExprRoot_Evaluate_m2052910567_MetadataUsageId;
extern "C"  Il2CppObject * ExprRoot_Evaluate_m2052910567 (ExprRoot_t4186752155 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprRoot_Evaluate_m2052910567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		___iter0 = ((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var));
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
	}

IL_001f:
	{
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t1075073278 * L_7 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		V_0 = L_7;
		XPathNavigator_t1075073278 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_8);
		XPathNavigator_t1075073278 * L_9 = V_0;
		BaseIterator_t1327316739 * L_10 = ___iter0;
		NullCheck(L_10);
		Il2CppObject * L_11 = BaseIterator_get_NamespaceManager_m3653702256(L_10, /*hidden argument*/NULL);
		SelfIterator_t197024894 * L_12 = (SelfIterator_t197024894 *)il2cpp_codegen_object_new(SelfIterator_t197024894_il2cpp_TypeInfo_var);
		SelfIterator__ctor_m4150585400(L_12, L_9, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean System.Xml.XPath.ExprRoot::get_Peer()
extern "C"  bool ExprRoot_get_Peer_m290743108 (ExprRoot_t4186752155 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.ExprRoot::get_Subtree()
extern "C"  bool ExprRoot_get_Subtree_m1326600798 (ExprRoot_t4186752155 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.ExprSLASH::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.NodeSet)
extern "C"  void ExprSLASH__ctor_m1380016254 (ExprSLASH_t628862782 * __this, Expression_t2556460284 * ___left0, NodeSet_t2875795446 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___left0;
		__this->set_left_0(L_0);
		NodeSet_t2875795446 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH::Optimize()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_Optimize_m3034839057_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprSLASH_Optimize_m3034839057 (ExprSLASH_t628862782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_Optimize_m3034839057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(((NodeSet_t2875795446 *)CastclassClass(L_3, NodeSet_t2875795446_il2cpp_TypeInfo_var)));
		return __this;
	}
}
// System.String System.Xml.XPath.ExprSLASH::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern const uint32_t ExprSLASH_ToString_m2156751823_MetadataUsageId;
extern "C"  String_t* ExprSLASH_ToString_m2156751823 (ExprSLASH_t628862782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_ToString_m2156751823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral47, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprSLASH::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var;
extern Il2CppClass* SlashIterator_t384731969_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedIterator_t4014821679_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_Evaluate_m2117643614_MetadataUsageId;
extern "C"  Il2CppObject * ExprSLASH_Evaluate_m2117643614 (ExprSLASH_t628862782 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_Evaluate_m2117643614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	BaseIterator_t1327316739 * V_1 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_left_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_3);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		NodeSet_t2875795446 * L_5 = __this->get_right_1();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_5);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		BaseIterator_t1327316739 * L_7 = V_0;
		NodeSet_t2875795446 * L_8 = __this->get_right_1();
		SimpleSlashIterator_t1419496431 * L_9 = (SimpleSlashIterator_t1419496431 *)il2cpp_codegen_object_new(SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m3208158132(L_9, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_003a:
	{
		BaseIterator_t1327316739 * L_10 = V_0;
		NodeSet_t2875795446 * L_11 = __this->get_right_1();
		SlashIterator_t384731969 * L_12 = (SlashIterator_t384731969 *)il2cpp_codegen_object_new(SlashIterator_t384731969_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_12, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		BaseIterator_t1327316739 * L_13 = V_1;
		SortedIterator_t4014821679 * L_14 = (SortedIterator_t4014821679 *)il2cpp_codegen_object_new(SortedIterator_t4014821679_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_14, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_RequireSorting()
extern "C"  bool ExprSLASH_get_RequireSorting_m2957526960 (ExprSLASH_t628862782 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_0);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_Peer()
extern "C"  bool ExprSLASH_get_Peer_m3801044979 (ExprSLASH_t628862782 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH::get_Subtree()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH_get_Subtree_m2865916751_MetadataUsageId;
extern "C"  bool ExprSLASH_get_Subtree_m2865916751 (ExprSLASH_t628862782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH_get_Subtree_m2865916751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t2875795446 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t2875795446 *)IsInstClass(L_0, NodeSet_t2875795446_il2cpp_TypeInfo_var));
		NodeSet_t2875795446 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t2875795446 * L_4 = __this->get_right_1();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
// System.Void System.Xml.XPath.ExprSLASH2::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.NodeSet)
extern "C"  void ExprSLASH2__ctor_m1370991914 (ExprSLASH2_t2003606286 * __this, Expression_t2556460284 * ___left0, NodeSet_t2875795446 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___left0;
		__this->set_left_0(L_0);
		NodeSet_t2875795446 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.ExprSLASH2::.cctor()
extern Il2CppClass* NodeTypeTest_t282671026_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH2_t2003606286_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2__cctor_m3450258365_MetadataUsageId;
extern "C"  void ExprSLASH2__cctor_m3450258365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2__cctor_m3450258365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NodeTypeTest_t282671026 * L_0 = (NodeTypeTest_t282671026 *)il2cpp_codegen_object_new(NodeTypeTest_t282671026_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2014120890(L_0, 5, ((int32_t)9), /*hidden argument*/NULL);
		((ExprSLASH2_t2003606286_StaticFields*)ExprSLASH2_t2003606286_il2cpp_TypeInfo_var->static_fields)->set_DescendantOrSelfStar_2(L_0);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH2::Optimize()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTest_t2939071960_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeNameTest_t2799571587_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH_t628862782_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTypeTest_t282671026_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_Optimize_m389619581_MetadataUsageId;
extern "C"  Expression_t2556460284 * ExprSLASH2_Optimize_m389619581 (ExprSLASH2_t2003606286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_Optimize_m389619581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeTest_t2939071960 * V_0 = NULL;
	NodeNameTest_t2799571587 * V_1 = NULL;
	NodeTypeTest_t282671026 * V_2 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(((NodeSet_t2875795446 *)CastclassClass(L_3, NodeSet_t2875795446_il2cpp_TypeInfo_var)));
		NodeSet_t2875795446 * L_4 = __this->get_right_1();
		V_0 = ((NodeTest_t2939071960 *)IsInstClass(L_4, NodeTest_t2939071960_il2cpp_TypeInfo_var));
		NodeTest_t2939071960 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_008a;
		}
	}
	{
		NodeTest_t2939071960 * L_6 = V_0;
		NullCheck(L_6);
		AxisSpecifier_t3783148883 * L_7 = NodeTest_get_Axis_m243839149(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = AxisSpecifier_get_Axis_m269195078(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_008a;
		}
	}
	{
		NodeTest_t2939071960 * L_9 = V_0;
		V_1 = ((NodeNameTest_t2799571587 *)IsInstClass(L_9, NodeNameTest_t2799571587_il2cpp_TypeInfo_var));
		NodeNameTest_t2799571587 * L_10 = V_1;
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		Expression_t2556460284 * L_11 = __this->get_left_0();
		NodeNameTest_t2799571587 * L_12 = V_1;
		NodeNameTest_t2799571587 * L_13 = (NodeNameTest_t2799571587 *)il2cpp_codegen_object_new(NodeNameTest_t2799571587_il2cpp_TypeInfo_var);
		NodeNameTest__ctor_m3377740561(L_13, L_12, 4, /*hidden argument*/NULL);
		ExprSLASH_t628862782 * L_14 = (ExprSLASH_t628862782 *)il2cpp_codegen_object_new(ExprSLASH_t628862782_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_14, L_11, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_006a:
	{
		NodeTest_t2939071960 * L_15 = V_0;
		V_2 = ((NodeTypeTest_t282671026 *)IsInstClass(L_15, NodeTypeTest_t282671026_il2cpp_TypeInfo_var));
		NodeTypeTest_t282671026 * L_16 = V_2;
		if (!L_16)
		{
			goto IL_008a;
		}
	}
	{
		Expression_t2556460284 * L_17 = __this->get_left_0();
		NodeTypeTest_t282671026 * L_18 = V_2;
		NodeTypeTest_t282671026 * L_19 = (NodeTypeTest_t282671026 *)il2cpp_codegen_object_new(NodeTypeTest_t282671026_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m1445907889(L_19, L_18, 4, /*hidden argument*/NULL);
		ExprSLASH_t628862782 * L_20 = (ExprSLASH_t628862782 *)il2cpp_codegen_object_new(ExprSLASH_t628862782_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_20, L_17, L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_008a:
	{
		return __this;
	}
}
// System.String System.Xml.XPath.ExprSLASH2::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1504;
extern const uint32_t ExprSLASH2_ToString_m2255083837_MetadataUsageId;
extern "C"  String_t* ExprSLASH2_ToString_m2255083837 (ExprSLASH2_t2003606286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_ToString_m2255083837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral1504, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprSLASH2::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ExprSLASH2_t2003606286_il2cpp_TypeInfo_var;
extern Il2CppClass* SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var;
extern Il2CppClass* SlashIterator_t384731969_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedIterator_t4014821679_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_Evaluate_m1368362004_MetadataUsageId;
extern "C"  Il2CppObject * ExprSLASH2_Evaluate_m1368362004 (ExprSLASH2_t2003606286 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_Evaluate_m1368362004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	BaseIterator_t1327316739 * V_1 = NULL;
	SlashIterator_t384731969 * V_2 = NULL;
	SortedIterator_t4014821679 * G_B6_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_left_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_3);
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t2556460284 * L_5 = __this->get_left_0();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		BaseIterator_t1327316739 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExprSLASH2_t2003606286_il2cpp_TypeInfo_var);
		NodeTest_t2939071960 * L_8 = ((ExprSLASH2_t2003606286_StaticFields*)ExprSLASH2_t2003606286_il2cpp_TypeInfo_var->static_fields)->get_DescendantOrSelfStar_2();
		SimpleSlashIterator_t1419496431 * L_9 = (SimpleSlashIterator_t1419496431 *)il2cpp_codegen_object_new(SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m3208158132(L_9, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0067;
	}

IL_003e:
	{
		BaseIterator_t1327316739 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExprSLASH2_t2003606286_il2cpp_TypeInfo_var);
		NodeTest_t2939071960 * L_11 = ((ExprSLASH2_t2003606286_StaticFields*)ExprSLASH2_t2003606286_il2cpp_TypeInfo_var->static_fields)->get_DescendantOrSelfStar_2();
		SlashIterator_t384731969 * L_12 = (SlashIterator_t384731969 *)il2cpp_codegen_object_new(SlashIterator_t384731969_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_12, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Expression_t2556460284 * L_13 = __this->get_left_0();
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_13);
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		BaseIterator_t1327316739 * L_15 = V_1;
		SortedIterator_t4014821679 * L_16 = (SortedIterator_t4014821679 *)il2cpp_codegen_object_new(SortedIterator_t4014821679_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_16, L_15, /*hidden argument*/NULL);
		G_B6_0 = L_16;
		goto IL_0066;
	}

IL_0065:
	{
		BaseIterator_t1327316739 * L_17 = V_1;
		G_B6_0 = ((SortedIterator_t4014821679 *)(L_17));
	}

IL_0066:
	{
		V_0 = G_B6_0;
	}

IL_0067:
	{
		BaseIterator_t1327316739 * L_18 = V_0;
		NodeSet_t2875795446 * L_19 = __this->get_right_1();
		SlashIterator_t384731969 * L_20 = (SlashIterator_t384731969 *)il2cpp_codegen_object_new(SlashIterator_t384731969_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m2348632994(L_20, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		SlashIterator_t384731969 * L_21 = V_2;
		SortedIterator_t4014821679 * L_22 = (SortedIterator_t4014821679 *)il2cpp_codegen_object_new(SortedIterator_t4014821679_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m2178587357(L_22, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_RequireSorting()
extern "C"  bool ExprSLASH2_get_RequireSorting_m786448756 (ExprSLASH2_t2003606286 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_0);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Xml.XPath.Expression::get_RequireSorting() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_Peer()
extern "C"  bool ExprSLASH2_get_Peer_m2193356471 (ExprSLASH2_t2003606286 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.ExprSLASH2::get_Subtree()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern const uint32_t ExprSLASH2_get_Subtree_m1397892619_MetadataUsageId;
extern "C"  bool ExprSLASH2_get_Subtree_m1397892619 (ExprSLASH2_t2003606286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprSLASH2_get_Subtree_m1397892619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t2875795446 * V_0 = NULL;
	int32_t G_B4_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t2875795446 *)IsInstClass(L_0, NodeSet_t2875795446_il2cpp_TypeInfo_var));
		NodeSet_t2875795446 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t2875795446 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_2);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		NodeSet_t2875795446 * L_4 = __this->get_right_1();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 0;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
// System.Void System.Xml.XPath.ExprUNION::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprUNION__ctor_m2912512622 (ExprUNION_t630776976 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___left0;
		__this->set_left_0(L_0);
		Expression_t2556460284 * L_1 = ___right1;
		__this->set_right_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.ExprUNION::Optimize()
extern "C"  Expression_t2556460284 * ExprUNION_Optimize_m4157719139 (ExprUNION_t630776976 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		Expression_t2556460284 * L_1 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_0);
		__this->set_left_0(L_1);
		Expression_t2556460284 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_2);
		__this->set_right_1(L_3);
		return __this;
	}
}
// System.String System.Xml.XPath.ExprUNION::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34628;
extern const uint32_t ExprUNION_ToString_m3279631905_MetadataUsageId;
extern "C"  String_t* ExprUNION_ToString_m3279631905 (ExprUNION_t630776976 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_ToString_m3279631905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Expression_t2556460284 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral34628, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object System.Xml.XPath.ExprUNION::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* UnionIterator_t1391611539_il2cpp_TypeInfo_var;
extern const uint32_t ExprUNION_Evaluate_m3576044620_MetadataUsageId;
extern "C"  Il2CppObject * ExprUNION_Evaluate_m3576044620 (ExprUNION_t630776976 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_Evaluate_m3576044620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	BaseIterator_t1327316739 * V_1 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		BaseIterator_t1327316739 * L_5 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		BaseIterator_t1327316739 * L_6 = ___iter0;
		BaseIterator_t1327316739 * L_7 = V_0;
		BaseIterator_t1327316739 * L_8 = V_1;
		UnionIterator_t1391611539 * L_9 = (UnionIterator_t1391611539 *)il2cpp_codegen_object_new(UnionIterator_t1391611539_il2cpp_TypeInfo_var);
		UnionIterator__ctor_m4002392457(L_9, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean System.Xml.XPath.ExprUNION::get_Peer()
extern "C"  bool ExprUNION_get_Peer_m628957765 (ExprUNION_t630776976 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_right_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.ExprUNION::get_Subtree()
extern Il2CppClass* NodeSet_t2875795446_il2cpp_TypeInfo_var;
extern const uint32_t ExprUNION_get_Subtree_m1086171069_MetadataUsageId;
extern "C"  bool ExprUNION_get_Subtree_m1086171069 (ExprUNION_t630776976 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprUNION_get_Subtree_m1086171069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t2875795446 * V_0 = NULL;
	NodeSet_t2875795446 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_left_0();
		V_0 = ((NodeSet_t2875795446 *)IsInstClass(L_0, NodeSet_t2875795446_il2cpp_TypeInfo_var));
		Expression_t2556460284 * L_1 = __this->get_right_1();
		V_1 = ((NodeSet_t2875795446 *)IsInstClass(L_1, NodeSet_t2875795446_il2cpp_TypeInfo_var));
		NodeSet_t2875795446 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t2875795446 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t2875795446 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_4);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		NodeSet_t2875795446 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Xml.XPath.NodeSet::get_Subtree() */, L_6);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0038;
	}

IL_0037:
	{
		G_B5_0 = 0;
	}

IL_0038:
	{
		return (bool)G_B5_0;
	}
}
// System.Void System.Xml.XPath.ExprVariable::.ctor(System.Xml.XmlQualifiedName,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var;
extern const uint32_t ExprVariable__ctor_m2228197543_MetadataUsageId;
extern "C"  void ExprVariable__ctor_m2228197543 (ExprVariable_t3764672565 * __this, XmlQualifiedName_t2133315502 * ___name0, Il2CppObject * ___ctx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable__ctor_m2228197543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___ctx1;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject * L_1 = ___ctx1;
		XmlQualifiedName_t2133315502 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_2);
		NullCheck(L_1);
		XmlQualifiedName_t2133315502 * L_4 = InterfaceFuncInvoker1< XmlQualifiedName_t2133315502 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var, L_1, L_3);
		___name0 = L_4;
		__this->set_resolvedName_1((bool)1);
	}

IL_0021:
	{
		XmlQualifiedName_t2133315502 * L_5 = ___name0;
		__this->set__name_0(L_5);
		return;
	}
}
// System.String System.Xml.XPath.ExprVariable::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral36;
extern const uint32_t ExprVariable_ToString_m4055062564_MetadataUsageId;
extern "C"  String_t* ExprVariable_ToString_m4055062564 (ExprVariable_t3764672565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable_ToString_m4055062564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlQualifiedName_t2133315502 * L_0 = __this->get__name_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral36, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprVariable::get_ReturnType()
extern "C"  int32_t ExprVariable_get_ReturnType_m158260906 (ExprVariable_t3764672565 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.ExprVariable::GetReturnType(System.Xml.XPath.BaseIterator)
extern "C"  int32_t ExprVariable_GetReturnType_m2092369121 (ExprVariable_t3764672565 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Object System.Xml.XPath.ExprVariable::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XsltContext_t894076946_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t2133315502_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppClass* IXsltContextVariable_t3376676363_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2849115671_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2209688223;
extern Il2CppCodeGenString* _stringLiteral4212489508;
extern Il2CppCodeGenString* _stringLiteral2113392597;
extern const uint32_t ExprVariable_Evaluate_m2065010189_MetadataUsageId;
extern "C"  Il2CppObject * ExprVariable_Evaluate_m2065010189 (ExprVariable_t3764672565 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExprVariable_Evaluate_m2065010189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	XsltContext_t894076946 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	XPathNodeIterator_t1383168931 * V_3 = NULL;
	Type_t * G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	Type_t * G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	Type_t * G_B8_0 = NULL;
	Type_t * G_B8_1 = NULL;
	String_t* G_B8_2 = NULL;
	XPathNodeIterator_t1383168931 * G_B15_0 = NULL;
	{
		V_0 = (Il2CppObject *)NULL;
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		V_1 = ((XsltContext_t894076946 *)IsInstClass(L_1, XsltContext_t894076946_il2cpp_TypeInfo_var));
		XsltContext_t894076946 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		bool L_3 = __this->get_resolvedName_1();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		XsltContext_t894076946 * L_4 = V_1;
		XmlQualifiedName_t2133315502 * L_5 = __this->get__name_0();
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, XmlQualifiedName_t2133315502 * >::Invoke(17 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName) */, L_4, L_5);
		V_0 = L_6;
		goto IL_0053;
	}

IL_0031:
	{
		XsltContext_t894076946 * L_7 = V_1;
		XmlQualifiedName_t2133315502 * L_8 = __this->get__name_0();
		NullCheck(L_8);
		String_t* L_9 = XmlQualifiedName_get_Name_m607016698(L_8, /*hidden argument*/NULL);
		XmlQualifiedName_t2133315502 * L_10 = __this->get__name_0();
		NullCheck(L_10);
		String_t* L_11 = XmlQualifiedName_get_Namespace_m2987642414(L_10, /*hidden argument*/NULL);
		XmlQualifiedName_t2133315502 * L_12 = (XmlQualifiedName_t2133315502 *)il2cpp_codegen_object_new(XmlQualifiedName_t2133315502_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_12, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, XmlQualifiedName_t2133315502 * >::Invoke(17 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName) */, L_7, L_12);
		V_0 = L_13;
	}

IL_0053:
	{
		goto IL_008a;
	}

IL_0058:
	{
		BaseIterator_t1327316739 * L_14 = ___iter0;
		NullCheck(L_14);
		Type_t * L_15 = Object_GetType_m2022236990(L_14, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_16 = ___iter0;
		NullCheck(L_16);
		Il2CppObject * L_17 = BaseIterator_get_NamespaceManager_m3653702256(L_16, /*hidden argument*/NULL);
		G_B6_0 = L_15;
		G_B6_1 = _stringLiteral2209688223;
		if (!L_17)
		{
			G_B7_0 = L_15;
			G_B7_1 = _stringLiteral2209688223;
			goto IL_007e;
		}
	}
	{
		BaseIterator_t1327316739 * L_18 = ___iter0;
		NullCheck(L_18);
		Il2CppObject * L_19 = BaseIterator_get_NamespaceManager_m3653702256(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Type_t * L_20 = Object_GetType_m2022236990(L_19, /*hidden argument*/NULL);
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_007f;
	}

IL_007e:
	{
		G_B8_0 = ((Type_t *)(NULL));
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m2398979370(NULL /*static, unused*/, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_22 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_008a:
	{
		Il2CppObject * L_23 = V_0;
		if (L_23)
		{
			goto IL_00b0;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_24 = __this->get__name_0();
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral4212489508, L_25, _stringLiteral2113392597, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_27 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_27, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00b0:
	{
		Il2CppObject * L_28 = V_0;
		XsltContext_t894076946 * L_29 = V_1;
		NullCheck(L_28);
		Il2CppObject * L_30 = InterfaceFuncInvoker1< Il2CppObject *, XsltContext_t894076946 * >::Invoke(0 /* System.Object System.Xml.Xsl.IXsltContextVariable::Evaluate(System.Xml.Xsl.XsltContext) */, IXsltContextVariable_t3376676363_il2cpp_TypeInfo_var, L_28, L_29);
		V_2 = L_30;
		Il2CppObject * L_31 = V_2;
		V_3 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_31, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_32 = V_3;
		if (!L_32)
		{
			goto IL_00e3;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_33 = V_3;
		if (!((BaseIterator_t1327316739 *)IsInstClass(L_33, BaseIterator_t1327316739_il2cpp_TypeInfo_var)))
		{
			goto IL_00d6;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_34 = V_3;
		G_B15_0 = L_34;
		goto IL_00e2;
	}

IL_00d6:
	{
		XPathNodeIterator_t1383168931 * L_35 = V_3;
		BaseIterator_t1327316739 * L_36 = ___iter0;
		NullCheck(L_36);
		Il2CppObject * L_37 = BaseIterator_get_NamespaceManager_m3653702256(L_36, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_38 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_38, L_35, L_37, /*hidden argument*/NULL);
		G_B15_0 = ((XPathNodeIterator_t1383168931 *)(L_38));
	}

IL_00e2:
	{
		return G_B15_0;
	}

IL_00e3:
	{
		Il2CppObject * L_39 = V_2;
		return L_39;
	}
}
// System.Boolean System.Xml.XPath.ExprVariable::get_Peer()
extern "C"  bool ExprVariable_get_Peer_m3838091998 (ExprVariable_t3764672565 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FollowingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void FollowingIterator__ctor_m4068454143 (FollowingIterator_t3149710293 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.FollowingIterator::.ctor(System.Xml.XPath.FollowingIterator)
extern "C"  void FollowingIterator__ctor_m4069268917 (FollowingIterator_t3149710293 * __this, FollowingIterator_t3149710293 * ___other0, const MethodInfo* method)
{
	{
		FollowingIterator_t3149710293 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.FollowingIterator::Clone()
extern Il2CppClass* FollowingIterator_t3149710293_il2cpp_TypeInfo_var;
extern const uint32_t FollowingIterator_Clone_m1372703741_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * FollowingIterator_Clone_m1372703741 (FollowingIterator_t3149710293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowingIterator_Clone_m1372703741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FollowingIterator_t3149710293 * L_0 = (FollowingIterator_t3149710293 *)il2cpp_codegen_object_new(FollowingIterator_t3149710293_il2cpp_TypeInfo_var);
		FollowingIterator__ctor_m4069268917(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.FollowingIterator::MoveNextCore()
extern "C"  bool FollowingIterator_MoveNextCore_m1477945986 (FollowingIterator_t3149710293 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get__finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (bool)1;
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_1)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = (bool)0;
		XPathNavigator_t1075073278 * L_2 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_2);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_003b;
		}
	}
	{
		goto IL_004e;
	}

IL_003b:
	{
		XPathNavigator_t1075073278 * L_6 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_6);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_6);
		V_0 = (bool)1;
		goto IL_008c;
	}

IL_004e:
	{
		XPathNavigator_t1075073278 * L_7 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_7);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)1;
	}

IL_0060:
	{
		goto IL_0077;
	}

IL_0065:
	{
		XPathNavigator_t1075073278 * L_9 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_9);
		if (!L_10)
		{
			goto IL_0077;
		}
	}
	{
		return (bool)1;
	}

IL_0077:
	{
		XPathNavigator_t1075073278 * L_11 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_11);
		if (L_12)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_008c;
	}

IL_008c:
	{
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_00c6;
		}
	}
	{
		XPathNavigator_t1075073278 * L_14 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_14);
		if (!L_15)
		{
			goto IL_00a4;
		}
	}
	{
		return (bool)1;
	}

IL_00a4:
	{
		XPathNavigator_t1075073278 * L_16 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_16);
		if (!L_17)
		{
			goto IL_00b6;
		}
	}
	{
		return (bool)1;
	}

IL_00b6:
	{
		XPathNavigator_t1075073278 * L_18 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_00a4;
		}
	}

IL_00c6:
	{
		__this->set__finished_6((bool)1);
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void FollowingSiblingIterator__ctor_m3707604177 (FollowingSiblingIterator_t116837539 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.FollowingSiblingIterator)
extern "C"  void FollowingSiblingIterator__ctor_m1388330545 (FollowingSiblingIterator_t116837539 * __this, FollowingSiblingIterator_t116837539 * ___other0, const MethodInfo* method)
{
	{
		FollowingSiblingIterator_t116837539 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.FollowingSiblingIterator::Clone()
extern Il2CppClass* FollowingSiblingIterator_t116837539_il2cpp_TypeInfo_var;
extern const uint32_t FollowingSiblingIterator_Clone_m83290849_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * FollowingSiblingIterator_Clone_m83290849 (FollowingSiblingIterator_t116837539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FollowingSiblingIterator_Clone_m83290849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FollowingSiblingIterator_t116837539 * L_0 = (FollowingSiblingIterator_t116837539 *)il2cpp_codegen_object_new(FollowingSiblingIterator_t116837539_il2cpp_TypeInfo_var);
		FollowingSiblingIterator__ctor_m1388330545(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.FollowingSiblingIterator::MoveNextCore()
extern "C"  bool FollowingSiblingIterator_MoveNextCore_m1382672580 (FollowingSiblingIterator_t116837539 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XPathNavigator_t1075073278 * L_0 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0021;
	}

IL_001f:
	{
		return (bool)0;
	}

IL_0021:
	{
		XPathNavigator_t1075073278 * L_4 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_4);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		return (bool)1;
	}

IL_0033:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.FunctionArguments::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.FunctionArguments)
extern "C"  void FunctionArguments__ctor_m3611561738 (FunctionArguments_t2391178772 * __this, Expression_t2556460284 * ___arg0, FunctionArguments_t2391178772 * ___tail1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Expression_t2556460284 * L_0 = ___arg0;
		__this->set__arg_0(L_0);
		FunctionArguments_t2391178772 * L_1 = ___tail1;
		__this->set__tail_1(L_1);
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::get_Arg()
extern "C"  Expression_t2556460284 * FunctionArguments_get_Arg_m969707333 (FunctionArguments_t2391178772 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get__arg_0();
		return L_0;
	}
}
// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::get_Tail()
extern "C"  FunctionArguments_t2391178772 * FunctionArguments_get_Tail_m52932169 (FunctionArguments_t2391178772 * __this, const MethodInfo* method)
{
	{
		FunctionArguments_t2391178772 * L_0 = __this->get__tail_1();
		return L_0;
	}
}
// System.Void System.Xml.XPath.FunctionArguments::ToArrayList(System.Collections.ArrayList)
extern "C"  void FunctionArguments_ToArrayList_m227079145 (FunctionArguments_t2391178772 * __this, ArrayList_t3948406897 * ___a0, const MethodInfo* method)
{
	FunctionArguments_t2391178772 * V_0 = NULL;
	{
		V_0 = __this;
	}

IL_0002:
	{
		ArrayList_t3948406897 * L_0 = ___a0;
		FunctionArguments_t2391178772 * L_1 = V_0;
		NullCheck(L_1);
		Expression_t2556460284 * L_2 = L_1->get__arg_0();
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_2);
		FunctionArguments_t2391178772 * L_3 = V_0;
		NullCheck(L_3);
		FunctionArguments_t2391178772 * L_4 = L_3->get__tail_1();
		V_0 = L_4;
		FunctionArguments_t2391178772 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Xml.XPath.BaseIterator,System.Collections.IList)
extern "C"  void ListIterator__ctor_m222741241 (ListIterator_t4080389072 * __this, BaseIterator_t1327316739 * ___iter0, Il2CppObject * ___list1, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___list1;
		__this->set__list_3(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Collections.IList,System.Xml.IXmlNamespaceResolver)
extern "C"  void ListIterator__ctor_m320855722 (ListIterator_t4080389072 * __this, Il2CppObject * ___list0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___list0;
		__this->set__list_3(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Xml.XPath.ListIterator)
extern "C"  void ListIterator__ctor_m3954479633 (ListIterator_t4080389072 * __this, ListIterator_t4080389072 * ___other0, const MethodInfo* method)
{
	{
		ListIterator_t4080389072 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		ListIterator_t4080389072 * L_1 = ___other0;
		NullCheck(L_1);
		Il2CppObject * L_2 = L_1->get__list_3();
		__this->set__list_3(L_2);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ListIterator::Clone()
extern Il2CppClass* ListIterator_t4080389072_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_Clone_m1676587988_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * ListIterator_Clone_m1676587988 (ListIterator_t4080389072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_Clone_m1676587988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ListIterator_t4080389072 * L_0 = (ListIterator_t4080389072 *)il2cpp_codegen_object_new(ListIterator_t4080389072_il2cpp_TypeInfo_var);
		ListIterator__ctor_m3954479633(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ListIterator::MoveNextCore()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_MoveNextCore_m2796201201_MetadataUsageId;
extern "C"  bool ListIterator_MoveNextCore_m2796201201 (ListIterator_t4080389072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_MoveNextCore_m2796201201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		Il2CppObject * L_1 = __this->get__list_3();
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_1);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		return (bool)1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ListIterator::get_Current()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_get_Current_m3594158222_MetadataUsageId;
extern "C"  XPathNavigator_t1075073278 * ListIterator_get_Current_m3594158222 (ListIterator_t4080389072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_get_Current_m3594158222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_3();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_2)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (XPathNavigator_t1075073278 *)NULL;
	}

IL_001d:
	{
		Il2CppObject * L_3 = __this->get__list_3();
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		NullCheck(L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, L_3, ((int32_t)((int32_t)L_4-(int32_t)1)));
		return ((XPathNavigator_t1075073278 *)CastclassClass(L_5, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
	}
}
// System.Int32 System.Xml.XPath.ListIterator::get_Count()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t ListIterator_get_Count_m773660244_MetadataUsageId;
extern "C"  int32_t ListIterator_get_Count_m773660244 (ListIterator_t4080389072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ListIterator_get_Count_m773660244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__list_3();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.NamespaceIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void NamespaceIterator__ctor_m729989193 (NamespaceIterator_t1295432863 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NamespaceIterator::.ctor(System.Xml.XPath.NamespaceIterator)
extern "C"  void NamespaceIterator__ctor_m30894881 (NamespaceIterator_t1295432863 * __this, NamespaceIterator_t1295432863 * ___other0, const MethodInfo* method)
{
	{
		NamespaceIterator_t1295432863 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.NamespaceIterator::Clone()
extern Il2CppClass* NamespaceIterator_t1295432863_il2cpp_TypeInfo_var;
extern const uint32_t NamespaceIterator_Clone_m1073878643_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * NamespaceIterator_Clone_m1073878643 (NamespaceIterator_t1295432863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NamespaceIterator_Clone_m1073878643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NamespaceIterator_t1295432863 * L_0 = (NamespaceIterator_t1295432863 *)il2cpp_codegen_object_new(NamespaceIterator_t1295432863_il2cpp_TypeInfo_var);
		NamespaceIterator__ctor_m30894881(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.NamespaceIterator::MoveNextCore()
extern "C"  bool NamespaceIterator_MoveNextCore_m212520780 (NamespaceIterator_t1295432863 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		XPathNavigator_t1075073278 * L_1 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = XPathNavigator_MoveToFirstNamespace_m3738691923(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)1;
	}

IL_001d:
	{
		goto IL_0034;
	}

IL_0022:
	{
		XPathNavigator_t1075073278 * L_3 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_3);
		bool L_4 = XPathNavigator_MoveToNextNamespace_m197928770(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)1;
	}

IL_0034:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.XPath.NodeNameTest::.ctor(System.Xml.XPath.Axes,System.Xml.XmlQualifiedName,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var;
extern const uint32_t NodeNameTest__ctor_m98601429_MetadataUsageId;
extern "C"  void NodeNameTest__ctor_m98601429 (NodeNameTest_t2799571587 * __this, int32_t ___axis0, XmlQualifiedName_t2133315502 * ___name1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest__ctor_m98601429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___ctx2;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_2 = ___ctx2;
		XmlQualifiedName_t2133315502 * L_3 = ___name1;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_3);
		NullCheck(L_2);
		XmlQualifiedName_t2133315502 * L_5 = InterfaceFuncInvoker1< XmlQualifiedName_t2133315502 *, String_t* >::Invoke(2 /* System.Xml.XmlQualifiedName System.Xml.Xsl.IStaticXsltContext::LookupQName(System.String) */, IStaticXsltContext_t2968841889_il2cpp_TypeInfo_var, L_2, L_4);
		___name1 = L_5;
		__this->set_resolvedName_2((bool)1);
	}

IL_0022:
	{
		XmlQualifiedName_t2133315502 * L_6 = ___name1;
		__this->set__name_1(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.NodeNameTest::.ctor(System.Xml.XPath.NodeNameTest,System.Xml.XPath.Axes)
extern "C"  void NodeNameTest__ctor_m3377740561 (NodeNameTest_t2799571587 * __this, NodeNameTest_t2799571587 * ___source0, int32_t ___axis1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis1;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		NodeNameTest_t2799571587 * L_1 = ___source0;
		NullCheck(L_1);
		XmlQualifiedName_t2133315502 * L_2 = L_1->get__name_1();
		__this->set__name_1(L_2);
		NodeNameTest_t2799571587 * L_3 = ___source0;
		NullCheck(L_3);
		bool L_4 = L_3->get_resolvedName_2();
		__this->set_resolvedName_2(L_4);
		return;
	}
}
// System.String System.Xml.XPath.NodeNameTest::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1856;
extern const uint32_t NodeNameTest_ToString_m2840726898_MetadataUsageId;
extern "C"  String_t* NodeNameTest_ToString_m2840726898 (NodeNameTest_t2799571587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest_ToString_m2840726898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisSpecifier_t3783148883 * L_0 = ((NodeTest_t2939071960 *)__this)->get__axis_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XPath.AxisSpecifier::ToString() */, L_0);
		XmlQualifiedName_t2133315502 * L_2 = __this->get__name_1();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral1856, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Xml.XPath.NodeNameTest::Match(System.Xml.IXmlNamespaceResolver,System.Xml.XPath.XPathNavigator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IXmlNamespaceResolver_t3774973253_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral638572358;
extern const uint32_t NodeNameTest_Match_m3615173812_MetadataUsageId;
extern "C"  bool NodeNameTest_Match_m3615173812 (NodeNameTest_t2799571587 * __this, Il2CppObject * ___nsm0, XPathNavigator_t1075073278 * ___nav1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeNameTest_Match_m3615173812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		XPathNavigator_t1075073278 * L_0 = ___nav1;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		AxisSpecifier_t3783148883 * L_2 = ((NodeTest_t2939071960 *)__this)->get__axis_0();
		NullCheck(L_2);
		int32_t L_3 = AxisSpecifier_get_NodeType_m3352025689(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_3)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		XmlQualifiedName_t2133315502 * L_4 = __this->get__name_1();
		NullCheck(L_4);
		String_t* L_5 = XmlQualifiedName_get_Name_m607016698(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_7 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004f;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_8 = __this->get__name_1();
		NullCheck(L_8);
		String_t* L_9 = XmlQualifiedName_get_Name_m607016698(L_8, /*hidden argument*/NULL);
		XPathNavigator_t1075073278 * L_10 = ___nav1;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		return (bool)0;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_13;
		XmlQualifiedName_t2133315502 * L_14 = __this->get__name_1();
		NullCheck(L_14);
		String_t* L_15 = XmlQualifiedName_get_Namespace_m2987642414(L_14, /*hidden argument*/NULL);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_17 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00c4;
		}
	}
	{
		bool L_18 = __this->get_resolvedName_2();
		if (!L_18)
		{
			goto IL_008b;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_19 = __this->get__name_1();
		NullCheck(L_19);
		String_t* L_20 = XmlQualifiedName_get_Namespace_m2987642414(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00a3;
	}

IL_008b:
	{
		Il2CppObject * L_21 = ___nsm0;
		if (!L_21)
		{
			goto IL_00a3;
		}
	}
	{
		Il2CppObject * L_22 = ___nsm0;
		XmlQualifiedName_t2133315502 * L_23 = __this->get__name_1();
		NullCheck(L_23);
		String_t* L_24 = XmlQualifiedName_get_Namespace_m2987642414(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_25 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(0 /* System.String System.Xml.IXmlNamespaceResolver::LookupNamespace(System.String) */, IXmlNamespaceResolver_t3774973253_il2cpp_TypeInfo_var, L_22, L_24);
		V_0 = L_25;
	}

IL_00a3:
	{
		String_t* L_26 = V_0;
		if (L_26)
		{
			goto IL_00c4;
		}
	}
	{
		XmlQualifiedName_t2133315502 * L_27 = __this->get__name_1();
		NullCheck(L_27);
		String_t* L_28 = XmlQualifiedName_get_Namespace_m2987642414(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral638572358, L_28, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_30 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_30, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00c4:
	{
		String_t* L_31 = V_0;
		XPathNavigator_t1075073278 * L_32 = ___nav1;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.Void System.Xml.XPath.NodeSet::.ctor()
extern "C"  void NodeSet__ctor_m3658293772 (NodeSet_t2875795446 * __this, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.NodeSet::get_ReturnType()
extern "C"  int32_t NodeSet_get_ReturnType_m2311151599 (NodeSet_t2875795446 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Void System.Xml.XPath.NodeTest::.ctor(System.Xml.XPath.Axes)
extern Il2CppClass* AxisSpecifier_t3783148883_il2cpp_TypeInfo_var;
extern const uint32_t NodeTest__ctor_m3652977216_MetadataUsageId;
extern "C"  void NodeTest__ctor_m3652977216 (NodeTest_t2939071960 * __this, int32_t ___axis0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTest__ctor_m3652977216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NodeSet__ctor_m3658293772(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___axis0;
		AxisSpecifier_t3783148883 * L_1 = (AxisSpecifier_t3783148883 *)il2cpp_codegen_object_new(AxisSpecifier_t3783148883_il2cpp_TypeInfo_var);
		AxisSpecifier__ctor_m223823863(L_1, L_0, /*hidden argument*/NULL);
		__this->set__axis_0(L_1);
		return;
	}
}
// System.Xml.XPath.AxisSpecifier System.Xml.XPath.NodeTest::get_Axis()
extern "C"  AxisSpecifier_t3783148883 * NodeTest_get_Axis_m243839149 (NodeTest_t2939071960 * __this, const MethodInfo* method)
{
	{
		AxisSpecifier_t3783148883 * L_0 = __this->get__axis_0();
		return L_0;
	}
}
// System.Object System.Xml.XPath.NodeTest::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* AxisIterator_t953634771_il2cpp_TypeInfo_var;
extern const uint32_t NodeTest_Evaluate_m2924662090_MetadataUsageId;
extern "C"  Il2CppObject * NodeTest_Evaluate_m2924662090 (NodeTest_t2939071960 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTest_Evaluate_m2924662090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		AxisSpecifier_t3783148883 * L_0 = __this->get__axis_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = AxisSpecifier_Evaluate_m3457862801(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		BaseIterator_t1327316739 * L_3 = V_0;
		AxisIterator_t953634771 * L_4 = (AxisIterator_t953634771 *)il2cpp_codegen_object_new(AxisIterator_t953634771_il2cpp_TypeInfo_var);
		AxisIterator__ctor_m2307956914(L_4, L_3, __this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_RequireSorting()
extern "C"  bool NodeTest_get_RequireSorting_m101109566 (NodeTest_t2939071960 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t3783148883 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0047;
		}
		if (L_2 == 1)
		{
			goto IL_0047;
		}
		if (L_2 == 2)
		{
			goto IL_0047;
		}
		if (L_2 == 3)
		{
			goto IL_0049;
		}
		if (L_2 == 4)
		{
			goto IL_0049;
		}
		if (L_2 == 5)
		{
			goto IL_0049;
		}
		if (L_2 == 6)
		{
			goto IL_0049;
		}
		if (L_2 == 7)
		{
			goto IL_0049;
		}
		if (L_2 == 8)
		{
			goto IL_0047;
		}
		if (L_2 == 9)
		{
			goto IL_0049;
		}
		if (L_2 == 10)
		{
			goto IL_0047;
		}
		if (L_2 == 11)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0049;
	}

IL_0047:
	{
		return (bool)1;
	}

IL_0049:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_Peer()
extern "C"  bool NodeTest_get_Peer_m1939429633 (NodeTest_t2939071960 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t3783148883 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0043;
		}
		if (L_2 == 1)
		{
			goto IL_0043;
		}
		if (L_2 == 2)
		{
			goto IL_0045;
		}
		if (L_2 == 3)
		{
			goto IL_0045;
		}
		if (L_2 == 4)
		{
			goto IL_0043;
		}
		if (L_2 == 5)
		{
			goto IL_0043;
		}
		if (L_2 == 6)
		{
			goto IL_0043;
		}
		if (L_2 == 7)
		{
			goto IL_0045;
		}
		if (L_2 == 8)
		{
			goto IL_0045;
		}
		if (L_2 == 9)
		{
			goto IL_0045;
		}
		if (L_2 == 10)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0045;
	}

IL_0043:
	{
		return (bool)0;
	}

IL_0045:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.NodeTest::get_Subtree()
extern "C"  bool NodeTest_get_Subtree_m100870017 (NodeTest_t2939071960 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		AxisSpecifier_t3783148883 * L_0 = __this->get__axis_0();
		NullCheck(L_0);
		int32_t L_1 = AxisSpecifier_get_Axis_m269195078(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_0047;
		}
		if (L_2 == 1)
		{
			goto IL_0047;
		}
		if (L_2 == 2)
		{
			goto IL_0049;
		}
		if (L_2 == 3)
		{
			goto IL_0049;
		}
		if (L_2 == 4)
		{
			goto IL_0049;
		}
		if (L_2 == 5)
		{
			goto IL_0049;
		}
		if (L_2 == 6)
		{
			goto IL_0047;
		}
		if (L_2 == 7)
		{
			goto IL_0047;
		}
		if (L_2 == 8)
		{
			goto IL_0049;
		}
		if (L_2 == 9)
		{
			goto IL_0047;
		}
		if (L_2 == 10)
		{
			goto IL_0047;
		}
		if (L_2 == 11)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0049;
	}

IL_0047:
	{
		return (bool)0;
	}

IL_0049:
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes)
extern "C"  void NodeTypeTest__ctor_m423301530 (NodeTypeTest_t282671026 * __this, int32_t ___axis0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		AxisSpecifier_t3783148883 * L_1 = ((NodeTest_t2939071960 *)__this)->get__axis_0();
		NullCheck(L_1);
		int32_t L_2 = AxisSpecifier_get_NodeType_m3352025689(L_1, /*hidden argument*/NULL);
		__this->set_type_1(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes,System.Xml.XPath.XPathNodeType)
extern "C"  void NodeTypeTest__ctor_m2014120890 (NodeTypeTest_t282671026 * __this, int32_t ___axis0, int32_t ___type1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___type1;
		__this->set_type_1(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.Axes,System.Xml.XPath.XPathNodeType,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1267081075;
extern Il2CppCodeGenString* _stringLiteral2347313233;
extern const uint32_t NodeTypeTest__ctor_m2764187254_MetadataUsageId;
extern "C"  void NodeTypeTest__ctor_m2764187254 (NodeTypeTest_t282671026 * __this, int32_t ___axis0, int32_t ___type1, String_t* ___param2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest__ctor_m2764187254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___type1;
		__this->set_type_1(L_1);
		String_t* L_2 = ___param2;
		__this->set__param_2(L_2);
		String_t* L_3 = ___param2;
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = ___type1;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_5 = ___type1;
		String_t* L_6 = NodeTypeTest_ToString_m1634841327(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1267081075, L_6, _stringLiteral2347313233, /*hidden argument*/NULL);
		XPathException_t1803876086 * L_8 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Xml.XPath.NodeTypeTest::.ctor(System.Xml.XPath.NodeTypeTest,System.Xml.XPath.Axes)
extern "C"  void NodeTypeTest__ctor_m1445907889 (NodeTypeTest_t282671026 * __this, NodeTypeTest_t282671026 * ___other0, int32_t ___axis1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis1;
		NodeTest__ctor_m3652977216(__this, L_0, /*hidden argument*/NULL);
		NodeTypeTest_t282671026 * L_1 = ___other0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_type_1();
		__this->set_type_1(L_2);
		NodeTypeTest_t282671026 * L_3 = ___other0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get__param_2();
		__this->set__param_2(L_4);
		return;
	}
}
// System.String System.Xml.XPath.NodeTypeTest::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1250;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1856;
extern const uint32_t NodeTypeTest_ToString_m2899043361_MetadataUsageId;
extern "C"  String_t* NodeTypeTest_ToString_m2899043361 (NodeTypeTest_t282671026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_ToString_m2899043361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = __this->get_type_1();
		String_t* L_1 = NodeTypeTest_ToString_m1634841327(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_type_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)7))))
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_3 = __this->get__param_2();
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_4 = V_0;
		String_t* L_5 = __this->get__param_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2933632197(NULL /*static, unused*/, L_4, _stringLiteral1279, L_5, _stringLiteral1250, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_004b;
	}

IL_003f:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, L_7, _stringLiteral1281, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_004b:
	{
		AxisSpecifier_t3783148883 * L_9 = ((NodeTest_t2939071960 *)__this)->get__axis_0();
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XPath.AxisSpecifier::ToString() */, L_9);
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, L_10, _stringLiteral1856, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Xml.XPath.NodeTypeTest::ToString(System.Xml.XPath.XPathNodeType)
extern Il2CppClass* XPathNodeType_t3637370479_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral950398559;
extern Il2CppCodeGenString* _stringLiteral3556653;
extern Il2CppCodeGenString* _stringLiteral3628607700;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern Il2CppCodeGenString* _stringLiteral3858170656;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t NodeTypeTest_ToString_m1634841327_MetadataUsageId;
extern "C"  String_t* NodeTypeTest_ToString_m1634841327 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_ToString_m1634841327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___type0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0039;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_003f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0033;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_004b;
	}

IL_0033:
	{
		return _stringLiteral950398559;
	}

IL_0039:
	{
		return _stringLiteral3556653;
	}

IL_003f:
	{
		return _stringLiteral3628607700;
	}

IL_0045:
	{
		return _stringLiteral3386882;
	}

IL_004b:
	{
		int32_t L_2 = ___type0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(XPathNodeType_t3637370479_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t2862688501 *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3858170656, L_5, _stringLiteral93, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean System.Xml.XPath.NodeTypeTest::Match(System.Xml.IXmlNamespaceResolver,System.Xml.XPath.XPathNavigator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t NodeTypeTest_Match_m4271858789_MetadataUsageId;
extern "C"  bool NodeTypeTest_Match_m4271858789 (NodeTypeTest_t282671026 * __this, Il2CppObject * ___nsm0, XPathNavigator_t1075073278 * ___nav1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NodeTypeTest_Match_m4271858789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		XPathNavigator_t1075073278 * L_0 = ___nav1;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		V_0 = L_1;
		int32_t L_2 = __this->get_type_1();
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 0)
		{
			goto IL_0063;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 1)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 2)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 3)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 4)
		{
			goto IL_0082;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 5)
		{
			goto IL_0033;
		}
	}
	{
		goto IL_0082;
	}

IL_0033:
	{
		return (bool)1;
	}

IL_0035:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_003e;
		}
	}
	{
		return (bool)0;
	}

IL_003e:
	{
		String_t* L_5 = __this->get__param_2();
		if (!L_5)
		{
			goto IL_0061;
		}
	}
	{
		XPathNavigator_t1075073278 * L_6 = ___nav1;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_6);
		String_t* L_8 = __this->get__param_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0061;
		}
	}
	{
		return (bool)0;
	}

IL_0061:
	{
		return (bool)1;
	}

IL_0063:
	{
		int32_t L_10 = V_0;
		V_2 = L_10;
		int32_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 0)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 1)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)4)) == 2)
		{
			goto IL_007e;
		}
	}
	{
		goto IL_0080;
	}

IL_007e:
	{
		return (bool)1;
	}

IL_0080:
	{
		return (bool)0;
	}

IL_0082:
	{
		int32_t L_12 = __this->get_type_1();
		int32_t L_13 = V_0;
		return (bool)((((int32_t)L_12) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void NullIterator__ctor_m2854644327 (NullIterator_t2905335737 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SelfIterator__ctor_m2835358572(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void NullIterator__ctor_m4158990173 (NullIterator_t2905335737 * __this, XPathNavigator_t1075073278 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = ___nav0;
		Il2CppObject * L_1 = ___nsm1;
		SelfIterator__ctor_m4150585400(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.NullIterator)
extern "C"  void NullIterator__ctor_m233625713 (NullIterator_t2905335737 * __this, NullIterator_t2905335737 * ___other0, const MethodInfo* method)
{
	{
		NullIterator_t2905335737 * L_0 = ___other0;
		SelfIterator__ctor_m3078894092(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.NullIterator::Clone()
extern Il2CppClass* NullIterator_t2905335737_il2cpp_TypeInfo_var;
extern const uint32_t NullIterator_Clone_m2491346443_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * NullIterator_Clone_m2491346443 (NullIterator_t2905335737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NullIterator_Clone_m2491346443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullIterator_t2905335737 * L_0 = (NullIterator_t2905335737 *)il2cpp_codegen_object_new(NullIterator_t2905335737_il2cpp_TypeInfo_var);
		NullIterator__ctor_m233625713(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.NullIterator::MoveNextCore()
extern "C"  bool NullIterator_MoveNextCore_m161861338 (NullIterator_t2905335737 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 System.Xml.XPath.NullIterator::get_CurrentPosition()
extern "C"  int32_t NullIterator_get_CurrentPosition_m3673814846 (NullIterator_t2905335737 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.NullIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * NullIterator_get_Current_m3786274181 (NullIterator_t2905335737 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		return L_0;
	}
}
// System.Void System.Xml.XPath.ParensIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ParensIterator__ctor_m938283977 (ParensIterator_t3418004251 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		__this->set__iter_3(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ParensIterator::.ctor(System.Xml.XPath.ParensIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t ParensIterator__ctor_m3840579889_MetadataUsageId;
extern "C"  void ParensIterator__ctor_m3840579889 (ParensIterator_t3418004251 * __this, ParensIterator_t3418004251 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParensIterator__ctor_m3840579889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParensIterator_t3418004251 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		ParensIterator_t3418004251 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t1327316739 * L_2 = L_1->get__iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iter_3(((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ParensIterator::Clone()
extern Il2CppClass* ParensIterator_t3418004251_il2cpp_TypeInfo_var;
extern const uint32_t ParensIterator_Clone_m2933303977_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * ParensIterator_Clone_m2933303977 (ParensIterator_t3418004251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParensIterator_Clone_m2933303977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParensIterator_t3418004251 * L_0 = (ParensIterator_t3418004251 *)il2cpp_codegen_object_new(ParensIterator_t3418004251_il2cpp_TypeInfo_var);
		ParensIterator__ctor_m3840579889(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ParensIterator::MoveNextCore()
extern "C"  bool ParensIterator_MoveNextCore_m2517159868 (ParensIterator_t3418004251 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.ParensIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * ParensIterator_get_Current_m1307635171 (ParensIterator_t3418004251 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		XPathNavigator_t1075073278 * L_1 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.ParensIterator::get_Count()
extern "C"  int32_t ParensIterator_get_Count_m3610177769 (ParensIterator_t3418004251 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.ParentIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void ParentIterator__ctor_m2909223434 (ParentIterator_t1610549788 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		XPathNavigator_t1075073278 * L_1 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_1);
		__this->set_canMove_6(L_2);
		return;
	}
}
// System.Void System.Xml.XPath.ParentIterator::.ctor(System.Xml.XPath.ParentIterator,System.Boolean)
extern "C"  void ParentIterator__ctor_m3526189836 (ParentIterator_t1610549788 * __this, ParentIterator_t1610549788 * ___other0, bool ___dummy1, const MethodInfo* method)
{
	{
		ParentIterator_t1610549788 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		ParentIterator_t1610549788 * L_1 = ___other0;
		NullCheck(L_1);
		bool L_2 = L_1->get_canMove_6();
		__this->set_canMove_6(L_2);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.ParentIterator::Clone()
extern Il2CppClass* ParentIterator_t1610549788_il2cpp_TypeInfo_var;
extern const uint32_t ParentIterator_Clone_m1936231624_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * ParentIterator_Clone_m1936231624 (ParentIterator_t1610549788 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParentIterator_Clone_m1936231624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParentIterator_t1610549788 * L_0 = (ParentIterator_t1610549788 *)il2cpp_codegen_object_new(ParentIterator_t1610549788_il2cpp_TypeInfo_var);
		ParentIterator__ctor_m3526189836(L_0, __this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.ParentIterator::MoveNextCore()
extern "C"  bool ParentIterator_MoveNextCore_m439950525 (ParentIterator_t1610549788 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_canMove_6();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		__this->set_canMove_6((bool)0);
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PrecedingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void PrecedingIterator__ctor_m3521034673 (PrecedingIterator_t1039893511 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.PrecedingIterator::.ctor(System.Xml.XPath.PrecedingIterator)
extern "C"  void PrecedingIterator__ctor_m3696965713 (PrecedingIterator_t1039893511 * __this, PrecedingIterator_t1039893511 * ___other0, const MethodInfo* method)
{
	{
		PrecedingIterator_t1039893511 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		PrecedingIterator_t1039893511 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		PrecedingIterator_t1039893511 * L_3 = ___other0;
		NullCheck(L_3);
		bool L_4 = L_3->get_started_7();
		__this->set_started_7(L_4);
		PrecedingIterator_t1039893511 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = L_5->get_finished_6();
		__this->set_finished_6(L_6);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PrecedingIterator::Clone()
extern Il2CppClass* PrecedingIterator_t1039893511_il2cpp_TypeInfo_var;
extern const uint32_t PrecedingIterator_Clone_m1082336267_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * PrecedingIterator_Clone_m1082336267 (PrecedingIterator_t1039893511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrecedingIterator_Clone_m1082336267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PrecedingIterator_t1039893511 * L_0 = (PrecedingIterator_t1039893511 *)il2cpp_codegen_object_new(PrecedingIterator_t1039893511_il2cpp_TypeInfo_var);
		PrecedingIterator__ctor_m3696965713(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PrecedingIterator::MoveNextCore()
extern "C"  bool PrecedingIterator_MoveNextCore_m3322136244 (PrecedingIterator_t1039893511 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_started_7();
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		__this->set_started_7((bool)1);
		XPathNavigator_t1075073278 * L_2 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_2);
	}

IL_002a:
	{
		V_0 = (bool)1;
		goto IL_009b;
	}

IL_0031:
	{
		goto IL_0069;
	}

IL_0036:
	{
		goto IL_0054;
	}

IL_003b:
	{
		XPathNavigator_t1075073278 * L_3 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
		if (L_4)
		{
			goto IL_0054;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0054:
	{
		XPathNavigator_t1075073278 * L_5 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_5);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_0079;
	}

IL_0069:
	{
		XPathNavigator_t1075073278 * L_7 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		if (!L_8)
		{
			goto IL_0036;
		}
	}

IL_0079:
	{
		XPathNavigator_t1075073278 * L_9 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		XPathNavigator_t1075073278 * L_10 = __this->get_startPosition_8();
		NullCheck(L_9);
		bool L_11 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_9, L_10);
		if (!L_11)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_009b;
	}

IL_0094:
	{
		V_0 = (bool)0;
		goto IL_00a1;
	}

IL_009b:
	{
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_0031;
		}
	}

IL_00a1:
	{
		XPathNavigator_t1075073278 * L_13 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		XPathNavigator_t1075073278 * L_14 = __this->get_startPosition_8();
		NullCheck(L_13);
		int32_t L_15 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_13, L_14);
		if (!L_15)
		{
			goto IL_00c0;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.PrecedingIterator::get_ReverseAxis()
extern "C"  bool PrecedingIterator_get_ReverseAxis_m3280303211 (PrecedingIterator_t1039893511 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m3406853599 (PrecedingSiblingIterator_t1318768689 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set_startPosition_8(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.PrecedingSiblingIterator::.ctor(System.Xml.XPath.PrecedingSiblingIterator)
extern "C"  void PrecedingSiblingIterator__ctor_m2932109425 (PrecedingSiblingIterator_t1318768689 * __this, PrecedingSiblingIterator_t1318768689 * ___other0, const MethodInfo* method)
{
	{
		PrecedingSiblingIterator_t1318768689 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		PrecedingSiblingIterator_t1318768689 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get_startPosition_8();
		__this->set_startPosition_8(L_2);
		PrecedingSiblingIterator_t1318768689 * L_3 = ___other0;
		NullCheck(L_3);
		bool L_4 = L_3->get_started_7();
		__this->set_started_7(L_4);
		PrecedingSiblingIterator_t1318768689 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = L_5->get_finished_6();
		__this->set_finished_6(L_6);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PrecedingSiblingIterator::Clone()
extern Il2CppClass* PrecedingSiblingIterator_t1318768689_il2cpp_TypeInfo_var;
extern const uint32_t PrecedingSiblingIterator_Clone_m1927481107_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * PrecedingSiblingIterator_Clone_m1927481107 (PrecedingSiblingIterator_t1318768689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrecedingSiblingIterator_Clone_m1927481107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PrecedingSiblingIterator_t1318768689 * L_0 = (PrecedingSiblingIterator_t1318768689 *)il2cpp_codegen_object_new(PrecedingSiblingIterator_t1318768689_il2cpp_TypeInfo_var);
		PrecedingSiblingIterator__ctor_m2932109425(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::MoveNextCore()
extern "C"  bool PrecedingSiblingIterator_MoveNextCore_m2380337234 (PrecedingSiblingIterator_t1318768689 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_started_7();
		if (L_1)
		{
			goto IL_0070;
		}
	}
	{
		__this->set_started_7((bool)1);
		XPathNavigator_t1075073278 * L_2 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0047;
	}

IL_003e:
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0047:
	{
		XPathNavigator_t1075073278 * L_6 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_6);
		VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst() */, L_6);
		XPathNavigator_t1075073278 * L_7 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		XPathNavigator_t1075073278 * L_8 = __this->get_startPosition_8();
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_7, L_8);
		if (L_9)
		{
			goto IL_006b;
		}
	}
	{
		return (bool)1;
	}

IL_006b:
	{
		goto IL_0089;
	}

IL_0070:
	{
		XPathNavigator_t1075073278 * L_10 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_10);
		if (L_11)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_0089:
	{
		XPathNavigator_t1075073278 * L_12 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		XPathNavigator_t1075073278 * L_13 = __this->get_startPosition_8();
		NullCheck(L_12);
		int32_t L_14 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_12, L_13);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}

IL_00a8:
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::get_ReverseAxis()
extern "C"  bool PrecedingSiblingIterator_get_ReverseAxis_m872373773 (PrecedingSiblingIterator_t1318768689 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.Expression)
extern "C"  void PredicateIterator__ctor_m3004206002 (PredicateIterator_t3158177019 * __this, BaseIterator_t1327316739 * ___iter0, Expression_t2556460284 * ___pred1, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		__this->set__iter_3(L_2);
		Expression_t2556460284 * L_3 = ___pred1;
		__this->set__pred_4(L_3);
		Expression_t2556460284 * L_4 = ___pred1;
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		__this->set_resType_5(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.PredicateIterator::.ctor(System.Xml.XPath.PredicateIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator__ctor_m1246946409_MetadataUsageId;
extern "C"  void PredicateIterator__ctor_m1246946409 (PredicateIterator_t3158177019 * __this, PredicateIterator_t3158177019 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator__ctor_m1246946409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PredicateIterator_t3158177019 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		PredicateIterator_t3158177019 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t1327316739 * L_2 = L_1->get__iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iter_3(((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		PredicateIterator_t3158177019 * L_4 = ___other0;
		NullCheck(L_4);
		Expression_t2556460284 * L_5 = L_4->get__pred_4();
		__this->set__pred_4(L_5);
		PredicateIterator_t3158177019 * L_6 = ___other0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_resType_5();
		__this->set_resType_5(L_7);
		PredicateIterator_t3158177019 * L_8 = ___other0;
		NullCheck(L_8);
		bool L_9 = L_8->get_finished_6();
		__this->set_finished_6(L_9);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.PredicateIterator::Clone()
extern Il2CppClass* PredicateIterator_t3158177019_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator_Clone_m2751366551_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * PredicateIterator_Clone_m2751366551 (PredicateIterator_t3158177019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator_Clone_m2751366551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PredicateIterator_t3158177019 * L_0 = (PredicateIterator_t3158177019 *)il2cpp_codegen_object_new(PredicateIterator_t3158177019_il2cpp_TypeInfo_var);
		PredicateIterator__ctor_m1246946409(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.PredicateIterator::MoveNextCore()
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t PredicateIterator_MoveNextCore_m3008221096_MetadataUsageId;
extern "C"  bool PredicateIterator_MoveNextCore_m3008221096 (PredicateIterator_t3158177019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PredicateIterator_MoveNextCore_m3008221096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_finished_6();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		goto IL_00cc;
	}

IL_0012:
	{
		int32_t L_1 = __this->get_resType_5();
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)5)))
		{
			goto IL_0057;
		}
	}
	{
		goto IL_00aa;
	}

IL_002b:
	{
		Expression_t2556460284 * L_4 = __this->get__pred_4();
		BaseIterator_t1327316739 * L_5 = __this->get__iter_3();
		NullCheck(L_4);
		double L_6 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		BaseIterator_t1327316739 * L_7 = __this->get__iter_3();
		NullCheck(L_7);
		int32_t L_8 = BaseIterator_get_ComparablePosition_m1201108051(L_7, /*hidden argument*/NULL);
		if ((((double)L_6) == ((double)(((double)((double)L_8))))))
		{
			goto IL_0052;
		}
	}
	{
		goto IL_00cc;
	}

IL_0052:
	{
		goto IL_00ca;
	}

IL_0057:
	{
		Expression_t2556460284 * L_9 = __this->get__pred_4();
		BaseIterator_t1327316739 * L_10 = __this->get__iter_3();
		NullCheck(L_9);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_9, L_10);
		V_0 = L_11;
		Il2CppObject * L_12 = V_0;
		if (!((Il2CppObject *)IsInstSealed(L_12, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0095;
		}
	}
	{
		Il2CppObject * L_13 = V_0;
		BaseIterator_t1327316739 * L_14 = __this->get__iter_3();
		NullCheck(L_14);
		int32_t L_15 = BaseIterator_get_ComparablePosition_m1201108051(L_14, /*hidden argument*/NULL);
		if ((((double)((*(double*)((double*)UnBox (L_13, Double_t3868226565_il2cpp_TypeInfo_var))))) == ((double)(((double)((double)L_15))))))
		{
			goto IL_0090;
		}
	}
	{
		goto IL_00cc;
	}

IL_0090:
	{
		goto IL_00a5;
	}

IL_0095:
	{
		Il2CppObject * L_16 = V_0;
		bool L_17 = XPathFunctions_ToBoolean_m2654560037(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00cc;
	}

IL_00a5:
	{
		goto IL_00ca;
	}

IL_00aa:
	{
		Expression_t2556460284 * L_18 = __this->get__pred_4();
		BaseIterator_t1327316739 * L_19 = __this->get__iter_3();
		NullCheck(L_18);
		bool L_20 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_18, L_19);
		if (L_20)
		{
			goto IL_00c5;
		}
	}
	{
		goto IL_00cc;
	}

IL_00c5:
	{
		goto IL_00ca;
	}

IL_00ca:
	{
		return (bool)1;
	}

IL_00cc:
	{
		BaseIterator_t1327316739 * L_21 = __this->get__iter_3();
		NullCheck(L_21);
		bool L_22 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_21);
		if (L_22)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_finished_6((bool)1);
		return (bool)0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.PredicateIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * PredicateIterator_get_Current_m508558563 (PredicateIterator_t3158177019 * __this, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_001c;
	}

IL_0011:
	{
		BaseIterator_t1327316739 * L_1 = __this->get__iter_3();
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.PredicateIterator::get_ReverseAxis()
extern "C"  bool PredicateIterator_get_ReverseAxis_m1577932535 (PredicateIterator_t3158177019 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XPath.BaseIterator::get_ReverseAxis() */, L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.PredicateIterator::ToString()
extern "C"  String_t* PredicateIterator_ToString_m891628812 (PredicateIterator_t3158177019 * __this, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = __this->get__iter_3();
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.RelationalExpr::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void RelationalExpr__ctor_m506647938 (RelationalExpr_t3718551712 * __this, Expression_t2556460284 * ___left0, Expression_t2556460284 * ___right1, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = ___left0;
		Expression_t2556460284 * L_1 = ___right1;
		ExprBoolean__ctor_m1871519559(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::get_StaticValueAsBoolean()
extern "C"  bool RelationalExpr_get_StaticValueAsBoolean_m1605638618 (RelationalExpr_t3718551712 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.ExprBinary::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Expression_t2556460284 * L_1 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		NullCheck(L_3);
		double L_4 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_3);
		bool L_5 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_2, L_4);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002d;
	}

IL_002c:
	{
		G_B3_0 = 0;
	}

IL_002d:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t RelationalExpr_EvaluateBoolean_m1387308217_MetadataUsageId;
extern "C"  bool RelationalExpr_EvaluateBoolean_m1387308217 (RelationalExpr_t3718551712 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RelationalExpr_EvaluateBoolean_m1387308217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	Expression_t2556460284 * V_3 = NULL;
	Expression_t2556460284 * V_4 = NULL;
	int32_t V_5 = 0;
	bool V_6 = false;
	bool V_7 = false;
	BaseIterator_t1327316739 * V_8 = NULL;
	double V_9 = 0.0;
	BaseIterator_t1327316739 * V_10 = NULL;
	ArrayList_t3948406897 * V_11 = NULL;
	double V_12 = 0.0;
	int32_t V_13 = 0;
	{
		Expression_t2556460284 * L_0 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		int32_t L_5 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)5))))
		{
			goto IL_0033;
		}
	}
	{
		Expression_t2556460284 * L_7 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_8 = ___iter0;
		NullCheck(L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		int32_t L_10 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0033:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)5))))
		{
			goto IL_004c;
		}
	}
	{
		Expression_t2556460284 * L_12 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_13 = ___iter0;
		NullCheck(L_12);
		Il2CppObject * L_14 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_12, L_13);
		int32_t L_15 = Expression_GetReturnType_m2715398160(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_004c:
	{
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 1;
	}

IL_0055:
	{
		int32_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)4))))
		{
			goto IL_005e;
		}
	}
	{
		V_1 = 1;
	}

IL_005e:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)3)))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_01cf;
		}
	}

IL_006c:
	{
		V_2 = (bool)0;
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)3)))
		{
			goto IL_0093;
		}
	}
	{
		V_2 = (bool)1;
		Expression_t2556460284 * L_21 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		V_3 = L_21;
		Expression_t2556460284 * L_22 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		V_4 = L_22;
		int32_t L_23 = V_0;
		V_5 = L_23;
		int32_t L_24 = V_1;
		V_0 = L_24;
		int32_t L_25 = V_5;
		V_1 = L_25;
		goto IL_00a2;
	}

IL_0093:
	{
		Expression_t2556460284 * L_26 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		V_3 = L_26;
		Expression_t2556460284 * L_27 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		V_4 = L_27;
	}

IL_00a2:
	{
		int32_t L_28 = V_1;
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_00d2;
		}
	}
	{
		Expression_t2556460284 * L_29 = V_3;
		BaseIterator_t1327316739 * L_30 = ___iter0;
		NullCheck(L_29);
		bool L_31 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_29, L_30);
		V_6 = L_31;
		Expression_t2556460284 * L_32 = V_4;
		BaseIterator_t1327316739 * L_33 = ___iter0;
		NullCheck(L_32);
		bool L_34 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_32, L_33);
		V_7 = L_34;
		bool L_35 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_36 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		bool L_37 = V_7;
		double L_38 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		bool L_39 = V_2;
		bool L_40 = RelationalExpr_Compare_m3318673384(__this, L_36, L_38, L_39, /*hidden argument*/NULL);
		return L_40;
	}

IL_00d2:
	{
		Expression_t2556460284 * L_41 = V_3;
		BaseIterator_t1327316739 * L_42 = ___iter0;
		NullCheck(L_41);
		BaseIterator_t1327316739 * L_43 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_41, L_42);
		V_8 = L_43;
		int32_t L_44 = V_1;
		if (!L_44)
		{
			goto IL_00e8;
		}
	}
	{
		int32_t L_45 = V_1;
		if ((!(((uint32_t)L_45) == ((uint32_t)1))))
		{
			goto IL_0129;
		}
	}

IL_00e8:
	{
		Expression_t2556460284 * L_46 = V_4;
		BaseIterator_t1327316739 * L_47 = ___iter0;
		NullCheck(L_46);
		double L_48 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_46, L_47);
		V_9 = L_48;
		goto IL_0118;
	}

IL_00f7:
	{
		BaseIterator_t1327316739 * L_49 = V_8;
		NullCheck(L_49);
		XPathNavigator_t1075073278 * L_50 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_49);
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_50);
		double L_52 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		double L_53 = V_9;
		bool L_54 = V_2;
		bool L_55 = RelationalExpr_Compare_m3318673384(__this, L_52, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0118;
		}
	}
	{
		return (bool)1;
	}

IL_0118:
	{
		BaseIterator_t1327316739 * L_56 = V_8;
		NullCheck(L_56);
		bool L_57 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_56);
		if (L_57)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_01cd;
	}

IL_0129:
	{
		int32_t L_58 = V_1;
		if ((!(((uint32_t)L_58) == ((uint32_t)3))))
		{
			goto IL_01cd;
		}
	}
	{
		Expression_t2556460284 * L_59 = V_4;
		BaseIterator_t1327316739 * L_60 = ___iter0;
		NullCheck(L_59);
		BaseIterator_t1327316739 * L_61 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_59, L_60);
		V_10 = L_61;
		ArrayList_t3948406897 * L_62 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_62, /*hidden argument*/NULL);
		V_11 = L_62;
		goto IL_0164;
	}

IL_0146:
	{
		ArrayList_t3948406897 * L_63 = V_11;
		BaseIterator_t1327316739 * L_64 = V_8;
		NullCheck(L_64);
		XPathNavigator_t1075073278 * L_65 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_64);
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_65);
		double L_67 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		double L_68 = L_67;
		Il2CppObject * L_69 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_63);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_63, L_69);
	}

IL_0164:
	{
		BaseIterator_t1327316739 * L_70 = V_8;
		NullCheck(L_70);
		bool L_71 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_70);
		if (L_71)
		{
			goto IL_0146;
		}
	}
	{
		goto IL_01c1;
	}

IL_0175:
	{
		BaseIterator_t1327316739 * L_72 = V_10;
		NullCheck(L_72);
		XPathNavigator_t1075073278 * L_73 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_72);
		NullCheck(L_73);
		String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_73);
		double L_75 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_74, /*hidden argument*/NULL);
		V_12 = L_75;
		V_13 = 0;
		goto IL_01b3;
	}

IL_0190:
	{
		ArrayList_t3948406897 * L_76 = V_11;
		int32_t L_77 = V_13;
		NullCheck(L_76);
		Il2CppObject * L_78 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_76, L_77);
		double L_79 = V_12;
		bool L_80 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, ((*(double*)((double*)UnBox (L_78, Double_t3868226565_il2cpp_TypeInfo_var)))), L_79);
		if (!L_80)
		{
			goto IL_01ad;
		}
	}
	{
		return (bool)1;
	}

IL_01ad:
	{
		int32_t L_81 = V_13;
		V_13 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01b3:
	{
		int32_t L_82 = V_13;
		ArrayList_t3948406897 * L_83 = V_11;
		NullCheck(L_83);
		int32_t L_84 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_83);
		if ((((int32_t)L_82) < ((int32_t)L_84)))
		{
			goto IL_0190;
		}
	}

IL_01c1:
	{
		BaseIterator_t1327316739 * L_85 = V_10;
		NullCheck(L_85);
		bool L_86 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_85);
		if (L_86)
		{
			goto IL_0175;
		}
	}

IL_01cd:
	{
		return (bool)0;
	}

IL_01cf:
	{
		Expression_t2556460284 * L_87 = ((ExprBinary_t1545048250 *)__this)->get__left_0();
		BaseIterator_t1327316739 * L_88 = ___iter0;
		NullCheck(L_87);
		double L_89 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_87, L_88);
		Expression_t2556460284 * L_90 = ((ExprBinary_t1545048250 *)__this)->get__right_1();
		BaseIterator_t1327316739 * L_91 = ___iter0;
		NullCheck(L_90);
		double L_92 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_90, L_91);
		bool L_93 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_89, L_92);
		return L_93;
	}
}
// System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double,System.Boolean)
extern "C"  bool RelationalExpr_Compare_m3318673384 (RelationalExpr_t3718551712 * __this, double ___arg10, double ___arg21, bool ___fReverse2, const MethodInfo* method)
{
	{
		bool L_0 = ___fReverse2;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		double L_1 = ___arg21;
		double L_2 = ___arg10;
		bool L_3 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_1, L_2);
		return L_3;
	}

IL_000f:
	{
		double L_4 = ___arg10;
		double L_5 = ___arg21;
		bool L_6 = VirtFuncInvoker2< bool, double, double >::Invoke(21 /* System.Boolean System.Xml.XPath.RelationalExpr::Compare(System.Double,System.Double) */, __this, L_4, L_5);
		return L_6;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void SelfIterator__ctor_m2835358572 (SelfIterator_t197024894 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		SimpleIterator__ctor_m1076221202(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void SelfIterator__ctor_m4150585400 (SelfIterator_t197024894 * __this, XPathNavigator_t1075073278 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = ___nav0;
		Il2CppObject * L_1 = ___nsm1;
		SimpleIterator__ctor_m1024992466(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.SelfIterator::.ctor(System.Xml.XPath.SelfIterator,System.Boolean)
extern "C"  void SelfIterator__ctor_m3078894092 (SelfIterator_t197024894 * __this, SelfIterator_t197024894 * ___other0, bool ___clone1, const MethodInfo* method)
{
	{
		SelfIterator_t197024894 * L_0 = ___other0;
		SimpleIterator__ctor_m403425804(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SelfIterator::Clone()
extern Il2CppClass* SelfIterator_t197024894_il2cpp_TypeInfo_var;
extern const uint32_t SelfIterator_Clone_m3186820838_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * SelfIterator_Clone_m3186820838 (SelfIterator_t197024894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SelfIterator_Clone_m3186820838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SelfIterator_t197024894 * L_0 = (SelfIterator_t197024894 *)il2cpp_codegen_object_new(SelfIterator_t197024894_il2cpp_TypeInfo_var);
		SelfIterator__ctor_m3078894092(L_0, __this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SelfIterator::MoveNextCore()
extern "C"  bool SelfIterator_MoveNextCore_m260577695 (SelfIterator_t197024894 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)1;
	}

IL_000d:
	{
		return (bool)0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SelfIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * SelfIterator_get_Current_m4205100576 (SelfIterator_t197024894 * __this, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_0017;
	}

IL_0011:
	{
		XPathNavigator_t1075073278 * L_1 = ((SimpleIterator_t544647972 *)__this)->get__nav_3();
		G_B3_0 = L_1;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void SimpleIterator__ctor_m1076221202 (SimpleIterator_t544647972 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_2);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		__this->set_skipfirst_5((bool)1);
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
	}

IL_0025:
	{
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_5);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(L_7);
		XPathNavigator_t1075073278 * L_8 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_7);
		NullCheck(L_8);
		XPathNavigator_t1075073278 * L_9 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_8);
		__this->set__nav_3(L_9);
	}

IL_0042:
	{
		return;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.SimpleIterator,System.Boolean)
extern "C"  void SimpleIterator__ctor_m403425804 (SimpleIterator_t544647972 * __this, SimpleIterator_t544647972 * ___other0, bool ___clone1, const MethodInfo* method)
{
	SimpleIterator_t544647972 * G_B3_0 = NULL;
	SimpleIterator_t544647972 * G_B2_0 = NULL;
	XPathNavigator_t1075073278 * G_B4_0 = NULL;
	SimpleIterator_t544647972 * G_B4_1 = NULL;
	{
		SimpleIterator_t544647972 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SimpleIterator_t544647972 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = L_1->get__nav_3();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		bool L_3 = ___clone1;
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B3_0 = __this;
			goto IL_0029;
		}
	}
	{
		SimpleIterator_t544647972 * L_4 = ___other0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = L_4->get__nav_3();
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		goto IL_002f;
	}

IL_0029:
	{
		SimpleIterator_t544647972 * L_7 = ___other0;
		NullCheck(L_7);
		XPathNavigator_t1075073278 * L_8 = L_7->get__nav_3();
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_002f:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__nav_3(G_B4_0);
	}

IL_0034:
	{
		SimpleIterator_t544647972 * L_9 = ___other0;
		NullCheck(L_9);
		bool L_10 = L_9->get_skipfirst_5();
		__this->set_skipfirst_5(L_10);
		return;
	}
}
// System.Void System.Xml.XPath.SimpleIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void SimpleIterator__ctor_m1024992466 (SimpleIterator_t544647972 * __this, XPathNavigator_t1075073278 * ___nav0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		XPathNavigator_t1075073278 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		__this->set__nav_3(L_2);
		return;
	}
}
// System.Boolean System.Xml.XPath.SimpleIterator::MoveNext()
extern "C"  bool SimpleIterator_MoveNext_m2872354886 (SimpleIterator_t544647972 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_skipfirst_5();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		XPathNavigator_t1075073278 * L_1 = __this->get__nav_3();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_skipfirst_5((bool)0);
		BaseIterator_SetPosition_m2844760437(__this, 1, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0028:
	{
		bool L_2 = BaseIterator_MoveNext_m2848019493(__this, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * SimpleIterator_get_Current_m1974708922 (SimpleIterator_t544647972 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (XPathNavigator_t1075073278 *)NULL;
	}

IL_000d:
	{
		XPathNavigator_t1075073278 * L_1 = __this->get__nav_3();
		__this->set__current_4(L_1);
		XPathNavigator_t1075073278 * L_2 = __this->get__current_4();
		return L_2;
	}
}
// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeSet)
extern "C"  void SimpleSlashIterator__ctor_m3208158132 (SimpleSlashIterator_t1419496431 * __this, BaseIterator_t1327316739 * ___left0, NodeSet_t2875795446 * ___expr1, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___left0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___left0;
		__this->set__left_4(L_2);
		NodeSet_t2875795446 * L_3 = ___expr1;
		__this->set__expr_3(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.SimpleSlashIterator::.ctor(System.Xml.XPath.SimpleSlashIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t SimpleSlashIterator__ctor_m1778448769_MetadataUsageId;
extern "C"  void SimpleSlashIterator__ctor_m1778448769 (SimpleSlashIterator_t1419496431 * __this, SimpleSlashIterator_t1419496431 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleSlashIterator__ctor_m1778448769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleSlashIterator_t1419496431 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SimpleSlashIterator_t1419496431 * L_1 = ___other0;
		NullCheck(L_1);
		NodeSet_t2875795446 * L_2 = L_1->get__expr_3();
		__this->set__expr_3(L_2);
		SimpleSlashIterator_t1419496431 * L_3 = ___other0;
		NullCheck(L_3);
		BaseIterator_t1327316739 * L_4 = L_3->get__left_4();
		NullCheck(L_4);
		XPathNodeIterator_t1383168931 * L_5 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_4);
		__this->set__left_4(((BaseIterator_t1327316739 *)CastclassClass(L_5, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		SimpleSlashIterator_t1419496431 * L_6 = ___other0;
		NullCheck(L_6);
		BaseIterator_t1327316739 * L_7 = L_6->get__right_5();
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		SimpleSlashIterator_t1419496431 * L_8 = ___other0;
		NullCheck(L_8);
		BaseIterator_t1327316739 * L_9 = L_8->get__right_5();
		NullCheck(L_9);
		XPathNodeIterator_t1383168931 * L_10 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_9);
		__this->set__right_5(((BaseIterator_t1327316739 *)CastclassClass(L_10, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
	}

IL_004a:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SimpleSlashIterator::Clone()
extern Il2CppClass* SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var;
extern const uint32_t SimpleSlashIterator_Clone_m2687801187_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * SimpleSlashIterator_Clone_m2687801187 (SimpleSlashIterator_t1419496431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleSlashIterator_Clone_m2687801187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleSlashIterator_t1419496431 * L_0 = (SimpleSlashIterator_t1419496431 *)il2cpp_codegen_object_new(SimpleSlashIterator_t1419496431_il2cpp_TypeInfo_var);
		SimpleSlashIterator__ctor_m1778448769(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SimpleSlashIterator::MoveNextCore()
extern "C"  bool SimpleSlashIterator_MoveNextCore_m3882540316 (SimpleSlashIterator_t1419496431 * __this, const MethodInfo* method)
{
	{
		goto IL_002e;
	}

IL_0005:
	{
		BaseIterator_t1327316739 * L_0 = __this->get__left_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_0);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		NodeSet_t2875795446 * L_2 = __this->get__expr_3();
		BaseIterator_t1327316739 * L_3 = __this->get__left_4();
		NullCheck(L_2);
		BaseIterator_t1327316739 * L_4 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_2, L_3);
		__this->set__right_5(L_4);
	}

IL_002e:
	{
		BaseIterator_t1327316739 * L_5 = __this->get__right_5();
		if (!L_5)
		{
			goto IL_0005;
		}
	}
	{
		BaseIterator_t1327316739 * L_6 = __this->get__right_5();
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_6);
		if (!L_7)
		{
			goto IL_0005;
		}
	}
	{
		XPathNavigator_t1075073278 * L_8 = __this->get__current_6();
		if (L_8)
		{
			goto IL_006f;
		}
	}
	{
		BaseIterator_t1327316739 * L_9 = __this->get__right_5();
		NullCheck(L_9);
		XPathNavigator_t1075073278 * L_10 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_9);
		NullCheck(L_10);
		XPathNavigator_t1075073278 * L_11 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_10);
		__this->set__current_6(L_11);
		goto IL_00a0;
	}

IL_006f:
	{
		XPathNavigator_t1075073278 * L_12 = __this->get__current_6();
		BaseIterator_t1327316739 * L_13 = __this->get__right_5();
		NullCheck(L_13);
		XPathNavigator_t1075073278 * L_14 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_13);
		NullCheck(L_12);
		bool L_15 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_12, L_14);
		if (L_15)
		{
			goto IL_00a0;
		}
	}
	{
		BaseIterator_t1327316739 * L_16 = __this->get__right_5();
		NullCheck(L_16);
		XPathNavigator_t1075073278 * L_17 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_16);
		NullCheck(L_17);
		XPathNavigator_t1075073278 * L_18 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_17);
		__this->set__current_6(L_18);
	}

IL_00a0:
	{
		return (bool)1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleSlashIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * SimpleSlashIterator_get_Current_m3159180143 (SimpleSlashIterator_t1419496431 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = __this->get__current_6();
		return L_0;
	}
}
// System.Void System.Xml.XPath.SlashIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.NodeSet)
extern "C"  void SlashIterator__ctor_m2348632994 (SlashIterator_t384731969 * __this, BaseIterator_t1327316739 * ___iter0, NodeSet_t2875795446 * ___expr1, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___iter0;
		__this->set__iterLeft_3(L_2);
		NodeSet_t2875795446 * L_3 = ___expr1;
		__this->set__expr_5(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.SlashIterator::.ctor(System.Xml.XPath.SlashIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedList_t4117722949_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator__ctor_m1699614173_MetadataUsageId;
extern "C"  void SlashIterator__ctor_m1699614173 (SlashIterator_t384731969 * __this, SlashIterator_t384731969 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator__ctor_m1699614173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlashIterator_t384731969 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SlashIterator_t384731969 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t1327316739 * L_2 = L_1->get__iterLeft_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__iterLeft_3(((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		SlashIterator_t384731969 * L_4 = ___other0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_5 = L_4->get__iterRight_4();
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		SlashIterator_t384731969 * L_6 = ___other0;
		NullCheck(L_6);
		BaseIterator_t1327316739 * L_7 = L_6->get__iterRight_4();
		NullCheck(L_7);
		XPathNodeIterator_t1383168931 * L_8 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_7);
		__this->set__iterRight_4(((BaseIterator_t1327316739 *)CastclassClass(L_8, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
	}

IL_003e:
	{
		SlashIterator_t384731969 * L_9 = ___other0;
		NullCheck(L_9);
		NodeSet_t2875795446 * L_10 = L_9->get__expr_5();
		__this->set__expr_5(L_10);
		SlashIterator_t384731969 * L_11 = ___other0;
		NullCheck(L_11);
		SortedList_t4117722949 * L_12 = L_11->get__iterList_6();
		if (!L_12)
		{
			goto IL_006b;
		}
	}
	{
		SlashIterator_t384731969 * L_13 = ___other0;
		NullCheck(L_13);
		SortedList_t4117722949 * L_14 = L_13->get__iterList_6();
		NullCheck(L_14);
		Il2CppObject * L_15 = VirtFuncInvoker0< Il2CppObject * >::Invoke(35 /* System.Object System.Collections.SortedList::Clone() */, L_14);
		__this->set__iterList_6(((SortedList_t4117722949 *)CastclassClass(L_15, SortedList_t4117722949_il2cpp_TypeInfo_var)));
	}

IL_006b:
	{
		SlashIterator_t384731969 * L_16 = ___other0;
		NullCheck(L_16);
		bool L_17 = L_16->get__finished_7();
		__this->set__finished_7(L_17);
		SlashIterator_t384731969 * L_18 = ___other0;
		NullCheck(L_18);
		BaseIterator_t1327316739 * L_19 = L_18->get__nextIterRight_8();
		if (!L_19)
		{
			goto IL_0098;
		}
	}
	{
		SlashIterator_t384731969 * L_20 = ___other0;
		NullCheck(L_20);
		BaseIterator_t1327316739 * L_21 = L_20->get__nextIterRight_8();
		NullCheck(L_21);
		XPathNodeIterator_t1383168931 * L_22 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_21);
		__this->set__nextIterRight_8(((BaseIterator_t1327316739 *)CastclassClass(L_22, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
	}

IL_0098:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SlashIterator::Clone()
extern Il2CppClass* SlashIterator_t384731969_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator_Clone_m3185525137_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * SlashIterator_Clone_m3185525137 (SlashIterator_t384731969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator_Clone_m3185525137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SlashIterator_t384731969 * L_0 = (SlashIterator_t384731969 *)il2cpp_codegen_object_new(SlashIterator_t384731969_il2cpp_TypeInfo_var);
		SlashIterator__ctor_m1699614173(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SlashIterator::MoveNextCore()
extern Il2CppClass* XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var;
extern Il2CppClass* SortedList_t4117722949_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t SlashIterator_MoveNextCore_m2132081902_MetadataUsageId;
extern "C"  bool SlashIterator_MoveNextCore_m2132081902 (SlashIterator_t384731969 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SlashIterator_MoveNextCore_m2132081902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		bool L_0 = __this->get__finished_7();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		BaseIterator_t1327316739 * L_1 = __this->get__iterRight_4();
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		BaseIterator_t1327316739 * L_2 = __this->get__iterLeft_3();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		NodeSet_t2875795446 * L_4 = __this->get__expr_5();
		BaseIterator_t1327316739 * L_5 = __this->get__iterLeft_3();
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_6 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		__this->set__iterRight_4(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var);
		XPathIteratorComparer_t3727165134 * L_7 = ((XPathIteratorComparer_t3727165134_StaticFields*)XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		SortedList_t4117722949 * L_8 = (SortedList_t4117722949 *)il2cpp_codegen_object_new(SortedList_t4117722949_il2cpp_TypeInfo_var);
		SortedList__ctor_m3446713763(L_8, L_7, /*hidden argument*/NULL);
		__this->set__iterList_6(L_8);
	}

IL_0051:
	{
		goto IL_00f0;
	}

IL_0056:
	{
		SortedList_t4117722949 * L_9 = __this->get__iterList_6();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.Collections.SortedList::get_Count() */, L_9);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_009d;
		}
	}
	{
		SortedList_t4117722949 * L_11 = __this->get__iterList_6();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.Collections.SortedList::get_Count() */, L_11);
		V_0 = ((int32_t)((int32_t)L_12-(int32_t)1));
		SortedList_t4117722949 * L_13 = __this->get__iterList_6();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(39 /* System.Object System.Collections.SortedList::GetByIndex(System.Int32) */, L_13, L_14);
		__this->set__iterRight_4(((BaseIterator_t1327316739 *)CastclassClass(L_15, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		SortedList_t4117722949 * L_16 = __this->get__iterList_6();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		VirtActionInvoker1< int32_t >::Invoke(36 /* System.Void System.Collections.SortedList::RemoveAt(System.Int32) */, L_16, L_17);
		goto IL_0100;
	}

IL_009d:
	{
		BaseIterator_t1327316739 * L_18 = __this->get__nextIterRight_8();
		if (!L_18)
		{
			goto IL_00c0;
		}
	}
	{
		BaseIterator_t1327316739 * L_19 = __this->get__nextIterRight_8();
		__this->set__iterRight_4(L_19);
		__this->set__nextIterRight_8((BaseIterator_t1327316739 *)NULL);
		goto IL_0100;
	}

IL_00c0:
	{
		BaseIterator_t1327316739 * L_20 = __this->get__iterLeft_3();
		NullCheck(L_20);
		bool L_21 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_20);
		if (L_21)
		{
			goto IL_00d9;
		}
	}
	{
		__this->set__finished_7((bool)1);
		return (bool)0;
	}

IL_00d9:
	{
		NodeSet_t2875795446 * L_22 = __this->get__expr_5();
		BaseIterator_t1327316739 * L_23 = __this->get__iterLeft_3();
		NullCheck(L_22);
		BaseIterator_t1327316739 * L_24 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_22, L_23);
		__this->set__iterRight_4(L_24);
	}

IL_00f0:
	{
		BaseIterator_t1327316739 * L_25 = __this->get__iterRight_4();
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (!L_26)
		{
			goto IL_0056;
		}
	}

IL_0100:
	{
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_0107:
	{
		V_1 = (bool)0;
		BaseIterator_t1327316739 * L_27 = __this->get__nextIterRight_8();
		if (L_27)
		{
			goto IL_0176;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_014e;
	}

IL_011b:
	{
		BaseIterator_t1327316739 * L_28 = __this->get__iterLeft_3();
		NullCheck(L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_28);
		if (!L_29)
		{
			goto IL_0147;
		}
	}
	{
		NodeSet_t2875795446 * L_30 = __this->get__expr_5();
		BaseIterator_t1327316739 * L_31 = __this->get__iterLeft_3();
		NullCheck(L_30);
		BaseIterator_t1327316739 * L_32 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_30, L_31);
		__this->set__nextIterRight_8(L_32);
		goto IL_014e;
	}

IL_0147:
	{
		V_2 = (bool)1;
		goto IL_0169;
	}

IL_014e:
	{
		BaseIterator_t1327316739 * L_33 = __this->get__nextIterRight_8();
		if (!L_33)
		{
			goto IL_011b;
		}
	}
	{
		BaseIterator_t1327316739 * L_34 = __this->get__nextIterRight_8();
		NullCheck(L_34);
		bool L_35 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_34);
		if (!L_35)
		{
			goto IL_011b;
		}
	}

IL_0169:
	{
		bool L_36 = V_2;
		if (!L_36)
		{
			goto IL_0176;
		}
	}
	{
		__this->set__nextIterRight_8((BaseIterator_t1327316739 *)NULL);
	}

IL_0176:
	{
		BaseIterator_t1327316739 * L_37 = __this->get__nextIterRight_8();
		if (!L_37)
		{
			goto IL_025e;
		}
	}
	{
		BaseIterator_t1327316739 * L_38 = __this->get__iterRight_4();
		NullCheck(L_38);
		XPathNavigator_t1075073278 * L_39 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_38);
		BaseIterator_t1327316739 * L_40 = __this->get__nextIterRight_8();
		NullCheck(L_40);
		XPathNavigator_t1075073278 * L_41 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_40);
		NullCheck(L_39);
		int32_t L_42 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_41);
		V_4 = L_42;
		int32_t L_43 = V_4;
		if ((((int32_t)L_43) == ((int32_t)1)))
		{
			goto IL_01b3;
		}
	}
	{
		int32_t L_44 = V_4;
		if ((((int32_t)L_44) == ((int32_t)2)))
		{
			goto IL_01e4;
		}
	}
	{
		goto IL_025e;
	}

IL_01b3:
	{
		SortedList_t4117722949 * L_45 = __this->get__iterList_6();
		BaseIterator_t1327316739 * L_46 = __this->get__iterRight_4();
		BaseIterator_t1327316739 * L_47 = __this->get__iterRight_4();
		NullCheck(L_45);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Void System.Collections.SortedList::set_Item(System.Object,System.Object) */, L_45, L_46, L_47);
		BaseIterator_t1327316739 * L_48 = __this->get__nextIterRight_8();
		__this->set__iterRight_4(L_48);
		__this->set__nextIterRight_8((BaseIterator_t1327316739 *)NULL);
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_01e4:
	{
		BaseIterator_t1327316739 * L_49 = __this->get__nextIterRight_8();
		NullCheck(L_49);
		bool L_50 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_49);
		if (L_50)
		{
			goto IL_0200;
		}
	}
	{
		__this->set__nextIterRight_8((BaseIterator_t1327316739 *)NULL);
		goto IL_0257;
	}

IL_0200:
	{
		SortedList_t4117722949 * L_51 = __this->get__iterList_6();
		NullCheck(L_51);
		int32_t L_52 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.Collections.SortedList::get_Count() */, L_51);
		V_3 = L_52;
		SortedList_t4117722949 * L_53 = __this->get__iterList_6();
		BaseIterator_t1327316739 * L_54 = __this->get__nextIterRight_8();
		BaseIterator_t1327316739 * L_55 = __this->get__nextIterRight_8();
		NullCheck(L_53);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Void System.Collections.SortedList::set_Item(System.Object,System.Object) */, L_53, L_54, L_55);
		int32_t L_56 = V_3;
		SortedList_t4117722949 * L_57 = __this->get__iterList_6();
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.Collections.SortedList::get_Count() */, L_57);
		if ((((int32_t)L_56) == ((int32_t)L_58)))
		{
			goto IL_0257;
		}
	}
	{
		SortedList_t4117722949 * L_59 = __this->get__iterList_6();
		int32_t L_60 = V_3;
		NullCheck(L_59);
		Il2CppObject * L_61 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(39 /* System.Object System.Collections.SortedList::GetByIndex(System.Int32) */, L_59, L_60);
		__this->set__nextIterRight_8(((BaseIterator_t1327316739 *)CastclassClass(L_61, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		SortedList_t4117722949 * L_62 = __this->get__iterList_6();
		int32_t L_63 = V_3;
		NullCheck(L_62);
		VirtActionInvoker1< int32_t >::Invoke(36 /* System.Void System.Collections.SortedList::RemoveAt(System.Int32) */, L_62, L_63);
	}

IL_0257:
	{
		V_1 = (bool)1;
		goto IL_025e;
	}

IL_025e:
	{
		bool L_64 = V_1;
		if (L_64)
		{
			goto IL_0107;
		}
	}
	{
		return (bool)1;
	}
	// Dead block : IL_0266: br IL_0051
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SlashIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * SlashIterator_get_Current_m812878301 (SlashIterator_t384731969 * __this, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_001c;
	}

IL_0011:
	{
		BaseIterator_t1327316739 * L_1 = __this->get__iterRight_4();
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.SortedIterator::.ctor(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator__ctor_m2178587357_MetadataUsageId;
extern "C"  void SortedIterator__ctor_m2178587357 (SortedIterator_t4014821679 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator__ctor_m2178587357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathNavigator_t1075073278 * V_2 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		ArrayList_t3948406897 * L_2 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_2, /*hidden argument*/NULL);
		__this->set_list_3(L_2);
		goto IL_0033;
	}

IL_001c:
	{
		ArrayList_t3948406897 * L_3 = __this->get_list_3();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		NullCheck(L_3);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_3, L_6);
	}

IL_0033:
	{
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_7);
		if (L_8)
		{
			goto IL_001c;
		}
	}
	{
		ArrayList_t3948406897 * L_9 = __this->get_list_3();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_9);
		if (L_10)
		{
			goto IL_004f;
		}
	}
	{
		return;
	}

IL_004f:
	{
		ArrayList_t3948406897 * L_11 = __this->get_list_3();
		NullCheck(L_11);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_11, 0);
		V_0 = ((XPathNavigator_t1075073278 *)CastclassClass(L_12, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		ArrayList_t3948406897 * L_13 = __this->get_list_3();
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var);
		XPathNavigatorComparer_t2221114443 * L_14 = ((XPathNavigatorComparer_t2221114443_StaticFields*)XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_13, L_14);
		V_1 = 1;
		goto IL_00b1;
	}

IL_0078:
	{
		ArrayList_t3948406897 * L_15 = __this->get_list_3();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_17 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_15, L_16);
		V_2 = ((XPathNavigator_t1075073278 *)CastclassClass(L_17, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		XPathNavigator_t1075073278 * L_18 = V_0;
		XPathNavigator_t1075073278 * L_19 = V_2;
		NullCheck(L_18);
		bool L_20 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_18, L_19);
		if (!L_20)
		{
			goto IL_00ab;
		}
	}
	{
		ArrayList_t3948406897 * L_21 = __this->get_list_3();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		VirtActionInvoker1< int32_t >::Invoke(39 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_21, L_22);
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23-(int32_t)1));
		goto IL_00ad;
	}

IL_00ab:
	{
		XPathNavigator_t1075073278 * L_24 = V_2;
		V_0 = L_24;
	}

IL_00ad:
	{
		int32_t L_25 = V_1;
		V_1 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_26 = V_1;
		ArrayList_t3948406897 * L_27 = __this->get_list_3();
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_27);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_0078;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.XPath.SortedIterator::.ctor(System.Xml.XPath.SortedIterator)
extern "C"  void SortedIterator__ctor_m3092690865 (SortedIterator_t4014821679 * __this, SortedIterator_t4014821679 * ___other0, const MethodInfo* method)
{
	{
		SortedIterator_t4014821679 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		SortedIterator_t4014821679 * L_1 = ___other0;
		NullCheck(L_1);
		ArrayList_t3948406897 * L_2 = L_1->get_list_3();
		__this->set_list_3(L_2);
		SortedIterator_t4014821679 * L_3 = ___other0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_3);
		BaseIterator_SetPosition_m2844760437(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.SortedIterator::Clone()
extern Il2CppClass* SortedIterator_t4014821679_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator_Clone_m3148712725_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * SortedIterator_Clone_m3148712725 (SortedIterator_t4014821679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator_Clone_m3148712725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedIterator_t4014821679 * L_0 = (SortedIterator_t4014821679 *)il2cpp_codegen_object_new(SortedIterator_t4014821679_il2cpp_TypeInfo_var);
		SortedIterator__ctor_m3092690865(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.SortedIterator::MoveNextCore()
extern "C"  bool SortedIterator_MoveNextCore_m1723129296 (SortedIterator_t4014821679 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		ArrayList_t3948406897 * L_1 = __this->get_list_3();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		return (bool)((((int32_t)L_0) < ((int32_t)L_2))? 1 : 0);
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.SortedIterator::get_Current()
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t SortedIterator_get_Current_m2806041935_MetadataUsageId;
extern "C"  XPathNavigator_t1075073278 * SortedIterator_get_Current_m2806041935 (SortedIterator_t4014821679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SortedIterator_get_Current_m2806041935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_0029;
	}

IL_0011:
	{
		ArrayList_t3948406897 * L_1 = __this->get_list_3();
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)));
		G_B3_0 = ((XPathNavigator_t1075073278 *)CastclassClass(L_3, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Int32 System.Xml.XPath.SortedIterator::get_Count()
extern "C"  int32_t SortedIterator_get_Count_m3817323349 (SortedIterator_t4014821679 * __this, const MethodInfo* method)
{
	{
		ArrayList_t3948406897 * L_0 = __this->get_list_3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.UnionIterator::.ctor(System.Xml.XPath.BaseIterator,System.Xml.XPath.BaseIterator,System.Xml.XPath.BaseIterator)
extern "C"  void UnionIterator__ctor_m4002392457 (UnionIterator_t1391611539 * __this, BaseIterator_t1327316739 * ___iter0, BaseIterator_t1327316739 * ___left1, BaseIterator_t1327316739 * ___right2, const MethodInfo* method)
{
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_1 = BaseIterator_get_NamespaceManager_m3653702256(L_0, /*hidden argument*/NULL);
		BaseIterator__ctor_m3826808382(__this, L_1, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_2 = ___left1;
		__this->set__left_3(L_2);
		BaseIterator_t1327316739 * L_3 = ___right2;
		__this->set__right_4(L_3);
		return;
	}
}
// System.Void System.Xml.XPath.UnionIterator::.ctor(System.Xml.XPath.UnionIterator)
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern const uint32_t UnionIterator__ctor_m1987098937_MetadataUsageId;
extern "C"  void UnionIterator__ctor_m1987098937 (UnionIterator_t1391611539 * __this, UnionIterator_t1391611539 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator__ctor_m1987098937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnionIterator_t1391611539 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		UnionIterator_t1391611539 * L_1 = ___other0;
		NullCheck(L_1);
		BaseIterator_t1327316739 * L_2 = L_1->get__left_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set__left_3(((BaseIterator_t1327316739 *)CastclassClass(L_3, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		UnionIterator_t1391611539 * L_4 = ___other0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_5 = L_4->get__right_4();
		NullCheck(L_5);
		XPathNodeIterator_t1383168931 * L_6 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_5);
		__this->set__right_4(((BaseIterator_t1327316739 *)CastclassClass(L_6, BaseIterator_t1327316739_il2cpp_TypeInfo_var)));
		UnionIterator_t1391611539 * L_7 = ___other0;
		NullCheck(L_7);
		bool L_8 = L_7->get_keepLeft_5();
		__this->set_keepLeft_5(L_8);
		UnionIterator_t1391611539 * L_9 = ___other0;
		NullCheck(L_9);
		bool L_10 = L_9->get_keepRight_6();
		__this->set_keepRight_6(L_10);
		UnionIterator_t1391611539 * L_11 = ___other0;
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = L_11->get__current_7();
		if (!L_12)
		{
			goto IL_0067;
		}
	}
	{
		UnionIterator_t1391611539 * L_13 = ___other0;
		NullCheck(L_13);
		XPathNavigator_t1075073278 * L_14 = L_13->get__current_7();
		NullCheck(L_14);
		XPathNavigator_t1075073278 * L_15 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_14);
		__this->set__current_7(L_15);
	}

IL_0067:
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.UnionIterator::Clone()
extern Il2CppClass* UnionIterator_t1391611539_il2cpp_TypeInfo_var;
extern const uint32_t UnionIterator_Clone_m2246908543_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * UnionIterator_Clone_m2246908543 (UnionIterator_t1391611539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_Clone_m2246908543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnionIterator_t1391611539 * L_0 = (UnionIterator_t1391611539 *)il2cpp_codegen_object_new(UnionIterator_t1391611539_il2cpp_TypeInfo_var);
		UnionIterator__ctor_m1987098937(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.UnionIterator::MoveNextCore()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91958834;
extern const uint32_t UnionIterator_MoveNextCore_m2409356352_MetadataUsageId;
extern "C"  bool UnionIterator_MoveNextCore_m2409356352 (UnionIterator_t1391611539 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnionIterator_MoveNextCore_m2409356352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		bool L_0 = __this->get_keepLeft_5();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = __this->get__left_3();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_1);
		__this->set_keepLeft_5(L_2);
	}

IL_001c:
	{
		bool L_3 = __this->get_keepRight_6();
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		BaseIterator_t1327316739 * L_4 = __this->get__right_4();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_4);
		__this->set_keepRight_6(L_5);
	}

IL_0038:
	{
		bool L_6 = __this->get_keepLeft_5();
		if (L_6)
		{
			goto IL_0050;
		}
	}
	{
		bool L_7 = __this->get_keepRight_6();
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		return (bool)0;
	}

IL_0050:
	{
		bool L_8 = __this->get_keepRight_6();
		if (L_8)
		{
			goto IL_0070;
		}
	}
	{
		__this->set_keepLeft_5((bool)0);
		BaseIterator_t1327316739 * L_9 = __this->get__left_3();
		UnionIterator_SetCurrent_m1765329958(__this, L_9, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0070:
	{
		bool L_10 = __this->get_keepLeft_5();
		if (L_10)
		{
			goto IL_0090;
		}
	}
	{
		__this->set_keepRight_6((bool)0);
		BaseIterator_t1327316739 * L_11 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_11, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0090:
	{
		BaseIterator_t1327316739 * L_12 = __this->get__left_3();
		NullCheck(L_12);
		XPathNavigator_t1075073278 * L_13 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_12);
		BaseIterator_t1327316739 * L_14 = __this->get__right_4();
		NullCheck(L_14);
		XPathNavigator_t1075073278 * L_15 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_14);
		NullCheck(L_13);
		int32_t L_16 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_13, L_15);
		V_0 = L_16;
		int32_t L_17 = V_0;
		if (L_17 == 0)
		{
			goto IL_00e5;
		}
		if (L_17 == 1)
		{
			goto IL_00fa;
		}
		if (L_17 == 2)
		{
			goto IL_00c7;
		}
		if (L_17 == 3)
		{
			goto IL_00e5;
		}
	}
	{
		goto IL_010f;
	}

IL_00c7:
	{
		int32_t L_18 = 0;
		V_1 = (bool)L_18;
		__this->set_keepRight_6((bool)L_18);
		bool L_19 = V_1;
		__this->set_keepLeft_5(L_19);
		BaseIterator_t1327316739 * L_20 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_20, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00e5:
	{
		__this->set_keepLeft_5((bool)0);
		BaseIterator_t1327316739 * L_21 = __this->get__left_3();
		UnionIterator_SetCurrent_m1765329958(__this, L_21, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00fa:
	{
		__this->set_keepRight_6((bool)0);
		BaseIterator_t1327316739 * L_22 = __this->get__right_4();
		UnionIterator_SetCurrent_m1765329958(__this, L_22, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_010f:
	{
		InvalidOperationException_t1589641621 * L_23 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_23, _stringLiteral91958834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}
}
// System.Void System.Xml.XPath.UnionIterator::SetCurrent(System.Xml.XPath.XPathNodeIterator)
extern "C"  void UnionIterator_SetCurrent_m1765329958 (UnionIterator_t1391611539 * __this, XPathNodeIterator_t1383168931 * ___iter0, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = __this->get__current_7();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_2);
		__this->set__current_7(L_3);
		goto IL_0048;
	}

IL_0021:
	{
		XPathNavigator_t1075073278 * L_4 = __this->get__current_7();
		XPathNodeIterator_t1383168931 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_4, L_6);
		if (L_7)
		{
			goto IL_0048;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_8 = ___iter0;
		NullCheck(L_8);
		XPathNavigator_t1075073278 * L_9 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_8);
		NullCheck(L_9);
		XPathNavigator_t1075073278 * L_10 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_9);
		__this->set__current_7(L_10);
	}

IL_0048:
	{
		return;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.UnionIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * UnionIterator_get_Current_m1791653963 (UnionIterator_t1391611539 * __this, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, __this);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		XPathNavigator_t1075073278 * L_1 = __this->get__current_7();
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.WrapperIterator::.ctor(System.Xml.XPath.XPathNodeIterator,System.Xml.IXmlNamespaceResolver)
extern "C"  void WrapperIterator__ctor_m802118820 (WrapperIterator_t2849115671 * __this, XPathNodeIterator_t1383168931 * ___iter0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___nsm1;
		BaseIterator__ctor_m3826808382(__this, L_0, /*hidden argument*/NULL);
		XPathNodeIterator_t1383168931 * L_1 = ___iter0;
		__this->set_iter_3(L_1);
		return;
	}
}
// System.Void System.Xml.XPath.WrapperIterator::.ctor(System.Xml.XPath.WrapperIterator)
extern "C"  void WrapperIterator__ctor_m2127158577 (WrapperIterator_t2849115671 * __this, WrapperIterator_t2849115671 * ___other0, const MethodInfo* method)
{
	{
		WrapperIterator_t2849115671 * L_0 = ___other0;
		BaseIterator__ctor_m3181771313(__this, L_0, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_1 = ___other0;
		NullCheck(L_1);
		XPathNodeIterator_t1383168931 * L_2 = L_1->get_iter_3();
		NullCheck(L_2);
		XPathNodeIterator_t1383168931 * L_3 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, L_2);
		__this->set_iter_3(L_3);
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.WrapperIterator::Clone()
extern Il2CppClass* WrapperIterator_t2849115671_il2cpp_TypeInfo_var;
extern const uint32_t WrapperIterator_Clone_m3191539131_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * WrapperIterator_Clone_m3191539131 (WrapperIterator_t2849115671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WrapperIterator_Clone_m3191539131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WrapperIterator_t2849115671 * L_0 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m2127158577(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.WrapperIterator::MoveNextCore()
extern "C"  bool WrapperIterator_MoveNextCore_m1284174404 (WrapperIterator_t2849115671 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t1383168931 * L_0 = __this->get_iter_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.WrapperIterator::get_Current()
extern "C"  XPathNavigator_t1075073278 * WrapperIterator_get_Current_m4028081223 (WrapperIterator_t2849115671 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t1383168931 * L_0 = __this->get_iter_3();
		NullCheck(L_0);
		XPathNavigator_t1075073278 * L_1 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_0);
		return L_1;
	}
}
// System.Void System.Xml.XPath.XPathBooleanFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathBooleanFunction__ctor_m3002905248 (XPathBooleanFunction_t3865271111 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathBooleanFunction::get_ReturnType()
extern "C"  int32_t XPathBooleanFunction_get_ReturnType_m3799540540 (XPathBooleanFunction_t3865271111 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Object System.Xml.XPath.XPathBooleanFunction::get_StaticValue()
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathBooleanFunction_get_StaticValue_m4271685220_MetadataUsageId;
extern "C"  Il2CppObject * XPathBooleanFunction_get_StaticValue_m4271685220 (XPathBooleanFunction_t3865271111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathBooleanFunction_get_StaticValue_m4271685220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.Expression::get_StaticValueAsBoolean() */, __this);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathException__ctor_m866447368_MetadataUsageId;
extern "C"  void XPathException__ctor_m866447368 (XPathException_t1803876086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathException__ctor_m866447368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		SystemException__ctor_m3697314481(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XPathException__ctor_m2948568393 (XPathException_t1803876086 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		SystemException__ctor_m2083527090(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.String,System.Exception)
extern "C"  void XPathException__ctor_m4004445308 (XPathException_t1803876086 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		SystemException__ctor_m253088421(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathException::.ctor(System.String)
extern "C"  void XPathException__ctor_m2838559738 (XPathException_t1803876086 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SystemException__ctor_m253088421(__this, L_0, (Exception_t3991598821 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.XPath.XPathException::get_Message()
extern "C"  String_t* XPathException_get_Message_m3664156007 (XPathException_t1803876086 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = Exception_get_Message_m1013139483(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void XPathException_GetObjectData_m488604582 (XPathException_t1803876086 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		Exception_GetObjectData_m1945031808(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathExpression::.ctor()
extern "C"  void XPathExpression__ctor_m883028087 (XPathExpression_t3588072235 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathExpression::Compile(System.String)
extern "C"  XPathExpression_t3588072235 * XPathExpression_Compile_m1263926045 (Il2CppObject * __this /* static, unused */, String_t* ___xpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t3588072235 * L_1 = XPathExpression_Compile_m3855669648(NULL /*static, unused*/, L_0, (Il2CppObject *)NULL, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathExpression::Compile(System.String,System.Xml.IXmlNamespaceResolver,System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* XPathParser_t2461941858_il2cpp_TypeInfo_var;
extern Il2CppClass* CompiledExpression_t3956813165_il2cpp_TypeInfo_var;
extern const uint32_t XPathExpression_Compile_m3855669648_MetadataUsageId;
extern "C"  XPathExpression_t3588072235 * XPathExpression_Compile_m3855669648 (Il2CppObject * __this /* static, unused */, String_t* ___xpath0, Il2CppObject * ___nsmgr1, Il2CppObject * ___ctx2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathExpression_Compile_m3855669648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathParser_t2461941858 * V_0 = NULL;
	CompiledExpression_t3956813165 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___ctx2;
		XPathParser_t2461941858 * L_1 = (XPathParser_t2461941858 *)il2cpp_codegen_object_new(XPathParser_t2461941858_il2cpp_TypeInfo_var);
		XPathParser__ctor_m2662166670(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___xpath0;
		XPathParser_t2461941858 * L_3 = V_0;
		String_t* L_4 = ___xpath0;
		NullCheck(L_3);
		Expression_t2556460284 * L_5 = XPathParser_Compile_m3447021097(L_3, L_4, /*hidden argument*/NULL);
		CompiledExpression_t3956813165 * L_6 = (CompiledExpression_t3956813165 *)il2cpp_codegen_object_new(CompiledExpression_t3956813165_il2cpp_TypeInfo_var);
		CompiledExpression__ctor_m2683771966(L_6, L_2, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		CompiledExpression_t3956813165 * L_7 = V_1;
		Il2CppObject * L_8 = ___nsmgr1;
		NullCheck(L_7);
		VirtActionInvoker1< Il2CppObject * >::Invoke(5 /* System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.IXmlNamespaceResolver) */, L_7, L_8);
		CompiledExpression_t3956813165 * L_9 = V_1;
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunction__ctor_m2315013248 (XPathFunction_t3895226859 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	{
		Expression__ctor_m158740418(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathFunctionBoolean::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral533703603;
extern const uint32_t XPathFunctionBoolean__ctor_m2311468944_MetadataUsageId;
extern "C"  void XPathFunctionBoolean__ctor_m2311468944 (XPathFunctionBoolean_t149146711 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean__ctor_m2311468944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral533703603, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionBoolean::get_Peer()
extern "C"  bool XPathFunctionBoolean_get_Peer_m4075223552 (XPathFunctionBoolean_t149146711 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionBoolean::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionBoolean_Evaluate_m2675549483_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionBoolean_Evaluate_m2675549483 (XPathFunctionBoolean_t149146711 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean_Evaluate_m2675549483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		bool L_4 = XPathFunctions_ToBoolean_m2432449683(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0021:
	{
		Expression_t2556460284 * L_7 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_8 = ___iter0;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}
}
// System.String System.Xml.XPath.XPathFunctionBoolean::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2006063360;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionBoolean_ToString_m2171607622_MetadataUsageId;
extern "C"  String_t* XPathFunctionBoolean_ToString_m2171607622 (XPathFunctionBoolean_t149146711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionBoolean_ToString_m2171607622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2006063360);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2006063360);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionCeil::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1658293869;
extern const uint32_t XPathFunctionCeil__ctor_m3298422843_MetadataUsageId;
extern "C"  void XPathFunctionCeil__ctor_m3298422843 (XPathFunctionCeil_t843991056 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil__ctor_m3298422843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral1658293869, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_HasStaticValue()
extern "C"  bool XPathFunctionCeil_get_HasStaticValue_m2317770444 (XPathFunctionCeil_t843991056 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionCeil::get_StaticValueAsNumber()
extern "C"  double XPathFunctionCeil_get_StaticValueAsNumber_m3864092994 (XPathFunctionCeil_t843991056 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionCeil::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = ceil(L_2);
		G_B3_0 = L_3;
		goto IL_0029;
	}

IL_0020:
	{
		G_B3_0 = (0.0);
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCeil::get_Peer()
extern "C"  bool XPathFunctionCeil_get_Peer_m2107441093 (XPathFunctionCeil_t843991056 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionCeil::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionCeil_Evaluate_m2197801932_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionCeil_Evaluate_m2197801932 (XPathFunctionCeil_t843991056 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil_Evaluate_m2197801932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = ceil(L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionCeil::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral94541763;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionCeil_ToString_m521177249_MetadataUsageId;
extern "C"  String_t* XPathFunctionCeil_ToString_m521177249 (XPathFunctionCeil_t843991056 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCeil_ToString_m521177249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral94541763);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral94541763);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionConcat::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3483196761;
extern const uint32_t XPathFunctionConcat__ctor_m2570590668_MetadataUsageId;
extern "C"  void XPathFunctionConcat__ctor_m2570590668 (XPathFunctionConcat_t2260200095 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat__ctor_m2570590668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t3948406897 * V_0 = NULL;
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3483196761, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		ArrayList_t3948406897 * L_6 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_6, /*hidden argument*/NULL);
		ArrayList_t3948406897 * L_7 = L_6;
		V_0 = L_7;
		__this->set_rgs_0(L_7);
		ArrayList_t3948406897 * L_8 = V_0;
		NullCheck(L_5);
		FunctionArguments_ToArrayList_m227079145(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionConcat::get_ReturnType()
extern "C"  int32_t XPathFunctionConcat_get_ReturnType_m827815768 (XPathFunctionConcat_t2260200095 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionConcat::get_Peer()
extern Il2CppClass* Expression_t2556460284_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionConcat_get_Peer_m1370169940_MetadataUsageId;
extern "C"  bool XPathFunctionConcat_get_Peer_m1370169940 (XPathFunctionConcat_t2260200095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_get_Peer_m1370169940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_0007:
	{
		ArrayList_t3948406897 * L_0 = __this->get_rgs_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((Expression_t2556460284 *)CastclassClass(L_2, Expression_t2556460284_il2cpp_TypeInfo_var)));
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, ((Expression_t2556460284 *)CastclassClass(L_2, Expression_t2556460284_il2cpp_TypeInfo_var)));
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_5 = V_0;
		ArrayList_t3948406897 * L_6 = __this->get_rgs_0();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_6);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionConcat::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t2556460284_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionConcat_Evaluate_m1093166813_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionConcat_Evaluate_m1093166813 (XPathFunctionConcat_t2260200095 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_Evaluate_m1093166813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t3948406897 * L_1 = __this->get_rgs_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		V_2 = 0;
		goto IL_003b;
	}

IL_0019:
	{
		StringBuilder_t243639308 * L_3 = V_0;
		ArrayList_t3948406897 * L_4 = __this->get_rgs_0();
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(((Expression_t2556460284 *)CastclassClass(L_6, Expression_t2556460284_il2cpp_TypeInfo_var)));
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, ((Expression_t2556460284 *)CastclassClass(L_6, Expression_t2556460284_il2cpp_TypeInfo_var)), L_7);
		NullCheck(L_3);
		StringBuilder_Append_m3898090075(L_3, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t243639308 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = StringBuilder_ToString_m350379841(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionConcat::ToString()
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral951020436;
extern Il2CppCodeGenString* _stringLiteral119816;
extern const uint32_t XPathFunctionConcat_ToString_m2710519920_MetadataUsageId;
extern "C"  String_t* XPathFunctionConcat_ToString_m2710519920 (XPathFunctionConcat_t2260200095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionConcat_ToString_m2710519920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t243639308 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral951020436, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0051;
	}

IL_0019:
	{
		StringBuilder_t243639308 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ArrayList_t3948406897 * L_5 = __this->get_rgs_0();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_8);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		NullCheck(L_2);
		StringBuilder_AppendFormat_m259793396(L_2, L_3, _stringLiteral119816, L_4, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_9 = V_0;
		NullCheck(L_9);
		StringBuilder_Append_m2143093878(L_9, ((int32_t)44), /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		ArrayList_t3948406897 * L_12 = __this->get_rgs_0();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)L_13-(int32_t)1)))))
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t243639308 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_15 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_16 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ArrayList_t3948406897 * L_17 = __this->get_rgs_0();
		ArrayList_t3948406897 * L_18 = __this->get_rgs_0();
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_18);
		NullCheck(L_17);
		Il2CppObject * L_20 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_17, ((int32_t)((int32_t)L_19-(int32_t)1)));
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		ArrayElementTypeCheck (L_16, L_21);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_21);
		NullCheck(L_14);
		StringBuilder_AppendFormat_m259793396(L_14, L_15, _stringLiteral119816, L_16, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, ((int32_t)41), /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = StringBuilder_ToString_m350379841(L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void System.Xml.XPath.XPathFunctionContains::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3609780800;
extern const uint32_t XPathFunctionContains__ctor_m2892508385_MetadataUsageId;
extern "C"  void XPathFunctionContains__ctor_m2892508385 (XPathFunctionContains_t1738767914 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains__ctor_m2892508385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1803876086 * L_7 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3609780800, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2391178772 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t2556460284 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2391178772 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2391178772 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t2556460284 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionContains::get_ReturnType()
extern "C"  int32_t XPathFunctionContains_get_ReturnType_m3276932515 (XPathFunctionContains_t1738767914 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionContains::get_Peer()
extern "C"  bool XPathFunctionContains_get_Peer_m4254071775 (XPathFunctionContains_t1738767914 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionContains::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionContains_Evaluate_m1003236466_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionContains_Evaluate_m1003236466 (XPathFunctionContains_t1738767914 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains_Evaluate_m1003236466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		NullCheck(L_2);
		int32_t L_6 = String_IndexOf_m1476794331(L_2, L_5, /*hidden argument*/NULL);
		bool L_7 = ((bool)((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		Il2CppObject * L_8 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
// System.String System.Xml.XPath.XPathFunctionContains::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3884010985;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionContains_ToString_m571760443_MetadataUsageId;
extern "C"  String_t* XPathFunctionContains_ToString_m571760443 (XPathFunctionContains_t1738767914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionContains_ToString_m571760443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3884010985);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3884010985);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionCount::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral549981186;
extern const uint32_t XPathFunctionCount__ctor_m74396489_MetadataUsageId;
extern "C"  void XPathFunctionCount__ctor_m74396489 (XPathFunctionCount_t82957758 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount__ctor_m74396489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral549981186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionCount::get_ReturnType()
extern "C"  int32_t XPathFunctionCount_get_ReturnType_m4130170227 (XPathFunctionCount_t82957758 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCount::get_Peer()
extern "C"  bool XPathFunctionCount_get_Peer_m2430742375 (XPathFunctionCount_t82957758 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionCount::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionCount_Evaluate_m3315815524_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionCount_Evaluate_m3315815524 (XPathFunctionCount_t82957758 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount_Evaluate_m3315815524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_2);
		double L_4 = (((double)((double)L_3)));
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionCount::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool XPathFunctionCount_EvaluateBoolean_m3382145239 (XPathFunctionCount_t82957758 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		int32_t L_2 = VirtFuncInvoker1< int32_t, BaseIterator_t1327316739 * >::Invoke(5 /* System.Xml.XPath.XPathResultType System.Xml.XPath.Expression::GetReturnType(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_001f;
		}
	}
	{
		Expression_t2556460284 * L_3 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		return L_5;
	}

IL_001f:
	{
		Expression_t2556460284 * L_6 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(L_6);
		BaseIterator_t1327316739 * L_8 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		return L_9;
	}
}
// System.String System.Xml.XPath.XPathFunctionCount::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2940391673;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionCount_ToString_m4291378413_MetadataUsageId;
extern "C"  String_t* XPathFunctionCount_ToString_m4291378413 (XPathFunctionCount_t82957758 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionCount_ToString_m4291378413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2940391673, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionFalse::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1914279038;
extern const uint32_t XPathFunctionFalse__ctor_m1496753845_MetadataUsageId;
extern "C"  void XPathFunctionFalse__ctor_m1496753845 (XPathFunctionFalse_t85302738 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse__ctor_m1496753845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1803876086 * L_2 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral1914279038, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_HasStaticValue()
extern "C"  bool XPathFunctionFalse_get_HasStaticValue_m3224612098 (XPathFunctionFalse_t85302738 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionFalse_get_StaticValueAsBoolean_m891110156 (XPathFunctionFalse_t85302738 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFalse::get_Peer()
extern "C"  bool XPathFunctionFalse_get_Peer_m1943435643 (XPathFunctionFalse_t85302738 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionFalse::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionFalse_Evaluate_m2923999952_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionFalse_Evaluate_m2923999952 (XPathFunctionFalse_t85302738 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse_Evaluate_m2923999952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((bool)0);
		Il2CppObject * L_1 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionFalse::ToString()
extern Il2CppCodeGenString* _stringLiteral3211354468;
extern const uint32_t XPathFunctionFalse_ToString_m3804071681_MetadataUsageId;
extern "C"  String_t* XPathFunctionFalse_ToString_m3804071681 (XPathFunctionFalse_t85302738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFalse_ToString_m3804071681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3211354468;
	}
}
// System.Void System.Xml.XPath.XPathFunctionFloor::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3373125940;
extern const uint32_t XPathFunctionFloor__ctor_m224085036_MetadataUsageId;
extern "C"  void XPathFunctionFloor__ctor_m224085036 (XPathFunctionFloor_t85633211 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor__ctor_m224085036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3373125940, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_HasStaticValue()
extern "C"  bool XPathFunctionFloor_get_HasStaticValue_m899265323 (XPathFunctionFloor_t85633211 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionFloor::get_StaticValueAsNumber()
extern "C"  double XPathFunctionFloor_get_StaticValueAsNumber_m3220284121 (XPathFunctionFloor_t85633211 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionFloor::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = floor(L_2);
		G_B3_0 = L_3;
		goto IL_0029;
	}

IL_0020:
	{
		G_B3_0 = (0.0);
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionFloor::get_Peer()
extern "C"  bool XPathFunctionFloor_get_Peer_m3353177828 (XPathFunctionFloor_t85633211 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionFloor::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionFloor_Evaluate_m1729380871_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionFloor_Evaluate_m1729380871 (XPathFunctionFloor_t85633211 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor_Evaluate_m1729380871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = floor(L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionFloor::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3023330716;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionFloor_ToString_m918846570_MetadataUsageId;
extern "C"  String_t* XPathFunctionFloor_ToString_m918846570 (XPathFunctionFloor_t85633211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionFloor_ToString_m918846570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3023330716);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3023330716);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionId::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3188397582;
extern const uint32_t XPathFunctionId__ctor_m3649212261_MetadataUsageId;
extern "C"  void XPathFunctionId__ctor_m3649212261 (XPathFunctionId_t1025767334 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId__ctor_m3649212261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3188397582, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Void System.Xml.XPath.XPathFunctionId::.cctor()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t1025767334_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D25_11_FieldInfo_var;
extern const uint32_t XPathFunctionId__cctor_m1596924113_MetadataUsageId;
extern "C"  void XPathFunctionId__cctor_m1596924113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId__cctor_m1596924113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3324145743* L_0 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D25_11_FieldInfo_var), /*hidden argument*/NULL);
		((XPathFunctionId_t1025767334_StaticFields*)XPathFunctionId_t1025767334_il2cpp_TypeInfo_var->static_fields)->set_rgchWhitespace_1(L_0);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionId::get_ReturnType()
extern "C"  int32_t XPathFunctionId_get_ReturnType_m2668233119 (XPathFunctionId_t1025767334 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionId::get_Peer()
extern "C"  bool XPathFunctionId_get_Peer_m1177386971 (XPathFunctionId_t1025767334 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionId::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathFunctionId_t1025767334_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var;
extern Il2CppClass* ListIterator_t4080389072_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t XPathFunctionId_Evaluate_m2633997366_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionId_Evaluate_m2633997366 (XPathFunctionId_t1025767334 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId_Evaluate_m2633997366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	XPathNodeIterator_t1383168931 * V_2 = NULL;
	XPathNavigator_t1075073278 * V_3 = NULL;
	ArrayList_t3948406897 * V_4 = NULL;
	StringU5BU5D_t4054002952* V_5 = NULL;
	int32_t V_6 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, BaseIterator_t1327316739 * >::Invoke(13 /* System.Object System.Xml.XPath.Expression::Evaluate(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_1 = L_2;
		Il2CppObject * L_3 = V_1;
		V_2 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_3, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_4 = V_2;
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_5;
		goto IL_003c;
	}

IL_0025:
	{
		String_t* L_6 = V_0;
		XPathNodeIterator_t1383168931 * L_7 = V_2;
		NullCheck(L_7);
		XPathNavigator_t1075073278 * L_8 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_7);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1825781833(NULL /*static, unused*/, L_6, L_9, _stringLiteral32, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_003c:
	{
		XPathNodeIterator_t1383168931 * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0053;
	}

IL_004c:
	{
		Il2CppObject * L_13 = V_1;
		String_t* L_14 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0053:
	{
		BaseIterator_t1327316739 * L_15 = ___iter0;
		NullCheck(L_15);
		XPathNavigator_t1075073278 * L_16 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_15);
		NullCheck(L_16);
		XPathNavigator_t1075073278 * L_17 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_16);
		V_3 = L_17;
		ArrayList_t3948406897 * L_18 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_18, /*hidden argument*/NULL);
		V_4 = L_18;
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathFunctionId_t1025767334_il2cpp_TypeInfo_var);
		CharU5BU5D_t3324145743* L_20 = ((XPathFunctionId_t1025767334_StaticFields*)XPathFunctionId_t1025767334_il2cpp_TypeInfo_var->static_fields)->get_rgchWhitespace_1();
		NullCheck(L_19);
		StringU5BU5D_t4054002952* L_21 = String_Split_m290179486(L_19, L_20, /*hidden argument*/NULL);
		V_5 = L_21;
		V_6 = 0;
		goto IL_009f;
	}

IL_007b:
	{
		XPathNavigator_t1075073278 * L_22 = V_3;
		StringU5BU5D_t4054002952* L_23 = V_5;
		int32_t L_24 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		String_t* L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_22);
		bool L_27 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToId(System.String) */, L_22, L_26);
		if (!L_27)
		{
			goto IL_0099;
		}
	}
	{
		ArrayList_t3948406897 * L_28 = V_4;
		XPathNavigator_t1075073278 * L_29 = V_3;
		NullCheck(L_29);
		XPathNavigator_t1075073278 * L_30 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_29);
		NullCheck(L_28);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_28, L_30);
	}

IL_0099:
	{
		int32_t L_31 = V_6;
		V_6 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_32 = V_6;
		StringU5BU5D_t4054002952* L_33 = V_5;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_007b;
		}
	}
	{
		ArrayList_t3948406897 * L_34 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var);
		XPathNavigatorComparer_t2221114443 * L_35 = ((XPathNavigatorComparer_t2221114443_StaticFields*)XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var->static_fields)->get_Instance_0();
		NullCheck(L_34);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_34, L_35);
		BaseIterator_t1327316739 * L_36 = ___iter0;
		ArrayList_t3948406897 * L_37 = V_4;
		ListIterator_t4080389072 * L_38 = (ListIterator_t4080389072 *)il2cpp_codegen_object_new(ListIterator_t4080389072_il2cpp_TypeInfo_var);
		ListIterator__ctor_m222741241(L_38, L_36, L_37, /*hidden argument*/NULL);
		return L_38;
	}
}
// System.String System.Xml.XPath.XPathFunctionId::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104045;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionId_ToString_m2274108279_MetadataUsageId;
extern "C"  String_t* XPathFunctionId_ToString_m2274108279 (XPathFunctionId_t1025767334 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionId_ToString_m2274108279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral104045, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLang::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2950777750;
extern const uint32_t XPathFunctionLang__ctor_m915197874_MetadataUsageId;
extern "C"  void XPathFunctionLang__ctor_m915197874 (XPathFunctionLang_t844255481 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang__ctor_m915197874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral2950777750, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLang::get_ReturnType()
extern "C"  int32_t XPathFunctionLang_get_ReturnType_m4120654770 (XPathFunctionLang_t844255481 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::get_Peer()
extern "C"  bool XPathFunctionLang_get_Peer_m3306550062 (XPathFunctionLang_t844255481 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLang::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_Evaluate_m1010965763_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLang_Evaluate_m1010965763 (XPathFunctionLang_t844255481 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_Evaluate_m1010965763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern Il2CppClass* CultureInfo_t1065375142_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_EvaluateBoolean_m1037770014_MetadataUsageId;
extern "C"  bool XPathFunctionLang_EvaluateBoolean_m1037770014 (XPathFunctionLang_t844255481 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_EvaluateBoolean_m1037770014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1065375142_il2cpp_TypeInfo_var);
		CultureInfo_t1065375142 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_4 = String_ToLower_m2140020155(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Xml.XPath.XPathNavigator::get_XmlLang() */, L_6);
		CultureInfo_t1065375142 * L_8 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_ToLower_m2140020155(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_0;
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_13 = V_0;
		String_t* L_14 = V_1;
		CharU5BU5D_t3324145743* L_15 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)45));
		NullCheck(L_14);
		StringU5BU5D_t4054002952* L_16 = String_Split_m290179486(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		String_t* L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_13, L_18, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_19));
		goto IL_0055;
	}

IL_0054:
	{
		G_B3_0 = 1;
	}

IL_0055:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctionLang::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102738938;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionLang_ToString_m1720286218_MetadataUsageId;
extern "C"  String_t* XPathFunctionLang_ToString_m1720286218 (XPathFunctionLang_t844255481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_ToString_m1720286218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral102738938);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral102738938);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLast::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral570445291;
extern const uint32_t XPathFunctionLast__ctor_m565310986_MetadataUsageId;
extern "C"  void XPathFunctionLast__ctor_m565310986 (XPathFunctionLast_t844255649 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast__ctor_m565310986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1803876086 * L_2 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral570445291, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLast::get_ReturnType()
extern "C"  int32_t XPathFunctionLast_get_ReturnType_m3983301210 (XPathFunctionLast_t844255649 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLast::get_Peer()
extern "C"  bool XPathFunctionLast_get_Peer_m1259485142 (XPathFunctionLast_t844255649 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLast::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLast_Evaluate_m380434779_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLast_Evaluate_m380434779 (XPathFunctionLast_t844255649 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_Evaluate_m380434779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_0);
		double L_2 = (((double)((double)L_1)));
		Il2CppObject * L_3 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionLast::ToString()
extern Il2CppCodeGenString* _stringLiteral3185068567;
extern const uint32_t XPathFunctionLast_ToString_m3968188594_MetadataUsageId;
extern "C"  String_t* XPathFunctionLast_ToString_m3968188594 (XPathFunctionLast_t844255649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_ToString_m3968188594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3185068567;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLocalName::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4160111950;
extern const uint32_t XPathFunctionLocalName__ctor_m2097528674_MetadataUsageId;
extern "C"  void XPathFunctionLocalName__ctor_m2097528674 (XPathFunctionLocalName_t1256488005 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName__ctor_m2097528674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral4160111950, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLocalName::get_ReturnType()
extern "C"  int32_t XPathFunctionLocalName_get_ReturnType_m4113090874 (XPathFunctionLocalName_t1256488005 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLocalName::get_Peer()
extern "C"  bool XPathFunctionLocalName_get_Peer_m565692014 (XPathFunctionLocalName_t1256488005 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLocalName::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLocalName_Evaluate_m1852518461_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionLocalName_Evaluate_m1852518461 (XPathFunctionLocalName_t1256488005 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_Evaluate_m1852518461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t2556460284 * L_4 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_6 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t1327316739 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t1327316739 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t1327316739 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionLocalName::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral323178011;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionLocalName_ToString_m1873365364_MetadataUsageId;
extern "C"  String_t* XPathFunctionLocalName_ToString_m1873365364 (XPathFunctionLocalName_t1256488005 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_ToString_m1873365364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral323178011, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionName::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral543394096;
extern const uint32_t XPathFunctionName__ctor_m17270517_MetadataUsageId;
extern "C"  void XPathFunctionName__ctor_m17270517 (XPathFunctionName_t844315030 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName__ctor_m17270517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral543394096, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionName::get_ReturnType()
extern "C"  int32_t XPathFunctionName_get_ReturnType_m2142310159 (XPathFunctionName_t844315030 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionName::get_Peer()
extern "C"  bool XPathFunctionName_get_Peer_m3377846347 (XPathFunctionName_t844315030 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionName::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionName_Evaluate_m2105002246_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionName_Evaluate_m2105002246 (XPathFunctionName_t844315030 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_Evaluate_m2105002246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t2556460284 * L_4 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_6 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t1327316739 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t1327316739 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t1327316739 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionName::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104584957;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionName_ToString_m1791582503_MetadataUsageId;
extern "C"  String_t* XPathFunctionName_ToString_m1791582503 (XPathFunctionName_t844315030 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_ToString_m1791582503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		G_B1_0 = _stringLiteral104584957;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral104584957;
			goto IL_0020;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0025;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1825781833(NULL /*static, unused*/, G_B3_1, G_B3_0, _stringLiteral41, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNamespaceUri::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2162484513;
extern const uint32_t XPathFunctionNamespaceUri__ctor_m503725999_MetadataUsageId;
extern "C"  void XPathFunctionNamespaceUri__ctor_m503725999 (XPathFunctionNamespaceUri_t4291495708 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri__ctor_m503725999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2162484513, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNamespaceUri::get_Peer()
extern "C"  bool XPathFunctionNamespaceUri_get_Peer_m3433121489 (XPathFunctionNamespaceUri_t4291495708 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNamespaceUri::get_ReturnType()
extern "C"  int32_t XPathFunctionNamespaceUri_get_ReturnType_m3395396885 (XPathFunctionNamespaceUri_t4291495708 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Object System.Xml.XPath.XPathFunctionNamespaceUri::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNamespaceUri_Evaluate_m1130007872_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNamespaceUri_Evaluate_m1130007872 (XPathFunctionNamespaceUri_t4291495708 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_Evaluate_m1130007872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BaseIterator_t1327316739 * V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t2556460284 * L_4 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t1327316739 * L_6 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t1327316739 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t1327316739 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t1327316739 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t1075073278 * L_12 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionNamespaceUri::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2367935854;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNamespaceUri_ToString_m1933919405_MetadataUsageId;
extern "C"  String_t* XPathFunctionNamespaceUri_ToString_m1933919405 (XPathFunctionNamespaceUri_t4291495708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_ToString_m1933919405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2367935854, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNormalizeSpace::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3904114773;
extern const uint32_t XPathFunctionNormalizeSpace__ctor_m3177496007_MetadataUsageId;
extern "C"  void XPathFunctionNormalizeSpace__ctor_m3177496007 (XPathFunctionNormalizeSpace_t1100613508 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace__ctor_m3177496007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral3904114773, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNormalizeSpace::get_ReturnType()
extern "C"  int32_t XPathFunctionNormalizeSpace_get_ReturnType_m873947901 (XPathFunctionNormalizeSpace_t1100613508 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNormalizeSpace::get_Peer()
extern "C"  bool XPathFunctionNormalizeSpace_get_Peer_m2843429305 (XPathFunctionNormalizeSpace_t1100613508 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNormalizeSpace::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNormalizeSpace_Evaluate_m3453465240_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNormalizeSpace_Evaluate_m3453465240 (XPathFunctionNormalizeSpace_t1100613508 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_Evaluate_m3453465240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringBuilder_t243639308 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppChar V_4 = 0x0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		StringBuilder_t243639308 * L_7 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = (bool)0;
		V_3 = 0;
		goto IL_0096;
	}

IL_0038:
	{
		String_t* L_8 = V_0;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m3015341861(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Il2CppChar L_11 = V_4;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)32))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)9))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_13 = V_4;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)13))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_14 = V_4;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_006c;
		}
	}

IL_0065:
	{
		V_2 = (bool)1;
		goto IL_0092;
	}

IL_006c:
	{
		bool L_15 = V_2;
		if (!L_15)
		{
			goto IL_0089;
		}
	}
	{
		V_2 = (bool)0;
		StringBuilder_t243639308 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m2443133099(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		StringBuilder_t243639308 * L_18 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m2143093878(L_18, ((int32_t)32), /*hidden argument*/NULL);
	}

IL_0089:
	{
		StringBuilder_t243639308 * L_19 = V_1;
		Il2CppChar L_20 = V_4;
		NullCheck(L_19);
		StringBuilder_Append_m2143093878(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_22 = V_3;
		String_t* L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m2979997331(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0038;
		}
	}
	{
		StringBuilder_t243639308 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = StringBuilder_ToString_m350379841(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.String System.Xml.XPath.XPathFunctionNormalizeSpace::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2915672738;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNormalizeSpace_ToString_m1950784725_MetadataUsageId;
extern "C"  String_t* XPathFunctionNormalizeSpace_ToString_m1950784725 (XPathFunctionNormalizeSpace_t1100613508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_ToString_m1950784725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2915672738);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2915672738);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_002b;
		}
	}
	{
		Expression_t2556460284 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0030;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral41);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m21867311(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNot::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1286369211;
extern const uint32_t XPathFunctionNot__ctor_m1396109829_MetadataUsageId;
extern "C"  void XPathFunctionNot__ctor_m1396109829 (XPathFunctionNot_t1422750722 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot__ctor_m1396109829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral1286369211, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNot::get_Peer()
extern "C"  bool XPathFunctionNot_get_Peer_m2256221739 (XPathFunctionNot_t1422750722 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNot::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNot_Evaluate_m2620128096_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNot_Evaluate_m2620128096 (XPathFunctionNot_t1422750722 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_Evaluate_m2620128096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t1327316739 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		bool L_3 = ((bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0));
		Il2CppObject * L_4 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.String System.Xml.XPath.XPathFunctionNot::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3387317;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNot_ToString_m1390051569_MetadataUsageId;
extern "C"  String_t* XPathFunctionNot_ToString_m1390051569 (XPathFunctionNot_t1422750722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_ToString_m1390051569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3387317);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3387317);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNumber::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2493164338;
extern const uint32_t XPathFunctionNumber__ctor_m2761557623_MetadataUsageId;
extern "C"  void XPathFunctionNumber__ctor_m2761557623 (XPathFunctionNumber_t2580631252 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber__ctor_m2761557623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2493164338, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNumber::Optimize()
extern Il2CppClass* ExprNumber_t1899651074_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNumber_Optimize_m2286708775_MetadataUsageId;
extern "C"  Expression_t2556460284 * XPathFunctionNumber_Optimize_m2286708775 (XPathFunctionNumber_t2580631252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Optimize_m2286708775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathFunctionNumber_t2580631252 * G_B5_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		Expression_t2556460284 * L_2 = VirtFuncInvoker0< Expression_t2556460284 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_1);
		__this->set_arg0_0(L_2);
		Expression_t2556460284 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_3);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		G_B5_0 = __this;
		goto IL_003f;
	}

IL_0034:
	{
		double L_5 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber() */, __this);
		ExprNumber_t1899651074 * L_6 = (ExprNumber_t1899651074 *)il2cpp_codegen_object_new(ExprNumber_t1899651074_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_6, L_5, /*hidden argument*/NULL);
		G_B5_0 = ((XPathFunctionNumber_t2580631252 *)(L_6));
	}

IL_003f:
	{
		return G_B5_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_HasStaticValue()
extern "C"  bool XPathFunctionNumber_get_HasStaticValue_m917305616 (XPathFunctionNumber_t2580631252 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber()
extern "C"  double XPathFunctionNumber_get_StaticValueAsNumber_m3694847038 (XPathFunctionNumber_t2580631252 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		G_B3_0 = L_2;
		goto IL_0024;
	}

IL_001b:
	{
		G_B3_0 = (0.0);
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_Peer()
extern "C"  bool XPathFunctionNumber_get_Peer_m384753417 (XPathFunctionNumber_t2580631252 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNumber::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNumber_Evaluate_m3471016520_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionNumber_Evaluate_m3471016520 (XPathFunctionNumber_t2580631252 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Evaluate_m3471016520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		double L_4 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0021:
	{
		Expression_t2556460284 * L_7 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_8 = ___iter0;
		NullCheck(L_7);
		double L_9 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		double L_10 = L_9;
		Il2CppObject * L_11 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}
}
// System.String System.Xml.XPath.XPathFunctionNumber::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2294451711;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionNumber_ToString_m1725103397_MetadataUsageId;
extern "C"  String_t* XPathFunctionNumber_ToString_m1725103397 (XPathFunctionNumber_t2580631252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_ToString_m1725103397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2294451711);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2294451711);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionPosition::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4139044408;
extern const uint32_t XPathFunctionPosition__ctor_m545736599_MetadataUsageId;
extern "C"  void XPathFunctionPosition__ctor_m545736599 (XPathFunctionPosition_t3054018868 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition__ctor_m545736599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1803876086 * L_2 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral4139044408, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionPosition::get_ReturnType()
extern "C"  int32_t XPathFunctionPosition_get_ReturnType_m1987852845 (XPathFunctionPosition_t3054018868 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionPosition::get_Peer()
extern "C"  bool XPathFunctionPosition_get_Peer_m393666537 (XPathFunctionPosition_t3054018868 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionPosition::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionPosition_Evaluate_m3275652264_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionPosition_Evaluate_m3275652264 (XPathFunctionPosition_t3054018868 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_Evaluate_m3275652264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_0);
		double L_2 = (((double)((double)L_1)));
		Il2CppObject * L_3 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionPosition::ToString()
extern Il2CppCodeGenString* _stringLiteral1381038058;
extern const uint32_t XPathFunctionPosition_ToString_m1006322501_MetadataUsageId;
extern "C"  String_t* XPathFunctionPosition_ToString_m1006322501 (XPathFunctionPosition_t3054018868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_ToString_m1006322501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral1381038058;
	}
}
// System.Void System.Xml.XPath.XPathFunctionRound::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3542717238;
extern const uint32_t XPathFunctionRound__ctor_m948443498_MetadataUsageId;
extern "C"  void XPathFunctionRound__ctor_m948443498 (XPathFunctionRound_t96810557 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound__ctor_m948443498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3542717238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue()
extern "C"  bool XPathFunctionRound_get_HasStaticValue_m4183577389 (XPathFunctionRound_t96810557 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::get_StaticValueAsNumber()
extern "C"  double XPathFunctionRound_get_StaticValueAsNumber_m272413719 (XPathFunctionRound_t96810557 * __this, const MethodInfo* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = XPathFunctionRound_Round_m561292306(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002a;
	}

IL_0021:
	{
		G_B3_0 = (0.0);
	}

IL_002a:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_Peer()
extern "C"  bool XPathFunctionRound_get_Peer_m1655032166 (XPathFunctionRound_t96810557 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionRound::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionRound_Evaluate_m760303301_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionRound_Evaluate_m760303301 (XPathFunctionRound_t96810557 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_Evaluate_m760303301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = XPathFunctionRound_Round_m561292306(__this, L_2, /*hidden argument*/NULL);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::Round(System.Double)
extern "C"  double XPathFunctionRound_Round_m561292306 (XPathFunctionRound_t96810557 * __this, double ___arg0, const MethodInfo* method)
{
	{
		double L_0 = ___arg0;
		if ((((double)L_0) < ((double)(-0.5))))
		{
			goto IL_001e;
		}
	}
	{
		double L_1 = ___arg0;
		if ((!(((double)L_1) > ((double)(0.0)))))
		{
			goto IL_002f;
		}
	}

IL_001e:
	{
		double L_2 = ___arg0;
		double L_3 = floor(((double)((double)L_2+(double)(0.5))));
		return L_3;
	}

IL_002f:
	{
		double L_4 = ___arg0;
		double L_5 = bankers_round(L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionRound::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3369828442;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionRound_ToString_m3515668204_MetadataUsageId;
extern "C"  String_t* XPathFunctionRound_ToString_m3515668204 (XPathFunctionRound_t96810557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_ToString_m3515668204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3369828442);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3369828442);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToBoolean_m2654560037_MetadataUsageId;
extern "C"  bool XPathFunctions_ToBoolean_m2654560037 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToBoolean_m2654560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	XPathNodeIterator_t1383168931 * V_1 = NULL;
	int32_t G_B8_0 = 0;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		return ((*(bool*)((bool*)UnBox (L_3, Boolean_t476798718_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		Il2CppObject * L_4 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_004c;
		}
	}
	{
		Il2CppObject * L_5 = ___arg0;
		V_0 = ((*(double*)((double*)UnBox (L_5, Double_t3868226565_il2cpp_TypeInfo_var))));
		double L_6 = V_0;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		double L_7 = V_0;
		bool L_8 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
	}

IL_004b:
	{
		return (bool)G_B8_0;
	}

IL_004c:
	{
		Il2CppObject * L_9 = ___arg0;
		if (!((String_t*)IsInstSealed(L_9, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0069;
		}
	}
	{
		Il2CppObject * L_10 = ___arg0;
		NullCheck(((String_t*)CastclassSealed(L_10, String_t_il2cpp_TypeInfo_var)));
		int32_t L_11 = String_get_Length_m2979997331(((String_t*)CastclassSealed(L_10, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_11) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0069:
	{
		Il2CppObject * L_12 = ___arg0;
		if (!((XPathNodeIterator_t1383168931 *)IsInstClass(L_12, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject * L_13 = ___arg0;
		V_1 = ((XPathNodeIterator_t1383168931 *)CastclassClass(L_13, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_14);
		return L_15;
	}

IL_0082:
	{
		Il2CppObject * L_16 = ___arg0;
		if (!((XPathNavigator_t1075073278 *)IsInstClass(L_16, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)))
		{
			goto IL_00a0;
		}
	}
	{
		Il2CppObject * L_17 = ___arg0;
		NullCheck(((XPathNavigator_t1075073278 *)CastclassClass(L_17, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		XPathNodeIterator_t1383168931 * L_18 = VirtFuncInvoker1< XPathNodeIterator_t1383168931 *, int32_t >::Invoke(33 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType) */, ((XPathNavigator_t1075073278 *)CastclassClass(L_17, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)), ((int32_t)9));
		bool L_19 = XPathFunctions_ToBoolean_m2654560037(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_00a0:
	{
		ArgumentException_t928607144 * L_20 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.String)
extern "C"  bool XPathFunctions_ToBoolean_m2432449683 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t XPathFunctions_ToString_m2310576707_MetadataUsageId;
extern "C"  String_t* XPathFunctions_ToString_m2310576707 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m2310576707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t1383168931 * V_0 = NULL;
	String_t* G_B8_0 = NULL;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (!((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		return ((String_t*)CastclassSealed(L_3, String_t_il2cpp_TypeInfo_var));
	}

IL_001e:
	{
		Il2CppObject * L_4 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		Il2CppObject * L_5 = ___arg0;
		if (!((*(bool*)((bool*)UnBox (L_5, Boolean_t476798718_il2cpp_TypeInfo_var)))))
		{
			goto IL_003e;
		}
	}
	{
		G_B8_0 = _stringLiteral3569038;
		goto IL_0043;
	}

IL_003e:
	{
		G_B8_0 = _stringLiteral97196323;
	}

IL_0043:
	{
		return G_B8_0;
	}

IL_0044:
	{
		Il2CppObject * L_6 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_6, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		Il2CppObject * L_7 = ___arg0;
		String_t* L_8 = XPathFunctions_ToString_m1520227313(NULL /*static, unused*/, ((*(double*)((double*)UnBox (L_7, Double_t3868226565_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_8;
	}

IL_005b:
	{
		Il2CppObject * L_9 = ___arg0;
		if (!((XPathNodeIterator_t1383168931 *)IsInstClass(L_9, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		Il2CppObject * L_10 = ___arg0;
		V_0 = ((XPathNodeIterator_t1383168931 *)CastclassClass(L_10, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_13;
	}

IL_007e:
	{
		XPathNodeIterator_t1383168931 * L_14 = V_0;
		NullCheck(L_14);
		XPathNavigator_t1075073278 * L_15 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_14);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_15);
		return L_16;
	}

IL_008a:
	{
		Il2CppObject * L_17 = ___arg0;
		if (!((XPathNavigator_t1075073278 *)IsInstClass(L_17, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)))
		{
			goto IL_00a1;
		}
	}
	{
		Il2CppObject * L_18 = ___arg0;
		NullCheck(((XPathNavigator_t1075073278 *)CastclassClass(L_18, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, ((XPathNavigator_t1075073278 *)CastclassClass(L_18, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)));
		return L_19;
	}

IL_00a1:
	{
		ArgumentException_t928607144 * L_20 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Double)
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral506745205;
extern Il2CppCodeGenString* _stringLiteral237817416;
extern Il2CppCodeGenString* _stringLiteral82;
extern const uint32_t XPathFunctions_ToString_m1520227313_MetadataUsageId;
extern "C"  String_t* XPathFunctions_ToString_m1520227313 (Il2CppObject * __this /* static, unused */, double ___d0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m1520227313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___d0;
		if ((!(((double)L_0) == ((double)(-std::numeric_limits<double>::infinity())))))
		{
			goto IL_0015;
		}
	}
	{
		return _stringLiteral506745205;
	}

IL_0015:
	{
		double L_1 = ___d0;
		if ((!(((double)L_1) == ((double)(std::numeric_limits<double>::infinity())))))
		{
			goto IL_002a;
		}
	}
	{
		return _stringLiteral237817416;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_2 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = Double_ToString_m2573497243((&___d0), _stringLiteral82, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.Object)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseIterator_t1327316739_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m1220011083_MetadataUsageId;
extern "C"  double XPathFunctions_ToNumber_m1220011083 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m1220011083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		Il2CppObject * L_2 = ___arg0;
		if (((BaseIterator_t1327316739 *)IsInstClass(L_2, BaseIterator_t1327316739_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		Il2CppObject * L_3 = ___arg0;
		if (!((XPathNavigator_t1075073278 *)IsInstClass(L_3, XPathNavigator_t1075073278_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}

IL_0022:
	{
		Il2CppObject * L_4 = ___arg0;
		String_t* L_5 = XPathFunctions_ToString_m2310576707(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		___arg0 = L_5;
	}

IL_002a:
	{
		Il2CppObject * L_6 = ___arg0;
		if (!((String_t*)IsInstSealed(L_6, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		Il2CppObject * L_7 = ___arg0;
		V_0 = ((String_t*)IsInstSealed(L_7, String_t_il2cpp_TypeInfo_var));
		String_t* L_8 = V_0;
		double L_9 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0043:
	{
		Il2CppObject * L_10 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0055;
		}
	}
	{
		Il2CppObject * L_11 = ___arg0;
		return ((*(double*)((double*)UnBox (L_11, Double_t3868226565_il2cpp_TypeInfo_var))));
	}

IL_0055:
	{
		Il2CppObject * L_12 = ___arg0;
		if (!((Il2CppObject *)IsInstSealed(L_12, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject * L_13 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_14 = Convert_ToDouble_m1966172364(NULL /*static, unused*/, ((*(bool*)((bool*)UnBox (L_13, Boolean_t476798718_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_14;
	}

IL_006c:
	{
		ArgumentException_t928607144 * L_15 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.String)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t856576415_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* OverflowException_t4020384771_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t3455918062_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m997900729_MetadataUsageId;
extern "C"  double XPathFunctions_ToNumber_m997900729 (Il2CppObject * __this /* static, unused */, String_t* ___arg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m997900729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2162372070(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		String_t* L_2 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t856576415_il2cpp_TypeInfo_var);
		CharU5BU5D_t3324145743* L_3 = ((XmlChar_t856576415_StaticFields*)XmlChar_t856576415_il2cpp_TypeInfo_var->static_fields)->get_WhitespaceChars_0();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m1469603388(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002d;
		}
	}
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_7 = V_0;
			NullCheck(L_7);
			Il2CppChar L_8 = String_get_Chars_m3015341861(L_7, 0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)46)))))
			{
				goto IL_0049;
			}
		}

IL_003b:
		{
			Il2CppChar L_9 = ((Il2CppChar)((int32_t)46));
			Il2CppObject * L_10 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_9);
			String_t* L_11 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m389863537(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0049:
		{
			String_t* L_13 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
			NumberFormatInfo_t1637637232 * L_14 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
			double L_15 = Double_Parse_m3249074997(NULL /*static, unused*/, L_13, ((int32_t)39), L_14, /*hidden argument*/NULL);
			V_1 = L_15;
			goto IL_008b;
		}

IL_005c:
		{
			; // IL_005c: leave IL_008b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t4020384771_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0061;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t3455918062_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0076;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.OverflowException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0071:
		{
			; // IL_0071: leave IL_008b
		}
	} // end catch (depth: 1)

CATCH_0076:
	{ // begin catch(System.FormatException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0086:
		{
			; // IL_0086: leave IL_008b
		}
	} // end catch (depth: 1)

IL_008b:
	{
		double L_16 = V_1;
		return L_16;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStartsWith::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3140805757;
extern const uint32_t XPathFunctionStartsWith__ctor_m3911729737_MetadataUsageId;
extern "C"  void XPathFunctionStartsWith__ctor_m3911729737 (XPathFunctionStartsWith_t1437129026 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith__ctor_m3911729737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1803876086 * L_7 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3140805757, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2391178772 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t2556460284 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2391178772 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2391178772 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t2556460284 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStartsWith::get_ReturnType()
extern "C"  int32_t XPathFunctionStartsWith_get_ReturnType_m116298299 (XPathFunctionStartsWith_t1437129026 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStartsWith::get_Peer()
extern "C"  bool XPathFunctionStartsWith_get_Peer_m267456887 (XPathFunctionStartsWith_t1437129026 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStartsWith::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionStartsWith_Evaluate_m846929178_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionStartsWith_Evaluate_m846929178 (XPathFunctionStartsWith_t1437129026 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_Evaluate_m846929178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		NullCheck(L_2);
		bool L_6 = String_StartsWith_m1500793453(L_2, L_5, /*hidden argument*/NULL);
		bool L_7 = L_6;
		Il2CppObject * L_8 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
// System.String System.Xml.XPath.XPathFunctionStartsWith::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2881029030;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionStartsWith_ToString_m1645832723_MetadataUsageId;
extern "C"  String_t* XPathFunctionStartsWith_ToString_m1645832723 (XPathFunctionStartsWith_t1437129026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_ToString_m1645832723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2881029030);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2881029030);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionString::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4266816362;
extern const uint32_t XPathFunctionString__ctor_m3584382127_MetadataUsageId;
extern "C"  void XPathFunctionString__ctor_m3584382127 (XPathFunctionString_t2723009436 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString__ctor_m3584382127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral4266816362, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionString::get_ReturnType()
extern "C"  int32_t XPathFunctionString_get_ReturnType_m463862549 (XPathFunctionString_t2723009436 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionString::get_Peer()
extern "C"  bool XPathFunctionString_get_Peer_m723679185 (XPathFunctionString_t2723009436 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionString::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionString_Evaluate_m2019360384 (XPathFunctionString_t2723009436 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t2556460284 * L_4 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		return L_6;
	}
}
// System.String System.Xml.XPath.XPathFunctionString::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2413208119;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionString_ToString_m2064029165_MetadataUsageId;
extern "C"  String_t* XPathFunctionString_ToString_m2064029165 (XPathFunctionString_t2723009436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString_ToString_m2064029165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2413208119, L_1, _stringLiteral41, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStringLength::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2119956473;
extern const uint32_t XPathFunctionStringLength__ctor_m2699284393_MetadataUsageId;
extern "C"  void XPathFunctionStringLength__ctor_m2699284393 (XPathFunctionStringLength_t1116358498 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength__ctor_m2699284393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t2556460284 * L_3 = FunctionArguments_get_Arg_m969707333(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1803876086 * L_6 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_6, _stringLiteral2119956473, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStringLength::get_ReturnType()
extern "C"  int32_t XPathFunctionStringLength_get_ReturnType_m1348968411 (XPathFunctionStringLength_t1116358498 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStringLength::get_Peer()
extern "C"  bool XPathFunctionStringLength_get_Peer_m3797175831 (XPathFunctionStringLength_t1116358498 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStringLength::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionStringLength_Evaluate_m913298362_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionStringLength_Evaluate_m913298362 (XPathFunctionStringLength_t1116358498 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_Evaluate_m913298362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		double L_9 = (((double)((double)L_8)));
		Il2CppObject * L_10 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.String System.Xml.XPath.XPathFunctionStringLength::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3998139462;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionStringLength_ToString_m2297973747_MetadataUsageId;
extern "C"  String_t* XPathFunctionStringLength_ToString_m2297973747 (XPathFunctionStringLength_t1116358498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_ToString_m2297973747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3998139462);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3998139462);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstring::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3692286390;
extern const uint32_t XPathFunctionSubstring__ctor_m2565884615_MetadataUsageId;
extern "C"  void XPathFunctionSubstring__ctor_m2565884615 (XPathFunctionSubstring_t2992425472 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring__ctor_m2565884615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		FunctionArguments_t2391178772 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t2391178772 * L_8 = FunctionArguments_get_Tail_m52932169(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t2391178772 * L_9 = FunctionArguments_get_Tail_m52932169(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t2391178772 * L_10 = FunctionArguments_get_Tail_m52932169(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t1803876086 * L_11 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_11, _stringLiteral3692286390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t2391178772 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t2556460284 * L_13 = FunctionArguments_get_Arg_m969707333(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t2391178772 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t2391178772 * L_15 = FunctionArguments_get_Tail_m52932169(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t2556460284 * L_16 = FunctionArguments_get_Arg_m969707333(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t2391178772 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t2391178772 * L_18 = FunctionArguments_get_Tail_m52932169(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t2391178772 * L_19 = FunctionArguments_get_Tail_m52932169(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008b;
		}
	}
	{
		FunctionArguments_t2391178772 * L_20 = ___args0;
		NullCheck(L_20);
		FunctionArguments_t2391178772 * L_21 = FunctionArguments_get_Tail_m52932169(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		FunctionArguments_t2391178772 * L_22 = FunctionArguments_get_Tail_m52932169(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Expression_t2556460284 * L_23 = FunctionArguments_get_Arg_m969707333(L_22, /*hidden argument*/NULL);
		__this->set_arg2_2(L_23);
	}

IL_008b:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstring::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstring_get_ReturnType_m3917299253 (XPathFunctionSubstring_t2992425472 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstring::get_Peer()
extern "C"  bool XPathFunctionSubstring_get_Peer_m3084538793 (XPathFunctionSubstring_t2992425472 * __this, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t2556460284 * L_4 = __this->get_arg2_2();
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		Expression_t2556460284 * L_5 = __this->get_arg2_2();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_5);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_003c;
	}

IL_003b:
	{
		G_B5_0 = 1;
	}

IL_003c:
	{
		G_B7_0 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 0;
	}

IL_003f:
	{
		return (bool)G_B7_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstring::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstring_Evaluate_m2782613730_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstring_Evaluate_m2782613730 (XPathFunctionSubstring_t2992425472 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_Evaluate_m2782613730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		double L_6 = bankers_round(L_5);
		V_1 = ((double)((double)L_6-(double)(1.0)));
		double L_7 = V_1;
		bool L_8 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004c;
		}
	}
	{
		double L_9 = V_1;
		bool L_10 = Double_IsNegativeInfinity_m553043933(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		double L_11 = V_1;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		if ((!(((double)L_11) >= ((double)(((double)((double)L_13)))))))
		{
			goto IL_0052;
		}
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_14;
	}

IL_0052:
	{
		Expression_t2556460284 * L_15 = __this->get_arg2_2();
		if (L_15)
		{
			goto IL_007f;
		}
	}
	{
		double L_16 = V_1;
		if ((!(((double)L_16) < ((double)(0.0)))))
		{
			goto IL_0076;
		}
	}
	{
		V_1 = (0.0);
	}

IL_0076:
	{
		String_t* L_17 = V_0;
		double L_18 = V_1;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m2809233063(L_17, (((int32_t)((int32_t)L_18))), /*hidden argument*/NULL);
		return L_19;
	}

IL_007f:
	{
		Expression_t2556460284 * L_20 = __this->get_arg2_2();
		BaseIterator_t1327316739 * L_21 = ___iter0;
		NullCheck(L_20);
		double L_22 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_20, L_21);
		double L_23 = bankers_round(L_22);
		V_2 = L_23;
		double L_24 = V_2;
		bool L_25 = Double_IsNaN_m506471885(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_26;
	}

IL_00a2:
	{
		double L_27 = V_1;
		if ((((double)L_27) < ((double)(0.0))))
		{
			goto IL_00c0;
		}
	}
	{
		double L_28 = V_2;
		if ((!(((double)L_28) < ((double)(0.0)))))
		{
			goto IL_00e3;
		}
	}

IL_00c0:
	{
		double L_29 = V_1;
		double L_30 = V_2;
		V_2 = ((double)((double)L_29+(double)L_30));
		double L_31 = V_2;
		if ((!(((double)L_31) <= ((double)(0.0)))))
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_32;
	}

IL_00d9:
	{
		V_1 = (0.0);
	}

IL_00e3:
	{
		String_t* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m2979997331(L_33, /*hidden argument*/NULL);
		double L_35 = V_1;
		V_3 = ((double)((double)(((double)((double)L_34)))-(double)L_35));
		double L_36 = V_2;
		double L_37 = V_3;
		if ((!(((double)L_36) > ((double)L_37))))
		{
			goto IL_00f6;
		}
	}
	{
		double L_38 = V_3;
		V_2 = L_38;
	}

IL_00f6:
	{
		String_t* L_39 = V_0;
		double L_40 = V_1;
		double L_41 = V_2;
		NullCheck(L_39);
		String_t* L_42 = String_Substring_m675079568(L_39, (((int32_t)((int32_t)L_40))), (((int32_t)((int32_t)L_41))), /*hidden argument*/NULL);
		return L_42;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstring::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3561905143;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstring_ToString_m97244847_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstring_ToString_m97244847 (XPathFunctionSubstring_t2992425472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_ToString_m97244847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3561905143);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3561905143);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral44);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_9 = L_8;
		Expression_t2556460284 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 5);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t4054002952* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringAfter::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1846764191;
extern const uint32_t XPathFunctionSubstringAfter__ctor_m4187633269_MetadataUsageId;
extern "C"  void XPathFunctionSubstringAfter__ctor_m4187633269 (XPathFunctionSubstringAfter_t1726871446 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter__ctor_m4187633269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1803876086 * L_7 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral1846764191, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2391178772 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t2556460284 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2391178772 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2391178772 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t2556460284 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringAfter::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringAfter_get_ReturnType_m4232721039 (XPathFunctionSubstringAfter_t1726871446 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringAfter::get_Peer()
extern "C"  bool XPathFunctionSubstringAfter_get_Peer_m94219979 (XPathFunctionSubstringAfter_t1726871446 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringAfter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstringAfter_Evaluate_m3905912518_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstringAfter_Evaluate_m3905912518 (XPathFunctionSubstringAfter_t1726871446 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_Evaluate_m3905912518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m1476794331(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		String_t* L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_15 = String_Substring_m2809233063(L_11, ((int32_t)((int32_t)L_12+(int32_t)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringAfter::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1994007752;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstringAfter_ToString_m3496542695_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstringAfter_ToString_m3496542695 (XPathFunctionSubstringAfter_t1726871446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_ToString_m3496542695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1994007752);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1994007752);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringBefore::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3519749988;
extern const uint32_t XPathFunctionSubstringBefore__ctor_m498616712_MetadataUsageId;
extern "C"  void XPathFunctionSubstringBefore__ctor_m498616712 (XPathFunctionSubstringBefore_t1709434719 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore__ctor_m498616712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1803876086 * L_7 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, _stringLiteral3519749988, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2391178772 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t2556460284 * L_9 = FunctionArguments_get_Arg_m969707333(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2391178772 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2391178772 * L_11 = FunctionArguments_get_Tail_m52932169(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t2556460284 * L_12 = FunctionArguments_get_Arg_m969707333(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringBefore::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringBefore_get_ReturnType_m529496916 (XPathFunctionSubstringBefore_t1709434719 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringBefore::get_Peer()
extern "C"  bool XPathFunctionSubstringBefore_get_Peer_m3859992328 (XPathFunctionSubstringBefore_t1709434719 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringBefore::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSubstringBefore_Evaluate_m2540736547_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSubstringBefore_Evaluate_m2540736547 (XPathFunctionSubstringBefore_t1709434719 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_Evaluate_m2540736547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m1476794331(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m675079568(L_11, 0, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringBefore::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2530943245;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSubstringBefore_ToString_m1453021774_MetadataUsageId;
extern "C"  String_t* XPathFunctionSubstringBefore_ToString_m1453021774 (XPathFunctionSubstringBefore_t1709434719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_ToString_m1453021774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2530943245);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2530943245);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral41);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m21867311(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSum::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3026784563;
extern const uint32_t XPathFunctionSum__ctor_m1037722509_MetadataUsageId;
extern "C"  void XPathFunctionSum__ctor_m1037722509 (XPathFunctionSum_t1422755706 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum__ctor_m1037722509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathNumericFunction__ctor_m3502463899(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1803876086 * L_4 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_4, _stringLiteral3026784563, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2391178772 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t2556460284 * L_6 = FunctionArguments_get_Arg_m969707333(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSum::get_Peer()
extern "C"  bool XPathFunctionSum_get_Peer_m224515491 (XPathFunctionSum_t1422755706 * __this, const MethodInfo* method)
{
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSum::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionSum_Evaluate_m2525900520_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionSum_Evaluate_m2525900520 (XPathFunctionSum_t1422755706 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_Evaluate_m2525900520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t1383168931 * V_0 = NULL;
	double V_1 = 0.0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t1327316739 * L_2 = VirtFuncInvoker1< BaseIterator_t1327316739 *, BaseIterator_t1327316739 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		V_1 = (0.0);
		goto IL_002f;
	}

IL_001c:
	{
		double L_3 = V_1;
		XPathNodeIterator_t1383168931 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		double L_7 = XPathFunctions_ToNumber_m997900729(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = ((double)((double)L_3+(double)L_7));
	}

IL_002f:
	{
		XPathNodeIterator_t1383168931 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_001c;
		}
	}
	{
		double L_10 = V_1;
		double L_11 = L_10;
		Il2CppObject * L_12 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}
}
// System.String System.Xml.XPath.XPathFunctionSum::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541821;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionSum_ToString_m3653312617_MetadataUsageId;
extern "C"  String_t* XPathFunctionSum_ToString_m3653312617 (XPathFunctionSum_t1422755706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_ToString_m3653312617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3541821);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3541821);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral41);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m21867311(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTranslate::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1187128336;
extern const uint32_t XPathFunctionTranslate__ctor_m1616227242_MetadataUsageId;
extern "C"  void XPathFunctionTranslate__ctor_m1616227242 (XPathFunctionTranslate_t3514715389 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate__ctor_m1616227242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2391178772 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2391178772 * L_3 = FunctionArguments_get_Tail_m52932169(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2391178772 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2391178772 * L_5 = FunctionArguments_get_Tail_m52932169(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2391178772 * L_6 = FunctionArguments_get_Tail_m52932169(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2391178772 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t2391178772 * L_8 = FunctionArguments_get_Tail_m52932169(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t2391178772 * L_9 = FunctionArguments_get_Tail_m52932169(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t2391178772 * L_10 = FunctionArguments_get_Tail_m52932169(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t1803876086 * L_11 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_11, _stringLiteral1187128336, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t2391178772 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t2556460284 * L_13 = FunctionArguments_get_Arg_m969707333(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t2391178772 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t2391178772 * L_15 = FunctionArguments_get_Tail_m52932169(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t2556460284 * L_16 = FunctionArguments_get_Arg_m969707333(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t2391178772 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t2391178772 * L_18 = FunctionArguments_get_Tail_m52932169(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t2391178772 * L_19 = FunctionArguments_get_Tail_m52932169(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Expression_t2556460284 * L_20 = FunctionArguments_get_Arg_m969707333(L_19, /*hidden argument*/NULL);
		__this->set_arg2_2(L_20);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionTranslate::get_ReturnType()
extern "C"  int32_t XPathFunctionTranslate_get_ReturnType_m2841959410 (XPathFunctionTranslate_t3514715389 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTranslate::get_Peer()
extern "C"  bool XPathFunctionTranslate_get_Peer_m1850098470 (XPathFunctionTranslate_t3514715389 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t2556460284 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t2556460284 * L_4 = __this->get_arg2_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B4_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B4_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTranslate::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTranslate_Evaluate_m4091121285_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTranslate_Evaluate_m4091121285 (XPathFunctionTranslate_t3514715389 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_Evaluate_m4091121285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t243639308 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Expression_t2556460284 * L_0 = __this->get_arg0_0();
		BaseIterator_t1327316739 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t2556460284 * L_3 = __this->get_arg1_1();
		BaseIterator_t1327316739 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		Expression_t2556460284 * L_6 = __this->get_arg2_2();
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		V_2 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2979997331(L_9, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_11 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		V_4 = 0;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m2979997331(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		goto IL_0095;
	}

IL_004b:
	{
		String_t* L_16 = V_1;
		String_t* L_17 = V_0;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		Il2CppChar L_19 = String_get_Chars_m3015341861(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_20 = String_IndexOf_m2775210486(L_16, L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		int32_t L_21 = V_7;
		if ((((int32_t)L_21) == ((int32_t)(-1))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_22 = V_7;
		int32_t L_23 = V_6;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007b;
		}
	}
	{
		StringBuilder_t243639308 * L_24 = V_3;
		String_t* L_25 = V_2;
		int32_t L_26 = V_7;
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m3015341861(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, L_27, /*hidden argument*/NULL);
	}

IL_007b:
	{
		goto IL_008f;
	}

IL_0080:
	{
		StringBuilder_t243639308 * L_28 = V_3;
		String_t* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		Il2CppChar L_31 = String_get_Chars_m3015341861(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		StringBuilder_Append_m2143093878(L_28, L_31, /*hidden argument*/NULL);
	}

IL_008f:
	{
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0095:
	{
		int32_t L_33 = V_4;
		int32_t L_34 = V_5;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_004b;
		}
	}
	{
		StringBuilder_t243639308 * L_35 = V_3;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m350379841(L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.String System.Xml.XPath.XPathFunctionTranslate::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3998139462;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t XPathFunctionTranslate_ToString_m3157771820_MetadataUsageId;
extern "C"  String_t* XPathFunctionTranslate_ToString_m3157771820 (XPathFunctionTranslate_t3514715389 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_ToString_m3157771820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t4054002952* L_0 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3998139462);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3998139462);
		StringU5BU5D_t4054002952* L_1 = L_0;
		Expression_t2556460284 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t4054002952* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral44);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_5 = L_4;
		Expression_t2556460284 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t4054002952* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral44);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral44);
		StringU5BU5D_t4054002952* L_9 = L_8;
		Expression_t2556460284 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 5);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t4054002952* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, _stringLiteral41);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTrue::.ctor(System.Xml.XPath.FunctionArguments)
extern Il2CppClass* XPathException_t1803876086_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral431422963;
extern const uint32_t XPathFunctionTrue__ctor_m3889883154_MetadataUsageId;
extern "C"  void XPathFunctionTrue__ctor_m3889883154 (XPathFunctionTrue_t844510361 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue__ctor_m3889883154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m3002905248(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2391178772 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1803876086 * L_2 = (XPathException_t1803876086 *)il2cpp_codegen_object_new(XPathException_t1803876086_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_2, _stringLiteral431422963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_HasStaticValue()
extern "C"  bool XPathFunctionTrue_get_HasStaticValue_m683691669 (XPathFunctionTrue_t844510361 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionTrue_get_StaticValueAsBoolean_m807479903 (XPathFunctionTrue_t844510361 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_Peer()
extern "C"  bool XPathFunctionTrue_get_Peer_m423716558 (XPathFunctionTrue_t844510361 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTrue::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionTrue_Evaluate_m955960675_MetadataUsageId;
extern "C"  Il2CppObject * XPathFunctionTrue_Evaluate_m955960675 (XPathFunctionTrue_t844510361 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_Evaluate_m955960675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((bool)1);
		Il2CppObject * L_1 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionTrue::ToString()
extern Il2CppCodeGenString* _stringLiteral3429846799;
extern const uint32_t XPathFunctionTrue_ToString_m3132420010_MetadataUsageId;
extern "C"  String_t* XPathFunctionTrue_ToString_m3132420010 (XPathFunctionTrue_t844510361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_ToString_m3132420010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3429846799;
	}
}
// System.Void System.Xml.XPath.XPathItem::.ctor()
extern "C"  void XPathItem__ctor_m4134533980 (XPathItem_t3597956134 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.ctor()
extern "C"  void XPathIteratorComparer__ctor_m2155618996 (XPathIteratorComparer_t3727165134 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.cctor()
extern Il2CppClass* XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer__cctor_m1917583225_MetadataUsageId;
extern "C"  void XPathIteratorComparer__cctor_m1917583225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer__cctor_m1917583225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathIteratorComparer_t3727165134 * L_0 = (XPathIteratorComparer_t3727165134 *)il2cpp_codegen_object_new(XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var);
		XPathIteratorComparer__ctor_m2155618996(L_0, /*hidden argument*/NULL);
		((XPathIteratorComparer_t3727165134_StaticFields*)XPathIteratorComparer_t3727165134_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Int32 System.Xml.XPath.XPathIteratorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer_Compare_m764851373_MetadataUsageId;
extern "C"  int32_t XPathIteratorComparer_Compare_m764851373 (XPathIteratorComparer_t3727165134 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer_Compare_m764851373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNodeIterator_t1383168931 * V_0 = NULL;
	XPathNodeIterator_t1383168931 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_0, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNodeIterator_t1383168931 *)IsInstClass(L_1, XPathNodeIterator_t1383168931_il2cpp_TypeInfo_var));
		XPathNodeIterator_t1383168931 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNodeIterator_t1383168931 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNodeIterator_t1383168931 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t1075073278 * L_5 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		XPathNodeIterator_t1383168931 * L_6 = V_1;
		NullCheck(L_6);
		XPathNavigator_t1075073278 * L_7 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		NullCheck(L_5);
		int32_t L_8 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_5, L_7);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0047;
	}

IL_0043:
	{
		return 0;
	}

IL_0045:
	{
		return (-1);
	}

IL_0047:
	{
		return 1;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.ctor()
extern "C"  void XPathNavigator__ctor_m2215010048 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		XPathItem__ctor_m4134533980(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.cctor()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D18_9_FieldInfo_var;
extern const uint32_t XPathNavigator__cctor_m3758705837_MetadataUsageId;
extern "C"  void XPathNavigator__cctor_m3758705837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator__cctor_m3758705837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3324145743* L_0 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		CharU5BU5D_t3324145743* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)60));
		CharU5BU5D_t3324145743* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppChar)((int32_t)62));
		((XPathNavigator_t1075073278_StaticFields*)XPathNavigator_t1075073278_il2cpp_TypeInfo_var->static_fields)->set_escape_text_chars_0(L_2);
		CharU5BU5D_t3324145743* L_3 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238937____U24U24fieldU2D18_9_FieldInfo_var), /*hidden argument*/NULL);
		((XPathNavigator_t1075073278_StaticFields*)XPathNavigator_t1075073278_il2cpp_TypeInfo_var->static_fields)->set_escape_attr_chars_1(L_3);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNavigator_System_ICloneable_Clone_m1822899695 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes()
extern "C"  bool XPathNavigator_get_HasAttributes_m238100238 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren()
extern "C"  bool XPathNavigator_get_HasChildren_m3581374262 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.String System.Xml.XPath.XPathNavigator::get_XmlLang()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3314158;
extern Il2CppCodeGenString* _stringLiteral1952986079;
extern const uint32_t XPathNavigator_get_XmlLang_m3014372733_MetadataUsageId;
extern "C"  String_t* XPathNavigator_get_XmlLang_m3014372733 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_get_XmlLang_m3014372733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		XPathNavigator_t1075073278 * L_0 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t1075073278 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	{
		XPathNavigator_t1075073278 * L_5 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		goto IL_002d;
	}

IL_002d:
	{
		XPathNavigator_t1075073278 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(20 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String) */, L_6, _stringLiteral3314158, _stringLiteral1952986079);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		XPathNavigator_t1075073278 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		return L_9;
	}

IL_0049:
	{
		XPathNavigator_t1075073278 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_10);
		if (L_11)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_12;
	}
}
// System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern "C"  int32_t XPathNavigator_ComparePosition_m3713421981 (XPathNavigator_t1075073278 * __this, XPathNavigator_t1075073278 * ___nav0, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * V_0 = NULL;
	XPathNavigator_t1075073278 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		XPathNavigator_t1075073278 * L_0 = ___nav0;
		bool L_1 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000e:
	{
		XPathNavigator_t1075073278 * L_2 = ___nav0;
		bool L_3 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, __this, L_2);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (int32_t)(0);
	}

IL_001c:
	{
		XPathNavigator_t1075073278 * L_4 = ___nav0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_4, __this);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(1);
	}

IL_002a:
	{
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_6;
		XPathNavigator_t1075073278 * L_7 = ___nav0;
		NullCheck(L_7);
		XPathNavigator_t1075073278 * L_8 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_7);
		V_1 = L_8;
		XPathNavigator_t1075073278 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_9);
		XPathNavigator_t1075073278 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_10);
		XPathNavigator_t1075073278 * L_11 = V_0;
		XPathNavigator_t1075073278 * L_12 = V_1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0052:
	{
		XPathNavigator_t1075073278 * L_14 = V_0;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_14, __this);
		XPathNavigator_t1075073278 * L_15 = V_1;
		XPathNavigator_t1075073278 * L_16 = ___nav0;
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
		V_2 = 0;
		goto IL_006d;
	}

IL_0069:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006d:
	{
		XPathNavigator_t1075073278 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_0069;
		}
	}
	{
		XPathNavigator_t1075073278 * L_20 = V_0;
		NullCheck(L_20);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_20, __this);
		V_3 = 0;
		goto IL_008b;
	}

IL_0087:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008b:
	{
		XPathNavigator_t1075073278 * L_22 = V_1;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_22);
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		XPathNavigator_t1075073278 * L_24 = V_1;
		XPathNavigator_t1075073278 * L_25 = ___nav0;
		NullCheck(L_24);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_24, L_25);
		int32_t L_26 = V_2;
		V_4 = L_26;
		goto IL_00b3;
	}

IL_00a6:
	{
		XPathNavigator_t1075073278 * L_27 = V_0;
		NullCheck(L_27);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_27);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = V_3;
		if ((((int32_t)L_29) > ((int32_t)L_30)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_31 = V_3;
		V_5 = L_31;
		goto IL_00d0;
	}

IL_00c3:
	{
		XPathNavigator_t1075073278 * L_32 = V_1;
		NullCheck(L_32);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_32);
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33-(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_4;
		if ((((int32_t)L_34) > ((int32_t)L_35)))
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_00f2;
	}

IL_00de:
	{
		XPathNavigator_t1075073278 * L_36 = V_0;
		NullCheck(L_36);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_36);
		XPathNavigator_t1075073278 * L_37 = V_1;
		NullCheck(L_37);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_37);
		int32_t L_38 = V_4;
		V_4 = ((int32_t)((int32_t)L_38-(int32_t)1));
	}

IL_00f2:
	{
		XPathNavigator_t1075073278 * L_39 = V_0;
		XPathNavigator_t1075073278 * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_40);
		if (!L_41)
		{
			goto IL_00de;
		}
	}
	{
		XPathNavigator_t1075073278 * L_42 = V_0;
		NullCheck(L_42);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_42, __this);
		int32_t L_43 = V_2;
		V_6 = L_43;
		goto IL_011b;
	}

IL_010e:
	{
		XPathNavigator_t1075073278 * L_44 = V_0;
		NullCheck(L_44);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_44);
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45-(int32_t)1));
	}

IL_011b:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = V_4;
		if ((((int32_t)L_46) > ((int32_t)((int32_t)((int32_t)L_47+(int32_t)1)))))
		{
			goto IL_010e;
		}
	}
	{
		XPathNavigator_t1075073278 * L_48 = V_1;
		XPathNavigator_t1075073278 * L_49 = ___nav0;
		NullCheck(L_48);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_48, L_49);
		int32_t L_50 = V_3;
		V_7 = L_50;
		goto IL_0143;
	}

IL_0136:
	{
		XPathNavigator_t1075073278 * L_51 = V_1;
		NullCheck(L_51);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_51);
		int32_t L_52 = V_7;
		V_7 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_0143:
	{
		int32_t L_53 = V_7;
		int32_t L_54 = V_4;
		if ((((int32_t)L_53) > ((int32_t)((int32_t)((int32_t)L_54+(int32_t)1)))))
		{
			goto IL_0136;
		}
	}
	{
		XPathNavigator_t1075073278 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_55);
		if ((!(((uint32_t)L_56) == ((uint32_t)3))))
		{
			goto IL_0188;
		}
	}
	{
		XPathNavigator_t1075073278 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_57);
		if ((((int32_t)L_58) == ((int32_t)3)))
		{
			goto IL_0168;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0168:
	{
		goto IL_017b;
	}

IL_016d:
	{
		XPathNavigator_t1075073278 * L_59 = V_0;
		XPathNavigator_t1075073278 * L_60 = V_1;
		NullCheck(L_59);
		bool L_61 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_59, L_60);
		if (!L_61)
		{
			goto IL_017b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_017b:
	{
		XPathNavigator_t1075073278 * L_62 = V_0;
		NullCheck(L_62);
		bool L_63 = XPathNavigator_MoveToNextNamespace_m197928770(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_016d;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0188:
	{
		XPathNavigator_t1075073278 * L_64 = V_1;
		NullCheck(L_64);
		int32_t L_65 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_64);
		if ((!(((uint32_t)L_65) == ((uint32_t)3))))
		{
			goto IL_0196;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0196:
	{
		XPathNavigator_t1075073278 * L_66 = V_0;
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_66);
		if ((!(((uint32_t)L_67) == ((uint32_t)2))))
		{
			goto IL_01d0;
		}
	}
	{
		XPathNavigator_t1075073278 * L_68 = V_1;
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_68);
		if ((((int32_t)L_69) == ((int32_t)2)))
		{
			goto IL_01b0;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01b0:
	{
		goto IL_01c3;
	}

IL_01b5:
	{
		XPathNavigator_t1075073278 * L_70 = V_0;
		XPathNavigator_t1075073278 * L_71 = V_1;
		NullCheck(L_70);
		bool L_72 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_70, L_71);
		if (!L_72)
		{
			goto IL_01c3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01c3:
	{
		XPathNavigator_t1075073278 * L_73 = V_0;
		NullCheck(L_73);
		bool L_74 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_73);
		if (L_74)
		{
			goto IL_01b5;
		}
	}
	{
		return (int32_t)(1);
	}

IL_01d0:
	{
		goto IL_01e3;
	}

IL_01d5:
	{
		XPathNavigator_t1075073278 * L_75 = V_0;
		XPathNavigator_t1075073278 * L_76 = V_1;
		NullCheck(L_75);
		bool L_77 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_75, L_76);
		if (!L_77)
		{
			goto IL_01e3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01e3:
	{
		XPathNavigator_t1075073278 * L_78 = V_0;
		NullCheck(L_78);
		bool L_79 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_78);
		if (L_79)
		{
			goto IL_01d5;
		}
	}
	{
		return (int32_t)(1);
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathNavigator::Compile(System.String)
extern "C"  XPathExpression_t3588072235 * XPathNavigator_Compile_m3812787918 (XPathNavigator_t1075073278 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t3588072235 * L_1 = XPathExpression_Compile_m1263926045(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XPathNavigator_IsDescendant_m3913947374 (XPathNavigator_t1075073278 * __this, XPathNavigator_t1075073278 * ___nav0, const MethodInfo* method)
{
	{
		XPathNavigator_t1075073278 * L_0 = ___nav0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		XPathNavigator_t1075073278 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t1075073278 * L_2 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		___nav0 = L_2;
		goto IL_0021;
	}

IL_0013:
	{
		XPathNavigator_t1075073278 * L_3 = ___nav0;
		bool L_4 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_3);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		XPathNavigator_t1075073278 * L_5 = ___nav0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		if (L_6)
		{
			goto IL_0013;
		}
	}

IL_002c:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToAttribute_m192146504_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToAttribute_m192146504 (XPathNavigator_t1075073278 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToAttribute_m192146504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (!L_0)
		{
			goto IL_0041;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, __this);
		String_t* L_5 = ___namespaceURI1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, __this);
		if (L_7)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_MoveToNamespace_m3481610349_MetadataUsageId;
extern "C"  bool XPathNavigator_MoveToNamespace_m3481610349 (XPathNavigator_t1075073278 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToNamespace_m3481610349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = XPathNavigator_MoveToFirstNamespace_m3738691923(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)1;
	}

IL_001e:
	{
		bool L_4 = XPathNavigator_MoveToNextNamespace_m197928770(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst()
extern "C"  bool XPathNavigator_MoveToFirst_m3064231242 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		bool L_0 = XPathNavigator_MoveToFirstImpl_m1974955786(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::MoveToRoot()
extern "C"  void XPathNavigator_MoveToRoot_m4278006194 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstImpl()
extern "C"  bool XPathNavigator_MoveToFirstImpl_m1974955786 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, __this);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_001c;
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace()
extern "C"  bool XPathNavigator_MoveToFirstNamespace_m3738691923 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace()
extern "C"  bool XPathNavigator_MoveToNextNamespace_m197928770 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression)
extern "C"  XPathNodeIterator_t1383168931 * XPathNavigator_Select_m1543264903 (XPathNavigator_t1075073278 * __this, XPathExpression_t3588072235 * ___expr0, const MethodInfo* method)
{
	{
		XPathExpression_t3588072235 * L_0 = ___expr0;
		XPathNodeIterator_t1383168931 * L_1 = XPathNavigator_Select_m2072065794(__this, L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* CompiledExpression_t3956813165_il2cpp_TypeInfo_var;
extern Il2CppClass* NullIterator_t2905335737_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_Select_m2072065794_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * XPathNavigator_Select_m2072065794 (XPathNavigator_t1075073278 * __this, XPathExpression_t3588072235 * ___expr0, Il2CppObject * ___ctx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_Select_m2072065794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CompiledExpression_t3956813165 * V_0 = NULL;
	BaseIterator_t1327316739 * V_1 = NULL;
	{
		XPathExpression_t3588072235 * L_0 = ___expr0;
		V_0 = ((CompiledExpression_t3956813165 *)CastclassClass(L_0, CompiledExpression_t3956813165_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___ctx1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		CompiledExpression_t3956813165 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = CompiledExpression_get_NamespaceManager_m652200026(L_2, /*hidden argument*/NULL);
		___ctx1 = L_3;
	}

IL_0015:
	{
		Il2CppObject * L_4 = ___ctx1;
		NullIterator_t2905335737 * L_5 = (NullIterator_t2905335737 *)il2cpp_codegen_object_new(NullIterator_t2905335737_il2cpp_TypeInfo_var);
		NullIterator__ctor_m4158990173(L_5, __this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		CompiledExpression_t3956813165 * L_6 = V_0;
		BaseIterator_t1327316739 * L_7 = V_1;
		NullCheck(L_6);
		XPathNodeIterator_t1383168931 * L_8 = CompiledExpression_EvaluateNodeSet_m2426588395(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator::EnumerateChildren(System.Xml.XPath.XPathNavigator,System.Xml.XPath.XPathNodeType)
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t1273927208_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_EnumerateChildren_m710965660_MetadataUsageId;
extern "C"  Il2CppObject * XPathNavigator_EnumerateChildren_m710965660 (Il2CppObject * __this /* static, unused */, XPathNavigator_t1075073278 * ___n0, int32_t ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_EnumerateChildren_m710965660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * V_0 = NULL;
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_0 = (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1273927208_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_1 = V_0;
		XPathNavigator_t1075073278 * L_2 = ___n0;
		NullCheck(L_1);
		L_1->set_n_0(L_2);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_3 = V_0;
		int32_t L_4 = ___type1;
		NullCheck(L_3);
		L_3->set_type_3(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_5 = V_0;
		XPathNavigator_t1075073278 * L_6 = ___n0;
		NullCheck(L_5);
		L_5->set_U3CU24U3En_6(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_7 = V_0;
		int32_t L_8 = ___type1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_7(L_8);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_9 = V_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType)
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern Il2CppClass* EnumerableIterator_t2530618692_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperIterator_t2849115671_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_SelectChildren_m2299731170_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * XPathNavigator_SelectChildren_m2299731170 (XPathNavigator_t1075073278 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_SelectChildren_m2299731170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t1075073278_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = XPathNavigator_EnumerateChildren_m710965660(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		EnumerableIterator_t2530618692 * L_2 = (EnumerableIterator_t2530618692 *)il2cpp_codegen_object_new(EnumerableIterator_t2530618692_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m3124749935(L_2, L_1, 0, /*hidden argument*/NULL);
		WrapperIterator_t2849115671 * L_3 = (WrapperIterator_t2849115671 *)il2cpp_codegen_object_new(WrapperIterator_t2849115671_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m802118820(L_3, L_2, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathNavigator::ToString()
extern "C"  String_t* XPathNavigator_ToString_m2589892525 (XPathNavigator_t1075073278 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		return L_0;
	}
}
// System.String System.Xml.XPath.XPathNavigator::LookupNamespace(System.String)
extern "C"  String_t* XPathNavigator_LookupNamespace_m3979214784 (XPathNavigator_t1075073278 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	XPathNavigator_t1075073278 * V_0 = NULL;
	{
		XPathNavigator_t1075073278 * L_0 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t1075073278 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigator_t1075073278 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
	}

IL_001a:
	{
		XPathNavigator_t1075073278 * L_4 = V_0;
		String_t* L_5 = ___prefix0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(21 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		XPathNavigator_t1075073278 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_7);
		return L_8;
	}

IL_002d:
	{
		return (String_t*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
