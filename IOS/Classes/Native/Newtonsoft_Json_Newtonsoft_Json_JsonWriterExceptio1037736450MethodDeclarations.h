﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonWriterException
struct JsonWriterException_t1037736450;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Exception
struct Exception_t3991598821;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Exception3991598821.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"

// System.Void Newtonsoft.Json.JsonWriterException::set_Path(System.String)
extern "C"  void JsonWriterException_set_Path_m64230813 (JsonWriterException_t1037736450 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriterException::.ctor()
extern "C"  void JsonWriterException__ctor_m1743113217 (JsonWriterException_t1037736450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonWriterException__ctor_m4291750338 (JsonWriterException_t1037736450 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String,System.Exception,System.String)
extern "C"  void JsonWriterException__ctor_m4203226673 (JsonWriterException_t1037736450 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, String_t* ___path2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(Newtonsoft.Json.JsonWriter,System.String,System.Exception)
extern "C"  JsonWriterException_t1037736450 * JsonWriterException_Create_m3406894431 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ___writer0, String_t* ___message1, Exception_t3991598821 * ___ex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(System.String,System.String,System.Exception)
extern "C"  JsonWriterException_t1037736450 * JsonWriterException_Create_m875081667 (Il2CppObject * __this /* static, unused */, String_t* ___path0, String_t* ___message1, Exception_t3991598821 * ___ex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
