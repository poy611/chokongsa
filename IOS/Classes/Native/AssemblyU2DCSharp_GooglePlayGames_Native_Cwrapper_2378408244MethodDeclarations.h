﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
struct OnAuthActionStartedCallback_t2378408244;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1624738888.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAuthActionStartedCallback__ctor_m1826211083 (OnAuthActionStartedCallback_t2378408244 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
extern "C"  void OnAuthActionStartedCallback_Invoke_m1811538739 (OnAuthActionStartedCallback_t2378408244 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAuthActionStartedCallback_BeginInvoke_m3632451220 (OnAuthActionStartedCallback_t2378408244 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnAuthActionStartedCallback_EndInvoke_m1398240411 (OnAuthActionStartedCallback_t2378408244 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
