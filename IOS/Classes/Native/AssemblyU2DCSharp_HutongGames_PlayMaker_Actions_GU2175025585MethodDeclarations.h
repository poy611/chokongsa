﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutFloatField
struct GUILayoutFloatField_t2175025585;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::.ctor()
extern "C"  void GUILayoutFloatField__ctor_m3168508069 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::Reset()
extern "C"  void GUILayoutFloatField_Reset_m814941010 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::OnGUI()
extern "C"  void GUILayoutFloatField_OnGUI_m2663906719 (GUILayoutFloatField_t2175025585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
