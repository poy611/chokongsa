﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>
struct DictionaryWrapper_2_t3980492226;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2483180780;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void DictionaryWrapper_2_Add_m169742595_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m169742595(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Add_m169742595_gshared)(__this, ___key0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool DictionaryWrapper_2_ContainsKey_m575413467_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_ContainsKey_m575413467(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_ContainsKey_m575413467_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* DictionaryWrapper_2_get_Keys_m2676182786_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Keys_m2676182786(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_Keys_m2676182786_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool DictionaryWrapper_2_Remove_m2795913013_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m2795913013(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m2795913013_gshared)(__this, ___key0, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool DictionaryWrapper_2_TryGetValue_m2022140468_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_TryGetValue_m2022140468(__this, ___key0, ___value1, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))DictionaryWrapper_2_TryGetValue_m2022140468_gshared)(__this, ___key0, ___value1, method)
// TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * DictionaryWrapper_2_get_Item_m1244011961_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_get_Item_m1244011961(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_get_Item_m1244011961_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void DictionaryWrapper_2_set_Item_m2397100594_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_set_Item_m2397100594(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_set_Item_m2397100594_gshared)(__this, ___key0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void DictionaryWrapper_2_Add_m758248834_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Add_m758248834(__this, ___item0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Add_m758248834_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Clear()
extern "C"  void DictionaryWrapper_2_Clear_m1785475101_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_Clear_m1785475101(__this, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_Clear_m1785475101_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Contains_m482039724_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Contains_m482039724(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Contains_m482039724_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void DictionaryWrapper_2_CopyTo_m4028713190_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2U5BU5D_t2483180780* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define DictionaryWrapper_2_CopyTo_m4028713190(__this, ___array0, ___arrayIndex1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2U5BU5D_t2483180780*, int32_t, const MethodInfo*))DictionaryWrapper_2_CopyTo_m4028713190_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t DictionaryWrapper_2_get_Count_m451164914_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_Count_m451164914(__this, method) ((  int32_t (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_Count_m451164914_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool DictionaryWrapper_2_get_IsReadOnly_m1339546929_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_IsReadOnly_m1339546929(__this, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_IsReadOnly_m1339546929_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool DictionaryWrapper_2_Remove_m743668177_gshared (DictionaryWrapper_2_t3980492226 * __this, KeyValuePair_2_t1944668977  ___item0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m743668177(__this, ___item0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, KeyValuePair_2_t1944668977 , const MethodInfo*))DictionaryWrapper_2_Remove_m743668177_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* DictionaryWrapper_2_GetEnumerator_m2402075769_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_GetEnumerator_m2402075769(__this, method) ((  Il2CppObject* (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_GetEnumerator_m2402075769_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2289560565_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2289560565(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IEnumerable_GetEnumerator_m2289560565_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Add_m79139574_gshared)(__this, ___key0, ___value1, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150(__this, ___key0, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Item_m584992150_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163(__this, ___key0, ___value1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_set_Item_m2287127163_gshared)(__this, ___key0, ___value1, method)
// System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_GetEnumerator_m1767051021_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456(__this, ___key0, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_Contains_m3211452456_gshared)(__this, ___key0, method)
// System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_IDictionary_get_Keys_m3011412688_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::Remove(System.Object)
extern "C"  void DictionaryWrapper_2_Remove_m1555833598_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define DictionaryWrapper_2_Remove_m1555833598(__this, ___key0, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppObject *, const MethodInfo*))DictionaryWrapper_2_Remove_m1555833598_gshared)(__this, ___key0, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506_gshared (DictionaryWrapper_2_t3980492226 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506(__this, ___array0, ___index1, method) ((  void (*) (DictionaryWrapper_2_t3980492226 *, Il2CppArray *, int32_t, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_CopyTo_m3249071506_gshared)(__this, ___array0, ___index1, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476(__this, method) ((  bool (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_IsSynchronized_m2155282476_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_System_Collections_ICollection_get_SyncRoot_m2928378176_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2<System.Object,System.Object>::get_UnderlyingDictionary()
extern "C"  Il2CppObject * DictionaryWrapper_2_get_UnderlyingDictionary_m150371939_gshared (DictionaryWrapper_2_t3980492226 * __this, const MethodInfo* method);
#define DictionaryWrapper_2_get_UnderlyingDictionary_m150371939(__this, method) ((  Il2CppObject * (*) (DictionaryWrapper_2_t3980492226 *, const MethodInfo*))DictionaryWrapper_2_get_UnderlyingDictionary_m150371939_gshared)(__this, method)
