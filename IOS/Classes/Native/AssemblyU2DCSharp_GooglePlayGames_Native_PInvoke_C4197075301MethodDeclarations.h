﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>
struct U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>::.ctor()
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9B_1__ctor_m1610901700_gshared (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 * __this, const MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey9B_1__ctor_m1610901700(__this, method) ((  void (*) (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 *, const MethodInfo*))U3CToIntPtrU3Ec__AnonStorey9B_1__ctor_m1610901700_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9B`1<System.Object>::<>m__9B(System.IntPtr)
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110_gshared (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 * __this, IntPtr_t ___result0, const MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110(__this, ___result0, method) ((  void (*) (U3CToIntPtrU3Ec__AnonStorey9B_1_t4197075301 *, IntPtr_t, const MethodInfo*))U3CToIntPtrU3Ec__AnonStorey9B_1_U3CU3Em__9B_m606462110_gshared)(__this, ___result0, method)
