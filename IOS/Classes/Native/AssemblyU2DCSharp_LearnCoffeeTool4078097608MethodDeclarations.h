﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearnCoffeeTool
struct LearnCoffeeTool_t4078097608;

#include "codegen/il2cpp-codegen.h"

// System.Void LearnCoffeeTool::.ctor()
extern "C"  void LearnCoffeeTool__ctor_m3731084195 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::Start()
extern "C"  void LearnCoffeeTool_Start_m2678221987 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::backButton()
extern "C"  void LearnCoffeeTool_backButton_m2652998106 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::prev()
extern "C"  void LearnCoffeeTool_prev_m2716028564 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::next()
extern "C"  void LearnCoffeeTool_next_m2647328596 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::deskit()
extern "C"  void LearnCoffeeTool_deskit_m1064459685 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeeTool::unableButton()
extern "C"  void LearnCoffeeTool_unableButton_m4098187366 (LearnCoffeeTool_t4078097608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
