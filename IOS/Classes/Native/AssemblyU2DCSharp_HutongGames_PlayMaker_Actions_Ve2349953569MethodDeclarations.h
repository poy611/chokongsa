﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Operator
struct Vector2Operator_t2349953569;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::.ctor()
extern "C"  void Vector2Operator__ctor_m2843515445 (Vector2Operator_t2349953569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::Reset()
extern "C"  void Vector2Operator_Reset_m489948386 (Vector2Operator_t2349953569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::OnEnter()
extern "C"  void Vector2Operator_OnEnter_m410870988 (Vector2Operator_t2349953569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::OnUpdate()
extern "C"  void Vector2Operator_OnUpdate_m3280625399 (Vector2Operator_t2349953569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::DoVector2Operator()
extern "C"  void Vector2Operator_DoVector2Operator_m487697243 (Vector2Operator_t2349953569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
