﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RestartLevel
struct RestartLevel_t2713825501;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RestartLevel::.ctor()
extern "C"  void RestartLevel__ctor_m1275700777 (RestartLevel_t2713825501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RestartLevel::OnEnter()
extern "C"  void RestartLevel_OnEnter_m1274495936 (RestartLevel_t2713825501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
