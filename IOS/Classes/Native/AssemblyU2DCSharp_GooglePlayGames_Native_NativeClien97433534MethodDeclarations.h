﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<LoadAchievements>c__AnonStorey54
struct U3CLoadAchievementsU3Ec__AnonStorey54_t97433534;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<LoadAchievements>c__AnonStorey54::.ctor()
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey54__ctor_m332113773 (U3CLoadAchievementsU3Ec__AnonStorey54_t97433534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadAchievements>c__AnonStorey54::<>m__28()
extern "C"  void U3CLoadAchievementsU3Ec__AnonStorey54_U3CU3Em__28_m3641833404 (U3CLoadAchievementsU3Ec__AnonStorey54_t97433534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
