﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionSlerp
struct QuaternionSlerp_t1616762106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::.ctor()
extern "C"  void QuaternionSlerp__ctor_m440359804 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::Reset()
extern "C"  void QuaternionSlerp_Reset_m2381760041 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnEnter()
extern "C"  void QuaternionSlerp_OnEnter_m1670705235 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnUpdate()
extern "C"  void QuaternionSlerp_OnUpdate_m3680781392 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnLateUpdate()
extern "C"  void QuaternionSlerp_OnLateUpdate_m4267401750 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnFixedUpdate()
extern "C"  void QuaternionSlerp_OnFixedUpdate_m21413400 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::DoQuatSlerp()
extern "C"  void QuaternionSlerp_DoQuatSlerp_m1237460194 (QuaternionSlerp_t1616762106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
