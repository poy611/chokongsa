﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4272688975MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1113965554(__this, ___host0, method) ((  void (*) (Enumerator_t13898955 *, Dictionary_2_t2082065547 *, const MethodInfo*))Enumerator__ctor_m76754913_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1437872953(__this, method) ((  Il2CppObject * (*) (Enumerator_t13898955 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3118196448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2964912515(__this, method) ((  void (*) (Enumerator_t13898955 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3702199860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::Dispose()
#define Enumerator_Dispose_m2031831828(__this, method) ((  void (*) (Enumerator_t13898955 *, const MethodInfo*))Enumerator_Dispose_m1628348611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::MoveNext()
#define Enumerator_MoveNext_m3964647347(__this, method) ((  bool (*) (Enumerator_t13898955 *, const MethodInfo*))Enumerator_MoveNext_m3556422944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::get_Current()
#define Enumerator_get_Current_m3502971287(__this, method) ((  Achievement_t1261647177 * (*) (Enumerator_t13898955 *, const MethodInfo*))Enumerator_get_Current_m841474402_gshared)(__this, method)
