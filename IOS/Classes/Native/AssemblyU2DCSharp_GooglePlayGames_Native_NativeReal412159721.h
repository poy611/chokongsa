﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t3078724631;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70
struct U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72
struct U3CAcceptInvitationU3Ec__AnonStorey72_t412159720;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73
struct  U3CAcceptInvitationU3Ec__AnonStorey73_t412159721  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73::helper
	RealTimeEventListenerHelper_t3078724631 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73::<>f__ref$112
	U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * ___U3CU3Ef__refU24112_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey73::<>f__ref$114
	U3CAcceptInvitationU3Ec__AnonStorey72_t412159720 * ___U3CU3Ef__refU24114_2;

public:
	inline static int32_t get_offset_of_helper_0() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey73_t412159721, ___helper_0)); }
	inline RealTimeEventListenerHelper_t3078724631 * get_helper_0() const { return ___helper_0; }
	inline RealTimeEventListenerHelper_t3078724631 ** get_address_of_helper_0() { return &___helper_0; }
	inline void set_helper_0(RealTimeEventListenerHelper_t3078724631 * value)
	{
		___helper_0 = value;
		Il2CppCodeGenWriteBarrier(&___helper_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24112_1() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey73_t412159721, ___U3CU3Ef__refU24112_1)); }
	inline U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * get_U3CU3Ef__refU24112_1() const { return ___U3CU3Ef__refU24112_1; }
	inline U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 ** get_address_of_U3CU3Ef__refU24112_1() { return &___U3CU3Ef__refU24112_1; }
	inline void set_U3CU3Ef__refU24112_1(U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * value)
	{
		___U3CU3Ef__refU24112_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24112_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24114_2() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey73_t412159721, ___U3CU3Ef__refU24114_2)); }
	inline U3CAcceptInvitationU3Ec__AnonStorey72_t412159720 * get_U3CU3Ef__refU24114_2() const { return ___U3CU3Ef__refU24114_2; }
	inline U3CAcceptInvitationU3Ec__AnonStorey72_t412159720 ** get_address_of_U3CU3Ef__refU24114_2() { return &___U3CU3Ef__refU24114_2; }
	inline void set_U3CU3Ef__refU24114_2(U3CAcceptInvitationU3Ec__AnonStorey72_t412159720 * value)
	{
		___U3CU3Ef__refU24114_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24114_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
