﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenRestart>c__IteratorB
struct U3CTweenRestartU3Ec__IteratorB_t4135396284;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenRestart>c__IteratorB::.ctor()
extern "C"  void U3CTweenRestartU3Ec__IteratorB__ctor_m428223791 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2938173123 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenRestartU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m136765527 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenRestart>c__IteratorB::MoveNext()
extern "C"  bool U3CTweenRestartU3Ec__IteratorB_MoveNext_m534588261 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__IteratorB::Dispose()
extern "C"  void U3CTweenRestartU3Ec__IteratorB_Dispose_m3399453100 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__IteratorB::Reset()
extern "C"  void U3CTweenRestartU3Ec__IteratorB_Reset_m2369624028 (U3CTweenRestartU3Ec__IteratorB_t4135396284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
