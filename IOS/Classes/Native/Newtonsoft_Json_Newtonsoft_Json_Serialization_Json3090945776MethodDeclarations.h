﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext
struct CreatorPropertyContext_t3090945776;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::.ctor()
extern "C"  void CreatorPropertyContext__ctor_m2311177424 (CreatorPropertyContext_t3090945776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
