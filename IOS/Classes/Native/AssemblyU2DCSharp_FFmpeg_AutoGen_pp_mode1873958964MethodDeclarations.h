﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FFmpeg.AutoGen.pp_mode
struct pp_mode_t1873958964;
struct pp_mode_t1873958964_marshaled_pinvoke;
struct pp_mode_t1873958964_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct pp_mode_t1873958964;
struct pp_mode_t1873958964_marshaled_pinvoke;

extern "C" void pp_mode_t1873958964_marshal_pinvoke(const pp_mode_t1873958964& unmarshaled, pp_mode_t1873958964_marshaled_pinvoke& marshaled);
extern "C" void pp_mode_t1873958964_marshal_pinvoke_back(const pp_mode_t1873958964_marshaled_pinvoke& marshaled, pp_mode_t1873958964& unmarshaled);
extern "C" void pp_mode_t1873958964_marshal_pinvoke_cleanup(pp_mode_t1873958964_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct pp_mode_t1873958964;
struct pp_mode_t1873958964_marshaled_com;

extern "C" void pp_mode_t1873958964_marshal_com(const pp_mode_t1873958964& unmarshaled, pp_mode_t1873958964_marshaled_com& marshaled);
extern "C" void pp_mode_t1873958964_marshal_com_back(const pp_mode_t1873958964_marshaled_com& marshaled, pp_mode_t1873958964& unmarshaled);
extern "C" void pp_mode_t1873958964_marshal_com_cleanup(pp_mode_t1873958964_marshaled_com& marshaled);
