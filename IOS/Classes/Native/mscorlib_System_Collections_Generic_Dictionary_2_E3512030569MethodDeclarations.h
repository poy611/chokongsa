﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t2194707177;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3512030569.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3626454267_gshared (Enumerator_t3512030569 * __this, Dictionary_2_t2194707177 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3626454267(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3512030569 *, Dictionary_2_t2194707177 *, const MethodInfo*))Enumerator__ctor_m3626454267_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4273680134_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4273680134(__this, method) ((  Il2CppObject * (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4273680134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1183956186_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1183956186(__this, method) ((  void (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1183956186_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2033855971_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2033855971(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2033855971_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m374378338_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m374378338(__this, method) ((  Il2CppObject * (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m374378338_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m48766324_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m48766324(__this, method) ((  Il2CppObject * (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m48766324_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m311749830_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m311749830(__this, method) ((  bool (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_MoveNext_m311749830_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_Current()
extern "C"  KeyValuePair_2_t2093487883  Enumerator_get_Current_m3980994474_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3980994474(__this, method) ((  KeyValuePair_2_t2093487883  (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_get_Current_m3980994474_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m699710483_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m699710483(__this, method) ((  Il2CppObject * (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_get_CurrentKey_m699710483_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::get_CurrentValue()
extern "C"  uint32_t Enumerator_get_CurrentValue_m2316576759_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2316576759(__this, method) ((  uint32_t (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_get_CurrentValue_m2316576759_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::Reset()
extern "C"  void Enumerator_Reset_m3804405453_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3804405453(__this, method) ((  void (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_Reset_m3804405453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4031045910_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4031045910(__this, method) ((  void (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_VerifyState_m4031045910_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2519490302_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2519490302(__this, method) ((  void (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_VerifyCurrent_m2519490302_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m3539900509_gshared (Enumerator_t3512030569 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3539900509(__this, method) ((  void (*) (Enumerator_t3512030569 *, const MethodInfo*))Enumerator_Dispose_m3539900509_gshared)(__this, method)
