﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3197568542_gshared (Nullable_1_t2443451997 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3197568542(__this, ___value0, method) ((  void (*) (Nullable_1_t2443451997 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3197568542_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1720441486_gshared (Nullable_1_t2443451997 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1720441486(__this, method) ((  bool (*) (Nullable_1_t2443451997 *, const MethodInfo*))Nullable_1_get_HasValue_m1720441486_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2672204261_gshared (Nullable_1_t2443451997 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2672204261(__this, method) ((  int32_t (*) (Nullable_1_t2443451997 *, const MethodInfo*))Nullable_1_get_Value_m2672204261_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m500131251_gshared (Nullable_1_t2443451997 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m500131251(__this, ___other0, method) ((  bool (*) (Nullable_1_t2443451997 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m500131251_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1475931692_gshared (Nullable_1_t2443451997 * __this, Nullable_1_t2443451997  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1475931692(__this, ___other0, method) ((  bool (*) (Nullable_1_t2443451997 *, Nullable_1_t2443451997 , const MethodInfo*))Nullable_1_Equals_m1475931692_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3077472523_gshared (Nullable_1_t2443451997 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3077472523(__this, method) ((  int32_t (*) (Nullable_1_t2443451997 *, const MethodInfo*))Nullable_1_GetHashCode_m3077472523_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1688987633_gshared (Nullable_1_t2443451997 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1688987633(__this, method) ((  int32_t (*) (Nullable_1_t2443451997 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1688987633_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1178251294_gshared (Nullable_1_t2443451997 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1178251294(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2443451997 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1178251294_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.TypeNameHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1215373229_gshared (Nullable_1_t2443451997 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1215373229(__this, method) ((  String_t* (*) (Nullable_1_t2443451997 *, const MethodInfo*))Nullable_1_ToString_m1215373229_gshared)(__this, method)
