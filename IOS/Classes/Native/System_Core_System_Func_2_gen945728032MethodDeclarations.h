﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2253398629MethodDeclarations.h"

// System.Void System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m597473397(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t945728032 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3902349173_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::Invoke(T)
#define Func_2_Invoke_m139573260(__this, ___arg10, method) ((  Type_t * (*) (Func_2_t945728032 *, TypeNameKey_t2971844791 , const MethodInfo*))Func_2_Invoke_m859000273_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m996663359(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t945728032 *, TypeNameKey_t2971844791 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m746175620_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2945076040(__this, ___result0, method) ((  Type_t * (*) (Func_2_t945728032 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m634417827_gshared)(__this, ___result0, method)
