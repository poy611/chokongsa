﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m822152535(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3451124703 *, Type_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m876371164_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::get_Key()
#define KeyValuePair_2_get_Key_m3457650865(__this, method) ((  Type_t * (*) (KeyValuePair_2_t3451124703 *, const MethodInfo*))KeyValuePair_2_get_Key_m1333639052_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2767393394(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3451124703 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1955292109_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::get_Value()
#define KeyValuePair_2_get_Value_m1253740849(__this, method) ((  int32_t (*) (KeyValuePair_2_t3451124703 *, const MethodInfo*))KeyValuePair_2_get_Value_m3015219660_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3954957682(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3451124703 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2848328013_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,Newtonsoft.Json.ReadType>::ToString()
#define KeyValuePair_2_ToString_m2813514480(__this, method) ((  String_t* (*) (KeyValuePair_2_t3451124703 *, const MethodInfo*))KeyValuePair_2_ToString_m1519115317_gshared)(__this, method)
