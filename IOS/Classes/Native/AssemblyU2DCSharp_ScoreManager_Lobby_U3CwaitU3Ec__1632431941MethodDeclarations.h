﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Lobby/<wait>c__Iterator20
struct U3CwaitU3Ec__Iterator20_t1632431941;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Lobby/<wait>c__Iterator20::.ctor()
extern "C"  void U3CwaitU3Ec__Iterator20__ctor_m3400299254 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<wait>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2750974182 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<wait>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2838829178 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Lobby/<wait>c__Iterator20::MoveNext()
extern "C"  bool U3CwaitU3Ec__Iterator20_MoveNext_m2549231654 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<wait>c__Iterator20::Dispose()
extern "C"  void U3CwaitU3Ec__Iterator20_Dispose_m3410721203 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<wait>c__Iterator20::Reset()
extern "C"  void U3CwaitU3Ec__Iterator20_Reset_m1046732195 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
