﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;
// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct LateBoundReflectionDelegateFactory_t1043944210;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;
// System.Reflection.MethodBase
struct MethodBase_t318515428;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::get_Instance()
extern "C"  ReflectionDelegateFactory_t1590616920 * LateBoundReflectionDelegateFactory_get_Instance_m2346117801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern "C"  ObjectConstructor_1_t2948332186 * LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m4251432592 (LateBoundReflectionDelegateFactory_t1043944210 * __this, MethodBase_t318515428 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.ctor()
extern "C"  void LateBoundReflectionDelegateFactory__ctor_m331046251 (LateBoundReflectionDelegateFactory_t1043944210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.cctor()
extern "C"  void LateBoundReflectionDelegateFactory__cctor_m1190402978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
