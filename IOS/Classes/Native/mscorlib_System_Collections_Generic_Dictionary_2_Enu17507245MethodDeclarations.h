﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct Dictionary_2_t2995151149;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu17507245.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m769348407_gshared (Enumerator_t17507245 * __this, Dictionary_2_t2995151149 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m769348407(__this, ___dictionary0, method) ((  void (*) (Enumerator_t17507245 *, Dictionary_2_t2995151149 *, const MethodInfo*))Enumerator__ctor_m769348407_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2861743818_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2861743818(__this, method) ((  Il2CppObject * (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2861743818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2311244062_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2311244062(__this, method) ((  void (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2311244062_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m927136423_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m927136423(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m927136423_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1615215654_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1615215654(__this, method) ((  Il2CppObject * (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1615215654_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787486008_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787486008(__this, method) ((  Il2CppObject * (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787486008_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2084814858_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2084814858(__this, method) ((  bool (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_MoveNext_m2084814858_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2893931855  Enumerator_get_Current_m903370982_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m903370982(__this, method) ((  KeyValuePair_2_t2893931855  (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_get_Current_m903370982_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_CurrentKey()
extern "C"  ResolverContractKey_t473801005  Enumerator_get_CurrentKey_m2361619287_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2361619287(__this, method) ((  ResolverContractKey_t473801005  (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_get_CurrentKey_m2361619287_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m435420475_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m435420475(__this, method) ((  Il2CppObject * (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_get_CurrentValue_m435420475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3285388041_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3285388041(__this, method) ((  void (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_Reset_m3285388041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1279930450_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1279930450(__this, method) ((  void (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_VerifyState_m1279930450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m102420282_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m102420282(__this, method) ((  void (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_VerifyCurrent_m102420282_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2980373913_gshared (Enumerator_t17507245 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2980373913(__this, method) ((  void (*) (Enumerator_t17507245 *, const MethodInfo*))Enumerator_Dispose_m2980373913_gshared)(__this, method)
