﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultOfGame/<okNet>c__Iterator19
struct U3CokNetU3Ec__Iterator19_t3669382025;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultOfGame/<okNet>c__Iterator19::.ctor()
extern "C"  void U3CokNetU3Ec__Iterator19__ctor_m95234 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResultOfGame/<okNet>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1758383568 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ResultOfGame/<okNet>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m4034294116 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultOfGame/<okNet>c__Iterator19::MoveNext()
extern "C"  bool U3CokNetU3Ec__Iterator19_MoveNext_m2209615794 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame/<okNet>c__Iterator19::Dispose()
extern "C"  void U3CokNetU3Ec__Iterator19_Dispose_m4284770239 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultOfGame/<okNet>c__Iterator19::Reset()
extern "C"  void U3CokNetU3Ec__Iterator19_Reset_m1941495471 (U3CokNetU3Ec__Iterator19_t3669382025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
