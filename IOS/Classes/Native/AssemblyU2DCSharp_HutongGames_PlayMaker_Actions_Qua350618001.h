﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionRotateTowards
struct  QuaternionRotateTowards_t350618001  : public QuaternionBaseAction_t1884049229
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::fromQuaternion
	FsmQuaternion_t3871136040 * ___fromQuaternion_13;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::toQuaternion
	FsmQuaternion_t3871136040 * ___toQuaternion_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionRotateTowards::maxDegreesDelta
	FsmFloat_t2134102846 * ___maxDegreesDelta_15;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::storeResult
	FsmQuaternion_t3871136040 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_fromQuaternion_13() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t350618001, ___fromQuaternion_13)); }
	inline FsmQuaternion_t3871136040 * get_fromQuaternion_13() const { return ___fromQuaternion_13; }
	inline FsmQuaternion_t3871136040 ** get_address_of_fromQuaternion_13() { return &___fromQuaternion_13; }
	inline void set_fromQuaternion_13(FsmQuaternion_t3871136040 * value)
	{
		___fromQuaternion_13 = value;
		Il2CppCodeGenWriteBarrier(&___fromQuaternion_13, value);
	}

	inline static int32_t get_offset_of_toQuaternion_14() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t350618001, ___toQuaternion_14)); }
	inline FsmQuaternion_t3871136040 * get_toQuaternion_14() const { return ___toQuaternion_14; }
	inline FsmQuaternion_t3871136040 ** get_address_of_toQuaternion_14() { return &___toQuaternion_14; }
	inline void set_toQuaternion_14(FsmQuaternion_t3871136040 * value)
	{
		___toQuaternion_14 = value;
		Il2CppCodeGenWriteBarrier(&___toQuaternion_14, value);
	}

	inline static int32_t get_offset_of_maxDegreesDelta_15() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t350618001, ___maxDegreesDelta_15)); }
	inline FsmFloat_t2134102846 * get_maxDegreesDelta_15() const { return ___maxDegreesDelta_15; }
	inline FsmFloat_t2134102846 ** get_address_of_maxDegreesDelta_15() { return &___maxDegreesDelta_15; }
	inline void set_maxDegreesDelta_15(FsmFloat_t2134102846 * value)
	{
		___maxDegreesDelta_15 = value;
		Il2CppCodeGenWriteBarrier(&___maxDegreesDelta_15, value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t350618001, ___storeResult_16)); }
	inline FsmQuaternion_t3871136040 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmQuaternion_t3871136040 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmQuaternion_t3871136040 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
