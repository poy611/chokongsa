﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindChild
struct FindChild_t514440753;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindChild::.ctor()
extern "C"  void FindChild__ctor_m3234311717 (FindChild_t514440753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::Reset()
extern "C"  void FindChild_Reset_m880744658 (FindChild_t514440753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::OnEnter()
extern "C"  void FindChild_OnEnter_m2303933628 (FindChild_t514440753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::DoFindChild()
extern "C"  void FindChild_DoFindChild_m1885490779 (FindChild_t514440753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
