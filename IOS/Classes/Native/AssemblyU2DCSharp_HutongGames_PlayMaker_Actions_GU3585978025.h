﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutSpace
struct  GUILayoutSpace_t3585978025  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutSpace::space
	FsmFloat_t2134102846 * ___space_11;

public:
	inline static int32_t get_offset_of_space_11() { return static_cast<int32_t>(offsetof(GUILayoutSpace_t3585978025, ___space_11)); }
	inline FsmFloat_t2134102846 * get_space_11() const { return ___space_11; }
	inline FsmFloat_t2134102846 ** get_address_of_space_11() { return &___space_11; }
	inline void set_space_11(FsmFloat_t2134102846 * value)
	{
		___space_11 = value;
		Il2CppCodeGenWriteBarrier(&___space_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
