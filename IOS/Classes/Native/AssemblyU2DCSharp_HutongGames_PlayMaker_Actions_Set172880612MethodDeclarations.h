﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmQuaternion
struct SetFsmQuaternion_t172880612;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::.ctor()
extern "C"  void SetFsmQuaternion__ctor_m1386475522 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::Reset()
extern "C"  void SetFsmQuaternion_Reset_m3327875759 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnEnter()
extern "C"  void SetFsmQuaternion_OnEnter_m354843481 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::DoSetFsmQuaternion()
extern "C"  void SetFsmQuaternion_DoSetFsmQuaternion_m3067159401 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnUpdate()
extern "C"  void SetFsmQuaternion_OnUpdate_m1543772682 (SetFsmQuaternion_t172880612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
