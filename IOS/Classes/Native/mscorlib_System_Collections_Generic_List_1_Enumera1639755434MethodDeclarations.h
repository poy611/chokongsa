﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3218047177(__this, ___l0, method) ((  void (*) (Enumerator_t1639755434 *, List_1_t1620082664 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1428322793(__this, method) ((  void (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2596460373(__this, method) ((  Il2CppObject * (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::Dispose()
#define Enumerator_Dispose_m1695420654(__this, method) ((  void (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::VerifyState()
#define Enumerator_VerifyState_m2245241127(__this, method) ((  void (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::MoveNext()
#define Enumerator_MoveNext_m1469574282(__this, method) ((  bool (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmOwnerDefault>::get_Current()
#define Enumerator_get_Current_m2298938950(__this, method) ((  FsmOwnerDefault_t251897112 * (*) (Enumerator_t1639755434 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
