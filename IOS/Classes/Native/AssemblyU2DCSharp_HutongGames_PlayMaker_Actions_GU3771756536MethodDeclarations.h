﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutIntLabel
struct GUILayoutIntLabel_t3771756536;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::.ctor()
extern "C"  void GUILayoutIntLabel__ctor_m1968022590 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::Reset()
extern "C"  void GUILayoutIntLabel_Reset_m3909422827 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::OnGUI()
extern "C"  void GUILayoutIntLabel_OnGUI_m1463421240 (GUILayoutIntLabel_t3771756536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
