﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t1240414705;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>
struct IEnumerable_1_t2485521619;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t3479575958;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C"  void FetchAllResponse__ctor_m3458740730 (FetchAllResponse_t1240414705 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::ResponseStatus()
extern "C"  int32_t FetchAllResponse_ResponseStatus_m115698676 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::RequestSucceeded()
extern "C"  bool FetchAllResponse_RequestSucceeded_m93376712 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata> GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::Data()
extern "C"  Il2CppObject* FetchAllResponse_Data_m3870451963 (FetchAllResponse_t1240414705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchAllResponse_CallDispose_m2705670902 (FetchAllResponse_t1240414705 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C"  FetchAllResponse_t1240414705 * FetchAllResponse_FromPointer_m1786032583 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::<Data>m__D2(System.UIntPtr)
extern "C"  NativeSnapshotMetadata_t3479575958 * FetchAllResponse_U3CDataU3Em__D2_m621860908 (FetchAllResponse_t1240414705 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
