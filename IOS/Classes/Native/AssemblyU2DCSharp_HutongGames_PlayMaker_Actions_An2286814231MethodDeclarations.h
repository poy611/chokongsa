﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateVector3
struct AnimateVector3_t2286814231;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::.ctor()
extern "C"  void AnimateVector3__ctor_m4284657199 (AnimateVector3_t2286814231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::Reset()
extern "C"  void AnimateVector3_Reset_m1931090140 (AnimateVector3_t2286814231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnEnter()
extern "C"  void AnimateVector3_OnEnter_m2368627270 (AnimateVector3_t2286814231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::UpdateVariableValue()
extern "C"  void AnimateVector3_UpdateVariableValue_m699338073 (AnimateVector3_t2286814231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnUpdate()
extern "C"  void AnimateVector3_OnUpdate_m3841527997 (AnimateVector3_t2286814231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
