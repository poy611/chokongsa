﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LookandseeIt
struct LookandseeIt_t2227248422;

#include "codegen/il2cpp-codegen.h"

// System.Void LookandseeIt::.ctor()
extern "C"  void LookandseeIt__ctor_m1625178165 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::Start()
extern "C"  void LookandseeIt_Start_m572315957 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::backButton()
extern "C"  void LookandseeIt_backButton_m3698206856 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::prev()
extern "C"  void LookandseeIt_prev_m1124075458 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::next()
extern "C"  void LookandseeIt_next_m1055375490 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::deskit()
extern "C"  void LookandseeIt_deskit_m205882195 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookandseeIt::unableButton()
extern "C"  void LookandseeIt_unableButton_m3521448852 (LookandseeIt_t2227248422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
