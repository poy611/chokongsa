﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge672607302.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger95600550.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge849715470.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3843052064.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect2840182460.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2104732159.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou2511441271.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1322796528.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEven384979233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2820857440.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputMod15847059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3771003169.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2039702646.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3613479957.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp4082623702.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1096194655.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1573608962.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputM894675487.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2327671059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRa935388869.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRay1170055767.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT678919098.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color4283662044.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2711705593.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatT493535356.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers115197445.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2796375743.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu3118111730.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2847075725.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry192658922.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock508458230.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls4097465341.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso2719315882.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte3215807551.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData185687546.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL1191310776.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEven902151918.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD1671035447.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec_518566703.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData704020325.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker340588230.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster911782554.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Blo1890318721.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry4169894699.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type3063828369.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod2255824731.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont4288402881.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical3958535251.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin90327434644.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin1801560531980.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin3601560533840.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField609046876.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy2662964855.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType1602890312.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterV737650598.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType2016592042.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEve3081690246.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeE2697516943.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState2313773259.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidat3952708057.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretB1169304295.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseD3777573361.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask8826680.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3186046376.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull2290505109.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities3879688356.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc8834360.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1108456480.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode356329147.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage821930207.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D3357079374.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (EventSystem_t2276120119), -1, sizeof(EventSystem_t2276120119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2510[10] = 
{
	EventSystem_t2276120119::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t2276120119::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t2276120119::get_offset_of_m_FirstSelected_4(),
	EventSystem_t2276120119::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t2276120119::get_offset_of_m_DragThreshold_6(),
	EventSystem_t2276120119::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t2276120119::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t2276120119::get_offset_of_m_DummyData_9(),
	EventSystem_t2276120119_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t2276120119_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (EventTrigger_t672607302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[2] = 
{
	EventTrigger_t672607302::get_offset_of_m_Delegates_2(),
	EventTrigger_t672607302::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (TriggerEvent_t95600550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (Entry_t849715470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	Entry_t849715470::get_offset_of_eventID_0(),
	Entry_t849715470::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (EventTriggerType_t3843052064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2514[18] = 
{
	EventTriggerType_t3843052064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (ExecuteEvents_t2704060668), -1, sizeof(ExecuteEvents_t2704060668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2515[20] = 
{
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (MoveDirection_t2840182460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2517[6] = 
{
	MoveDirection_t2840182460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (RaycasterManager_t2104732159), -1, sizeof(RaycasterManager_t2104732159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2518[1] = 
{
	RaycasterManager_t2104732159_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (RaycastResult_t3762661364)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[10] = 
{
	RaycastResult_t3762661364::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (UIBehaviour_t2511441271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (AxisEventData_t3355659985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[2] = 
{
	AxisEventData_t3355659985::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t3355659985::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (AbstractEventData_t1322796528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	AbstractEventData_t1322796528::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (BaseEventData_t2054899105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	BaseEventData_t2054899105::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (PointerEventData_t1848751023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[21] = 
{
	PointerEventData_t1848751023::get_offset_of_m_PointerPress_2(),
	PointerEventData_t1848751023::get_offset_of_hovered_3(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerEnterU3Ek__BackingField_4(),
	PointerEventData_t1848751023::get_offset_of_U3ClastPressU3Ek__BackingField_5(),
	PointerEventData_t1848751023::get_offset_of_U3CrawPointerPressU3Ek__BackingField_6(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerDragU3Ek__BackingField_7(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_t1848751023::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1848751023::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1848751023::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1848751023::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1848751023::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1848751023::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1848751023::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1848751023::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1848751023::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1848751023::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1848751023::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1848751023::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (InputButton_t384979233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[4] = 
{
	InputButton_t384979233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (FramePressState_t2820857440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2526[5] = 
{
	FramePressState_t2820857440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (BaseInputModule_t15847059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[4] = 
{
	BaseInputModule_t15847059::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t15847059::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t15847059::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t15847059::get_offset_of_m_BaseEventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (PointerInputModule_t3771003169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3771003169::get_offset_of_m_PointerData_10(),
	PointerInputModule_t3771003169::get_offset_of_m_MouseState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ButtonState_t2039702646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[2] = 
{
	ButtonState_t2039702646::get_offset_of_m_Button_0(),
	ButtonState_t2039702646::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (MouseState_t3613479957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[1] = 
{
	MouseState_t3613479957::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (MouseButtonEventData_t4082623702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[2] = 
{
	MouseButtonEventData_t4082623702::get_offset_of_buttonState_0(),
	MouseButtonEventData_t4082623702::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (StandaloneInputModule_t1096194655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[12] = 
{
	StandaloneInputModule_t1096194655::get_offset_of_m_PrevActionTime_12(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMoveVector_13(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ConsecutiveMoveCount_14(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMousePosition_15(),
	StandaloneInputModule_t1096194655::get_offset_of_m_MousePosition_16(),
	StandaloneInputModule_t1096194655::get_offset_of_m_HorizontalAxis_17(),
	StandaloneInputModule_t1096194655::get_offset_of_m_VerticalAxis_18(),
	StandaloneInputModule_t1096194655::get_offset_of_m_SubmitButton_19(),
	StandaloneInputModule_t1096194655::get_offset_of_m_CancelButton_20(),
	StandaloneInputModule_t1096194655::get_offset_of_m_InputActionsPerSecond_21(),
	StandaloneInputModule_t1096194655::get_offset_of_m_RepeatDelay_22(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ForceModuleActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (InputMode_t1573608962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2533[3] = 
{
	InputMode_t1573608962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (TouchInputModule_t894675487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[3] = 
{
	TouchInputModule_t894675487::get_offset_of_m_LastMousePosition_12(),
	TouchInputModule_t894675487::get_offset_of_m_MousePosition_13(),
	TouchInputModule_t894675487::get_offset_of_m_ForceModuleActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (BaseRaycaster_t2327671059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (Physics2DRaycaster_t935388869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (PhysicsRaycaster_t1170055767), -1, sizeof(PhysicsRaycaster_t1170055767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	0,
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t1170055767_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (ColorTween_t723277650)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[6] = 
{
	ColorTween_t723277650::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (ColorTweenMode_t678919098)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[4] = 
{
	ColorTweenMode_t678919098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (ColorTweenCallback_t4283662044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (FloatTween_t2711705593)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[5] = 
{
	FloatTween_t2711705593::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (FloatTweenCallback_t493535356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (AnimationTriggers_t115197445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t115197445::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t115197445::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t115197445::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t115197445::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (Button_t3896396478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[1] = 
{
	Button_t3896396478::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (ButtonClickedEvent_t2796375743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (U3COnFinishSubmitU3Ec__Iterator1_t3118111730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[5] = 
{
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CelapsedTimeU3E__1_1(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U24PC_2(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (CanvasUpdate_t2847075725)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2550[7] = 
{
	CanvasUpdate_t2847075725::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (CanvasUpdateRegistry_t192658922), -1, sizeof(CanvasUpdateRegistry_t192658922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[6] = 
{
	CanvasUpdateRegistry_t192658922_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t192658922_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (ColorBlock_t508458230)+ sizeof (Il2CppObject), sizeof(ColorBlock_t508458230_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2553[6] = 
{
	ColorBlock_t508458230::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (DefaultControls_t4097465341), -1, sizeof(DefaultControls_t4097465341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2554[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_TextColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (Resources_t2719315882)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[7] = 
{
	Resources_t2719315882::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2719315882::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (Dropdown_t4201779933), -1, sizeof(Dropdown_t4201779933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2556[14] = 
{
	Dropdown_t4201779933::get_offset_of_m_Template_16(),
	Dropdown_t4201779933::get_offset_of_m_CaptionText_17(),
	Dropdown_t4201779933::get_offset_of_m_CaptionImage_18(),
	Dropdown_t4201779933::get_offset_of_m_ItemText_19(),
	Dropdown_t4201779933::get_offset_of_m_ItemImage_20(),
	Dropdown_t4201779933::get_offset_of_m_Value_21(),
	Dropdown_t4201779933::get_offset_of_m_Options_22(),
	Dropdown_t4201779933::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t4201779933::get_offset_of_m_Dropdown_24(),
	Dropdown_t4201779933::get_offset_of_m_Blocker_25(),
	Dropdown_t4201779933::get_offset_of_m_Items_26(),
	Dropdown_t4201779933::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t4201779933::get_offset_of_validTemplate_28(),
	Dropdown_t4201779933_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (DropdownItem_t3215807551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[4] = 
{
	DropdownItem_t3215807551::get_offset_of_m_Text_2(),
	DropdownItem_t3215807551::get_offset_of_m_Image_3(),
	DropdownItem_t3215807551::get_offset_of_m_RectTransform_4(),
	DropdownItem_t3215807551::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (OptionData_t185687546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[2] = 
{
	OptionData_t185687546::get_offset_of_m_Text_0(),
	OptionData_t185687546::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (OptionDataList_t1191310776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[1] = 
{
	OptionDataList_t1191310776::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (DropdownEvent_t902151918), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[6] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_U3CiU3E__0_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_U24PC_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_U24current_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_U3CU24U3Edelay_4(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t1671035447::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (U3CShowU3Ec__AnonStorey6_t518566703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	U3CShowU3Ec__AnonStorey6_t518566703::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey6_t518566703::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (FontData_t704020325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[12] = 
{
	FontData_t704020325::get_offset_of_m_Font_0(),
	FontData_t704020325::get_offset_of_m_FontSize_1(),
	FontData_t704020325::get_offset_of_m_FontStyle_2(),
	FontData_t704020325::get_offset_of_m_BestFit_3(),
	FontData_t704020325::get_offset_of_m_MinSize_4(),
	FontData_t704020325::get_offset_of_m_MaxSize_5(),
	FontData_t704020325::get_offset_of_m_Alignment_6(),
	FontData_t704020325::get_offset_of_m_AlignByGeometry_7(),
	FontData_t704020325::get_offset_of_m_RichText_8(),
	FontData_t704020325::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t704020325::get_offset_of_m_VerticalOverflow_10(),
	FontData_t704020325::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (FontUpdateTracker_t340588230), -1, sizeof(FontUpdateTracker_t340588230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2564[1] = 
{
	FontUpdateTracker_t340588230_StaticFields::get_offset_of_m_Tracked_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (Graphic_t836799438), -1, sizeof(Graphic_t836799438_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2565[17] = 
{
	Graphic_t836799438_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t836799438_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t836799438::get_offset_of_m_Material_4(),
	Graphic_t836799438::get_offset_of_m_Color_5(),
	Graphic_t836799438::get_offset_of_m_RaycastTarget_6(),
	Graphic_t836799438::get_offset_of_m_RectTransform_7(),
	Graphic_t836799438::get_offset_of_m_CanvasRender_8(),
	Graphic_t836799438::get_offset_of_m_Canvas_9(),
	Graphic_t836799438::get_offset_of_m_VertsDirty_10(),
	Graphic_t836799438::get_offset_of_m_MaterialDirty_11(),
	Graphic_t836799438::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t836799438::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t836799438::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t836799438_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t836799438_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t836799438::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t836799438::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (GraphicRaycaster_t911782554), -1, sizeof(GraphicRaycaster_t911782554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2566[8] = 
{
	0,
	GraphicRaycaster_t911782554::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t911782554::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t911782554::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t911782554::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t911782554::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t911782554_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t911782554_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (BlockingObjects_t1890318721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[5] = 
{
	BlockingObjects_t1890318721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (GraphicRegistry_t4169894699), -1, sizeof(GraphicRegistry_t4169894699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[3] = 
{
	GraphicRegistry_t4169894699_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t4169894699::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t4169894699_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (Image_t538875265), -1, sizeof(Image_t538875265_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2570[15] = 
{
	Image_t538875265_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t538875265::get_offset_of_m_Sprite_29(),
	Image_t538875265::get_offset_of_m_OverrideSprite_30(),
	Image_t538875265::get_offset_of_m_Type_31(),
	Image_t538875265::get_offset_of_m_PreserveAspect_32(),
	Image_t538875265::get_offset_of_m_FillCenter_33(),
	Image_t538875265::get_offset_of_m_FillMethod_34(),
	Image_t538875265::get_offset_of_m_FillAmount_35(),
	Image_t538875265::get_offset_of_m_FillClockwise_36(),
	Image_t538875265::get_offset_of_m_FillOrigin_37(),
	Image_t538875265::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t538875265_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t538875265_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t538875265_StaticFields::get_offset_of_s_Xy_41(),
	Image_t538875265_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Type_t3063828369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2571[5] = 
{
	Type_t3063828369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (FillMethod_t2255824731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2572[6] = 
{
	FillMethod_t2255824731::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (OriginHorizontal_t4288402881)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	OriginHorizontal_t4288402881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (OriginVertical_t3958535251)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	OriginVertical_t3958535251::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (Origin90_t327434644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[5] = 
{
	Origin90_t327434644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Origin180_t1560531980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2576[5] = 
{
	Origin180_t1560531980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Origin360_t1560533840)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2577[5] = 
{
	Origin360_t1560533840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (InputField_t609046876), -1, sizeof(InputField_t609046876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2580[48] = 
{
	0,
	0,
	0,
	InputField_t609046876::get_offset_of_m_Keyboard_19(),
	InputField_t609046876_StaticFields::get_offset_of_kSeparators_20(),
	InputField_t609046876::get_offset_of_m_TextComponent_21(),
	InputField_t609046876::get_offset_of_m_Placeholder_22(),
	InputField_t609046876::get_offset_of_m_ContentType_23(),
	InputField_t609046876::get_offset_of_m_InputType_24(),
	InputField_t609046876::get_offset_of_m_AsteriskChar_25(),
	InputField_t609046876::get_offset_of_m_KeyboardType_26(),
	InputField_t609046876::get_offset_of_m_LineType_27(),
	InputField_t609046876::get_offset_of_m_HideMobileInput_28(),
	InputField_t609046876::get_offset_of_m_CharacterValidation_29(),
	InputField_t609046876::get_offset_of_m_CharacterLimit_30(),
	InputField_t609046876::get_offset_of_m_OnEndEdit_31(),
	InputField_t609046876::get_offset_of_m_OnValueChanged_32(),
	InputField_t609046876::get_offset_of_m_OnValidateInput_33(),
	InputField_t609046876::get_offset_of_m_CaretColor_34(),
	InputField_t609046876::get_offset_of_m_CustomCaretColor_35(),
	InputField_t609046876::get_offset_of_m_SelectionColor_36(),
	InputField_t609046876::get_offset_of_m_Text_37(),
	InputField_t609046876::get_offset_of_m_CaretBlinkRate_38(),
	InputField_t609046876::get_offset_of_m_CaretWidth_39(),
	InputField_t609046876::get_offset_of_m_ReadOnly_40(),
	InputField_t609046876::get_offset_of_m_CaretPosition_41(),
	InputField_t609046876::get_offset_of_m_CaretSelectPosition_42(),
	InputField_t609046876::get_offset_of_caretRectTrans_43(),
	InputField_t609046876::get_offset_of_m_CursorVerts_44(),
	InputField_t609046876::get_offset_of_m_InputTextCache_45(),
	InputField_t609046876::get_offset_of_m_CachedInputRenderer_46(),
	InputField_t609046876::get_offset_of_m_PreventFontCallback_47(),
	InputField_t609046876::get_offset_of_m_Mesh_48(),
	InputField_t609046876::get_offset_of_m_AllowInput_49(),
	InputField_t609046876::get_offset_of_m_ShouldActivateNextUpdate_50(),
	InputField_t609046876::get_offset_of_m_UpdateDrag_51(),
	InputField_t609046876::get_offset_of_m_DragPositionOutOfBounds_52(),
	InputField_t609046876::get_offset_of_m_CaretVisible_53(),
	InputField_t609046876::get_offset_of_m_BlinkCoroutine_54(),
	InputField_t609046876::get_offset_of_m_BlinkStartTime_55(),
	InputField_t609046876::get_offset_of_m_DrawStart_56(),
	InputField_t609046876::get_offset_of_m_DrawEnd_57(),
	InputField_t609046876::get_offset_of_m_DragCoroutine_58(),
	InputField_t609046876::get_offset_of_m_OriginalText_59(),
	InputField_t609046876::get_offset_of_m_WasCanceled_60(),
	InputField_t609046876::get_offset_of_m_HasDoneFocusTransition_61(),
	InputField_t609046876::get_offset_of_m_ProcessingEvent_62(),
	InputField_t609046876_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_63(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (ContentType_t2662964855)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2581[11] = 
{
	ContentType_t2662964855::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (InputType_t1602890312)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[4] = 
{
	InputType_t1602890312::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (CharacterValidation_t737650598)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2583[7] = 
{
	CharacterValidation_t737650598::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (LineType_t2016592042)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2584[4] = 
{
	LineType_t2016592042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (SubmitEvent_t3081690246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (OnChangeEvent_t2697516943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (EditState_t2313773259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2587[3] = 
{
	EditState_t2313773259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (OnValidateInput_t3952708057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (U3CCaretBlinkU3Ec__Iterator3_t1169304295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[5] = 
{
	U3CCaretBlinkU3Ec__Iterator3_t1169304295::get_offset_of_U3CblinkPeriodU3E__0_0(),
	U3CCaretBlinkU3Ec__Iterator3_t1169304295::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator3_t1169304295::get_offset_of_U24PC_2(),
	U3CCaretBlinkU3Ec__Iterator3_t1169304295::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator3_t1169304295::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U3CdelayU3E__2_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U24PC_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U3CU24U3EeventData_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t3777573361::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Mask_t8826680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[5] = 
{
	Mask_t8826680::get_offset_of_m_RectTransform_2(),
	Mask_t8826680::get_offset_of_m_ShowMaskGraphic_3(),
	Mask_t8826680::get_offset_of_m_Graphic_4(),
	Mask_t8826680::get_offset_of_m_MaskMaterial_5(),
	Mask_t8826680::get_offset_of_m_UnmaskMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (MaskableGraphic_t3186046376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[9] = 
{
	MaskableGraphic_t3186046376::get_offset_of_m_ShouldRecalculateStencil_19(),
	MaskableGraphic_t3186046376::get_offset_of_m_MaskMaterial_20(),
	MaskableGraphic_t3186046376::get_offset_of_m_ParentMask_21(),
	MaskableGraphic_t3186046376::get_offset_of_m_Maskable_22(),
	MaskableGraphic_t3186046376::get_offset_of_m_IncludeForMasking_23(),
	MaskableGraphic_t3186046376::get_offset_of_m_OnCullStateChanged_24(),
	MaskableGraphic_t3186046376::get_offset_of_m_ShouldRecalculate_25(),
	MaskableGraphic_t3186046376::get_offset_of_m_StencilValue_26(),
	MaskableGraphic_t3186046376::get_offset_of_m_Corners_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (CullStateChangedEvent_t2290505109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (MaskUtilities_t3879688356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (Misc_t8834360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (Navigation_t1108456480)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[5] = 
{
	Navigation_t1108456480::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1108456480::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (Mode_t356329147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[6] = 
{
	Mode_t356329147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (RawImage_t821930207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[2] = 
{
	RawImage_t821930207::get_offset_of_m_Texture_28(),
	RawImage_t821930207::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (RectMask2D_t3357079374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[8] = 
{
	RectMask2D_t3357079374::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t3357079374::get_offset_of_m_RectTransform_3(),
	RectMask2D_t3357079374::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t3357079374::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t3357079374::get_offset_of_m_Clippers_6(),
	RectMask2D_t3357079374::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t3357079374::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t3357079374::get_offset_of_m_ForceClip_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
