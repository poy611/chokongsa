﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour759180893.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645.h"
#include "UnityEngine_UnityEngine_TrackedReference2089686725.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2574965573.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerM3900400668.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache1171347191.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall1559630662.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall1277370263.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3502354656.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall2972625667.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup4062675100.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList3597236437.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent1266085011.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime3698499994.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4222409188.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3749225885.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1017505774.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt1844602099.h"
#include "UnityEngine_UnityEngine_Logger2997509588.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "JsonFx_Json_U3CModuleU3E86524790.h"
#include "JsonFx_Json_JsonFx_Json_JsonSpecifiedPropertyAttri3902267749.h"
#include "JsonFx_Json_JsonFx_Json_JsonReader3434342065.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriter3589747297.h"
#include "JsonFx_Json_JsonFx_Json_JsonToken37183731.h"
#include "JsonFx_Json_JsonFx_Json_JsonSerializationException3458665517.h"
#include "JsonFx_Json_JsonFx_Json_JsonDeserializationExcepti1696613230.h"
#include "JsonFx_Json_JsonFx_Json_JsonTypeCoercionException4269543025.h"
#include "JsonFx_Json_JsonFx_Json_JsonReaderSettings24058356.h"
#include "JsonFx_Json_JsonFx_Json_JsonWriterSettings323204516.h"
#include "JsonFx_Json_JsonFx_Json_JsonNameAttribute1794268491.h"
#include "JsonFx_Json_JsonFx_Json_TypeCoercionUtility700629014.h"
#include "JsonFx_Json_JsonFx_Json_JsonIgnoreAttribute2459039844.h"
#include "PlayMaker_U3CModuleU3E86524790.h"
#include "PlayMaker_HutongGames_Extensions_RectExtensions1980939226.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions3620936505.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"
#include "PlayMaker_HutongGames_Utility_ColorUtils3685901928.h"
#include "PlayMaker_HutongGames_Utility_StringUtils888966490.h"
#include "PlayMaker_HutongGames_PlayMaker_CollisionType1355765302.h"
#include "PlayMaker_HutongGames_PlayMaker_TriggerType3348422332.h"
#include "PlayMaker_HutongGames_PlayMaker_Collision2DType2043425288.h"
#include "PlayMaker_HutongGames_PlayMaker_Trigger2DType1431417102.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseEventType1876570897.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategory2692830870.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHint1985716317.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseButton82682337.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport662142796.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport_U3CU33035583220.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionTarget715758505.h"
#include "PlayMaker_HutongGames_PlayMaker_NoActionTargetsAtt2678551411.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategoryAttri321770322.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionSection3062711417.h"
#include "PlayMaker_HutongGames_PlayMaker_ArrayEditorAttribu3496841304.h"
#include "PlayMaker_HutongGames_PlayMaker_CheckForComponentA4040605154.h"
#include "PlayMaker_HutongGames_PlayMaker_CompoundArrayAttri2338634768.h"
#include "PlayMaker_HutongGames_PlayMaker_EventTargetAttribut259554899.h"
#include "PlayMaker_HutongGames_PlayMaker_HasFloatSliderAttr3315538467.h"
#include "PlayMaker_HutongGames_PlayMaker_HelpUrlAttribute2247792144.h"
#include "PlayMaker_HutongGames_PlayMaker_HideTypeFilter4178726774.h"
#include "PlayMaker_HutongGames_PlayMaker_MatchElementTypeAt1656137461.h"
#include "PlayMaker_HutongGames_PlayMaker_MatchFieldTypeAttr3836515319.h"
#include "PlayMaker_HutongGames_PlayMaker_NoteAttribute294051700.h"
#include "PlayMaker_HutongGames_PlayMaker_ObjectTypeAttribut1425126701.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeAttrib2776351760.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeFilter3331841584.h"
#include "PlayMaker_HutongGames_PlayMaker_RequiredFieldAttrib353655171.h"
#include "PlayMaker_HutongGames_PlayMaker_TitleAttribute3939375206.h"
#include "PlayMaker_HutongGames_PlayMaker_TooltipAttribute177137691.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHintAttribute149723499.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (StateMachineBehaviour_t759180893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (SystemClock_t4036018645), -1, sizeof(SystemClock_t4036018645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	SystemClock_t4036018645_StaticFields::get_offset_of_s_Epoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (TrackedReference_t2089686725), sizeof(TrackedReference_t2089686725_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	TrackedReference_t2089686725::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (UnityAPICompatibilityVersionAttribute_t2574965573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	UnityAPICompatibilityVersionAttribute_t2574965573::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (PersistentListenerMode_t3900400668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2304[8] = 
{
	PersistentListenerMode_t3900400668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ArgumentCache_t1171347191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[6] = 
{
	ArgumentCache_t1171347191::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t1171347191::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t1171347191::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t1171347191::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t1171347191::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t1171347191::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (BaseInvokableCall_t1559630662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (InvokableCall_t1277370263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[1] = 
{
	InvokableCall_t1277370263::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (UnityEventCallState_t3502354656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[4] = 
{
	UnityEventCallState_t3502354656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (PersistentCall_t2972625667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[5] = 
{
	PersistentCall_t2972625667::get_offset_of_m_Target_0(),
	PersistentCall_t2972625667::get_offset_of_m_MethodName_1(),
	PersistentCall_t2972625667::get_offset_of_m_Mode_2(),
	PersistentCall_t2972625667::get_offset_of_m_Arguments_3(),
	PersistentCall_t2972625667::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (PersistentCallGroup_t4062675100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[1] = 
{
	PersistentCallGroup_t4062675100::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (InvokableCallList_t3597236437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[4] = 
{
	InvokableCallList_t3597236437::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t3597236437::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t3597236437::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t3597236437::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (UnityEventBase_t1020378628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	UnityEventBase_t1020378628::get_offset_of_m_Calls_0(),
	UnityEventBase_t1020378628::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t1020378628::get_offset_of_m_TypeName_2(),
	UnityEventBase_t1020378628::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (UnityEvent_t1266085011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[1] = 
{
	UnityEvent_t1266085011::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (WaitForSecondsRealtime_t3698499994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[1] = 
{
	WaitForSecondsRealtime_t3698499994::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (AnimationPlayableUtilities_t4222409188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (FrameData_t3749225885)+ sizeof (Il2CppObject), sizeof(FrameData_t3749225885_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2325[4] = 
{
	FrameData_t3749225885::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3749225885::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (DefaultValueAttribute_t1017505774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[1] = 
{
	DefaultValueAttribute_t1017505774::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (ExcludeFromDocsAttribute_t1844602099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (Logger_t2997509588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[3] = 
{
	Logger_t2997509588::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t2997509588::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t2997509588::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (UsedByNativeCodeAttribute_t3197444790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (RequiredByNativeCodeAttribute_t3165457172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (FormerlySerializedAsAttribute_t2216353654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	FormerlySerializedAsAttribute_t2216353654::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (TypeInferenceRules_t2889237774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[5] = 
{
	TypeInferenceRules_t2889237774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (TypeInferenceRuleAttribute_t1657757719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[1] = 
{
	TypeInferenceRuleAttribute_t1657757719::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (GenericStack_t931085639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (NetFxCoreExtensions_t2541795172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (UnityAction_t594794173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (U3CModuleU3E_t86524798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (JsonSpecifiedPropertyAttribute_t3902267749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[1] = 
{
	JsonSpecifiedPropertyAttribute_t3902267749::get_offset_of_specifiedProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (JsonReader_t3434342065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[4] = 
{
	JsonReader_t3434342065::get_offset_of_Settings_0(),
	JsonReader_t3434342065::get_offset_of_Source_1(),
	JsonReader_t3434342065::get_offset_of_SourceLength_2(),
	JsonReader_t3434342065::get_offset_of_index_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (JsonWriter_t3589747297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[3] = 
{
	JsonWriter_t3589747297::get_offset_of_Writer_0(),
	JsonWriter_t3589747297::get_offset_of_settings_1(),
	JsonWriter_t3589747297::get_offset_of_depth_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (JsonToken_t37183731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[18] = 
{
	JsonToken_t37183731::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (JsonSerializationException_t3458665517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (JsonDeserializationException_t1696613230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[1] = 
{
	JsonDeserializationException_t1696613230::get_offset_of_index_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (JsonTypeCoercionException_t4269543025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (JsonReaderSettings_t24058356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[3] = 
{
	JsonReaderSettings_t24058356::get_offset_of_Coercion_0(),
	JsonReaderSettings_t24058356::get_offset_of_allowUnquotedObjectKeys_1(),
	JsonReaderSettings_t24058356::get_offset_of_typeHintName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (JsonWriterSettings_t323204516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[7] = 
{
	JsonWriterSettings_t323204516::get_offset_of_dateTimeSerializer_0(),
	JsonWriterSettings_t323204516::get_offset_of_maxDepth_1(),
	JsonWriterSettings_t323204516::get_offset_of_newLine_2(),
	JsonWriterSettings_t323204516::get_offset_of_prettyPrint_3(),
	JsonWriterSettings_t323204516::get_offset_of_tab_4(),
	JsonWriterSettings_t323204516::get_offset_of_typeHintName_5(),
	JsonWriterSettings_t323204516::get_offset_of_useXmlSerializationAttributes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (JsonNameAttribute_t1794268491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[1] = 
{
	JsonNameAttribute_t1794268491::get_offset_of_jsonName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (TypeCoercionUtility_t700629014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	TypeCoercionUtility_t700629014::get_offset_of_memberMapCache_0(),
	TypeCoercionUtility_t700629014::get_offset_of_allowNullValueTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (JsonIgnoreAttribute_t2459039844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (U3CModuleU3E_t86524799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (RectExtensions_t1980939226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (TextureExtensions_t3620936505), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (Point_t1271214244)+ sizeof (Il2CppObject), sizeof(Point_t1271214244_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[2] = 
{
	Point_t1271214244::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Point_t1271214244::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (ColorUtils_t3685901928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (StringUtils_t888966490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (CollisionType_t1355765302)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2366[6] = 
{
	CollisionType_t1355765302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (TriggerType_t3348422332)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2367[4] = 
{
	TriggerType_t3348422332::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (Collision2DType_t2043425288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2368[5] = 
{
	Collision2DType_t2043425288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (Trigger2DType_t1431417102)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	Trigger2DType_t1431417102::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (InterpolationType_t930981288)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[3] = 
{
	InterpolationType_t930981288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (MouseEventType_t1876570897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[7] = 
{
	MouseEventType_t1876570897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ActionCategory_t2692830870)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2372[54] = 
{
	ActionCategory_t2692830870::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (UIHint_t1985716317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2373[38] = 
{
	UIHint_t1985716317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (MouseButton_t82682337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2374[5] = 
{
	MouseButton_t82682337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (LogLevel_t284580066)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	LogLevel_t284580066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ActionHelpers_t1898294297), -1, sizeof(ActionHelpers_t1898294297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2376[5] = 
{
	ActionHelpers_t1898294297_StaticFields::get_offset_of_whiteTexture_0(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickInfo_1(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickRaycastTime_2(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickDistanceUsed_3(),
	ActionHelpers_t1898294297_StaticFields::get_offset_of_mousePickLayerMaskUsed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ActionReport_t662142796), -1, sizeof(ActionReport_t662142796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2377[10] = 
{
	ActionReport_t662142796_StaticFields::get_offset_of_ActionReportList_0(),
	ActionReport_t662142796_StaticFields::get_offset_of_InfoCount_1(),
	ActionReport_t662142796_StaticFields::get_offset_of_ErrorCount_2(),
	ActionReport_t662142796::get_offset_of_fsm_3(),
	ActionReport_t662142796::get_offset_of_state_4(),
	ActionReport_t662142796::get_offset_of_action_5(),
	ActionReport_t662142796::get_offset_of_actionIndex_6(),
	ActionReport_t662142796::get_offset_of_logText_7(),
	ActionReport_t662142796::get_offset_of_isError_8(),
	ActionReport_t662142796::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3CU3Ec__DisplayClass2_t3035583220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	U3CU3Ec__DisplayClass2_t3035583220::get_offset_of_fsm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ActionTarget_t715758505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[3] = 
{
	ActionTarget_t715758505::get_offset_of_objectType_0(),
	ActionTarget_t715758505::get_offset_of_fieldName_1(),
	ActionTarget_t715758505::get_offset_of_allowPrefabs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (NoActionTargetsAttribute_t2678551411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (ActionCategoryAttribute_t321770322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	ActionCategoryAttribute_t321770322::get_offset_of_category_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (ActionSection_t3062711417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[1] = 
{
	ActionSection_t3062711417::get_offset_of_section_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (ArrayEditorAttribute_t3496841304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[6] = 
{
	ArrayEditorAttribute_t3496841304::get_offset_of_variableType_0(),
	ArrayEditorAttribute_t3496841304::get_offset_of_objectType_1(),
	ArrayEditorAttribute_t3496841304::get_offset_of_elementName_2(),
	ArrayEditorAttribute_t3496841304::get_offset_of_fixedSize_3(),
	ArrayEditorAttribute_t3496841304::get_offset_of_maxSize_4(),
	ArrayEditorAttribute_t3496841304::get_offset_of_minSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (CheckForComponentAttribute_t4040605154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[3] = 
{
	CheckForComponentAttribute_t4040605154::get_offset_of_type0_0(),
	CheckForComponentAttribute_t4040605154::get_offset_of_type1_1(),
	CheckForComponentAttribute_t4040605154::get_offset_of_type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (CompoundArrayAttribute_t2338634768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	CompoundArrayAttribute_t2338634768::get_offset_of_name_0(),
	CompoundArrayAttribute_t2338634768::get_offset_of_firstArrayName_1(),
	CompoundArrayAttribute_t2338634768::get_offset_of_secondArrayName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (EventTargetAttribute_t259554899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[1] = 
{
	EventTargetAttribute_t259554899::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (HasFloatSliderAttribute_t3315538467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	HasFloatSliderAttribute_t3315538467::get_offset_of_minValue_0(),
	HasFloatSliderAttribute_t3315538467::get_offset_of_maxValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (HelpUrlAttribute_t2247792144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	HelpUrlAttribute_t2247792144::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (HideTypeFilter_t4178726774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (MatchElementTypeAttribute_t1656137461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	MatchElementTypeAttribute_t1656137461::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (MatchFieldTypeAttribute_t3836515319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[1] = 
{
	MatchFieldTypeAttribute_t3836515319::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (NoteAttribute_t294051700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	NoteAttribute_t294051700::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (ObjectTypeAttribute_t1425126701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	ObjectTypeAttribute_t1425126701::get_offset_of_objectType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (VariableTypeAttribute_t2776351760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[1] = 
{
	VariableTypeAttribute_t2776351760::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (VariableTypeFilter_t3331841584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (RequiredFieldAttribute_t353655171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (TitleAttribute_t3939375206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[1] = 
{
	TitleAttribute_t3939375206::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (TooltipAttribute_t177137691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	TooltipAttribute_t177137691::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (UIHintAttribute_t149723499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[1] = 
{
	UIHintAttribute_t149723499::get_offset_of_hint_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
