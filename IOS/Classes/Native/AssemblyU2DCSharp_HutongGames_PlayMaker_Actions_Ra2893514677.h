﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomBool
struct  RandomBool_t2893514677  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RandomBool::storeResult
	FsmBool_t1075959796 * ___storeResult_11;

public:
	inline static int32_t get_offset_of_storeResult_11() { return static_cast<int32_t>(offsetof(RandomBool_t2893514677, ___storeResult_11)); }
	inline FsmBool_t1075959796 * get_storeResult_11() const { return ___storeResult_11; }
	inline FsmBool_t1075959796 ** get_address_of_storeResult_11() { return &___storeResult_11; }
	inline void set_storeResult_11(FsmBool_t1075959796 * value)
	{
		___storeResult_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
