﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t3001083477;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayRandomSound
struct  PlayRandomSound_t3980109638  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlayRandomSound::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PlayRandomSound::position
	FsmVector3_t533912882 * ___position_12;
	// UnityEngine.AudioClip[] HutongGames.PlayMaker.Actions.PlayRandomSound::audioClips
	AudioClipU5BU5D_t3001083477* ___audioClips_13;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayRandomSound::weights
	FsmFloatU5BU5D_t2945380875* ___weights_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlayRandomSound::volume
	FsmFloat_t2134102846 * ___volume_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3980109638, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3980109638, ___position_12)); }
	inline FsmVector3_t533912882 * get_position_12() const { return ___position_12; }
	inline FsmVector3_t533912882 ** get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(FsmVector3_t533912882 * value)
	{
		___position_12 = value;
		Il2CppCodeGenWriteBarrier(&___position_12, value);
	}

	inline static int32_t get_offset_of_audioClips_13() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3980109638, ___audioClips_13)); }
	inline AudioClipU5BU5D_t3001083477* get_audioClips_13() const { return ___audioClips_13; }
	inline AudioClipU5BU5D_t3001083477** get_address_of_audioClips_13() { return &___audioClips_13; }
	inline void set_audioClips_13(AudioClipU5BU5D_t3001083477* value)
	{
		___audioClips_13 = value;
		Il2CppCodeGenWriteBarrier(&___audioClips_13, value);
	}

	inline static int32_t get_offset_of_weights_14() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3980109638, ___weights_14)); }
	inline FsmFloatU5BU5D_t2945380875* get_weights_14() const { return ___weights_14; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_weights_14() { return &___weights_14; }
	inline void set_weights_14(FsmFloatU5BU5D_t2945380875* value)
	{
		___weights_14 = value;
		Il2CppCodeGenWriteBarrier(&___weights_14, value);
	}

	inline static int32_t get_offset_of_volume_15() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3980109638, ___volume_15)); }
	inline FsmFloat_t2134102846 * get_volume_15() const { return ___volume_15; }
	inline FsmFloat_t2134102846 ** get_address_of_volume_15() { return &___volume_15; }
	inline void set_volume_15(FsmFloat_t2134102846 * value)
	{
		___volume_15 = value;
		Il2CppCodeGenWriteBarrier(&___volume_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
