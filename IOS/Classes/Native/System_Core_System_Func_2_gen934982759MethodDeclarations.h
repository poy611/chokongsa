﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2304636665MethodDeclarations.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1644784025(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t934982759 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2523847628_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>::Invoke(T)
#define Func_2_Invoke_m3604266265(__this, ___arg10, method) ((  CommitResponse_t2801162465 * (*) (Func_2_t934982759 *, IntPtr_t, const MethodInfo*))Func_2_Invoke_m1794082454_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1191007304(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t934982759 *, IntPtr_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2959052997_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2455167067(__this, ___result0, method) ((  CommitResponse_t2801162465 * (*) (Func_2_t934982759 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m558880510_gshared)(__this, ___result0, method)
