﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatRound
struct FloatRound_t1749403130;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatRound::.ctor()
extern "C"  void FloatRound__ctor_m2872846252 (FloatRound_t1749403130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatRound::Reset()
extern "C"  void FloatRound_Reset_m519279193 (FloatRound_t1749403130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatRound::OnEnter()
extern "C"  void FloatRound_OnEnter_m2827972739 (FloatRound_t1749403130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatRound::OnUpdate()
extern "C"  void FloatRound_OnUpdate_m901368352 (FloatRound_t1749403130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatRound::DoFloatRound()
extern "C"  void FloatRound_DoFloatRound_m1411620181 (FloatRound_t1749403130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
