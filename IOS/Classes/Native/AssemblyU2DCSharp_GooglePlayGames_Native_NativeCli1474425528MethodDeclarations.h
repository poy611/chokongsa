﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A
struct U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528;
// GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse
struct FetchServerAuthCodeResponse_t1052060023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_G1052060023.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A::.ctor()
extern "C"  void U3CGetServerAuthCodeU3Ec__AnonStorey4A__ctor_m944914659 (U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A::<>m__1F(GooglePlayGames.Native.PInvoke.GameServices/FetchServerAuthCodeResponse)
extern "C"  void U3CGetServerAuthCodeU3Ec__AnonStorey4A_U3CU3Em__1F_m1020779226 (U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * __this, FetchServerAuthCodeResponse_t1052060023 * ___serverAuthCodeResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
