﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t294375316;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t2043220689;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64
struct  U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64::config
	RealtimeRoomConfig_t294375316 * ___config_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64::<>f__this
	NativeRealtimeMultiplayerClient_t2043220689 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159, ___config_0)); }
	inline RealtimeRoomConfig_t294375316 * get_config_0() const { return ___config_0; }
	inline RealtimeRoomConfig_t294375316 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(RealtimeRoomConfig_t294375316 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier(&___config_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159, ___U3CU3Ef__this_1)); }
	inline NativeRealtimeMultiplayerClient_t2043220689 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline NativeRealtimeMultiplayerClient_t2043220689 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(NativeRealtimeMultiplayerClient_t2043220689 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
