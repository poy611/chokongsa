﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetOwner
struct  GetOwner_t1709396389  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetOwner::storeGameObject
	FsmGameObject_t1697147867 * ___storeGameObject_11;

public:
	inline static int32_t get_offset_of_storeGameObject_11() { return static_cast<int32_t>(offsetof(GetOwner_t1709396389, ___storeGameObject_11)); }
	inline FsmGameObject_t1697147867 * get_storeGameObject_11() const { return ___storeGameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_storeGameObject_11() { return &___storeGameObject_11; }
	inline void set_storeGameObject_11(FsmGameObject_t1697147867 * value)
	{
		___storeGameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeGameObject_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
