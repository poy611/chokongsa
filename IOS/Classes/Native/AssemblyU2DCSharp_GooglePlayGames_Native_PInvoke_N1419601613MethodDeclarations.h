﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativePlayerStats
struct NativePlayerStats_t1419601613;
// GooglePlayGames.BasicApi.PlayerStats
struct PlayerStats_t60064856;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.NativePlayerStats::.ctor(System.IntPtr)
extern "C"  void NativePlayerStats__ctor_m248646333 (NativePlayerStats_t1419601613 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::Valid()
extern "C"  bool NativePlayerStats_Valid_m582999261 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasAverageSessionLength()
extern "C"  bool NativePlayerStats_HasAverageSessionLength_m4221278554 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.PInvoke.NativePlayerStats::AverageSessionLength()
extern "C"  float NativePlayerStats_AverageSessionLength_m1372204664 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasChurnProbability()
extern "C"  bool NativePlayerStats_HasChurnProbability_m7338884 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.PInvoke.NativePlayerStats::ChurnProbability()
extern "C"  float NativePlayerStats_ChurnProbability_m3657551778 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasDaysSinceLastPlayed()
extern "C"  bool NativePlayerStats_HasDaysSinceLastPlayed_m4075890483 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.NativePlayerStats::DaysSinceLastPlayed()
extern "C"  int32_t NativePlayerStats_DaysSinceLastPlayed_m2253091539 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasNumberOfPurchases()
extern "C"  bool NativePlayerStats_HasNumberOfPurchases_m3351579929 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.NativePlayerStats::NumberOfPurchases()
extern "C"  int32_t NativePlayerStats_NumberOfPurchases_m645775545 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasNumberOfSessions()
extern "C"  bool NativePlayerStats_HasNumberOfSessions_m1195368600 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.NativePlayerStats::NumberOfSessions()
extern "C"  int32_t NativePlayerStats_NumberOfSessions_m1523726584 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasSessionPercentile()
extern "C"  bool NativePlayerStats_HasSessionPercentile_m855726298 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.PInvoke.NativePlayerStats::SessionPercentile()
extern "C"  float NativePlayerStats_SessionPercentile_m2343176316 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativePlayerStats::HasSpendPercentile()
extern "C"  bool NativePlayerStats_HasSpendPercentile_m3080043810 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.PInvoke.NativePlayerStats::SpendPercentile()
extern "C"  float NativePlayerStats_SpendPercentile_m2089413956 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativePlayerStats::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativePlayerStats_CallDispose_m912838291 (NativePlayerStats_t1419601613 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayerStats GooglePlayGames.Native.PInvoke.NativePlayerStats::AsPlayerStats()
extern "C"  PlayerStats_t60064856 * NativePlayerStats_AsPlayerStats_m3488951578 (NativePlayerStats_t1419601613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
