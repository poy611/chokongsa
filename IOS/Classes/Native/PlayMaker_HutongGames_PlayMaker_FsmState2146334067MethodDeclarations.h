﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction>
struct List_1_t3734714585;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Collision
struct Collision_t2494107688;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// UnityEngine.Joint2D
struct Joint2D_t2513613714;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t2476090292;
// HutongGames.PlayMaker.ActionData
struct ActionData_t3958426178;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Single HutongGames.PlayMaker.FsmState::get_StateTime()
extern "C"  float FsmState_get_StateTime_m459577127 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_StateTime(System.Single)
extern "C"  void FsmState_set_StateTime_m4015660812 (FsmState_t2146334067 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmState::get_RealStartTime()
extern "C"  float FsmState_get_RealStartTime_m648470970 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_RealStartTime(System.Single)
extern "C"  void FsmState_set_RealStartTime_m2191001433 (FsmState_t2146334067 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_loopCount()
extern "C"  int32_t FsmState_get_loopCount_m820531342 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_loopCount(System.Int32)
extern "C"  void FsmState_set_loopCount_m817081085 (FsmState_t2146334067 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_maxLoopCount()
extern "C"  int32_t FsmState_get_maxLoopCount_m2766332486 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_maxLoopCount(System.Int32)
extern "C"  void FsmState_set_maxLoopCount_m2903685049 (FsmState_t2146334067 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::GetFullStateLabel(HutongGames.PlayMaker.FsmState)
extern "C"  String_t* FsmState_GetFullStateLabel_m3559469574 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::.ctor(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmState__ctor_m1946941374 (FsmState_t2146334067 * __this, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::.ctor(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmState__ctor_m1783314275 (FsmState_t2146334067 * __this, FsmState_t2146334067 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::CopyActionData(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmState_CopyActionData_m2986916540 (FsmState_t2146334067 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::LoadActions()
extern "C"  void FsmState_LoadActions_m1250897045 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::SaveActions()
extern "C"  void FsmState_SaveActions_m2407098846 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::get_ActiveActions()
extern "C"  List_1_t3734714585 * FsmState_get_ActiveActions_m1865845030 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::get_finishedActions()
extern "C"  List_1_t3734714585 * FsmState_get_finishedActions_m2306958810 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnEnter()
extern "C"  void FsmState_OnEnter_m162855319 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::ActivateActions(System.Int32)
extern "C"  bool FsmState_ActivateActions_m2658526977 (FsmState_t2146334067 * __this, int32_t ___startIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmState_OnEvent_m3327454939 (FsmState_t2146334067 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnFixedUpdate()
extern "C"  void FsmState_OnFixedUpdate_m3030631516 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnUpdate()
extern "C"  void FsmState_OnUpdate_m4182074252 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnLateUpdate()
extern "C"  void FsmState_OnLateUpdate_m4225925970 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnAnimatorMove()
extern "C"  bool FsmState_OnAnimatorMove_m4087730899 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnAnimatorIK(System.Int32)
extern "C"  bool FsmState_OnAnimatorIK_m3846604629 (FsmState_t2146334067 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionEnter(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionEnter_m1371367062 (FsmState_t2146334067 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionStay(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionStay_m2893583813 (FsmState_t2146334067 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionExit(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionExit_m3337196896 (FsmState_t2146334067 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerEnter(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerEnter_m4113480624 (FsmState_t2146334067 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerStay(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerStay_m461042413 (FsmState_t2146334067 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerExit(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerExit_m2137920498 (FsmState_t2146334067 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnParticleCollision(UnityEngine.GameObject)
extern "C"  bool FsmState_OnParticleCollision_m1777854539 (FsmState_t2146334067 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionEnter2D_m402877746 (FsmState_t2146334067 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionStay2D_m908422369 (FsmState_t2146334067 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionExit2D_m2860984060 (FsmState_t2146334067 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerEnter2D_m2865042352 (FsmState_t2146334067 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerStay2D_m3581502573 (FsmState_t2146334067 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerExit2D_m3644488434 (FsmState_t2146334067 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  bool FsmState_OnControllerColliderHit_m3438493244 (FsmState_t2146334067 * __this, ControllerColliderHit_t2416790841 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnJointBreak(System.Single)
extern "C"  bool FsmState_OnJointBreak_m3581673051 (FsmState_t2146334067 * __this, float ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  bool FsmState_OnJointBreak2D_m4210486699 (FsmState_t2146334067 * __this, Joint2D_t2513613714 * ___joint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnGUI()
extern "C"  void FsmState_OnGUI_m3531950778 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::FinishAction(HutongGames.PlayMaker.FsmStateAction)
extern "C"  void FsmState_FinishAction_m546922234 (FsmState_t2146334067 * __this, FsmStateAction_t2366529033 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::RemoveFinishedActions()
extern "C"  void FsmState_RemoveFinishedActions_m1080004133 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::CheckAllActionsFinished()
extern "C"  void FsmState_CheckAllActionsFinished_m833751028 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnExit()
extern "C"  void FsmState_OnExit_m2092383009 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::ResetLoopCount()
extern "C"  void FsmState_ResetLoopCount_m2165085440 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmState::GetTransition(System.Int32)
extern "C"  FsmTransition_t3771611999 * FsmState_GetTransition_m2806923760 (FsmState_t2146334067 * __this, int32_t ___transitionIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::GetTransitionIndex(HutongGames.PlayMaker.FsmTransition)
extern "C"  int32_t FsmState_GetTransitionIndex_m4078199724 (FsmState_t2146334067 * __this, FsmTransition_t3771611999 * ___transition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_Active()
extern "C"  bool FsmState_get_Active_m4134178699 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmState::get_ActiveAction()
extern "C"  FsmStateAction_t2366529033 * FsmState_get_ActiveAction_m1848135799 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsInitialized()
extern "C"  bool FsmState_get_IsInitialized_m996080711 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmState::get_Fsm()
extern "C"  Fsm_t1527112426 * FsmState_get_Fsm_m686586998 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmState_set_Fsm_m2742848989 (FsmState_t2146334067 * __this, Fsm_t1527112426 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::get_Name()
extern "C"  String_t* FsmState_get_Name_m3930587579 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Name(System.String)
extern "C"  void FsmState_set_Name_m390226486 (FsmState_t2146334067 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsSequence()
extern "C"  bool FsmState_get_IsSequence_m143728720 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_IsSequence(System.Boolean)
extern "C"  void FsmState_set_IsSequence_m2758303043 (FsmState_t2146334067 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_ActiveActionIndex()
extern "C"  int32_t FsmState_get_ActiveActionIndex_m1734620217 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmState::get_Position()
extern "C"  Rect_t4241904616  FsmState_get_Position_m394414012 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Position(UnityEngine.Rect)
extern "C"  void FsmState_set_Position_m744270127 (FsmState_t2146334067 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsBreakpoint()
extern "C"  bool FsmState_get_IsBreakpoint_m448292096 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_IsBreakpoint(System.Boolean)
extern "C"  void FsmState_set_IsBreakpoint_m2094710515 (FsmState_t2146334067 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_HideUnused()
extern "C"  bool FsmState_get_HideUnused_m3044630461 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_HideUnused(System.Boolean)
extern "C"  void FsmState_set_HideUnused_m3705838064 (FsmState_t2146334067 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::get_Actions()
extern "C"  FsmStateActionU5BU5D_t2476090292* FsmState_get_Actions_m61102534 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Actions(HutongGames.PlayMaker.FsmStateAction[])
extern "C"  void FsmState_set_Actions_m547027753 (FsmState_t2146334067 * __this, FsmStateActionU5BU5D_t2476090292* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_ActionsLoaded()
extern "C"  bool FsmState_get_ActionsLoaded_m3504211551 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.FsmState::get_ActionData()
extern "C"  ActionData_t3958426178 * FsmState_get_ActionData_m3007499394 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.FsmState::get_Transitions()
extern "C"  FsmTransitionU5BU5D_t818210886* FsmState_get_Transitions_m3655227339 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Transitions(HutongGames.PlayMaker.FsmTransition[])
extern "C"  void FsmState_set_Transitions_m940090600 (FsmState_t2146334067 * __this, FsmTransitionU5BU5D_t818210886* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::get_Description()
extern "C"  String_t* FsmState_get_Description_m1586325038 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Description(System.String)
extern "C"  void FsmState_set_Description_m406214565 (FsmState_t2146334067 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_ColorIndex()
extern "C"  int32_t FsmState_get_ColorIndex_m1798534990 (FsmState_t2146334067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_ColorIndex(System.Int32)
extern "C"  void FsmState_set_ColorIndex_m3953910081 (FsmState_t2146334067 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::GetStateIndex(HutongGames.PlayMaker.FsmState)
extern "C"  int32_t FsmState_GetStateIndex_m1579272486 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
