﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetIdToken>c__AnonStorey49
struct U3CGetIdTokenU3Ec__AnonStorey49_t2672899810;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetIdToken>c__AnonStorey49::.ctor()
extern "C"  void U3CGetIdTokenU3Ec__AnonStorey49__ctor_m2661859017 (U3CGetIdTokenU3Ec__AnonStorey49_t2672899810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetIdToken>c__AnonStorey49::<>m__1D()
extern "C"  void U3CGetIdTokenU3Ec__AnonStorey49_U3CU3Em__1D_m554066117 (U3CGetIdTokenU3Ec__AnonStorey49_t2672899810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetIdToken>c__AnonStorey49::<>m__1E()
extern "C"  void U3CGetIdTokenU3Ec__AnonStorey49_U3CU3Em__1E_m554067078 (U3CGetIdTokenU3Ec__AnonStorey49_t2672899810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
