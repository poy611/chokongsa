﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2FromVector2
struct GetAtan2FromVector2_t4248152919;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::.ctor()
extern "C"  void GetAtan2FromVector2__ctor_m997173695 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::Reset()
extern "C"  void GetAtan2FromVector2_Reset_m2938573932 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnEnter()
extern "C"  void GetAtan2FromVector2_OnEnter_m4192909782 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnUpdate()
extern "C"  void GetAtan2FromVector2_OnUpdate_m264743725 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::DoATan()
extern "C"  void GetAtan2FromVector2_DoATan_m1109383824 (GetAtan2FromVector2_t4248152919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
