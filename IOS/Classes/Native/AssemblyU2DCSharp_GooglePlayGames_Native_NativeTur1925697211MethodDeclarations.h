﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey93
struct U3CFindEqualVersionMatchU3Ec__AnonStorey93_t1925697211;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey93::.ctor()
extern "C"  void U3CFindEqualVersionMatchU3Ec__AnonStorey93__ctor_m1712549952 (U3CFindEqualVersionMatchU3Ec__AnonStorey93_t1925697211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey93::<>m__82(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void U3CFindEqualVersionMatchU3Ec__AnonStorey93_U3CU3Em__82_m111502014 (U3CFindEqualVersionMatchU3Ec__AnonStorey93_t1925697211 * __this, TurnBasedMatchResponse_t4255652325 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
