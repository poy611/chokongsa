﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerStay
struct PlayMakerTriggerStay_t3495636161;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void PlayMakerTriggerStay::OnTriggerStay(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerStay_OnTriggerStay_m1928126945 (PlayMakerTriggerStay_t3495636161 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerStay::.ctor()
extern "C"  void PlayMakerTriggerStay__ctor_m4085508540 (PlayMakerTriggerStay_t3495636161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
