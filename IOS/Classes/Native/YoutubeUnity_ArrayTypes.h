﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;

#include "mscorlib_System_Array1146569071.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"

#pragma once
// YoutubeExtractor.VideoInfo[]
struct VideoInfoU5BU5D_t4179167598  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VideoInfo_t2099471191 * m_Items[1];

public:
	inline VideoInfo_t2099471191 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VideoInfo_t2099471191 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VideoInfo_t2099471191 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
