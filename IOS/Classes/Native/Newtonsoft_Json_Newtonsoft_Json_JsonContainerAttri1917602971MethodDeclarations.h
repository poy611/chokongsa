﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t1917602971;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Type Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterType()
extern "C"  Type_t * JsonContainerAttribute_get_ItemConverterType_m1220175280 (JsonContainerAttribute_t1917602971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterParameters()
extern "C"  ObjectU5BU5D_t1108656482* JsonContainerAttribute_get_ItemConverterParameters_m3065080711 (JsonContainerAttribute_t1917602971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
