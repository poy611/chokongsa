﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LookAt
struct LookAt_t3138770714;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void HutongGames.PlayMaker.Actions.LookAt::.ctor()
extern "C"  void LookAt__ctor_m1391208076 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::Reset()
extern "C"  void LookAt_Reset_m3332608313 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnEnter()
extern "C"  void LookAt_OnEnter_m607860579 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnLateUpdate()
extern "C"  void LookAt_OnLateUpdate_m4174841094 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::DoLookAt()
extern "C"  void LookAt_DoLookAt_m2081285653 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.LookAt::UpdateLookAtPosition()
extern "C"  bool LookAt_UpdateLookAtPosition_m1083860624 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPosition()
extern "C"  Vector3_t4282066566  LookAt_GetLookAtPosition_m2612037921 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPositionWithVertical()
extern "C"  Vector3_t4282066566  LookAt_GetLookAtPositionWithVertical_m100816029 (LookAt_t3138770714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
