﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackButton
struct BackButton_t3899878169;

#include "codegen/il2cpp-codegen.h"

// System.Void BackButton::.ctor()
extern "C"  void BackButton__ctor_m304912802 (BackButton_t3899878169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackButton::buttonBack()
extern "C"  void BackButton_buttonBack_m4030448507 (BackButton_t3899878169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
