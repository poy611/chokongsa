﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<Start>c__IteratorC
struct U3CStartU3Ec__IteratorC_t2251047613;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<Start>c__IteratorC::.ctor()
extern "C"  void U3CStartU3Ec__IteratorC__ctor_m3696159358 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3405356510 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m942611826 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__IteratorC::MoveNext()
extern "C"  bool U3CStartU3Ec__IteratorC_MoveNext_m167254430 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__IteratorC::Dispose()
extern "C"  void U3CStartU3Ec__IteratorC_Dispose_m4264439611 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__IteratorC::Reset()
extern "C"  void U3CStartU3Ec__IteratorC_Reset_m1342592299 (U3CStartU3Ec__IteratorC_t2251047613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
