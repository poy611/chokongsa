﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m365533482(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t51964386 *, ResolverContractKey_t473801005 , JsonContract_t1328848902 *, const MethodInfo*))KeyValuePair_2__ctor_m3343032681_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::get_Key()
#define KeyValuePair_2_get_Key_m835984254(__this, method) ((  ResolverContractKey_t473801005  (*) (KeyValuePair_2_t51964386 *, const MethodInfo*))KeyValuePair_2_get_Key_m546969567_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1140928191(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t51964386 *, ResolverContractKey_t473801005 , const MethodInfo*))KeyValuePair_2_set_Key_m2701507360_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::get_Value()
#define KeyValuePair_2_get_Value_m888355518(__this, method) ((  JsonContract_t1328848902 * (*) (KeyValuePair_2_t51964386 *, const MethodInfo*))KeyValuePair_2_get_Value_m3270771651_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3801680703(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t51964386 *, JsonContract_t1328848902 *, const MethodInfo*))KeyValuePair_2_set_Value_m3620634400_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::ToString()
#define KeyValuePair_2_ToString_m4273866435(__this, method) ((  String_t* (*) (KeyValuePair_2_t51964386 *, const MethodInfo*))KeyValuePair_2_ToString_m2151616104_gshared)(__this, method)
