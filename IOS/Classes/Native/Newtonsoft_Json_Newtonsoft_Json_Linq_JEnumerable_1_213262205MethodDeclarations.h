﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JEnumerable_1_971832625MethodDeclarations.h"

// System.Void Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define JEnumerable_1__ctor_m647732359(__this, ___enumerable0, method) ((  void (*) (JEnumerable_1_t213262205 *, Il2CppObject*, const MethodInfo*))JEnumerable_1__ctor_m313850865_gshared)(__this, ___enumerable0, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::GetEnumerator()
#define JEnumerable_1_GetEnumerator_m3817977196(__this, method) ((  Il2CppObject* (*) (JEnumerable_1_t213262205 *, const MethodInfo*))JEnumerable_1_GetEnumerator_m3571439426_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::System.Collections.IEnumerable.GetEnumerator()
#define JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1865494515(__this, method) ((  Il2CppObject * (*) (JEnumerable_1_t213262205 *, const MethodInfo*))JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::Equals(Newtonsoft.Json.Linq.JEnumerable`1<T>)
#define JEnumerable_1_Equals_m3072708341(__this, ___other0, method) ((  bool (*) (JEnumerable_1_t213262205 *, JEnumerable_1_t213262205 , const MethodInfo*))JEnumerable_1_Equals_m1430828747_gshared)(__this, ___other0, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::Equals(System.Object)
#define JEnumerable_1_Equals_m3209300691(__this, ___obj0, method) ((  bool (*) (JEnumerable_1_t213262205 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_Equals_m2597819561_gshared)(__this, ___obj0, method)
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::GetHashCode()
#define JEnumerable_1_GetHashCode_m180206635(__this, method) ((  int32_t (*) (JEnumerable_1_t213262205 *, const MethodInfo*))JEnumerable_1_GetHashCode_m2089431425_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken>::.cctor()
#define JEnumerable_1__cctor_m2442560949(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))JEnumerable_1__cctor_m3840698783_gshared)(__this /* static, unused */, method)
