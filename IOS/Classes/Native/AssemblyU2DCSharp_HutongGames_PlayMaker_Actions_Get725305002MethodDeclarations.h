﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMass2d
struct GetMass2d_t725305002;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMass2d::.ctor()
extern "C"  void GetMass2d__ctor_m3257245132 (GetMass2d_t725305002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::Reset()
extern "C"  void GetMass2d_Reset_m903678073 (GetMass2d_t725305002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::OnEnter()
extern "C"  void GetMass2d_OnEnter_m2868108963 (GetMass2d_t725305002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::DoGetMass()
extern "C"  void GetMass2d_DoGetMass_m2747933449 (GetMass2d_t725305002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
