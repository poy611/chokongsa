﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StructforMinigame
struct StructforMinigame_t1286533533;
// NetworkMng/OnSave
struct OnSave_t2578843667;
// NetworkMng/OnSuccess
struct OnSuccess_t2806737325;
// NetworkMng/OnFail
struct OnFail_t2578455988;
// NetworkMng/OnNoInfo
struct OnNoInfo_t4236056357;
// NetworkMng/OnAllResult
struct OnAllResult_t2910001224;
// NetworkMng/OnAllResultFail
struct OnAllResultFail_t306118534;
// NetworkMng/OnAllResultLoadFail
struct OnAllResultLoadFail_t435151244;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Singleton_1_gen1768030745.h"
#include "AssemblyU2DCSharp_NetworkMng_NetWorkState454878650.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng
struct  NetworkMng_t1515215352  : public Singleton_1_t1768030745
{
public:
	// NetworkMng/NetWorkState NetworkMng::state
	int32_t ___state_3;
	// StructforMinigame NetworkMng::stuff
	StructforMinigame_t1286533533 * ___stuff_4;
	// NetworkMng/OnSave NetworkMng::onSave
	OnSave_t2578843667 * ___onSave_5;
	// NetworkMng/OnSuccess NetworkMng::onSuccess
	OnSuccess_t2806737325 * ___onSuccess_6;
	// NetworkMng/OnFail NetworkMng::onFail
	OnFail_t2578455988 * ___onFail_7;
	// NetworkMng/OnNoInfo NetworkMng::noInfo
	OnNoInfo_t4236056357 * ___noInfo_8;
	// NetworkMng/OnAllResult NetworkMng::onAllResult
	OnAllResult_t2910001224 * ___onAllResult_9;
	// NetworkMng/OnAllResultFail NetworkMng::onAllResultFail
	OnAllResultFail_t306118534 * ___onAllResultFail_10;
	// NetworkMng/OnAllResultLoadFail NetworkMng::onAllResultLoadFail
	OnAllResultLoadFail_t435151244 * ___onAllResultLoadFail_11;
	// System.String NetworkMng::errorLog
	String_t* ___errorLog_12;
	// System.String NetworkMng::urlFront
	String_t* ___urlFront_13;
	// System.String NetworkMng::m_strPath
	String_t* ___m_strPath_14;

public:
	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_stuff_4() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___stuff_4)); }
	inline StructforMinigame_t1286533533 * get_stuff_4() const { return ___stuff_4; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_4() { return &___stuff_4; }
	inline void set_stuff_4(StructforMinigame_t1286533533 * value)
	{
		___stuff_4 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_4, value);
	}

	inline static int32_t get_offset_of_onSave_5() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onSave_5)); }
	inline OnSave_t2578843667 * get_onSave_5() const { return ___onSave_5; }
	inline OnSave_t2578843667 ** get_address_of_onSave_5() { return &___onSave_5; }
	inline void set_onSave_5(OnSave_t2578843667 * value)
	{
		___onSave_5 = value;
		Il2CppCodeGenWriteBarrier(&___onSave_5, value);
	}

	inline static int32_t get_offset_of_onSuccess_6() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onSuccess_6)); }
	inline OnSuccess_t2806737325 * get_onSuccess_6() const { return ___onSuccess_6; }
	inline OnSuccess_t2806737325 ** get_address_of_onSuccess_6() { return &___onSuccess_6; }
	inline void set_onSuccess_6(OnSuccess_t2806737325 * value)
	{
		___onSuccess_6 = value;
		Il2CppCodeGenWriteBarrier(&___onSuccess_6, value);
	}

	inline static int32_t get_offset_of_onFail_7() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onFail_7)); }
	inline OnFail_t2578455988 * get_onFail_7() const { return ___onFail_7; }
	inline OnFail_t2578455988 ** get_address_of_onFail_7() { return &___onFail_7; }
	inline void set_onFail_7(OnFail_t2578455988 * value)
	{
		___onFail_7 = value;
		Il2CppCodeGenWriteBarrier(&___onFail_7, value);
	}

	inline static int32_t get_offset_of_noInfo_8() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___noInfo_8)); }
	inline OnNoInfo_t4236056357 * get_noInfo_8() const { return ___noInfo_8; }
	inline OnNoInfo_t4236056357 ** get_address_of_noInfo_8() { return &___noInfo_8; }
	inline void set_noInfo_8(OnNoInfo_t4236056357 * value)
	{
		___noInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___noInfo_8, value);
	}

	inline static int32_t get_offset_of_onAllResult_9() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onAllResult_9)); }
	inline OnAllResult_t2910001224 * get_onAllResult_9() const { return ___onAllResult_9; }
	inline OnAllResult_t2910001224 ** get_address_of_onAllResult_9() { return &___onAllResult_9; }
	inline void set_onAllResult_9(OnAllResult_t2910001224 * value)
	{
		___onAllResult_9 = value;
		Il2CppCodeGenWriteBarrier(&___onAllResult_9, value);
	}

	inline static int32_t get_offset_of_onAllResultFail_10() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onAllResultFail_10)); }
	inline OnAllResultFail_t306118534 * get_onAllResultFail_10() const { return ___onAllResultFail_10; }
	inline OnAllResultFail_t306118534 ** get_address_of_onAllResultFail_10() { return &___onAllResultFail_10; }
	inline void set_onAllResultFail_10(OnAllResultFail_t306118534 * value)
	{
		___onAllResultFail_10 = value;
		Il2CppCodeGenWriteBarrier(&___onAllResultFail_10, value);
	}

	inline static int32_t get_offset_of_onAllResultLoadFail_11() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___onAllResultLoadFail_11)); }
	inline OnAllResultLoadFail_t435151244 * get_onAllResultLoadFail_11() const { return ___onAllResultLoadFail_11; }
	inline OnAllResultLoadFail_t435151244 ** get_address_of_onAllResultLoadFail_11() { return &___onAllResultLoadFail_11; }
	inline void set_onAllResultLoadFail_11(OnAllResultLoadFail_t435151244 * value)
	{
		___onAllResultLoadFail_11 = value;
		Il2CppCodeGenWriteBarrier(&___onAllResultLoadFail_11, value);
	}

	inline static int32_t get_offset_of_errorLog_12() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___errorLog_12)); }
	inline String_t* get_errorLog_12() const { return ___errorLog_12; }
	inline String_t** get_address_of_errorLog_12() { return &___errorLog_12; }
	inline void set_errorLog_12(String_t* value)
	{
		___errorLog_12 = value;
		Il2CppCodeGenWriteBarrier(&___errorLog_12, value);
	}

	inline static int32_t get_offset_of_urlFront_13() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___urlFront_13)); }
	inline String_t* get_urlFront_13() const { return ___urlFront_13; }
	inline String_t** get_address_of_urlFront_13() { return &___urlFront_13; }
	inline void set_urlFront_13(String_t* value)
	{
		___urlFront_13 = value;
		Il2CppCodeGenWriteBarrier(&___urlFront_13, value);
	}

	inline static int32_t get_offset_of_m_strPath_14() { return static_cast<int32_t>(offsetof(NetworkMng_t1515215352, ___m_strPath_14)); }
	inline String_t* get_m_strPath_14() const { return ___m_strPath_14; }
	inline String_t** get_address_of_m_strPath_14() { return &___m_strPath_14; }
	inline void set_m_strPath_14(String_t* value)
	{
		___m_strPath_14 = value;
		Il2CppCodeGenWriteBarrier(&___m_strPath_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
