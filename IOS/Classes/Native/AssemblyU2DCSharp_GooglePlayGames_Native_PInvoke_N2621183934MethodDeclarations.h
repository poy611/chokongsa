﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeAchievement
struct NativeAchievement_t2621183934;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1457931865.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3372201074.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeAchievement::.ctor(System.IntPtr)
extern "C"  void NativeAchievement__ctor_m2650669294 (NativeAchievement_t2621183934 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeAchievement::CurrentSteps()
extern "C"  uint32_t NativeAchievement_CurrentSteps_m2563234855 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeAchievement::Description()
extern "C"  String_t* NativeAchievement_Description_m1242815101 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeAchievement::Id()
extern "C"  String_t* NativeAchievement_Id_m1100695260 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeAchievement::Name()
extern "C"  String_t* NativeAchievement_Name_m1348679756 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/AchievementState GooglePlayGames.Native.PInvoke.NativeAchievement::State()
extern "C"  int32_t NativeAchievement_State_m536338001 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeAchievement::TotalSteps()
extern "C"  uint32_t NativeAchievement_TotalSteps_m3025318396 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/AchievementType GooglePlayGames.Native.PInvoke.NativeAchievement::Type()
extern "C"  int32_t NativeAchievement_Type_m3666363577 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeAchievement::LastModifiedTime()
extern "C"  uint64_t NativeAchievement_LastModifiedTime_m234753382 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeAchievement::getXP()
extern "C"  uint64_t NativeAchievement_getXP_m241106838 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeAchievement::getRevealedImageUrl()
extern "C"  String_t* NativeAchievement_getRevealedImageUrl_m1263251191 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeAchievement::getUnlockedImageUrl()
extern "C"  String_t* NativeAchievement_getUnlockedImageUrl_m4145163246 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeAchievement::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeAchievement_CallDispose_m2988974722 (NativeAchievement_t2621183934 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.PInvoke.NativeAchievement::AsAchievement()
extern "C"  Achievement_t1261647177 * NativeAchievement_AsAchievement_m4193306027 (NativeAchievement_t2621183934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeAchievement::<Description>m__94(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeAchievement_U3CDescriptionU3Em__94_m75701047 (NativeAchievement_t2621183934 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeAchievement::<Id>m__95(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeAchievement_U3CIdU3Em__95_m3491140517 (NativeAchievement_t2621183934 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeAchievement::<Name>m__96(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeAchievement_U3CNameU3Em__96_m3429670422 (NativeAchievement_t2621183934 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeAchievement::<getRevealedImageUrl>m__97(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeAchievement_U3CgetRevealedImageUrlU3Em__97_m2727296372 (NativeAchievement_t2621183934 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeAchievement::<getUnlockedImageUrl>m__98(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeAchievement_U3CgetUnlockedImageUrlU3Em__98_m209260780 (NativeAchievement_t2621183934 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
