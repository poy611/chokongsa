﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan
struct GetAtan_t1738223652;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan::.ctor()
extern "C"  void GetAtan__ctor_m1350588306 (GetAtan_t1738223652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::Reset()
extern "C"  void GetAtan_Reset_m3291988543 (GetAtan_t1738223652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnEnter()
extern "C"  void GetAtan_OnEnter_m226967273 (GetAtan_t1738223652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnUpdate()
extern "C"  void GetAtan_OnUpdate_m1874577530 (GetAtan_t1738223652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::DoATan()
extern "C"  void GetAtan_DoATan_m3475302173 (GetAtan_t1738223652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
