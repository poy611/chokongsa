﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>
struct KeyCollection_t3821466628;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t2194707177;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2809643231.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3137692258_gshared (KeyCollection_t3821466628 * __this, Dictionary_2_t2194707177 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3137692258(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3821466628 *, Dictionary_2_t2194707177 *, const MethodInfo*))KeyCollection__ctor_m3137692258_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3238035892_gshared (KeyCollection_t3821466628 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3238035892(__this, ___item0, method) ((  void (*) (KeyCollection_t3821466628 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3238035892_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4213408811_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4213408811(__this, method) ((  void (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4213408811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2107760086_gshared (KeyCollection_t3821466628 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2107760086(__this, ___item0, method) ((  bool (*) (KeyCollection_t3821466628 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2107760086_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2505559227_gshared (KeyCollection_t3821466628 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2505559227(__this, ___item0, method) ((  bool (*) (KeyCollection_t3821466628 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2505559227_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2614311655_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2614311655(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2614311655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3194060829_gshared (KeyCollection_t3821466628 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3194060829(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3821466628 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3194060829_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m250342616_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m250342616(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m250342616_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1213937591_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1213937591(__this, method) ((  bool (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1213937591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1542859049_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1542859049(__this, method) ((  bool (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1542859049_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m575153813_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m575153813(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m575153813_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3162820887_gshared (KeyCollection_t3821466628 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3162820887(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3821466628 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3162820887_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::GetEnumerator()
extern "C"  Enumerator_t2809643231  KeyCollection_GetEnumerator_m3181750650_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3181750650(__this, method) ((  Enumerator_t2809643231  (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_GetEnumerator_m3181750650_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m6474735_gshared (KeyCollection_t3821466628 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m6474735(__this, method) ((  int32_t (*) (KeyCollection_t3821466628 *, const MethodInfo*))KeyCollection_get_Count_m6474735_gshared)(__this, method)
