﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3124613658;
// System.Type
struct Type_t;
// Newtonsoft.Json.Converters.KeyValuePairConverter
struct KeyValuePairConverter_t3986497414;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.KeyValuePairConverter::InitializeReflectionObject(System.Type)
extern "C"  ReflectionObject_t3124613658 * KeyValuePairConverter_InitializeReflectionObject_m2783485027 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void KeyValuePairConverter_WriteJson_m984578108 (KeyValuePairConverter_t3986497414 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * KeyValuePairConverter_ReadJson_m1898287127 (KeyValuePairConverter_t3986497414 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::CanConvert(System.Type)
extern "C"  bool KeyValuePairConverter_CanConvert_m2299309312 (KeyValuePairConverter_t3986497414 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.ctor()
extern "C"  void KeyValuePairConverter__ctor_m2086305816 (KeyValuePairConverter_t3986497414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.cctor()
extern "C"  void KeyValuePairConverter__cctor_m4063841941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
