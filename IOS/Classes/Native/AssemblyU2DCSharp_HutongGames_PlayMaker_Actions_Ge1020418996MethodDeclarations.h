﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVector3
struct GetFsmVector3_t1020418996;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::.ctor()
extern "C"  void GetFsmVector3__ctor_m1080483842 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::Reset()
extern "C"  void GetFsmVector3_Reset_m3021884079 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::OnEnter()
extern "C"  void GetFsmVector3_OnEnter_m2649582425 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::OnUpdate()
extern "C"  void GetFsmVector3_OnUpdate_m3961203210 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::DoGetFsmVector3()
extern "C"  void GetFsmVector3_DoGetFsmVector3_m2141474491 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
