﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.VideoNotAvailableException
struct VideoNotAvailableException_t1343733958;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void YoutubeExtractor.VideoNotAvailableException::.ctor()
extern "C"  void VideoNotAvailableException__ctor_m1707482460 (VideoNotAvailableException_t1343733958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.VideoNotAvailableException::.ctor(System.String)
extern "C"  void VideoNotAvailableException__ctor_m3454474022 (VideoNotAvailableException_t1343733958 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
