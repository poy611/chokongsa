﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3206263253;
// System.Object
struct Il2CppObject;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2095059871;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2336740304;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1756209413;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Exception
struct Exception_t3991598821;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t645006031;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t676510742;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t886686464;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t918191175;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3206263253MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2095059871MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2336740304MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1756209413MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen645006031MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen886686464MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1109560158.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1109560158MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2811027361.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2811027361MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1920129602.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1920129602MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen53556920.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen53556920MethodDeclarations.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen973669728.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen973669728MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1554155346.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1554155346MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1455007855.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1455007855MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2155190829MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex3372848153.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1949385224MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3167042548.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen798349321MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2016006645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2647289085.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2647289085MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3749301894.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3749301894MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter_State671991922.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2699240237.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2699240237MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2229264188.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2229264188MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1754187467.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1754187467MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1674672166.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1674672166MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3551110977.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3551110977MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943444146.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3943444146MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1211634336.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1211634336MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen970376284.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen970376284MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2188033608.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3554108690.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3554108690MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644952336.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644952336MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644965214.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1644965214MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen533949290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen533949290MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4166879682.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4166879682MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1089569710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3891715294.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3891715294MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li814405322.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen904941831.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen904941831MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2122599155.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1859153240.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1859153240MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795846257.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795846257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1676274531.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1676274531MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1598245646.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1598245646MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4127192417.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4127192417MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2849202992.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2849202992MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3178380060.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3178380060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3116794.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3116794MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3743492068.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3743492068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3280454238.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3280454238MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1327961296.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1327961296MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005001078.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005001078MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen727011653.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen727011653MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen848114254.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen848114254MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen875830559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen875830559MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2225906962.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2225906962MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen702156392.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen702156392MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3377690443.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3377690443MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen846010146.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen846010146MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2063667470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1042872857.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1042872857MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2260530181.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen854365966.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen854365966MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2072023290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3066004003.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3066004003MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen736693307.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen736693307MethodDeclarations.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2650569241.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2650569241MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148414MethodDeclarations.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148472.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148472MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148567.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4231148567MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2792744647.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2792744647MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1841955665.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1841955665MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2083636098.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2083636098MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1990166460.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1990166460MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3207823784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3737689414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3737689414MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi660379442.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen136423630.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen136423630MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo1354080954.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3819239998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3819239998MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen896245509.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen896245509MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC2113902833.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795948550.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2795948550MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4013605874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen563161977.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen563161977MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1203046106.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1203046106MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B2420703430.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4239079749.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4239079749MethodDeclarations.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3844211903.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3844211903MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3074261648.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3074261648MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2593882473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2593882473MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark3811539797.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490832959.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490832959MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977895.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977895MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977953.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101977953MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101978048.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3101978048MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen73011651.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen73011651MethodDeclarations.h"
#include "System_System_Uri_UriScheme1290668975.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2440554239.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2440554239MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3658211563.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen531556423.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen531556423MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope1749213747.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3594029982.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3594029982MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathResultType516720010.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2976889581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2976889581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3676163660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3676163660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3320393320.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3320393320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint243083348.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3070775034.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3070775034MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D4288432358.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2545004040.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2545004040MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3148142670.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3148142670MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2861398790.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2861398790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2013616441.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2013616441MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen336045558.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen336045558MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2785518402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2785518402MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen157087060.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen157087060MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3024247292.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3024247292MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1991476773.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1991476773MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2263718591.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2263718591MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen963639266.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen963639266MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2928303786.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2928303786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240* p0, CustomAttributeNamedArgument_t3059612989  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m3345236030_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251* p0, CustomAttributeTypedArgument_t3301293422  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m858079087_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisParticipantResult_t2327217482_m262741684_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParticipantResult_t2327217482_m262741684(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParticipantResult_t2327217482_m262741684_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisParticipantStatus_t4028684685_m3944006901_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParticipantStatus_t4028684685_m3944006901(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParticipantStatus_t4028684685_m3944006901_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisParticipantStatus_t3137786926_m3126575184_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParticipantStatus_t3137786926_m3126575184(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParticipantStatus_t3137786926_m3126575184_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<HutongGames.Extensions.TextureExtensions/Point>(System.Int32)
extern "C"  Point_t1271214244  Array_InternalArray__get_Item_TisPoint_t1271214244_m358578686_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPoint_t1271214244_m358578686(__this, p0, method) ((  Point_t1271214244  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPoint_t1271214244_m358578686_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisCalculation_t2191327052_m2309372758_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCalculation_t2191327052_m2309372758(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCalculation_t2191327052_m2309372758_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisCalculation_t2771812670_m3857519524_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCalculation_t2771812670_m3857519524(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCalculation_t2771812670_m3857519524_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<HutongGames.PlayMaker.ParamDataType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisParamDataType_t2672665179_m3837766137_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParamDataType_t2672665179_m3837766137(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParamDataType_t2672665179_m3837766137_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t3372848153  Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249(__this, p0, method) ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t3372848153_m3354422249_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t3167042548_m115939321_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2016006645  Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273(__this, p0, method) ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2016006645_m2189903273_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.JsonPosition>(System.Int32)
extern "C"  JsonPosition_t3864946409  Array_InternalArray__get_Item_TisJsonPosition_t3864946409_m2284205724_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJsonPosition_t3864946409_m2284205724(__this, p0, method) ((  JsonPosition_t3864946409  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJsonPosition_t3864946409_m2284205724_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.JsonWriter/State>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisState_t671991922_m806392816_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisState_t671991922_m806392816(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisState_t671991922_m806392816_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Linq.JTokenType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisJTokenType_t3916897561_m3146714076_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.ReadType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisReadType_t3446921512_m2980607869_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisReadType_t3446921512_m2980607869(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisReadType_t3446921512_m2980607869_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>(System.Int32)
extern "C"  TypeNameKey_t2971844791  Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107(__this, p0, method) ((  TypeNameKey_t2971844791  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeNameKey_t2971844791_m94980107_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyPresence_t2892329490_m702118636_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Serialization.ResolverContractKey>(System.Int32)
extern "C"  ResolverContractKey_t473801005  Array_InternalArray__get_Item_TisResolverContractKey_t473801005_m398648394_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResolverContractKey_t473801005_m398648394(__this, p0, method) ((  ResolverContractKey_t473801005  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResolverContractKey_t473801005_m398648394_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>(System.Int32)
extern "C"  TypeConvertKey_t866134174  Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768(__this, p0, method) ((  TypeConvertKey_t866134174  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeConvertKey_t866134174_m2084540768_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Newtonsoft.Json.Utilities.PrimitiveTypeCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPrimitiveTypeCode_t2429291660_m2626022665_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPrimitiveTypeCode_t2429291660_m2626022665(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPrimitiveTypeCode_t2429291660_m2626022665_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2188033608  Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617(__this, p0, method) ((  ArraySegment_1_t2188033608  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2188033608_m3907926617_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t476798718_m1243823257_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t2862609660_m3484475127_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t2862622538_m125306601_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t2862622538_m125306601(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t2862622538_m125306601_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t1751606614  Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038(__this, p0, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t1751606614_m297283038_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>(System.Int32)
extern "C"  Link_t1089569710  Array_InternalArray__get_Item_TisLink_t1089569710_m526219785_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t1089569710_m526219785(__this, p0, method) ((  Link_t1089569710  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t1089569710_m526219785_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Char>>(System.Int32)
extern "C"  Link_t814405322  Array_InternalArray__get_Item_TisLink_t814405322_m2438429968_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t814405322_m2438429968(__this, p0, method) ((  Link_t814405322  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t814405322_m2438429968_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t2122599155  Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033(__this, p0, method) ((  Link_t2122599155  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2122599155_m1741943033_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>(System.Int32)
extern "C"  KeyValuePair_2_t3076810564  Array_InternalArray__get_Item_TisKeyValuePair_2_t3076810564_m4163612602_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3076810564_m4163612602(__this, p0, method) ((  KeyValuePair_2_t3076810564  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3076810564_m4163612602_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4013503581  Array_InternalArray__get_Item_TisKeyValuePair_2_t4013503581_m3457465496_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4013503581_m3457465496(__this, p0, method) ((  KeyValuePair_2_t4013503581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4013503581_m3457465496_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2893931855  Array_InternalArray__get_Item_TisKeyValuePair_2_t2893931855_m2933550971_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2893931855_m2933550971(__this, p0, method) ((  KeyValuePair_2_t2893931855  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2893931855_m2933550971_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2815902970  Array_InternalArray__get_Item_TisKeyValuePair_2_t2815902970_m3397022501_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2815902970_m3397022501(__this, p0, method) ((  KeyValuePair_2_t2815902970  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2815902970_m3397022501_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t1049882445  Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353(__this, p0, method) ((  KeyValuePair_2_t1049882445  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1049882445_m876792353_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4066860316  Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488(__this, p0, method) ((  KeyValuePair_2_t4066860316  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4066860316_m203509488_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>(System.Int32)
extern "C"  KeyValuePair_2_t101070088  Array_InternalArray__get_Item_TisKeyValuePair_2_t101070088_m1925684497_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t101070088_m1925684497(__this, p0, method) ((  KeyValuePair_2_t101070088  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t101070088_m1925684497_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>(System.Int32)
extern "C"  KeyValuePair_2_t1220774118  Array_InternalArray__get_Item_TisKeyValuePair_2_t1220774118_m2260206850_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1220774118_m2260206850(__this, p0, method) ((  KeyValuePair_2_t1220774118  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1220774118_m2260206850_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>(System.Int32)
extern "C"  KeyValuePair_2_t666182096  Array_InternalArray__get_Item_TisKeyValuePair_2_t666182096_m4061918425_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t666182096_m4061918425(__this, p0, method) ((  KeyValuePair_2_t666182096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t666182096_m4061918425_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>(System.Int32)
extern "C"  KeyValuePair_2_t203144266  Array_InternalArray__get_Item_TisKeyValuePair_2_t203144266_m2083197532_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t203144266_m2083197532(__this, p0, method) ((  KeyValuePair_2_t203144266  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t203144266_m2083197532_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t2545618620  Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102(__this, p0, method) ((  KeyValuePair_2_t2545618620  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2545618620_m2380168102_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3222658402  Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332(__this, p0, method) ((  KeyValuePair_2_t3222658402  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3222658402_m1457368332_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1944668977  Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653(__this, p0, method) ((  KeyValuePair_2_t1944668977  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1944668977_m1021495653_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>(System.Int32)
extern "C"  KeyValuePair_2_t2065771578  Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278(__this, p0, method) ((  KeyValuePair_2_t2065771578  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2065771578_m405895278_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>(System.Int32)
extern "C"  KeyValuePair_2_t2093487883  Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487883_m762487103_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487883_m762487103(__this, p0, method) ((  KeyValuePair_2_t2093487883  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2093487883_m762487103_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>(System.Int32)
extern "C"  KeyValuePair_2_t3443564286  Array_InternalArray__get_Item_TisKeyValuePair_2_t3443564286_m26748272_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3443564286_m26748272(__this, p0, method) ((  KeyValuePair_2_t3443564286  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3443564286_m26748272_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t1919813716  Array_InternalArray__get_Item_TisKeyValuePair_2_t1919813716_m1001108893_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1919813716_m1001108893(__this, p0, method) ((  KeyValuePair_2_t1919813716  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1919813716_m1001108893_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t300380471  Array_InternalArray__get_Item_TisKeyValuePair_2_t300380471_m2887503844_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t300380471_m2887503844(__this, p0, method) ((  KeyValuePair_2_t300380471  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t300380471_m2887503844_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2063667470  Array_InternalArray__get_Item_TisLink_t2063667470_m818406549_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2063667470_m818406549(__this, p0, method) ((  Link_t2063667470  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2063667470_m818406549_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2260530181  Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401(__this, p0, method) ((  Slot_t2260530181  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2260530181_m2684325401_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2072023290  Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440(__this, p0, method) ((  Slot_t2072023290  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2072023290_m2216062440_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t4283661327  Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548(__this, p0, method) ((  DateTime_t4283661327  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t4283661327_m185788548_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t1954350631  Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984(__this, p0, method) ((  Decimal_t1954350631  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1954350631_m4127931984_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t3868226565_m3142342990_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t1153838442_m2614347053_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t1153838500_m3068135859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t1153838595_m1812029300_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m1819425504(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m1537058848(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667(__this, p0, method) ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t3059612989_m25283667_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746(__this, p0, method) ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t3301293422_m193823746_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3207823784  Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434(__this, p0, method) ((  LabelData_t3207823784  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3207823784_m64093434_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t660379442  Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500(__this, p0, method) ((  LabelFixup_t660379442  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t660379442_m2255271500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t1354080954  Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117(__this, p0, method) ((  ILTokenInfo_t1354080954  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t1354080954_m1012704117_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t741930026  Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410(__this, p0, method) ((  ParameterModifier_t741930026  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t741930026_m2808893410_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t2113902833  Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445(__this, p0, method) ((  ResourceCacheItem_t2113902833  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t2113902833_m4118445_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4013605874  Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776(__this, p0, method) ((  ResourceInfo_t4013605874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4013605874_m3024657776_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.HandleRef>(System.Int32)
extern "C"  HandleRef_t1780819301  Array_InternalArray__get_Item_TisHandleRef_t1780819301_m2790064831_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHandleRef_t1780819301_m2790064831(__this, p0, method) ((  HandleRef_t1780819301  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHandleRef_t1780819301_m2790064831_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t2420703430_m673122397_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t1161769777_m2884868230_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t766901931  Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218(__this, p0, method) ((  X509ChainStatus_t766901931  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t766901931_m1414334218_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t4291918972_m1101558775_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3811539797  Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3811539797_m160824484(__this, p0, method) ((  Mark_t3811539797  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3811539797_m160824484_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t413522987  Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760(__this, p0, method) ((  TimeSpan_t413522987  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t413522987_m1118358760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t24667923_m1629104256_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t24667981_m2082893062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t24668076_m826786503_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1290668975  Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123(__this, p0, method) ((  UriScheme_t1290668975  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1290668975_m2328943123_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t3658211563  Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851(__this, p0, method) ((  NsDecl_t3658211563  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t3658211563_m1899540851_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t1749213747  Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815(__this, p0, method) ((  NsScope_t1749213747  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t1749213747_m3910748815_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XPath.XPathResultType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisXPathResultType_t516720010_m848952949_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisXPathResultType_t516720010_m848952949(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisXPathResultType_t516720010_m848952949_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t4194546905  Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850(__this, p0, method) ((  Color_t4194546905  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t4194546905_m3851996850_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t598853688  Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403(__this, p0, method) ((  Color32_t598853688  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t598853688_m2218876403_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t243083348  Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859(__this, p0, method) ((  ContactPoint_t243083348  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t243083348_m451644859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t4288432358  Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017(__this, p0, method) ((  ContactPoint2D_t4288432358  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t4288432358_m997735017_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t3762661364  Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905(__this, p0, method) ((  RaycastResult_t3762661364  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t3762661364_m1513632905_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Experimental.Director.Playable>(System.Int32)
extern "C"  Playable_t70832698  Array_InternalArray__get_Item_TisPlayable_t70832698_m683585505_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPlayable_t70832698_m683585505(__this, p0, method) ((  Playable_t70832698  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPlayable_t70832698_m683585505_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t4079056114  Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013(__this, p0, method) ((  Keyframe_t4079056114  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t4079056114_m1061522013_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.NetworkPlayer>(System.Int32)
extern "C"  NetworkPlayer_t3231273765  Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030(__this, p0, method) ((  NetworkPlayer_t3231273765  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNetworkPlayer_t3231273765_m660685030_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Quaternion>(System.Int32)
extern "C"  Quaternion_t1553702882  Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941(__this, p0, method) ((  Quaternion_t1553702882  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisQuaternion_t1553702882_m3380552941_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t4003175726  Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793(__this, p0, method) ((  RaycastHit_t4003175726  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t4003175726_m959669793_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t1374744384  Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143(__this, p0, method) ((  RaycastHit2D_t1374744384  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t1374744384_m3878392143_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Rect>(System.Int32)
extern "C"  Rect_t4241904616  Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRect_t4241904616_m261192103(__this, p0, method) ((  Rect_t4241904616  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRect_t4241904616_m261192103_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t3209134097  Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997(__this, p0, method) ((  HitInfo_t3209134097  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t3209134097_m3354825997_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t3481375915  Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226(__this, p0, method) ((  GcAchievementData_t3481375915  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t3481375915_m625859226_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2181296590  Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207(__this, p0, method) ((  GcScoreData_t2181296590  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2181296590_m3611437207_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t4145961110_m4291319144_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m4262077373_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2204526468_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4054268681_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4201941769_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1756209413 * L_2 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1756209413 * L_9 = (ArrayReadOnlyList_1_t1756209413 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t1108656482* L_10 = (ObjectU5BU5D_t1108656482*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2650805434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2799549978_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2027929923_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1186685398_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t3059612989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4084754896_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t645006031 * L_2 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t645006031 * L_9 = (ArrayReadOnlyList_1_t645006031 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_10 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1616284631_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m643544331_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4108562996_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2728697221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t3301293422  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1461469503_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t886686464 * L_2 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t886686464 * L_9 = (ArrayReadOnlyList_1_t886686464 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_10 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m4164061832_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m240689135_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2499499206_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1756209413 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1756209413 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1756209413 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m535939909_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2625374704_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3813449101_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1486920810_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m976758002_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2221418008_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m158553866_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1244492898_gshared (ArrayReadOnlyList_1_t1756209413 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1770413409_gshared (ArrayReadOnlyList_1_t1756209413 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3206263253 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3206263253 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1098739118_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m738278233_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m12996997_gshared (ArrayReadOnlyList_1_t1756209413 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2907098399_gshared (ArrayReadOnlyList_1_t1756209413 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m740547916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3527945938_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m932360725_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t645006031 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t645006031 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t645006031 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  ArrayReadOnlyList_1_get_Item_m3165498630_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3031060371_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2401585746_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2438902353_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3652230485_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m3556686357_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2530929859_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1650178565_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2319686054_gshared (ArrayReadOnlyList_1_t645006031 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2095059871 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2095059871 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3408499785_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get_array_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2906295740_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m609878398_gshared (ArrayReadOnlyList_1_t645006031 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m780148610_gshared (ArrayReadOnlyList_1_t645006031 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m467076531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m904660545_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042005508_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t886686464 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t886686464 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t886686464 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  ArrayReadOnlyList_1_get_Item_m957797877_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3144480962_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2684117187_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m3126393472_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3859776580_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1400680710_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2813461300_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1763599156_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2480410519_gshared (ArrayReadOnlyList_1_t886686464 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2336740304 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2336740304 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m785214392_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get_array_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m698594987_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m3157655599_gshared (ArrayReadOnlyList_1_t886686464 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2867415153_gshared (ArrayReadOnlyList_1_t886686464 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t3991598821 * L_0 = ((  Exception_t3991598821 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId;
extern "C"  Exception_t3991598821 * ArrayReadOnlyList_1_ReadOnlyError_m627800996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1209494991_gshared (InternalEnumerator_1_t1109560158 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1209494991_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	InternalEnumerator_1__ctor_m1209494991(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1070987576((InternalEnumerator_1_t1109560158 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2262703718_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2262703718_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2262703718(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3581448865_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3581448865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3581448865(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1070987576_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1070987576_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1070987576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1070987576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1109560158 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1109560158 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1070987576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3131686238_gshared (InternalEnumerator_1_t2811027361 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3131686238_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	InternalEnumerator_1__ctor_m3131686238(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1744325285((InternalEnumerator_1_t2811027361 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3195323573_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3195323573_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3195323573(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1523763438_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1523763438_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1523763438(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1744325285_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1744325285_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1744325285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1744325285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2811027361 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2811027361 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1744325285(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2103005363_gshared (InternalEnumerator_1_t1920129602 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2103005363_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	InternalEnumerator_1__ctor_m2103005363(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847028892((InternalEnumerator_1_t1920129602 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2821688906_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2821688906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2821688906(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1340035645_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1340035645_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1340035645(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3847028892_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847028892_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847028892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847028892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1920129602 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1920129602 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847028892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1358800949_gshared (InternalEnumerator_1_t53556920 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1358800949_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	InternalEnumerator_1__ctor_m1358800949(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method)
{
	{
		Point_t1271214244  L_0 = InternalEnumerator_1_get_Current_m4220172348((InternalEnumerator_1_t53556920 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Point_t1271214244  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m665911372_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m665911372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	InternalEnumerator_1_Dispose_m665911372(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2455145271_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2455145271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2455145271(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4220172348_MetadataUsageId;
extern "C"  Point_t1271214244  InternalEnumerator_1_get_Current_m4220172348_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4220172348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Point_t1271214244  L_8 = ((  Point_t1271214244  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Point_t1271214244  InternalEnumerator_1_get_Current_m4220172348_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t53556920 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t53556920 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4220172348(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3694985181_gshared (InternalEnumerator_1_t973669728 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3694985181_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	InternalEnumerator_1__ctor_m3694985181(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m969991843(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3061040996((InternalEnumerator_1_t973669728 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1319196367(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2669824500_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2669824500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2669824500(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2708343183_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2708343183_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2708343183(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3061040996_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3061040996_gshared (InternalEnumerator_1_t973669728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3061040996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3061040996_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t973669728 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t973669728 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3061040996(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m254177743_gshared (InternalEnumerator_1_t1554155346 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m254177743_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	InternalEnumerator_1__ctor_m254177743(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256487793_gshared (InternalEnumerator_1_t1554155346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256487793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1256487793(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1259223005_gshared (InternalEnumerator_1_t1554155346 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1172118166((InternalEnumerator_1_t1554155346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1259223005_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1259223005(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2149720678_gshared (InternalEnumerator_1_t1554155346 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2149720678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2149720678(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1259009757_gshared (InternalEnumerator_1_t1554155346 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1259009757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1259009757(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1172118166_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1172118166_gshared (InternalEnumerator_1_t1554155346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1172118166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1172118166_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1554155346 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1554155346 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1172118166(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2855123690_gshared (InternalEnumerator_1_t1455007855 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2855123690_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	InternalEnumerator_1__ctor_m2855123690(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1977727635((InternalEnumerator_1_t1455007855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2202140993_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2202140993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2202140993(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2345374502_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2345374502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2345374502(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1977727635_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1977727635_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1977727635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1977727635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1455007855 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1455007855 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1977727635(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2510835690_gshared (InternalEnumerator_1_t2155190829 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2510835690_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1__ctor_m2510835690(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		TableRange_t3372848153  L_0 = InternalEnumerator_1_get_Current_m2306301105((InternalEnumerator_1_t2155190829 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t3372848153  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2516768321(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1609192930(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId;
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_gshared (InternalEnumerator_1_t2155190829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t3372848153  L_8 = ((  TableRange_t3372848153  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t3372848153  InternalEnumerator_1_get_Current_m2306301105_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2155190829 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2155190829 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2306301105(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4155127258_gshared (InternalEnumerator_1_t1949385224 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4155127258_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1__ctor_m4155127258(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3784081185((InternalEnumerator_1_t1949385224 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4263405617(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m569750258(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_gshared (InternalEnumerator_1_t1949385224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1949385224 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1949385224 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3784081185(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3894762810_gshared (InternalEnumerator_1_t798349321 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3894762810_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1__ctor_m3894762810(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		TagName_t2016006645  L_0 = InternalEnumerator_1_get_Current_m1405735267((InternalEnumerator_1_t798349321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t2016006645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1537296273(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3872827350(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId;
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_gshared (InternalEnumerator_1_t798349321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t2016006645  L_8 = ((  TagName_t2016006645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TagName_t2016006645  InternalEnumerator_1_get_Current_m1405735267_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t798349321 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t798349321 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1405735267(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2927575_gshared (InternalEnumerator_1_t2647289085 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2927575_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	InternalEnumerator_1__ctor_m2927575(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2814293097(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method)
{
	{
		JsonPosition_t3864946409  L_0 = InternalEnumerator_1_get_Current_m2332499742((InternalEnumerator_1_t2647289085 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		JsonPosition_t3864946409  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m513586517(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1740621422_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1740621422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1740621422(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m511491285_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m511491285_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m511491285(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonPosition>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2332499742_MetadataUsageId;
extern "C"  JsonPosition_t3864946409  InternalEnumerator_1_get_Current_m2332499742_gshared (InternalEnumerator_1_t2647289085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2332499742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		JsonPosition_t3864946409  L_8 = ((  JsonPosition_t3864946409  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  JsonPosition_t3864946409  InternalEnumerator_1_get_Current_m2332499742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2647289085 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2647289085 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2332499742(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2097196035_gshared (InternalEnumerator_1_t3749301894 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2097196035_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1__ctor_m2097196035(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m37457085(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m904353994((InternalEnumerator_1_t3749301894 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1301492009(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m249679258_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m249679258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	InternalEnumerator_1_Dispose_m249679258(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1045138473_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1045138473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1045138473(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.JsonWriter/State>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m904353994_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m904353994_gshared (InternalEnumerator_1_t3749301894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m904353994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m904353994_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3749301894 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3749301894 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m904353994(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2748237735_gshared (InternalEnumerator_1_t2699240237 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2748237735_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1__ctor_m2748237735(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m301393561(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1002099088((InternalEnumerator_1_t2699240237 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4034811983(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4165690942_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4165690942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4165690942(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3748683465_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3748683465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3748683465(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Linq.JTokenType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1002099088_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1002099088_gshared (InternalEnumerator_1_t2699240237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1002099088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1002099088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2699240237 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2699240237 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1002099088(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4059731926_gshared (InternalEnumerator_1_t2229264188 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4059731926_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	InternalEnumerator_1__ctor_m4059731926(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m316919261((InternalEnumerator_1_t2229264188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m681956653_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m681956653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	InternalEnumerator_1_Dispose_m681956653(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m671618294_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m671618294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m671618294(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m316919261_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m316919261_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m316919261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m316919261_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2229264188 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2229264188 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m316919261(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1440454536_gshared (InternalEnumerator_1_t1754187467 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1440454536_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1__ctor_m1440454536(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2650743064(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = InternalEnumerator_1_get_Current_m3704502415((InternalEnumerator_1_t1754187467 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeNameKey_t2971844791  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20732868(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2321509215_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2321509215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2321509215(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3367673860_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3367673860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3367673860(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3704502415_MetadataUsageId;
extern "C"  TypeNameKey_t2971844791  InternalEnumerator_1_get_Current_m3704502415_gshared (InternalEnumerator_1_t1754187467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3704502415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeNameKey_t2971844791  L_8 = ((  TypeNameKey_t2971844791  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeNameKey_t2971844791  InternalEnumerator_1_get_Current_m3704502415_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1754187467 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1754187467 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3704502415(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m589683351_gshared (InternalEnumerator_1_t1674672166 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m589683351_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1__ctor_m589683351(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517339561(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m4237519616((InternalEnumerator_1_t1674672166 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2188531679(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2699060526_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2699060526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2699060526(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m655721177_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m655721177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m655721177(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4237519616_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m4237519616_gshared (InternalEnumerator_1_t1674672166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4237519616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m4237519616_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1674672166 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1674672166 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4237519616(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m343115129_gshared (InternalEnumerator_1_t3551110977 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m343115129_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	InternalEnumerator_1__ctor_m343115129(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1960371079(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method)
{
	{
		ResolverContractKey_t473801005  L_0 = InternalEnumerator_1_get_Current_m3388255906((InternalEnumerator_1_t3551110977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResolverContractKey_t473801005  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2696353661(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4204389008_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4204389008_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4204389008(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1197756471_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1197756471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1197756471(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3388255906_MetadataUsageId;
extern "C"  ResolverContractKey_t473801005  InternalEnumerator_1_get_Current_m3388255906_gshared (InternalEnumerator_1_t3551110977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3388255906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResolverContractKey_t473801005  L_8 = ((  ResolverContractKey_t473801005  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResolverContractKey_t473801005  InternalEnumerator_1_get_Current_m3388255906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3551110977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3551110977 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3388255906(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m447238051_gshared (InternalEnumerator_1_t3943444146 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m447238051_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1__ctor_m447238051(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3990422813(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = InternalEnumerator_1_get_Current_m624396236((InternalEnumerator_1_t3943444146 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TypeConvertKey_t866134174  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m966888979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2608036154_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2608036154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2608036154(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2535341517_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2535341517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2535341517(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m624396236_MetadataUsageId;
extern "C"  TypeConvertKey_t866134174  InternalEnumerator_1_get_Current_m624396236_gshared (InternalEnumerator_1_t3943444146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m624396236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TypeConvertKey_t866134174  L_8 = ((  TypeConvertKey_t866134174  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TypeConvertKey_t866134174  InternalEnumerator_1_get_Current_m624396236_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3943444146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3943444146 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m624396236(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m708140506_gshared (InternalEnumerator_1_t1211634336 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m708140506_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	InternalEnumerator_1__ctor_m708140506(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m782910854(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3936493699((InternalEnumerator_1_t1211634336 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2507540988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2767949873_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2767949873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2767949873(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1301430326_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1301430326_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1301430326(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3936493699_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3936493699_gshared (InternalEnumerator_1_t1211634336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3936493699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3936493699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1211634336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1211634336 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3936493699(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1191579514_gshared (InternalEnumerator_1_t970376284 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1191579514_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1__ctor_m1191579514(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2188033608  L_0 = InternalEnumerator_1_get_Current_m3176144321((InternalEnumerator_1_t970376284 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2188033608  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2416953809_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2416953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2416953809(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m552394578_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m552394578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m552394578(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId;
extern "C"  ArraySegment_1_t2188033608  InternalEnumerator_1_get_Current_m3176144321_gshared (InternalEnumerator_1_t970376284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2188033608  L_8 = ((  ArraySegment_1_t2188033608  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2188033608  InternalEnumerator_1_get_Current_m3176144321_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t970376284 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t970376284 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3176144321(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3904876858_gshared (InternalEnumerator_1_t3554108690 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3904876858_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1__ctor_m3904876858(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m94889217((InternalEnumerator_1_t3554108690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2505350033(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2858864786(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m94889217_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_gshared (InternalEnumerator_1_t3554108690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m94889217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3554108690 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3554108690 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m94889217(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2342462508_gshared (InternalEnumerator_1_t1644952336 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2342462508_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1__ctor_m2342462508(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4103229525((InternalEnumerator_1_t1644952336 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1621603587(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3669835172(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_gshared (InternalEnumerator_1_t1644952336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644952336 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644952336 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4103229525(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3601500154_gshared (InternalEnumerator_1_t1644965214 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3601500154_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1__ctor_m3601500154(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m3048220323((InternalEnumerator_1_t1644965214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2748899921(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4256283158(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m3048220323_gshared (InternalEnumerator_1_t1644965214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m3048220323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1644965214 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1644965214 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3048220323(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m263261269_gshared (InternalEnumerator_1_t533949290 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m263261269_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1__ctor_m263261269(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t1751606614  L_0 = InternalEnumerator_1_get_Current_m2915343068((InternalEnumerator_1_t533949290 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1282215020(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4030197783(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  InternalEnumerator_1_get_Current_m2915343068_gshared (InternalEnumerator_1_t533949290 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t1751606614  L_8 = ((  DictionaryEntry_t1751606614  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t1751606614  InternalEnumerator_1_get_Current_m2915343068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t533949290 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t533949290 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2915343068(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1146423754_gshared (InternalEnumerator_1_t4166879682 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1146423754_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	InternalEnumerator_1__ctor_m1146423754(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m202748822(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method)
{
	{
		Link_t1089569710  L_0 = InternalEnumerator_1_get_Current_m909351953((InternalEnumerator_1_t4166879682 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t1089569710  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3181075074(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3029624865_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3029624865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3029624865(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4096989954_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4096989954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4096989954(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m909351953_MetadataUsageId;
extern "C"  Link_t1089569710  InternalEnumerator_1_get_Current_m909351953_gshared (InternalEnumerator_1_t4166879682 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m909351953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t1089569710  L_8 = ((  Link_t1089569710  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t1089569710  InternalEnumerator_1_get_Current_m909351953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4166879682 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4166879682 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m909351953(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2175399139_gshared (InternalEnumerator_1_t3891715294 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2175399139_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	InternalEnumerator_1__ctor_m2175399139(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method)
{
	{
		Link_t814405322  L_0 = InternalEnumerator_1_get_Current_m3975311466((InternalEnumerator_1_t3891715294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t814405322  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3422989946_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3422989946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3422989946(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3932259529_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3932259529_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3932259529(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3975311466_MetadataUsageId;
extern "C"  Link_t814405322  InternalEnumerator_1_get_Current_m3975311466_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3975311466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t814405322  L_8 = ((  Link_t814405322  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t814405322  InternalEnumerator_1_get_Current_m3975311466_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3891715294 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3891715294 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3975311466(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2684410586_gshared (InternalEnumerator_1_t904941831 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2684410586_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1__ctor_m2684410586(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		Link_t2122599155  L_0 = InternalEnumerator_1_get_Current_m13756385((InternalEnumerator_1_t904941831 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2122599155  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3733933361_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3733933361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3733933361(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3124962674_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3124962674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3124962674(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m13756385_MetadataUsageId;
extern "C"  Link_t2122599155  InternalEnumerator_1_get_Current_m13756385_gshared (InternalEnumerator_1_t904941831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m13756385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2122599155  L_8 = ((  Link_t2122599155  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2122599155  InternalEnumerator_1_get_Current_m13756385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t904941831 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t904941831 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m13756385(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m977979401_gshared (InternalEnumerator_1_t1859153240 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m977979401_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	InternalEnumerator_1__ctor_m977979401(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3076810564  L_0 = InternalEnumerator_1_get_Current_m1383438002((InternalEnumerator_1_t1859153240 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3076810564  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3348633888_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3348633888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3348633888(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2462826151_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2462826151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2462826151(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1383438002_MetadataUsageId;
extern "C"  KeyValuePair_2_t3076810564  InternalEnumerator_1_get_Current_m1383438002_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1383438002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3076810564  L_8 = ((  KeyValuePair_2_t3076810564  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3076810564  InternalEnumerator_1_get_Current_m1383438002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1859153240 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1859153240 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1383438002(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2233519467_gshared (InternalEnumerator_1_t2795846257 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2233519467_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	InternalEnumerator_1__ctor_m2233519467(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3162301013_gshared (InternalEnumerator_1_t2795846257 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3162301013_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3162301013(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4083217483_gshared (InternalEnumerator_1_t2795846257 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4013503581  L_0 = InternalEnumerator_1_get_Current_m695894676((InternalEnumerator_1_t2795846257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4013503581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4083217483_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4083217483(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2920354050_gshared (InternalEnumerator_1_t2795846257 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2920354050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2920354050(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3150277893_gshared (InternalEnumerator_1_t2795846257 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3150277893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3150277893(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m695894676_MetadataUsageId;
extern "C"  KeyValuePair_2_t4013503581  InternalEnumerator_1_get_Current_m695894676_gshared (InternalEnumerator_1_t2795846257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m695894676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4013503581  L_8 = ((  KeyValuePair_2_t4013503581  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4013503581  InternalEnumerator_1_get_Current_m695894676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795846257 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795846257 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m695894676(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m270858264_gshared (InternalEnumerator_1_t1676274531 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m270858264_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	InternalEnumerator_1__ctor_m270858264(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2893931855  L_0 = InternalEnumerator_1_get_Current_m3404344799((InternalEnumerator_1_t1676274531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2893931855  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m272449519_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m272449519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	InternalEnumerator_1_Dispose_m272449519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2936217332_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2936217332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2936217332(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3404344799_MetadataUsageId;
extern "C"  KeyValuePair_2_t2893931855  InternalEnumerator_1_get_Current_m3404344799_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3404344799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2893931855  L_8 = ((  KeyValuePair_2_t2893931855  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2893931855  InternalEnumerator_1_get_Current_m3404344799_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1676274531 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1676274531 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3404344799(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1207608622_gshared (InternalEnumerator_1_t1598245646 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1207608622_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	InternalEnumerator_1__ctor_m1207608622(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3428054450_gshared (InternalEnumerator_1_t1598245646 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3428054450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3428054450(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2391527198_gshared (InternalEnumerator_1_t1598245646 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2815902970  L_0 = InternalEnumerator_1_get_Current_m1498262517((InternalEnumerator_1_t1598245646 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2815902970  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2391527198_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2391527198(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1920872581_gshared (InternalEnumerator_1_t1598245646 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1920872581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1920872581(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m508457502_gshared (InternalEnumerator_1_t1598245646 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m508457502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m508457502(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1498262517_MetadataUsageId;
extern "C"  KeyValuePair_2_t2815902970  InternalEnumerator_1_get_Current_m1498262517_gshared (InternalEnumerator_1_t1598245646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1498262517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2815902970  L_8 = ((  KeyValuePair_2_t2815902970  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2815902970  InternalEnumerator_1_get_Current_m1498262517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1598245646 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1598245646 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1498262517(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2135273650_gshared (InternalEnumerator_1_t4127192417 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2135273650_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1__ctor_m2135273650(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1049882445  L_0 = InternalEnumerator_1_get_Current_m2170878265((InternalEnumerator_1_t4127192417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1049882445  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1660227849_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1660227849_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1660227849(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m47820698_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m47820698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m47820698(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId;
extern "C"  KeyValuePair_2_t1049882445  InternalEnumerator_1_get_Current_m2170878265_gshared (InternalEnumerator_1_t4127192417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1049882445  L_8 = ((  KeyValuePair_2_t1049882445  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1049882445  InternalEnumerator_1_get_Current_m2170878265_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4127192417 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4127192417 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2170878265(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m664150035_gshared (InternalEnumerator_1_t2849202992 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m664150035_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1__ctor_m664150035(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4066860316  L_0 = InternalEnumerator_1_get_Current_m818645564((InternalEnumerator_1_t2849202992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4066860316  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m233182634_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m233182634_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	InternalEnumerator_1_Dispose_m233182634(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m44493661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m818645564_MetadataUsageId;
extern "C"  KeyValuePair_2_t4066860316  InternalEnumerator_1_get_Current_m818645564_gshared (InternalEnumerator_1_t2849202992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m818645564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4066860316  L_8 = ((  KeyValuePair_2_t4066860316  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4066860316  InternalEnumerator_1_get_Current_m818645564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2849202992 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2849202992 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m818645564(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2858706882_gshared (InternalEnumerator_1_t3178380060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2858706882_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	InternalEnumerator_1__ctor_m2858706882(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773410462(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t101070088  L_0 = InternalEnumerator_1_get_Current_m3580605257((InternalEnumerator_1_t3178380060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t101070088  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m225993674(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1571445785_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1571445785_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1571445785(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3159979146_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3159979146_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3159979146(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3580605257_MetadataUsageId;
extern "C"  KeyValuePair_2_t101070088  InternalEnumerator_1_get_Current_m3580605257_gshared (InternalEnumerator_1_t3178380060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3580605257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t101070088  L_8 = ((  KeyValuePair_2_t101070088  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t101070088  InternalEnumerator_1_get_Current_m3580605257_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3178380060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3178380060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3580605257(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1571587521_gshared (InternalEnumerator_1_t3116794 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1571587521_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	InternalEnumerator_1__ctor_m1571587521(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1220774118  L_0 = InternalEnumerator_1_get_Current_m850510058((InternalEnumerator_1_t3116794 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1220774118  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2546015448_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2546015448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2546015448(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m100128495_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m100128495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m100128495(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m850510058_MetadataUsageId;
extern "C"  KeyValuePair_2_t1220774118  InternalEnumerator_1_get_Current_m850510058_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m850510058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1220774118  L_8 = ((  KeyValuePair_2_t1220774118  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1220774118  InternalEnumerator_1_get_Current_m850510058_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3116794 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3116794 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m850510058(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m792053498_gshared (InternalEnumerator_1_t3743492068 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m792053498_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	InternalEnumerator_1__ctor_m792053498(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1590823014_gshared (InternalEnumerator_1_t3743492068 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1590823014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1590823014(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1630615698_gshared (InternalEnumerator_1_t3743492068 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t666182096  L_0 = InternalEnumerator_1_get_Current_m3681152385((InternalEnumerator_1_t3743492068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t666182096  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1630615698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1630615698(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3063341393_gshared (InternalEnumerator_1_t3743492068 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3063341393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3063341393(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2296707154_gshared (InternalEnumerator_1_t3743492068 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2296707154_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2296707154(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3681152385_MetadataUsageId;
extern "C"  KeyValuePair_2_t666182096  InternalEnumerator_1_get_Current_m3681152385_gshared (InternalEnumerator_1_t3743492068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3681152385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t666182096  L_8 = ((  KeyValuePair_2_t666182096  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t666182096  InternalEnumerator_1_get_Current_m3681152385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3743492068 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3743492068 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3681152385(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4101758999_gshared (InternalEnumerator_1_t3280454238 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4101758999_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	InternalEnumerator_1__ctor_m4101758999(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m588290089(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t203144266  L_0 = InternalEnumerator_1_get_Current_m170531934((InternalEnumerator_1_t3280454238 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t203144266  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2307724821(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m796347054_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m796347054_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	InternalEnumerator_1_Dispose_m796347054(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3714593429_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3714593429_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3714593429(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m170531934_MetadataUsageId;
extern "C"  KeyValuePair_2_t203144266  InternalEnumerator_1_get_Current_m170531934_gshared (InternalEnumerator_1_t3280454238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m170531934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t203144266  L_8 = ((  KeyValuePair_2_t203144266  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t203144266  InternalEnumerator_1_get_Current_m170531934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3280454238 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3280454238 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m170531934(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2662086813_gshared (InternalEnumerator_1_t1327961296 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2662086813_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1__ctor_m2662086813(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2545618620  L_0 = InternalEnumerator_1_get_Current_m1242840582((InternalEnumerator_1_t1327961296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2545618620  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1742566068(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3398874899(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId;
extern "C"  KeyValuePair_2_t2545618620  InternalEnumerator_1_get_Current_m1242840582_gshared (InternalEnumerator_1_t1327961296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2545618620  L_8 = ((  KeyValuePair_2_t2545618620  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2545618620  InternalEnumerator_1_get_Current_m1242840582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1327961296 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1327961296 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1242840582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2624907895_gshared (InternalEnumerator_1_t2005001078 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2624907895_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1__ctor_m2624907895(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3222658402  L_0 = InternalEnumerator_1_get_Current_m1033564064((InternalEnumerator_1_t2005001078 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3222658402  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1793576206(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1172054137(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId;
extern "C"  KeyValuePair_2_t3222658402  InternalEnumerator_1_get_Current_m1033564064_gshared (InternalEnumerator_1_t2005001078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3222658402  L_8 = ((  KeyValuePair_2_t3222658402  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3222658402  InternalEnumerator_1_get_Current_m1033564064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005001078 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005001078 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1033564064(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2957909742_gshared (InternalEnumerator_1_t727011653 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2957909742_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1__ctor_m2957909742(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1944668977  L_0 = InternalEnumerator_1_get_Current_m4216610997((InternalEnumerator_1_t727011653 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1944668977  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m72014405_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m72014405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	InternalEnumerator_1_Dispose_m72014405(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m535991902(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId;
extern "C"  KeyValuePair_2_t1944668977  InternalEnumerator_1_get_Current_m4216610997_gshared (InternalEnumerator_1_t727011653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1944668977  L_8 = ((  KeyValuePair_2_t1944668977  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1944668977  InternalEnumerator_1_get_Current_m4216610997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t727011653 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t727011653 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4216610997(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3050775493_gshared (InternalEnumerator_1_t848114254 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3050775493_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1__ctor_m3050775493(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2065771578  L_0 = InternalEnumerator_1_get_Current_m2312868556((InternalEnumerator_1_t848114254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2065771578  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2373234652_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2373234652_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2373234652(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3154342823_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3154342823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3154342823(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2312868556_MetadataUsageId;
extern "C"  KeyValuePair_2_t2065771578  InternalEnumerator_1_get_Current_m2312868556_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2312868556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2065771578  L_8 = ((  KeyValuePair_2_t2065771578  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2065771578  InternalEnumerator_1_get_Current_m2312868556_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t848114254 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t848114254 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2312868556(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1666096596_gshared (InternalEnumerator_1_t875830559 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1666096596_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	InternalEnumerator_1__ctor_m1666096596(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1287520076(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2093487883  L_0 = InternalEnumerator_1_get_Current_m482313243((InternalEnumerator_1_t875830559 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2093487883  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3007595576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m166437035_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m166437035_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	InternalEnumerator_1_Dispose_m166437035(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3463093432_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3463093432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3463093432(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m482313243_MetadataUsageId;
extern "C"  KeyValuePair_2_t2093487883  InternalEnumerator_1_get_Current_m482313243_gshared (InternalEnumerator_1_t875830559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m482313243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2093487883  L_8 = ((  KeyValuePair_2_t2093487883  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2093487883  InternalEnumerator_1_get_Current_m482313243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t875830559 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t875830559 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m482313243(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2938744211_gshared (InternalEnumerator_1_t2225906962 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2938744211_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	InternalEnumerator_1__ctor_m2938744211(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3528556845(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3443564286  L_0 = InternalEnumerator_1_get_Current_m332770876((InternalEnumerator_1_t2225906962 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3443564286  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m405163171(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4035216170_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4035216170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4035216170(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3315677917_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3315677917_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3315677917(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m332770876_MetadataUsageId;
extern "C"  KeyValuePair_2_t3443564286  InternalEnumerator_1_get_Current_m332770876_gshared (InternalEnumerator_1_t2225906962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m332770876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3443564286  L_8 = ((  KeyValuePair_2_t3443564286  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3443564286  InternalEnumerator_1_get_Current_m332770876_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2225906962 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2225906962 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m332770876(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2318391222_gshared (InternalEnumerator_1_t702156392 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2318391222_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	InternalEnumerator_1__ctor_m2318391222(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1440179754_gshared (InternalEnumerator_1_t702156392 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1440179754_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1440179754(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445212950_gshared (InternalEnumerator_1_t702156392 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1919813716  L_0 = InternalEnumerator_1_get_Current_m2754542077((InternalEnumerator_1_t702156392 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1919813716  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445212950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445212950(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m579824909_gshared (InternalEnumerator_1_t702156392 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m579824909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	InternalEnumerator_1_Dispose_m579824909(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1060688278_gshared (InternalEnumerator_1_t702156392 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1060688278_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1060688278(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2754542077_MetadataUsageId;
extern "C"  KeyValuePair_2_t1919813716  InternalEnumerator_1_get_Current_m2754542077_gshared (InternalEnumerator_1_t702156392 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2754542077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1919813716  L_8 = ((  KeyValuePair_2_t1919813716  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1919813716  InternalEnumerator_1_get_Current_m2754542077_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t702156392 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t702156392 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2754542077(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1014499487_gshared (InternalEnumerator_1_t3377690443 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1014499487_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	InternalEnumerator_1__ctor_m1014499487(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t300380471  L_0 = InternalEnumerator_1_get_Current_m2391040072((InternalEnumerator_1_t3377690443 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t300380471  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3983211830_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3983211830_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3983211830(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m813678161_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m813678161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m813678161(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2391040072_MetadataUsageId;
extern "C"  KeyValuePair_2_t300380471  InternalEnumerator_1_get_Current_m2391040072_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2391040072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t300380471  L_8 = ((  KeyValuePair_2_t300380471  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t300380471  InternalEnumerator_1_get_Current_m2391040072_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3377690443 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3377690443 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2391040072(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1865178318_gshared (InternalEnumerator_1_t846010146 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1865178318_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1__ctor_m1865178318(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		Link_t2063667470  L_0 = InternalEnumerator_1_get_Current_m1839009783((InternalEnumerator_1_t846010146 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2063667470  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1986195493(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m608833986(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId;
extern "C"  Link_t2063667470  InternalEnumerator_1_get_Current_m1839009783_gshared (InternalEnumerator_1_t846010146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2063667470  L_8 = ((  Link_t2063667470  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2063667470  InternalEnumerator_1_get_Current_m1839009783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t846010146 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t846010146 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1839009783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1735201994_gshared (InternalEnumerator_1_t1042872857 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1735201994_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1__ctor_m1735201994(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		Slot_t2260530181  L_0 = InternalEnumerator_1_get_Current_m3036426419((InternalEnumerator_1_t1042872857 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2260530181  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2838377249(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2675725766(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId;
extern "C"  Slot_t2260530181  InternalEnumerator_1_get_Current_m3036426419_gshared (InternalEnumerator_1_t1042872857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2260530181  L_8 = ((  Slot_t2260530181  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2260530181  InternalEnumerator_1_get_Current_m3036426419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1042872857 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1042872857 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3036426419(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m831950091_gshared (InternalEnumerator_1_t854365966 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m831950091_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1__ctor_m831950091(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		Slot_t2072023290  L_0 = InternalEnumerator_1_get_Current_m2267962386((InternalEnumerator_1_t854365966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2072023290  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m857987234_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m857987234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	InternalEnumerator_1_Dispose_m857987234(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3764038305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId;
extern "C"  Slot_t2072023290  InternalEnumerator_1_get_Current_m2267962386_gshared (InternalEnumerator_1_t854365966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2072023290  L_8 = ((  Slot_t2072023290  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2072023290  InternalEnumerator_1_get_Current_m2267962386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t854365966 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t854365966 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2267962386(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3756518911_gshared (InternalEnumerator_1_t3066004003 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3756518911_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1__ctor_m3756518911(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = InternalEnumerator_1_get_Current_m4103732200((InternalEnumerator_1_t3066004003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t4283661327  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1192762006(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m556104049(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId;
extern "C"  DateTime_t4283661327  InternalEnumerator_1_get_Current_m4103732200_gshared (InternalEnumerator_1_t3066004003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t4283661327  L_8 = ((  DateTime_t4283661327  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DateTime_t4283661327  InternalEnumerator_1_get_Current_m4103732200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3066004003 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3066004003 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4103732200(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868618147_gshared (InternalEnumerator_1_t736693307 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2868618147_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1__ctor_m2868618147(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		Decimal_t1954350631  L_0 = InternalEnumerator_1_get_Current_m3602913834((InternalEnumerator_1_t736693307 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t1954350631  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m136301882_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m136301882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	InternalEnumerator_1_Dispose_m136301882(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2432816137(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId;
extern "C"  Decimal_t1954350631  InternalEnumerator_1_get_Current_m3602913834_gshared (InternalEnumerator_1_t736693307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t1954350631  L_8 = ((  Decimal_t1954350631  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Decimal_t1954350631  InternalEnumerator_1_get_Current_m3602913834_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t736693307 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t736693307 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3602913834(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m923076597_gshared (InternalEnumerator_1_t2650569241 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m923076597_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1__ctor_m923076597(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m403053214((InternalEnumerator_1_t2650569241 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3854110732(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3253414715(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m403053214_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m403053214_gshared (InternalEnumerator_1_t2650569241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m403053214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m403053214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2650569241 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2650569241 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m403053214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1585494054_gshared (InternalEnumerator_1_t4231148414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1585494054_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1__ctor_m1585494054(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m296300717((InternalEnumerator_1_t4231148414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2583239037(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3253274534(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m296300717_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_gshared (InternalEnumerator_1_t4231148414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m296300717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m296300717(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m767389920_gshared (InternalEnumerator_1_t4231148472 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m767389920_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1__ctor_m767389920(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1478851815((InternalEnumerator_1_t4231148472 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2745733815(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3995645356(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_gshared (InternalEnumerator_1_t4231148472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148472 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148472 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1478851815(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3055898623_gshared (InternalEnumerator_1_t4231148567 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3055898623_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1__ctor_m3055898623(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m1194254150((InternalEnumerator_1_t4231148567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m642251926_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m642251926_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	InternalEnumerator_1_Dispose_m642251926(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3212216237(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_gshared (InternalEnumerator_1_t4231148567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4231148567 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4231148567 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1194254150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m152804899_gshared (InternalEnumerator_1_t2792744647 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m152804899_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1__ctor_m152804899(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m2342284108((InternalEnumerator_1_t2792744647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1686038458(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m467683661(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_gshared (InternalEnumerator_1_t2792744647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2792744647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2792744647 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2342284108(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2616641763_gshared (InternalEnumerator_1_t2953159047 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2616641763_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1__ctor_m2616641763(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m2178852364((InternalEnumerator_1_t2953159047 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2760671866(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3716548237(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_gshared (InternalEnumerator_1_t2953159047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2953159047 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2953159047 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2178852364(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2025782336_gshared (InternalEnumerator_1_t1841955665 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2025782336_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1__ctor_m2025782336(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = InternalEnumerator_1_get_Current_m2477961351((InternalEnumerator_1_t1841955665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t3059612989  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3114194455(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1042509516(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  InternalEnumerator_1_get_Current_m2477961351_gshared (InternalEnumerator_1_t1841955665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t3059612989  L_8 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t3059612989  InternalEnumerator_1_get_Current_m2477961351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1841955665 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1841955665 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2477961351(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m374673841_gshared (InternalEnumerator_1_t2083636098 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m374673841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1__ctor_m374673841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = InternalEnumerator_1_get_Current_m3407736504((InternalEnumerator_1_t2083636098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t3301293422  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1367004360(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2714191419(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  InternalEnumerator_1_get_Current_m3407736504_gshared (InternalEnumerator_1_t2083636098 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t3301293422  L_8 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t3301293422  InternalEnumerator_1_get_Current_m3407736504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2083636098 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2083636098 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3407736504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238137273_gshared (InternalEnumerator_1_t1990166460 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m238137273_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1__ctor_m238137273(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		LabelData_t3207823784  L_0 = InternalEnumerator_1_get_Current_m2308068992((InternalEnumerator_1_t1990166460 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t3207823784  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3252411600(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4284495539(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId;
extern "C"  LabelData_t3207823784  InternalEnumerator_1_get_Current_m2308068992_gshared (InternalEnumerator_1_t1990166460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t3207823784  L_8 = ((  LabelData_t3207823784  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelData_t3207823784  InternalEnumerator_1_get_Current_m2308068992_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1990166460 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1990166460 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2308068992(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4080636215_gshared (InternalEnumerator_1_t3737689414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4080636215_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1__ctor_m4080636215(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t660379442  L_0 = InternalEnumerator_1_get_Current_m3571284320((InternalEnumerator_1_t3737689414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t660379442  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2169103310(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1336166329(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId;
extern "C"  LabelFixup_t660379442  InternalEnumerator_1_get_Current_m3571284320_gshared (InternalEnumerator_1_t3737689414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t660379442  L_8 = ((  LabelFixup_t660379442  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelFixup_t660379442  InternalEnumerator_1_get_Current_m3571284320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3737689414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3737689414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3571284320(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2480959198_gshared (InternalEnumerator_1_t136423630 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2480959198_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1__ctor_m2480959198(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t1354080954  L_0 = InternalEnumerator_1_get_Current_m3618560037((InternalEnumerator_1_t136423630 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t1354080954  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1771263541(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2010832750(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId;
extern "C"  ILTokenInfo_t1354080954  InternalEnumerator_1_get_Current_m3618560037_gshared (InternalEnumerator_1_t136423630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t1354080954  L_8 = ((  ILTokenInfo_t1354080954  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t1354080954  InternalEnumerator_1_get_Current_m3618560037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t136423630 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t136423630 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3618560037(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2078231777_gshared (InternalEnumerator_1_t3819239998 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2078231777_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1__ctor_m2078231777(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t741930026  L_0 = InternalEnumerator_1_get_Current_m147444170((InternalEnumerator_1_t3819239998 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t741930026  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2387364856(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3792346959(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m147444170_MetadataUsageId;
extern "C"  ParameterModifier_t741930026  InternalEnumerator_1_get_Current_m147444170_gshared (InternalEnumerator_1_t3819239998 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m147444170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t741930026  L_8 = ((  ParameterModifier_t741930026  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ParameterModifier_t741930026  InternalEnumerator_1_get_Current_m147444170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3819239998 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3819239998 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m147444170(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2772733110_gshared (InternalEnumerator_1_t896245509 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2772733110_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1__ctor_m2772733110(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t2113902833  L_0 = InternalEnumerator_1_get_Current_m4042729375((InternalEnumerator_1_t896245509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t2113902833  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m420635149_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m420635149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	InternalEnumerator_1_Dispose_m420635149(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4160471130(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId;
extern "C"  ResourceCacheItem_t2113902833  InternalEnumerator_1_get_Current_m4042729375_gshared (InternalEnumerator_1_t896245509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t2113902833  L_8 = ((  ResourceCacheItem_t2113902833  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceCacheItem_t2113902833  InternalEnumerator_1_get_Current_m4042729375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t896245509 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t896245509 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4042729375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2865872515_gshared (InternalEnumerator_1_t2795948550 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2865872515_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1__ctor_m2865872515(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t4013605874  L_0 = InternalEnumerator_1_get_Current_m3891250378((InternalEnumerator_1_t2795948550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t4013605874  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3639607322(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3402661033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId;
extern "C"  ResourceInfo_t4013605874  InternalEnumerator_1_get_Current_m3891250378_gshared (InternalEnumerator_1_t2795948550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t4013605874  L_8 = ((  ResourceInfo_t4013605874  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceInfo_t4013605874  InternalEnumerator_1_get_Current_m3891250378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2795948550 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2795948550 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3891250378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3958787668_gshared (InternalEnumerator_1_t563161977 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3958787668_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	InternalEnumerator_1__ctor_m3958787668(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2969796300(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = InternalEnumerator_1_get_Current_m578280667((InternalEnumerator_1_t563161977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HandleRef_t1780819301  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m805552376(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2933780779_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2933780779_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2933780779(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m89275576_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m89275576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m89275576(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.HandleRef>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m578280667_MetadataUsageId;
extern "C"  HandleRef_t1780819301  InternalEnumerator_1_get_Current_m578280667_gshared (InternalEnumerator_1_t563161977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m578280667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HandleRef_t1780819301  L_8 = ((  HandleRef_t1780819301  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  HandleRef_t1780819301  InternalEnumerator_1_get_Current_m578280667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t563161977 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t563161977 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m578280667(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2400880886_gshared (InternalEnumerator_1_t1203046106 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2400880886_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1__ctor_m2400880886(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m790384317((InternalEnumerator_1_t1203046106 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m44740173_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m44740173_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	InternalEnumerator_1_Dispose_m44740173(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2285731670(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m790384317_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_gshared (InternalEnumerator_1_t1203046106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m790384317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1203046106 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1203046106 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m790384317(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1925586605_gshared (InternalEnumerator_1_t4239079749 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1925586605_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1__ctor_m1925586605(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m92522612((InternalEnumerator_1_t4239079749 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2750196932(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4134001983(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m92522612_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_gshared (InternalEnumerator_1_t4239079749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m92522612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4239079749 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4239079749 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m92522612(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3415441081_gshared (InternalEnumerator_1_t3844211903 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3415441081_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1__ctor_m3415441081(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t766901931  L_0 = InternalEnumerator_1_get_Current_m2419281506((InternalEnumerator_1_t3844211903 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t766901931  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2305059792(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1811839991(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId;
extern "C"  X509ChainStatus_t766901931  InternalEnumerator_1_get_Current_m2419281506_gshared (InternalEnumerator_1_t3844211903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t766901931  L_8 = ((  X509ChainStatus_t766901931  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t766901931  InternalEnumerator_1_get_Current_m2419281506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3844211903 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3844211903 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2419281506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2203995436_gshared (InternalEnumerator_1_t3074261648 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2203995436_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1__ctor_m2203995436(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m1563251989((InternalEnumerator_1_t3074261648 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m479600131_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m479600131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	InternalEnumerator_1_Dispose_m479600131(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1722801188(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_gshared (InternalEnumerator_1_t3074261648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3074261648 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3074261648 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1563251989(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m207626719_gshared (InternalEnumerator_1_t2593882473 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m207626719_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1__ctor_m207626719(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		Mark_t3811539797  L_0 = InternalEnumerator_1_get_Current_m3956653384((InternalEnumerator_1_t2593882473 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t3811539797  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1252370038(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2967245969(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId;
extern "C"  Mark_t3811539797  InternalEnumerator_1_get_Current_m3956653384_gshared (InternalEnumerator_1_t2593882473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t3811539797  L_8 = ((  Mark_t3811539797  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t3811539797  InternalEnumerator_1_get_Current_m3956653384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2593882473 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2593882473 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3956653384(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m111087899_gshared (InternalEnumerator_1_t3490832959 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m111087899_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1__ctor_m111087899(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t413522987  L_0 = InternalEnumerator_1_get_Current_m2948637700((InternalEnumerator_1_t3490832959 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t413522987  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m774844594_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m774844594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	InternalEnumerator_1_Dispose_m774844594(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m485566165(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId;
extern "C"  TimeSpan_t413522987  InternalEnumerator_1_get_Current_m2948637700_gshared (InternalEnumerator_1_t3490832959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t413522987  L_8 = ((  TimeSpan_t413522987  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t413522987  InternalEnumerator_1_get_Current_m2948637700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490832959 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490832959 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2948637700(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1037769859_gshared (InternalEnumerator_1_t3101977895 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1037769859_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1__ctor_m1037769859(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m737292716((InternalEnumerator_1_t3101977895 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1215749658(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3068600045(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m737292716_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_gshared (InternalEnumerator_1_t3101977895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m737292716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977895 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977895 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m737292716(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m219665725_gshared (InternalEnumerator_1_t3101977953 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m219665725_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1__ctor_m219665725(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m1919843814((InternalEnumerator_1_t3101977953 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1378244436(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3810970867(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_gshared (InternalEnumerator_1_t3101977953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101977953 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101977953 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1919843814(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2508174428_gshared (InternalEnumerator_1_t3101978048 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2508174428_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1__ctor_m2508174428(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m1635246149((InternalEnumerator_1_t3101978048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3569729843(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3027541748(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_gshared (InternalEnumerator_1_t3101978048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3101978048 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3101978048 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1635246149(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2879257728_gshared (InternalEnumerator_1_t73011651 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2879257728_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1__ctor_m2879257728(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		UriScheme_t1290668975  L_0 = InternalEnumerator_1_get_Current_m3881062791((InternalEnumerator_1_t73011651 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t1290668975  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1861559895(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m91355148(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId;
extern "C"  UriScheme_t1290668975  InternalEnumerator_1_get_Current_m3881062791_gshared (InternalEnumerator_1_t73011651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t1290668975  L_8 = ((  UriScheme_t1290668975  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t1290668975  InternalEnumerator_1_get_Current_m3881062791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t73011651 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t73011651 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3881062791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2145162288_gshared (InternalEnumerator_1_t2440554239 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2145162288_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1__ctor_m2145162288(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		NsDecl_t3658211563  L_0 = InternalEnumerator_1_get_Current_m105405465((InternalEnumerator_1_t2440554239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsDecl_t3658211563  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3530332679_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3530332679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3530332679(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1353284256_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1353284256_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1353284256(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m105405465_MetadataUsageId;
extern "C"  NsDecl_t3658211563  InternalEnumerator_1_get_Current_m105405465_gshared (InternalEnumerator_1_t2440554239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m105405465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsDecl_t3658211563  L_8 = ((  NsDecl_t3658211563  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsDecl_t3658211563  InternalEnumerator_1_get_Current_m105405465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2440554239 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2440554239 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m105405465(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3201546884_gshared (InternalEnumerator_1_t531556423 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3201546884_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1__ctor_m3201546884(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		NsScope_t1749213747  L_0 = InternalEnumerator_1_get_Current_m860004555((InternalEnumerator_1_t531556423 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsScope_t1749213747  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1025235291_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1025235291_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1025235291(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3063542280_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3063542280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3063542280(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m860004555_MetadataUsageId;
extern "C"  NsScope_t1749213747  InternalEnumerator_1_get_Current_m860004555_gshared (InternalEnumerator_1_t531556423 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m860004555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsScope_t1749213747  L_8 = ((  NsScope_t1749213747  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NsScope_t1749213747  InternalEnumerator_1_get_Current_m860004555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t531556423 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t531556423 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m860004555(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1927827934_gshared (InternalEnumerator_1_t3594029982 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1927827934_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	InternalEnumerator_1__ctor_m1927827934(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842_gshared (InternalEnumerator_1_t3594029982 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3549571842(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950_gshared (InternalEnumerator_1_t3594029982 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m2223718117((InternalEnumerator_1_t3594029982 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2701739950(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1938487605_gshared (InternalEnumerator_1_t3594029982 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1938487605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1938487605(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1858589678_gshared (InternalEnumerator_1_t3594029982 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1858589678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1858589678(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Xml.XPath.XPathResultType>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2223718117_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m2223718117_gshared (InternalEnumerator_1_t3594029982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2223718117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m2223718117_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3594029982 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3594029982 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2223718117(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1839379985_gshared (InternalEnumerator_1_t2976889581 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1839379985_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1__ctor_m1839379985(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		Color_t4194546905  L_0 = InternalEnumerator_1_get_Current_m3730699578((InternalEnumerator_1_t2976889581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t4194546905  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m643330344_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m643330344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	InternalEnumerator_1_Dispose_m643330344(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1891900575(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId;
extern "C"  Color_t4194546905  InternalEnumerator_1_get_Current_m3730699578_gshared (InternalEnumerator_1_t2976889581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t4194546905  L_8 = ((  Color_t4194546905  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color_t4194546905  InternalEnumerator_1_get_Current_m3730699578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2976889581 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2976889581 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3730699578(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2826013104_gshared (InternalEnumerator_1_t3676163660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2826013104_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1__ctor_m2826013104(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = InternalEnumerator_1_get_Current_m3648321497((InternalEnumerator_1_t3676163660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t598853688  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4247678855(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1461072288(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId;
extern "C"  Color32_t598853688  InternalEnumerator_1_get_Current_m3648321497_gshared (InternalEnumerator_1_t3676163660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t598853688  L_8 = ((  Color32_t598853688  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Color32_t598853688  InternalEnumerator_1_get_Current_m3648321497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3676163660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3676163660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3648321497(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2847645656_gshared (InternalEnumerator_1_t3320393320 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2847645656_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1__ctor_m2847645656(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t243083348  L_0 = InternalEnumerator_1_get_Current_m3523444575((InternalEnumerator_1_t3320393320 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t243083348  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2261686191(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2398593716(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId;
extern "C"  ContactPoint_t243083348  InternalEnumerator_1_get_Current_m3523444575_gshared (InternalEnumerator_1_t3320393320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t243083348  L_8 = ((  ContactPoint_t243083348  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint_t243083348  InternalEnumerator_1_get_Current_m3523444575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3320393320 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3320393320 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3523444575(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2104644970_gshared (InternalEnumerator_1_t3070775034 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2104644970_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	InternalEnumerator_1__ctor_m2104644970(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2865529846_gshared (InternalEnumerator_1_t3070775034 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2865529846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2865529846(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1836373474_gshared (InternalEnumerator_1_t3070775034 * __this, const MethodInfo* method)
{
	{
		ContactPoint2D_t4288432358  L_0 = InternalEnumerator_1_get_Current_m1631602353((InternalEnumerator_1_t3070775034 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint2D_t4288432358  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1836373474_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1836373474(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4228758465_gshared (InternalEnumerator_1_t3070775034 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4228758465_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4228758465(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1044203874_gshared (InternalEnumerator_1_t3070775034 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1044203874_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1044203874(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1631602353_MetadataUsageId;
extern "C"  ContactPoint2D_t4288432358  InternalEnumerator_1_get_Current_m1631602353_gshared (InternalEnumerator_1_t3070775034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1631602353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint2D_t4288432358  L_8 = ((  ContactPoint2D_t4288432358  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ContactPoint2D_t4288432358  InternalEnumerator_1_get_Current_m1631602353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3070775034 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3070775034 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1631602353(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3551691594_gshared (InternalEnumerator_1_t2545004040 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3551691594_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	InternalEnumerator_1__ctor_m3551691594(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1289144854_gshared (InternalEnumerator_1_t2545004040 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1289144854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1289144854(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1671609218_gshared (InternalEnumerator_1_t2545004040 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t3762661364  L_0 = InternalEnumerator_1_get_Current_m3858822673((InternalEnumerator_1_t2545004040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastResult_t3762661364  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1671609218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1671609218(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2318647713_gshared (InternalEnumerator_1_t2545004040 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2318647713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2318647713(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m209654402_gshared (InternalEnumerator_1_t2545004040 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m209654402_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m209654402(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3858822673_MetadataUsageId;
extern "C"  RaycastResult_t3762661364  InternalEnumerator_1_get_Current_m3858822673_gshared (InternalEnumerator_1_t2545004040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3858822673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastResult_t3762661364  L_8 = ((  RaycastResult_t3762661364  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastResult_t3762661364  InternalEnumerator_1_get_Current_m3858822673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2545004040 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2545004040 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3858822673(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1529633010_gshared (InternalEnumerator_1_t3148142670 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1529633010_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	InternalEnumerator_1__ctor_m1529633010(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1649378158_gshared (InternalEnumerator_1_t3148142670 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1649378158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1649378158(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m71574874_gshared (InternalEnumerator_1_t3148142670 * __this, const MethodInfo* method)
{
	{
		Playable_t70832698  L_0 = InternalEnumerator_1_get_Current_m1063989817((InternalEnumerator_1_t3148142670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Playable_t70832698  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m71574874_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m71574874(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1329924425_gshared (InternalEnumerator_1_t3148142670 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1329924425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1329924425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3385684186_gshared (InternalEnumerator_1_t3148142670 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3385684186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3385684186(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Experimental.Director.Playable>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1063989817_MetadataUsageId;
extern "C"  Playable_t70832698  InternalEnumerator_1_get_Current_m1063989817_gshared (InternalEnumerator_1_t3148142670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1063989817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Playable_t70832698  L_8 = ((  Playable_t70832698  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Playable_t70832698  InternalEnumerator_1_get_Current_m1063989817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3148142670 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3148142670 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1063989817(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2728019190_gshared (InternalEnumerator_1_t2861398790 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2728019190_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	InternalEnumerator_1__ctor_m2728019190(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3521736938_gshared (InternalEnumerator_1_t2861398790 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3521736938_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3521736938(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3512084502_gshared (InternalEnumerator_1_t2861398790 * __this, const MethodInfo* method)
{
	{
		Keyframe_t4079056114  L_0 = InternalEnumerator_1_get_Current_m3245714045((InternalEnumerator_1_t2861398790 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t4079056114  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3512084502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3512084502(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1130719821_gshared (InternalEnumerator_1_t2861398790 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1130719821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1130719821(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3205116630_gshared (InternalEnumerator_1_t2861398790 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3205116630_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3205116630(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId;
extern "C"  Keyframe_t4079056114  InternalEnumerator_1_get_Current_m3245714045_gshared (InternalEnumerator_1_t2861398790 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t4079056114  L_8 = ((  Keyframe_t4079056114  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Keyframe_t4079056114  InternalEnumerator_1_get_Current_m3245714045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2861398790 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2861398790 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3245714045(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2566756701_gshared (InternalEnumerator_1_t2013616441 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2566756701_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	InternalEnumerator_1__ctor_m2566756701(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1327160611(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = InternalEnumerator_1_get_Current_m1630916742((InternalEnumerator_1_t2013616441 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NetworkPlayer_t3231273765  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3324148505(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1301070708_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1301070708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1301070708(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467923411_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m467923411_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m467923411(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1630916742_MetadataUsageId;
extern "C"  NetworkPlayer_t3231273765  InternalEnumerator_1_get_Current_m1630916742_gshared (InternalEnumerator_1_t2013616441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1630916742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NetworkPlayer_t3231273765  L_8 = ((  NetworkPlayer_t3231273765  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  NetworkPlayer_t3231273765  InternalEnumerator_1_get_Current_m1630916742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2013616441 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2013616441 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1630916742(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1241123430_gshared (InternalEnumerator_1_t336045558 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1241123430_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	InternalEnumerator_1__ctor_m1241123430(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m955209082_gshared (InternalEnumerator_1_t336045558 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m955209082_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m955209082(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2558878182_gshared (InternalEnumerator_1_t336045558 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = InternalEnumerator_1_get_Current_m734814253((InternalEnumerator_1_t336045558 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Quaternion_t1553702882  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2558878182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2558878182(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m55065533_gshared (InternalEnumerator_1_t336045558 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m55065533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	InternalEnumerator_1_Dispose_m55065533(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1083840486_gshared (InternalEnumerator_1_t336045558 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1083840486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1083840486(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m734814253_MetadataUsageId;
extern "C"  Quaternion_t1553702882  InternalEnumerator_1_get_Current_m734814253_gshared (InternalEnumerator_1_t336045558 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m734814253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Quaternion_t1553702882  L_8 = ((  Quaternion_t1553702882  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Quaternion_t1553702882  InternalEnumerator_1_get_Current_m734814253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t336045558 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t336045558 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m734814253(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3454518962_gshared (InternalEnumerator_1_t2785518402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3454518962_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3454518962(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3915103662_gshared (InternalEnumerator_1_t2785518402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3915103662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3915103662(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395460378_gshared (InternalEnumerator_1_t2785518402 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t4003175726  L_0 = InternalEnumerator_1_get_Current_m2996847993((InternalEnumerator_1_t2785518402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit_t4003175726  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395460378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395460378(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3675957001_gshared (InternalEnumerator_1_t2785518402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3675957001_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3675957001(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1662326298_gshared (InternalEnumerator_1_t2785518402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1662326298_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1662326298(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2996847993_MetadataUsageId;
extern "C"  RaycastHit_t4003175726  InternalEnumerator_1_get_Current_m2996847993_gshared (InternalEnumerator_1_t2785518402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2996847993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit_t4003175726  L_8 = ((  RaycastHit_t4003175726  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit_t4003175726  InternalEnumerator_1_get_Current_m2996847993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2785518402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2785518402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2996847993(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1194339780_gshared (InternalEnumerator_1_t157087060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1194339780_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	InternalEnumerator_1__ctor_m1194339780(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2176125276_gshared (InternalEnumerator_1_t157087060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2176125276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2176125276(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4112569736_gshared (InternalEnumerator_1_t157087060 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t1374744384  L_0 = InternalEnumerator_1_get_Current_m2378427979((InternalEnumerator_1_t157087060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t1374744384  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4112569736_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4112569736(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1838374043_gshared (InternalEnumerator_1_t157087060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1838374043_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1838374043(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2160819016_gshared (InternalEnumerator_1_t157087060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2160819016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2160819016(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2378427979_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  InternalEnumerator_1_get_Current_m2378427979_gshared (InternalEnumerator_1_t157087060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2378427979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit2D_t1374744384  L_8 = ((  RaycastHit2D_t1374744384  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  RaycastHit2D_t1374744384  InternalEnumerator_1_get_Current_m2378427979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t157087060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t157087060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2378427979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3939904108_gshared (InternalEnumerator_1_t3024247292 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3939904108_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	InternalEnumerator_1__ctor_m3939904108(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2066365364_gshared (InternalEnumerator_1_t3024247292 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2066365364_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2066365364(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m440025312_gshared (InternalEnumerator_1_t3024247292 * __this, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = InternalEnumerator_1_get_Current_m730462195((InternalEnumerator_1_t3024247292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Rect_t4241904616  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m440025312_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m440025312(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m159746883_gshared (InternalEnumerator_1_t3024247292 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m159746883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	InternalEnumerator_1_Dispose_m159746883(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rect>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3197943712_gshared (InternalEnumerator_1_t3024247292 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3197943712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3197943712(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.Rect>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m730462195_MetadataUsageId;
extern "C"  Rect_t4241904616  InternalEnumerator_1_get_Current_m730462195_gshared (InternalEnumerator_1_t3024247292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m730462195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Rect_t4241904616  L_8 = ((  Rect_t4241904616  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Rect_t4241904616  InternalEnumerator_1_get_Current_m730462195_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3024247292 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3024247292 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m730462195(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2952786262_gshared (InternalEnumerator_1_t1991476773 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2952786262_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	InternalEnumerator_1__ctor_m2952786262(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3508316298_gshared (InternalEnumerator_1_t1991476773 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3508316298_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3508316298(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2891046656_gshared (InternalEnumerator_1_t1991476773 * __this, const MethodInfo* method)
{
	{
		HitInfo_t3209134097  L_0 = InternalEnumerator_1_get_Current_m4186452479((InternalEnumerator_1_t1991476773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t3209134097  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2891046656_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2891046656(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m54857389_gshared (InternalEnumerator_1_t1991476773 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m54857389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	InternalEnumerator_1_Dispose_m54857389(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m204092218_gshared (InternalEnumerator_1_t1991476773 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m204092218_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m204092218(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId;
extern "C"  HitInfo_t3209134097  InternalEnumerator_1_get_Current_m4186452479_gshared (InternalEnumerator_1_t1991476773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t3209134097  L_8 = ((  HitInfo_t3209134097  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  HitInfo_t3209134097  InternalEnumerator_1_get_Current_m4186452479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1991476773 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1991476773 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4186452479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3202091545_gshared (InternalEnumerator_1_t2263718591 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3202091545_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	InternalEnumerator_1__ctor_m3202091545(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3642743527_gshared (InternalEnumerator_1_t2263718591 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3642743527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3642743527(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3542460115_gshared (InternalEnumerator_1_t2263718591 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t3481375915  L_0 = InternalEnumerator_1_get_Current_m4069864032((InternalEnumerator_1_t2263718591 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t3481375915  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3542460115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3542460115(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2757865264_gshared (InternalEnumerator_1_t2263718591 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2757865264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2757865264(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2931622739_gshared (InternalEnumerator_1_t2263718591 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2931622739_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2931622739(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId;
extern "C"  GcAchievementData_t3481375915  InternalEnumerator_1_get_Current_m4069864032_gshared (InternalEnumerator_1_t2263718591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t3481375915  L_8 = ((  GcAchievementData_t3481375915  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcAchievementData_t3481375915  InternalEnumerator_1_get_Current_m4069864032_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2263718591 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2263718591 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4069864032(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m909397884_gshared (InternalEnumerator_1_t963639266 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m909397884_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	InternalEnumerator_1__ctor_m909397884(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3844431012_gshared (InternalEnumerator_1_t963639266 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3844431012_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3844431012(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3502383888_gshared (InternalEnumerator_1_t963639266 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t2181296590  L_0 = InternalEnumerator_1_get_Current_m2404034883((InternalEnumerator_1_t963639266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t2181296590  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3502383888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3502383888(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3662398547_gshared (InternalEnumerator_1_t963639266 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3662398547_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3662398547(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3456760080_gshared (InternalEnumerator_1_t963639266 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3456760080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3456760080(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId;
extern "C"  GcScoreData_t2181296590  InternalEnumerator_1_get_Current_m2404034883_gshared (InternalEnumerator_1_t963639266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t2181296590  L_8 = ((  GcScoreData_t2181296590  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  GcScoreData_t2181296590  InternalEnumerator_1_get_Current_m2404034883_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t963639266 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t963639266 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2404034883(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1325614747_gshared (InternalEnumerator_1_t2928303786 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1325614747_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	InternalEnumerator_1__ctor_m1325614747(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2147875621_gshared (InternalEnumerator_1_t2928303786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2147875621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2147875621(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3798108827_gshared (InternalEnumerator_1_t2928303786 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m1313624900((InternalEnumerator_1_t2928303786 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3798108827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3798108827(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3603973682_gshared (InternalEnumerator_1_t2928303786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3603973682_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3603973682(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m639411413_gshared (InternalEnumerator_1_t2928303786 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m639411413_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m639411413(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1313624900_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1313624900_gshared (InternalEnumerator_1_t2928303786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1313624900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m1313624900_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2928303786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2928303786 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1313624900(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
