﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t2526458961;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GUIText
struct GUIText_t3371372606;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_GTToggle
struct  CFX_Demo_GTToggle_t4197841651  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture CFX_Demo_GTToggle::Normal
	Texture_t2526458961 * ___Normal_2;
	// UnityEngine.Texture CFX_Demo_GTToggle::Hover
	Texture_t2526458961 * ___Hover_3;
	// UnityEngine.Color CFX_Demo_GTToggle::NormalColor
	Color_t4194546905  ___NormalColor_4;
	// UnityEngine.Color CFX_Demo_GTToggle::DisabledColor
	Color_t4194546905  ___DisabledColor_5;
	// System.Boolean CFX_Demo_GTToggle::State
	bool ___State_6;
	// System.String CFX_Demo_GTToggle::Callback
	String_t* ___Callback_7;
	// UnityEngine.GameObject CFX_Demo_GTToggle::Receiver
	GameObject_t3674682005 * ___Receiver_8;
	// UnityEngine.Rect CFX_Demo_GTToggle::CollisionRect
	Rect_t4241904616  ___CollisionRect_9;
	// System.Boolean CFX_Demo_GTToggle::Over
	bool ___Over_10;
	// UnityEngine.GUIText CFX_Demo_GTToggle::Label
	GUIText_t3371372606 * ___Label_11;

public:
	inline static int32_t get_offset_of_Normal_2() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Normal_2)); }
	inline Texture_t2526458961 * get_Normal_2() const { return ___Normal_2; }
	inline Texture_t2526458961 ** get_address_of_Normal_2() { return &___Normal_2; }
	inline void set_Normal_2(Texture_t2526458961 * value)
	{
		___Normal_2 = value;
		Il2CppCodeGenWriteBarrier(&___Normal_2, value);
	}

	inline static int32_t get_offset_of_Hover_3() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Hover_3)); }
	inline Texture_t2526458961 * get_Hover_3() const { return ___Hover_3; }
	inline Texture_t2526458961 ** get_address_of_Hover_3() { return &___Hover_3; }
	inline void set_Hover_3(Texture_t2526458961 * value)
	{
		___Hover_3 = value;
		Il2CppCodeGenWriteBarrier(&___Hover_3, value);
	}

	inline static int32_t get_offset_of_NormalColor_4() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___NormalColor_4)); }
	inline Color_t4194546905  get_NormalColor_4() const { return ___NormalColor_4; }
	inline Color_t4194546905 * get_address_of_NormalColor_4() { return &___NormalColor_4; }
	inline void set_NormalColor_4(Color_t4194546905  value)
	{
		___NormalColor_4 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_5() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___DisabledColor_5)); }
	inline Color_t4194546905  get_DisabledColor_5() const { return ___DisabledColor_5; }
	inline Color_t4194546905 * get_address_of_DisabledColor_5() { return &___DisabledColor_5; }
	inline void set_DisabledColor_5(Color_t4194546905  value)
	{
		___DisabledColor_5 = value;
	}

	inline static int32_t get_offset_of_State_6() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___State_6)); }
	inline bool get_State_6() const { return ___State_6; }
	inline bool* get_address_of_State_6() { return &___State_6; }
	inline void set_State_6(bool value)
	{
		___State_6 = value;
	}

	inline static int32_t get_offset_of_Callback_7() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Callback_7)); }
	inline String_t* get_Callback_7() const { return ___Callback_7; }
	inline String_t** get_address_of_Callback_7() { return &___Callback_7; }
	inline void set_Callback_7(String_t* value)
	{
		___Callback_7 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_7, value);
	}

	inline static int32_t get_offset_of_Receiver_8() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Receiver_8)); }
	inline GameObject_t3674682005 * get_Receiver_8() const { return ___Receiver_8; }
	inline GameObject_t3674682005 ** get_address_of_Receiver_8() { return &___Receiver_8; }
	inline void set_Receiver_8(GameObject_t3674682005 * value)
	{
		___Receiver_8 = value;
		Il2CppCodeGenWriteBarrier(&___Receiver_8, value);
	}

	inline static int32_t get_offset_of_CollisionRect_9() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___CollisionRect_9)); }
	inline Rect_t4241904616  get_CollisionRect_9() const { return ___CollisionRect_9; }
	inline Rect_t4241904616 * get_address_of_CollisionRect_9() { return &___CollisionRect_9; }
	inline void set_CollisionRect_9(Rect_t4241904616  value)
	{
		___CollisionRect_9 = value;
	}

	inline static int32_t get_offset_of_Over_10() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Over_10)); }
	inline bool get_Over_10() const { return ___Over_10; }
	inline bool* get_address_of_Over_10() { return &___Over_10; }
	inline void set_Over_10(bool value)
	{
		___Over_10 = value;
	}

	inline static int32_t get_offset_of_Label_11() { return static_cast<int32_t>(offsetof(CFX_Demo_GTToggle_t4197841651, ___Label_11)); }
	inline GUIText_t3371372606 * get_Label_11() const { return ___Label_11; }
	inline GUIText_t3371372606 ** get_address_of_Label_11() { return &___Label_11; }
	inline void set_Label_11(GUIText_t3371372606 * value)
	{
		___Label_11 = value;
		Il2CppCodeGenWriteBarrier(&___Label_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
