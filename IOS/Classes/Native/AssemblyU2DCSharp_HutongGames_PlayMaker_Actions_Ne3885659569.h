﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections
struct  NetworkSetMaximumConnections_t3885659569  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::maximumConnections
	FsmInt_t1596138449 * ___maximumConnections_11;

public:
	inline static int32_t get_offset_of_maximumConnections_11() { return static_cast<int32_t>(offsetof(NetworkSetMaximumConnections_t3885659569, ___maximumConnections_11)); }
	inline FsmInt_t1596138449 * get_maximumConnections_11() const { return ___maximumConnections_11; }
	inline FsmInt_t1596138449 ** get_address_of_maximumConnections_11() { return &___maximumConnections_11; }
	inline void set_maximumConnections_11(FsmInt_t1596138449 * value)
	{
		___maximumConnections_11 = value;
		Il2CppCodeGenWriteBarrier(&___maximumConnections_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
