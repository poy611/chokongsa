﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertFloatToString
struct ConvertFloatToString_t4020358941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::.ctor()
extern "C"  void ConvertFloatToString__ctor_m1082259433 (ConvertFloatToString_t4020358941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::Reset()
extern "C"  void ConvertFloatToString_Reset_m3023659670 (ConvertFloatToString_t4020358941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::OnEnter()
extern "C"  void ConvertFloatToString_OnEnter_m60958080 (ConvertFloatToString_t4020358941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::OnUpdate()
extern "C"  void ConvertFloatToString_OnUpdate_m1023259843 (ConvertFloatToString_t4020358941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::DoConvertFloatToString()
extern "C"  void ConvertFloatToString_DoConvertFloatToString_m2259495131 (ConvertFloatToString_t4020358941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
