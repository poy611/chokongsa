﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapArea2d
struct GetNextOverlapArea2d_t1935424293;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::.ctor()
extern "C"  void GetNextOverlapArea2d__ctor_m3960247521 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::Reset()
extern "C"  void GetNextOverlapArea2d_Reset_m1606680462 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::OnEnter()
extern "C"  void GetNextOverlapArea2d_OnEnter_m4143539320 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::DoGetNextCollider()
extern "C"  void GetNextOverlapArea2d_DoGetNextCollider_m3406082801 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::GetOverlapAreaAll()
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapArea2d_GetOverlapAreaAll_m1375757892 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
