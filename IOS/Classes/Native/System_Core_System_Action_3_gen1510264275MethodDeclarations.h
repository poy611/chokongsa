﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen178818711MethodDeclarations.h"

// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m158517567(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t1510264275 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2603739911_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2342865913(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1510264275 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m3469900163_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m4199196010(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t1510264275 *, int32_t, Il2CppObject *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m1158295226_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1088598735(__this, ___result0, method) ((  void (*) (Action_3_t1510264275 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m1187641495_gshared)(__this, ___result0, method)
