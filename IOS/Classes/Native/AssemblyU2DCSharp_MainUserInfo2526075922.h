﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<UserExperience>
struct List_1_t1035126405;

#include "AssemblyU2DCSharp_Singleton_1_gen2778891315.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUserInfo
struct  MainUserInfo_t2526075922  : public Singleton_1_t2778891315
{
public:
	// System.String MainUserInfo::userId
	String_t* ___userId_3;
	// System.String MainUserInfo::userName
	String_t* ___userName_4;
	// System.Int32 MainUserInfo::grade
	int32_t ___grade_5;
	// System.Int32 MainUserInfo::gameId
	int32_t ___gameId_6;
	// System.Int32 MainUserInfo::guestTh
	int32_t ___guestTh_7;
	// System.Int32 MainUserInfo::trialTh
	int32_t ___trialTh_8;
	// System.Int32 MainUserInfo::guestId
	int32_t ___guestId_9;
	// System.Int32 MainUserInfo::menu
	int32_t ___menu_10;
	// System.Int32 MainUserInfo::cream
	int32_t ___cream_11;
	// System.Collections.Generic.List`1<UserExperience> MainUserInfo::userLog
	List_1_t1035126405 * ___userLog_12;

public:
	inline static int32_t get_offset_of_userId_3() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___userId_3)); }
	inline String_t* get_userId_3() const { return ___userId_3; }
	inline String_t** get_address_of_userId_3() { return &___userId_3; }
	inline void set_userId_3(String_t* value)
	{
		___userId_3 = value;
		Il2CppCodeGenWriteBarrier(&___userId_3, value);
	}

	inline static int32_t get_offset_of_userName_4() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___userName_4)); }
	inline String_t* get_userName_4() const { return ___userName_4; }
	inline String_t** get_address_of_userName_4() { return &___userName_4; }
	inline void set_userName_4(String_t* value)
	{
		___userName_4 = value;
		Il2CppCodeGenWriteBarrier(&___userName_4, value);
	}

	inline static int32_t get_offset_of_grade_5() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___grade_5)); }
	inline int32_t get_grade_5() const { return ___grade_5; }
	inline int32_t* get_address_of_grade_5() { return &___grade_5; }
	inline void set_grade_5(int32_t value)
	{
		___grade_5 = value;
	}

	inline static int32_t get_offset_of_gameId_6() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___gameId_6)); }
	inline int32_t get_gameId_6() const { return ___gameId_6; }
	inline int32_t* get_address_of_gameId_6() { return &___gameId_6; }
	inline void set_gameId_6(int32_t value)
	{
		___gameId_6 = value;
	}

	inline static int32_t get_offset_of_guestTh_7() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___guestTh_7)); }
	inline int32_t get_guestTh_7() const { return ___guestTh_7; }
	inline int32_t* get_address_of_guestTh_7() { return &___guestTh_7; }
	inline void set_guestTh_7(int32_t value)
	{
		___guestTh_7 = value;
	}

	inline static int32_t get_offset_of_trialTh_8() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___trialTh_8)); }
	inline int32_t get_trialTh_8() const { return ___trialTh_8; }
	inline int32_t* get_address_of_trialTh_8() { return &___trialTh_8; }
	inline void set_trialTh_8(int32_t value)
	{
		___trialTh_8 = value;
	}

	inline static int32_t get_offset_of_guestId_9() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___guestId_9)); }
	inline int32_t get_guestId_9() const { return ___guestId_9; }
	inline int32_t* get_address_of_guestId_9() { return &___guestId_9; }
	inline void set_guestId_9(int32_t value)
	{
		___guestId_9 = value;
	}

	inline static int32_t get_offset_of_menu_10() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___menu_10)); }
	inline int32_t get_menu_10() const { return ___menu_10; }
	inline int32_t* get_address_of_menu_10() { return &___menu_10; }
	inline void set_menu_10(int32_t value)
	{
		___menu_10 = value;
	}

	inline static int32_t get_offset_of_cream_11() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___cream_11)); }
	inline int32_t get_cream_11() const { return ___cream_11; }
	inline int32_t* get_address_of_cream_11() { return &___cream_11; }
	inline void set_cream_11(int32_t value)
	{
		___cream_11 = value;
	}

	inline static int32_t get_offset_of_userLog_12() { return static_cast<int32_t>(offsetof(MainUserInfo_t2526075922, ___userLog_12)); }
	inline List_1_t1035126405 * get_userLog_12() const { return ___userLog_12; }
	inline List_1_t1035126405 ** get_address_of_userLog_12() { return &___userLog_12; }
	inline void set_userLog_12(List_1_t1035126405 * value)
	{
		___userLog_12 = value;
		Il2CppCodeGenWriteBarrier(&___userLog_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
