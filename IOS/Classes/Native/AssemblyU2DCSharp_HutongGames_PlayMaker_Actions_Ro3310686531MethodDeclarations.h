﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Rotate
struct Rotate_t3310686531;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Rotate::.ctor()
extern "C"  void Rotate__ctor_m586238595 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::Reset()
extern "C"  void Rotate_Reset_m2527638832 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnPreprocess()
extern "C"  void Rotate_OnPreprocess_m2874115404 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnEnter()
extern "C"  void Rotate_OnEnter_m126302618 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnUpdate()
extern "C"  void Rotate_OnUpdate_m3048940521 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnLateUpdate()
extern "C"  void Rotate_OnLateUpdate_m211209519 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnFixedUpdate()
extern "C"  void Rotate_OnFixedUpdate_m3128473119 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::DoRotate()
extern "C"  void Rotate_DoRotate_m2040233639 (Rotate_t3310686531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
