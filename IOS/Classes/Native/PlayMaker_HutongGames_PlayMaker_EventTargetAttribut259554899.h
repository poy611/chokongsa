﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute2523058482.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.EventTargetAttribute
struct  EventTargetAttribute_t259554899  : public Attribute_t2523058482
{
public:
	// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.EventTargetAttribute::target
	int32_t ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(EventTargetAttribute_t259554899, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
