﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DragRigidBody
struct DragRigidBody_t327854439;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::.ctor()
extern "C"  void DragRigidBody__ctor_m1594837935 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::Reset()
extern "C"  void DragRigidBody_Reset_m3536238172 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::OnEnter()
extern "C"  void DragRigidBody_OnEnter_m3022626758 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::OnUpdate()
extern "C"  void DragRigidBody_OnUpdate_m2640675645 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::StartDragging(UnityEngine.RaycastHit)
extern "C"  void DragRigidBody_StartDragging_m1570477533 (DragRigidBody_t327854439 * __this, RaycastHit_t4003175726  ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::Drag()
extern "C"  void DragRigidBody_Drag_m3188416489 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::StopDragging()
extern "C"  void DragRigidBody_StopDragging_m4119448294 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DragRigidBody::OnExit()
extern "C"  void DragRigidBody_OnExit_m3708654354 (DragRigidBody_t327854439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
