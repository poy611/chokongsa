﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo>
struct IEnumerable_1_t1105416852;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>
struct IEnumerable_1_t4226351297;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo>
struct List_1_t3467656743;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void YoutubeExtractor.DownloadUrlResolver::DecryptDownloadUrl(YoutubeExtractor.VideoInfo)
extern "C"  void DownloadUrlResolver_DecryptDownloadUrl_m1994299011 (Il2CppObject * __this /* static, unused */, VideoInfo_t2099471191 * ___videoInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::DecryptSignature(System.String,System.String)
extern "C"  String_t* DownloadUrlResolver_DecryptSignature_m2435384427 (Il2CppObject * __this /* static, unused */, String_t* ___signature0, String_t* ___htmlPlayerVersion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo> YoutubeExtractor.DownloadUrlResolver::GetDownloadUrls(System.String,System.Boolean)
extern "C"  Il2CppObject* DownloadUrlResolver_GetDownloadUrls_m3448163278 (Il2CppObject * __this /* static, unused */, String_t* ___videoUrl0, bool ___decryptSignature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.DownloadUrlResolver::TryNormalizeYoutubeUrl(System.String,System.String&)
extern "C"  bool DownloadUrlResolver_TryNormalizeYoutubeUrl_m2637065517 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t** ___normalizedUrl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo> YoutubeExtractor.DownloadUrlResolver::ExtractDownloadUrls(Newtonsoft.Json.Linq.JObject)
extern "C"  Il2CppObject* DownloadUrlResolver_ExtractDownloadUrls_m2099554026 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::ExtractSignatureFromManifest(System.String)
extern "C"  String_t* DownloadUrlResolver_ExtractSignatureFromManifest_m3925939498 (Il2CppObject * __this /* static, unused */, String_t* ___manifestUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::GetHtml5PlayerVersion(Newtonsoft.Json.Linq.JObject)
extern "C"  String_t* DownloadUrlResolver_GetHtml5PlayerVersion_m1659753901 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::GetStreamMap(Newtonsoft.Json.Linq.JObject)
extern "C"  String_t* DownloadUrlResolver_GetStreamMap_m1317351358 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo> YoutubeExtractor.DownloadUrlResolver::GetVideoInfos(System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>,System.String)
extern "C"  List_1_t3467656743 * DownloadUrlResolver_GetVideoInfos_m3217043869 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___extractionInfos0, String_t* ___videoTitle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// YoutubeExtractor.VideoInfo YoutubeExtractor.DownloadUrlResolver::GetSingleVideoInfo(System.Int32,System.String,System.String,System.Boolean)
extern "C"  VideoInfo_t2099471191 * DownloadUrlResolver_GetSingleVideoInfo_m3669702907 (Il2CppObject * __this /* static, unused */, int32_t ___formatCode0, String_t* ___queryUrl1, String_t* ___videoTitle2, bool ___requiresDecryption3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::GetVideoTitle(Newtonsoft.Json.Linq.JObject)
extern "C"  String_t* DownloadUrlResolver_GetVideoTitle_m3499604573 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.DownloadUrlResolver::GetDashManifest(Newtonsoft.Json.Linq.JObject)
extern "C"  String_t* DownloadUrlResolver_GetDashManifest_m3557462241 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.DownloadUrlResolver::IsVideoUnavailable(System.String)
extern "C"  bool DownloadUrlResolver_IsVideoUnavailable_m1282682818 (Il2CppObject * __this /* static, unused */, String_t* ___pageSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject YoutubeExtractor.DownloadUrlResolver::LoadJson(System.String)
extern "C"  JObject_t1798544199 * DownloadUrlResolver_LoadJson_m2844638074 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver::ParseDashManifest(System.String,System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo>,System.String)
extern "C"  void DownloadUrlResolver_ParseDashManifest_m3210265877 (Il2CppObject * __this /* static, unused */, String_t* ___dashManifestUrl0, List_1_t3467656743 * ___previousFormats1, String_t* ___videoTitle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YoutubeExtractor.DownloadUrlResolver::ThrowYoutubeParseException(System.Exception,System.String)
extern "C"  void DownloadUrlResolver_ThrowYoutubeParseException_m2477564202 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___innerException0, String_t* ___videoUrl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
