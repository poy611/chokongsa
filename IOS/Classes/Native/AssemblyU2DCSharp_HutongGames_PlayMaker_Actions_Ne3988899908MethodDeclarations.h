﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkInstantiate
struct NetworkInstantiate_t3988899908;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::.ctor()
extern "C"  void NetworkInstantiate__ctor_m4119953570 (NetworkInstantiate_t3988899908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::Reset()
extern "C"  void NetworkInstantiate_Reset_m1766386511 (NetworkInstantiate_t3988899908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::OnEnter()
extern "C"  void NetworkInstantiate_OnEnter_m3002229753 (NetworkInstantiate_t3988899908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
