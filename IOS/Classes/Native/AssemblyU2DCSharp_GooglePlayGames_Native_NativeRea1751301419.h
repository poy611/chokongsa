﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t1804230813;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t3760582229;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A
struct  U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A::participant
	Participant_t1804230813 * ___participant_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey7A::<>f__this
	OnGameThreadForwardingListener_t3760582229 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_participant_0() { return static_cast<int32_t>(offsetof(U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419, ___participant_0)); }
	inline Participant_t1804230813 * get_participant_0() const { return ___participant_0; }
	inline Participant_t1804230813 ** get_address_of_participant_0() { return &___participant_0; }
	inline void set_participant_0(Participant_t1804230813 * value)
	{
		___participant_0 = value;
		Il2CppCodeGenWriteBarrier(&___participant_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CParticipantLeftU3Ec__AnonStorey7A_t1751301419, ___U3CU3Ef__this_1)); }
	inline OnGameThreadForwardingListener_t3760582229 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline OnGameThreadForwardingListener_t3760582229 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(OnGameThreadForwardingListener_t3760582229 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
