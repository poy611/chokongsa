﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// UnityEngine.GameObject HutongGames.PlayMaker.FsmGameObject::get_Value()
extern "C"  GameObject_t3674682005 * FsmGameObject_get_Value_m673294275 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::set_Value(UnityEngine.GameObject)
extern "C"  void FsmGameObject_set_Value_m297051598 (FsmGameObject_t1697147867 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmGameObject::get_RawValue()
extern "C"  Il2CppObject * FsmGameObject_get_RawValue_m1542938365 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::set_RawValue(System.Object)
extern "C"  void FsmGameObject_set_RawValue_m3749970318 (FsmGameObject_t1697147867 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::SafeAssign(System.Object)
extern "C"  void FsmGameObject_SafeAssign_m4091484568 (FsmGameObject_t1697147867 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor()
extern "C"  void FsmGameObject__ctor_m1048269796 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor(System.String)
extern "C"  void FsmGameObject__ctor_m1236057502 (FsmGameObject_t1697147867 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor(HutongGames.PlayMaker.FsmGameObject)
extern "C"  void FsmGameObject__ctor_m1180812553 (FsmGameObject_t1697147867 * __this, FsmGameObject_t1697147867 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmGameObject::Clone()
extern "C"  NamedVariable_t3211770239 * FsmGameObject_Clone_m4194168329 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmGameObject::get_VariableType()
extern "C"  int32_t FsmGameObject_get_VariableType_m2135426368 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmGameObject::ToString()
extern "C"  String_t* FsmGameObject_ToString_m1021529033 (FsmGameObject_t1697147867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmGameObject::op_Implicit(UnityEngine.GameObject)
extern "C"  FsmGameObject_t1697147867 * FsmGameObject_op_Implicit_m1529951499 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
