﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2FromVector3
struct GetAtan2FromVector3_t4248152920;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::.ctor()
extern "C"  void GetAtan2FromVector3__ctor_m800660190 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::Reset()
extern "C"  void GetAtan2FromVector3_Reset_m2742060427 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnEnter()
extern "C"  void GetAtan2FromVector3_OnEnter_m27025205 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnUpdate()
extern "C"  void GetAtan2FromVector3_OnUpdate_m4266308014 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::DoATan()
extern "C"  void GetAtan2FromVector3_DoATan_m3607399761 (GetAtan2FromVector3_t4248152920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
