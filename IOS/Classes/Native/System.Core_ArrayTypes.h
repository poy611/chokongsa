﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>
struct Func_2_t2363589633;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Array1146569071.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2122599155.h"
#include "System_Core_System_Func_2_gen184564025.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li814405322.h"
#include "System_Core_System_Func_2_gen2363589633.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2749541277.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1089569710.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2253981637.h"

#pragma once
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3085427682  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2122599155  m_Items[1];

public:
	inline Link_t2122599155  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2122599155 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2122599155  value)
	{
		m_Items[index] = value;
	}
};
// System.Func`2<System.Object,System.Object>[]
struct Func_2U5BU5D_t2870019524  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Func_2_t184564025 * m_Items[1];

public:
	inline Func_2_t184564025 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Func_2_t184564025 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Func_2_t184564025 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Char>[]
struct LinkU5BU5D_t1005949647  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t814405322  m_Items[1];

public:
	inline Link_t814405322  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t814405322 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t814405322  value)
	{
		m_Items[index] = value;
	}
};
// System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>[]
struct Func_2U5BU5D_t1064210140  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Func_2_t2363589633 * m_Items[1];

public:
	inline Func_2_t2363589633 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Func_2_t2363589633 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Func_2_t2363589633 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.IClippable>[]
struct LinkU5BU5D_t1725180240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2749541277  m_Items[1];

public:
	inline Link_t2749541277  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2749541277 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2749541277  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t1643143343  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3771233898 * m_Items[1];

public:
	inline Action_t3771233898 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3771233898 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3771233898 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>[]
struct LinkU5BU5D_t64733659  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t1089569710  m_Items[1];

public:
	inline Link_t1089569710  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t1089569710 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t1089569710  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct LinkU5BU5D_t1735806856  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2253981637  m_Items[1];

public:
	inline Link_t2253981637  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2253981637 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2253981637  value)
	{
		m_Items[index] = value;
	}
};
