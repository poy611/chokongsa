﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey61
struct U3CFromQuestUICallbackU3Ec__AnonStorey61_t3667433359;
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
struct QuestUIResponse_t3819980022;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Q3819980022.h"

// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey61::.ctor()
extern "C"  void U3CFromQuestUICallbackU3Ec__AnonStorey61__ctor_m1218878140 (U3CFromQuestUICallbackU3Ec__AnonStorey61_t3667433359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey61::<>m__3E(GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse)
extern "C"  void U3CFromQuestUICallbackU3Ec__AnonStorey61_U3CU3Em__3E_m2405914853 (U3CFromQuestUICallbackU3Ec__AnonStorey61_t3667433359 * __this, QuestUIResponse_t3819980022 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
