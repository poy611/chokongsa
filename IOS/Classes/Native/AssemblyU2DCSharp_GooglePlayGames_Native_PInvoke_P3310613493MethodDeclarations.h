﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse
struct FetchResponse_t3310613493;
// GooglePlayGames.Native.PInvoke.NativePlayer
struct NativePlayer_t2636885988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse::.ctor(System.IntPtr)
extern "C"  void FetchResponse__ctor_m3334376910 (FetchResponse_t3310613493 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchResponse_CallDispose_m2088022434 (FetchResponse_t3310613493 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativePlayer GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse::GetPlayer()
extern "C"  NativePlayer_t2636885988 * FetchResponse_GetPlayer_m3078346563 (FetchResponse_t3310613493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse::Status()
extern "C"  int32_t FetchResponse_Status_m3221892121 (FetchResponse_t3310613493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse GooglePlayGames.Native.PInvoke.PlayerManager/FetchResponse::FromPointer(System.IntPtr)
extern "C"  FetchResponse_t3310613493 * FetchResponse_FromPointer_m486055493 (Il2CppObject * __this /* static, unused */, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
