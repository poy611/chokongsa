﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.UpdateHelper
struct UpdateHelper_t2222357017;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"

// System.Void HutongGames.PlayMaker.UpdateHelper::.ctor()
extern "C"  void UpdateHelper__ctor_m1665992600 (UpdateHelper_t2222357017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.UpdateHelper::SetDirty(HutongGames.PlayMaker.Fsm)
extern "C"  void UpdateHelper_SetDirty_m2930215746 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
