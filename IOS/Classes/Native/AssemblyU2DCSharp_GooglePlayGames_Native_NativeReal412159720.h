﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71
struct U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70
struct U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72
struct  U3CAcceptInvitationU3Ec__AnonStorey72_t412159720  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72::invitation
	MultiplayerInvitation_t3411188537 * ___invitation_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72::<>f__ref$113
	U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * ___U3CU3Ef__refU24113_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72::<>f__ref$112
	U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * ___U3CU3Ef__refU24112_2;

public:
	inline static int32_t get_offset_of_invitation_0() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey72_t412159720, ___invitation_0)); }
	inline MultiplayerInvitation_t3411188537 * get_invitation_0() const { return ___invitation_0; }
	inline MultiplayerInvitation_t3411188537 ** get_address_of_invitation_0() { return &___invitation_0; }
	inline void set_invitation_0(MultiplayerInvitation_t3411188537 * value)
	{
		___invitation_0 = value;
		Il2CppCodeGenWriteBarrier(&___invitation_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24113_1() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey72_t412159720, ___U3CU3Ef__refU24113_1)); }
	inline U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * get_U3CU3Ef__refU24113_1() const { return ___U3CU3Ef__refU24113_1; }
	inline U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 ** get_address_of_U3CU3Ef__refU24113_1() { return &___U3CU3Ef__refU24113_1; }
	inline void set_U3CU3Ef__refU24113_1(U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * value)
	{
		___U3CU3Ef__refU24113_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24113_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24112_2() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey72_t412159720, ___U3CU3Ef__refU24112_2)); }
	inline U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * get_U3CU3Ef__refU24112_2() const { return ___U3CU3Ef__refU24112_2; }
	inline U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 ** get_address_of_U3CU3Ef__refU24112_2() { return &___U3CU3Ef__refU24112_2; }
	inline void set_U3CU3Ef__refU24112_2(U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574 * value)
	{
		___U3CU3Ef__refU24112_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24112_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
