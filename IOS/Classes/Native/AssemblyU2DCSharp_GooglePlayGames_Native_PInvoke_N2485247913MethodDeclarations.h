﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeEvent
struct NativeEvent_t2485247913;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_3429751962.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeEvent::.ctor(System.IntPtr)
extern "C"  void NativeEvent__ctor_m3341223065 (NativeEvent_t2485247913 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeEvent::get_Id()
extern "C"  String_t* NativeEvent_get_Id_m1006737552 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeEvent::get_Name()
extern "C"  String_t* NativeEvent_get_Name_m1249635584 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeEvent::get_Description()
extern "C"  String_t* NativeEvent_get_Description_m2732340041 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeEvent::get_ImageUrl()
extern "C"  String_t* NativeEvent_get_ImageUrl_m3296386121 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeEvent::get_CurrentCount()
extern "C"  uint64_t NativeEvent_get_CurrentCount_m105631364 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.Native.PInvoke.NativeEvent::get_Visibility()
extern "C"  int32_t NativeEvent_get_Visibility_m2518888952 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeEvent::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeEvent_CallDispose_m1168343095 (NativeEvent_t2485247913 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeEvent::ToString()
extern "C"  String_t* NativeEvent_ToString_m611360664 (NativeEvent_t2485247913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeEvent::<get_Id>m__AA(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeEvent_U3Cget_IdU3Em__AA_m3659973159 (NativeEvent_t2485247913 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeEvent::<get_Name>m__AB(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeEvent_U3Cget_NameU3Em__AB_m1582966040 (NativeEvent_t2485247913 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeEvent::<get_Description>m__AC(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeEvent_U3Cget_DescriptionU3Em__AC_m2186157760 (NativeEvent_t2485247913 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeEvent::<get_ImageUrl>m__AD(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeEvent_U3Cget_ImageUrlU3Em__AD_m3856175523 (NativeEvent_t2485247913 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
