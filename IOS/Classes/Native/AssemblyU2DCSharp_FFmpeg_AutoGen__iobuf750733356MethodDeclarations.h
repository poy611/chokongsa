﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FFmpeg.AutoGen._iobuf
struct _iobuf_t750733356;
struct _iobuf_t750733356_marshaled_pinvoke;
struct _iobuf_t750733356_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct _iobuf_t750733356;
struct _iobuf_t750733356_marshaled_pinvoke;

extern "C" void _iobuf_t750733356_marshal_pinvoke(const _iobuf_t750733356& unmarshaled, _iobuf_t750733356_marshaled_pinvoke& marshaled);
extern "C" void _iobuf_t750733356_marshal_pinvoke_back(const _iobuf_t750733356_marshaled_pinvoke& marshaled, _iobuf_t750733356& unmarshaled);
extern "C" void _iobuf_t750733356_marshal_pinvoke_cleanup(_iobuf_t750733356_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct _iobuf_t750733356;
struct _iobuf_t750733356_marshaled_com;

extern "C" void _iobuf_t750733356_marshal_com(const _iobuf_t750733356& unmarshaled, _iobuf_t750733356_marshaled_com& marshaled);
extern "C" void _iobuf_t750733356_marshal_com_back(const _iobuf_t750733356_marshaled_com& marshaled, _iobuf_t750733356& unmarshaled);
extern "C" void _iobuf_t750733356_marshal_com_cleanup(_iobuf_t750733356_marshaled_com& marshaled);
