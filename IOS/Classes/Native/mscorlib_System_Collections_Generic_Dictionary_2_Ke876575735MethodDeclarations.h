﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>
struct KeyCollection_t876575735;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4159719634.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3025266057_gshared (KeyCollection_t876575735 * __this, Dictionary_2_t3544783580 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3025266057(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t876575735 *, Dictionary_2_t3544783580 *, const MethodInfo*))KeyCollection__ctor_m3025266057_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1666867757_gshared (KeyCollection_t876575735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1666867757(__this, ___item0, method) ((  void (*) (KeyCollection_t876575735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1666867757_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3174903524_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3174903524(__this, method) ((  void (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3174903524_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3440796865_gshared (KeyCollection_t876575735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3440796865(__this, ___item0, method) ((  bool (*) (KeyCollection_t876575735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3440796865_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3011973734_gshared (KeyCollection_t876575735 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3011973734(__this, ___item0, method) ((  bool (*) (KeyCollection_t876575735 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3011973734_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2276350902_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2276350902(__this, method) ((  Il2CppObject* (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2276350902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2180767062_gshared (KeyCollection_t876575735 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2180767062(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t876575735 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2180767062_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m458376805_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m458376805(__this, method) ((  Il2CppObject * (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m458376805_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2362028002_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2362028002(__this, method) ((  bool (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2362028002_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m164219668_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m164219668(__this, method) ((  bool (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m164219668_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4206686406_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4206686406(__this, method) ((  Il2CppObject * (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4206686406_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3308236862_gshared (KeyCollection_t876575735 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3308236862(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t876575735 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3308236862_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t4159719634  KeyCollection_GetEnumerator_m119094283_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m119094283(__this, method) ((  Enumerator_t4159719634  (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_GetEnumerator_m119094283_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3090149838_gshared (KeyCollection_t876575735 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3090149838(__this, method) ((  int32_t (*) (KeyCollection_t876575735 *, const MethodInfo*))KeyCollection_get_Count_m3090149838_gshared)(__this, method)
