﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8
struct U3CCheckIfAliveU3Ec__Iterator8_t161768900;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::.ctor()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator8__ctor_m4256188967 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckIfAliveU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m485314571 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckIfAliveU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m4179544479 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::MoveNext()
extern "C"  bool U3CCheckIfAliveU3Ec__Iterator8_MoveNext_m3807703149 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::Dispose()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator8_Dispose_m1287014564 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::Reset()
extern "C"  void U3CCheckIfAliveU3Ec__Iterator8_Reset_m1902621908 (U3CCheckIfAliveU3Ec__Iterator8_t161768900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
