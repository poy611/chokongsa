﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey57
struct U3CUpdateAchievementU3Ec__AnonStorey57_t4165615717;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t2513188365;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_A2513188365.h"

// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey57::.ctor()
extern "C"  void U3CUpdateAchievementU3Ec__AnonStorey57__ctor_m3113354710 (U3CUpdateAchievementU3Ec__AnonStorey57_t4165615717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey57::<>m__2D(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern "C"  void U3CUpdateAchievementU3Ec__AnonStorey57_U3CU3Em__2D_m3618211316 (U3CUpdateAchievementU3Ec__AnonStorey57_t4165615717 * __this, FetchResponse_t2513188365 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
