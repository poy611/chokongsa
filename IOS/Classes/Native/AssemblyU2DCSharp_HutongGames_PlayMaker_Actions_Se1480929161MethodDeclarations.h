﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITextureColor
struct SetGUITextureColor_t1480929161;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::.ctor()
extern "C"  void SetGUITextureColor__ctor_m314406653 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::Reset()
extern "C"  void SetGUITextureColor_Reset_m2255806890 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnEnter()
extern "C"  void SetGUITextureColor_OnEnter_m888811412 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnUpdate()
extern "C"  void SetGUITextureColor_OnUpdate_m916909359 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::DoSetGUITextureColor()
extern "C"  void SetGUITextureColor_DoSetGUITextureColor_m647941811 (SetGUITextureColor_t1480929161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
