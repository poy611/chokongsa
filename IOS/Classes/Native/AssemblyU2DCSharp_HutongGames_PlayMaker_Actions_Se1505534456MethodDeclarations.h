﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomGameObject
struct SelectRandomGameObject_t1505534456;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::.ctor()
extern "C"  void SelectRandomGameObject__ctor_m1549895022 (SelectRandomGameObject_t1505534456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::Reset()
extern "C"  void SelectRandomGameObject_Reset_m3491295259 (SelectRandomGameObject_t1505534456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::OnEnter()
extern "C"  void SelectRandomGameObject_OnEnter_m2782160325 (SelectRandomGameObject_t1505534456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::DoSelectRandomGameObject()
extern "C"  void SelectRandomGameObject_DoSelectRandomGameObject_m2596546897 (SelectRandomGameObject_t1505534456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
