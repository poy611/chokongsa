﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGravity
struct SetGravity_t1737623796;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGravity::.ctor()
extern "C"  void SetGravity__ctor_m2689768946 (SetGravity_t1737623796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity::Reset()
extern "C"  void SetGravity_Reset_m336201887 (SetGravity_t1737623796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity::OnEnter()
extern "C"  void SetGravity_OnEnter_m2984340809 (SetGravity_t1737623796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity::OnUpdate()
extern "C"  void SetGravity_OnUpdate_m1453811226 (SetGravity_t1737623796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity::DoSetGravity()
extern "C"  void SetGravity_DoSetGravity_m701896905 (SetGravity_t1737623796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
