﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Wait
struct Wait_t3399487645;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Wait::.ctor()
extern "C"  void Wait__ctor_m3344471657 (Wait_t3399487645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::Reset()
extern "C"  void Wait_Reset_m990904598 (Wait_t3399487645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::OnEnter()
extern "C"  void Wait_OnEnter_m793453568 (Wait_t3399487645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::OnUpdate()
extern "C"  void Wait_OnUpdate_m2255783491 (Wait_t3399487645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
