﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetPreviousStateName
struct  GetPreviousStateName_t1600272887  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetPreviousStateName::storeName
	FsmString_t952858651 * ___storeName_11;

public:
	inline static int32_t get_offset_of_storeName_11() { return static_cast<int32_t>(offsetof(GetPreviousStateName_t1600272887, ___storeName_11)); }
	inline FsmString_t952858651 * get_storeName_11() const { return ___storeName_11; }
	inline FsmString_t952858651 ** get_address_of_storeName_11() { return &___storeName_11; }
	inline void set_storeName_11(FsmString_t952858651 * value)
	{
		___storeName_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeName_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
