﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>
struct U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t479520291;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::.ctor()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m2761761578_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m2761761578(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m2761761578_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppChar U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1335465147_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1335465147(__this, method) ((  Il2CppChar (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1335465147_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m4106410044_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m4106410044(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m4106410044_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m3119159063_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m3119159063(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m3119159063_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3798368340_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3798368340(__this, method) ((  Il2CppObject* (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3798368340_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::MoveNext()
extern "C"  bool U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2883752874_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2883752874(__this, method) ((  bool (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2883752874_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::Dispose()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3956337895_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3956337895(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3956337895_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Char>::Reset()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m408194519_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m408194519(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t3390549795 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m408194519_gshared)(__this, method)
