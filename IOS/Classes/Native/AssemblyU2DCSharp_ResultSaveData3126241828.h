﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultSaveData
struct  ResultSaveData_t3126241828  : public Il2CppObject
{
public:
	// System.String ResultSaveData::user_id
	String_t* ___user_id_0;
	// System.Int32 ResultSaveData::prctc_grade
	int32_t ___prctc_grade_1;
	// System.Int32 ResultSaveData::prctc_game_id
	int32_t ___prctc_game_id_2;
	// System.Int32 ResultSaveData::prctc_guest_th
	int32_t ___prctc_guest_th_3;
	// System.Single ResultSaveData::prctc_score
	float ___prctc_score_4;
	// System.Single ResultSaveData::prctc_play_time
	float ___prctc_play_time_5;
	// System.Int32 ResultSaveData::prctc_miss_cnt
	int32_t ___prctc_miss_cnt_6;
	// System.Single ResultSaveData::prctc_error_range
	float ___prctc_error_range_7;
	// System.Int32 ResultSaveData::prctc_trial_th
	int32_t ___prctc_trial_th_8;

public:
	inline static int32_t get_offset_of_user_id_0() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___user_id_0)); }
	inline String_t* get_user_id_0() const { return ___user_id_0; }
	inline String_t** get_address_of_user_id_0() { return &___user_id_0; }
	inline void set_user_id_0(String_t* value)
	{
		___user_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_0, value);
	}

	inline static int32_t get_offset_of_prctc_grade_1() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_grade_1)); }
	inline int32_t get_prctc_grade_1() const { return ___prctc_grade_1; }
	inline int32_t* get_address_of_prctc_grade_1() { return &___prctc_grade_1; }
	inline void set_prctc_grade_1(int32_t value)
	{
		___prctc_grade_1 = value;
	}

	inline static int32_t get_offset_of_prctc_game_id_2() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_game_id_2)); }
	inline int32_t get_prctc_game_id_2() const { return ___prctc_game_id_2; }
	inline int32_t* get_address_of_prctc_game_id_2() { return &___prctc_game_id_2; }
	inline void set_prctc_game_id_2(int32_t value)
	{
		___prctc_game_id_2 = value;
	}

	inline static int32_t get_offset_of_prctc_guest_th_3() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_guest_th_3)); }
	inline int32_t get_prctc_guest_th_3() const { return ___prctc_guest_th_3; }
	inline int32_t* get_address_of_prctc_guest_th_3() { return &___prctc_guest_th_3; }
	inline void set_prctc_guest_th_3(int32_t value)
	{
		___prctc_guest_th_3 = value;
	}

	inline static int32_t get_offset_of_prctc_score_4() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_score_4)); }
	inline float get_prctc_score_4() const { return ___prctc_score_4; }
	inline float* get_address_of_prctc_score_4() { return &___prctc_score_4; }
	inline void set_prctc_score_4(float value)
	{
		___prctc_score_4 = value;
	}

	inline static int32_t get_offset_of_prctc_play_time_5() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_play_time_5)); }
	inline float get_prctc_play_time_5() const { return ___prctc_play_time_5; }
	inline float* get_address_of_prctc_play_time_5() { return &___prctc_play_time_5; }
	inline void set_prctc_play_time_5(float value)
	{
		___prctc_play_time_5 = value;
	}

	inline static int32_t get_offset_of_prctc_miss_cnt_6() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_miss_cnt_6)); }
	inline int32_t get_prctc_miss_cnt_6() const { return ___prctc_miss_cnt_6; }
	inline int32_t* get_address_of_prctc_miss_cnt_6() { return &___prctc_miss_cnt_6; }
	inline void set_prctc_miss_cnt_6(int32_t value)
	{
		___prctc_miss_cnt_6 = value;
	}

	inline static int32_t get_offset_of_prctc_error_range_7() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_error_range_7)); }
	inline float get_prctc_error_range_7() const { return ___prctc_error_range_7; }
	inline float* get_address_of_prctc_error_range_7() { return &___prctc_error_range_7; }
	inline void set_prctc_error_range_7(float value)
	{
		___prctc_error_range_7 = value;
	}

	inline static int32_t get_offset_of_prctc_trial_th_8() { return static_cast<int32_t>(offsetof(ResultSaveData_t3126241828, ___prctc_trial_th_8)); }
	inline int32_t get_prctc_trial_th_8() const { return ___prctc_trial_th_8; }
	inline int32_t* get_address_of_prctc_trial_th_8() { return &___prctc_trial_th_8; }
	inline void set_prctc_trial_th_8(int32_t value)
	{
		___prctc_trial_th_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
