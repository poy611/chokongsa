﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.PlayerStats
struct PlayerStats_t60064856;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.BasicApi.PlayerStats::.ctor()
extern "C"  void PlayerStats__ctor_m3220740325 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::.cctor()
extern "C"  void PlayerStats__cctor_m576606056 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::get_Valid()
extern "C"  bool PlayerStats_get_Valid_m4027792866 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_Valid(System.Boolean)
extern "C"  void PlayerStats_set_Valid_m3548614937 (PlayerStats_t60064856 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.PlayerStats::get_NumberOfPurchases()
extern "C"  int32_t PlayerStats_get_NumberOfPurchases_m2019887166 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_NumberOfPurchases(System.Int32)
extern "C"  void PlayerStats_set_NumberOfPurchases_m3471478761 (PlayerStats_t60064856 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_AvgSessonLength()
extern "C"  float PlayerStats_get_AvgSessonLength_m1615915799 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_AvgSessonLength(System.Single)
extern "C"  void PlayerStats_set_AvgSessonLength_m452662428 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.PlayerStats::get_DaysSinceLastPlayed()
extern "C"  int32_t PlayerStats_get_DaysSinceLastPlayed_m4219399448 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_DaysSinceLastPlayed(System.Int32)
extern "C"  void PlayerStats_set_DaysSinceLastPlayed_m2161086275 (PlayerStats_t60064856 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.PlayerStats::get_NumberOfSessions()
extern "C"  int32_t PlayerStats_get_NumberOfSessions_m3369168083 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_NumberOfSessions(System.Int32)
extern "C"  void PlayerStats_set_NumberOfSessions_m1539369738 (PlayerStats_t60064856 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_SessPercentile()
extern "C"  float PlayerStats_get_SessPercentile_m3899612707 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_SessPercentile(System.Single)
extern "C"  void PlayerStats_set_SessPercentile_m1983004352 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_SpendPercentile()
extern "C"  float PlayerStats_get_SpendPercentile_m1250751497 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_SpendPercentile(System.Single)
extern "C"  void PlayerStats_set_SpendPercentile_m607429866 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_SpendProbability()
extern "C"  float PlayerStats_get_SpendProbability_m1201097803 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_SpendProbability(System.Single)
extern "C"  void PlayerStats_set_SpendProbability_m1457216408 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_ChurnProbability()
extern "C"  float PlayerStats_get_ChurnProbability_m3428819325 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_ChurnProbability(System.Single)
extern "C"  void PlayerStats_set_ChurnProbability_m2555807654 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_HighSpenderProbability()
extern "C"  float PlayerStats_get_HighSpenderProbability_m3612754624 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_HighSpenderProbability(System.Single)
extern "C"  void PlayerStats_set_HighSpenderProbability_m104769859 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.BasicApi.PlayerStats::get_TotalSpendNext28Days()
extern "C"  float PlayerStats_get_TotalSpendNext28Days_m2251957918 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayerStats::set_TotalSpendNext28Days(System.Single)
extern "C"  void PlayerStats_set_TotalSpendNext28Days_m700527653 (PlayerStats_t60064856 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasNumberOfPurchases()
extern "C"  bool PlayerStats_HasNumberOfPurchases_m3587533675 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasAvgSessonLength()
extern "C"  bool PlayerStats_HasAvgSessonLength_m2989588226 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasDaysSinceLastPlayed()
extern "C"  bool PlayerStats_HasDaysSinceLastPlayed_m3194173701 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasNumberOfSessions()
extern "C"  bool PlayerStats_HasNumberOfSessions_m4251021318 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasSessPercentile()
extern "C"  bool PlayerStats_HasSessPercentile_m3112640728 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasSpendPercentile()
extern "C"  bool PlayerStats_HasSpendPercentile_m2624423924 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasChurnProbability()
extern "C"  bool PlayerStats_HasChurnProbability_m3062991602 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasHighSpenderProbability()
extern "C"  bool PlayerStats_HasHighSpenderProbability_m1374584949 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayerStats::HasTotalSpendNext28Days()
extern "C"  bool PlayerStats_HasTotalSpendNext28Days_m3384823187 (PlayerStats_t60064856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
