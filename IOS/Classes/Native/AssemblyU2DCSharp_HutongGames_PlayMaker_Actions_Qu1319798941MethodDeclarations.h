﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLerp
struct QuaternionLerp_t1319798941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::.ctor()
extern "C"  void QuaternionLerp__ctor_m1565253737 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::Reset()
extern "C"  void QuaternionLerp_Reset_m3506653974 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnEnter()
extern "C"  void QuaternionLerp_OnEnter_m362016256 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnUpdate()
extern "C"  void QuaternionLerp_OnUpdate_m1766128707 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnLateUpdate()
extern "C"  void QuaternionLerp_OnLateUpdate_m3160991881 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnFixedUpdate()
extern "C"  void QuaternionLerp_OnFixedUpdate_m82445829 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::DoQuatLerp()
extern "C"  void QuaternionLerp_DoQuatLerp_m4055740180 (QuaternionLerp_t1319798941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
