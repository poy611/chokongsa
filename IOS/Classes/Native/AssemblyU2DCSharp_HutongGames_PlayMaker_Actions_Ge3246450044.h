﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmBool
struct  GetFsmBool_t3246450044  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmBool::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmBool::fsmName
	FsmString_t952858651 * ___fsmName_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmBool::variableName
	FsmString_t952858651 * ___variableName_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetFsmBool::storeValue
	FsmBool_t1075959796 * ___storeValue_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmBool::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmBool::goLastFrame
	GameObject_t3674682005 * ___goLastFrame_16;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmBool::fsm
	PlayMakerFSM_t3799847376 * ___fsm_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_fsmName_12() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___fsmName_12)); }
	inline FsmString_t952858651 * get_fsmName_12() const { return ___fsmName_12; }
	inline FsmString_t952858651 ** get_address_of_fsmName_12() { return &___fsmName_12; }
	inline void set_fsmName_12(FsmString_t952858651 * value)
	{
		___fsmName_12 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_12, value);
	}

	inline static int32_t get_offset_of_variableName_13() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___variableName_13)); }
	inline FsmString_t952858651 * get_variableName_13() const { return ___variableName_13; }
	inline FsmString_t952858651 ** get_address_of_variableName_13() { return &___variableName_13; }
	inline void set_variableName_13(FsmString_t952858651 * value)
	{
		___variableName_13 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_13, value);
	}

	inline static int32_t get_offset_of_storeValue_14() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___storeValue_14)); }
	inline FsmBool_t1075959796 * get_storeValue_14() const { return ___storeValue_14; }
	inline FsmBool_t1075959796 ** get_address_of_storeValue_14() { return &___storeValue_14; }
	inline void set_storeValue_14(FsmBool_t1075959796 * value)
	{
		___storeValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeValue_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_16() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___goLastFrame_16)); }
	inline GameObject_t3674682005 * get_goLastFrame_16() const { return ___goLastFrame_16; }
	inline GameObject_t3674682005 ** get_address_of_goLastFrame_16() { return &___goLastFrame_16; }
	inline void set_goLastFrame_16(GameObject_t3674682005 * value)
	{
		___goLastFrame_16 = value;
		Il2CppCodeGenWriteBarrier(&___goLastFrame_16, value);
	}

	inline static int32_t get_offset_of_fsm_17() { return static_cast<int32_t>(offsetof(GetFsmBool_t3246450044, ___fsm_17)); }
	inline PlayMakerFSM_t3799847376 * get_fsm_17() const { return ___fsm_17; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsm_17() { return &___fsm_17; }
	inline void set_fsm_17(PlayMakerFSM_t3799847376 * value)
	{
		___fsm_17 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
