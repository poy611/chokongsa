﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Runtime.InteropServices.HandleRef>
struct DefaultComparer_t228595968;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Runtime.InteropServices.HandleRef>::.ctor()
extern "C"  void DefaultComparer__ctor_m771776175_gshared (DefaultComparer_t228595968 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m771776175(__this, method) ((  void (*) (DefaultComparer_t228595968 *, const MethodInfo*))DefaultComparer__ctor_m771776175_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Runtime.InteropServices.HandleRef>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2039486820_gshared (DefaultComparer_t228595968 * __this, HandleRef_t1780819301  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2039486820(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t228595968 *, HandleRef_t1780819301 , const MethodInfo*))DefaultComparer_GetHashCode_m2039486820_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Runtime.InteropServices.HandleRef>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3562911044_gshared (DefaultComparer_t228595968 * __this, HandleRef_t1780819301  ___x0, HandleRef_t1780819301  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3562911044(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t228595968 *, HandleRef_t1780819301 , HandleRef_t1780819301 , const MethodInfo*))DefaultComparer_Equals_m3562911044_gshared)(__this, ___x0, ___y1, method)
