﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddTorque
struct AddTorque_t135433561;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddTorque::.ctor()
extern "C"  void AddTorque__ctor_m2052156413 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::Reset()
extern "C"  void AddTorque_Reset_m3993556650 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnPreprocess()
extern "C"  void AddTorque_OnPreprocess_m3490162322 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnEnter()
extern "C"  void AddTorque_OnEnter_m124052628 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnFixedUpdate()
extern "C"  void AddTorque_OnFixedUpdate_m751091097 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::DoAddTorque()
extern "C"  void AddTorque_DoAddTorque_m3986550619 (AddTorque_t135433561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
