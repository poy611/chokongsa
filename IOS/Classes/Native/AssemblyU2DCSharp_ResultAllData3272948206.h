﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultAllData
struct  ResultAllData_t3272948206  : public Il2CppObject
{
public:
	// System.Single ResultAllData::prctcScore
	float ___prctcScore_0;
	// System.Int32 ResultAllData::prctcGameId
	int32_t ___prctcGameId_1;
	// System.Single ResultAllData::prctcPlayTime
	float ___prctcPlayTime_2;
	// System.Int32 ResultAllData::prctcGrade
	int32_t ___prctcGrade_3;
	// System.Single ResultAllData::prctcErrorRange
	float ___prctcErrorRange_4;
	// System.Int32 ResultAllData::prctcGuestTh
	int32_t ___prctcGuestTh_5;
	// System.String ResultAllData::prctcRecordDate
	String_t* ___prctcRecordDate_6;
	// System.Int32 ResultAllData::prctcTrialTh
	int32_t ___prctcTrialTh_7;
	// System.String ResultAllData::userId
	String_t* ___userId_8;
	// System.Int32 ResultAllData::prctcMissCnt
	int32_t ___prctcMissCnt_9;

public:
	inline static int32_t get_offset_of_prctcScore_0() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcScore_0)); }
	inline float get_prctcScore_0() const { return ___prctcScore_0; }
	inline float* get_address_of_prctcScore_0() { return &___prctcScore_0; }
	inline void set_prctcScore_0(float value)
	{
		___prctcScore_0 = value;
	}

	inline static int32_t get_offset_of_prctcGameId_1() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcGameId_1)); }
	inline int32_t get_prctcGameId_1() const { return ___prctcGameId_1; }
	inline int32_t* get_address_of_prctcGameId_1() { return &___prctcGameId_1; }
	inline void set_prctcGameId_1(int32_t value)
	{
		___prctcGameId_1 = value;
	}

	inline static int32_t get_offset_of_prctcPlayTime_2() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcPlayTime_2)); }
	inline float get_prctcPlayTime_2() const { return ___prctcPlayTime_2; }
	inline float* get_address_of_prctcPlayTime_2() { return &___prctcPlayTime_2; }
	inline void set_prctcPlayTime_2(float value)
	{
		___prctcPlayTime_2 = value;
	}

	inline static int32_t get_offset_of_prctcGrade_3() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcGrade_3)); }
	inline int32_t get_prctcGrade_3() const { return ___prctcGrade_3; }
	inline int32_t* get_address_of_prctcGrade_3() { return &___prctcGrade_3; }
	inline void set_prctcGrade_3(int32_t value)
	{
		___prctcGrade_3 = value;
	}

	inline static int32_t get_offset_of_prctcErrorRange_4() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcErrorRange_4)); }
	inline float get_prctcErrorRange_4() const { return ___prctcErrorRange_4; }
	inline float* get_address_of_prctcErrorRange_4() { return &___prctcErrorRange_4; }
	inline void set_prctcErrorRange_4(float value)
	{
		___prctcErrorRange_4 = value;
	}

	inline static int32_t get_offset_of_prctcGuestTh_5() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcGuestTh_5)); }
	inline int32_t get_prctcGuestTh_5() const { return ___prctcGuestTh_5; }
	inline int32_t* get_address_of_prctcGuestTh_5() { return &___prctcGuestTh_5; }
	inline void set_prctcGuestTh_5(int32_t value)
	{
		___prctcGuestTh_5 = value;
	}

	inline static int32_t get_offset_of_prctcRecordDate_6() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcRecordDate_6)); }
	inline String_t* get_prctcRecordDate_6() const { return ___prctcRecordDate_6; }
	inline String_t** get_address_of_prctcRecordDate_6() { return &___prctcRecordDate_6; }
	inline void set_prctcRecordDate_6(String_t* value)
	{
		___prctcRecordDate_6 = value;
		Il2CppCodeGenWriteBarrier(&___prctcRecordDate_6, value);
	}

	inline static int32_t get_offset_of_prctcTrialTh_7() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcTrialTh_7)); }
	inline int32_t get_prctcTrialTh_7() const { return ___prctcTrialTh_7; }
	inline int32_t* get_address_of_prctcTrialTh_7() { return &___prctcTrialTh_7; }
	inline void set_prctcTrialTh_7(int32_t value)
	{
		___prctcTrialTh_7 = value;
	}

	inline static int32_t get_offset_of_userId_8() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___userId_8)); }
	inline String_t* get_userId_8() const { return ___userId_8; }
	inline String_t** get_address_of_userId_8() { return &___userId_8; }
	inline void set_userId_8(String_t* value)
	{
		___userId_8 = value;
		Il2CppCodeGenWriteBarrier(&___userId_8, value);
	}

	inline static int32_t get_offset_of_prctcMissCnt_9() { return static_cast<int32_t>(offsetof(ResultAllData_t3272948206, ___prctcMissCnt_9)); }
	inline int32_t get_prctcMissCnt_9() const { return ___prctcMissCnt_9; }
	inline int32_t* get_address_of_prctcMissCnt_9() { return &___prctcMissCnt_9; }
	inline void set_prctcMissCnt_9(int32_t value)
	{
		___prctcMissCnt_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
