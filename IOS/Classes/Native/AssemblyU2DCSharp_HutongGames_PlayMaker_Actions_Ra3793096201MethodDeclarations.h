﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RayCast2d
struct RayCast2d_t3793096201;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RayCast2d::.ctor()
extern "C"  void RayCast2d__ctor_m3736842573 (RayCast2d_t3793096201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RayCast2d::Reset()
extern "C"  void RayCast2d_Reset_m1383275514 (RayCast2d_t3793096201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RayCast2d::OnEnter()
extern "C"  void RayCast2d_OnEnter_m4199749092 (RayCast2d_t3793096201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RayCast2d::OnUpdate()
extern "C"  void RayCast2d_OnUpdate_m476762335 (RayCast2d_t3793096201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RayCast2d::DoRaycast()
extern "C"  void RayCast2d_DoRaycast_m2242799945 (RayCast2d_t3793096201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
