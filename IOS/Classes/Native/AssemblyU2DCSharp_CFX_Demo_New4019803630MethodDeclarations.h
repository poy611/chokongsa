﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_New
struct CFX_Demo_New_t4019803630;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void CFX_Demo_New::.ctor()
extern "C"  void CFX_Demo_New__ctor_m2468702317 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::Awake()
extern "C"  void CFX_Demo_New_Awake_m2706307536 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::Update()
extern "C"  void CFX_Demo_New_Update_m947222592 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::OnToggleGround()
extern "C"  void CFX_Demo_New_OnToggleGround_m2484928177 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::OnToggleCamera()
extern "C"  void CFX_Demo_New_OnToggleCamera_m1829042383 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::OnToggleSlowMo()
extern "C"  void CFX_Demo_New_OnToggleSlowMo_m894635437 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::OnPreviousEffect()
extern "C"  void CFX_Demo_New_OnPreviousEffect_m3212437406 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::OnNextEffect()
extern "C"  void CFX_Demo_New_OnNextEffect_m2289229338 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::UpdateUI()
extern "C"  void CFX_Demo_New_UpdateUI_m4044184084 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_Demo_New::spawnParticle()
extern "C"  GameObject_t3674682005 * CFX_Demo_New_spawnParticle_m1097580535 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_Demo_New::CheckForDeletedParticles()
extern "C"  Il2CppObject * CFX_Demo_New_CheckForDeletedParticles_m2105398420 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::prevParticle()
extern "C"  void CFX_Demo_New_prevParticle_m4091551920 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::nextParticle()
extern "C"  void CFX_Demo_New_nextParticle_m2078480752 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_New::destroyParticles()
extern "C"  void CFX_Demo_New_destroyParticles_m4236589354 (CFX_Demo_New_t4019803630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CFX_Demo_New::<Awake>m__D9(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  int32_t CFX_Demo_New_U3CAwakeU3Em__D9_m1094724276 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___o10, GameObject_t3674682005 * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
