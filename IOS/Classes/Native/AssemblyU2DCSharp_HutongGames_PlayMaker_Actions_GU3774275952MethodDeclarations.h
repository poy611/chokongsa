﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutTextField
struct GUILayoutTextField_t3774275952;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::.ctor()
extern "C"  void GUILayoutTextField__ctor_m4138203382 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::Reset()
extern "C"  void GUILayoutTextField_Reset_m1784636323 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutTextField::OnGUI()
extern "C"  void GUILayoutTextField_OnGUI_m3633602032 (GUILayoutTextField_t3774275952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
