﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t1627299724;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3225298463_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3225298463(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3225298463_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  IntPtr_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1020078060_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1020078060(__this, method) ((  IntPtr_t (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1020078060_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2949923943_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2949923943(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2949923943_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1759652482_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1759652482(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1759652482_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1027735433_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1027735433(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1027735433_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4192229525_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4192229525(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m4192229525_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2738685596_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2738685596(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2738685596_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m871731404_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m871731404(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t4210414661 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m871731404_gshared)(__this, method)
