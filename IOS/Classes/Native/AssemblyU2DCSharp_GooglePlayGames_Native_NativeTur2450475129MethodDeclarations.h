﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey94
struct U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey94_t2450475129;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey94::.ctor()
extern "C"  void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey94__ctor_m838447378 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey94_t2450475129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey94::<>m__83(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey94_U3CU3Em__83_m31737395 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey94_t2450475129 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
