﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed
struct  SetAnimatorPlayBackSpeed_t3822504587  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::playBackSpeed
	FsmFloat_t2134102846 * ___playBackSpeed_12;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::_animator
	Animator_t2776330603 * ____animator_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t3822504587, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_playBackSpeed_12() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t3822504587, ___playBackSpeed_12)); }
	inline FsmFloat_t2134102846 * get_playBackSpeed_12() const { return ___playBackSpeed_12; }
	inline FsmFloat_t2134102846 ** get_address_of_playBackSpeed_12() { return &___playBackSpeed_12; }
	inline void set_playBackSpeed_12(FsmFloat_t2134102846 * value)
	{
		___playBackSpeed_12 = value;
		Il2CppCodeGenWriteBarrier(&___playBackSpeed_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t3822504587, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t3822504587, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
