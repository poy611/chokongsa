﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3792965912.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3399470600_gshared (Enumerator_t3792965912 * __this, Dictionary_2_t3178029858 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3399470600(__this, ___host0, method) ((  void (*) (Enumerator_t3792965912 *, Dictionary_2_t3178029858 *, const MethodInfo*))Enumerator__ctor_m3399470600_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3802876963_gshared (Enumerator_t3792965912 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3802876963(__this, method) ((  Il2CppObject * (*) (Enumerator_t3792965912 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3802876963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m635702701_gshared (Enumerator_t3792965912 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m635702701(__this, method) ((  void (*) (Enumerator_t3792965912 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m635702701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m1493083818_gshared (Enumerator_t3792965912 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1493083818(__this, method) ((  void (*) (Enumerator_t3792965912 *, const MethodInfo*))Enumerator_Dispose_m1493083818_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2254198109_gshared (Enumerator_t3792965912 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2254198109(__this, method) ((  bool (*) (Enumerator_t3792965912 *, const MethodInfo*))Enumerator_MoveNext_m2254198109_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1547675803_gshared (Enumerator_t3792965912 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1547675803(__this, method) ((  int32_t (*) (Enumerator_t3792965912 *, const MethodInfo*))Enumerator_get_Current_m1547675803_gshared)(__this, method)
