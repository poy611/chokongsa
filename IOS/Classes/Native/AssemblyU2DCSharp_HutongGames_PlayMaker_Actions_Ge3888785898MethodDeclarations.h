﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTrigger2dInfo
struct GetTrigger2dInfo_t3888785898;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::.ctor()
extern "C"  void GetTrigger2dInfo__ctor_m1638809532 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::Reset()
extern "C"  void GetTrigger2dInfo_Reset_m3580209769 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::StoreTriggerInfo()
extern "C"  void GetTrigger2dInfo_StoreTriggerInfo_m4123633965 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::OnEnter()
extern "C"  void GetTrigger2dInfo_OnEnter_m2329658515 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
