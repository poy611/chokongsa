﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FinishFSM
struct FinishFSM_t701881083;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FinishFSM::.ctor()
extern "C"  void FinishFSM__ctor_m1366185115 (FinishFSM_t701881083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FinishFSM::OnEnter()
extern "C"  void FinishFSM_OnEnter_m2330598834 (FinishFSM_t701881083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
