﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultPageManager
struct ResultPageManager_t3629511073;
// ResponseResultAllData
struct ResponseResultAllData_t4071630637;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResponseResultAllData4071630637.h"

// System.Void ResultPageManager::.ctor()
extern "C"  void ResultPageManager__ctor_m348979434 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::Awake()
extern "C"  void ResultPageManager_Awake_m586584653 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::OnSuccessLoadData(ResponseResultAllData)
extern "C"  void ResultPageManager_OnSuccessLoadData_m2427040553 (ResultPageManager_t3629511073 * __this, ResponseResultAllData_t4071630637 * ___allData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::OnFailLoadData()
extern "C"  void ResultPageManager_OnFailLoadData_m322568967 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::csSwitch(System.Single)
extern "C"  void ResultPageManager_csSwitch_m4232733997 (ResultPageManager_t3629511073 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::SetTrialImage()
extern "C"  void ResultPageManager_SetTrialImage_m2363178799 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ResultPageManager::AverageScoreByTrial(System.Int32)
extern "C"  float ResultPageManager_AverageScoreByTrial_m494046511 (ResultPageManager_t3629511073 * __this, int32_t ___trialTh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::NextPageButton()
extern "C"  void ResultPageManager_NextPageButton_m4007252174 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::PrevPageButton()
extern "C"  void ResultPageManager_PrevPageButton_m1538394126 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::AverageByTrial(System.Int32)
extern "C"  void ResultPageManager_AverageByTrial_m1021179597 (ResultPageManager_t3629511073 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultPageManager::OnDestroy()
extern "C"  void ResultPageManager_OnDestroy_m2197836003 (ResultPageManager_t3629511073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
