﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutSpace
struct GUILayoutSpace_t3585978025;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::.ctor()
extern "C"  void GUILayoutSpace__ctor_m1976879069 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::Reset()
extern "C"  void GUILayoutSpace_Reset_m3918279306 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutSpace::OnGUI()
extern "C"  void GUILayoutSpace_OnGUI_m1472277719 (GUILayoutSpace_t3585978025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
