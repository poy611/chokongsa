﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedColle4035875907MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::.ctor()
#define KeyedCollection_2__ctor_m338514250(__this, method) ((  void (*) (KeyedCollection_2_t3713061183 *, const MethodInfo*))KeyedCollection_2__ctor_m2938177468_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define KeyedCollection_2__ctor_m18255289(__this, ___comparer0, method) ((  void (*) (KeyedCollection_2_t3713061183 *, Il2CppObject*, const MethodInfo*))KeyedCollection_2__ctor_m935917619_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>,System.Int32)
#define KeyedCollection_2__ctor_m2273224630(__this, ___comparer0, ___dictionaryCreationThreshold1, method) ((  void (*) (KeyedCollection_2_t3713061183 *, Il2CppObject*, int32_t, const MethodInfo*))KeyedCollection_2__ctor_m1994149764_gshared)(__this, ___comparer0, ___dictionaryCreationThreshold1, method)
// System.Boolean System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::Contains(TKey)
#define KeyedCollection_2_Contains_m557920960(__this, ___key0, method) ((  bool (*) (KeyedCollection_2_t3713061183 *, String_t*, const MethodInfo*))KeyedCollection_2_Contains_m2392882714_gshared)(__this, ___key0, method)
// System.Int32 System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::IndexOfKey(TKey)
#define KeyedCollection_2_IndexOfKey_m2744280161(__this, ___key0, method) ((  int32_t (*) (KeyedCollection_2_t3713061183 *, String_t*, const MethodInfo*))KeyedCollection_2_IndexOfKey_m1524122743_gshared)(__this, ___key0, method)
// TItem System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::get_Item(TKey)
#define KeyedCollection_2_get_Item_m3426789782(__this, ___key0, method) ((  JsonProperty_t902655177 * (*) (KeyedCollection_2_t3713061183 *, String_t*, const MethodInfo*))KeyedCollection_2_get_Item_m4267281901_gshared)(__this, ___key0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::ClearItems()
#define KeyedCollection_2_ClearItems_m4290819245(__this, method) ((  void (*) (KeyedCollection_2_t3713061183 *, const MethodInfo*))KeyedCollection_2_ClearItems_m2570532219_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::InsertItem(System.Int32,TItem)
#define KeyedCollection_2_InsertItem_m1758200508(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3713061183 *, int32_t, JsonProperty_t902655177 *, const MethodInfo*))KeyedCollection_2_InsertItem_m2159881226_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::RemoveItem(System.Int32)
#define KeyedCollection_2_RemoveItem_m295494594(__this, ___index0, method) ((  void (*) (KeyedCollection_2_t3713061183 *, int32_t, const MethodInfo*))KeyedCollection_2_RemoveItem_m2596446096_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::SetItem(System.Int32,TItem)
#define KeyedCollection_2_SetItem_m906305171(__this, ___index0, ___item1, method) ((  void (*) (KeyedCollection_2_t3713061183 *, int32_t, JsonProperty_t902655177 *, const MethodInfo*))KeyedCollection_2_SetItem_m874457093_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IDictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>::get_Dictionary()
#define KeyedCollection_2_get_Dictionary_m41742093(__this, method) ((  Il2CppObject* (*) (KeyedCollection_2_t3713061183 *, const MethodInfo*))KeyedCollection_2_get_Dictionary_m1700228474_gshared)(__this, method)
