﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionData/Context
struct Context_t1489737484;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.ActionData/Context::.ctor()
extern "C"  void Context__ctor_m1873146577 (Context_t1489737484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
