﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615769.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615732.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676616792.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A435478332.h"
#include "System_Xml_U3CModuleU3E86524790.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser2461941858.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser_YYRules237985297.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyException1999805465.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyUnexpectedEof3814908085.h"
#include "System_Xml_System_MonoTODOAttribute2091695241.h"
#include "System_Xml_Mono_Xml_XPath_XPathEditableDocument61026146.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentEditableNaviga792096397.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet1710562669.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType730532811.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType1663577061.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic230829008.h"
#include "System_Xml_Mono_Xml_Schema_XsdString3497764096.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString3069561175.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2036792492.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage2730590407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken280424909.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens613775752.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1434515194.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName3308285615.h"
#include "System_Xml_Mono_Xml_Schema_XsdID744268010.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2025328715.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs3166186186.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity3091474642.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities2242065712.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation1627605361.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal696098180.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1111886705.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong1434469099.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt2108011298.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort2035664687.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1434180983.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger4252412075.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1954151808.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2817511917.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort965959482.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte1953863692.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger346877240.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger4018722407.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger580566908.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat2023777551.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble3063791808.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3891592351.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary4085742767.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2033029455.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean3513513563.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI2977086671.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration2352167683.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration621607249.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration36650571.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime1841962250.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate1434217501.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime1434701628.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth1947427211.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay297523254.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear2024125431.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth3118112584.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay1434278436.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated4226823396.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaAttribute2904598248.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaCompilationS1577913490.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype196391954.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMet998439334.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaElement3664762632.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet2982631811.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet3197530270.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaInfo4089849692.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaObject3280570797.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaParticle3890186420.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSet4125473774.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleType3060492794.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeCo1889388569.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeLi1048666168.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeRe2016622060.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaSimpleTypeUn3206834991.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaType4090188264.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil4090213040.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity1280069984.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAt1769351841.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2146545409.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut927884470.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri2315070405.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerNa142497835.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions181677634.h"
#include "System_Xml_System_Xml_XPath_XPathFunction3895226859.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238936), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1406[12] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (U24ArrayTypeU24136_t1676615770)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1676615770_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (U24ArrayTypeU24120_t1676615733)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615733_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (U24ArrayTypeU24256_t1676616794)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616794_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (U24ArrayTypeU241024_t435478333)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478333_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (U3CModuleU3E_t86524794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (XPathParser_t2461941858), -1, sizeof(XPathParser_t2461941858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1412[17] = 
{
	XPathParser_t2461941858::get_offset_of_Context_0(),
	XPathParser_t2461941858::get_offset_of_ErrorOutput_1(),
	XPathParser_t2461941858::get_offset_of_eof_token_2(),
	XPathParser_t2461941858::get_offset_of_debug_3(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyFinal_4(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyNames_5(),
	XPathParser_t2461941858::get_offset_of_yyExpectingState_6(),
	XPathParser_t2461941858::get_offset_of_yyMax_7(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyLhs_8(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyLen_9(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyDefRed_10(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyDgoto_11(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yySindex_12(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyRindex_13(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyGindex_14(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyTable_15(),
	XPathParser_t2461941858_StaticFields::get_offset_of_yyCheck_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (YYRules_t237985297), -1, sizeof(YYRules_t237985297_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1413[1] = 
{
	YYRules_t237985297_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (yyException_t1999805465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (yyUnexpectedEof_t3814908085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (MonoTODOAttribute_t2091695244), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (XPathEditableDocument_t61026146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1419[1] = 
{
	XPathEditableDocument_t61026146::get_offset_of_node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (XmlDocumentEditableNavigator_t792096397), -1, sizeof(XmlDocumentEditableNavigator_t792096397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1420[3] = 
{
	XmlDocumentEditableNavigator_t792096397_StaticFields::get_offset_of_isXmlDocumentNavigatorImpl_2(),
	XmlDocumentEditableNavigator_t792096397::get_offset_of_document_3(),
	XmlDocumentEditableNavigator_t792096397::get_offset_of_navigator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (XsdWhitespaceFacet_t1710562669)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1422[4] = 
{
	XsdWhitespaceFacet_t1710562669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (XsdAnySimpleType_t730532811), -1, sizeof(XsdAnySimpleType_t730532811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1423[6] = 
{
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t730532811_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (XdtAnyAtomicType_t1663577061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (XdtUntypedAtomic_t230829008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (XsdString_t3497764096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (XsdNormalizedString_t3069561175), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (XsdToken_t2036792492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (XsdLanguage_t2730590407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (XsdNMToken_t280424909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (XsdNMTokens_t613775752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (XsdName_t1434515194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (XsdNCName_t3308285615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (XsdID_t744268010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (XsdIDRef_t2025328715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (XsdIDRefs_t3166186186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (XsdEntity_t3091474642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (XsdEntities_t2242065712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (XsdNotation_t1627605361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (XsdDecimal_t696098180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (XsdInteger_t1111886705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (XsdLong_t1434469099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (XsdInt_t2108011298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (XsdShort_t2035664687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (XsdByte_t1434180983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (XsdNonNegativeInteger_t4252412075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (XsdUnsignedLong_t1954151808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (XsdUnsignedInt_t2817511917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (XsdUnsignedShort_t965959482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (XsdUnsignedByte_t1953863692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (XsdPositiveInteger_t346877240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (XsdNonPositiveInteger_t4018722407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (XsdNegativeInteger_t580566908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (XsdFloat_t2023777551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (XsdDouble_t3063791808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (XsdBase64Binary_t3891592351), -1, sizeof(XsdBase64Binary_t3891592351_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1456[2] = 
{
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t3891592351_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (XsdHexBinary_t4085742767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (XsdQName_t2033029455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (XsdBoolean_t3513513563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (XsdAnyURI_t2977086671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (XsdDuration_t2352167683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (XdtDayTimeDuration_t621607249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (XdtYearMonthDuration_t36650571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (XsdDateTime_t1841962250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (XsdDate_t1434217501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (XsdTime_t1434701628), -1, sizeof(XsdTime_t1434701628_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1466[1] = 
{
	XsdTime_t1434701628_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (XsdGYearMonth_t1947427211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (XsdGMonthDay_t297523254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (XsdGYear_t2024125431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (XsdGMonth_t3118112584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (XsdGDay_t1434278436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (XmlSchemaAnnotated_t4226823396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (XmlSchemaAttribute_t2904598248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (XmlSchemaCompilationSettings_t1577913490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1475[1] = 
{
	XmlSchemaCompilationSettings_t1577913490::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (XmlSchemaDatatype_t196391954), -1, sizeof(XmlSchemaDatatype_t196391954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1476[55] = 
{
	XmlSchemaDatatype_t196391954::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t196391954::get_offset_of_sb_2(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t196391954_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (XmlSchemaDerivationMethod_t998439334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1477[9] = 
{
	XmlSchemaDerivationMethod_t998439334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (XmlSchemaElement_t3664762632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (XmlSchemaFacet_t2982631811), -1, sizeof(XmlSchemaFacet_t2982631811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1479[1] = 
{
	XmlSchemaFacet_t2982631811_StaticFields::get_offset_of_AllFacets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (Facet_t3197530270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1480[14] = 
{
	Facet_t3197530270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (XmlSchemaInfo_t4089849692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1481[7] = 
{
	XmlSchemaInfo_t4089849692::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t4089849692::get_offset_of_isNil_1(),
	XmlSchemaInfo_t4089849692::get_offset_of_memberType_2(),
	XmlSchemaInfo_t4089849692::get_offset_of_attr_3(),
	XmlSchemaInfo_t4089849692::get_offset_of_elem_4(),
	XmlSchemaInfo_t4089849692::get_offset_of_type_5(),
	XmlSchemaInfo_t4089849692::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (XmlSchemaObject_t3280570797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1482[3] = 
{
	XmlSchemaObject_t3280570797::get_offset_of_namespaces_0(),
	XmlSchemaObject_t3280570797::get_offset_of_unhandledAttributeList_1(),
	XmlSchemaObject_t3280570797::get_offset_of_CompilationId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (XmlSchemaParticle_t3890186420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (XmlSchemaSet_t4125473774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1484[5] = 
{
	XmlSchemaSet_t4125473774::get_offset_of_nameTable_0(),
	XmlSchemaSet_t4125473774::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t4125473774::get_offset_of_schemas_2(),
	XmlSchemaSet_t4125473774::get_offset_of_settings_3(),
	XmlSchemaSet_t4125473774::get_offset_of_CompilationId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (XmlSchemaSimpleType_t3060492794), -1, sizeof(XmlSchemaSimpleType_t3060492794_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1485[53] = 
{
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_schemaLocationType_9(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_content_10(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_islocal_11(),
	XmlSchemaSimpleType_t3060492794::get_offset_of_variety_12(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnySimpleType_13(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsString_14(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBoolean_15(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDecimal_16(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsFloat_17(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDouble_18(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDuration_19(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDateTime_20(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsTime_21(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsDate_22(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYearMonth_23(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGYear_24(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonthDay_25(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGDay_26(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsGMonth_27(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsHexBinary_28(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsBase64Binary_29(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsAnyUri_30(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsQName_31(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNotation_32(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNormalizedString_33(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsToken_34(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLanguage_35(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMToken_36(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNMTokens_37(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsName_38(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNCName_39(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsID_40(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRef_41(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsIDRefs_42(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntity_43(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsEntities_44(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInteger_45(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonPositiveInteger_46(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNegativeInteger_47(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsLong_48(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsInt_49(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsShort_50(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsByte_51(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsNonNegativeInteger_52(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedLong_53(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedInt_54(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedShort_55(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsUnsignedByte_56(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XsPositiveInteger_57(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtUntypedAtomic_58(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtAnyAtomicType_59(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtYearMonthDuration_60(),
	XmlSchemaSimpleType_t3060492794_StaticFields::get_offset_of_XdtDayTimeDuration_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (XmlSchemaSimpleTypeContent_t1889388569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (XmlSchemaSimpleTypeList_t1048666168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[2] = 
{
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemType_3(),
	XmlSchemaSimpleTypeList_t1048666168::get_offset_of_itemTypeName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (XmlSchemaSimpleTypeRestriction_t2016622060), -1, sizeof(XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1488[2] = 
{
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_lengthStyle_3(),
	XmlSchemaSimpleTypeRestriction_t2016622060_StaticFields::get_offset_of_listFacets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (XmlSchemaSimpleTypeUnion_t3206834991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (XmlSchemaType_t4090188264), -1, sizeof(XmlSchemaType_t4090188264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1490[6] = 
{
	XmlSchemaType_t4090188264::get_offset_of_final_3(),
	XmlSchemaType_t4090188264::get_offset_of_BaseXmlSchemaTypeInternal_4(),
	XmlSchemaType_t4090188264::get_offset_of_DatatypeInternal_5(),
	XmlSchemaType_t4090188264::get_offset_of_QNameInternal_6(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_7(),
	XmlSchemaType_t4090188264_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (XmlSchemaUtil_t4090213040), -1, sizeof(XmlSchemaUtil_t4090213040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1491[4] = 
{
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t4090213040_StaticFields::get_offset_of_StrictMsCompliant_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (XmlSchemaValidity_t1280069984)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1492[4] = 
{
	XmlSchemaValidity_t1280069984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (XmlAttributeAttribute_t1769351841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1493[1] = 
{
	XmlAttributeAttribute_t1769351841::get_offset_of_attributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (XmlElementAttribute_t2146545409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1494[3] = 
{
	XmlElementAttribute_t2146545409::get_offset_of_elementName_0(),
	XmlElementAttribute_t2146545409::get_offset_of_type_1(),
	XmlElementAttribute_t2146545409::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (XmlEnumAttribute_t927884470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1495[1] = 
{
	XmlEnumAttribute_t927884470::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (XmlIgnoreAttribute_t2315070405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (XmlSerializerNamespaces_t142497835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1497[1] = 
{
	XmlSerializerNamespaces_t142497835::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (XPathFunctions_t181677634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (XPathFunction_t3895226859), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
