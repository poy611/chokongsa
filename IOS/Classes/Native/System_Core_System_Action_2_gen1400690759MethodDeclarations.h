﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen4293064463MethodDeclarations.h"

// System.Void System.Action`2<System.Byte[],System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2279466932(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1400690759 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2309492639_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Byte[],System.Byte[]>::Invoke(T1,T2)
#define Action_2_Invoke_m3683602743(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1400690759 *, ByteU5BU5D_t4260760469*, ByteU5BU5D_t4260760469*, const MethodInfo*))Action_2_Invoke_m172731500_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Byte[],System.Byte[]>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1340994822(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1400690759 *, ByteU5BU5D_t4260760469*, ByteU5BU5D_t4260760469*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3413657584_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Byte[],System.Byte[]>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3954886868(__this, ___result0, method) ((  void (*) (Action_2_t1400690759 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3926193450_gshared)(__this, ___result0, method)
