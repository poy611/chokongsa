﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorTrigger
struct SetAnimatorTrigger_t2149117047;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::.ctor()
extern "C"  void SetAnimatorTrigger__ctor_m329541071 (SetAnimatorTrigger_t2149117047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::Reset()
extern "C"  void SetAnimatorTrigger_Reset_m2270941308 (SetAnimatorTrigger_t2149117047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::OnEnter()
extern "C"  void SetAnimatorTrigger_OnEnter_m2548085222 (SetAnimatorTrigger_t2149117047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::SetTrigger()
extern "C"  void SetAnimatorTrigger_SetTrigger_m3471379467 (SetAnimatorTrigger_t2149117047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
