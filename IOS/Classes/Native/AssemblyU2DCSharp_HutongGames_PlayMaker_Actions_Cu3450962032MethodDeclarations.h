﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CutToCamera
struct CutToCamera_t3450962032;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void HutongGames.PlayMaker.Actions.CutToCamera::.ctor()
extern "C"  void CutToCamera__ctor_m1766254790 (CutToCamera_t3450962032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::Reset()
extern "C"  void CutToCamera_Reset_m3707655027 (CutToCamera_t3450962032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::OnEnter()
extern "C"  void CutToCamera_OnEnter_m250499869 (CutToCamera_t3450962032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::OnExit()
extern "C"  void CutToCamera_OnExit_m432642267 (CutToCamera_t3450962032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::SwitchCamera(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void CutToCamera_SwitchCamera_m969097967 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___camera10, Camera_t2727095145 * ___camera21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
