﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2661426558MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3652876456(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2751370656 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m1546088691_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::Invoke(T1,T2)
#define Action_2_Invoke_m4107382483(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2751370656 *, int32_t, ByteU5BU5D_t4260760469*, const MethodInfo*))Action_2_Invoke_m2057936216_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m753259682(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2751370656 *, int32_t, ByteU5BU5D_t4260760469*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m2131733111_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m3895754424(__this, ___result0, method) ((  void (*) (Action_2_t2751370656 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1323783299_gshared)(__this, ___result0, method)
