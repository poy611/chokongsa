﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetASine
struct GetASine_t1695390080;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetASine::.ctor()
extern "C"  void GetASine__ctor_m347181286 (GetASine_t1695390080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::Reset()
extern "C"  void GetASine_Reset_m2288581523 (GetASine_t1695390080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnEnter()
extern "C"  void GetASine_OnEnter_m2320462653 (GetASine_t1695390080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnUpdate()
extern "C"  void GetASine_OnUpdate_m2348424870 (GetASine_t1695390080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::DoASine()
extern "C"  void GetASine_DoASine_m2432497991 (GetASine_t1695390080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
