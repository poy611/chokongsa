﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GradeData
struct  GradeData_t483491841  : public Il2CppObject
{
public:
	// System.Int32 GradeData::grand_up
	int32_t ___grand_up_0;
	// System.String GradeData::name
	String_t* ___name_1;
	// System.String GradeData::teacher_name
	String_t* ___teacher_name_2;
	// System.Int32 GradeData::guest_cnt
	int32_t ___guest_cnt_3;

public:
	inline static int32_t get_offset_of_grand_up_0() { return static_cast<int32_t>(offsetof(GradeData_t483491841, ___grand_up_0)); }
	inline int32_t get_grand_up_0() const { return ___grand_up_0; }
	inline int32_t* get_address_of_grand_up_0() { return &___grand_up_0; }
	inline void set_grand_up_0(int32_t value)
	{
		___grand_up_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GradeData_t483491841, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_teacher_name_2() { return static_cast<int32_t>(offsetof(GradeData_t483491841, ___teacher_name_2)); }
	inline String_t* get_teacher_name_2() const { return ___teacher_name_2; }
	inline String_t** get_address_of_teacher_name_2() { return &___teacher_name_2; }
	inline void set_teacher_name_2(String_t* value)
	{
		___teacher_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___teacher_name_2, value);
	}

	inline static int32_t get_offset_of_guest_cnt_3() { return static_cast<int32_t>(offsetof(GradeData_t483491841, ___guest_cnt_3)); }
	inline int32_t get_guest_cnt_3() const { return ___guest_cnt_3; }
	inline int32_t* get_address_of_guest_cnt_3() { return &___guest_cnt_3; }
	inline void set_guest_cnt_3(int32_t value)
	{
		___guest_cnt_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
