﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetJointBreak2dInfo
struct GetJointBreak2dInfo_t936465433;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::.ctor()
extern "C"  void GetJointBreak2dInfo__ctor_m286833469 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::Reset()
extern "C"  void GetJointBreak2dInfo_Reset_m2228233706 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::StoreInfo()
extern "C"  void GetJointBreak2dInfo_StoreInfo_m2960510154 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::OnEnter()
extern "C"  void GetJointBreak2dInfo_OnEnter_m160785364 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
