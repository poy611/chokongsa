﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyObject
struct  DestroyObject_t317818279  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DestroyObject::gameObject
	FsmGameObject_t1697147867 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DestroyObject::delay
	FsmFloat_t2134102846 * ___delay_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroyObject::detachChildren
	FsmBool_t1075959796 * ___detachChildren_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___gameObject_11)); }
	inline FsmGameObject_t1697147867 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t1697147867 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___delay_12)); }
	inline FsmFloat_t2134102846 * get_delay_12() const { return ___delay_12; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(FsmFloat_t2134102846 * value)
	{
		___delay_12 = value;
		Il2CppCodeGenWriteBarrier(&___delay_12, value);
	}

	inline static int32_t get_offset_of_detachChildren_13() { return static_cast<int32_t>(offsetof(DestroyObject_t317818279, ___detachChildren_13)); }
	inline FsmBool_t1075959796 * get_detachChildren_13() const { return ___detachChildren_13; }
	inline FsmBool_t1075959796 ** get_address_of_detachChildren_13() { return &___detachChildren_13; }
	inline void set_detachChildren_13(FsmBool_t1075959796 * value)
	{
		___detachChildren_13 = value;
		Il2CppCodeGenWriteBarrier(&___detachChildren_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
