﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t2281118987;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0__ctor_m708049109 (U3CU3Ec__DisplayClass9_0_t2281118987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::<CreateCastConverter>b__0(System.Object)
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass9_0_U3CCreateCastConverterU3Eb__0_m102408905 (U3CU3Ec__DisplayClass9_0_t2281118987 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
