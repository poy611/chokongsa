﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties
struct SetHingeJoint2dProperties_t3339300148;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::.ctor()
extern "C"  void SetHingeJoint2dProperties__ctor_m602284674 (SetHingeJoint2dProperties_t3339300148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::Reset()
extern "C"  void SetHingeJoint2dProperties_Reset_m2543684911 (SetHingeJoint2dProperties_t3339300148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::OnEnter()
extern "C"  void SetHingeJoint2dProperties_OnEnter_m2661682649 (SetHingeJoint2dProperties_t3339300148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::OnUpdate()
extern "C"  void SetHingeJoint2dProperties_OnUpdate_m41342858 (SetHingeJoint2dProperties_t3339300148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::SetProperties()
extern "C"  void SetHingeJoint2dProperties_SetProperties_m1190335125 (SetHingeJoint2dProperties_t3339300148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
