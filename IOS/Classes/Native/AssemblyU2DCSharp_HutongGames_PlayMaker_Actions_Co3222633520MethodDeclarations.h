﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertIntToString
struct ConvertIntToString_t3222633520;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::.ctor()
extern "C"  void ConvertIntToString__ctor_m2657842742 (ConvertIntToString_t3222633520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::Reset()
extern "C"  void ConvertIntToString_Reset_m304275683 (ConvertIntToString_t3222633520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::OnEnter()
extern "C"  void ConvertIntToString_OnEnter_m2368029837 (ConvertIntToString_t3222633520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::OnUpdate()
extern "C"  void ConvertIntToString_OnUpdate_m3823007574 (ConvertIntToString_t3222633520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::DoConvertIntToString()
extern "C"  void ConvertIntToString_DoConvertIntToString_m1706890561 (ConvertIntToString_t3222633520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
