﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmEventOptions
struct FsmEventOptions_t3746547986;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::.ctor()
extern "C"  void FsmEventOptions__ctor_m3240264292 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::Reset()
extern "C"  void FsmEventOptions_Reset_m886697233 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::OnUpdate()
extern "C"  void FsmEventOptions_OnUpdate_m3075527784 (FsmEventOptions_t3746547986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
