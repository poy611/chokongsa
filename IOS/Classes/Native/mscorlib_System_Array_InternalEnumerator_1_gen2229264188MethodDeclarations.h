﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2229264188.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"

// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4059731926_gshared (InternalEnumerator_1_t2229264188 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4059731926(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2229264188 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4059731926_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306(__this, method) ((  void (*) (InternalEnumerator_1_t2229264188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m181367306_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2229264188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1061782454_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m681956653_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m681956653(__this, method) ((  void (*) (InternalEnumerator_1_t2229264188 *, const MethodInfo*))InternalEnumerator_1_Dispose_m681956653_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m671618294_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m671618294(__this, method) ((  bool (*) (InternalEnumerator_1_t2229264188 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m671618294_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Newtonsoft.Json.ReadType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m316919261_gshared (InternalEnumerator_1_t2229264188 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m316919261(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2229264188 *, const MethodInfo*))InternalEnumerator_1_get_Current_m316919261_gshared)(__this, method)
