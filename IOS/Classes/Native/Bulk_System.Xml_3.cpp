﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct U3CEnumerateChildrenU3Ec__Iterator0_t1273927208;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct EnumerableIterator_t2530618692;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t1383168931;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1075073278;
// System.Xml.XPath.XPathNavigatorComparer
struct XPathNavigatorComparer_t2221114443;
// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct U3CGetEnumeratorU3Ec__Iterator2_t471241848;
// System.Xml.XPath.XPathNumericFunction
struct XPathNumericFunction_t670389548;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t2391178772;
// System.Xml.XPath.XPathSortElement
struct XPathSortElement_t2953066405;
// System.Xml.XPath.XPathSorter
struct XPathSorter_t3393478238;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t1327316739;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t1807339567;
// System.Collections.ArrayList
struct ArrayList_t3948406897;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3774973253;
// System.Xml.Xsl.XsltContext
struct XsltContext_t894076946;
// System.Xml.Xsl.IXsltContextVariable
struct IXsltContextVariable_t3376676363;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2133315502;
// System.Xml.Xsl.IXsltContextFunction
struct IXsltContextFunction_t1712234343;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t1178308879;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1273927208.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1273927208MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_UInt3224667981.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera2530618692.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera2530618692MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2221114443.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2221114443MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNodeOrder444538963.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe471241848MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe471241848.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction670389548.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction670389548MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2391178772.h"
#include "System_Xml_System_Xml_XPath_XPathFunction3895226859MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathResultType516720010.h"
#include "System_Xml_System_Xml_XPath_Expression2556460284MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_Expression2556460284.h"
#include "mscorlib_System_Double3868226565.h"
#include "System_Xml_System_Xml_XPath_XPathResultType516720010MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement2953066405.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement2953066405MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3393478238.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3393478238MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_BaseIterator1327316739.h"
#include "System_Xml_System_Xml_XPath_XmlDataType545259441.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XPath_XPathSorters1807339567.h"
#include "System_Xml_System_Xml_XPath_XPathSorters1807339567MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "mscorlib_System_Collections_ArrayList3948406897MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Xml_System_Xml_XPath_BaseIterator1327316739MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ListIterator4080389072MethodDeclarations.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_System_Xml_XPath_ListIterator4080389072.h"
#include "System_Xml_System_Xml_Xsl_XsltContext894076946.h"
#include "System_Xml_System_Xml_Xsl_XsltContext894076946MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::.ctor()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m574357945 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2855442253 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m2006013620 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern Il2CppClass* U3CEnumerateChildrenU3Ec__Iterator0_t1273927208_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558_MetadataUsageId;
extern "C"  Il2CppObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1876009558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_2 = (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1273927208_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2265504953(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_3 = V_0;
		XPathNavigator_t1075073278 * L_4 = __this->get_U3CU24U3En_6();
		NullCheck(L_3);
		L_3->set_n_0(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Etype_7();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::MoveNext()
extern "C"  bool U3CEnumerateChildrenU3Ec__Iterator0_MoveNext_m4101567175 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_00eb;
	}

IL_0021:
	{
		XPathNavigator_t1075073278 * L_2 = __this->get_n_0();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_00eb;
	}

IL_0036:
	{
		XPathNavigator_t1075073278 * L_4 = __this->get_n_0();
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_4);
		XPathNavigator_t1075073278 * L_5 = __this->get_n_0();
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		__this->set_U3CnavU3E__0_1(L_6);
		XPathNavigator_t1075073278 * L_7 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_7);
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		__this->set_U3Cnav2U3E__1_2((XPathNavigator_t1075073278 *)NULL);
	}

IL_0066:
	{
		int32_t L_8 = __this->get_type_3();
		if ((((int32_t)L_8) == ((int32_t)((int32_t)9))))
		{
			goto IL_0089;
		}
	}
	{
		XPathNavigator_t1075073278 * L_9 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_9);
		int32_t L_11 = __this->get_type_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00d4;
		}
	}

IL_0089:
	{
		XPathNavigator_t1075073278 * L_12 = __this->get_U3Cnav2U3E__1_2();
		if (L_12)
		{
			goto IL_00aa;
		}
	}
	{
		XPathNavigator_t1075073278 * L_13 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_13);
		XPathNavigator_t1075073278 * L_14 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_13);
		__this->set_U3Cnav2U3E__1_2(L_14);
		goto IL_00bc;
	}

IL_00aa:
	{
		XPathNavigator_t1075073278 * L_15 = __this->get_U3Cnav2U3E__1_2();
		XPathNavigator_t1075073278 * L_16 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
	}

IL_00bc:
	{
		XPathNavigator_t1075073278 * L_17 = __this->get_U3Cnav2U3E__1_2();
		__this->set_U24current_5(L_17);
		__this->set_U24PC_4(1);
		goto IL_00ed;
	}

IL_00d4:
	{
		XPathNavigator_t1075073278 * L_18 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_18);
		if (L_19)
		{
			goto IL_0066;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00eb:
	{
		return (bool)0;
	}

IL_00ed:
	{
		return (bool)1;
	}
	// Dead block : IL_00ef: ldloc.1
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Dispose()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Dispose_m3795091126 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190_MetadataUsageId;
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190 (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_Reset_m4206905190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNavigator/EnumerableIterator::.ctor(System.Collections.IEnumerable,System.Int32)
extern "C"  void EnumerableIterator__ctor_m3124749935 (EnumerableIterator_t2530618692 * __this, Il2CppObject * ___source0, int32_t ___pos1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XPathNodeIterator__ctor_m2186940031(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___source0;
		__this->set_source_1(L_0);
		V_0 = 0;
		goto IL_001f;
	}

IL_0014:
	{
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext() */, __this);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = ___pos1;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator/EnumerableIterator::Clone()
extern Il2CppClass* EnumerableIterator_t2530618692_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_Clone_m1533086993_MetadataUsageId;
extern "C"  XPathNodeIterator_t1383168931 * EnumerableIterator_Clone_m1533086993 (EnumerableIterator_t2530618692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_Clone_m1533086993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_source_1();
		int32_t L_1 = __this->get_pos_3();
		EnumerableIterator_t2530618692 * L_2 = (EnumerableIterator_t2530618692 *)il2cpp_codegen_object_new(EnumerableIterator_t2530618692_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m3124749935(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_MoveNext_m1878288367_MetadataUsageId;
extern "C"  bool EnumerableIterator_MoveNext_m1878288367 (EnumerableIterator_t2530618692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_MoveNext_m1878288367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_e_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_source_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_1);
		__this->set_e_2(L_2);
	}

IL_001c:
	{
		Il2CppObject * L_3 = __this->get_e_2();
		NullCheck(L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_3);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)0;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_pos_3();
		__this->set_pos_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::get_CurrentPosition()
extern "C"  int32_t EnumerableIterator_get_CurrentPosition_m3954342198 (EnumerableIterator_t2530618692 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_pos_3();
		return L_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/EnumerableIterator::get_Current()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_get_Current_m2720214749_MetadataUsageId;
extern "C"  XPathNavigator_t1075073278 * EnumerableIterator_get_Current_m2720214749 (EnumerableIterator_t2530618692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_get_Current_m2720214749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_pos_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t1075073278 *)(NULL));
		goto IL_0021;
	}

IL_0011:
	{
		Il2CppObject * L_1 = __this->get_e_2();
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((XPathNavigator_t1075073278 *)CastclassClass(L_2, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.ctor()
extern "C"  void XPathNavigatorComparer__ctor_m2769507411 (XPathNavigatorComparer_t2221114443 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.cctor()
extern Il2CppClass* XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer__cctor_m3768254906_MetadataUsageId;
extern "C"  void XPathNavigatorComparer__cctor_m3768254906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer__cctor_m3768254906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathNavigatorComparer_t2221114443 * L_0 = (XPathNavigatorComparer_t2221114443 *)il2cpp_codegen_object_new(XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var);
		XPathNavigatorComparer__ctor_m2769507411(L_0, /*hidden argument*/NULL);
		((XPathNavigatorComparer_t2221114443_StaticFields*)XPathNavigatorComparer_t2221114443_il2cpp_TypeInfo_var->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652_MetadataUsageId;
extern "C"  bool XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652 (XPathNavigatorComparer_t2221114443 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m2382237652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	XPathNavigator_t1075073278 * V_1 = NULL;
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t1075073278 *)IsInstClass(L_0, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t1075073278 *)IsInstClass(L_1, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		XPathNavigator_t1075073278 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t1075073278 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t1075073278 * L_4 = V_0;
		XPathNavigator_t1075073278 * L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, XPathNavigator_t1075073278 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		G_B4_0 = ((int32_t)(L_6));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_GetHashCode_m608806058 (XPathNavigatorComparer_t2221114443 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::Compare(System.Object,System.Object)
extern Il2CppClass* XPathNavigator_t1075073278_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer_Compare_m3372576234_MetadataUsageId;
extern "C"  int32_t XPathNavigatorComparer_Compare_m3372576234 (XPathNavigatorComparer_t2221114443 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_Compare_m3372576234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigator_t1075073278 * V_0 = NULL;
	XPathNavigator_t1075073278 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t1075073278 *)IsInstClass(L_0, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t1075073278 *)IsInstClass(L_1, XPathNavigator_t1075073278_il2cpp_TypeInfo_var));
		XPathNavigator_t1075073278 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNavigator_t1075073278 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNavigator_t1075073278 * L_4 = V_0;
		XPathNavigator_t1075073278 * L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_003d;
	}

IL_0039:
	{
		return 0;
	}

IL_003b:
	{
		return 1;
	}

IL_003d:
	{
		return (-1);
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator::.ctor()
extern "C"  void XPathNodeIterator__ctor_m2186940031 (XPathNodeIterator_t1383168931 * __this, const MethodInfo* method)
{
	{
		__this->set__count_0((-1));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator::System.ICloneable.Clone()
extern "C"  Il2CppObject * XPathNodeIterator_System_ICloneable_Clone_m216028580 (XPathNodeIterator_t1383168931 * __this, const MethodInfo* method)
{
	{
		XPathNodeIterator_t1383168931 * L_0 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		return L_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count()
extern "C"  int32_t XPathNodeIterator_get_Count_m995835617 (XPathNodeIterator_t1383168931 * __this, const MethodInfo* method)
{
	XPathNodeIterator_t1383168931 * V_0 = NULL;
	{
		int32_t L_0 = __this->get__count_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_1 = VirtFuncInvoker0< XPathNodeIterator_t1383168931 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		XPathNodeIterator_t1383168931 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		XPathNodeIterator_t1383168931 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_CurrentPosition() */, L_4);
		__this->set__count_0(L_5);
	}

IL_002f:
	{
		int32_t L_6 = __this->get__count_0();
		return L_6;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNodeIterator::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator2_t471241848_il2cpp_TypeInfo_var;
extern const uint32_t XPathNodeIterator_GetEnumerator_m3547213243_MetadataUsageId;
extern "C"  Il2CppObject * XPathNodeIterator_GetEnumerator_m3547213243 (XPathNodeIterator_t1383168931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNodeIterator_GetEnumerator_m3547213243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator2_t471241848 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator2_t471241848 * L_0 = (U3CGetEnumeratorU3Ec__Iterator2_t471241848 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator2_t471241848_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator2__ctor_m3892894189(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator2_t471241848 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CGetEnumeratorU3Ec__Iterator2_t471241848 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2__ctor_m3892894189 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3164273103 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3326114659 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator2_MoveNext_m1382971427 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005a;
	}

IL_0021:
	{
		goto IL_0043;
	}

IL_0026:
	{
		XPathNodeIterator_t1383168931 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		XPathNavigator_t1075073278 * L_3 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_2);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_005c;
	}

IL_0043:
	{
		XPathNodeIterator_t1383168931 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		return (bool)1;
	}
	// Dead block : IL_005e: ldloc.1
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Dispose_m53083882 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130 (U3CGetEnumeratorU3Ec__Iterator2_t471241848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator2_Reset_m1539327130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNumericFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathNumericFunction__ctor_m3502463899 (XPathNumericFunction_t670389548 * __this, FunctionArguments_t2391178772 * ___args0, const MethodInfo* method)
{
	{
		FunctionArguments_t2391178772 * L_0 = ___args0;
		XPathFunction__ctor_m2315013248(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathNumericFunction::get_ReturnType()
extern "C"  int32_t XPathNumericFunction_get_ReturnType_m4098330081 (XPathNumericFunction_t670389548 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Object System.Xml.XPath.XPathNumericFunction::get_StaticValue()
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathNumericFunction_get_StaticValue_m649259103_MetadataUsageId;
extern "C"  Il2CppObject * XPathNumericFunction_get_StaticValue_m649259103 (XPathNumericFunction_t670389548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathNumericFunction_get_StaticValue_m649259103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m2356815033 (XPathSortElement_t2953066405 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathSorter::Evaluate(System.Xml.XPath.BaseIterator)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Evaluate_m2674889470_MetadataUsageId;
extern "C"  Il2CppObject * XPathSorter_Evaluate_m2674889470 (XPathSorter_t3393478238 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Evaluate_m2674889470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get__type_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		Expression_t2556460284 * L_1 = __this->get__expr_0();
		BaseIterator_t1327316739 * L_2 = ___iter0;
		NullCheck(L_1);
		double L_3 = VirtFuncInvoker1< double, BaseIterator_t1327316739 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		double L_4 = L_3;
		Il2CppObject * L_5 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_001e:
	{
		Expression_t2556460284 * L_6 = __this->get__expr_0();
		BaseIterator_t1327316739 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t1327316739 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		return L_8;
	}
}
// System.Int32 System.Xml.XPath.XPathSorter::Compare(System.Object,System.Object)
extern Il2CppClass* IComparer_t4034294160_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Compare_m78114877_MetadataUsageId;
extern "C"  int32_t XPathSorter_Compare_m78114877 (XPathSorter_t3393478238 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Compare_m78114877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__cmp_1();
		Il2CppObject * L_1 = ___o10;
		Il2CppObject * L_2 = ___o21;
		NullCheck(L_0);
		int32_t L_3 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t4034294160_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Int32 System.Xml.XPath.XPathSorters::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* XPathSortElement_t2953066405_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3393478238_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_System_Collections_IComparer_Compare_m247046298_MetadataUsageId;
extern "C"  int32_t XPathSorters_System_Collections_IComparer_Compare_m247046298 (XPathSorters_t1807339567 * __this, Il2CppObject * ___o10, Il2CppObject * ___o21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_System_Collections_IComparer_Compare_m247046298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathSortElement_t2953066405 * V_0 = NULL;
	XPathSortElement_t2953066405 * V_1 = NULL;
	int32_t V_2 = 0;
	XPathSorter_t3393478238 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		Il2CppObject * L_0 = ___o10;
		V_0 = ((XPathSortElement_t2953066405 *)CastclassClass(L_0, XPathSortElement_t2953066405_il2cpp_TypeInfo_var));
		Il2CppObject * L_1 = ___o21;
		V_1 = ((XPathSortElement_t2953066405 *)CastclassClass(L_1, XPathSortElement_t2953066405_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_004d;
	}

IL_0015:
	{
		ArrayList_t3948406897 * L_2 = __this->get__rgSorters_0();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_3 = ((XPathSorter_t3393478238 *)CastclassClass(L_4, XPathSorter_t3393478238_il2cpp_TypeInfo_var));
		XPathSorter_t3393478238 * L_5 = V_3;
		XPathSortElement_t2953066405 * L_6 = V_0;
		NullCheck(L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_6->get_Values_1();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		XPathSortElement_t2953066405 * L_11 = V_1;
		NullCheck(L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_11->get_Values_1();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_5);
		int32_t L_16 = XPathSorter_Compare_m78114877(L_5, L_10, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		int32_t L_17 = V_4;
		if (!L_17)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_18 = V_4;
		return L_18;
	}

IL_0049:
	{
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_20 = V_2;
		ArrayList_t3948406897 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0015;
		}
	}
	{
		XPathSortElement_t2953066405 * L_23 = V_0;
		NullCheck(L_23);
		XPathNavigator_t1075073278 * L_24 = L_23->get_Navigator_0();
		XPathSortElement_t2953066405 * L_25 = V_1;
		NullCheck(L_25);
		XPathNavigator_t1075073278 * L_26 = L_25->get_Navigator_0();
		NullCheck(L_24);
		int32_t L_27 = VirtFuncInvoker1< int32_t, XPathNavigator_t1075073278 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_24, L_26);
		V_5 = L_27;
		int32_t L_28 = V_5;
		if ((((int32_t)L_28) == ((int32_t)1)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_29 = V_5;
		if ((((int32_t)L_29) == ((int32_t)2)))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_008a;
	}

IL_0086:
	{
		return 0;
	}

IL_0088:
	{
		return 1;
	}

IL_008a:
	{
		return (-1);
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Xml.XPath.BaseIterator)
extern "C"  BaseIterator_t1327316739 * XPathSorters_Sort_m2612507910 (XPathSorters_t1807339567 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	ArrayList_t3948406897 * V_0 = NULL;
	{
		BaseIterator_t1327316739 * L_0 = ___iter0;
		ArrayList_t3948406897 * L_1 = XPathSorters_ToSortElementList_m2931616076(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ArrayList_t3948406897 * L_2 = V_0;
		BaseIterator_t1327316739 * L_3 = ___iter0;
		NullCheck(L_3);
		Il2CppObject * L_4 = BaseIterator_get_NamespaceManager_m3653702256(L_3, /*hidden argument*/NULL);
		BaseIterator_t1327316739 * L_5 = XPathSorters_Sort_m386310882(__this, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.ArrayList System.Xml.XPath.XPathSorters::ToSortElementList(System.Xml.XPath.BaseIterator)
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t2953066405_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSorter_t3393478238_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_ToSortElementList_m2931616076_MetadataUsageId;
extern "C"  ArrayList_t3948406897 * XPathSorters_ToSortElementList_m2931616076 (XPathSorters_t1807339567 * __this, BaseIterator_t1327316739 * ___iter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_ToSortElementList_m2931616076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t3948406897 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t2953066405 * V_2 = NULL;
	int32_t V_3 = 0;
	XPathSorter_t3393478238 * V_4 = NULL;
	{
		ArrayList_t3948406897 * L_0 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t3948406897 * L_1 = __this->get__rgSorters_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0081;
	}

IL_0017:
	{
		XPathSortElement_t2953066405 * L_3 = (XPathSortElement_t2953066405 *)il2cpp_codegen_object_new(XPathSortElement_t2953066405_il2cpp_TypeInfo_var);
		XPathSortElement__ctor_m2356815033(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		XPathSortElement_t2953066405 * L_4 = V_2;
		BaseIterator_t1327316739 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t1075073278 * L_6 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t1075073278 * L_7 = VirtFuncInvoker0< XPathNavigator_t1075073278 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		NullCheck(L_4);
		L_4->set_Navigator_0(L_7);
		XPathSortElement_t2953066405 * L_8 = V_2;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		L_8->set_Values_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)L_9)));
		V_3 = 0;
		goto IL_0068;
	}

IL_0041:
	{
		ArrayList_t3948406897 * L_10 = __this->get__rgSorters_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		V_4 = ((XPathSorter_t3393478238 *)CastclassClass(L_12, XPathSorter_t3393478238_il2cpp_TypeInfo_var));
		XPathSortElement_t2953066405 * L_13 = V_2;
		NullCheck(L_13);
		ObjectU5BU5D_t1108656482* L_14 = L_13->get_Values_1();
		int32_t L_15 = V_3;
		XPathSorter_t3393478238 * L_16 = V_4;
		BaseIterator_t1327316739 * L_17 = ___iter0;
		NullCheck(L_16);
		Il2CppObject * L_18 = XPathSorter_Evaluate_m2674889470(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_18);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_20 = V_3;
		ArrayList_t3948406897 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0041;
		}
	}
	{
		ArrayList_t3948406897 * L_23 = V_0;
		XPathSortElement_t2953066405 * L_24 = V_2;
		NullCheck(L_23);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_23, L_24);
	}

IL_0081:
	{
		BaseIterator_t1327316739 * L_25 = ___iter0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (L_26)
		{
			goto IL_0017;
		}
	}
	{
		ArrayList_t3948406897 * L_27 = V_0;
		return L_27;
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Collections.ArrayList,System.Xml.IXmlNamespaceResolver)
extern Il2CppClass* XPathNavigatorU5BU5D_t2360214859_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathSortElement_t2953066405_il2cpp_TypeInfo_var;
extern Il2CppClass* ListIterator_t4080389072_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_Sort_m386310882_MetadataUsageId;
extern "C"  BaseIterator_t1327316739 * XPathSorters_Sort_m386310882 (XPathSorters_t1807339567 * __this, ArrayList_t3948406897 * ___rgElts0, Il2CppObject * ___nsm1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_Sort_m386310882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XPathNavigatorU5BU5D_t2360214859* V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t2953066405 * V_2 = NULL;
	{
		ArrayList_t3948406897 * L_0 = ___rgElts0;
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_0, __this);
		ArrayList_t3948406897 * L_1 = ___rgElts0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_0 = ((XPathNavigatorU5BU5D_t2360214859*)SZArrayNew(XPathNavigatorU5BU5D_t2360214859_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = 0;
		goto IL_0034;
	}

IL_001a:
	{
		ArrayList_t3948406897 * L_3 = ___rgElts0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		V_2 = ((XPathSortElement_t2953066405 *)CastclassClass(L_5, XPathSortElement_t2953066405_il2cpp_TypeInfo_var));
		XPathNavigatorU5BU5D_t2360214859* L_6 = V_0;
		int32_t L_7 = V_1;
		XPathSortElement_t2953066405 * L_8 = V_2;
		NullCheck(L_8);
		XPathNavigator_t1075073278 * L_9 = L_8->get_Navigator_0();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (XPathNavigator_t1075073278 *)L_9);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_11 = V_1;
		ArrayList_t3948406897 * L_12 = ___rgElts0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigatorU5BU5D_t2360214859* L_14 = V_0;
		Il2CppObject * L_15 = ___nsm1;
		ListIterator_t4080389072 * L_16 = (ListIterator_t4080389072 *)il2cpp_codegen_object_new(ListIterator_t4080389072_il2cpp_TypeInfo_var);
		ListIterator__ctor_m320855722(L_16, (Il2CppObject *)(Il2CppObject *)L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName)
extern "C"  Il2CppObject * XsltContext_ResolveVariable_m3172955259 (XsltContext_t894076946 * __this, XmlQualifiedName_t2133315502 * ___name0, const MethodInfo* method)
{
	{
		XmlQualifiedName_t2133315502 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2987642414(L_0, /*hidden argument*/NULL);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(11 /* System.String System.Xml.XmlNamespaceManager::LookupPrefix(System.String) */, __this, L_1);
		XmlQualifiedName_t2133315502 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m607016698(L_3, /*hidden argument*/NULL);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, String_t*, String_t* >::Invoke(16 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.String,System.String) */, __this, L_2, L_4);
		return L_5;
	}
}
// System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[])
extern "C"  Il2CppObject * XsltContext_ResolveFunction_m96561364 (XsltContext_t894076946 * __this, XmlQualifiedName_t2133315502 * ___name0, XPathResultTypeU5BU5D_t1178308879* ___argTypes1, const MethodInfo* method)
{
	{
		XmlQualifiedName_t2133315502 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Name_m607016698(L_0, /*hidden argument*/NULL);
		XmlQualifiedName_t2133315502 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2987642414(L_2, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t1178308879* L_4 = ___argTypes1;
		Il2CppObject * L_5 = VirtFuncInvoker3< Il2CppObject *, String_t*, String_t*, XPathResultTypeU5BU5D_t1178308879* >::Invoke(15 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, __this, L_1, L_3, L_4);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
