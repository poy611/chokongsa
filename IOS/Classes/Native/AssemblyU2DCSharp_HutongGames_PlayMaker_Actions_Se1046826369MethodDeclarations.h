﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMouseCursor
struct SetMouseCursor_t1046826369;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::.ctor()
extern "C"  void SetMouseCursor__ctor_m1255562245 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::Reset()
extern "C"  void SetMouseCursor_Reset_m3196962482 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::OnEnter()
extern "C"  void SetMouseCursor_OnEnter_m3396203164 (SetMouseCursor_t1046826369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
