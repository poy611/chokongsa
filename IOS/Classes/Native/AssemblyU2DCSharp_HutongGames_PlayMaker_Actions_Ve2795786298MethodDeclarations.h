﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Add
struct Vector2Add_t2795786298;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Add::.ctor()
extern "C"  void Vector2Add__ctor_m1609901420 (Vector2Add_t2795786298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Add::Reset()
extern "C"  void Vector2Add_Reset_m3551301657 (Vector2Add_t2795786298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Add::OnEnter()
extern "C"  void Vector2Add_OnEnter_m318766659 (Vector2Add_t2795786298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Add::OnUpdate()
extern "C"  void Vector2Add_OnUpdate_m425391200 (Vector2Add_t2795786298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Add::DoVector2Add()
extern "C"  void Vector2Add_DoVector2Add_m4085948885 (Vector2Add_t2795786298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
