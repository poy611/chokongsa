﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35
struct U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576;
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3251685236;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35::.ctor()
extern "C"  void U3CLoadAchievementDescriptionsU3Ec__AnonStorey35__ctor_m210144355 (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform/<LoadAchievementDescriptions>c__AnonStorey35::<>m__4(GooglePlayGames.BasicApi.Achievement[])
extern "C"  void U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_U3CU3Em__4_m2468199617 (U3CLoadAchievementDescriptionsU3Ec__AnonStorey35_t1592139576 * __this, AchievementU5BU5D_t3251685236* ___ach0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
