﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateTest
struct FsmStateTest_t3341384075;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::.ctor()
extern "C"  void FsmStateTest__ctor_m3033383227 (FsmStateTest_t3341384075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::Reset()
extern "C"  void FsmStateTest_Reset_m679816168 (FsmStateTest_t3341384075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnEnter()
extern "C"  void FsmStateTest_OnEnter_m2485183058 (FsmStateTest_t3341384075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnUpdate()
extern "C"  void FsmStateTest_OnUpdate_m3159790129 (FsmStateTest_t3341384075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::DoFsmStateTest()
extern "C"  void FsmStateTest_DoFsmStateTest_m3949619895 (FsmStateTest_t3341384075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
