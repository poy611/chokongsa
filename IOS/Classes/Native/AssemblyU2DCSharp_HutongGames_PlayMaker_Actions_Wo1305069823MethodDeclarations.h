﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WorldToScreenPoint
struct WorldToScreenPoint_t1305069823;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::.ctor()
extern "C"  void WorldToScreenPoint__ctor_m2419008071 (WorldToScreenPoint_t1305069823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::Reset()
extern "C"  void WorldToScreenPoint_Reset_m65441012 (WorldToScreenPoint_t1305069823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::OnEnter()
extern "C"  void WorldToScreenPoint_OnEnter_m481177694 (WorldToScreenPoint_t1305069823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::OnUpdate()
extern "C"  void WorldToScreenPoint_OnUpdate_m1165165989 (WorldToScreenPoint_t1305069823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::DoWorldToScreenPoint()
extern "C"  void WorldToScreenPoint_DoWorldToScreenPoint_m1127217695 (WorldToScreenPoint_t1305069823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
