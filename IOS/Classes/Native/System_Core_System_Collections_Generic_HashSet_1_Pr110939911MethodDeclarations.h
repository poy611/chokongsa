﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.cctor()
extern "C"  void PrimeHelper__cctor_m1646749866_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PrimeHelper__cctor_m1646749866(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PrimeHelper__cctor_m1646749866_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::TestPrime(System.Int32)
extern "C"  bool PrimeHelper_TestPrime_m350716979_gshared (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method);
#define PrimeHelper_TestPrime_m350716979(__this /* static, unused */, ___x0, method) ((  bool (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_TestPrime_m350716979_gshared)(__this /* static, unused */, ___x0, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CalcPrime(System.Int32)
extern "C"  int32_t PrimeHelper_CalcPrime_m2607028842_gshared (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method);
#define PrimeHelper_CalcPrime_m2607028842(__this /* static, unused */, ___x0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_CalcPrime_m2607028842_gshared)(__this /* static, unused */, ___x0, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::ToPrime(System.Int32)
extern "C"  int32_t PrimeHelper_ToPrime_m4028136452_gshared (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method);
#define PrimeHelper_ToPrime_m4028136452(__this /* static, unused */, ___x0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_ToPrime_m4028136452_gshared)(__this /* static, unused */, ___x0, method)
