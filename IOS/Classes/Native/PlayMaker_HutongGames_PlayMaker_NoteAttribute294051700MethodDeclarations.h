﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.NoteAttribute
struct NoteAttribute_t294051700;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.NoteAttribute::get_Text()
extern "C"  String_t* NoteAttribute_get_Text_m3842120044 (NoteAttribute_t294051700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NoteAttribute::.ctor(System.String)
extern "C"  void NoteAttribute__ctor_m3674703991 (NoteAttribute_t294051700 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
