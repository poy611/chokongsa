﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetEventInfo
struct GetEventInfo_t3599372762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::.ctor()
extern "C"  void GetEventInfo__ctor_m4014084556 (GetEventInfo_t3599372762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::Reset()
extern "C"  void GetEventInfo_Reset_m1660517497 (GetEventInfo_t3599372762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetEventInfo::OnEnter()
extern "C"  void GetEventInfo_OnEnter_m46355107 (GetEventInfo_t3599372762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
