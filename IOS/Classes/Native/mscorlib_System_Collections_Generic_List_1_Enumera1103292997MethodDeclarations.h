﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t1083620227;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1103292997.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3131791237_gshared (Enumerator_t1103292997 * __this, List_1_t1083620227 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3131791237(__this, ___l0, method) ((  void (*) (Enumerator_t1103292997 *, List_1_t1083620227 *, const MethodInfo*))Enumerator__ctor_m3131791237_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1446141357_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1446141357(__this, method) ((  void (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1446141357_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2615577_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2615577(__this, method) ((  Il2CppObject * (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2615577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::Dispose()
extern "C"  void Enumerator_Dispose_m1829656234_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1829656234(__this, method) ((  void (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_Dispose_m1829656234_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1686286563_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1686286563(__this, method) ((  void (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_VerifyState_m1686286563_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.IntPtr>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1890566937_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1890566937(__this, method) ((  bool (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_MoveNext_m1890566937_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.IntPtr>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m67455194_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m67455194(__this, method) ((  IntPtr_t (*) (Enumerator_t1103292997 *, const MethodInfo*))Enumerator_get_Current_m67455194_gshared)(__this, method)
