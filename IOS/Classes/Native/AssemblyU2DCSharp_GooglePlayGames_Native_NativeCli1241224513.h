﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A
struct U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528;
// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B
struct U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C
struct  U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C::authCode
	String_t* ___authCode_0;
	// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C::<>f__ref$74
	U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * ___U3CU3Ef__refU2474_1;
	// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4B GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C::<>f__ref$75
	U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512 * ___U3CU3Ef__refU2475_2;

public:
	inline static int32_t get_offset_of_authCode_0() { return static_cast<int32_t>(offsetof(U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513, ___authCode_0)); }
	inline String_t* get_authCode_0() const { return ___authCode_0; }
	inline String_t** get_address_of_authCode_0() { return &___authCode_0; }
	inline void set_authCode_0(String_t* value)
	{
		___authCode_0 = value;
		Il2CppCodeGenWriteBarrier(&___authCode_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2474_1() { return static_cast<int32_t>(offsetof(U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513, ___U3CU3Ef__refU2474_1)); }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * get_U3CU3Ef__refU2474_1() const { return ___U3CU3Ef__refU2474_1; }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 ** get_address_of_U3CU3Ef__refU2474_1() { return &___U3CU3Ef__refU2474_1; }
	inline void set_U3CU3Ef__refU2474_1(U3CGetServerAuthCodeU3Ec__AnonStorey4A_t1474425528 * value)
	{
		___U3CU3Ef__refU2474_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2474_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2475_2() { return static_cast<int32_t>(offsetof(U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513, ___U3CU3Ef__refU2475_2)); }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512 * get_U3CU3Ef__refU2475_2() const { return ___U3CU3Ef__refU2475_2; }
	inline U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512 ** get_address_of_U3CU3Ef__refU2475_2() { return &___U3CU3Ef__refU2475_2; }
	inline void set_U3CU3Ef__refU2475_2(U3CGetServerAuthCodeU3Ec__AnonStorey4B_t1241224512 * value)
	{
		___U3CU3Ef__refU2475_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2475_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
