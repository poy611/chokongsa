﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t2513188365;
// GooglePlayGames.Native.PInvoke.NativeAchievement
struct NativeAchievement_t2621183934;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::.ctor(System.IntPtr)
extern "C"  void FetchResponse__ctor_m2102194454 (FetchResponse_t2513188365 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Status()
extern "C"  int32_t FetchResponse_Status_m2148756911 (FetchResponse_t2513188365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::Achievement()
extern "C"  NativeAchievement_t2621183934 * FetchResponse_Achievement_m393760659 (FetchResponse_t2513188365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchResponse_CallDispose_m3379418970 (FetchResponse_t2513188365 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse::FromPointer(System.IntPtr)
extern "C"  FetchResponse_t2513188365 * FetchResponse_FromPointer_m3584411135 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
