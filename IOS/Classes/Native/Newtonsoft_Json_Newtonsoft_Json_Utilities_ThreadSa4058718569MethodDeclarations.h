﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m2182106314(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t4058718569 *, Func_2_t1368711897 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::Get(TKey)
#define ThreadSafeStore_2_Get_m209425136(__this, ___key0, method) ((  ReflectionObject_t3124613658 * (*) (ThreadSafeStore_2_t4058718569 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m896828834(__this, ___key0, method) ((  ReflectionObject_t3124613658 * (*) (ThreadSafeStore_2_t4058718569 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
