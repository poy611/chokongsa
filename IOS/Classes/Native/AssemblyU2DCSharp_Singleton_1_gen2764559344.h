﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameStepManager
struct GameStepManager_t2511743951;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GameStepManager>
struct  Singleton_1_t2764559344  : public MonoBehaviour_t667441552
{
public:

public:
};

struct Singleton_1_t2764559344_StaticFields
{
public:
	// T Singleton`1::instance
	GameStepManager_t2511743951 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Singleton_1_t2764559344_StaticFields, ___instance_2)); }
	inline GameStepManager_t2511743951 * get_instance_2() const { return ___instance_2; }
	inline GameStepManager_t2511743951 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GameStepManager_t2511743951 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
