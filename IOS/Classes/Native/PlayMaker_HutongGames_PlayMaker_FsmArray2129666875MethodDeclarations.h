﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Array
struct Il2CppArray;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "mscorlib_System_String7231557.h"

// System.Object HutongGames.PlayMaker.FsmArray::get_RawValue()
extern "C"  Il2CppObject * FsmArray_get_RawValue_m440556339 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_RawValue(System.Object)
extern "C"  void FsmArray_set_RawValue_m455185570 (FsmArray_t2129666875 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmArray::get_ObjectType()
extern "C"  Type_t * FsmArray_get_ObjectType_m64717256 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_ObjectType(System.Type)
extern "C"  void FsmArray_set_ObjectType_m3785296151 (FsmArray_t2129666875 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmArray::get_ObjectTypeName()
extern "C"  String_t* FsmArray_get_ObjectTypeName_m1053749052 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] HutongGames.PlayMaker.FsmArray::get_Values()
extern "C"  ObjectU5BU5D_t1108656482* FsmArray_get_Values_m1362031434 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_Values(System.Object[])
extern "C"  void FsmArray_set_Values_m2547392359 (FsmArray_t2129666875 * __this, ObjectU5BU5D_t1108656482* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmArray::get_Length()
extern "C"  int32_t FsmArray_get_Length_m3299442701 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_TypeConstraint()
extern "C"  int32_t FsmArray_get_TypeConstraint_m1781471723 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_ElementType()
extern "C"  int32_t FsmArray_get_ElementType_m310141636 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_ElementType(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmArray_set_ElementType_m3640436205 (FsmArray_t2129666875 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::InitArray()
extern "C"  void FsmArray_InitArray_m1232645247 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmArray::Get(System.Int32)
extern "C"  Il2CppObject * FsmArray_Get_m160560104 (FsmArray_t2129666875 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Set(System.Int32,System.Object)
extern "C"  void FsmArray_Set_m410178455 (FsmArray_t2129666875 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmArray::Load(System.Int32)
extern "C"  Il2CppObject * FsmArray_Load_m608506104 (FsmArray_t2129666875 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Save(System.Int32,System.Object)
extern "C"  void FsmArray_Save_m183885992 (FsmArray_t2129666875 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::SetType(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmArray_SetType_m2918258736 (FsmArray_t2129666875 * __this, int32_t ___newType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::SaveChanges()
extern "C"  void FsmArray_SaveChanges_m2787717980 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::CopyValues(HutongGames.PlayMaker.FsmArray)
extern "C"  void FsmArray_CopyValues_m2727203294 (FsmArray_t2129666875 * __this, FsmArray_t2129666875 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::ConformSourceArraySize()
extern "C"  void FsmArray_ConformSourceArraySize_m246471749 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array HutongGames.PlayMaker.FsmArray::GetSourceArray()
extern "C"  Il2CppArray * FsmArray_GetSourceArray_m1585974459 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Resize(System.Int32)
extern "C"  void FsmArray_Resize_m3367576465 (FsmArray_t2129666875 * __this, int32_t ___newLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Reset()
extern "C"  void FsmArray_Reset_m826385317 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor()
extern "C"  void FsmArray__ctor_m3179952376 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor(System.String)
extern "C"  void FsmArray__ctor_m1007532298 (FsmArray_t2129666875 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor(HutongGames.PlayMaker.FsmArray)
extern "C"  void FsmArray__ctor_m2827559891 (FsmArray_t2129666875 * __this, FsmArray_t2129666875 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmArray::Clone()
extern "C"  NamedVariable_t3211770239 * FsmArray_Clone_m375632649 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_VariableType()
extern "C"  int32_t FsmArray_get_VariableType_m1323476490 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmArray::ToString()
extern "C"  String_t* FsmArray_ToString_m729806363 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmArray::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmArray_TestTypeConstraint_m2804970302 (FsmArray_t2129666875 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmArray::RealType()
extern "C"  Type_t * FsmArray_RealType_m1224609374 (FsmArray_t2129666875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
