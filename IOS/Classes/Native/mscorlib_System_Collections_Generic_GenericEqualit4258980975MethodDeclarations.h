﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>
struct GenericEqualityComparer_1_t4258980975;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m1900401490_gshared (GenericEqualityComparer_1_t4258980975 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m1900401490(__this, method) ((  void (*) (GenericEqualityComparer_1_t4258980975 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m1900401490_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m288627041_gshared (GenericEqualityComparer_1_t4258980975 * __this, uint32_t ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m288627041(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t4258980975 *, uint32_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m288627041_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m668727655_gshared (GenericEqualityComparer_1_t4258980975 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m668727655(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t4258980975 *, uint32_t, uint32_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m668727655_gshared)(__this, ___x0, ___y1, method)
