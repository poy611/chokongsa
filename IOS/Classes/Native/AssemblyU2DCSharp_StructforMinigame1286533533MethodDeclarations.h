﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StructforMinigame
struct StructforMinigame_t1286533533;
// System.String
struct String_t;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void StructforMinigame::.ctor()
extern "C"  void StructforMinigame__ctor_m2741910638 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::.ctor(System.Int32)
extern "C"  void StructforMinigame__ctor_m919518399 (StructforMinigame_t1286533533 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::.cctor()
extern "C"  void StructforMinigame__cctor_m2912754943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String StructforMinigame::get_Userid()
extern "C"  String_t* StructforMinigame_get_Userid_m3023313986 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Userid(System.String)
extern "C"  void StructforMinigame_set_Userid_m3465234537 (StructforMinigame_t1286533533 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 StructforMinigame::get_Gameid()
extern "C"  int32_t StructforMinigame_get_Gameid_m349399966 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Gameid(System.Int32)
extern "C"  void StructforMinigame_set_Gameid_m1686107441 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 StructforMinigame::get_Level()
extern "C"  int32_t StructforMinigame_get_Level_m2077766709 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Level(System.Int32)
extern "C"  void StructforMinigame_set_Level_m547274884 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String StructforMinigame::get_RecordDate()
extern "C"  String_t* StructforMinigame_get_RecordDate_m24046075 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_RecordDate(System.String)
extern "C"  void StructforMinigame_set_RecordDate_m450818768 (StructforMinigame_t1286533533 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single StructforMinigame::get_Score()
extern "C"  float StructforMinigame_get_Score_m239363585 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Score(System.Single)
extern "C"  void StructforMinigame_set_Score_m1127316490 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single StructforMinigame::get_Playtime()
extern "C"  float StructforMinigame_get_Playtime_m2221809428 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Playtime(System.Single)
extern "C"  void StructforMinigame_set_Playtime_m3659740247 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 StructforMinigame::get_Misscount()
extern "C"  int32_t StructforMinigame_get_Misscount_m2827575940 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_Misscount(System.Int32)
extern "C"  void StructforMinigame_set_Misscount_m2498396499 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single StructforMinigame::get_ErrorRate()
extern "C"  float StructforMinigame_get_ErrorRate_m2504854007 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StructforMinigame::set_ErrorRate(System.Single)
extern "C"  void StructforMinigame_set_ErrorRate_m2862618580 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.Stopwatch StructforMinigame::get_Sw()
extern "C"  Stopwatch_t3420517611 * StructforMinigame_get_Sw_m1790624722 (StructforMinigame_t1286533533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StructforMinigame StructforMinigame::getSingleton(System.Int32)
extern "C"  StructforMinigame_t1286533533 * StructforMinigame_getSingleton_m51070278 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StructforMinigame[] StructforMinigame::getSingleton()
extern "C"  StructforMinigameU5BU5D_t1843399504* StructforMinigame_getSingleton_m919431639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
