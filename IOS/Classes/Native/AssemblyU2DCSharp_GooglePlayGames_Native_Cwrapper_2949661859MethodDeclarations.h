﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadataChange_SnapshotMetadataChange_Description_m3578059163 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Image(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotMetadataChange_SnapshotMetadataChange_Image_m1929964574 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTimeIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_PlayedTimeIsChanged_m1083010508 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_Valid_m3444210814 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t SnapshotMetadataChange_SnapshotMetadataChange_PlayedTime_m2411950354 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadataChange_SnapshotMetadataChange_Dispose_m2040672045 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_ImageIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_ImageIsChanged_m2404129033 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_DescriptionIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_DescriptionIsChanged_m2503180232 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
