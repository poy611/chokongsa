﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct Action_1_t3953224079;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.Callbacks
struct  Callbacks_t3420130036  : public Il2CppObject
{
public:

public:
};

struct Callbacks_t3420130036_StaticFields
{
public:
	// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus> GooglePlayGames.Native.PInvoke.Callbacks::NoopUICallback
	Action_1_t3953224079 * ___NoopUICallback_0;
	// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus> GooglePlayGames.Native.PInvoke.Callbacks::<>f__am$cache1
	Action_1_t3953224079 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_NoopUICallback_0() { return static_cast<int32_t>(offsetof(Callbacks_t3420130036_StaticFields, ___NoopUICallback_0)); }
	inline Action_1_t3953224079 * get_NoopUICallback_0() const { return ___NoopUICallback_0; }
	inline Action_1_t3953224079 ** get_address_of_NoopUICallback_0() { return &___NoopUICallback_0; }
	inline void set_NoopUICallback_0(Action_1_t3953224079 * value)
	{
		___NoopUICallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___NoopUICallback_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Callbacks_t3420130036_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t3953224079 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t3953224079 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t3953224079 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
