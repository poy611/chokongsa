﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey69
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey69::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey69__ctor_m2812469653 (U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
