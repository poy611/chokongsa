﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t554063095;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Type2863145774.h"

// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::get_TypeCode()
extern "C"  int32_t JsonPrimitiveContract_get_TypeCode_m628476724 (JsonPrimitiveContract_t554063095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern "C"  void JsonPrimitiveContract_set_TypeCode_m2402902327 (JsonPrimitiveContract_t554063095 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.ctor(System.Type)
extern "C"  void JsonPrimitiveContract__ctor_m4258133943 (JsonPrimitiveContract_t554063095 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.cctor()
extern "C"  void JsonPrimitiveContract__cctor_m2431539563 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
