﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An4201352541.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFloatV2
struct  AnimateFloatV2_t1182581439  : public AnimateFsmAction_t4201352541
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFloatV2::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_34;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateFloatV2::animCurve
	FsmAnimationCurve_t2685995989 * ___animCurve_35;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateFloatV2::calculation
	int32_t ___calculation_36;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloatV2::finishInNextStep
	bool ___finishInNextStep_37;

public:
	inline static int32_t get_offset_of_floatVariable_34() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t1182581439, ___floatVariable_34)); }
	inline FsmFloat_t2134102846 * get_floatVariable_34() const { return ___floatVariable_34; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_34() { return &___floatVariable_34; }
	inline void set_floatVariable_34(FsmFloat_t2134102846 * value)
	{
		___floatVariable_34 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_34, value);
	}

	inline static int32_t get_offset_of_animCurve_35() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t1182581439, ___animCurve_35)); }
	inline FsmAnimationCurve_t2685995989 * get_animCurve_35() const { return ___animCurve_35; }
	inline FsmAnimationCurve_t2685995989 ** get_address_of_animCurve_35() { return &___animCurve_35; }
	inline void set_animCurve_35(FsmAnimationCurve_t2685995989 * value)
	{
		___animCurve_35 = value;
		Il2CppCodeGenWriteBarrier(&___animCurve_35, value);
	}

	inline static int32_t get_offset_of_calculation_36() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t1182581439, ___calculation_36)); }
	inline int32_t get_calculation_36() const { return ___calculation_36; }
	inline int32_t* get_address_of_calculation_36() { return &___calculation_36; }
	inline void set_calculation_36(int32_t value)
	{
		___calculation_36 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_37() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t1182581439, ___finishInNextStep_37)); }
	inline bool get_finishInNextStep_37() const { return ___finishInNextStep_37; }
	inline bool* get_address_of_finishInNextStep_37() { return &___finishInNextStep_37; }
	inline void set_finishInNextStep_37(bool value)
	{
		___finishInNextStep_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
