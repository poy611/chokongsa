﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse
struct WaitingRoomUIResponse_t3333463309;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t3104490121;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::.ctor(System.IntPtr)
extern "C"  void WaitingRoomUIResponse__ctor_m2695320294 (WaitingRoomUIResponse_t3333463309 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::ResponseStatus()
extern "C"  int32_t WaitingRoomUIResponse_ResponseStatus_m1150982399 (WaitingRoomUIResponse_t3333463309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::Room()
extern "C"  NativeRealTimeRoom_t3104490121 * WaitingRoomUIResponse_Room_m3200753764 (WaitingRoomUIResponse_t3333463309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void WaitingRoomUIResponse_CallDispose_m2893213578 (WaitingRoomUIResponse_t3333463309 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::FromPointer(System.IntPtr)
extern "C"  WaitingRoomUIResponse_t3333463309 * WaitingRoomUIResponse_FromPointer_m3283248901 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
