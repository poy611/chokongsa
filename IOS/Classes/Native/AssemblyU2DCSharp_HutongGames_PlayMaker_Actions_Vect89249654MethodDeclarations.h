﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2LowPassFilter
struct Vector2LowPassFilter_t89249654;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::.ctor()
extern "C"  void Vector2LowPassFilter__ctor_m3387114416 (Vector2LowPassFilter_t89249654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::Reset()
extern "C"  void Vector2LowPassFilter_Reset_m1033547357 (Vector2LowPassFilter_t89249654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::OnEnter()
extern "C"  void Vector2LowPassFilter_OnEnter_m3118439303 (Vector2LowPassFilter_t89249654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::OnUpdate()
extern "C"  void Vector2LowPassFilter_OnUpdate_m1315897244 (Vector2LowPassFilter_t89249654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
