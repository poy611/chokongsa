﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6
struct U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6::.ctor()
extern "C"  void U3CPlayerIdAtIndexU3Ec__AnonStoreyA6__ctor_m2932992860 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6::<>m__CC(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_U3CU3Em__CC_m4215582500 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
