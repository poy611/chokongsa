﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionTarget
struct ActionTarget_t715758505;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionTarget715758505.h"

// System.Type HutongGames.PlayMaker.ActionTarget::get_ObjectType()
extern "C"  Type_t * ActionTarget_get_ObjectType_m1997208246 (ActionTarget_t715758505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionTarget::get_FieldName()
extern "C"  String_t* ActionTarget_get_FieldName_m3380969441 (ActionTarget_t715758505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionTarget::get_AllowPrefabs()
extern "C"  bool ActionTarget_get_AllowPrefabs_m3302454689 (ActionTarget_t715758505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionTarget::.ctor(System.Type,System.String,System.Boolean)
extern "C"  void ActionTarget__ctor_m2751379506 (ActionTarget_t715758505 * __this, Type_t * ___objectType0, String_t* ___fieldName1, bool ___allowPrefabs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionTarget::IsSameAs(HutongGames.PlayMaker.ActionTarget)
extern "C"  bool ActionTarget_IsSameAs_m889770593 (ActionTarget_t715758505 * __this, ActionTarget_t715758505 * ___actionTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionTarget::ToString()
extern "C"  String_t* ActionTarget_ToString_m3810848009 (ActionTarget_t715758505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
