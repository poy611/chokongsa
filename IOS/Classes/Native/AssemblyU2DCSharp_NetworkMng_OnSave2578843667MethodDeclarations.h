﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnSave
struct OnSave_t2578843667;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnSave::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSave__ctor_m3134070378 (OnSave_t2578843667 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnSave::Invoke()
extern "C"  void OnSave_Invoke_m1098054468 (OnSave_t2578843667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnSave::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSave_BeginInvoke_m3451681479 (OnSave_t2578843667 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnSave::EndInvoke(System.IAsyncResult)
extern "C"  void OnSave_EndInvoke_m1458918778 (OnSave_t2578843667 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
