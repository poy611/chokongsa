﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3368609491_gshared (KeyValuePair_2_t101070088 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3368609491(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t101070088 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3368609491_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3321873205_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3321873205(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t101070088 *, const MethodInfo*))KeyValuePair_2_get_Key_m3321873205_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1825586038_gshared (KeyValuePair_2_t101070088 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1825586038(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t101070088 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1825586038_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2372325657_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2372325657(__this, method) ((  int32_t (*) (KeyValuePair_2_t101070088 *, const MethodInfo*))KeyValuePair_2_get_Value_m2372325657_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3205817462_gshared (KeyValuePair_2_t101070088 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3205817462(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t101070088 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m3205817462_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1079106322_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1079106322(__this, method) ((  String_t* (*) (KeyValuePair_2_t101070088 *, const MethodInfo*))KeyValuePair_2_ToString_m1079106322_gshared)(__this, method)
