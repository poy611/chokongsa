﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeUp
struct WakeUp_t3440637767;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeUp::.ctor()
extern "C"  void WakeUp__ctor_m3183143167 (WakeUp_t3440637767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::Reset()
extern "C"  void WakeUp_Reset_m829576108 (WakeUp_t3440637767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::OnEnter()
extern "C"  void WakeUp_OnEnter_m375597334 (WakeUp_t3440637767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::DoWakeUp()
extern "C"  void WakeUp_DoWakeUp_m1507521455 (WakeUp_t3440637767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
