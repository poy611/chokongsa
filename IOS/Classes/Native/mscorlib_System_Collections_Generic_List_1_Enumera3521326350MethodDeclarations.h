﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1235617113(__this, ___l0, method) ((  void (*) (Enumerator_t3521326350 *, List_1_t3501653580 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2555337561(__this, method) ((  void (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4203998927(__this, method) ((  Il2CppObject * (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::Dispose()
#define Enumerator_Dispose_m2619521406(__this, method) ((  void (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::VerifyState()
#define Enumerator_VerifyState_m1514244535(__this, method) ((  void (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::MoveNext()
#define Enumerator_MoveNext_m2760983924(__this, method) ((  bool (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::get_Current()
#define Enumerator_get_Current_m5331560(__this, method) ((  FsmEvent_t2133468028 * (*) (Enumerator_t3521326350 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
