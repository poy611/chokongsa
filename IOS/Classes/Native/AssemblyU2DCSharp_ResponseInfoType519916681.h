﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserInfoType
struct UserInfoType_t3869313555;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResponseInfoType
struct  ResponseInfoType_t519916681  : public Il2CppObject
{
public:
	// UserInfoType ResponseInfoType::resultData
	UserInfoType_t3869313555 * ___resultData_0;
	// System.Int32 ResponseInfoType::resultCode
	int32_t ___resultCode_1;
	// System.String ResponseInfoType::resultMsg
	String_t* ___resultMsg_2;

public:
	inline static int32_t get_offset_of_resultData_0() { return static_cast<int32_t>(offsetof(ResponseInfoType_t519916681, ___resultData_0)); }
	inline UserInfoType_t3869313555 * get_resultData_0() const { return ___resultData_0; }
	inline UserInfoType_t3869313555 ** get_address_of_resultData_0() { return &___resultData_0; }
	inline void set_resultData_0(UserInfoType_t3869313555 * value)
	{
		___resultData_0 = value;
		Il2CppCodeGenWriteBarrier(&___resultData_0, value);
	}

	inline static int32_t get_offset_of_resultCode_1() { return static_cast<int32_t>(offsetof(ResponseInfoType_t519916681, ___resultCode_1)); }
	inline int32_t get_resultCode_1() const { return ___resultCode_1; }
	inline int32_t* get_address_of_resultCode_1() { return &___resultCode_1; }
	inline void set_resultCode_1(int32_t value)
	{
		___resultCode_1 = value;
	}

	inline static int32_t get_offset_of_resultMsg_2() { return static_cast<int32_t>(offsetof(ResponseInfoType_t519916681, ___resultMsg_2)); }
	inline String_t* get_resultMsg_2() const { return ___resultMsg_2; }
	inline String_t** get_address_of_resultMsg_2() { return &___resultMsg_2; }
	inline void set_resultMsg_2(String_t* value)
	{
		___resultMsg_2 = value;
		Il2CppCodeGenWriteBarrier(&___resultMsg_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
