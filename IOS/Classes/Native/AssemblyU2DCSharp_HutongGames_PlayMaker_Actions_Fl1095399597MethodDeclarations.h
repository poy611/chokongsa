﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatInterpolate
struct FloatInterpolate_t1095399597;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::.ctor()
extern "C"  void FloatInterpolate__ctor_m748084313 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::Reset()
extern "C"  void FloatInterpolate_Reset_m2689484550 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnEnter()
extern "C"  void FloatInterpolate_OnEnter_m1041214960 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatInterpolate::OnUpdate()
extern "C"  void FloatInterpolate_OnUpdate_m1346452051 (FloatInterpolate_t1095399597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
