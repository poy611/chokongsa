﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PopupManagement
struct PopupManagement_t282433007;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PopupScoring
struct  PopupScoring_t3800015011  : public MonoBehaviour_t667441552
{
public:
	// PopupManagement PopupScoring::popup
	PopupManagement_t282433007 * ___popup_2;

public:
	inline static int32_t get_offset_of_popup_2() { return static_cast<int32_t>(offsetof(PopupScoring_t3800015011, ___popup_2)); }
	inline PopupManagement_t282433007 * get_popup_2() const { return ___popup_2; }
	inline PopupManagement_t282433007 ** get_address_of_popup_2() { return &___popup_2; }
	inline void set_popup_2(PopupManagement_t282433007 * value)
	{
		___popup_2 = value;
		Il2CppCodeGenWriteBarrier(&___popup_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
