﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutLabel
struct GUILayoutLabel_t3579067543;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::.ctor()
extern "C"  void GUILayoutLabel__ctor_m781452719 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::Reset()
extern "C"  void GUILayoutLabel_Reset_m2722852956 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutLabel::OnGUI()
extern "C"  void GUILayoutLabel_OnGUI_m276851369 (GUILayoutLabel_t3579067543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
