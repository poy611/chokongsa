﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs2852864039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct  GetAnimatorIsMatchingTarget_t2980401859  : public FsmStateActionAnimatorBase_t2852864039
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::isMatchingActive
	FsmBool_t1075959796 * ___isMatchingActive_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingActivatedEvent
	FsmEvent_t2133468028 * ___matchingActivatedEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingDeactivedEvent
	FsmEvent_t2133468028 * ___matchingDeactivedEvent_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::_animator
	Animator_t2776330603 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___gameObject_14)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_14, value);
	}

	inline static int32_t get_offset_of_isMatchingActive_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___isMatchingActive_15)); }
	inline FsmBool_t1075959796 * get_isMatchingActive_15() const { return ___isMatchingActive_15; }
	inline FsmBool_t1075959796 ** get_address_of_isMatchingActive_15() { return &___isMatchingActive_15; }
	inline void set_isMatchingActive_15(FsmBool_t1075959796 * value)
	{
		___isMatchingActive_15 = value;
		Il2CppCodeGenWriteBarrier(&___isMatchingActive_15, value);
	}

	inline static int32_t get_offset_of_matchingActivatedEvent_16() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___matchingActivatedEvent_16)); }
	inline FsmEvent_t2133468028 * get_matchingActivatedEvent_16() const { return ___matchingActivatedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_matchingActivatedEvent_16() { return &___matchingActivatedEvent_16; }
	inline void set_matchingActivatedEvent_16(FsmEvent_t2133468028 * value)
	{
		___matchingActivatedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___matchingActivatedEvent_16, value);
	}

	inline static int32_t get_offset_of_matchingDeactivedEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ___matchingDeactivedEvent_17)); }
	inline FsmEvent_t2133468028 * get_matchingDeactivedEvent_17() const { return ___matchingDeactivedEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_matchingDeactivedEvent_17() { return &___matchingDeactivedEvent_17; }
	inline void set_matchingDeactivedEvent_17(FsmEvent_t2133468028 * value)
	{
		___matchingDeactivedEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___matchingDeactivedEvent_17, value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t2980401859, ____animator_18)); }
	inline Animator_t2776330603 * get__animator_18() const { return ____animator_18; }
	inline Animator_t2776330603 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t2776330603 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier(&____animator_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
