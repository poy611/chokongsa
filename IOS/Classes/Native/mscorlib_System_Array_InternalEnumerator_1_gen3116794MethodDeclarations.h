﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3116794.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1571587521_gshared (InternalEnumerator_1_t3116794 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1571587521(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3116794 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1571587521_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919(__this, method) ((  void (*) (InternalEnumerator_1_t3116794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1820601919_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3116794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m320037941_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2546015448_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2546015448(__this, method) ((  void (*) (InternalEnumerator_1_t3116794 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2546015448_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m100128495_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m100128495(__this, method) ((  bool (*) (InternalEnumerator_1_t3116794 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m100128495_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>>::get_Current()
extern "C"  KeyValuePair_2_t1220774118  InternalEnumerator_1_get_Current_m850510058_gshared (InternalEnumerator_1_t3116794 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m850510058(__this, method) ((  KeyValuePair_2_t1220774118  (*) (InternalEnumerator_1_t3116794 *, const MethodInfo*))InternalEnumerator_1_get_Current_m850510058_gshared)(__this, method)
