﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmArray
struct GetFsmArray_t1205911805;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::.ctor()
extern "C"  void GetFsmArray__ctor_m3702716121 (GetFsmArray_t1205911805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::Reset()
extern "C"  void GetFsmArray_Reset_m1349149062 (GetFsmArray_t1205911805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::OnEnter()
extern "C"  void GetFsmArray_OnEnter_m1468999792 (GetFsmArray_t1205911805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::DoSetFsmArrayCopy()
extern "C"  void GetFsmArray_DoSetFsmArrayCopy_m4230331196 (GetFsmArray_t1205911805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
