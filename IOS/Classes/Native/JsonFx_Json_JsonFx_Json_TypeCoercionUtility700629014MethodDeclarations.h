﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.TypeCoercionUtility
struct TypeCoercionUtility_t700629014;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>
struct Dictionary_2_t626389457;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t520966972;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Object4170816371.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> JsonFx.Json.TypeCoercionUtility::get_MemberMapCache()
extern "C"  Dictionary_2_t626389457 * TypeCoercionUtility_get_MemberMapCache_m2283621214 (TypeCoercionUtility_t700629014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern "C"  Il2CppObject * TypeCoercionUtility_ProcessTypeHint_m4089338635 (TypeCoercionUtility_t700629014 * __this, Il2CppObject * ___result0, String_t* ___typeInfo1, Type_t ** ___objectType2, Dictionary_2_t520966972 ** ___memberMap3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern "C"  Il2CppObject * TypeCoercionUtility_InstantiateObject_m3710207535 (TypeCoercionUtility_t700629014 * __this, Type_t * ___objectType0, Dictionary_2_t520966972 ** ___memberMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> JsonFx.Json.TypeCoercionUtility::CreateMemberMap(System.Type)
extern "C"  Dictionary_2_t520966972 * TypeCoercionUtility_CreateMemberMap_m983839384 (TypeCoercionUtility_t700629014 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type JsonFx.Json.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern "C"  Type_t * TypeCoercionUtility_GetMemberInfo_m2964064126 (Il2CppObject * __this /* static, unused */, Dictionary_2_t520966972 * ___memberMap0, String_t* ___memberName1, MemberInfo_t ** ___memberInfo2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern "C"  void TypeCoercionUtility_SetMemberValue_m3887813087 (TypeCoercionUtility_t700629014 * __this, Il2CppObject * ___result0, Type_t * ___memberType1, MemberInfo_t * ___memberInfo2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3277329918 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3471229122 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, Dictionary_2_t520966972 ** ___memberMap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JsonFx.Json.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceList_m2295736878 (TypeCoercionUtility_t700629014 * __this, Type_t * ___targetType0, Type_t * ___arrayType1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array JsonFx.Json.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern "C"  Il2CppArray * TypeCoercionUtility_CoerceArray_m2028205290 (TypeCoercionUtility_t700629014 * __this, Type_t * ___elementType0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.TypeCoercionUtility::IsNullable(System.Type)
extern "C"  bool TypeCoercionUtility_IsNullable_m1425495541 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.TypeCoercionUtility::.ctor()
extern "C"  void TypeCoercionUtility__ctor_m3843210747 (TypeCoercionUtility_t700629014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
