﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempingPointer/<MainCameraMove>c__Iterator2F
struct U3CMainCameraMoveU3Ec__Iterator2F_t3998083026;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::.ctor()
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F__ctor_m1971214089 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<MainCameraMove>c__Iterator2F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMainCameraMoveU3Ec__Iterator2F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1138446067 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TempingPointer/<MainCameraMove>c__Iterator2F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMainCameraMoveU3Ec__Iterator2F_System_Collections_IEnumerator_get_Current_m91723911 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TempingPointer/<MainCameraMove>c__Iterator2F::MoveNext()
extern "C"  bool U3CMainCameraMoveU3Ec__Iterator2F_MoveNext_m3077134387 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::Dispose()
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F_Dispose_m154445062 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::Reset()
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F_Reset_m3912614326 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
