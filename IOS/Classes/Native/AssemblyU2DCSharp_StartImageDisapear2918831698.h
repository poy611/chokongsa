﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// StructforMinigame
struct StructforMinigame_t1286533533;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartImageDisapear
struct  StartImageDisapear_t2918831698  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 StartImageDisapear::nowNum
	int32_t ___nowNum_2;
	// System.String StartImageDisapear::nowScene
	String_t* ___nowScene_3;
	// StructforMinigame StartImageDisapear::stuff
	StructforMinigame_t1286533533 * ___stuff_4;
	// System.Diagnostics.Stopwatch StartImageDisapear::sw
	Stopwatch_t3420517611 * ___sw_5;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(StartImageDisapear_t2918831698, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_nowScene_3() { return static_cast<int32_t>(offsetof(StartImageDisapear_t2918831698, ___nowScene_3)); }
	inline String_t* get_nowScene_3() const { return ___nowScene_3; }
	inline String_t** get_address_of_nowScene_3() { return &___nowScene_3; }
	inline void set_nowScene_3(String_t* value)
	{
		___nowScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_3, value);
	}

	inline static int32_t get_offset_of_stuff_4() { return static_cast<int32_t>(offsetof(StartImageDisapear_t2918831698, ___stuff_4)); }
	inline StructforMinigame_t1286533533 * get_stuff_4() const { return ___stuff_4; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_4() { return &___stuff_4; }
	inline void set_stuff_4(StructforMinigame_t1286533533 * value)
	{
		___stuff_4 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_4, value);
	}

	inline static int32_t get_offset_of_sw_5() { return static_cast<int32_t>(offsetof(StartImageDisapear_t2918831698, ___sw_5)); }
	inline Stopwatch_t3420517611 * get_sw_5() const { return ___sw_5; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_5() { return &___sw_5; }
	inline void set_sw_5(Stopwatch_t3420517611 * value)
	{
		___sw_5 = value;
		Il2CppCodeGenWriteBarrier(&___sw_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
