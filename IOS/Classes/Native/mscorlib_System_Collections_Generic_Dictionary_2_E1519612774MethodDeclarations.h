﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t202289382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1519612774.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2159893197_gshared (Enumerator_t1519612774 * __this, Dictionary_2_t202289382 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2159893197(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1519612774 *, Dictionary_2_t202289382 *, const MethodInfo*))Enumerator__ctor_m2159893197_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2266147124_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2266147124(__this, method) ((  Il2CppObject * (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2266147124_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3658641352_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3658641352(__this, method) ((  void (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3658641352_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3550727057_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3550727057(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3550727057_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4211289872_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4211289872(__this, method) ((  Il2CppObject * (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4211289872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2238810530_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2238810530(__this, method) ((  Il2CppObject * (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2238810530_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2906371636_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2906371636(__this, method) ((  bool (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_MoveNext_m2906371636_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern "C"  KeyValuePair_2_t101070088  Enumerator_get_Current_m604579836_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m604579836(__this, method) ((  KeyValuePair_2_t101070088  (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_get_Current_m604579836_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m15664513_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15664513(__this, method) ((  Il2CppObject * (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_get_CurrentKey_m15664513_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m455036005_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m455036005(__this, method) ((  int32_t (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_get_CurrentValue_m455036005_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Reset()
extern "C"  void Enumerator_Reset_m1185419679_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1185419679(__this, method) ((  void (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_Reset_m1185419679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4289293928_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4289293928(__this, method) ((  void (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_VerifyState_m4289293928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1587732432_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1587732432(__this, method) ((  void (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_VerifyCurrent_m1587732432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3545407151_gshared (Enumerator_t1519612774 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3545407151(__this, method) ((  void (*) (Enumerator_t1519612774 *, const MethodInfo*))Enumerator_Dispose_m3545407151_gshared)(__this, method)
