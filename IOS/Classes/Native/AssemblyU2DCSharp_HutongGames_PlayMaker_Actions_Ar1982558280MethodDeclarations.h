﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayGetRandom
struct ArrayGetRandom_t1982558280;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::.ctor()
extern "C"  void ArrayGetRandom__ctor_m401434910 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::Reset()
extern "C"  void ArrayGetRandom_Reset_m2342835147 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::OnEnter()
extern "C"  void ArrayGetRandom_OnEnter_m2918587765 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::OnUpdate()
extern "C"  void ArrayGetRandom_OnUpdate_m3710434158 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetRandom::DoGetRandomValue()
extern "C"  void ArrayGetRandom_DoGetRandomValue_m3649088201 (ArrayGetRandom_t1982558280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
