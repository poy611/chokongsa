﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t3339007067;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_t3905956676  : public Il2CppObject
{
public:

public:
};

struct ReflectionUtils_t3905956676_StaticFields
{
public:
	// System.Type[] Newtonsoft.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t3339007067* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t3905956676_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t3339007067* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t3339007067** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t3339007067* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyTypes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
