﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t2971844791;
struct TypeNameKey_t2971844791_marshaled_pinvoke;
struct TypeNameKey_t2971844791_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::.ctor(System.String,System.String)
extern "C"  void TypeNameKey__ctor_m3669860623 (TypeNameKey_t2971844791 * __this, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::GetHashCode()
extern "C"  int32_t TypeNameKey_GetHashCode_m815232186 (TypeNameKey_t2971844791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::Equals(System.Object)
extern "C"  bool TypeNameKey_Equals_m1743571286 (TypeNameKey_t2971844791 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::Equals(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern "C"  bool TypeNameKey_Equals_m1887846567 (TypeNameKey_t2971844791 * __this, TypeNameKey_t2971844791  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TypeNameKey_t2971844791;
struct TypeNameKey_t2971844791_marshaled_pinvoke;

extern "C" void TypeNameKey_t2971844791_marshal_pinvoke(const TypeNameKey_t2971844791& unmarshaled, TypeNameKey_t2971844791_marshaled_pinvoke& marshaled);
extern "C" void TypeNameKey_t2971844791_marshal_pinvoke_back(const TypeNameKey_t2971844791_marshaled_pinvoke& marshaled, TypeNameKey_t2971844791& unmarshaled);
extern "C" void TypeNameKey_t2971844791_marshal_pinvoke_cleanup(TypeNameKey_t2971844791_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TypeNameKey_t2971844791;
struct TypeNameKey_t2971844791_marshaled_com;

extern "C" void TypeNameKey_t2971844791_marshal_com(const TypeNameKey_t2971844791& unmarshaled, TypeNameKey_t2971844791_marshaled_com& marshaled);
extern "C" void TypeNameKey_t2971844791_marshal_com_back(const TypeNameKey_t2971844791_marshaled_com& marshaled, TypeNameKey_t2971844791& unmarshaled);
extern "C" void TypeNameKey_t2971844791_marshal_com_cleanup(TypeNameKey_t2971844791_marshaled_com& marshaled);
