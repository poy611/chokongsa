﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayTransferValue
struct  ArrayTransferValue_t3288840917  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayTransferValue::arraySource
	FsmArray_t2129666875 * ___arraySource_11;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayTransferValue::arrayTarget
	FsmArray_t2129666875 * ___arrayTarget_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayTransferValue::indexToTransfer
	FsmInt_t1596138449 * ___indexToTransfer_13;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.ArrayTransferValue::copyType
	FsmEnum_t1076048395 * ___copyType_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.ArrayTransferValue::pasteType
	FsmEnum_t1076048395 * ___pasteType_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayTransferValue::indexOutOfRange
	FsmEvent_t2133468028 * ___indexOutOfRange_16;

public:
	inline static int32_t get_offset_of_arraySource_11() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___arraySource_11)); }
	inline FsmArray_t2129666875 * get_arraySource_11() const { return ___arraySource_11; }
	inline FsmArray_t2129666875 ** get_address_of_arraySource_11() { return &___arraySource_11; }
	inline void set_arraySource_11(FsmArray_t2129666875 * value)
	{
		___arraySource_11 = value;
		Il2CppCodeGenWriteBarrier(&___arraySource_11, value);
	}

	inline static int32_t get_offset_of_arrayTarget_12() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___arrayTarget_12)); }
	inline FsmArray_t2129666875 * get_arrayTarget_12() const { return ___arrayTarget_12; }
	inline FsmArray_t2129666875 ** get_address_of_arrayTarget_12() { return &___arrayTarget_12; }
	inline void set_arrayTarget_12(FsmArray_t2129666875 * value)
	{
		___arrayTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___arrayTarget_12, value);
	}

	inline static int32_t get_offset_of_indexToTransfer_13() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___indexToTransfer_13)); }
	inline FsmInt_t1596138449 * get_indexToTransfer_13() const { return ___indexToTransfer_13; }
	inline FsmInt_t1596138449 ** get_address_of_indexToTransfer_13() { return &___indexToTransfer_13; }
	inline void set_indexToTransfer_13(FsmInt_t1596138449 * value)
	{
		___indexToTransfer_13 = value;
		Il2CppCodeGenWriteBarrier(&___indexToTransfer_13, value);
	}

	inline static int32_t get_offset_of_copyType_14() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___copyType_14)); }
	inline FsmEnum_t1076048395 * get_copyType_14() const { return ___copyType_14; }
	inline FsmEnum_t1076048395 ** get_address_of_copyType_14() { return &___copyType_14; }
	inline void set_copyType_14(FsmEnum_t1076048395 * value)
	{
		___copyType_14 = value;
		Il2CppCodeGenWriteBarrier(&___copyType_14, value);
	}

	inline static int32_t get_offset_of_pasteType_15() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___pasteType_15)); }
	inline FsmEnum_t1076048395 * get_pasteType_15() const { return ___pasteType_15; }
	inline FsmEnum_t1076048395 ** get_address_of_pasteType_15() { return &___pasteType_15; }
	inline void set_pasteType_15(FsmEnum_t1076048395 * value)
	{
		___pasteType_15 = value;
		Il2CppCodeGenWriteBarrier(&___pasteType_15, value);
	}

	inline static int32_t get_offset_of_indexOutOfRange_16() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t3288840917, ___indexOutOfRange_16)); }
	inline FsmEvent_t2133468028 * get_indexOutOfRange_16() const { return ___indexOutOfRange_16; }
	inline FsmEvent_t2133468028 ** get_address_of_indexOutOfRange_16() { return &___indexOutOfRange_16; }
	inline void set_indexOutOfRange_16(FsmEvent_t2133468028 * value)
	{
		___indexOutOfRange_16 = value;
		Il2CppCodeGenWriteBarrier(&___indexOutOfRange_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
