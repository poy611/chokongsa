﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringSplit
struct StringSplit_t2355590167;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringSplit::.ctor()
extern "C"  void StringSplit__ctor_m2021335295 (StringSplit_t2355590167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSplit::Reset()
extern "C"  void StringSplit_Reset_m3962735532 (StringSplit_t2355590167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSplit::OnEnter()
extern "C"  void StringSplit_OnEnter_m569729302 (StringSplit_t2355590167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
