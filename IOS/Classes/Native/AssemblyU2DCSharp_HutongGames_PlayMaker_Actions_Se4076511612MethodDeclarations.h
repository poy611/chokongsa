﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGameVolume
struct SetGameVolume_t4076511612;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::.ctor()
extern "C"  void SetGameVolume__ctor_m2619120442 (SetGameVolume_t4076511612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::Reset()
extern "C"  void SetGameVolume_Reset_m265553383 (SetGameVolume_t4076511612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnEnter()
extern "C"  void SetGameVolume_OnEnter_m3810605201 (SetGameVolume_t4076511612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnUpdate()
extern "C"  void SetGameVolume_OnUpdate_m1298203602 (SetGameVolume_t4076511612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
