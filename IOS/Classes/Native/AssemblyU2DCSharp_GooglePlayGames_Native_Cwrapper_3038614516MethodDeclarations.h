﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2241332029.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3906033828.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.UInt64 GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_ApproximateNumberOfScores(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScoreSummary_ScoreSummary_ApproximateNumberOfScores_m681694017 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScoreSummary_ScoreSummary_TimeSpan_m1941048333 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScoreSummary_ScoreSummary_LeaderboardId_m1270467645 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Collection(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScoreSummary_ScoreSummary_Collection_m942568027 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScoreSummary_ScoreSummary_Valid_m3692325664 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_CurrentPlayerScore(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScoreSummary_ScoreSummary_CurrentPlayerScore_m2788018287 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScoreSummary_ScoreSummary_Dispose_m93297487 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
