﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34
struct U3CGetStatsU3Ec__AnonStorey34_t1599266691;
// GooglePlayGames.BasicApi.PlayerStats
struct PlayerStats_t60064856;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayerSta60064856.h"

// System.Void GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34::.ctor()
extern "C"  void U3CGetStatsU3Ec__AnonStorey34__ctor_m545212024 (U3CGetStatsU3Ec__AnonStorey34_t1599266691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesLocalUser/<GetStats>c__AnonStorey34::<>m__3(GooglePlayGames.BasicApi.CommonStatusCodes,GooglePlayGames.BasicApi.PlayerStats)
extern "C"  void U3CGetStatsU3Ec__AnonStorey34_U3CU3Em__3_m3952412733 (U3CGetStatsU3Ec__AnonStorey34_t1599266691 * __this, int32_t ___rc0, PlayerStats_t60064856 * ___stats1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
