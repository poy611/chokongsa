﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// PlayMakerAnimatorMove
struct PlayMakerAnimatorMove_t1973299400;
// PlayMakerApplicationEvents
struct PlayMakerApplicationEvents_t1615127321;
// PlayMakerCollisionEnter
struct PlayMakerCollisionEnter_t4046453814;
// UnityEngine.Collision
struct Collision_t2494107688;
// PlayMakerCollisionEnter2D
struct PlayMakerCollisionEnter2D_t1696713992;
// UnityEngine.Collision2D
struct Collision2D_t2859305914;
// PlayMakerCollisionExit
struct PlayMakerCollisionExit_t3871318016;
// PlayMakerCollisionExit2D
struct PlayMakerCollisionExit2D_t894936658;
// PlayMakerCollisionStay
struct PlayMakerCollisionStay_t3871731003;
// PlayMakerCollisionStay2D
struct PlayMakerCollisionStay2D_t1291817165;
// PlayMakerControllerColliderHit
struct PlayMakerControllerColliderHit_t1717485139;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// PlayMakerFixedUpdate
struct PlayMakerFixedUpdate_t997170669;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t873065632;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// FsmTemplate
struct FsmTemplate_t1237263802;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUIText
struct GUIText_t3371372606;
// PlayMakerOnGUI
struct PlayMakerOnGUI_t940239724;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// UnityEngine.BitStream
struct BitStream_t239125475;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t963491929;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t2644459362;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886;
// PlayMakerFSM/<DoCoroutine>d__1
struct U3CDoCoroutineU3Ed__1_t3331551771;
// PlayMakerGlobals
struct PlayMakerGlobals_t3097244096;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// PlayMakerGUI
struct PlayMakerGUI_t3799848395;
// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// UnityEngine.Texture
struct Texture_t2526458961;
// PlayMakerJointBreak
struct PlayMakerJointBreak_t197925893;
// PlayMakerJointBreak2D
struct PlayMakerJointBreak2D_t1228223767;
// UnityEngine.Joint2D
struct Joint2D_t2513613714;
// PlayMakerMouseEvents
struct PlayMakerMouseEvents_t316289710;
// PlayMakerParticleCollision
struct PlayMakerParticleCollision_t2428451804;
// PlayMakerPrefs
struct PlayMakerPrefs_t941311808;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.String[]
struct StringU5BU5D_t4054002952;
// PlayMakerProxyBase
struct PlayMakerProxyBase_t3469687535;
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t191094001;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// PlayMakerTriggerEnter
struct PlayMakerTriggerEnter_t977448304;
// UnityEngine.Collider
struct Collider_t2939674232;
// PlayMakerTriggerEnter2D
struct PlayMakerTriggerEnter2D_t3024951234;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// PlayMakerTriggerExit
struct PlayMakerTriggerExit_t3495223174;
// PlayMakerTriggerExit2D
struct PlayMakerTriggerExit2D_t245046360;
// PlayMakerTriggerStay
struct PlayMakerTriggerStay_t3495636161;
// PlayMakerTriggerStay2D
struct PlayMakerTriggerStay2D_t641926867;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_PlayMakerAnimatorMove1973299400.h"
#include "PlayMaker_PlayMakerAnimatorMove1973299400MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_PlayMakerProxyBase3469687535.h"
#include "PlayMaker_ArrayTypes.h"
#include "mscorlib_System_Boolean476798718.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_PlayMakerProxyBase3469687535MethodDeclarations.h"
#include "PlayMaker_PlayMakerApplicationEvents1615127321.h"
#include "PlayMaker_PlayMakerApplicationEvents1615127321MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_PlayMakerCollisionEnter4046453814.h"
#include "PlayMaker_PlayMakerCollisionEnter4046453814MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"
#include "PlayMaker_PlayMakerCollisionEnter2D1696713992.h"
#include "PlayMaker_PlayMakerCollisionEnter2D1696713992MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D2859305914.h"
#include "PlayMaker_PlayMakerCollisionExit3871318016.h"
#include "PlayMaker_PlayMakerCollisionExit3871318016MethodDeclarations.h"
#include "PlayMaker_PlayMakerCollisionExit2D894936658.h"
#include "PlayMaker_PlayMakerCollisionExit2D894936658MethodDeclarations.h"
#include "PlayMaker_PlayMakerCollisionStay3871731003.h"
#include "PlayMaker_PlayMakerCollisionStay3871731003MethodDeclarations.h"
#include "PlayMaker_PlayMakerCollisionStay2D1291817165.h"
#include "PlayMaker_PlayMakerCollisionStay2D1291817165MethodDeclarations.h"
#include "PlayMaker_PlayMakerControllerColliderHit1717485139.h"
#include "PlayMaker_PlayMakerControllerColliderHit1717485139MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "PlayMaker_PlayMakerFixedUpdate997170669.h"
#include "PlayMaker_PlayMakerFixedUpdate997170669MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Collections_Generic_List_1_gen873065632.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_Collections_Generic_List_1_gen873065632MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat892738402.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat892738402MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "PlayMaker_FsmTemplate1237263802.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "PlayMaker_PlayMakerGlobals3097244096MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog1596141350MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility81143630MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "PlayMaker_PlayMakerOnGUI940239724.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM_U3CDoCoroutineU3Ed__13331551771MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM_U3CDoCoroutineU3Ed__13331551771.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData1076900934.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "UnityEngine_UnityEngine_BitStream239125475MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2862622538.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "mscorlib_System_Single4291918972.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2733244747.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmExecutionStack2677439066MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "PlayMaker_PlayMakerGlobals3097244096.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "PlayMaker_PlayMakerGUI3799848395.h"
#include "PlayMaker_PlayMakerGUI3799848395MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin3371348110.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_GUIStyleState1997423985.h"
#include "UnityEngine_UnityEngine_TextAnchor213922566.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "mscorlib_System_Comparison_1_gen2516208563MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2516208563.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "PlayMaker_PlayMakerPrefs941311808MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2895297978MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor2745727898MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2895297978.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_PlayMakerJointBreak197925893.h"
#include "PlayMaker_PlayMakerJointBreak197925893MethodDeclarations.h"
#include "PlayMaker_PlayMakerJointBreak2D1228223767.h"
#include "PlayMaker_PlayMakerJointBreak2D1228223767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"
#include "PlayMaker_PlayMakerMouseEvents316289710.h"
#include "PlayMaker_PlayMakerMouseEvents316289710MethodDeclarations.h"
#include "PlayMaker_PlayMakerOnGUI940239724MethodDeclarations.h"
#include "PlayMaker_PlayMakerParticleCollision2428451804.h"
#include "PlayMaker_PlayMakerParticleCollision2428451804MethodDeclarations.h"
#include "PlayMaker_PlayMakerPrefs941311808.h"
#include "PlayMaker_PlayMakerTriggerEnter977448304.h"
#include "PlayMaker_PlayMakerTriggerEnter977448304MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "PlayMaker_PlayMakerTriggerEnter2D3024951234.h"
#include "PlayMaker_PlayMakerTriggerEnter2D3024951234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "PlayMaker_PlayMakerTriggerExit3495223174.h"
#include "PlayMaker_PlayMakerTriggerExit3495223174MethodDeclarations.h"
#include "PlayMaker_PlayMakerTriggerExit2D245046360.h"
#include "PlayMaker_PlayMakerTriggerExit2D245046360MethodDeclarations.h"
#include "PlayMaker_PlayMakerTriggerStay3495636161.h"
#include "PlayMaker_PlayMakerTriggerStay3495636161MethodDeclarations.h"
#include "PlayMaker_PlayMakerTriggerStay2D641926867.h"
#include "PlayMaker_PlayMakerTriggerStay2D641926867MethodDeclarations.h"

// System.Void PlayMakerFSM::AddEventHandlerComponent<System.Object>(UnityEngine.HideFlags)
extern "C"  void PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared (PlayMakerFSM_t3799847376 * __this, int32_t ___hide0, const MethodInfo* method);
#define PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerMouseEvents>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t316289710_m3110809133(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t4046453814_m3868272769(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3871318016_m4047408895(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t3871731003_m3180305210(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t977448304_m922051899(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t3495223174_m627233541(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t3495636161_m4055097152(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t1696713992_m4243548627(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t894936658_m299706577(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1291817165_m236720716(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t3024951234_m3308740621(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t245046360_m3456140119(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t641926867_m3393154258(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerParticleCollision>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t2428451804_m931842523(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerControllerColliderHit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t1717485139_m2985429586(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerJointBreak>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerFixedUpdate>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t997170669_m1796377708(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayMakerOnGUI>()
#define Component_GetComponent_TisPlayMakerOnGUI_t940239724_m795880923(__this, method) ((  PlayMakerOnGUI_t940239724 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m816128272_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m816128272(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m816128272_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerOnGUI>()
#define GameObject_AddComponent_TisPlayMakerOnGUI_t940239724_m1339728068(__this, method) ((  PlayMakerOnGUI_t940239724 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m816128272_gshared)(__this, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerApplicationEvents>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t1615127321_m164481560(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorMove>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t1973299400_m1995069587(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorIK>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t2388642745_m4244238852(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t3799847376 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m26731433_gshared)(__this, ___hide0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
#define Component_GetComponent_TisGUITexture_t4020448292_m3507547536(__this, method) ((  GUITexture_t4020448292 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t3371372606_m737059366(__this, method) ((  GUIText_t3371372606 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerGlobals>()
#define ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312(__this /* static, unused */, method) ((  PlayMakerGlobals_t3097244096 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerGUI>()
#define GameObject_AddComponent_TisPlayMakerGUI_t3799848395_m3208523333(__this, method) ((  PlayMakerGUI_t3799848395 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m816128272_gshared)(__this, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerPrefs>()
#define ScriptableObject_CreateInstance_TisPlayMakerPrefs_t941311808_m3675345656(__this /* static, unused */, method) ((  PlayMakerPrefs_t941311808 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m1108124040_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* Component_GetComponents_TisIl2CppObject_m4290235696_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m4290235696(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4290235696_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<PlayMakerFSM>()
#define Component_GetComponents_TisPlayMakerFSM_t3799847376_m1766213632(__this, method) ((  PlayMakerFSMU5BU5D_t191094001* (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4290235696_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerAnimatorMove::OnAnimatorMove()
extern "C"  void PlayMakerAnimatorMove_OnAnimatorMove_m2999358618 (PlayMakerAnimatorMove_t1973299400 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleAnimatorMove_m3734961120(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_OnAnimatorMove_m839048454(L_10, /*hidden argument*/NULL);
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_13 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerAnimatorMove::.ctor()
extern "C"  void PlayMakerAnimatorMove__ctor_m1475188513 (PlayMakerAnimatorMove_t1973299400 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerApplicationEvents::OnApplicationFocus()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationFocus_m3979291463_MetadataUsageId;
extern "C"  void PlayMakerApplicationEvents_OnApplicationFocus_m3979291463 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationFocus_m3979291463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_HandleApplicationEvents_m1205607459(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_ApplicationFocus_m2966195833(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::OnApplicationPause()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationPause_m3880135909_MetadataUsageId;
extern "C"  void PlayMakerApplicationEvents_OnApplicationPause_m3880135909 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationPause_m3880135909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_HandleApplicationEvents_m1205607459(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_ApplicationPause_m2867040279(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::.ctor()
extern "C"  void PlayMakerApplicationEvents__ctor_m3083946724 (PlayMakerApplicationEvents_t1615127321 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionEnter_OnCollisionEnter_m1529344961 (PlayMakerCollisionEnter_t4046453814 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionEnter_m1653735950(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision_t2494107688 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionEnter_m2124714755(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionEnter::.ctor()
extern "C"  void PlayMakerCollisionEnter__ctor_m1235992051 (PlayMakerCollisionEnter_t4046453814 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter2D::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionEnter2D_OnCollisionEnter2D_m914417611 (PlayMakerCollisionEnter2D_t1696713992 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionEnter2D_m102673568(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision2D_t2859305914 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionEnter2D_m3950287647(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionEnter2D::.ctor()
extern "C"  void PlayMakerCollisionEnter2D__ctor_m912332769 (PlayMakerCollisionEnter2D_t1696713992 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit::OnCollisionExit(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionExit_OnCollisionExit_m567244971 (PlayMakerCollisionExit_t3871318016 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionExit_m3664496586(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision_t2494107688 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionExit_m3915687763(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionExit::.ctor()
extern "C"  void PlayMakerCollisionExit__ctor_m2875580125 (PlayMakerCollisionExit_t3871318016 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit2D::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionExit2D_OnCollisionExit2D_m1093697177 (PlayMakerCollisionExit2D_t894936658 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionExit2D_m4003328860(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision2D_t2859305914 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionExit2D_m343017327(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionExit2D::.ctor()
extern "C"  void PlayMakerCollisionExit2D__ctor_m303474251 (PlayMakerCollisionExit2D_t894936658 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay::OnCollisionStay(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionStay_OnCollisionStay_m2671619915 (PlayMakerCollisionStay_t3871731003 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionStay_m4061377093(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision_t2494107688 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionStay_m3472074680(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionStay::.ctor()
extern "C"  void PlayMakerCollisionStay__ctor_m3054715906 (PlayMakerCollisionStay_t3871731003 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay2D::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionStay2D_OnCollisionStay2D_m2197627513 (PlayMakerCollisionStay2D_t1291817165 * __this, Collision2D_t2859305914 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionStay2D_m3153406743(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collision2D_t2859305914 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionStay2D_m2685422932(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionStay2D::.ctor()
extern "C"  void PlayMakerCollisionStay2D__ctor_m654267952 (PlayMakerCollisionStay2D_t1291817165 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerControllerColliderHit::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void PlayMakerControllerColliderHit_OnControllerColliderHit_m3358584794 (PlayMakerControllerColliderHit_t1717485139 * __this, ControllerColliderHit_t2416790841 * ___hitCollider0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleControllerColliderHit_m456508509(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		ControllerColliderHit_t2416790841 * L_11 = ___hitCollider0;
		NullCheck(L_10);
		Fsm_OnControllerColliderHit_m23910575(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerControllerColliderHit::.ctor()
extern "C"  void PlayMakerControllerColliderHit__ctor_m2054239210 (PlayMakerControllerColliderHit_t1717485139 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFixedUpdate::FixedUpdate()
extern "C"  void PlayMakerFixedUpdate_FixedUpdate_m4072414091 (PlayMakerFixedUpdate_t997170669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleFixedUpdate_m1243618487(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_FixedUpdate_m2195343088(L_10, /*hidden argument*/NULL);
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_13 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerFixedUpdate::.ctor()
extern "C"  void PlayMakerFixedUpdate__ctor_m645794320 (PlayMakerFixedUpdate_t997170669 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerFSM::get_VersionNotes()
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t PlayMakerFSM_get_VersionNotes_m3395899468_MetadataUsageId;
extern "C"  String_t* PlayMakerFSM_get_VersionNotes_m3395899468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionNotes_m3395899468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral0;
	}
}
// System.String PlayMakerFSM::get_VersionLabel()
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t PlayMakerFSM_get_VersionLabel_m1203453887_MetadataUsageId;
extern "C"  String_t* PlayMakerFSM_get_VersionLabel_m1203453887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionLabel_m1203453887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral0;
	}
}
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_get_FsmList_m1444651517_MetadataUsageId;
extern "C"  List_1_t873065632 * PlayMakerFSM_get_FsmList_m1444651517 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_FsmList_m1444651517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		return L_0;
	}
}
// PlayMakerFSM PlayMakerFSM::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1029143498_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m162772018_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1053705578_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3902241800_MethodInfo_var;
extern const uint32_t PlayMakerFSM_FindFsmOnGameObject_m3929556549_MetadataUsageId;
extern "C"  PlayMakerFSM_t3799847376 * PlayMakerFSM_FindFsmOnGameObject_m3929556549 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___fsmName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_FindFsmOnGameObject_m3929556549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayMakerFSM_t3799847376 * V_0 = NULL;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	Enumerator_t892738402  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		Enumerator_t892738402  L_1 = List_1_GetEnumerator_m1029143498(L_0, /*hidden argument*/List_1_GetEnumerator_m1029143498_MethodInfo_var);
		V_2 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_000d:
		{
			PlayMakerFSM_t3799847376 * L_2 = Enumerator_get_Current_m162772018((&V_2), /*hidden argument*/Enumerator_get_Current_m162772018_MethodInfo_var);
			V_0 = L_2;
			PlayMakerFSM_t3799847376 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
			GameObject_t3674682005 * L_5 = ___go0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0023:
		{
			PlayMakerFSM_t3799847376 * L_7 = V_0;
			NullCheck(L_7);
			String_t* L_8 = PlayMakerFSM_get_FsmName_m1703792682(L_7, /*hidden argument*/NULL);
			String_t* L_9 = ___fsmName1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0035;
			}
		}

IL_0031:
		{
			PlayMakerFSM_t3799847376 * L_11 = V_0;
			V_1 = L_11;
			IL2CPP_LEAVE(0x50, FINALLY_0040);
		}

IL_0035:
		{
			bool L_12 = Enumerator_MoveNext_m1053705578((&V_2), /*hidden argument*/Enumerator_MoveNext_m1053705578_MethodInfo_var);
			if (L_12)
			{
				goto IL_000d;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3902241800((&V_2), /*hidden argument*/Enumerator_Dispose_m3902241800_MethodInfo_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004e:
	{
		return (PlayMakerFSM_t3799847376 *)NULL;
	}

IL_0050:
	{
		PlayMakerFSM_t3799847376 * L_13 = V_1;
		return L_13;
	}
}
// FsmTemplate PlayMakerFSM::get_FsmTemplate()
extern "C"  FsmTemplate_t1237263802 * PlayMakerFSM_get_FsmTemplate_m821320227 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		FsmTemplate_t1237263802 * L_0 = __this->get_fsmTemplate_7();
		return L_0;
	}
}
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C"  GUITexture_t4020448292 * PlayMakerFSM_get_GuiTexture_m219309322 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		GUITexture_t4020448292 * L_0 = __this->get_U3CGuiTextureU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_GuiTexture(UnityEngine.GUITexture)
extern "C"  void PlayMakerFSM_set_GuiTexture_m229602685 (PlayMakerFSM_t3799847376 * __this, GUITexture_t4020448292 * ___value0, const MethodInfo* method)
{
	{
		GUITexture_t4020448292 * L_0 = ___value0;
		__this->set_U3CGuiTextureU3Ek__BackingField_9(L_0);
		return;
	}
}
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C"  GUIText_t3371372606 * PlayMakerFSM_get_GuiText_m1922746504 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		GUIText_t3371372606 * L_0 = __this->get_U3CGuiTextU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_GuiText(UnityEngine.GUIText)
extern "C"  void PlayMakerFSM_set_GuiText_m3473618123 (PlayMakerFSM_t3799847376 * __this, GUIText_t3371372606 * ___value0, const MethodInfo* method)
{
	{
		GUIText_t3371372606 * L_0 = ___value0;
		__this->set_U3CGuiTextU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean PlayMakerFSM::get_DrawGizmos()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_get_DrawGizmos_m976637493_MetadataUsageId;
extern "C"  bool PlayMakerFSM_get_DrawGizmos_m976637493 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_DrawGizmos_m976637493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_U3CDrawGizmosU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_DrawGizmos(System.Boolean)
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_set_DrawGizmos_m3647490152_MetadataUsageId;
extern "C"  void PlayMakerFSM_set_DrawGizmos_m3647490152 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_set_DrawGizmos_m3647490152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->set_U3CDrawGizmosU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void PlayMakerFSM::Reset()
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Reset_m865086010_MetadataUsageId;
extern "C"  void PlayMakerFSM_Reset_m865086010 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Reset_m865086010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t1527112426 * L_1 = (Fsm_t1527112426 *)il2cpp_codegen_object_new(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm__ctor_m2246436149(L_1, /*hidden argument*/NULL);
		__this->set_fsm_6(L_1);
	}

IL_0013:
	{
		__this->set_fsmTemplate_7((FsmTemplate_t1237263802 *)NULL);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_Reset_m1527928237(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Awake()
extern Il2CppClass* FsmLog_t1596141350_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Awake_m3456258288_MetadataUsageId;
extern "C"  void PlayMakerFSM_Awake_m3456258288 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Awake_m3456258288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m3132596247(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = PlayMakerGlobals_get_IsEditor_m4210118399(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmLog_t1596141350_il2cpp_TypeInfo_var);
		FsmLog_set_LoggingEnabled_m1472154829(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		PlayMakerFSM_Init_m4073236775(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Preprocess()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Preprocess_m1559229187_MetadataUsageId;
extern "C"  void PlayMakerFSM_Preprocess_m1559229187 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Preprocess_m1559229187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmTemplate_t1237263802 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m1217959233(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		PlayMakerFSM_InitFsm_m115766331(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_Preprocess_m1676843348(L_2, __this, /*hidden argument*/NULL);
		PlayMakerFSM_AddEventHandlerComponents_m3970753138(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Init()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Init_m4073236775_MetadataUsageId;
extern "C"  void PlayMakerFSM_Init_m4073236775 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Init_m4073236775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmTemplate_t1237263802 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m1217959233(__this, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_001d:
	{
		PlayMakerFSM_InitFsm_m115766331(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		bool L_3 = PlayMakerGlobals_get_IsEditor_m4210118399(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_set_Preprocessed_m3949206638(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_eventHandlerComponentsAdded_8((bool)0);
	}

IL_003d:
	{
		Fsm_t1527112426 * L_5 = __this->get_fsm_6();
		NullCheck(L_5);
		Fsm_Init_m4020318640(L_5, __this, /*hidden argument*/NULL);
		bool L_6 = __this->get_eventHandlerComponentsAdded_8();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		Fsm_t1527112426 * L_7 = __this->get_fsm_6();
		NullCheck(L_7);
		bool L_8 = Fsm_get_Preprocessed_m3347034827(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}

IL_005e:
	{
		PlayMakerFSM_AddEventHandlerComponents_m3970753138(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void PlayMakerFSM::InitTemplate()
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_InitTemplate_m1217959233_MetadataUsageId;
extern "C"  void PlayMakerFSM_InitTemplate_m1217959233 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitTemplate_m1217959233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	Fsm_t1527112426 * V_4 = NULL;
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m2749457024(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = L_2->get_EnableDebugFlow_34();
		V_1 = L_3;
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		bool L_5 = L_4->get_EnableBreakpoints_35();
		V_2 = L_5;
		Fsm_t1527112426 * L_6 = __this->get_fsm_6();
		NullCheck(L_6);
		bool L_7 = Fsm_get_ShowStateLabel_m1575907712(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		FsmTemplate_t1237263802 * L_8 = __this->get_fsmTemplate_7();
		NullCheck(L_8);
		Fsm_t1527112426 * L_9 = L_8->get_fsm_3();
		Fsm_t1527112426 * L_10 = __this->get_fsm_6();
		NullCheck(L_10);
		FsmVariables_t963491929 * L_11 = Fsm_get_Variables_m2281949087(L_10, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_12 = (Fsm_t1527112426 *)il2cpp_codegen_object_new(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm__ctor_m2998734970(L_12, L_9, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		Fsm_t1527112426 * L_13 = V_4;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		Fsm_set_Name_m2790512555(L_13, L_14, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_15 = V_4;
		NullCheck(L_15);
		Fsm_set_UsedInTemplate_m2612368368(L_15, (FsmTemplate_t1237263802 *)NULL, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_16 = V_4;
		bool L_17 = V_1;
		NullCheck(L_16);
		L_16->set_EnableDebugFlow_34(L_17);
		Fsm_t1527112426 * L_18 = V_4;
		bool L_19 = V_2;
		NullCheck(L_18);
		L_18->set_EnableBreakpoints_35(L_19);
		Fsm_t1527112426 * L_20 = V_4;
		bool L_21 = V_3;
		NullCheck(L_20);
		Fsm_set_ShowStateLabel_m3525398371(L_20, L_21, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_22 = V_4;
		__this->set_fsm_6(L_22);
		return;
	}
}
// System.Void PlayMakerFSM::InitFsm()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2791695429;
extern const uint32_t PlayMakerFSM_InitFsm_m115766331_MetadataUsageId;
extern "C"  void PlayMakerFSM_InitFsm_m115766331 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitFsm_m115766331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		PlayMakerFSM_Reset_m865086010(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2791695429, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t316289710_m3110809133_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t4046453814_m3868272769_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3871318016_m4047408895_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t3871731003_m3180305210_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t977448304_m922051899_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t3495223174_m627233541_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t3495636161_m4055097152_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t1696713992_m4243548627_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t894936658_m299706577_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1291817165_m236720716_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t3024951234_m3308740621_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t245046360_m3456140119_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t641926867_m3393154258_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t2428451804_m931842523_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t1717485139_m2985429586_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t997170669_m1796377708_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayMakerOnGUI_t940239724_m795880923_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayMakerOnGUI_t940239724_m1339728068_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t1615127321_m164481560_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t1973299400_m1995069587_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t2388642745_m4244238852_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral30829694;
extern const uint32_t PlayMakerFSM_AddEventHandlerComponents_m3970753138_MetadataUsageId;
extern "C"  void PlayMakerFSM_AddEventHandlerComponents_m3970753138 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_AddEventHandlerComponents_m3970753138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayMakerOnGUI_t940239724 * V_0 = NULL;
	{
		bool L_0 = PlayMakerGlobals_get_IsEditor_m4210118399(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		String_t* L_2 = FsmUtility_GetFullFsmLabel_m175618885(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral30829694, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		bool L_5 = Fsm_get_MouseEvents_m624787264(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t316289710_m3110809133(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t316289710_m3110809133_MethodInfo_var);
	}

IL_0035:
	{
		Fsm_t1527112426 * L_6 = __this->get_fsm_6();
		NullCheck(L_6);
		bool L_7 = Fsm_get_HandleCollisionEnter_m1653735950(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t4046453814_m3868272769(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t4046453814_m3868272769_MethodInfo_var);
	}

IL_0049:
	{
		Fsm_t1527112426 * L_8 = __this->get_fsm_6();
		NullCheck(L_8);
		bool L_9 = Fsm_get_HandleCollisionExit_m3664496586(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3871318016_m4047408895(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3871318016_m4047408895_MethodInfo_var);
	}

IL_005d:
	{
		Fsm_t1527112426 * L_10 = __this->get_fsm_6();
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionStay_m4061377093(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0071;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t3871731003_m3180305210(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t3871731003_m3180305210_MethodInfo_var);
	}

IL_0071:
	{
		Fsm_t1527112426 * L_12 = __this->get_fsm_6();
		NullCheck(L_12);
		bool L_13 = Fsm_get_HandleTriggerEnter_m204797576(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0085;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t977448304_m922051899(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t977448304_m922051899_MethodInfo_var);
	}

IL_0085:
	{
		Fsm_t1527112426 * L_14 = __this->get_fsm_6();
		NullCheck(L_14);
		bool L_15 = Fsm_get_HandleTriggerExit_m985357328(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0099;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t3495223174_m627233541(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t3495223174_m627233541_MethodInfo_var);
	}

IL_0099:
	{
		Fsm_t1527112426 * L_16 = __this->get_fsm_6();
		NullCheck(L_16);
		bool L_17 = Fsm_get_HandleTriggerStay_m1382237835(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ad;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t3495636161_m4055097152(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t3495636161_m4055097152_MethodInfo_var);
	}

IL_00ad:
	{
		Fsm_t1527112426 * L_18 = __this->get_fsm_6();
		NullCheck(L_18);
		bool L_19 = Fsm_get_HandleCollisionEnter2D_m102673568(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c1;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t1696713992_m4243548627(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t1696713992_m4243548627_MethodInfo_var);
	}

IL_00c1:
	{
		Fsm_t1527112426 * L_20 = __this->get_fsm_6();
		NullCheck(L_20);
		bool L_21 = Fsm_get_HandleCollisionExit2D_m4003328860(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d5;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t894936658_m299706577(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t894936658_m299706577_MethodInfo_var);
	}

IL_00d5:
	{
		Fsm_t1527112426 * L_22 = __this->get_fsm_6();
		NullCheck(L_22);
		bool L_23 = Fsm_get_HandleCollisionStay2D_m3153406743(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e9;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1291817165_m236720716(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1291817165_m236720716_MethodInfo_var);
	}

IL_00e9:
	{
		Fsm_t1527112426 * L_24 = __this->get_fsm_6();
		NullCheck(L_24);
		bool L_25 = Fsm_get_HandleTriggerEnter2D_m3537267354(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fd;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t3024951234_m3308740621(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t3024951234_m3308740621_MethodInfo_var);
	}

IL_00fd:
	{
		Fsm_t1527112426 * L_26 = __this->get_fsm_6();
		NullCheck(L_26);
		bool L_27 = Fsm_get_HandleTriggerExit2D_m2035912226(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0111;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t245046360_m3456140119(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t245046360_m3456140119_MethodInfo_var);
	}

IL_0111:
	{
		Fsm_t1527112426 * L_28 = __this->get_fsm_6();
		NullCheck(L_28);
		bool L_29 = Fsm_get_HandleTriggerStay2D_m1185990109(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0125;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t641926867_m3393154258(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t641926867_m3393154258_MethodInfo_var);
	}

IL_0125:
	{
		Fsm_t1527112426 * L_30 = __this->get_fsm_6();
		NullCheck(L_30);
		bool L_31 = Fsm_get_HandleParticleCollision_m1126387750(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0139;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t2428451804_m931842523(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t2428451804_m931842523_MethodInfo_var);
	}

IL_0139:
	{
		Fsm_t1527112426 * L_32 = __this->get_fsm_6();
		NullCheck(L_32);
		bool L_33 = Fsm_get_HandleControllerColliderHit_m456508509(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_014d;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t1717485139_m2985429586(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t1717485139_m2985429586_MethodInfo_var);
	}

IL_014d:
	{
		Fsm_t1527112426 * L_34 = __this->get_fsm_6();
		NullCheck(L_34);
		bool L_35 = Fsm_get_HandleJointBreak_m2499030429(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0161;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056_MethodInfo_var);
	}

IL_0161:
	{
		Fsm_t1527112426 * L_36 = __this->get_fsm_6();
		NullCheck(L_36);
		bool L_37 = Fsm_get_HandleJointBreak2D_m681848943(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0175;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t197925893_m1288511056_MethodInfo_var);
	}

IL_0175:
	{
		Fsm_t1527112426 * L_38 = __this->get_fsm_6();
		NullCheck(L_38);
		bool L_39 = Fsm_get_HandleFixedUpdate_m1243618487(L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0189;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t997170669_m1796377708(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t997170669_m1796377708_MethodInfo_var);
	}

IL_0189:
	{
		Fsm_t1527112426 * L_40 = __this->get_fsm_6();
		NullCheck(L_40);
		bool L_41 = Fsm_get_HandleOnGUI_m3061956918(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_01b7;
		}
	}
	{
		PlayMakerOnGUI_t940239724 * L_42 = Component_GetComponent_TisPlayMakerOnGUI_t940239724_m795880923(__this, /*hidden argument*/Component_GetComponent_TisPlayMakerOnGUI_t940239724_m795880923_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_42, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01b7;
		}
	}
	{
		GameObject_t3674682005 * L_44 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		PlayMakerOnGUI_t940239724 * L_45 = GameObject_AddComponent_TisPlayMakerOnGUI_t940239724_m1339728068(L_44, /*hidden argument*/GameObject_AddComponent_TisPlayMakerOnGUI_t940239724_m1339728068_MethodInfo_var);
		V_0 = L_45;
		PlayMakerOnGUI_t940239724 * L_46 = V_0;
		NullCheck(L_46);
		L_46->set_playMakerFSM_2(__this);
	}

IL_01b7:
	{
		Fsm_t1527112426 * L_47 = __this->get_fsm_6();
		NullCheck(L_47);
		bool L_48 = Fsm_get_HandleApplicationEvents_m1205607459(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01cb;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t1615127321_m164481560(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t1615127321_m164481560_MethodInfo_var);
	}

IL_01cb:
	{
		Fsm_t1527112426 * L_49 = __this->get_fsm_6();
		NullCheck(L_49);
		bool L_50 = Fsm_get_HandleAnimatorMove_m3734961120(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01df;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t1973299400_m1995069587(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t1973299400_m1995069587_MethodInfo_var);
	}

IL_01df:
	{
		Fsm_t1527112426 * L_51 = __this->get_fsm_6();
		NullCheck(L_51);
		bool L_52 = Fsm_get_HandleAnimatorIK_m3243950161(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01f3;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t2388642745_m4244238852(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t2388642745_m4244238852_MethodInfo_var);
	}

IL_01f3:
	{
		__this->set_eventHandlerComponentsAdded_8((bool)1);
		return;
	}
}
// System.Void PlayMakerFSM::SetFsmTemplate(FsmTemplate)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVariables_t963491929_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_SetFsmTemplate_m2282548073_MetadataUsageId;
extern "C"  void PlayMakerFSM_SetFsmTemplate_m2282548073 (PlayMakerFSM_t3799847376 * __this, FsmTemplate_t1237263802 * ___template0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SetFsmTemplate_m2282548073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmTemplate_t1237263802 * L_0 = ___template0;
		__this->set_fsmTemplate_7(L_0);
		Fsm_t1527112426 * L_1 = PlayMakerFSM_get_Fsm_m886945091(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_Clear_m1648954863(L_1, __this, /*hidden argument*/NULL);
		FsmTemplate_t1237263802 * L_2 = ___template0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Fsm_t1527112426 * L_4 = PlayMakerFSM_get_Fsm_m886945091(__this, /*hidden argument*/NULL);
		FsmTemplate_t1237263802 * L_5 = __this->get_fsmTemplate_7();
		NullCheck(L_5);
		Fsm_t1527112426 * L_6 = L_5->get_fsm_3();
		NullCheck(L_6);
		FsmVariables_t963491929 * L_7 = Fsm_get_Variables_m2281949087(L_6, /*hidden argument*/NULL);
		FsmVariables_t963491929 * L_8 = (FsmVariables_t963491929 *)il2cpp_codegen_object_new(FsmVariables_t963491929_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m3281811991(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Fsm_set_Variables_m1137150058(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003c:
	{
		PlayMakerFSM_Init_m4073236775(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Start()
extern const MethodInfo* Component_GetComponent_TisGUITexture_t4020448292_m3507547536_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUIText_t3371372606_m737059366_MethodInfo_var;
extern const uint32_t PlayMakerFSM_Start_m2165790861_MetadataUsageId;
extern "C"  void PlayMakerFSM_Start_m2165790861 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Start_m2165790861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUITexture_t4020448292 * L_0 = Component_GetComponent_TisGUITexture_t4020448292_m3507547536(__this, /*hidden argument*/Component_GetComponent_TisGUITexture_t4020448292_m3507547536_MethodInfo_var);
		PlayMakerFSM_set_GuiTexture_m229602685(__this, L_0, /*hidden argument*/NULL);
		GUIText_t3371372606 * L_1 = Component_GetComponent_TisGUIText_t3371372606_m737059366(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t3371372606_m737059366_MethodInfo_var);
		PlayMakerFSM_set_GuiText_m3473618123(__this, L_1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_Started_m2754310499(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0030;
		}
	}
	{
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_Start_m1193573941(L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnEnable()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4014241641_MethodInfo_var;
extern const uint32_t PlayMakerFSM_OnEnable_m3820273881_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnEnable_m3820273881 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnEnable_m3820273881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Add_m4014241641(L_0, __this, /*hidden argument*/List_1_Add_m4014241641_MethodInfo_var);
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_OnEnable_m1470487089(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Update()
extern "C"  void PlayMakerFSM_Update_m2720859424 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m2204445938(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_ManualUpdate_m4192751(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_Update_m2646905976(L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void PlayMakerFSM::LateUpdate()
extern "C"  void PlayMakerFSM_LateUpdate_m3191290598 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		FsmVariables_set_GlobalVariablesSynced_m997765184(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m2204445938(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_LateUpdate_m4198981182(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Collections.IEnumerator PlayMakerFSM::DoCoroutine(System.Collections.IEnumerator)
extern Il2CppClass* U3CDoCoroutineU3Ed__1_t3331551771_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_DoCoroutine_m3892440951_MetadataUsageId;
extern "C"  Il2CppObject * PlayMakerFSM_DoCoroutine_m3892440951 (PlayMakerFSM_t3799847376 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_DoCoroutine_m3892440951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CDoCoroutineU3Ed__1_t3331551771 * V_0 = NULL;
	{
		U3CDoCoroutineU3Ed__1_t3331551771 * L_0 = (U3CDoCoroutineU3Ed__1_t3331551771 *)il2cpp_codegen_object_new(U3CDoCoroutineU3Ed__1_t3331551771_il2cpp_TypeInfo_var);
		U3CDoCoroutineU3Ed__1__ctor_m2949731187(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoCoroutineU3Ed__1_t3331551771 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CDoCoroutineU3Ed__1_t3331551771 * L_2 = V_0;
		Il2CppObject * L_3 = ___routine0;
		NullCheck(L_2);
		L_2->set_routine_3(L_3);
		U3CDoCoroutineU3Ed__1_t3331551771 * L_4 = V_0;
		return L_4;
	}
}
// System.Void PlayMakerFSM::OnDisable()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m197639144_MethodInfo_var;
extern const uint32_t PlayMakerFSM_OnDisable_m2905310580_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnDisable_m2905310580 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDisable_m2905310580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Remove_m197639144(L_0, __this, /*hidden argument*/List_1_Remove_m197639144_MethodInfo_var);
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_Finished_m2204445938(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Fsm_t1527112426 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_OnDisable_m3076364060(L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnDestroy()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Remove_m197639144_MethodInfo_var;
extern const uint32_t PlayMakerFSM_OnDestroy_m787875334_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnDestroy_m787875334 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDestroy_m787875334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Remove_m197639144(L_0, __this, /*hidden argument*/List_1_Remove_m197639144_MethodInfo_var);
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_OnDestroy_m958928814(L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnApplicationQuit()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnApplicationQuit_m3370467723_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnApplicationQuit_m3370467723 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnApplicationQuit_m3370467723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_ApplicationQuit_m1247604976(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->set_ApplicationIsQuitting_4((bool)1);
		return;
	}
}
// System.Void PlayMakerFSM::OnDrawGizmos()
extern "C"  void PlayMakerFSM_OnDrawGizmos_m1555179763 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_OnDrawGizmos_m3578189387(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::SetState(System.String)
extern "C"  void PlayMakerFSM_SetState_m3084739100 (PlayMakerFSM_t3799847376 * __this, String_t* ___stateName0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___stateName0;
		NullCheck(L_0);
		Fsm_SetState_m1081751492(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_ChangeState_m3790102374 (PlayMakerFSM_t3799847376 * __this, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		FsmEvent_t2133468028 * L_1 = ___fsmEvent0;
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(System.String)
extern "C"  void PlayMakerFSM_ChangeState_m1344427734 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m4127177141(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendEvent(System.String)
extern "C"  void PlayMakerFSM_SendEvent_m1191977733 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m4127177141(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEvent(System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEvent_m1166116995 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m4127177141(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEventWithData(System.String,System.String)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_SendRemoteFsmEventWithData_m3428386255_MetadataUsageId;
extern "C"  void PlayMakerFSM_SendRemoteFsmEventWithData_m3428386255 (PlayMakerFSM_t3799847376 * __this, String_t* ___eventName0, String_t* ___eventData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SendRemoteFsmEventWithData_m3428386255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		String_t* L_1 = ___eventData1;
		NullCheck(L_0);
		L_0->set_StringData_8(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		String_t* L_3 = ___eventName0;
		NullCheck(L_2);
		Fsm_Event_m4127177141(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m3077906514_MetadataUsageId;
extern "C"  void PlayMakerFSM_BroadcastEvent_m3077906514 (Il2CppObject * __this /* static, unused */, String_t* ___fsmEventName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m3077906514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_2 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_GetFsmEvent_m2820038136(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		PlayMakerFSM_BroadcastEvent_m2315926890(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t873065632_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m683104147_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1029143498_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m162772018_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1053705578_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3902241800_MethodInfo_var;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m2315926890_MetadataUsageId;
extern "C"  void PlayMakerFSM_BroadcastEvent_m2315926890 (Il2CppObject * __this /* static, unused */, FsmEvent_t2133468028 * ___fsmEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m2315926890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t873065632 * V_0 = NULL;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	Enumerator_t892738402  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = PlayMakerFSM_get_FsmList_m1444651517(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t873065632 * L_1 = (List_1_t873065632 *)il2cpp_codegen_object_new(List_1_t873065632_il2cpp_TypeInfo_var);
		List_1__ctor_m683104147(L_1, L_0, /*hidden argument*/List_1__ctor_m683104147_MethodInfo_var);
		V_0 = L_1;
		List_1_t873065632 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t892738402  L_3 = List_1_GetEnumerator_m1029143498(L_2, /*hidden argument*/List_1_GetEnumerator_m1029143498_MethodInfo_var);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003a;
		}

IL_0014:
		{
			PlayMakerFSM_t3799847376 * L_4 = Enumerator_get_Current_m162772018((&V_2), /*hidden argument*/Enumerator_get_Current_m162772018_MethodInfo_var);
			V_1 = L_4;
			PlayMakerFSM_t3799847376 * L_5 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
			if (L_6)
			{
				goto IL_003a;
			}
		}

IL_0025:
		{
			PlayMakerFSM_t3799847376 * L_7 = V_1;
			NullCheck(L_7);
			Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_003a;
			}
		}

IL_002d:
		{
			PlayMakerFSM_t3799847376 * L_9 = V_1;
			NullCheck(L_9);
			Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
			FsmEvent_t2133468028 * L_11 = ___fsmEvent0;
			NullCheck(L_10);
			Fsm_ProcessEvent_m713583330(L_10, L_11, (FsmEventData_t1076900934 *)NULL, /*hidden argument*/NULL);
		}

IL_003a:
		{
			bool L_12 = Enumerator_MoveNext_m1053705578((&V_2), /*hidden argument*/Enumerator_MoveNext_m1053705578_MethodInfo_var);
			if (L_12)
			{
				goto IL_0014;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x53, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3902241800((&V_2), /*hidden argument*/Enumerator_Dispose_m3902241800_MethodInfo_var);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0053:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameVisible()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnBecameVisible_m3186968325_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnBecameVisible_m3186968325 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameVisible_m3186968325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_BecameVisible_m3823864746(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameInvisible()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnBecameInvisible_m2256533248_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnBecameInvisible_m2256533248 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameInvisible_m2256533248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_BecameInvisible_m133670501(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnLevelWasLoaded()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnLevelWasLoaded_m3548116416_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnLevelWasLoaded_m3548116416 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnLevelWasLoaded_m3548116416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_LevelLoaded_m1228564314(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnPlayerConnected_m3502723622_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnPlayerConnected_m3502723622 (PlayMakerFSM_t3799847376 * __this, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnPlayerConnected_m3502723622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkPlayer_t3231273765  L_1 = ___player0;
		NullCheck(L_0);
		L_0->set_Player_16(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_PlayerConnected_m4086688793(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnServerInitialized()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnServerInitialized_m1710961181_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnServerInitialized_m1710961181 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnServerInitialized_m1710961181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_ServerInitialized_m1749326914(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnConnectedToServer()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnConnectedToServer_m1863472339_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnConnectedToServer_m1863472339 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnConnectedToServer_m1863472339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_1 = FsmEvent_get_ConnectedToServer_m1901838072(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m625948263(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnPlayerDisconnected_m3176107848_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnPlayerDisconnected_m3176107848 (PlayMakerFSM_t3799847376 * __this, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnPlayerDisconnected_m3176107848_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkPlayer_t3231273765  L_1 = ___player0;
		NullCheck(L_0);
		L_0->set_Player_16(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_PlayerDisconnected_m3693946829(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnDisconnectedFromServer_m2633773257_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnDisconnectedFromServer_m2633773257 (PlayMakerFSM_t3799847376 * __this, int32_t ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDisconnectedFromServer_m2633773257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___info0;
		NullCheck(L_0);
		L_0->set_DisconnectionInfo_17(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_DisconnectedFromServer_m2566089017(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnFailedToConnect_m2978582699_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnFailedToConnect_m2978582699 (PlayMakerFSM_t3799847376 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnFailedToConnect_m2978582699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___error0;
		NullCheck(L_0);
		L_0->set_ConnectionError_18(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_FailedToConnect_m1479245475(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnNetworkInstantiate_m123436980_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnNetworkInstantiate_m123436980 (PlayMakerFSM_t3799847376 * __this, NetworkMessageInfo_t3807997963  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnNetworkInstantiate_m123436980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkMessageInfo_t3807997963  L_1 = ___info0;
		NullCheck(L_0);
		L_0->set_NetworkMessageInfo_19(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_NetworkInstantiate_m1398445037(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnSerializeNetworkView_m3151096693_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnSerializeNetworkView_m3151096693 (PlayMakerFSM_t3799847376 * __this, BitStream_t239125475 * ___stream0, NetworkMessageInfo_t3807997963  ___info1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnSerializeNetworkView_m3151096693_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = FsmVariables_get_GlobalVariablesSynced_m2820414597(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		FsmVariables_set_GlobalVariablesSynced_m997765184(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		BitStream_t239125475 * L_1 = ___stream0;
		FsmVariables_t963491929 * L_2 = FsmVariables_get_GlobalVariables_m2551687717(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		PlayMakerFSM_NetworkSyncVariables_m4130023628(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		BitStream_t239125475 * L_3 = ___stream0;
		Fsm_t1527112426 * L_4 = PlayMakerFSM_get_Fsm_m886945091(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmVariables_t963491929 * L_5 = Fsm_get_Variables_m2281949087(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		PlayMakerFSM_NetworkSyncVariables_m4130023628(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::NetworkSyncVariables(UnityEngine.BitStream,HutongGames.PlayMaker.FsmVariables)
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_NetworkSyncVariables_m4130023628_MetadataUsageId;
extern "C"  void PlayMakerFSM_NetworkSyncVariables_m4130023628 (Il2CppObject * __this /* static, unused */, BitStream_t239125475 * ___stream0, FsmVariables_t963491929 * ___variables1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_NetworkSyncVariables_m4130023628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * V_0 = NULL;
	CharU5BU5D_t3324145743* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	FsmBool_t1075959796 * V_4 = NULL;
	bool V_5 = false;
	FsmFloat_t2134102846 * V_6 = NULL;
	float V_7 = 0.0f;
	FsmInt_t1596138449 * V_8 = NULL;
	int32_t V_9 = 0;
	FsmQuaternion_t3871136040 * V_10 = NULL;
	Quaternion_t1553702882  V_11;
	memset(&V_11, 0, sizeof(V_11));
	FsmVector3_t533912882 * V_12 = NULL;
	Vector3_t4282066566  V_13;
	memset(&V_13, 0, sizeof(V_13));
	FsmColor_t2131419205 * V_14 = NULL;
	Color_t4194546905  V_15;
	memset(&V_15, 0, sizeof(V_15));
	FsmVector2_t533912881 * V_16 = NULL;
	Vector2_t4282066565  V_17;
	memset(&V_17, 0, sizeof(V_17));
	FsmString_t952858651 * V_18 = NULL;
	int32_t V_19 = 0;
	CharU5BU5D_t3324145743* V_20 = NULL;
	int32_t V_21 = 0;
	FsmBool_t1075959796 * V_22 = NULL;
	bool V_23 = false;
	FsmFloat_t2134102846 * V_24 = NULL;
	float V_25 = 0.0f;
	FsmInt_t1596138449 * V_26 = NULL;
	int32_t V_27 = 0;
	FsmQuaternion_t3871136040 * V_28 = NULL;
	Quaternion_t1553702882  V_29;
	memset(&V_29, 0, sizeof(V_29));
	FsmVector3_t533912882 * V_30 = NULL;
	Vector3_t4282066566  V_31;
	memset(&V_31, 0, sizeof(V_31));
	FsmColor_t2131419205 * V_32 = NULL;
	float V_33 = 0.0f;
	float V_34 = 0.0f;
	float V_35 = 0.0f;
	float V_36 = 0.0f;
	FsmVector2_t533912881 * V_37 = NULL;
	float V_38 = 0.0f;
	float V_39 = 0.0f;
	FsmStringU5BU5D_t2523845914* V_40 = NULL;
	int32_t V_41 = 0;
	FsmBoolU5BU5D_t3689162173* V_42 = NULL;
	int32_t V_43 = 0;
	FsmFloatU5BU5D_t2945380875* V_44 = NULL;
	int32_t V_45 = 0;
	FsmIntU5BU5D_t1976821196* V_46 = NULL;
	int32_t V_47 = 0;
	FsmQuaternionU5BU5D_t1443435833* V_48 = NULL;
	int32_t V_49 = 0;
	FsmVector3U5BU5D_t607182279* V_50 = NULL;
	int32_t V_51 = 0;
	FsmColorU5BU5D_t530285832* V_52 = NULL;
	int32_t V_53 = 0;
	FsmVector2U5BU5D_t120994540* V_54 = NULL;
	int32_t V_55 = 0;
	FsmStringU5BU5D_t2523845914* V_56 = NULL;
	int32_t V_57 = 0;
	FsmBoolU5BU5D_t3689162173* V_58 = NULL;
	int32_t V_59 = 0;
	FsmFloatU5BU5D_t2945380875* V_60 = NULL;
	{
		BitStream_t239125475 * L_0 = ___stream0;
		NullCheck(L_0);
		bool L_1 = BitStream_get_isWriting_m3632391065(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0248;
		}
	}
	{
		FsmVariables_t963491929 * L_2 = ___variables1;
		NullCheck(L_2);
		FsmStringU5BU5D_t2523845914* L_3 = FsmVariables_get_StringVariables_m3261297161(L_2, /*hidden argument*/NULL);
		V_40 = L_3;
		V_41 = 0;
		goto IL_005d;
	}

IL_0018:
	{
		FsmStringU5BU5D_t2523845914* L_4 = V_40;
		int32_t L_5 = V_41;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		FsmString_t952858651 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		FsmString_t952858651 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = NamedVariable_get_NetworkSync_m3720647190(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		FsmString_t952858651 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		CharU5BU5D_t3324145743* L_12 = String_ToCharArray_m1208288742(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		CharU5BU5D_t3324145743* L_13 = V_1;
		NullCheck(L_13);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		BitStream_t239125475 * L_14 = ___stream0;
		NullCheck(L_14);
		BitStream_Serialize_m4204493009(L_14, (&V_2), /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0053;
	}

IL_0042:
	{
		BitStream_t239125475 * L_15 = ___stream0;
		CharU5BU5D_t3324145743* L_16 = V_1;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		NullCheck(L_15);
		BitStream_Serialize_m2728753561(L_15, ((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17))), /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_19 = V_3;
		int32_t L_20 = V_2;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0042;
		}
	}

IL_0057:
	{
		int32_t L_21 = V_41;
		V_41 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_22 = V_41;
		FsmStringU5BU5D_t2523845914* L_23 = V_40;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		FsmVariables_t963491929 * L_24 = ___variables1;
		NullCheck(L_24);
		FsmBoolU5BU5D_t3689162173* L_25 = FsmVariables_get_BoolVariables_m4286069353(L_24, /*hidden argument*/NULL);
		V_42 = L_25;
		V_43 = 0;
		goto IL_0099;
	}

IL_0072:
	{
		FsmBoolU5BU5D_t3689162173* L_26 = V_42;
		int32_t L_27 = V_43;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		FsmBool_t1075959796 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_4 = L_29;
		FsmBool_t1075959796 * L_30 = V_4;
		NullCheck(L_30);
		bool L_31 = NamedVariable_get_NetworkSync_m3720647190(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0093;
		}
	}
	{
		FsmBool_t1075959796 * L_32 = V_4;
		NullCheck(L_32);
		bool L_33 = FsmBool_get_Value_m3101329097(L_32, /*hidden argument*/NULL);
		V_5 = L_33;
		BitStream_t239125475 * L_34 = ___stream0;
		NullCheck(L_34);
		BitStream_Serialize_m2001881067(L_34, (&V_5), /*hidden argument*/NULL);
	}

IL_0093:
	{
		int32_t L_35 = V_43;
		V_43 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_36 = V_43;
		FsmBoolU5BU5D_t3689162173* L_37 = V_42;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0072;
		}
	}
	{
		FsmVariables_t963491929 * L_38 = ___variables1;
		NullCheck(L_38);
		FsmFloatU5BU5D_t2945380875* L_39 = FsmVariables_get_FloatVariables_m1157149957(L_38, /*hidden argument*/NULL);
		V_44 = L_39;
		V_45 = 0;
		goto IL_00d5;
	}

IL_00ae:
	{
		FsmFloatU5BU5D_t2945380875* L_40 = V_44;
		int32_t L_41 = V_45;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		FsmFloat_t2134102846 * L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		V_6 = L_43;
		FsmFloat_t2134102846 * L_44 = V_6;
		NullCheck(L_44);
		bool L_45 = NamedVariable_get_NetworkSync_m3720647190(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00cf;
		}
	}
	{
		FsmFloat_t2134102846 * L_46 = V_6;
		NullCheck(L_46);
		float L_47 = FsmFloat_get_Value_m4137923823(L_46, /*hidden argument*/NULL);
		V_7 = L_47;
		BitStream_t239125475 * L_48 = ___stream0;
		NullCheck(L_48);
		BitStream_Serialize_m1473984011(L_48, (&V_7), /*hidden argument*/NULL);
	}

IL_00cf:
	{
		int32_t L_49 = V_45;
		V_45 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_50 = V_45;
		FsmFloatU5BU5D_t2945380875* L_51 = V_44;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_51)->max_length)))))))
		{
			goto IL_00ae;
		}
	}
	{
		FsmVariables_t963491929 * L_52 = ___variables1;
		NullCheck(L_52);
		FsmIntU5BU5D_t1976821196* L_53 = FsmVariables_get_IntVariables_m1527799903(L_52, /*hidden argument*/NULL);
		V_46 = L_53;
		V_47 = 0;
		goto IL_0111;
	}

IL_00ea:
	{
		FsmIntU5BU5D_t1976821196* L_54 = V_46;
		int32_t L_55 = V_47;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, L_55);
		int32_t L_56 = L_55;
		FsmInt_t1596138449 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_8 = L_57;
		FsmInt_t1596138449 * L_58 = V_8;
		NullCheck(L_58);
		bool L_59 = NamedVariable_get_NetworkSync_m3720647190(L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_010b;
		}
	}
	{
		FsmInt_t1596138449 * L_60 = V_8;
		NullCheck(L_60);
		int32_t L_61 = FsmInt_get_Value_m27059446(L_60, /*hidden argument*/NULL);
		V_9 = L_61;
		BitStream_t239125475 * L_62 = ___stream0;
		NullCheck(L_62);
		BitStream_Serialize_m4204493009(L_62, (&V_9), /*hidden argument*/NULL);
	}

IL_010b:
	{
		int32_t L_63 = V_47;
		V_47 = ((int32_t)((int32_t)L_63+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_64 = V_47;
		FsmIntU5BU5D_t1976821196* L_65 = V_46;
		NullCheck(L_65);
		if ((((int32_t)L_64) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_65)->max_length)))))))
		{
			goto IL_00ea;
		}
	}
	{
		FsmVariables_t963491929 * L_66 = ___variables1;
		NullCheck(L_66);
		FsmQuaternionU5BU5D_t1443435833* L_67 = FsmVariables_get_QuaternionVariables_m1625098601(L_66, /*hidden argument*/NULL);
		V_48 = L_67;
		V_49 = 0;
		goto IL_014d;
	}

IL_0126:
	{
		FsmQuaternionU5BU5D_t1443435833* L_68 = V_48;
		int32_t L_69 = V_49;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, L_69);
		int32_t L_70 = L_69;
		FsmQuaternion_t3871136040 * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		V_10 = L_71;
		FsmQuaternion_t3871136040 * L_72 = V_10;
		NullCheck(L_72);
		bool L_73 = NamedVariable_get_NetworkSync_m3720647190(L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0147;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_74 = V_10;
		NullCheck(L_74);
		Quaternion_t1553702882  L_75 = FsmQuaternion_get_Value_m3393858025(L_74, /*hidden argument*/NULL);
		V_11 = L_75;
		BitStream_t239125475 * L_76 = ___stream0;
		NullCheck(L_76);
		BitStream_Serialize_m4132505111(L_76, (&V_11), /*hidden argument*/NULL);
	}

IL_0147:
	{
		int32_t L_77 = V_49;
		V_49 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_014d:
	{
		int32_t L_78 = V_49;
		FsmQuaternionU5BU5D_t1443435833* L_79 = V_48;
		NullCheck(L_79);
		if ((((int32_t)L_78) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_79)->max_length)))))))
		{
			goto IL_0126;
		}
	}
	{
		FsmVariables_t963491929 * L_80 = ___variables1;
		NullCheck(L_80);
		FsmVector3U5BU5D_t607182279* L_81 = FsmVariables_get_Vector3Variables_m3478402589(L_80, /*hidden argument*/NULL);
		V_50 = L_81;
		V_51 = 0;
		goto IL_0189;
	}

IL_0162:
	{
		FsmVector3U5BU5D_t607182279* L_82 = V_50;
		int32_t L_83 = V_51;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, L_83);
		int32_t L_84 = L_83;
		FsmVector3_t533912882 * L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		V_12 = L_85;
		FsmVector3_t533912882 * L_86 = V_12;
		NullCheck(L_86);
		bool L_87 = NamedVariable_get_NetworkSync_m3720647190(L_86, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0183;
		}
	}
	{
		FsmVector3_t533912882 * L_88 = V_12;
		NullCheck(L_88);
		Vector3_t4282066566  L_89 = FsmVector3_get_Value_m2779135117(L_88, /*hidden argument*/NULL);
		V_13 = L_89;
		BitStream_t239125475 * L_90 = ___stream0;
		NullCheck(L_90);
		BitStream_Serialize_m981417501(L_90, (&V_13), /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_91 = V_51;
		V_51 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_0189:
	{
		int32_t L_92 = V_51;
		FsmVector3U5BU5D_t607182279* L_93 = V_50;
		NullCheck(L_93);
		if ((((int32_t)L_92) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_93)->max_length)))))))
		{
			goto IL_0162;
		}
	}
	{
		FsmVariables_t963491929 * L_94 = ___variables1;
		NullCheck(L_94);
		FsmColorU5BU5D_t530285832* L_95 = FsmVariables_get_ColorVariables_m3443159095(L_94, /*hidden argument*/NULL);
		V_52 = L_95;
		V_53 = 0;
		goto IL_01f1;
	}

IL_019e:
	{
		FsmColorU5BU5D_t530285832* L_96 = V_52;
		int32_t L_97 = V_53;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		int32_t L_98 = L_97;
		FsmColor_t2131419205 * L_99 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		V_14 = L_99;
		FsmColor_t2131419205 * L_100 = V_14;
		NullCheck(L_100);
		bool L_101 = NamedVariable_get_NetworkSync_m3720647190(L_100, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_01eb;
		}
	}
	{
		FsmColor_t2131419205 * L_102 = V_14;
		NullCheck(L_102);
		Color_t4194546905  L_103 = FsmColor_get_Value_m1679829997(L_102, /*hidden argument*/NULL);
		V_15 = L_103;
		BitStream_t239125475 * L_104 = ___stream0;
		float* L_105 = (&V_15)->get_address_of_r_0();
		NullCheck(L_104);
		BitStream_Serialize_m1473984011(L_104, L_105, /*hidden argument*/NULL);
		BitStream_t239125475 * L_106 = ___stream0;
		float* L_107 = (&V_15)->get_address_of_g_1();
		NullCheck(L_106);
		BitStream_Serialize_m1473984011(L_106, L_107, /*hidden argument*/NULL);
		BitStream_t239125475 * L_108 = ___stream0;
		float* L_109 = (&V_15)->get_address_of_b_2();
		NullCheck(L_108);
		BitStream_Serialize_m1473984011(L_108, L_109, /*hidden argument*/NULL);
		BitStream_t239125475 * L_110 = ___stream0;
		float* L_111 = (&V_15)->get_address_of_a_3();
		NullCheck(L_110);
		BitStream_Serialize_m1473984011(L_110, L_111, /*hidden argument*/NULL);
	}

IL_01eb:
	{
		int32_t L_112 = V_53;
		V_53 = ((int32_t)((int32_t)L_112+(int32_t)1));
	}

IL_01f1:
	{
		int32_t L_113 = V_53;
		FsmColorU5BU5D_t530285832* L_114 = V_52;
		NullCheck(L_114);
		if ((((int32_t)L_113) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_114)->max_length)))))))
		{
			goto IL_019e;
		}
	}
	{
		FsmVariables_t963491929 * L_115 = ___variables1;
		NullCheck(L_115);
		FsmVector2U5BU5D_t120994540* L_116 = FsmVariables_get_Vector2Variables_m862200095(L_115, /*hidden argument*/NULL);
		V_54 = L_116;
		V_55 = 0;
		goto IL_023f;
	}

IL_0206:
	{
		FsmVector2U5BU5D_t120994540* L_117 = V_54;
		int32_t L_118 = V_55;
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, L_118);
		int32_t L_119 = L_118;
		FsmVector2_t533912881 * L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		V_16 = L_120;
		FsmVector2_t533912881 * L_121 = V_16;
		NullCheck(L_121);
		bool L_122 = NamedVariable_get_NetworkSync_m3720647190(L_121, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_0239;
		}
	}
	{
		FsmVector2_t533912881 * L_123 = V_16;
		NullCheck(L_123);
		Vector2_t4282066565  L_124 = FsmVector2_get_Value_m1313754285(L_123, /*hidden argument*/NULL);
		V_17 = L_124;
		BitStream_t239125475 * L_125 = ___stream0;
		float* L_126 = (&V_17)->get_address_of_x_1();
		NullCheck(L_125);
		BitStream_Serialize_m1473984011(L_125, L_126, /*hidden argument*/NULL);
		BitStream_t239125475 * L_127 = ___stream0;
		float* L_128 = (&V_17)->get_address_of_y_2();
		NullCheck(L_127);
		BitStream_Serialize_m1473984011(L_127, L_128, /*hidden argument*/NULL);
	}

IL_0239:
	{
		int32_t L_129 = V_55;
		V_55 = ((int32_t)((int32_t)L_129+(int32_t)1));
	}

IL_023f:
	{
		int32_t L_130 = V_55;
		FsmVector2U5BU5D_t120994540* L_131 = V_54;
		NullCheck(L_131);
		if ((((int32_t)L_130) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_131)->max_length)))))))
		{
			goto IL_0206;
		}
	}
	{
		return;
	}

IL_0248:
	{
		FsmVariables_t963491929 * L_132 = ___variables1;
		NullCheck(L_132);
		FsmStringU5BU5D_t2523845914* L_133 = FsmVariables_get_StringVariables_m3261297161(L_132, /*hidden argument*/NULL);
		V_56 = L_133;
		V_57 = 0;
		goto IL_02ad;
	}

IL_0255:
	{
		FsmStringU5BU5D_t2523845914* L_134 = V_56;
		int32_t L_135 = V_57;
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, L_135);
		int32_t L_136 = L_135;
		FsmString_t952858651 * L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		V_18 = L_137;
		FsmString_t952858651 * L_138 = V_18;
		NullCheck(L_138);
		bool L_139 = NamedVariable_get_NetworkSync_m3720647190(L_138, /*hidden argument*/NULL);
		if (!L_139)
		{
			goto IL_02a7;
		}
	}
	{
		V_19 = 0;
		BitStream_t239125475 * L_140 = ___stream0;
		NullCheck(L_140);
		BitStream_Serialize_m4204493009(L_140, (&V_19), /*hidden argument*/NULL);
		int32_t L_141 = V_19;
		V_20 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)L_141));
		V_21 = 0;
		goto IL_0293;
	}

IL_027e:
	{
		BitStream_t239125475 * L_142 = ___stream0;
		CharU5BU5D_t3324145743* L_143 = V_20;
		int32_t L_144 = V_21;
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, L_144);
		NullCheck(L_142);
		BitStream_Serialize_m2728753561(L_142, ((L_143)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_144))), /*hidden argument*/NULL);
		int32_t L_145 = V_21;
		V_21 = ((int32_t)((int32_t)L_145+(int32_t)1));
	}

IL_0293:
	{
		int32_t L_146 = V_21;
		int32_t L_147 = V_19;
		if ((((int32_t)L_146) < ((int32_t)L_147)))
		{
			goto IL_027e;
		}
	}
	{
		FsmString_t952858651 * L_148 = V_18;
		CharU5BU5D_t3324145743* L_149 = V_20;
		String_t* L_150 = String_CreateString_m578950865(NULL, L_149, /*hidden argument*/NULL);
		NullCheck(L_148);
		FsmString_set_Value_m829393196(L_148, L_150, /*hidden argument*/NULL);
	}

IL_02a7:
	{
		int32_t L_151 = V_57;
		V_57 = ((int32_t)((int32_t)L_151+(int32_t)1));
	}

IL_02ad:
	{
		int32_t L_152 = V_57;
		FsmStringU5BU5D_t2523845914* L_153 = V_56;
		NullCheck(L_153);
		if ((((int32_t)L_152) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_153)->max_length)))))))
		{
			goto IL_0255;
		}
	}
	{
		FsmVariables_t963491929 * L_154 = ___variables1;
		NullCheck(L_154);
		FsmBoolU5BU5D_t3689162173* L_155 = FsmVariables_get_BoolVariables_m4286069353(L_154, /*hidden argument*/NULL);
		V_58 = L_155;
		V_59 = 0;
		goto IL_02ec;
	}

IL_02c2:
	{
		FsmBoolU5BU5D_t3689162173* L_156 = V_58;
		int32_t L_157 = V_59;
		NullCheck(L_156);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_156, L_157);
		int32_t L_158 = L_157;
		FsmBool_t1075959796 * L_159 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		V_22 = L_159;
		FsmBool_t1075959796 * L_160 = V_22;
		NullCheck(L_160);
		bool L_161 = NamedVariable_get_NetworkSync_m3720647190(L_160, /*hidden argument*/NULL);
		if (!L_161)
		{
			goto IL_02e6;
		}
	}
	{
		V_23 = (bool)0;
		BitStream_t239125475 * L_162 = ___stream0;
		NullCheck(L_162);
		BitStream_Serialize_m2001881067(L_162, (&V_23), /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_163 = V_22;
		bool L_164 = V_23;
		NullCheck(L_163);
		FsmBool_set_Value_m1126216340(L_163, L_164, /*hidden argument*/NULL);
	}

IL_02e6:
	{
		int32_t L_165 = V_59;
		V_59 = ((int32_t)((int32_t)L_165+(int32_t)1));
	}

IL_02ec:
	{
		int32_t L_166 = V_59;
		FsmBoolU5BU5D_t3689162173* L_167 = V_58;
		NullCheck(L_167);
		if ((((int32_t)L_166) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_167)->max_length)))))))
		{
			goto IL_02c2;
		}
	}
	{
		FsmVariables_t963491929 * L_168 = ___variables1;
		NullCheck(L_168);
		FsmFloatU5BU5D_t2945380875* L_169 = FsmVariables_get_FloatVariables_m1157149957(L_168, /*hidden argument*/NULL);
		V_60 = L_169;
		V_41 = 0;
		goto IL_032f;
	}

IL_0301:
	{
		FsmFloatU5BU5D_t2945380875* L_170 = V_60;
		int32_t L_171 = V_41;
		NullCheck(L_170);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_170, L_171);
		int32_t L_172 = L_171;
		FsmFloat_t2134102846 * L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		V_24 = L_173;
		FsmFloat_t2134102846 * L_174 = V_24;
		NullCheck(L_174);
		bool L_175 = NamedVariable_get_NetworkSync_m3720647190(L_174, /*hidden argument*/NULL);
		if (!L_175)
		{
			goto IL_0329;
		}
	}
	{
		V_25 = (0.0f);
		BitStream_t239125475 * L_176 = ___stream0;
		NullCheck(L_176);
		BitStream_Serialize_m1473984011(L_176, (&V_25), /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_177 = V_24;
		float L_178 = V_25;
		NullCheck(L_177);
		FsmFloat_set_Value_m1568963140(L_177, L_178, /*hidden argument*/NULL);
	}

IL_0329:
	{
		int32_t L_179 = V_41;
		V_41 = ((int32_t)((int32_t)L_179+(int32_t)1));
	}

IL_032f:
	{
		int32_t L_180 = V_41;
		FsmFloatU5BU5D_t2945380875* L_181 = V_60;
		NullCheck(L_181);
		if ((((int32_t)L_180) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_181)->max_length)))))))
		{
			goto IL_0301;
		}
	}
	{
		FsmVariables_t963491929 * L_182 = ___variables1;
		NullCheck(L_182);
		FsmIntU5BU5D_t1976821196* L_183 = FsmVariables_get_IntVariables_m1527799903(L_182, /*hidden argument*/NULL);
		V_46 = L_183;
		V_41 = 0;
		goto IL_036e;
	}

IL_0344:
	{
		FsmIntU5BU5D_t1976821196* L_184 = V_46;
		int32_t L_185 = V_41;
		NullCheck(L_184);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_184, L_185);
		int32_t L_186 = L_185;
		FsmInt_t1596138449 * L_187 = (L_184)->GetAt(static_cast<il2cpp_array_size_t>(L_186));
		V_26 = L_187;
		FsmInt_t1596138449 * L_188 = V_26;
		NullCheck(L_188);
		bool L_189 = NamedVariable_get_NetworkSync_m3720647190(L_188, /*hidden argument*/NULL);
		if (!L_189)
		{
			goto IL_0368;
		}
	}
	{
		V_27 = 0;
		BitStream_t239125475 * L_190 = ___stream0;
		NullCheck(L_190);
		BitStream_Serialize_m4204493009(L_190, (&V_27), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_191 = V_26;
		int32_t L_192 = V_27;
		NullCheck(L_191);
		FsmInt_set_Value_m2087583461(L_191, L_192, /*hidden argument*/NULL);
	}

IL_0368:
	{
		int32_t L_193 = V_41;
		V_41 = ((int32_t)((int32_t)L_193+(int32_t)1));
	}

IL_036e:
	{
		int32_t L_194 = V_41;
		FsmIntU5BU5D_t1976821196* L_195 = V_46;
		NullCheck(L_195);
		if ((((int32_t)L_194) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_195)->max_length)))))))
		{
			goto IL_0344;
		}
	}
	{
		FsmVariables_t963491929 * L_196 = ___variables1;
		NullCheck(L_196);
		FsmQuaternionU5BU5D_t1443435833* L_197 = FsmVariables_get_QuaternionVariables_m1625098601(L_196, /*hidden argument*/NULL);
		V_48 = L_197;
		V_41 = 0;
		goto IL_03b1;
	}

IL_0383:
	{
		FsmQuaternionU5BU5D_t1443435833* L_198 = V_48;
		int32_t L_199 = V_41;
		NullCheck(L_198);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_198, L_199);
		int32_t L_200 = L_199;
		FsmQuaternion_t3871136040 * L_201 = (L_198)->GetAt(static_cast<il2cpp_array_size_t>(L_200));
		V_28 = L_201;
		FsmQuaternion_t3871136040 * L_202 = V_28;
		NullCheck(L_202);
		bool L_203 = NamedVariable_get_NetworkSync_m3720647190(L_202, /*hidden argument*/NULL);
		if (!L_203)
		{
			goto IL_03ab;
		}
	}
	{
		Quaternion_t1553702882  L_204 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_29 = L_204;
		BitStream_t239125475 * L_205 = ___stream0;
		NullCheck(L_205);
		BitStream_Serialize_m4132505111(L_205, (&V_29), /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_206 = V_28;
		Quaternion_t1553702882  L_207 = V_29;
		NullCheck(L_206);
		FsmQuaternion_set_Value_m446581172(L_206, L_207, /*hidden argument*/NULL);
	}

IL_03ab:
	{
		int32_t L_208 = V_41;
		V_41 = ((int32_t)((int32_t)L_208+(int32_t)1));
	}

IL_03b1:
	{
		int32_t L_209 = V_41;
		FsmQuaternionU5BU5D_t1443435833* L_210 = V_48;
		NullCheck(L_210);
		if ((((int32_t)L_209) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_210)->max_length)))))))
		{
			goto IL_0383;
		}
	}
	{
		FsmVariables_t963491929 * L_211 = ___variables1;
		NullCheck(L_211);
		FsmVector3U5BU5D_t607182279* L_212 = FsmVariables_get_Vector3Variables_m3478402589(L_211, /*hidden argument*/NULL);
		V_50 = L_212;
		V_41 = 0;
		goto IL_03f4;
	}

IL_03c6:
	{
		FsmVector3U5BU5D_t607182279* L_213 = V_50;
		int32_t L_214 = V_41;
		NullCheck(L_213);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_213, L_214);
		int32_t L_215 = L_214;
		FsmVector3_t533912882 * L_216 = (L_213)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		V_30 = L_216;
		FsmVector3_t533912882 * L_217 = V_30;
		NullCheck(L_217);
		bool L_218 = NamedVariable_get_NetworkSync_m3720647190(L_217, /*hidden argument*/NULL);
		if (!L_218)
		{
			goto IL_03ee;
		}
	}
	{
		Vector3_t4282066566  L_219 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_31 = L_219;
		BitStream_t239125475 * L_220 = ___stream0;
		NullCheck(L_220);
		BitStream_Serialize_m981417501(L_220, (&V_31), /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_221 = V_30;
		Vector3_t4282066566  L_222 = V_31;
		NullCheck(L_221);
		FsmVector3_set_Value_m716982822(L_221, L_222, /*hidden argument*/NULL);
	}

IL_03ee:
	{
		int32_t L_223 = V_41;
		V_41 = ((int32_t)((int32_t)L_223+(int32_t)1));
	}

IL_03f4:
	{
		int32_t L_224 = V_41;
		FsmVector3U5BU5D_t607182279* L_225 = V_50;
		NullCheck(L_225);
		if ((((int32_t)L_224) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_225)->max_length)))))))
		{
			goto IL_03c6;
		}
	}
	{
		FsmVariables_t963491929 * L_226 = ___variables1;
		NullCheck(L_226);
		FsmColorU5BU5D_t530285832* L_227 = FsmVariables_get_ColorVariables_m3443159095(L_226, /*hidden argument*/NULL);
		V_52 = L_227;
		V_41 = 0;
		goto IL_046f;
	}

IL_0409:
	{
		FsmColorU5BU5D_t530285832* L_228 = V_52;
		int32_t L_229 = V_41;
		NullCheck(L_228);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_228, L_229);
		int32_t L_230 = L_229;
		FsmColor_t2131419205 * L_231 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230));
		V_32 = L_231;
		FsmColor_t2131419205 * L_232 = V_32;
		NullCheck(L_232);
		bool L_233 = NamedVariable_get_NetworkSync_m3720647190(L_232, /*hidden argument*/NULL);
		if (!L_233)
		{
			goto IL_0469;
		}
	}
	{
		V_33 = (0.0f);
		BitStream_t239125475 * L_234 = ___stream0;
		NullCheck(L_234);
		BitStream_Serialize_m1473984011(L_234, (&V_33), /*hidden argument*/NULL);
		V_34 = (0.0f);
		BitStream_t239125475 * L_235 = ___stream0;
		NullCheck(L_235);
		BitStream_Serialize_m1473984011(L_235, (&V_34), /*hidden argument*/NULL);
		V_35 = (0.0f);
		BitStream_t239125475 * L_236 = ___stream0;
		NullCheck(L_236);
		BitStream_Serialize_m1473984011(L_236, (&V_35), /*hidden argument*/NULL);
		V_36 = (0.0f);
		BitStream_t239125475 * L_237 = ___stream0;
		NullCheck(L_237);
		BitStream_Serialize_m1473984011(L_237, (&V_36), /*hidden argument*/NULL);
		FsmColor_t2131419205 * L_238 = V_32;
		float L_239 = V_33;
		float L_240 = V_34;
		float L_241 = V_35;
		float L_242 = V_36;
		Color_t4194546905  L_243;
		memset(&L_243, 0, sizeof(L_243));
		Color__ctor_m2252924356(&L_243, L_239, L_240, L_241, L_242, /*hidden argument*/NULL);
		NullCheck(L_238);
		FsmColor_set_Value_m1684002054(L_238, L_243, /*hidden argument*/NULL);
	}

IL_0469:
	{
		int32_t L_244 = V_41;
		V_41 = ((int32_t)((int32_t)L_244+(int32_t)1));
	}

IL_046f:
	{
		int32_t L_245 = V_41;
		FsmColorU5BU5D_t530285832* L_246 = V_52;
		NullCheck(L_246);
		if ((((int32_t)L_245) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_246)->max_length)))))))
		{
			goto IL_0409;
		}
	}
	{
		FsmVariables_t963491929 * L_247 = ___variables1;
		NullCheck(L_247);
		FsmVector2U5BU5D_t120994540* L_248 = FsmVariables_get_Vector2Variables_m862200095(L_247, /*hidden argument*/NULL);
		V_54 = L_248;
		V_41 = 0;
		goto IL_04c8;
	}

IL_0484:
	{
		FsmVector2U5BU5D_t120994540* L_249 = V_54;
		int32_t L_250 = V_41;
		NullCheck(L_249);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_249, L_250);
		int32_t L_251 = L_250;
		FsmVector2_t533912881 * L_252 = (L_249)->GetAt(static_cast<il2cpp_array_size_t>(L_251));
		V_37 = L_252;
		FsmVector2_t533912881 * L_253 = V_37;
		NullCheck(L_253);
		bool L_254 = NamedVariable_get_NetworkSync_m3720647190(L_253, /*hidden argument*/NULL);
		if (!L_254)
		{
			goto IL_04c2;
		}
	}
	{
		V_38 = (0.0f);
		BitStream_t239125475 * L_255 = ___stream0;
		NullCheck(L_255);
		BitStream_Serialize_m1473984011(L_255, (&V_38), /*hidden argument*/NULL);
		V_39 = (0.0f);
		BitStream_t239125475 * L_256 = ___stream0;
		NullCheck(L_256);
		BitStream_Serialize_m1473984011(L_256, (&V_39), /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_257 = V_37;
		float L_258 = V_38;
		float L_259 = V_39;
		Vector2_t4282066565  L_260;
		memset(&L_260, 0, sizeof(L_260));
		Vector2__ctor_m1517109030(&L_260, L_258, L_259, /*hidden argument*/NULL);
		NullCheck(L_257);
		FsmVector2_set_Value_m2900659718(L_257, L_260, /*hidden argument*/NULL);
	}

IL_04c2:
	{
		int32_t L_261 = V_41;
		V_41 = ((int32_t)((int32_t)L_261+(int32_t)1));
	}

IL_04c8:
	{
		int32_t L_262 = V_41;
		FsmVector2U5BU5D_t120994540* L_263 = V_54;
		NullCheck(L_263);
		if ((((int32_t)L_262) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_263)->max_length)))))))
		{
			goto IL_0484;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnMasterServerEvent_m4092191539_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnMasterServerEvent_m4092191539 (PlayMakerFSM_t3799847376 * __this, int32_t ___masterServerEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnMasterServerEvent_m4092191539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		FsmEventData_t1076900934 * L_0 = ((Fsm_t1527112426_StaticFields*)Fsm_t1527112426_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___masterServerEvent0;
		NullCheck(L_0);
		L_0->set_MasterServerEvent_20(L_1);
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_get_MasterServerEvent_m3705414918(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C"  Fsm_t1527112426 * PlayMakerFSM_get_Fsm_m886945091 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		Fsm_set_Owner_m1411892998(L_0, __this, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		return L_1;
	}
}
// System.String PlayMakerFSM::get_FsmName()
extern "C"  String_t* PlayMakerFSM_get_FsmName_m1703792682 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m2749457024(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmName(System.String)
extern "C"  void PlayMakerFSM_set_FsmName_m2603037929 (PlayMakerFSM_t3799847376 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Name_m2790512555(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerFSM::get_FsmDescription()
extern "C"  String_t* PlayMakerFSM_get_FsmDescription_m3630583007 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Description_m3085929929(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmDescription(System.String)
extern "C"  void PlayMakerFSM_set_FsmDescription_m2232657298 (PlayMakerFSM_t3799847376 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Description_m124830864(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerFSM::get_Active()
extern "C"  bool PlayMakerFSM_get_Active_m964418526 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Active_m3073608262(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PlayMakerFSM::get_ActiveStateName()
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t PlayMakerFSM_get_ActiveStateName_m1548898773_MetadataUsageId;
extern "C"  String_t* PlayMakerFSM_get_ActiveStateName_m1548898773 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_ActiveStateName_m1548898773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmState_t2146334067 * L_1 = Fsm_get_ActiveState_m580729145(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Fsm_t1527112426 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		FsmState_t2146334067 * L_3 = Fsm_get_ActiveState_m580729145(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = FsmState_get_Name_m3930587579(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001e:
	{
		return _stringLiteral0;
	}
}
// HutongGames.PlayMaker.FsmState[] PlayMakerFSM::get_FsmStates()
extern "C"  FsmStateU5BU5D_t2644459362* PlayMakerFSM_get_FsmStates_m1358962914 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmStateU5BU5D_t2644459362* L_1 = Fsm_get_States_m3319335032(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmEvent[] PlayMakerFSM::get_FsmEvents()
extern "C"  FsmEventU5BU5D_t2862142229* PlayMakerFSM_get_FsmEvents_m1956320322 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmEventU5BU5D_t2862142229* L_1 = Fsm_get_Events_m3588867224(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmTransition[] PlayMakerFSM::get_FsmGlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t818210886* PlayMakerFSM_get_FsmGlobalTransitions_m1561155331 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmTransitionU5BU5D_t818210886* L_1 = Fsm_get_GlobalTransitions_m2969873965(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerFSM::get_FsmVariables()
extern "C"  FsmVariables_t963491929 * PlayMakerFSM_get_FsmVariables_m1986570741 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmVariables_t963491929 * L_1 = Fsm_get_Variables_m2281949087(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PlayMakerFSM::get_UsesTemplate()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_get_UsesTemplate_m2822797310_MetadataUsageId;
extern "C"  bool PlayMakerFSM_get_UsesTemplate_m2822797310 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_UsesTemplate_m2822797310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmTemplate_t1237263802 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::OnBeforeSerialize()
extern "C"  void PlayMakerFSM_OnBeforeSerialize_m3445752685 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnAfterDeserialize()
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_OnAfterDeserialize_m4203045145_MetadataUsageId;
extern "C"  void PlayMakerFSM_OnAfterDeserialize_m4203045145 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnAfterDeserialize_m4203045145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->set_NotMainThread_5((bool)1);
		bool L_0 = PlayMakerGlobals_get_Initialized_m645627118(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Fsm_t1527112426 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_InitData_m1165351113(L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->set_NotMainThread_5((bool)0);
		return;
	}
}
// System.Void PlayMakerFSM::.ctor()
extern "C"  void PlayMakerFSM__ctor_m3218653069 (PlayMakerFSM_t3799847376 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::.cctor()
extern Il2CppClass* List_1_t873065632_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m24913529_MethodInfo_var;
extern const uint32_t PlayMakerFSM__cctor_m511901120_MetadataUsageId;
extern "C"  void PlayMakerFSM__cctor_m511901120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM__cctor_m511901120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t873065632 * L_0 = (List_1_t873065632 *)il2cpp_codegen_object_new(List_1_t873065632_il2cpp_TypeInfo_var);
		List_1__ctor_m24913529(L_0, /*hidden argument*/List_1__ctor_m24913529_MethodInfo_var);
		((PlayMakerFSM_t3799847376_StaticFields*)PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var->static_fields)->set_fsmList_2(L_0);
		return;
	}
}
// System.Boolean PlayMakerFSM/<DoCoroutine>d__1::MoveNext()
extern Il2CppClass* FsmExecutionStack_t2677439066_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoCoroutineU3Ed__1_MoveNext_m3610815678_MetadataUsageId;
extern "C"  bool U3CDoCoroutineU3Ed__1_MoveNext_m3610815678 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__1_MoveNext_m3610815678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_006a;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_001e:
	{
		PlayMakerFSM_t3799847376 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		Fsm_t1527112426 * L_3 = L_2->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2677439066_il2cpp_TypeInfo_var);
		FsmExecutionStack_PushFsm_m2702729589(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Il2CppObject * L_4 = __this->get_routine_3();
		NullCheck(L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_4);
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2677439066_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m2354017614(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2677439066_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m2354017614(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_6 = __this->get_routine_3();
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
		__this->set_U3CU3E2__current_0(L_7);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0061:
	{
		__this->set_U3CU3E1__state_1((-1));
		goto IL_001e;
	}

IL_006a:
	{
		return (bool)0;
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2225328651 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m2342883224_MetadataUsageId;
extern "C"  void U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m2342883224 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m2342883224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.IDisposable.Dispose()
extern "C"  void U3CDoCoroutineU3Ed__1_System_IDisposable_Dispose_m2465789885 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_get_Current_m1954460494 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::.ctor(System.Int32)
extern "C"  void U3CDoCoroutineU3Ed__1__ctor_m2949731187 (U3CDoCoroutineU3Ed__1_t3331551771 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_Initialized()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_Initialized_m645627118_MetadataUsageId;
extern "C"  bool PlayMakerGlobals_get_Initialized_m645627118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Initialized_m645627118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_U3CInitializedU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_set_Initialized_m3462637545_MetadataUsageId;
extern "C"  void PlayMakerGlobals_set_Initialized_m3462637545 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_Initialized_m3462637545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_U3CInitializedU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_IsPlayingInEditor_m680276368_MetadataUsageId;
extern "C"  bool PlayMakerGlobals_get_IsPlayingInEditor_m680276368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlayingInEditor_m680276368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_U3CIsPlayingInEditorU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_set_IsPlayingInEditor_m4196468235_MetadataUsageId;
extern "C"  void PlayMakerGlobals_set_IsPlayingInEditor_m4196468235 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlayingInEditor_m4196468235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_U3CIsPlayingInEditorU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlaying()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_IsPlaying_m1647553534_MetadataUsageId;
extern "C"  bool PlayMakerGlobals_get_IsPlaying_m1647553534 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlaying_m1647553534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_U3CIsPlayingU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_set_IsPlaying_m1932520441_MetadataUsageId;
extern "C"  void PlayMakerGlobals_set_IsPlaying_m1932520441 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlaying_m1932520441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_U3CIsPlayingU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_IsEditor_m4210118399_MetadataUsageId;
extern "C"  bool PlayMakerGlobals_get_IsEditor_m4210118399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsEditor_m4210118399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_U3CIsEditorU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_set_IsEditor_m1626540722_MetadataUsageId;
extern "C"  void PlayMakerGlobals_set_IsEditor_m1626540722 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsEditor_m1626540722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_U3CIsEditorU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_IsBuilding_m1160993350_MetadataUsageId;
extern "C"  bool PlayMakerGlobals_get_IsBuilding_m1160993350 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsBuilding_m1160993350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_U3CIsBuildingU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsBuilding(System.Boolean)
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_set_IsBuilding_m1085677241_MetadataUsageId;
extern "C"  void PlayMakerGlobals_set_IsBuilding_m1085677241 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsBuilding_m1085677241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_U3CIsBuildingU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C"  void PlayMakerGlobals_InitApplicationFlags_m3042944910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = Application_get_isEditor_m1279348309(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		PlayMakerGlobals_set_IsPlayingInEditor_m4196468235(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = PlayMakerGlobals_get_IsBuilding_m1160993350(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		PlayMakerGlobals_set_IsPlaying_m1932520441(NULL /*static, unused*/, (bool)G_B6_0, /*hidden argument*/NULL);
		bool L_4 = Application_get_isEditor_m1279348309(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_set_IsEditor_m1626540722(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::Initialize()
extern const Il2CppType* PlayMakerGlobals_t3097244096_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVariables_t963491929_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1375417109_il2cpp_TypeInfo_var;
extern const MethodInfo* ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m907220395_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3097244096;
extern const uint32_t PlayMakerGlobals_Initialize_m3132596247_MetadataUsageId;
extern "C"  void PlayMakerGlobals_Initialize_m3132596247 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_Initialize_m3132596247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Object_t3071478659 * V_0 = NULL;
	PlayMakerGlobals_t3097244096 * V_1 = NULL;
	{
		bool L_0 = PlayMakerGlobals_get_Initialized_m645627118(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		PlayMakerGlobals_InitApplicationFlags_m3042944910(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PlayMakerGlobals_t3097244096_0_0_0_var), /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, _stringLiteral3097244096, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Object_t3071478659 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007e;
		}
	}
	{
		bool L_5 = PlayMakerGlobals_get_IsPlayingInEditor_m680276368(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		Object_t3071478659 * L_6 = V_0;
		V_1 = ((PlayMakerGlobals_t3097244096 *)CastclassClass(L_6, PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var));
		PlayMakerGlobals_t3097244096 * L_7 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312_MethodInfo_var);
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_7);
		PlayMakerGlobals_t3097244096 * L_8 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		PlayMakerGlobals_t3097244096 * L_9 = V_1;
		NullCheck(L_9);
		FsmVariables_t963491929 * L_10 = L_9->get_variables_3();
		FsmVariables_t963491929 * L_11 = (FsmVariables_t963491929 *)il2cpp_codegen_object_new(FsmVariables_t963491929_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m3281811991(L_11, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		PlayMakerGlobals_set_Variables_m1711829458(L_8, L_11, /*hidden argument*/NULL);
		PlayMakerGlobals_t3097244096 * L_12 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		PlayMakerGlobals_t3097244096 * L_13 = V_1;
		NullCheck(L_13);
		List_1_t1375417109 * L_14 = PlayMakerGlobals_get_Events_m1315403806(L_13, /*hidden argument*/NULL);
		List_1_t1375417109 * L_15 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m907220395(L_15, L_14, /*hidden argument*/List_1__ctor_m907220395_MethodInfo_var);
		NullCheck(L_12);
		PlayMakerGlobals_set_Events_m990891217(L_12, L_15, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0071:
	{
		Object_t3071478659 * L_16 = V_0;
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_instance_2(((PlayMakerGlobals_t3097244096 *)IsInstClass(L_16, PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var)));
		goto IL_0088;
	}

IL_007e:
	{
		PlayMakerGlobals_t3097244096 * L_17 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t3097244096_m1795104312_MethodInfo_var);
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_17);
	}

IL_0088:
	{
		PlayMakerGlobals_set_Initialized_m3462637545(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_Instance_m3643716518_MetadataUsageId;
extern "C"  PlayMakerGlobals_t3097244096 * PlayMakerGlobals_get_Instance_m3643716518 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Instance_m3643716518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m3132596247(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_t3097244096 * L_0 = ((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::ResetInstance()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_ResetInstance_m173162943_MetadataUsageId;
extern "C"  void PlayMakerGlobals_ResetInstance_m173162943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_ResetInstance_m173162943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayMakerGlobals_t3097244096 *)NULL);
		return;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::get_Variables()
extern "C"  FsmVariables_t963491929 * PlayMakerGlobals_get_Variables_m733169483 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method)
{
	{
		FsmVariables_t963491929 * L_0 = __this->get_variables_3();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerGlobals_set_Variables_m1711829458 (PlayMakerGlobals_t3097244096 * __this, FsmVariables_t963491929 * ___value0, const MethodInfo* method)
{
	{
		FsmVariables_t963491929 * L_0 = ___value0;
		__this->set_variables_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C"  List_1_t1375417109 * PlayMakerGlobals_get_Events_m1315403806 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method)
{
	{
		List_1_t1375417109 * L_0 = __this->get_events_4();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayMakerGlobals_set_Events_m990891217 (PlayMakerGlobals_t3097244096 * __this, List_1_t1375417109 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1375417109 * L_0 = ___value0;
		__this->set_events_4(L_0);
		return;
	}
}
// HutongGames.PlayMaker.FsmEvent PlayMakerGlobals::AddEvent(System.String)
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4975193_MethodInfo_var;
extern const uint32_t PlayMakerGlobals_AddEvent_m1265974759_MetadataUsageId;
extern "C"  FsmEvent_t2133468028 * PlayMakerGlobals_AddEvent_m1265974759 (PlayMakerGlobals_t3097244096 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_AddEvent_m1265974759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmEvent_t2133468028 * V_0 = NULL;
	FsmEvent_t2133468028 * G_B2_0 = NULL;
	FsmEvent_t2133468028 * G_B1_0 = NULL;
	{
		List_1_t1375417109 * L_0 = __this->get_events_4();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		List_1_Add_m4975193(L_0, L_1, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		String_t* L_2 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_3 = FsmEvent_FindEvent_m3809574375(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = L_3;
		G_B1_0 = L_4;
		if (L_4)
		{
			G_B2_0 = L_4;
			goto IL_001c;
		}
	}
	{
		String_t* L_5 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_6 = FsmEvent_GetFsmEvent_m2820038136(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		G_B2_0 = L_6;
	}

IL_001c:
	{
		V_0 = G_B2_0;
		FsmEvent_t2133468028 * L_7 = V_0;
		NullCheck(L_7);
		FsmEvent_set_IsGlobal_m470108142(L_7, (bool)1, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_8 = V_0;
		return L_8;
	}
}
// System.Void PlayMakerGlobals::OnEnable()
extern "C"  void PlayMakerGlobals_OnEnable_m201299401 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGlobals::OnDestroy()
extern Il2CppClass* PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_OnDestroy_m268816150_MetadataUsageId;
extern "C"  void PlayMakerGlobals_OnDestroy_m268816150 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_OnDestroy_m268816150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerGlobals_set_Initialized_m3462637545(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		((PlayMakerGlobals_t3097244096_StaticFields*)PlayMakerGlobals_t3097244096_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayMakerGlobals_t3097244096 *)NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::.ctor()
extern Il2CppClass* FsmVariables_t963491929_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1375417109_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t PlayMakerGlobals__ctor_m1305972893_MetadataUsageId;
extern "C"  void PlayMakerGlobals__ctor_m1305972893 (PlayMakerGlobals_t3097244096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals__ctor_m1305972893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmVariables_t963491929 * L_0 = (FsmVariables_t963491929 *)il2cpp_codegen_object_new(FsmVariables_t963491929_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m3149252762(L_0, /*hidden argument*/NULL);
		__this->set_variables_3(L_0);
		List_1_t1375417109 * L_1 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_1, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_events_4(L_1);
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern const Il2CppType* PlayMakerGUI_t3799848395_0_0_0_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_EnableStateLabels_m369831644_MetadataUsageId;
extern "C"  bool PlayMakerGUI_get_EnableStateLabels_m369831644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_EnableStateLabels_m369831644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PlayMakerGUI_t3799848395_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17(((PlayMakerGUI_t3799848395 *)CastclassClass(L_3, PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var)));
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_4 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_6 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		NullCheck(L_6);
		bool L_7 = L_6->get_drawStateLabels_8();
		return L_7;
	}

IL_003e:
	{
		return (bool)0;
	}
}
// System.Void PlayMakerGUI::set_EnableStateLabels(System.Boolean)
extern const Il2CppType* PlayMakerGUI_t3799848395_0_0_0_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_EnableStateLabels_m2134116951_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_EnableStateLabels_m2134116951 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_EnableStateLabels_m2134116951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PlayMakerGUI_t3799848395_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17(((PlayMakerGUI_t3799848395 *)CastclassClass(L_3, PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var)));
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_4 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_6 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		bool L_7 = ___value0;
		NullCheck(L_6);
		L_6->set_drawStateLabels_8(L_7);
	}

IL_003e:
	{
		return;
	}
}
// PlayMakerGUI PlayMakerGUI::get_Instance()
extern const Il2CppType* PlayMakerGUI_t3799848395_0_0_0_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayMakerGUI_t3799848395_m3208523333_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3799848395;
extern const uint32_t PlayMakerGUI_get_Instance_m1734509702_MetadataUsageId;
extern "C"  PlayMakerGUI_t3799848395 * PlayMakerGUI_get_Instance_m1734509702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Instance_m1734509702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PlayMakerGUI_t3799848395_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17(((PlayMakerGUI_t3799848395 *)CastclassClass(L_3, PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var)));
		PlayMakerGUI_t3799848395 * L_4 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		GameObject_t3674682005 * L_6 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m3920833606(L_6, _stringLiteral3799848395, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t3674682005 * L_7 = V_0;
		NullCheck(L_7);
		PlayMakerGUI_t3799848395 * L_8 = GameObject_AddComponent_TisPlayMakerGUI_t3799848395_m3208523333(L_7, /*hidden argument*/GameObject_AddComponent_TisPlayMakerGUI_t3799848395_m3208523333_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17(L_8);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_9 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		return L_9;
	}
}
// System.Boolean PlayMakerGUI::get_Enabled()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_Enabled_m2747798576_MetadataUsageId;
extern "C"  bool PlayMakerGUI_get_Enabled_m2747798576 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Enabled_m2747798576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_2 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m1239363704(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_GUISkin_m2871771085_MetadataUsageId;
extern "C"  GUISkin_t3371348110 * PlayMakerGUI_get_GUISkin_m2871771085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUISkin_m2871771085_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUISkin_t3371348110 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_guiSkin_18();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUISkin(UnityEngine.GUISkin)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_GUISkin_m2923764070_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_GUISkin_m2923764070 (Il2CppObject * __this /* static, unused */, GUISkin_t3371348110 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUISkin_m2923764070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t3371348110 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiSkin_18(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_GUIColor_m3696729802_MetadataUsageId;
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIColor_m3696729802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIColor_m3696729802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Color_t4194546905  L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_guiColor_19();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIColor(UnityEngine.Color)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_GUIColor_m2497813007_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_GUIColor_m2497813007 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIColor_m2497813007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiColor_19(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_GUIBackgroundColor_m579738172_MetadataUsageId;
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIBackgroundColor_m579738172 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIBackgroundColor_m579738172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Color_t4194546905  L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_guiBackgroundColor_20();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIBackgroundColor(UnityEngine.Color)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_GUIBackgroundColor_m4250287389_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_GUIBackgroundColor_m4250287389 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIBackgroundColor_m4250287389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiBackgroundColor_20(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_GUIContentColor_m4207416069_MetadataUsageId;
extern "C"  Color_t4194546905  PlayMakerGUI_get_GUIContentColor_m4207416069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIContentColor_m4207416069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Color_t4194546905  L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_guiContentColor_21();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIContentColor(UnityEngine.Color)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_GUIContentColor_m3079522542_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_GUIContentColor_m3079522542 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIContentColor_m3079522542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiContentColor_21(L_0);
		return;
	}
}
// UnityEngine.Matrix4x4 PlayMakerGUI::get_GUIMatrix()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_GUIMatrix_m643020872_MetadataUsageId;
extern "C"  Matrix4x4_t1651859333  PlayMakerGUI_get_GUIMatrix_m643020872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIMatrix_m643020872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Matrix4x4_t1651859333  L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_guiMatrix_22();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_GUIMatrix_m925123147_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_GUIMatrix_m925123147 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1651859333  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIMatrix_m925123147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Matrix4x4_t1651859333  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiMatrix_22(L_0);
		return;
	}
}
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_MouseCursor_m3692364755_MetadataUsageId;
extern "C"  Texture_t2526458961 * PlayMakerGUI_get_MouseCursor_m3692364755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_MouseCursor_m3692364755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Texture_t2526458961 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_U3CMouseCursorU3Ek__BackingField_25();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_MouseCursor(UnityEngine.Texture)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_MouseCursor_m346590176_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_MouseCursor_m346590176 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_MouseCursor_m346590176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2526458961 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_U3CMouseCursorU3Ek__BackingField_25(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_LockCursor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_LockCursor_m563741236_MetadataUsageId;
extern "C"  bool PlayMakerGUI_get_LockCursor_m563741236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_LockCursor_m563741236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_U3CLockCursorU3Ek__BackingField_26();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_LockCursor(System.Boolean)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_LockCursor_m2221249063_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_LockCursor_m2221249063 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_LockCursor_m2221249063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_U3CLockCursorU3Ek__BackingField_26(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_HideCursor()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_HideCursor_m2495307_MetadataUsageId;
extern "C"  bool PlayMakerGUI_get_HideCursor_m2495307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_HideCursor_m2495307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_U3CHideCursorU3Ek__BackingField_27();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_HideCursor(System.Boolean)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_set_HideCursor_m2729864958_MetadataUsageId;
extern "C"  void PlayMakerGUI_set_HideCursor_m2729864958 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_HideCursor_m2729864958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_U3CHideCursorU3Ek__BackingField_27(L_0);
		return;
	}
}
// System.Void PlayMakerGUI::InitLabelStyle()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* RectOffset_t3056157787_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_InitLabelStyle_m3613734111_MetadataUsageId;
extern "C"  void PlayMakerGUI_InitLabelStyle_m3613734111 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_InitLabelStyle_m3613734111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t2990928826 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_2 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Texture2D_t3884108195 * L_3 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_3, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_stateLabelBackground_24(L_3);
		Texture2D_t3884108195 * L_4 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_24();
		Color_t4194546905  L_5 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Texture2D_SetPixel_m378278602(L_4, 0, 0, L_5, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_6 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_24();
		NullCheck(L_6);
		Texture2D_Apply_m1364130776(L_6, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_7 = (GUIStyle_t2990928826 *)il2cpp_codegen_object_new(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m478034167(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		GUIStyle_t2990928826 * L_8 = V_0;
		NullCheck(L_8);
		GUIStyleState_t1997423985 * L_9 = GUIStyle_get_normal_m42729964(L_8, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_10 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_24();
		NullCheck(L_9);
		GUIStyleState_set_background_m3314627691(L_9, L_10, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_11 = V_0;
		NullCheck(L_11);
		GUIStyleState_t1997423985 * L_12 = GUIStyle_get_normal_m42729964(L_11, /*hidden argument*/NULL);
		Color_t4194546905  L_13 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyleState_set_textColor_m3058467057(L_12, L_13, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_14 = V_0;
		NullCheck(L_14);
		GUIStyle_set_fontSize_m3621764235(L_14, ((int32_t)10), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_15 = V_0;
		NullCheck(L_15);
		GUIStyle_set_alignment_m4252900834(L_15, 3, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_16 = V_0;
		RectOffset_t3056157787 * L_17 = (RectOffset_t3056157787 *)il2cpp_codegen_object_new(RectOffset_t3056157787_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2631865360(L_17, 4, 4, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_set_padding_m458170651(L_16, L_17, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_18 = V_0;
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_stateLabelStyle_23(L_18);
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabels()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t2516208563_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m1726014116_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m977624102_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4014241641_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2577869007_MethodInfo_var;
extern const MethodInfo* PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m313350148_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m786420061_MethodInfo_var;
extern const uint32_t PlayMakerGUI_DrawStateLabels_m3093789020_MetadataUsageId;
extern "C"  void PlayMakerGUI_DrawStateLabels_m3093789020 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabels_m3093789020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	int32_t V_2 = 0;
	PlayMakerFSM_t3799847376 * V_3 = NULL;
	List_1_t873065632 * G_B7_0 = NULL;
	List_1_t873065632 * G_B6_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_14();
		NullCheck(L_0);
		List_1_Clear_m1726014116(L_0, /*hidden argument*/List_1_Clear_m1726014116_MethodInfo_var);
		V_0 = 0;
		goto IL_0031;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_1 = PlayMakerFSM_get_FsmList_m1444651517(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		PlayMakerFSM_t3799847376 * L_3 = List_1_get_Item_m977624102(L_1, L_2, /*hidden argument*/List_1_get_Item_m977624102_MethodInfo_var);
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_6 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_14();
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m4014241641(L_6, L_7, /*hidden argument*/List_1_Add_m4014241641_MethodInfo_var);
	}

IL_002d:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_10 = PlayMakerFSM_get_FsmList_m1444651517(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m2577869007(L_10, /*hidden argument*/List_1_get_Count_m2577869007_MethodInfo_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_12 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_14();
		Comparison_1_t2516208563 * L_13 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28();
		G_B6_0 = L_12;
		if (L_13)
		{
			G_B7_0 = L_12;
			goto IL_005b;
		}
	}
	{
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315_MethodInfo_var);
		Comparison_1_t2516208563 * L_15 = (Comparison_1_t2516208563 *)il2cpp_codegen_object_new(Comparison_1_t2516208563_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m313350148(L_15, NULL, L_14, /*hidden argument*/Comparison_1__ctor_m313350148_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28(L_15);
		G_B7_0 = G_B6_0;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Comparison_1_t2516208563 * L_16 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_28();
		NullCheck(G_B7_0);
		List_1_Sort_m786420061(G_B7_0, L_16, /*hidden argument*/List_1_Sort_m786420061_MethodInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_labelGameObject_15((GameObject_t3674682005 *)NULL);
		V_2 = 0;
		goto IL_0093;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_17 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_14();
		int32_t L_18 = V_2;
		NullCheck(L_17);
		PlayMakerFSM_t3799847376 * L_19 = List_1_get_Item_m977624102(L_17, L_18, /*hidden argument*/List_1_get_Item_m977624102_MethodInfo_var);
		V_3 = L_19;
		PlayMakerFSM_t3799847376 * L_20 = V_3;
		NullCheck(L_20);
		Fsm_t1527112426 * L_21 = PlayMakerFSM_get_Fsm_m886945091(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		bool L_22 = Fsm_get_ShowStateLabel_m1575907712(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_008f;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_23 = V_3;
		PlayMakerGUI_DrawStateLabel_m1299770249(__this, L_23, /*hidden argument*/NULL);
	}

IL_008f:
	{
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0093:
	{
		int32_t L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_26 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_14();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m2577869007(L_26, /*hidden argument*/List_1_get_Count_m2577869007_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_006f;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t4282066565_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_DrawStateLabel_m1299770249_MetadataUsageId;
extern "C"  void PlayMakerGUI_DrawStateLabel_m1299770249 (PlayMakerGUI_t3799848395 * __this, PlayMakerFSM_t3799847376 * ___fsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabel_m1299770249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t4194546905  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Color_t4194546905  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Rect_t4241904616  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Rect_t4241904616  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_23();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitLabelStyle_m3613734111(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		Camera_t2727095145 * L_1 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		PlayMakerFSM_t3799847376 * L_3 = ___fsm0;
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		Camera_t2727095145 * L_5 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		PlayMakerFSM_t3799847376 * L_7 = ___fsm0;
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GameObject_t3674682005 * L_9 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_labelGameObject_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		float L_11 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmLabelIndex_16();
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_fsmLabelIndex_16(((float)((float)L_11+(float)(1.0f))));
		goto IL_0066;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_fsmLabelIndex_16((0.0f));
		PlayMakerFSM_t3799847376 * L_12 = ___fsm0;
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = Component_get_gameObject_m1170635899(L_12, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_labelGameObject_15(L_13);
	}

IL_0066:
	{
		PlayMakerFSM_t3799847376 * L_14 = ___fsm0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		String_t* L_15 = PlayMakerGUI_GenerateStateLabel_m3664809373(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0076;
		}
	}
	{
		return;
	}

IL_0076:
	{
		Initobj (Vector2_t4282066565_il2cpp_TypeInfo_var, (&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_18 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_labelContent_5();
		String_t* L_19 = V_0;
		NullCheck(L_18);
		GUIContent_set_text_m1575840163(L_18, L_19, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_20 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_23();
		GUIContent_t2094828418 * L_21 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_labelContent_5();
		NullCheck(L_20);
		Vector2_t4282066565  L_22 = GUIStyle_CalcSize_m2299809257(L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		float L_23 = (&V_2)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_23, (10.0f), (200.0f), /*hidden argument*/NULL);
		(&V_2)->set_x_1(L_24);
		bool L_25 = __this->get_GUITextureStateLabels_9();
		if (!L_25)
		{
			goto IL_0144;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_26 = ___fsm0;
		NullCheck(L_26);
		GUITexture_t4020448292 * L_27 = PlayMakerFSM_get_GuiTexture_m219309322(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_27, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0144;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_29 = ___fsm0;
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = Component_get_gameObject_m1170635899(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_t1659122786 * L_31 = GameObject_get_transform_m1278640159(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t4282066566  L_32 = Transform_get_position_m2211398607(L_31, /*hidden argument*/NULL);
		float L_33 = L_32.get_x_1();
		int32_t L_34 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_35 = ___fsm0;
		NullCheck(L_35);
		GUITexture_t4020448292 * L_36 = PlayMakerFSM_get_GuiTexture_m219309322(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Rect_t4241904616  L_37 = GUITexture_get_pixelInset_m2745549995(L_36, /*hidden argument*/NULL);
		V_9 = L_37;
		float L_38 = Rect_get_x_m982385354((&V_9), /*hidden argument*/NULL);
		(&V_1)->set_x_1(((float)((float)((float)((float)L_33*(float)(((float)((float)L_34)))))+(float)L_38)));
		PlayMakerFSM_t3799847376 * L_39 = ___fsm0;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_t1659122786 * L_41 = GameObject_get_transform_m1278640159(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t4282066566  L_42 = Transform_get_position_m2211398607(L_41, /*hidden argument*/NULL);
		float L_43 = L_42.get_y_2();
		int32_t L_44 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_45 = ___fsm0;
		NullCheck(L_45);
		GUITexture_t4020448292 * L_46 = PlayMakerFSM_get_GuiTexture_m219309322(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Rect_t4241904616  L_47 = GUITexture_get_pixelInset_m2745549995(L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		float L_48 = Rect_get_y_m982386315((&V_10), /*hidden argument*/NULL);
		(&V_1)->set_y_2(((float)((float)((float)((float)L_43*(float)(((float)((float)L_44)))))+(float)L_48)));
		goto IL_0238;
	}

IL_0144:
	{
		bool L_49 = __this->get_GUITextStateLabels_10();
		if (!L_49)
		{
			goto IL_01a5;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_50 = ___fsm0;
		NullCheck(L_50);
		GUIText_t3371372606 * L_51 = PlayMakerFSM_get_GuiText_m1922746504(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_51, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01a5;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_53 = ___fsm0;
		NullCheck(L_53);
		GameObject_t3674682005 * L_54 = Component_get_gameObject_m1170635899(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t1659122786 * L_55 = GameObject_get_transform_m1278640159(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Vector3_t4282066566  L_56 = Transform_get_position_m2211398607(L_55, /*hidden argument*/NULL);
		float L_57 = L_56.get_x_1();
		int32_t L_58 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_x_1(((float)((float)L_57*(float)(((float)((float)L_58))))));
		PlayMakerFSM_t3799847376 * L_59 = ___fsm0;
		NullCheck(L_59);
		GameObject_t3674682005 * L_60 = Component_get_gameObject_m1170635899(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_t1659122786 * L_61 = GameObject_get_transform_m1278640159(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t4282066566  L_62 = Transform_get_position_m2211398607(L_61, /*hidden argument*/NULL);
		float L_63 = L_62.get_y_2();
		int32_t L_64 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_y_2(((float)((float)L_63*(float)(((float)((float)L_64))))));
		goto IL_0238;
	}

IL_01a5:
	{
		bool L_65 = __this->get_filterLabelsWithDistance_11();
		if (!L_65)
		{
			goto IL_01d7;
		}
	}
	{
		Camera_t2727095145 * L_66 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_t1659122786 * L_67 = Component_get_transform_m4257140443(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t4282066566  L_68 = Transform_get_position_m2211398607(L_67, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_69 = ___fsm0;
		NullCheck(L_69);
		Transform_t1659122786 * L_70 = Component_get_transform_m4257140443(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Vector3_t4282066566  L_71 = Transform_get_position_m2211398607(L_70, /*hidden argument*/NULL);
		float L_72 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_68, L_71, /*hidden argument*/NULL);
		V_3 = L_72;
		float L_73 = V_3;
		float L_74 = __this->get_maxLabelDistance_12();
		if ((!(((float)L_73) > ((float)L_74))))
		{
			goto IL_01d7;
		}
	}
	{
		return;
	}

IL_01d7:
	{
		Camera_t2727095145 * L_75 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_75);
		Transform_t1659122786 * L_76 = Component_get_transform_m4257140443(L_75, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_77 = ___fsm0;
		NullCheck(L_77);
		Transform_t1659122786 * L_78 = Component_get_transform_m4257140443(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t4282066566  L_79 = Transform_get_position_m2211398607(L_78, /*hidden argument*/NULL);
		NullCheck(L_76);
		Vector3_t4282066566  L_80 = Transform_InverseTransformPoint_m1626812000(L_76, L_79, /*hidden argument*/NULL);
		V_4 = L_80;
		float L_81 = (&V_4)->get_z_3();
		if ((!(((float)L_81) <= ((float)(0.0f)))))
		{
			goto IL_0202;
		}
	}
	{
		return;
	}

IL_0202:
	{
		Camera_t2727095145 * L_82 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_83 = ___fsm0;
		NullCheck(L_83);
		Transform_t1659122786 * L_84 = Component_get_transform_m4257140443(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		Vector3_t4282066566  L_85 = Transform_get_position_m2211398607(L_84, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t4282066566  L_86 = Camera_WorldToScreenPoint_m2400233676(L_82, L_85, /*hidden argument*/NULL);
		Vector2_t4282066565  L_87 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		V_1 = L_87;
		Vector2_t4282066565 * L_88 = (&V_1);
		float L_89 = L_88->get_x_1();
		float L_90 = (&V_2)->get_x_1();
		L_88->set_x_1(((float)((float)L_89-(float)((float)((float)L_90*(float)(0.5f))))));
	}

IL_0238:
	{
		int32_t L_91 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_92 = (&V_1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		float L_93 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmLabelIndex_16();
		(&V_1)->set_y_2(((float)((float)((float)((float)(((float)((float)L_91)))-(float)L_92))-(float)((float)((float)L_93*(float)(15.0f))))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Color_t4194546905  L_94 = GUI_get_backgroundColor_m542505775(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_94;
		Color_t4194546905  L_95 = GUI_get_color_m1489208189(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_95;
		V_7 = 0;
		PlayMakerFSM_t3799847376 * L_96 = ___fsm0;
		NullCheck(L_96);
		Fsm_t1527112426 * L_97 = PlayMakerFSM_get_Fsm_m886945091(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		FsmState_t2146334067 * L_98 = Fsm_get_ActiveState_m580729145(L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_0289;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_99 = ___fsm0;
		NullCheck(L_99);
		Fsm_t1527112426 * L_100 = PlayMakerFSM_get_Fsm_m886945091(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		FsmState_t2146334067 * L_101 = Fsm_get_ActiveState_m580729145(L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		int32_t L_102 = FsmState_get_ColorIndex_m1798534990(L_101, /*hidden argument*/NULL);
		V_7 = L_102;
	}

IL_0289:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_103 = PlayMakerPrefs_get_Colors_m2303259045(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_104 = V_7;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		V_8 = (*(Color_t4194546905 *)((L_103)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_104))));
		float L_105 = (&V_8)->get_r_0();
		float L_106 = (&V_8)->get_g_1();
		float L_107 = (&V_8)->get_b_2();
		Color_t4194546905  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Color__ctor_m2252924356(&L_108, L_105, L_106, L_107, (0.5f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m885419314(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		Color_t4194546905  L_109 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		float L_110 = (&V_1)->get_x_1();
		float L_111 = (&V_1)->get_y_2();
		float L_112 = (&V_2)->get_x_1();
		float L_113 = (&V_2)->get_y_2();
		Rect_t4241904616  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Rect__ctor_m3291325233(&L_114, L_110, L_111, L_112, L_113, /*hidden argument*/NULL);
		String_t* L_115 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_116 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_23();
		GUI_Label_m4283747336(NULL /*static, unused*/, L_114, L_115, L_116, /*hidden argument*/NULL);
		Color_t4194546905  L_117 = V_5;
		GUI_set_backgroundColor_m885419314(NULL /*static, unused*/, L_117, /*hidden argument*/NULL);
		Color_t4194546905  L_118 = V_6;
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern Il2CppCodeGenString* _stringLiteral1892965222;
extern const uint32_t PlayMakerGUI_GenerateStateLabel_m3664809373_MetadataUsageId;
extern "C"  String_t* PlayMakerGUI_GenerateStateLabel_m3664809373 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___fsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_GenerateStateLabel_m3664809373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = ___fsm0;
		NullCheck(L_0);
		Fsm_t1527112426 * L_1 = PlayMakerFSM_get_Fsm_m886945091(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmState_t2146334067 * L_2 = Fsm_get_ActiveState_m580729145(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return _stringLiteral1892965222;
	}

IL_0013:
	{
		PlayMakerFSM_t3799847376 * L_3 = ___fsm0;
		NullCheck(L_3);
		Fsm_t1527112426 * L_4 = PlayMakerFSM_get_Fsm_m886945091(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmState_t2146334067 * L_5 = Fsm_get_ActiveState_m580729145(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = FsmState_get_Name_m3930587579(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PlayMakerGUI::Awake()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1965092665;
extern const uint32_t PlayMakerGUI_Awake_m777492309_MetadataUsageId;
extern "C"  void PlayMakerGUI_Awake_m777492309 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_Awake_m777492309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17(__this);
		return;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral1965092665, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGUI::OnEnable()
extern "C"  void PlayMakerGUI_OnEnable_m1195353172 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnGUI()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m1726014116_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m221812961_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m977624102_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1639762222_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2874272263_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2577869007_MethodInfo_var;
extern const uint32_t PlayMakerGUI_OnGUI_m35285740_MetadataUsageId;
extern "C"  void PlayMakerGUI_OnGUI_m35285740 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnGUI_m35285740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	int32_t V_2 = 0;
	Fsm_t1527112426 * V_3 = NULL;
	Matrix4x4_t1651859333  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B26_0 = 0;
	{
		bool L_0 = __this->get_enableGUILayout_7();
		MonoBehaviour_set_useGUILayout_m589898551(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUISkin_t3371348110 * L_1 = PlayMakerGUI_get_GUISkin_m2871771085(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		GUISkin_t3371348110 * L_3 = PlayMakerGUI_get_GUISkin_m2871771085(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_skin_m1213959601(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Color_t4194546905  L_4 = PlayMakerGUI_get_GUIColor_m3696729802(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_color_m2304110692(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Color_t4194546905  L_5 = PlayMakerGUI_get_GUIBackgroundColor_m579738172(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_backgroundColor_m885419314(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Color_t4194546905  L_6 = PlayMakerGUI_get_GUIContentColor_m4207416069(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_previewOnGUI_6();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		bool L_8 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_DoEditGUI_m3828862422(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_9 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		NullCheck(L_9);
		List_1_Clear_m1726014116(L_9, /*hidden argument*/List_1_Clear_m1726014116_MethodInfo_var);
		List_1_t873065632 * L_10 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t3799847376_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_11 = PlayMakerFSM_get_FsmList_m1444651517(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_AddRange_m221812961(L_10, L_11, /*hidden argument*/List_1_AddRange_m221812961_MethodInfo_var);
		V_0 = 0;
		goto IL_00ee;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_12 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PlayMakerFSM_t3799847376 * L_14 = List_1_get_Item_m977624102(L_12, L_13, /*hidden argument*/List_1_get_Item_m977624102_MethodInfo_var);
		V_1 = L_14;
		PlayMakerFSM_t3799847376 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_15, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_17 = V_1;
		NullCheck(L_17);
		bool L_18 = PlayMakerFSM_get_Active_m964418526(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_19 = V_1;
		NullCheck(L_19);
		Fsm_t1527112426 * L_20 = PlayMakerFSM_get_Fsm_m886945091(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmState_t2146334067 * L_21 = Fsm_get_ActiveState_m580729145(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_22 = V_1;
		NullCheck(L_22);
		Fsm_t1527112426 * L_23 = PlayMakerFSM_get_Fsm_m886945091(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = Fsm_get_HandleOnGUI_m3061956918(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_25 = V_1;
		NullCheck(L_25);
		Fsm_t1527112426 * L_26 = PlayMakerFSM_get_Fsm_m886945091(L_25, /*hidden argument*/NULL);
		PlayMakerGUI_CallOnGUI_m575607312(__this, L_26, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00d7;
	}

IL_00ba:
	{
		PlayMakerFSM_t3799847376 * L_27 = V_1;
		NullCheck(L_27);
		Fsm_t1527112426 * L_28 = PlayMakerFSM_get_Fsm_m886945091(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		List_1_t2895297978 * L_29 = Fsm_get_SubFsmList_m2986382845(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_2;
		NullCheck(L_29);
		Fsm_t1527112426 * L_31 = List_1_get_Item_m1639762222(L_29, L_30, /*hidden argument*/List_1_get_Item_m1639762222_MethodInfo_var);
		V_3 = L_31;
		Fsm_t1527112426 * L_32 = V_3;
		PlayMakerGUI_CallOnGUI_m575607312(__this, L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_34 = V_2;
		PlayMakerFSM_t3799847376 * L_35 = V_1;
		NullCheck(L_35);
		Fsm_t1527112426 * L_36 = PlayMakerFSM_get_Fsm_m886945091(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		List_1_t2895297978 * L_37 = Fsm_get_SubFsmList_m2986382845(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m2874272263(L_37, /*hidden argument*/List_1_get_Count_m2874272263_MethodInfo_var);
		if ((((int32_t)L_34) < ((int32_t)L_38)))
		{
			goto IL_00ba;
		}
	}

IL_00ea:
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		List_1_t873065632 * L_41 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m2577869007(L_41, /*hidden argument*/List_1_get_Count_m2577869007_MethodInfo_var);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_0073;
		}
	}
	{
		bool L_43 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01e9;
		}
	}
	{
		Event_t4196595728 * L_44 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = Event_get_type_m2209939250(L_44, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)7))))
		{
			goto IL_01e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		Matrix4x4_t1651859333  L_46 = GUI_get_matrix_m4136533621(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_46;
		Matrix4x4_t1651859333  L_47 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_matrix_m1023514902(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Texture_t2526458961 * L_48 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_48, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_019e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_50 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_51 = L_50.get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Texture_t2526458961 * L_52 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_52);
		int32_t L_54 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_55 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_56 = L_55.get_y_2();
		Texture_t2526458961 * L_57 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_57);
		Texture_t2526458961 * L_59 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_59);
		Texture_t2526458961 * L_61 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_61);
		int32_t L_62 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_61);
		Rect__ctor_m3291325233((&V_5), ((float)((float)L_51-(float)((float)((float)(((float)((float)L_53)))*(float)(0.5f))))), ((float)((float)((float)((float)(((float)((float)L_54)))-(float)L_56))-(float)((float)((float)(((float)((float)L_58)))*(float)(0.5f))))), (((float)((float)L_60))), (((float)((float)L_62))), /*hidden argument*/NULL);
		Rect_t4241904616  L_63 = V_5;
		Texture_t2526458961 * L_64 = PlayMakerGUI_get_MouseCursor_m3692364755(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m418809280(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
	}

IL_019e:
	{
		bool L_65 = __this->get_drawStateLabels_8();
		if (!L_65)
		{
			goto IL_01b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		bool L_66 = PlayMakerGUI_get_EnableStateLabels_m369831644(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01b3;
		}
	}
	{
		PlayMakerGUI_DrawStateLabels_m3093789020(__this, /*hidden argument*/NULL);
	}

IL_01b3:
	{
		Matrix4x4_t1651859333  L_67 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_matrix_m1023514902(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		Matrix4x4_t1651859333  L_68 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIMatrix_m925123147(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		bool L_69 = __this->get_controlMouseCursor_13();
		if (!L_69)
		{
			goto IL_01e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		bool L_70 = PlayMakerGUI_get_LockCursor_m563741236(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_01d6;
		}
	}
	{
		G_B26_0 = 0;
		goto IL_01d7;
	}

IL_01d6:
	{
		G_B26_0 = 1;
	}

IL_01d7:
	{
		Cursor_set_lockState_m3065915939(NULL /*static, unused*/, G_B26_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		bool L_71 = PlayMakerGUI_get_HideCursor_m2495307(NULL /*static, unused*/, /*hidden argument*/NULL);
		Cursor_set_visible_m4101409761(NULL /*static, unused*/, (bool)((((int32_t)L_71) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01e9:
	{
		return;
	}
}
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C"  void PlayMakerGUI_CallOnGUI_m575607312 (PlayMakerGUI_t3799848395 * __this, Fsm_t1527112426 * ___fsm0, const MethodInfo* method)
{
	FsmStateActionU5BU5D_t2476090292* V_0 = NULL;
	FsmStateAction_t2366529033 * V_1 = NULL;
	FsmStateActionU5BU5D_t2476090292* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Fsm_t1527112426 * L_0 = ___fsm0;
		NullCheck(L_0);
		FsmState_t2146334067 * L_1 = Fsm_get_ActiveState_m580729145(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Fsm_t1527112426 * L_2 = ___fsm0;
		NullCheck(L_2);
		FsmState_t2146334067 * L_3 = Fsm_get_ActiveState_m580729145(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmStateActionU5BU5D_t2476090292* L_4 = FsmState_get_Actions_m61102534(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmStateActionU5BU5D_t2476090292* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0030;
	}

IL_001a:
	{
		FsmStateActionU5BU5D_t2476090292* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		FsmStateAction_t2366529033 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		FsmStateAction_t2366529033 * L_10 = V_1;
		NullCheck(L_10);
		bool L_11 = FsmStateAction_get_Active_m638802721(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_002c;
		}
	}
	{
		FsmStateAction_t2366529033 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_12);
	}

IL_002c:
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_14 = V_3;
		FsmStateActionU5BU5D_t2476090292* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_001a;
		}
	}

IL_0036:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnDisable()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_OnDisable_m3137147225_MetadataUsageId;
extern "C"  void PlayMakerGUI_OnDisable_m3137147225 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnDisable_m3137147225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		PlayMakerGUI_t3799848395 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_instance_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17((PlayMakerGUI_t3799848395 *)NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerGUI::DoEditGUI()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_DoEditGUI_m3828862422_MetadataUsageId;
extern "C"  void PlayMakerGUI_DoEditGUI_m3828862422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DoEditGUI_m3828862422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmState_t2146334067 * V_0 = NULL;
	FsmStateActionU5BU5D_t2476090292* V_1 = NULL;
	FsmStateAction_t2366529033 * V_2 = NULL;
	FsmStateActionU5BU5D_t2476090292* V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Fsm_t1527112426 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		if (!L_0)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Fsm_t1527112426 * L_1 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_1);
		bool L_2 = Fsm_get_HandleOnGUI_m3061956918(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Fsm_t1527112426 * L_3 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_3);
		FsmState_t2146334067 * L_4 = Fsm_get_EditState_m3234409717(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmState_t2146334067 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		FsmState_t2146334067 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = FsmState_get_IsInitialized_m996080711(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		FsmState_t2146334067 * L_8 = V_0;
		NullCheck(L_8);
		FsmStateActionU5BU5D_t2476090292* L_9 = FsmState_get_Actions_m61102534(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmStateActionU5BU5D_t2476090292* L_10 = V_1;
		V_3 = L_10;
		V_4 = 0;
		goto IL_0050;
	}

IL_0037:
	{
		FsmStateActionU5BU5D_t2476090292* L_11 = V_3;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		FsmStateAction_t2366529033 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_2 = L_14;
		FsmStateAction_t2366529033 * L_15 = V_2;
		NullCheck(L_15);
		bool L_16 = FsmStateAction_get_Enabled_m1786926760(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_004a;
		}
	}
	{
		FsmStateAction_t2366529033 * L_17 = V_2;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_17);
	}

IL_004a:
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_19 = V_4;
		FsmStateActionU5BU5D_t2476090292* L_20 = V_3;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0037;
		}
	}

IL_0057:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnApplicationQuit()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_OnApplicationQuit_m1060313200_MetadataUsageId;
extern "C"  void PlayMakerGUI_OnApplicationQuit_m1060313200 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnApplicationQuit_m1060313200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_instance_17((PlayMakerGUI_t3799848395 *)NULL);
		return;
	}
}
// System.Void PlayMakerGUI::.ctor()
extern "C"  void PlayMakerGUI__ctor_m539887090 (PlayMakerGUI_t3799848395 * __this, const MethodInfo* method)
{
	{
		__this->set_previewOnGUI_6((bool)1);
		__this->set_enableGUILayout_7((bool)1);
		__this->set_drawStateLabels_8((bool)1);
		__this->set_GUITextureStateLabels_9((bool)1);
		__this->set_GUITextStateLabels_10((bool)1);
		__this->set_maxLabelDistance_12((10.0f));
		__this->set_controlMouseCursor_13((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlayMakerGUI::<DrawStateLabels>b__1(PlayMakerFSM,PlayMakerFSM)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315_MetadataUsageId;
extern "C"  int32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t3799847376 * ___x0, PlayMakerFSM_t3799847376 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m510746315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = ___x0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m3709440845(L_1, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_3 = ___y1;
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m3709440845(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_Compare_m1439712187(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PlayMakerGUI::.cctor()
extern Il2CppClass* List_1_t873065632_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m24913529_MethodInfo_var;
extern const uint32_t PlayMakerGUI__cctor_m3369501691_MetadataUsageId;
extern "C"  void PlayMakerGUI__cctor_m3369501691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI__cctor_m3369501691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t873065632 * L_0 = (List_1_t873065632 *)il2cpp_codegen_object_new(List_1_t873065632_il2cpp_TypeInfo_var);
		List_1__ctor_m24913529(L_0, /*hidden argument*/List_1__ctor_m24913529_MethodInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_fsmList_3(L_0);
		GUIContent_t2094828418 * L_1 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m923375087(L_1, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_labelContent_5(L_1);
		List_1_t873065632 * L_2 = (List_1_t873065632 *)il2cpp_codegen_object_new(List_1_t873065632_il2cpp_TypeInfo_var);
		List_1__ctor_m24913529(L_2, /*hidden argument*/List_1__ctor_m24913529_MethodInfo_var);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_SortedFsmList_14(L_2);
		Color_t4194546905  L_3 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiColor_19(L_3);
		Color_t4194546905  L_4 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiBackgroundColor_20(L_4);
		Color_t4194546905  L_5 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiContentColor_21(L_5);
		Matrix4x4_t1651859333  L_6 = Matrix4x4_get_identity_m3946683782(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->set_guiMatrix_22(L_6);
		return;
	}
}
// System.Void PlayMakerJointBreak::OnJointBreak(System.Single)
extern "C"  void PlayMakerJointBreak_OnJointBreak_m105815447 (PlayMakerJointBreak_t197925893 * __this, float ___breakForce0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleJointBreak_m2499030429(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		float L_11 = ___breakForce0;
		NullCheck(L_10);
		Fsm_OnJointBreak_m1675142728(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak::.ctor()
extern "C"  void PlayMakerJointBreak__ctor_m133126660 (PlayMakerJointBreak_t197925893 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerJointBreak2D::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void PlayMakerJointBreak2D_OnJointBreak2D_m266402197 (PlayMakerJointBreak2D_t1228223767 * __this, Joint2D_t2513613714 * ___brokenJoint0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleJointBreak2D_m681848943(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Joint2D_t2513613714 * L_11 = ___brokenJoint0;
		NullCheck(L_10);
		Fsm_OnJointBreak2D_m1427593368(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak2D::.ctor()
extern "C"  void PlayMakerJointBreak2D__ctor_m1915614130 (PlayMakerJointBreak2D_t1228223767 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseEnter()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseEnter_m479540455_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseEnter_m479540455 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseEnter_m479540455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseEnter_m321674116(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDown()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseDown_m403501141_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseDown_m403501141 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDown_m403501141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseDown_m2060976664(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUp()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseUp_m121595022_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseUp_m121595022 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUp_m121595022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseUp_m3055160081(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m1114637630(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_13 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUpAsButton()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseUpAsButton_m3394544818_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseUpAsButton_m3394544818 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUpAsButton_m3394544818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseUpAsButton_m1043208245(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m1114637630(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_13 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseExit()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseExit_m440030673_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseExit_m440030673 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseExit_m440030673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseExit_m2097506196(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDrag()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseDrag_m405609575_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseDrag_m405609575 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDrag_m405609575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseDrag_m2063085098(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseOver()
extern Il2CppClass* FsmEvent_t2133468028_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerMouseEvents_OnMouseOver_m724354055_MetadataUsageId;
extern "C"  void PlayMakerMouseEvents_OnMouseOver_m724354055 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseOver_m724354055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m624787264(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t1527112426 * L_8 = PlayMakerFSM_get_Fsm_m886945091(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t2133468028_il2cpp_TypeInfo_var);
		FsmEvent_t2133468028 * L_9 = FsmEvent_get_MouseOver_m2381829578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m625948263(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_12 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::.ctor()
extern "C"  void PlayMakerMouseEvents__ctor_m2803383919 (PlayMakerMouseEvents_t316289710 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerOnGUI::Start()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerOnGUI_Start_m2713337841_MetadataUsageId;
extern "C"  void PlayMakerOnGUI_Start_m2713337841 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_Start_m2713337841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = __this->get_playMakerFSM_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_2 = __this->get_playMakerFSM_2();
		NullCheck(L_2);
		Fsm_t1527112426 * L_3 = PlayMakerFSM_get_Fsm_m886945091(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Fsm_set_HandleOnGUI_m1667829569(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::OnGUI()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerOnGUI_OnGUI_m3261598699_MetadataUsageId;
extern "C"  void PlayMakerOnGUI_OnGUI_m3261598699 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_OnGUI_m3261598699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_previewInEditMode_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		PlayMakerOnGUI_DoEditGUI_m2461980501(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		PlayMakerFSM_t3799847376 * L_2 = __this->get_playMakerFSM_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_4 = __this->get_playMakerFSM_2();
		NullCheck(L_4);
		Fsm_t1527112426 * L_5 = PlayMakerFSM_get_Fsm_m886945091(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = __this->get_playMakerFSM_2();
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleOnGUI_m3061956918(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = __this->get_playMakerFSM_2();
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_OnGUI_m1741834799(L_10, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::DoEditGUI()
extern Il2CppClass* PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerOnGUI_DoEditGUI_m2461980501_MetadataUsageId;
extern "C"  void PlayMakerOnGUI_DoEditGUI_m2461980501 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_DoEditGUI_m2461980501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmState_t2146334067 * V_0 = NULL;
	FsmStateActionU5BU5D_t2476090292* V_1 = NULL;
	int32_t V_2 = 0;
	FsmStateAction_t2366529033 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Fsm_t1527112426 * L_0 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var);
		Fsm_t1527112426 * L_1 = ((PlayMakerGUI_t3799848395_StaticFields*)PlayMakerGUI_t3799848395_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_1);
		FsmState_t2146334067 * L_2 = Fsm_get_EditState_m3234409717(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmState_t2146334067 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t2146334067 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = FsmState_get_IsInitialized_m996080711(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t2146334067 * L_6 = V_0;
		NullCheck(L_6);
		FsmStateActionU5BU5D_t2476090292* L_7 = FsmState_get_Actions_m61102534(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_003e;
	}

IL_0028:
	{
		FsmStateActionU5BU5D_t2476090292* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		FsmStateAction_t2366529033 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		FsmStateAction_t2366529033 * L_12 = V_3;
		NullCheck(L_12);
		bool L_13 = FsmStateAction_get_Active_m638802721(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_t2366529033 * L_14 = V_3;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_14);
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_16 = V_2;
		FsmStateActionU5BU5D_t2476090292* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0028;
		}
	}

IL_0044:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::.ctor()
extern "C"  void PlayMakerOnGUI__ctor_m3766200049 (PlayMakerOnGUI_t940239724 * __this, const MethodInfo* method)
{
	{
		__this->set_previewInEditMode_3((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerParticleCollision::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void PlayMakerParticleCollision_OnParticleCollision_m1753553028 (PlayMakerParticleCollision_t2428451804 * __this, GameObject_t3674682005 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleParticleCollision_m1126387750(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnParticleCollision_m1030297144(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerParticleCollision::.ctor()
extern "C"  void PlayMakerParticleCollision__ctor_m3278577025 (PlayMakerParticleCollision_t2428451804 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* ScriptableObject_CreateInstance_TisPlayMakerPrefs_t941311808_m3675345656_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral941311808;
extern const uint32_t PlayMakerPrefs_get_Instance_m1461573414_MetadataUsageId;
extern "C"  PlayMakerPrefs_t941311808 * PlayMakerPrefs_get_Instance_m1461573414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Instance_m1461573414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_0 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Object_t3071478659 * L_2 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral941311808, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->set_instance_2(((PlayMakerPrefs_t941311808 *)IsInstClass(L_2, PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var)));
		PlayMakerPrefs_t941311808 * L_3 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		PlayMakerPrefs_t941311808 * L_5 = ScriptableObject_CreateInstance_TisPlayMakerPrefs_t941311808_m3675345656(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerPrefs_t941311808_m3675345656_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_5);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_6 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		return L_6;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_get_Colors_m2303259045_MetadataUsageId;
extern "C"  ColorU5BU5D_t2441545636* PlayMakerPrefs_get_Colors_m2303259045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Colors_m2303259045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_0 = PlayMakerPrefs_get_Instance_m1461573414(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ColorU5BU5D_t2441545636* L_1 = L_0->get_colors_5();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_Colors(UnityEngine.Color[])
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_set_Colors_m71542544_MetadataUsageId;
extern "C"  void PlayMakerPrefs_set_Colors_m71542544 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t2441545636* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_Colors_m71542544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_0 = PlayMakerPrefs_get_Instance_m1461573414(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorU5BU5D_t2441545636* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colors_5(L_1);
		return;
	}
}
// System.String[] PlayMakerPrefs::get_ColorNames()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_get_ColorNames_m2268906614_MetadataUsageId;
extern "C"  StringU5BU5D_t4054002952* PlayMakerPrefs_get_ColorNames_m2268906614 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_ColorNames_m2268906614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_0 = PlayMakerPrefs_get_Instance_m1461573414(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_1 = L_0->get_colorNames_6();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_ColorNames(System.String[])
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_set_ColorNames_m2640639191_MetadataUsageId;
extern "C"  void PlayMakerPrefs_set_ColorNames_m2640639191 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_ColorNames_m2640639191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t941311808 * L_0 = PlayMakerPrefs_get_Instance_m1461573414(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colorNames_6(L_1);
		return;
	}
}
// System.Void PlayMakerPrefs::ResetDefaultColors()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_ResetDefaultColors_m4260521737_MetadataUsageId;
extern "C"  void PlayMakerPrefs_ResetDefaultColors_m4260521737 (PlayMakerPrefs_t941311808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_ResetDefaultColors_m4260521737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0038;
	}

IL_0004:
	{
		ColorU5BU5D_t2441545636* L_0 = __this->get_colors_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_2 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_defaultColors_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(*(Color_t4194546905 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))) = (*(Color_t4194546905 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		StringU5BU5D_t4054002952* L_4 = __this->get_colorNames_6();
		int32_t L_5 = V_0;
		StringU5BU5D_t4054002952* L_6 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_defaultColorNames_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_9);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (String_t*)L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_12 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_defaultColors_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_MinimapColors()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_get_MinimapColors_m2755696002_MetadataUsageId;
extern "C"  ColorU5BU5D_t2441545636* PlayMakerPrefs_get_MinimapColors_m2755696002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_MinimapColors_m2755696002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_0 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m170820455(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_1 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::SaveChanges()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_SaveChanges_m270435393_MetadataUsageId;
extern "C"  void PlayMakerPrefs_SaveChanges_m270435393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_SaveChanges_m270435393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m170820455(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_UpdateMinimapColors_m170820455_MetadataUsageId;
extern "C"  void PlayMakerPrefs_UpdateMinimapColors_m170820455 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_UpdateMinimapColors_m170820455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Color_t4194546905  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_0 = PlayMakerPrefs_get_Colors_m2303259045(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->set_minimapColors_7(((ColorU5BU5D_t2441545636*)SZArrayNew(ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0059;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_1 = PlayMakerPrefs_get_Colors_m2303259045(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		V_1 = (*(Color_t4194546905 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		ColorU5BU5D_t2441545636* L_3 = ((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		float L_5 = (&V_1)->get_r_0();
		float L_6 = (&V_1)->get_g_1();
		float L_7 = (&V_1)->get_b_2();
		Color_t4194546905  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m2252924356(&L_8, L_5, L_6, L_7, (0.5f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))) = L_8;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var);
		ColorU5BU5D_t2441545636* L_11 = PlayMakerPrefs_get_Colors_m2303259045(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerPrefs::.ctor()
extern Il2CppClass* ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3209457185;
extern Il2CppCodeGenString* _stringLiteral2073722;
extern Il2CppCodeGenString* _stringLiteral2115395;
extern Il2CppCodeGenString* _stringLiteral69066467;
extern Il2CppCodeGenString* _stringLiteral2644594836;
extern Il2CppCodeGenString* _stringLiteral2369983054;
extern Il2CppCodeGenString* _stringLiteral82033;
extern Il2CppCodeGenString* _stringLiteral2401891292;
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t PlayMakerPrefs__ctor_m1517509021_MetadataUsageId;
extern "C"  void PlayMakerPrefs__ctor_m1517509021 (PlayMakerPrefs_t941311808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__ctor_m1517509021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t2441545636* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	{
		V_0 = ((ColorU5BU5D_t2441545636*)SZArrayNew(ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		ColorU5BU5D_t2441545636* L_0 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Color_t4194546905  L_1 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		ColorU5BU5D_t2441545636* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Color_t4194546905  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m103496991(&L_3, (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		ColorU5BU5D_t2441545636* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Color_t4194546905  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m103496991(&L_5, (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		ColorU5BU5D_t2441545636* L_6 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Color_t4194546905  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m103496991(&L_7, (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_7;
		ColorU5BU5D_t2441545636* L_8 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		Color_t4194546905  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m103496991(&L_9, (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_9;
		ColorU5BU5D_t2441545636* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		Color_t4194546905  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m103496991(&L_11, (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))) = L_11;
		ColorU5BU5D_t2441545636* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		Color_t4194546905  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m103496991(&L_13, (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))) = L_13;
		ColorU5BU5D_t2441545636* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		Color_t4194546905  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m103496991(&L_15, (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))) = L_15;
		ColorU5BU5D_t2441545636* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		Color_t4194546905  L_17 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(8)))) = L_17;
		ColorU5BU5D_t2441545636* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		Color_t4194546905  L_19 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)9))))) = L_19;
		ColorU5BU5D_t2441545636* L_20 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)10));
		Color_t4194546905  L_21 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)10))))) = L_21;
		ColorU5BU5D_t2441545636* L_22 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)11));
		Color_t4194546905  L_23 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)11))))) = L_23;
		ColorU5BU5D_t2441545636* L_24 = V_0;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)12));
		Color_t4194546905  L_25 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12))))) = L_25;
		ColorU5BU5D_t2441545636* L_26 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)13));
		Color_t4194546905  L_27 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13))))) = L_27;
		ColorU5BU5D_t2441545636* L_28 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)14));
		Color_t4194546905  L_29 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14))))) = L_29;
		ColorU5BU5D_t2441545636* L_30 = V_0;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)15));
		Color_t4194546905  L_31 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)15))))) = L_31;
		ColorU5BU5D_t2441545636* L_32 = V_0;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)16));
		Color_t4194546905  L_33 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)16))))) = L_33;
		ColorU5BU5D_t2441545636* L_34 = V_0;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)17));
		Color_t4194546905  L_35 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)17))))) = L_35;
		ColorU5BU5D_t2441545636* L_36 = V_0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)18));
		Color_t4194546905  L_37 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)18))))) = L_37;
		ColorU5BU5D_t2441545636* L_38 = V_0;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)19));
		Color_t4194546905  L_39 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)19))))) = L_39;
		ColorU5BU5D_t2441545636* L_40 = V_0;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)20));
		Color_t4194546905  L_41 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)20))))) = L_41;
		ColorU5BU5D_t2441545636* L_42 = V_0;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)21));
		Color_t4194546905  L_43 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)21))))) = L_43;
		ColorU5BU5D_t2441545636* L_44 = V_0;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)22));
		Color_t4194546905  L_45 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)22))))) = L_45;
		ColorU5BU5D_t2441545636* L_46 = V_0;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)23));
		Color_t4194546905  L_47 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)23))))) = L_47;
		ColorU5BU5D_t2441545636* L_48 = V_0;
		__this->set_colors_5(L_48);
		V_1 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		StringU5BU5D_t4054002952* L_49 = V_1;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 0);
		ArrayElementTypeCheck (L_49, _stringLiteral3209457185);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3209457185);
		StringU5BU5D_t4054002952* L_50 = V_1;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 1);
		ArrayElementTypeCheck (L_50, _stringLiteral2073722);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2073722);
		StringU5BU5D_t4054002952* L_51 = V_1;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 2);
		ArrayElementTypeCheck (L_51, _stringLiteral2115395);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2115395);
		StringU5BU5D_t4054002952* L_52 = V_1;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 3);
		ArrayElementTypeCheck (L_52, _stringLiteral69066467);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral69066467);
		StringU5BU5D_t4054002952* L_53 = V_1;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 4);
		ArrayElementTypeCheck (L_53, _stringLiteral2644594836);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2644594836);
		StringU5BU5D_t4054002952* L_54 = V_1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 5);
		ArrayElementTypeCheck (L_54, _stringLiteral2369983054);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral2369983054);
		StringU5BU5D_t4054002952* L_55 = V_1;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 6);
		ArrayElementTypeCheck (L_55, _stringLiteral82033);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral82033);
		StringU5BU5D_t4054002952* L_56 = V_1;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 7);
		ArrayElementTypeCheck (L_56, _stringLiteral2401891292);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral2401891292);
		StringU5BU5D_t4054002952* L_57 = V_1;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 8);
		ArrayElementTypeCheck (L_57, _stringLiteral0);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_58 = V_1;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)9));
		ArrayElementTypeCheck (L_58, _stringLiteral0);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_59 = V_1;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, ((int32_t)10));
		ArrayElementTypeCheck (L_59, _stringLiteral0);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_60 = V_1;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)11));
		ArrayElementTypeCheck (L_60, _stringLiteral0);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_61 = V_1;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)12));
		ArrayElementTypeCheck (L_61, _stringLiteral0);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_62 = V_1;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)13));
		ArrayElementTypeCheck (L_62, _stringLiteral0);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_63 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)14));
		ArrayElementTypeCheck (L_63, _stringLiteral0);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_64 = V_1;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)15));
		ArrayElementTypeCheck (L_64, _stringLiteral0);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_65 = V_1;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)16));
		ArrayElementTypeCheck (L_65, _stringLiteral0);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_66 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)17));
		ArrayElementTypeCheck (L_66, _stringLiteral0);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_67 = V_1;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)18));
		ArrayElementTypeCheck (L_67, _stringLiteral0);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_68 = V_1;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)19));
		ArrayElementTypeCheck (L_68, _stringLiteral0);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_69 = V_1;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)20));
		ArrayElementTypeCheck (L_69, _stringLiteral0);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_70 = V_1;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)21));
		ArrayElementTypeCheck (L_70, _stringLiteral0);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_71 = V_1;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)22));
		ArrayElementTypeCheck (L_71, _stringLiteral0);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_72 = V_1;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, ((int32_t)23));
		ArrayElementTypeCheck (L_72, _stringLiteral0);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral0);
		StringU5BU5D_t4054002952* L_73 = V_1;
		__this->set_colorNames_6(L_73);
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::.cctor()
extern Il2CppClass* ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3209457185;
extern Il2CppCodeGenString* _stringLiteral2073722;
extern Il2CppCodeGenString* _stringLiteral2115395;
extern Il2CppCodeGenString* _stringLiteral69066467;
extern Il2CppCodeGenString* _stringLiteral2644594836;
extern Il2CppCodeGenString* _stringLiteral2369983054;
extern Il2CppCodeGenString* _stringLiteral82033;
extern Il2CppCodeGenString* _stringLiteral2401891292;
extern const uint32_t PlayMakerPrefs__cctor_m3611010480_MetadataUsageId;
extern "C"  void PlayMakerPrefs__cctor_m3611010480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__cctor_m3611010480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t2441545636* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	{
		V_0 = ((ColorU5BU5D_t2441545636*)SZArrayNew(ColorU5BU5D_t2441545636_il2cpp_TypeInfo_var, (uint32_t)8));
		ColorU5BU5D_t2441545636* L_0 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Color_t4194546905  L_1 = Color_get_grey_m3805481615(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		ColorU5BU5D_t2441545636* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Color_t4194546905  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m103496991(&L_3, (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		ColorU5BU5D_t2441545636* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Color_t4194546905  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m103496991(&L_5, (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		ColorU5BU5D_t2441545636* L_6 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Color_t4194546905  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m103496991(&L_7, (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_7;
		ColorU5BU5D_t2441545636* L_8 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		Color_t4194546905  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m103496991(&L_9, (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_9;
		ColorU5BU5D_t2441545636* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		Color_t4194546905  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m103496991(&L_11, (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))) = L_11;
		ColorU5BU5D_t2441545636* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		Color_t4194546905  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m103496991(&L_13, (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))) = L_13;
		ColorU5BU5D_t2441545636* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		Color_t4194546905  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m103496991(&L_15, (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		(*(Color_t4194546905 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))) = L_15;
		ColorU5BU5D_t2441545636* L_16 = V_0;
		((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->set_defaultColors_3(L_16);
		V_1 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)8));
		StringU5BU5D_t4054002952* L_17 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, _stringLiteral3209457185);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3209457185);
		StringU5BU5D_t4054002952* L_18 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, _stringLiteral2073722);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2073722);
		StringU5BU5D_t4054002952* L_19 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, _stringLiteral2115395);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2115395);
		StringU5BU5D_t4054002952* L_20 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, _stringLiteral69066467);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral69066467);
		StringU5BU5D_t4054002952* L_21 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		ArrayElementTypeCheck (L_21, _stringLiteral2644594836);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2644594836);
		StringU5BU5D_t4054002952* L_22 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 5);
		ArrayElementTypeCheck (L_22, _stringLiteral2369983054);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral2369983054);
		StringU5BU5D_t4054002952* L_23 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 6);
		ArrayElementTypeCheck (L_23, _stringLiteral82033);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral82033);
		StringU5BU5D_t4054002952* L_24 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 7);
		ArrayElementTypeCheck (L_24, _stringLiteral2401891292);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral2401891292);
		StringU5BU5D_t4054002952* L_25 = V_1;
		((PlayMakerPrefs_t941311808_StaticFields*)PlayMakerPrefs_t941311808_il2cpp_TypeInfo_var->static_fields)->set_defaultColorNames_4(L_25);
		return;
	}
}
// System.Void PlayMakerProxyBase::Awake()
extern "C"  void PlayMakerProxyBase_Awake_m1062924081 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase_Reset_m2766719099(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::Reset()
extern const MethodInfo* Component_GetComponents_TisPlayMakerFSM_t3799847376_m1766213632_MethodInfo_var;
extern const uint32_t PlayMakerProxyBase_Reset_m2766719099_MetadataUsageId;
extern "C"  void PlayMakerProxyBase_Reset_m2766719099 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_Reset_m2766719099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = Component_GetComponents_TisPlayMakerFSM_t3799847376_m1766213632(__this, /*hidden argument*/Component_GetComponents_TisPlayMakerFSM_t3799847376_m1766213632_MethodInfo_var);
		__this->set_playMakerFSMs_2(L_0);
		return;
	}
}
// System.Void PlayMakerProxyBase::.ctor()
extern "C"  void PlayMakerProxyBase__ctor_m825318862 (PlayMakerProxyBase_t3469687535 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerEnter_OnTriggerEnter_m2450496159 (PlayMakerTriggerEnter_t977448304 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerEnter_m204797576(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider_t2939674232 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerEnter_m3743133283(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerEnter::.ctor()
extern "C"  void PlayMakerTriggerEnter__ctor_m3125792633 (PlayMakerTriggerEnter_t977448304 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter2D::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerEnter2D_OnTriggerEnter2D_m2751469681 (PlayMakerTriggerEnter2D_t3024951234 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerEnter2D_m3537267354(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider2D_t1552025098 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerEnter2D_m449017059(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerEnter2D::.ctor()
extern "C"  void PlayMakerTriggerEnter2D__ctor_m239525863 (PlayMakerTriggerEnter2D_t3024951234 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerExit_OnTriggerExit_m3825211201 (PlayMakerTriggerExit_t3495223174 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerExit_m985357328(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider_t2939674232 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerExit_m3649994463(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerExit::.ctor()
extern "C"  void PlayMakerTriggerExit__ctor_m3906372759 (PlayMakerTriggerExit_t3495223174 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerExit2D_OnTriggerExit2D_m2518370223 (PlayMakerTriggerExit2D_t245046360 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerExit2D_m2035912226(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider2D_t1552025098 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerExit2D_m102868831(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerExit2D::.ctor()
extern "C"  void PlayMakerTriggerExit2D__ctor_m3052717445 (PlayMakerTriggerExit2D_t245046360 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay::OnTriggerStay(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerStay_OnTriggerStay_m1928126945 (PlayMakerTriggerStay_t3495636161 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerStay_m1382237835(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider_t2939674232 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerStay_m1973116378(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerStay::.ctor()
extern "C"  void PlayMakerTriggerStay__ctor_m4085508540 (PlayMakerTriggerStay_t3495636161 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay2D::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerStay2D_OnTriggerStay2D_m314707215 (PlayMakerTriggerStay2D_t641926867 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t191094001* L_0 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		PlayMakerFSM_t3799847376 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t3799847376 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m964418526(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t1527112426 * L_7 = PlayMakerFSM_get_Fsm_m886945091(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerStay2D_m1185990109(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t1527112426 * L_10 = PlayMakerFSM_get_Fsm_m886945091(L_9, /*hidden argument*/NULL);
		Collider2D_t1552025098 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerStay2D_m39882970(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t191094001* L_14 = ((PlayMakerProxyBase_t3469687535 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerStay2D::.ctor()
extern "C"  void PlayMakerTriggerStay2D__ctor_m3403511146 (PlayMakerTriggerStay2D_t641926867 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m825318862(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
