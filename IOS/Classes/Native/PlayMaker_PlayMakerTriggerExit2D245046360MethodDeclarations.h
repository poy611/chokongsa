﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerExit2D
struct PlayMakerTriggerExit2D_t245046360;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void PlayMakerTriggerExit2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerExit2D_OnTriggerExit2D_m2518370223 (PlayMakerTriggerExit2D_t245046360 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerExit2D::.ctor()
extern "C"  void PlayMakerTriggerExit2D__ctor_m3052717445 (PlayMakerTriggerExit2D_t245046360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
