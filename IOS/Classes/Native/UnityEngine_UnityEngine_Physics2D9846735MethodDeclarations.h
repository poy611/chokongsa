﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void UnityEngine.Physics2D::.cctor()
extern "C"  void Physics2D__cctor_m2087591309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::set_gravity(UnityEngine.Vector2)
extern "C"  void Physics2D_set_gravity_m2019264139 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
extern "C"  void Physics2D_INTERNAL_set_gravity_m3872262661 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Linecast_m3653471167 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m4170255972 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m1824977262 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m2367926937 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m740601359 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_LinecastAll_m3511439778 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m4294843026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1435321255 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m301626417 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m1583954576 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m3437166214 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_RaycastAll_m765742583 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_GetRayIntersection_m2828566441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m2375445855 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m3849560536 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m2520210479 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m4265382893 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m3910604131 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m2585342016 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m1000299574 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2584047348 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2026172650 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pointA0, Vector2_t4282066565 * ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
