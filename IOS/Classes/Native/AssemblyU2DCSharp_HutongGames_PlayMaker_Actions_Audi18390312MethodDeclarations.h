﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AudioPause
struct AudioPause_t18390312;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AudioPause::.ctor()
extern "C"  void AudioPause__ctor_m1910342718 (AudioPause_t18390312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPause::Reset()
extern "C"  void AudioPause_Reset_m3851742955 (AudioPause_t18390312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPause::OnEnter()
extern "C"  void AudioPause_OnEnter_m1280045205 (AudioPause_t18390312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
