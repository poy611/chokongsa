﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayAdd
struct  ArrayAdd_t2749463600  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayAdd::array
	FsmArray_t2129666875 * ___array_11;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayAdd::value
	FsmVar_t1596150537 * ___value_12;

public:
	inline static int32_t get_offset_of_array_11() { return static_cast<int32_t>(offsetof(ArrayAdd_t2749463600, ___array_11)); }
	inline FsmArray_t2129666875 * get_array_11() const { return ___array_11; }
	inline FsmArray_t2129666875 ** get_address_of_array_11() { return &___array_11; }
	inline void set_array_11(FsmArray_t2129666875 * value)
	{
		___array_11 = value;
		Il2CppCodeGenWriteBarrier(&___array_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(ArrayAdd_t2749463600, ___value_12)); }
	inline FsmVar_t1596150537 * get_value_12() const { return ___value_12; }
	inline FsmVar_t1596150537 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmVar_t1596150537 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
