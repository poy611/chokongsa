﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2ClampMagnitude
struct Vector2ClampMagnitude_t3504697234;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::.ctor()
extern "C"  void Vector2ClampMagnitude__ctor_m4217101796 (Vector2ClampMagnitude_t3504697234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::Reset()
extern "C"  void Vector2ClampMagnitude_Reset_m1863534737 (Vector2ClampMagnitude_t3504697234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::OnEnter()
extern "C"  void Vector2ClampMagnitude_OnEnter_m1872394427 (Vector2ClampMagnitude_t3504697234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::OnUpdate()
extern "C"  void Vector2ClampMagnitude_OnUpdate_m1343211752 (Vector2ClampMagnitude_t3504697234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::DoVector2ClampMagnitude()
extern "C"  void Vector2ClampMagnitude_DoVector2ClampMagnitude_m2549017467 (Vector2ClampMagnitude_t3504697234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
