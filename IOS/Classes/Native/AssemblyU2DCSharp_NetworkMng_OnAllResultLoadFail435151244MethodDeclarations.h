﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnAllResultLoadFail
struct OnAllResultLoadFail_t435151244;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnAllResultLoadFail::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAllResultLoadFail__ctor_m491749939 (OnAllResultLoadFail_t435151244 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResultLoadFail::Invoke()
extern "C"  void OnAllResultLoadFail_Invoke_m3888187469 (OnAllResultLoadFail_t435151244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnAllResultLoadFail::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAllResultLoadFail_BeginInvoke_m3271442198 (OnAllResultLoadFail_t435151244 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnAllResultLoadFail::EndInvoke(System.IAsyncResult)
extern "C"  void OnAllResultLoadFail_EndInvoke_m1330251203 (OnAllResultLoadFail_t435151244 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
