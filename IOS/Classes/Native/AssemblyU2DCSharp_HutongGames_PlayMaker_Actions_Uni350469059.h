﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UnityAdsShowAd
struct  UnityAdsShowAd_t350469059  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnityAdsShowAd::zoneId
	FsmString_t952858651 * ___zoneId_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnityAdsShowAd::gamerSid
	FsmString_t952858651 * ___gamerSid_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UnityAdsShowAd::isReady
	FsmBool_t1075959796 * ___isReady_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnityAdsShowAd::isNotReadyEvent
	FsmEvent_t2133468028 * ___isNotReadyEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnityAdsShowAd::successEvent
	FsmEvent_t2133468028 * ___successEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnityAdsShowAd::skippedEvent
	FsmEvent_t2133468028 * ___skippedEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnityAdsShowAd::failedEvent
	FsmEvent_t2133468028 * ___failedEvent_17;

public:
	inline static int32_t get_offset_of_zoneId_11() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___zoneId_11)); }
	inline FsmString_t952858651 * get_zoneId_11() const { return ___zoneId_11; }
	inline FsmString_t952858651 ** get_address_of_zoneId_11() { return &___zoneId_11; }
	inline void set_zoneId_11(FsmString_t952858651 * value)
	{
		___zoneId_11 = value;
		Il2CppCodeGenWriteBarrier(&___zoneId_11, value);
	}

	inline static int32_t get_offset_of_gamerSid_12() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___gamerSid_12)); }
	inline FsmString_t952858651 * get_gamerSid_12() const { return ___gamerSid_12; }
	inline FsmString_t952858651 ** get_address_of_gamerSid_12() { return &___gamerSid_12; }
	inline void set_gamerSid_12(FsmString_t952858651 * value)
	{
		___gamerSid_12 = value;
		Il2CppCodeGenWriteBarrier(&___gamerSid_12, value);
	}

	inline static int32_t get_offset_of_isReady_13() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___isReady_13)); }
	inline FsmBool_t1075959796 * get_isReady_13() const { return ___isReady_13; }
	inline FsmBool_t1075959796 ** get_address_of_isReady_13() { return &___isReady_13; }
	inline void set_isReady_13(FsmBool_t1075959796 * value)
	{
		___isReady_13 = value;
		Il2CppCodeGenWriteBarrier(&___isReady_13, value);
	}

	inline static int32_t get_offset_of_isNotReadyEvent_14() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___isNotReadyEvent_14)); }
	inline FsmEvent_t2133468028 * get_isNotReadyEvent_14() const { return ___isNotReadyEvent_14; }
	inline FsmEvent_t2133468028 ** get_address_of_isNotReadyEvent_14() { return &___isNotReadyEvent_14; }
	inline void set_isNotReadyEvent_14(FsmEvent_t2133468028 * value)
	{
		___isNotReadyEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotReadyEvent_14, value);
	}

	inline static int32_t get_offset_of_successEvent_15() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___successEvent_15)); }
	inline FsmEvent_t2133468028 * get_successEvent_15() const { return ___successEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_successEvent_15() { return &___successEvent_15; }
	inline void set_successEvent_15(FsmEvent_t2133468028 * value)
	{
		___successEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_15, value);
	}

	inline static int32_t get_offset_of_skippedEvent_16() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___skippedEvent_16)); }
	inline FsmEvent_t2133468028 * get_skippedEvent_16() const { return ___skippedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_skippedEvent_16() { return &___skippedEvent_16; }
	inline void set_skippedEvent_16(FsmEvent_t2133468028 * value)
	{
		___skippedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___skippedEvent_16, value);
	}

	inline static int32_t get_offset_of_failedEvent_17() { return static_cast<int32_t>(offsetof(UnityAdsShowAd_t350469059, ___failedEvent_17)); }
	inline FsmEvent_t2133468028 * get_failedEvent_17() const { return ___failedEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_failedEvent_17() { return &___failedEvent_17; }
	inline void set_failedEvent_17(FsmEvent_t2133468028 * value)
	{
		___failedEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___failedEvent_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
