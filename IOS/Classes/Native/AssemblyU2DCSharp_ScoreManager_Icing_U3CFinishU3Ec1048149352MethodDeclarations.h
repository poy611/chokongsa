﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Icing/<Finish>c__Iterator1B
struct U3CFinishU3Ec__Iterator1B_t1048149352;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::.ctor()
extern "C"  void U3CFinishU3Ec__Iterator1B__ctor_m2451273267 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<Finish>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFinishU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2006702217 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Icing/<Finish>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFinishU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m389680669 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Icing/<Finish>c__Iterator1B::MoveNext()
extern "C"  bool U3CFinishU3Ec__Iterator1B_MoveNext_m1512860361 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::Dispose()
extern "C"  void U3CFinishU3Ec__Iterator1B_Dispose_m1929814448 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::Reset()
extern "C"  void U3CFinishU3Ec__Iterator1B_Reset_m97706208 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
