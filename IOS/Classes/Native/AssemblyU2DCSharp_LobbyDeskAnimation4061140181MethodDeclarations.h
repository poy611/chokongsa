﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LobbyDeskAnimation
struct LobbyDeskAnimation_t4061140181;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void LobbyDeskAnimation::.ctor()
extern "C"  void LobbyDeskAnimation__ctor_m906464102 (LobbyDeskAnimation_t4061140181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyDeskAnimation::DeskCry()
extern "C"  void LobbyDeskAnimation_DeskCry_m566826005 (LobbyDeskAnimation_t4061140181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LobbyDeskAnimation::DeskCryCo()
extern "C"  Il2CppObject * LobbyDeskAnimation_DeskCryCo_m3512167769 (LobbyDeskAnimation_t4061140181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
