﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1740094460MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m4017614747(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1151548080 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2564041433_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(T1,T2)
#define Action_2_Invoke_m3049137664(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1151548080 *, int32_t, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m3217670066_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2583177935(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1151548080 *, int32_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3813320401_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m2689689387(__this, ___result0, method) ((  void (*) (Action_2_t1151548080 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1763242345_gshared)(__this, ___result0, method)
