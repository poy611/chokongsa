﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void GooglePlayGames.BasicApi.Achievement::.ctor()
extern "C"  void Achievement__ctor_m2701354772 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::.cctor()
extern "C"  void Achievement__cctor_m1655523097 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::ToString()
extern "C"  String_t* Achievement_ToString_m1724601279 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsIncremental()
extern "C"  bool Achievement_get_IsIncremental_m3147909093 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsIncremental(System.Boolean)
extern "C"  void Achievement_set_IsIncremental_m1044715996 (Achievement_t1261647177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_CurrentSteps()
extern "C"  int32_t Achievement_get_CurrentSteps_m2632110837 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_CurrentSteps(System.Int32)
extern "C"  void Achievement_set_CurrentSteps_m557502764 (Achievement_t1261647177 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Achievement::get_TotalSteps()
extern "C"  int32_t Achievement_get_TotalSteps_m951649354 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_TotalSteps(System.Int32)
extern "C"  void Achievement_set_TotalSteps_m3209006209 (Achievement_t1261647177 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsUnlocked()
extern "C"  bool Achievement_get_IsUnlocked_m3028755930 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsUnlocked(System.Boolean)
extern "C"  void Achievement_set_IsUnlocked_m1040567441 (Achievement_t1261647177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Achievement::get_IsRevealed()
extern "C"  bool Achievement_get_IsRevealed_m2612314595 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_IsRevealed(System.Boolean)
extern "C"  void Achievement_set_IsRevealed_m1877550298 (Achievement_t1261647177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Id()
extern "C"  String_t* Achievement_get_Id_m502868599 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Id(System.String)
extern "C"  void Achievement_set_Id_m4094565370 (Achievement_t1261647177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Description()
extern "C"  String_t* Achievement_get_Description_m236770626 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Description(System.String)
extern "C"  void Achievement_set_Description_m168662993 (Achievement_t1261647177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_Name()
extern "C"  String_t* Achievement_get_Name_m2362876199 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Name(System.String)
extern "C"  void Achievement_set_Name_m3293035402 (Achievement_t1261647177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.BasicApi.Achievement::get_LastModifiedTime()
extern "C"  DateTime_t4283661327  Achievement_get_LastModifiedTime_m2988362366 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_LastModifiedTime(System.DateTime)
extern "C"  void Achievement_set_LastModifiedTime_m2055694463 (Achievement_t1261647177 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.BasicApi.Achievement::get_Points()
extern "C"  uint64_t Achievement_get_Points_m3921891320 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_Points(System.UInt64)
extern "C"  void Achievement_set_Points_m3612579499 (Achievement_t1261647177 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_RevealedImageUrl()
extern "C"  String_t* Achievement_get_RevealedImageUrl_m89171004 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_RevealedImageUrl(System.String)
extern "C"  void Achievement_set_RevealedImageUrl_m3793888277 (Achievement_t1261647177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Achievement::get_UnlockedImageUrl()
extern "C"  String_t* Achievement_get_UnlockedImageUrl_m2971083059 (Achievement_t1261647177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Achievement::set_UnlockedImageUrl(System.String)
extern "C"  void Achievement_set_UnlockedImageUrl_m1656926334 (Achievement_t1261647177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
