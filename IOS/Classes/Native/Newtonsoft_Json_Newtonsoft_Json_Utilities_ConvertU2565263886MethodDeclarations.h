﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// Newtonsoft.Json.Utilities.TypeInformation
struct TypeInformation_t2222045584;
// System.IConvertible
struct IConvertible_t2116191568;
// System.String
struct String_t;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Object
struct Il2CppObject;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// System.Version
struct Version_t763695022;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2340202136.h"
#include "mscorlib_System_Version763695022.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ParseRes4245056558.h"
#include "mscorlib_System_Guid2862754429.h"

// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type)
extern "C"  int32_t ConvertUtils_GetTypeCode_m3728064781 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type,System.Boolean&)
extern "C"  int32_t ConvertUtils_GetTypeCode_m1509827324 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, bool* ___isEnum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.TypeInformation Newtonsoft.Json.Utilities.ConvertUtils::GetTypeInformation(System.IConvertible)
extern "C"  TypeInformation_t2222045584 * ConvertUtils_GetTypeInformation_m1932646156 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___convertable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsConvertible(System.Type)
extern "C"  bool ConvertUtils_IsConvertible_m4144288731 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.Utilities.ConvertUtils::ParseTimeSpan(System.String)
extern "C"  TimeSpan_t413522987  ConvertUtils_ParseTimeSpan_m1171495752 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey)
extern "C"  Func_2_t184564025 * ConvertUtils_CreateCastConverter_m3165284224 (Il2CppObject * __this /* static, unused */, TypeConvertKey_t866134174  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  bool ConvertUtils_TryConvert_m483503506 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ConvertUtils/ConvertResult Newtonsoft.Json.Utilities.ConvertUtils::TryConvertInternal(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern "C"  int32_t ConvertUtils_TryConvertInternal_m3693745284 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, Il2CppObject ** ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * ConvertUtils_ConvertOrCast_m318987874 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initialValue0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern "C"  Il2CppObject * ConvertUtils_EnsureTypeAssignable_m618920751 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___initialType1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Utilities.ConvertUtils::GetConverter(System.Type)
extern "C"  TypeConverter_t1753450284 * ConvertUtils_GetConverter_m329942794 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::VersionTryParse(System.String,System.Version&)
extern "C"  bool ConvertUtils_VersionTryParse_m3225814958 (Il2CppObject * __this /* static, unused */, String_t* ___input0, Version_t763695022 ** ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern "C"  bool ConvertUtils_IsInteger_m1986925469 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int32TryParse(System.Char[],System.Int32,System.Int32,System.Int32&)
extern "C"  int32_t ConvertUtils_Int32TryParse_m2021053401 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, int32_t ___length2, int32_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int64TryParse(System.Char[],System.Int32,System.Int32,System.Int64&)
extern "C"  int32_t ConvertUtils_Int64TryParse_m1584965689 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, int32_t ___length2, int64_t* ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertGuid(System.String,System.Guid&)
extern "C"  bool ConvertUtils_TryConvertGuid_m3029735494 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Guid_t2862754429 * ___g1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.ConvertUtils::HexTextToInt(System.Char[],System.Int32,System.Int32)
extern "C"  int32_t ConvertUtils_HexTextToInt_m3467575446 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___text0, int32_t ___start1, int32_t ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.ConvertUtils::HexCharToInt(System.Char)
extern "C"  int32_t ConvertUtils_HexCharToInt_m3861739393 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern "C"  void ConvertUtils__cctor_m13564766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
