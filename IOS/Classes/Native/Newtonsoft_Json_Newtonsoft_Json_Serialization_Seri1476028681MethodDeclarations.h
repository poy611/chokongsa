﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.SerializationErrorCallback
struct SerializationErrorCallback_t1476028681;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t18794611;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_ErrorC18794611.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SerializationErrorCallback__ctor_m1748773260 (SerializationErrorCallback_t1476028681 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern "C"  void SerializationErrorCallback_Invoke_m3894726674 (SerializationErrorCallback_t1476028681 * __this, Il2CppObject * ___o0, StreamingContext_t2761351129  ___context1, ErrorContext_t18794611 * ___errorContext2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Serialization.SerializationErrorCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SerializationErrorCallback_BeginInvoke_m3308586933 (SerializationErrorCallback_t1476028681 * __this, Il2CppObject * ___o0, StreamingContext_t2761351129  ___context1, ErrorContext_t18794611 * ___errorContext2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SerializationErrorCallback_EndInvoke_m2530886044 (SerializationErrorCallback_t1476028681 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
