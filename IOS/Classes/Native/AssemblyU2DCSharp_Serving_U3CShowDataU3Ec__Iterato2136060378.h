﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.Object
struct Il2CppObject;
// Serving
struct Serving_t3648806892;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Serving/<ShowData>c__Iterator29
struct  U3CShowDataU3Ec__Iterator29_t2136060378  : public Il2CppObject
{
public:
	// UnityEngine.Sprite Serving/<ShowData>c__Iterator29::<source>__0
	Sprite_t3199167241 * ___U3CsourceU3E__0_0;
	// System.Int32 Serving/<ShowData>c__Iterator29::<tempNum>__1
	int32_t ___U3CtempNumU3E__1_1;
	// System.Single Serving/<ShowData>c__Iterator29::<temp>__2
	float ___U3CtempU3E__2_2;
	// System.Int32 Serving/<ShowData>c__Iterator29::$PC
	int32_t ___U24PC_3;
	// System.Object Serving/<ShowData>c__Iterator29::$current
	Il2CppObject * ___U24current_4;
	// Serving Serving/<ShowData>c__Iterator29::<>f__this
	Serving_t3648806892 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CsourceU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U3CsourceU3E__0_0)); }
	inline Sprite_t3199167241 * get_U3CsourceU3E__0_0() const { return ___U3CsourceU3E__0_0; }
	inline Sprite_t3199167241 ** get_address_of_U3CsourceU3E__0_0() { return &___U3CsourceU3E__0_0; }
	inline void set_U3CsourceU3E__0_0(Sprite_t3199167241 * value)
	{
		___U3CsourceU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsourceU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtempNumU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U3CtempNumU3E__1_1)); }
	inline int32_t get_U3CtempNumU3E__1_1() const { return ___U3CtempNumU3E__1_1; }
	inline int32_t* get_address_of_U3CtempNumU3E__1_1() { return &___U3CtempNumU3E__1_1; }
	inline void set_U3CtempNumU3E__1_1(int32_t value)
	{
		___U3CtempNumU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U3CtempU3E__2_2)); }
	inline float get_U3CtempU3E__2_2() const { return ___U3CtempU3E__2_2; }
	inline float* get_address_of_U3CtempU3E__2_2() { return &___U3CtempU3E__2_2; }
	inline void set_U3CtempU3E__2_2(float value)
	{
		___U3CtempU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CShowDataU3Ec__Iterator29_t2136060378, ___U3CU3Ef__this_5)); }
	inline Serving_t3648806892 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline Serving_t3648806892 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(Serving_t3648806892 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
