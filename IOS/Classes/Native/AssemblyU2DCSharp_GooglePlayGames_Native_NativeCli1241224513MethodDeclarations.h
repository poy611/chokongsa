﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C
struct U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C::.ctor()
extern "C"  void U3CGetServerAuthCodeU3Ec__AnonStorey4C__ctor_m829684042 (U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetServerAuthCode>c__AnonStorey4A/<GetServerAuthCode>c__AnonStorey4C::<>m__34()
extern "C"  void U3CGetServerAuthCodeU3Ec__AnonStorey4C_U3CU3Em__34_m770550708 (U3CGetServerAuthCodeU3Ec__AnonStorey4C_t1241224513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
