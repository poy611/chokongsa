﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t191094001;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerRPCProxy
struct  PlayMakerRPCProxy_t3160235257  : public MonoBehaviour_t667441552
{
public:
	// PlayMakerFSM[] PlayMakerRPCProxy::fsms
	PlayMakerFSMU5BU5D_t191094001* ___fsms_2;

public:
	inline static int32_t get_offset_of_fsms_2() { return static_cast<int32_t>(offsetof(PlayMakerRPCProxy_t3160235257, ___fsms_2)); }
	inline PlayMakerFSMU5BU5D_t191094001* get_fsms_2() const { return ___fsms_2; }
	inline PlayMakerFSMU5BU5D_t191094001** get_address_of_fsms_2() { return &___fsms_2; }
	inline void set_fsms_2(PlayMakerFSMU5BU5D_t191094001* value)
	{
		___fsms_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsms_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
