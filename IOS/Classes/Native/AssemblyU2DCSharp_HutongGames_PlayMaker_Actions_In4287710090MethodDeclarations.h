﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntCompare2
struct IntCompare2_t4287710090;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntCompare2::.ctor()
extern "C"  void IntCompare2__ctor_m2733114092 (IntCompare2_t4287710090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare2::Reset()
extern "C"  void IntCompare2_Reset_m379547033 (IntCompare2_t4287710090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare2::OnEnter()
extern "C"  void IntCompare2_OnEnter_m1689353155 (IntCompare2_t4287710090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare2::OnUpdate()
extern "C"  void IntCompare2_OnUpdate_m4258866912 (IntCompare2_t4287710090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntCompare2::DoIntCompare()
extern "C"  void IntCompare2_DoIntCompare_m1133402745 (IntCompare2_t4287710090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
