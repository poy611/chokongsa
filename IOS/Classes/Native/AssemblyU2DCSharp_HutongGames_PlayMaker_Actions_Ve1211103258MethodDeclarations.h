﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2RotateTowards
struct Vector2RotateTowards_t1211103258;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2RotateTowards::.ctor()
extern "C"  void Vector2RotateTowards__ctor_m2381683596 (Vector2RotateTowards_t1211103258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2RotateTowards::Reset()
extern "C"  void Vector2RotateTowards_Reset_m28116537 (Vector2RotateTowards_t1211103258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2RotateTowards::OnEnter()
extern "C"  void Vector2RotateTowards_OnEnter_m3267062883 (Vector2RotateTowards_t1211103258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2RotateTowards::OnUpdate()
extern "C"  void Vector2RotateTowards_OnUpdate_m1628260928 (Vector2RotateTowards_t1211103258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
