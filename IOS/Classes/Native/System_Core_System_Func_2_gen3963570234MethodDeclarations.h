﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen785513668MethodDeclarations.h"

// System.Void System.Func`2<System.Reflection.PropertyInfo,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1997645523(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3963570234 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m563515303_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Reflection.PropertyInfo,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m1538208084(__this, ___arg10, method) ((  bool (*) (Func_2_t3963570234 *, PropertyInfo_t *, const MethodInfo*))Func_2_Invoke_m1882130143_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Reflection.PropertyInfo,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4197763587(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3963570234 *, PropertyInfo_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1852288274_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Reflection.PropertyInfo,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m320794368(__this, ___result0, method) ((  bool (*) (Func_2_t3963570234 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1659014741_gshared)(__this, ___result0, method)
