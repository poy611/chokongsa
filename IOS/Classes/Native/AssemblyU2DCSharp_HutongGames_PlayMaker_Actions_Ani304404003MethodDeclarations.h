﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFloat
struct AnimateFloat_t304404003;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::.ctor()
extern "C"  void AnimateFloat__ctor_m74780579 (AnimateFloat_t304404003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::Reset()
extern "C"  void AnimateFloat_Reset_m2016180816 (AnimateFloat_t304404003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnEnter()
extern "C"  void AnimateFloat_OnEnter_m2536388282 (AnimateFloat_t304404003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnUpdate()
extern "C"  void AnimateFloat_OnUpdate_m452184777 (AnimateFloat_t304404003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
