﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>
struct U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t479520291;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::.ctor()
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1__ctor_m608354489_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1__ctor_m608354489(__this, method) ((  void (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1__ctor_m608354489_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppChar U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m895775170_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m895775170(__this, method) ((  Il2CppChar (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m895775170_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerator_get_Current_m2427578199_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerator_get_Current_m2427578199(__this, method) ((  Il2CppObject * (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerator_get_Current_m2427578199_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerable_GetEnumerator_m2377074168_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerable_GetEnumerator_m2377074168(__this, method) ((  Il2CppObject * (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_IEnumerable_GetEnumerator_m2377074168_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m445165909_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m445165909(__this, method) ((  Il2CppObject* (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m445165909_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::MoveNext()
extern "C"  bool U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m790648419_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m790648419(__this, method) ((  bool (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_MoveNext_m790648419_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::Dispose()
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m411394742_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m411394742(__this, method) ((  void (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_Dispose_m411394742_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateUnionIterator>c__Iterator1C`1<System.Char>::Reset()
extern "C"  void U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m2549754726_gshared (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 * __this, const MethodInfo* method);
#define U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m2549754726(__this, method) ((  void (*) (U3CCreateUnionIteratorU3Ec__Iterator1C_1_t4293079946 *, const MethodInfo*))U3CCreateUnionIteratorU3Ec__Iterator1C_1_Reset_m2549754726_gshared)(__this, method)
