﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Attribute_t2523058482_0_0_0;
static const Il2CppType* GenInst_Attribute_t2523058482_0_0_0_Types[] = { &Attribute_t2523058482_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t2523058482_0_0_0 = { 1, GenInst_Attribute_t2523058482_0_0_0_Types };
extern const Il2CppType _Attribute_t3253047175_0_0_0;
static const Il2CppType* GenInst__Attribute_t3253047175_0_0_0_Types[] = { &_Attribute_t3253047175_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t3253047175_0_0_0 = { 1, GenInst__Attribute_t3253047175_0_0_0_Types };
extern const Il2CppType Int32_t1153838500_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0 = { 1, GenInst_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType Char_t2862622538_0_0_0;
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0 = { 1, GenInst_Char_t2862622538_0_0_0_Types };
extern const Il2CppType IConvertible_t2116191568_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2116191568_0_0_0_Types[] = { &IConvertible_t2116191568_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2116191568_0_0_0 = { 1, GenInst_IConvertible_t2116191568_0_0_0_Types };
extern const Il2CppType IComparable_t1391370361_0_0_0;
static const Il2CppType* GenInst_IComparable_t1391370361_0_0_0_Types[] = { &IComparable_t1391370361_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1391370361_0_0_0 = { 1, GenInst_IComparable_t1391370361_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772552329_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772552329_0_0_0_Types[] = { &IComparable_1_t2772552329_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772552329_0_0_0 = { 1, GenInst_IComparable_1_t2772552329_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411901105_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411901105_0_0_0_Types[] = { &IEquatable_1_t2411901105_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411901105_0_0_0 = { 1, GenInst_IEquatable_1_t2411901105_0_0_0_Types };
extern const Il2CppType ValueType_t1744280289_0_0_0;
static const Il2CppType* GenInst_ValueType_t1744280289_0_0_0_Types[] = { &ValueType_t1744280289_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1744280289_0_0_0 = { 1, GenInst_ValueType_t1744280289_0_0_0_Types };
extern const Il2CppType Int64_t1153838595_0_0_0;
static const Il2CppType* GenInst_Int64_t1153838595_0_0_0_Types[] = { &Int64_t1153838595_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1153838595_0_0_0 = { 1, GenInst_Int64_t1153838595_0_0_0_Types };
extern const Il2CppType UInt32_t24667981_0_0_0;
static const Il2CppType* GenInst_UInt32_t24667981_0_0_0_Types[] = { &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t24667981_0_0_0 = { 1, GenInst_UInt32_t24667981_0_0_0_Types };
extern const Il2CppType UInt64_t24668076_0_0_0;
static const Il2CppType* GenInst_UInt64_t24668076_0_0_0_Types[] = { &UInt64_t24668076_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t24668076_0_0_0 = { 1, GenInst_UInt64_t24668076_0_0_0_Types };
extern const Il2CppType Byte_t2862609660_0_0_0;
static const Il2CppType* GenInst_Byte_t2862609660_0_0_0_Types[] = { &Byte_t2862609660_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2862609660_0_0_0 = { 1, GenInst_Byte_t2862609660_0_0_0_Types };
extern const Il2CppType SByte_t1161769777_0_0_0;
static const Il2CppType* GenInst_SByte_t1161769777_0_0_0_Types[] = { &SByte_t1161769777_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1161769777_0_0_0 = { 1, GenInst_SByte_t1161769777_0_0_0_Types };
extern const Il2CppType Int16_t1153838442_0_0_0;
static const Il2CppType* GenInst_Int16_t1153838442_0_0_0_Types[] = { &Int16_t1153838442_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1153838442_0_0_0 = { 1, GenInst_Int16_t1153838442_0_0_0_Types };
extern const Il2CppType UInt16_t24667923_0_0_0;
static const Il2CppType* GenInst_UInt16_t24667923_0_0_0_Types[] = { &UInt16_t24667923_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t24667923_0_0_0 = { 1, GenInst_UInt16_t24667923_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t3464557803_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t3464557803_0_0_0_Types[] = { &IEnumerable_t3464557803_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t3464557803_0_0_0 = { 1, GenInst_IEnumerable_t3464557803_0_0_0_Types };
extern const Il2CppType ICloneable_t1025544834_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1025544834_0_0_0_Types[] = { &ICloneable_t1025544834_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1025544834_0_0_0 = { 1, GenInst_ICloneable_t1025544834_0_0_0_Types };
extern const Il2CppType IComparable_1_t4212128644_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4212128644_0_0_0_Types[] = { &IComparable_1_t4212128644_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4212128644_0_0_0 = { 1, GenInst_IComparable_1_t4212128644_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3851477420_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3851477420_0_0_0_Types[] = { &IEquatable_1_t3851477420_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3851477420_0_0_0 = { 1, GenInst_IEquatable_1_t3851477420_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t2853506214_0_0_0;
static const Il2CppType* GenInst_IReflect_t2853506214_0_0_0_Types[] = { &IReflect_t2853506214_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2853506214_0_0_0 = { 1, GenInst_IReflect_t2853506214_0_0_0_Types };
extern const Il2CppType _Type_t2149739635_0_0_0;
static const Il2CppType* GenInst__Type_t2149739635_0_0_0_Types[] = { &_Type_t2149739635_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2149739635_0_0_0 = { 1, GenInst__Type_t2149739635_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1425685797_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types[] = { &ICustomAttributeProvider_t1425685797_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1425685797_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1425685797_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3353101921_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3353101921_0_0_0_Types[] = { &_MemberInfo_t3353101921_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3353101921_0_0_0 = { 1, GenInst__MemberInfo_t3353101921_0_0_0_Types };
extern const Il2CppType IFormattable_t382002946_0_0_0;
static const Il2CppType* GenInst_IFormattable_t382002946_0_0_0_Types[] = { &IFormattable_t382002946_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t382002946_0_0_0 = { 1, GenInst_IFormattable_t382002946_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768291_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768291_0_0_0_Types[] = { &IComparable_1_t1063768291_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768291_0_0_0 = { 1, GenInst_IComparable_1_t1063768291_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117067_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117067_0_0_0_Types[] = { &IEquatable_1_t703117067_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117067_0_0_0 = { 1, GenInst_IEquatable_1_t703117067_0_0_0_Types };
extern const Il2CppType Double_t3868226565_0_0_0;
static const Il2CppType* GenInst_Double_t3868226565_0_0_0_Types[] = { &Double_t3868226565_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t3868226565_0_0_0 = { 1, GenInst_Double_t3868226565_0_0_0_Types };
extern const Il2CppType IComparable_1_t3778156356_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3778156356_0_0_0_Types[] = { &IComparable_1_t3778156356_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3778156356_0_0_0 = { 1, GenInst_IComparable_1_t3778156356_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3417505132_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3417505132_0_0_0_Types[] = { &IEquatable_1_t3417505132_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3417505132_0_0_0 = { 1, GenInst_IEquatable_1_t3417505132_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565068_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565068_0_0_0_Types[] = { &IComparable_1_t4229565068_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565068_0_0_0 = { 1, GenInst_IComparable_1_t4229565068_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913844_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913844_0_0_0_Types[] = { &IEquatable_1_t3868913844_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913844_0_0_0 = { 1, GenInst_IEquatable_1_t3868913844_0_0_0_Types };
extern const Il2CppType IComparable_1_t2772539451_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2772539451_0_0_0_Types[] = { &IComparable_1_t2772539451_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2772539451_0_0_0 = { 1, GenInst_IComparable_1_t2772539451_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2411888227_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2411888227_0_0_0_Types[] = { &IEquatable_1_t2411888227_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2411888227_0_0_0 = { 1, GenInst_IEquatable_1_t2411888227_0_0_0_Types };
extern const Il2CppType Single_t4291918972_0_0_0;
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0 = { 1, GenInst_Single_t4291918972_0_0_0_Types };
extern const Il2CppType IComparable_1_t4201848763_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4201848763_0_0_0_Types[] = { &IComparable_1_t4201848763_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4201848763_0_0_0 = { 1, GenInst_IComparable_1_t4201848763_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3841197539_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3841197539_0_0_0_Types[] = { &IEquatable_1_t3841197539_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3841197539_0_0_0 = { 1, GenInst_IEquatable_1_t3841197539_0_0_0_Types };
extern const Il2CppType Decimal_t1954350631_0_0_0;
static const Il2CppType* GenInst_Decimal_t1954350631_0_0_0_Types[] = { &Decimal_t1954350631_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1954350631_0_0_0 = { 1, GenInst_Decimal_t1954350631_0_0_0_Types };
extern const Il2CppType Boolean_t476798718_0_0_0;
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0 = { 1, GenInst_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Delegate_t3310234105_0_0_0;
static const Il2CppType* GenInst_Delegate_t3310234105_0_0_0_Types[] = { &Delegate_t3310234105_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3310234105_0_0_0 = { 1, GenInst_Delegate_t3310234105_0_0_0_Types };
extern const Il2CppType ISerializable_t867484142_0_0_0;
static const Il2CppType* GenInst_ISerializable_t867484142_0_0_0_Types[] = { &ISerializable_t867484142_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t867484142_0_0_0 = { 1, GenInst_ISerializable_t867484142_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2235474049_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2235474049_0_0_0_Types[] = { &ParameterInfo_t2235474049_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2235474049_0_0_0 = { 1, GenInst_ParameterInfo_t2235474049_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2787166306_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2787166306_0_0_0_Types[] = { &_ParameterInfo_t2787166306_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2787166306_0_0_0 = { 1, GenInst__ParameterInfo_t2787166306_0_0_0_Types };
extern const Il2CppType ParameterModifier_t741930026_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t741930026_0_0_0_Types[] = { &ParameterModifier_t741930026_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t741930026_0_0_0 = { 1, GenInst_ParameterModifier_t741930026_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565010_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565010_0_0_0_Types[] = { &IComparable_1_t4229565010_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565010_0_0_0 = { 1, GenInst_IComparable_1_t4229565010_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913786_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913786_0_0_0_Types[] = { &IEquatable_1_t3868913786_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913786_0_0_0 = { 1, GenInst_IEquatable_1_t3868913786_0_0_0_Types };
extern const Il2CppType IComparable_1_t4229565163_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4229565163_0_0_0_Types[] = { &IComparable_1_t4229565163_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4229565163_0_0_0 = { 1, GenInst_IComparable_1_t4229565163_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3868913939_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3868913939_0_0_0_Types[] = { &IEquatable_1_t3868913939_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3868913939_0_0_0 = { 1, GenInst_IEquatable_1_t3868913939_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768233_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768233_0_0_0_Types[] = { &IComparable_1_t1063768233_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768233_0_0_0 = { 1, GenInst_IComparable_1_t1063768233_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117009_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117009_0_0_0_Types[] = { &IEquatable_1_t703117009_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117009_0_0_0 = { 1, GenInst_IEquatable_1_t703117009_0_0_0_Types };
extern const Il2CppType IComparable_1_t1071699568_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1071699568_0_0_0_Types[] = { &IComparable_1_t1071699568_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1071699568_0_0_0 = { 1, GenInst_IComparable_1_t1071699568_0_0_0_Types };
extern const Il2CppType IEquatable_1_t711048344_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t711048344_0_0_0_Types[] = { &IEquatable_1_t711048344_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t711048344_0_0_0 = { 1, GenInst_IEquatable_1_t711048344_0_0_0_Types };
extern const Il2CppType IComparable_1_t1063768386_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1063768386_0_0_0_Types[] = { &IComparable_1_t1063768386_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1063768386_0_0_0 = { 1, GenInst_IComparable_1_t1063768386_0_0_0_Types };
extern const Il2CppType IEquatable_1_t703117162_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t703117162_0_0_0_Types[] = { &IEquatable_1_t703117162_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t703117162_0_0_0 = { 1, GenInst_IEquatable_1_t703117162_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t3271054163_0_0_0;
static const Il2CppType* GenInst__EventInfo_t3271054163_0_0_0_Types[] = { &_EventInfo_t3271054163_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t3271054163_0_0_0 = { 1, GenInst__EventInfo_t3271054163_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t209867187_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t209867187_0_0_0_Types[] = { &_FieldInfo_t209867187_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t209867187_0_0_0 = { 1, GenInst__FieldInfo_t209867187_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3971289384_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3971289384_0_0_0_Types[] = { &_MethodInfo_t3971289384_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3971289384_0_0_0 = { 1, GenInst__MethodInfo_t3971289384_0_0_0_Types };
extern const Il2CppType MethodBase_t318515428_0_0_0;
static const Il2CppType* GenInst_MethodBase_t318515428_0_0_0_Types[] = { &MethodBase_t318515428_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t318515428_0_0_0 = { 1, GenInst_MethodBase_t318515428_0_0_0_Types };
extern const Il2CppType _MethodBase_t3971068747_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3971068747_0_0_0_Types[] = { &_MethodBase_t3971068747_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3971068747_0_0_0 = { 1, GenInst__MethodBase_t3971068747_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3711326812_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3711326812_0_0_0_Types[] = { &_PropertyInfo_t3711326812_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3711326812_0_0_0 = { 1, GenInst__PropertyInfo_t3711326812_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t4136801618_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t4136801618_0_0_0_Types[] = { &ConstructorInfo_t4136801618_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t4136801618_0_0_0 = { 1, GenInst_ConstructorInfo_t4136801618_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3408715251_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3408715251_0_0_0_Types[] = { &_ConstructorInfo_t3408715251_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3408715251_0_0_0 = { 1, GenInst__ConstructorInfo_t3408715251_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t3372848153_0_0_0;
static const Il2CppType* GenInst_TableRange_t3372848153_0_0_0_Types[] = { &TableRange_t3372848153_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t3372848153_0_0_0 = { 1, GenInst_TableRange_t3372848153_0_0_0_Types };
extern const Il2CppType TailoringInfo_t3025807515_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t3025807515_0_0_0_Types[] = { &TailoringInfo_t3025807515_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t3025807515_0_0_0 = { 1, GenInst_TailoringInfo_t3025807515_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3222658402_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0 = { 1, GenInst_KeyValuePair_2_t3222658402_0_0_0_Types };
extern const Il2CppType Link_t2063667470_0_0_0;
static const Il2CppType* GenInst_Link_t2063667470_0_0_0_Types[] = { &Link_t2063667470_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2063667470_0_0_0 = { 1, GenInst_Link_t2063667470_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1751606614_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0 = { 1, GenInst_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1873037576_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1873037576_0_0_0_Types[] = { &KeyValuePair_2_t1873037576_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1873037576_0_0_0 = { 1, GenInst_KeyValuePair_2_t1873037576_0_0_0_Types };
extern const Il2CppType Contraction_t3998770676_0_0_0;
static const Il2CppType* GenInst_Contraction_t3998770676_0_0_0_Types[] = { &Contraction_t3998770676_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t3998770676_0_0_0 = { 1, GenInst_Contraction_t3998770676_0_0_0_Types };
extern const Il2CppType Level2Map_t3664214860_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3664214860_0_0_0_Types[] = { &Level2Map_t3664214860_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3664214860_0_0_0 = { 1, GenInst_Level2Map_t3664214860_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373498_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373498_0_0_0_Types[] = { &BigInteger_t3334373498_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373498_0_0_0 = { 1, GenInst_BigInteger_t3334373498_0_0_0_Types };
extern const Il2CppType KeySizes_t2106826975_0_0_0;
static const Il2CppType* GenInst_KeySizes_t2106826975_0_0_0_Types[] = { &KeySizes_t2106826975_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t2106826975_0_0_0 = { 1, GenInst_KeySizes_t2106826975_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1944668977_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0 = { 1, GenInst_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
extern const Il2CppType IComparable_1_t386728509_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t386728509_0_0_0_Types[] = { &IComparable_1_t386728509_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t386728509_0_0_0 = { 1, GenInst_IComparable_1_t386728509_0_0_0_Types };
extern const Il2CppType IEquatable_1_t26077285_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t26077285_0_0_0_Types[] = { &IEquatable_1_t26077285_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t26077285_0_0_0 = { 1, GenInst_IEquatable_1_t26077285_0_0_0_Types };
extern const Il2CppType Slot_t2260530181_0_0_0;
static const Il2CppType* GenInst_Slot_t2260530181_0_0_0_Types[] = { &Slot_t2260530181_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2260530181_0_0_0 = { 1, GenInst_Slot_t2260530181_0_0_0_Types };
extern const Il2CppType Slot_t2072023290_0_0_0;
static const Il2CppType* GenInst_Slot_t2072023290_0_0_0_Types[] = { &Slot_t2072023290_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2072023290_0_0_0 = { 1, GenInst_Slot_t2072023290_0_0_0_Types };
extern const Il2CppType StackFrame_t1034942277_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1034942277_0_0_0_Types[] = { &StackFrame_t1034942277_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1034942277_0_0_0 = { 1, GenInst_StackFrame_t1034942277_0_0_0_Types };
extern const Il2CppType Calendar_t3558528576_0_0_0;
static const Il2CppType* GenInst_Calendar_t3558528576_0_0_0_Types[] = { &Calendar_t3558528576_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t3558528576_0_0_0 = { 1, GenInst_Calendar_t3558528576_0_0_0_Types };
extern const Il2CppType CultureInfo_t1065375142_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t1065375142_0_0_0_Types[] = { &CultureInfo_t1065375142_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t1065375142_0_0_0 = { 1, GenInst_CultureInfo_t1065375142_0_0_0_Types };
extern const Il2CppType IFormatProvider_t192740775_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t192740775_0_0_0_Types[] = { &IFormatProvider_t192740775_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t192740775_0_0_0 = { 1, GenInst_IFormatProvider_t192740775_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t595214213_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t595214213_0_0_0_Types[] = { &ModuleBuilder_t595214213_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t595214213_0_0_0 = { 1, GenInst_ModuleBuilder_t595214213_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1764509690_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1764509690_0_0_0_Types[] = { &_ModuleBuilder_t1764509690_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1764509690_0_0_0 = { 1, GenInst__ModuleBuilder_t1764509690_0_0_0_Types };
extern const Il2CppType Module_t1394482686_0_0_0;
static const Il2CppType* GenInst_Module_t1394482686_0_0_0_Types[] = { &Module_t1394482686_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1394482686_0_0_0 = { 1, GenInst_Module_t1394482686_0_0_0_Types };
extern const Il2CppType _Module_t2601912805_0_0_0;
static const Il2CppType* GenInst__Module_t2601912805_0_0_0_Types[] = { &_Module_t2601912805_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2601912805_0_0_0 = { 1, GenInst__Module_t2601912805_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3159962230_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3159962230_0_0_0_Types[] = { &ParameterBuilder_t3159962230_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3159962230_0_0_0 = { 1, GenInst_ParameterBuilder_t3159962230_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t4122453611_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t4122453611_0_0_0_Types[] = { &_ParameterBuilder_t4122453611_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t4122453611_0_0_0 = { 1, GenInst__ParameterBuilder_t4122453611_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3339007067_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3339007067_0_0_0_Types[] = { &TypeU5BU5D_t3339007067_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3339007067_0_0_0 = { 1, GenInst_TypeU5BU5D_t3339007067_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t2643922881_0_0_0;
static const Il2CppType* GenInst_ICollection_t2643922881_0_0_0_Types[] = { &ICollection_t2643922881_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t2643922881_0_0_0 = { 1, GenInst_ICollection_t2643922881_0_0_0_Types };
extern const Il2CppType IList_t1751339649_0_0_0;
static const Il2CppType* GenInst_IList_t1751339649_0_0_0_Types[] = { &IList_t1751339649_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1751339649_0_0_0 = { 1, GenInst_IList_t1751339649_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1354080954_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1354080954_0_0_0_Types[] = { &ILTokenInfo_t1354080954_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1354080954_0_0_0 = { 1, GenInst_ILTokenInfo_t1354080954_0_0_0_Types };
extern const Il2CppType LabelData_t3207823784_0_0_0;
static const Il2CppType* GenInst_LabelData_t3207823784_0_0_0_Types[] = { &LabelData_t3207823784_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3207823784_0_0_0 = { 1, GenInst_LabelData_t3207823784_0_0_0_Types };
extern const Il2CppType LabelFixup_t660379442_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t660379442_0_0_0_Types[] = { &LabelFixup_t660379442_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t660379442_0_0_0 = { 1, GenInst_LabelFixup_t660379442_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t553556921_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types[] = { &GenericTypeParameterBuilder_t553556921_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t553556921_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t553556921_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1918497079_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1918497079_0_0_0_Types[] = { &TypeBuilder_t1918497079_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1918497079_0_0_0 = { 1, GenInst_TypeBuilder_t1918497079_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3501492652_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3501492652_0_0_0_Types[] = { &_TypeBuilder_t3501492652_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3501492652_0_0_0 = { 1, GenInst__TypeBuilder_t3501492652_0_0_0_Types };
extern const Il2CppType MethodBuilder_t302405488_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t302405488_0_0_0_Types[] = { &MethodBuilder_t302405488_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t302405488_0_0_0 = { 1, GenInst_MethodBuilder_t302405488_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1471700965_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1471700965_0_0_0_Types[] = { &_MethodBuilder_t1471700965_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1471700965_0_0_0 = { 1, GenInst__MethodBuilder_t1471700965_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t3217839941_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t3217839941_0_0_0_Types[] = { &ConstructorBuilder_t3217839941_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t3217839941_0_0_0 = { 1, GenInst_ConstructorBuilder_t3217839941_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t788093754_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t788093754_0_0_0_Types[] = { &_ConstructorBuilder_t788093754_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t788093754_0_0_0 = { 1, GenInst__ConstructorBuilder_t788093754_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t2012258748_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t2012258748_0_0_0_Types[] = { &PropertyBuilder_t2012258748_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t2012258748_0_0_0 = { 1, GenInst_PropertyBuilder_t2012258748_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t752753201_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t752753201_0_0_0_Types[] = { &_PropertyBuilder_t752753201_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t752753201_0_0_0 = { 1, GenInst__PropertyBuilder_t752753201_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1754069893_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1754069893_0_0_0_Types[] = { &FieldBuilder_t1754069893_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1754069893_0_0_0 = { 1, GenInst_FieldBuilder_t1754069893_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t639782778_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t639782778_0_0_0_Types[] = { &_FieldBuilder_t639782778_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t639782778_0_0_0 = { 1, GenInst__FieldBuilder_t639782778_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t3301293422_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types[] = { &CustomAttributeTypedArgument_t3301293422_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t3059612989_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types[] = { &CustomAttributeNamedArgument_t3059612989_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t2955630591_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t2955630591_0_0_0_Types[] = { &CustomAttributeData_t2955630591_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2955630591_0_0_0 = { 1, GenInst_CustomAttributeData_t2955630591_0_0_0_Types };
extern const Il2CppType ResourceInfo_t4013605874_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t4013605874_0_0_0_Types[] = { &ResourceInfo_t4013605874_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t4013605874_0_0_0 = { 1, GenInst_ResourceInfo_t4013605874_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t2113902833_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t2113902833_0_0_0_Types[] = { &ResourceCacheItem_t2113902833_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t2113902833_0_0_0 = { 1, GenInst_ResourceCacheItem_t2113902833_0_0_0_Types };
extern const Il2CppType IContextProperty_t82913453_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t82913453_0_0_0_Types[] = { &IContextProperty_t82913453_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t82913453_0_0_0 = { 1, GenInst_IContextProperty_t82913453_0_0_0_Types };
extern const Il2CppType Header_t1689611527_0_0_0;
static const Il2CppType* GenInst_Header_t1689611527_0_0_0_Types[] = { &Header_t1689611527_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1689611527_0_0_0 = { 1, GenInst_Header_t1689611527_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2228500544_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2228500544_0_0_0_Types[] = { &ITrackingHandler_t2228500544_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2228500544_0_0_0 = { 1, GenInst_ITrackingHandler_t2228500544_0_0_0_Types };
extern const Il2CppType IContextAttribute_t3913746816_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t3913746816_0_0_0_Types[] = { &IContextAttribute_t3913746816_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t3913746816_0_0_0 = { 1, GenInst_IContextAttribute_t3913746816_0_0_0_Types };
extern const Il2CppType DateTime_t4283661327_0_0_0;
static const Il2CppType* GenInst_DateTime_t4283661327_0_0_0_Types[] = { &DateTime_t4283661327_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t4283661327_0_0_0 = { 1, GenInst_DateTime_t4283661327_0_0_0_Types };
extern const Il2CppType IComparable_1_t4193591118_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4193591118_0_0_0_Types[] = { &IComparable_1_t4193591118_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4193591118_0_0_0 = { 1, GenInst_IComparable_1_t4193591118_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3832939894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3832939894_0_0_0_Types[] = { &IEquatable_1_t3832939894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3832939894_0_0_0 = { 1, GenInst_IEquatable_1_t3832939894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1864280422_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1864280422_0_0_0_Types[] = { &IComparable_1_t1864280422_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1864280422_0_0_0 = { 1, GenInst_IComparable_1_t1864280422_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1503629198_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1503629198_0_0_0_Types[] = { &IEquatable_1_t1503629198_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1503629198_0_0_0 = { 1, GenInst_IEquatable_1_t1503629198_0_0_0_Types };
extern const Il2CppType TimeSpan_t413522987_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t413522987_0_0_0_Types[] = { &TimeSpan_t413522987_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t413522987_0_0_0 = { 1, GenInst_TimeSpan_t413522987_0_0_0_Types };
extern const Il2CppType IComparable_1_t323452778_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t323452778_0_0_0_Types[] = { &IComparable_1_t323452778_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t323452778_0_0_0 = { 1, GenInst_IComparable_1_t323452778_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4257768850_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4257768850_0_0_0_Types[] = { &IEquatable_1_t4257768850_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4257768850_0_0_0 = { 1, GenInst_IEquatable_1_t4257768850_0_0_0_Types };
extern const Il2CppType TypeTag_t2420703430_0_0_0;
static const Il2CppType* GenInst_TypeTag_t2420703430_0_0_0_Types[] = { &TypeTag_t2420703430_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t2420703430_0_0_0 = { 1, GenInst_TypeTag_t2420703430_0_0_0_Types };
extern const Il2CppType Enum_t2862688501_0_0_0;
static const Il2CppType* GenInst_Enum_t2862688501_0_0_0_Types[] = { &Enum_t2862688501_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2862688501_0_0_0 = { 1, GenInst_Enum_t2862688501_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t2878058698_0_0_0;
static const Il2CppType* GenInst_StrongName_t2878058698_0_0_0_Types[] = { &StrongName_t2878058698_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2878058698_0_0_0 = { 1, GenInst_StrongName_t2878058698_0_0_0_Types };
extern const Il2CppType Assembly_t1418687608_0_0_0;
static const Il2CppType* GenInst_Assembly_t1418687608_0_0_0_Types[] = { &Assembly_t1418687608_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t1418687608_0_0_0 = { 1, GenInst_Assembly_t1418687608_0_0_0_Types };
extern const Il2CppType _Assembly_t3789461407_0_0_0;
static const Il2CppType* GenInst__Assembly_t3789461407_0_0_0_Types[] = { &_Assembly_t3789461407_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t3789461407_0_0_0 = { 1, GenInst__Assembly_t3789461407_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3884714306_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3884714306_0_0_0_Types[] = { &DateTimeOffset_t3884714306_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3884714306_0_0_0 = { 1, GenInst_DateTimeOffset_t3884714306_0_0_0_Types };
extern const Il2CppType Guid_t2862754429_0_0_0;
static const Il2CppType* GenInst_Guid_t2862754429_0_0_0_Types[] = { &Guid_t2862754429_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2862754429_0_0_0 = { 1, GenInst_Guid_t2862754429_0_0_0_Types };
extern const Il2CppType Version_t763695022_0_0_0;
static const Il2CppType* GenInst_Version_t763695022_0_0_0_Types[] = { &Version_t763695022_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t763695022_0_0_0 = { 1, GenInst_Version_t763695022_0_0_0_Types };
extern const Il2CppType BigInteger_t3334373499_0_0_0;
static const Il2CppType* GenInst_BigInteger_t3334373499_0_0_0_Types[] = { &BigInteger_t3334373499_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t3334373499_0_0_0 = { 1, GenInst_BigInteger_t3334373499_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t4260760469_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4260760469_0_0_0_Types[] = { &ByteU5BU5D_t4260760469_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4260760469_0_0_0 = { 1, GenInst_ByteU5BU5D_t4260760469_0_0_0_Types };
extern const Il2CppType X509Certificate_t3076817455_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t3076817455_0_0_0_Types[] = { &X509Certificate_t3076817455_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t3076817455_0_0_0 = { 1, GenInst_X509Certificate_t3076817455_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t675596727_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t675596727_0_0_0_Types[] = { &IDeserializationCallback_t675596727_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t675596727_0_0_0 = { 1, GenInst_IDeserializationCallback_t675596727_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t3167042548_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t3167042548_0_0_0_Types[] = { &ClientCertificateType_t3167042548_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t3167042548_0_0_0 = { 1, GenInst_ClientCertificateType_t3167042548_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t2073374448_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t2073374448_0_0_0_Types[] = { &PropertyDescriptor_t2073374448_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t2073374448_0_0_0 = { 1, GenInst_PropertyDescriptor_t2073374448_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t2617136693_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t2617136693_0_0_0_Types[] = { &MemberDescriptor_t2617136693_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t2617136693_0_0_0 = { 1, GenInst_MemberDescriptor_t2617136693_0_0_0_Types };
extern const Il2CppType EventDescriptor_t1405012495_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t1405012495_0_0_0_Types[] = { &EventDescriptor_t1405012495_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t1405012495_0_0_0 = { 1, GenInst_EventDescriptor_t1405012495_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t4055800263_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t4055800263_0_0_0_Types[] = { &AttributeU5BU5D_t4055800263_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t4055800263_0_0_0 = { 1, GenInst_AttributeU5BU5D_t4055800263_0_0_0_Types };
extern const Il2CppType LinkedList_1_t1417720186_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t1417720186_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t3543085017_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t3543085017_0_0_0_Types[] = { &TypeDescriptionProvider_t3543085017_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t3543085017_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t3543085017_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t1417720186_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1421923377_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1421923377_0_0_0_Types[] = { &KeyValuePair_2_t1421923377_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1421923377_0_0_0 = { 1, GenInst_KeyValuePair_2_t1421923377_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t1518976226_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0, &LinkedList_1_t1417720186_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0 = { 2, GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0 = { 1, GenInst_WeakObjectWrapper_t1518976226_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &WeakObjectWrapper_t1518976226_0_0_0, &LinkedList_1_t1417720186_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472936237_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472936237_0_0_0_Types[] = { &KeyValuePair_2_t1472936237_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472936237_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472936237_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t766901931_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t766901931_0_0_0_Types[] = { &X509ChainStatus_t766901931_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t766901931_0_0_0 = { 1, GenInst_X509ChainStatus_t766901931_0_0_0_Types };
extern const Il2CppType IPAddress_t3525271463_0_0_0;
static const Il2CppType* GenInst_IPAddress_t3525271463_0_0_0_Types[] = { &IPAddress_t3525271463_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t3525271463_0_0_0 = { 1, GenInst_IPAddress_t3525271463_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2188033608_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2188033608_0_0_0_Types[] = { &ArraySegment_1_t2188033608_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2188033608_0_0_0 = { 1, GenInst_ArraySegment_1_t2188033608_0_0_0_Types };
extern const Il2CppType Cookie_t2033273982_0_0_0;
static const Il2CppType* GenInst_Cookie_t2033273982_0_0_0_Types[] = { &Cookie_t2033273982_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t2033273982_0_0_0 = { 1, GenInst_Cookie_t2033273982_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2545618620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0 = { 1, GenInst_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t476798718_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1195997794_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1195997794_0_0_0_Types[] = { &KeyValuePair_2_t1195997794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1195997794_0_0_0 = { 1, GenInst_KeyValuePair_2_t1195997794_0_0_0_Types };
extern const Il2CppType Capture_t754001812_0_0_0;
static const Il2CppType* GenInst_Capture_t754001812_0_0_0_Types[] = { &Capture_t754001812_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t754001812_0_0_0 = { 1, GenInst_Capture_t754001812_0_0_0_Types };
extern const Il2CppType Group_t2151468941_0_0_0;
static const Il2CppType* GenInst_Group_t2151468941_0_0_0_Types[] = { &Group_t2151468941_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t2151468941_0_0_0 = { 1, GenInst_Group_t2151468941_0_0_0_Types };
extern const Il2CppType Mark_t3811539797_0_0_0;
static const Il2CppType* GenInst_Mark_t3811539797_0_0_0_Types[] = { &Mark_t3811539797_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3811539797_0_0_0 = { 1, GenInst_Mark_t3811539797_0_0_0_Types };
extern const Il2CppType UriScheme_t1290668975_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1290668975_0_0_0_Types[] = { &UriScheme_t1290668975_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1290668975_0_0_0 = { 1, GenInst_UriScheme_t1290668975_0_0_0_Types };
extern const Il2CppType Link_t2122599155_0_0_0;
static const Il2CppType* GenInst_Link_t2122599155_0_0_0_Types[] = { &Link_t2122599155_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2122599155_0_0_0 = { 1, GenInst_Link_t2122599155_0_0_0_Types };
extern const Il2CppType List_1_t1244034627_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t1244034627_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t1244034627_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3312854529_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3312854529_0_0_0_Types[] = { &KeyValuePair_2_t3312854529_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312854529_0_0_0 = { 1, GenInst_KeyValuePair_2_t3312854529_0_0_0_Types };
extern const Il2CppType IGrouping_2_t641185655_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t641185655_0_0_0_Types[] = { &IGrouping_2_t641185655_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t641185655_0_0_0 = { 1, GenInst_IGrouping_2_t641185655_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3176762032_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IEnumerable_1_t3176762032_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType XPathNavigator_t1075073278_0_0_0;
static const Il2CppType* GenInst_XPathNavigator_t1075073278_0_0_0_Types[] = { &XPathNavigator_t1075073278_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNavigator_t1075073278_0_0_0 = { 1, GenInst_XPathNavigator_t1075073278_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t153087709_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t153087709_0_0_0_Types[] = { &IXPathNavigable_t153087709_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t153087709_0_0_0 = { 1, GenInst_IXPathNavigable_t153087709_0_0_0_Types };
extern const Il2CppType IXmlNamespaceResolver_t3774973253_0_0_0;
static const Il2CppType* GenInst_IXmlNamespaceResolver_t3774973253_0_0_0_Types[] = { &IXmlNamespaceResolver_t3774973253_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlNamespaceResolver_t3774973253_0_0_0 = { 1, GenInst_IXmlNamespaceResolver_t3774973253_0_0_0_Types };
extern const Il2CppType XPathItem_t3597956134_0_0_0;
static const Il2CppType* GenInst_XPathItem_t3597956134_0_0_0_Types[] = { &XPathItem_t3597956134_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathItem_t3597956134_0_0_0 = { 1, GenInst_XPathItem_t3597956134_0_0_0_Types };
extern const Il2CppType XPathResultType_t516720010_0_0_0;
static const Il2CppType* GenInst_XPathResultType_t516720010_0_0_0_Types[] = { &XPathResultType_t516720010_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathResultType_t516720010_0_0_0 = { 1, GenInst_XPathResultType_t516720010_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2758969756_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2758969756_0_0_0_Types[] = { &KeyValuePair_2_t2758969756_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2758969756_0_0_0 = { 1, GenInst_KeyValuePair_2_t2758969756_0_0_0_Types };
extern const Il2CppType DTDNode_t2039770680_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t2039770680_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t2039770680_0_0_0_Types[] = { &DTDNode_t2039770680_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t2039770680_0_0_0 = { 1, GenInst_DTDNode_t2039770680_0_0_0_Types };
extern const Il2CppType Entry_t2866414864_0_0_0;
static const Il2CppType* GenInst_Entry_t2866414864_0_0_0_Types[] = { &Entry_t2866414864_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2866414864_0_0_0 = { 1, GenInst_Entry_t2866414864_0_0_0_Types };
extern const Il2CppType XmlNode_t856910923_0_0_0;
static const Il2CppType* GenInst_XmlNode_t856910923_0_0_0_Types[] = { &XmlNode_t856910923_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t856910923_0_0_0 = { 1, GenInst_XmlNode_t856910923_0_0_0_Types };
extern const Il2CppType NsDecl_t3658211563_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3658211563_0_0_0_Types[] = { &NsDecl_t3658211563_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3658211563_0_0_0 = { 1, GenInst_NsDecl_t3658211563_0_0_0_Types };
extern const Il2CppType NsScope_t1749213747_0_0_0;
static const Il2CppType* GenInst_NsScope_t1749213747_0_0_0_Types[] = { &NsScope_t1749213747_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t1749213747_0_0_0 = { 1, GenInst_NsScope_t1749213747_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t982414386_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t982414386_0_0_0_Types[] = { &XmlAttributeTokenInfo_t982414386_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t982414386_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t982414386_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t597808448_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t597808448_0_0_0_Types[] = { &XmlTokenInfo_t597808448_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t597808448_0_0_0 = { 1, GenInst_XmlTokenInfo_t597808448_0_0_0_Types };
extern const Il2CppType TagName_t2016006645_0_0_0;
static const Il2CppType* GenInst_TagName_t2016006645_0_0_0_Types[] = { &TagName_t2016006645_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2016006645_0_0_0 = { 1, GenInst_TagName_t2016006645_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t1808742809_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t1808742809_0_0_0_Types[] = { &XmlNodeInfo_t1808742809_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t1808742809_0_0_0 = { 1, GenInst_XmlNodeInfo_t1808742809_0_0_0_Types };
extern const Il2CppType ReferenceLoopHandling_t2761661122_0_0_0;
static const Il2CppType* GenInst_ReferenceLoopHandling_t2761661122_0_0_0_Types[] = { &ReferenceLoopHandling_t2761661122_0_0_0 };
extern const Il2CppGenericInst GenInst_ReferenceLoopHandling_t2761661122_0_0_0 = { 1, GenInst_ReferenceLoopHandling_t2761661122_0_0_0_Types };
extern const Il2CppType TypeNameHandling_t2359325474_0_0_0;
static const Il2CppType* GenInst_TypeNameHandling_t2359325474_0_0_0_Types[] = { &TypeNameHandling_t2359325474_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameHandling_t2359325474_0_0_0 = { 1, GenInst_TypeNameHandling_t2359325474_0_0_0_Types };
extern const Il2CppType JsonSerializerSettings_t2589405525_0_0_0;
static const Il2CppType* GenInst_JsonSerializerSettings_t2589405525_0_0_0_Types[] = { &JsonSerializerSettings_t2589405525_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonSerializerSettings_t2589405525_0_0_0 = { 1, GenInst_JsonSerializerSettings_t2589405525_0_0_0_Types };
extern const Il2CppType JsonConverter_t2159686854_0_0_0;
static const Il2CppType* GenInst_JsonConverter_t2159686854_0_0_0_Types[] = { &JsonConverter_t2159686854_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConverter_t2159686854_0_0_0 = { 1, GenInst_JsonConverter_t2159686854_0_0_0_Types };
extern const Il2CppType Required_t3921306327_0_0_0;
static const Il2CppType* GenInst_Required_t3921306327_0_0_0_Types[] = { &Required_t3921306327_0_0_0 };
extern const Il2CppGenericInst GenInst_Required_t3921306327_0_0_0 = { 1, GenInst_Required_t3921306327_0_0_0_Types };
extern const Il2CppType JsonPosition_t3864946409_0_0_0;
static const Il2CppType* GenInst_JsonPosition_t3864946409_0_0_0_Types[] = { &JsonPosition_t3864946409_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonPosition_t3864946409_0_0_0 = { 1, GenInst_JsonPosition_t3864946409_0_0_0_Types };
extern const Il2CppType NullValueHandling_t2754652381_0_0_0;
static const Il2CppType* GenInst_NullValueHandling_t2754652381_0_0_0_Types[] = { &NullValueHandling_t2754652381_0_0_0 };
extern const Il2CppGenericInst GenInst_NullValueHandling_t2754652381_0_0_0 = { 1, GenInst_NullValueHandling_t2754652381_0_0_0_Types };
extern const Il2CppType DefaultValueHandling_t1569448045_0_0_0;
static const Il2CppType* GenInst_DefaultValueHandling_t1569448045_0_0_0_Types[] = { &DefaultValueHandling_t1569448045_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultValueHandling_t1569448045_0_0_0 = { 1, GenInst_DefaultValueHandling_t1569448045_0_0_0_Types };
extern const Il2CppType ObjectCreationHandling_t56081595_0_0_0;
static const Il2CppType* GenInst_ObjectCreationHandling_t56081595_0_0_0_Types[] = { &ObjectCreationHandling_t56081595_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectCreationHandling_t56081595_0_0_0 = { 1, GenInst_ObjectCreationHandling_t56081595_0_0_0_Types };
extern const Il2CppType Formatting_t732683613_0_0_0;
static const Il2CppType* GenInst_Formatting_t732683613_0_0_0_Types[] = { &Formatting_t732683613_0_0_0 };
extern const Il2CppGenericInst GenInst_Formatting_t732683613_0_0_0 = { 1, GenInst_Formatting_t732683613_0_0_0_Types };
extern const Il2CppType DateFormatHandling_t4014082626_0_0_0;
static const Il2CppType* GenInst_DateFormatHandling_t4014082626_0_0_0_Types[] = { &DateFormatHandling_t4014082626_0_0_0 };
extern const Il2CppGenericInst GenInst_DateFormatHandling_t4014082626_0_0_0 = { 1, GenInst_DateFormatHandling_t4014082626_0_0_0_Types };
extern const Il2CppType DateTimeZoneHandling_t2945560484_0_0_0;
static const Il2CppType* GenInst_DateTimeZoneHandling_t2945560484_0_0_0_Types[] = { &DateTimeZoneHandling_t2945560484_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeZoneHandling_t2945560484_0_0_0 = { 1, GenInst_DateTimeZoneHandling_t2945560484_0_0_0_Types };
extern const Il2CppType DateParseHandling_t2108333400_0_0_0;
static const Il2CppType* GenInst_DateParseHandling_t2108333400_0_0_0_Types[] = { &DateParseHandling_t2108333400_0_0_0 };
extern const Il2CppGenericInst GenInst_DateParseHandling_t2108333400_0_0_0 = { 1, GenInst_DateParseHandling_t2108333400_0_0_0_Types };
extern const Il2CppType FloatFormatHandling_t3887485542_0_0_0;
static const Il2CppType* GenInst_FloatFormatHandling_t3887485542_0_0_0_Types[] = { &FloatFormatHandling_t3887485542_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatFormatHandling_t3887485542_0_0_0 = { 1, GenInst_FloatFormatHandling_t3887485542_0_0_0_Types };
extern const Il2CppType FloatParseHandling_t3074080948_0_0_0;
static const Il2CppType* GenInst_FloatParseHandling_t3074080948_0_0_0_Types[] = { &FloatParseHandling_t3074080948_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatParseHandling_t3074080948_0_0_0 = { 1, GenInst_FloatParseHandling_t3074080948_0_0_0_Types };
extern const Il2CppType StringEscapeHandling_t1042460335_0_0_0;
static const Il2CppType* GenInst_StringEscapeHandling_t1042460335_0_0_0_Types[] = { &StringEscapeHandling_t1042460335_0_0_0 };
extern const Il2CppGenericInst GenInst_StringEscapeHandling_t1042460335_0_0_0 = { 1, GenInst_StringEscapeHandling_t1042460335_0_0_0_Types };
extern const Il2CppType ErrorEventArgs_t792639131_0_0_0;
static const Il2CppType* GenInst_ErrorEventArgs_t792639131_0_0_0_Types[] = { &ErrorEventArgs_t792639131_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorEventArgs_t792639131_0_0_0 = { 1, GenInst_ErrorEventArgs_t792639131_0_0_0_Types };
extern const Il2CppType MetadataPropertyHandling_t2626038881_0_0_0;
static const Il2CppType* GenInst_MetadataPropertyHandling_t2626038881_0_0_0_Types[] = { &MetadataPropertyHandling_t2626038881_0_0_0 };
extern const Il2CppGenericInst GenInst_MetadataPropertyHandling_t2626038881_0_0_0 = { 1, GenInst_MetadataPropertyHandling_t2626038881_0_0_0_Types };
extern const Il2CppType FormatterAssemblyStyle_t3005881063_0_0_0;
static const Il2CppType* GenInst_FormatterAssemblyStyle_t3005881063_0_0_0_Types[] = { &FormatterAssemblyStyle_t3005881063_0_0_0 };
extern const Il2CppGenericInst GenInst_FormatterAssemblyStyle_t3005881063_0_0_0 = { 1, GenInst_FormatterAssemblyStyle_t3005881063_0_0_0_Types };
extern const Il2CppType PreserveReferencesHandling_t4230591217_0_0_0;
static const Il2CppType* GenInst_PreserveReferencesHandling_t4230591217_0_0_0_Types[] = { &PreserveReferencesHandling_t4230591217_0_0_0 };
extern const Il2CppGenericInst GenInst_PreserveReferencesHandling_t4230591217_0_0_0 = { 1, GenInst_PreserveReferencesHandling_t4230591217_0_0_0_Types };
extern const Il2CppType MissingMemberHandling_t2077487315_0_0_0;
static const Il2CppType* GenInst_MissingMemberHandling_t2077487315_0_0_0_Types[] = { &MissingMemberHandling_t2077487315_0_0_0 };
extern const Il2CppGenericInst GenInst_MissingMemberHandling_t2077487315_0_0_0 = { 1, GenInst_MissingMemberHandling_t2077487315_0_0_0_Types };
extern const Il2CppType ConstructorHandling_t2475221485_0_0_0;
static const Il2CppType* GenInst_ConstructorHandling_t2475221485_0_0_0_Types[] = { &ConstructorHandling_t2475221485_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorHandling_t2475221485_0_0_0 = { 1, GenInst_ConstructorHandling_t2475221485_0_0_0_Types };
extern const Il2CppType StreamingContext_t2761351129_0_0_0;
static const Il2CppType* GenInst_StreamingContext_t2761351129_0_0_0_Types[] = { &StreamingContext_t2761351129_0_0_0 };
extern const Il2CppGenericInst GenInst_StreamingContext_t2761351129_0_0_0 = { 1, GenInst_StreamingContext_t2761351129_0_0_0_Types };
extern const Il2CppType IReferenceResolver_t425424564_0_0_0;
static const Il2CppType* GenInst_IReferenceResolver_t425424564_0_0_0_Types[] = { &IReferenceResolver_t425424564_0_0_0 };
extern const Il2CppGenericInst GenInst_IReferenceResolver_t425424564_0_0_0 = { 1, GenInst_IReferenceResolver_t425424564_0_0_0_Types };
extern const Il2CppType StateU5BU5D_t871800199_0_0_0;
static const Il2CppType* GenInst_StateU5BU5D_t871800199_0_0_0_Types[] = { &StateU5BU5D_t871800199_0_0_0 };
extern const Il2CppGenericInst GenInst_StateU5BU5D_t871800199_0_0_0 = { 1, GenInst_StateU5BU5D_t871800199_0_0_0_Types };
extern const Il2CppType State_t671991922_0_0_0;
static const Il2CppType* GenInst_State_t671991922_0_0_0_Types[] = { &State_t671991922_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t671991922_0_0_0 = { 1, GenInst_State_t671991922_0_0_0_Types };
extern const Il2CppType PrimitiveTypeCode_t2429291660_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types[] = { &Type_t_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0 = { 2, GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t203144266_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t203144266_0_0_0_Types[] = { &KeyValuePair_2_t203144266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t203144266_0_0_0 = { 1, GenInst_KeyValuePair_2_t203144266_0_0_0_Types };
static const Il2CppType* GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Types[] = { &PrimitiveTypeCode_t2429291660_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimitiveTypeCode_t2429291660_0_0_0 = { 1, GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_KeyValuePair_2_t203144266_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0, &KeyValuePair_2_t203144266_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_KeyValuePair_2_t203144266_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_KeyValuePair_2_t203144266_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2433494851_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2433494851_0_0_0_Types[] = { &KeyValuePair_2_t2433494851_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2433494851_0_0_0 = { 1, GenInst_KeyValuePair_2_t2433494851_0_0_0_Types };
extern const Il2CppType TypeInformation_t2222045584_0_0_0;
static const Il2CppType* GenInst_TypeInformation_t2222045584_0_0_0_Types[] = { &TypeInformation_t2222045584_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeInformation_t2222045584_0_0_0 = { 1, GenInst_TypeInformation_t2222045584_0_0_0_Types };
extern const Il2CppType TypeConvertKey_t866134174_0_0_0;
extern const Il2CppType Func_2_t184564025_0_0_0;
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Func_2_t184564025_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0 = { 2, GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2815902970_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2815902970_0_0_0_Types[] = { &KeyValuePair_2_t2815902970_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815902970_0_0_0 = { 1, GenInst_KeyValuePair_2_t2815902970_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0 = { 1, GenInst_TypeConvertKey_t866134174_0_0_0_Types };
extern const Il2CppType IEquatable_1_t415412741_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t415412741_0_0_0_Types[] = { &IEquatable_1_t415412741_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t415412741_0_0_0 = { 1, GenInst_IEquatable_1_t415412741_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t866134174_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Il2CppObject_0_0_0, &TypeConvertKey_t866134174_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t866134174_0_0_0 = { 3, GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t866134174_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2815902970_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2815902970_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2815902970_0_0_0 = { 3, GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2815902970_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &Func_2_t184564025_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t157076046_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_Types[] = { &Type_t_0_0_0, &BidirectionalDictionary_2_t157076046_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0 = { 2, GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &BidirectionalDictionary_2_t157076046_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType EnumMemberAttribute_t1202205191_0_0_0;
static const Il2CppType* GenInst_EnumMemberAttribute_t1202205191_0_0_0_Types[] = { &EnumMemberAttribute_t1202205191_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberAttribute_t1202205191_0_0_0 = { 1, GenInst_EnumMemberAttribute_t1202205191_0_0_0_Types };
static const Il2CppType* GenInst_EnumMemberAttribute_t1202205191_0_0_0_String_t_0_0_0_Types[] = { &EnumMemberAttribute_t1202205191_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMemberAttribute_t1202205191_0_0_0_String_t_0_0_0 = { 2, GenInst_EnumMemberAttribute_t1202205191_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &FieldInfo_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Link_t814405322_0_0_0;
static const Il2CppType* GenInst_Link_t814405322_0_0_0_Types[] = { &Link_t814405322_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t814405322_0_0_0 = { 1, GenInst_Link_t814405322_0_0_0_Types };
extern const Il2CppType Entry_t1172375224_0_0_0;
static const Il2CppType* GenInst_Entry_t1172375224_0_0_0_Types[] = { &Entry_t1172375224_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1172375224_0_0_0 = { 1, GenInst_Entry_t1172375224_0_0_0_Types };
extern const Il2CppType ReflectionMember_t3070212469_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_Types[] = { &String_t_0_0_0, &ReflectionMember_t3070212469_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0 = { 2, GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ReflectionMember_t3070212469_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3789411545_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3789411545_0_0_0_Types[] = { &KeyValuePair_2_t3789411545_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3789411545_0_0_0 = { 1, GenInst_KeyValuePair_2_t3789411545_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorInfo_t4136801618_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &ConstructorInfo_t4136801618_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t4136801618_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_ConstructorInfo_t4136801618_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType IGrouping_2_t3411231652_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t3411231652_0_0_0_Types[] = { &IGrouping_2_t3411231652_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t3411231652_0_0_0 = { 1, GenInst_IGrouping_2_t3411231652_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types[] = { &String_t_0_0_0, &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberInfo_t_0_0_0 = { 2, GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types };
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_String_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0_String_t_0_0_0 = { 2, GenInst_MemberInfo_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_ParameterInfo_t2235474049_0_0_0_Type_t_0_0_0_Types[] = { &ParameterInfo_t2235474049_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2235474049_0_0_0_Type_t_0_0_0 = { 2, GenInst_ParameterInfo_t2235474049_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &PropertyInfo_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ResolverContractKey_t473801005_0_0_0;
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0 = { 1, GenInst_ResolverContractKey_t473801005_0_0_0_Types };
extern const Il2CppType JsonContract_t1328848902_0_0_0;
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &JsonContract_t1328848902_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0 = { 2, GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2893931855_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2893931855_0_0_0_Types[] = { &KeyValuePair_2_t2893931855_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2893931855_0_0_0 = { 1, GenInst_KeyValuePair_2_t2893931855_0_0_0_Types };
extern const Il2CppType IEquatable_1_t23079572_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t23079572_0_0_0_Types[] = { &IEquatable_1_t23079572_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t23079572_0_0_0 = { 1, GenInst_IEquatable_1_t23079572_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_ResolverContractKey_t473801005_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &Il2CppObject_0_0_0, &ResolverContractKey_t473801005_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_ResolverContractKey_t473801005_0_0_0 = { 3, GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_ResolverContractKey_t473801005_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2893931855_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2893931855_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2893931855_0_0_0 = { 3, GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2893931855_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &JsonContract_t1328848902_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t51964386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t51964386_0_0_0_Types[] = { &KeyValuePair_2_t51964386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t51964386_0_0_0 = { 1, GenInst_KeyValuePair_2_t51964386_0_0_0_Types };
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &MemberInfo_t_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_MemberInfo_t_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType JsonProperty_t902655177_0_0_0;
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0 = { 1, GenInst_JsonProperty_t902655177_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3001461559_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IEnumerable_1_t3001461559_0_0_0_Types[] = { &Type_t_0_0_0, &IEnumerable_1_t3001461559_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IEnumerable_1_t3001461559_0_0_0 = { 2, GenInst_Type_t_0_0_0_IEnumerable_1_t3001461559_0_0_0_Types };
extern const Il2CppType SerializationCallback_t799500859_0_0_0;
static const Il2CppType* GenInst_SerializationCallback_t799500859_0_0_0_Types[] = { &SerializationCallback_t799500859_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationCallback_t799500859_0_0_0 = { 1, GenInst_SerializationCallback_t799500859_0_0_0_Types };
extern const Il2CppType SerializationErrorCallback_t1476028681_0_0_0;
static const Il2CppType* GenInst_SerializationErrorCallback_t1476028681_0_0_0_Types[] = { &SerializationErrorCallback_t1476028681_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializationErrorCallback_t1476028681_0_0_0 = { 1, GenInst_SerializationErrorCallback_t1476028681_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_JsonProperty_t902655177_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType TypeNameKey_t2971844791_0_0_0;
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0 = { 2, GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4013503581_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4013503581_0_0_0_Types[] = { &KeyValuePair_2_t4013503581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4013503581_0_0_0 = { 1, GenInst_KeyValuePair_2_t4013503581_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0 = { 1, GenInst_TypeNameKey_t2971844791_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2521123358_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2521123358_0_0_0_Types[] = { &IEquatable_1_t2521123358_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2521123358_0_0_0 = { 1, GenInst_IEquatable_1_t2521123358_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t2971844791_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Il2CppObject_0_0_0, &TypeNameKey_t2971844791_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t2971844791_0_0_0 = { 3, GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t2971844791_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4013503581_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4013503581_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4013503581_0_0_0 = { 3, GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4013503581_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ReadType_t3446921512_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_Types[] = { &Type_t_0_0_0, &ReadType_t3446921512_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0 = { 2, GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ReadType_t3446921512_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1220774118_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1220774118_0_0_0_Types[] = { &KeyValuePair_2_t1220774118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1220774118_0_0_0 = { 1, GenInst_KeyValuePair_2_t1220774118_0_0_0_Types };
static const Il2CppType* GenInst_ReadType_t3446921512_0_0_0_Types[] = { &ReadType_t3446921512_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadType_t3446921512_0_0_0 = { 1, GenInst_ReadType_t3446921512_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ReadType_t3446921512_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ReadType_t3446921512_0_0_0, &ReadType_t3446921512_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ReadType_t3446921512_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_KeyValuePair_2_t1220774118_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ReadType_t3446921512_0_0_0, &KeyValuePair_2_t1220774118_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_KeyValuePair_2_t1220774118_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_KeyValuePair_2_t1220774118_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ReadType_t3446921512_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3451124703_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3451124703_0_0_0_Types[] = { &KeyValuePair_2_t3451124703_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3451124703_0_0_0 = { 1, GenInst_KeyValuePair_2_t3451124703_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_Types[] = { &String_t_0_0_0, &JsonProperty_t902655177_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &JsonProperty_t902655177_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType CreatorPropertyContext_t3090945776_0_0_0;
static const Il2CppType* GenInst_CreatorPropertyContext_t3090945776_0_0_0_Types[] = { &CreatorPropertyContext_t3090945776_0_0_0 };
extern const Il2CppGenericInst GenInst_CreatorPropertyContext_t3090945776_0_0_0 = { 1, GenInst_CreatorPropertyContext_t3090945776_0_0_0_Types };
extern const Il2CppType PropertyPresence_t2892329490_0_0_0;
static const Il2CppType* GenInst_PropertyPresence_t2892329490_0_0_0_Types[] = { &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t2892329490_0_0_0 = { 1, GenInst_PropertyPresence_t2892329490_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_String_t_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_String_t_0_0_0 = { 2, GenInst_JsonProperty_t902655177_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_CreatorPropertyContext_t3090945776_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &CreatorPropertyContext_t3090945776_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_CreatorPropertyContext_t3090945776_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_CreatorPropertyContext_t3090945776_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 2, GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t666182096_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t666182096_0_0_0_Types[] = { &KeyValuePair_2_t666182096_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t666182096_0_0_0 = { 1, GenInst_KeyValuePair_2_t666182096_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_KeyValuePair_2_t666182096_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0, &KeyValuePair_2_t666182096_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_KeyValuePair_2_t666182096_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_KeyValuePair_2_t666182096_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &PropertyPresence_t2892329490_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t595436130_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t595436130_0_0_0_Types[] = { &KeyValuePair_2_t595436130_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t595436130_0_0_0 = { 1, GenInst_KeyValuePair_2_t595436130_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &JsonProperty_t902655177_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0 = { 2, GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_Types };
extern const Il2CppType Func_2_t2363589633_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_Types[] = { &Type_t_0_0_0, &Func_2_t2363589633_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0 = { 2, GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_Types };
extern const Il2CppType ObjectU5BU5D_t1108656482_0_0_0;
static const Il2CppType* GenInst_ObjectU5BU5D_t1108656482_0_0_0_JsonConverter_t2159686854_0_0_0_Types[] = { &ObjectU5BU5D_t1108656482_0_0_0, &JsonConverter_t2159686854_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectU5BU5D_t1108656482_0_0_0_JsonConverter_t2159686854_0_0_0 = { 2, GenInst_ObjectU5BU5D_t1108656482_0_0_0_JsonConverter_t2159686854_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &Func_2_t2363589633_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType DataContractAttribute_t2462274566_0_0_0;
static const Il2CppType* GenInst_DataContractAttribute_t2462274566_0_0_0_Types[] = { &DataContractAttribute_t2462274566_0_0_0 };
extern const Il2CppGenericInst GenInst_DataContractAttribute_t2462274566_0_0_0 = { 1, GenInst_DataContractAttribute_t2462274566_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DataContractAttribute_t2462274566_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DataContractAttribute_t2462274566_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType DataMemberAttribute_t2601848894_0_0_0;
static const Il2CppType* GenInst_DataMemberAttribute_t2601848894_0_0_0_Types[] = { &DataMemberAttribute_t2601848894_0_0_0 };
extern const Il2CppGenericInst GenInst_DataMemberAttribute_t2601848894_0_0_0 = { 1, GenInst_DataMemberAttribute_t2601848894_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DataMemberAttribute_t2601848894_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DataMemberAttribute_t2601848894_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Type_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Type_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType JToken_t3412245951_0_0_0;
static const Il2CppType* GenInst_JToken_t3412245951_0_0_0_Types[] = { &JToken_t3412245951_0_0_0 };
extern const Il2CppGenericInst GenInst_JToken_t3412245951_0_0_0 = { 1, GenInst_JToken_t3412245951_0_0_0_Types };
extern const Il2CppType JEnumerable_1_t971832625_0_0_0;
static const Il2CppType* GenInst_JEnumerable_1_t971832625_0_0_0_Types[] = { &JEnumerable_1_t971832625_0_0_0 };
extern const Il2CppGenericInst GenInst_JEnumerable_1_t971832625_0_0_0 = { 1, GenInst_JEnumerable_1_t971832625_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_Types[] = { &String_t_0_0_0, &JToken_t3412245951_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0 = { 2, GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4131445027_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4131445027_0_0_0_Types[] = { &KeyValuePair_2_t4131445027_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4131445027_0_0_0 = { 1, GenInst_KeyValuePair_2_t4131445027_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &JToken_t3412245951_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType JTokenType_t3916897561_0_0_0;
static const Il2CppType* GenInst_JTokenType_t3916897561_0_0_0_Types[] = { &JTokenType_t3916897561_0_0_0 };
extern const Il2CppGenericInst GenInst_JTokenType_t3916897561_0_0_0 = { 1, GenInst_JTokenType_t3916897561_0_0_0_Types };
extern const Il2CppType JsonToken_t4173078175_0_0_0;
static const Il2CppType* GenInst_JsonToken_t4173078175_0_0_0_Types[] = { &JsonToken_t4173078175_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonToken_t4173078175_0_0_0 = { 1, GenInst_JsonToken_t4173078175_0_0_0_Types };
extern const Il2CppType JValue_t3413677367_0_0_0;
static const Il2CppType* GenInst_JValue_t3413677367_0_0_0_Types[] = { &JValue_t3413677367_0_0_0 };
extern const Il2CppGenericInst GenInst_JValue_t3413677367_0_0_0 = { 1, GenInst_JValue_t3413677367_0_0_0_Types };
extern const Il2CppType ReflectionObject_t3124613658_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_Types[] = { &Type_t_0_0_0, &ReflectionObject_t3124613658_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0 = { 2, GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &ReflectionObject_t3124613658_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType RegexOptions_t3066443743_0_0_0;
static const Il2CppType* GenInst_RegexOptions_t3066443743_0_0_0_Types[] = { &RegexOptions_t3066443743_0_0_0 };
extern const Il2CppGenericInst GenInst_RegexOptions_t3066443743_0_0_0 = { 1, GenInst_RegexOptions_t3066443743_0_0_0_Types };
extern const Il2CppType BsonProperty_t4293821333_0_0_0;
static const Il2CppType* GenInst_BsonProperty_t4293821333_0_0_0_Types[] = { &BsonProperty_t4293821333_0_0_0 };
extern const Il2CppGenericInst GenInst_BsonProperty_t4293821333_0_0_0 = { 1, GenInst_BsonProperty_t4293821333_0_0_0_Types };
extern const Il2CppType BsonToken_t455725415_0_0_0;
static const Il2CppType* GenInst_BsonToken_t455725415_0_0_0_Types[] = { &BsonToken_t455725415_0_0_0 };
extern const Il2CppGenericInst GenInst_BsonToken_t455725415_0_0_0 = { 1, GenInst_BsonToken_t455725415_0_0_0_Types };
extern const Il2CppType Object_t3071478659_0_0_0;
static const Il2CppType* GenInst_Object_t3071478659_0_0_0_Types[] = { &Object_t3071478659_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t3071478659_0_0_0 = { 1, GenInst_Object_t3071478659_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4128901257_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4128901257_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t655461400_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t655461400_0_0_0_Types[] = { &IAchievementDescription_t655461400_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t655461400_0_0_0 = { 1, GenInst_IAchievementDescription_t655461400_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1953253797_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types[] = { &IAchievementU5BU5D_t1953253797_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1953253797_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1953253797_0_0_0_Types };
extern const Il2CppType IAchievement_t2957812780_0_0_0;
static const Il2CppType* GenInst_IAchievement_t2957812780_0_0_0_Types[] = { &IAchievement_t2957812780_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t2957812780_0_0_0 = { 1, GenInst_IAchievement_t2957812780_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t250104726_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t250104726_0_0_0_Types[] = { &IScoreU5BU5D_t250104726_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t250104726_0_0_0 = { 1, GenInst_IScoreU5BU5D_t250104726_0_0_0_Types };
extern const Il2CppType IScore_t4279057999_0_0_0;
static const Il2CppType* GenInst_IScore_t4279057999_0_0_0_Types[] = { &IScore_t4279057999_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t4279057999_0_0_0 = { 1, GenInst_IScore_t4279057999_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3419104218_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types[] = { &IUserProfileU5BU5D_t3419104218_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3419104218_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3419104218_0_0_0_Types };
extern const Il2CppType IUserProfile_t598900827_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t598900827_0_0_0_Types[] = { &IUserProfile_t598900827_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t598900827_0_0_0 = { 1, GenInst_IUserProfile_t598900827_0_0_0_Types };
extern const Il2CppType AchievementDescription_t2116066607_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t2116066607_0_0_0_Types[] = { &AchievementDescription_t2116066607_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t2116066607_0_0_0 = { 1, GenInst_AchievementDescription_t2116066607_0_0_0_Types };
extern const Il2CppType UserProfile_t2280656072_0_0_0;
static const Il2CppType* GenInst_UserProfile_t2280656072_0_0_0_Types[] = { &UserProfile_t2280656072_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t2280656072_0_0_0 = { 1, GenInst_UserProfile_t2280656072_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t1820874799_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t1820874799_0_0_0_Types[] = { &GcLeaderboard_t1820874799_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t1820874799_0_0_0 = { 1, GenInst_GcLeaderboard_t1820874799_0_0_0_Types };
extern const Il2CppType GcAchievementData_t3481375915_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t3481375915_0_0_0_Types[] = { &GcAchievementData_t3481375915_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3481375915_0_0_0 = { 1, GenInst_GcAchievementData_t3481375915_0_0_0_Types };
extern const Il2CppType Achievement_t344600729_0_0_0;
static const Il2CppType* GenInst_Achievement_t344600729_0_0_0_Types[] = { &Achievement_t344600729_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t344600729_0_0_0 = { 1, GenInst_Achievement_t344600729_0_0_0_Types };
extern const Il2CppType GcScoreData_t2181296590_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2181296590_0_0_0_Types[] = { &GcScoreData_t2181296590_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2181296590_0_0_0 = { 1, GenInst_GcScoreData_t2181296590_0_0_0_Types };
extern const Il2CppType Score_t3396031228_0_0_0;
static const Il2CppType* GenInst_Score_t3396031228_0_0_0_Types[] = { &Score_t3396031228_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t3396031228_0_0_0 = { 1, GenInst_Score_t3396031228_0_0_0_Types };
extern const Il2CppType Material_t3870600107_0_0_0;
static const Il2CppType* GenInst_Material_t3870600107_0_0_0_Types[] = { &Material_t3870600107_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t3870600107_0_0_0 = { 1, GenInst_Material_t3870600107_0_0_0_Types };
extern const Il2CppType Color_t4194546905_0_0_0;
static const Il2CppType* GenInst_Color_t4194546905_0_0_0_Types[] = { &Color_t4194546905_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t4194546905_0_0_0 = { 1, GenInst_Color_t4194546905_0_0_0_Types };
extern const Il2CppType Color32_t598853688_0_0_0;
static const Il2CppType* GenInst_Color32_t598853688_0_0_0_Types[] = { &Color32_t598853688_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t598853688_0_0_0 = { 1, GenInst_Color32_t598853688_0_0_0_Types };
extern const Il2CppType Keyframe_t4079056114_0_0_0;
static const Il2CppType* GenInst_Keyframe_t4079056114_0_0_0_Types[] = { &Keyframe_t4079056114_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t4079056114_0_0_0 = { 1, GenInst_Keyframe_t4079056114_0_0_0_Types };
extern const Il2CppType Vector3_t4282066566_0_0_0;
static const Il2CppType* GenInst_Vector3_t4282066566_0_0_0_Types[] = { &Vector3_t4282066566_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t4282066566_0_0_0 = { 1, GenInst_Vector3_t4282066566_0_0_0_Types };
extern const Il2CppType Vector4_t4282066567_0_0_0;
static const Il2CppType* GenInst_Vector4_t4282066567_0_0_0_Types[] = { &Vector4_t4282066567_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t4282066567_0_0_0 = { 1, GenInst_Vector4_t4282066567_0_0_0_Types };
extern const Il2CppType Vector2_t4282066565_0_0_0;
static const Il2CppType* GenInst_Vector2_t4282066565_0_0_0_Types[] = { &Vector2_t4282066565_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t4282066565_0_0_0 = { 1, GenInst_Vector2_t4282066565_0_0_0_Types };
extern const Il2CppType NetworkPlayer_t3231273765_0_0_0;
static const Il2CppType* GenInst_NetworkPlayer_t3231273765_0_0_0_Types[] = { &NetworkPlayer_t3231273765_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkPlayer_t3231273765_0_0_0 = { 1, GenInst_NetworkPlayer_t3231273765_0_0_0_Types };
extern const Il2CppType HostData_t3270478838_0_0_0;
static const Il2CppType* GenInst_HostData_t3270478838_0_0_0_Types[] = { &HostData_t3270478838_0_0_0 };
extern const Il2CppGenericInst GenInst_HostData_t3270478838_0_0_0 = { 1, GenInst_HostData_t3270478838_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t726430633_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t726430633_0_0_0_Types[] = { &KeyValuePair_2_t726430633_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t726430633_0_0_0 = { 1, GenInst_KeyValuePair_2_t726430633_0_0_0_Types };
extern const Il2CppType Camera_t2727095145_0_0_0;
static const Il2CppType* GenInst_Camera_t2727095145_0_0_0_Types[] = { &Camera_t2727095145_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t2727095145_0_0_0 = { 1, GenInst_Camera_t2727095145_0_0_0_Types };
extern const Il2CppType Behaviour_t200106419_0_0_0;
static const Il2CppType* GenInst_Behaviour_t200106419_0_0_0_Types[] = { &Behaviour_t200106419_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t200106419_0_0_0 = { 1, GenInst_Behaviour_t200106419_0_0_0_Types };
extern const Il2CppType Component_t3501516275_0_0_0;
static const Il2CppType* GenInst_Component_t3501516275_0_0_0_Types[] = { &Component_t3501516275_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3501516275_0_0_0 = { 1, GenInst_Component_t3501516275_0_0_0_Types };
extern const Il2CppType Display_t1321072632_0_0_0;
static const Il2CppType* GenInst_Display_t1321072632_0_0_0_Types[] = { &Display_t1321072632_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1321072632_0_0_0 = { 1, GenInst_Display_t1321072632_0_0_0_Types };
extern const Il2CppType Touch_t4210255029_0_0_0;
static const Il2CppType* GenInst_Touch_t4210255029_0_0_0_Types[] = { &Touch_t4210255029_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t4210255029_0_0_0 = { 1, GenInst_Touch_t4210255029_0_0_0_Types };
extern const Il2CppType GameObject_t3674682005_0_0_0;
static const Il2CppType* GenInst_GameObject_t3674682005_0_0_0_Types[] = { &GameObject_t3674682005_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t3674682005_0_0_0 = { 1, GenInst_GameObject_t3674682005_0_0_0_Types };
extern const Il2CppType Playable_t70832698_0_0_0;
static const Il2CppType* GenInst_Playable_t70832698_0_0_0_Types[] = { &Playable_t70832698_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t70832698_0_0_0 = { 1, GenInst_Playable_t70832698_0_0_0_Types };
extern const Il2CppType Scene_t1080795294_0_0_0;
extern const Il2CppType LoadSceneMode_t3067001883_0_0_0;
static const Il2CppType* GenInst_Scene_t1080795294_0_0_0_LoadSceneMode_t3067001883_0_0_0_Types[] = { &Scene_t1080795294_0_0_0, &LoadSceneMode_t3067001883_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1080795294_0_0_0_LoadSceneMode_t3067001883_0_0_0 = { 2, GenInst_Scene_t1080795294_0_0_0_LoadSceneMode_t3067001883_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1080795294_0_0_0_Types[] = { &Scene_t1080795294_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1080795294_0_0_0 = { 1, GenInst_Scene_t1080795294_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1080795294_0_0_0_Scene_t1080795294_0_0_0_Types[] = { &Scene_t1080795294_0_0_0, &Scene_t1080795294_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1080795294_0_0_0_Scene_t1080795294_0_0_0 = { 2, GenInst_Scene_t1080795294_0_0_0_Scene_t1080795294_0_0_0_Types };
extern const Il2CppType ContactPoint_t243083348_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t243083348_0_0_0_Types[] = { &ContactPoint_t243083348_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t243083348_0_0_0 = { 1, GenInst_ContactPoint_t243083348_0_0_0_Types };
extern const Il2CppType RaycastHit_t4003175726_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t4003175726_0_0_0_Types[] = { &RaycastHit_t4003175726_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t4003175726_0_0_0 = { 1, GenInst_RaycastHit_t4003175726_0_0_0_Types };
extern const Il2CppType Collider_t2939674232_0_0_0;
static const Il2CppType* GenInst_Collider_t2939674232_0_0_0_Types[] = { &Collider_t2939674232_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t2939674232_0_0_0 = { 1, GenInst_Collider_t2939674232_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t1743771669_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t1743771669_0_0_0_Types[] = { &Rigidbody2D_t1743771669_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1743771669_0_0_0 = { 1, GenInst_Rigidbody2D_t1743771669_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t1374744384_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t1374744384_0_0_0_Types[] = { &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t1374744384_0_0_0 = { 1, GenInst_RaycastHit2D_t1374744384_0_0_0_Types };
extern const Il2CppType Collider2D_t1552025098_0_0_0;
static const Il2CppType* GenInst_Collider2D_t1552025098_0_0_0_Types[] = { &Collider2D_t1552025098_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider2D_t1552025098_0_0_0 = { 1, GenInst_Collider2D_t1552025098_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t4288432358_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t4288432358_0_0_0_Types[] = { &ContactPoint2D_t4288432358_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t4288432358_0_0_0 = { 1, GenInst_ContactPoint2D_t4288432358_0_0_0_Types };
extern const Il2CppType UIVertex_t4244065212_0_0_0;
static const Il2CppType* GenInst_UIVertex_t4244065212_0_0_0_Types[] = { &UIVertex_t4244065212_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4244065212_0_0_0 = { 1, GenInst_UIVertex_t4244065212_0_0_0_Types };
extern const Il2CppType UICharInfo_t65807484_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t65807484_0_0_0_Types[] = { &UICharInfo_t65807484_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t65807484_0_0_0 = { 1, GenInst_UICharInfo_t65807484_0_0_0_Types };
extern const Il2CppType UILineInfo_t4113875482_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t4113875482_0_0_0_Types[] = { &UILineInfo_t4113875482_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4113875482_0_0_0 = { 1, GenInst_UILineInfo_t4113875482_0_0_0_Types };
extern const Il2CppType Font_t4241557075_0_0_0;
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_Types[] = { &Font_t4241557075_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0 = { 1, GenInst_Font_t4241557075_0_0_0_Types };
extern const Il2CppType GUIContent_t2094828418_0_0_0;
static const Il2CppType* GenInst_GUIContent_t2094828418_0_0_0_Types[] = { &GUIContent_t2094828418_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t2094828418_0_0_0 = { 1, GenInst_GUIContent_t2094828418_0_0_0_Types };
extern const Il2CppType Rect_t4241904616_0_0_0;
static const Il2CppType* GenInst_Rect_t4241904616_0_0_0_Types[] = { &Rect_t4241904616_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t4241904616_0_0_0 = { 1, GenInst_Rect_t4241904616_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t331591504_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t331591504_0_0_0_Types[] = { &GUILayoutOption_t331591504_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t331591504_0_0_0 = { 1, GenInst_GUILayoutOption_t331591504_0_0_0_Types };
extern const Il2CppType LayoutCache_t879908455_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4066860316_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0 = { 1, GenInst_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &LayoutCache_t879908455_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t775952400_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t775952400_0_0_0_Types[] = { &KeyValuePair_2_t775952400_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t775952400_0_0_0 = { 1, GenInst_KeyValuePair_2_t775952400_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t1336615025_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t1336615025_0_0_0_Types[] = { &GUILayoutEntry_t1336615025_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1336615025_0_0_0 = { 1, GenInst_GUILayoutEntry_t1336615025_0_0_0_Types };
extern const Il2CppType GUIStyle_t2990928826_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t2990928826_0_0_0_Types[] = { &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t2990928826_0_0_0 = { 1, GenInst_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t2990928826_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3710127902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3710127902_0_0_0_Types[] = { &KeyValuePair_2_t3710127902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3710127902_0_0_0 = { 1, GenInst_KeyValuePair_2_t3710127902_0_0_0_Types };
extern const Il2CppType Event_t4196595728_0_0_0;
extern const Il2CppType TextEditOp_t4145961110_0_0_0;
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Event_t4196595728_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1919813716_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0 = { 1, GenInst_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0 = { 1, GenInst_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t4145961110_0_0_0, &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_Types[] = { &Event_t4196595728_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0 = { 1, GenInst_Event_t4196595728_0_0_0_Types };
static const Il2CppType* GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Event_t4196595728_0_0_0, &TextEditOp_t4145961110_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1289591971_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1289591971_0_0_0_Types[] = { &KeyValuePair_2_t1289591971_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1289591971_0_0_0 = { 1, GenInst_KeyValuePair_2_t1289591971_0_0_0_Types };
extern const Il2CppType IMultipartFormSection_t2606995300_0_0_0;
static const Il2CppType* GenInst_IMultipartFormSection_t2606995300_0_0_0_Types[] = { &IMultipartFormSection_t2606995300_0_0_0 };
extern const Il2CppGenericInst GenInst_IMultipartFormSection_t2606995300_0_0_0 = { 1, GenInst_IMultipartFormSection_t2606995300_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t62111112_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types[] = { &DisallowMultipleComponent_t62111112_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t62111112_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t62111112_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3132250205_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types[] = { &ExecuteInEditMode_t3132250205_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3132250205_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3132250205_0_0_0_Types };
extern const Il2CppType RequireComponent_t1687166108_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t1687166108_0_0_0_Types[] = { &RequireComponent_t1687166108_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t1687166108_0_0_0 = { 1, GenInst_RequireComponent_t1687166108_0_0_0_Types };
extern const Il2CppType HitInfo_t3209134097_0_0_0;
static const Il2CppType* GenInst_HitInfo_t3209134097_0_0_0_Types[] = { &HitInfo_t3209134097_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t3209134097_0_0_0 = { 1, GenInst_HitInfo_t3209134097_0_0_0_Types };
extern const Il2CppType PersistentCall_t2972625667_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t2972625667_0_0_0_Types[] = { &PersistentCall_t2972625667_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t2972625667_0_0_0 = { 1, GenInst_PersistentCall_t2972625667_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1559630662_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1559630662_0_0_0_Types[] = { &BaseInvokableCall_t1559630662_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1559630662_0_0_0 = { 1, GenInst_BaseInvokableCall_t1559630662_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &MemberInfo_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t419747678_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t419747678_0_0_0_Types[] = { &KeyValuePair_2_t419747678_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t419747678_0_0_0 = { 1, GenInst_KeyValuePair_2_t419747678_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t595048151_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t595048151_0_0_0_Types[] = { &KeyValuePair_2_t595048151_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t595048151_0_0_0 = { 1, GenInst_KeyValuePair_2_t595048151_0_0_0_Types };
extern const Il2CppType Dictionary_2_t520966972_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t520966972_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0 = { 2, GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t520966972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t525170163_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t525170163_0_0_0_Types[] = { &KeyValuePair_2_t525170163_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t525170163_0_0_0 = { 1, GenInst_KeyValuePair_2_t525170163_0_0_0_Types };
extern const Il2CppType Point_t1271214244_0_0_0;
static const Il2CppType* GenInst_Point_t1271214244_0_0_0_Types[] = { &Point_t1271214244_0_0_0 };
extern const Il2CppGenericInst GenInst_Point_t1271214244_0_0_0 = { 1, GenInst_Point_t1271214244_0_0_0_Types };
extern const Il2CppType PlayMakerFSM_t3799847376_0_0_0;
static const Il2CppType* GenInst_PlayMakerFSM_t3799847376_0_0_0_Types[] = { &PlayMakerFSM_t3799847376_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerFSM_t3799847376_0_0_0 = { 1, GenInst_PlayMakerFSM_t3799847376_0_0_0_Types };
extern const Il2CppType ISerializationCallbackReceiver_t3119329471_0_0_0;
static const Il2CppType* GenInst_ISerializationCallbackReceiver_t3119329471_0_0_0_Types[] = { &ISerializationCallbackReceiver_t3119329471_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializationCallbackReceiver_t3119329471_0_0_0 = { 1, GenInst_ISerializationCallbackReceiver_t3119329471_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t667441552_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t667441552_0_0_0_Types[] = { &MonoBehaviour_t667441552_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t667441552_0_0_0 = { 1, GenInst_MonoBehaviour_t667441552_0_0_0_Types };
extern const Il2CppType FsmFloat_t2134102846_0_0_0;
static const Il2CppType* GenInst_FsmFloat_t2134102846_0_0_0_Types[] = { &FsmFloat_t2134102846_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmFloat_t2134102846_0_0_0 = { 1, GenInst_FsmFloat_t2134102846_0_0_0_Types };
extern const Il2CppType NamedVariable_t3211770239_0_0_0;
static const Il2CppType* GenInst_NamedVariable_t3211770239_0_0_0_Types[] = { &NamedVariable_t3211770239_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedVariable_t3211770239_0_0_0 = { 1, GenInst_NamedVariable_t3211770239_0_0_0_Types };
extern const Il2CppType INameable_t2730839192_0_0_0;
static const Il2CppType* GenInst_INameable_t2730839192_0_0_0_Types[] = { &INameable_t2730839192_0_0_0 };
extern const Il2CppGenericInst GenInst_INameable_t2730839192_0_0_0 = { 1, GenInst_INameable_t2730839192_0_0_0_Types };
extern const Il2CppType INamedVariable_t1024128046_0_0_0;
static const Il2CppType* GenInst_INamedVariable_t1024128046_0_0_0_Types[] = { &INamedVariable_t1024128046_0_0_0 };
extern const Il2CppGenericInst GenInst_INamedVariable_t1024128046_0_0_0 = { 1, GenInst_INamedVariable_t1024128046_0_0_0_Types };
extern const Il2CppType FsmInt_t1596138449_0_0_0;
static const Il2CppType* GenInst_FsmInt_t1596138449_0_0_0_Types[] = { &FsmInt_t1596138449_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmInt_t1596138449_0_0_0 = { 1, GenInst_FsmInt_t1596138449_0_0_0_Types };
extern const Il2CppType FsmTransition_t3771611999_0_0_0;
static const Il2CppType* GenInst_FsmTransition_t3771611999_0_0_0_Types[] = { &FsmTransition_t3771611999_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTransition_t3771611999_0_0_0 = { 1, GenInst_FsmTransition_t3771611999_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3320890566_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3320890566_0_0_0_Types[] = { &IEquatable_1_t3320890566_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3320890566_0_0_0 = { 1, GenInst_IEquatable_1_t3320890566_0_0_0_Types };
extern const Il2CppType ActionReport_t662142796_0_0_0;
static const Il2CppType* GenInst_ActionReport_t662142796_0_0_0_Types[] = { &ActionReport_t662142796_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionReport_t662142796_0_0_0 = { 1, GenInst_ActionReport_t662142796_0_0_0_Types };
extern const Il2CppType FsmVarOverride_t3235106805_0_0_0;
static const Il2CppType* GenInst_FsmVarOverride_t3235106805_0_0_0_Types[] = { &FsmVarOverride_t3235106805_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVarOverride_t3235106805_0_0_0 = { 1, GenInst_FsmVarOverride_t3235106805_0_0_0_Types };
extern const Il2CppType Fsm_t1527112426_0_0_0;
static const Il2CppType* GenInst_Fsm_t1527112426_0_0_0_Types[] = { &Fsm_t1527112426_0_0_0 };
extern const Il2CppGenericInst GenInst_Fsm_t1527112426_0_0_0 = { 1, GenInst_Fsm_t1527112426_0_0_0_Types };
extern const Il2CppType Texture_t2526458961_0_0_0;
static const Il2CppType* GenInst_Texture_t2526458961_0_0_0_Types[] = { &Texture_t2526458961_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2526458961_0_0_0 = { 1, GenInst_Texture_t2526458961_0_0_0_Types };
extern const Il2CppType Quaternion_t1553702882_0_0_0;
static const Il2CppType* GenInst_Quaternion_t1553702882_0_0_0_Types[] = { &Quaternion_t1553702882_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t1553702882_0_0_0 = { 1, GenInst_Quaternion_t1553702882_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3582344850_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3582344850_0_0_0_Types[] = { &KeyValuePair_2_t3582344850_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3582344850_0_0_0 = { 1, GenInst_KeyValuePair_2_t3582344850_0_0_0_Types };
extern const Il2CppType FieldInfoU5BU5D_t2567562023_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_Types[] = { &Type_t_0_0_0, &FieldInfoU5BU5D_t2567562023_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0 = { 2, GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &FieldInfoU5BU5D_t2567562023_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2571765214_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2571765214_0_0_0_Types[] = { &KeyValuePair_2_t2571765214_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2571765214_0_0_0 = { 1, GenInst_KeyValuePair_2_t2571765214_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Type_t_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Type_t_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1158041691_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1158041691_0_0_0_Types[] = { &KeyValuePair_2_t1158041691_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1158041691_0_0_0 = { 1, GenInst_KeyValuePair_2_t1158041691_0_0_0_Types };
extern const Il2CppType FsmGameObject_t1697147867_0_0_0;
static const Il2CppType* GenInst_FsmGameObject_t1697147867_0_0_0_Types[] = { &FsmGameObject_t1697147867_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmGameObject_t1697147867_0_0_0 = { 1, GenInst_FsmGameObject_t1697147867_0_0_0_Types };
extern const Il2CppType FsmOwnerDefault_t251897112_0_0_0;
static const Il2CppType* GenInst_FsmOwnerDefault_t251897112_0_0_0_Types[] = { &FsmOwnerDefault_t251897112_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmOwnerDefault_t251897112_0_0_0 = { 1, GenInst_FsmOwnerDefault_t251897112_0_0_0_Types };
extern const Il2CppType FsmAnimationCurve_t2685995989_0_0_0;
static const Il2CppType* GenInst_FsmAnimationCurve_t2685995989_0_0_0_Types[] = { &FsmAnimationCurve_t2685995989_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmAnimationCurve_t2685995989_0_0_0 = { 1, GenInst_FsmAnimationCurve_t2685995989_0_0_0_Types };
extern const Il2CppType FunctionCall_t3279845016_0_0_0;
static const Il2CppType* GenInst_FunctionCall_t3279845016_0_0_0_Types[] = { &FunctionCall_t3279845016_0_0_0 };
extern const Il2CppGenericInst GenInst_FunctionCall_t3279845016_0_0_0 = { 1, GenInst_FunctionCall_t3279845016_0_0_0_Types };
extern const Il2CppType FsmTemplateControl_t2786508133_0_0_0;
static const Il2CppType* GenInst_FsmTemplateControl_t2786508133_0_0_0_Types[] = { &FsmTemplateControl_t2786508133_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTemplateControl_t2786508133_0_0_0 = { 1, GenInst_FsmTemplateControl_t2786508133_0_0_0_Types };
extern const Il2CppType FsmEventTarget_t1823904941_0_0_0;
static const Il2CppType* GenInst_FsmEventTarget_t1823904941_0_0_0_Types[] = { &FsmEventTarget_t1823904941_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmEventTarget_t1823904941_0_0_0 = { 1, GenInst_FsmEventTarget_t1823904941_0_0_0_Types };
extern const Il2CppType FsmProperty_t3927159007_0_0_0;
static const Il2CppType* GenInst_FsmProperty_t3927159007_0_0_0_Types[] = { &FsmProperty_t3927159007_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmProperty_t3927159007_0_0_0 = { 1, GenInst_FsmProperty_t3927159007_0_0_0_Types };
extern const Il2CppType LayoutOption_t964995201_0_0_0;
static const Il2CppType* GenInst_LayoutOption_t964995201_0_0_0_Types[] = { &LayoutOption_t964995201_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutOption_t964995201_0_0_0 = { 1, GenInst_LayoutOption_t964995201_0_0_0_Types };
extern const Il2CppType FsmString_t952858651_0_0_0;
static const Il2CppType* GenInst_FsmString_t952858651_0_0_0_Types[] = { &FsmString_t952858651_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmString_t952858651_0_0_0 = { 1, GenInst_FsmString_t952858651_0_0_0_Types };
extern const Il2CppType FsmObject_t821476169_0_0_0;
static const Il2CppType* GenInst_FsmObject_t821476169_0_0_0_Types[] = { &FsmObject_t821476169_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmObject_t821476169_0_0_0 = { 1, GenInst_FsmObject_t821476169_0_0_0_Types };
extern const Il2CppType FsmVar_t1596150537_0_0_0;
static const Il2CppType* GenInst_FsmVar_t1596150537_0_0_0_Types[] = { &FsmVar_t1596150537_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVar_t1596150537_0_0_0 = { 1, GenInst_FsmVar_t1596150537_0_0_0_Types };
extern const Il2CppType FsmArray_t2129666875_0_0_0;
static const Il2CppType* GenInst_FsmArray_t2129666875_0_0_0_Types[] = { &FsmArray_t2129666875_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmArray_t2129666875_0_0_0 = { 1, GenInst_FsmArray_t2129666875_0_0_0_Types };
extern const Il2CppType FsmEnum_t1076048395_0_0_0;
static const Il2CppType* GenInst_FsmEnum_t1076048395_0_0_0_Types[] = { &FsmEnum_t1076048395_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmEnum_t1076048395_0_0_0 = { 1, GenInst_FsmEnum_t1076048395_0_0_0_Types };
extern const Il2CppType FsmBool_t1075959796_0_0_0;
static const Il2CppType* GenInst_FsmBool_t1075959796_0_0_0_Types[] = { &FsmBool_t1075959796_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmBool_t1075959796_0_0_0 = { 1, GenInst_FsmBool_t1075959796_0_0_0_Types };
extern const Il2CppType FsmVector2_t533912881_0_0_0;
static const Il2CppType* GenInst_FsmVector2_t533912881_0_0_0_Types[] = { &FsmVector2_t533912881_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVector2_t533912881_0_0_0 = { 1, GenInst_FsmVector2_t533912881_0_0_0_Types };
extern const Il2CppType FsmVector3_t533912882_0_0_0;
static const Il2CppType* GenInst_FsmVector3_t533912882_0_0_0_Types[] = { &FsmVector3_t533912882_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmVector3_t533912882_0_0_0 = { 1, GenInst_FsmVector3_t533912882_0_0_0_Types };
extern const Il2CppType FsmColor_t2131419205_0_0_0;
static const Il2CppType* GenInst_FsmColor_t2131419205_0_0_0_Types[] = { &FsmColor_t2131419205_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmColor_t2131419205_0_0_0 = { 1, GenInst_FsmColor_t2131419205_0_0_0_Types };
extern const Il2CppType FsmRect_t1076426478_0_0_0;
static const Il2CppType* GenInst_FsmRect_t1076426478_0_0_0_Types[] = { &FsmRect_t1076426478_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmRect_t1076426478_0_0_0 = { 1, GenInst_FsmRect_t1076426478_0_0_0_Types };
extern const Il2CppType FsmQuaternion_t3871136040_0_0_0;
static const Il2CppType* GenInst_FsmQuaternion_t3871136040_0_0_0_Types[] = { &FsmQuaternion_t3871136040_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmQuaternion_t3871136040_0_0_0 = { 1, GenInst_FsmQuaternion_t3871136040_0_0_0_Types };
extern const Il2CppType ParamDataType_t2672665179_0_0_0;
static const Il2CppType* GenInst_ParamDataType_t2672665179_0_0_0_Types[] = { &ParamDataType_t2672665179_0_0_0 };
extern const Il2CppGenericInst GenInst_ParamDataType_t2672665179_0_0_0 = { 1, GenInst_ParamDataType_t2672665179_0_0_0_Types };
extern const Il2CppType FsmStateAction_t2366529033_0_0_0;
static const Il2CppType* GenInst_FsmStateAction_t2366529033_0_0_0_Types[] = { &FsmStateAction_t2366529033_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmStateAction_t2366529033_0_0_0 = { 1, GenInst_FsmStateAction_t2366529033_0_0_0_Types };
extern const Il2CppType IFsmStateAction_t3269097786_0_0_0;
static const Il2CppType* GenInst_IFsmStateAction_t3269097786_0_0_0_Types[] = { &IFsmStateAction_t3269097786_0_0_0 };
extern const Il2CppGenericInst GenInst_IFsmStateAction_t3269097786_0_0_0 = { 1, GenInst_IFsmStateAction_t3269097786_0_0_0_Types };
extern const Il2CppType DelayedEvent_t1938906778_0_0_0;
static const Il2CppType* GenInst_DelayedEvent_t1938906778_0_0_0_Types[] = { &DelayedEvent_t1938906778_0_0_0 };
extern const Il2CppGenericInst GenInst_DelayedEvent_t1938906778_0_0_0 = { 1, GenInst_DelayedEvent_t1938906778_0_0_0_Types };
extern const Il2CppType FsmState_t2146334067_0_0_0;
static const Il2CppType* GenInst_FsmState_t2146334067_0_0_0_Types[] = { &FsmState_t2146334067_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmState_t2146334067_0_0_0 = { 1, GenInst_FsmState_t2146334067_0_0_0_Types };
extern const Il2CppType FsmEvent_t2133468028_0_0_0;
static const Il2CppType* GenInst_FsmEvent_t2133468028_0_0_0_Types[] = { &FsmEvent_t2133468028_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmEvent_t2133468028_0_0_0 = { 1, GenInst_FsmEvent_t2133468028_0_0_0_Types };
static const Il2CppType* GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types[] = { &Fsm_t1527112426_0_0_0, &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0 = { 2, GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3443564286_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3443564286_0_0_0_Types[] = { &KeyValuePair_2_t3443564286_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3443564286_0_0_0 = { 1, GenInst_KeyValuePair_2_t3443564286_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RaycastHit2D_t1374744384_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RaycastHit2D_t1374744384_0_0_0, &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RaycastHit2D_t1374744384_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_KeyValuePair_2_t3443564286_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RaycastHit2D_t1374744384_0_0_0, &KeyValuePair_2_t3443564286_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_KeyValuePair_2_t3443564286_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_KeyValuePair_2_t3443564286_0_0_0_Types };
static const Il2CppType* GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Fsm_t1527112426_0_0_0, &RaycastHit2D_t1374744384_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1397821387_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1397821387_0_0_0_Types[] = { &KeyValuePair_2_t1397821387_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1397821387_0_0_0 = { 1, GenInst_KeyValuePair_2_t1397821387_0_0_0_Types };
extern const Il2CppType FsmLogEntry_t2614866584_0_0_0;
static const Il2CppType* GenInst_FsmLogEntry_t2614866584_0_0_0_Types[] = { &FsmLogEntry_t2614866584_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmLogEntry_t2614866584_0_0_0 = { 1, GenInst_FsmLogEntry_t2614866584_0_0_0_Types };
extern const Il2CppType FsmLog_t1596141350_0_0_0;
static const Il2CppType* GenInst_FsmLog_t1596141350_0_0_0_Types[] = { &FsmLog_t1596141350_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmLog_t1596141350_0_0_0 = { 1, GenInst_FsmLog_t1596141350_0_0_0_Types };
extern const Il2CppType FsmMaterial_t924399665_0_0_0;
static const Il2CppType* GenInst_FsmMaterial_t924399665_0_0_0_Types[] = { &FsmMaterial_t924399665_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmMaterial_t924399665_0_0_0 = { 1, GenInst_FsmMaterial_t924399665_0_0_0_Types };
extern const Il2CppType FsmTexture_t3073272573_0_0_0;
static const Il2CppType* GenInst_FsmTexture_t3073272573_0_0_0_Types[] = { &FsmTexture_t3073272573_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmTexture_t3073272573_0_0_0 = { 1, GenInst_FsmTexture_t3073272573_0_0_0_Types };
extern const Il2CppType BaseInputModule_t15847059_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t15847059_0_0_0_Types[] = { &BaseInputModule_t15847059_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t15847059_0_0_0 = { 1, GenInst_BaseInputModule_t15847059_0_0_0_Types };
extern const Il2CppType RaycastResult_t3762661364_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t3762661364_0_0_0_Types[] = { &RaycastResult_t3762661364_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3762661364_0_0_0 = { 1, GenInst_RaycastResult_t3762661364_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t4282929196_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t4282929196_0_0_0_Types[] = { &IDeselectHandler_t4282929196_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t4282929196_0_0_0 = { 1, GenInst_IDeselectHandler_t4282929196_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t1130525624_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t1130525624_0_0_0_Types[] = { &IEventSystemHandler_t1130525624_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t1130525624_0_0_0 = { 1, GenInst_IEventSystemHandler_t1130525624_0_0_0_Types };
extern const Il2CppType List_1_t2498711176_0_0_0;
static const Il2CppType* GenInst_List_1_t2498711176_0_0_0_Types[] = { &List_1_t2498711176_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2498711176_0_0_0 = { 1, GenInst_List_1_t2498711176_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1244034627_0_0_0_Types[] = { &List_1_t1244034627_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1244034627_0_0_0 = { 1, GenInst_List_1_t1244034627_0_0_0_Types };
extern const Il2CppType List_1_t574734531_0_0_0;
static const Il2CppType* GenInst_List_1_t574734531_0_0_0_Types[] = { &List_1_t574734531_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t574734531_0_0_0 = { 1, GenInst_List_1_t574734531_0_0_0_Types };
extern const Il2CppType ISelectHandler_t3580292109_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t3580292109_0_0_0_Types[] = { &ISelectHandler_t3580292109_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t3580292109_0_0_0 = { 1, GenInst_ISelectHandler_t3580292109_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2327671059_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2327671059_0_0_0_Types[] = { &BaseRaycaster_t2327671059_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2327671059_0_0_0 = { 1, GenInst_BaseRaycaster_t2327671059_0_0_0_Types };
extern const Il2CppType Entry_t849715470_0_0_0;
static const Il2CppType* GenInst_Entry_t849715470_0_0_0_Types[] = { &Entry_t849715470_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t849715470_0_0_0 = { 1, GenInst_Entry_t849715470_0_0_0_Types };
extern const Il2CppType BaseEventData_t2054899105_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2054899105_0_0_0_Types[] = { &BaseEventData_t2054899105_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2054899105_0_0_0 = { 1, GenInst_BaseEventData_t2054899105_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t1772318350_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t1772318350_0_0_0_Types[] = { &IPointerEnterHandler_t1772318350_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t1772318350_0_0_0 = { 1, GenInst_IPointerEnterHandler_t1772318350_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t3471580134_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t3471580134_0_0_0_Types[] = { &IPointerExitHandler_t3471580134_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t3471580134_0_0_0 = { 1, GenInst_IPointerExitHandler_t3471580134_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t1340699618_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t1340699618_0_0_0_Types[] = { &IPointerDownHandler_t1340699618_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1340699618_0_0_0 = { 1, GenInst_IPointerDownHandler_t1340699618_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t3049278345_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t3049278345_0_0_0_Types[] = { &IPointerUpHandler_t3049278345_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t3049278345_0_0_0 = { 1, GenInst_IPointerUpHandler_t3049278345_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t2979908062_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t2979908062_0_0_0_Types[] = { &IPointerClickHandler_t2979908062_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t2979908062_0_0_0 = { 1, GenInst_IPointerClickHandler_t2979908062_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t220416063_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t220416063_0_0_0_Types[] = { &IInitializePotentialDragHandler_t220416063_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t220416063_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t220416063_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t1569653796_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t1569653796_0_0_0_Types[] = { &IBeginDragHandler_t1569653796_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t1569653796_0_0_0 = { 1, GenInst_IBeginDragHandler_t1569653796_0_0_0_Types };
extern const Il2CppType IDragHandler_t880586197_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t880586197_0_0_0_Types[] = { &IDragHandler_t880586197_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t880586197_0_0_0 = { 1, GenInst_IDragHandler_t880586197_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t2789914546_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t2789914546_0_0_0_Types[] = { &IEndDragHandler_t2789914546_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t2789914546_0_0_0 = { 1, GenInst_IEndDragHandler_t2789914546_0_0_0_Types };
extern const Il2CppType IDropHandler_t4146418618_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t4146418618_0_0_0_Types[] = { &IDropHandler_t4146418618_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t4146418618_0_0_0 = { 1, GenInst_IDropHandler_t4146418618_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3315383580_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3315383580_0_0_0_Types[] = { &IScrollHandler_t3315383580_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3315383580_0_0_0 = { 1, GenInst_IScrollHandler_t3315383580_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3805412741_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3805412741_0_0_0_Types[] = { &IUpdateSelectedHandler_t3805412741_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3805412741_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3805412741_0_0_0_Types };
extern const Il2CppType IMoveHandler_t3984942232_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t3984942232_0_0_0_Types[] = { &IMoveHandler_t3984942232_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t3984942232_0_0_0 = { 1, GenInst_IMoveHandler_t3984942232_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t2608359793_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t2608359793_0_0_0_Types[] = { &ISubmitHandler_t2608359793_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t2608359793_0_0_0 = { 1, GenInst_ISubmitHandler_t2608359793_0_0_0_Types };
extern const Il2CppType ICancelHandler_t4180011215_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t4180011215_0_0_0_Types[] = { &ICancelHandler_t4180011215_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t4180011215_0_0_0 = { 1, GenInst_ICancelHandler_t4180011215_0_0_0_Types };
extern const Il2CppType Transform_t1659122786_0_0_0;
static const Il2CppType* GenInst_Transform_t1659122786_0_0_0_Types[] = { &Transform_t1659122786_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t1659122786_0_0_0 = { 1, GenInst_Transform_t1659122786_0_0_0_Types };
extern const Il2CppType PointerEventData_t1848751023_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PointerEventData_t1848751023_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &PointerEventData_t1848751023_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744794968_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744794968_0_0_0_Types[] = { &KeyValuePair_2_t1744794968_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744794968_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744794968_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1848751023_0_0_0_Types[] = { &PointerEventData_t1848751023_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1848751023_0_0_0 = { 1, GenInst_PointerEventData_t1848751023_0_0_0_Types };
extern const Il2CppType ButtonState_t2039702646_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2039702646_0_0_0_Types[] = { &ButtonState_t2039702646_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2039702646_0_0_0 = { 1, GenInst_ButtonState_t2039702646_0_0_0_Types };
extern const Il2CppType ICanvasElement_t1249239335_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0 = { 1, GenInst_ICanvasElement_t1249239335_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ICanvasElement_t1249239335_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ColorBlock_t508458230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t508458230_0_0_0_Types[] = { &ColorBlock_t508458230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t508458230_0_0_0 = { 1, GenInst_ColorBlock_t508458230_0_0_0_Types };
extern const Il2CppType OptionData_t185687546_0_0_0;
static const Il2CppType* GenInst_OptionData_t185687546_0_0_0_Types[] = { &OptionData_t185687546_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t185687546_0_0_0 = { 1, GenInst_OptionData_t185687546_0_0_0_Types };
extern const Il2CppType DropdownItem_t3215807551_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t3215807551_0_0_0_Types[] = { &DropdownItem_t3215807551_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t3215807551_0_0_0 = { 1, GenInst_DropdownItem_t3215807551_0_0_0_Types };
extern const Il2CppType FloatTween_t2711705593_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2711705593_0_0_0_Types[] = { &FloatTween_t2711705593_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2711705593_0_0_0 = { 1, GenInst_FloatTween_t2711705593_0_0_0_Types };
extern const Il2CppType Sprite_t3199167241_0_0_0;
static const Il2CppType* GenInst_Sprite_t3199167241_0_0_0_Types[] = { &Sprite_t3199167241_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t3199167241_0_0_0 = { 1, GenInst_Sprite_t3199167241_0_0_0_Types };
extern const Il2CppType Canvas_t2727140764_0_0_0;
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0 = { 1, GenInst_Canvas_t2727140764_0_0_0_Types };
extern const Il2CppType List_1_t4095326316_0_0_0;
static const Il2CppType* GenInst_List_1_t4095326316_0_0_0_Types[] = { &List_1_t4095326316_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4095326316_0_0_0 = { 1, GenInst_List_1_t4095326316_0_0_0_Types };
extern const Il2CppType List_1_t1377224777_0_0_0;
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_Types[] = { &Font_t4241557075_0_0_0, &List_1_t1377224777_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0 = { 2, GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_Types };
extern const Il2CppType Text_t9039225_0_0_0;
static const Il2CppType* GenInst_Text_t9039225_0_0_0_Types[] = { &Text_t9039225_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t9039225_0_0_0 = { 1, GenInst_Text_t9039225_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Font_t4241557075_0_0_0, &List_1_t1377224777_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1496360359_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1496360359_0_0_0_Types[] = { &KeyValuePair_2_t1496360359_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1496360359_0_0_0 = { 1, GenInst_KeyValuePair_2_t1496360359_0_0_0_Types };
extern const Il2CppType ColorTween_t723277650_0_0_0;
static const Il2CppType* GenInst_ColorTween_t723277650_0_0_0_Types[] = { &ColorTween_t723277650_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t723277650_0_0_0 = { 1, GenInst_ColorTween_t723277650_0_0_0_Types };
extern const Il2CppType Graphic_t836799438_0_0_0;
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Types[] = { &Graphic_t836799438_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0 = { 1, GenInst_Graphic_t836799438_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3656898711_0_0_0;
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0, &IndexedSet_1_t3656898711_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0 = { 2, GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Graphic_t836799438_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Graphic_t836799438_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Canvas_t2727140764_0_0_0, &IndexedSet_1_t3656898711_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3226014568_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3226014568_0_0_0_Types[] = { &KeyValuePair_2_t3226014568_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3226014568_0_0_0 = { 1, GenInst_KeyValuePair_2_t3226014568_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1354567995_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1354567995_0_0_0_Types[] = { &KeyValuePair_2_t1354567995_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1354567995_0_0_0 = { 1, GenInst_KeyValuePair_2_t1354567995_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3983107678_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3983107678_0_0_0_Types[] = { &KeyValuePair_2_t3983107678_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3983107678_0_0_0 = { 1, GenInst_KeyValuePair_2_t3983107678_0_0_0_Types };
extern const Il2CppType ContentType_t2662964855_0_0_0;
static const Il2CppType* GenInst_ContentType_t2662964855_0_0_0_Types[] = { &ContentType_t2662964855_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t2662964855_0_0_0 = { 1, GenInst_ContentType_t2662964855_0_0_0_Types };
extern const Il2CppType Mask_t8826680_0_0_0;
static const Il2CppType* GenInst_Mask_t8826680_0_0_0_Types[] = { &Mask_t8826680_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t8826680_0_0_0 = { 1, GenInst_Mask_t8826680_0_0_0_Types };
extern const Il2CppType List_1_t1377012232_0_0_0;
static const Il2CppType* GenInst_List_1_t1377012232_0_0_0_Types[] = { &List_1_t1377012232_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1377012232_0_0_0 = { 1, GenInst_List_1_t1377012232_0_0_0_Types };
extern const Il2CppType RectMask2D_t3357079374_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t3357079374_0_0_0_Types[] = { &RectMask2D_t3357079374_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t3357079374_0_0_0 = { 1, GenInst_RectMask2D_t3357079374_0_0_0_Types };
extern const Il2CppType List_1_t430297630_0_0_0;
static const Il2CppType* GenInst_List_1_t430297630_0_0_0_Types[] = { &List_1_t430297630_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t430297630_0_0_0 = { 1, GenInst_List_1_t430297630_0_0_0_Types };
extern const Il2CppType Navigation_t1108456480_0_0_0;
static const Il2CppType* GenInst_Navigation_t1108456480_0_0_0_Types[] = { &Navigation_t1108456480_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1108456480_0_0_0 = { 1, GenInst_Navigation_t1108456480_0_0_0_Types };
extern const Il2CppType IClippable_t502791197_0_0_0;
static const Il2CppType* GenInst_IClippable_t502791197_0_0_0_Types[] = { &IClippable_t502791197_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t502791197_0_0_0 = { 1, GenInst_IClippable_t502791197_0_0_0_Types };
extern const Il2CppType Selectable_t1885181538_0_0_0;
static const Il2CppType* GenInst_Selectable_t1885181538_0_0_0_Types[] = { &Selectable_t1885181538_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1885181538_0_0_0 = { 1, GenInst_Selectable_t1885181538_0_0_0_Types };
extern const Il2CppType SpriteState_t2895308594_0_0_0;
static const Il2CppType* GenInst_SpriteState_t2895308594_0_0_0_Types[] = { &SpriteState_t2895308594_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t2895308594_0_0_0 = { 1, GenInst_SpriteState_t2895308594_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3702418109_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3702418109_0_0_0_Types[] = { &CanvasGroup_t3702418109_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3702418109_0_0_0 = { 1, GenInst_CanvasGroup_t3702418109_0_0_0_Types };
extern const Il2CppType MatEntry_t1574154081_0_0_0;
static const Il2CppType* GenInst_MatEntry_t1574154081_0_0_0_Types[] = { &MatEntry_t1574154081_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t1574154081_0_0_0 = { 1, GenInst_MatEntry_t1574154081_0_0_0_Types };
extern const Il2CppType Toggle_t110812896_0_0_0;
static const Il2CppType* GenInst_Toggle_t110812896_0_0_0_Types[] = { &Toggle_t110812896_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t110812896_0_0_0 = { 1, GenInst_Toggle_t110812896_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Toggle_t110812896_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType IClipper_t1175935472_0_0_0;
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0 = { 1, GenInst_IClipper_t1175935472_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &IClipper_t1175935472_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1329917009_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1329917009_0_0_0_Types[] = { &KeyValuePair_2_t1329917009_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1329917009_0_0_0 = { 1, GenInst_KeyValuePair_2_t1329917009_0_0_0_Types };
extern const Il2CppType RectTransform_t972643934_0_0_0;
static const Il2CppType* GenInst_RectTransform_t972643934_0_0_0_Types[] = { &RectTransform_t972643934_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t972643934_0_0_0 = { 1, GenInst_RectTransform_t972643934_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t1942933988_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t1942933988_0_0_0_Types[] = { &LayoutRebuilder_t1942933988_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t1942933988_0_0_0 = { 1, GenInst_LayoutRebuilder_t1942933988_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1646037781_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0_Types[] = { &ILayoutElement_t1646037781_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType List_1_t1355284822_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284822_0_0_0_Types[] = { &List_1_t1355284822_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284822_0_0_0 = { 1, GenInst_List_1_t1355284822_0_0_0_Types };
extern const Il2CppType List_1_t1967039240_0_0_0;
static const Il2CppType* GenInst_List_1_t1967039240_0_0_0_Types[] = { &List_1_t1967039240_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1967039240_0_0_0 = { 1, GenInst_List_1_t1967039240_0_0_0_Types };
extern const Il2CppType List_1_t1355284821_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284821_0_0_0_Types[] = { &List_1_t1355284821_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284821_0_0_0 = { 1, GenInst_List_1_t1355284821_0_0_0_Types };
extern const Il2CppType List_1_t1355284823_0_0_0;
static const Il2CppType* GenInst_List_1_t1355284823_0_0_0_Types[] = { &List_1_t1355284823_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1355284823_0_0_0 = { 1, GenInst_List_1_t1355284823_0_0_0_Types };
extern const Il2CppType List_1_t2522024052_0_0_0;
static const Il2CppType* GenInst_List_1_t2522024052_0_0_0_Types[] = { &List_1_t2522024052_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2522024052_0_0_0 = { 1, GenInst_List_1_t2522024052_0_0_0_Types };
extern const Il2CppType List_1_t1317283468_0_0_0;
static const Il2CppType* GenInst_List_1_t1317283468_0_0_0_Types[] = { &List_1_t1317283468_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1317283468_0_0_0 = { 1, GenInst_List_1_t1317283468_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType VideoInfo_t2099471191_0_0_0;
static const Il2CppType* GenInst_VideoInfo_t2099471191_0_0_0_Types[] = { &VideoInfo_t2099471191_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoInfo_t2099471191_0_0_0 = { 1, GenInst_VideoInfo_t2099471191_0_0_0_Types };
extern const Il2CppType ExtractionInfo_t925438340_0_0_0;
static const Il2CppType* GenInst_ExtractionInfo_t925438340_0_0_0_Types[] = { &ExtractionInfo_t925438340_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtractionInfo_t925438340_0_0_0 = { 1, GenInst_ExtractionInfo_t925438340_0_0_0_Types };
static const Il2CppType* GenInst_VideoInfo_t2099471191_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &VideoInfo_t2099471191_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoInfo_t2099471191_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_VideoInfo_t2099471191_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Action_t3771233898_0_0_0;
static const Il2CppType* GenInst_Action_t3771233898_0_0_0_Types[] = { &Action_t3771233898_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3771233898_0_0_0 = { 1, GenInst_Action_t3771233898_0_0_0_Types };
extern const Il2CppType CommonStatusCodes_t671203459_0_0_0;
static const Il2CppType* GenInst_CommonStatusCodes_t671203459_0_0_0_String_t_0_0_0_Types[] = { &CommonStatusCodes_t671203459_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonStatusCodes_t671203459_0_0_0_String_t_0_0_0 = { 2, GenInst_CommonStatusCodes_t671203459_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_CommonStatusCodes_t671203459_0_0_0_Il2CppObject_0_0_0_Types[] = { &CommonStatusCodes_t671203459_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonStatusCodes_t671203459_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_CommonStatusCodes_t671203459_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PlayerStats_t60064856_0_0_0;
static const Il2CppType* GenInst_CommonStatusCodes_t671203459_0_0_0_PlayerStats_t60064856_0_0_0_Types[] = { &CommonStatusCodes_t671203459_0_0_0, &PlayerStats_t60064856_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonStatusCodes_t671203459_0_0_0_PlayerStats_t60064856_0_0_0 = { 2, GenInst_CommonStatusCodes_t671203459_0_0_0_PlayerStats_t60064856_0_0_0_Types };
extern const Il2CppType AchievementU5BU5D_t3251685236_0_0_0;
static const Il2CppType* GenInst_AchievementU5BU5D_t3251685236_0_0_0_Types[] = { &AchievementU5BU5D_t3251685236_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementU5BU5D_t3251685236_0_0_0 = { 1, GenInst_AchievementU5BU5D_t3251685236_0_0_0_Types };
extern const Il2CppType Achievement_t1261647177_0_0_0;
static const Il2CppType* GenInst_Achievement_t1261647177_0_0_0_Types[] = { &Achievement_t1261647177_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1261647177_0_0_0 = { 1, GenInst_Achievement_t1261647177_0_0_0_Types };
extern const Il2CppType UIStatus_t427705392_0_0_0;
static const Il2CppType* GenInst_UIStatus_t427705392_0_0_0_Types[] = { &UIStatus_t427705392_0_0_0 };
extern const Il2CppGenericInst GenInst_UIStatus_t427705392_0_0_0 = { 1, GenInst_UIStatus_t427705392_0_0_0_Types };
extern const Il2CppType LeaderboardScoreData_t4006482697_0_0_0;
static const Il2CppType* GenInst_LeaderboardScoreData_t4006482697_0_0_0_Types[] = { &LeaderboardScoreData_t4006482697_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardScoreData_t4006482697_0_0_0 = { 1, GenInst_LeaderboardScoreData_t4006482697_0_0_0_Types };
extern const Il2CppType ResponseStatus_t419677757_0_0_0;
extern const Il2CppType List_1_t2555968713_0_0_0;
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_List_1_t2555968713_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &List_1_t2555968713_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_List_1_t2555968713_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_List_1_t2555968713_0_0_0_Types };
extern const Il2CppType IEvent_t1187783161_0_0_0;
static const Il2CppType* GenInst_IEvent_t1187783161_0_0_0_Types[] = { &IEvent_t1187783161_0_0_0 };
extern const Il2CppGenericInst GenInst_IEvent_t1187783161_0_0_0 = { 1, GenInst_IEvent_t1187783161_0_0_0_Types };
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_Il2CppObject_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_IEvent_t1187783161_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &IEvent_t1187783161_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_IEvent_t1187783161_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_IEvent_t1187783161_0_0_0_Types };
extern const Il2CppType PlayGamesScore_t486124539_0_0_0;
static const Il2CppType* GenInst_PlayGamesScore_t486124539_0_0_0_Types[] = { &PlayGamesScore_t486124539_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayGamesScore_t486124539_0_0_0 = { 1, GenInst_PlayGamesScore_t486124539_0_0_0_Types };
extern const Il2CppType InvitationU5BU5D_t4211826234_0_0_0;
static const Il2CppType* GenInst_InvitationU5BU5D_t4211826234_0_0_0_Types[] = { &InvitationU5BU5D_t4211826234_0_0_0 };
extern const Il2CppGenericInst GenInst_InvitationU5BU5D_t4211826234_0_0_0 = { 1, GenInst_InvitationU5BU5D_t4211826234_0_0_0_Types };
extern const Il2CppType Invitation_t2200833403_0_0_0;
static const Il2CppType* GenInst_Invitation_t2200833403_0_0_0_Types[] = { &Invitation_t2200833403_0_0_0 };
extern const Il2CppGenericInst GenInst_Invitation_t2200833403_0_0_0 = { 1, GenInst_Invitation_t2200833403_0_0_0_Types };
extern const Il2CppType Participant_t1804230813_0_0_0;
static const Il2CppType* GenInst_Participant_t1804230813_0_0_0_Types[] = { &Participant_t1804230813_0_0_0 };
extern const Il2CppGenericInst GenInst_Participant_t1804230813_0_0_0 = { 1, GenInst_Participant_t1804230813_0_0_0_Types };
extern const Il2CppType TurnBasedMatch_t3573041681_0_0_0;
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_TurnBasedMatch_t3573041681_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &TurnBasedMatch_t3573041681_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_TurnBasedMatch_t3573041681_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_TurnBasedMatch_t3573041681_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UIStatus_t427705392_0_0_0_TurnBasedMatch_t3573041681_0_0_0_Types[] = { &UIStatus_t427705392_0_0_0, &TurnBasedMatch_t3573041681_0_0_0 };
extern const Il2CppGenericInst GenInst_UIStatus_t427705392_0_0_0_TurnBasedMatch_t3573041681_0_0_0 = { 2, GenInst_UIStatus_t427705392_0_0_0_TurnBasedMatch_t3573041681_0_0_0_Types };
static const Il2CppType* GenInst_UIStatus_t427705392_0_0_0_Il2CppObject_0_0_0_Types[] = { &UIStatus_t427705392_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UIStatus_t427705392_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UIStatus_t427705392_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType TurnBasedMatchU5BU5D_t2145394316_0_0_0;
static const Il2CppType* GenInst_TurnBasedMatchU5BU5D_t2145394316_0_0_0_Types[] = { &TurnBasedMatchU5BU5D_t2145394316_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnBasedMatchU5BU5D_t2145394316_0_0_0 = { 1, GenInst_TurnBasedMatchU5BU5D_t2145394316_0_0_0_Types };
static const Il2CppType* GenInst_TurnBasedMatch_t3573041681_0_0_0_Types[] = { &TurnBasedMatch_t3573041681_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnBasedMatch_t3573041681_0_0_0 = { 1, GenInst_TurnBasedMatch_t3573041681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_Types[] = { &String_t_0_0_0, &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0 = { 2, GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2093487883_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2093487883_0_0_0_Types[] = { &KeyValuePair_2_t2093487883_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487883_0_0_0 = { 1, GenInst_KeyValuePair_2_t2093487883_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt32_t24667981_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt32_t24667981_0_0_0, &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt32_t24667981_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_KeyValuePair_2_t2093487883_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt32_t24667981_0_0_0, &KeyValuePair_2_t2093487883_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_KeyValuePair_2_t2093487883_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_KeyValuePair_2_t2093487883_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &UInt32_t24667981_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t743867057_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t743867057_0_0_0_Types[] = { &KeyValuePair_2_t743867057_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t743867057_0_0_0 = { 1, GenInst_KeyValuePair_2_t743867057_0_0_0_Types };
extern const Il2CppType ParticipantResult_t2327217482_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_Types[] = { &String_t_0_0_0, &ParticipantResult_t2327217482_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0 = { 2, GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ParticipantResult_t2327217482_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t101070088_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t101070088_0_0_0_Types[] = { &KeyValuePair_2_t101070088_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t101070088_0_0_0 = { 1, GenInst_KeyValuePair_2_t101070088_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantResult_t2327217482_0_0_0_Types[] = { &ParticipantResult_t2327217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantResult_t2327217482_0_0_0 = { 1, GenInst_ParticipantResult_t2327217482_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ParticipantResult_t2327217482_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ParticipantResult_t2327217482_0_0_0, &ParticipantResult_t2327217482_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ParticipantResult_t2327217482_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_KeyValuePair_2_t101070088_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ParticipantResult_t2327217482_0_0_0, &KeyValuePair_2_t101070088_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_KeyValuePair_2_t101070088_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_KeyValuePair_2_t101070088_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &ParticipantResult_t2327217482_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3046416558_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3046416558_0_0_0_Types[] = { &KeyValuePair_2_t3046416558_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3046416558_0_0_0 = { 1, GenInst_KeyValuePair_2_t3046416558_0_0_0_Types };
static const Il2CppType* GenInst_Participant_t1804230813_0_0_0_String_t_0_0_0_Types[] = { &Participant_t1804230813_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Participant_t1804230813_0_0_0_String_t_0_0_0 = { 2, GenInst_Participant_t1804230813_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType AdvertisingResult_t68742423_0_0_0;
static const Il2CppType* GenInst_AdvertisingResult_t68742423_0_0_0_Types[] = { &AdvertisingResult_t68742423_0_0_0 };
extern const Il2CppGenericInst GenInst_AdvertisingResult_t68742423_0_0_0 = { 1, GenInst_AdvertisingResult_t68742423_0_0_0_Types };
extern const Il2CppType ConnectionRequest_t4159799943_0_0_0;
static const Il2CppType* GenInst_ConnectionRequest_t4159799943_0_0_0_Types[] = { &ConnectionRequest_t4159799943_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionRequest_t4159799943_0_0_0 = { 1, GenInst_ConnectionRequest_t4159799943_0_0_0_Types };
extern const Il2CppType ConnectionResponse_t4280027939_0_0_0;
static const Il2CppType* GenInst_ConnectionResponse_t4280027939_0_0_0_Types[] = { &ConnectionResponse_t4280027939_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionResponse_t4280027939_0_0_0 = { 1, GenInst_ConnectionResponse_t4280027939_0_0_0_Types };
extern const Il2CppType InitializationStatus_t518463702_0_0_0;
static const Il2CppType* GenInst_InitializationStatus_t518463702_0_0_0_Types[] = { &InitializationStatus_t518463702_0_0_0 };
extern const Il2CppGenericInst GenInst_InitializationStatus_t518463702_0_0_0 = { 1, GenInst_InitializationStatus_t518463702_0_0_0_Types };
extern const Il2CppType IQuest_t562088433_0_0_0;
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_IQuest_t562088433_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &IQuest_t562088433_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_IQuest_t562088433_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_IQuest_t562088433_0_0_0_Types };
extern const Il2CppType List_1_t1930273985_0_0_0;
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_List_1_t1930273985_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &List_1_t1930273985_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_List_1_t1930273985_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_List_1_t1930273985_0_0_0_Types };
static const Il2CppType* GenInst_IQuest_t562088433_0_0_0_Types[] = { &IQuest_t562088433_0_0_0 };
extern const Il2CppGenericInst GenInst_IQuest_t562088433_0_0_0 = { 1, GenInst_IQuest_t562088433_0_0_0_Types };
extern const Il2CppType QuestUiResult_t3187252993_0_0_0;
extern const Il2CppType IQuestMilestone_t3485030629_0_0_0;
static const Il2CppType* GenInst_QuestUiResult_t3187252993_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0_Types[] = { &QuestUiResult_t3187252993_0_0_0, &IQuest_t562088433_0_0_0, &IQuestMilestone_t3485030629_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestUiResult_t3187252993_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0 = { 3, GenInst_QuestUiResult_t3187252993_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0_Types };
static const Il2CppType* GenInst_QuestUiResult_t3187252993_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &QuestUiResult_t3187252993_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestUiResult_t3187252993_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_QuestUiResult_t3187252993_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType QuestAcceptStatus_t3857037258_0_0_0;
static const Il2CppType* GenInst_QuestAcceptStatus_t3857037258_0_0_0_IQuest_t562088433_0_0_0_Types[] = { &QuestAcceptStatus_t3857037258_0_0_0, &IQuest_t562088433_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestAcceptStatus_t3857037258_0_0_0_IQuest_t562088433_0_0_0 = { 2, GenInst_QuestAcceptStatus_t3857037258_0_0_0_IQuest_t562088433_0_0_0_Types };
static const Il2CppType* GenInst_QuestAcceptStatus_t3857037258_0_0_0_Il2CppObject_0_0_0_Types[] = { &QuestAcceptStatus_t3857037258_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestAcceptStatus_t3857037258_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_QuestAcceptStatus_t3857037258_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType QuestClaimMilestoneStatus_t935666006_0_0_0;
static const Il2CppType* GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0_Types[] = { &QuestClaimMilestoneStatus_t935666006_0_0_0, &IQuest_t562088433_0_0_0, &IQuestMilestone_t3485030629_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0 = { 3, GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0_Types };
static const Il2CppType* GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &QuestClaimMilestoneStatus_t935666006_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType SavedGameRequestStatus_t3786215536_0_0_0;
extern const Il2CppType ISavedGameMetadata_t3582269991_0_0_0;
static const Il2CppType* GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ISavedGameMetadata_t3582269991_0_0_0_Types[] = { &SavedGameRequestStatus_t3786215536_0_0_0, &ISavedGameMetadata_t3582269991_0_0_0 };
extern const Il2CppGenericInst GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ISavedGameMetadata_t3582269991_0_0_0 = { 2, GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ISavedGameMetadata_t3582269991_0_0_0_Types };
static const Il2CppType* GenInst_SavedGameRequestStatus_t3786215536_0_0_0_Il2CppObject_0_0_0_Types[] = { &SavedGameRequestStatus_t3786215536_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SavedGameRequestStatus_t3786215536_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SavedGameRequestStatus_t3786215536_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Types[] = { &SavedGameRequestStatus_t3786215536_0_0_0, &ByteU5BU5D_t4260760469_0_0_0 };
extern const Il2CppGenericInst GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ByteU5BU5D_t4260760469_0_0_0 = { 2, GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Types };
extern const Il2CppType SelectUIStatus_t4210182474_0_0_0;
static const Il2CppType* GenInst_SelectUIStatus_t4210182474_0_0_0_ISavedGameMetadata_t3582269991_0_0_0_Types[] = { &SelectUIStatus_t4210182474_0_0_0, &ISavedGameMetadata_t3582269991_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectUIStatus_t4210182474_0_0_0_ISavedGameMetadata_t3582269991_0_0_0 = { 2, GenInst_SelectUIStatus_t4210182474_0_0_0_ISavedGameMetadata_t3582269991_0_0_0_Types };
static const Il2CppType* GenInst_SelectUIStatus_t4210182474_0_0_0_Il2CppObject_0_0_0_Types[] = { &SelectUIStatus_t4210182474_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectUIStatus_t4210182474_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SelectUIStatus_t4210182474_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType List_1_t655488247_0_0_0;
static const Il2CppType* GenInst_SavedGameRequestStatus_t3786215536_0_0_0_List_1_t655488247_0_0_0_Types[] = { &SavedGameRequestStatus_t3786215536_0_0_0, &List_1_t655488247_0_0_0 };
extern const Il2CppGenericInst GenInst_SavedGameRequestStatus_t3786215536_0_0_0_List_1_t655488247_0_0_0 = { 2, GenInst_SavedGameRequestStatus_t3786215536_0_0_0_List_1_t655488247_0_0_0_Types };
static const Il2CppType* GenInst_ISavedGameMetadata_t3582269991_0_0_0_Types[] = { &ISavedGameMetadata_t3582269991_0_0_0 };
extern const Il2CppGenericInst GenInst_ISavedGameMetadata_t3582269991_0_0_0 = { 1, GenInst_ISavedGameMetadata_t3582269991_0_0_0_Types };
extern const Il2CppType INearbyConnectionClient_t1732200103_0_0_0;
static const Il2CppType* GenInst_INearbyConnectionClient_t1732200103_0_0_0_Types[] = { &INearbyConnectionClient_t1732200103_0_0_0 };
extern const Il2CppGenericInst GenInst_INearbyConnectionClient_t1732200103_0_0_0 = { 1, GenInst_INearbyConnectionClient_t1732200103_0_0_0_Types };
extern const Il2CppType Action_1_t872614854_0_0_0;
static const Il2CppType* GenInst_Action_1_t872614854_0_0_0_Types[] = { &Action_1_t872614854_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t872614854_0_0_0 = { 1, GenInst_Action_1_t872614854_0_0_0_Types };
static const Il2CppType* GenInst_Invitation_t2200833403_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Invitation_t2200833403_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Invitation_t2200833403_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Invitation_t2200833403_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_Types[] = { &String_t_0_0_0, &Achievement_t1261647177_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0 = { 2, GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Achievement_t1261647177_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1980846253_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1980846253_0_0_0_Types[] = { &KeyValuePair_2_t1980846253_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1980846253_0_0_0 = { 1, GenInst_KeyValuePair_2_t1980846253_0_0_0_Types };
extern const Il2CppType Player_t3727527619_0_0_0;
static const Il2CppType* GenInst_Player_t3727527619_0_0_0_Types[] = { &Player_t3727527619_0_0_0 };
extern const Il2CppGenericInst GenInst_Player_t3727527619_0_0_0 = { 1, GenInst_Player_t3727527619_0_0_0_Types };
extern const Il2CppType MultiplayerEvent_t2613789975_0_0_0;
extern const Il2CppType NativeTurnBasedMatch_t302853426_0_0_0;
static const Il2CppType* GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types[] = { &MultiplayerEvent_t2613789975_0_0_0, &String_t_0_0_0, &NativeTurnBasedMatch_t302853426_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0 = { 3, GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerEvent_t2613789975_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &MultiplayerEvent_t2613789975_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerEvent_t2613789975_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_MultiplayerEvent_t2613789975_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType MultiplayerInvitation_t3411188537_0_0_0;
static const Il2CppType* GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0_Types[] = { &MultiplayerEvent_t2613789975_0_0_0, &String_t_0_0_0, &MultiplayerInvitation_t3411188537_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0 = { 3, GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0_Types };
extern const Il2CppType FetchServerAuthCodeResponse_t1052060023_0_0_0;
static const Il2CppType* GenInst_FetchServerAuthCodeResponse_t1052060023_0_0_0_Types[] = { &FetchServerAuthCodeResponse_t1052060023_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchServerAuthCodeResponse_t1052060023_0_0_0 = { 1, GenInst_FetchServerAuthCodeResponse_t1052060023_0_0_0_Types };
extern const Il2CppType List_1_t800745875_0_0_0;
static const Il2CppType* GenInst_ResponseStatus_t419677757_0_0_0_List_1_t800745875_0_0_0_Types[] = { &ResponseStatus_t419677757_0_0_0, &List_1_t800745875_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t419677757_0_0_0_List_1_t800745875_0_0_0 = { 2, GenInst_ResponseStatus_t419677757_0_0_0_List_1_t800745875_0_0_0_Types };
extern const Il2CppType NativeAchievement_t2621183934_0_0_0;
static const Il2CppType* GenInst_NativeAchievement_t2621183934_0_0_0_Types[] = { &NativeAchievement_t2621183934_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeAchievement_t2621183934_0_0_0 = { 1, GenInst_NativeAchievement_t2621183934_0_0_0_Types };
extern const Il2CppType FetchAllResponse_t2805709750_0_0_0;
static const Il2CppType* GenInst_FetchAllResponse_t2805709750_0_0_0_Types[] = { &FetchAllResponse_t2805709750_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchAllResponse_t2805709750_0_0_0 = { 1, GenInst_FetchAllResponse_t2805709750_0_0_0_Types };
extern const Il2CppType FetchSelfResponse_t1087840609_0_0_0;
static const Il2CppType* GenInst_FetchSelfResponse_t1087840609_0_0_0_Types[] = { &FetchSelfResponse_t1087840609_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchSelfResponse_t1087840609_0_0_0 = { 1, GenInst_FetchSelfResponse_t1087840609_0_0_0_Types };
extern const Il2CppType FetchForPlayerResponse_t1267220047_0_0_0;
static const Il2CppType* GenInst_FetchForPlayerResponse_t1267220047_0_0_0_Types[] = { &FetchForPlayerResponse_t1267220047_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchForPlayerResponse_t1267220047_0_0_0 = { 1, GenInst_FetchForPlayerResponse_t1267220047_0_0_0_Types };
extern const Il2CppType NativePlayer_t2636885988_0_0_0;
static const Il2CppType* GenInst_NativePlayer_t2636885988_0_0_0_Types[] = { &NativePlayer_t2636885988_0_0_0 };
extern const Il2CppGenericInst GenInst_NativePlayer_t2636885988_0_0_0 = { 1, GenInst_NativePlayer_t2636885988_0_0_0_Types };
extern const Il2CppType BaseReferenceHolder_t2237584300_0_0_0;
static const Il2CppType* GenInst_BaseReferenceHolder_t2237584300_0_0_0_Types[] = { &BaseReferenceHolder_t2237584300_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseReferenceHolder_t2237584300_0_0_0 = { 1, GenInst_BaseReferenceHolder_t2237584300_0_0_0_Types };
extern const Il2CppType IDisposable_t1423340799_0_0_0;
static const Il2CppType* GenInst_IDisposable_t1423340799_0_0_0_Types[] = { &IDisposable_t1423340799_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t1423340799_0_0_0 = { 1, GenInst_IDisposable_t1423340799_0_0_0_Types };
extern const Il2CppType NativePlayerU5BU5D_t3461726221_0_0_0;
static const Il2CppType* GenInst_NativePlayerU5BU5D_t3461726221_0_0_0_Types[] = { &NativePlayerU5BU5D_t3461726221_0_0_0 };
extern const Il2CppGenericInst GenInst_NativePlayerU5BU5D_t3461726221_0_0_0 = { 1, GenInst_NativePlayerU5BU5D_t3461726221_0_0_0_Types };
extern const Il2CppType FetchResponse_t2513188365_0_0_0;
static const Il2CppType* GenInst_FetchResponse_t2513188365_0_0_0_Types[] = { &FetchResponse_t2513188365_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchResponse_t2513188365_0_0_0 = { 1, GenInst_FetchResponse_t2513188365_0_0_0_Types };
extern const Il2CppType UIStatus_t3557407943_0_0_0;
static const Il2CppType* GenInst_UIStatus_t3557407943_0_0_0_Types[] = { &UIStatus_t3557407943_0_0_0 };
extern const Il2CppGenericInst GenInst_UIStatus_t3557407943_0_0_0 = { 1, GenInst_UIStatus_t3557407943_0_0_0_Types };
extern const Il2CppType FetchAllResponse_t1890238433_0_0_0;
static const Il2CppType* GenInst_FetchAllResponse_t1890238433_0_0_0_Types[] = { &FetchAllResponse_t1890238433_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchAllResponse_t1890238433_0_0_0 = { 1, GenInst_FetchAllResponse_t1890238433_0_0_0_Types };
extern const Il2CppType FetchResponse_t3476068802_0_0_0;
static const Il2CppType* GenInst_FetchResponse_t3476068802_0_0_0_Types[] = { &FetchResponse_t3476068802_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchResponse_t3476068802_0_0_0 = { 1, GenInst_FetchResponse_t3476068802_0_0_0_Types };
extern const Il2CppType NativeEvent_t2485247913_0_0_0;
static const Il2CppType* GenInst_NativeEvent_t2485247913_0_0_0_Types[] = { &NativeEvent_t2485247913_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeEvent_t2485247913_0_0_0 = { 1, GenInst_NativeEvent_t2485247913_0_0_0_Types };
extern const Il2CppType FetchResponse_t3559403642_0_0_0;
static const Il2CppType* GenInst_FetchResponse_t3559403642_0_0_0_Types[] = { &FetchResponse_t3559403642_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchResponse_t3559403642_0_0_0 = { 1, GenInst_FetchResponse_t3559403642_0_0_0_Types };
extern const Il2CppType FetchListResponse_t32965304_0_0_0;
static const Il2CppType* GenInst_FetchListResponse_t32965304_0_0_0_Types[] = { &FetchListResponse_t32965304_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchListResponse_t32965304_0_0_0 = { 1, GenInst_FetchListResponse_t32965304_0_0_0_Types };
extern const Il2CppType QuestUIResponse_t3819980022_0_0_0;
static const Il2CppType* GenInst_QuestUIResponse_t3819980022_0_0_0_Types[] = { &QuestUIResponse_t3819980022_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestUIResponse_t3819980022_0_0_0 = { 1, GenInst_QuestUIResponse_t3819980022_0_0_0_Types };
extern const Il2CppType AcceptResponse_t3485599210_0_0_0;
static const Il2CppType* GenInst_AcceptResponse_t3485599210_0_0_0_Types[] = { &AcceptResponse_t3485599210_0_0_0 };
extern const Il2CppGenericInst GenInst_AcceptResponse_t3485599210_0_0_0 = { 1, GenInst_AcceptResponse_t3485599210_0_0_0_Types };
extern const Il2CppType ClaimMilestoneResponse_t683149430_0_0_0;
static const Il2CppType* GenInst_ClaimMilestoneResponse_t683149430_0_0_0_Types[] = { &ClaimMilestoneResponse_t683149430_0_0_0 };
extern const Il2CppGenericInst GenInst_ClaimMilestoneResponse_t683149430_0_0_0 = { 1, GenInst_ClaimMilestoneResponse_t683149430_0_0_0_Types };
extern const Il2CppType NativeQuest_t2496300529_0_0_0;
static const Il2CppType* GenInst_NativeQuest_t2496300529_0_0_0_Types[] = { &NativeQuest_t2496300529_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeQuest_t2496300529_0_0_0 = { 1, GenInst_NativeQuest_t2496300529_0_0_0_Types };
extern const Il2CppType NativeRealTimeRoom_t3104490121_0_0_0;
extern const Il2CppType MultiplayerParticipant_t3337232325_0_0_0;
static const Il2CppType* GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &NativeRealTimeRoom_t3104490121_0_0_0, &MultiplayerParticipant_t3337232325_0_0_0, &ByteU5BU5D_t4260760469_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Boolean_t476798718_0_0_0 = { 4, GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types[] = { &NativeRealTimeRoom_t3104490121_0_0_0, &MultiplayerParticipant_t3337232325_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0 = { 2, GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types };
static const Il2CppType* GenInst_NativeRealTimeRoom_t3104490121_0_0_0_Types[] = { &NativeRealTimeRoom_t3104490121_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeRealTimeRoom_t3104490121_0_0_0 = { 1, GenInst_NativeRealTimeRoom_t3104490121_0_0_0_Types };
extern const Il2CppType PlayerSelectUIResponse_t922401854_0_0_0;
static const Il2CppType* GenInst_PlayerSelectUIResponse_t922401854_0_0_0_Types[] = { &PlayerSelectUIResponse_t922401854_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerSelectUIResponse_t922401854_0_0_0 = { 1, GenInst_PlayerSelectUIResponse_t922401854_0_0_0_Types };
extern const Il2CppType FetchInvitationsResponse_t815788017_0_0_0;
static const Il2CppType* GenInst_FetchInvitationsResponse_t815788017_0_0_0_Types[] = { &FetchInvitationsResponse_t815788017_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchInvitationsResponse_t815788017_0_0_0 = { 1, GenInst_FetchInvitationsResponse_t815788017_0_0_0_Types };
extern const Il2CppType RoomInboxUIResponse_t1117529840_0_0_0;
static const Il2CppType* GenInst_RoomInboxUIResponse_t1117529840_0_0_0_Types[] = { &RoomInboxUIResponse_t1117529840_0_0_0 };
extern const Il2CppGenericInst GenInst_RoomInboxUIResponse_t1117529840_0_0_0 = { 1, GenInst_RoomInboxUIResponse_t1117529840_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types[] = { &String_t_0_0_0, &MultiplayerParticipant_t3337232325_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0 = { 2, GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &MultiplayerParticipant_t3337232325_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4056431401_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4056431401_0_0_0_Types[] = { &KeyValuePair_2_t4056431401_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4056431401_0_0_0 = { 1, GenInst_KeyValuePair_2_t4056431401_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_Types[] = { &String_t_0_0_0, &Participant_t1804230813_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0 = { 2, GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Participant_t1804230813_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2523429889_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2523429889_0_0_0_Types[] = { &KeyValuePair_2_t2523429889_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2523429889_0_0_0 = { 1, GenInst_KeyValuePair_2_t2523429889_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerParticipant_t3337232325_0_0_0_String_t_0_0_0_Types[] = { &MultiplayerParticipant_t3337232325_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerParticipant_t3337232325_0_0_0_String_t_0_0_0 = { 2, GenInst_MultiplayerParticipant_t3337232325_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerParticipant_t3337232325_0_0_0_Participant_t1804230813_0_0_0_Types[] = { &MultiplayerParticipant_t3337232325_0_0_0, &Participant_t1804230813_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerParticipant_t3337232325_0_0_0_Participant_t1804230813_0_0_0 = { 2, GenInst_MultiplayerParticipant_t3337232325_0_0_0_Participant_t1804230813_0_0_0_Types };
static const Il2CppType* GenInst_Participant_t1804230813_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Participant_t1804230813_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Participant_t1804230813_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Participant_t1804230813_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerParticipant_t3337232325_0_0_0_Types[] = { &MultiplayerParticipant_t3337232325_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerParticipant_t3337232325_0_0_0 = { 1, GenInst_MultiplayerParticipant_t3337232325_0_0_0_Types };
extern const Il2CppType MultiplayerStatus_t2675372491_0_0_0;
static const Il2CppType* GenInst_MultiplayerStatus_t2675372491_0_0_0_Types[] = { &MultiplayerStatus_t2675372491_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerStatus_t2675372491_0_0_0 = { 1, GenInst_MultiplayerStatus_t2675372491_0_0_0_Types };
extern const Il2CppType ParticipantStatus_t3137786926_0_0_0;
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0 = { 1, GenInst_ParticipantStatus_t3137786926_0_0_0_Types };
extern const Il2CppType Link_t1089569710_0_0_0;
static const Il2CppType* GenInst_Link_t1089569710_0_0_0_Types[] = { &Link_t1089569710_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1089569710_0_0_0 = { 1, GenInst_Link_t1089569710_0_0_0_Types };
extern const Il2CppType WaitingRoomUIResponse_t3333463309_0_0_0;
static const Il2CppType* GenInst_WaitingRoomUIResponse_t3333463309_0_0_0_Types[] = { &WaitingRoomUIResponse_t3333463309_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitingRoomUIResponse_t3333463309_0_0_0 = { 1, GenInst_WaitingRoomUIResponse_t3333463309_0_0_0_Types };
extern const Il2CppType ResponseStatus_t4049911828_0_0_0;
static const Il2CppType* GenInst_ResponseStatus_t4049911828_0_0_0_Types[] = { &ResponseStatus_t4049911828_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseStatus_t4049911828_0_0_0 = { 1, GenInst_ResponseStatus_t4049911828_0_0_0_Types };
extern const Il2CppType RealTimeRoomResponse_t2530940439_0_0_0;
static const Il2CppType* GenInst_RealTimeRoomResponse_t2530940439_0_0_0_Types[] = { &RealTimeRoomResponse_t2530940439_0_0_0 };
extern const Il2CppGenericInst GenInst_RealTimeRoomResponse_t2530940439_0_0_0 = { 1, GenInst_RealTimeRoomResponse_t2530940439_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerInvitation_t3411188537_0_0_0_Types[] = { &MultiplayerInvitation_t3411188537_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerInvitation_t3411188537_0_0_0 = { 1, GenInst_MultiplayerInvitation_t3411188537_0_0_0_Types };
extern const Il2CppType OpenResponse_t1933021492_0_0_0;
static const Il2CppType* GenInst_OpenResponse_t1933021492_0_0_0_Types[] = { &OpenResponse_t1933021492_0_0_0 };
extern const Il2CppGenericInst GenInst_OpenResponse_t1933021492_0_0_0 = { 1, GenInst_OpenResponse_t1933021492_0_0_0_Types };
extern const Il2CppType ReadResponse_t2292627584_0_0_0;
static const Il2CppType* GenInst_ReadResponse_t2292627584_0_0_0_Types[] = { &ReadResponse_t2292627584_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadResponse_t2292627584_0_0_0 = { 1, GenInst_ReadResponse_t2292627584_0_0_0_Types };
extern const Il2CppType SnapshotSelectUIResponse_t2500674014_0_0_0;
static const Il2CppType* GenInst_SnapshotSelectUIResponse_t2500674014_0_0_0_Types[] = { &SnapshotSelectUIResponse_t2500674014_0_0_0 };
extern const Il2CppGenericInst GenInst_SnapshotSelectUIResponse_t2500674014_0_0_0 = { 1, GenInst_SnapshotSelectUIResponse_t2500674014_0_0_0_Types };
extern const Il2CppType CommitResponse_t2801162465_0_0_0;
static const Il2CppType* GenInst_CommitResponse_t2801162465_0_0_0_Types[] = { &CommitResponse_t2801162465_0_0_0 };
extern const Il2CppGenericInst GenInst_CommitResponse_t2801162465_0_0_0 = { 1, GenInst_CommitResponse_t2801162465_0_0_0_Types };
extern const Il2CppType FetchAllResponse_t1240414705_0_0_0;
static const Il2CppType* GenInst_FetchAllResponse_t1240414705_0_0_0_Types[] = { &FetchAllResponse_t1240414705_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchAllResponse_t1240414705_0_0_0 = { 1, GenInst_FetchAllResponse_t1240414705_0_0_0_Types };
static const Il2CppType* GenInst_ByteU5BU5D_t4260760469_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Types[] = { &ByteU5BU5D_t4260760469_0_0_0, &ByteU5BU5D_t4260760469_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4260760469_0_0_0_ByteU5BU5D_t4260760469_0_0_0 = { 2, GenInst_ByteU5BU5D_t4260760469_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Types };
extern const Il2CppType NativeSnapshotMetadata_t3479575958_0_0_0;
static const Il2CppType* GenInst_NativeSnapshotMetadata_t3479575958_0_0_0_Types[] = { &NativeSnapshotMetadata_t3479575958_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeSnapshotMetadata_t3479575958_0_0_0 = { 1, GenInst_NativeSnapshotMetadata_t3479575958_0_0_0_Types };
static const Il2CppType* GenInst_TurnBasedMatch_t3573041681_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &TurnBasedMatch_t3573041681_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnBasedMatch_t3573041681_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_TurnBasedMatch_t3573041681_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType TurnBasedMatchResponse_t4255652325_0_0_0;
static const Il2CppType* GenInst_TurnBasedMatchResponse_t4255652325_0_0_0_Types[] = { &TurnBasedMatchResponse_t4255652325_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnBasedMatchResponse_t4255652325_0_0_0 = { 1, GenInst_TurnBasedMatchResponse_t4255652325_0_0_0_Types };
extern const Il2CppType TurnBasedMatchesResponse_t3460122515_0_0_0;
static const Il2CppType* GenInst_TurnBasedMatchesResponse_t3460122515_0_0_0_Types[] = { &TurnBasedMatchesResponse_t3460122515_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnBasedMatchesResponse_t3460122515_0_0_0 = { 1, GenInst_TurnBasedMatchesResponse_t3460122515_0_0_0_Types };
extern const Il2CppType MatchInboxUIResponse_t1459363083_0_0_0;
static const Il2CppType* GenInst_MatchInboxUIResponse_t1459363083_0_0_0_Types[] = { &MatchInboxUIResponse_t1459363083_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchInboxUIResponse_t1459363083_0_0_0 = { 1, GenInst_MatchInboxUIResponse_t1459363083_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerParticipant_t3337232325_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types[] = { &MultiplayerParticipant_t3337232325_0_0_0, &NativeTurnBasedMatch_t302853426_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerParticipant_t3337232325_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0 = { 2, GenInst_MultiplayerParticipant_t3337232325_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types };
static const Il2CppType* GenInst_NativeTurnBasedMatch_t302853426_0_0_0_Types[] = { &NativeTurnBasedMatch_t302853426_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeTurnBasedMatch_t302853426_0_0_0 = { 1, GenInst_NativeTurnBasedMatch_t302853426_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchAllResponse_t2805709750_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchAllResponse_t2805709750_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t2805709750_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchAllResponse_t2805709750_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchResponse_t2513188365_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchResponse_t2513188365_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t2513188365_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchResponse_t2513188365_0_0_0_Types };
extern const Il2CppType UIntPtr_t_0_0_0;
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativeAchievement_t2621183934_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativeAchievement_t2621183934_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeAchievement_t2621183934_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativeAchievement_t2621183934_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType HandleRef_t1780819301_0_0_0;
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &BaseReferenceHolder_t2237584300_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0 = { 2, GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t300380471_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t300380471_0_0_0_Types[] = { &KeyValuePair_2_t300380471_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t300380471_0_0_0 = { 1, GenInst_KeyValuePair_2_t300380471_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0 = { 1, GenInst_HandleRef_t1780819301_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_HandleRef_t1780819301_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &Il2CppObject_0_0_0, &HandleRef_t1780819301_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_HandleRef_t1780819301_0_0_0 = { 3, GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_HandleRef_t1780819301_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t300380471_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t300380471_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t300380471_0_0_0 = { 3, GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t300380471_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &BaseReferenceHolder_t2237584300_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2662115696_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2662115696_0_0_0_Types[] = { &KeyValuePair_2_t2662115696_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2662115696_0_0_0 = { 1, GenInst_KeyValuePair_2_t2662115696_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1890238433_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchAllResponse_t1890238433_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1890238433_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1890238433_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchResponse_t3476068802_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchResponse_t3476068802_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t3476068802_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchResponse_t3476068802_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_NativeEvent_t2485247913_0_0_0_Types[] = { &IntPtr_t_0_0_0, &NativeEvent_t2485247913_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_NativeEvent_t2485247913_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_NativeEvent_t2485247913_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchServerAuthCodeResponse_t1052060023_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchServerAuthCodeResponse_t1052060023_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchServerAuthCodeResponse_t1052060023_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchServerAuthCodeResponse_t1052060023_0_0_0_Types };
extern const Il2CppType FetchResponse_t642400449_0_0_0;
static const Il2CppType* GenInst_FetchResponse_t642400449_0_0_0_Types[] = { &FetchResponse_t642400449_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchResponse_t642400449_0_0_0 = { 1, GenInst_FetchResponse_t642400449_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchResponse_t642400449_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchResponse_t642400449_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t642400449_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchResponse_t642400449_0_0_0_Types };
extern const Il2CppType FetchScoreSummaryResponse_t877231893_0_0_0;
static const Il2CppType* GenInst_FetchScoreSummaryResponse_t877231893_0_0_0_Types[] = { &FetchScoreSummaryResponse_t877231893_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchScoreSummaryResponse_t877231893_0_0_0 = { 1, GenInst_FetchScoreSummaryResponse_t877231893_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchScoreSummaryResponse_t877231893_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchScoreSummaryResponse_t877231893_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchScoreSummaryResponse_t877231893_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchScoreSummaryResponse_t877231893_0_0_0_Types };
extern const Il2CppType FetchScorePageResponse_t1467305556_0_0_0;
static const Il2CppType* GenInst_FetchScorePageResponse_t1467305556_0_0_0_Types[] = { &FetchScorePageResponse_t1467305556_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchScorePageResponse_t1467305556_0_0_0 = { 1, GenInst_FetchScorePageResponse_t1467305556_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchScorePageResponse_t1467305556_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchScorePageResponse_t1467305556_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchScorePageResponse_t1467305556_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchScorePageResponse_t1467305556_0_0_0_Types };
extern const Il2CppType NativeScoreEntry_t3997924611_0_0_0;
static const Il2CppType* GenInst_NativeScoreEntry_t3997924611_0_0_0_Types[] = { &NativeScoreEntry_t3997924611_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeScoreEntry_t3997924611_0_0_0 = { 1, GenInst_NativeScoreEntry_t3997924611_0_0_0_Types };
extern const Il2CppType ParticipantStatus_t4028684685_0_0_0;
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t4028684685_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0 = { 2, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3076810564_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3076810564_0_0_0_Types[] = { &KeyValuePair_2_t3076810564_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3076810564_0_0_0 = { 1, GenInst_KeyValuePair_2_t3076810564_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t4028684685_0_0_0_Types[] = { &ParticipantStatus_t4028684685_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t4028684685_0_0_0 = { 1, GenInst_ParticipantStatus_t4028684685_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t3137786926_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t4028684685_0_0_0, &ParticipantStatus_t3137786926_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t3137786926_0_0_0 = { 3, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t3137786926_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t4028684685_0_0_0, &ParticipantStatus_t4028684685_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0 = { 3, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t4028684685_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_KeyValuePair_2_t3076810564_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t4028684685_0_0_0, &KeyValuePair_2_t3076810564_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_KeyValuePair_2_t3076810564_0_0_0 = { 3, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_KeyValuePair_2_t3076810564_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &MultiplayerParticipant_t3337232325_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativeScoreEntry_t3997924611_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativeScoreEntry_t3997924611_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeScoreEntry_t3997924611_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativeScoreEntry_t3997924611_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchSelfResponse_t1087840609_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchSelfResponse_t1087840609_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchSelfResponse_t1087840609_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchSelfResponse_t1087840609_0_0_0_Types };
extern const Il2CppType FetchResponse_t3310613493_0_0_0;
static const Il2CppType* GenInst_FetchResponse_t3310613493_0_0_0_Types[] = { &FetchResponse_t3310613493_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchResponse_t3310613493_0_0_0 = { 1, GenInst_FetchResponse_t3310613493_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchResponse_t3310613493_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchResponse_t3310613493_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t3310613493_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchResponse_t3310613493_0_0_0_Types };
extern const Il2CppType FetchListResponse_t676237491_0_0_0;
static const Il2CppType* GenInst_FetchListResponse_t676237491_0_0_0_Types[] = { &FetchListResponse_t676237491_0_0_0 };
extern const Il2CppGenericInst GenInst_FetchListResponse_t676237491_0_0_0 = { 1, GenInst_FetchListResponse_t676237491_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchListResponse_t676237491_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchListResponse_t676237491_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchListResponse_t676237491_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchListResponse_t676237491_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativePlayer_t2636885988_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativePlayer_t2636885988_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativePlayer_t2636885988_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativePlayer_t2636885988_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_String_t_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_String_t_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchResponse_t3559403642_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchResponse_t3559403642_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t3559403642_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchResponse_t3559403642_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchListResponse_t32965304_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchListResponse_t32965304_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchListResponse_t32965304_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchListResponse_t32965304_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_QuestUIResponse_t3819980022_0_0_0_Types[] = { &IntPtr_t_0_0_0, &QuestUIResponse_t3819980022_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_QuestUIResponse_t3819980022_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_QuestUIResponse_t3819980022_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_AcceptResponse_t3485599210_0_0_0_Types[] = { &IntPtr_t_0_0_0, &AcceptResponse_t3485599210_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_AcceptResponse_t3485599210_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_AcceptResponse_t3485599210_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_ClaimMilestoneResponse_t683149430_0_0_0_Types[] = { &IntPtr_t_0_0_0, &ClaimMilestoneResponse_t683149430_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_ClaimMilestoneResponse_t683149430_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_ClaimMilestoneResponse_t683149430_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativeQuest_t2496300529_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativeQuest_t2496300529_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeQuest_t2496300529_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativeQuest_t2496300529_0_0_0_Types };
static const Il2CppType* GenInst_MultiplayerParticipant_t3337232325_0_0_0_IntPtr_t_0_0_0_Types[] = { &MultiplayerParticipant_t3337232325_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiplayerParticipant_t3337232325_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_MultiplayerParticipant_t3337232325_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_PlayerSelectUIResponse_t922401854_0_0_0_Types[] = { &IntPtr_t_0_0_0, &PlayerSelectUIResponse_t922401854_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_PlayerSelectUIResponse_t922401854_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_PlayerSelectUIResponse_t922401854_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_RoomInboxUIResponse_t1117529840_0_0_0_Types[] = { &IntPtr_t_0_0_0, &RoomInboxUIResponse_t1117529840_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RoomInboxUIResponse_t1117529840_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RoomInboxUIResponse_t1117529840_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_WaitingRoomUIResponse_t3333463309_0_0_0_Types[] = { &IntPtr_t_0_0_0, &WaitingRoomUIResponse_t3333463309_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WaitingRoomUIResponse_t3333463309_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WaitingRoomUIResponse_t3333463309_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchInvitationsResponse_t815788017_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchInvitationsResponse_t815788017_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchInvitationsResponse_t815788017_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchInvitationsResponse_t815788017_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_RealTimeRoomResponse_t2530940439_0_0_0_Types[] = { &IntPtr_t_0_0_0, &RealTimeRoomResponse_t2530940439_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RealTimeRoomResponse_t2530940439_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RealTimeRoomResponse_t2530940439_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &MultiplayerInvitation_t3411188537_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1240414705_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchAllResponse_t1240414705_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1240414705_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1240414705_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_SnapshotSelectUIResponse_t2500674014_0_0_0_Types[] = { &IntPtr_t_0_0_0, &SnapshotSelectUIResponse_t2500674014_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_SnapshotSelectUIResponse_t2500674014_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_SnapshotSelectUIResponse_t2500674014_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_OpenResponse_t1933021492_0_0_0_Types[] = { &IntPtr_t_0_0_0, &OpenResponse_t1933021492_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_OpenResponse_t1933021492_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_OpenResponse_t1933021492_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_CommitResponse_t2801162465_0_0_0_Types[] = { &IntPtr_t_0_0_0, &CommitResponse_t2801162465_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_CommitResponse_t2801162465_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_CommitResponse_t2801162465_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_ReadResponse_t2292627584_0_0_0_Types[] = { &IntPtr_t_0_0_0, &ReadResponse_t2292627584_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_ReadResponse_t2292627584_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_ReadResponse_t2292627584_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativeSnapshotMetadata_t3479575958_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativeSnapshotMetadata_t3479575958_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeSnapshotMetadata_t3479575958_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativeSnapshotMetadata_t3479575958_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FetchForPlayerResponse_t1267220047_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FetchForPlayerResponse_t1267220047_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchForPlayerResponse_t1267220047_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FetchForPlayerResponse_t1267220047_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_TurnBasedMatchesResponse_t3460122515_0_0_0_Types[] = { &IntPtr_t_0_0_0, &TurnBasedMatchesResponse_t3460122515_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_TurnBasedMatchesResponse_t3460122515_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_TurnBasedMatchesResponse_t3460122515_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_MatchInboxUIResponse_t1459363083_0_0_0_Types[] = { &IntPtr_t_0_0_0, &MatchInboxUIResponse_t1459363083_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_MatchInboxUIResponse_t1459363083_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_MatchInboxUIResponse_t1459363083_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_TurnBasedMatchResponse_t4255652325_0_0_0_Types[] = { &IntPtr_t_0_0_0, &TurnBasedMatchResponse_t4255652325_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_TurnBasedMatchResponse_t4255652325_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_TurnBasedMatchResponse_t4255652325_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &NativeTurnBasedMatch_t302853426_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_Types[] = { &String_t_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2065771578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0 = { 1, GenInst_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t4291918972_0_0_0, &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &String_t_0_0_0, &Single_t4291918972_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t716150752_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t716150752_0_0_0_Types[] = { &KeyValuePair_2_t716150752_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t716150752_0_0_0 = { 1, GenInst_KeyValuePair_2_t716150752_0_0_0_Types };
extern const Il2CppType ParticleSystem_t381473177_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t381473177_0_0_0_Types[] = { &ParticleSystem_t381473177_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t381473177_0_0_0 = { 1, GenInst_ParticleSystem_t381473177_0_0_0_Types };
extern const Il2CppType List_1_t747900261_0_0_0;
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t747900261_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &List_1_t747900261_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t643944206_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t643944206_0_0_0_Types[] = { &KeyValuePair_2_t643944206_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t643944206_0_0_0 = { 1, GenInst_KeyValuePair_2_t643944206_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1049882445_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0 = { 1, GenInst_KeyValuePair_2_t1049882445_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &Int32_t1153838500_0_0_0, &Int32_t1153838500_0_0_0, &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0 = { 3, GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types };
extern const Il2CppType CFX_AutoDestructShuriken_t107909964_0_0_0;
static const Il2CppType* GenInst_CFX_AutoDestructShuriken_t107909964_0_0_0_Types[] = { &CFX_AutoDestructShuriken_t107909964_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_AutoDestructShuriken_t107909964_0_0_0 = { 1, GenInst_CFX_AutoDestructShuriken_t107909964_0_0_0_Types };
extern const Il2CppType CFX_LightIntensityFade_t2919443491_0_0_0;
static const Il2CppType* GenInst_CFX_LightIntensityFade_t2919443491_0_0_0_Types[] = { &CFX_LightIntensityFade_t2919443491_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_LightIntensityFade_t2919443491_0_0_0 = { 1, GenInst_CFX_LightIntensityFade_t2919443491_0_0_0_Types };
extern const Il2CppType Rigidbody_t3346577219_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t3346577219_0_0_0_Types[] = { &Rigidbody_t3346577219_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t3346577219_0_0_0 = { 1, GenInst_Rigidbody_t3346577219_0_0_0_Types };
extern const Il2CppType Animation_t1724966010_0_0_0;
static const Il2CppType* GenInst_Animation_t1724966010_0_0_0_Types[] = { &Animation_t1724966010_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t1724966010_0_0_0 = { 1, GenInst_Animation_t1724966010_0_0_0_Types };
extern const Il2CppType AnimationCurve_t3667593487_0_0_0;
static const Il2CppType* GenInst_AnimationCurve_t3667593487_0_0_0_Types[] = { &AnimationCurve_t3667593487_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationCurve_t3667593487_0_0_0 = { 1, GenInst_AnimationCurve_t3667593487_0_0_0_Types };
extern const Il2CppType Calculation_t2191327052_0_0_0;
static const Il2CppType* GenInst_Calculation_t2191327052_0_0_0_Types[] = { &Calculation_t2191327052_0_0_0 };
extern const Il2CppGenericInst GenInst_Calculation_t2191327052_0_0_0 = { 1, GenInst_Calculation_t2191327052_0_0_0_Types };
extern const Il2CppType Calculation_t2771812670_0_0_0;
static const Il2CppType* GenInst_Calculation_t2771812670_0_0_0_Types[] = { &Calculation_t2771812670_0_0_0 };
extern const Il2CppGenericInst GenInst_Calculation_t2771812670_0_0_0 = { 1, GenInst_Calculation_t2771812670_0_0_0_Types };
extern const Il2CppType Renderer_t3076687687_0_0_0;
static const Il2CppType* GenInst_Renderer_t3076687687_0_0_0_Types[] = { &Renderer_t3076687687_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t3076687687_0_0_0 = { 1, GenInst_Renderer_t3076687687_0_0_0_Types };
extern const Il2CppType NetworkView_t3656680617_0_0_0;
static const Il2CppType* GenInst_NetworkView_t3656680617_0_0_0_Types[] = { &NetworkView_t3656680617_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkView_t3656680617_0_0_0 = { 1, GenInst_NetworkView_t3656680617_0_0_0_Types };
extern const Il2CppType AudioClip_t794140988_0_0_0;
static const Il2CppType* GenInst_AudioClip_t794140988_0_0_0_Types[] = { &AudioClip_t794140988_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t794140988_0_0_0 = { 1, GenInst_AudioClip_t794140988_0_0_0_Types };
extern const Il2CppType AudioSource_t1740077639_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1740077639_0_0_0_Types[] = { &AudioSource_t1740077639_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1740077639_0_0_0 = { 1, GenInst_AudioSource_t1740077639_0_0_0_Types };
extern const Il2CppType GUIText_t3371372606_0_0_0;
static const Il2CppType* GenInst_GUIText_t3371372606_0_0_0_Types[] = { &GUIText_t3371372606_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t3371372606_0_0_0 = { 1, GenInst_GUIText_t3371372606_0_0_0_Types };
extern const Il2CppType GUITexture_t4020448292_0_0_0;
static const Il2CppType* GenInst_GUITexture_t4020448292_0_0_0_Types[] = { &GUITexture_t4020448292_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t4020448292_0_0_0 = { 1, GenInst_GUITexture_t4020448292_0_0_0_Types };
extern const Il2CppType Light_t4202674828_0_0_0;
static const Il2CppType* GenInst_Light_t4202674828_0_0_0_Types[] = { &Light_t4202674828_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t4202674828_0_0_0 = { 1, GenInst_Light_t4202674828_0_0_0_Types };
extern const Il2CppType GPGSMng_t946704145_0_0_0;
static const Il2CppType* GenInst_GPGSMng_t946704145_0_0_0_Types[] = { &GPGSMng_t946704145_0_0_0 };
extern const Il2CppGenericInst GenInst_GPGSMng_t946704145_0_0_0 = { 1, GenInst_GPGSMng_t946704145_0_0_0_Types };
extern const Il2CppType NetworkMng_t1515215352_0_0_0;
static const Il2CppType* GenInst_NetworkMng_t1515215352_0_0_0_Types[] = { &NetworkMng_t1515215352_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkMng_t1515215352_0_0_0 = { 1, GenInst_NetworkMng_t1515215352_0_0_0_Types };
extern const Il2CppType GameStepManager_t2511743951_0_0_0;
static const Il2CppType* GenInst_GameStepManager_t2511743951_0_0_0_Types[] = { &GameStepManager_t2511743951_0_0_0 };
extern const Il2CppGenericInst GenInst_GameStepManager_t2511743951_0_0_0 = { 1, GenInst_GameStepManager_t2511743951_0_0_0_Types };
extern const Il2CppType MainUserInfo_t2526075922_0_0_0;
static const Il2CppType* GenInst_MainUserInfo_t2526075922_0_0_0_Types[] = { &MainUserInfo_t2526075922_0_0_0 };
extern const Il2CppGenericInst GenInst_MainUserInfo_t2526075922_0_0_0 = { 1, GenInst_MainUserInfo_t2526075922_0_0_0_Types };
extern const Il2CppType ConstforMinigame_t2789090703_0_0_0;
static const Il2CppType* GenInst_ConstforMinigame_t2789090703_0_0_0_Types[] = { &ConstforMinigame_t2789090703_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstforMinigame_t2789090703_0_0_0 = { 1, GenInst_ConstforMinigame_t2789090703_0_0_0_Types };
extern const Il2CppType CalculationData_t2213441011_0_0_0;
static const Il2CppType* GenInst_CalculationData_t2213441011_0_0_0_Types[] = { &CalculationData_t2213441011_0_0_0 };
extern const Il2CppGenericInst GenInst_CalculationData_t2213441011_0_0_0 = { 1, GenInst_CalculationData_t2213441011_0_0_0_Types };
extern const Il2CppType ResultData_t1421128071_0_0_0;
static const Il2CppType* GenInst_ResultData_t1421128071_0_0_0_Types[] = { &ResultData_t1421128071_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultData_t1421128071_0_0_0 = { 1, GenInst_ResultData_t1421128071_0_0_0_Types };
extern const Il2CppType ResultAllData_t3272948206_0_0_0;
static const Il2CppType* GenInst_ResultAllData_t3272948206_0_0_0_Types[] = { &ResultAllData_t3272948206_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultAllData_t3272948206_0_0_0 = { 1, GenInst_ResultAllData_t3272948206_0_0_0_Types };
extern const Il2CppType ResultSaveData_t3126241828_0_0_0;
static const Il2CppType* GenInst_ResultSaveData_t3126241828_0_0_0_Types[] = { &ResultSaveData_t3126241828_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultSaveData_t3126241828_0_0_0 = { 1, GenInst_ResultSaveData_t3126241828_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3230847821_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3230847821_0_0_0_Types[] = { &Int32U5BU5D_t3230847821_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3230847821_0_0_0 = { 1, GenInst_Int32U5BU5D_t3230847821_0_0_0_Types };
extern const Il2CppType GradeData_t483491841_0_0_0;
static const Il2CppType* GenInst_GradeData_t483491841_0_0_0_Types[] = { &GradeData_t483491841_0_0_0 };
extern const Il2CppGenericInst GenInst_GradeData_t483491841_0_0_0 = { 1, GenInst_GradeData_t483491841_0_0_0_Types };
extern const Il2CppType UserExperience_t3961908149_0_0_0;
static const Il2CppType* GenInst_UserExperience_t3961908149_0_0_0_Types[] = { &UserExperience_t3961908149_0_0_0 };
extern const Il2CppGenericInst GenInst_UserExperience_t3961908149_0_0_0 = { 1, GenInst_UserExperience_t3961908149_0_0_0_Types };
extern const Il2CppType StructforMinigame_t1286533533_0_0_0;
static const Il2CppType* GenInst_StructforMinigame_t1286533533_0_0_0_Types[] = { &StructforMinigame_t1286533533_0_0_0 };
extern const Il2CppGenericInst GenInst_StructforMinigame_t1286533533_0_0_0 = { 1, GenInst_StructforMinigame_t1286533533_0_0_0_Types };
extern const Il2CppType Image_t538875265_0_0_0;
static const Il2CppType* GenInst_Image_t538875265_0_0_0_Types[] = { &Image_t538875265_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t538875265_0_0_0 = { 1, GenInst_Image_t538875265_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t1511988644_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t1511988644_0_0_0_Types[] = { &ICanvasRaycastFilter_t1511988644_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t1511988644_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t1511988644_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t1646037781_0_0_0_Types[] = { &ILayoutElement_t1646037781_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1646037781_0_0_0 = { 1, GenInst_ILayoutElement_t1646037781_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t3186046376_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t3186046376_0_0_0_Types[] = { &MaskableGraphic_t3186046376_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t3186046376_0_0_0 = { 1, GenInst_MaskableGraphic_t3186046376_0_0_0_Types };
extern const Il2CppType IMaskable_t1719715701_0_0_0;
static const Il2CppType* GenInst_IMaskable_t1719715701_0_0_0_Types[] = { &IMaskable_t1719715701_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t1719715701_0_0_0 = { 1, GenInst_IMaskable_t1719715701_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t2120790093_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t2120790093_0_0_0_Types[] = { &IMaterialModifier_t2120790093_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t2120790093_0_0_0 = { 1, GenInst_IMaterialModifier_t2120790093_0_0_0_Types };
extern const Il2CppType UIBehaviour_t2511441271_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t2511441271_0_0_0_Types[] = { &UIBehaviour_t2511441271_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t2511441271_0_0_0 = { 1, GenInst_UIBehaviour_t2511441271_0_0_0_Types };
extern const Il2CppType Button_t3896396478_0_0_0;
static const Il2CppType* GenInst_Button_t3896396478_0_0_0_Types[] = { &Button_t3896396478_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t3896396478_0_0_0 = { 1, GenInst_Button_t3896396478_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t309087608_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types[] = { &IEnumerable_1_t309087608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t309087608_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t309087608_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1181603442_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types[] = { &Array_Sort_m1181603442_gp_0_0_0_0, &Array_Sort_m1181603442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3908760906_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3908760906_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types[] = { &Array_Sort_m3908760906_gp_0_0_0_0, &Array_Sort_m3908760906_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m646233104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0, &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0, &Array_Sort_m2404937677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m377069906_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types[] = { &Array_Sort_m377069906_gp_0_0_0_0, &Array_Sort_m377069906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1327718954_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1327718954_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types[] = { &Array_Sort_m1327718954_gp_0_0_0_0, &Array_Sort_m1327718954_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m4084526832_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0, &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0, &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3566161319_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types[] = { &Array_Sort_m3566161319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3566161319_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1767877396_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types[] = { &Array_Sort_m1767877396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1767877396_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0, &Array_qsort_m785378185_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m3718693973_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types[] = { &Array_compare_m3718693973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m3718693973_gp_0_0_0_0 = { 1, GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m3270161954_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types[] = { &Array_qsort_m3270161954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3270161954_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m2347367271_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types[] = { &Array_Resize_m2347367271_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m2347367271_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m123384663_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m123384663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1326446122_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types[] = { &Array_ForEach_m1326446122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1326446122_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1697145810_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1697145810_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1697145810_gp_0_0_0_0, &Array_ConvertAll_m1697145810_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m614217902_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m614217902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m653822311_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m653822311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1650781518_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1650781518_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m308210168_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types[] = { &Array_FindIndex_m308210168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m308210168_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3338256477_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3338256477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1605056024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1605056024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3507857443_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3507857443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1578426497_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1578426497_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3933814211_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3933814211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1779658273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1779658273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3823835921_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3823835921_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1695958374_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1695958374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3280162097_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3280162097_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m921616327_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m921616327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m4157241456_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m4157241456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m925096551_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m925096551_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1181152586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types[] = { &Array_FindAll_m1181152586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1181152586_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m2312185345_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types[] = { &Array_Exists_m2312185345_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m2312185345_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m4233614634_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m4233614634_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m3878993575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types[] = { &Array_Find_m3878993575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m3878993575_gp_0_0_0_0 = { 1, GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3671273393_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types[] = { &Array_FindLast_m3671273393_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3671273393_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t813279015_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t813279015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2706020430_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2706020430_gp_0_0_0_0_Types[] = { &IList_1_t2706020430_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2706020430_gp_0_0_0_0 = { 1, GenInst_IList_1_t2706020430_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1952910030_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types[] = { &ICollection_1_t1952910030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1952910030_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1952910030_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1122404262_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types[] = { &Nullable_1_t1122404262_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1122404262_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1122404262_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1701613202_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types[] = { &Comparer_1_t1701613202_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1701613202_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1701613202_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3219634540_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types[] = { &DefaultComparer_t3219634540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3219634540_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3219634540_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2982791755_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2982791755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t898371836_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4022413346_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0 = { 1, GenInst_KeyValuePair_2_t4022413346_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3290534805_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3290534805_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3290534805_gp_0_0_0_0, &ShimEnumerator_t3290534805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1176480892_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1176480892_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types[] = { &Enumerator_t1176480892_gp_0_0_0_0, &Enumerator_t1176480892_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912719906_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912719906_0_0_0_Types[] = { &KeyValuePair_2_t1912719906_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912719906_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912719906_0_0_0_Types };
extern const Il2CppType KeyCollection_t2018332325_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t2018332325_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4049866126_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4049866126_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0, &Enumerator_t4049866126_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types[] = { &Enumerator_t4049866126_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4049866126_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4049866126_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_1_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types[] = { &KeyCollection_t2018332325_gp_0_0_0_0, &KeyCollection_t2018332325_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3097895223_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3097895223_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2802076604_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2802076604_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_0_0_0_0, &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types[] = { &Enumerator_t2802076604_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2802076604_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2802076604_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_0_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types[] = { &ValueCollection_t3097895223_gp_1_0_0_0, &ValueCollection_t3097895223_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &DictionaryEntry_t1751606614_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_0_0_0_0, &Dictionary_2_t898371836_gp_1_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 3, GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types[] = { &KeyValuePair_2_t4022413346_0_0_0, &KeyValuePair_2_t4022413346_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0 = { 2, GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types[] = { &Dictionary_2_t898371836_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t898371836_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t898371836_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2934485036_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2934485036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4247873286_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types[] = { &DefaultComparer_t4247873286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4247873286_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4247873286_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t961487589_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t961487589_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t435039943_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t435039943_gp_0_0_0_0_Types[] = { &IDictionary_2_t435039943_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t435039943_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t435039943_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2536192150_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2536192150_0_0_0_Types[] = { &KeyValuePair_2_t2536192150_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536192150_0_0_0 = { 1, GenInst_KeyValuePair_2_t2536192150_0_0_0_Types };
extern const Il2CppType IDictionary_2_t435039943_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types[] = { &IDictionary_2_t435039943_gp_0_0_0_0, &IDictionary_2_t435039943_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t911337330_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t911337330_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t911337330_gp_0_0_0_0, &KeyValuePair_2_t911337330_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t951551555_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t951551555_gp_0_0_0_0_Types[] = { &List_1_t951551555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t951551555_gp_0_0_0_0 = { 1, GenInst_List_1_t951551555_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2095969493_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types[] = { &Enumerator_t2095969493_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2095969493_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2095969493_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3473121937_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types[] = { &Collection_1_t3473121937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3473121937_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3473121937_gp_0_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t363916344_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t363916344_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t363916344_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t363916344_gp_1_0_0_0 = { 1, GenInst_KeyedCollection_2_t363916344_gp_1_0_0_0_Types };
extern const Il2CppType KeyedCollection_2_t363916344_gp_0_0_0_0;
static const Il2CppType* GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_Types[] = { &KeyedCollection_2_t363916344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0 = { 1, GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_KeyedCollection_2_t363916344_gp_1_0_0_0_Types[] = { &KeyedCollection_2_t363916344_gp_0_0_0_0, &KeyedCollection_2_t363916344_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_KeyedCollection_2_t363916344_gp_1_0_0_0 = { 2, GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_KeyedCollection_2_t363916344_gp_1_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t4004629011_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2757088543_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0_Types[] = { &ArraySegment_1_t2757088543_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2631682684_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types[] = { &LinkedList_1_t2631682684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2631682684_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t2631682684_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2473886460_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types[] = { &Enumerator_t2473886460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2473886460_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2473886460_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t603318558_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t603318558_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t2386261560_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t2386261560_gp_0_0_0_0_Types[] = { &Queue_1_t2386261560_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t2386261560_gp_0_0_0_0 = { 1, GenInst_Queue_1_t2386261560_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3723618752_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3723618752_gp_0_0_0_0_Types[] = { &Enumerator_t3723618752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3723618752_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3723618752_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4128415215_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types[] = { &Stack_1_t4128415215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4128415215_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4128415215_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2313787817_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types[] = { &Enumerator_t2313787817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2313787817_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2313787817_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t3763949723_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types[] = { &HashSet_1_t3763949723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3763949723_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3763949723_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1211810685_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types[] = { &Enumerator_t1211810685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1211810685_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1211810685_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3144359540_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types[] = { &PrimeHelper_t3144359540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3144359540_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3144359540_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m131196905_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m131196905_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m131196905_gp_0_0_0_0 = { 1, GenInst_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m131196905_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Types[] = { &Enumerable_Aggregate_m131196905_gp_1_0_0_0, &Enumerable_Aggregate_m131196905_gp_0_0_0_0, &Enumerable_Aggregate_m131196905_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Enumerable_Aggregate_m131196905_gp_1_0_0_0 = { 3, GenInst_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_All_m3316688406_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Types[] = { &Enumerable_All_m3316688406_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m3316688406_gp_0_0_0_0 = { 1, GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_All_m3316688406_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m1224968048_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types[] = { &Enumerable_Any_m1224968048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m3421076499_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m3421076499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2710245799_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2710245799_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m3995152903_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0_Types[] = { &Enumerable_Count_m3995152903_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Empty_m1047782786_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0_Types[] = { &Enumerable_Empty_m1047782786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0 = { 1, GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Except_m3558828234_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Except_m3558828234_gp_0_0_0_0_Types[] = { &Enumerable_Except_m3558828234_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Except_m3558828234_gp_0_0_0_0 = { 1, GenInst_Enumerable_Except_m3558828234_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Except_m3897958717_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Except_m3897958717_gp_0_0_0_0_Types[] = { &Enumerable_Except_m3897958717_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Except_m3897958717_gp_0_0_0_0 = { 1, GenInst_Enumerable_Except_m3897958717_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0_Types[] = { &Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1340241468_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types[] = { &Enumerable_First_m1340241468_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1340241468_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0;
extern const Il2CppType List_1_t3315066695_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_List_1_t3315066695_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0, &List_1_t3315066695_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_List_1_t3315066695_0_0_0 = { 2, GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_List_1_t3315066695_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1149957246_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1149957246_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1149957246_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1149957246_gp_0_0_0_0, &Enumerable_GroupBy_m1149957246_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Enumerable_GroupBy_m1149957246_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2748065910_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2748065910_0_0_0_Types[] = { &IGrouping_2_t2748065910_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2748065910_0_0_0 = { 1, GenInst_IGrouping_2_t2748065910_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1149957246_gp_1_0_0_0, &Enumerable_GroupBy_m1149957246_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Enumerable_GroupBy_m1149957246_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1306123839_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1306123839_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1306123839_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1306123839_gp_0_0_0_0, &Enumerable_GroupBy_m1306123839_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Enumerable_GroupBy_m1306123839_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1306123839_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2748065911_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2748065911_0_0_0_Types[] = { &IGrouping_2_t2748065911_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2748065911_0_0_0 = { 1, GenInst_IGrouping_2_t2748065911_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1306123839_gp_1_0_0_0, &Enumerable_GroupBy_m1306123839_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Enumerable_GroupBy_m1306123839_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0, &Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2933082222_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2933082222_0_0_0_Types[] = { &IGrouping_2_t2933082222_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2933082222_0_0_0 = { 1, GenInst_IGrouping_2_t2933082222_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0, &Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Last_m342737789_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Types[] = { &Enumerable_Last_m342737789_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m342737789_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Last_m342737789_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Types[] = { &Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0 = { 1, GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_LongCount_m2029959972_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_LongCount_m2029959972_gp_0_0_0_0_Types[] = { &Enumerable_LongCount_m2029959972_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_LongCount_m2029959972_gp_0_0_0_0 = { 1, GenInst_Enumerable_LongCount_m2029959972_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m593931412_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m593931412_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m593931412_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Enumerable_OrderBy_m593931412_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m593931412_gp_0_0_0_0, &Enumerable_OrderBy_m593931412_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Enumerable_OrderBy_m593931412_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Enumerable_OrderBy_m593931412_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m3650685123_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m3650685123_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m3650685123_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Enumerable_OrderBy_m3650685123_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m3650685123_gp_0_0_0_0, &Enumerable_OrderBy_m3650685123_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Enumerable_OrderBy_m3650685123_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Enumerable_OrderBy_m3650685123_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m3650685123_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m3650685123_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m3650685123_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m3650685123_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Reverse_m3244172289_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Reverse_m3244172289_gp_0_0_0_0_Types[] = { &Enumerable_Reverse_m3244172289_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Reverse_m3244172289_gp_0_0_0_0 = { 1, GenInst_Enumerable_Reverse_m3244172289_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0_Types[] = { &Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2198038980_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2198038980_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_0_0_0_0, &Enumerable_Select_m2198038980_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2198038980_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m3064021096_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2457202127_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0, &IEnumerable_1_t2457202127_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m3064021096_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m3064021096_gp_0_0_0_0, &Enumerable_SelectMany_m3064021096_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t402820473_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0, &IEnumerable_1_t402820473_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0, &Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2414118603_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2414118603_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Single_m2414118603_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2162276958_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2162276958_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_Skip_m832324711_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Skip_m832324711_gp_0_0_0_0_Types[] = { &Enumerable_Skip_m832324711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Skip_m832324711_gp_0_0_0_0 = { 1, GenInst_Enumerable_Skip_m832324711_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0_Types[] = { &Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Take_m3047183215_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Take_m3047183215_gp_0_0_0_0_Types[] = { &Enumerable_Take_m3047183215_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Take_m3047183215_gp_0_0_0_0 = { 1, GenInst_Enumerable_Take_m3047183215_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0_Types[] = { &Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m600216364_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m600216364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m4019664173_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m4019664173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m4019664173_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m4019664173_gp_0_0_0_0, &Enumerable_ToDictionary_m4019664173_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m4019664173_gp_2_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m4019664173_gp_0_0_0_0, &Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m4019664173_gp_1_0_0_0, &Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m4019664173_gp_0_0_0_0, &Enumerable_ToDictionary_m4019664173_gp_1_0_0_0, &Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0 = { 3, GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m717303024_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m717303024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m717303024_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m717303024_gp_0_0_0_0, &Enumerable_ToDictionary_m717303024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m717303024_gp_2_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m717303024_gp_0_0_0_0, &Enumerable_ToDictionary_m717303024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m717303024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0_Types[] = { &Enumerable_ToDictionary_m717303024_gp_1_0_0_0, &Enumerable_ToDictionary_m717303024_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2993041198_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2993041198_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2993041198_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m2993041198_gp_0_0_0_0, &Enumerable_ToDictionary_m2993041198_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2993041198_gp_1_0_0_0, &Enumerable_ToDictionary_m2993041198_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2155213199_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToDictionary_m2155213199_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m2155213199_gp_0_0_0_0, &Enumerable_ToDictionary_m2155213199_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Types[] = { &Enumerable_ToDictionary_m2155213199_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0 = { 1, GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2155213199_gp_1_0_0_0, &Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 = { 2, GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types[] = { &Enumerable_ToDictionary_m2155213199_gp_0_0_0_0, &Enumerable_ToDictionary_m2155213199_gp_1_0_0_0, &Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0 = { 3, GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m3865828353_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m3865828353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0_Types[] = { &Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0 = { 1, GenInst_Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0_Types[] = { &Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0 = { 1, GenInst_Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Union_m3162669992_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Union_m3162669992_gp_0_0_0_0_Types[] = { &Enumerable_Union_m3162669992_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Union_m3162669992_gp_0_0_0_0 = { 1, GenInst_Enumerable_Union_m3162669992_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Union_m3942844955_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Union_m3942844955_gp_0_0_0_0_Types[] = { &Enumerable_Union_m3942844955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Union_m3942844955_gp_0_0_0_0 = { 1, GenInst_Enumerable_Union_m3942844955_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0_Types[] = { &Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m4110359_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Types[] = { &Enumerable_Where_m4110359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m4110359_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_Where_m4110359_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Function_1_t3519260161_gp_0_0_0_0;
static const Il2CppType* GenInst_Function_1_t3519260161_gp_0_0_0_0_Function_1_t3519260161_gp_0_0_0_0_Types[] = { &Function_1_t3519260161_gp_0_0_0_0, &Function_1_t3519260161_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t3519260161_gp_0_0_0_0_Function_1_t3519260161_gp_0_0_0_0 = { 2, GenInst_Function_1_t3519260161_gp_0_0_0_0_Function_1_t3519260161_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Function_1_t3519260161_gp_0_0_0_0_Types[] = { &Function_1_t3519260161_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Function_1_t3519260161_gp_0_0_0_0 = { 1, GenInst_Function_1_t3519260161_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0_Types[] = { &U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0 = { 1, GenInst_U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0;
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2205907554_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2205907554_0_0_0_Types[] = { &IGrouping_2_t2205907554_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2205907554_0_0_0 = { 1, GenInst_IGrouping_2_t2205907554_0_0_0_Types };
extern const Il2CppType List_1_t2704670177_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_List_1_t2704670177_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0, &List_1_t2704670177_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_List_1_t2704670177_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_List_1_t2704670177_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0_Types[] = { &U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0 = { 1, GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4053127014_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0, &IEnumerable_1_t4053127014_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0, &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0_Types[] = { &U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0 = { 1, GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0_Types[] = { &U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0 = { 1, GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0_Types[] = { &U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0 = { 1, GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType Grouping_2_t3051269225_gp_1_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t3051269225_gp_1_0_0_0_Types[] = { &Grouping_2_t3051269225_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t3051269225_gp_1_0_0_0 = { 1, GenInst_Grouping_2_t3051269225_gp_1_0_0_0_Types };
extern const Il2CppType Grouping_2_t3051269225_gp_0_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t3051269225_gp_0_0_0_0_Grouping_2_t3051269225_gp_1_0_0_0_Types[] = { &Grouping_2_t3051269225_gp_0_0_0_0, &Grouping_2_t3051269225_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t3051269225_gp_0_0_0_0_Grouping_2_t3051269225_gp_1_0_0_0 = { 2, GenInst_Grouping_2_t3051269225_gp_0_0_0_0_Grouping_2_t3051269225_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t403732852_gp_1_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t403732852_gp_1_0_0_0_Types[] = { &IGrouping_2_t403732852_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t403732852_gp_1_0_0_0 = { 1, GenInst_IGrouping_2_t403732852_gp_1_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t2451113233_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t2451113233_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t2451113233_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t2451113233_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t2451113233_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t3194770876_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t3194770876_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t3194770876_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t3194770876_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t3194770876_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1971240694_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1971240694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1971240694_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_OrderedSequence_2_t1971240694_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1971240694_gp_0_0_0_0, &OrderedSequence_2_t1971240694_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_OrderedSequence_2_t1971240694_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_OrderedSequence_2_t1971240694_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1971240694_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1971240694_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1971240694_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1971240694_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1296963154_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1296963154_gp_0_0_0_0_Types[] = { &QuickSort_1_t1296963154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1296963154_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1296963154_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t869602712_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t869602712_gp_0_0_0_0_Types[] = { &SortContext_1_t869602712_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t869602712_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t869602712_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t2890572760_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t2890572760_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t2890572760_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_SortSequenceContext_2_t2890572760_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t2890572760_gp_0_0_0_0, &SortSequenceContext_2_t2890572760_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_SortSequenceContext_2_t2890572760_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_SortSequenceContext_2_t2890572760_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t2890572760_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t2890572760_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t2890572760_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t2890572760_gp_1_0_0_0_Types };
extern const Il2CppType JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0_Types[] = { &JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0 = { 1, GenInst_JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t2548264713_gp_0_0_0_0;
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_Types[] = { &BidirectionalDictionary_2_t2548264713_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0 = { 1, GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_Types };
extern const Il2CppType BidirectionalDictionary_2_t2548264713_gp_1_0_0_0;
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_Types[] = { &BidirectionalDictionary_2_t2548264713_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0 = { 1, GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_Types[] = { &BidirectionalDictionary_2_t2548264713_gp_0_0_0_0, &BidirectionalDictionary_2_t2548264713_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0 = { 2, GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_Types[] = { &BidirectionalDictionary_2_t2548264713_gp_1_0_0_0, &BidirectionalDictionary_2_t2548264713_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0 = { 2, GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0_Types[] = { &CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddRange_m20899124_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddRange_m20899124_gp_0_0_0_0_Types[] = { &CollectionUtils_AddRange_m20899124_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddRange_m20899124_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddRange_m20899124_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_AddRange_m1321937586_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_AddRange_m1321937586_gp_0_0_0_0_Types[] = { &CollectionUtils_AddRange_m1321937586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_AddRange_m1321937586_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_AddRange_m1321937586_gp_0_0_0_0_Types };
extern const Il2CppType CollectionUtils_IndexOf_m168261572_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Types[] = { &CollectionUtils_IndexOf_m168261572_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &CollectionUtils_IndexOf_m168261572_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0_Types[] = { &CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0 = { 1, GenInst_CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0_Types };
extern const Il2CppType CollectionWrapper_1_t338490148_gp_0_0_0_0;
static const Il2CppType* GenInst_CollectionWrapper_1_t338490148_gp_0_0_0_0_Types[] = { &CollectionWrapper_1_t338490148_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CollectionWrapper_1_t338490148_gp_0_0_0_0 = { 1, GenInst_CollectionWrapper_1_t338490148_gp_0_0_0_0_Types };
extern const Il2CppType DictionaryWrapper_2_t3825985581_gp_0_0_0_0;
static const Il2CppType* GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_Types[] = { &DictionaryWrapper_2_t3825985581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0 = { 1, GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_Types };
extern const Il2CppType DictionaryWrapper_2_t3825985581_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_Types[] = { &DictionaryWrapper_2_t3825985581_gp_0_0_0_0, &DictionaryWrapper_2_t3825985581_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0 = { 2, GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1302665278_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1302665278_0_0_0_Types[] = { &KeyValuePair_2_t1302665278_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1302665278_0_0_0 = { 1, GenInst_KeyValuePair_2_t1302665278_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2795671894_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2795671894_0_0_0_Types[] = { &KeyValuePair_2_t2795671894_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2795671894_0_0_0 = { 1, GenInst_KeyValuePair_2_t2795671894_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_2_t3334679397_gp_2_0_0_0;
extern const Il2CppType DictionaryEnumerator_2_t3334679397_gp_3_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0_Types[] = { &DictionaryEnumerator_2_t3334679397_gp_2_0_0_0, &DictionaryEnumerator_2_t3334679397_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0 = { 2, GenInst_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_2_t3334679397_gp_0_0_0_0;
extern const Il2CppType DictionaryEnumerator_2_t3334679397_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_2_t3334679397_gp_0_0_0_0_DictionaryEnumerator_2_t3334679397_gp_1_0_0_0_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0_Types[] = { &DictionaryEnumerator_2_t3334679397_gp_0_0_0_0, &DictionaryEnumerator_2_t3334679397_gp_1_0_0_0, &DictionaryEnumerator_2_t3334679397_gp_2_0_0_0, &DictionaryEnumerator_2_t3334679397_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_2_t3334679397_gp_0_0_0_0_DictionaryEnumerator_2_t3334679397_gp_1_0_0_0_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0 = { 4, GenInst_DictionaryEnumerator_2_t3334679397_gp_0_0_0_0_DictionaryEnumerator_2_t3334679397_gp_1_0_0_0_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0_Types };
extern const Il2CppType U3CU3Ec_t3532683464_gp_0_0_0_0;
extern const Il2CppType U3CU3Ec_t3532683464_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec_t3532683464_gp_0_0_0_0_U3CU3Ec_t3532683464_gp_1_0_0_0_Types[] = { &U3CU3Ec_t3532683464_gp_0_0_0_0, &U3CU3Ec_t3532683464_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec_t3532683464_gp_0_0_0_0_U3CU3Ec_t3532683464_gp_1_0_0_0 = { 2, GenInst_U3CU3Ec_t3532683464_gp_0_0_0_0_U3CU3Ec_t3532683464_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1450275698_0_0_0;
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1450275698_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &KeyValuePair_2_t1450275698_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1450275698_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1450275698_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1302665278_0_0_0_Types[] = { &DictionaryEntry_t1751606614_0_0_0, &KeyValuePair_2_t1302665278_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1302665278_0_0_0 = { 2, GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1302665278_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_Types[] = { &DictionaryWrapper_2_t3825985581_gp_0_0_0_0, &DictionaryWrapper_2_t3825985581_gp_1_0_0_0, &DictionaryWrapper_2_t3825985581_gp_0_0_0_0, &DictionaryWrapper_2_t3825985581_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0 = { 4, GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Types };
extern const Il2CppType LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0;
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Types[] = { &LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0 = { 1, GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0 = { 1, GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0_Types[] = { &ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0_Types[] = { &ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0_Types };
extern const Il2CppType ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0;
static const Il2CppType* GenInst_ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0_Types[] = { &ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0 = { 1, GenInst_ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0_Types };
extern const Il2CppType StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0;
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0 = { 1, GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_String_t_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_String_t_0_0_0 = { 2, GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Boolean_t476798718_0_0_0_Types };
extern const Il2CppType U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0_String_t_0_0_0_Types[] = { &U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0_String_t_0_0_0 = { 2, GenInst_U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ThreadSafeStore_2_t2768378_gp_0_0_0_0;
extern const Il2CppType ThreadSafeStore_2_t2768378_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeStore_2_t2768378_gp_0_0_0_0_ThreadSafeStore_2_t2768378_gp_1_0_0_0_Types[] = { &ThreadSafeStore_2_t2768378_gp_0_0_0_0, &ThreadSafeStore_2_t2768378_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeStore_2_t2768378_gp_0_0_0_0_ThreadSafeStore_2_t2768378_gp_1_0_0_0 = { 2, GenInst_ThreadSafeStore_2_t2768378_gp_0_0_0_0_ThreadSafeStore_2_t2768378_gp_1_0_0_0_Types };
extern const Il2CppType CachedAttributeGetter_1_t3812731516_gp_0_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0_Types[] = { &Il2CppObject_0_0_0, &CachedAttributeGetter_1_t3812731516_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0_Types[] = { &CachedAttributeGetter_1_t3812731516_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0 = { 1, GenInst_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1444726174_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1444726174_0_0_0_Types[] = { &KeyValuePair_2_t1444726174_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1444726174_0_0_0 = { 1, GenInst_KeyValuePair_2_t1444726174_0_0_0_Types };
extern const Il2CppType DictionaryEnumerator_2_t2588053397_gp_0_0_0_0;
extern const Il2CppType DictionaryEnumerator_2_t2588053397_gp_1_0_0_0;
static const Il2CppType* GenInst_DictionaryEnumerator_2_t2588053397_gp_0_0_0_0_DictionaryEnumerator_2_t2588053397_gp_1_0_0_0_Types[] = { &DictionaryEnumerator_2_t2588053397_gp_0_0_0_0, &DictionaryEnumerator_2_t2588053397_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEnumerator_2_t2588053397_gp_0_0_0_0_DictionaryEnumerator_2_t2588053397_gp_1_0_0_0 = { 2, GenInst_DictionaryEnumerator_2_t2588053397_gp_0_0_0_0_DictionaryEnumerator_2_t2588053397_gp_1_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0_Types };
extern const Il2CppType JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0_Types[] = { &JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0 = { 1, GenInst_JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0_Types };
extern const Il2CppType IJEnumerable_1_t1879559754_gp_0_0_0_0;
static const Il2CppType* GenInst_IJEnumerable_1_t1879559754_gp_0_0_0_0_Types[] = { &IJEnumerable_1_t1879559754_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IJEnumerable_1_t1879559754_gp_0_0_0_0 = { 1, GenInst_IJEnumerable_1_t1879559754_gp_0_0_0_0_Types };
extern const Il2CppType JEnumerable_1_t2167239905_gp_0_0_0_0;
static const Il2CppType* GenInst_JEnumerable_1_t2167239905_gp_0_0_0_0_Types[] = { &JEnumerable_1_t2167239905_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JEnumerable_1_t2167239905_gp_0_0_0_0 = { 1, GenInst_JEnumerable_1_t2167239905_gp_0_0_0_0_Types };
extern const Il2CppType JEnumerable_1_t3910855904_0_0_0;
static const Il2CppType* GenInst_JEnumerable_1_t3910855904_0_0_0_Types[] = { &JEnumerable_1_t3910855904_0_0_0 };
extern const Il2CppGenericInst GenInst_JEnumerable_1_t3910855904_0_0_0 = { 1, GenInst_JEnumerable_1_t3910855904_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m2907158863_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m2907158863_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m2907158863_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m2907158863_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m2907158863_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m2393171610_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m2393171610_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m2393171610_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m2393171610_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m2393171610_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m2565332604_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m2565332604_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m2565332604_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m2565332604_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m2565332604_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0_Types };
extern const Il2CppType Resources_LoadAll_m3849581656_gp_0_0_0_0;
static const Il2CppType* GenInst_Resources_LoadAll_m3849581656_gp_0_0_0_0_Types[] = { &Resources_LoadAll_m3849581656_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Resources_LoadAll_m3849581656_gp_0_0_0_0 = { 1, GenInst_Resources_LoadAll_m3849581656_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m2068134811_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m2548367508_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m885533373_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m290105122_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2190484233_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2983504498_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m228373778_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m228373778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2204190206_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2204190206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m482375811_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types[] = { &Component_GetComponents_m482375811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m482375811_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m311092670_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m311092670_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3431730600_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3431730600_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3431730601_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3431730601_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_0_0_0_0, &InvokableCall_2_t3431730601_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3431730601_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3431730602_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3431730602_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3431730602_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_0_0_0_0, &InvokableCall_3_t3431730602_gp_1_0_0_0, &InvokableCall_3_t3431730602_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3431730602_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3431730603_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3431730603_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_0_0_0_0, &InvokableCall_4_t3431730603_gp_1_0_0_0, &InvokableCall_4_t3431730603_gp_2_0_0_0, &InvokableCall_4_t3431730603_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3431730603_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t1222316710_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t1222316710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t1176538020_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0_Types[] = { &UnityEvent_1_t1176538020_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t1176538021_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1176538021_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0_Types[] = { &UnityEvent_2_t1176538021_gp_0_0_0_0, &UnityEvent_2_t1176538021_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t1176538022_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1176538022_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1176538022_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0_Types[] = { &UnityEvent_3_t1176538022_gp_0_0_0_0, &UnityEvent_3_t1176538022_gp_1_0_0_0, &UnityEvent_3_t1176538022_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t1176538023_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1176538023_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0_Types[] = { &UnityEvent_4_t1176538023_gp_0_0_0_0, &UnityEvent_4_t1176538023_gp_1_0_0_0, &UnityEvent_4_t1176538023_gp_2_0_0_0, &UnityEvent_4_t1176538023_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0_Types };
extern const Il2CppType Arrays_1_t2739302885_gp_0_0_0_0;
static const Il2CppType* GenInst_Arrays_1_t2739302885_gp_0_0_0_0_Types[] = { &Arrays_1_t2739302885_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Arrays_1_t2739302885_gp_0_0_0_0 = { 1, GenInst_Arrays_1_t2739302885_gp_0_0_0_0_Types };
extern const Il2CppType Lists_1_t4025286752_gp_0_0_0_0;
static const Il2CppType* GenInst_Lists_1_t4025286752_gp_0_0_0_0_Types[] = { &Lists_1_t4025286752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Lists_1_t4025286752_gp_0_0_0_0 = { 1, GenInst_Lists_1_t4025286752_gp_0_0_0_0_Types };
extern const Il2CppType PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0;
static const Il2CppType* GenInst_PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0_Types[] = { &PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0 = { 1, GenInst_PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m825525191_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m825525191_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2054808038_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2054808038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t1486860772_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Types[] = { &IndexedSet_1_t1486860772_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0_Types[] = { &IndexedSet_1_t1486860772_gp_0_0_0_0, &Int32_t1153838500_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0 = { 2, GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0_Types };
extern const Il2CppType ListPool_1_t3815171383_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t3815171383_gp_0_0_0_0_Types[] = { &ListPool_1_t3815171383_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t3815171383_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t3815171383_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1536022088_0_0_0;
static const Il2CppType* GenInst_List_1_t1536022088_0_0_0_Types[] = { &List_1_t1536022088_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1536022088_0_0_0 = { 1, GenInst_List_1_t1536022088_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3981551192_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3981551192_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0_Types };
extern const Il2CppType CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0_Types[] = { &CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0 = { 1, GenInst_CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0_Types };
extern const Il2CppType CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0;
extern const Il2CppType CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0;
static const Il2CppType* GenInst_CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0_Types[] = { &CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0, &CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0 = { 2, GenInst_CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0_Types };
extern const Il2CppType CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0;
extern const Il2CppType CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0;
extern const Il2CppType CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0;
static const Il2CppType* GenInst_CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0_Types[] = { &CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0, &CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0, &CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0 = { 3, GenInst_CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0 = { 1, GenInst_U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0 = { 1, GenInst_U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0 = { 2, GenInst_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0 = { 2, GenInst_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0 = { 3, GenInst_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0 = { 3, GenInst_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0_Types };
extern const Il2CppType NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0;
static const Il2CppType* GenInst_NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0_Types[] = { &NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0 = { 1, GenInst_NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0_Types };
extern const Il2CppType NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0;
static const Il2CppType* GenInst_NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0_Types[] = { &NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0 = { 1, GenInst_NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0_Types };
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0_Types[] = { &U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0 = { 1, GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0_Types };
extern const Il2CppType U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0_Types[] = { &U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0 = { 1, GenInst_U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0_Types };
extern const Il2CppType NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0;
extern const Il2CppType NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0;
static const Il2CppType* GenInst_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0_Types[] = { &NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0, &NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0 = { 2, GenInst_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0 = { 2, GenInst_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0_Types };
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0;
extern const Il2CppType U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0_Types[] = { &U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0, &U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0 = { 2, GenInst_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0_Types };
extern const Il2CppType Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0;
static const Il2CppType* GenInst_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0_Types[] = { &Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0 = { 1, GenInst_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0_Types };
extern const Il2CppType Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0;
extern const Il2CppType Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0;
static const Il2CppType* GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0_Types[] = { &Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0, &Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0 = { 2, GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_IntPtr_t_0_0_0_Types[] = { &Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_IntPtr_t_0_0_0_Types };
extern const Il2CppType Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0;
static const Il2CppType* GenInst_Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0_Types[] = { &Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0 = { 1, GenInst_Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0_Types };
extern const Il2CppType Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0;
static const Il2CppType* GenInst_Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0_Types[] = { &Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0 = { 1, GenInst_Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0_Types };
extern const Il2CppType Action_2_t3477941569_0_0_0;
static const Il2CppType* GenInst_Action_2_t3477941569_0_0_0_Types[] = { &Action_2_t3477941569_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t3477941569_0_0_0 = { 1, GenInst_Action_2_t3477941569_0_0_0_Types };
extern const Il2CppType Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0;
static const Il2CppType* GenInst_Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0_IntPtr_t_0_0_0_Types[] = { &Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0_IntPtr_t_0_0_0_Types };
extern const Il2CppType Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0;
static const Il2CppType* GenInst_Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0_Types[] = { &Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0 = { 1, GenInst_Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0_Types };
extern const Il2CppType Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0;
extern const Il2CppType Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0;
static const Il2CppType* GenInst_Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0_Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0_Types[] = { &Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0, &Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0_Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0 = { 2, GenInst_Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0_Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0_Types };
extern const Il2CppType U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0_Types[] = { &IntPtr_t_0_0_0, &U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0_Types[] = { &U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0 = { 1, GenInst_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0_Types };
extern const Il2CppType U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0_Types[] = { &IntPtr_t_0_0_0, &U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0_Types };
extern const Il2CppType U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0_Types[] = { &U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0, &U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0 = { 2, GenInst_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0_Types };
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0_Types[] = { &U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0 = { 1, GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0_Types };
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0_Types[] = { &U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0 = { 1, GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0_Types };
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0;
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0_Types[] = { &U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0, &U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0 = { 2, GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0_Types };
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0;
extern const Il2CppType U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0_Types[] = { &U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0, &U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0 = { 2, GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0_Types };
extern const Il2CppType PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0;
static const Il2CppType* GenInst_PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0_Types[] = { &PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0 = { 1, GenInst_PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0_Types };
extern const Il2CppType PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0;
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0_Types[] = { &PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0 = { 1, GenInst_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0_Types };
extern const Il2CppType PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0;
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0_Types[] = { &PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0 = { 1, GenInst_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0_Types };
extern const Il2CppType U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0_Types[] = { &U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0 = { 1, GenInst_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_UIntPtr_t_0_0_0_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0_Types[] = { &UIntPtr_t_0_0_0, &U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0 = { 2, GenInst_UIntPtr_t_0_0_0_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0_Types };
extern const Il2CppType ComponentAction_1_t2706192626_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0_Types[] = { &ComponentAction_1_t2706192626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0 = { 1, GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0_Types };
extern const Il2CppType Singleton_1_t4250359036_gp_0_0_0_0;
static const Il2CppType* GenInst_Singleton_1_t4250359036_gp_0_0_0_0_Types[] = { &Singleton_1_t4250359036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Singleton_1_t4250359036_gp_0_0_0_0 = { 1, GenInst_Singleton_1_t4250359036_gp_0_0_0_0_Types };
extern const Il2CppType LineInfoAnnotation_t3303106648_0_0_0;
static const Il2CppType* GenInst_LineInfoAnnotation_t3303106648_0_0_0_Types[] = { &LineInfoAnnotation_t3303106648_0_0_0 };
extern const Il2CppGenericInst GenInst_LineInfoAnnotation_t3303106648_0_0_0 = { 1, GenInst_LineInfoAnnotation_t3303106648_0_0_0_Types };
extern const Il2CppType JsonPropertyAttribute_t3435603053_0_0_0;
static const Il2CppType* GenInst_JsonPropertyAttribute_t3435603053_0_0_0_Types[] = { &JsonPropertyAttribute_t3435603053_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonPropertyAttribute_t3435603053_0_0_0 = { 1, GenInst_JsonPropertyAttribute_t3435603053_0_0_0_Types };
extern const Il2CppType JsonRequiredAttribute_t224246243_0_0_0;
static const Il2CppType* GenInst_JsonRequiredAttribute_t224246243_0_0_0_Types[] = { &JsonRequiredAttribute_t224246243_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonRequiredAttribute_t224246243_0_0_0 = { 1, GenInst_JsonRequiredAttribute_t224246243_0_0_0_Types };
extern const Il2CppType JsonObjectAttribute_t863918851_0_0_0;
static const Il2CppType* GenInst_JsonObjectAttribute_t863918851_0_0_0_Types[] = { &JsonObjectAttribute_t863918851_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonObjectAttribute_t863918851_0_0_0 = { 1, GenInst_JsonObjectAttribute_t863918851_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MemberInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MemberInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MemberInfo_t_0_0_0_Types };
extern const Il2CppType JsonExtensionDataAttribute_t3618022163_0_0_0;
static const Il2CppType* GenInst_JsonExtensionDataAttribute_t3618022163_0_0_0_Types[] = { &JsonExtensionDataAttribute_t3618022163_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonExtensionDataAttribute_t3618022163_0_0_0 = { 1, GenInst_JsonExtensionDataAttribute_t3618022163_0_0_0_Types };
extern const Il2CppType JsonContainerAttribute_t1917602971_0_0_0;
static const Il2CppType* GenInst_JsonContainerAttribute_t1917602971_0_0_0_Types[] = { &JsonContainerAttribute_t1917602971_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonContainerAttribute_t1917602971_0_0_0 = { 1, GenInst_JsonContainerAttribute_t1917602971_0_0_0_Types };
extern const Il2CppType JsonIgnoreAttribute_t4056167888_0_0_0;
static const Il2CppType* GenInst_JsonIgnoreAttribute_t4056167888_0_0_0_Types[] = { &JsonIgnoreAttribute_t4056167888_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonIgnoreAttribute_t4056167888_0_0_0 = { 1, GenInst_JsonIgnoreAttribute_t4056167888_0_0_0_Types };
extern const Il2CppType NonSerializedAttribute_t1507371935_0_0_0;
static const Il2CppType* GenInst_NonSerializedAttribute_t1507371935_0_0_0_Types[] = { &NonSerializedAttribute_t1507371935_0_0_0 };
extern const Il2CppGenericInst GenInst_NonSerializedAttribute_t1507371935_0_0_0 = { 1, GenInst_NonSerializedAttribute_t1507371935_0_0_0_Types };
extern const Il2CppType DefaultValueAttribute_t3756983154_0_0_0;
static const Il2CppType* GenInst_DefaultValueAttribute_t3756983154_0_0_0_Types[] = { &DefaultValueAttribute_t3756983154_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultValueAttribute_t3756983154_0_0_0 = { 1, GenInst_DefaultValueAttribute_t3756983154_0_0_0_Types };
static const Il2CppType* GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &JsonProperty_t902655177_0_0_0, &JsonProperty_t902655177_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 3, GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
extern const Il2CppType SerializableAttribute_t2632961395_0_0_0;
static const Il2CppType* GenInst_SerializableAttribute_t2632961395_0_0_0_Types[] = { &SerializableAttribute_t2632961395_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializableAttribute_t2632961395_0_0_0 = { 1, GenInst_SerializableAttribute_t2632961395_0_0_0_Types };
extern const Il2CppType JsonConverterAttribute_t1044341340_0_0_0;
static const Il2CppType* GenInst_JsonConverterAttribute_t1044341340_0_0_0_Types[] = { &JsonConverterAttribute_t1044341340_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConverterAttribute_t1044341340_0_0_0 = { 1, GenInst_JsonConverterAttribute_t1044341340_0_0_0_Types };
extern const Il2CppType GUILayer_t2983897946_0_0_0;
static const Il2CppType* GenInst_GUILayer_t2983897946_0_0_0_Types[] = { &GUILayer_t2983897946_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t2983897946_0_0_0 = { 1, GenInst_GUILayer_t2983897946_0_0_0_Types };
extern const Il2CppType PlayMakerMouseEvents_t316289710_0_0_0;
static const Il2CppType* GenInst_PlayMakerMouseEvents_t316289710_0_0_0_Types[] = { &PlayMakerMouseEvents_t316289710_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerMouseEvents_t316289710_0_0_0 = { 1, GenInst_PlayMakerMouseEvents_t316289710_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionEnter_t4046453814_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0_Types[] = { &PlayMakerCollisionEnter_t4046453814_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0 = { 1, GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionExit_t3871318016_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionExit_t3871318016_0_0_0_Types[] = { &PlayMakerCollisionExit_t3871318016_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionExit_t3871318016_0_0_0 = { 1, GenInst_PlayMakerCollisionExit_t3871318016_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionStay_t3871731003_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionStay_t3871731003_0_0_0_Types[] = { &PlayMakerCollisionStay_t3871731003_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionStay_t3871731003_0_0_0 = { 1, GenInst_PlayMakerCollisionStay_t3871731003_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerEnter_t977448304_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerEnter_t977448304_0_0_0_Types[] = { &PlayMakerTriggerEnter_t977448304_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerEnter_t977448304_0_0_0 = { 1, GenInst_PlayMakerTriggerEnter_t977448304_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerExit_t3495223174_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerExit_t3495223174_0_0_0_Types[] = { &PlayMakerTriggerExit_t3495223174_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerExit_t3495223174_0_0_0 = { 1, GenInst_PlayMakerTriggerExit_t3495223174_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerStay_t3495636161_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerStay_t3495636161_0_0_0_Types[] = { &PlayMakerTriggerStay_t3495636161_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerStay_t3495636161_0_0_0 = { 1, GenInst_PlayMakerTriggerStay_t3495636161_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionEnter2D_t1696713992_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionEnter2D_t1696713992_0_0_0_Types[] = { &PlayMakerCollisionEnter2D_t1696713992_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionEnter2D_t1696713992_0_0_0 = { 1, GenInst_PlayMakerCollisionEnter2D_t1696713992_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionExit2D_t894936658_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionExit2D_t894936658_0_0_0_Types[] = { &PlayMakerCollisionExit2D_t894936658_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionExit2D_t894936658_0_0_0 = { 1, GenInst_PlayMakerCollisionExit2D_t894936658_0_0_0_Types };
extern const Il2CppType PlayMakerCollisionStay2D_t1291817165_0_0_0;
static const Il2CppType* GenInst_PlayMakerCollisionStay2D_t1291817165_0_0_0_Types[] = { &PlayMakerCollisionStay2D_t1291817165_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerCollisionStay2D_t1291817165_0_0_0 = { 1, GenInst_PlayMakerCollisionStay2D_t1291817165_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerEnter2D_t3024951234_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerEnter2D_t3024951234_0_0_0_Types[] = { &PlayMakerTriggerEnter2D_t3024951234_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerEnter2D_t3024951234_0_0_0 = { 1, GenInst_PlayMakerTriggerEnter2D_t3024951234_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerExit2D_t245046360_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerExit2D_t245046360_0_0_0_Types[] = { &PlayMakerTriggerExit2D_t245046360_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerExit2D_t245046360_0_0_0 = { 1, GenInst_PlayMakerTriggerExit2D_t245046360_0_0_0_Types };
extern const Il2CppType PlayMakerTriggerStay2D_t641926867_0_0_0;
static const Il2CppType* GenInst_PlayMakerTriggerStay2D_t641926867_0_0_0_Types[] = { &PlayMakerTriggerStay2D_t641926867_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerTriggerStay2D_t641926867_0_0_0 = { 1, GenInst_PlayMakerTriggerStay2D_t641926867_0_0_0_Types };
extern const Il2CppType PlayMakerParticleCollision_t2428451804_0_0_0;
static const Il2CppType* GenInst_PlayMakerParticleCollision_t2428451804_0_0_0_Types[] = { &PlayMakerParticleCollision_t2428451804_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerParticleCollision_t2428451804_0_0_0 = { 1, GenInst_PlayMakerParticleCollision_t2428451804_0_0_0_Types };
extern const Il2CppType PlayMakerControllerColliderHit_t1717485139_0_0_0;
static const Il2CppType* GenInst_PlayMakerControllerColliderHit_t1717485139_0_0_0_Types[] = { &PlayMakerControllerColliderHit_t1717485139_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerControllerColliderHit_t1717485139_0_0_0 = { 1, GenInst_PlayMakerControllerColliderHit_t1717485139_0_0_0_Types };
extern const Il2CppType PlayMakerJointBreak_t197925893_0_0_0;
static const Il2CppType* GenInst_PlayMakerJointBreak_t197925893_0_0_0_Types[] = { &PlayMakerJointBreak_t197925893_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerJointBreak_t197925893_0_0_0 = { 1, GenInst_PlayMakerJointBreak_t197925893_0_0_0_Types };
extern const Il2CppType PlayMakerFixedUpdate_t997170669_0_0_0;
static const Il2CppType* GenInst_PlayMakerFixedUpdate_t997170669_0_0_0_Types[] = { &PlayMakerFixedUpdate_t997170669_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerFixedUpdate_t997170669_0_0_0 = { 1, GenInst_PlayMakerFixedUpdate_t997170669_0_0_0_Types };
extern const Il2CppType PlayMakerOnGUI_t940239724_0_0_0;
static const Il2CppType* GenInst_PlayMakerOnGUI_t940239724_0_0_0_Types[] = { &PlayMakerOnGUI_t940239724_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerOnGUI_t940239724_0_0_0 = { 1, GenInst_PlayMakerOnGUI_t940239724_0_0_0_Types };
extern const Il2CppType PlayMakerApplicationEvents_t1615127321_0_0_0;
static const Il2CppType* GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0_Types[] = { &PlayMakerApplicationEvents_t1615127321_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0 = { 1, GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0_Types };
extern const Il2CppType PlayMakerAnimatorMove_t1973299400_0_0_0;
static const Il2CppType* GenInst_PlayMakerAnimatorMove_t1973299400_0_0_0_Types[] = { &PlayMakerAnimatorMove_t1973299400_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerAnimatorMove_t1973299400_0_0_0 = { 1, GenInst_PlayMakerAnimatorMove_t1973299400_0_0_0_Types };
extern const Il2CppType PlayMakerAnimatorIK_t2388642745_0_0_0;
static const Il2CppType* GenInst_PlayMakerAnimatorIK_t2388642745_0_0_0_Types[] = { &PlayMakerAnimatorIK_t2388642745_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerAnimatorIK_t2388642745_0_0_0 = { 1, GenInst_PlayMakerAnimatorIK_t2388642745_0_0_0_Types };
extern const Il2CppType PlayMakerGlobals_t3097244096_0_0_0;
static const Il2CppType* GenInst_PlayMakerGlobals_t3097244096_0_0_0_Types[] = { &PlayMakerGlobals_t3097244096_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerGlobals_t3097244096_0_0_0 = { 1, GenInst_PlayMakerGlobals_t3097244096_0_0_0_Types };
extern const Il2CppType PlayMakerGUI_t3799848395_0_0_0;
static const Il2CppType* GenInst_PlayMakerGUI_t3799848395_0_0_0_Types[] = { &PlayMakerGUI_t3799848395_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerGUI_t3799848395_0_0_0 = { 1, GenInst_PlayMakerGUI_t3799848395_0_0_0_Types };
extern const Il2CppType PlayMakerPrefs_t941311808_0_0_0;
static const Il2CppType* GenInst_PlayMakerPrefs_t941311808_0_0_0_Types[] = { &PlayMakerPrefs_t941311808_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayMakerPrefs_t941311808_0_0_0 = { 1, GenInst_PlayMakerPrefs_t941311808_0_0_0_Types };
extern const Il2CppType EventSystem_t2276120119_0_0_0;
static const Il2CppType* GenInst_EventSystem_t2276120119_0_0_0_Types[] = { &EventSystem_t2276120119_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t2276120119_0_0_0 = { 1, GenInst_EventSystem_t2276120119_0_0_0_Types };
extern const Il2CppType AxisEventData_t3355659985_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t3355659985_0_0_0_Types[] = { &AxisEventData_t3355659985_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t3355659985_0_0_0 = { 1, GenInst_AxisEventData_t3355659985_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t2548470764_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t2548470764_0_0_0_Types[] = { &SpriteRenderer_t2548470764_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t2548470764_0_0_0 = { 1, GenInst_SpriteRenderer_t2548470764_0_0_0_Types };
extern const Il2CppType AspectMode_t2149445162_0_0_0;
static const Il2CppType* GenInst_AspectMode_t2149445162_0_0_0_Types[] = { &AspectMode_t2149445162_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t2149445162_0_0_0 = { 1, GenInst_AspectMode_t2149445162_0_0_0_Types };
extern const Il2CppType FitMode_t909765868_0_0_0;
static const Il2CppType* GenInst_FitMode_t909765868_0_0_0_Types[] = { &FitMode_t909765868_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t909765868_0_0_0 = { 1, GenInst_FitMode_t909765868_0_0_0_Types };
extern const Il2CppType RawImage_t821930207_0_0_0;
static const Il2CppType* GenInst_RawImage_t821930207_0_0_0_Types[] = { &RawImage_t821930207_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t821930207_0_0_0 = { 1, GenInst_RawImage_t821930207_0_0_0_Types };
extern const Il2CppType Slider_t79469677_0_0_0;
static const Il2CppType* GenInst_Slider_t79469677_0_0_0_Types[] = { &Slider_t79469677_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t79469677_0_0_0 = { 1, GenInst_Slider_t79469677_0_0_0_Types };
extern const Il2CppType Scrollbar_t2601556940_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t2601556940_0_0_0_Types[] = { &Scrollbar_t2601556940_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t2601556940_0_0_0 = { 1, GenInst_Scrollbar_t2601556940_0_0_0_Types };
extern const Il2CppType InputField_t609046876_0_0_0;
static const Il2CppType* GenInst_InputField_t609046876_0_0_0_Types[] = { &InputField_t609046876_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t609046876_0_0_0 = { 1, GenInst_InputField_t609046876_0_0_0_Types };
extern const Il2CppType ScrollRect_t3606982749_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t3606982749_0_0_0_Types[] = { &ScrollRect_t3606982749_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t3606982749_0_0_0 = { 1, GenInst_ScrollRect_t3606982749_0_0_0_Types };
extern const Il2CppType Dropdown_t4201779933_0_0_0;
static const Il2CppType* GenInst_Dropdown_t4201779933_0_0_0_Types[] = { &Dropdown_t4201779933_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t4201779933_0_0_0 = { 1, GenInst_Dropdown_t4201779933_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t911782554_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t911782554_0_0_0_Types[] = { &GraphicRaycaster_t911782554_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t911782554_0_0_0 = { 1, GenInst_GraphicRaycaster_t911782554_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t3950887807_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t3950887807_0_0_0_Types[] = { &CanvasRenderer_t3950887807_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t3950887807_0_0_0 = { 1, GenInst_CanvasRenderer_t3950887807_0_0_0_Types };
extern const Il2CppType Corner_t284493240_0_0_0;
static const Il2CppType* GenInst_Corner_t284493240_0_0_0_Types[] = { &Corner_t284493240_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t284493240_0_0_0 = { 1, GenInst_Corner_t284493240_0_0_0_Types };
extern const Il2CppType Axis_t1399125956_0_0_0;
static const Il2CppType* GenInst_Axis_t1399125956_0_0_0_Types[] = { &Axis_t1399125956_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1399125956_0_0_0 = { 1, GenInst_Axis_t1399125956_0_0_0_Types };
extern const Il2CppType Constraint_t1640775616_0_0_0;
static const Il2CppType* GenInst_Constraint_t1640775616_0_0_0_Types[] = { &Constraint_t1640775616_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t1640775616_0_0_0 = { 1, GenInst_Constraint_t1640775616_0_0_0_Types };
extern const Il2CppType Type_t3063828369_0_0_0;
static const Il2CppType* GenInst_Type_t3063828369_0_0_0_Types[] = { &Type_t3063828369_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3063828369_0_0_0 = { 1, GenInst_Type_t3063828369_0_0_0_Types };
extern const Il2CppType FillMethod_t2255824731_0_0_0;
static const Il2CppType* GenInst_FillMethod_t2255824731_0_0_0_Types[] = { &FillMethod_t2255824731_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t2255824731_0_0_0 = { 1, GenInst_FillMethod_t2255824731_0_0_0_Types };
extern const Il2CppType SubmitEvent_t3081690246_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t3081690246_0_0_0_Types[] = { &SubmitEvent_t3081690246_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t3081690246_0_0_0 = { 1, GenInst_SubmitEvent_t3081690246_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2697516943_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2697516943_0_0_0_Types[] = { &OnChangeEvent_t2697516943_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2697516943_0_0_0 = { 1, GenInst_OnChangeEvent_t2697516943_0_0_0_Types };
extern const Il2CppType OnValidateInput_t3952708057_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t3952708057_0_0_0_Types[] = { &OnValidateInput_t3952708057_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t3952708057_0_0_0 = { 1, GenInst_OnValidateInput_t3952708057_0_0_0_Types };
extern const Il2CppType LineType_t2016592042_0_0_0;
static const Il2CppType* GenInst_LineType_t2016592042_0_0_0_Types[] = { &LineType_t2016592042_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2016592042_0_0_0 = { 1, GenInst_LineType_t2016592042_0_0_0_Types };
extern const Il2CppType InputType_t1602890312_0_0_0;
static const Il2CppType* GenInst_InputType_t1602890312_0_0_0_Types[] = { &InputType_t1602890312_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1602890312_0_0_0 = { 1, GenInst_InputType_t1602890312_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t2604324130_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t2604324130_0_0_0_Types[] = { &TouchScreenKeyboardType_t2604324130_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t2604324130_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t2604324130_0_0_0_Types };
extern const Il2CppType CharacterValidation_t737650598_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t737650598_0_0_0_Types[] = { &CharacterValidation_t737650598_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t737650598_0_0_0 = { 1, GenInst_CharacterValidation_t737650598_0_0_0_Types };
extern const Il2CppType LayoutElement_t1596995480_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t1596995480_0_0_0_Types[] = { &LayoutElement_t1596995480_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t1596995480_0_0_0 = { 1, GenInst_LayoutElement_t1596995480_0_0_0_Types };
extern const Il2CppType RectOffset_t3056157787_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3056157787_0_0_0_Types[] = { &RectOffset_t3056157787_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3056157787_0_0_0 = { 1, GenInst_RectOffset_t3056157787_0_0_0_Types };
extern const Il2CppType TextAnchor_t213922566_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t213922566_0_0_0_Types[] = { &TextAnchor_t213922566_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t213922566_0_0_0 = { 1, GenInst_TextAnchor_t213922566_0_0_0_Types };
extern const Il2CppType Direction_t522766867_0_0_0;
static const Il2CppType* GenInst_Direction_t522766867_0_0_0_Types[] = { &Direction_t522766867_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t522766867_0_0_0 = { 1, GenInst_Direction_t522766867_0_0_0_Types };
extern const Il2CppType Transition_t1922345195_0_0_0;
static const Il2CppType* GenInst_Transition_t1922345195_0_0_0_Types[] = { &Transition_t1922345195_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t1922345195_0_0_0 = { 1, GenInst_Transition_t1922345195_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t115197445_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t115197445_0_0_0_Types[] = { &AnimationTriggers_t115197445_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t115197445_0_0_0 = { 1, GenInst_AnimationTriggers_t115197445_0_0_0_Types };
extern const Il2CppType Animator_t2776330603_0_0_0;
static const Il2CppType* GenInst_Animator_t2776330603_0_0_0_Types[] = { &Animator_t2776330603_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t2776330603_0_0_0 = { 1, GenInst_Animator_t2776330603_0_0_0_Types };
extern const Il2CppType Direction_t94975348_0_0_0;
static const Il2CppType* GenInst_Direction_t94975348_0_0_0_Types[] = { &Direction_t94975348_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t94975348_0_0_0 = { 1, GenInst_Direction_t94975348_0_0_0_Types };
extern const Il2CppType CFX_Demo_Translate_t2936908572_0_0_0;
static const Il2CppType* GenInst_CFX_Demo_Translate_t2936908572_0_0_0_Types[] = { &CFX_Demo_Translate_t2936908572_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_Demo_Translate_t2936908572_0_0_0 = { 1, GenInst_CFX_Demo_Translate_t2936908572_0_0_0_Types };
extern const Il2CppType CFX_AutoStopLoopedEffect_t3902828655_0_0_0;
static const Il2CppType* GenInst_CFX_AutoStopLoopedEffect_t3902828655_0_0_0_Types[] = { &CFX_AutoStopLoopedEffect_t3902828655_0_0_0 };
extern const Il2CppGenericInst GenInst_CFX_AutoStopLoopedEffect_t3902828655_0_0_0 = { 1, GenInst_CFX_AutoStopLoopedEffect_t3902828655_0_0_0_Types };
extern const Il2CppType Action_1_t914279838_0_0_0;
static const Il2CppType* GenInst_Action_1_t914279838_0_0_0_Types[] = { &Action_1_t914279838_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t914279838_0_0_0 = { 1, GenInst_Action_1_t914279838_0_0_0_Types };
extern const Il2CppType InvitationReceivedDelegate_t2409308905_0_0_0;
static const Il2CppType* GenInst_InvitationReceivedDelegate_t2409308905_0_0_0_Types[] = { &InvitationReceivedDelegate_t2409308905_0_0_0 };
extern const Il2CppGenericInst GenInst_InvitationReceivedDelegate_t2409308905_0_0_0 = { 1, GenInst_InvitationReceivedDelegate_t2409308905_0_0_0_Types };
extern const Il2CppType MatchDelegate_t1377674964_0_0_0;
static const Il2CppType* GenInst_MatchDelegate_t1377674964_0_0_0_Types[] = { &MatchDelegate_t1377674964_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDelegate_t1377674964_0_0_0 = { 1, GenInst_MatchDelegate_t1377674964_0_0_0_Types };
extern const Il2CppType PlayGamesClientConfiguration_t4135364200_0_0_0;
static const Il2CppType* GenInst_PlayGamesClientConfiguration_t4135364200_0_0_0_Types[] = { &PlayGamesClientConfiguration_t4135364200_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayGamesClientConfiguration_t4135364200_0_0_0 = { 1, GenInst_PlayGamesClientConfiguration_t4135364200_0_0_0_Types };
extern const Il2CppType EventManager_t2071886271_0_0_0;
static const Il2CppType* GenInst_EventManager_t2071886271_0_0_0_Types[] = { &EventManager_t2071886271_0_0_0 };
extern const Il2CppGenericInst GenInst_EventManager_t2071886271_0_0_0 = { 1, GenInst_EventManager_t2071886271_0_0_0_Types };
extern const Il2CppType Action_2_t1791805235_0_0_0;
static const Il2CppType* GenInst_Action_2_t1791805235_0_0_0_Types[] = { &Action_2_t1791805235_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1791805235_0_0_0 = { 1, GenInst_Action_2_t1791805235_0_0_0_Types };
extern const Il2CppType Action_2_t423619683_0_0_0;
static const Il2CppType* GenInst_Action_2_t423619683_0_0_0_Types[] = { &Action_2_t423619683_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t423619683_0_0_0 = { 1, GenInst_Action_2_t423619683_0_0_0_Types };
extern const Il2CppType QuestManager_t675525239_0_0_0;
static const Il2CppType* GenInst_QuestManager_t675525239_0_0_0_Types[] = { &QuestManager_t675525239_0_0_0 };
extern const Il2CppGenericInst GenInst_QuestManager_t675525239_0_0_0 = { 1, GenInst_QuestManager_t675525239_0_0_0_Types };
extern const Il2CppType Action_2_t4092892251_0_0_0;
static const Il2CppType* GenInst_Action_2_t4092892251_0_0_0_Types[] = { &Action_2_t4092892251_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4092892251_0_0_0 = { 1, GenInst_Action_2_t4092892251_0_0_0_Types };
extern const Il2CppType Action_2_t1166110507_0_0_0;
static const Il2CppType* GenInst_Action_2_t1166110507_0_0_0_Types[] = { &Action_2_t1166110507_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1166110507_0_0_0 = { 1, GenInst_Action_2_t1166110507_0_0_0_Types };
extern const Il2CppType Action_3_t1510264275_0_0_0;
static const Il2CppType* GenInst_Action_3_t1510264275_0_0_0_Types[] = { &Action_3_t1510264275_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t1510264275_0_0_0 = { 1, GenInst_Action_3_t1510264275_0_0_0_Types };
extern const Il2CppType Action_2_t2211148282_0_0_0;
static const Il2CppType* GenInst_Action_2_t2211148282_0_0_0_Types[] = { &Action_2_t2211148282_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2211148282_0_0_0 = { 1, GenInst_Action_2_t2211148282_0_0_0_Types };
static const Il2CppType* GenInst_IQuestMilestone_t3485030629_0_0_0_Types[] = { &IQuestMilestone_t3485030629_0_0_0 };
extern const Il2CppGenericInst GenInst_IQuestMilestone_t3485030629_0_0_0 = { 1, GenInst_IQuestMilestone_t3485030629_0_0_0_Types };
extern const Il2CppType Action_3_t2040057952_0_0_0;
static const Il2CppType* GenInst_Action_3_t2040057952_0_0_0_Types[] = { &Action_3_t2040057952_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2040057952_0_0_0 = { 1, GenInst_Action_3_t2040057952_0_0_0_Types };
extern const Il2CppType NativeClient_t3798002602_0_0_0;
static const Il2CppType* GenInst_NativeClient_t3798002602_0_0_0_Types[] = { &NativeClient_t3798002602_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeClient_t3798002602_0_0_0 = { 1, GenInst_NativeClient_t3798002602_0_0_0_Types };
extern const Il2CppType RealtimeManager_t1839590440_0_0_0;
static const Il2CppType* GenInst_RealtimeManager_t1839590440_0_0_0_Types[] = { &RealtimeManager_t1839590440_0_0_0 };
extern const Il2CppGenericInst GenInst_RealtimeManager_t1839590440_0_0_0 = { 1, GenInst_RealtimeManager_t1839590440_0_0_0_Types };
extern const Il2CppType RoomSession_t1352686482_0_0_0;
static const Il2CppType* GenInst_RoomSession_t1352686482_0_0_0_Types[] = { &RoomSession_t1352686482_0_0_0 };
extern const Il2CppGenericInst GenInst_RoomSession_t1352686482_0_0_0 = { 1, GenInst_RoomSession_t1352686482_0_0_0_Types };
extern const Il2CppType RealTimeMultiplayerListener_t8218125_0_0_0;
static const Il2CppType* GenInst_RealTimeMultiplayerListener_t8218125_0_0_0_Types[] = { &RealTimeMultiplayerListener_t8218125_0_0_0 };
extern const Il2CppGenericInst GenInst_RealTimeMultiplayerListener_t8218125_0_0_0 = { 1, GenInst_RealTimeMultiplayerListener_t8218125_0_0_0_Types };
extern const Il2CppType State_t1703243816_0_0_0;
static const Il2CppType* GenInst_State_t1703243816_0_0_0_Types[] = { &State_t1703243816_0_0_0 };
extern const Il2CppGenericInst GenInst_State_t1703243816_0_0_0 = { 1, GenInst_State_t1703243816_0_0_0_Types };
extern const Il2CppType SnapshotManager_t2359319983_0_0_0;
static const Il2CppType* GenInst_SnapshotManager_t2359319983_0_0_0_Types[] = { &SnapshotManager_t2359319983_0_0_0 };
extern const Il2CppGenericInst GenInst_SnapshotManager_t2359319983_0_0_0 = { 1, GenInst_SnapshotManager_t2359319983_0_0_0_Types };
extern const Il2CppType Action_2_t2072880178_0_0_0;
static const Il2CppType* GenInst_Action_2_t2072880178_0_0_0_Types[] = { &Action_2_t2072880178_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2072880178_0_0_0 = { 1, GenInst_Action_2_t2072880178_0_0_0_Types };
extern const Il2CppType ConflictCallback_t942269343_0_0_0;
static const Il2CppType* GenInst_ConflictCallback_t942269343_0_0_0_Types[] = { &ConflictCallback_t942269343_0_0_0 };
extern const Il2CppGenericInst GenInst_ConflictCallback_t942269343_0_0_0 = { 1, GenInst_ConflictCallback_t942269343_0_0_0_Types };
extern const Il2CppType Action_2_t2751370656_0_0_0;
static const Il2CppType* GenInst_Action_2_t2751370656_0_0_0_Types[] = { &Action_2_t2751370656_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2751370656_0_0_0 = { 1, GenInst_Action_2_t2751370656_0_0_0_Types };
extern const Il2CppType Action_2_t1151548080_0_0_0;
static const Il2CppType* GenInst_Action_2_t1151548080_0_0_0_Types[] = { &Action_2_t1151548080_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1151548080_0_0_0 = { 1, GenInst_Action_2_t1151548080_0_0_0_Types };
extern const Il2CppType Action_2_t3441065730_0_0_0;
static const Il2CppType* GenInst_Action_2_t3441065730_0_0_0_Types[] = { &Action_2_t3441065730_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t3441065730_0_0_0 = { 1, GenInst_Action_2_t3441065730_0_0_0_Types };
extern const Il2CppType Action_2_t1400690759_0_0_0;
static const Il2CppType* GenInst_Action_2_t1400690759_0_0_0_Types[] = { &Action_2_t1400690759_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1400690759_0_0_0 = { 1, GenInst_Action_2_t1400690759_0_0_0_Types };
extern const Il2CppType GameServices_t1862808700_0_0_0;
static const Il2CppType* GenInst_GameServices_t1862808700_0_0_0_Types[] = { &GameServices_t1862808700_0_0_0 };
extern const Il2CppGenericInst GenInst_GameServices_t1862808700_0_0_0 = { 1, GenInst_GameServices_t1862808700_0_0_0_Types };
extern const Il2CppType Action_1_t3953224079_0_0_0;
static const Il2CppType* GenInst_Action_1_t3953224079_0_0_0_Types[] = { &Action_1_t3953224079_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3953224079_0_0_0 = { 1, GenInst_Action_1_t3953224079_0_0_0_Types };
extern const Il2CppType Action_1_t3201525886_0_0_0;
static const Il2CppType* GenInst_Action_1_t3201525886_0_0_0_Types[] = { &Action_1_t3201525886_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3201525886_0_0_0 = { 1, GenInst_Action_1_t3201525886_0_0_0_Types };
extern const Il2CppType Action_1_t2909004501_0_0_0;
static const Il2CppType* GenInst_Action_1_t2909004501_0_0_0_Types[] = { &Action_1_t2909004501_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t2909004501_0_0_0 = { 1, GenInst_Action_1_t2909004501_0_0_0_Types };
extern const Il2CppType Action_1_t111250811_0_0_0;
static const Il2CppType* GenInst_Action_1_t111250811_0_0_0_Types[] = { &Action_1_t111250811_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t111250811_0_0_0 = { 1, GenInst_Action_1_t111250811_0_0_0_Types };
extern const Il2CppType Action_1_t1447876159_0_0_0;
static const Il2CppType* GenInst_Action_1_t1447876159_0_0_0_Types[] = { &Action_1_t1447876159_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t1447876159_0_0_0 = { 1, GenInst_Action_1_t1447876159_0_0_0_Types };
extern const Il2CppType AuthFinishedCallback_t563494438_0_0_0;
static const Il2CppType* GenInst_AuthFinishedCallback_t563494438_0_0_0_Types[] = { &AuthFinishedCallback_t563494438_0_0_0 };
extern const Il2CppGenericInst GenInst_AuthFinishedCallback_t563494438_0_0_0 = { 1, GenInst_AuthFinishedCallback_t563494438_0_0_0_Types };
extern const Il2CppType AuthStartedCallback_t2497315511_0_0_0;
static const Il2CppType* GenInst_AuthStartedCallback_t2497315511_0_0_0_Types[] = { &AuthStartedCallback_t2497315511_0_0_0 };
extern const Il2CppGenericInst GenInst_AuthStartedCallback_t2497315511_0_0_0 = { 1, GenInst_AuthStartedCallback_t2497315511_0_0_0_Types };
extern const Il2CppType Action_3_t3317244834_0_0_0;
static const Il2CppType* GenInst_Action_3_t3317244834_0_0_0_Types[] = { &Action_3_t3317244834_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3317244834_0_0_0 = { 1, GenInst_Action_3_t3317244834_0_0_0_Types };
extern const Il2CppType Action_3_t2130612649_0_0_0;
static const Il2CppType* GenInst_Action_3_t2130612649_0_0_0_Types[] = { &Action_3_t2130612649_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2130612649_0_0_0 = { 1, GenInst_Action_3_t2130612649_0_0_0_Types };
extern const Il2CppType Action_2_t2637682483_0_0_0;
static const Il2CppType* GenInst_Action_2_t2637682483_0_0_0_Types[] = { &Action_2_t2637682483_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2637682483_0_0_0 = { 1, GenInst_Action_2_t2637682483_0_0_0_Types };
extern const Il2CppType Action_4_t2045751174_0_0_0;
static const Il2CppType* GenInst_Action_4_t2045751174_0_0_0_Types[] = { &Action_4_t2045751174_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_4_t2045751174_0_0_0 = { 1, GenInst_Action_4_t2045751174_0_0_0_Types };
extern const Il2CppType Action_1_t150760668_0_0_0;
static const Il2CppType* GenInst_Action_1_t150760668_0_0_0_Types[] = { &Action_1_t150760668_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t150760668_0_0_0 = { 1, GenInst_Action_1_t150760668_0_0_0_Types };
extern const Il2CppType Action_1_t3071188627_0_0_0;
static const Il2CppType* GenInst_Action_1_t3071188627_0_0_0_Types[] = { &Action_1_t3071188627_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3071188627_0_0_0 = { 1, GenInst_Action_1_t3071188627_0_0_0_Types };
extern const Il2CppType Action_1_t2328837628_0_0_0;
static const Il2CppType* GenInst_Action_1_t2328837628_0_0_0_Types[] = { &Action_1_t2328837628_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t2328837628_0_0_0 = { 1, GenInst_Action_1_t2328837628_0_0_0_Types };
extern const Il2CppType NativeSnapshotMetadataChange_t2885515174_0_0_0;
static const Il2CppType* GenInst_NativeSnapshotMetadataChange_t2885515174_0_0_0_Types[] = { &NativeSnapshotMetadataChange_t2885515174_0_0_0 };
extern const Il2CppGenericInst GenInst_NativeSnapshotMetadataChange_t2885515174_0_0_0 = { 1, GenInst_NativeSnapshotMetadataChange_t2885515174_0_0_0_Types };
extern const Il2CppType Action_1_t2688443720_0_0_0;
static const Il2CppType* GenInst_Action_1_t2688443720_0_0_0_Types[] = { &Action_1_t2688443720_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t2688443720_0_0_0 = { 1, GenInst_Action_1_t2688443720_0_0_0_Types };
extern const Il2CppType Action_1_t1663036183_0_0_0;
static const Il2CppType* GenInst_Action_1_t1663036183_0_0_0_Types[] = { &Action_1_t1663036183_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t1663036183_0_0_0 = { 1, GenInst_Action_1_t1663036183_0_0_0_Types };
extern const Il2CppType PlayGamesHelperObject_t727662960_0_0_0;
static const Il2CppType* GenInst_PlayGamesHelperObject_t727662960_0_0_0_Types[] = { &PlayGamesHelperObject_t727662960_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayGamesHelperObject_t727662960_0_0_0 = { 1, GenInst_PlayGamesHelperObject_t727662960_0_0_0_Types };
extern const Il2CppType IPlayGamesClient_t2528289561_0_0_0;
static const Il2CppType* GenInst_IPlayGamesClient_t2528289561_0_0_0_Types[] = { &IPlayGamesClient_t2528289561_0_0_0 };
extern const Il2CppGenericInst GenInst_IPlayGamesClient_t2528289561_0_0_0 = { 1, GenInst_IPlayGamesClient_t2528289561_0_0_0_Types };
extern const Il2CppType CharacterController_t1618060635_0_0_0;
static const Il2CppType* GenInst_CharacterController_t1618060635_0_0_0_Types[] = { &CharacterController_t1618060635_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterController_t1618060635_0_0_0 = { 1, GenInst_CharacterController_t1618060635_0_0_0_Types };
extern const Il2CppType SpringJoint_t558455091_0_0_0;
static const Il2CppType* GenInst_SpringJoint_t558455091_0_0_0_Types[] = { &SpringJoint_t558455091_0_0_0 };
extern const Il2CppGenericInst GenInst_SpringJoint_t558455091_0_0_0 = { 1, GenInst_SpringJoint_t558455091_0_0_0_Types };
extern const Il2CppType MeshFilter_t3839065225_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3839065225_0_0_0_Types[] = { &MeshFilter_t3839065225_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3839065225_0_0_0 = { 1, GenInst_MeshFilter_t3839065225_0_0_0_Types };
extern const Il2CppType iTweenFSMEvents_t871409943_0_0_0;
static const Il2CppType* GenInst_iTweenFSMEvents_t871409943_0_0_0_Types[] = { &iTweenFSMEvents_t871409943_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenFSMEvents_t871409943_0_0_0 = { 1, GenInst_iTweenFSMEvents_t871409943_0_0_0_Types };
extern const Il2CppType NavMeshAgent_t588466745_0_0_0;
static const Il2CppType* GenInst_NavMeshAgent_t588466745_0_0_0_Types[] = { &NavMeshAgent_t588466745_0_0_0 };
extern const Il2CppGenericInst GenInst_NavMeshAgent_t588466745_0_0_0 = { 1, GenInst_NavMeshAgent_t588466745_0_0_0_Types };
extern const Il2CppType HingeJoint2D_t2650814389_0_0_0;
static const Il2CppType* GenInst_HingeJoint2D_t2650814389_0_0_0_Types[] = { &HingeJoint2D_t2650814389_0_0_0 };
extern const Il2CppGenericInst GenInst_HingeJoint2D_t2650814389_0_0_0 = { 1, GenInst_HingeJoint2D_t2650814389_0_0_0_Types };
extern const Il2CppType Joint_t4201008640_0_0_0;
static const Il2CppType* GenInst_Joint_t4201008640_0_0_0_Types[] = { &Joint_t4201008640_0_0_0 };
extern const Il2CppGenericInst GenInst_Joint_t4201008640_0_0_0 = { 1, GenInst_Joint_t4201008640_0_0_0_Types };
extern const Il2CppType WheelJoint2D_t2492372869_0_0_0;
static const Il2CppType* GenInst_WheelJoint2D_t2492372869_0_0_0_Types[] = { &WheelJoint2D_t2492372869_0_0_0 };
extern const Il2CppGenericInst GenInst_WheelJoint2D_t2492372869_0_0_0 = { 1, GenInst_WheelJoint2D_t2492372869_0_0_0_Types };
extern const Il2CppType iTween_t3087282050_0_0_0;
static const Il2CppType* GenInst_iTween_t3087282050_0_0_0_Types[] = { &iTween_t3087282050_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t3087282050_0_0_0 = { 1, GenInst_iTween_t3087282050_0_0_0_Types };
extern const Il2CppType Movie_t147243648_0_0_0;
static const Il2CppType* GenInst_Movie_t147243648_0_0_0_Types[] = { &Movie_t147243648_0_0_0 };
extern const Il2CppGenericInst GenInst_Movie_t147243648_0_0_0 = { 1, GenInst_Movie_t147243648_0_0_0_Types };
extern const Il2CppType MeshRenderer_t2804666580_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t2804666580_0_0_0_Types[] = { &MeshRenderer_t2804666580_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t2804666580_0_0_0 = { 1, GenInst_MeshRenderer_t2804666580_0_0_0_Types };
extern const Il2CppType MediaPlayerCtrl_t3572035536_0_0_0;
static const Il2CppType* GenInst_MediaPlayerCtrl_t3572035536_0_0_0_Types[] = { &MediaPlayerCtrl_t3572035536_0_0_0 };
extern const Il2CppGenericInst GenInst_MediaPlayerCtrl_t3572035536_0_0_0 = { 1, GenInst_MediaPlayerCtrl_t3572035536_0_0_0_Types };
extern const Il2CppType ResponseInfo_t1603221679_0_0_0;
static const Il2CppType* GenInst_ResponseInfo_t1603221679_0_0_0_Types[] = { &ResponseInfo_t1603221679_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseInfo_t1603221679_0_0_0 = { 1, GenInst_ResponseInfo_t1603221679_0_0_0_Types };
extern const Il2CppType ResponseInfoType_t519916681_0_0_0;
static const Il2CppType* GenInst_ResponseInfoType_t519916681_0_0_0_Types[] = { &ResponseInfoType_t519916681_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseInfoType_t519916681_0_0_0 = { 1, GenInst_ResponseInfoType_t519916681_0_0_0_Types };
extern const Il2CppType ResponseSaveData_t2563836488_0_0_0;
static const Il2CppType* GenInst_ResponseSaveData_t2563836488_0_0_0_Types[] = { &ResponseSaveData_t2563836488_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseSaveData_t2563836488_0_0_0 = { 1, GenInst_ResponseSaveData_t2563836488_0_0_0_Types };
extern const Il2CppType ResponseResultAllData_t4071630637_0_0_0;
static const Il2CppType* GenInst_ResponseResultAllData_t4071630637_0_0_0_Types[] = { &ResponseResultAllData_t4071630637_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseResultAllData_t4071630637_0_0_0 = { 1, GenInst_ResponseResultAllData_t4071630637_0_0_0_Types };
extern const Il2CppType CaculationResultData_t1977689536_0_0_0;
static const Il2CppType* GenInst_CaculationResultData_t1977689536_0_0_0_Types[] = { &CaculationResultData_t1977689536_0_0_0 };
extern const Il2CppGenericInst GenInst_CaculationResultData_t1977689536_0_0_0 = { 1, GenInst_CaculationResultData_t1977689536_0_0_0_Types };
extern const Il2CppType LearnCalculation_t2957617669_0_0_0;
static const Il2CppType* GenInst_LearnCalculation_t2957617669_0_0_0_Types[] = { &LearnCalculation_t2957617669_0_0_0 };
extern const Il2CppGenericInst GenInst_LearnCalculation_t2957617669_0_0_0 = { 1, GenInst_LearnCalculation_t2957617669_0_0_0_Types };
extern const Il2CppType ScoreManager_Lobby_t3853030226_0_0_0;
static const Il2CppType* GenInst_ScoreManager_Lobby_t3853030226_0_0_0_Types[] = { &ScoreManager_Lobby_t3853030226_0_0_0 };
extern const Il2CppGenericInst GenInst_ScoreManager_Lobby_t3853030226_0_0_0 = { 1, GenInst_ScoreManager_Lobby_t3853030226_0_0_0_Types };
extern const Il2CppType ScoreManager_Variation_t1665153935_0_0_0;
static const Il2CppType* GenInst_ScoreManager_Variation_t1665153935_0_0_0_Types[] = { &ScoreManager_Variation_t1665153935_0_0_0 };
extern const Il2CppGenericInst GenInst_ScoreManager_Variation_t1665153935_0_0_0 = { 1, GenInst_ScoreManager_Variation_t1665153935_0_0_0_Types };
extern const Il2CppType ScoreManagerMiniGame_t3434204900_0_0_0;
static const Il2CppType* GenInst_ScoreManagerMiniGame_t3434204900_0_0_0_Types[] = { &ScoreManagerMiniGame_t3434204900_0_0_0 };
extern const Il2CppGenericInst GenInst_ScoreManagerMiniGame_t3434204900_0_0_0 = { 1, GenInst_ScoreManagerMiniGame_t3434204900_0_0_0_Types };
extern const Il2CppType ScoreManager_Temping_t3029572618_0_0_0;
static const Il2CppType* GenInst_ScoreManager_Temping_t3029572618_0_0_0_Types[] = { &ScoreManager_Temping_t3029572618_0_0_0 };
extern const Il2CppGenericInst GenInst_ScoreManager_Temping_t3029572618_0_0_0 = { 1, GenInst_ScoreManager_Temping_t3029572618_0_0_0_Types };
extern const Il2CppType ScoreManager_Icing_t3849909252_0_0_0;
static const Il2CppType* GenInst_ScoreManager_Icing_t3849909252_0_0_0_Types[] = { &ScoreManager_Icing_t3849909252_0_0_0 };
extern const Il2CppGenericInst GenInst_ScoreManager_Icing_t3849909252_0_0_0 = { 1, GenInst_ScoreManager_Icing_t3849909252_0_0_0_Types };
extern const Il2CppType Popup_t77299852_0_0_0;
static const Il2CppType* GenInst_Popup_t77299852_0_0_0_Types[] = { &Popup_t77299852_0_0_0 };
extern const Il2CppGenericInst GenInst_Popup_t77299852_0_0_0 = { 1, GenInst_Popup_t77299852_0_0_0_Types };
extern const Il2CppType TempingPointer_t2678149871_0_0_0;
static const Il2CppType* GenInst_TempingPointer_t2678149871_0_0_0_Types[] = { &TempingPointer_t2678149871_0_0_0 };
extern const Il2CppGenericInst GenInst_TempingPointer_t2678149871_0_0_0 = { 1, GenInst_TempingPointer_t2678149871_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
static const Il2CppType* GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0_Types[] = { &ParamDataType_t2672665179_0_0_0, &ParamDataType_t2672665179_0_0_0 };
extern const Il2CppGenericInst GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0 = { 2, GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0_Types };
static const Il2CppType* GenInst_JsonPosition_t3864946409_0_0_0_JsonPosition_t3864946409_0_0_0_Types[] = { &JsonPosition_t3864946409_0_0_0, &JsonPosition_t3864946409_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonPosition_t3864946409_0_0_0_JsonPosition_t3864946409_0_0_0 = { 2, GenInst_JsonPosition_t3864946409_0_0_0_JsonPosition_t3864946409_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types[] = { &Boolean_t476798718_0_0_0, &Boolean_t476798718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0 = { 2, GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0_Types[] = { &Byte_t2862609660_0_0_0, &Byte_t2862609660_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0 = { 2, GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0_Types };
static const Il2CppType* GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0_Types[] = { &Char_t2862622538_0_0_0, &Char_t2862622538_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0 = { 2, GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &KeyValuePair_2_t1944668977_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0_Types[] = { &CustomAttributeNamedArgument_t3059612989_0_0_0, &CustomAttributeNamedArgument_t3059612989_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0_Types[] = { &CustomAttributeTypedArgument_t3301293422_0_0_0, &CustomAttributeTypedArgument_t3301293422_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0_Types[] = { &Color32_t598853688_0_0_0, &Color32_t598853688_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0 = { 2, GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0_Types[] = { &RaycastResult_t3762661364_0_0_0, &RaycastResult_t3762661364_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0 = { 2, GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t70832698_0_0_0_Playable_t70832698_0_0_0_Types[] = { &Playable_t70832698_0_0_0, &Playable_t70832698_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t70832698_0_0_0_Playable_t70832698_0_0_0 = { 2, GenInst_Playable_t70832698_0_0_0_Playable_t70832698_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0_Types[] = { &UICharInfo_t65807484_0_0_0, &UICharInfo_t65807484_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0 = { 2, GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0_Types[] = { &UILineInfo_t4113875482_0_0_0, &UILineInfo_t4113875482_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0 = { 2, GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0_Types[] = { &UIVertex_t4244065212_0_0_0, &UIVertex_t4244065212_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0 = { 2, GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0_Types[] = { &Vector2_t4282066565_0_0_0, &Vector2_t4282066565_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0 = { 2, GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0_Types[] = { &Vector3_t4282066566_0_0_0, &Vector3_t4282066566_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0 = { 2, GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0_Types[] = { &Vector4_t4282066567_0_0_0, &Vector4_t4282066567_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0 = { 2, GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types[] = { &ParticipantStatus_t4028684685_0_0_0, &ParticipantStatus_t4028684685_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0 = { 2, GenInst_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t4028684685_0_0_0_Il2CppObject_0_0_0_Types[] = { &ParticipantStatus_t4028684685_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t4028684685_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ParticipantStatus_t4028684685_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t3137786926_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &ParticipantStatus_t3137786926_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t3137786926_0_0_0 = { 2, GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t3137786926_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantStatus_t3137786926_0_0_0_Il2CppObject_0_0_0_Types[] = { &ParticipantStatus_t3137786926_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantStatus_t3137786926_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ParticipantStatus_t3137786926_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3076810564_0_0_0_KeyValuePair_2_t3076810564_0_0_0_Types[] = { &KeyValuePair_2_t3076810564_0_0_0, &KeyValuePair_2_t3076810564_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3076810564_0_0_0_KeyValuePair_2_t3076810564_0_0_0 = { 2, GenInst_KeyValuePair_2_t3076810564_0_0_0_KeyValuePair_2_t3076810564_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3076810564_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3076810564_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3076810564_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3076810564_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeNameKey_t2971844791_0_0_0_TypeNameKey_t2971844791_0_0_0_Types[] = { &TypeNameKey_t2971844791_0_0_0, &TypeNameKey_t2971844791_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeNameKey_t2971844791_0_0_0_TypeNameKey_t2971844791_0_0_0 = { 2, GenInst_TypeNameKey_t2971844791_0_0_0_TypeNameKey_t2971844791_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4013503581_0_0_0_KeyValuePair_2_t4013503581_0_0_0_Types[] = { &KeyValuePair_2_t4013503581_0_0_0, &KeyValuePair_2_t4013503581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4013503581_0_0_0_KeyValuePair_2_t4013503581_0_0_0 = { 2, GenInst_KeyValuePair_2_t4013503581_0_0_0_KeyValuePair_2_t4013503581_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4013503581_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4013503581_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4013503581_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4013503581_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ResolverContractKey_t473801005_0_0_0_ResolverContractKey_t473801005_0_0_0_Types[] = { &ResolverContractKey_t473801005_0_0_0, &ResolverContractKey_t473801005_0_0_0 };
extern const Il2CppGenericInst GenInst_ResolverContractKey_t473801005_0_0_0_ResolverContractKey_t473801005_0_0_0 = { 2, GenInst_ResolverContractKey_t473801005_0_0_0_ResolverContractKey_t473801005_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2893931855_0_0_0_KeyValuePair_2_t2893931855_0_0_0_Types[] = { &KeyValuePair_2_t2893931855_0_0_0, &KeyValuePair_2_t2893931855_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2893931855_0_0_0_KeyValuePair_2_t2893931855_0_0_0 = { 2, GenInst_KeyValuePair_2_t2893931855_0_0_0_KeyValuePair_2_t2893931855_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2893931855_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2893931855_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2893931855_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2893931855_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TypeConvertKey_t866134174_0_0_0_TypeConvertKey_t866134174_0_0_0_Types[] = { &TypeConvertKey_t866134174_0_0_0, &TypeConvertKey_t866134174_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeConvertKey_t866134174_0_0_0_TypeConvertKey_t866134174_0_0_0 = { 2, GenInst_TypeConvertKey_t866134174_0_0_0_TypeConvertKey_t866134174_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2815902970_0_0_0_KeyValuePair_2_t2815902970_0_0_0_Types[] = { &KeyValuePair_2_t2815902970_0_0_0, &KeyValuePair_2_t2815902970_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815902970_0_0_0_KeyValuePair_2_t2815902970_0_0_0 = { 2, GenInst_KeyValuePair_2_t2815902970_0_0_0_KeyValuePair_2_t2815902970_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2815902970_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2815902970_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2815902970_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2815902970_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0, &KeyValuePair_2_t1049882445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0 = { 2, GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1049882445_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &KeyValuePair_2_t4066860316_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4066860316_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0_Types[] = { &ParticipantResult_t2327217482_0_0_0, &ParticipantResult_t2327217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0 = { 2, GenInst_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0_Types };
static const Il2CppType* GenInst_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0_Types[] = { &ParticipantResult_t2327217482_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t101070088_0_0_0_KeyValuePair_2_t101070088_0_0_0_Types[] = { &KeyValuePair_2_t101070088_0_0_0, &KeyValuePair_2_t101070088_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t101070088_0_0_0_KeyValuePair_2_t101070088_0_0_0 = { 2, GenInst_KeyValuePair_2_t101070088_0_0_0_KeyValuePair_2_t101070088_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t101070088_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t101070088_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t101070088_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t101070088_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0_Types[] = { &ReadType_t3446921512_0_0_0, &ReadType_t3446921512_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0 = { 2, GenInst_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0_Types };
static const Il2CppType* GenInst_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0_Types[] = { &ReadType_t3446921512_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1220774118_0_0_0_KeyValuePair_2_t1220774118_0_0_0_Types[] = { &KeyValuePair_2_t1220774118_0_0_0, &KeyValuePair_2_t1220774118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1220774118_0_0_0_KeyValuePair_2_t1220774118_0_0_0 = { 2, GenInst_KeyValuePair_2_t1220774118_0_0_0_KeyValuePair_2_t1220774118_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1220774118_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1220774118_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1220774118_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1220774118_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0_Types[] = { &PropertyPresence_t2892329490_0_0_0, &PropertyPresence_t2892329490_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0 = { 2, GenInst_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0_Types };
static const Il2CppType* GenInst_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0_Types[] = { &PropertyPresence_t2892329490_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t666182096_0_0_0_KeyValuePair_2_t666182096_0_0_0_Types[] = { &KeyValuePair_2_t666182096_0_0_0, &KeyValuePair_2_t666182096_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t666182096_0_0_0_KeyValuePair_2_t666182096_0_0_0 = { 2, GenInst_KeyValuePair_2_t666182096_0_0_0_KeyValuePair_2_t666182096_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t666182096_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t666182096_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t666182096_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t666182096_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types[] = { &PrimitiveTypeCode_t2429291660_0_0_0, &PrimitiveTypeCode_t2429291660_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0 = { 2, GenInst_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Types };
static const Il2CppType* GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0_Types[] = { &PrimitiveTypeCode_t2429291660_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t203144266_0_0_0_KeyValuePair_2_t203144266_0_0_0_Types[] = { &KeyValuePair_2_t203144266_0_0_0, &KeyValuePair_2_t203144266_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t203144266_0_0_0_KeyValuePair_2_t203144266_0_0_0 = { 2, GenInst_KeyValuePair_2_t203144266_0_0_0_KeyValuePair_2_t203144266_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t203144266_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t203144266_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t203144266_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t203144266_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &KeyValuePair_2_t2545618620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2545618620_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &KeyValuePair_2_t3222658402_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3222658402_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1944668977_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0, &KeyValuePair_2_t2065771578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0 = { 2, GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2065771578_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t4291918972_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types[] = { &Single_t4291918972_0_0_0, &Single_t4291918972_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0 = { 2, GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487883_0_0_0_KeyValuePair_2_t2093487883_0_0_0_Types[] = { &KeyValuePair_2_t2093487883_0_0_0, &KeyValuePair_2_t2093487883_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487883_0_0_0_KeyValuePair_2_t2093487883_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487883_0_0_0_KeyValuePair_2_t2093487883_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2093487883_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2093487883_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2093487883_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2093487883_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt32_t24667981_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0_Types[] = { &UInt32_t24667981_0_0_0, &UInt32_t24667981_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0 = { 2, GenInst_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3443564286_0_0_0_KeyValuePair_2_t3443564286_0_0_0_Types[] = { &KeyValuePair_2_t3443564286_0_0_0, &KeyValuePair_2_t3443564286_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3443564286_0_0_0_KeyValuePair_2_t3443564286_0_0_0 = { 2, GenInst_KeyValuePair_2_t3443564286_0_0_0_KeyValuePair_2_t3443564286_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3443564286_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3443564286_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3443564286_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3443564286_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0_Types[] = { &RaycastHit2D_t1374744384_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types[] = { &RaycastHit2D_t1374744384_0_0_0, &RaycastHit2D_t1374744384_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0 = { 2, GenInst_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0, &KeyValuePair_2_t1919813716_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0 = { 2, GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1919813716_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types[] = { &TextEditOp_t4145961110_0_0_0, &TextEditOp_t4145961110_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0 = { 2, GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t300380471_0_0_0_KeyValuePair_2_t300380471_0_0_0_Types[] = { &KeyValuePair_2_t300380471_0_0_0, &KeyValuePair_2_t300380471_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t300380471_0_0_0_KeyValuePair_2_t300380471_0_0_0 = { 2, GenInst_KeyValuePair_2_t300380471_0_0_0_KeyValuePair_2_t300380471_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t300380471_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t300380471_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t300380471_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t300380471_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_HandleRef_t1780819301_0_0_0_HandleRef_t1780819301_0_0_0_Types[] = { &HandleRef_t1780819301_0_0_0, &HandleRef_t1780819301_0_0_0 };
extern const Il2CppGenericInst GenInst_HandleRef_t1780819301_0_0_0_HandleRef_t1780819301_0_0_0 = { 2, GenInst_HandleRef_t1780819301_0_0_0_HandleRef_t1780819301_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1470] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Attribute_t2523058482_0_0_0,
	&GenInst__Attribute_t3253047175_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0,
	&GenInst_Char_t2862622538_0_0_0,
	&GenInst_IConvertible_t2116191568_0_0_0,
	&GenInst_IComparable_t1391370361_0_0_0,
	&GenInst_IComparable_1_t2772552329_0_0_0,
	&GenInst_IEquatable_1_t2411901105_0_0_0,
	&GenInst_ValueType_t1744280289_0_0_0,
	&GenInst_Int64_t1153838595_0_0_0,
	&GenInst_UInt32_t24667981_0_0_0,
	&GenInst_UInt64_t24668076_0_0_0,
	&GenInst_Byte_t2862609660_0_0_0,
	&GenInst_SByte_t1161769777_0_0_0,
	&GenInst_Int16_t1153838442_0_0_0,
	&GenInst_UInt16_t24667923_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t3464557803_0_0_0,
	&GenInst_ICloneable_t1025544834_0_0_0,
	&GenInst_IComparable_1_t4212128644_0_0_0,
	&GenInst_IEquatable_1_t3851477420_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t2853506214_0_0_0,
	&GenInst__Type_t2149739635_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1425685797_0_0_0,
	&GenInst__MemberInfo_t3353101921_0_0_0,
	&GenInst_IFormattable_t382002946_0_0_0,
	&GenInst_IComparable_1_t1063768291_0_0_0,
	&GenInst_IEquatable_1_t703117067_0_0_0,
	&GenInst_Double_t3868226565_0_0_0,
	&GenInst_IComparable_1_t3778156356_0_0_0,
	&GenInst_IEquatable_1_t3417505132_0_0_0,
	&GenInst_IComparable_1_t4229565068_0_0_0,
	&GenInst_IEquatable_1_t3868913844_0_0_0,
	&GenInst_IComparable_1_t2772539451_0_0_0,
	&GenInst_IEquatable_1_t2411888227_0_0_0,
	&GenInst_Single_t4291918972_0_0_0,
	&GenInst_IComparable_1_t4201848763_0_0_0,
	&GenInst_IEquatable_1_t3841197539_0_0_0,
	&GenInst_Decimal_t1954350631_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0,
	&GenInst_Delegate_t3310234105_0_0_0,
	&GenInst_ISerializable_t867484142_0_0_0,
	&GenInst_ParameterInfo_t2235474049_0_0_0,
	&GenInst__ParameterInfo_t2787166306_0_0_0,
	&GenInst_ParameterModifier_t741930026_0_0_0,
	&GenInst_IComparable_1_t4229565010_0_0_0,
	&GenInst_IEquatable_1_t3868913786_0_0_0,
	&GenInst_IComparable_1_t4229565163_0_0_0,
	&GenInst_IEquatable_1_t3868913939_0_0_0,
	&GenInst_IComparable_1_t1063768233_0_0_0,
	&GenInst_IEquatable_1_t703117009_0_0_0,
	&GenInst_IComparable_1_t1071699568_0_0_0,
	&GenInst_IEquatable_1_t711048344_0_0_0,
	&GenInst_IComparable_1_t1063768386_0_0_0,
	&GenInst_IEquatable_1_t703117162_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t3271054163_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t209867187_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3971289384_0_0_0,
	&GenInst_MethodBase_t318515428_0_0_0,
	&GenInst__MethodBase_t3971068747_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3711326812_0_0_0,
	&GenInst_ConstructorInfo_t4136801618_0_0_0,
	&GenInst__ConstructorInfo_t3408715251_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t3372848153_0_0_0,
	&GenInst_TailoringInfo_t3025807515_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_Link_t2063667470_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1873037576_0_0_0,
	&GenInst_Contraction_t3998770676_0_0_0,
	&GenInst_Level2Map_t3664214860_0_0_0,
	&GenInst_BigInteger_t3334373498_0_0_0,
	&GenInst_KeySizes_t2106826975_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_IComparable_1_t386728509_0_0_0,
	&GenInst_IEquatable_1_t26077285_0_0_0,
	&GenInst_Slot_t2260530181_0_0_0,
	&GenInst_Slot_t2072023290_0_0_0,
	&GenInst_StackFrame_t1034942277_0_0_0,
	&GenInst_Calendar_t3558528576_0_0_0,
	&GenInst_CultureInfo_t1065375142_0_0_0,
	&GenInst_IFormatProvider_t192740775_0_0_0,
	&GenInst_ModuleBuilder_t595214213_0_0_0,
	&GenInst__ModuleBuilder_t1764509690_0_0_0,
	&GenInst_Module_t1394482686_0_0_0,
	&GenInst__Module_t2601912805_0_0_0,
	&GenInst_ParameterBuilder_t3159962230_0_0_0,
	&GenInst__ParameterBuilder_t4122453611_0_0_0,
	&GenInst_TypeU5BU5D_t3339007067_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t2643922881_0_0_0,
	&GenInst_IList_t1751339649_0_0_0,
	&GenInst_ILTokenInfo_t1354080954_0_0_0,
	&GenInst_LabelData_t3207823784_0_0_0,
	&GenInst_LabelFixup_t660379442_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t553556921_0_0_0,
	&GenInst_TypeBuilder_t1918497079_0_0_0,
	&GenInst__TypeBuilder_t3501492652_0_0_0,
	&GenInst_MethodBuilder_t302405488_0_0_0,
	&GenInst__MethodBuilder_t1471700965_0_0_0,
	&GenInst_ConstructorBuilder_t3217839941_0_0_0,
	&GenInst__ConstructorBuilder_t788093754_0_0_0,
	&GenInst_PropertyBuilder_t2012258748_0_0_0,
	&GenInst__PropertyBuilder_t752753201_0_0_0,
	&GenInst_FieldBuilder_t1754069893_0_0_0,
	&GenInst__FieldBuilder_t639782778_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0,
	&GenInst_CustomAttributeData_t2955630591_0_0_0,
	&GenInst_ResourceInfo_t4013605874_0_0_0,
	&GenInst_ResourceCacheItem_t2113902833_0_0_0,
	&GenInst_IContextProperty_t82913453_0_0_0,
	&GenInst_Header_t1689611527_0_0_0,
	&GenInst_ITrackingHandler_t2228500544_0_0_0,
	&GenInst_IContextAttribute_t3913746816_0_0_0,
	&GenInst_DateTime_t4283661327_0_0_0,
	&GenInst_IComparable_1_t4193591118_0_0_0,
	&GenInst_IEquatable_1_t3832939894_0_0_0,
	&GenInst_IComparable_1_t1864280422_0_0_0,
	&GenInst_IEquatable_1_t1503629198_0_0_0,
	&GenInst_TimeSpan_t413522987_0_0_0,
	&GenInst_IComparable_1_t323452778_0_0_0,
	&GenInst_IEquatable_1_t4257768850_0_0_0,
	&GenInst_TypeTag_t2420703430_0_0_0,
	&GenInst_Enum_t2862688501_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2878058698_0_0_0,
	&GenInst_Assembly_t1418687608_0_0_0,
	&GenInst__Assembly_t3789461407_0_0_0,
	&GenInst_DateTimeOffset_t3884714306_0_0_0,
	&GenInst_Guid_t2862754429_0_0_0,
	&GenInst_Version_t763695022_0_0_0,
	&GenInst_BigInteger_t3334373499_0_0_0,
	&GenInst_ByteU5BU5D_t4260760469_0_0_0,
	&GenInst_X509Certificate_t3076817455_0_0_0,
	&GenInst_IDeserializationCallback_t675596727_0_0_0,
	&GenInst_ClientCertificateType_t3167042548_0_0_0,
	&GenInst_PropertyDescriptor_t2073374448_0_0_0,
	&GenInst_MemberDescriptor_t2617136693_0_0_0,
	&GenInst_EventDescriptor_t1405012495_0_0_0,
	&GenInst_AttributeU5BU5D_t4055800263_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0,
	&GenInst_TypeDescriptionProvider_t3543085017_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1421923377_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0,
	&GenInst_WeakObjectWrapper_t1518976226_0_0_0_LinkedList_1_t1417720186_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1472936237_0_0_0,
	&GenInst_X509ChainStatus_t766901931_0_0_0,
	&GenInst_IPAddress_t3525271463_0_0_0,
	&GenInst_ArraySegment_1_t2188033608_0_0_0,
	&GenInst_Cookie_t2033273982_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t476798718_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1195997794_0_0_0,
	&GenInst_Capture_t754001812_0_0_0,
	&GenInst_Group_t2151468941_0_0_0,
	&GenInst_Mark_t3811539797_0_0_0,
	&GenInst_UriScheme_t1290668975_0_0_0,
	&GenInst_Link_t2122599155_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t1244034627_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3312854529_0_0_0,
	&GenInst_IGrouping_2_t641185655_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IEnumerable_1_t3176762032_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_XPathNavigator_t1075073278_0_0_0,
	&GenInst_IXPathNavigable_t153087709_0_0_0,
	&GenInst_IXmlNamespaceResolver_t3774973253_0_0_0,
	&GenInst_XPathItem_t3597956134_0_0_0,
	&GenInst_XPathResultType_t516720010_0_0_0,
	&GenInst_KeyValuePair_2_t2758969756_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t2039770680_0_0_0,
	&GenInst_DTDNode_t2039770680_0_0_0,
	&GenInst_Entry_t2866414864_0_0_0,
	&GenInst_XmlNode_t856910923_0_0_0,
	&GenInst_NsDecl_t3658211563_0_0_0,
	&GenInst_NsScope_t1749213747_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t982414386_0_0_0,
	&GenInst_XmlTokenInfo_t597808448_0_0_0,
	&GenInst_TagName_t2016006645_0_0_0,
	&GenInst_XmlNodeInfo_t1808742809_0_0_0,
	&GenInst_ReferenceLoopHandling_t2761661122_0_0_0,
	&GenInst_TypeNameHandling_t2359325474_0_0_0,
	&GenInst_JsonSerializerSettings_t2589405525_0_0_0,
	&GenInst_JsonConverter_t2159686854_0_0_0,
	&GenInst_Required_t3921306327_0_0_0,
	&GenInst_JsonPosition_t3864946409_0_0_0,
	&GenInst_NullValueHandling_t2754652381_0_0_0,
	&GenInst_DefaultValueHandling_t1569448045_0_0_0,
	&GenInst_ObjectCreationHandling_t56081595_0_0_0,
	&GenInst_Formatting_t732683613_0_0_0,
	&GenInst_DateFormatHandling_t4014082626_0_0_0,
	&GenInst_DateTimeZoneHandling_t2945560484_0_0_0,
	&GenInst_DateParseHandling_t2108333400_0_0_0,
	&GenInst_FloatFormatHandling_t3887485542_0_0_0,
	&GenInst_FloatParseHandling_t3074080948_0_0_0,
	&GenInst_StringEscapeHandling_t1042460335_0_0_0,
	&GenInst_ErrorEventArgs_t792639131_0_0_0,
	&GenInst_MetadataPropertyHandling_t2626038881_0_0_0,
	&GenInst_FormatterAssemblyStyle_t3005881063_0_0_0,
	&GenInst_PreserveReferencesHandling_t4230591217_0_0_0,
	&GenInst_MissingMemberHandling_t2077487315_0_0_0,
	&GenInst_ConstructorHandling_t2475221485_0_0_0,
	&GenInst_StreamingContext_t2761351129_0_0_0,
	&GenInst_IReferenceResolver_t425424564_0_0_0,
	&GenInst_StateU5BU5D_t871800199_0_0_0,
	&GenInst_State_t671991922_0_0_0,
	&GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0,
	&GenInst_KeyValuePair_2_t203144266_0_0_0,
	&GenInst_PrimitiveTypeCode_t2429291660_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_KeyValuePair_2_t203144266_0_0_0,
	&GenInst_Type_t_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2433494851_0_0_0,
	&GenInst_TypeInformation_t2222045584_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2815902970_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0,
	&GenInst_IEquatable_1_t415412741_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_TypeConvertKey_t866134174_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2815902970_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_Func_2_t184564025_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_Type_t_0_0_0_BidirectionalDictionary_2_t157076046_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_EnumMemberAttribute_t1202205191_0_0_0,
	&GenInst_EnumMemberAttribute_t1202205191_0_0_0_String_t_0_0_0,
	&GenInst_FieldInfo_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Link_t814405322_0_0_0,
	&GenInst_Entry_t1172375224_0_0_0,
	&GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0,
	&GenInst_String_t_0_0_0_ReflectionMember_t3070212469_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3789411545_0_0_0,
	&GenInst_ConstructorInfo_t4136801618_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_IGrouping_2_t3411231652_0_0_0,
	&GenInst_String_t_0_0_0_MemberInfo_t_0_0_0,
	&GenInst_MemberInfo_t_0_0_0_String_t_0_0_0,
	&GenInst_ParameterInfo_t2235474049_0_0_0_Type_t_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2893931855_0_0_0,
	&GenInst_IEquatable_1_t23079572_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_ResolverContractKey_t473801005_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2893931855_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_JsonContract_t1328848902_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t51964386_0_0_0,
	&GenInst_MemberInfo_t_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0,
	&GenInst_Type_t_0_0_0_IEnumerable_1_t3001461559_0_0_0,
	&GenInst_SerializationCallback_t799500859_0_0_0,
	&GenInst_SerializationErrorCallback_t1476028681_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4013503581_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0,
	&GenInst_IEquatable_1_t2521123358_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_TypeNameKey_t2971844791_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4013503581_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0,
	&GenInst_KeyValuePair_2_t1220774118_0_0_0,
	&GenInst_ReadType_t3446921512_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ReadType_t3446921512_0_0_0_KeyValuePair_2_t1220774118_0_0_0,
	&GenInst_Type_t_0_0_0_ReadType_t3446921512_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3451124703_0_0_0,
	&GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0,
	&GenInst_String_t_0_0_0_JsonProperty_t902655177_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_CreatorPropertyContext_t3090945776_0_0_0,
	&GenInst_PropertyPresence_t2892329490_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_String_t_0_0_0,
	&GenInst_CreatorPropertyContext_t3090945776_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_KeyValuePair_2_t666182096_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0_KeyValuePair_2_t666182096_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t595436130_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0,
	&GenInst_ObjectU5BU5D_t1108656482_0_0_0_JsonConverter_t2159686854_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t2363589633_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DataContractAttribute_t2462274566_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DataContractAttribute_t2462274566_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_DataMemberAttribute_t2601848894_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DataMemberAttribute_t2601848894_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Type_t_0_0_0,
	&GenInst_JToken_t3412245951_0_0_0,
	&GenInst_JEnumerable_1_t971832625_0_0_0,
	&GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0,
	&GenInst_KeyValuePair_2_t4131445027_0_0_0,
	&GenInst_String_t_0_0_0_JToken_t3412245951_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_JTokenType_t3916897561_0_0_0,
	&GenInst_JsonToken_t4173078175_0_0_0,
	&GenInst_JValue_t3413677367_0_0_0,
	&GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0,
	&GenInst_Type_t_0_0_0_ReflectionObject_t3124613658_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_RegexOptions_t3066443743_0_0_0,
	&GenInst_BsonProperty_t4293821333_0_0_0,
	&GenInst_BsonToken_t455725415_0_0_0,
	&GenInst_Object_t3071478659_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4128901257_0_0_0,
	&GenInst_IAchievementDescription_t655461400_0_0_0,
	&GenInst_IAchievementU5BU5D_t1953253797_0_0_0,
	&GenInst_IAchievement_t2957812780_0_0_0,
	&GenInst_IScoreU5BU5D_t250104726_0_0_0,
	&GenInst_IScore_t4279057999_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3419104218_0_0_0,
	&GenInst_IUserProfile_t598900827_0_0_0,
	&GenInst_AchievementDescription_t2116066607_0_0_0,
	&GenInst_UserProfile_t2280656072_0_0_0,
	&GenInst_GcLeaderboard_t1820874799_0_0_0,
	&GenInst_GcAchievementData_t3481375915_0_0_0,
	&GenInst_Achievement_t344600729_0_0_0,
	&GenInst_GcScoreData_t2181296590_0_0_0,
	&GenInst_Score_t3396031228_0_0_0,
	&GenInst_Material_t3870600107_0_0_0,
	&GenInst_Color_t4194546905_0_0_0,
	&GenInst_Color32_t598853688_0_0_0,
	&GenInst_Keyframe_t4079056114_0_0_0,
	&GenInst_Vector3_t4282066566_0_0_0,
	&GenInst_Vector4_t4282066567_0_0_0,
	&GenInst_Vector2_t4282066565_0_0_0,
	&GenInst_NetworkPlayer_t3231273765_0_0_0,
	&GenInst_HostData_t3270478838_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t726430633_0_0_0,
	&GenInst_Camera_t2727095145_0_0_0,
	&GenInst_Behaviour_t200106419_0_0_0,
	&GenInst_Component_t3501516275_0_0_0,
	&GenInst_Display_t1321072632_0_0_0,
	&GenInst_Touch_t4210255029_0_0_0,
	&GenInst_GameObject_t3674682005_0_0_0,
	&GenInst_Playable_t70832698_0_0_0,
	&GenInst_Scene_t1080795294_0_0_0_LoadSceneMode_t3067001883_0_0_0,
	&GenInst_Scene_t1080795294_0_0_0,
	&GenInst_Scene_t1080795294_0_0_0_Scene_t1080795294_0_0_0,
	&GenInst_ContactPoint_t243083348_0_0_0,
	&GenInst_RaycastHit_t4003175726_0_0_0,
	&GenInst_Collider_t2939674232_0_0_0,
	&GenInst_Rigidbody2D_t1743771669_0_0_0,
	&GenInst_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_Collider2D_t1552025098_0_0_0,
	&GenInst_ContactPoint2D_t4288432358_0_0_0,
	&GenInst_UIVertex_t4244065212_0_0_0,
	&GenInst_UICharInfo_t65807484_0_0_0,
	&GenInst_UILineInfo_t4113875482_0_0_0,
	&GenInst_Font_t4241557075_0_0_0,
	&GenInst_GUIContent_t2094828418_0_0_0,
	&GenInst_Rect_t4241904616_0_0_0,
	&GenInst_GUILayoutOption_t331591504_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_LayoutCache_t879908455_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t775952400_0_0_0,
	&GenInst_GUILayoutEntry_t1336615025_0_0_0,
	&GenInst_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2990928826_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3710127902_0_0_0,
	&GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t4145961110_0_0_0_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_Event_t4196595728_0_0_0,
	&GenInst_Event_t4196595728_0_0_0_TextEditOp_t4145961110_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1289591971_0_0_0,
	&GenInst_IMultipartFormSection_t2606995300_0_0_0,
	&GenInst_DisallowMultipleComponent_t62111112_0_0_0,
	&GenInst_ExecuteInEditMode_t3132250205_0_0_0,
	&GenInst_RequireComponent_t1687166108_0_0_0,
	&GenInst_HitInfo_t3209134097_0_0_0,
	&GenInst_PersistentCall_t2972625667_0_0_0,
	&GenInst_BaseInvokableCall_t1559630662_0_0_0,
	&GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t419747678_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t595048151_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t520966972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t525170163_0_0_0,
	&GenInst_Point_t1271214244_0_0_0,
	&GenInst_PlayMakerFSM_t3799847376_0_0_0,
	&GenInst_ISerializationCallbackReceiver_t3119329471_0_0_0,
	&GenInst_MonoBehaviour_t667441552_0_0_0,
	&GenInst_FsmFloat_t2134102846_0_0_0,
	&GenInst_NamedVariable_t3211770239_0_0_0,
	&GenInst_INameable_t2730839192_0_0_0,
	&GenInst_INamedVariable_t1024128046_0_0_0,
	&GenInst_FsmInt_t1596138449_0_0_0,
	&GenInst_FsmTransition_t3771611999_0_0_0,
	&GenInst_IEquatable_1_t3320890566_0_0_0,
	&GenInst_ActionReport_t662142796_0_0_0,
	&GenInst_FsmVarOverride_t3235106805_0_0_0,
	&GenInst_Fsm_t1527112426_0_0_0,
	&GenInst_Texture_t2526458961_0_0_0,
	&GenInst_Quaternion_t1553702882_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3582344850_0_0_0,
	&GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0,
	&GenInst_Type_t_0_0_0_FieldInfoU5BU5D_t2567562023_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2571765214_0_0_0,
	&GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Type_t_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1158041691_0_0_0,
	&GenInst_FsmGameObject_t1697147867_0_0_0,
	&GenInst_FsmOwnerDefault_t251897112_0_0_0,
	&GenInst_FsmAnimationCurve_t2685995989_0_0_0,
	&GenInst_FunctionCall_t3279845016_0_0_0,
	&GenInst_FsmTemplateControl_t2786508133_0_0_0,
	&GenInst_FsmEventTarget_t1823904941_0_0_0,
	&GenInst_FsmProperty_t3927159007_0_0_0,
	&GenInst_LayoutOption_t964995201_0_0_0,
	&GenInst_FsmString_t952858651_0_0_0,
	&GenInst_FsmObject_t821476169_0_0_0,
	&GenInst_FsmVar_t1596150537_0_0_0,
	&GenInst_FsmArray_t2129666875_0_0_0,
	&GenInst_FsmEnum_t1076048395_0_0_0,
	&GenInst_FsmBool_t1075959796_0_0_0,
	&GenInst_FsmVector2_t533912881_0_0_0,
	&GenInst_FsmVector3_t533912882_0_0_0,
	&GenInst_FsmColor_t2131419205_0_0_0,
	&GenInst_FsmRect_t1076426478_0_0_0,
	&GenInst_FsmQuaternion_t3871136040_0_0_0,
	&GenInst_ParamDataType_t2672665179_0_0_0,
	&GenInst_FsmStateAction_t2366529033_0_0_0,
	&GenInst_IFsmStateAction_t3269097786_0_0_0,
	&GenInst_DelayedEvent_t1938906778_0_0_0,
	&GenInst_FsmState_t2146334067_0_0_0,
	&GenInst_FsmEvent_t2133468028_0_0_0,
	&GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_KeyValuePair_2_t3443564286_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RaycastHit2D_t1374744384_0_0_0_KeyValuePair_2_t3443564286_0_0_0,
	&GenInst_Fsm_t1527112426_0_0_0_RaycastHit2D_t1374744384_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1397821387_0_0_0,
	&GenInst_FsmLogEntry_t2614866584_0_0_0,
	&GenInst_FsmLog_t1596141350_0_0_0,
	&GenInst_FsmMaterial_t924399665_0_0_0,
	&GenInst_FsmTexture_t3073272573_0_0_0,
	&GenInst_BaseInputModule_t15847059_0_0_0,
	&GenInst_RaycastResult_t3762661364_0_0_0,
	&GenInst_IDeselectHandler_t4282929196_0_0_0,
	&GenInst_IEventSystemHandler_t1130525624_0_0_0,
	&GenInst_List_1_t2498711176_0_0_0,
	&GenInst_List_1_t1244034627_0_0_0,
	&GenInst_List_1_t574734531_0_0_0,
	&GenInst_ISelectHandler_t3580292109_0_0_0,
	&GenInst_BaseRaycaster_t2327671059_0_0_0,
	&GenInst_Entry_t849715470_0_0_0,
	&GenInst_BaseEventData_t2054899105_0_0_0,
	&GenInst_IPointerEnterHandler_t1772318350_0_0_0,
	&GenInst_IPointerExitHandler_t3471580134_0_0_0,
	&GenInst_IPointerDownHandler_t1340699618_0_0_0,
	&GenInst_IPointerUpHandler_t3049278345_0_0_0,
	&GenInst_IPointerClickHandler_t2979908062_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t220416063_0_0_0,
	&GenInst_IBeginDragHandler_t1569653796_0_0_0,
	&GenInst_IDragHandler_t880586197_0_0_0,
	&GenInst_IEndDragHandler_t2789914546_0_0_0,
	&GenInst_IDropHandler_t4146418618_0_0_0,
	&GenInst_IScrollHandler_t3315383580_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3805412741_0_0_0,
	&GenInst_IMoveHandler_t3984942232_0_0_0,
	&GenInst_ISubmitHandler_t2608359793_0_0_0,
	&GenInst_ICancelHandler_t4180011215_0_0_0,
	&GenInst_Transform_t1659122786_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_PointerEventData_t1848751023_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1744794968_0_0_0,
	&GenInst_PointerEventData_t1848751023_0_0_0,
	&GenInst_ButtonState_t2039702646_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_ICanvasElement_t1249239335_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ColorBlock_t508458230_0_0_0,
	&GenInst_OptionData_t185687546_0_0_0,
	&GenInst_DropdownItem_t3215807551_0_0_0,
	&GenInst_FloatTween_t2711705593_0_0_0,
	&GenInst_Sprite_t3199167241_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0,
	&GenInst_List_1_t4095326316_0_0_0,
	&GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0,
	&GenInst_Text_t9039225_0_0_0,
	&GenInst_Font_t4241557075_0_0_0_List_1_t1377224777_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1496360359_0_0_0,
	&GenInst_ColorTween_t723277650_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Graphic_t836799438_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Canvas_t2727140764_0_0_0_IndexedSet_1_t3656898711_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3226014568_0_0_0,
	&GenInst_KeyValuePair_2_t1354567995_0_0_0,
	&GenInst_KeyValuePair_2_t3983107678_0_0_0,
	&GenInst_ContentType_t2662964855_0_0_0,
	&GenInst_Mask_t8826680_0_0_0,
	&GenInst_List_1_t1377012232_0_0_0,
	&GenInst_RectMask2D_t3357079374_0_0_0,
	&GenInst_List_1_t430297630_0_0_0,
	&GenInst_Navigation_t1108456480_0_0_0,
	&GenInst_IClippable_t502791197_0_0_0,
	&GenInst_Selectable_t1885181538_0_0_0,
	&GenInst_SpriteState_t2895308594_0_0_0,
	&GenInst_CanvasGroup_t3702418109_0_0_0,
	&GenInst_MatEntry_t1574154081_0_0_0,
	&GenInst_Toggle_t110812896_0_0_0,
	&GenInst_Toggle_t110812896_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_IClipper_t1175935472_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1329917009_0_0_0,
	&GenInst_RectTransform_t972643934_0_0_0,
	&GenInst_LayoutRebuilder_t1942933988_0_0_0,
	&GenInst_ILayoutElement_t1646037781_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_List_1_t1355284822_0_0_0,
	&GenInst_List_1_t1967039240_0_0_0,
	&GenInst_List_1_t1355284821_0_0_0,
	&GenInst_List_1_t1355284823_0_0_0,
	&GenInst_List_1_t2522024052_0_0_0,
	&GenInst_List_1_t1317283468_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_VideoInfo_t2099471191_0_0_0,
	&GenInst_ExtractionInfo_t925438340_0_0_0,
	&GenInst_VideoInfo_t2099471191_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Action_t3771233898_0_0_0,
	&GenInst_CommonStatusCodes_t671203459_0_0_0_String_t_0_0_0,
	&GenInst_CommonStatusCodes_t671203459_0_0_0_Il2CppObject_0_0_0,
	&GenInst_CommonStatusCodes_t671203459_0_0_0_PlayerStats_t60064856_0_0_0,
	&GenInst_AchievementU5BU5D_t3251685236_0_0_0,
	&GenInst_Achievement_t1261647177_0_0_0,
	&GenInst_UIStatus_t427705392_0_0_0,
	&GenInst_LeaderboardScoreData_t4006482697_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_List_1_t2555968713_0_0_0,
	&GenInst_IEvent_t1187783161_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_IEvent_t1187783161_0_0_0,
	&GenInst_PlayGamesScore_t486124539_0_0_0,
	&GenInst_InvitationU5BU5D_t4211826234_0_0_0,
	&GenInst_Invitation_t2200833403_0_0_0,
	&GenInst_Participant_t1804230813_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_TurnBasedMatch_t3573041681_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UIStatus_t427705392_0_0_0_TurnBasedMatch_t3573041681_0_0_0,
	&GenInst_UIStatus_t427705392_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TurnBasedMatchU5BU5D_t2145394316_0_0_0,
	&GenInst_TurnBasedMatch_t3573041681_0_0_0,
	&GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0,
	&GenInst_KeyValuePair_2_t2093487883_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt32_t24667981_0_0_0_KeyValuePair_2_t2093487883_0_0_0,
	&GenInst_String_t_0_0_0_UInt32_t24667981_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t743867057_0_0_0,
	&GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0,
	&GenInst_KeyValuePair_2_t101070088_0_0_0,
	&GenInst_ParticipantResult_t2327217482_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ParticipantResult_t2327217482_0_0_0_KeyValuePair_2_t101070088_0_0_0,
	&GenInst_String_t_0_0_0_ParticipantResult_t2327217482_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t3046416558_0_0_0,
	&GenInst_Participant_t1804230813_0_0_0_String_t_0_0_0,
	&GenInst_AdvertisingResult_t68742423_0_0_0,
	&GenInst_ConnectionRequest_t4159799943_0_0_0,
	&GenInst_ConnectionResponse_t4280027939_0_0_0,
	&GenInst_InitializationStatus_t518463702_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_IQuest_t562088433_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_List_1_t1930273985_0_0_0,
	&GenInst_IQuest_t562088433_0_0_0,
	&GenInst_QuestUiResult_t3187252993_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0,
	&GenInst_QuestUiResult_t3187252993_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_QuestAcceptStatus_t3857037258_0_0_0_IQuest_t562088433_0_0_0,
	&GenInst_QuestAcceptStatus_t3857037258_0_0_0_Il2CppObject_0_0_0,
	&GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_IQuest_t562088433_0_0_0_IQuestMilestone_t3485030629_0_0_0,
	&GenInst_QuestClaimMilestoneStatus_t935666006_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ISavedGameMetadata_t3582269991_0_0_0,
	&GenInst_SavedGameRequestStatus_t3786215536_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SavedGameRequestStatus_t3786215536_0_0_0_ByteU5BU5D_t4260760469_0_0_0,
	&GenInst_SelectUIStatus_t4210182474_0_0_0_ISavedGameMetadata_t3582269991_0_0_0,
	&GenInst_SelectUIStatus_t4210182474_0_0_0_Il2CppObject_0_0_0,
	&GenInst_SavedGameRequestStatus_t3786215536_0_0_0_List_1_t655488247_0_0_0,
	&GenInst_ISavedGameMetadata_t3582269991_0_0_0,
	&GenInst_INearbyConnectionClient_t1732200103_0_0_0,
	&GenInst_Action_1_t872614854_0_0_0,
	&GenInst_Invitation_t2200833403_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0,
	&GenInst_String_t_0_0_0_Achievement_t1261647177_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t1980846253_0_0_0,
	&GenInst_Player_t3727527619_0_0_0,
	&GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0,
	&GenInst_MultiplayerEvent_t2613789975_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_MultiplayerEvent_t2613789975_0_0_0_String_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0,
	&GenInst_FetchServerAuthCodeResponse_t1052060023_0_0_0,
	&GenInst_ResponseStatus_t419677757_0_0_0_List_1_t800745875_0_0_0,
	&GenInst_NativeAchievement_t2621183934_0_0_0,
	&GenInst_FetchAllResponse_t2805709750_0_0_0,
	&GenInst_FetchSelfResponse_t1087840609_0_0_0,
	&GenInst_FetchForPlayerResponse_t1267220047_0_0_0,
	&GenInst_NativePlayer_t2636885988_0_0_0,
	&GenInst_BaseReferenceHolder_t2237584300_0_0_0,
	&GenInst_IDisposable_t1423340799_0_0_0,
	&GenInst_NativePlayerU5BU5D_t3461726221_0_0_0,
	&GenInst_FetchResponse_t2513188365_0_0_0,
	&GenInst_UIStatus_t3557407943_0_0_0,
	&GenInst_FetchAllResponse_t1890238433_0_0_0,
	&GenInst_FetchResponse_t3476068802_0_0_0,
	&GenInst_NativeEvent_t2485247913_0_0_0,
	&GenInst_FetchResponse_t3559403642_0_0_0,
	&GenInst_FetchListResponse_t32965304_0_0_0,
	&GenInst_QuestUIResponse_t3819980022_0_0_0,
	&GenInst_AcceptResponse_t3485599210_0_0_0,
	&GenInst_ClaimMilestoneResponse_t683149430_0_0_0,
	&GenInst_NativeQuest_t2496300529_0_0_0,
	&GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_ByteU5BU5D_t4260760469_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_NativeRealTimeRoom_t3104490121_0_0_0_MultiplayerParticipant_t3337232325_0_0_0,
	&GenInst_NativeRealTimeRoom_t3104490121_0_0_0,
	&GenInst_PlayerSelectUIResponse_t922401854_0_0_0,
	&GenInst_FetchInvitationsResponse_t815788017_0_0_0,
	&GenInst_RoomInboxUIResponse_t1117529840_0_0_0,
	&GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0,
	&GenInst_String_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t4056431401_0_0_0,
	&GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0,
	&GenInst_String_t_0_0_0_Participant_t1804230813_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2523429889_0_0_0,
	&GenInst_MultiplayerParticipant_t3337232325_0_0_0_String_t_0_0_0,
	&GenInst_MultiplayerParticipant_t3337232325_0_0_0_Participant_t1804230813_0_0_0,
	&GenInst_Participant_t1804230813_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_MultiplayerParticipant_t3337232325_0_0_0,
	&GenInst_MultiplayerStatus_t2675372491_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0,
	&GenInst_Link_t1089569710_0_0_0,
	&GenInst_WaitingRoomUIResponse_t3333463309_0_0_0,
	&GenInst_ResponseStatus_t4049911828_0_0_0,
	&GenInst_RealTimeRoomResponse_t2530940439_0_0_0,
	&GenInst_MultiplayerInvitation_t3411188537_0_0_0,
	&GenInst_OpenResponse_t1933021492_0_0_0,
	&GenInst_ReadResponse_t2292627584_0_0_0,
	&GenInst_SnapshotSelectUIResponse_t2500674014_0_0_0,
	&GenInst_CommitResponse_t2801162465_0_0_0,
	&GenInst_FetchAllResponse_t1240414705_0_0_0,
	&GenInst_ByteU5BU5D_t4260760469_0_0_0_ByteU5BU5D_t4260760469_0_0_0,
	&GenInst_NativeSnapshotMetadata_t3479575958_0_0_0,
	&GenInst_TurnBasedMatch_t3573041681_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_TurnBasedMatchResponse_t4255652325_0_0_0,
	&GenInst_TurnBasedMatchesResponse_t3460122515_0_0_0,
	&GenInst_MatchInboxUIResponse_t1459363083_0_0_0,
	&GenInst_MultiplayerParticipant_t3337232325_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0,
	&GenInst_NativeTurnBasedMatch_t302853426_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchAllResponse_t2805709750_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchResponse_t2513188365_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativeAchievement_t2621183934_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t300380471_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_HandleRef_t1780819301_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t300380471_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_BaseReferenceHolder_t2237584300_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t2662115696_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1890238433_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchResponse_t3476068802_0_0_0,
	&GenInst_IntPtr_t_0_0_0_NativeEvent_t2485247913_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchServerAuthCodeResponse_t1052060023_0_0_0,
	&GenInst_FetchResponse_t642400449_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchResponse_t642400449_0_0_0,
	&GenInst_FetchScoreSummaryResponse_t877231893_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchScoreSummaryResponse_t877231893_0_0_0,
	&GenInst_FetchScorePageResponse_t1467305556_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchScorePageResponse_t1467305556_0_0_0,
	&GenInst_NativeScoreEntry_t3997924611_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0,
	&GenInst_KeyValuePair_2_t3076810564_0_0_0,
	&GenInst_ParticipantStatus_t4028684685_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t3137786926_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t4028684685_0_0_0_KeyValuePair_2_t3076810564_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_MultiplayerParticipant_t3337232325_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativeScoreEntry_t3997924611_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchSelfResponse_t1087840609_0_0_0,
	&GenInst_FetchResponse_t3310613493_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchResponse_t3310613493_0_0_0,
	&GenInst_FetchListResponse_t676237491_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchListResponse_t676237491_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativePlayer_t2636885988_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_String_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchResponse_t3559403642_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchListResponse_t32965304_0_0_0,
	&GenInst_IntPtr_t_0_0_0_QuestUIResponse_t3819980022_0_0_0,
	&GenInst_IntPtr_t_0_0_0_AcceptResponse_t3485599210_0_0_0,
	&GenInst_IntPtr_t_0_0_0_ClaimMilestoneResponse_t683149430_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativeQuest_t2496300529_0_0_0,
	&GenInst_MultiplayerParticipant_t3337232325_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_PlayerSelectUIResponse_t922401854_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RoomInboxUIResponse_t1117529840_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WaitingRoomUIResponse_t3333463309_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchInvitationsResponse_t815788017_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RealTimeRoomResponse_t2530940439_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_MultiplayerInvitation_t3411188537_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchAllResponse_t1240414705_0_0_0,
	&GenInst_IntPtr_t_0_0_0_SnapshotSelectUIResponse_t2500674014_0_0_0,
	&GenInst_IntPtr_t_0_0_0_OpenResponse_t1933021492_0_0_0,
	&GenInst_IntPtr_t_0_0_0_CommitResponse_t2801162465_0_0_0,
	&GenInst_IntPtr_t_0_0_0_ReadResponse_t2292627584_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativeSnapshotMetadata_t3479575958_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FetchForPlayerResponse_t1267220047_0_0_0,
	&GenInst_IntPtr_t_0_0_0_TurnBasedMatchesResponse_t3460122515_0_0_0,
	&GenInst_IntPtr_t_0_0_0_MatchInboxUIResponse_t1459363083_0_0_0,
	&GenInst_IntPtr_t_0_0_0_TurnBasedMatchResponse_t4255652325_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_NativeTurnBasedMatch_t302853426_0_0_0,
	&GenInst_String_t_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t4291918972_0_0_0_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_String_t_0_0_0_Single_t4291918972_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t716150752_0_0_0,
	&GenInst_ParticleSystem_t381473177_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_List_1_t747900261_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_KeyValuePair_2_t643944206_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Int32_t1153838500_0_0_0_Int32_t1153838500_0_0_0_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_CFX_AutoDestructShuriken_t107909964_0_0_0,
	&GenInst_CFX_LightIntensityFade_t2919443491_0_0_0,
	&GenInst_Rigidbody_t3346577219_0_0_0,
	&GenInst_Animation_t1724966010_0_0_0,
	&GenInst_AnimationCurve_t3667593487_0_0_0,
	&GenInst_Calculation_t2191327052_0_0_0,
	&GenInst_Calculation_t2771812670_0_0_0,
	&GenInst_Renderer_t3076687687_0_0_0,
	&GenInst_NetworkView_t3656680617_0_0_0,
	&GenInst_AudioClip_t794140988_0_0_0,
	&GenInst_AudioSource_t1740077639_0_0_0,
	&GenInst_GUIText_t3371372606_0_0_0,
	&GenInst_GUITexture_t4020448292_0_0_0,
	&GenInst_Light_t4202674828_0_0_0,
	&GenInst_GPGSMng_t946704145_0_0_0,
	&GenInst_NetworkMng_t1515215352_0_0_0,
	&GenInst_GameStepManager_t2511743951_0_0_0,
	&GenInst_MainUserInfo_t2526075922_0_0_0,
	&GenInst_ConstforMinigame_t2789090703_0_0_0,
	&GenInst_CalculationData_t2213441011_0_0_0,
	&GenInst_ResultData_t1421128071_0_0_0,
	&GenInst_ResultAllData_t3272948206_0_0_0,
	&GenInst_ResultSaveData_t3126241828_0_0_0,
	&GenInst_Int32U5BU5D_t3230847821_0_0_0,
	&GenInst_GradeData_t483491841_0_0_0,
	&GenInst_UserExperience_t3961908149_0_0_0,
	&GenInst_StructforMinigame_t1286533533_0_0_0,
	&GenInst_Image_t538875265_0_0_0,
	&GenInst_ICanvasRaycastFilter_t1511988644_0_0_0,
	&GenInst_ILayoutElement_t1646037781_0_0_0,
	&GenInst_MaskableGraphic_t3186046376_0_0_0,
	&GenInst_IMaskable_t1719715701_0_0_0,
	&GenInst_IMaterialModifier_t2120790093_0_0_0,
	&GenInst_UIBehaviour_t2511441271_0_0_0,
	&GenInst_Button_t3896396478_0_0_0,
	&GenInst_IEnumerable_1_t309087608_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0,
	&GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0,
	&GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0,
	&GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0,
	&GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3566161319_gp_0_0_0_0,
	&GenInst_Array_Sort_m1767877396_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0,
	&GenInst_Array_compare_m3718693973_gp_0_0_0_0,
	&GenInst_Array_qsort_m3270161954_gp_0_0_0_0,
	&GenInst_Array_Resize_m2347367271_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1326446122_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m308210168_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1181152586_gp_0_0_0_0,
	&GenInst_Array_Exists_m2312185345_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0,
	&GenInst_Array_Find_m3878993575_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3671273393_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t813279015_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2902282453_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t704486949_gp_0_0_0_0,
	&GenInst_IList_1_t2706020430_gp_0_0_0_0,
	&GenInst_ICollection_1_t1952910030_gp_0_0_0_0,
	&GenInst_Nullable_1_t1122404262_gp_0_0_0_0,
	&GenInst_Comparer_1_t1701613202_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3219634540_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2982791755_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_ShimEnumerator_t3290534805_gp_0_0_0_0_ShimEnumerator_t3290534805_gp_1_0_0_0,
	&GenInst_Enumerator_t1176480892_gp_0_0_0_0_Enumerator_t1176480892_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1912719906_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0_Enumerator_t4049866126_gp_1_0_0_0,
	&GenInst_Enumerator_t4049866126_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_1_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_KeyCollection_t2018332325_gp_0_0_0_0_KeyCollection_t2018332325_gp_0_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_0_0_0_0_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_Enumerator_t2802076604_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_0_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_ValueCollection_t3097895223_gp_1_0_0_0_ValueCollection_t3097895223_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_DictionaryEntry_t1751606614_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_0_0_0_0_Dictionary_2_t898371836_gp_1_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_KeyValuePair_2_t4022413346_0_0_0_KeyValuePair_2_t4022413346_0_0_0,
	&GenInst_Dictionary_2_t898371836_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2934485036_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4247873286_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t961487589_gp_0_0_0_0,
	&GenInst_IDictionary_2_t435039943_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2536192150_0_0_0,
	&GenInst_IDictionary_2_t435039943_gp_0_0_0_0_IDictionary_2_t435039943_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t911337330_gp_0_0_0_0_KeyValuePair_2_t911337330_gp_1_0_0_0,
	&GenInst_List_1_t951551555_gp_0_0_0_0,
	&GenInst_Enumerator_t2095969493_gp_0_0_0_0,
	&GenInst_Collection_1_t3473121937_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t363916344_gp_1_0_0_0,
	&GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0,
	&GenInst_KeyedCollection_2_t363916344_gp_0_0_0_0_KeyedCollection_2_t363916344_gp_1_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4004629011_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t2757088543_gp_0_0_0_0,
	&GenInst_LinkedList_1_t2631682684_gp_0_0_0_0,
	&GenInst_Enumerator_t2473886460_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t603318558_gp_0_0_0_0,
	&GenInst_Queue_1_t2386261560_gp_0_0_0_0,
	&GenInst_Enumerator_t3723618752_gp_0_0_0_0,
	&GenInst_Stack_1_t4128415215_gp_0_0_0_0,
	&GenInst_Enumerator_t2313787817_gp_0_0_0_0,
	&GenInst_HashSet_1_t3763949723_gp_0_0_0_0,
	&GenInst_Enumerator_t1211810685_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3144359540_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m131196905_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m131196905_gp_1_0_0_0_Enumerable_Aggregate_m131196905_gp_0_0_0_0_Enumerable_Aggregate_m131196905_gp_1_0_0_0,
	&GenInst_Enumerable_All_m3316688406_gp_0_0_0_0,
	&GenInst_Enumerable_All_m3316688406_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m3995152903_gp_0_0_0_0,
	&GenInst_Enumerable_Empty_m1047782786_gp_0_0_0_0,
	&GenInst_Enumerable_Except_m3558828234_gp_0_0_0_0,
	&GenInst_Enumerable_Except_m3897958717_gp_0_0_0_0,
	&GenInst_Enumerable_CreateExceptIterator_m3598482259_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1340241468_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m715082200_gp_0_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0_List_1_t3315066695_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1233200021_gp_1_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1233200021_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1149957246_gp_0_0_0_0_Enumerable_GroupBy_m1149957246_gp_1_0_0_0,
	&GenInst_IGrouping_2_t2748065910_0_0_0,
	&GenInst_Enumerable_GroupBy_m1149957246_gp_1_0_0_0_Enumerable_GroupBy_m1149957246_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1306123839_gp_0_0_0_0_Enumerable_GroupBy_m1306123839_gp_1_0_0_0,
	&GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0,
	&GenInst_IGrouping_2_t2748065911_0_0_0,
	&GenInst_Enumerable_GroupBy_m1306123839_gp_1_0_0_0_Enumerable_GroupBy_m1306123839_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0,
	&GenInst_IGrouping_2_t2933082222_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m3203422129_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m3203422129_gp_0_0_0_0,
	&GenInst_Enumerable_Last_m342737789_gp_0_0_0_0,
	&GenInst_Enumerable_Last_m342737789_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0,
	&GenInst_Enumerable_LastOrDefault_m2352343999_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_LongCount_m2029959972_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m593931412_gp_0_0_0_0_Enumerable_OrderBy_m593931412_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m3650685123_gp_0_0_0_0_Enumerable_OrderBy_m3650685123_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m3650685123_gp_1_0_0_0,
	&GenInst_Enumerable_Reverse_m3244172289_gp_0_0_0_0,
	&GenInst_Enumerable_CreateReverseIterator_m2569088847_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_0_0_0_0_Enumerable_Select_m2198038980_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2198038980_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_0_0_0_0_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m2898637550_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_IEnumerable_1_t2457202127_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m3064021096_gp_0_0_0_0_Enumerable_SelectMany_m3064021096_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_IEnumerable_1_t402820473_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m2276988946_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m2276988946_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m2414118603_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_Single_m2162276958_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m2622327798_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m4016654385_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_Skip_m832324711_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSkipIterator_m3222360977_gp_0_0_0_0,
	&GenInst_Enumerable_Take_m3047183215_gp_0_0_0_0,
	&GenInst_Enumerable_CreateTakeIterator_m2763643545_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m4019664173_gp_0_0_0_0_Enumerable_ToDictionary_m4019664173_gp_1_0_0_0_Enumerable_ToDictionary_m4019664173_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m717303024_gp_0_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m717303024_gp_1_0_0_0_Enumerable_ToDictionary_m717303024_gp_2_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2993041198_gp_1_0_0_0_Enumerable_ToDictionary_m2993041198_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0,
	&GenInst_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0_Enumerable_ToDictionary_m2155213199_gp_1_0_0_0_Enumerable_ToDictionary_m2155213199_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0,
	&GenInst_Enumerable_SequenceEqual_m3902804285_gp_0_0_0_0,
	&GenInst_Enumerable_SequenceEqual_m2218710192_gp_0_0_0_0,
	&GenInst_Enumerable_Union_m3162669992_gp_0_0_0_0,
	&GenInst_Enumerable_Union_m3942844955_gp_0_0_0_0,
	&GenInst_Enumerable_CreateUnionIterator_m970815465_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m4110359_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m4110359_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1402094821_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Function_1_t3519260161_gp_0_0_0_0_Function_1_t3519260161_gp_0_0_0_0,
	&GenInst_Function_1_t3519260161_gp_0_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t2047936865_gp_0_0_0_0,
	&GenInst_U3CCreateExceptIteratorU3Ec__Iterator4_1_t4187679051_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0,
	&GenInst_IGrouping_2_t2205907554_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0_List_1_t2704670177_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t688852176_gp_1_0_0_0,
	&GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3101979508_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2442374030_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_IEnumerable_1_t4053127014_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t2732974735_gp_1_0_0_0,
	&GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t4010584470_gp_0_0_0_0,
	&GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t4166151137_gp_0_0_0_0,
	&GenInst_U3CCreateUnionIteratorU3Ec__Iterator1C_1_t2618524979_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3294766860_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Grouping_2_t3051269225_gp_1_0_0_0,
	&GenInst_Grouping_2_t3051269225_gp_0_0_0_0_Grouping_2_t3051269225_gp_1_0_0_0,
	&GenInst_IGrouping_2_t403732852_gp_1_0_0_0,
	&GenInst_IOrderedEnumerable_1_t2451113233_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t3194770876_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1971240694_gp_0_0_0_0_OrderedSequence_2_t1971240694_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1971240694_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1296963154_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t4263572490_gp_0_0_0_0,
	&GenInst_SortContext_1_t869602712_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t2890572760_gp_0_0_0_0_SortSequenceContext_2_t2890572760_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t2890572760_gp_1_0_0_0,
	&GenInst_JsonConvert_DeserializeObject_m2706976093_gp_0_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0,
	&GenInst_BidirectionalDictionary_2_t2548264713_gp_1_0_0_0_BidirectionalDictionary_2_t2548264713_gp_0_0_0_0,
	&GenInst_CollectionUtils_IsNullOrEmpty_m2177901390_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddRange_m20899124_gp_0_0_0_0,
	&GenInst_CollectionUtils_AddRange_m1321937586_gp_0_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m168261572_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_CollectionUtils_IndexOf_m2380710873_gp_0_0_0_0,
	&GenInst_CollectionWrapper_1_t338490148_gp_0_0_0_0,
	&GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0,
	&GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1302665278_0_0_0,
	&GenInst_KeyValuePair_2_t2795671894_0_0_0,
	&GenInst_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0,
	&GenInst_DictionaryEnumerator_2_t3334679397_gp_0_0_0_0_DictionaryEnumerator_2_t3334679397_gp_1_0_0_0_DictionaryEnumerator_2_t3334679397_gp_2_0_0_0_DictionaryEnumerator_2_t3334679397_gp_3_0_0_0,
	&GenInst_U3CU3Ec_t3532683464_gp_0_0_0_0_U3CU3Ec_t3532683464_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1450275698_0_0_0,
	&GenInst_DictionaryEntry_t1751606614_0_0_0_KeyValuePair_2_t1302665278_0_0_0,
	&GenInst_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0_DictionaryWrapper_2_t3825985581_gp_0_0_0_0_DictionaryWrapper_2_t3825985581_gp_1_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateMethodCall_m1948748551_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateDefaultConstructor_m3943901130_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m1426186952_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateGet_m3090937151_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3714244965_gp_0_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LateBoundReflectionDelegateFactory_CreateSet_m3245653218_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m3733510623_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m774072733_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateMethodCall_m2995835713_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateDefaultConstructor_m2491915520_gp_0_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m149881860_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateGet_m2849553795_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m3169730053_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionDelegateFactory_CreateSet_m3693386818_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReflectionUtils_GetAttribute_m3758501180_gp_0_0_0_0,
	&GenInst_ReflectionUtils_GetAttribute_m3769272001_gp_0_0_0_0,
	&GenInst_ReflectionUtils_GetAttributes_m1614686852_gp_0_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_String_t_0_0_0,
	&GenInst_StringUtils_ForgivingCaseSensitiveFind_m3321324581_gp_0_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_U3CU3Ec__DisplayClass15_0_1_t128553205_gp_0_0_0_0_String_t_0_0_0,
	&GenInst_ThreadSafeStore_2_t2768378_gp_0_0_0_0_ThreadSafeStore_2_t2768378_gp_1_0_0_0,
	&GenInst_Il2CppObject_0_0_0_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0,
	&GenInst_CachedAttributeGetter_1_t3812731516_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1444726174_0_0_0,
	&GenInst_DictionaryEnumerator_2_t2588053397_gp_0_0_0_0_DictionaryEnumerator_2_t2588053397_gp_1_0_0_0,
	&GenInst_JsonTypeReflector_GetCachedAttribute_m344225504_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m3379908931_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m2873653962_gp_0_0_0_0,
	&GenInst_JsonTypeReflector_GetAttribute_m290703582_gp_0_0_0_0,
	&GenInst_IJEnumerable_1_t1879559754_gp_0_0_0_0,
	&GenInst_JEnumerable_1_t2167239905_gp_0_0_0_0,
	&GenInst_JEnumerable_1_t3910855904_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m4164974250_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m2907158863_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m2393171610_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m2565332604_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m2317656012_gp_0_0_0_0,
	&GenInst_Resources_LoadAll_m3849581656_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m482375811_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m311092670_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m4155248588_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2457787036_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2255317291_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3431730600_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0_InvokableCall_2_t3431730601_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3431730601_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0_InvokableCall_3_t3431730602_gp_1_0_0_0_InvokableCall_3_t3431730602_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3431730602_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0_InvokableCall_4_t3431730603_gp_1_0_0_0_InvokableCall_4_t3431730603_gp_2_0_0_0_InvokableCall_4_t3431730603_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3431730603_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1222316710_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t1176538020_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t1176538021_gp_0_0_0_0_UnityEvent_2_t1176538021_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t1176538022_gp_0_0_0_0_UnityEvent_3_t1176538022_gp_1_0_0_0_UnityEvent_3_t1176538022_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t1176538023_gp_0_0_0_0_UnityEvent_4_t1176538023_gp_1_0_0_0_UnityEvent_4_t1176538023_gp_2_0_0_0_UnityEvent_4_t1176538023_gp_3_0_0_0,
	&GenInst_Arrays_1_t2739302885_gp_0_0_0_0,
	&GenInst_Lists_1_t4025286752_gp_0_0_0_0,
	&GenInst_PlayMakerFSM_AddEventHandlerComponent_m512374721_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m825525191_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3852335751_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m2829165969_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m299170357_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3823219844_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2054808038_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m3921383492_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m1789762351_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t1486860772_gp_0_0_0_0_Int32_t1153838500_0_0_0,
	&GenInst_ListPool_1_t3815171383_gp_0_0_0_0,
	&GenInst_List_1_t1536022088_0_0_0,
	&GenInst_ObjectPool_1_t3981551192_gp_0_0_0_0,
	&GenInst_CallbackUtils_ToOnGameThread_m1913623339_gp_0_0_0_0,
	&GenInst_CallbackUtils_ToOnGameThread_m2630662047_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m2630662047_gp_1_0_0_0,
	&GenInst_CallbackUtils_ToOnGameThread_m1838473937_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_1_0_0_0_CallbackUtils_ToOnGameThread_m1838473937_gp_2_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1589933054_gp_0_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey3F_1_t1979984398_gp_0_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey40_2_t1589942665_gp_1_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey41_2_t2708165220_gp_1_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey42_3_t1589944588_gp_2_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey43_3_t798212266_gp_2_0_0_0,
	&GenInst_NativeClient_AsOnGameThreadCallback_m471581517_gp_0_0_0_0,
	&GenInst_NativeClient_InvokeCallbackOnGameThread_m892835528_gp_0_0_0_0,
	&GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t1266059800_gp_0_0_0_0,
	&GenInst_U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t1756710687_gp_0_0_0_0,
	&GenInst_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_0_0_0_0_NativeSavedGameClient_ToOnGameThread_m2667450464_gp_1_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey85_2_t179751499_gp_1_0_0_0,
	&GenInst_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey86_2_t1695856359_gp_1_0_0_0,
	&GenInst_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m2697261956_gp_0_0_0_0,
	&GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m1279430139_gp_1_0_0_0,
	&GenInst_Callbacks_ToIntPtr_m1279430139_gp_0_0_0_0_IntPtr_t_0_0_0,
	&GenInst_Callbacks_IntPtrToTempCallback_m708226062_gp_0_0_0_0,
	&GenInst_Callbacks_IntPtrToPermanentCallback_m1987546508_gp_0_0_0_0,
	&GenInst_Action_2_t3477941569_0_0_0,
	&GenInst_Callbacks_PerformInternalCallback_m869709683_gp_0_0_0_0_IntPtr_t_0_0_0,
	&GenInst_Callbacks_AsOnGameThreadCallback_m1878442403_gp_0_0_0_0,
	&GenInst_Callbacks_AsOnGameThreadCallback_m1043776279_gp_0_0_0_0_Callbacks_AsOnGameThreadCallback_m1043776279_gp_1_0_0_0,
	&GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0,
	&GenInst_U3CToIntPtrU3Ec__AnonStorey9B_1_t2167181365_gp_0_0_0_0,
	&GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0,
	&GenInst_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_0_0_0_0_U3CToIntPtrU3Ec__AnonStorey9C_2_t2167182327_gp_1_0_0_0,
	&GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9D_1_t834939247_gp_0_0_0_0,
	&GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9E_1_t3488135508_gp_0_0_0_0,
	&GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey9F_2_t834941170_gp_1_0_0_0,
	&GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t3933143355_gp_1_0_0_0,
	&GenInst_PInvokeUtilities_OutParamsToArray_m2927585307_gp_0_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0,
	&GenInst_PInvokeUtilities_ToEnumerable_m1093742942_gp_0_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0,
	&GenInst_PInvokeUtilities_ToEnumerator_m3636183006_gp_0_0_0_0,
	&GenInst_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0,
	&GenInst_UIntPtr_t_0_0_0_U3CToEnumerableU3Ec__Iterator5_1_t1420204030_gp_0_0_0_0,
	&GenInst_ComponentAction_1_t2706192626_gp_0_0_0_0,
	&GenInst_Singleton_1_t4250359036_gp_0_0_0_0,
	&GenInst_LineInfoAnnotation_t3303106648_0_0_0,
	&GenInst_JsonPropertyAttribute_t3435603053_0_0_0,
	&GenInst_JsonRequiredAttribute_t224246243_0_0_0,
	&GenInst_JsonObjectAttribute_t863918851_0_0_0,
	&GenInst_Type_t_0_0_0_MemberInfo_t_0_0_0,
	&GenInst_JsonExtensionDataAttribute_t3618022163_0_0_0,
	&GenInst_JsonContainerAttribute_t1917602971_0_0_0,
	&GenInst_JsonIgnoreAttribute_t4056167888_0_0_0,
	&GenInst_NonSerializedAttribute_t1507371935_0_0_0,
	&GenInst_DefaultValueAttribute_t3756983154_0_0_0,
	&GenInst_JsonProperty_t902655177_0_0_0_JsonProperty_t902655177_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_SerializableAttribute_t2632961395_0_0_0,
	&GenInst_JsonConverterAttribute_t1044341340_0_0_0,
	&GenInst_GUILayer_t2983897946_0_0_0,
	&GenInst_PlayMakerMouseEvents_t316289710_0_0_0,
	&GenInst_PlayMakerCollisionEnter_t4046453814_0_0_0,
	&GenInst_PlayMakerCollisionExit_t3871318016_0_0_0,
	&GenInst_PlayMakerCollisionStay_t3871731003_0_0_0,
	&GenInst_PlayMakerTriggerEnter_t977448304_0_0_0,
	&GenInst_PlayMakerTriggerExit_t3495223174_0_0_0,
	&GenInst_PlayMakerTriggerStay_t3495636161_0_0_0,
	&GenInst_PlayMakerCollisionEnter2D_t1696713992_0_0_0,
	&GenInst_PlayMakerCollisionExit2D_t894936658_0_0_0,
	&GenInst_PlayMakerCollisionStay2D_t1291817165_0_0_0,
	&GenInst_PlayMakerTriggerEnter2D_t3024951234_0_0_0,
	&GenInst_PlayMakerTriggerExit2D_t245046360_0_0_0,
	&GenInst_PlayMakerTriggerStay2D_t641926867_0_0_0,
	&GenInst_PlayMakerParticleCollision_t2428451804_0_0_0,
	&GenInst_PlayMakerControllerColliderHit_t1717485139_0_0_0,
	&GenInst_PlayMakerJointBreak_t197925893_0_0_0,
	&GenInst_PlayMakerFixedUpdate_t997170669_0_0_0,
	&GenInst_PlayMakerOnGUI_t940239724_0_0_0,
	&GenInst_PlayMakerApplicationEvents_t1615127321_0_0_0,
	&GenInst_PlayMakerAnimatorMove_t1973299400_0_0_0,
	&GenInst_PlayMakerAnimatorIK_t2388642745_0_0_0,
	&GenInst_PlayMakerGlobals_t3097244096_0_0_0,
	&GenInst_PlayMakerGUI_t3799848395_0_0_0,
	&GenInst_PlayMakerPrefs_t941311808_0_0_0,
	&GenInst_EventSystem_t2276120119_0_0_0,
	&GenInst_AxisEventData_t3355659985_0_0_0,
	&GenInst_SpriteRenderer_t2548470764_0_0_0,
	&GenInst_AspectMode_t2149445162_0_0_0,
	&GenInst_FitMode_t909765868_0_0_0,
	&GenInst_RawImage_t821930207_0_0_0,
	&GenInst_Slider_t79469677_0_0_0,
	&GenInst_Scrollbar_t2601556940_0_0_0,
	&GenInst_InputField_t609046876_0_0_0,
	&GenInst_ScrollRect_t3606982749_0_0_0,
	&GenInst_Dropdown_t4201779933_0_0_0,
	&GenInst_GraphicRaycaster_t911782554_0_0_0,
	&GenInst_CanvasRenderer_t3950887807_0_0_0,
	&GenInst_Corner_t284493240_0_0_0,
	&GenInst_Axis_t1399125956_0_0_0,
	&GenInst_Constraint_t1640775616_0_0_0,
	&GenInst_Type_t3063828369_0_0_0,
	&GenInst_FillMethod_t2255824731_0_0_0,
	&GenInst_SubmitEvent_t3081690246_0_0_0,
	&GenInst_OnChangeEvent_t2697516943_0_0_0,
	&GenInst_OnValidateInput_t3952708057_0_0_0,
	&GenInst_LineType_t2016592042_0_0_0,
	&GenInst_InputType_t1602890312_0_0_0,
	&GenInst_TouchScreenKeyboardType_t2604324130_0_0_0,
	&GenInst_CharacterValidation_t737650598_0_0_0,
	&GenInst_LayoutElement_t1596995480_0_0_0,
	&GenInst_RectOffset_t3056157787_0_0_0,
	&GenInst_TextAnchor_t213922566_0_0_0,
	&GenInst_Direction_t522766867_0_0_0,
	&GenInst_Transition_t1922345195_0_0_0,
	&GenInst_AnimationTriggers_t115197445_0_0_0,
	&GenInst_Animator_t2776330603_0_0_0,
	&GenInst_Direction_t94975348_0_0_0,
	&GenInst_CFX_Demo_Translate_t2936908572_0_0_0,
	&GenInst_CFX_AutoStopLoopedEffect_t3902828655_0_0_0,
	&GenInst_Action_1_t914279838_0_0_0,
	&GenInst_InvitationReceivedDelegate_t2409308905_0_0_0,
	&GenInst_MatchDelegate_t1377674964_0_0_0,
	&GenInst_PlayGamesClientConfiguration_t4135364200_0_0_0,
	&GenInst_EventManager_t2071886271_0_0_0,
	&GenInst_Action_2_t1791805235_0_0_0,
	&GenInst_Action_2_t423619683_0_0_0,
	&GenInst_QuestManager_t675525239_0_0_0,
	&GenInst_Action_2_t4092892251_0_0_0,
	&GenInst_Action_2_t1166110507_0_0_0,
	&GenInst_Action_3_t1510264275_0_0_0,
	&GenInst_Action_2_t2211148282_0_0_0,
	&GenInst_IQuestMilestone_t3485030629_0_0_0,
	&GenInst_Action_3_t2040057952_0_0_0,
	&GenInst_NativeClient_t3798002602_0_0_0,
	&GenInst_RealtimeManager_t1839590440_0_0_0,
	&GenInst_RoomSession_t1352686482_0_0_0,
	&GenInst_RealTimeMultiplayerListener_t8218125_0_0_0,
	&GenInst_State_t1703243816_0_0_0,
	&GenInst_SnapshotManager_t2359319983_0_0_0,
	&GenInst_Action_2_t2072880178_0_0_0,
	&GenInst_ConflictCallback_t942269343_0_0_0,
	&GenInst_Action_2_t2751370656_0_0_0,
	&GenInst_Action_2_t1151548080_0_0_0,
	&GenInst_Action_2_t3441065730_0_0_0,
	&GenInst_Action_2_t1400690759_0_0_0,
	&GenInst_GameServices_t1862808700_0_0_0,
	&GenInst_Action_1_t3953224079_0_0_0,
	&GenInst_Action_1_t3201525886_0_0_0,
	&GenInst_Action_1_t2909004501_0_0_0,
	&GenInst_Action_1_t111250811_0_0_0,
	&GenInst_Action_1_t1447876159_0_0_0,
	&GenInst_AuthFinishedCallback_t563494438_0_0_0,
	&GenInst_AuthStartedCallback_t2497315511_0_0_0,
	&GenInst_Action_3_t3317244834_0_0_0,
	&GenInst_Action_3_t2130612649_0_0_0,
	&GenInst_Action_2_t2637682483_0_0_0,
	&GenInst_Action_4_t2045751174_0_0_0,
	&GenInst_Action_1_t150760668_0_0_0,
	&GenInst_Action_1_t3071188627_0_0_0,
	&GenInst_Action_1_t2328837628_0_0_0,
	&GenInst_NativeSnapshotMetadataChange_t2885515174_0_0_0,
	&GenInst_Action_1_t2688443720_0_0_0,
	&GenInst_Action_1_t1663036183_0_0_0,
	&GenInst_PlayGamesHelperObject_t727662960_0_0_0,
	&GenInst_IPlayGamesClient_t2528289561_0_0_0,
	&GenInst_CharacterController_t1618060635_0_0_0,
	&GenInst_SpringJoint_t558455091_0_0_0,
	&GenInst_MeshFilter_t3839065225_0_0_0,
	&GenInst_iTweenFSMEvents_t871409943_0_0_0,
	&GenInst_NavMeshAgent_t588466745_0_0_0,
	&GenInst_HingeJoint2D_t2650814389_0_0_0,
	&GenInst_Joint_t4201008640_0_0_0,
	&GenInst_WheelJoint2D_t2492372869_0_0_0,
	&GenInst_iTween_t3087282050_0_0_0,
	&GenInst_Movie_t147243648_0_0_0,
	&GenInst_MeshRenderer_t2804666580_0_0_0,
	&GenInst_MediaPlayerCtrl_t3572035536_0_0_0,
	&GenInst_ResponseInfo_t1603221679_0_0_0,
	&GenInst_ResponseInfoType_t519916681_0_0_0,
	&GenInst_ResponseSaveData_t2563836488_0_0_0,
	&GenInst_ResponseResultAllData_t4071630637_0_0_0,
	&GenInst_CaculationResultData_t1977689536_0_0_0,
	&GenInst_LearnCalculation_t2957617669_0_0_0,
	&GenInst_ScoreManager_Lobby_t3853030226_0_0_0,
	&GenInst_ScoreManager_Variation_t1665153935_0_0_0,
	&GenInst_ScoreManagerMiniGame_t3434204900_0_0_0,
	&GenInst_ScoreManager_Temping_t3029572618_0_0_0,
	&GenInst_ScoreManager_Icing_t3849909252_0_0_0,
	&GenInst_Popup_t77299852_0_0_0,
	&GenInst_TempingPointer_t2678149871_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_ParamDataType_t2672665179_0_0_0_ParamDataType_t2672665179_0_0_0,
	&GenInst_JsonPosition_t3864946409_0_0_0_JsonPosition_t3864946409_0_0_0,
	&GenInst_Boolean_t476798718_0_0_0_Boolean_t476798718_0_0_0,
	&GenInst_Byte_t2862609660_0_0_0_Byte_t2862609660_0_0_0,
	&GenInst_Char_t2862622538_0_0_0_Char_t2862622538_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_KeyValuePair_2_t1944668977_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t3059612989_0_0_0_CustomAttributeNamedArgument_t3059612989_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t3301293422_0_0_0_CustomAttributeTypedArgument_t3301293422_0_0_0,
	&GenInst_Color32_t598853688_0_0_0_Color32_t598853688_0_0_0,
	&GenInst_RaycastResult_t3762661364_0_0_0_RaycastResult_t3762661364_0_0_0,
	&GenInst_Playable_t70832698_0_0_0_Playable_t70832698_0_0_0,
	&GenInst_UICharInfo_t65807484_0_0_0_UICharInfo_t65807484_0_0_0,
	&GenInst_UILineInfo_t4113875482_0_0_0_UILineInfo_t4113875482_0_0_0,
	&GenInst_UIVertex_t4244065212_0_0_0_UIVertex_t4244065212_0_0_0,
	&GenInst_Vector2_t4282066565_0_0_0_Vector2_t4282066565_0_0_0,
	&GenInst_Vector3_t4282066566_0_0_0_Vector3_t4282066566_0_0_0,
	&GenInst_Vector4_t4282066567_0_0_0_Vector4_t4282066567_0_0_0,
	&GenInst_ParticipantStatus_t4028684685_0_0_0_ParticipantStatus_t4028684685_0_0_0,
	&GenInst_ParticipantStatus_t4028684685_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_ParticipantStatus_t3137786926_0_0_0,
	&GenInst_ParticipantStatus_t3137786926_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3076810564_0_0_0_KeyValuePair_2_t3076810564_0_0_0,
	&GenInst_KeyValuePair_2_t3076810564_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeNameKey_t2971844791_0_0_0_TypeNameKey_t2971844791_0_0_0,
	&GenInst_KeyValuePair_2_t4013503581_0_0_0_KeyValuePair_2_t4013503581_0_0_0,
	&GenInst_KeyValuePair_2_t4013503581_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ResolverContractKey_t473801005_0_0_0_ResolverContractKey_t473801005_0_0_0,
	&GenInst_KeyValuePair_2_t2893931855_0_0_0_KeyValuePair_2_t2893931855_0_0_0,
	&GenInst_KeyValuePair_2_t2893931855_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TypeConvertKey_t866134174_0_0_0_TypeConvertKey_t866134174_0_0_0,
	&GenInst_KeyValuePair_2_t2815902970_0_0_0_KeyValuePair_2_t2815902970_0_0_0,
	&GenInst_KeyValuePair_2_t2815902970_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0_KeyValuePair_2_t1049882445_0_0_0,
	&GenInst_KeyValuePair_2_t1049882445_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_KeyValuePair_2_t4066860316_0_0_0,
	&GenInst_KeyValuePair_2_t4066860316_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ParticipantResult_t2327217482_0_0_0_ParticipantResult_t2327217482_0_0_0,
	&GenInst_ParticipantResult_t2327217482_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t101070088_0_0_0_KeyValuePair_2_t101070088_0_0_0,
	&GenInst_KeyValuePair_2_t101070088_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ReadType_t3446921512_0_0_0_ReadType_t3446921512_0_0_0,
	&GenInst_ReadType_t3446921512_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1220774118_0_0_0_KeyValuePair_2_t1220774118_0_0_0,
	&GenInst_KeyValuePair_2_t1220774118_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PropertyPresence_t2892329490_0_0_0_PropertyPresence_t2892329490_0_0_0,
	&GenInst_PropertyPresence_t2892329490_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t666182096_0_0_0_KeyValuePair_2_t666182096_0_0_0,
	&GenInst_KeyValuePair_2_t666182096_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PrimitiveTypeCode_t2429291660_0_0_0_PrimitiveTypeCode_t2429291660_0_0_0,
	&GenInst_PrimitiveTypeCode_t2429291660_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t203144266_0_0_0_KeyValuePair_2_t203144266_0_0_0,
	&GenInst_KeyValuePair_2_t203144266_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_KeyValuePair_2_t2545618620_0_0_0,
	&GenInst_KeyValuePair_2_t2545618620_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_KeyValuePair_2_t3222658402_0_0_0,
	&GenInst_KeyValuePair_2_t3222658402_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1944668977_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0_KeyValuePair_2_t2065771578_0_0_0,
	&GenInst_KeyValuePair_2_t2065771578_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t4291918972_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t4291918972_0_0_0_Single_t4291918972_0_0_0,
	&GenInst_KeyValuePair_2_t2093487883_0_0_0_KeyValuePair_2_t2093487883_0_0_0,
	&GenInst_KeyValuePair_2_t2093487883_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt32_t24667981_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt32_t24667981_0_0_0_UInt32_t24667981_0_0_0,
	&GenInst_KeyValuePair_2_t3443564286_0_0_0_KeyValuePair_2_t3443564286_0_0_0,
	&GenInst_KeyValuePair_2_t3443564286_0_0_0_Il2CppObject_0_0_0,
	&GenInst_RaycastHit2D_t1374744384_0_0_0_Il2CppObject_0_0_0,
	&GenInst_RaycastHit2D_t1374744384_0_0_0_RaycastHit2D_t1374744384_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0_KeyValuePair_2_t1919813716_0_0_0,
	&GenInst_KeyValuePair_2_t1919813716_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t4145961110_0_0_0_TextEditOp_t4145961110_0_0_0,
	&GenInst_KeyValuePair_2_t300380471_0_0_0_KeyValuePair_2_t300380471_0_0_0,
	&GenInst_KeyValuePair_2_t300380471_0_0_0_Il2CppObject_0_0_0,
	&GenInst_HandleRef_t1780819301_0_0_0_HandleRef_t1780819301_0_0_0,
};
