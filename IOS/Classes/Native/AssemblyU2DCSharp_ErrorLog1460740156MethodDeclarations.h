﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorLog
struct ErrorLog_t1460740156;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorLog::.ctor()
extern "C"  void ErrorLog__ctor_m2213159391 (ErrorLog_t1460740156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorLog::Start()
extern "C"  void ErrorLog_Start_m1160297183 (ErrorLog_t1460740156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorLog::Update()
extern "C"  void ErrorLog_Update_m1615326478 (ErrorLog_t1460740156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
