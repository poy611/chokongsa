﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.PropertyNameTable/Entry
struct Entry_t1172375224;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Property1172375224.h"

// System.Void Newtonsoft.Json.Utilities.PropertyNameTable/Entry::.ctor(System.String,System.Int32,Newtonsoft.Json.Utilities.PropertyNameTable/Entry)
extern "C"  void Entry__ctor_m3546220945 (Entry_t1172375224 * __this, String_t* ___value0, int32_t ___hashCode1, Entry_t1172375224 * ___next2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
