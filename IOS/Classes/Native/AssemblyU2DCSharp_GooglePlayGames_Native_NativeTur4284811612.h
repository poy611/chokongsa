﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;
// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t3828344078;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4
struct  U3CWaitForLoginU3Ec__Iterator4_t4284811612  : public Il2CppObject
{
public:
	// System.Action GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::method
	Action_t3771233898 * ___method_0;
	// System.Int32 GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::$PC
	int32_t ___U24PC_1;
	// System.Object GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::$current
	Il2CppObject * ___U24current_2;
	// System.Action GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::<$>method
	Action_t3771233898 * ___U3CU24U3Emethod_3;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<WaitForLogin>c__Iterator4::<>f__this
	NativeTurnBasedMultiplayerClient_t3828344078 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CWaitForLoginU3Ec__Iterator4_t4284811612, ___method_0)); }
	inline Action_t3771233898 * get_method_0() const { return ___method_0; }
	inline Action_t3771233898 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Action_t3771233898 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CWaitForLoginU3Ec__Iterator4_t4284811612, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForLoginU3Ec__Iterator4_t4284811612, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Emethod_3() { return static_cast<int32_t>(offsetof(U3CWaitForLoginU3Ec__Iterator4_t4284811612, ___U3CU24U3Emethod_3)); }
	inline Action_t3771233898 * get_U3CU24U3Emethod_3() const { return ___U3CU24U3Emethod_3; }
	inline Action_t3771233898 ** get_address_of_U3CU24U3Emethod_3() { return &___U3CU24U3Emethod_3; }
	inline void set_U3CU24U3Emethod_3(Action_t3771233898 * value)
	{
		___U3CU24U3Emethod_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Emethod_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CWaitForLoginU3Ec__Iterator4_t4284811612, ___U3CU3Ef__this_4)); }
	inline NativeTurnBasedMultiplayerClient_t3828344078 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline NativeTurnBasedMultiplayerClient_t3828344078 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(NativeTurnBasedMultiplayerClient_t3828344078 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
