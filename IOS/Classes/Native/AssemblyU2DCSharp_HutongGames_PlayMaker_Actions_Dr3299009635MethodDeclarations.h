﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawDebugRay
struct DrawDebugRay_t3299009635;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::.ctor()
extern "C"  void DrawDebugRay__ctor_m1447233891 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::Reset()
extern "C"  void DrawDebugRay_Reset_m3388634128 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::OnUpdate()
extern "C"  void DrawDebugRay_OnUpdate_m3415111945 (DrawDebugRay_t3299009635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
