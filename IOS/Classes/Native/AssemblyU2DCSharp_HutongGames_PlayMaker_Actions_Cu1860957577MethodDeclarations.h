﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveVector3
struct CurveVector3_t1860957577;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveVector3::.ctor()
extern "C"  void CurveVector3__ctor_m1367597821 (CurveVector3_t1860957577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::Reset()
extern "C"  void CurveVector3_Reset_m3308998058 (CurveVector3_t1860957577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnEnter()
extern "C"  void CurveVector3_OnEnter_m3688209300 (CurveVector3_t1860957577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnExit()
extern "C"  void CurveVector3_OnExit_m959178116 (CurveVector3_t1860957577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnUpdate()
extern "C"  void CurveVector3_OnUpdate_m1798897967 (CurveVector3_t1860957577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
