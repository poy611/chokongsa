﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugVector2
struct DebugVector2_t523847588;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugVector2::.ctor()
extern "C"  void DebugVector2__ctor_m4057055042 (DebugVector2_t523847588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugVector2::Reset()
extern "C"  void DebugVector2_Reset_m1703487983 (DebugVector2_t523847588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugVector2::OnEnter()
extern "C"  void DebugVector2_OnEnter_m2686286489 (DebugVector2_t523847588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
