﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Temping
struct  ScoreManager_Temping_t3029572618  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreManager_Temping::nowNum
	int32_t ___nowNum_2;
	// System.String ScoreManager_Temping::nowScene
	String_t* ___nowScene_3;
	// StructforMinigame ScoreManager_Temping::stuff
	StructforMinigame_t1286533533 * ___stuff_4;
	// ConstforMinigame ScoreManager_Temping::cuff
	ConstforMinigame_t2789090703 * ___cuff_5;
	// System.Diagnostics.Stopwatch ScoreManager_Temping::sw
	Stopwatch_t3420517611 * ___sw_6;
	// PopupManagement ScoreManager_Temping::pop
	PopupManagement_t282433007 * ___pop_7;
	// UnityEngine.UI.Text ScoreManager_Temping::timmer
	Text_t9039225 * ___timmer_8;
	// UnityEngine.Sprite[] ScoreManager_Temping::res
	SpriteU5BU5D_t2761310900* ___res_9;
	// System.Single ScoreManager_Temping::ticks
	float ___ticks_10;
	// System.Single ScoreManager_Temping::tempingLimitAmount
	float ___tempingLimitAmount_11;
	// System.Single ScoreManager_Temping::tempingAmount
	float ___tempingAmount_12;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_nowScene_3() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___nowScene_3)); }
	inline String_t* get_nowScene_3() const { return ___nowScene_3; }
	inline String_t** get_address_of_nowScene_3() { return &___nowScene_3; }
	inline void set_nowScene_3(String_t* value)
	{
		___nowScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_3, value);
	}

	inline static int32_t get_offset_of_stuff_4() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___stuff_4)); }
	inline StructforMinigame_t1286533533 * get_stuff_4() const { return ___stuff_4; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_4() { return &___stuff_4; }
	inline void set_stuff_4(StructforMinigame_t1286533533 * value)
	{
		___stuff_4 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_4, value);
	}

	inline static int32_t get_offset_of_cuff_5() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___cuff_5)); }
	inline ConstforMinigame_t2789090703 * get_cuff_5() const { return ___cuff_5; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_5() { return &___cuff_5; }
	inline void set_cuff_5(ConstforMinigame_t2789090703 * value)
	{
		___cuff_5 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_5, value);
	}

	inline static int32_t get_offset_of_sw_6() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___sw_6)); }
	inline Stopwatch_t3420517611 * get_sw_6() const { return ___sw_6; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_6() { return &___sw_6; }
	inline void set_sw_6(Stopwatch_t3420517611 * value)
	{
		___sw_6 = value;
		Il2CppCodeGenWriteBarrier(&___sw_6, value);
	}

	inline static int32_t get_offset_of_pop_7() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___pop_7)); }
	inline PopupManagement_t282433007 * get_pop_7() const { return ___pop_7; }
	inline PopupManagement_t282433007 ** get_address_of_pop_7() { return &___pop_7; }
	inline void set_pop_7(PopupManagement_t282433007 * value)
	{
		___pop_7 = value;
		Il2CppCodeGenWriteBarrier(&___pop_7, value);
	}

	inline static int32_t get_offset_of_timmer_8() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___timmer_8)); }
	inline Text_t9039225 * get_timmer_8() const { return ___timmer_8; }
	inline Text_t9039225 ** get_address_of_timmer_8() { return &___timmer_8; }
	inline void set_timmer_8(Text_t9039225 * value)
	{
		___timmer_8 = value;
		Il2CppCodeGenWriteBarrier(&___timmer_8, value);
	}

	inline static int32_t get_offset_of_res_9() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___res_9)); }
	inline SpriteU5BU5D_t2761310900* get_res_9() const { return ___res_9; }
	inline SpriteU5BU5D_t2761310900** get_address_of_res_9() { return &___res_9; }
	inline void set_res_9(SpriteU5BU5D_t2761310900* value)
	{
		___res_9 = value;
		Il2CppCodeGenWriteBarrier(&___res_9, value);
	}

	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___ticks_10)); }
	inline float get_ticks_10() const { return ___ticks_10; }
	inline float* get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(float value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_tempingLimitAmount_11() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___tempingLimitAmount_11)); }
	inline float get_tempingLimitAmount_11() const { return ___tempingLimitAmount_11; }
	inline float* get_address_of_tempingLimitAmount_11() { return &___tempingLimitAmount_11; }
	inline void set_tempingLimitAmount_11(float value)
	{
		___tempingLimitAmount_11 = value;
	}

	inline static int32_t get_offset_of_tempingAmount_12() { return static_cast<int32_t>(offsetof(ScoreManager_Temping_t3029572618, ___tempingAmount_12)); }
	inline float get_tempingAmount_12() const { return ___tempingAmount_12; }
	inline float* get_address_of_tempingAmount_12() { return &___tempingAmount_12; }
	inline void set_tempingAmount_12(float value)
	{
		___tempingAmount_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
