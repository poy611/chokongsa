﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// UnityEngine.Material
struct Material_t3870600107;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "mscorlib_System_Type2863145774.h"

// UnityEngine.Material HutongGames.PlayMaker.FsmMaterial::get_Value()
extern "C"  Material_t3870600107 * FsmMaterial_get_Value_m1376213207 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::set_Value(UnityEngine.Material)
extern "C"  void FsmMaterial_set_Value_m3341549218 (FsmMaterial_t924399665 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor()
extern "C"  void FsmMaterial__ctor_m4051814862 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(System.String)
extern "C"  void FsmMaterial__ctor_m38188020 (FsmMaterial_t924399665 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmMaterial__ctor_m987867825 (FsmMaterial_t924399665 * __this, FsmObject_t821476169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmMaterial::Clone()
extern "C"  NamedVariable_t3211770239 * FsmMaterial_Clone_m3985249907 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmMaterial::get_VariableType()
extern "C"  int32_t FsmMaterial_get_VariableType_m280413526 (FsmMaterial_t924399665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmMaterial::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmMaterial_TestTypeConstraint_m3394603448 (FsmMaterial_t924399665 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
