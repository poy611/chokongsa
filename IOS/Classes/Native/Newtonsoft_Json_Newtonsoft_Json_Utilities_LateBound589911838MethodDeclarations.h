﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>
struct U3CU3Ec__DisplayClass7_0_1_t589911838;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0_1__ctor_m2805806240_gshared (U3CU3Ec__DisplayClass7_0_1_t589911838 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass7_0_1__ctor_m2805806240(__this, method) ((  void (*) (U3CU3Ec__DisplayClass7_0_1_t589911838 *, const MethodInfo*))U3CU3Ec__DisplayClass7_0_1__ctor_m2805806240_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1<System.Object>::<CreateGet>b__0(T)
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass7_0_1_U3CCreateGetU3Eb__0_m1809507751_gshared (U3CU3Ec__DisplayClass7_0_1_t589911838 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define U3CU3Ec__DisplayClass7_0_1_U3CCreateGetU3Eb__0_m1809507751(__this, ___o0, method) ((  Il2CppObject * (*) (U3CU3Ec__DisplayClass7_0_1_t589911838 *, Il2CppObject *, const MethodInfo*))U3CU3Ec__DisplayClass7_0_1_U3CCreateGetU3Eb__0_m1809507751_gshared)(__this, ___o0, method)
