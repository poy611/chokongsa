﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse
struct RoomInboxUIResponse_t1117529840;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::.ctor(System.IntPtr)
extern "C"  void RoomInboxUIResponse__ctor_m3956611721 (RoomInboxUIResponse_t1117529840 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::ResponseStatus()
extern "C"  int32_t RoomInboxUIResponse_ResponseStatus_m749402018 (RoomInboxUIResponse_t1117529840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::Invitation()
extern "C"  MultiplayerInvitation_t3411188537 * RoomInboxUIResponse_Invitation_m2156398655 (RoomInboxUIResponse_t1117529840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RoomInboxUIResponse_CallDispose_m3616794695 (RoomInboxUIResponse_t1117529840 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse::FromPointer(System.IntPtr)
extern "C"  RoomInboxUIResponse_t1117529840 * RoomInboxUIResponse_FromPointer_m3517709157 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
