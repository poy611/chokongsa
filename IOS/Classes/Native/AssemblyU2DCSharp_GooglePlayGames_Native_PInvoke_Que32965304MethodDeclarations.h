﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
struct FetchListResponse_t32965304;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeQuest>
struct IEnumerable_1_t1502246190;
// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::.ctor(System.IntPtr)
extern "C"  void FetchListResponse__ctor_m3339436417 (FetchListResponse_t32965304 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::ResponseStatus()
extern "C"  int32_t FetchListResponse_ResponseStatus_m4224592059 (FetchListResponse_t32965304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::RequestSucceeded()
extern "C"  bool FetchListResponse_RequestSucceeded_m2335004943 (FetchListResponse_t32965304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeQuest> GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::Data()
extern "C"  Il2CppObject* FetchListResponse_Data_m1629323819 (FetchListResponse_t32965304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchListResponse_CallDispose_m2333081679 (FetchListResponse_t32965304 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::FromPointer(System.IntPtr)
extern "C"  FetchListResponse_t32965304 * FetchListResponse_FromPointer_m3420486357 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::<Data>m__CD(System.UIntPtr)
extern "C"  NativeQuest_t2496300529 * FetchListResponse_U3CDataU3Em__CD_m3730032167 (FetchListResponse_t32965304 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
