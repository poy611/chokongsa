﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StopLocationServiceUpdates
struct StopLocationServiceUpdates_t2528039284;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::.ctor()
extern "C"  void StopLocationServiceUpdates__ctor_m4149228914 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::Reset()
extern "C"  void StopLocationServiceUpdates_Reset_m1795661855 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::OnEnter()
extern "C"  void StopLocationServiceUpdates_OnEnter_m1071064265 (StopLocationServiceUpdates_t2528039284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
