﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_String7231557.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Construct()
extern "C"  IntPtr_t IosPlatformConfiguration_IosPlatformConfiguration_Construct_m1863237347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void IosPlatformConfiguration_IosPlatformConfiguration_Dispose_m1495653331 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool IosPlatformConfiguration_IosPlatformConfiguration_Valid_m3397918884 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_SetClientID(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void IosPlatformConfiguration_IosPlatformConfiguration_SetClientID_m1483856728 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___client_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
