﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartFirst
struct StartFirst_t396912526;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void StartFirst::.ctor()
extern "C"  void StartFirst__ctor_m2925777101 (StartFirst_t396912526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartFirst::Start()
extern "C"  void StartFirst_Start_m1872914893 (StartFirst_t396912526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StartFirst::BlackBlink()
extern "C"  Il2CppObject * StartFirst_BlackBlink_m3299090428 (StartFirst_t396912526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
