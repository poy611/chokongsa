﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>
struct ValueCollection_t22599125;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.ReadType>
struct Dictionary_2_t1321993412;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.ReadType>
struct IEnumerator_1_t1063819265;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.ReadType[]
struct ReadTypeU5BU5D_t2190873913;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3548794116.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3894124041_gshared (ValueCollection_t22599125 * __this, Dictionary_2_t1321993412 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3894124041(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t22599125 *, Dictionary_2_t1321993412 *, const MethodInfo*))ValueCollection__ctor_m3894124041_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3043113225_gshared (ValueCollection_t22599125 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3043113225(__this, ___item0, method) ((  void (*) (ValueCollection_t22599125 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3043113225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1239945810_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1239945810(__this, method) ((  void (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1239945810_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m186888577_gshared (ValueCollection_t22599125 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m186888577(__this, ___item0, method) ((  bool (*) (ValueCollection_t22599125 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m186888577_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2995598950_gshared (ValueCollection_t22599125 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2995598950(__this, ___item0, method) ((  bool (*) (ValueCollection_t22599125 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2995598950_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2404837778_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2404837778(__this, method) ((  Il2CppObject* (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2404837778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m435254486_gshared (ValueCollection_t22599125 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m435254486(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t22599125 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m435254486_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m468360037_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m468360037(__this, method) ((  Il2CppObject * (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m468360037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2757031732_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2757031732(__this, method) ((  bool (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2757031732_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4241320724_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4241320724(__this, method) ((  bool (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4241320724_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1755684870_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1755684870(__this, method) ((  Il2CppObject * (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1755684870_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1800693776_gshared (ValueCollection_t22599125 * __this, ReadTypeU5BU5D_t2190873913* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1800693776(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t22599125 *, ReadTypeU5BU5D_t2190873913*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1800693776_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::GetEnumerator()
extern "C"  Enumerator_t3548794116  ValueCollection_GetEnumerator_m4192399353_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4192399353(__this, method) ((  Enumerator_t3548794116  (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_GetEnumerator_m4192399353_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.ReadType>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m824890190_gshared (ValueCollection_t22599125 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m824890190(__this, method) ((  int32_t (*) (ValueCollection_t22599125 *, const MethodInfo*))ValueCollection_get_Count_m824890190_gshared)(__this, method)
