﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmArraySet
struct FsmArraySet_t3475177271;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::.ctor()
extern "C"  void FsmArraySet__ctor_m2176457183 (FsmArraySet_t3475177271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::Reset()
extern "C"  void FsmArraySet_Reset_m4117857420 (FsmArraySet_t3475177271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnEnter()
extern "C"  void FsmArraySet_OnEnter_m3612975606 (FsmArraySet_t3475177271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::DoSetFsmString()
extern "C"  void FsmArraySet_DoSetFsmString_m2702089055 (FsmArraySet_t3475177271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnUpdate()
extern "C"  void FsmArraySet_OnUpdate_m3761620749 (FsmArraySet_t3475177271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
