﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetVideo
struct  GetVideo_t2032672485  : public MonoBehaviour_t667441552
{
public:
	// System.String GetVideo::videoId1
	String_t* ___videoId1_2;
	// System.String GetVideo::videoId2
	String_t* ___videoId2_3;

public:
	inline static int32_t get_offset_of_videoId1_2() { return static_cast<int32_t>(offsetof(GetVideo_t2032672485, ___videoId1_2)); }
	inline String_t* get_videoId1_2() const { return ___videoId1_2; }
	inline String_t** get_address_of_videoId1_2() { return &___videoId1_2; }
	inline void set_videoId1_2(String_t* value)
	{
		___videoId1_2 = value;
		Il2CppCodeGenWriteBarrier(&___videoId1_2, value);
	}

	inline static int32_t get_offset_of_videoId2_3() { return static_cast<int32_t>(offsetof(GetVideo_t2032672485, ___videoId2_3)); }
	inline String_t* get_videoId2_3() const { return ___videoId2_3; }
	inline String_t** get_address_of_videoId2_3() { return &___videoId2_3; }
	inline void set_videoId2_3(String_t* value)
	{
		___videoId2_3 = value;
		Il2CppCodeGenWriteBarrier(&___videoId2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
