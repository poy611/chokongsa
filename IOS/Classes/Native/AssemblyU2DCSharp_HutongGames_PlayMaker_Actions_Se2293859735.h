﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmGameObject
struct  SetFsmGameObject_t2293859735  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmGameObject::fsmName
	FsmString_t952858651 * ___fsmName_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmGameObject::variableName
	FsmString_t952858651 * ___variableName_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetFsmGameObject::setValue
	FsmGameObject_t1697147867 * ___setValue_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmGameObject::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmGameObject::goLastFrame
	GameObject_t3674682005 * ___goLastFrame_16;
	// System.String HutongGames.PlayMaker.Actions.SetFsmGameObject::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_17;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmGameObject::fsm
	PlayMakerFSM_t3799847376 * ___fsm_18;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_fsmName_12() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___fsmName_12)); }
	inline FsmString_t952858651 * get_fsmName_12() const { return ___fsmName_12; }
	inline FsmString_t952858651 ** get_address_of_fsmName_12() { return &___fsmName_12; }
	inline void set_fsmName_12(FsmString_t952858651 * value)
	{
		___fsmName_12 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_12, value);
	}

	inline static int32_t get_offset_of_variableName_13() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___variableName_13)); }
	inline FsmString_t952858651 * get_variableName_13() const { return ___variableName_13; }
	inline FsmString_t952858651 ** get_address_of_variableName_13() { return &___variableName_13; }
	inline void set_variableName_13(FsmString_t952858651 * value)
	{
		___variableName_13 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_13, value);
	}

	inline static int32_t get_offset_of_setValue_14() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___setValue_14)); }
	inline FsmGameObject_t1697147867 * get_setValue_14() const { return ___setValue_14; }
	inline FsmGameObject_t1697147867 ** get_address_of_setValue_14() { return &___setValue_14; }
	inline void set_setValue_14(FsmGameObject_t1697147867 * value)
	{
		___setValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___setValue_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_16() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___goLastFrame_16)); }
	inline GameObject_t3674682005 * get_goLastFrame_16() const { return ___goLastFrame_16; }
	inline GameObject_t3674682005 ** get_address_of_goLastFrame_16() { return &___goLastFrame_16; }
	inline void set_goLastFrame_16(GameObject_t3674682005 * value)
	{
		___goLastFrame_16 = value;
		Il2CppCodeGenWriteBarrier(&___goLastFrame_16, value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_17() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___fsmNameLastFrame_17)); }
	inline String_t* get_fsmNameLastFrame_17() const { return ___fsmNameLastFrame_17; }
	inline String_t** get_address_of_fsmNameLastFrame_17() { return &___fsmNameLastFrame_17; }
	inline void set_fsmNameLastFrame_17(String_t* value)
	{
		___fsmNameLastFrame_17 = value;
		Il2CppCodeGenWriteBarrier(&___fsmNameLastFrame_17, value);
	}

	inline static int32_t get_offset_of_fsm_18() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2293859735, ___fsm_18)); }
	inline PlayMakerFSM_t3799847376 * get_fsm_18() const { return ___fsm_18; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_fsm_18() { return &___fsm_18; }
	inline void set_fsm_18(PlayMakerFSM_t3799847376 * value)
	{
		___fsm_18 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
