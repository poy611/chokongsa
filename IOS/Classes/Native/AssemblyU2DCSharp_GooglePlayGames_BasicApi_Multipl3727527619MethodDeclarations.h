﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t3727527619;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.Player::.ctor(System.String,System.String,System.String)
extern "C"  void Player__ctor_m3194827276 (Player_t3727527619 * __this, String_t* ___displayName0, String_t* ___playerId1, String_t* ___avatarUrl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
