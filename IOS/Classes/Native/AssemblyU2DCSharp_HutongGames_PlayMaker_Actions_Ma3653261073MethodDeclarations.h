﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerUnregisterHost
struct MasterServerUnregisterHost_t3653261073;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::.ctor()
extern "C"  void MasterServerUnregisterHost__ctor_m2418978933 (MasterServerUnregisterHost_t3653261073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::OnEnter()
extern "C"  void MasterServerUnregisterHost_OnEnter_m453176076 (MasterServerUnregisterHost_t3653261073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
