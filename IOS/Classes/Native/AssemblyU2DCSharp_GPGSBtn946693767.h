﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.RawImage
struct RawImage_t821930207;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPGSBtn
struct  GPGSBtn_t946693767  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text GPGSBtn::Login_Label
	Text_t9039225 * ___Login_Label_2;
	// UnityEngine.UI.Text GPGSBtn::User_Label
	Text_t9039225 * ___User_Label_3;
	// UnityEngine.UI.RawImage GPGSBtn::User_Texture
	RawImage_t821930207 * ___User_Texture_4;
	// UnityEngine.Texture2D GPGSBtn::_img
	Texture2D_t3884108195 * ____img_5;
	// System.Boolean GPGSBtn::isSetting
	bool ___isSetting_6;
	// System.Boolean GPGSBtn::autoLogin
	bool ___autoLogin_7;
	// UnityEngine.GameObject GPGSBtn::bt
	GameObject_t3674682005 * ___bt_8;
	// UnityEngine.GameObject GPGSBtn::loginBt
	GameObject_t3674682005 * ___loginBt_9;
	// UnityEngine.GameObject GPGSBtn::exitBt
	GameObject_t3674682005 * ___exitBt_10;
	// UnityEngine.UI.Text GPGSBtn::tempText
	Text_t9039225 * ___tempText_11;

public:
	inline static int32_t get_offset_of_Login_Label_2() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___Login_Label_2)); }
	inline Text_t9039225 * get_Login_Label_2() const { return ___Login_Label_2; }
	inline Text_t9039225 ** get_address_of_Login_Label_2() { return &___Login_Label_2; }
	inline void set_Login_Label_2(Text_t9039225 * value)
	{
		___Login_Label_2 = value;
		Il2CppCodeGenWriteBarrier(&___Login_Label_2, value);
	}

	inline static int32_t get_offset_of_User_Label_3() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___User_Label_3)); }
	inline Text_t9039225 * get_User_Label_3() const { return ___User_Label_3; }
	inline Text_t9039225 ** get_address_of_User_Label_3() { return &___User_Label_3; }
	inline void set_User_Label_3(Text_t9039225 * value)
	{
		___User_Label_3 = value;
		Il2CppCodeGenWriteBarrier(&___User_Label_3, value);
	}

	inline static int32_t get_offset_of_User_Texture_4() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___User_Texture_4)); }
	inline RawImage_t821930207 * get_User_Texture_4() const { return ___User_Texture_4; }
	inline RawImage_t821930207 ** get_address_of_User_Texture_4() { return &___User_Texture_4; }
	inline void set_User_Texture_4(RawImage_t821930207 * value)
	{
		___User_Texture_4 = value;
		Il2CppCodeGenWriteBarrier(&___User_Texture_4, value);
	}

	inline static int32_t get_offset_of__img_5() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ____img_5)); }
	inline Texture2D_t3884108195 * get__img_5() const { return ____img_5; }
	inline Texture2D_t3884108195 ** get_address_of__img_5() { return &____img_5; }
	inline void set__img_5(Texture2D_t3884108195 * value)
	{
		____img_5 = value;
		Il2CppCodeGenWriteBarrier(&____img_5, value);
	}

	inline static int32_t get_offset_of_isSetting_6() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___isSetting_6)); }
	inline bool get_isSetting_6() const { return ___isSetting_6; }
	inline bool* get_address_of_isSetting_6() { return &___isSetting_6; }
	inline void set_isSetting_6(bool value)
	{
		___isSetting_6 = value;
	}

	inline static int32_t get_offset_of_autoLogin_7() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___autoLogin_7)); }
	inline bool get_autoLogin_7() const { return ___autoLogin_7; }
	inline bool* get_address_of_autoLogin_7() { return &___autoLogin_7; }
	inline void set_autoLogin_7(bool value)
	{
		___autoLogin_7 = value;
	}

	inline static int32_t get_offset_of_bt_8() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___bt_8)); }
	inline GameObject_t3674682005 * get_bt_8() const { return ___bt_8; }
	inline GameObject_t3674682005 ** get_address_of_bt_8() { return &___bt_8; }
	inline void set_bt_8(GameObject_t3674682005 * value)
	{
		___bt_8 = value;
		Il2CppCodeGenWriteBarrier(&___bt_8, value);
	}

	inline static int32_t get_offset_of_loginBt_9() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___loginBt_9)); }
	inline GameObject_t3674682005 * get_loginBt_9() const { return ___loginBt_9; }
	inline GameObject_t3674682005 ** get_address_of_loginBt_9() { return &___loginBt_9; }
	inline void set_loginBt_9(GameObject_t3674682005 * value)
	{
		___loginBt_9 = value;
		Il2CppCodeGenWriteBarrier(&___loginBt_9, value);
	}

	inline static int32_t get_offset_of_exitBt_10() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___exitBt_10)); }
	inline GameObject_t3674682005 * get_exitBt_10() const { return ___exitBt_10; }
	inline GameObject_t3674682005 ** get_address_of_exitBt_10() { return &___exitBt_10; }
	inline void set_exitBt_10(GameObject_t3674682005 * value)
	{
		___exitBt_10 = value;
		Il2CppCodeGenWriteBarrier(&___exitBt_10, value);
	}

	inline static int32_t get_offset_of_tempText_11() { return static_cast<int32_t>(offsetof(GPGSBtn_t946693767, ___tempText_11)); }
	inline Text_t9039225 * get_tempText_11() const { return ___tempText_11; }
	inline Text_t9039225 ** get_address_of_tempText_11() { return &___tempText_11; }
	inline void set_tempText_11(Text_t9039225 * value)
	{
		___tempText_11 = value;
		Il2CppCodeGenWriteBarrier(&___tempText_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
