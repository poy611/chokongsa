﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47
struct U3CGetUserEmailU3Ec__AnonStorey47_t447114323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "mscorlib_System_String7231557.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47::.ctor()
extern "C"  void U3CGetUserEmailU3Ec__AnonStorey47__ctor_m1607597688 (U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47::<>m__1B()
extern "C"  void U3CGetUserEmailU3Ec__AnonStorey47_U3CU3Em__1B_m1021208882 (U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47::<>m__1C(GooglePlayGames.BasicApi.CommonStatusCodes,System.String)
extern "C"  void U3CGetUserEmailU3Ec__AnonStorey47_U3CU3Em__1C_m588532398 (U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * __this, int32_t ___status0, String_t* ___email1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
