﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m865078383(__this, ___l0, method) ((  void (*) (Enumerator_t3487329513 *, List_1_t3467656743 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2117516931(__this, method) ((  void (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1728720121(__this, method) ((  Il2CppObject * (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::Dispose()
#define Enumerator_Dispose_m1731683860(__this, method) ((  void (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::VerifyState()
#define Enumerator_VerifyState_m4217502541(__this, method) ((  void (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::MoveNext()
#define Enumerator_MoveNext_m1869127936(__this, method) ((  bool (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<YoutubeExtractor.VideoInfo>::get_Current()
#define Enumerator_get_Current_m2489758556(__this, method) ((  VideoInfo_t2099471191 * (*) (Enumerator_t3487329513 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
