﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAllFsmGameObject
struct SetAllFsmGameObject_t3039021856;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAllFsmGameObject::.ctor()
extern "C"  void SetAllFsmGameObject__ctor_m1100889110 (SetAllFsmGameObject_t3039021856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAllFsmGameObject::Reset()
extern "C"  void SetAllFsmGameObject_Reset_m3042289347 (SetAllFsmGameObject_t3039021856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAllFsmGameObject::OnEnter()
extern "C"  void SetAllFsmGameObject_OnEnter_m784208493 (SetAllFsmGameObject_t3039021856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAllFsmGameObject::DoSetFsmGameObject()
extern "C"  void SetAllFsmGameObject_DoSetFsmGameObject_m2098710984 (SetAllFsmGameObject_t3039021856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
