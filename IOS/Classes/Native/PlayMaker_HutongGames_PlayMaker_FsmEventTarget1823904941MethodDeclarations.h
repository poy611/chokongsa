﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"

// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmEventTarget::get_Self()
extern "C"  FsmEventTarget_t1823904941 * FsmEventTarget_get_Self_m3277355005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::.ctor()
extern "C"  void FsmEventTarget__ctor_m3123387462 (FsmEventTarget_t1823904941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::.ctor(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmEventTarget__ctor_m2236534191 (FsmEventTarget_t1823904941 * __this, FsmEventTarget_t1823904941 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::ResetParameters()
extern "C"  void FsmEventTarget_ResetParameters_m1742767101 (FsmEventTarget_t1823904941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
