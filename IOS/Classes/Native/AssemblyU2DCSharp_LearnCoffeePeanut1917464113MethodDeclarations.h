﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearnCoffeePeanut
struct LearnCoffeePeanut_t1917464113;

#include "codegen/il2cpp-codegen.h"

// System.Void LearnCoffeePeanut::.ctor()
extern "C"  void LearnCoffeePeanut__ctor_m220980826 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::Start()
extern "C"  void LearnCoffeePeanut_Start_m3463085914 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::backButton()
extern "C"  void LearnCoffeePeanut_backButton_m1541116291 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::prev()
extern "C"  void LearnCoffeePeanut_prev_m386042109 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::next()
extern "C"  void LearnCoffeePeanut_next_m317342141 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::deskit()
extern "C"  void LearnCoffeePeanut_deskit_m3920404942 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCoffeePeanut::unableButton()
extern "C"  void LearnCoffeePeanut_unableButton_m731652559 (LearnCoffeePeanut_t1917464113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
