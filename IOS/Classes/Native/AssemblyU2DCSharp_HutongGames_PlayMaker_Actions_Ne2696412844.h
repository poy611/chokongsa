﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch
struct  NetworkPeerTypeSwitch_t2696412844  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::isDisconnected
	FsmEvent_t2133468028 * ___isDisconnected_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::isServer
	FsmEvent_t2133468028 * ___isServer_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::isClient
	FsmEvent_t2133468028 * ___isClient_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::isConnecting
	FsmEvent_t2133468028 * ___isConnecting_14;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_isDisconnected_11() { return static_cast<int32_t>(offsetof(NetworkPeerTypeSwitch_t2696412844, ___isDisconnected_11)); }
	inline FsmEvent_t2133468028 * get_isDisconnected_11() const { return ___isDisconnected_11; }
	inline FsmEvent_t2133468028 ** get_address_of_isDisconnected_11() { return &___isDisconnected_11; }
	inline void set_isDisconnected_11(FsmEvent_t2133468028 * value)
	{
		___isDisconnected_11 = value;
		Il2CppCodeGenWriteBarrier(&___isDisconnected_11, value);
	}

	inline static int32_t get_offset_of_isServer_12() { return static_cast<int32_t>(offsetof(NetworkPeerTypeSwitch_t2696412844, ___isServer_12)); }
	inline FsmEvent_t2133468028 * get_isServer_12() const { return ___isServer_12; }
	inline FsmEvent_t2133468028 ** get_address_of_isServer_12() { return &___isServer_12; }
	inline void set_isServer_12(FsmEvent_t2133468028 * value)
	{
		___isServer_12 = value;
		Il2CppCodeGenWriteBarrier(&___isServer_12, value);
	}

	inline static int32_t get_offset_of_isClient_13() { return static_cast<int32_t>(offsetof(NetworkPeerTypeSwitch_t2696412844, ___isClient_13)); }
	inline FsmEvent_t2133468028 * get_isClient_13() const { return ___isClient_13; }
	inline FsmEvent_t2133468028 ** get_address_of_isClient_13() { return &___isClient_13; }
	inline void set_isClient_13(FsmEvent_t2133468028 * value)
	{
		___isClient_13 = value;
		Il2CppCodeGenWriteBarrier(&___isClient_13, value);
	}

	inline static int32_t get_offset_of_isConnecting_14() { return static_cast<int32_t>(offsetof(NetworkPeerTypeSwitch_t2696412844, ___isConnecting_14)); }
	inline FsmEvent_t2133468028 * get_isConnecting_14() const { return ___isConnecting_14; }
	inline FsmEvent_t2133468028 ** get_address_of_isConnecting_14() { return &___isConnecting_14; }
	inline void set_isConnecting_14(FsmEvent_t2133468028 * value)
	{
		___isConnecting_14 = value;
		Il2CppCodeGenWriteBarrier(&___isConnecting_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(NetworkPeerTypeSwitch_t2696412844, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
