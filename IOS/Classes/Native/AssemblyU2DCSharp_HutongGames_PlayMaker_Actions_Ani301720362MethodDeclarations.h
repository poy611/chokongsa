﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateColor
struct AnimateColor_t301720362;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateColor::.ctor()
extern "C"  void AnimateColor__ctor_m1329511036 (AnimateColor_t301720362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::Reset()
extern "C"  void AnimateColor_Reset_m3270911273 (AnimateColor_t301720362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnEnter()
extern "C"  void AnimateColor_OnEnter_m1446547283 (AnimateColor_t301720362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::UpdateVariableValue()
extern "C"  void AnimateColor_UpdateVariableValue_m3632598758 (AnimateColor_t301720362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnUpdate()
extern "C"  void AnimateColor_OnUpdate_m1026852176 (AnimateColor_t301720362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
