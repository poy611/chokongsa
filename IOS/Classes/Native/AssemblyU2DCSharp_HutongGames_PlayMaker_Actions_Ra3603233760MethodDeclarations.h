﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RaycastAll
struct RaycastAll_t3603233760;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RaycastAll::.ctor()
extern "C"  void RaycastAll__ctor_m3389273222 (RaycastAll_t3603233760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::Reset()
extern "C"  void RaycastAll_Reset_m1035706163 (RaycastAll_t3603233760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::OnEnter()
extern "C"  void RaycastAll_OnEnter_m898084573 (RaycastAll_t3603233760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::OnUpdate()
extern "C"  void RaycastAll_OnUpdate_m1204377350 (RaycastAll_t3603233760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::DoRaycast()
extern "C"  void RaycastAll_DoRaycast_m3324028930 (RaycastAll_t3603233760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
