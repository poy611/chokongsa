﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t1492900827;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t926437290;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t530165700;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.String
struct String_t;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Serialization.ErrorEventArgs
struct ErrorEventArgs_t792639131;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl2761661122.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandl2077487315.h"
#include "Newtonsoft_Json_Newtonsoft_Json_NullValueHandling2754652381.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHandli56081595.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin2475221485.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHa2626038881.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting732683613.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializerSett2589405525.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_Nullable_1_gen3029687007.h"
#include "mscorlib_System_Nullable_1_gen2192459923.h"
#include "mscorlib_System_Nullable_1_gen3158207471.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Error792639131.h"

// System.Void Newtonsoft.Json.JsonSerializer::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializer_add_Error_m4140660243 (JsonSerializer_t251850770 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializer_remove_Error_m3387094008 (JsonSerializer_t251850770 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializer_set_ReferenceResolver_m1329831854 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializer_set_Binder_m585672685 (JsonSerializer_t251850770 * __this, SerializationBinder_t2137423328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::get_TraceWriter()
extern "C"  Il2CppObject * JsonSerializer_get_TraceWriter_m496131507 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern "C"  void JsonSerializer_set_TraceWriter_m362101710 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_EqualityComparer(System.Collections.IEqualityComparer)
extern "C"  void JsonSerializer_set_EqualityComparer_m756147705 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializer_set_TypeNameHandling_m1160570197 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializer_set_TypeNameAssemblyFormat_m13776697 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializer_set_PreserveReferencesHandling_m774550005 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializer_set_ReferenceLoopHandling_m3496087623 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializer_set_MissingMemberHandling_m2984535333 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializer_set_NullValueHandling_m1796037137 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializer_set_DefaultValueHandling_m967911157 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializer_set_ObjectCreationHandling_m2039435317 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializer_set_ConstructorHandling_m3263694449 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::get_MetadataPropertyHandling()
extern "C"  int32_t JsonSerializer_get_MetadataPropertyHandling_m252187380 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern "C"  void JsonSerializer_set_MetadataPropertyHandling_m1527291765 (JsonSerializer_t251850770 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::get_Converters()
extern "C"  JsonConverterCollection_t530165700 * JsonSerializer_get_Converters_m1751373947 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializer_get_ContractResolver_m2530554355 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializer_set_ContractResolver_m1037791554 (JsonSerializer_t251850770 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializer_get_Context_m1686154856 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializer_set_Context_m1740252139 (JsonSerializer_t251850770 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonSerializer::get_Formatting()
extern "C"  int32_t JsonSerializer_get_Formatting_m114972724 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonSerializer::get_CheckAdditionalContent()
extern "C"  bool JsonSerializer_get_CheckAdditionalContent_m3451935338 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::set_CheckAdditionalContent(System.Boolean)
extern "C"  void JsonSerializer_set_CheckAdditionalContent_m3406626539 (JsonSerializer_t251850770 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonSerializer::IsCheckAdditionalContentSet()
extern "C"  bool JsonSerializer_IsCheckAdditionalContentSet_m2487304985 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::.ctor()
extern "C"  void JsonSerializer__ctor_m3811413463 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create()
extern "C"  JsonSerializer_t251850770 * JsonSerializer_Create_m183548845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  JsonSerializer_t251850770 * JsonSerializer_Create_m2572986427 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault()
extern "C"  JsonSerializer_t251850770 * JsonSerializer_CreateDefault_m3324649142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault(Newtonsoft.Json.JsonSerializerSettings)
extern "C"  JsonSerializer_t251850770 * JsonSerializer_CreateDefault_m2457631876 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ___settings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ApplySerializerSettings(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  void JsonSerializer_ApplySerializerSettings_m735936747 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ___serializer0, JsonSerializerSettings_t2589405525 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializer_Deserialize_m2134755442 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonSerializer::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializer_DeserializeInternal_m674059343 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::SetupReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo&,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>&,System.Nullable`1<Newtonsoft.Json.DateParseHandling>&,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>&,System.Nullable`1<System.Int32>&,System.String&)
extern "C"  void JsonSerializer_SetupReader_m762076749 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, CultureInfo_t1065375142 ** ___previousCulture1, Nullable_1_t3029687007 * ___previousDateTimeZoneHandling2, Nullable_1_t2192459923 * ___previousDateParseHandling3, Nullable_1_t3158207471 * ___previousFloatParseHandling4, Nullable_1_t1237965023 * ___previousMaxDepth5, String_t** ___previousDateFormatString6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::ResetReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>,System.Nullable`1<Newtonsoft.Json.DateParseHandling>,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>,System.Nullable`1<System.Int32>,System.String)
extern "C"  void JsonSerializer_ResetReader_m3392788727 (JsonSerializer_t251850770 * __this, JsonReader_t816925123 * ___reader0, CultureInfo_t1065375142 * ___previousCulture1, Nullable_1_t3029687007  ___previousDateTimeZoneHandling2, Nullable_1_t2192459923  ___previousDateParseHandling3, Nullable_1_t3158207471  ___previousFloatParseHandling4, Nullable_1_t1237965023  ___previousMaxDepth5, String_t* ___previousDateFormatString6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern "C"  void JsonSerializer_Serialize_m635289798 (JsonSerializer_t251850770 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializer_Serialize_m2543171091 (JsonSerializer_t251850770 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern "C"  void JsonSerializer_SerializeInternal_m2393763811 (JsonSerializer_t251850770 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::GetReferenceResolver()
extern "C"  Il2CppObject * JsonSerializer_GetReferenceResolver_m516679456 (JsonSerializer_t251850770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializer_GetMatchingConverter_m2011720879 (JsonSerializer_t251850770 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializer_GetMatchingConverter_m109378829 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___converters0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonSerializer::OnError(Newtonsoft.Json.Serialization.ErrorEventArgs)
extern "C"  void JsonSerializer_OnError_m301313148 (JsonSerializer_t251850770 * __this, ErrorEventArgs_t792639131 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
