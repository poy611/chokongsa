﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0
struct U3CU3Ec__DisplayClass73_0_t2259160450;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass73_0__ctor_m4096299620 (U3CU3Ec__DisplayClass73_0_t2259160450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0::<CreateSerializationCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void U3CU3Ec__DisplayClass73_0_U3CCreateSerializationCallbackU3Eb__0_m1782072943 (U3CU3Ec__DisplayClass73_0_t2259160450 * __this, Il2CppObject * ___o0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
