﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67
struct U3CHelperForSessionU3Ec__AnonStorey67_t3144318968;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t3104490121;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N3104490121.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3337232325.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67::.ctor()
extern "C"  void U3CHelperForSessionU3Ec__AnonStorey67__ctor_m2719925667 (U3CHelperForSessionU3Ec__AnonStorey67_t3144318968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67::<>m__42(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C"  void U3CHelperForSessionU3Ec__AnonStorey67_U3CU3Em__42_m500700898 (U3CHelperForSessionU3Ec__AnonStorey67_t3144318968 * __this, NativeRealTimeRoom_t3104490121 * ___room0, MultiplayerParticipant_t3337232325 * ___participant1, ByteU5BU5D_t4260760469* ___data2, bool ___isReliable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67::<>m__43(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C"  void U3CHelperForSessionU3Ec__AnonStorey67_U3CU3Em__43_m2870375767 (U3CHelperForSessionU3Ec__AnonStorey67_t3144318968 * __this, NativeRealTimeRoom_t3104490121 * ___room0, MultiplayerParticipant_t3337232325 * ___participant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67::<>m__44(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C"  void U3CHelperForSessionU3Ec__AnonStorey67_U3CU3Em__44_m1915899046 (U3CHelperForSessionU3Ec__AnonStorey67_t3144318968 * __this, NativeRealTimeRoom_t3104490121 * ___room0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey67::<>m__45(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C"  void U3CHelperForSessionU3Ec__AnonStorey67_U3CU3Em__45_m2685069061 (U3CHelperForSessionU3Ec__AnonStorey67_t3144318968 * __this, NativeRealTimeRoom_t3104490121 * ___room0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
