﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// UnityEngine.Animator
struct Animator_t2776330603;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
struct  NavMeshAgentAnimatorSynchronizer_t2695016998  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_animator
	Animator_t2776330603 * ____animator_12;
	// UnityEngine.NavMeshAgent HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_agent
	NavMeshAgent_t588466745 * ____agent_13;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_trans
	Transform_t1659122786 * ____trans_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of__animator_12() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____animator_12)); }
	inline Animator_t2776330603 * get__animator_12() const { return ____animator_12; }
	inline Animator_t2776330603 ** get_address_of__animator_12() { return &____animator_12; }
	inline void set__animator_12(Animator_t2776330603 * value)
	{
		____animator_12 = value;
		Il2CppCodeGenWriteBarrier(&____animator_12, value);
	}

	inline static int32_t get_offset_of__agent_13() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____agent_13)); }
	inline NavMeshAgent_t588466745 * get__agent_13() const { return ____agent_13; }
	inline NavMeshAgent_t588466745 ** get_address_of__agent_13() { return &____agent_13; }
	inline void set__agent_13(NavMeshAgent_t588466745 * value)
	{
		____agent_13 = value;
		Il2CppCodeGenWriteBarrier(&____agent_13, value);
	}

	inline static int32_t get_offset_of__trans_14() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t2695016998, ____trans_14)); }
	inline Transform_t1659122786 * get__trans_14() const { return ____trans_14; }
	inline Transform_t1659122786 ** get_address_of__trans_14() { return &____trans_14; }
	inline void set__trans_14(Transform_t1659122786 * value)
	{
		____trans_14 = value;
		Il2CppCodeGenWriteBarrier(&____trans_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
