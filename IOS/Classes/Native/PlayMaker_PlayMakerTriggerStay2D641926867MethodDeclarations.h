﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerStay2D
struct PlayMakerTriggerStay2D_t641926867;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void PlayMakerTriggerStay2D::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerStay2D_OnTriggerStay2D_m314707215 (PlayMakerTriggerStay2D_t641926867 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerStay2D::.ctor()
extern "C"  void PlayMakerTriggerStay2D__ctor_m3403511146 (PlayMakerTriggerStay2D_t641926867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
