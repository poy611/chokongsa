﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Add
struct Vector3Add_t2795816089;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Add::.ctor()
extern "C"  void Vector3Add__ctor_m1316498413 (Vector3Add_t2795816089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::Reset()
extern "C"  void Vector3Add_Reset_m3257898650 (Vector3Add_t2795816089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::OnEnter()
extern "C"  void Vector3Add_OnEnter_m1826318468 (Vector3Add_t2795816089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::OnUpdate()
extern "C"  void Vector3Add_OnUpdate_m4209824319 (Vector3Add_t2795816089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::DoVector3Add()
extern "C"  void Vector3Add_DoVector3Add_m3705753811 (Vector3Add_t2795816089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
