﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Cach1288273172MethodDeclarations.h"

// T Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataMemberAttribute>::GetAttribute(System.Object)
#define CachedAttributeGetter_1_GetAttribute_m3606221833(__this /* static, unused */, ___type0, method) ((  DataMemberAttribute_t2601848894 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CachedAttributeGetter_1_GetAttribute_m3559125336_gshared)(__this /* static, unused */, ___type0, method)
// System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1<System.Runtime.Serialization.DataMemberAttribute>::.cctor()
#define CachedAttributeGetter_1__cctor_m777549581(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))CachedAttributeGetter_1__cctor_m3626143424_gshared)(__this /* static, unused */, method)
