﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1718923157MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2402345181(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4080658382 *, Dictionary_2_t2763334990 *, const MethodInfo*))Enumerator__ctor_m2838867140_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4090084196(__this, method) ((  Il2CppObject * (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2596440103_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m707084216(__this, method) ((  void (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4146917745_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1629031489(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1595280872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3721387328(__this, method) ((  Il2CppObject * (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m761187267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3888868306(__this, method) ((  Il2CppObject * (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2404959637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::MoveNext()
#define Enumerator_MoveNext_m1468651684(__this, method) ((  bool (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_MoveNext_m626881441_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::get_Current()
#define Enumerator_get_Current_m1120368396(__this, method) ((  KeyValuePair_2_t2662115696  (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_get_Current_m2672115835_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4177508721(__this, method) ((  HandleRef_t1780819301  (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_get_CurrentKey_m3344151658_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2228991445(__this, method) ((  BaseReferenceHolder_t2237584300 * (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_get_CurrentValue_m1179426602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::Reset()
#define Enumerator_Reset_m883654575(__this, method) ((  void (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_Reset_m2660327702_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::VerifyState()
#define Enumerator_VerifyState_m1531318392(__this, method) ((  void (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_VerifyState_m2820737183_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1168063968(__this, method) ((  void (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_VerifyCurrent_m3348940871_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::Dispose()
#define Enumerator_Dispose_m1311951039(__this, method) ((  void (*) (Enumerator_t4080658382 *, const MethodInfo*))Enumerator_Dispose_m3592809574_gshared)(__this, method)
