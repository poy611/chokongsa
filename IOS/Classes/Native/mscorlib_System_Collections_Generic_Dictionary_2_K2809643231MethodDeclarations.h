﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t2194707177;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2809643231.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2367230413_gshared (Enumerator_t2809643231 * __this, Dictionary_2_t2194707177 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2367230413(__this, ___host0, method) ((  void (*) (Enumerator_t2809643231 *, Dictionary_2_t2194707177 *, const MethodInfo*))Enumerator__ctor_m2367230413_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3651918132_gshared (Enumerator_t2809643231 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3651918132(__this, method) ((  Il2CppObject * (*) (Enumerator_t2809643231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3651918132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4126300360_gshared (Enumerator_t2809643231 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4126300360(__this, method) ((  void (*) (Enumerator_t2809643231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4126300360_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m882513327_gshared (Enumerator_t2809643231 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m882513327(__this, method) ((  void (*) (Enumerator_t2809643231 *, const MethodInfo*))Enumerator_Dispose_m882513327_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3136271668_gshared (Enumerator_t2809643231 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3136271668(__this, method) ((  bool (*) (Enumerator_t2809643231 *, const MethodInfo*))Enumerator_MoveNext_m3136271668_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3192681888_gshared (Enumerator_t2809643231 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3192681888(__this, method) ((  Il2CppObject * (*) (Enumerator_t2809643231 *, const MethodInfo*))Enumerator_get_Current_m3192681888_gshared)(__this, method)
