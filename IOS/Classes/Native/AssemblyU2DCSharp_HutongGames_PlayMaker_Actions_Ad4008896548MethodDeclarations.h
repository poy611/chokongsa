﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddForce2d
struct AddForce2d_t4008896548;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddForce2d::.ctor()
extern "C"  void AddForce2d__ctor_m742511810 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::Reset()
extern "C"  void AddForce2d_Reset_m2683912047 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnPreprocess()
extern "C"  void AddForce2d_OnPreprocess_m2056898093 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnEnter()
extern "C"  void AddForce2d_OnEnter_m4275974169 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnFixedUpdate()
extern "C"  void AddForce2d_OnFixedUpdate_m3564540254 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::DoAddForce()
extern "C"  void AddForce2d_DoAddForce_m4291546807 (AddForce2d_t4008896548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
