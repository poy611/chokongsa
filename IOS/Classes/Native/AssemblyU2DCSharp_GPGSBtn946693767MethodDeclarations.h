﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPGSBtn
struct GPGSBtn_t946693767;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void GPGSBtn::.ctor()
extern "C"  void GPGSBtn__ctor_m3123261636 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn::Awake()
extern "C"  void GPGSBtn_Awake_m3360866855 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn::SetOnInit()
extern "C"  void GPGSBtn_SetOnInit_m118166451 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPGSBtn::LoginChecker()
extern "C"  Il2CppObject * GPGSBtn_LoginChecker_m4107403972 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPGSBtn::LoginCheckerNet()
extern "C"  Il2CppObject * GPGSBtn_LoginCheckerNet_m90460827 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn::ClickEvent()
extern "C"  void GPGSBtn_ClickEvent_m2313098802 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn::SettingUser()
extern "C"  void GPGSBtn_SettingUser_m1225378973 (GPGSBtn_t946693767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
