﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonContainerContract
struct JsonContainerContract_t3227540113;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "mscorlib_System_Type2863145774.h"

// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemContract()
extern "C"  JsonContract_t1328848902 * JsonContainerContract_get_ItemContract_m2333378148 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemContract(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonContainerContract_set_ItemContract_m203637169 (JsonContainerContract_t3227540113 * __this, JsonContract_t1328848902 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_FinalItemContract()
extern "C"  JsonContract_t1328848902 * JsonContainerContract_get_FinalItemContract_m796939646 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemConverter()
extern "C"  JsonConverter_t2159686854 * JsonContainerContract_get_ItemConverter_m3625260858 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonContainerContract_set_ItemConverter_m1486305081 (JsonContainerContract_t3227540113 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemIsReference()
extern "C"  Nullable_1_t560925241  JsonContainerContract_get_ItemIsReference_m234450900 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonContainerContract_set_ItemIsReference_m2937472625 (JsonContainerContract_t3227540113 * __this, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemReferenceLoopHandling()
extern "C"  Nullable_1_t2845787645  JsonContainerContract_get_ItemReferenceLoopHandling_m3499171911 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern "C"  void JsonContainerContract_set_ItemReferenceLoopHandling_m3672676128 (JsonContainerContract_t3227540113 * __this, Nullable_1_t2845787645  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemTypeNameHandling()
extern "C"  Nullable_1_t2443451997  JsonContainerContract_get_ItemTypeNameHandling_m1724156739 (JsonContainerContract_t3227540113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void JsonContainerContract_set_ItemTypeNameHandling_m2323198216 (JsonContainerContract_t3227540113 * __this, Nullable_1_t2443451997  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonContainerContract::.ctor(System.Type)
extern "C"  void JsonContainerContract__ctor_m4244057169 (JsonContainerContract_t3227540113 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
