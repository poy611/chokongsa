﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2PerSecond
struct Vector2PerSecond_t1744921866;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::.ctor()
extern "C"  void Vector2PerSecond__ctor_m2778171548 (Vector2PerSecond_t1744921866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::Reset()
extern "C"  void Vector2PerSecond_Reset_m424604489 (Vector2PerSecond_t1744921866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::OnEnter()
extern "C"  void Vector2PerSecond_OnEnter_m2039895411 (Vector2PerSecond_t1744921866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::OnUpdate()
extern "C"  void Vector2PerSecond_OnUpdate_m2240774960 (Vector2PerSecond_t1744921866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
