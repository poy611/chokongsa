﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2024483409MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3932695800(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4254833994 *, Dictionary_2_t1259260985 *, const MethodInfo*))ValueCollection__ctor_m2532250131_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1658884154(__this, ___item0, method) ((  void (*) (ValueCollection_t4254833994 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1336613823_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m122399235(__this, method) ((  void (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3578445832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2609953904(__this, ___item0, method) ((  bool (*) (ValueCollection_t4254833994 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m559088523_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1867395349(__this, ___item0, method) ((  bool (*) (ValueCollection_t4254833994 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3416097520_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3751402499(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2678085320_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2401493831(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4254833994 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3124164364_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2979890582(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m207982107_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m885129763(__this, method) ((  bool (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3129231678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2176328835(__this, method) ((  bool (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2385509406_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3020774901(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1611904272_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3238622015(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4254833994 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3524503962_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1305419432(__this, method) ((  Enumerator_t3486061689  (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_GetEnumerator_m3215728515_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.Int32>::get_Count()
#define ValueCollection_get_Count_m3196730685(__this, method) ((  int32_t (*) (ValueCollection_t4254833994 *, const MethodInfo*))ValueCollection_get_Count_m3355151704_gshared)(__this, method)
