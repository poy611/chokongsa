﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct ThreadSafeStore_2_t648438005;
// System.Func`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Func_2_t2253398629;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Func`2<TKey,TValue>)
extern "C"  void ThreadSafeStore_2__ctor_m713611997_gshared (ThreadSafeStore_2_t648438005 * __this, Func_2_t2253398629 * ___creator0, const MethodInfo* method);
#define ThreadSafeStore_2__ctor_m713611997(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t648438005 *, Func_2_t2253398629 *, const MethodInfo*))ThreadSafeStore_2__ctor_m713611997_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_Get_m2486152451_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method);
#define ThreadSafeStore_2_Get_m2486152451(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t648438005 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_Get_m2486152451_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeStore_2_AddValue_m3702308719_gshared (ThreadSafeStore_2_t648438005 * __this, TypeNameKey_t2971844791  ___key0, const MethodInfo* method);
#define ThreadSafeStore_2_AddValue_m3702308719(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeStore_2_t648438005 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_AddValue_m3702308719_gshared)(__this, ___key0, method)
