﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct GetAnimatorDelta_t946274563;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::.ctor()
extern "C"  void GetAnimatorDelta__ctor_m2311017667 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::Reset()
extern "C"  void GetAnimatorDelta_Reset_m4252417904 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnEnter()
extern "C"  void GetAnimatorDelta_OnEnter_m4076581850 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnActionUpdate()
extern "C"  void GetAnimatorDelta_OnActionUpdate_m2662918847 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::DoGetDeltaPosition()
extern "C"  void GetAnimatorDelta_DoGetDeltaPosition_m84291223 (GetAnimatorDelta_t946274563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
