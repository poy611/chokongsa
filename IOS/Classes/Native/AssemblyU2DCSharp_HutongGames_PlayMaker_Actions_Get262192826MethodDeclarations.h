﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetDeviceAcceleration
struct GetDeviceAcceleration_t262192826;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::.ctor()
extern "C"  void GetDeviceAcceleration__ctor_m4100135868 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::Reset()
extern "C"  void GetDeviceAcceleration_Reset_m1746568809 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnEnter()
extern "C"  void GetDeviceAcceleration_OnEnter_m1137287315 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnUpdate()
extern "C"  void GetDeviceAcceleration_OnUpdate_m29727760 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::DoGetDeviceAcceleration()
extern "C"  void GetDeviceAcceleration_DoGetDeviceAcceleration_m2607253627 (GetDeviceAcceleration_t262192826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
