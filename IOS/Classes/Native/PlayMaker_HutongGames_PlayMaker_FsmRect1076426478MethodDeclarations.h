﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// UnityEngine.Rect HutongGames.PlayMaker.FsmRect::get_Value()
extern "C"  Rect_t4241904616  FsmRect_get_Value_m1002500317 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::set_Value(UnityEngine.Rect)
extern "C"  void FsmRect_set_Value_m1159518952 (FsmRect_t1076426478 * __this, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmRect::get_RawValue()
extern "C"  Il2CppObject * FsmRect_get_RawValue_m2451016656 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::set_RawValue(System.Object)
extern "C"  void FsmRect_set_RawValue_m4042602459 (FsmRect_t1076426478 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor()
extern "C"  void FsmRect__ctor_m2674586289 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor(System.String)
extern "C"  void FsmRect__ctor_m3861402609 (FsmRect_t1076426478 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor(HutongGames.PlayMaker.FsmRect)
extern "C"  void FsmRect__ctor_m2972064873 (FsmRect_t1076426478 * __this, FsmRect_t1076426478 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmRect::Clone()
extern "C"  NamedVariable_t3211770239 * FsmRect_Clone_m1191609430 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmRect::get_VariableType()
extern "C"  int32_t FsmRect_get_VariableType_m1768343827 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmRect::ToString()
extern "C"  String_t* FsmRect_ToString_m2280808348 (FsmRect_t1076426478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmRect::op_Implicit(UnityEngine.Rect)
extern "C"  FsmRect_t1076426478 * FsmRect_op_Implicit_m872084402 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
