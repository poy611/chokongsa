﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AudioStop
struct AudioStop_t298729542;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AudioStop::.ctor()
extern "C"  void AudioStop__ctor_m1805366192 (AudioStop_t298729542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioStop::Reset()
extern "C"  void AudioStop_Reset_m3746766429 (AudioStop_t298729542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioStop::OnEnter()
extern "C"  void AudioStop_OnEnter_m3476818823 (AudioStop_t298729542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
