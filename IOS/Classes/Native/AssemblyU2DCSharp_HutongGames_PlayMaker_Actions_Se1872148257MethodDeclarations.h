﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIsKinematic
struct SetIsKinematic_t1872148257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::.ctor()
extern "C"  void SetIsKinematic__ctor_m2920400485 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::Reset()
extern "C"  void SetIsKinematic_Reset_m566833426 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::OnEnter()
extern "C"  void SetIsKinematic_OnEnter_m1282950396 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::DoSetIsKinematic()
extern "C"  void SetIsKinematic_DoSetIsKinematic_m2196068835 (SetIsKinematic_t1872148257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
