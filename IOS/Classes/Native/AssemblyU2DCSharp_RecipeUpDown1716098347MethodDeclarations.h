﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecipeUpDown
struct RecipeUpDown_t1716098347;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void RecipeUpDown::.ctor()
extern "C"  void RecipeUpDown__ctor_m3939237328 (RecipeUpDown_t1716098347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown::Up()
extern "C"  void RecipeUpDown_Up_m1085606415 (RecipeUpDown_t1716098347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecipeUpDown::powerUp()
extern "C"  void RecipeUpDown_powerUp_m229524046 (RecipeUpDown_t1716098347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RecipeUpDown::GoUP()
extern "C"  Il2CppObject * RecipeUpDown_GoUP_m1940154495 (RecipeUpDown_t1716098347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RecipeUpDown::GoDown()
extern "C"  Il2CppObject * RecipeUpDown_GoDown_m17017894 (RecipeUpDown_t1716098347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
