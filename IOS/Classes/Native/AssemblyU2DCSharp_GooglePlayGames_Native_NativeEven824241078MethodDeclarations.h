﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey5D
struct U3CFetchAllEventsU3Ec__AnonStorey5D_t824241078;
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct FetchAllResponse_t1890238433;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_E1890238433.h"

// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey5D::.ctor()
extern "C"  void U3CFetchAllEventsU3Ec__AnonStorey5D__ctor_m800186277 (U3CFetchAllEventsU3Ec__AnonStorey5D_t824241078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey5D::<>m__3A(GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse)
extern "C"  void U3CFetchAllEventsU3Ec__AnonStorey5D_U3CU3Em__3A_m2647371227 (U3CFetchAllEventsU3Ec__AnonStorey5D_t824241078 * __this, FetchAllResponse_t1890238433 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
