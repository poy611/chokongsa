﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextLineCast2d
struct GetNextLineCast2d_t3332969372;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::.ctor()
extern "C"  void GetNextLineCast2d__ctor_m3834173722 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::Reset()
extern "C"  void GetNextLineCast2d_Reset_m1480606663 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::OnEnter()
extern "C"  void GetNextLineCast2d_OnEnter_m3245702769 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::DoGetNextCollider()
extern "C"  void GetNextLineCast2d_DoGetNextCollider_m3529992618 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextLineCast2d::GetLineCastAll()
extern "C"  RaycastHit2DU5BU5D_t889400257* GetNextLineCast2d_GetLineCastAll_m1089683514 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
