﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointMotor2D
struct JointMotor2D_t682576033;
struct JointMotor2D_t682576033_marshaled_pinvoke;
struct JointMotor2D_t682576033_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"

// System.Void UnityEngine.JointMotor2D::set_motorSpeed(System.Single)
extern "C"  void JointMotor2D_set_motorSpeed_m3632600456 (JointMotor2D_t682576033 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointMotor2D::set_maxMotorTorque(System.Single)
extern "C"  void JointMotor2D_set_maxMotorTorque_m1907539039 (JointMotor2D_t682576033 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointMotor2D_t682576033;
struct JointMotor2D_t682576033_marshaled_pinvoke;

extern "C" void JointMotor2D_t682576033_marshal_pinvoke(const JointMotor2D_t682576033& unmarshaled, JointMotor2D_t682576033_marshaled_pinvoke& marshaled);
extern "C" void JointMotor2D_t682576033_marshal_pinvoke_back(const JointMotor2D_t682576033_marshaled_pinvoke& marshaled, JointMotor2D_t682576033& unmarshaled);
extern "C" void JointMotor2D_t682576033_marshal_pinvoke_cleanup(JointMotor2D_t682576033_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointMotor2D_t682576033;
struct JointMotor2D_t682576033_marshaled_com;

extern "C" void JointMotor2D_t682576033_marshal_com(const JointMotor2D_t682576033& unmarshaled, JointMotor2D_t682576033_marshaled_com& marshaled);
extern "C" void JointMotor2D_t682576033_marshal_com_back(const JointMotor2D_t682576033_marshaled_com& marshaled, JointMotor2D_t682576033& unmarshaled);
extern "C" void JointMotor2D_t682576033_marshal_com_cleanup(JointMotor2D_t682576033_marshaled_com& marshaled);
