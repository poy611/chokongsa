﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmRect
struct GetFsmRect_t3246916726;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::.ctor()
extern "C"  void GetFsmRect__ctor_m3107091632 (GetFsmRect_t3246916726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::Reset()
extern "C"  void GetFsmRect_Reset_m753524573 (GetFsmRect_t3246916726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::OnEnter()
extern "C"  void GetFsmRect_OnEnter_m304516231 (GetFsmRect_t3246916726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::OnUpdate()
extern "C"  void GetFsmRect_OnUpdate_m4278595228 (GetFsmRect_t3246916726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::DoGetFsmVariable()
extern "C"  void GetFsmRect_DoGetFsmVariable_m1948561893 (GetFsmRect_t3246916726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
