﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3857037258.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788  : public Il2CppObject
{
public:
	// T1 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2::val1
	int32_t ___val1_0;
	// T2 GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2::val2
	Il2CppObject * ___val2_1;
	// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2<T1,T2> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2::<>f__ref$64
	U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * ___U3CU3Ef__refU2464_2;

public:
	inline static int32_t get_offset_of_val1_0() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788, ___val1_0)); }
	inline int32_t get_val1_0() const { return ___val1_0; }
	inline int32_t* get_address_of_val1_0() { return &___val1_0; }
	inline void set_val1_0(int32_t value)
	{
		___val1_0 = value;
	}

	inline static int32_t get_offset_of_val2_1() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788, ___val2_1)); }
	inline Il2CppObject * get_val2_1() const { return ___val2_1; }
	inline Il2CppObject ** get_address_of_val2_1() { return &___val2_1; }
	inline void set_val2_1(Il2CppObject * value)
	{
		___val2_1 = value;
		Il2CppCodeGenWriteBarrier(&___val2_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2464_2() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey41_2_t13892788, ___U3CU3Ef__refU2464_2)); }
	inline U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * get_U3CU3Ef__refU2464_2() const { return ___U3CU3Ef__refU2464_2; }
	inline U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 ** get_address_of_U3CU3Ef__refU2464_2() { return &___U3CU3Ef__refU2464_2; }
	inline void set_U3CU3Ef__refU2464_2(U3CToOnGameThreadU3Ec__AnonStorey40_2_t1259290051 * value)
	{
		___U3CU3Ef__refU2464_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2464_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
