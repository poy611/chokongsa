﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonManager
struct ButtonManager_t2890896379;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonManager::.ctor()
extern "C"  void ButtonManager__ctor_m1396840400 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::Awake()
extern "C"  void ButtonManager_Awake_m1634445619 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::Start()
extern "C"  void ButtonManager_Start_m343978192 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::OnSuccess()
extern "C"  void ButtonManager_OnSuccess_m2208122514 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::OnFail()
extern "C"  void ButtonManager_OnFail_m1873078545 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::NoInfo()
extern "C"  void ButtonManager_NoInfo_m1115578627 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::start()
extern "C"  void ButtonManager_start_m2974292208 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::minigame()
extern "C"  void ButtonManager_minigame_m3016780893 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::option()
extern "C"  void ButtonManager_option_m612984265 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::study()
extern "C"  void ButtonManager_study_m2992350359 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::ResultPage()
extern "C"  void ButtonManager_ResultPage_m1503714944 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::NotLogin()
extern "C"  void ButtonManager_NotLogin_m397561034 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::exit()
extern "C"  void ButtonManager_exit_m114710770 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonManager::OnDestroy()
extern "C"  void ButtonManager_OnDestroy_m3248719049 (ButtonManager_t2890896379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
