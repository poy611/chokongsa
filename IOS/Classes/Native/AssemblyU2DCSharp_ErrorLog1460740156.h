﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// NetworkMng
struct NetworkMng_t1515215352;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorLog
struct  ErrorLog_t1460740156  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text ErrorLog::text
	Text_t9039225 * ___text_2;
	// NetworkMng ErrorLog::net
	NetworkMng_t1515215352 * ___net_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(ErrorLog_t1460740156, ___text_2)); }
	inline Text_t9039225 * get_text_2() const { return ___text_2; }
	inline Text_t9039225 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t9039225 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_net_3() { return static_cast<int32_t>(offsetof(ErrorLog_t1460740156, ___net_3)); }
	inline NetworkMng_t1515215352 * get_net_3() const { return ___net_3; }
	inline NetworkMng_t1515215352 ** get_address_of_net_3() { return &___net_3; }
	inline void set_net_3(NetworkMng_t1515215352 * value)
	{
		___net_3 = value;
		Il2CppCodeGenWriteBarrier(&___net_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
