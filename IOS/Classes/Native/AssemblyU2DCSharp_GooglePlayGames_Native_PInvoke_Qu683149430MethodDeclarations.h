﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
struct ClaimMilestoneResponse_t683149430;
// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;
// GooglePlayGames.Native.PInvoke.NativeQuestMilestone
struct NativeQuestMilestone_t2033850801;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1924926087.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::.ctor(System.IntPtr)
extern "C"  void ClaimMilestoneResponse__ctor_m3914888079 (ClaimMilestoneResponse_t683149430 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ResponseStatus()
extern "C"  int32_t ClaimMilestoneResponse_ResponseStatus_m896481534 (ClaimMilestoneResponse_t683149430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::Quest()
extern "C"  NativeQuest_t2496300529 * ClaimMilestoneResponse_Quest_m670471696 (ClaimMilestoneResponse_t683149430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ClaimedMilestone()
extern "C"  NativeQuestMilestone_t2033850801 * ClaimMilestoneResponse_ClaimedMilestone_m1051602847 (ClaimMilestoneResponse_t683149430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::RequestSucceeded()
extern "C"  bool ClaimMilestoneResponse_RequestSucceeded_m2442679941 (ClaimMilestoneResponse_t683149430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ClaimMilestoneResponse_CallDispose_m1348356097 (ClaimMilestoneResponse_t683149430 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::FromPointer(System.IntPtr)
extern "C"  ClaimMilestoneResponse_t683149430 * ClaimMilestoneResponse_FromPointer_m2886505893 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
