﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2245389293MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2359618273(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t199646394 *, Dictionary_2_t1499040681 *, const MethodInfo*))ValueCollection__ctor_m440413687_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m915403057(__this, ___item0, method) ((  void (*) (ValueCollection_t199646394 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m160756571_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m264773242(__this, method) ((  void (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1596507556_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3040239449(__this, ___item0, method) ((  bool (*) (ValueCollection_t199646394 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2005047919_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2426501694(__this, ___item0, method) ((  bool (*) (ValueCollection_t199646394 *, RaycastHit2D_t1374744384 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3989668564_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m703838266(__this, method) ((  Il2CppObject* (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3156224740_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m65957630(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t199646394 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m963619240_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4270825613(__this, method) ((  Il2CppObject * (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3107242167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1315415308(__this, method) ((  bool (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m280223778_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m701766892(__this, method) ((  bool (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3439315202_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1795678046(__this, method) ((  Il2CppObject * (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1840810100_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4119805672(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t199646394 *, RaycastHit2DU5BU5D_t889400257*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m183626878_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1955204177(__this, method) ((  Enumerator_t3725841385  (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_GetEnumerator_m1027386599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Count()
#define ValueCollection_get_Count_m3206800934(__this, method) ((  int32_t (*) (ValueCollection_t199646394 *, const MethodInfo*))ValueCollection_get_Count_m4040439612_gshared)(__this, method)
