﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsSleeping
struct IsSleeping_t599267933;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsSleeping::.ctor()
extern "C"  void IsSleeping__ctor_m1099895465 (IsSleeping_t599267933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::Reset()
extern "C"  void IsSleeping_Reset_m3041295702 (IsSleeping_t599267933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnEnter()
extern "C"  void IsSleeping_OnEnter_m4124282944 (IsSleeping_t599267933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnUpdate()
extern "C"  void IsSleeping_OnUpdate_m2432279043 (IsSleeping_t599267933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::DoIsSleeping()
extern "C"  void IsSleeping_DoIsSleeping_m2186122075 (IsSleeping_t599267933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
