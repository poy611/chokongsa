﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutIntField
struct GUILayoutIntField_t3766456830;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::.ctor()
extern "C"  void GUILayoutIntField__ctor_m624781560 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::Reset()
extern "C"  void GUILayoutIntField_Reset_m2566181797 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::OnGUI()
extern "C"  void GUILayoutIntField_OnGUI_m120180210 (GUILayoutIntField_t3766456830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
