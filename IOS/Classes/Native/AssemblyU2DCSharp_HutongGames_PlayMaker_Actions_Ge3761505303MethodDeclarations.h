﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetControllerHitInfo
struct GetControllerHitInfo_t3761505303;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::.ctor()
extern "C"  void GetControllerHitInfo__ctor_m1323482159 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::Reset()
extern "C"  void GetControllerHitInfo_Reset_m3264882396 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::OnPreprocess()
extern "C"  void GetControllerHitInfo_OnPreprocess_m2388072992 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::StoreTriggerInfo()
extern "C"  void GetControllerHitInfo_StoreTriggerInfo_m16578266 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerHitInfo::OnEnter()
extern "C"  void GetControllerHitInfo_OnEnter_m4242731078 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.GetControllerHitInfo::ErrorCheck()
extern "C"  String_t* GetControllerHitInfo_ErrorCheck_m489327160 (GetControllerHitInfo_t3761505303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
