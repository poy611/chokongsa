﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3053988647(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t547493001 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m420802513_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::Invoke(T)
#define Func_2_Invoke_m2655335919(__this, ___arg10, method) ((  Participant_t1804230813 * (*) (Func_2_t547493001 *, MultiplayerParticipant_t3337232325 *, const MethodInfo*))Func_2_Invoke_m1009799647_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1819765154(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t547493001 *, MultiplayerParticipant_t3337232325 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m4096485701(__this, ___result0, method) ((  Participant_t1804230813 * (*) (Func_2_t547493001 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
