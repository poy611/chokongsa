﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTan
struct GetTan_t2986517587;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTan::.ctor()
extern "C"  void GetTan__ctor_m2574264691 (GetTan_t2986517587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::Reset()
extern "C"  void GetTan_Reset_m220697632 (GetTan_t2986517587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnEnter()
extern "C"  void GetTan_OnEnter_m3653901450 (GetTan_t2986517587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnUpdate()
extern "C"  void GetTan_OnUpdate_m735354617 (GetTan_t2986517587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::DoTan()
extern "C"  void GetTan_DoTan_m938085415 (GetTan_t2986517587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
