﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct HashSet_1_t2292215702;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2679459368.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m610354150_gshared (Enumerator_t2679459368 * __this, HashSet_1_t2292215702 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m610354150(__this, ___hashset0, method) ((  void (*) (Enumerator_t2679459368 *, HashSet_1_t2292215702 *, const MethodInfo*))Enumerator__ctor_m610354150_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2818389684_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2818389684(__this, method) ((  Il2CppObject * (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2818389684_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1809599038_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1809599038(__this, method) ((  void (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1809599038_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2747945216_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2747945216(__this, method) ((  bool (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_MoveNext_m2747945216_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3291611339_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3291611339(__this, method) ((  int32_t (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_get_Current_m3291611339_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Dispose()
extern "C"  void Enumerator_Dispose_m1224855673_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1224855673(__this, method) ((  void (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_Dispose_m1224855673_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CheckState()
extern "C"  void Enumerator_CheckState_m1252159345_gshared (Enumerator_t2679459368 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m1252159345(__this, method) ((  void (*) (Enumerator_t2679459368 *, const MethodInfo*))Enumerator_CheckState_m1252159345_gshared)(__this, method)
