﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ScoreManager_Temping
struct ScoreManager_Temping_t3029572618;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TempingPointer
struct  TempingPointer_t2678149871  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera TempingPointer::_camera
	Camera_t2727095145 * ____camera_2;
	// PlayMakerFSM TempingPointer::_myFsm
	PlayMakerFSM_t3799847376 * ____myFsm_3;
	// UnityEngine.Transform TempingPointer::_cameraPos
	Transform_t1659122786 * ____cameraPos_4;
	// UnityEngine.Transform TempingPointer::_lookTarget
	Transform_t1659122786 * ____lookTarget_5;
	// UnityEngine.Transform TempingPointer::_filterTarget
	Transform_t1659122786 * ____filterTarget_6;
	// UnityEngine.Transform TempingPointer::_filterDown
	Transform_t1659122786 * ____filterDown_7;
	// UnityEngine.GameObject TempingPointer::Temper
	GameObject_t3674682005 * ___Temper_8;
	// ScoreManager_Temping TempingPointer::_tempingMng
	ScoreManager_Temping_t3029572618 * ____tempingMng_9;
	// UnityEngine.Transform TempingPointer::gaugePoint
	Transform_t1659122786 * ___gaugePoint_10;
	// UnityEngine.Vector3 TempingPointer::FirstGaugeTrans
	Vector3_t4282066566  ___FirstGaugeTrans_11;
	// System.Single TempingPointer::speed
	float ___speed_12;
	// System.Single TempingPointer::speedDown
	float ___speedDown_13;

public:
	inline static int32_t get_offset_of__camera_2() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____camera_2)); }
	inline Camera_t2727095145 * get__camera_2() const { return ____camera_2; }
	inline Camera_t2727095145 ** get_address_of__camera_2() { return &____camera_2; }
	inline void set__camera_2(Camera_t2727095145 * value)
	{
		____camera_2 = value;
		Il2CppCodeGenWriteBarrier(&____camera_2, value);
	}

	inline static int32_t get_offset_of__myFsm_3() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____myFsm_3)); }
	inline PlayMakerFSM_t3799847376 * get__myFsm_3() const { return ____myFsm_3; }
	inline PlayMakerFSM_t3799847376 ** get_address_of__myFsm_3() { return &____myFsm_3; }
	inline void set__myFsm_3(PlayMakerFSM_t3799847376 * value)
	{
		____myFsm_3 = value;
		Il2CppCodeGenWriteBarrier(&____myFsm_3, value);
	}

	inline static int32_t get_offset_of__cameraPos_4() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____cameraPos_4)); }
	inline Transform_t1659122786 * get__cameraPos_4() const { return ____cameraPos_4; }
	inline Transform_t1659122786 ** get_address_of__cameraPos_4() { return &____cameraPos_4; }
	inline void set__cameraPos_4(Transform_t1659122786 * value)
	{
		____cameraPos_4 = value;
		Il2CppCodeGenWriteBarrier(&____cameraPos_4, value);
	}

	inline static int32_t get_offset_of__lookTarget_5() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____lookTarget_5)); }
	inline Transform_t1659122786 * get__lookTarget_5() const { return ____lookTarget_5; }
	inline Transform_t1659122786 ** get_address_of__lookTarget_5() { return &____lookTarget_5; }
	inline void set__lookTarget_5(Transform_t1659122786 * value)
	{
		____lookTarget_5 = value;
		Il2CppCodeGenWriteBarrier(&____lookTarget_5, value);
	}

	inline static int32_t get_offset_of__filterTarget_6() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____filterTarget_6)); }
	inline Transform_t1659122786 * get__filterTarget_6() const { return ____filterTarget_6; }
	inline Transform_t1659122786 ** get_address_of__filterTarget_6() { return &____filterTarget_6; }
	inline void set__filterTarget_6(Transform_t1659122786 * value)
	{
		____filterTarget_6 = value;
		Il2CppCodeGenWriteBarrier(&____filterTarget_6, value);
	}

	inline static int32_t get_offset_of__filterDown_7() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____filterDown_7)); }
	inline Transform_t1659122786 * get__filterDown_7() const { return ____filterDown_7; }
	inline Transform_t1659122786 ** get_address_of__filterDown_7() { return &____filterDown_7; }
	inline void set__filterDown_7(Transform_t1659122786 * value)
	{
		____filterDown_7 = value;
		Il2CppCodeGenWriteBarrier(&____filterDown_7, value);
	}

	inline static int32_t get_offset_of_Temper_8() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ___Temper_8)); }
	inline GameObject_t3674682005 * get_Temper_8() const { return ___Temper_8; }
	inline GameObject_t3674682005 ** get_address_of_Temper_8() { return &___Temper_8; }
	inline void set_Temper_8(GameObject_t3674682005 * value)
	{
		___Temper_8 = value;
		Il2CppCodeGenWriteBarrier(&___Temper_8, value);
	}

	inline static int32_t get_offset_of__tempingMng_9() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ____tempingMng_9)); }
	inline ScoreManager_Temping_t3029572618 * get__tempingMng_9() const { return ____tempingMng_9; }
	inline ScoreManager_Temping_t3029572618 ** get_address_of__tempingMng_9() { return &____tempingMng_9; }
	inline void set__tempingMng_9(ScoreManager_Temping_t3029572618 * value)
	{
		____tempingMng_9 = value;
		Il2CppCodeGenWriteBarrier(&____tempingMng_9, value);
	}

	inline static int32_t get_offset_of_gaugePoint_10() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ___gaugePoint_10)); }
	inline Transform_t1659122786 * get_gaugePoint_10() const { return ___gaugePoint_10; }
	inline Transform_t1659122786 ** get_address_of_gaugePoint_10() { return &___gaugePoint_10; }
	inline void set_gaugePoint_10(Transform_t1659122786 * value)
	{
		___gaugePoint_10 = value;
		Il2CppCodeGenWriteBarrier(&___gaugePoint_10, value);
	}

	inline static int32_t get_offset_of_FirstGaugeTrans_11() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ___FirstGaugeTrans_11)); }
	inline Vector3_t4282066566  get_FirstGaugeTrans_11() const { return ___FirstGaugeTrans_11; }
	inline Vector3_t4282066566 * get_address_of_FirstGaugeTrans_11() { return &___FirstGaugeTrans_11; }
	inline void set_FirstGaugeTrans_11(Vector3_t4282066566  value)
	{
		___FirstGaugeTrans_11 = value;
	}

	inline static int32_t get_offset_of_speed_12() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ___speed_12)); }
	inline float get_speed_12() const { return ___speed_12; }
	inline float* get_address_of_speed_12() { return &___speed_12; }
	inline void set_speed_12(float value)
	{
		___speed_12 = value;
	}

	inline static int32_t get_offset_of_speedDown_13() { return static_cast<int32_t>(offsetof(TempingPointer_t2678149871, ___speedDown_13)); }
	inline float get_speedDown_13() const { return ___speedDown_13; }
	inline float* get_address_of_speedDown_13() { return &___speedDown_13; }
	inline void set_speedDown_13(float value)
	{
		___speedDown_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
