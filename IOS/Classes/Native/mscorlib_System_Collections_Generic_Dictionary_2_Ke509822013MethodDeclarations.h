﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct KeyCollection_t509822013;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerator_1_t754684679;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t2382929755;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3792965912.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2033373907_gshared (KeyCollection_t509822013 * __this, Dictionary_2_t3178029858 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2033373907(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t509822013 *, Dictionary_2_t3178029858 *, const MethodInfo*))KeyCollection__ctor_m2033373907_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2784546467_gshared (KeyCollection_t509822013 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2784546467(__this, ___item0, method) ((  void (*) (KeyCollection_t509822013 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2784546467_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3663216858_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3663216858(__this, method) ((  void (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3663216858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m754380043_gshared (KeyCollection_t509822013 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m754380043(__this, ___item0, method) ((  bool (*) (KeyCollection_t509822013 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m754380043_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m832644400_gshared (KeyCollection_t509822013 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m832644400(__this, ___item0, method) ((  bool (*) (KeyCollection_t509822013 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m832644400_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m773551148_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m773551148(__this, method) ((  Il2CppObject* (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m773551148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3018698316_gshared (KeyCollection_t509822013 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3018698316(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t509822013 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3018698316_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2483683291_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2483683291(__this, method) ((  Il2CppObject * (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2483683291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1990806956_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1990806956(__this, method) ((  bool (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1990806956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1584676190_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1584676190(__this, method) ((  bool (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1584676190_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m334517008_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m334517008(__this, method) ((  Il2CppObject * (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m334517008_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4182263176_gshared (KeyCollection_t509822013 * __this, ParticipantStatusU5BU5D_t2382929755* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4182263176(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t509822013 *, ParticipantStatusU5BU5D_t2382929755*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4182263176_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
extern "C"  Enumerator_t3792965912  KeyCollection_GetEnumerator_m749966293_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m749966293(__this, method) ((  Enumerator_t3792965912  (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_GetEnumerator_m749966293_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2630735128_gshared (KeyCollection_t509822013 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2630735128(__this, method) ((  int32_t (*) (KeyCollection_t509822013 *, const MethodInfo*))KeyCollection_get_Count_m2630735128_gshared)(__this, method)
