﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t922401854;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_UIntPtr3365854250.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6
struct  U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791  : public Il2CppObject
{
public:
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6::index
	UIntPtr_t  ___index_0;
	// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStoreyA6::<>f__this
	PlayerSelectUIResponse_t922401854 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791, ___index_0)); }
	inline UIntPtr_t  get_index_0() const { return ___index_0; }
	inline UIntPtr_t * get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(UIntPtr_t  value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CPlayerIdAtIndexU3Ec__AnonStoreyA6_t710261791, ___U3CU3Ef__this_1)); }
	inline PlayerSelectUIResponse_t922401854 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline PlayerSelectUIResponse_t922401854 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(PlayerSelectUIResponse_t922401854 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
