﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>::.ctor()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m1698430227_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m1698430227(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1__ctor_m1698430227_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey45`1<System.Boolean>::<>m__18()
extern "C"  void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 * __this, const MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331(__this, method) ((  void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_t860015178 *, const MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey45_1_U3CU3Em__18_m2411923331_gshared)(__this, method)
