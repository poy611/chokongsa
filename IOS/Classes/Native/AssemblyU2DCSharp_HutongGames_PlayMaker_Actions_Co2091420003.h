﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ColorRamp
struct  ColorRamp_t2091420003  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.ColorRamp::colors
	FsmColorU5BU5D_t530285832* ___colors_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ColorRamp::sampleAt
	FsmFloat_t2134102846 * ___sampleAt_12;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorRamp::storeColor
	FsmColor_t2131419205 * ___storeColor_13;
	// System.Boolean HutongGames.PlayMaker.Actions.ColorRamp::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_colors_11() { return static_cast<int32_t>(offsetof(ColorRamp_t2091420003, ___colors_11)); }
	inline FsmColorU5BU5D_t530285832* get_colors_11() const { return ___colors_11; }
	inline FsmColorU5BU5D_t530285832** get_address_of_colors_11() { return &___colors_11; }
	inline void set_colors_11(FsmColorU5BU5D_t530285832* value)
	{
		___colors_11 = value;
		Il2CppCodeGenWriteBarrier(&___colors_11, value);
	}

	inline static int32_t get_offset_of_sampleAt_12() { return static_cast<int32_t>(offsetof(ColorRamp_t2091420003, ___sampleAt_12)); }
	inline FsmFloat_t2134102846 * get_sampleAt_12() const { return ___sampleAt_12; }
	inline FsmFloat_t2134102846 ** get_address_of_sampleAt_12() { return &___sampleAt_12; }
	inline void set_sampleAt_12(FsmFloat_t2134102846 * value)
	{
		___sampleAt_12 = value;
		Il2CppCodeGenWriteBarrier(&___sampleAt_12, value);
	}

	inline static int32_t get_offset_of_storeColor_13() { return static_cast<int32_t>(offsetof(ColorRamp_t2091420003, ___storeColor_13)); }
	inline FsmColor_t2131419205 * get_storeColor_13() const { return ___storeColor_13; }
	inline FsmColor_t2131419205 ** get_address_of_storeColor_13() { return &___storeColor_13; }
	inline void set_storeColor_13(FsmColor_t2131419205 * value)
	{
		___storeColor_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeColor_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(ColorRamp_t2091420003, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
