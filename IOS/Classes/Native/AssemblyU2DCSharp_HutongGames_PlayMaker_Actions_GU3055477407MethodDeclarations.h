﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIAction
struct GUIAction_t3055477407;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIAction::.ctor()
extern "C"  void GUIAction__ctor_m141141367 (GUIAction_t3055477407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIAction::Reset()
extern "C"  void GUIAction_Reset_m2082541604 (GUIAction_t3055477407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIAction::OnGUI()
extern "C"  void GUIAction_OnGUI_m3931507313 (GUIAction_t3055477407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
