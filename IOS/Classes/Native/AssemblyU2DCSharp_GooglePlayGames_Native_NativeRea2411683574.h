﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t1352686482;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71
struct U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t2043220689;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70
struct  U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70::newRoom
	RoomSession_t1352686482 * ___newRoom_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey71 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70::<>f__ref$113
	U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * ___U3CU3Ef__refU24113_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70::<>f__this
	NativeRealtimeMultiplayerClient_t2043220689 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_newRoom_0() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574, ___newRoom_0)); }
	inline RoomSession_t1352686482 * get_newRoom_0() const { return ___newRoom_0; }
	inline RoomSession_t1352686482 ** get_address_of_newRoom_0() { return &___newRoom_0; }
	inline void set_newRoom_0(RoomSession_t1352686482 * value)
	{
		___newRoom_0 = value;
		Il2CppCodeGenWriteBarrier(&___newRoom_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24113_1() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574, ___U3CU3Ef__refU24113_1)); }
	inline U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * get_U3CU3Ef__refU24113_1() const { return ___U3CU3Ef__refU24113_1; }
	inline U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 ** get_address_of_U3CU3Ef__refU24113_1() { return &___U3CU3Ef__refU24113_1; }
	inline void set_U3CU3Ef__refU24113_1(U3CAcceptInvitationU3Ec__AnonStorey71_t2411683575 * value)
	{
		___U3CU3Ef__refU24113_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24113_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CAcceptInvitationU3Ec__AnonStorey70_t2411683574, ___U3CU3Ef__this_2)); }
	inline NativeRealtimeMultiplayerClient_t2043220689 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline NativeRealtimeMultiplayerClient_t2043220689 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(NativeRealtimeMultiplayerClient_t2043220689 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
