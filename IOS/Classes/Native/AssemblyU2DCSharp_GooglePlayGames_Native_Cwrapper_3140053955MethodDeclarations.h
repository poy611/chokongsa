﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2938557483.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_WithResult(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32,GooglePlayGames.Native.Cwrapper.Types/MatchResult)
extern "C"  IntPtr_t ParticipantResults_ParticipantResults_WithResult_m3749793133 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, uint32_t ___placing2, int32_t ___result3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ParticipantResults_ParticipantResults_Valid_m1338331198 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_MatchResultForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  int32_t ParticipantResults_ParticipantResults_MatchResultForParticipant_m421575006 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_PlaceForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  uint32_t ParticipantResults_ParticipantResults_PlaceForParticipant_m3371389750 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_HasResultsForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool ParticipantResults_ParticipantResults_HasResultsForParticipant_m638434428 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ParticipantResults_ParticipantResults_Dispose_m3239255789 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
