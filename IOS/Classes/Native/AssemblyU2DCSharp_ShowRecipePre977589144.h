﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t3798907012;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t4039083868;
// RecipeUpDown
struct RecipeUpDown_t1716098347;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowRecipePre
struct  ShowRecipePre_t977589144  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text ShowRecipePre::menuName
	Text_t9039225 * ___menuName_2;
	// UnityEngine.UI.Text[] ShowRecipePre::recipes
	TextU5BU5D_t3798907012* ___recipes_3;
	// UnityEngine.UI.Image ShowRecipePre::coffeeImg
	Image_t538875265 * ___coffeeImg_4;
	// UnityEngine.UI.Image ShowRecipePre::creamImg
	Image_t538875265 * ___creamImg_5;
	// UnityEngine.Transform[] ShowRecipePre::recipeTrans
	TransformU5BU5D_t3792884695* ___recipeTrans_6;
	// UnityEngine.Transform ShowRecipePre::imgTrans
	Transform_t1659122786 * ___imgTrans_7;
	// UnityEngine.UI.Image[] ShowRecipePre::checks
	ImageU5BU5D_t4039083868* ___checks_8;
	// RecipeUpDown ShowRecipePre::UpDown
	RecipeUpDown_t1716098347 * ___UpDown_9;
	// System.Collections.Generic.List`1<System.String> ShowRecipePre::menuList
	List_1_t1375417109 * ___menuList_10;
	// System.Collections.Generic.List`1<System.String> ShowRecipePre::unitList
	List_1_t1375417109 * ___unitList_11;
	// System.Int32 ShowRecipePre::currentIngredient
	int32_t ___currentIngredient_12;
	// System.String ShowRecipePre::nowScene
	String_t* ___nowScene_13;

public:
	inline static int32_t get_offset_of_menuName_2() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___menuName_2)); }
	inline Text_t9039225 * get_menuName_2() const { return ___menuName_2; }
	inline Text_t9039225 ** get_address_of_menuName_2() { return &___menuName_2; }
	inline void set_menuName_2(Text_t9039225 * value)
	{
		___menuName_2 = value;
		Il2CppCodeGenWriteBarrier(&___menuName_2, value);
	}

	inline static int32_t get_offset_of_recipes_3() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___recipes_3)); }
	inline TextU5BU5D_t3798907012* get_recipes_3() const { return ___recipes_3; }
	inline TextU5BU5D_t3798907012** get_address_of_recipes_3() { return &___recipes_3; }
	inline void set_recipes_3(TextU5BU5D_t3798907012* value)
	{
		___recipes_3 = value;
		Il2CppCodeGenWriteBarrier(&___recipes_3, value);
	}

	inline static int32_t get_offset_of_coffeeImg_4() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___coffeeImg_4)); }
	inline Image_t538875265 * get_coffeeImg_4() const { return ___coffeeImg_4; }
	inline Image_t538875265 ** get_address_of_coffeeImg_4() { return &___coffeeImg_4; }
	inline void set_coffeeImg_4(Image_t538875265 * value)
	{
		___coffeeImg_4 = value;
		Il2CppCodeGenWriteBarrier(&___coffeeImg_4, value);
	}

	inline static int32_t get_offset_of_creamImg_5() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___creamImg_5)); }
	inline Image_t538875265 * get_creamImg_5() const { return ___creamImg_5; }
	inline Image_t538875265 ** get_address_of_creamImg_5() { return &___creamImg_5; }
	inline void set_creamImg_5(Image_t538875265 * value)
	{
		___creamImg_5 = value;
		Il2CppCodeGenWriteBarrier(&___creamImg_5, value);
	}

	inline static int32_t get_offset_of_recipeTrans_6() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___recipeTrans_6)); }
	inline TransformU5BU5D_t3792884695* get_recipeTrans_6() const { return ___recipeTrans_6; }
	inline TransformU5BU5D_t3792884695** get_address_of_recipeTrans_6() { return &___recipeTrans_6; }
	inline void set_recipeTrans_6(TransformU5BU5D_t3792884695* value)
	{
		___recipeTrans_6 = value;
		Il2CppCodeGenWriteBarrier(&___recipeTrans_6, value);
	}

	inline static int32_t get_offset_of_imgTrans_7() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___imgTrans_7)); }
	inline Transform_t1659122786 * get_imgTrans_7() const { return ___imgTrans_7; }
	inline Transform_t1659122786 ** get_address_of_imgTrans_7() { return &___imgTrans_7; }
	inline void set_imgTrans_7(Transform_t1659122786 * value)
	{
		___imgTrans_7 = value;
		Il2CppCodeGenWriteBarrier(&___imgTrans_7, value);
	}

	inline static int32_t get_offset_of_checks_8() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___checks_8)); }
	inline ImageU5BU5D_t4039083868* get_checks_8() const { return ___checks_8; }
	inline ImageU5BU5D_t4039083868** get_address_of_checks_8() { return &___checks_8; }
	inline void set_checks_8(ImageU5BU5D_t4039083868* value)
	{
		___checks_8 = value;
		Il2CppCodeGenWriteBarrier(&___checks_8, value);
	}

	inline static int32_t get_offset_of_UpDown_9() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___UpDown_9)); }
	inline RecipeUpDown_t1716098347 * get_UpDown_9() const { return ___UpDown_9; }
	inline RecipeUpDown_t1716098347 ** get_address_of_UpDown_9() { return &___UpDown_9; }
	inline void set_UpDown_9(RecipeUpDown_t1716098347 * value)
	{
		___UpDown_9 = value;
		Il2CppCodeGenWriteBarrier(&___UpDown_9, value);
	}

	inline static int32_t get_offset_of_menuList_10() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___menuList_10)); }
	inline List_1_t1375417109 * get_menuList_10() const { return ___menuList_10; }
	inline List_1_t1375417109 ** get_address_of_menuList_10() { return &___menuList_10; }
	inline void set_menuList_10(List_1_t1375417109 * value)
	{
		___menuList_10 = value;
		Il2CppCodeGenWriteBarrier(&___menuList_10, value);
	}

	inline static int32_t get_offset_of_unitList_11() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___unitList_11)); }
	inline List_1_t1375417109 * get_unitList_11() const { return ___unitList_11; }
	inline List_1_t1375417109 ** get_address_of_unitList_11() { return &___unitList_11; }
	inline void set_unitList_11(List_1_t1375417109 * value)
	{
		___unitList_11 = value;
		Il2CppCodeGenWriteBarrier(&___unitList_11, value);
	}

	inline static int32_t get_offset_of_currentIngredient_12() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___currentIngredient_12)); }
	inline int32_t get_currentIngredient_12() const { return ___currentIngredient_12; }
	inline int32_t* get_address_of_currentIngredient_12() { return &___currentIngredient_12; }
	inline void set_currentIngredient_12(int32_t value)
	{
		___currentIngredient_12 = value;
	}

	inline static int32_t get_offset_of_nowScene_13() { return static_cast<int32_t>(offsetof(ShowRecipePre_t977589144, ___nowScene_13)); }
	inline String_t* get_nowScene_13() const { return ___nowScene_13; }
	inline String_t** get_address_of_nowScene_13() { return &___nowScene_13; }
	inline void set_nowScene_13(String_t* value)
	{
		___nowScene_13 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
