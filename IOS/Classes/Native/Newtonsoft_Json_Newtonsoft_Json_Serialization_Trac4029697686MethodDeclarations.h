﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.TraceJsonReader
struct TraceJsonReader_t4029697686;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonToken4173078175.h"

// System.Void Newtonsoft.Json.Serialization.TraceJsonReader::.ctor(Newtonsoft.Json.JsonReader)
extern "C"  void TraceJsonReader__ctor_m3214800355 (TraceJsonReader_t4029697686 * __this, JsonReader_t816925123 * ___innerReader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.TraceJsonReader::GetDeserializedJsonMessage()
extern "C"  String_t* TraceJsonReader_GetDeserializedJsonMessage_m315179362 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Read()
extern "C"  bool TraceJsonReader_Read_m2273608781 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsInt32()
extern "C"  Nullable_1_t1237965023  TraceJsonReader_ReadAsInt32_m2315292694 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsString()
extern "C"  String_t* TraceJsonReader_ReadAsString_m2040784311 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBytes()
extern "C"  ByteU5BU5D_t4260760469* TraceJsonReader_ReadAsBytes_m2666727790 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDecimal()
extern "C"  Nullable_1_t2038477154  TraceJsonReader_ReadAsDecimal_m3969988636 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDouble()
extern "C"  Nullable_1_t3952353088  TraceJsonReader_ReadAsDouble_m3238025152 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBoolean()
extern "C"  Nullable_1_t560925241  TraceJsonReader_ReadAsBoolean_m2807617546 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTime()
extern "C"  Nullable_1_t72820554  TraceJsonReader_ReadAsDateTime_m949871616 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTimeOffset()
extern "C"  Nullable_1_t3968840829  TraceJsonReader_ReadAsDateTimeOffset_m1493158880 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::get_Depth()
extern "C"  int32_t TraceJsonReader_get_Depth_m624414527 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.TraceJsonReader::get_Path()
extern "C"  String_t* TraceJsonReader_get_Path_m3655977932 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Serialization.TraceJsonReader::get_TokenType()
extern "C"  int32_t TraceJsonReader_get_TokenType_m4193801250 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.TraceJsonReader::get_Value()
extern "C"  Il2CppObject * TraceJsonReader_get_Value_m3827862302 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.TraceJsonReader::get_ValueType()
extern "C"  Type_t * TraceJsonReader_get_ValueType_m3770626653 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.TraceJsonReader::Close()
extern "C"  void TraceJsonReader_Close_m722227865 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern "C"  bool TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m2365963639 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern "C"  int32_t TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m4159592019 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern "C"  int32_t TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m527969779 (TraceJsonReader_t4029697686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
