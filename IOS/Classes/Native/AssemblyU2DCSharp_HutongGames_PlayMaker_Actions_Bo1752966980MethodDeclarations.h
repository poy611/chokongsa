﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolTest
struct BoolTest_t1752966980;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolTest::.ctor()
extern "C"  void BoolTest__ctor_m2290892706 (BoolTest_t1752966980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::Reset()
extern "C"  void BoolTest_Reset_m4232292943 (BoolTest_t1752966980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::OnEnter()
extern "C"  void BoolTest_OnEnter_m1916363513 (BoolTest_t1752966980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::OnUpdate()
extern "C"  void BoolTest_OnUpdate_m2706253418 (BoolTest_t1752966980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
