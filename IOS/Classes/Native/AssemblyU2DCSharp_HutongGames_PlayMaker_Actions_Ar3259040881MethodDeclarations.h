﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayReverse
struct ArrayReverse_t3259040881;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::.ctor()
extern "C"  void ArrayReverse__ctor_m1418533141 (ArrayReverse_t3259040881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::Reset()
extern "C"  void ArrayReverse_Reset_m3359933378 (ArrayReverse_t3259040881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::OnEnter()
extern "C"  void ArrayReverse_OnEnter_m1097444268 (ArrayReverse_t3259040881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
