﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo
struct CFX_Demo_t1114149805;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo::.ctor()
extern "C"  void CFX_Demo__ctor_m1018101134 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnMouseDown()
extern "C"  void CFX_Demo_OnMouseDown_m3543768116 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CFX_Demo::spawnParticle()
extern "C"  GameObject_t3674682005 * CFX_Demo_spawnParticle_m2251821464 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::OnGUI()
extern "C"  void CFX_Demo_OnGUI_m513499784 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_Demo::RandomSpawnsCoroutine()
extern "C"  Il2CppObject * CFX_Demo_RandomSpawnsCoroutine_m3219792449 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::Update()
extern "C"  void CFX_Demo_Update_m3223226175 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::prevParticle()
extern "C"  void CFX_Demo_prevParticle_m2707141743 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo::nextParticle()
extern "C"  void CFX_Demo_nextParticle_m694070575 (CFX_Demo_t1114149805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
