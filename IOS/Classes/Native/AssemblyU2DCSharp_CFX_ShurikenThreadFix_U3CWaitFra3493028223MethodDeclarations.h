﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9
struct U3CWaitFrameU3Ec__Iterator9_t3493028223;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::.ctor()
extern "C"  void U3CWaitFrameU3Ec__Iterator9__ctor_m3126173388 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitFrameU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m380249286 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitFrameU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m257586266 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::MoveNext()
extern "C"  bool U3CWaitFrameU3Ec__Iterator9_MoveNext_m1380787880 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::Dispose()
extern "C"  void U3CWaitFrameU3Ec__Iterator9_Dispose_m1968769033 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix/<WaitFrame>c__Iterator9::Reset()
extern "C"  void U3CWaitFrameU3Ec__Iterator9_Reset_m772606329 (U3CWaitFrameU3Ec__Iterator9_t3493028223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
