﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey81
struct U3CReadBinaryDataU3Ec__AnonStorey81_t529588800;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t2292627584;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2292627584.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey81::.ctor()
extern "C"  void U3CReadBinaryDataU3Ec__AnonStorey81__ctor_m4229224539 (U3CReadBinaryDataU3Ec__AnonStorey81_t529588800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey81::<>m__6A(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern "C"  void U3CReadBinaryDataU3Ec__AnonStorey81_U3CU3Em__6A_m698768291 (U3CReadBinaryDataU3Ec__AnonStorey81_t529588800 * __this, ReadResponse_t2292627584 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
