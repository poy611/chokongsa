﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionSection
struct ActionSection_t3062711417;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String HutongGames.PlayMaker.ActionSection::get_Section()
extern "C"  String_t* ActionSection_get_Section_m3933654979 (ActionSection_t3062711417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionSection::.ctor(System.String)
extern "C"  void ActionSection__ctor_m1543252924 (ActionSection_t3062711417 * __this, String_t* ___section0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
