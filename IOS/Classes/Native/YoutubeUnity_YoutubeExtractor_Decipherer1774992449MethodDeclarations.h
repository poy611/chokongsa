﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String YoutubeExtractor.Decipherer::DecipherWithVersion(System.String,System.String)
extern "C"  String_t* Decipherer_DecipherWithVersion_m2393510710 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___cipherVersion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.Decipherer::ApplyOperation(System.String,System.String)
extern "C"  String_t* Decipherer_ApplyOperation_m1048729765 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___op1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.Decipherer::DecipherWithOperations(System.String,System.String)
extern "C"  String_t* Decipherer_DecipherWithOperations_m1663412512 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___operations1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.Decipherer::GetFunctionFromLine(System.String)
extern "C"  String_t* Decipherer_GetFunctionFromLine_m3216534868 (Il2CppObject * __this /* static, unused */, String_t* ___currentLine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 YoutubeExtractor.Decipherer::GetOpIndex(System.String)
extern "C"  int32_t Decipherer_GetOpIndex_m2765359674 (Il2CppObject * __this /* static, unused */, String_t* ___op0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String YoutubeExtractor.Decipherer::SwapFirstChar(System.String,System.Int32)
extern "C"  String_t* Decipherer_SwapFirstChar_m643924970 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
