﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResponseInfoType
struct ResponseInfoType_t519916681;

#include "codegen/il2cpp-codegen.h"

// System.Void ResponseInfoType::.ctor()
extern "C"  void ResponseInfoType__ctor_m3738887218 (ResponseInfoType_t519916681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
