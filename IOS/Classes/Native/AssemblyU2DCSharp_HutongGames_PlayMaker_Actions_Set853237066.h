﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEventData
struct  SetEventData_t853237066  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetEventData::setGameObjectData
	FsmGameObject_t1697147867 * ___setGameObjectData_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetEventData::setIntData
	FsmInt_t1596138449 * ___setIntData_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetEventData::setFloatData
	FsmFloat_t2134102846 * ___setFloatData_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetEventData::setStringData
	FsmString_t952858651 * ___setStringData_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetEventData::setBoolData
	FsmBool_t1075959796 * ___setBoolData_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetEventData::setVector2Data
	FsmVector2_t533912881 * ___setVector2Data_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetEventData::setVector3Data
	FsmVector3_t533912882 * ___setVector3Data_17;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetEventData::setRectData
	FsmRect_t1076426478 * ___setRectData_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetEventData::setQuaternionData
	FsmQuaternion_t3871136040 * ___setQuaternionData_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetEventData::setColorData
	FsmColor_t2131419205 * ___setColorData_20;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetEventData::setMaterialData
	FsmMaterial_t924399665 * ___setMaterialData_21;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetEventData::setTextureData
	FsmTexture_t3073272573 * ___setTextureData_22;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetEventData::setObjectData
	FsmObject_t821476169 * ___setObjectData_23;

public:
	inline static int32_t get_offset_of_setGameObjectData_11() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setGameObjectData_11)); }
	inline FsmGameObject_t1697147867 * get_setGameObjectData_11() const { return ___setGameObjectData_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_setGameObjectData_11() { return &___setGameObjectData_11; }
	inline void set_setGameObjectData_11(FsmGameObject_t1697147867 * value)
	{
		___setGameObjectData_11 = value;
		Il2CppCodeGenWriteBarrier(&___setGameObjectData_11, value);
	}

	inline static int32_t get_offset_of_setIntData_12() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setIntData_12)); }
	inline FsmInt_t1596138449 * get_setIntData_12() const { return ___setIntData_12; }
	inline FsmInt_t1596138449 ** get_address_of_setIntData_12() { return &___setIntData_12; }
	inline void set_setIntData_12(FsmInt_t1596138449 * value)
	{
		___setIntData_12 = value;
		Il2CppCodeGenWriteBarrier(&___setIntData_12, value);
	}

	inline static int32_t get_offset_of_setFloatData_13() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setFloatData_13)); }
	inline FsmFloat_t2134102846 * get_setFloatData_13() const { return ___setFloatData_13; }
	inline FsmFloat_t2134102846 ** get_address_of_setFloatData_13() { return &___setFloatData_13; }
	inline void set_setFloatData_13(FsmFloat_t2134102846 * value)
	{
		___setFloatData_13 = value;
		Il2CppCodeGenWriteBarrier(&___setFloatData_13, value);
	}

	inline static int32_t get_offset_of_setStringData_14() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setStringData_14)); }
	inline FsmString_t952858651 * get_setStringData_14() const { return ___setStringData_14; }
	inline FsmString_t952858651 ** get_address_of_setStringData_14() { return &___setStringData_14; }
	inline void set_setStringData_14(FsmString_t952858651 * value)
	{
		___setStringData_14 = value;
		Il2CppCodeGenWriteBarrier(&___setStringData_14, value);
	}

	inline static int32_t get_offset_of_setBoolData_15() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setBoolData_15)); }
	inline FsmBool_t1075959796 * get_setBoolData_15() const { return ___setBoolData_15; }
	inline FsmBool_t1075959796 ** get_address_of_setBoolData_15() { return &___setBoolData_15; }
	inline void set_setBoolData_15(FsmBool_t1075959796 * value)
	{
		___setBoolData_15 = value;
		Il2CppCodeGenWriteBarrier(&___setBoolData_15, value);
	}

	inline static int32_t get_offset_of_setVector2Data_16() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setVector2Data_16)); }
	inline FsmVector2_t533912881 * get_setVector2Data_16() const { return ___setVector2Data_16; }
	inline FsmVector2_t533912881 ** get_address_of_setVector2Data_16() { return &___setVector2Data_16; }
	inline void set_setVector2Data_16(FsmVector2_t533912881 * value)
	{
		___setVector2Data_16 = value;
		Il2CppCodeGenWriteBarrier(&___setVector2Data_16, value);
	}

	inline static int32_t get_offset_of_setVector3Data_17() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setVector3Data_17)); }
	inline FsmVector3_t533912882 * get_setVector3Data_17() const { return ___setVector3Data_17; }
	inline FsmVector3_t533912882 ** get_address_of_setVector3Data_17() { return &___setVector3Data_17; }
	inline void set_setVector3Data_17(FsmVector3_t533912882 * value)
	{
		___setVector3Data_17 = value;
		Il2CppCodeGenWriteBarrier(&___setVector3Data_17, value);
	}

	inline static int32_t get_offset_of_setRectData_18() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setRectData_18)); }
	inline FsmRect_t1076426478 * get_setRectData_18() const { return ___setRectData_18; }
	inline FsmRect_t1076426478 ** get_address_of_setRectData_18() { return &___setRectData_18; }
	inline void set_setRectData_18(FsmRect_t1076426478 * value)
	{
		___setRectData_18 = value;
		Il2CppCodeGenWriteBarrier(&___setRectData_18, value);
	}

	inline static int32_t get_offset_of_setQuaternionData_19() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setQuaternionData_19)); }
	inline FsmQuaternion_t3871136040 * get_setQuaternionData_19() const { return ___setQuaternionData_19; }
	inline FsmQuaternion_t3871136040 ** get_address_of_setQuaternionData_19() { return &___setQuaternionData_19; }
	inline void set_setQuaternionData_19(FsmQuaternion_t3871136040 * value)
	{
		___setQuaternionData_19 = value;
		Il2CppCodeGenWriteBarrier(&___setQuaternionData_19, value);
	}

	inline static int32_t get_offset_of_setColorData_20() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setColorData_20)); }
	inline FsmColor_t2131419205 * get_setColorData_20() const { return ___setColorData_20; }
	inline FsmColor_t2131419205 ** get_address_of_setColorData_20() { return &___setColorData_20; }
	inline void set_setColorData_20(FsmColor_t2131419205 * value)
	{
		___setColorData_20 = value;
		Il2CppCodeGenWriteBarrier(&___setColorData_20, value);
	}

	inline static int32_t get_offset_of_setMaterialData_21() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setMaterialData_21)); }
	inline FsmMaterial_t924399665 * get_setMaterialData_21() const { return ___setMaterialData_21; }
	inline FsmMaterial_t924399665 ** get_address_of_setMaterialData_21() { return &___setMaterialData_21; }
	inline void set_setMaterialData_21(FsmMaterial_t924399665 * value)
	{
		___setMaterialData_21 = value;
		Il2CppCodeGenWriteBarrier(&___setMaterialData_21, value);
	}

	inline static int32_t get_offset_of_setTextureData_22() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setTextureData_22)); }
	inline FsmTexture_t3073272573 * get_setTextureData_22() const { return ___setTextureData_22; }
	inline FsmTexture_t3073272573 ** get_address_of_setTextureData_22() { return &___setTextureData_22; }
	inline void set_setTextureData_22(FsmTexture_t3073272573 * value)
	{
		___setTextureData_22 = value;
		Il2CppCodeGenWriteBarrier(&___setTextureData_22, value);
	}

	inline static int32_t get_offset_of_setObjectData_23() { return static_cast<int32_t>(offsetof(SetEventData_t853237066, ___setObjectData_23)); }
	inline FsmObject_t821476169 * get_setObjectData_23() const { return ___setObjectData_23; }
	inline FsmObject_t821476169 ** get_address_of_setObjectData_23() { return &___setObjectData_23; }
	inline void set_setObjectData_23(FsmObject_t821476169 * value)
	{
		___setObjectData_23 = value;
		Il2CppCodeGenWriteBarrier(&___setObjectData_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
