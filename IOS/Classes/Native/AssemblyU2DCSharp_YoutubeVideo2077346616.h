﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// YoutubeVideo
struct YoutubeVideo_t2077346616;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeVideo
struct  YoutubeVideo_t2077346616  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean YoutubeVideo::drawBackground
	bool ___drawBackground_3;
	// UnityEngine.Texture2D YoutubeVideo::backgroundImage
	Texture2D_t3884108195 * ___backgroundImage_4;

public:
	inline static int32_t get_offset_of_drawBackground_3() { return static_cast<int32_t>(offsetof(YoutubeVideo_t2077346616, ___drawBackground_3)); }
	inline bool get_drawBackground_3() const { return ___drawBackground_3; }
	inline bool* get_address_of_drawBackground_3() { return &___drawBackground_3; }
	inline void set_drawBackground_3(bool value)
	{
		___drawBackground_3 = value;
	}

	inline static int32_t get_offset_of_backgroundImage_4() { return static_cast<int32_t>(offsetof(YoutubeVideo_t2077346616, ___backgroundImage_4)); }
	inline Texture2D_t3884108195 * get_backgroundImage_4() const { return ___backgroundImage_4; }
	inline Texture2D_t3884108195 ** get_address_of_backgroundImage_4() { return &___backgroundImage_4; }
	inline void set_backgroundImage_4(Texture2D_t3884108195 * value)
	{
		___backgroundImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImage_4, value);
	}
};

struct YoutubeVideo_t2077346616_StaticFields
{
public:
	// YoutubeVideo YoutubeVideo::Instance
	YoutubeVideo_t2077346616 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(YoutubeVideo_t2077346616_StaticFields, ___Instance_2)); }
	inline YoutubeVideo_t2077346616 * get_Instance_2() const { return ___Instance_2; }
	inline YoutubeVideo_t2077346616 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(YoutubeVideo_t2077346616 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
