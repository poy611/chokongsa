﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47
struct U3CGetUserEmailU3Ec__AnonStorey47_t447114323;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48
struct  U3CGetUserEmailU3Ec__AnonStorey48_t183568840  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.CommonStatusCodes GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48::status
	int32_t ___status_0;
	// System.String GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48::email
	String_t* ___email_1;
	// GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47 GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48::<>f__ref$71
	U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * ___U3CU3Ef__refU2471_2;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(U3CGetUserEmailU3Ec__AnonStorey48_t183568840, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_email_1() { return static_cast<int32_t>(offsetof(U3CGetUserEmailU3Ec__AnonStorey48_t183568840, ___email_1)); }
	inline String_t* get_email_1() const { return ___email_1; }
	inline String_t** get_address_of_email_1() { return &___email_1; }
	inline void set_email_1(String_t* value)
	{
		___email_1 = value;
		Il2CppCodeGenWriteBarrier(&___email_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2471_2() { return static_cast<int32_t>(offsetof(U3CGetUserEmailU3Ec__AnonStorey48_t183568840, ___U3CU3Ef__refU2471_2)); }
	inline U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * get_U3CU3Ef__refU2471_2() const { return ___U3CU3Ef__refU2471_2; }
	inline U3CGetUserEmailU3Ec__AnonStorey47_t447114323 ** get_address_of_U3CU3Ef__refU2471_2() { return &___U3CU3Ef__refU2471_2; }
	inline void set_U3CU3Ef__refU2471_2(U3CGetUserEmailU3Ec__AnonStorey47_t447114323 * value)
	{
		___U3CU3Ef__refU2471_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2471_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
