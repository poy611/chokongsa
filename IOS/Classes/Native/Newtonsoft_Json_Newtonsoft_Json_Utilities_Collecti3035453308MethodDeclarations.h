﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>
struct CollectionWrapper_1_t3035453308;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Array
struct Il2CppArray;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"

// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Add(T)
extern "C"  void CollectionWrapper_1_Add_m1347557672_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Add_m1347557672(__this, ___item0, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Add_m1347557672_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Clear()
extern "C"  void CollectionWrapper_1_Clear_m831305890_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_Clear_m831305890(__this, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_Clear_m831305890_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Contains(T)
extern "C"  bool CollectionWrapper_1_Contains_m687940200_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Contains_m687940200(__this, ___item0, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Contains_m687940200_gshared)(__this, ___item0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void CollectionWrapper_1_CopyTo_m857933592_gshared (CollectionWrapper_1_t3035453308 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define CollectionWrapper_1_CopyTo_m857933592(__this, ___array0, ___arrayIndex1, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))CollectionWrapper_1_CopyTo_m857933592_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_Count()
extern "C"  int32_t CollectionWrapper_1_get_Count_m1049256439_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_Count_m1049256439(__this, method) ((  int32_t (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_get_Count_m1049256439_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_IsReadOnly()
extern "C"  bool CollectionWrapper_1_get_IsReadOnly_m4049681356_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_IsReadOnly_m4049681356(__this, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_get_IsReadOnly_m4049681356_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::Remove(T)
extern "C"  bool CollectionWrapper_1_Remove_m3043712099_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define CollectionWrapper_1_Remove_m3043712099(__this, ___item0, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_Remove_m3043712099_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* CollectionWrapper_1_GetEnumerator_m1532560691_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_GetEnumerator_m1532560691(__this, method) ((  Il2CppObject* (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_GetEnumerator_m1532560691_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_System_Collections_IEnumerable_GetEnumerator_m22535248_gshared)(__this, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_Add_m433705633_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Add_m433705633(__this, ___value0, method) ((  int32_t (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Add_m433705633_gshared)(__this, ___value0, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool CollectionWrapper_1_System_Collections_IList_Contains_m3576282059_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Contains_m3576282059(__this, ___value0, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Contains_m3576282059_gshared)(__this, ___value0, method)
// System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737(__this, ___value0, method) ((  int32_t (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_IndexOf_m3434991737_gshared)(__this, ___value0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268(__this, ___index0, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_RemoveAt_m3529258268_gshared)(__this, ___index0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Insert_m2912955276_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Insert_m2912955276(__this, ___index0, ___value1, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Insert_m2912955276_gshared)(__this, ___index0, ___value1, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906(__this, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_get_IsFixedSize_m1961942906_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_Remove_m1863821852_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_Remove_m1863821852(__this, ___value0, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_Remove_m1863821852_gshared)(__this, ___value0, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404(__this, ___index0, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t3035453308 *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_get_Item_m1854507404_gshared)(__this, ___index0, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011_gshared (CollectionWrapper_1_t3035453308 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011(__this, ___index0, ___value1, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_System_Collections_IList_set_Item_m3940263011_gshared)(__this, ___index0, ___value1, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void CollectionWrapper_1_System_Collections_ICollection_CopyTo_m3925762221_gshared (CollectionWrapper_1_t3035453308 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_CopyTo_m3925762221(__this, ___array0, ___arrayIndex1, method) ((  void (*) (CollectionWrapper_1_t3035453308 *, Il2CppArray *, int32_t, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_CopyTo_m3925762221_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m55051185_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m55051185(__this, method) ((  bool (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_get_IsSynchronized_m55051185_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_System_Collections_ICollection_get_SyncRoot_m3777353029_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::VerifyValueType(System.Object)
extern "C"  void CollectionWrapper_1_VerifyValueType_m753172749_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_VerifyValueType_m753172749(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_VerifyValueType_m753172749_gshared)(__this /* static, unused */, ___value0, method)
// System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::IsCompatibleObject(System.Object)
extern "C"  bool CollectionWrapper_1_IsCompatibleObject_m2245613824_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionWrapper_1_IsCompatibleObject_m2245613824(__this /* static, unused */, ___value0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))CollectionWrapper_1_IsCompatibleObject_m2245613824_gshared)(__this /* static, unused */, ___value0, method)
// System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1<System.Object>::get_UnderlyingCollection()
extern "C"  Il2CppObject * CollectionWrapper_1_get_UnderlyingCollection_m2146542214_gshared (CollectionWrapper_1_t3035453308 * __this, const MethodInfo* method);
#define CollectionWrapper_1_get_UnderlyingCollection_m2146542214(__this, method) ((  Il2CppObject * (*) (CollectionWrapper_1_t3035453308 *, const MethodInfo*))CollectionWrapper_1_get_UnderlyingCollection_m2146542214_gshared)(__this, method)
