﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonBinary
struct BsonBinary_t2378136577;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinaryType407343707.h"

// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::get_BinaryType()
extern "C"  uint8_t BsonBinary_get_BinaryType_m2633477635 (BsonBinary_t2378136577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinary::set_BinaryType(Newtonsoft.Json.Bson.BsonBinaryType)
extern "C"  void BsonBinary_set_BinaryType_m2254789384 (BsonBinary_t2378136577 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonBinary::.ctor(System.Byte[],Newtonsoft.Json.Bson.BsonBinaryType)
extern "C"  void BsonBinary__ctor_m1690645147 (BsonBinary_t2378136577 * __this, ByteU5BU5D_t4260760469* ___value0, uint8_t ___binaryType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
