﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_InspectorHelp
struct  CFX_InspectorHelp_t4208687182  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean CFX_InspectorHelp::Locked
	bool ___Locked_2;
	// System.String CFX_InspectorHelp::Title
	String_t* ___Title_3;
	// System.String CFX_InspectorHelp::HelpText
	String_t* ___HelpText_4;
	// System.Int32 CFX_InspectorHelp::MsgType
	int32_t ___MsgType_5;

public:
	inline static int32_t get_offset_of_Locked_2() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4208687182, ___Locked_2)); }
	inline bool get_Locked_2() const { return ___Locked_2; }
	inline bool* get_address_of_Locked_2() { return &___Locked_2; }
	inline void set_Locked_2(bool value)
	{
		___Locked_2 = value;
	}

	inline static int32_t get_offset_of_Title_3() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4208687182, ___Title_3)); }
	inline String_t* get_Title_3() const { return ___Title_3; }
	inline String_t** get_address_of_Title_3() { return &___Title_3; }
	inline void set_Title_3(String_t* value)
	{
		___Title_3 = value;
		Il2CppCodeGenWriteBarrier(&___Title_3, value);
	}

	inline static int32_t get_offset_of_HelpText_4() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4208687182, ___HelpText_4)); }
	inline String_t* get_HelpText_4() const { return ___HelpText_4; }
	inline String_t** get_address_of_HelpText_4() { return &___HelpText_4; }
	inline void set_HelpText_4(String_t* value)
	{
		___HelpText_4 = value;
		Il2CppCodeGenWriteBarrier(&___HelpText_4, value);
	}

	inline static int32_t get_offset_of_MsgType_5() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4208687182, ___MsgType_5)); }
	inline int32_t get_MsgType_5() const { return ___MsgType_5; }
	inline int32_t* get_address_of_MsgType_5() { return &___MsgType_5; }
	inline void set_MsgType_5(int32_t value)
	{
		___MsgType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
