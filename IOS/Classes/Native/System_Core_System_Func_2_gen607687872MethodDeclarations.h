﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1192475960(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t607687872 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m420802513_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::Invoke(T)
#define Func_2_Invoke_m1459634921(__this, ___arg10, method) ((  Func_2_t2363589633 * (*) (Func_2_t607687872 *, Type_t *, const MethodInfo*))Func_2_Invoke_m1009799647_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m259918876(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t607687872 *, Type_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m833100939(__this, ___result0, method) ((  Func_2_t2363589633 * (*) (Func_2_t607687872 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
