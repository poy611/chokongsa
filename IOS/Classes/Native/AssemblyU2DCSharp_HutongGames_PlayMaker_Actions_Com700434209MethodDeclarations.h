﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3146284570MethodDeclarations.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::.ctor()
#define ComponentAction_1__ctor_m1381897493(__this, method) ((  void (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1__ctor_m1981820550_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_rigidbody()
#define ComponentAction_1_get_rigidbody_m190804410(__this, method) ((  Rigidbody_t3346577219 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3654614315_gshared)(__this, method)
// UnityEngine.Rigidbody2D HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_rigidbody2d()
#define ComponentAction_1_get_rigidbody2d_m3419785854(__this, method) ((  Rigidbody2D_t1743771669 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_rigidbody2d_m3969478575_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_renderer()
#define ComponentAction_1_get_renderer_m2797262142(__this, method) ((  Renderer_t3076687687 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_renderer_m2707840429_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_animation()
#define ComponentAction_1_get_animation_m4211453288(__this, method) ((  Animation_t1724966010 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_animation_m3244187609_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_audio()
#define ComponentAction_1_get_audio_m4103848647(__this, method) ((  AudioSource_t1740077639 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_audio_m1198830264_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_camera()
#define ComponentAction_1_get_camera_m761126910(__this, method) ((  Camera_t2727095145 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_camera_m2580411949_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_guiText()
#define ComponentAction_1_get_guiText_m1141080240(__this, method) ((  GUIText_t3371372606 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_guiText_m995647329_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_guiTexture()
#define ComponentAction_1_get_guiTexture_m2523014814(__this, method) ((  GUITexture_t4020448292 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m3296952909_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_light()
#define ComponentAction_1_get_light_m3185584012(__this, method) ((  Light_t4202674828 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_light_m3399183485_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::get_networkView()
#define ComponentAction_1_get_networkView_m148759942(__this, method) ((  NetworkView_t3656680617 * (*) (ComponentAction_1_t700434209 *, const MethodInfo*))ComponentAction_1_get_networkView_m1393581751_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>::UpdateCache(UnityEngine.GameObject)
#define ComponentAction_1_UpdateCache_m3952061936(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t700434209 *, GameObject_t3674682005 *, const MethodInfo*))ComponentAction_1_UpdateCache_m1764863969_gshared)(__this, ___go0, method)
