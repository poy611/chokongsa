﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t1088765775;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t3582269991;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D
struct U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E
struct  U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.SavedGame.IConflictResolver GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::resolver
	Il2CppObject * ___resolver_0;
	// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::original
	Il2CppObject * ___original_1;
	// System.Byte[] GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::originalData
	ByteU5BU5D_t4260760469* ___originalData_2;
	// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::unmerged
	Il2CppObject * ___unmerged_3;
	// System.Byte[] GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::unmergedData
	ByteU5BU5D_t4260760469* ___unmergedData_4;
	// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D/<ToOnGameThread>c__AnonStorey7E::<>f__ref$125
	U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 * ___U3CU3Ef__refU24125_5;

public:
	inline static int32_t get_offset_of_resolver_0() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___resolver_0)); }
	inline Il2CppObject * get_resolver_0() const { return ___resolver_0; }
	inline Il2CppObject ** get_address_of_resolver_0() { return &___resolver_0; }
	inline void set_resolver_0(Il2CppObject * value)
	{
		___resolver_0 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_0, value);
	}

	inline static int32_t get_offset_of_original_1() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___original_1)); }
	inline Il2CppObject * get_original_1() const { return ___original_1; }
	inline Il2CppObject ** get_address_of_original_1() { return &___original_1; }
	inline void set_original_1(Il2CppObject * value)
	{
		___original_1 = value;
		Il2CppCodeGenWriteBarrier(&___original_1, value);
	}

	inline static int32_t get_offset_of_originalData_2() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___originalData_2)); }
	inline ByteU5BU5D_t4260760469* get_originalData_2() const { return ___originalData_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_originalData_2() { return &___originalData_2; }
	inline void set_originalData_2(ByteU5BU5D_t4260760469* value)
	{
		___originalData_2 = value;
		Il2CppCodeGenWriteBarrier(&___originalData_2, value);
	}

	inline static int32_t get_offset_of_unmerged_3() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___unmerged_3)); }
	inline Il2CppObject * get_unmerged_3() const { return ___unmerged_3; }
	inline Il2CppObject ** get_address_of_unmerged_3() { return &___unmerged_3; }
	inline void set_unmerged_3(Il2CppObject * value)
	{
		___unmerged_3 = value;
		Il2CppCodeGenWriteBarrier(&___unmerged_3, value);
	}

	inline static int32_t get_offset_of_unmergedData_4() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___unmergedData_4)); }
	inline ByteU5BU5D_t4260760469* get_unmergedData_4() const { return ___unmergedData_4; }
	inline ByteU5BU5D_t4260760469** get_address_of_unmergedData_4() { return &___unmergedData_4; }
	inline void set_unmergedData_4(ByteU5BU5D_t4260760469* value)
	{
		___unmergedData_4 = value;
		Il2CppCodeGenWriteBarrier(&___unmergedData_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24125_5() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey7E_t287093411, ___U3CU3Ef__refU24125_5)); }
	inline U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 * get_U3CU3Ef__refU24125_5() const { return ___U3CU3Ef__refU24125_5; }
	inline U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 ** get_address_of_U3CU3Ef__refU24125_5() { return &___U3CU3Ef__refU24125_5; }
	inline void set_U3CU3Ef__refU24125_5(U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 * value)
	{
		___U3CU3Ef__refU24125_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24125_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
