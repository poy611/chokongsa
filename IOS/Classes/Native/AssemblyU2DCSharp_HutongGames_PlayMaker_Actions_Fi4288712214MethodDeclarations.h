﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindGameObject2
struct FindGameObject2_t4288712214;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindGameObject2::.ctor()
extern "C"  void FindGameObject2__ctor_m1342513632 (FindGameObject2_t4288712214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject2::Reset()
extern "C"  void FindGameObject2_Reset_m3283913869 (FindGameObject2_t4288712214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject2::OnEnter()
extern "C"  void FindGameObject2_OnEnter_m1057140151 (FindGameObject2_t4288712214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject2::Find()
extern "C"  void FindGameObject2_Find_m3922344669 (FindGameObject2_t4288712214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FindGameObject2::ErrorCheck()
extern "C"  String_t* FindGameObject2_ErrorCheck_m971554049 (FindGameObject2_t4288712214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
