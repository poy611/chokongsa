﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionEuler
struct  QuaternionEuler_t1604107257  : public QuaternionBaseAction_t1884049229
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionEuler::eulerAngles
	FsmVector3_t533912882 * ___eulerAngles_13;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionEuler::result
	FsmQuaternion_t3871136040 * ___result_14;

public:
	inline static int32_t get_offset_of_eulerAngles_13() { return static_cast<int32_t>(offsetof(QuaternionEuler_t1604107257, ___eulerAngles_13)); }
	inline FsmVector3_t533912882 * get_eulerAngles_13() const { return ___eulerAngles_13; }
	inline FsmVector3_t533912882 ** get_address_of_eulerAngles_13() { return &___eulerAngles_13; }
	inline void set_eulerAngles_13(FsmVector3_t533912882 * value)
	{
		___eulerAngles_13 = value;
		Il2CppCodeGenWriteBarrier(&___eulerAngles_13, value);
	}

	inline static int32_t get_offset_of_result_14() { return static_cast<int32_t>(offsetof(QuaternionEuler_t1604107257, ___result_14)); }
	inline FsmQuaternion_t3871136040 * get_result_14() const { return ___result_14; }
	inline FsmQuaternion_t3871136040 ** get_address_of_result_14() { return &___result_14; }
	inline void set_result_14(FsmQuaternion_t3871136040 * value)
	{
		___result_14 = value;
		Il2CppCodeGenWriteBarrier(&___result_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
