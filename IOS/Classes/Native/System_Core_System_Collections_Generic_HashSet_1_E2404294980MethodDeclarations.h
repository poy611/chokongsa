﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Char>
struct HashSet_1_t2017051314;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2404294980.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m2947431871_gshared (Enumerator_t2404294980 * __this, HashSet_1_t2017051314 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m2947431871(__this, ___hashset0, method) ((  void (*) (Enumerator_t2404294980 *, HashSet_1_t2017051314 *, const MethodInfo*))Enumerator__ctor_m2947431871_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2957831757_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2957831757(__this, method) ((  Il2CppObject * (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2957831757_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3945344279_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3945344279(__this, method) ((  void (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3945344279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1003508057_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1003508057(__this, method) ((  bool (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_MoveNext_m1003508057_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m2543268178_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2543268178(__this, method) ((  Il2CppChar (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_get_Current_m2543268178_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m1636314880_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1636314880(__this, method) ((  void (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_Dispose_m1636314880_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Char>::CheckState()
extern "C"  void Enumerator_CheckState_m1196732298_gshared (Enumerator_t2404294980 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m1196732298(__this, method) ((  void (*) (Enumerator_t2404294980 *, const MethodInfo*))Enumerator_CheckState_m1196732298_gshared)(__this, method)
