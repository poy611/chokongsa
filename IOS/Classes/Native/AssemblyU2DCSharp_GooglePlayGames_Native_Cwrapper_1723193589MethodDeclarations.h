﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
struct FetchScoreSummaryCallback_t1723193589;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchScoreSummaryCallback__ctor_m1247634204 (FetchScoreSummaryCallback_t1723193589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchScoreSummaryCallback_Invoke_m1318918828 (FetchScoreSummaryCallback_t1723193589 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FetchScoreSummaryCallback_BeginInvoke_m4182798641 (FetchScoreSummaryCallback_t1723193589 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchScoreSummaryCallback_EndInvoke_m3845571372 (FetchScoreSummaryCallback_t1723193589 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
