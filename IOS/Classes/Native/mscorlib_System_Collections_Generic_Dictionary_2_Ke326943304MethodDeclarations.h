﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct KeyCollection_t326943304;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>
struct Dictionary_2_t2995151149;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Serialization.ResolverContractKey>
struct IEnumerator_1_t2385666054;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.ResolverContractKey[]
struct ResolverContractKeyU5BU5D_t2057919360;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3610087203.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m647561502_gshared (KeyCollection_t326943304 * __this, Dictionary_2_t2995151149 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m647561502(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t326943304 *, Dictionary_2_t2995151149 *, const MethodInfo*))KeyCollection__ctor_m647561502_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2677775736_gshared (KeyCollection_t326943304 * __this, ResolverContractKey_t473801005  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2677775736(__this, ___item0, method) ((  void (*) (KeyCollection_t326943304 *, ResolverContractKey_t473801005 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2677775736_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3667575023_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3667575023(__this, method) ((  void (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3667575023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4213601682_gshared (KeyCollection_t326943304 * __this, ResolverContractKey_t473801005  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4213601682(__this, ___item0, method) ((  bool (*) (KeyCollection_t326943304 *, ResolverContractKey_t473801005 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4213601682_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m358032247_gshared (KeyCollection_t326943304 * __this, ResolverContractKey_t473801005  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m358032247(__this, ___item0, method) ((  bool (*) (KeyCollection_t326943304 *, ResolverContractKey_t473801005 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m358032247_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3259770155_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3259770155(__this, method) ((  Il2CppObject* (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3259770155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1966061281_gshared (KeyCollection_t326943304 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1966061281(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t326943304 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1966061281_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m767539612_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m767539612(__this, method) ((  Il2CppObject * (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m767539612_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1998114931_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1998114931(__this, method) ((  bool (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1998114931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1224490213_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1224490213(__this, method) ((  bool (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1224490213_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1852736721_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1852736721(__this, method) ((  Il2CppObject * (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1852736721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3345113043_gshared (KeyCollection_t326943304 * __this, ResolverContractKeyU5BU5D_t2057919360* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3345113043(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t326943304 *, ResolverContractKeyU5BU5D_t2057919360*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3345113043_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3610087203  KeyCollection_GetEnumerator_m1558823094_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1558823094(__this, method) ((  Enumerator_t3610087203  (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_GetEnumerator_m1558823094_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m4160483499_gshared (KeyCollection_t326943304 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m4160483499(__this, method) ((  int32_t (*) (KeyCollection_t326943304 *, const MethodInfo*))KeyCollection_get_Count_m4160483499_gshared)(__this, method)
