﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetNextHostData
struct MasterServerGetNextHostData_t2113983876;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::.ctor()
extern "C"  void MasterServerGetNextHostData__ctor_m2294102578 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::Reset()
extern "C"  void MasterServerGetNextHostData_Reset_m4235502815 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::OnEnter()
extern "C"  void MasterServerGetNextHostData_OnEnter_m706083209 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::DoGetNextHostData()
extern "C"  void MasterServerGetNextHostData_DoGetNextHostData_m3470601408 (MasterServerGetNextHostData_t2113983876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
