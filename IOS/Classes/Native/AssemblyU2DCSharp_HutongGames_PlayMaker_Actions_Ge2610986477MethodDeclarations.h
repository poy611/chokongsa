﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVector3XYZ
struct GetVector3XYZ_t2610986477;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::.ctor()
extern "C"  void GetVector3XYZ__ctor_m3468242409 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::Reset()
extern "C"  void GetVector3XYZ_Reset_m1114675350 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnEnter()
extern "C"  void GetVector3XYZ_OnEnter_m3773029248 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnUpdate()
extern "C"  void GetVector3XYZ_OnUpdate_m133349059 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::DoGetVector3XYZ()
extern "C"  void GetVector3XYZ_DoGetVector3XYZ_m3613794267 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
