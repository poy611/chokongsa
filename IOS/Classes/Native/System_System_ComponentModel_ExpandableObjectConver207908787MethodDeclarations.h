﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ExpandableObjectConverter
struct ExpandableObjectConverter_t207908787;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ComponentModel.ExpandableObjectConverter::.ctor()
extern "C"  void ExpandableObjectConverter__ctor_m309980032 (ExpandableObjectConverter_t207908787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
