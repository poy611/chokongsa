﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JEnumerable_1_971832625.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void JEnumerable_1__ctor_m313850865_gshared (JEnumerable_1_t971832625 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define JEnumerable_1__ctor_m313850865(__this, ___enumerable0, method) ((  void (*) (JEnumerable_1_t971832625 *, Il2CppObject*, const MethodInfo*))JEnumerable_1__ctor_m313850865_gshared)(__this, ___enumerable0, method)
// System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* JEnumerable_1_GetEnumerator_m3571439426_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method);
#define JEnumerable_1_GetEnumerator_m3571439426(__this, method) ((  Il2CppObject* (*) (JEnumerable_1_t971832625 *, const MethodInfo*))JEnumerable_1_GetEnumerator_m3571439426_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method);
#define JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165(__this, method) ((  Il2CppObject * (*) (JEnumerable_1_t971832625 *, const MethodInfo*))JEnumerable_1_System_Collections_IEnumerable_GetEnumerator_m1664307165_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(Newtonsoft.Json.Linq.JEnumerable`1<T>)
extern "C"  bool JEnumerable_1_Equals_m1430828747_gshared (JEnumerable_1_t971832625 * __this, JEnumerable_1_t971832625  ___other0, const MethodInfo* method);
#define JEnumerable_1_Equals_m1430828747(__this, ___other0, method) ((  bool (*) (JEnumerable_1_t971832625 *, JEnumerable_1_t971832625 , const MethodInfo*))JEnumerable_1_Equals_m1430828747_gshared)(__this, ___other0, method)
// System.Boolean Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::Equals(System.Object)
extern "C"  bool JEnumerable_1_Equals_m2597819561_gshared (JEnumerable_1_t971832625 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define JEnumerable_1_Equals_m2597819561(__this, ___obj0, method) ((  bool (*) (JEnumerable_1_t971832625 *, Il2CppObject *, const MethodInfo*))JEnumerable_1_Equals_m2597819561_gshared)(__this, ___obj0, method)
// System.Int32 Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::GetHashCode()
extern "C"  int32_t JEnumerable_1_GetHashCode_m2089431425_gshared (JEnumerable_1_t971832625 * __this, const MethodInfo* method);
#define JEnumerable_1_GetHashCode_m2089431425(__this, method) ((  int32_t (*) (JEnumerable_1_t971832625 *, const MethodInfo*))JEnumerable_1_GetHashCode_m2089431425_gshared)(__this, method)
// System.Void Newtonsoft.Json.Linq.JEnumerable`1<System.Object>::.cctor()
extern "C"  void JEnumerable_1__cctor_m3840698783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define JEnumerable_1__cctor_m3840698783(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))JEnumerable_1__cctor_m3840698783_gshared)(__this /* static, unused */, method)
