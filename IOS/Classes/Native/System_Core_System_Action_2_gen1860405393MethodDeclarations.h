﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1729022911MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1331879867(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1860405393 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m907218493_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m1185907408(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1860405393 *, int32_t, String_t*, const MethodInfo*))Action_2_Invoke_m970130894_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1564310343(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1860405393 *, int32_t, String_t*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1465736309_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.CommonStatusCodes,System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1085983803(__this, ___result0, method) ((  void (*) (Action_2_t1860405393 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2247698125_gshared)(__this, ___result0, method)
