﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEvent
struct SendEvent_t113322816;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEvent::.ctor()
extern "C"  void SendEvent__ctor_m2550694390 (SendEvent_t113322816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::Reset()
extern "C"  void SendEvent_Reset_m197127331 (SendEvent_t113322816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::OnEnter()
extern "C"  void SendEvent_OnEnter_m2477678669 (SendEvent_t113322816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::OnUpdate()
extern "C"  void SendEvent_OnUpdate_m2927154070 (SendEvent_t113322816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
