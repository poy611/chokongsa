﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2559348008.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin2475221485.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2043491307_gshared (Nullable_1_t2559348008 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2043491307(__this, ___value0, method) ((  void (*) (Nullable_1_t2559348008 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2043491307_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1086755527_gshared (Nullable_1_t2559348008 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1086755527(__this, method) ((  bool (*) (Nullable_1_t2559348008 *, const MethodInfo*))Nullable_1_get_HasValue_m1086755527_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1685543706_gshared (Nullable_1_t2559348008 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1685543706(__this, method) ((  int32_t (*) (Nullable_1_t2559348008 *, const MethodInfo*))Nullable_1_get_Value_m1685543706_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2393865410_gshared (Nullable_1_t2559348008 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2393865410(__this, ___other0, method) ((  bool (*) (Nullable_1_t2559348008 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2393865410_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2085787709_gshared (Nullable_1_t2559348008 * __this, Nullable_1_t2559348008  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2085787709(__this, ___other0, method) ((  bool (*) (Nullable_1_t2559348008 *, Nullable_1_t2559348008 , const MethodInfo*))Nullable_1_Equals_m2085787709_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m767441830_gshared (Nullable_1_t2559348008 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m767441830(__this, method) ((  int32_t (*) (Nullable_1_t2559348008 *, const MethodInfo*))Nullable_1_GetHashCode_m767441830_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m68748580_gshared (Nullable_1_t2559348008 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m68748580(__this, method) ((  int32_t (*) (Nullable_1_t2559348008 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m68748580_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1216838281_gshared (Nullable_1_t2559348008 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1216838281(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t2559348008 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1216838281_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.ConstructorHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2188374432_gshared (Nullable_1_t2559348008 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2188374432(__this, method) ((  String_t* (*) (Nullable_1_t2559348008 *, const MethodInfo*))Nullable_1_ToString_m2188374432_gshared)(__this, method)
