﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey5B
struct U3CShowLeaderboardUIU3Ec__AnonStorey5B_t4060327070;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"

// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey5B::.ctor()
extern "C"  void U3CShowLeaderboardUIU3Ec__AnonStorey5B__ctor_m805309 (U3CShowLeaderboardUIU3Ec__AnonStorey5B_t4060327070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey5B::<>m__31(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C"  void U3CShowLeaderboardUIU3Ec__AnonStorey5B_U3CU3Em__31_m462191741 (U3CShowLeaderboardUIU3Ec__AnonStorey5B_t4060327070 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
