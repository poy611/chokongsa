﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVelocity
struct SetVelocity_t3705741805;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVelocity::.ctor()
extern "C"  void SetVelocity__ctor_m1996006377 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::Reset()
extern "C"  void SetVelocity_Reset_m3937406614 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnPreprocess()
extern "C"  void SetVelocity_OnPreprocess_m3183078694 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnEnter()
extern "C"  void SetVelocity_OnEnter_m1998442880 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnFixedUpdate()
extern "C"  void SetVelocity_OnFixedUpdate_m4116400517 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::DoSetVelocity()
extern "C"  void SetVelocity_DoSetVelocity_m136396763 (SetVelocity_t3705741805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
