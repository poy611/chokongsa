﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<GetAllInvitations>c__AnonStorey6C
struct U3CGetAllInvitationsU3Ec__AnonStorey6C_t1196056562;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t815788017;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re815788017.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<GetAllInvitations>c__AnonStorey6C::.ctor()
extern "C"  void U3CGetAllInvitationsU3Ec__AnonStorey6C__ctor_m3765859769 (U3CGetAllInvitationsU3Ec__AnonStorey6C_t1196056562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<GetAllInvitations>c__AnonStorey6C::<>m__47(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern "C"  void U3CGetAllInvitationsU3Ec__AnonStorey6C_U3CU3Em__47_m68703996 (U3CGetAllInvitationsU3Ec__AnonStorey6C_t1196056562 * __this, FetchInvitationsResponse_t815788017 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
