﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1621686952MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m231443731(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3852037537 *, Dictionary_2_t2534714145 *, const MethodInfo*))Enumerator__ctor_m2684317400_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3446268782(__this, method) ((  Il2CppObject * (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m205140361_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2490015170(__this, method) ((  void (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1939311325_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2171455819(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3885062374_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m162551242(__this, method) ((  Il2CppObject * (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2799068069_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2641357276(__this, method) ((  Il2CppObject * (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2303323383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
#define Enumerator_MoveNext_m3725054126(__this, method) ((  bool (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_MoveNext_m3030199497_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
#define Enumerator_get_Current_m745377730(__this, method) ((  KeyValuePair_2_t2433494851  (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_get_Current_m443046727_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m105913083(__this, method) ((  Type_t * (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_get_CurrentKey_m1115479638_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4251745759(__this, method) ((  int32_t (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_get_CurrentValue_m3095157242_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Reset()
#define Enumerator_Reset_m826030821(__this, method) ((  void (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_Reset_m1798260010_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::VerifyState()
#define Enumerator_VerifyState_m28248878(__this, method) ((  void (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_VerifyState_m4190145971_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4122239766(__this, method) ((  void (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_VerifyCurrent_m795826267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Dispose()
#define Enumerator_Dispose_m1770098293(__this, method) ((  void (*) (Enumerator_t3852037537 *, const MethodInfo*))Enumerator_Dispose_m4074445690_gshared)(__this, method)
