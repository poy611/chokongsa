﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey62
struct U3CAcceptU3Ec__AnonStorey62_t1936577171;
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
struct AcceptResponse_t3485599210;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Q3485599210.h"

// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey62::.ctor()
extern "C"  void U3CAcceptU3Ec__AnonStorey62__ctor_m712112488 (U3CAcceptU3Ec__AnonStorey62_t1936577171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey62::<>m__3F(GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse)
extern "C"  void U3CAcceptU3Ec__AnonStorey62_U3CU3Em__3F_m1421010810 (U3CAcceptU3Ec__AnonStorey62_t1936577171 * __this, AcceptResponse_t3485599210 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
