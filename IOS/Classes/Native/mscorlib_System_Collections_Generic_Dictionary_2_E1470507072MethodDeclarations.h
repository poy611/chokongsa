﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu17507245MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m86070164(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1470507072 *, Dictionary_2_t153183680 *, const MethodInfo*))Enumerator__ctor_m769348407_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1026677655(__this, method) ((  Il2CppObject * (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2861743818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2820450465(__this, method) ((  void (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2311244062_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3442357208(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m927136423_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3397241651(__this, method) ((  Il2CppObject * (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1615215654_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1622518021(__this, method) ((  Il2CppObject * (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2787486008_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::MoveNext()
#define Enumerator_MoveNext_m1689362769(__this, method) ((  bool (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_MoveNext_m2084814858_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::get_Current()
#define Enumerator_get_Current_m421808203(__this, method) ((  KeyValuePair_2_t51964386  (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_get_Current_m903370982_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2585706394(__this, method) ((  ResolverContractKey_t473801005  (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_get_CurrentKey_m2361619287_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m173747162(__this, method) ((  JsonContract_t1328848902 * (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_get_CurrentValue_m435420475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::Reset()
#define Enumerator_Reset_m1904725990(__this, method) ((  void (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_Reset_m3285388041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::VerifyState()
#define Enumerator_VerifyState_m3541900143(__this, method) ((  void (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_VerifyState_m1279930450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m601843479(__this, method) ((  void (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_VerifyCurrent_m102420282_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>::Dispose()
#define Enumerator_Dispose_m3309037366(__this, method) ((  void (*) (Enumerator_t1470507072 *, const MethodInfo*))Enumerator_Dispose_m2980373913_gshared)(__this, method)
