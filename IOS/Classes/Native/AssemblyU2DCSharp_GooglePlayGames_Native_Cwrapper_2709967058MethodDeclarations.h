﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
struct OnParticipantStatusChangedCallback_t2709967058;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnParticipantStatusChangedCallback__ctor_m2799928953 (OnParticipantStatusChangedCallback_t2709967058 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void OnParticipantStatusChangedCallback_Invoke_m3211706941 (OnParticipantStatusChangedCallback_t2709967058 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnParticipantStatusChangedCallback_BeginInvoke_m3705352102 (OnParticipantStatusChangedCallback_t2709967058 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnParticipantStatusChangedCallback_EndInvoke_m3882294537 (OnParticipantStatusChangedCallback_t2709967058 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
