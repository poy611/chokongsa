﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonReaderSettings
struct JsonReaderSettings_t24058356;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean JsonFx.Json.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern "C"  bool JsonReaderSettings_get_AllowUnquotedObjectKeys_m3720636459 (JsonReaderSettings_t24058356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonReaderSettings::IsTypeHintName(System.String)
extern "C"  bool JsonReaderSettings_IsTypeHintName_m2842466433 (JsonReaderSettings_t24058356 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonReaderSettings::.ctor()
extern "C"  void JsonReaderSettings__ctor_m302022697 (JsonReaderSettings_t24058356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
