﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonReaderException
struct JsonReaderException_t354140082;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Exception
struct Exception_t3991598821;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Exception3991598821.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

// System.Void Newtonsoft.Json.JsonReaderException::set_LineNumber(System.Int32)
extern "C"  void JsonReaderException_set_LineNumber_m4050520062 (JsonReaderException_t354140082 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::set_LinePosition(System.Int32)
extern "C"  void JsonReaderException_set_LinePosition_m2103222558 (JsonReaderException_t354140082 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::set_Path(System.String)
extern "C"  void JsonReaderException_set_Path_m3592402413 (JsonReaderException_t354140082 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor()
extern "C"  void JsonReaderException__ctor_m3114011729 (JsonReaderException_t354140082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String)
extern "C"  void JsonReaderException__ctor_m1994776145 (JsonReaderException_t354140082 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonReaderException__ctor_m825365010 (JsonReaderException_t354140082 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.Exception,System.String,System.Int32,System.Int32)
extern "C"  void JsonReaderException__ctor_m828961953 (JsonReaderException_t354140082 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, String_t* ___path2, int32_t ___lineNumber3, int32_t ___linePosition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String)
extern "C"  JsonReaderException_t354140082 * JsonReaderException_Create_m1626706375 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern "C"  JsonReaderException_t354140082 * JsonReaderException_Create_m2295929423 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, String_t* ___message1, Exception_t3991598821 * ___ex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern "C"  JsonReaderException_t354140082 * JsonReaderException_Create_m1054137813 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___lineInfo0, String_t* ___path1, String_t* ___message2, Exception_t3991598821 * ___ex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
