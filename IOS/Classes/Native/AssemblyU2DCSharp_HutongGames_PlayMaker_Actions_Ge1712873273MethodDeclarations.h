﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSpeed
struct GetSpeed_t1712873273;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSpeed::.ctor()
extern "C"  void GetSpeed__ctor_m2886150989 (GetSpeed_t1712873273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::Reset()
extern "C"  void GetSpeed_Reset_m532583930 (GetSpeed_t1712873273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnEnter()
extern "C"  void GetSpeed_OnEnter_m2728923108 (GetSpeed_t1712873273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnUpdate()
extern "C"  void GetSpeed_OnUpdate_m2125797087 (GetSpeed_t1712873273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::DoGetSpeed()
extern "C"  void GetSpeed_DoGetSpeed_m3185395987 (GetSpeed_t1712873273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
