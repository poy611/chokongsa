﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddExplosionForce
struct AddExplosionForce_t3285275685;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::.ctor()
extern "C"  void AddExplosionForce__ctor_m1274421937 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::Reset()
extern "C"  void AddExplosionForce_Reset_m3215822174 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnPreprocess()
extern "C"  void AddExplosionForce_OnPreprocess_m3189620062 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnEnter()
extern "C"  void AddExplosionForce_OnEnter_m45530696 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnFixedUpdate()
extern "C"  void AddExplosionForce_OnFixedUpdate_m24215629 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::DoAddExplosionForce()
extern "C"  void AddExplosionForce_DoAddExplosionForce_m3509564123 (AddExplosionForce_t3285275685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
