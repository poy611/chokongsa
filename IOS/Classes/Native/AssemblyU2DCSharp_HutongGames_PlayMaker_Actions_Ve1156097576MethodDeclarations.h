﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2HighPassFilter
struct Vector2HighPassFilter_t1156097576;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2HighPassFilter::.ctor()
extern "C"  void Vector2HighPassFilter__ctor_m4221415438 (Vector2HighPassFilter_t1156097576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2HighPassFilter::Reset()
extern "C"  void Vector2HighPassFilter_Reset_m1867848379 (Vector2HighPassFilter_t1156097576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2HighPassFilter::OnEnter()
extern "C"  void Vector2HighPassFilter_OnEnter_m1722837093 (Vector2HighPassFilter_t1156097576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2HighPassFilter::OnUpdate()
extern "C"  void Vector2HighPassFilter_OnUpdate_m1001901694 (Vector2HighPassFilter_t1156097576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
