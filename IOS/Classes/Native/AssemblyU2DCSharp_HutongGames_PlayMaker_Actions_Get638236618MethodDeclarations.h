﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionFromRotation
struct GetQuaternionFromRotation_t638236618;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::.ctor()
extern "C"  void GetQuaternionFromRotation__ctor_m393781420 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::Reset()
extern "C"  void GetQuaternionFromRotation_Reset_m2335181657 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnEnter()
extern "C"  void GetQuaternionFromRotation_OnEnter_m4153518467 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnUpdate()
extern "C"  void GetQuaternionFromRotation_OnUpdate_m3338580256 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnLateUpdate()
extern "C"  void GetQuaternionFromRotation_OnLateUpdate_m2320688870 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnFixedUpdate()
extern "C"  void GetQuaternionFromRotation_OnFixedUpdate_m4097823560 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::DoQuatFromRotation()
extern "C"  void GetQuaternionFromRotation_DoQuatFromRotation_m1731561282 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
