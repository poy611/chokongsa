﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionStay
struct PlayMakerCollisionStay_t3871731003;
// UnityEngine.Collision
struct Collision_t2494107688;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2494107688.h"

// System.Void PlayMakerCollisionStay::OnCollisionStay(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionStay_OnCollisionStay_m2671619915 (PlayMakerCollisionStay_t3871731003 * __this, Collision_t2494107688 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionStay::.ctor()
extern "C"  void PlayMakerCollisionStay__ctor_m3054715906 (PlayMakerCollisionStay_t3871731003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
