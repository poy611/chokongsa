﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
struct QuestUIResponse_t3819980022;
// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;
// GooglePlayGames.Native.PInvoke.NativeQuestMilestone
struct NativeQuestMilestone_t2033850801;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::.ctor(System.IntPtr)
extern "C"  void QuestUIResponse__ctor_m437241663 (QuestUIResponse_t3819980022 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestStatus()
extern "C"  int32_t QuestUIResponse_RequestStatus_m1679408650 (QuestUIResponse_t3819980022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::RequestSucceeded()
extern "C"  bool QuestUIResponse_RequestSucceeded_m4035262797 (QuestUIResponse_t3819980022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::AcceptedQuest()
extern "C"  NativeQuest_t2496300529 * QuestUIResponse_AcceptedQuest_m2559045315 (QuestUIResponse_t3819980022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::MilestoneToClaim()
extern "C"  NativeQuestMilestone_t2033850801 * QuestUIResponse_MilestoneToClaim_m442217269 (QuestUIResponse_t3819980022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestUIResponse_CallDispose_m2767465041 (QuestUIResponse_t3819980022 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse::FromPointer(System.IntPtr)
extern "C"  QuestUIResponse_t3819980022 * QuestUIResponse_FromPointer_m4174937297 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
