﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIButton
struct GUIButton_t3100740507;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIButton::.ctor()
extern "C"  void GUIButton__ctor_m2558482427 (GUIButton_t3100740507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIButton::Reset()
extern "C"  void GUIButton_Reset_m204915368 (GUIButton_t3100740507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIButton::OnGUI()
extern "C"  void GUIButton_OnGUI_m2053881077 (GUIButton_t3100740507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
