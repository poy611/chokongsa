﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3582821007.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_IntPtr4010401971.h"

// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeRoom_RealTimeRoom_Status_m2039121804 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Description_m2559466261 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoom_RealTimeRoom_Variant_m3087259124 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t RealTimeRoom_RealTimeRoom_CreationTime_m1329026200 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Participants_Length_m3618104417 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t RealTimeRoom_RealTimeRoom_Participants_GetElement_m3658866019 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool RealTimeRoom_RealTimeRoom_Valid_m135601220 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_RemainingAutomatchingSlots(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoom_RealTimeRoom_RemainingAutomatchingSlots_m940488768 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_AutomatchWaitEstimate(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t RealTimeRoom_RealTimeRoom_AutomatchWaitEstimate_m4268584131 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeRoom_RealTimeRoom_CreatingParticipant_m596834755 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Id_m1805342290 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoom_RealTimeRoom_Dispose_m875074419 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
