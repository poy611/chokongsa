﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoStopLoopedEffect
struct  CFX_AutoStopLoopedEffect_t3902828655  : public MonoBehaviour_t667441552
{
public:
	// System.Single CFX_AutoStopLoopedEffect::effectDuration
	float ___effectDuration_2;
	// System.Single CFX_AutoStopLoopedEffect::d
	float ___d_3;

public:
	inline static int32_t get_offset_of_effectDuration_2() { return static_cast<int32_t>(offsetof(CFX_AutoStopLoopedEffect_t3902828655, ___effectDuration_2)); }
	inline float get_effectDuration_2() const { return ___effectDuration_2; }
	inline float* get_address_of_effectDuration_2() { return &___effectDuration_2; }
	inline void set_effectDuration_2(float value)
	{
		___effectDuration_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(CFX_AutoStopLoopedEffect_t3902828655, ___d_3)); }
	inline float get_d_3() const { return ___d_3; }
	inline float* get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(float value)
	{
		___d_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
