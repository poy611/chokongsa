﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1809280638;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t2281118987  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::call
	MethodCall_2_t1809280638 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t2281118987, ___call_0)); }
	inline MethodCall_2_t1809280638 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t1809280638 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t1809280638 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier(&___call_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
