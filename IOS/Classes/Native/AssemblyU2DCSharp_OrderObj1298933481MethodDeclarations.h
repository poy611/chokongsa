﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OrderObj
struct OrderObj_t1298933481;

#include "codegen/il2cpp-codegen.h"

// System.Void OrderObj::.ctor()
extern "C"  void OrderObj__ctor_m1379356114 (OrderObj_t1298933481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
