﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// GooglePlayGames.BasicApi.Achievement
struct Achievement_t1261647177;
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t1187783161;
// GooglePlayGames.PlayGamesScore
struct PlayGamesScore_t486124539;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t2200833403;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t1804230813;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t562088433;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t3582269991;
// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t3727527619;
// GooglePlayGames.Native.PInvoke.NativePlayer
struct NativePlayer_t2636885988;
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct BaseReferenceHolder_t2237584300;
// GooglePlayGames.Native.PInvoke.NativeEvent
struct NativeEvent_t2485247913;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t107909964;
// CFX_LightIntensityFade
struct CFX_LightIntensityFade_t2919443491;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// CalculationData
struct CalculationData_t2213441011;
// ResultData
struct ResultData_t1421128071;
// ResultAllData
struct ResultAllData_t3272948206;
// ResultSaveData
struct ResultSaveData_t3126241828;
// GradeData
struct GradeData_t483491841;
// UserExperience
struct UserExperience_t3961908149;
// StructforMinigame
struct StructforMinigame_t1286533533;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve1261647177.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesScore486124539.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2200833403.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl1804230813.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3727527619.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N2636885988.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_B2237584300.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_N2485247913.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3337232325.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "AssemblyU2DCSharp_CFX_AutoDestructShuriken107909964.h"
#include "AssemblyU2DCSharp_CFX_LightIntensityFade2919443491.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_An2191327052.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2771812670.h"
#include "AssemblyU2DCSharp_ConstforMinigame2789090703.h"
#include "AssemblyU2DCSharp_CalculationData2213441011.h"
#include "AssemblyU2DCSharp_ResultData1421128071.h"
#include "AssemblyU2DCSharp_ResultAllData3272948206.h"
#include "AssemblyU2DCSharp_ResultSaveData3126241828.h"
#include "AssemblyU2DCSharp_GradeData483491841.h"
#include "AssemblyU2DCSharp_UserExperience3961908149.h"
#include "AssemblyU2DCSharp_StructforMinigame1286533533.h"

#pragma once
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3251685236  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Achievement_t1261647177 * m_Items[1];

public:
	inline Achievement_t1261647177 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Achievement_t1261647177 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Achievement_t1261647177 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Events.IEvent[]
struct IEventU5BU5D_t659202564  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.PlayGamesScore[]
struct PlayGamesScoreU5BU5D_t797504954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayGamesScore_t486124539 * m_Items[1];

public:
	inline PlayGamesScore_t486124539 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayGamesScore_t486124539 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayGamesScore_t486124539 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.Invitation[]
struct InvitationU5BU5D_t4211826234  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Invitation_t2200833403 * m_Items[1];

public:
	inline Invitation_t2200833403 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Invitation_t2200833403 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Invitation_t2200833403 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
struct ParticipantU5BU5D_t366346320  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Participant_t1804230813 * m_Items[1];

public:
	inline Participant_t1804230813 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Participant_t1804230813 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Participant_t1804230813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch[]
struct TurnBasedMatchU5BU5D_t2145394316  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TurnBasedMatch_t3573041681 * m_Items[1];

public:
	inline TurnBasedMatch_t3573041681 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TurnBasedMatch_t3573041681 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TurnBasedMatch_t3573041681 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult[]
struct ParticipantResultU5BU5D_t1824578127  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GooglePlayGames.BasicApi.Quests.IQuest[]
struct IQuestU5BU5D_t3125887788  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
struct ISavedGameMetadataU5BU5D_t2977959262  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.Player[]
struct PlayerU5BU5D_t2747172562  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Player_t3727527619 * m_Items[1];

public:
	inline Player_t3727527619 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Player_t3727527619 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Player_t3727527619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.Native.PInvoke.NativePlayer[]
struct NativePlayerU5BU5D_t3461726221  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NativePlayer_t2636885988 * m_Items[1];

public:
	inline NativePlayer_t2636885988 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NativePlayer_t2636885988 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NativePlayer_t2636885988 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder[]
struct BaseReferenceHolderU5BU5D_t2685004837  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BaseReferenceHolder_t2237584300 * m_Items[1];

public:
	inline BaseReferenceHolder_t2237584300 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BaseReferenceHolder_t2237584300 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BaseReferenceHolder_t2237584300 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.Native.PInvoke.NativeEvent[]
struct NativeEventU5BU5D_t260917140  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NativeEvent_t2485247913 * m_Items[1];

public:
	inline NativeEvent_t2485247913 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NativeEvent_t2485247913 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NativeEvent_t2485247913 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
struct MultiplayerParticipantU5BU5D_t3838171016  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MultiplayerParticipant_t3337232325 * m_Items[1];

public:
	inline MultiplayerParticipant_t3337232325 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MultiplayerParticipant_t3337232325 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MultiplayerParticipant_t3337232325 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t2382929755  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t360142240  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// CFX_AutoDestructShuriken[]
struct CFX_AutoDestructShurikenU5BU5D_t2718308869  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_AutoDestructShuriken_t107909964 * m_Items[1];

public:
	inline CFX_AutoDestructShuriken_t107909964 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CFX_AutoDestructShuriken_t107909964 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CFX_AutoDestructShuriken_t107909964 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CFX_LightIntensityFade[]
struct CFX_LightIntensityFadeU5BU5D_t3436374002  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CFX_LightIntensityFade_t2919443491 * m_Items[1];

public:
	inline CFX_LightIntensityFade_t2919443491 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CFX_LightIntensityFade_t2919443491 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CFX_LightIntensityFade_t2919443491 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation[]
struct CalculationU5BU5D_t3054796293  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation[]
struct CalculationU5BU5D_t3522546699  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// ConstforMinigame[,]
struct ConstforMinigameU5BU2CU5D_t3700727639  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstforMinigame_t2789090703 * m_Items[1];

public:
	inline ConstforMinigame_t2789090703 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstforMinigame_t2789090703 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstforMinigame_t2789090703 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ConstforMinigame_t2789090703 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items[index];
	}
	inline ConstforMinigame_t2789090703 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, ConstforMinigame_t2789090703 * value)
	{
		il2cpp_array_size_t index = i * bounds[1].length + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ConstforMinigame[]
struct ConstforMinigameU5BU5D_t3700727638  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConstforMinigame_t2789090703 * m_Items[1];

public:
	inline ConstforMinigame_t2789090703 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstforMinigame_t2789090703 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstforMinigame_t2789090703 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CalculationData[]
struct CalculationDataU5BU5D_t2983843042  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CalculationData_t2213441011 * m_Items[1];

public:
	inline CalculationData_t2213441011 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CalculationData_t2213441011 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CalculationData_t2213441011 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ResultData[]
struct ResultDataU5BU5D_t4039199870  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultData_t1421128071 * m_Items[1];

public:
	inline ResultData_t1421128071 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultData_t1421128071 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultData_t1421128071 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ResultAllData[]
struct ResultAllDataU5BU5D_t3963431579  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultAllData_t3272948206 * m_Items[1];

public:
	inline ResultAllData_t3272948206 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultAllData_t3272948206 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultAllData_t3272948206 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ResultSaveData[]
struct ResultSaveDataU5BU5D_t1458984141  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResultSaveData_t3126241828 * m_Items[1];

public:
	inline ResultSaveData_t3126241828 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResultSaveData_t3126241828 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResultSaveData_t3126241828 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GradeData[]
struct GradeDataU5BU5D_t1462969052  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GradeData_t483491841 * m_Items[1];

public:
	inline GradeData_t483491841 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GradeData_t483491841 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GradeData_t483491841 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UserExperience[]
struct UserExperienceU5BU5D_t4267388376  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UserExperience_t3961908149 * m_Items[1];

public:
	inline UserExperience_t3961908149 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UserExperience_t3961908149 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UserExperience_t3961908149 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StructforMinigame_t1286533533 * m_Items[1];

public:
	inline StructforMinigame_t1286533533 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StructforMinigame_t1286533533 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StructforMinigame_t1286533533 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
