﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct GetAnimatorIsMatchingTarget_t2980401859;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::.ctor()
extern "C"  void GetAnimatorIsMatchingTarget__ctor_m669529043 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::Reset()
extern "C"  void GetAnimatorIsMatchingTarget_Reset_m2610929280 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnEnter()
extern "C"  void GetAnimatorIsMatchingTarget_OnEnter_m2859011818 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnActionUpdate()
extern "C"  void GetAnimatorIsMatchingTarget_OnActionUpdate_m2880568751 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::DoCheckIsMatchingActive()
extern "C"  void GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m3097764091 (GetAnimatorIsMatchingTarget_t2980401859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
