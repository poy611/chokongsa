﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
struct NativeConflictResolver_t4054085262;
// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t3479575958;
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F
struct U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80
struct  U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::resolver
	NativeConflictResolver_t4054085262 * ___resolver_0;
	// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::original
	NativeSnapshotMetadata_t3479575958 * ___original_1;
	// GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::unmerged
	NativeSnapshotMetadata_t3479575958 * ___unmerged_2;
	// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F/<InternalManualOpen>c__AnonStorey80::<>f__ref$127
	U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 * ___U3CU3Ef__refU24127_3;

public:
	inline static int32_t get_offset_of_resolver_0() { return static_cast<int32_t>(offsetof(U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821, ___resolver_0)); }
	inline NativeConflictResolver_t4054085262 * get_resolver_0() const { return ___resolver_0; }
	inline NativeConflictResolver_t4054085262 ** get_address_of_resolver_0() { return &___resolver_0; }
	inline void set_resolver_0(NativeConflictResolver_t4054085262 * value)
	{
		___resolver_0 = value;
		Il2CppCodeGenWriteBarrier(&___resolver_0, value);
	}

	inline static int32_t get_offset_of_original_1() { return static_cast<int32_t>(offsetof(U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821, ___original_1)); }
	inline NativeSnapshotMetadata_t3479575958 * get_original_1() const { return ___original_1; }
	inline NativeSnapshotMetadata_t3479575958 ** get_address_of_original_1() { return &___original_1; }
	inline void set_original_1(NativeSnapshotMetadata_t3479575958 * value)
	{
		___original_1 = value;
		Il2CppCodeGenWriteBarrier(&___original_1, value);
	}

	inline static int32_t get_offset_of_unmerged_2() { return static_cast<int32_t>(offsetof(U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821, ___unmerged_2)); }
	inline NativeSnapshotMetadata_t3479575958 * get_unmerged_2() const { return ___unmerged_2; }
	inline NativeSnapshotMetadata_t3479575958 ** get_address_of_unmerged_2() { return &___unmerged_2; }
	inline void set_unmerged_2(NativeSnapshotMetadata_t3479575958 * value)
	{
		___unmerged_2 = value;
		Il2CppCodeGenWriteBarrier(&___unmerged_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24127_3() { return static_cast<int32_t>(offsetof(U3CInternalManualOpenU3Ec__AnonStorey80_t3391433821, ___U3CU3Ef__refU24127_3)); }
	inline U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 * get_U3CU3Ef__refU24127_3() const { return ___U3CU3Ef__refU24127_3; }
	inline U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 ** get_address_of_U3CU3Ef__refU24127_3() { return &___U3CU3Ef__refU24127_3; }
	inline void set_U3CU3Ef__refU24127_3(U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 * value)
	{
		___U3CU3Ef__refU24127_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24127_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
