﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArraySet
struct ArraySet_t2749480945;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArraySet::.ctor()
extern "C"  void ArraySet__ctor_m4287830421 (ArraySet_t2749480945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::Reset()
extern "C"  void ArraySet_Reset_m1934263362 (ArraySet_t2749480945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnEnter()
extern "C"  void ArraySet_OnEnter_m1123126316 (ArraySet_t2749480945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnUpdate()
extern "C"  void ArraySet_OnUpdate_m3885704087 (ArraySet_t2749480945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::DoGetValue()
extern "C"  void ArraySet_DoGetValue_m183957109 (ArraySet_t2749480945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
