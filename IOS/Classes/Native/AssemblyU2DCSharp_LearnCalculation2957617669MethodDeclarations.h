﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearnCalculation
struct LearnCalculation_t2957617669;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void LearnCalculation::.ctor()
extern "C"  void LearnCalculation__ctor_m79295030 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::Start()
extern "C"  void LearnCalculation_Start_m3321400118 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::SettingRandomNum()
extern "C"  void LearnCalculation_SettingRandomNum_m3019646817 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::PadClick(System.String)
extern "C"  void LearnCalculation_PadClick_m4047840927 (LearnCalculation_t2957617669 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::deleteClick()
extern "C"  void LearnCalculation_deleteClick_m3596864657 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::cancelClick()
extern "C"  void LearnCalculation_cancelClick_m829844706 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::selectClick()
extern "C"  void LearnCalculation_selectClick_m230125600 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnCalculation::backButtonClick()
extern "C"  void LearnCalculation_backButtonClick_m1444609539 (LearnCalculation_t2957617669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LearnCalculation::SetRandNum2DIV(System.Int32)
extern "C"  int32_t LearnCalculation_SetRandNum2DIV_m2666335051 (LearnCalculation_t2957617669 * __this, int32_t ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
