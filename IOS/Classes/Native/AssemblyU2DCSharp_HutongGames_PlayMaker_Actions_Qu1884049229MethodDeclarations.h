﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionBaseAction
struct QuaternionBaseAction_t1884049229;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionBaseAction::.ctor()
extern "C"  void QuaternionBaseAction__ctor_m1701621177 (QuaternionBaseAction_t1884049229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionBaseAction::Awake()
extern "C"  void QuaternionBaseAction_Awake_m1939226396 (QuaternionBaseAction_t1884049229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
