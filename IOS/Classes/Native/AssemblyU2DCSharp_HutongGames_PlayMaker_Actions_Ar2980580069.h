﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArraySort
struct  ArraySort_t2980580069  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArraySort::array
	FsmArray_t2129666875 * ___array_11;

public:
	inline static int32_t get_offset_of_array_11() { return static_cast<int32_t>(offsetof(ArraySort_t2980580069, ___array_11)); }
	inline FsmArray_t2129666875 * get_array_11() const { return ___array_11; }
	inline FsmArray_t2129666875 ** get_address_of_array_11() { return &___array_11; }
	inline void set_array_11(FsmArray_t2129666875 * value)
	{
		___array_11 = value;
		Il2CppCodeGenWriteBarrier(&___array_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
