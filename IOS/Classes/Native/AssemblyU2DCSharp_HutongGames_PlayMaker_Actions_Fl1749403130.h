﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatRound
struct  FloatRound_t1749403130  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatRound::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.FloatRound::resultAsInt
	FsmInt_t1596138449 * ___resultAsInt_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatRound::resultAsFloat
	FsmFloat_t2134102846 * ___resultAsFloat_13;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatRound::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_floatVariable_11() { return static_cast<int32_t>(offsetof(FloatRound_t1749403130, ___floatVariable_11)); }
	inline FsmFloat_t2134102846 * get_floatVariable_11() const { return ___floatVariable_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_11() { return &___floatVariable_11; }
	inline void set_floatVariable_11(FsmFloat_t2134102846 * value)
	{
		___floatVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_11, value);
	}

	inline static int32_t get_offset_of_resultAsInt_12() { return static_cast<int32_t>(offsetof(FloatRound_t1749403130, ___resultAsInt_12)); }
	inline FsmInt_t1596138449 * get_resultAsInt_12() const { return ___resultAsInt_12; }
	inline FsmInt_t1596138449 ** get_address_of_resultAsInt_12() { return &___resultAsInt_12; }
	inline void set_resultAsInt_12(FsmInt_t1596138449 * value)
	{
		___resultAsInt_12 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsInt_12, value);
	}

	inline static int32_t get_offset_of_resultAsFloat_13() { return static_cast<int32_t>(offsetof(FloatRound_t1749403130, ___resultAsFloat_13)); }
	inline FsmFloat_t2134102846 * get_resultAsFloat_13() const { return ___resultAsFloat_13; }
	inline FsmFloat_t2134102846 ** get_address_of_resultAsFloat_13() { return &___resultAsFloat_13; }
	inline void set_resultAsFloat_13(FsmFloat_t2134102846 * value)
	{
		___resultAsFloat_13 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsFloat_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(FloatRound_t1749403130, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
