﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState
struct ShutdownState_t1060206706;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t1352686482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRea1352686482.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C"  void ShutdownState__ctor_m618805527 (ShutdownState_t1060206706 * __this, RoomSession_t1352686482 * ___session0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::IsActive()
extern "C"  bool ShutdownState_IsActive_m2969927743 (ShutdownState_t1060206706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::LeaveRoom()
extern "C"  void ShutdownState_LeaveRoom_m3584569401 (ShutdownState_t1060206706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
