﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey8D
struct U3CAcceptFromInboxU3Ec__AnonStorey8D_t668712729;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t1459363083;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T1459363083.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey8D::.ctor()
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey8D__ctor_m891026338 (U3CAcceptFromInboxU3Ec__AnonStorey8D_t668712729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey8D::<>m__7C(GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse)
extern "C"  void U3CAcceptFromInboxU3Ec__AnonStorey8D_U3CU3Em__7C_m3202813740 (U3CAcceptFromInboxU3Ec__AnonStorey8D_t668712729 * __this, MatchInboxUIResponse_t1459363083 * ___callbackResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
