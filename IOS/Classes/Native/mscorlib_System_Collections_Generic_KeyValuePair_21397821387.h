﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>
struct  KeyValuePair_2_t1397821387 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Fsm_t1527112426 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RaycastHit2D_t1374744384  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1397821387, ___key_0)); }
	inline Fsm_t1527112426 * get_key_0() const { return ___key_0; }
	inline Fsm_t1527112426 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(Fsm_t1527112426 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1397821387, ___value_1)); }
	inline RaycastHit2D_t1374744384  get_value_1() const { return ___value_1; }
	inline RaycastHit2D_t1374744384 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RaycastHit2D_t1374744384  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
