﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntAdd
struct IntAdd_t3052069402;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntAdd::.ctor()
extern "C"  void IntAdd__ctor_m3750984588 (IntAdd_t3052069402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntAdd::Reset()
extern "C"  void IntAdd_Reset_m1397417529 (IntAdd_t3052069402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntAdd::OnEnter()
extern "C"  void IntAdd_OnEnter_m610356323 (IntAdd_t3052069402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntAdd::OnUpdate()
extern "C"  void IntAdd_OnUpdate_m874736192 (IntAdd_t3052069402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
