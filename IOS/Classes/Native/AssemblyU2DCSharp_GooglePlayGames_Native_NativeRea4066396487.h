﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D
struct U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E
struct  U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E::invitation
	MultiplayerInvitation_t3411188537 * ___invitation_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E::<>f__ref$109
	U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * ___U3CU3Ef__refU24109_1;

public:
	inline static int32_t get_offset_of_invitation_0() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487, ___invitation_0)); }
	inline MultiplayerInvitation_t3411188537 * get_invitation_0() const { return ___invitation_0; }
	inline MultiplayerInvitation_t3411188537 ** get_address_of_invitation_0() { return &___invitation_0; }
	inline void set_invitation_0(MultiplayerInvitation_t3411188537 * value)
	{
		___invitation_0 = value;
		Il2CppCodeGenWriteBarrier(&___invitation_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24109_1() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487, ___U3CU3Ef__refU24109_1)); }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * get_U3CU3Ef__refU24109_1() const { return ___U3CU3Ef__refU24109_1; }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 ** get_address_of_U3CU3Ef__refU24109_1() { return &___U3CU3Ef__refU24109_1; }
	inline void set_U3CU3Ef__refU24109_1(U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * value)
	{
		___U3CU3Ef__refU24109_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24109_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
