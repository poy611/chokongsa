﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey60
struct U3CFetchMatchingStateU3Ec__AnonStorey60_t2052728451;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
struct FetchListResponse_t32965304;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Que32965304.h"

// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey60::.ctor()
extern "C"  void U3CFetchMatchingStateU3Ec__AnonStorey60__ctor_m615559032 (U3CFetchMatchingStateU3Ec__AnonStorey60_t2052728451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey60::<>m__3D(GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse)
extern "C"  void U3CFetchMatchingStateU3Ec__AnonStorey60_U3CU3Em__3D_m3793067720 (U3CFetchMatchingStateU3Ec__AnonStorey60_t2052728451 * __this, FetchListResponse_t32965304 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
