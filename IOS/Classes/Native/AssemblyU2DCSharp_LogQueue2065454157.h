﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ResultData>
struct List_1_t2789313623;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogQueue
struct  LogQueue_t2065454157  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<ResultData> LogQueue::logQueue
	List_1_t2789313623 * ___logQueue_0;

public:
	inline static int32_t get_offset_of_logQueue_0() { return static_cast<int32_t>(offsetof(LogQueue_t2065454157, ___logQueue_0)); }
	inline List_1_t2789313623 * get_logQueue_0() const { return ___logQueue_0; }
	inline List_1_t2789313623 ** get_address_of_logQueue_0() { return &___logQueue_0; }
	inline void set_logQueue_0(List_1_t2789313623 * value)
	{
		___logQueue_0 = value;
		Il2CppCodeGenWriteBarrier(&___logQueue_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
