﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1455007855.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2855123690_gshared (InternalEnumerator_1_t1455007855 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2855123690(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1455007855 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2855123690_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054(__this, method) ((  void (*) (InternalEnumerator_1_t1455007855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1085288054_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1455007855 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1118585836_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2202140993_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2202140993(__this, method) ((  void (*) (InternalEnumerator_1_t1455007855 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2202140993_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2345374502_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2345374502(__this, method) ((  bool (*) (InternalEnumerator_1_t1455007855 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2345374502_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1977727635_gshared (InternalEnumerator_1_t1455007855 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1977727635(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1455007855 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1977727635_gshared)(__this, method)
