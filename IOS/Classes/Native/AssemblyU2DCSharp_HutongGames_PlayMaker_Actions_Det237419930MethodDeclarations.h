﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DetachChildren
struct DetachChildren_t237419930;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.DetachChildren::.ctor()
extern "C"  void DetachChildren__ctor_m2698967052 (DetachChildren_t237419930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::Reset()
extern "C"  void DetachChildren_Reset_m345399993 (DetachChildren_t237419930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::OnEnter()
extern "C"  void DetachChildren_OnEnter_m3233786083 (DetachChildren_t237419930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::DoDetachChildren(UnityEngine.GameObject)
extern "C"  void DetachChildren_DoDetachChildren_m2713073613 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
