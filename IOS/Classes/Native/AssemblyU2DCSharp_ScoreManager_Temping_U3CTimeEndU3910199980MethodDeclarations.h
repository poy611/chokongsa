﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Temping/<TimeEnd>c__Iterator24
struct U3CTimeEndU3Ec__Iterator24_t3910199980;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator24__ctor_m3658756671 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<TimeEnd>c__Iterator24::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator24_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3789971955 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<TimeEnd>c__Iterator24::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator24_System_Collections_IEnumerator_get_Current_m1242082183 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Temping/<TimeEnd>c__Iterator24::MoveNext()
extern "C"  bool U3CTimeEndU3Ec__Iterator24_MoveNext_m3718282581 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator24_Dispose_m2680195772 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::Reset()
extern "C"  void U3CTimeEndU3Ec__Iterator24_Reset_m1305189612 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
