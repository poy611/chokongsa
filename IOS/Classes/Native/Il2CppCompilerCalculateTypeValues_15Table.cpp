﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast844255649.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3054018868.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount82957758.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId1025767334.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName1256488005.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespace4291495708.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName844315030.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString2723009436.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat2260200095.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit1437129026.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains1738767914.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1709434719.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1726871446.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2992425472.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen1116358498.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize1100613508.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate3514715389.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction3865271111.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean149146711.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1422750722.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue844510361.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse85302738.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang844255481.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction670389548.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber2580631252.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum1422755706.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor85633211.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil843991056.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound96810557.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3956813165.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement2953066405.h"
#include "System_Xml_System_Xml_XPath_XPathSorters1807339567.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3393478238.h"
#include "System_Xml_System_Xml_XPath_Expression2556460284.h"
#include "System_Xml_System_Xml_XPath_ExprBinary1545048250.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean513597673.h"
#include "System_Xml_System_Xml_XPath_ExprOR2486227068.h"
#include "System_Xml_System_Xml_XPath_ExprAND3747310744.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr1406255795.h"
#include "System_Xml_System_Xml_XPath_ExprEQ2486226757.h"
#include "System_Xml_System_Xml_XPath_ExprNE2486227024.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr3718551712.h"
#include "System_Xml_System_Xml_XPath_ExprGT2486226822.h"
#include "System_Xml_System_Xml_XPath_ExprGE2486226807.h"
#include "System_Xml_System_Xml_XPath_ExprLT2486226977.h"
#include "System_Xml_System_Xml_XPath_ExprLE2486226962.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric2743439310.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS4186658099.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS623244849.h"
#include "System_Xml_System_Xml_XPath_ExprMULT4186577097.h"
#include "System_Xml_System_Xml_XPath_ExprDIV3747313490.h"
#include "System_Xml_System_Xml_XPath_ExprMOD3747322307.h"
#include "System_Xml_System_Xml_XPath_ExprNEG3747322961.h"
#include "System_Xml_System_Xml_XPath_NodeSet2875795446.h"
#include "System_Xml_System_Xml_XPath_ExprUNION630776976.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH628862782.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH22003606286.h"
#include "System_Xml_System_Xml_XPath_ExprRoot4186752155.h"
#include "System_Xml_System_Xml_XPath_Axes2202774217.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier3783148883.h"
#include "System_Xml_System_Xml_XPath_NodeTest2939071960.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest282671026.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest2799571587.h"
#include "System_Xml_System_Xml_XPath_ExprFilter1659523121.h"
#include "System_Xml_System_Xml_XPath_ExprNumber1899651074.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral631346544.h"
#include "System_Xml_System_Xml_XPath_ExprVariable3764672565.h"
#include "System_Xml_System_Xml_XPath_ExprParens1938591074.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2391178772.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3990388239.h"
#include "System_Xml_System_Xml_XPath_BaseIterator1327316739.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2849115671.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator544647972.h"
#include "System_Xml_System_Xml_XPath_SelfIterator197024894.h"
#include "System_Xml_System_Xml_XPath_NullIterator2905335737.h"
#include "System_Xml_System_Xml_XPath_ParensIterator3418004251.h"
#include "System_Xml_System_Xml_XPath_ParentIterator1610549788.h"
#include "System_Xml_System_Xml_XPath_ChildIterator3087618016.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterato116837539.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat1318768689.h"
#include "System_Xml_System_Xml_XPath_AncestorIterator2080208005.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator3368289780.h"
#include "System_Xml_System_Xml_XPath_DescendantIterator928464879.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat1437473246.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator3149710293.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator1039893511.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator1295432863.h"
#include "System_Xml_System_Xml_XPath_AttributeIterator4254482656.h"
#include "System_Xml_System_Xml_XPath_AxisIterator953634771.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator1419496431.h"
#include "System_Xml_System_Xml_XPath_SortedIterator4014821679.h"
#include "System_Xml_System_Xml_XPath_SlashIterator384731969.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator3158177019.h"
#include "System_Xml_System_Xml_XPath_ListIterator4080389072.h"
#include "System_Xml_System_Xml_XPath_UnionIterator1391611539.h"
#include "System_Xml_Mono_Xml_XPath_Tokenizer1510902653.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer3727165134.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2221114443.h"
#include "System_Xml_System_Xml_XPath_XPathException1803876086.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (XPathFunctionLast_t844255649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (XPathFunctionPosition_t3054018868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (XPathFunctionCount_t82957758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[1] = 
{
	XPathFunctionCount_t82957758::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (XPathFunctionId_t1025767334), -1, sizeof(XPathFunctionId_t1025767334_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1503[2] = 
{
	XPathFunctionId_t1025767334::get_offset_of_arg0_0(),
	XPathFunctionId_t1025767334_StaticFields::get_offset_of_rgchWhitespace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (XPathFunctionLocalName_t1256488005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[1] = 
{
	XPathFunctionLocalName_t1256488005::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (XPathFunctionNamespaceUri_t4291495708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[1] = 
{
	XPathFunctionNamespaceUri_t4291495708::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (XPathFunctionName_t844315030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[1] = 
{
	XPathFunctionName_t844315030::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (XPathFunctionString_t2723009436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[1] = 
{
	XPathFunctionString_t2723009436::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (XPathFunctionConcat_t2260200095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[1] = 
{
	XPathFunctionConcat_t2260200095::get_offset_of_rgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (XPathFunctionStartsWith_t1437129026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[2] = 
{
	XPathFunctionStartsWith_t1437129026::get_offset_of_arg0_0(),
	XPathFunctionStartsWith_t1437129026::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (XPathFunctionContains_t1738767914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[2] = 
{
	XPathFunctionContains_t1738767914::get_offset_of_arg0_0(),
	XPathFunctionContains_t1738767914::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (XPathFunctionSubstringBefore_t1709434719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[2] = 
{
	XPathFunctionSubstringBefore_t1709434719::get_offset_of_arg0_0(),
	XPathFunctionSubstringBefore_t1709434719::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (XPathFunctionSubstringAfter_t1726871446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[2] = 
{
	XPathFunctionSubstringAfter_t1726871446::get_offset_of_arg0_0(),
	XPathFunctionSubstringAfter_t1726871446::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (XPathFunctionSubstring_t2992425472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[3] = 
{
	XPathFunctionSubstring_t2992425472::get_offset_of_arg0_0(),
	XPathFunctionSubstring_t2992425472::get_offset_of_arg1_1(),
	XPathFunctionSubstring_t2992425472::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (XPathFunctionStringLength_t1116358498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[1] = 
{
	XPathFunctionStringLength_t1116358498::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (XPathFunctionNormalizeSpace_t1100613508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[1] = 
{
	XPathFunctionNormalizeSpace_t1100613508::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (XPathFunctionTranslate_t3514715389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[3] = 
{
	XPathFunctionTranslate_t3514715389::get_offset_of_arg0_0(),
	XPathFunctionTranslate_t3514715389::get_offset_of_arg1_1(),
	XPathFunctionTranslate_t3514715389::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (XPathBooleanFunction_t3865271111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (XPathFunctionBoolean_t149146711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[1] = 
{
	XPathFunctionBoolean_t149146711::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (XPathFunctionNot_t1422750722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	XPathFunctionNot_t1422750722::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (XPathFunctionTrue_t844510361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (XPathFunctionFalse_t85302738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (XPathFunctionLang_t844255481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1522[1] = 
{
	XPathFunctionLang_t844255481::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (XPathNumericFunction_t670389548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (XPathFunctionNumber_t2580631252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	XPathFunctionNumber_t2580631252::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (XPathFunctionSum_t1422755706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[1] = 
{
	XPathFunctionSum_t1422755706::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (XPathFunctionFloor_t85633211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1526[1] = 
{
	XPathFunctionFloor_t85633211::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (XPathFunctionCeil_t843991056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1527[1] = 
{
	XPathFunctionCeil_t843991056::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (XPathFunctionRound_t96810557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[1] = 
{
	XPathFunctionRound_t96810557::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (CompiledExpression_t3956813165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[4] = 
{
	CompiledExpression_t3956813165::get_offset_of__nsm_0(),
	CompiledExpression_t3956813165::get_offset_of__expr_1(),
	CompiledExpression_t3956813165::get_offset_of__sorters_2(),
	CompiledExpression_t3956813165::get_offset_of_rawExpression_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (XPathSortElement_t2953066405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[2] = 
{
	XPathSortElement_t2953066405::get_offset_of_Navigator_0(),
	XPathSortElement_t2953066405::get_offset_of_Values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (XPathSorters_t1807339567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1531[1] = 
{
	XPathSorters_t1807339567::get_offset_of__rgSorters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (XPathSorter_t3393478238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[3] = 
{
	XPathSorter_t3393478238::get_offset_of__expr_0(),
	XPathSorter_t3393478238::get_offset_of__cmp_1(),
	XPathSorter_t3393478238::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (Expression_t2556460284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (ExprBinary_t1545048250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[2] = 
{
	ExprBinary_t1545048250::get_offset_of__left_0(),
	ExprBinary_t1545048250::get_offset_of__right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (ExprBoolean_t513597673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (ExprOR_t2486227068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (ExprAND_t3747310744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (EqualityExpr_t1406255795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1538[1] = 
{
	EqualityExpr_t1406255795::get_offset_of_trueVal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (ExprEQ_t2486226757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (ExprNE_t2486227024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (RelationalExpr_t3718551712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (ExprGT_t2486226822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (ExprGE_t2486226807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (ExprLT_t2486226977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (ExprLE_t2486226962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (ExprNumeric_t2743439310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (ExprPLUS_t4186658099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (ExprMINUS_t623244849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (ExprMULT_t4186577097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (ExprDIV_t3747313490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (ExprMOD_t3747322307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (ExprNEG_t3747322961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[1] = 
{
	ExprNEG_t3747322961::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (NodeSet_t2875795446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (ExprUNION_t630776976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[2] = 
{
	ExprUNION_t630776976::get_offset_of_left_0(),
	ExprUNION_t630776976::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (ExprSLASH_t628862782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[2] = 
{
	ExprSLASH_t628862782::get_offset_of_left_0(),
	ExprSLASH_t628862782::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (ExprSLASH2_t2003606286), -1, sizeof(ExprSLASH2_t2003606286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1556[3] = 
{
	ExprSLASH2_t2003606286::get_offset_of_left_0(),
	ExprSLASH2_t2003606286::get_offset_of_right_1(),
	ExprSLASH2_t2003606286_StaticFields::get_offset_of_DescendantOrSelfStar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (ExprRoot_t4186752155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (Axes_t2202774217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1558[14] = 
{
	Axes_t2202774217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (AxisSpecifier_t3783148883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1559[1] = 
{
	AxisSpecifier_t3783148883::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (NodeTest_t2939071960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1560[1] = 
{
	NodeTest_t2939071960::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (NodeTypeTest_t282671026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[2] = 
{
	NodeTypeTest_t282671026::get_offset_of_type_1(),
	NodeTypeTest_t282671026::get_offset_of__param_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (NodeNameTest_t2799571587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1562[2] = 
{
	NodeNameTest_t2799571587::get_offset_of__name_1(),
	NodeNameTest_t2799571587::get_offset_of_resolvedName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (ExprFilter_t1659523121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1563[2] = 
{
	ExprFilter_t1659523121::get_offset_of_expr_0(),
	ExprFilter_t1659523121::get_offset_of_pred_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (ExprNumber_t1899651074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1564[1] = 
{
	ExprNumber_t1899651074::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (ExprLiteral_t631346544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1565[1] = 
{
	ExprLiteral_t631346544::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (ExprVariable_t3764672565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1566[2] = 
{
	ExprVariable_t3764672565::get_offset_of__name_0(),
	ExprVariable_t3764672565::get_offset_of_resolvedName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (ExprParens_t1938591074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1567[1] = 
{
	ExprParens_t1938591074::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (FunctionArguments_t2391178772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[2] = 
{
	FunctionArguments_t2391178772::get_offset_of__arg_0(),
	FunctionArguments_t2391178772::get_offset_of__tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (ExprFunctionCall_t3990388239), -1, sizeof(ExprFunctionCall_t3990388239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1569[4] = 
{
	ExprFunctionCall_t3990388239::get_offset_of__name_0(),
	ExprFunctionCall_t3990388239::get_offset_of_resolvedName_1(),
	ExprFunctionCall_t3990388239::get_offset_of__args_2(),
	ExprFunctionCall_t3990388239_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (BaseIterator_t1327316739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[2] = 
{
	BaseIterator_t1327316739::get_offset_of__nsm_1(),
	BaseIterator_t1327316739::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (WrapperIterator_t2849115671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1572[1] = 
{
	WrapperIterator_t2849115671::get_offset_of_iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (SimpleIterator_t544647972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[3] = 
{
	SimpleIterator_t544647972::get_offset_of__nav_3(),
	SimpleIterator_t544647972::get_offset_of__current_4(),
	SimpleIterator_t544647972::get_offset_of_skipfirst_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (SelfIterator_t197024894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (NullIterator_t2905335737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (ParensIterator_t3418004251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[1] = 
{
	ParensIterator_t3418004251::get_offset_of__iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (ParentIterator_t1610549788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[1] = 
{
	ParentIterator_t1610549788::get_offset_of_canMove_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (ChildIterator_t3087618016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[1] = 
{
	ChildIterator_t3087618016::get_offset_of__nav_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (FollowingSiblingIterator_t116837539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (PrecedingSiblingIterator_t1318768689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[3] = 
{
	PrecedingSiblingIterator_t1318768689::get_offset_of_finished_6(),
	PrecedingSiblingIterator_t1318768689::get_offset_of_started_7(),
	PrecedingSiblingIterator_t1318768689::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (AncestorIterator_t2080208005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[3] = 
{
	AncestorIterator_t2080208005::get_offset_of_currentPosition_6(),
	AncestorIterator_t2080208005::get_offset_of_navigators_7(),
	AncestorIterator_t2080208005::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (AncestorOrSelfIterator_t3368289780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[3] = 
{
	AncestorOrSelfIterator_t3368289780::get_offset_of_currentPosition_6(),
	AncestorOrSelfIterator_t3368289780::get_offset_of_navigators_7(),
	AncestorOrSelfIterator_t3368289780::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (DescendantIterator_t928464879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[2] = 
{
	DescendantIterator_t928464879::get_offset_of__depth_6(),
	DescendantIterator_t928464879::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (DescendantOrSelfIterator_t1437473246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[2] = 
{
	DescendantOrSelfIterator_t1437473246::get_offset_of__depth_6(),
	DescendantOrSelfIterator_t1437473246::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (FollowingIterator_t3149710293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[1] = 
{
	FollowingIterator_t3149710293::get_offset_of__finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (PrecedingIterator_t1039893511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[3] = 
{
	PrecedingIterator_t1039893511::get_offset_of_finished_6(),
	PrecedingIterator_t1039893511::get_offset_of_started_7(),
	PrecedingIterator_t1039893511::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (NamespaceIterator_t1295432863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (AttributeIterator_t4254482656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (AxisIterator_t953634771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[2] = 
{
	AxisIterator_t953634771::get_offset_of__iter_3(),
	AxisIterator_t953634771::get_offset_of__test_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (SimpleSlashIterator_t1419496431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[4] = 
{
	SimpleSlashIterator_t1419496431::get_offset_of__expr_3(),
	SimpleSlashIterator_t1419496431::get_offset_of__left_4(),
	SimpleSlashIterator_t1419496431::get_offset_of__right_5(),
	SimpleSlashIterator_t1419496431::get_offset_of__current_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (SortedIterator_t4014821679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[1] = 
{
	SortedIterator_t4014821679::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (SlashIterator_t384731969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[6] = 
{
	SlashIterator_t384731969::get_offset_of__iterLeft_3(),
	SlashIterator_t384731969::get_offset_of__iterRight_4(),
	SlashIterator_t384731969::get_offset_of__expr_5(),
	SlashIterator_t384731969::get_offset_of__iterList_6(),
	SlashIterator_t384731969::get_offset_of__finished_7(),
	SlashIterator_t384731969::get_offset_of__nextIterRight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (PredicateIterator_t3158177019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[4] = 
{
	PredicateIterator_t3158177019::get_offset_of__iter_3(),
	PredicateIterator_t3158177019::get_offset_of__pred_4(),
	PredicateIterator_t3158177019::get_offset_of_resType_5(),
	PredicateIterator_t3158177019::get_offset_of_finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (ListIterator_t4080389072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[1] = 
{
	ListIterator_t4080389072::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (UnionIterator_t1391611539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[5] = 
{
	UnionIterator_t1391611539::get_offset_of__left_3(),
	UnionIterator_t1391611539::get_offset_of__right_4(),
	UnionIterator_t1391611539::get_offset_of_keepLeft_5(),
	UnionIterator_t1391611539::get_offset_of_keepRight_6(),
	UnionIterator_t1391611539::get_offset_of__current_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (Tokenizer_t1510902653), -1, sizeof(Tokenizer_t1510902653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1596[10] = 
{
	Tokenizer_t1510902653::get_offset_of_m_rgchInput_0(),
	Tokenizer_t1510902653::get_offset_of_m_ich_1(),
	Tokenizer_t1510902653::get_offset_of_m_cch_2(),
	Tokenizer_t1510902653::get_offset_of_m_iToken_3(),
	Tokenizer_t1510902653::get_offset_of_m_iTokenPrev_4(),
	Tokenizer_t1510902653::get_offset_of_m_objToken_5(),
	Tokenizer_t1510902653::get_offset_of_m_fPrevWasOperator_6(),
	Tokenizer_t1510902653::get_offset_of_m_fThisIsOperator_7(),
	Tokenizer_t1510902653_StaticFields::get_offset_of_s_mapTokens_8(),
	Tokenizer_t1510902653_StaticFields::get_offset_of_s_rgTokenMap_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (XPathIteratorComparer_t3727165134), -1, sizeof(XPathIteratorComparer_t3727165134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1597[1] = 
{
	XPathIteratorComparer_t3727165134_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (XPathNavigatorComparer_t2221114443), -1, sizeof(XPathNavigatorComparer_t2221114443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1598[1] = 
{
	XPathNavigatorComparer_t2221114443_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (XPathException_t1803876086), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
