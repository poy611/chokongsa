﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Trigger2dEvent
struct Trigger2dEvent_t1692280184;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"

// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::.ctor()
extern "C"  void Trigger2dEvent__ctor_m1278970350 (Trigger2dEvent_t1692280184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::Reset()
extern "C"  void Trigger2dEvent_Reset_m3220370587 (Trigger2dEvent_t1692280184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::OnPreprocess()
extern "C"  void Trigger2dEvent_OnPreprocess_m804136833 (Trigger2dEvent_t1692280184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::StoreCollisionInfo(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_StoreCollisionInfo_m2601361816 (Trigger2dEvent_t1692280184 * __this, Collider2D_t1552025098 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerEnter2D_m3277760758 (Trigger2dEvent_t1692280184 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerStay2D_m408227431 (Trigger2dEvent_t1692280184 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerExit2D_m471213292 (Trigger2dEvent_t1692280184 * __this, Collider2D_t1552025098 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.Trigger2dEvent::ErrorCheck()
extern "C"  String_t* Trigger2dEvent_ErrorCheck_m3006829081 (Trigger2dEvent_t1692280184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
