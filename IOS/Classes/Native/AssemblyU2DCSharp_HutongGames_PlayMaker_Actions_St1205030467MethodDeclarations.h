﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringJoin
struct StringJoin_t1205030467;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringJoin::.ctor()
extern "C"  void StringJoin__ctor_m2807619971 (StringJoin_t1205030467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringJoin::OnEnter()
extern "C"  void StringJoin_OnEnter_m275058842 (StringJoin_t1205030467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
