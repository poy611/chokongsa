﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultAllData
struct ResultAllData_t3272948206;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultAllData::.ctor()
extern "C"  void ResultAllData__ctor_m1102869309 (ResultAllData_t3272948206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
