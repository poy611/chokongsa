﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse
struct FetchSelfResponse_t1087840609;
// GooglePlayGames.Native.PInvoke.NativePlayer
struct NativePlayer_t2636885988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse::.ctor(System.IntPtr)
extern "C"  void FetchSelfResponse__ctor_m3682659898 (FetchSelfResponse_t1087840609 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse::Status()
extern "C"  int32_t FetchSelfResponse_Status_m1964167685 (FetchSelfResponse_t1087840609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativePlayer GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse::Self()
extern "C"  NativePlayer_t2636885988 * FetchSelfResponse_Self_m3598534062 (FetchSelfResponse_t1087840609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchSelfResponse_CallDispose_m988090038 (FetchSelfResponse_t1087840609 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse::FromPointer(System.IntPtr)
extern "C"  FetchSelfResponse_t1087840609 * FetchSelfResponse_FromPointer_m4096413253 (Il2CppObject * __this /* static, unused */, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
