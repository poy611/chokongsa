﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey74
struct U3CDeclineInvitationU3Ec__AnonStorey74_t3350151842;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t815788017;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Re815788017.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey74::.ctor()
extern "C"  void U3CDeclineInvitationU3Ec__AnonStorey74__ctor_m1491966217 (U3CDeclineInvitationU3Ec__AnonStorey74_t3350151842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey74::<>m__4A(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern "C"  void U3CDeclineInvitationU3Ec__AnonStorey74_U3CU3Em__4A_m390605026 (U3CDeclineInvitationU3Ec__AnonStorey74_t3350151842 * __this, FetchInvitationsResponse_t815788017 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
