﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.LeaderboardManager/<HandleFetch>c__AnonStoreyA2
struct U3CHandleFetchU3Ec__AnonStoreyA2_t932101484;
// GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse
struct FetchScoreSummaryResponse_t877231893;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Fe877231893.h"

// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<HandleFetch>c__AnonStoreyA2::.ctor()
extern "C"  void U3CHandleFetchU3Ec__AnonStoreyA2__ctor_m1671080111 (U3CHandleFetchU3Ec__AnonStoreyA2_t932101484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<HandleFetch>c__AnonStoreyA2::<>m__A5(GooglePlayGames.Native.PInvoke.FetchScoreSummaryResponse)
extern "C"  void U3CHandleFetchU3Ec__AnonStoreyA2_U3CU3Em__A5_m2465618656 (U3CHandleFetchU3Ec__AnonStoreyA2_t932101484 * __this, FetchScoreSummaryResponse_t877231893 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
