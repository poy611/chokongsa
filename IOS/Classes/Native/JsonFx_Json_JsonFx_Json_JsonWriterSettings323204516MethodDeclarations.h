﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonWriterSettings
struct JsonWriterSettings_t323204516;
// System.String
struct String_t;
// JsonFx.Json.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t3884844371;

#include "codegen/il2cpp-codegen.h"

// System.String JsonFx.Json.JsonWriterSettings::get_TypeHintName()
extern "C"  String_t* JsonWriterSettings_get_TypeHintName_m1522970531 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonWriterSettings::get_PrettyPrint()
extern "C"  bool JsonWriterSettings_get_PrettyPrint_m2574152245 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonFx.Json.JsonWriterSettings::get_Tab()
extern "C"  String_t* JsonWriterSettings_get_Tab_m1988222976 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JsonFx.Json.JsonWriterSettings::get_NewLine()
extern "C"  String_t* JsonWriterSettings_get_NewLine_m1619438943 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JsonFx.Json.JsonWriterSettings::get_MaxDepth()
extern "C"  int32_t JsonWriterSettings_get_MaxDepth_m4039989341 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JsonFx.Json.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern "C"  bool JsonWriterSettings_get_UseXmlSerializationAttributes_m2086015829 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::get_DateTimeSerializer()
extern "C"  WriteDelegate_1_t3884844371 * JsonWriterSettings_get_DateTimeSerializer_m909902429 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonFx.Json.JsonWriterSettings::.ctor()
extern "C"  void JsonWriterSettings__ctor_m950536825 (JsonWriterSettings_t323204516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
