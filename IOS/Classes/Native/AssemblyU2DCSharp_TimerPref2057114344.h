﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.TextMesh
struct TextMesh_t2567681854;
// System.String
struct String_t;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// UnityEngine.UI.Image
struct Image_t538875265;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimerPref
struct  TimerPref_t2057114344  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text TimerPref::timerText
	Text_t9039225 * ___timerText_2;
	// UnityEngine.TextMesh TimerPref::timerTextMesh
	TextMesh_t2567681854 * ___timerTextMesh_3;
	// System.Int32 TimerPref::nowNum
	int32_t ___nowNum_4;
	// System.String TimerPref::nowScene
	String_t* ___nowScene_5;
	// StructforMinigame TimerPref::stuff
	StructforMinigame_t1286533533 * ___stuff_6;
	// ConstforMinigame TimerPref::cuff
	ConstforMinigame_t2789090703 * ___cuff_7;
	// UnityEngine.UI.Image TimerPref::img
	Image_t538875265 * ___img_8;

public:
	inline static int32_t get_offset_of_timerText_2() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___timerText_2)); }
	inline Text_t9039225 * get_timerText_2() const { return ___timerText_2; }
	inline Text_t9039225 ** get_address_of_timerText_2() { return &___timerText_2; }
	inline void set_timerText_2(Text_t9039225 * value)
	{
		___timerText_2 = value;
		Il2CppCodeGenWriteBarrier(&___timerText_2, value);
	}

	inline static int32_t get_offset_of_timerTextMesh_3() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___timerTextMesh_3)); }
	inline TextMesh_t2567681854 * get_timerTextMesh_3() const { return ___timerTextMesh_3; }
	inline TextMesh_t2567681854 ** get_address_of_timerTextMesh_3() { return &___timerTextMesh_3; }
	inline void set_timerTextMesh_3(TextMesh_t2567681854 * value)
	{
		___timerTextMesh_3 = value;
		Il2CppCodeGenWriteBarrier(&___timerTextMesh_3, value);
	}

	inline static int32_t get_offset_of_nowNum_4() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___nowNum_4)); }
	inline int32_t get_nowNum_4() const { return ___nowNum_4; }
	inline int32_t* get_address_of_nowNum_4() { return &___nowNum_4; }
	inline void set_nowNum_4(int32_t value)
	{
		___nowNum_4 = value;
	}

	inline static int32_t get_offset_of_nowScene_5() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___nowScene_5)); }
	inline String_t* get_nowScene_5() const { return ___nowScene_5; }
	inline String_t** get_address_of_nowScene_5() { return &___nowScene_5; }
	inline void set_nowScene_5(String_t* value)
	{
		___nowScene_5 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_5, value);
	}

	inline static int32_t get_offset_of_stuff_6() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___stuff_6)); }
	inline StructforMinigame_t1286533533 * get_stuff_6() const { return ___stuff_6; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_6() { return &___stuff_6; }
	inline void set_stuff_6(StructforMinigame_t1286533533 * value)
	{
		___stuff_6 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_6, value);
	}

	inline static int32_t get_offset_of_cuff_7() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___cuff_7)); }
	inline ConstforMinigame_t2789090703 * get_cuff_7() const { return ___cuff_7; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_7() { return &___cuff_7; }
	inline void set_cuff_7(ConstforMinigame_t2789090703 * value)
	{
		___cuff_7 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_7, value);
	}

	inline static int32_t get_offset_of_img_8() { return static_cast<int32_t>(offsetof(TimerPref_t2057114344, ___img_8)); }
	inline Image_t538875265 * get_img_8() const { return ___img_8; }
	inline Image_t538875265 ** get_address_of_img_8() { return &___img_8; }
	inline void set_img_8(Image_t538875265 * value)
	{
		___img_8 = value;
		Il2CppCodeGenWriteBarrier(&___img_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
