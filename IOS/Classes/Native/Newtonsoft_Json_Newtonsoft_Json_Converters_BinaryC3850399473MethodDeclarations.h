﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.BinaryConverter
struct BinaryConverter_t3850399473;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

// System.Void Newtonsoft.Json.Converters.BinaryConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  void BinaryConverter_WriteJson_m1097649639 (BinaryConverter_t3850399473 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonSerializer_t251850770 * ___serializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::GetByteArray(System.Object)
extern "C"  ByteU5BU5D_t4260760469* BinaryConverter_GetByteArray_m611111640 (BinaryConverter_t3850399473 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.BinaryConverter::EnsureReflectionObject(System.Type)
extern "C"  void BinaryConverter_EnsureReflectionObject_m334383514 (BinaryConverter_t3850399473 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.BinaryConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * BinaryConverter_ReadJson_m3151011276 (BinaryConverter_t3850399473 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, Il2CppObject * ___existingValue2, JsonSerializer_t251850770 * ___serializer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::ReadByteArray(Newtonsoft.Json.JsonReader)
extern "C"  ByteU5BU5D_t4260760469* BinaryConverter_ReadByteArray_m1701803036 (BinaryConverter_t3850399473 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Converters.BinaryConverter::CanConvert(System.Type)
extern "C"  bool BinaryConverter_CanConvert_m1197599413 (BinaryConverter_t3850399473 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.BinaryConverter::.ctor()
extern "C"  void BinaryConverter__ctor_m3331206989 (BinaryConverter_t3850399473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
