﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2345743608MethodDeclarations.h"

// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2656348232(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1747968918 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m1942110965_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(T1,T2)
#define Action_2_Invoke_m752718611(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1747968918 *, bool, TurnBasedMatch_t3573041681 *, const MethodInfo*))Action_2_Invoke_m2625457686_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m4076807050(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1747968918 *, bool, TurnBasedMatch_t3573041681 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m516909757_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m462913112(__this, ___result0, method) ((  void (*) (Action_2_t1747968918 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3710875525_gshared)(__this, ___result0, method)
