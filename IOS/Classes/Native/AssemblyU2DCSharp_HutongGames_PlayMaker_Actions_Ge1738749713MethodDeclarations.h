﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSine
struct GetSine_t1738749713;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSine::.ctor()
extern "C"  void GetSine__ctor_m3122449221 (GetSine_t1738749713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::Reset()
extern "C"  void GetSine_Reset_m768882162 (GetSine_t1738749713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnEnter()
extern "C"  void GetSine_OnEnter_m2178257372 (GetSine_t1738749713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnUpdate()
extern "C"  void GetSine_OnUpdate_m2235028455 (GetSine_t1738749713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::DoSine()
extern "C"  void GetSine_DoSine_m3103512983 (GetSine_t1738749713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
