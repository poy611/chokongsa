﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LogQueue
struct LogQueue_t2065454157;

#include "codegen/il2cpp-codegen.h"

// System.Void LogQueue::.ctor()
extern "C"  void LogQueue__ctor_m3759796462 (LogQueue_t2065454157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
