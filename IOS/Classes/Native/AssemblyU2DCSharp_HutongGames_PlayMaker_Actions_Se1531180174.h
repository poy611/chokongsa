﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRotation
struct  SetRotation_t1531180174  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetRotation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetRotation::quaternion
	FsmQuaternion_t3871136040 * ___quaternion_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetRotation::vector
	FsmVector3_t533912882 * ___vector_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::xAngle
	FsmFloat_t2134102846 * ___xAngle_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::yAngle
	FsmFloat_t2134102846 * ___yAngle_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::zAngle
	FsmFloat_t2134102846 * ___zAngle_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetRotation::space
	int32_t ___space_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRotation::everyFrame
	bool ___everyFrame_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRotation::lateUpdate
	bool ___lateUpdate_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_quaternion_12() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___quaternion_12)); }
	inline FsmQuaternion_t3871136040 * get_quaternion_12() const { return ___quaternion_12; }
	inline FsmQuaternion_t3871136040 ** get_address_of_quaternion_12() { return &___quaternion_12; }
	inline void set_quaternion_12(FsmQuaternion_t3871136040 * value)
	{
		___quaternion_12 = value;
		Il2CppCodeGenWriteBarrier(&___quaternion_12, value);
	}

	inline static int32_t get_offset_of_vector_13() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___vector_13)); }
	inline FsmVector3_t533912882 * get_vector_13() const { return ___vector_13; }
	inline FsmVector3_t533912882 ** get_address_of_vector_13() { return &___vector_13; }
	inline void set_vector_13(FsmVector3_t533912882 * value)
	{
		___vector_13 = value;
		Il2CppCodeGenWriteBarrier(&___vector_13, value);
	}

	inline static int32_t get_offset_of_xAngle_14() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___xAngle_14)); }
	inline FsmFloat_t2134102846 * get_xAngle_14() const { return ___xAngle_14; }
	inline FsmFloat_t2134102846 ** get_address_of_xAngle_14() { return &___xAngle_14; }
	inline void set_xAngle_14(FsmFloat_t2134102846 * value)
	{
		___xAngle_14 = value;
		Il2CppCodeGenWriteBarrier(&___xAngle_14, value);
	}

	inline static int32_t get_offset_of_yAngle_15() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___yAngle_15)); }
	inline FsmFloat_t2134102846 * get_yAngle_15() const { return ___yAngle_15; }
	inline FsmFloat_t2134102846 ** get_address_of_yAngle_15() { return &___yAngle_15; }
	inline void set_yAngle_15(FsmFloat_t2134102846 * value)
	{
		___yAngle_15 = value;
		Il2CppCodeGenWriteBarrier(&___yAngle_15, value);
	}

	inline static int32_t get_offset_of_zAngle_16() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___zAngle_16)); }
	inline FsmFloat_t2134102846 * get_zAngle_16() const { return ___zAngle_16; }
	inline FsmFloat_t2134102846 ** get_address_of_zAngle_16() { return &___zAngle_16; }
	inline void set_zAngle_16(FsmFloat_t2134102846 * value)
	{
		___zAngle_16 = value;
		Il2CppCodeGenWriteBarrier(&___zAngle_16, value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_19() { return static_cast<int32_t>(offsetof(SetRotation_t1531180174, ___lateUpdate_19)); }
	inline bool get_lateUpdate_19() const { return ___lateUpdate_19; }
	inline bool* get_address_of_lateUpdate_19() { return &___lateUpdate_19; }
	inline void set_lateUpdate_19(bool value)
	{
		___lateUpdate_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
