﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2301125574MethodDeclarations.h"

// System.Void System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3120573176(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1609885161 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m161237399_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>::Invoke(T)
#define Func_2_Invoke_m230965630(__this, ___arg10, method) ((  NativeSnapshotMetadata_t3479575958 * (*) (Func_2_t1609885161 *, UIntPtr_t , const MethodInfo*))Func_2_Invoke_m527793903_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2249986737(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1609885161 *, UIntPtr_t , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3185104162_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativeSnapshotMetadata>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1573158166(__this, ___result0, method) ((  NativeSnapshotMetadata_t3479575958 * (*) (Func_2_t1609885161 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m858964037_gshared)(__this, ___result0, method)
