﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeQuestMilestone
struct NativeQuestMilestone_t2033850801;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_1709442041.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeQuestMilestone::.ctor(System.IntPtr)
extern "C"  void NativeQuestMilestone__ctor_m2584960977 (NativeQuestMilestone_t2033850801 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_Id()
extern "C"  String_t* NativeQuestMilestone_get_Id_m244175330 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_EventId()
extern "C"  String_t* NativeQuestMilestone_get_EventId_m1006062160 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_QuestId()
extern "C"  String_t* NativeQuestMilestone_get_QuestId_m3486747800 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_CurrentCount()
extern "C"  uint64_t NativeQuestMilestone_get_CurrentCount_m4165626372 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_TargetCount()
extern "C"  uint64_t NativeQuestMilestone_get_TargetCount_m3832181362 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_CompletionRewardData()
extern "C"  ByteU5BU5D_t4260760469* NativeQuestMilestone_get_CompletionRewardData_m771944853 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.MilestoneState GooglePlayGames.Native.PInvoke.NativeQuestMilestone::get_State()
extern "C"  int32_t NativeQuestMilestone_get_State_m2009164688 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.NativeQuestMilestone::Valid()
extern "C"  bool NativeQuestMilestone_Valid_m4036070449 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeQuestMilestone::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeQuestMilestone_CallDispose_m702639103 (NativeQuestMilestone_t2033850801 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeQuestMilestone::ToString()
extern "C"  String_t* NativeQuestMilestone_ToString_m2228472938 (NativeQuestMilestone_t2033850801 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuestMilestone GooglePlayGames.Native.PInvoke.NativeQuestMilestone::FromPointer(System.IntPtr)
extern "C"  NativeQuestMilestone_t2033850801 * NativeQuestMilestone_FromPointer_m2773577205 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuestMilestone::<get_Id>m__B7(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuestMilestone_U3Cget_IdU3Em__B7_m579326164 (NativeQuestMilestone_t2033850801 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuestMilestone::<get_EventId>m__B8(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuestMilestone_U3Cget_EventIdU3Em__B8_m4195747029 (NativeQuestMilestone_t2033850801 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuestMilestone::<get_QuestId>m__B9(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuestMilestone_U3Cget_QuestIdU3Em__B9_m2590701534 (NativeQuestMilestone_t2033850801 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeQuestMilestone::<get_CompletionRewardData>m__BA(System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__BA_m2633633337 (NativeQuestMilestone_t2033850801 * __this, ByteU5BU5D_t4260760469* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
