﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Lobby
struct ScoreManager_Lobby_t3853030226;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.UI.Button
struct Button_t3896396478;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"

// System.Void ScoreManager_Lobby::.ctor()
extern "C"  void ScoreManager_Lobby__ctor_m989399433 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::Start()
extern "C"  void ScoreManager_Lobby_Start_m4231504521 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Lobby::TimerCheck()
extern "C"  Il2CppObject * ScoreManager_Lobby_TimerCheck_m3722550630 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Lobby::TimeEnd()
extern "C"  Il2CppObject * ScoreManager_Lobby_TimeEnd_m4145239853 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::makeActive()
extern "C"  void ScoreManager_Lobby_makeActive_m749326479 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::backButton()
extern "C"  void ScoreManager_Lobby_backButton_m3489665972 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Lobby::wait()
extern "C"  Il2CppObject * ScoreManager_Lobby_wait_m3214450424 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::SetTextOrder(System.Int32,System.Int32,System.Int32)
extern "C"  void ScoreManager_Lobby_SetTextOrder_m502009003 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ____randomMenu1, int32_t ____randomHI2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::SetTextWrongOrder(System.Int32,System.Int32,System.Int32)
extern "C"  void ScoreManager_Lobby_SetTextWrongOrder_m4236314504 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ____randomMenu1, int32_t ____randomHI2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::SetOnInit()
extern "C"  void ScoreManager_Lobby_SetOnInit_m1996947960 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::getOrder(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_getOrder_m3323546474 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::getCreamOrder(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_getCreamOrder_m467325460 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::buttonsWhite()
extern "C"  void ScoreManager_Lobby_buttonsWhite_m2723922851 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::buttonsWhite2()
extern "C"  void ScoreManager_Lobby_buttonsWhite2_m2837239377 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::buttonWhite(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_buttonWhite_m2268534975 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::buttonRed(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_buttonRed_m836565591 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::confirm()
extern "C"  void ScoreManager_Lobby_confirm_m2759373831 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::RightOrder()
extern "C"  void ScoreManager_Lobby_RightOrder_m2078416173 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScoreManager_Lobby::RightText(System.Single)
extern "C"  int32_t ScoreManager_Lobby_RightText_m4200486377 (ScoreManager_Lobby_t3853030226 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScoreManager_Lobby::HiIndex(System.Int32)
extern "C"  int32_t ScoreManager_Lobby_HiIndex_m47683643 (ScoreManager_Lobby_t3853030226 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::CreamText(System.Int32,System.Int32)
extern "C"  void ScoreManager_Lobby_CreamText_m1005494064 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::ReadyAndStart()
extern "C"  void ScoreManager_Lobby_ReadyAndStart_m3132264629 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Lobby::RS()
extern "C"  Il2CppObject * ScoreManager_Lobby_RS_m4060323140 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::TalkBoxShowAndDown()
extern "C"  void ScoreManager_Lobby_TalkBoxShowAndDown_m2134674488 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::prevButtonClick()
extern "C"  void ScoreManager_Lobby_prevButtonClick_m2819073994 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby::nextButtonClick()
extern "C"  void ScoreManager_Lobby_nextButtonClick_m2044262154 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
