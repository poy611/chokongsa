﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Temping/<TimerCheck>c__Iterator23
struct U3CTimerCheckU3Ec__Iterator23_t859433390;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator23__ctor_m1392936621 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<TimerCheck>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m402183311 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<TimerCheck>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m539743779 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Temping/<TimerCheck>c__Iterator23::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator23_MoveNext_m4060313871 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator23_Dispose_m2775546794 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::Reset()
extern "C"  void U3CTimerCheckU3Ec__Iterator23_Reset_m3334336858 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
