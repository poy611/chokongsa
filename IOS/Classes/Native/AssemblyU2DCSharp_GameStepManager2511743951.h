﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[][]
struct Int32U5BU5DU5BU5D_t1820556512;

#include "AssemblyU2DCSharp_Singleton_1_gen2764559344.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameStepManager
struct  GameStepManager_t2511743951  : public Singleton_1_t2764559344
{
public:
	// System.Int32[][] GameStepManager::gameStep
	Int32U5BU5DU5BU5D_t1820556512* ___gameStep_3;

public:
	inline static int32_t get_offset_of_gameStep_3() { return static_cast<int32_t>(offsetof(GameStepManager_t2511743951, ___gameStep_3)); }
	inline Int32U5BU5DU5BU5D_t1820556512* get_gameStep_3() const { return ___gameStep_3; }
	inline Int32U5BU5DU5BU5D_t1820556512** get_address_of_gameStep_3() { return &___gameStep_3; }
	inline void set_gameStep_3(Int32U5BU5DU5BU5D_t1820556512* value)
	{
		___gameStep_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameStep_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
