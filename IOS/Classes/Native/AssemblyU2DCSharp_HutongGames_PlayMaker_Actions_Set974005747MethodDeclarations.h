﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIAlpha
struct SetGUIAlpha_t974005747;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::.ctor()
extern "C"  void SetGUIAlpha__ctor_m2078852771 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::Reset()
extern "C"  void SetGUIAlpha_Reset_m4020253008 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::OnGUI()
extern "C"  void SetGUIAlpha_OnGUI_m1574251421 (SetGUIAlpha_t974005747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
