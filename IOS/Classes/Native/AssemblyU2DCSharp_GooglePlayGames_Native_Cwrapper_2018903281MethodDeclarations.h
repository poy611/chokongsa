﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2590224534.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerInvitation_MultiplayerInvitation_AutomatchingSlotsAvailable_m1782766457 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_InvitingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t MultiplayerInvitation_MultiplayerInvitation_InvitingParticipant_m1805269738 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerInvitation_MultiplayerInvitation_Variant_m98103368 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t MultiplayerInvitation_MultiplayerInvitation_CreationTime_m2074854660 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Participants_Length_m3870351221 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t MultiplayerInvitation_MultiplayerInvitation_Participants_GetElement_m153709327 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerInvitation_MultiplayerInvitation_Valid_m903083864 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Type(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerInvitation_MultiplayerInvitation_Type_m8447793 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Id_m2447988070 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MultiplayerInvitation_MultiplayerInvitation_Dispose_m1530150535 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
