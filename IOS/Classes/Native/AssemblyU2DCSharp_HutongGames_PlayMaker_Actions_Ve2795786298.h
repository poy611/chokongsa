﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Add
struct  Vector2Add_t2795786298  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Add::vector2Variable
	FsmVector2_t533912881 * ___vector2Variable_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Add::addVector
	FsmVector2_t533912881 * ___addVector_12;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Add::everyFrame
	bool ___everyFrame_13;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Add::perSecond
	bool ___perSecond_14;

public:
	inline static int32_t get_offset_of_vector2Variable_11() { return static_cast<int32_t>(offsetof(Vector2Add_t2795786298, ___vector2Variable_11)); }
	inline FsmVector2_t533912881 * get_vector2Variable_11() const { return ___vector2Variable_11; }
	inline FsmVector2_t533912881 ** get_address_of_vector2Variable_11() { return &___vector2Variable_11; }
	inline void set_vector2Variable_11(FsmVector2_t533912881 * value)
	{
		___vector2Variable_11 = value;
		Il2CppCodeGenWriteBarrier(&___vector2Variable_11, value);
	}

	inline static int32_t get_offset_of_addVector_12() { return static_cast<int32_t>(offsetof(Vector2Add_t2795786298, ___addVector_12)); }
	inline FsmVector2_t533912881 * get_addVector_12() const { return ___addVector_12; }
	inline FsmVector2_t533912881 ** get_address_of_addVector_12() { return &___addVector_12; }
	inline void set_addVector_12(FsmVector2_t533912881 * value)
	{
		___addVector_12 = value;
		Il2CppCodeGenWriteBarrier(&___addVector_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(Vector2Add_t2795786298, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_perSecond_14() { return static_cast<int32_t>(offsetof(Vector2Add_t2795786298, ___perSecond_14)); }
	inline bool get_perSecond_14() const { return ___perSecond_14; }
	inline bool* get_address_of_perSecond_14() { return &___perSecond_14; }
	inline void set_perSecond_14(bool value)
	{
		___perSecond_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
