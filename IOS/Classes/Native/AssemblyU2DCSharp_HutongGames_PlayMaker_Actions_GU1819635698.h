﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField
struct  GUILayoutConfirmPasswordField_t1819635698  : public GUILayoutAction_t2615417833
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::text
	FsmString_t952858651 * ___text_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::maxLength
	FsmInt_t1596138449 * ___maxLength_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::style
	FsmString_t952858651 * ___style_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::changedEvent
	FsmEvent_t2133468028 * ___changedEvent_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::mask
	FsmString_t952858651 * ___mask_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::confirm
	FsmBool_t1075959796 * ___confirm_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::password
	FsmString_t952858651 * ___password_19;

public:
	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___text_13)); }
	inline FsmString_t952858651 * get_text_13() const { return ___text_13; }
	inline FsmString_t952858651 ** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(FsmString_t952858651 * value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier(&___text_13, value);
	}

	inline static int32_t get_offset_of_maxLength_14() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___maxLength_14)); }
	inline FsmInt_t1596138449 * get_maxLength_14() const { return ___maxLength_14; }
	inline FsmInt_t1596138449 ** get_address_of_maxLength_14() { return &___maxLength_14; }
	inline void set_maxLength_14(FsmInt_t1596138449 * value)
	{
		___maxLength_14 = value;
		Il2CppCodeGenWriteBarrier(&___maxLength_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___style_15)); }
	inline FsmString_t952858651 * get_style_15() const { return ___style_15; }
	inline FsmString_t952858651 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t952858651 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_changedEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___changedEvent_16)); }
	inline FsmEvent_t2133468028 * get_changedEvent_16() const { return ___changedEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_changedEvent_16() { return &___changedEvent_16; }
	inline void set_changedEvent_16(FsmEvent_t2133468028 * value)
	{
		___changedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_16, value);
	}

	inline static int32_t get_offset_of_mask_17() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___mask_17)); }
	inline FsmString_t952858651 * get_mask_17() const { return ___mask_17; }
	inline FsmString_t952858651 ** get_address_of_mask_17() { return &___mask_17; }
	inline void set_mask_17(FsmString_t952858651 * value)
	{
		___mask_17 = value;
		Il2CppCodeGenWriteBarrier(&___mask_17, value);
	}

	inline static int32_t get_offset_of_confirm_18() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___confirm_18)); }
	inline FsmBool_t1075959796 * get_confirm_18() const { return ___confirm_18; }
	inline FsmBool_t1075959796 ** get_address_of_confirm_18() { return &___confirm_18; }
	inline void set_confirm_18(FsmBool_t1075959796 * value)
	{
		___confirm_18 = value;
		Il2CppCodeGenWriteBarrier(&___confirm_18, value);
	}

	inline static int32_t get_offset_of_password_19() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t1819635698, ___password_19)); }
	inline FsmString_t952858651 * get_password_19() const { return ___password_19; }
	inline FsmString_t952858651 ** get_address_of_password_19() { return &___password_19; }
	inline void set_password_19(FsmString_t952858651 * value)
	{
		___password_19 = value;
		Il2CppCodeGenWriteBarrier(&___password_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
