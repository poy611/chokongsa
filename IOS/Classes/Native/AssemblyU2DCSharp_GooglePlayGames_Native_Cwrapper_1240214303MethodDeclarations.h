﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2241332029.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3906033828.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4146228892.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_Dispose_m427433031 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_TimeSpan_m3308009877 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScorePage_ScorePage_LeaderboardId_m191657541 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Collection(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_Collection_m3994076963 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Start(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_Start_m84508075 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_Valid_m3695976728 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasPreviousScorePage(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_HasPreviousScorePage_m3906038640 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasNextScorePage(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_HasNextScorePage_m3899782644 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_PreviousScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_PreviousScorePageToken_m223637390 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_NextScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_NextScorePageToken_m1126495242 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  ScorePage_ScorePage_Entries_Length_m1701400801 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t ScorePage_ScorePage_Entries_GetElement_m658925533 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_Entry_Dispose_m536074778 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_PlayerId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScorePage_ScorePage_Entry_PlayerId_m2556016984 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModified(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScorePage_ScorePage_Entry_LastModified_m483083204 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Score(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_Entry_Score_m161561410 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_Entry_Valid_m595583147 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScorePage_ScorePage_Entry_LastModifiedTime_m1928048497 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_ScorePageToken_Valid_m3846258869 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_ScorePageToken_Dispose_m3670275724 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
