﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfo
struct  UserInfo_t4092807993  : public Il2CppObject
{
public:
	// System.String UserInfo::user_id
	String_t* ___user_id_0;
	// System.String UserInfo::user_name
	String_t* ___user_name_1;

public:
	inline static int32_t get_offset_of_user_id_0() { return static_cast<int32_t>(offsetof(UserInfo_t4092807993, ___user_id_0)); }
	inline String_t* get_user_id_0() const { return ___user_id_0; }
	inline String_t** get_address_of_user_id_0() { return &___user_id_0; }
	inline void set_user_id_0(String_t* value)
	{
		___user_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_0, value);
	}

	inline static int32_t get_offset_of_user_name_1() { return static_cast<int32_t>(offsetof(UserInfo_t4092807993, ___user_name_1)); }
	inline String_t* get_user_name_1() const { return ___user_name_1; }
	inline String_t** get_address_of_user_name_1() { return &___user_name_1; }
	inline void set_user_name_1(String_t* value)
	{
		___user_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___user_name_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
