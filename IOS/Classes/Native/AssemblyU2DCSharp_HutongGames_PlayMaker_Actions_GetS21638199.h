﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScreenHeight
struct  GetScreenHeight_t21638199  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScreenHeight::storeScreenHeight
	FsmFloat_t2134102846 * ___storeScreenHeight_11;

public:
	inline static int32_t get_offset_of_storeScreenHeight_11() { return static_cast<int32_t>(offsetof(GetScreenHeight_t21638199, ___storeScreenHeight_11)); }
	inline FsmFloat_t2134102846 * get_storeScreenHeight_11() const { return ___storeScreenHeight_11; }
	inline FsmFloat_t2134102846 ** get_address_of_storeScreenHeight_11() { return &___storeScreenHeight_11; }
	inline void set_storeScreenHeight_11(FsmFloat_t2134102846 * value)
	{
		___storeScreenHeight_11 = value;
		Il2CppCodeGenWriteBarrier(&___storeScreenHeight_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
