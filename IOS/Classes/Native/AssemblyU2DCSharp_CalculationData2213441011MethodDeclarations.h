﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CalculationData
struct CalculationData_t2213441011;

#include "codegen/il2cpp-codegen.h"

// System.Void CalculationData::.ctor()
extern "C"  void CalculationData__ctor_m14310104 (CalculationData_t2213441011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
