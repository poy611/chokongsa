﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayShuffle
struct ArrayShuffle_t4231526536;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::.ctor()
extern "C"  void ArrayShuffle__ctor_m3787575006 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::Reset()
extern "C"  void ArrayShuffle_Reset_m1434007947 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::OnEnter()
extern "C"  void ArrayShuffle_OnEnter_m1414009653 (ArrayShuffle_t4231526536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
