﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionData
struct ActionData_t3958426178;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat>
struct List_1_t3502288398;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt>
struct List_1_t2964324001;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool>
struct List_1_t2444145348;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2>
struct List_1_t1902098433;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3>
struct List_1_t1902098434;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor>
struct List_1_t3499604757;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect>
struct List_1_t2444612030;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion>
struct List_1_t944354296;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t2321044203;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t2189661721;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t3065333419;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t1620082664;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t4054181541;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t353063272;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t4154693685;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t2964336089;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray>
struct List_1_t3497852427;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>
struct List_1_t2444233947;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t1000377263;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t3192090493;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t2333180753;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t2567562023;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t2476090292;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.ActionData/Context
struct Context_t1489737484;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// System.Object
struct Il2CppObject;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Array
struct Il2CppArray;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t3279845016;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t3927159007;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t964995201;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t3757199647;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData_Context1489737484.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Int32 HutongGames.PlayMaker.ActionData::get_ActionCount()
extern "C"  int32_t ActionData_get_ActionCount_m1125497069 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.ActionData::Copy()
extern "C"  ActionData_t3958426178 * ActionData_Copy_m1426077309 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::CopyStringParams()
extern "C"  List_1_t1375417109 * ActionData_CopyStringParams_m3113021204 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat> HutongGames.PlayMaker.ActionData::CopyFsmFloatParams()
extern "C"  List_1_t3502288398 * ActionData_CopyFsmFloatParams_m4226437349 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt> HutongGames.PlayMaker.ActionData::CopyFsmIntParams()
extern "C"  List_1_t2964324001 * ActionData_CopyFsmIntParams_m408019467 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool> HutongGames.PlayMaker.ActionData::CopyFsmBoolParams()
extern "C"  List_1_t2444145348 * ActionData_CopyFsmBoolParams_m2622988487 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2> HutongGames.PlayMaker.ActionData::CopyFsmVector2Params()
extern "C"  List_1_t1902098433 * ActionData_CopyFsmVector2Params_m265440459 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3> HutongGames.PlayMaker.ActionData::CopyFsmVector3Params()
extern "C"  List_1_t1902098434 * ActionData_CopyFsmVector3Params_m3033303181 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor> HutongGames.PlayMaker.ActionData::CopyFsmColorParams()
extern "C"  List_1_t3499604757 * ActionData_CopyFsmColorParams_m3600528115 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect> HutongGames.PlayMaker.ActionData::CopyFsmRectParams()
extern "C"  List_1_t2444612030 * ActionData_CopyFsmRectParams_m1175532935 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion> HutongGames.PlayMaker.ActionData::CopyFsmQuaternionParams()
extern "C"  List_1_t944354296 * ActionData_CopyFsmQuaternionParams_m975698567 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::CopyFsmStringParams()
extern "C"  List_1_t2321044203 * ActionData_CopyFsmStringParams_m560035175 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::CopyFsmObjectParams()
extern "C"  List_1_t2189661721 * ActionData_CopyFsmObjectParams_m2512514855 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::CopyFsmGameObjectParams()
extern "C"  List_1_t3065333419 * ActionData_CopyFsmGameObjectParams_m4253082727 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::CopyFsmOwnerDefaultParams()
extern "C"  List_1_t1620082664 * ActionData_CopyFsmOwnerDefaultParams_m3703922503 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::CopyAnimationCurveParams()
extern "C"  List_1_t4054181541 * ActionData_CopyAnimationCurveParams_m2581662909 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::CopyFunctionCallParams()
extern "C"  List_1_t353063272 * ActionData_CopyFunctionCallParams_m3770948633 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::CopyFsmTemplateControlParams()
extern "C"  List_1_t4154693685 * ActionData_CopyFsmTemplateControlParams_m2258795571 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::CopyFsmVarParams()
extern "C"  List_1_t2964336089 * ActionData_CopyFsmVarParams_m2646929019 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray> HutongGames.PlayMaker.ActionData::CopyFsmArrayParams()
extern "C"  List_1_t3497852427 * ActionData_CopyFsmArrayParams_m2472118495 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum> HutongGames.PlayMaker.ActionData::CopyFsmEnumParams()
extern "C"  List_1_t2444233947 * ActionData_CopyFsmEnumParams_m4100443111 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::CopyFsmPropertyParams()
extern "C"  List_1_t1000377263 * ActionData_CopyFsmPropertyParams_m2190701159 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::CopyFsmEventTargetParams()
extern "C"  List_1_t3192090493 * ActionData_CopyFsmEventTargetParams_m2654400195 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::CopyLayoutOptionParams()
extern "C"  List_1_t2333180753 * ActionData_CopyLayoutOptionParams_m1847597931 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::ClearActionData()
extern "C"  void ActionData_ClearActionData_m873293020 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ActionData::GetActionType(System.String)
extern "C"  Type_t * ActionData_GetActionType_m1601647431 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] HutongGames.PlayMaker.ActionData::GetFields(System.Type)
extern "C"  FieldInfoU5BU5D_t2567562023* ActionData_GetFields_m3305443080 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionData::GetActionTypeHashCode(System.Type)
extern "C"  int32_t ActionData_GetActionTypeHashCode_m1842636123 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionData::GetStableHash(System.String)
extern "C"  int32_t ActionData_GetStableHash_m473935494 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.ActionData::LoadActions(HutongGames.PlayMaker.FsmState)
extern "C"  FsmStateActionU5BU5D_t2476090292* ActionData_LoadActions_m1529704733 (ActionData_t3958426178 * __this, FsmState_t2146334067 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::CreateAction(HutongGames.PlayMaker.FsmState,System.Int32)
extern "C"  FsmStateAction_t2366529033 * ActionData_CreateAction_m1781451649 (ActionData_t3958426178 * __this, FsmState_t2146334067 * ___state0, int32_t ___actionIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::CreateAction(HutongGames.PlayMaker.ActionData/Context,System.Int32)
extern "C"  FsmStateAction_t2366529033 * ActionData_CreateAction_m1393843472 (ActionData_t3958426178 * __this, Context_t1489737484 * ___context0, int32_t ___actionIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadActionField(HutongGames.PlayMaker.Fsm,System.Object,System.Reflection.FieldInfo,System.Int32)
extern "C"  void ActionData_LoadActionField_m3581337456 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, Il2CppObject * ___obj1, FieldInfo_t * ___field2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadArrayElement(HutongGames.PlayMaker.Fsm,System.Array,System.Type,System.Int32,System.Int32)
extern "C"  void ActionData_LoadArrayElement_m78797053 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, Il2CppArray * ___field1, Type_t * ___fieldType2, int32_t ___elementIndex3, int32_t ___paramIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogError(HutongGames.PlayMaker.ActionData/Context,System.String)
extern "C"  void ActionData_LogError_m3778351207 (Il2CppObject * __this /* static, unused */, Context_t1489737484 * ___context0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogInfo(HutongGames.PlayMaker.ActionData/Context,System.String)
extern "C"  void ActionData_LogInfo_m1203504241 (Il2CppObject * __this /* static, unused */, Context_t1489737484 * ___context0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.ActionData::GetFsmFloat(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmFloat_t2134102846 * ActionData_GetFsmFloat_m2691183393 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.ActionData::GetFsmInt(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmInt_t1596138449 * ActionData_GetFsmInt_m1373387719 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.ActionData::GetFsmBool(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmBool_t1075959796 * ActionData_GetFsmBool_m1303389333 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.ActionData::GetFsmVector2(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVector2_t533912881 * ActionData_GetFsmVector2_m1723195655 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.ActionData::GetFsmVector3(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVector3_t533912882 * ActionData_GetFsmVector3_m3697135881 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.ActionData::GetFsmColor(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmColor_t2131419205 * ActionData_GetFsmColor_m3589812975 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.ActionData::GetFsmRect(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmRect_t1076426478 * ActionData_GetFsmRect_m1271788245 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.ActionData::GetFsmQuaternion(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmQuaternion_t3871136040 * ActionData_GetFsmQuaternion_m4033111957 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.ActionData::GetFsmGameObject(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmGameObject_t1697147867 * ActionData_GetFsmGameObject_m683899061 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.ActionData::GetFsmTemplateControl(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmTemplateControl_t2786508133 * ActionData_GetFsmTemplateControl_m622572143 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.ActionData::GetFsmVar(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVar_t1596150537 * ActionData_GetFsmVar_m547190327 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.ActionData::GetFsmArray(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmArray_t2129666875 * ActionData_GetFsmArray_m4030668379 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.ActionData::GetFsmEnum(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmEnum_t1076048395 * ActionData_GetFsmEnum_m2679848437 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FunctionCall HutongGames.PlayMaker.ActionData::GetFunctionCall(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FunctionCall_t3279845016 * ActionData_GetFunctionCall_m4216463189 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmProperty HutongGames.PlayMaker.ActionData::GetFsmProperty(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmProperty_t3927159007 * ActionData_GetFsmProperty_m1261914357 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.ActionData::GetFsmEventTarget(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmEventTarget_t1823904941 * ActionData_GetFsmEventTarget_m2211656831 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.LayoutOption HutongGames.PlayMaker.ActionData::GetLayoutOption(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  LayoutOption_t964995201 * ActionData_GetLayoutOption_m3301170407 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.ActionData::GetFsmOwnerDefault(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmOwnerDefault_t251897112 * ActionData_GetFsmOwnerDefault_m3553402133 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.ActionData::GetFsmString(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmString_t952858651 * ActionData_GetFsmString_m3971657781 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.ActionData::GetFsmObject(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmObject_t821476169 * ActionData_GetFsmObject_m2965251701 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.ActionData::GetFsmMaterial(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmMaterial_t924399665 * ActionData_GetFsmMaterial_m2055610165 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.ActionData::GetFsmTexture(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmTexture_t3073272573 * ActionData_GetFsmTexture_m644889759 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::UsesDataVersion2()
extern "C"  bool ActionData_UsesDataVersion2_m36501723 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionData::TryFixActionName(System.String)
extern "C"  String_t* ActionData_TryFixActionName_m752710545 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::TryRecoverAction(HutongGames.PlayMaker.ActionData/Context,System.Type,HutongGames.PlayMaker.FsmStateAction,System.Int32)
extern "C"  FsmStateAction_t2366529033 * ActionData_TryRecoverAction_m3167388779 (ActionData_t3958426178 * __this, Context_t1489737484 * ___context0, Type_t * ___actionType1, FsmStateAction_t2366529033 * ___action2, int32_t ___actionIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.Int32)
extern "C"  FieldInfo_t * ActionData_FindField_m3677596223 (ActionData_t3958426178 * __this, Type_t * ___actionType0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.String)
extern "C"  FieldInfo_t * ActionData_FindField_m2793031636 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertParameter(HutongGames.PlayMaker.ActionData/Context,HutongGames.PlayMaker.FsmStateAction,System.Reflection.FieldInfo,System.Int32)
extern "C"  bool ActionData_TryConvertParameter_m3940902456 (ActionData_t3958426178 * __this, Context_t1489737484 * ___context0, FsmStateAction_t2366529033 * ___action1, FieldInfo_t * ___field2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertArrayElement(HutongGames.PlayMaker.Fsm,System.Array,HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32,System.Int32)
extern "C"  bool ActionData_TryConvertArrayElement_m3507514466 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, Il2CppArray * ___field1, int32_t ___originalParamType2, int32_t ___currentParamType3, int32_t ___elementIndex4, int32_t ___paramIndex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ActionData::ConvertType(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32)
extern "C"  Il2CppObject * ActionData_ConvertType_m2137578290 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, int32_t ___originalParamType1, int32_t ___currentParamType2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActions(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction[])
extern "C"  void ActionData_SaveActions_m2561605151 (ActionData_t3958426178 * __this, FsmState_t2146334067 * ___state0, FsmStateActionU5BU5D_t2476090292* ___actions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveAction(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmStateAction)
extern "C"  void ActionData_SaveAction_m1579918971 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, FsmStateAction_t2366529033 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActionField(HutongGames.PlayMaker.Fsm,System.Type,System.Object)
extern "C"  void ActionData_SaveActionField_m611707177 (ActionData_t3958426178 * __this, Fsm_t1527112426 * ___fsm0, Type_t * ___fieldType1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::AddByteData(System.Collections.Generic.ICollection`1<System.Byte>)
extern "C"  void ActionData_AddByteData_m4153890598 (ActionData_t3958426178 * __this, Il2CppObject* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveString(System.String)
extern "C"  void ActionData_SaveString_m1954720225 (ActionData_t3958426178 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ParamDataType HutongGames.PlayMaker.ActionData::GetParamDataType(System.Type)
extern "C"  int32_t ActionData_GetParamDataType_m11844701 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.ctor()
extern "C"  void ActionData__ctor_m256498513 (ActionData_t3958426178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.cctor()
extern "C"  void ActionData__cctor_m3174390396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
