﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen4005432850.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Required3921306327.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.Required>::.ctor(T)
extern "C"  void Nullable_1__ctor_m973246322_gshared (Nullable_1_t4005432850 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m973246322(__this, ___value0, method) ((  void (*) (Nullable_1_t4005432850 *, int32_t, const MethodInfo*))Nullable_1__ctor_m973246322_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Required>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4011153785_gshared (Nullable_1_t4005432850 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m4011153785(__this, method) ((  bool (*) (Nullable_1_t4005432850 *, const MethodInfo*))Nullable_1_get_HasValue_m4011153785_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Required>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m258593754_gshared (Nullable_1_t4005432850 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m258593754(__this, method) ((  int32_t (*) (Nullable_1_t4005432850 *, const MethodInfo*))Nullable_1_get_Value_m258593754_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Required>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2394465384_gshared (Nullable_1_t4005432850 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2394465384(__this, ___other0, method) ((  bool (*) (Nullable_1_t4005432850 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2394465384_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.Required>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3120736727_gshared (Nullable_1_t4005432850 * __this, Nullable_1_t4005432850  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3120736727(__this, ___other0, method) ((  bool (*) (Nullable_1_t4005432850 *, Nullable_1_t4005432850 , const MethodInfo*))Nullable_1_Equals_m3120736727_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.Required>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3968171712_gshared (Nullable_1_t4005432850 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3968171712(__this, method) ((  int32_t (*) (Nullable_1_t4005432850 *, const MethodInfo*))Nullable_1_GetHashCode_m3968171712_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Required>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m51289574_gshared (Nullable_1_t4005432850 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m51289574(__this, method) ((  int32_t (*) (Nullable_1_t4005432850 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m51289574_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.Required>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2044634199_gshared (Nullable_1_t4005432850 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2044634199(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t4005432850 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m2044634199_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.Required>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2530897688_gshared (Nullable_1_t4005432850 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2530897688(__this, method) ((  String_t* (*) (Nullable_1_t4005432850 *, const MethodInfo*))Nullable_1_ToString_m2530897688_gshared)(__this, method)
