﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntAdd
struct  IntAdd_t3052069402  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAdd::intVariable
	FsmInt_t1596138449 * ___intVariable_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAdd::add
	FsmInt_t1596138449 * ___add_12;
	// System.Boolean HutongGames.PlayMaker.Actions.IntAdd::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_intVariable_11() { return static_cast<int32_t>(offsetof(IntAdd_t3052069402, ___intVariable_11)); }
	inline FsmInt_t1596138449 * get_intVariable_11() const { return ___intVariable_11; }
	inline FsmInt_t1596138449 ** get_address_of_intVariable_11() { return &___intVariable_11; }
	inline void set_intVariable_11(FsmInt_t1596138449 * value)
	{
		___intVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_11, value);
	}

	inline static int32_t get_offset_of_add_12() { return static_cast<int32_t>(offsetof(IntAdd_t3052069402, ___add_12)); }
	inline FsmInt_t1596138449 * get_add_12() const { return ___add_12; }
	inline FsmInt_t1596138449 ** get_address_of_add_12() { return &___add_12; }
	inline void set_add_12(FsmInt_t1596138449 * value)
	{
		___add_12 = value;
		Il2CppCodeGenWriteBarrier(&___add_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(IntAdd_t3052069402, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
