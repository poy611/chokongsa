﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetScale
struct SetScale_t1151369904;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetScale::.ctor()
extern "C"  void SetScale__ctor_m1058667958 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::Reset()
extern "C"  void SetScale_Reset_m3000068195 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnEnter()
extern "C"  void SetScale_OnEnter_m3159354381 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnUpdate()
extern "C"  void SetScale_OnUpdate_m2584264662 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnLateUpdate()
extern "C"  void SetScale_OnLateUpdate_m2544544412 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::DoSetScale()
extern "C"  void SetScale_DoSetScale_m2952245761 (SetScale_t1151369904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
