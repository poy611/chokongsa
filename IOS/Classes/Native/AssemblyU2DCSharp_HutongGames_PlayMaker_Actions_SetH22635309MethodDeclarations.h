﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetHaloStrength
struct SetHaloStrength_t22635309;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::.ctor()
extern "C"  void SetHaloStrength__ctor_m3406108329 (SetHaloStrength_t22635309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::Reset()
extern "C"  void SetHaloStrength_Reset_m1052541270 (SetHaloStrength_t22635309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnEnter()
extern "C"  void SetHaloStrength_OnEnter_m4191720512 (SetHaloStrength_t22635309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnUpdate()
extern "C"  void SetHaloStrength_OnUpdate_m227876355 (SetHaloStrength_t22635309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::DoSetHaloStrength()
extern "C"  void SetHaloStrength_DoSetHaloStrength_m3078107 (SetHaloStrength_t22635309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
