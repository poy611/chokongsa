﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPGSBtn/<LoginChecker>c__IteratorD
struct U3CLoginCheckerU3Ec__IteratorD_t4246608817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPGSBtn/<LoginChecker>c__IteratorD::.ctor()
extern "C"  void U3CLoginCheckerU3Ec__IteratorD__ctor_m3162131466 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPGSBtn/<LoginChecker>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1830786770 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPGSBtn/<LoginChecker>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoginCheckerU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m19758694 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPGSBtn/<LoginChecker>c__IteratorD::MoveNext()
extern "C"  bool U3CLoginCheckerU3Ec__IteratorD_MoveNext_m1345046674 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn/<LoginChecker>c__IteratorD::Dispose()
extern "C"  void U3CLoginCheckerU3Ec__IteratorD_Dispose_m2164743623 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPGSBtn/<LoginChecker>c__IteratorD::Reset()
extern "C"  void U3CLoginCheckerU3Ec__IteratorD_Reset_m808564407 (U3CLoginCheckerU3Ec__IteratorD_t4246608817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
