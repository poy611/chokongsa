﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JRaw
struct JRaw_t225059758;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JRaw225059758.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"

// System.Void Newtonsoft.Json.Linq.JRaw::.ctor(Newtonsoft.Json.Linq.JRaw)
extern "C"  void JRaw__ctor_m2088175762 (JRaw_t225059758 * __this, JRaw_t225059758 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JRaw::.ctor(System.Object)
extern "C"  void JRaw__ctor_m2176512075 (JRaw_t225059758 * __this, Il2CppObject * ___rawJson0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JRaw Newtonsoft.Json.Linq.JRaw::Create(Newtonsoft.Json.JsonReader)
extern "C"  JRaw_t225059758 * JRaw_Create_m2655760603 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JRaw::CloneToken()
extern "C"  JToken_t3412245951 * JRaw_CloneToken_m920086060 (JRaw_t225059758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
