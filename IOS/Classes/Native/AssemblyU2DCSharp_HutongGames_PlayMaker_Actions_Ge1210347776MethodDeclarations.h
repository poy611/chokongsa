﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmFloat
struct GetFsmFloat_t1210347776;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::.ctor()
extern "C"  void GetFsmFloat__ctor_m3530660406 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::Reset()
extern "C"  void GetFsmFloat_Reset_m1177093347 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnEnter()
extern "C"  void GetFsmFloat_OnEnter_m3627182221 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnUpdate()
extern "C"  void GetFsmFloat_OnUpdate_m4202025814 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::DoGetFsmFloat()
extern "C"  void GetFsmFloat_DoGetFsmFloat_m1854238587 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
