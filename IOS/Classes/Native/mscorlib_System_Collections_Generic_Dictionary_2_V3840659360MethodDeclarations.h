﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va895312890MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2140050566(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3840659360 *, Dictionary_2_t845086351 *, const MethodInfo*))ValueCollection__ctor_m3882881716_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1185414892(__this, ___item0, method) ((  void (*) (ValueCollection_t3840659360 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3735487230_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1349140917(__this, method) ((  void (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2963414727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3756916922(__this, ___item0, method) ((  bool (*) (ValueCollection_t3840659360 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1634252776_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m31719391(__this, ___item0, method) ((  bool (*) (ValueCollection_t3840659360 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4150176397_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1465375171(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m288523477_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4147512441(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3840659360 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2769433355_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m967287476(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26128838_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2032092781(__this, method) ((  bool (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4204395931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3002206157(__this, method) ((  bool (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060686331_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m982305017(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3168702375_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m993519437(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3840659360 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3021523451_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3030195952(__this, method) ((  Enumerator_t3071887055  (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_GetEnumerator_m1405102622_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>::get_Count()
#define ValueCollection_get_Count_m2703215891(__this, method) ((  int32_t (*) (ValueCollection_t3840659360 *, const MethodInfo*))ValueCollection_get_Count_m1341366081_gshared)(__this, method)
