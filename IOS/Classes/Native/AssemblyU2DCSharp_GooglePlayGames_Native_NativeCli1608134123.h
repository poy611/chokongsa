﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeClient
struct NativeClient_t3798002602;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E
struct  U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123  : public Il2CppObject
{
public:
	// System.UInt32 GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E::currentAuthGeneration
	uint32_t ___currentAuthGeneration_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E::<>f__this
	NativeClient_t3798002602 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_currentAuthGeneration_0() { return static_cast<int32_t>(offsetof(U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123, ___currentAuthGeneration_0)); }
	inline uint32_t get_currentAuthGeneration_0() const { return ___currentAuthGeneration_0; }
	inline uint32_t* get_address_of_currentAuthGeneration_0() { return &___currentAuthGeneration_0; }
	inline void set_currentAuthGeneration_0(uint32_t value)
	{
		___currentAuthGeneration_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123, ___U3CU3Ef__this_1)); }
	inline NativeClient_t3798002602 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline NativeClient_t3798002602 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(NativeClient_t3798002602 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
