﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t937589677;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t1492900827;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t926437290;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t530165700;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3659144454;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3814549686;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandl2077487315.h"
#include "Newtonsoft_Json_Newtonsoft_Json_NullValueHandling2754652381.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHandli56081595.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl2761661122.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHa2626038881.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin2475221485.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting732683613.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3659144454.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3814549686.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_add_Error_m2493553643 (JsonSerializerProxy_t3893567258 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern "C"  void JsonSerializerProxy_remove_Error_m140762912 (JsonSerializerProxy_t3893567258 * __this, EventHandler_1_t937589677 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern "C"  void JsonSerializerProxy_set_ReferenceResolver_m2947603670 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TraceWriter()
extern "C"  Il2CppObject * JsonSerializerProxy_get_TraceWriter_m3860231901 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern "C"  void JsonSerializerProxy_set_TraceWriter_m458683638 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_EqualityComparer(System.Collections.IEqualityComparer)
extern "C"  void JsonSerializerProxy_set_EqualityComparer_m2837473233 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Converters()
extern "C"  JsonConverterCollection_t530165700 * JsonSerializerProxy_get_Converters_m3148881391 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern "C"  void JsonSerializerProxy_set_DefaultValueHandling_m612039885 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ContractResolver()
extern "C"  Il2CppObject * JsonSerializerProxy_get_ContractResolver_m234414689 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSerializerProxy_set_ContractResolver_m1329977450 (JsonSerializerProxy_t3893567258 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern "C"  void JsonSerializerProxy_set_MissingMemberHandling_m294659325 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern "C"  void JsonSerializerProxy_set_NullValueHandling_m1342337513 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern "C"  void JsonSerializerProxy_set_ObjectCreationHandling_m2638903821 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern "C"  void JsonSerializerProxy_set_ReferenceLoopHandling_m806211615 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  void JsonSerializerProxy_set_PreserveReferencesHandling_m1986692045 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern "C"  void JsonSerializerProxy_set_TypeNameHandling_m2813727533 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_MetadataPropertyHandling()
extern "C"  int32_t JsonSerializerProxy_get_MetadataPropertyHandling_m684862658 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern "C"  void JsonSerializerProxy_set_MetadataPropertyHandling_m1995119949 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  void JsonSerializerProxy_set_TypeNameAssemblyFormat_m2829277457 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern "C"  void JsonSerializerProxy_set_ConstructorHandling_m1962766921 (JsonSerializerProxy_t3893567258 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C"  void JsonSerializerProxy_set_Binder_m3579712453 (JsonSerializerProxy_t3893567258 * __this, SerializationBinder_t2137423328 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Context()
extern "C"  StreamingContext_t2761351129  JsonSerializerProxy_get_Context_m2385129494 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerProxy_set_Context_m3821577667 (JsonSerializerProxy_t3893567258 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Formatting Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Formatting()
extern "C"  int32_t JsonSerializerProxy_get_Formatting_m1393424122 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerProxy::get_CheckAdditionalContent()
extern "C"  bool JsonSerializerProxy_get_CheckAdditionalContent_m773852462 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_CheckAdditionalContent(System.Boolean)
extern "C"  void JsonSerializerProxy_set_CheckAdditionalContent_m3460968643 (JsonSerializerProxy_t3893567258 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase Newtonsoft.Json.Serialization.JsonSerializerProxy::GetInternalSerializer()
extern "C"  JsonSerializerInternalBase_t2068678036 * JsonSerializerProxy_GetInternalSerializer_m150690746 (JsonSerializerProxy_t3893567258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern "C"  void JsonSerializerProxy__ctor_m2356865874 (JsonSerializerProxy_t3893567258 * __this, JsonSerializerInternalReader_t3659144454 * ___serializerReader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern "C"  void JsonSerializerProxy__ctor_m2879460770 (JsonSerializerProxy_t3893567258 * __this, JsonSerializerInternalWriter_t3814549686 * ___serializerWriter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializerProxy_DeserializeInternal_m3159658689 (JsonSerializerProxy_t3893567258 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern "C"  void JsonSerializerProxy_SerializeInternal_m4011535627 (JsonSerializerProxy_t3893567258 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, Type_t * ___rootType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
