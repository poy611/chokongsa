﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LineCast2d
struct LineCast2d_t2116063341;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LineCast2d::.ctor()
extern "C"  void LineCast2d__ctor_m987917977 (LineCast2d_t2116063341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::Reset()
extern "C"  void LineCast2d_Reset_m2929318214 (LineCast2d_t2116063341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::OnEnter()
extern "C"  void LineCast2d_OnEnter_m3888099376 (LineCast2d_t2116063341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::OnUpdate()
extern "C"  void LineCast2d_OnUpdate_m3700523027 (LineCast2d_t2116063341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::DoRaycast()
extern "C"  void LineCast2d_DoRaycast_m3395133589 (LineCast2d_t2116063341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
