﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties
struct  NetworkGetMessagePlayerProperties_t906059777  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::IpAddress
	FsmString_t952858651 * ___IpAddress_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::port
	FsmInt_t1596138449 * ___port_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::guid
	FsmString_t952858651 * ___guid_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::externalIPAddress
	FsmString_t952858651 * ___externalIPAddress_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetMessagePlayerProperties::externalPort
	FsmInt_t1596138449 * ___externalPort_15;

public:
	inline static int32_t get_offset_of_IpAddress_11() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___IpAddress_11)); }
	inline FsmString_t952858651 * get_IpAddress_11() const { return ___IpAddress_11; }
	inline FsmString_t952858651 ** get_address_of_IpAddress_11() { return &___IpAddress_11; }
	inline void set_IpAddress_11(FsmString_t952858651 * value)
	{
		___IpAddress_11 = value;
		Il2CppCodeGenWriteBarrier(&___IpAddress_11, value);
	}

	inline static int32_t get_offset_of_port_12() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___port_12)); }
	inline FsmInt_t1596138449 * get_port_12() const { return ___port_12; }
	inline FsmInt_t1596138449 ** get_address_of_port_12() { return &___port_12; }
	inline void set_port_12(FsmInt_t1596138449 * value)
	{
		___port_12 = value;
		Il2CppCodeGenWriteBarrier(&___port_12, value);
	}

	inline static int32_t get_offset_of_guid_13() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___guid_13)); }
	inline FsmString_t952858651 * get_guid_13() const { return ___guid_13; }
	inline FsmString_t952858651 ** get_address_of_guid_13() { return &___guid_13; }
	inline void set_guid_13(FsmString_t952858651 * value)
	{
		___guid_13 = value;
		Il2CppCodeGenWriteBarrier(&___guid_13, value);
	}

	inline static int32_t get_offset_of_externalIPAddress_14() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___externalIPAddress_14)); }
	inline FsmString_t952858651 * get_externalIPAddress_14() const { return ___externalIPAddress_14; }
	inline FsmString_t952858651 ** get_address_of_externalIPAddress_14() { return &___externalIPAddress_14; }
	inline void set_externalIPAddress_14(FsmString_t952858651 * value)
	{
		___externalIPAddress_14 = value;
		Il2CppCodeGenWriteBarrier(&___externalIPAddress_14, value);
	}

	inline static int32_t get_offset_of_externalPort_15() { return static_cast<int32_t>(offsetof(NetworkGetMessagePlayerProperties_t906059777, ___externalPort_15)); }
	inline FsmInt_t1596138449 * get_externalPort_15() const { return ___externalPort_15; }
	inline FsmInt_t1596138449 ** get_address_of_externalPort_15() { return &___externalPort_15; }
	inline void set_externalPort_15(FsmInt_t1596138449 * value)
	{
		___externalPort_15 = value;
		Il2CppCodeGenWriteBarrier(&___externalPort_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
