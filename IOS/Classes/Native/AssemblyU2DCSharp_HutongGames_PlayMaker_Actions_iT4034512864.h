﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenScaleFrom
struct  iTweenScaleFrom_t4034512864  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenScaleFrom::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenScaleFrom::id
	FsmString_t952858651 * ___id_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenScaleFrom::transformScale
	FsmGameObject_t1697147867 * ___transformScale_21;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenScaleFrom::vectorScale
	FsmVector3_t533912882 * ___vectorScale_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleFrom::time
	FsmFloat_t2134102846 * ___time_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleFrom::delay
	FsmFloat_t2134102846 * ___delay_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleFrom::speed
	FsmFloat_t2134102846 * ___speed_25;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenScaleFrom::easeType
	int32_t ___easeType_26;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenScaleFrom::loopType
	int32_t ___loopType_27;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___gameObject_19)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_19, value);
	}

	inline static int32_t get_offset_of_id_20() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___id_20)); }
	inline FsmString_t952858651 * get_id_20() const { return ___id_20; }
	inline FsmString_t952858651 ** get_address_of_id_20() { return &___id_20; }
	inline void set_id_20(FsmString_t952858651 * value)
	{
		___id_20 = value;
		Il2CppCodeGenWriteBarrier(&___id_20, value);
	}

	inline static int32_t get_offset_of_transformScale_21() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___transformScale_21)); }
	inline FsmGameObject_t1697147867 * get_transformScale_21() const { return ___transformScale_21; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformScale_21() { return &___transformScale_21; }
	inline void set_transformScale_21(FsmGameObject_t1697147867 * value)
	{
		___transformScale_21 = value;
		Il2CppCodeGenWriteBarrier(&___transformScale_21, value);
	}

	inline static int32_t get_offset_of_vectorScale_22() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___vectorScale_22)); }
	inline FsmVector3_t533912882 * get_vectorScale_22() const { return ___vectorScale_22; }
	inline FsmVector3_t533912882 ** get_address_of_vectorScale_22() { return &___vectorScale_22; }
	inline void set_vectorScale_22(FsmVector3_t533912882 * value)
	{
		___vectorScale_22 = value;
		Il2CppCodeGenWriteBarrier(&___vectorScale_22, value);
	}

	inline static int32_t get_offset_of_time_23() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___time_23)); }
	inline FsmFloat_t2134102846 * get_time_23() const { return ___time_23; }
	inline FsmFloat_t2134102846 ** get_address_of_time_23() { return &___time_23; }
	inline void set_time_23(FsmFloat_t2134102846 * value)
	{
		___time_23 = value;
		Il2CppCodeGenWriteBarrier(&___time_23, value);
	}

	inline static int32_t get_offset_of_delay_24() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___delay_24)); }
	inline FsmFloat_t2134102846 * get_delay_24() const { return ___delay_24; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_24() { return &___delay_24; }
	inline void set_delay_24(FsmFloat_t2134102846 * value)
	{
		___delay_24 = value;
		Il2CppCodeGenWriteBarrier(&___delay_24, value);
	}

	inline static int32_t get_offset_of_speed_25() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___speed_25)); }
	inline FsmFloat_t2134102846 * get_speed_25() const { return ___speed_25; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_25() { return &___speed_25; }
	inline void set_speed_25(FsmFloat_t2134102846 * value)
	{
		___speed_25 = value;
		Il2CppCodeGenWriteBarrier(&___speed_25, value);
	}

	inline static int32_t get_offset_of_easeType_26() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___easeType_26)); }
	inline int32_t get_easeType_26() const { return ___easeType_26; }
	inline int32_t* get_address_of_easeType_26() { return &___easeType_26; }
	inline void set_easeType_26(int32_t value)
	{
		___easeType_26 = value;
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(iTweenScaleFrom_t4034512864, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
