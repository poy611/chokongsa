﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>
struct U3CU3Ec__DisplayClass9_0_1_t4245322960;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0_1__ctor_m4192009634_gshared (U3CU3Ec__DisplayClass9_0_1_t4245322960 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass9_0_1__ctor_m4192009634(__this, method) ((  void (*) (U3CU3Ec__DisplayClass9_0_1_t4245322960 *, const MethodInfo*))U3CU3Ec__DisplayClass9_0_1__ctor_m4192009634_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1<System.Object>::<CreateSet>b__0(T,System.Object)
extern "C"  void U3CU3Ec__DisplayClass9_0_1_U3CCreateSetU3Eb__0_m3306643594_gshared (U3CU3Ec__DisplayClass9_0_1_t4245322960 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method);
#define U3CU3Ec__DisplayClass9_0_1_U3CCreateSetU3Eb__0_m3306643594(__this, ___o0, ___v1, method) ((  void (*) (U3CU3Ec__DisplayClass9_0_1_t4245322960 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3Ec__DisplayClass9_0_1_U3CCreateSetU3Eb__0_m3306643594_gshared)(__this, ___o0, ___v1, method)
