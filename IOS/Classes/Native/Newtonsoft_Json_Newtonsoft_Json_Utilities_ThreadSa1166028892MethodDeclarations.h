﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataContractAttribute>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m2392302415(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t1166028892 *, Func_2_t2770989516 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataContractAttribute>::Get(TKey)
#define ThreadSafeStore_2_Get_m3061462261(__this, ___key0, method) ((  DataContractAttribute_t2462274566 * (*) (ThreadSafeStore_2_t1166028892 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Object,System.Runtime.Serialization.DataContractAttribute>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m2480103101(__this, ___key0, method) ((  DataContractAttribute_t2462274566 * (*) (ThreadSafeStore_2_t1166028892 *, Il2CppObject *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
