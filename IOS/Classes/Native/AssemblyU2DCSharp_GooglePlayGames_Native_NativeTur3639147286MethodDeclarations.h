﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey92
struct U3CTakeTurnU3Ec__AnonStorey92_t3639147286;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3337232325.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T4255652325.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey92::.ctor()
extern "C"  void U3CTakeTurnU3Ec__AnonStorey92__ctor_m3733555989 (U3CTakeTurnU3Ec__AnonStorey92_t3639147286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey92::<>m__81(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CTakeTurnU3Ec__AnonStorey92_U3CU3Em__81_m2714738658 (U3CTakeTurnU3Ec__AnonStorey92_t3639147286 * __this, MultiplayerParticipant_t3337232325 * ___pendingParticipant0, NativeTurnBasedMatch_t302853426 * ___foundMatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey92::<>m__8D(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C"  void U3CTakeTurnU3Ec__AnonStorey92_U3CU3Em__8D_m3394170533 (U3CTakeTurnU3Ec__AnonStorey92_t3639147286 * __this, TurnBasedMatchResponse_t4255652325 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
