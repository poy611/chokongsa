﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookUpdate
struct iTweenLookUpdate_t2645160178;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::.ctor()
extern "C"  void iTweenLookUpdate__ctor_m2414469556 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::Reset()
extern "C"  void iTweenLookUpdate_Reset_m60902497 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnEnter()
extern "C"  void iTweenLookUpdate_OnEnter_m414632075 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnExit()
extern "C"  void iTweenLookUpdate_OnExit_m3347430829 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnUpdate()
extern "C"  void iTweenLookUpdate_OnUpdate_m3397219096 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::DoiTween()
extern "C"  void iTweenLookUpdate_DoiTween_m1419824669 (iTweenLookUpdate_t2645160178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
