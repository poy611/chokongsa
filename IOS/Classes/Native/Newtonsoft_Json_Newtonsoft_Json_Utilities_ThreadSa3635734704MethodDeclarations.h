﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSaf648438005MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m837338456(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t3635734704 *, Func_2_t945728032 *, const MethodInfo*))ThreadSafeStore_2__ctor_m713611997_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::Get(TKey)
#define ThreadSafeStore_2_Get_m619787774(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3635734704 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_Get_m2486152451_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1542495060(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3635734704 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_AddValue_m3702308719_gshared)(__this, ___key0, method)
