﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindGameObject
struct FindGameObject_t2514584402;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindGameObject::.ctor()
extern "C"  void FindGameObject__ctor_m1127948628 (FindGameObject_t2514584402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::Reset()
extern "C"  void FindGameObject_Reset_m3069348865 (FindGameObject_t2514584402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::OnEnter()
extern "C"  void FindGameObject_OnEnter_m1018601515 (FindGameObject_t2514584402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::Find()
extern "C"  void FindGameObject_Find_m3638328553 (FindGameObject_t2514584402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FindGameObject::ErrorCheck()
extern "C"  String_t* FindGameObject_ErrorCheck_m2641209971 (FindGameObject_t2514584402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
