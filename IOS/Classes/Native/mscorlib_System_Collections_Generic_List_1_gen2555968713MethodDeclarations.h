﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor()
#define List_1__ctor_m826655785(__this, method) ((  void (*) (List_1_t2555968713 *, const MethodInfo*))List_1__ctor_m1457888136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3621712333(__this, ___collection0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1384868247_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Int32)
#define List_1__ctor_m2763235779(__this, ___capacity0, method) ((  void (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.cctor()
#define List_1__cctor_m2304062075(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3527150076(__this, method) ((  Il2CppObject* (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1125368338(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2555968713 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3063470881(__this, method) ((  Il2CppObject * (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3932424764(__this, ___item0, method) ((  int32_t (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3104902916(__this, ___item0, method) ((  bool (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1474085524(__this, ___item0, method) ((  int32_t (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1980940039(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2555968713 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3283356353(__this, ___item0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2978548229(__this, method) ((  bool (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1742059992(__this, method) ((  bool (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3608869386(__this, method) ((  Il2CppObject * (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1623282035(__this, method) ((  bool (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m644920102(__this, method) ((  bool (*) (List_1_t2555968713 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3150483729(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1626817822(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2555968713 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Add(T)
#define List_1_Add_m3327115981(__this, ___item0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_Add_m1152248952_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m889882760(__this, ___newCount0, method) ((  void (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1733981791(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2555968713 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m813088646(__this, ___collection0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m74060870(__this, ___enumerable0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m314623633(__this, ___collection0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AsReadOnly()
#define List_1_AsReadOnly_m1378655444(__this, method) ((  ReadOnlyCollection_1_t2744860697 * (*) (List_1_t2555968713 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Clear()
#define List_1_Clear_m2068071325(__this, method) ((  void (*) (List_1_t2555968713 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Contains(T)
#define List_1_Contains_m239422351(__this, ___item0, method) ((  bool (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_Contains_m786374326_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CopyTo(T[])
#define List_1_CopyTo_m826217882(__this, ___array0, method) ((  void (*) (List_1_t2555968713 *, IEventU5BU5D_t659202564*, const MethodInfo*))List_1_CopyTo_m3016810556_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m221868413(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2555968713 *, IEventU5BU5D_t659202564*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Find(System.Predicate`1<T>)
#define List_1_Find_m2590767401(__this, ___match0, method) ((  Il2CppObject * (*) (List_1_t2555968713 *, Predicate_1_t798840044 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3824421382(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t798840044 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1994197731(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2555968713 *, int32_t, int32_t, Predicate_1_t798840044 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GetEnumerator()
#define List_1_GetEnumerator_m1873554764(__this, method) ((  Enumerator_t2575641483  (*) (List_1_t2555968713 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::IndexOf(T)
#define List_1_IndexOf_m3614486793(__this, ___item0, method) ((  int32_t (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3405413076(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2555968713 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4263202893(__this, ___index0, method) ((  void (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Insert(System.Int32,T)
#define List_1_Insert_m3633300276(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2555968713 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_Insert_m3135247846_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m220809385(__this, ___collection0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Remove(T)
#define List_1_Remove_m1613079370(__this, ___item0, method) ((  bool (*) (List_1_t2555968713 *, Il2CppObject *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3342801932(__this, ___match0, method) ((  int32_t (*) (List_1_t2555968713 *, Predicate_1_t798840044 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1507153146(__this, ___index0, method) ((  void (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4092449316_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1949039901(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2555968713 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Reverse()
#define List_1_Reverse_m509372210(__this, method) ((  void (*) (List_1_t2555968713 *, const MethodInfo*))List_1_Reverse_m2062055293_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Sort()
#define List_1_Sort_m3853088176(__this, method) ((  void (*) (List_1_t2555968713 *, const MethodInfo*))List_1_Sort_m3444130117_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3002594868(__this, ___comparer0, method) ((  void (*) (List_1_t2555968713 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2811436547(__this, ___comparison0, method) ((  void (*) (List_1_t2555968713 *, Comparison_1_t4199111644 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::ToArray()
#define List_1_ToArray_m1709126923(__this, method) ((  IEventU5BU5D_t659202564* (*) (List_1_t2555968713 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::TrimExcess()
#define List_1_TrimExcess_m3262484809(__this, method) ((  void (*) (List_1_t2555968713 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Capacity()
#define List_1_get_Capacity_m1045363897(__this, method) ((  int32_t (*) (List_1_t2555968713 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2395038874(__this, ___value0, method) ((  void (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Count()
#define List_1_get_Count_m197248402(__this, method) ((  int32_t (*) (List_1_t2555968713 *, const MethodInfo*))List_1_get_Count_m2222238344_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Item(System.Int32)
#define List_1_get_Item_m3443502048(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2555968713 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::set_Item(System.Int32,T)
#define List_1_set_Item_m1602750219(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2555968713 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_set_Item_m2510829103_gshared)(__this, ___index0, ___value1, method)
