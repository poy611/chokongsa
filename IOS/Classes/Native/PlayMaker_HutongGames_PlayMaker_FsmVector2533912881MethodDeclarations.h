﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"

// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVector2::get_Value()
extern "C"  Vector2_t4282066565  FsmVector2_get_Value_m1313754285 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::set_Value(UnityEngine.Vector2)
extern "C"  void FsmVector2_set_Value_m2900659718 (FsmVector2_t533912881 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmVector2::get_RawValue()
extern "C"  Il2CppObject * FsmVector2_get_RawValue_m1187454569 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::set_RawValue(System.Object)
extern "C"  void FsmVector2_set_RawValue_m948762604 (FsmVector2_t533912881 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor()
extern "C"  void FsmVector2__ctor_m1412212034 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor(System.String)
extern "C"  void FsmVector2__ctor_m3621197056 (FsmVector2_t533912881 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor(HutongGames.PlayMaker.FsmVector2)
extern "C"  void FsmVector2__ctor_m1515554087 (FsmVector2_t533912881 * __this, FsmVector2_t533912881 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVector2::Clone()
extern "C"  NamedVariable_t3211770239 * FsmVector2_Clone_m2095759827 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVector2::get_VariableType()
extern "C"  int32_t FsmVector2_get_VariableType_m3530836928 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVector2::ToString()
extern "C"  String_t* FsmVector2_ToString_m3504701649 (FsmVector2_t533912881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVector2::op_Implicit(UnityEngine.Vector2)
extern "C"  FsmVector2_t533912881 * FsmVector2_op_Implicit_m3781697693 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
