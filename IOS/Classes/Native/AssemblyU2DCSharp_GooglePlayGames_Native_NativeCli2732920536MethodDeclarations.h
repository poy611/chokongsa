﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey58
struct U3CIncrementAchievementU3Ec__AnonStorey58_t2732920536;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
struct FetchResponse_t2513188365;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_A2513188365.h"

// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey58::.ctor()
extern "C"  void U3CIncrementAchievementU3Ec__AnonStorey58__ctor_m421656979 (U3CIncrementAchievementU3Ec__AnonStorey58_t2732920536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey58::<>m__2E(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern "C"  void U3CIncrementAchievementU3Ec__AnonStorey58_U3CU3Em__2E_m1333197302 (U3CIncrementAchievementU3Ec__AnonStorey58_t2732920536 * __this, FetchResponse_t2513188365 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
