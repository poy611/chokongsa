﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVertexCount
struct GetVertexCount_t1542357885;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::.ctor()
extern "C"  void GetVertexCount__ctor_m69476745 (GetVertexCount_t1542357885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::Reset()
extern "C"  void GetVertexCount_Reset_m2010876982 (GetVertexCount_t1542357885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnEnter()
extern "C"  void GetVertexCount_OnEnter_m1734371104 (GetVertexCount_t1542357885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnUpdate()
extern "C"  void GetVertexCount_OnUpdate_m1359456035 (GetVertexCount_t1542357885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::DoGetVertexCount()
extern "C"  void GetVertexCount_DoGetVertexCount_m2112718235 (GetVertexCount_t1542357885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
