﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t922401854;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Pl922401854.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey68__ctor_m3008983158 (U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68::<>m__46(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey68_U3CU3Em__46_m2498043036 (U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581 * __this, PlayerSelectUIResponse_t922401854 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
