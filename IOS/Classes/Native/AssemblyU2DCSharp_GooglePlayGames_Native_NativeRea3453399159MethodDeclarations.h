﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64
struct U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64::.ctor()
extern "C"  void U3CCreateQuickGameU3Ec__AnonStorey64__ctor_m280364244 (U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
