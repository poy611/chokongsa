﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3150570266.h"
#include "System_System_Text_RegularExpressions_RegexOptions3066443743.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2971210514_gshared (Nullable_1_t3150570266 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2971210514(__this, ___value0, method) ((  void (*) (Nullable_1_t3150570266 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2971210514_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2100077933_gshared (Nullable_1_t3150570266 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2100077933(__this, method) ((  bool (*) (Nullable_1_t3150570266 *, const MethodInfo*))Nullable_1_get_HasValue_m2100077933_gshared)(__this, method)
// T System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2102899508_gshared (Nullable_1_t3150570266 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2102899508(__this, method) ((  int32_t (*) (Nullable_1_t3150570266 *, const MethodInfo*))Nullable_1_get_Value_m2102899508_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3663990620_gshared (Nullable_1_t3150570266 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3663990620(__this, ___other0, method) ((  bool (*) (Nullable_1_t3150570266 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3663990620_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1193985123_gshared (Nullable_1_t3150570266 * __this, Nullable_1_t3150570266  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1193985123(__this, ___other0, method) ((  bool (*) (Nullable_1_t3150570266 *, Nullable_1_t3150570266 , const MethodInfo*))Nullable_1_Equals_m1193985123_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1503232576_gshared (Nullable_1_t3150570266 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1503232576(__this, method) ((  int32_t (*) (Nullable_1_t3150570266 *, const MethodInfo*))Nullable_1_GetHashCode_m1503232576_gshared)(__this, method)
// T System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2153874494_gshared (Nullable_1_t3150570266 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2153874494(__this, method) ((  int32_t (*) (Nullable_1_t3150570266 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2153874494_gshared)(__this, method)
// T System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m973053871_gshared (Nullable_1_t3150570266 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m973053871(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3150570266 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m973053871_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Text.RegularExpressions.RegexOptions>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3863398982_gshared (Nullable_1_t3150570266 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3863398982(__this, method) ((  String_t* (*) (Nullable_1_t3150570266 *, const MethodInfo*))Nullable_1_ToString_m3863398982_gshared)(__this, method)
