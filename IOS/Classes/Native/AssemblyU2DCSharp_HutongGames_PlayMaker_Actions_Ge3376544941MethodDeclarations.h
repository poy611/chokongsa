﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct GetAnimatorBody_t3376544941;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::.ctor()
extern "C"  void GetAnimatorBody__ctor_m1903242537 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::Reset()
extern "C"  void GetAnimatorBody_Reset_m3844642774 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnEnter()
extern "C"  void GetAnimatorBody_OnEnter_m3046705856 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnActionUpdate()
extern "C"  void GetAnimatorBody_OnActionUpdate_m450700569 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::DoGetBodyPosition()
extern "C"  void GetAnimatorBody_DoGetBodyPosition_m4155442973 (GetAnimatorBody_t3376544941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
