﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Variation/<TimerCheck>c__Iterator26
struct U3CTimerCheckU3Ec__Iterator26_t3278601462;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator26__ctor_m3907616357 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<TimerCheck>c__Iterator26::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2946221719 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Variation/<TimerCheck>c__Iterator26::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m247715883 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Variation/<TimerCheck>c__Iterator26::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator26_MoveNext_m3271174487 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator26_Dispose_m1316185442 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::Reset()
extern "C"  void U3CTimerCheckU3Ec__Iterator26_Reset_m1554049298 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
