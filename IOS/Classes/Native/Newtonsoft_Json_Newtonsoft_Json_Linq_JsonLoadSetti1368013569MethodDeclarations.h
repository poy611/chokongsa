﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JsonLoadSettings
struct JsonLoadSettings_t1368013569;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_CommentHandli2319298658.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_LineInfoHandl1889929367.h"

// Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_CommentHandling()
extern "C"  int32_t JsonLoadSettings_get_CommentHandling_m1425198775 (JsonLoadSettings_t1368013569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_LineInfoHandling()
extern "C"  int32_t JsonLoadSettings_get_LineInfoHandling_m3924633227 (JsonLoadSettings_t1368013569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
