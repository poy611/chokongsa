﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButtonUp
struct GetButtonUp_t552055153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::.ctor()
extern "C"  void GetButtonUp__ctor_m1438730469 (GetButtonUp_t552055153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::Reset()
extern "C"  void GetButtonUp_Reset_m3380130706 (GetButtonUp_t552055153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::OnUpdate()
extern "C"  void GetButtonUp_OnUpdate_m3492737607 (GetButtonUp_t552055153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
