﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t1352686482;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey69
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t2043220689;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68
struct  U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68::newRoom
	RoomSession_t1352686482 * ___newRoom_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey69 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68::<>f__ref$105
	U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582 * ___U3CU3Ef__refU24105_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey68::<>f__this
	NativeRealtimeMultiplayerClient_t2043220689 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_newRoom_0() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581, ___newRoom_0)); }
	inline RoomSession_t1352686482 * get_newRoom_0() const { return ___newRoom_0; }
	inline RoomSession_t1352686482 ** get_address_of_newRoom_0() { return &___newRoom_0; }
	inline void set_newRoom_0(RoomSession_t1352686482 * value)
	{
		___newRoom_0 = value;
		Il2CppCodeGenWriteBarrier(&___newRoom_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24105_1() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581, ___U3CU3Ef__refU24105_1)); }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582 * get_U3CU3Ef__refU24105_1() const { return ___U3CU3Ef__refU24105_1; }
	inline U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582 ** get_address_of_U3CU3Ef__refU24105_1() { return &___U3CU3Ef__refU24105_1; }
	inline void set_U3CU3Ef__refU24105_1(U3CCreateWithInvitationScreenU3Ec__AnonStorey69_t703024582 * value)
	{
		___U3CU3Ef__refU24105_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24105_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CCreateWithInvitationScreenU3Ec__AnonStorey68_t703024581, ___U3CU3Ef__this_2)); }
	inline NativeRealtimeMultiplayerClient_t2043220689 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline NativeRealtimeMultiplayerClient_t2043220689 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(NativeRealtimeMultiplayerClient_t2043220689 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
