﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Newtonsoft_Json_Newtonsoft_Json_JsonContainerAttri1917602971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonArrayAttribute
struct  JsonArrayAttribute_t2333618243  : public JsonContainerAttribute_t1917602971
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
