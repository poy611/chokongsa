﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t2457384100;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_ShurikenThreadFix::.ctor()
extern "C"  void CFX_ShurikenThreadFix__ctor_m444939591 (CFX_ShurikenThreadFix_t2457384100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_ShurikenThreadFix::OnEnable()
extern "C"  void CFX_ShurikenThreadFix_OnEnable_m2997858527 (CFX_ShurikenThreadFix_t2457384100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CFX_ShurikenThreadFix::WaitFrame()
extern "C"  Il2CppObject * CFX_ShurikenThreadFix_WaitFrame_m409577381 (CFX_ShurikenThreadFix_t2457384100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
