﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// UnityEngine.Animator
struct Animator_t2776330603;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct  GetAnimatorBoneGameObject_t14629568  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::bone
	FsmEnum_t1076048395 * ___bone_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::boneGameObject
	FsmGameObject_t1697147867 * ___boneGameObject_13;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::_animator
	Animator_t2776330603 * ____animator_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_bone_12() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___bone_12)); }
	inline FsmEnum_t1076048395 * get_bone_12() const { return ___bone_12; }
	inline FsmEnum_t1076048395 ** get_address_of_bone_12() { return &___bone_12; }
	inline void set_bone_12(FsmEnum_t1076048395 * value)
	{
		___bone_12 = value;
		Il2CppCodeGenWriteBarrier(&___bone_12, value);
	}

	inline static int32_t get_offset_of_boneGameObject_13() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ___boneGameObject_13)); }
	inline FsmGameObject_t1697147867 * get_boneGameObject_13() const { return ___boneGameObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_boneGameObject_13() { return &___boneGameObject_13; }
	inline void set_boneGameObject_13(FsmGameObject_t1697147867 * value)
	{
		___boneGameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___boneGameObject_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t14629568, ____animator_14)); }
	inline Animator_t2776330603 * get__animator_14() const { return ____animator_14; }
	inline Animator_t2776330603 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t2776330603 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
