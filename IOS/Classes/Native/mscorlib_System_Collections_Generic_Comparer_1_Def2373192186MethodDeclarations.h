﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>
struct DefaultComparer_t2373192186;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>::.ctor()
extern "C"  void DefaultComparer__ctor_m3745262092_gshared (DefaultComparer_t2373192186 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3745262092(__this, method) ((  void (*) (DefaultComparer_t2373192186 *, const MethodInfo*))DefaultComparer__ctor_m3745262092_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1501508587_gshared (DefaultComparer_t2373192186 * __this, JsonPosition_t3864946409  ___x0, JsonPosition_t3864946409  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1501508587(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2373192186 *, JsonPosition_t3864946409 , JsonPosition_t3864946409 , const MethodInfo*))DefaultComparer_Compare_m1501508587_gshared)(__this, ___x0, ___y1, method)
