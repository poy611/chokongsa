﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowRecipe
struct  ShowRecipe_t2733639435  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.UI.Text ShowRecipe::menuTitle
	Text_t9039225 * ___menuTitle_2;
	// UnityEngine.UI.Text ShowRecipe::recipe
	Text_t9039225 * ___recipe_3;
	// UnityEngine.UI.Text ShowRecipe::goalPoint
	Text_t9039225 * ___goalPoint_4;

public:
	inline static int32_t get_offset_of_menuTitle_2() { return static_cast<int32_t>(offsetof(ShowRecipe_t2733639435, ___menuTitle_2)); }
	inline Text_t9039225 * get_menuTitle_2() const { return ___menuTitle_2; }
	inline Text_t9039225 ** get_address_of_menuTitle_2() { return &___menuTitle_2; }
	inline void set_menuTitle_2(Text_t9039225 * value)
	{
		___menuTitle_2 = value;
		Il2CppCodeGenWriteBarrier(&___menuTitle_2, value);
	}

	inline static int32_t get_offset_of_recipe_3() { return static_cast<int32_t>(offsetof(ShowRecipe_t2733639435, ___recipe_3)); }
	inline Text_t9039225 * get_recipe_3() const { return ___recipe_3; }
	inline Text_t9039225 ** get_address_of_recipe_3() { return &___recipe_3; }
	inline void set_recipe_3(Text_t9039225 * value)
	{
		___recipe_3 = value;
		Il2CppCodeGenWriteBarrier(&___recipe_3, value);
	}

	inline static int32_t get_offset_of_goalPoint_4() { return static_cast<int32_t>(offsetof(ShowRecipe_t2733639435, ___goalPoint_4)); }
	inline Text_t9039225 * get_goalPoint_4() const { return ___goalPoint_4; }
	inline Text_t9039225 ** get_address_of_goalPoint_4() { return &___goalPoint_4; }
	inline void set_goalPoint_4(Text_t9039225 * value)
	{
		___goalPoint_4 = value;
		Il2CppCodeGenWriteBarrier(&___goalPoint_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
