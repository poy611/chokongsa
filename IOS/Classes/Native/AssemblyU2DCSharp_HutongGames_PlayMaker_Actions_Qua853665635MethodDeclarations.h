﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLookRotation
struct QuaternionLookRotation_t853665635;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::.ctor()
extern "C"  void QuaternionLookRotation__ctor_m3809224803 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::Reset()
extern "C"  void QuaternionLookRotation_Reset_m1455657744 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnEnter()
extern "C"  void QuaternionLookRotation_OnEnter_m744628090 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnUpdate()
extern "C"  void QuaternionLookRotation_OnUpdate_m742193673 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnLateUpdate()
extern "C"  void QuaternionLookRotation_OnLateUpdate_m604017487 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnFixedUpdate()
extern "C"  void QuaternionLookRotation_OnFixedUpdate_m2420618239 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::DoQuatLookRotation()
extern "C"  void QuaternionLookRotation_DoQuatLookRotation_m54575520 (QuaternionLookRotation_t853665635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
