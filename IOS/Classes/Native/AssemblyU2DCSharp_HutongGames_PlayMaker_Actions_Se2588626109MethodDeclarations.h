﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCameraCullingMask
struct SetCameraCullingMask_t2588626109;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::.ctor()
extern "C"  void SetCameraCullingMask__ctor_m236703305 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::Reset()
extern "C"  void SetCameraCullingMask_Reset_m2178103542 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnEnter()
extern "C"  void SetCameraCullingMask_OnEnter_m3525305312 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnUpdate()
extern "C"  void SetCameraCullingMask_OnUpdate_m1043841635 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::DoSetCameraCullingMask()
extern "C"  void SetCameraCullingMask_DoSetCameraCullingMask_m3459216411 (SetCameraCullingMask_t2588626109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
