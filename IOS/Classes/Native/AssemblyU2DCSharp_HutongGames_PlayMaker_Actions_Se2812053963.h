﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3178143027.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightSpotAngle
struct  SetLightSpotAngle_t2812053963  : public ComponentAction_1_t3178143027
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightSpotAngle::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetLightSpotAngle::lightSpotAngle
	FsmFloat_t2134102846 * ___lightSpotAngle_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetLightSpotAngle::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t2812053963, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_lightSpotAngle_14() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t2812053963, ___lightSpotAngle_14)); }
	inline FsmFloat_t2134102846 * get_lightSpotAngle_14() const { return ___lightSpotAngle_14; }
	inline FsmFloat_t2134102846 ** get_address_of_lightSpotAngle_14() { return &___lightSpotAngle_14; }
	inline void set_lightSpotAngle_14(FsmFloat_t2134102846 * value)
	{
		___lightSpotAngle_14 = value;
		Il2CppCodeGenWriteBarrier(&___lightSpotAngle_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t2812053963, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
