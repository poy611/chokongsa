﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenResume
struct iTweenResume_t603494487;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenResume::.ctor()
extern "C"  void iTweenResume__ctor_m2527496175 (iTweenResume_t603494487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::Reset()
extern "C"  void iTweenResume_Reset_m173929116 (iTweenResume_t603494487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::OnEnter()
extern "C"  void iTweenResume_OnEnter_m1659030534 (iTweenResume_t603494487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::DoiTween()
extern "C"  void iTweenResume_DoiTween_m1341471234 (iTweenResume_t603494487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
