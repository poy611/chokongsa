﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PerSecond
struct PerSecond_t38067007;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PerSecond::.ctor()
extern "C"  void PerSecond__ctor_m1767989463 (PerSecond_t38067007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::Reset()
extern "C"  void PerSecond_Reset_m3709389700 (PerSecond_t38067007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::OnEnter()
extern "C"  void PerSecond_OnEnter_m1917520622 (PerSecond_t38067007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::OnUpdate()
extern "C"  void PerSecond_OnUpdate_m2742123797 (PerSecond_t38067007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::DoPerSecond()
extern "C"  void PerSecond_DoPerSecond_m3368797467 (PerSecond_t38067007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
