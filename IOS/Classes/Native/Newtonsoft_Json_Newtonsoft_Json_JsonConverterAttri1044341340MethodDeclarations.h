﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonConverterAttribute
struct JsonConverterAttribute_t1044341340;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Type Newtonsoft.Json.JsonConverterAttribute::get_ConverterType()
extern "C"  Type_t * JsonConverterAttribute_get_ConverterType_m1503851042 (JsonConverterAttribute_t1044341340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] Newtonsoft.Json.JsonConverterAttribute::get_ConverterParameters()
extern "C"  ObjectU5BU5D_t1108656482* JsonConverterAttribute_get_ConverterParameters_m1438049785 (JsonConverterAttribute_t1044341340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
