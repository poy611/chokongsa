﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3872441731(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2662115696 *, HandleRef_t1780819301 , BaseReferenceHolder_t2237584300 *, const MethodInfo*))KeyValuePair_2__ctor_m3704436026_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::get_Key()
#define KeyValuePair_2_get_Key_m1509092997(__this, method) ((  HandleRef_t1780819301  (*) (KeyValuePair_2_t2662115696 *, const MethodInfo*))KeyValuePair_2_get_Key_m2927877486_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1990454982(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2662115696 *, HandleRef_t1780819301 , const MethodInfo*))KeyValuePair_2_set_Key_m2433763503_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::get_Value()
#define KeyValuePair_2_get_Value_m2069387753(__this, method) ((  BaseReferenceHolder_t2237584300 * (*) (KeyValuePair_2_t2662115696 *, const MethodInfo*))KeyValuePair_2_get_Value_m3910513454_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2252238790(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2662115696 *, BaseReferenceHolder_t2237584300 *, const MethodInfo*))KeyValuePair_2_set_Value_m2108271919_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::ToString()
#define KeyValuePair_2_ToString_m1071639042(__this, method) ((  String_t* (*) (KeyValuePair_2_t2662115696 *, const MethodInfo*))KeyValuePair_2_ToString_m2948487315_gshared)(__this, method)
