﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// System.String
struct String_t;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t175677893;
// Newtonsoft.Json.Serialization.IAttributeProvider
struct IAttributeProvider_t47205104;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Required3921306327.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"

// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::get_PropertyContract()
extern "C"  JsonContract_t1328848902 * JsonProperty_get_PropertyContract_m540790020 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyContract(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonProperty_set_PropertyContract_m2806280965 (JsonProperty_t902655177 * __this, JsonContract_t1328848902 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern "C"  String_t* JsonProperty_get_PropertyName_m2274710618 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern "C"  void JsonProperty_set_PropertyName_m3567303863 (JsonProperty_t902655177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonProperty::get_DeclaringType()
extern "C"  Type_t * JsonProperty_get_DeclaringType_m1198845170 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DeclaringType(System.Type)
extern "C"  void JsonProperty_set_DeclaringType_m2340480033 (JsonProperty_t902655177 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern "C"  Nullable_1_t1237965023  JsonProperty_get_Order_m1540066442 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern "C"  void JsonProperty_set_Order_m3665495753 (JsonProperty_t902655177 * __this, Nullable_1_t1237965023  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern "C"  String_t* JsonProperty_get_UnderlyingName_m1882359202 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern "C"  void JsonProperty_set_UnderlyingName_m1956063407 (JsonProperty_t902655177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern "C"  Il2CppObject * JsonProperty_get_ValueProvider_m2863328902 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern "C"  void JsonProperty_set_ValueProvider_m2084556301 (JsonProperty_t902655177 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_AttributeProvider(Newtonsoft.Json.Serialization.IAttributeProvider)
extern "C"  void JsonProperty_set_AttributeProvider_m723227821 (JsonProperty_t902655177 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern "C"  Type_t * JsonProperty_get_PropertyType_m3012129088 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern "C"  void JsonProperty_set_PropertyType_m3205645855 (JsonProperty_t902655177 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern "C"  JsonConverter_t2159686854 * JsonProperty_get_Converter_m3340390107 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_Converter_m2475984792 (JsonProperty_t902655177 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_MemberConverter()
extern "C"  JsonConverter_t2159686854 * JsonProperty_get_MemberConverter_m3658486881 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_MemberConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_MemberConverter_m1016868050 (JsonProperty_t902655177 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern "C"  bool JsonProperty_get_Ignored_m1658498483 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern "C"  void JsonProperty_set_Ignored_m249596832 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern "C"  bool JsonProperty_get_Readable_m3171627153 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern "C"  void JsonProperty_set_Readable_m2186423634 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern "C"  bool JsonProperty_get_Writable_m2539725889 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern "C"  void JsonProperty_set_Writable_m815525122 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_HasMemberAttribute()
extern "C"  bool JsonProperty_get_HasMemberAttribute_m4094751177 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_HasMemberAttribute(System.Boolean)
extern "C"  void JsonProperty_set_HasMemberAttribute_m1104488458 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern "C"  Il2CppObject * JsonProperty_get_DefaultValue_m1314351388 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern "C"  void JsonProperty_set_DefaultValue_m3159201689 (JsonProperty_t902655177 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonProperty::GetResolvedDefaultValue()
extern "C"  Il2CppObject * JsonProperty_GetResolvedDefaultValue_m2650166045 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern "C"  int32_t JsonProperty_get_Required_m1605272789 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern "C"  Nullable_1_t560925241  JsonProperty_get_IsReference_m569541539 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonProperty_set_IsReference_m2353501072 (JsonProperty_t902655177 * __this, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern "C"  Nullable_1_t2838778904  JsonProperty_get_NullValueHandling_m2242841398 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern "C"  void JsonProperty_set_NullValueHandling_m1672170007 (JsonProperty_t902655177 * __this, Nullable_1_t2838778904  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern "C"  Nullable_1_t1653574568  JsonProperty_get_DefaultValueHandling_m1770517270 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern "C"  void JsonProperty_set_DefaultValueHandling_m3798477737 (JsonProperty_t902655177 * __this, Nullable_1_t1653574568  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern "C"  Nullable_1_t2845787645  JsonProperty_get_ReferenceLoopHandling_m2782803094 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern "C"  void JsonProperty_set_ReferenceLoopHandling_m481143265 (JsonProperty_t902655177 * __this, Nullable_1_t2845787645  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern "C"  Nullable_1_t140208118  JsonProperty_get_ObjectCreationHandling_m150519090 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern "C"  void JsonProperty_set_ObjectCreationHandling_m3426223017 (JsonProperty_t902655177 * __this, Nullable_1_t140208118  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern "C"  Nullable_1_t2443451997  JsonProperty_get_TypeNameHandling_m888022976 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void JsonProperty_set_TypeNameHandling_m1649512713 (JsonProperty_t902655177 * __this, Nullable_1_t2443451997  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern "C"  Predicate_1_t3781873254 * JsonProperty_get_ShouldSerialize_m4035679352 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_ShouldSerialize_m3909765531 (JsonProperty_t902655177 * __this, Predicate_1_t3781873254 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldDeserialize()
extern "C"  Predicate_1_t3781873254 * JsonProperty_get_ShouldDeserialize_m2822411159 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern "C"  Predicate_1_t3781873254 * JsonProperty_get_GetIsSpecified_m505934311 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_GetIsSpecified_m2713026852 (JsonProperty_t902655177 * __this, Predicate_1_t3781873254 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern "C"  Action_2_t4293064463 * JsonProperty_get_SetIsSpecified_m3173333579 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(System.Action`2<System.Object,System.Object>)
extern "C"  void JsonProperty_set_SetIsSpecified_m1743790604 (JsonProperty_t902655177 * __this, Action_2_t4293064463 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern "C"  String_t* JsonProperty_ToString_m550836669 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_ItemConverter()
extern "C"  JsonConverter_t2159686854 * JsonProperty_get_ItemConverter_m2046047432 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_ItemConverter_m167752395 (JsonProperty_t902655177 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_ItemIsReference()
extern "C"  Nullable_1_t560925241  JsonProperty_get_ItemIsReference_m3345372880 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonProperty_set_ItemIsReference_m2084850819 (JsonProperty_t902655177 * __this, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemTypeNameHandling()
extern "C"  Nullable_1_t2443451997  JsonProperty_get_ItemTypeNameHandling_m902016435 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void JsonProperty_set_ItemTypeNameHandling_m2332063414 (JsonProperty_t902655177 * __this, Nullable_1_t2443451997  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemReferenceLoopHandling()
extern "C"  Nullable_1_t2845787645  JsonProperty_get_ItemReferenceLoopHandling_m1502208899 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern "C"  void JsonProperty_set_ItemReferenceLoopHandling_m4132379470 (JsonProperty_t902655177 * __this, Nullable_1_t2845787645  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::WritePropertyName(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonProperty_WritePropertyName_m3730435427 (JsonProperty_t902655177 * __this, JsonWriter_t972330355 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern "C"  void JsonProperty__ctor_m1200650838 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
