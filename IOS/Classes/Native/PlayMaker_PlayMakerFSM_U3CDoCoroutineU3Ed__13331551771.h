﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFSM/<DoCoroutine>d__1
struct  U3CDoCoroutineU3Ed__1_t3331551771  : public Il2CppObject
{
public:
	// System.Object PlayMakerFSM/<DoCoroutine>d__1::<>2__current
	Il2CppObject * ___U3CU3E2__current_0;
	// System.Int32 PlayMakerFSM/<DoCoroutine>d__1::<>1__state
	int32_t ___U3CU3E1__state_1;
	// PlayMakerFSM PlayMakerFSM/<DoCoroutine>d__1::<>4__this
	PlayMakerFSM_t3799847376 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator PlayMakerFSM/<DoCoroutine>d__1::routine
	Il2CppObject * ___routine_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__1_t3331551771, ___U3CU3E2__current_0)); }
	inline Il2CppObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(Il2CppObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__1_t3331551771, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__1_t3331551771, ___U3CU3E4__this_2)); }
	inline PlayMakerFSM_t3799847376 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayMakerFSM_t3799847376 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_2, value);
	}

	inline static int32_t get_offset_of_routine_3() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__1_t3331551771, ___routine_3)); }
	inline Il2CppObject * get_routine_3() const { return ___routine_3; }
	inline Il2CppObject ** get_address_of_routine_3() { return &___routine_3; }
	inline void set_routine_3(Il2CppObject * value)
	{
		___routine_3 = value;
		Il2CppCodeGenWriteBarrier(&___routine_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
