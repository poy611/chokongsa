﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2845477652.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Runtime.Serialization.StreamingContext>::.ctor(T)
extern "C"  void Nullable_1__ctor_m36707221_gshared (Nullable_1_t2845477652 * __this, StreamingContext_t2761351129  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m36707221(__this, ___value0, method) ((  void (*) (Nullable_1_t2845477652 *, StreamingContext_t2761351129 , const MethodInfo*))Nullable_1__ctor_m36707221_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.StreamingContext>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m550836081_gshared (Nullable_1_t2845477652 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m550836081(__this, method) ((  bool (*) (Nullable_1_t2845477652 *, const MethodInfo*))Nullable_1_get_HasValue_m550836081_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.StreamingContext>::get_Value()
extern "C"  StreamingContext_t2761351129  Nullable_1_get_Value_m824145392_gshared (Nullable_1_t2845477652 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m824145392(__this, method) ((  StreamingContext_t2761351129  (*) (Nullable_1_t2845477652 *, const MethodInfo*))Nullable_1_get_Value_m824145392_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.StreamingContext>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1940530136_gshared (Nullable_1_t2845477652 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1940530136(__this, ___other0, method) ((  bool (*) (Nullable_1_t2845477652 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1940530136_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.StreamingContext>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3149392999_gshared (Nullable_1_t2845477652 * __this, Nullable_1_t2845477652  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3149392999(__this, ___other0, method) ((  bool (*) (Nullable_1_t2845477652 *, Nullable_1_t2845477652 , const MethodInfo*))Nullable_1_Equals_m3149392999_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Runtime.Serialization.StreamingContext>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m611524924_gshared (Nullable_1_t2845477652 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m611524924(__this, method) ((  int32_t (*) (Nullable_1_t2845477652 *, const MethodInfo*))Nullable_1_GetHashCode_m611524924_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.StreamingContext>::GetValueOrDefault()
extern "C"  StreamingContext_t2761351129  Nullable_1_GetValueOrDefault_m3601881914_gshared (Nullable_1_t2845477652 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3601881914(__this, method) ((  StreamingContext_t2761351129  (*) (Nullable_1_t2845477652 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3601881914_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.StreamingContext>::GetValueOrDefault(T)
extern "C"  StreamingContext_t2761351129  Nullable_1_GetValueOrDefault_m3821088883_gshared (Nullable_1_t2845477652 * __this, StreamingContext_t2761351129  ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3821088883(__this, ___defaultValue0, method) ((  StreamingContext_t2761351129  (*) (Nullable_1_t2845477652 *, StreamingContext_t2761351129 , const MethodInfo*))Nullable_1_GetValueOrDefault_m3821088883_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Runtime.Serialization.StreamingContext>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3841148682_gshared (Nullable_1_t2845477652 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3841148682(__this, method) ((  String_t* (*) (Nullable_1_t2845477652 *, const MethodInfo*))Nullable_1_ToString_m3841148682_gshared)(__this, method)
