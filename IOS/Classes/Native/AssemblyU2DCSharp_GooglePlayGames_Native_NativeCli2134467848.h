﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t3798002602;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55
struct  U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848  : public Il2CppObject
{
public:
	// System.String GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55::achId
	String_t* ___achId_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey55::<>f__this
	NativeClient_t3798002602 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_achId_0() { return static_cast<int32_t>(offsetof(U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848, ___achId_0)); }
	inline String_t* get_achId_0() const { return ___achId_0; }
	inline String_t** get_address_of_achId_0() { return &___achId_0; }
	inline void set_achId_0(String_t* value)
	{
		___achId_0 = value;
		Il2CppCodeGenWriteBarrier(&___achId_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CUnlockAchievementU3Ec__AnonStorey55_t2134467848, ___U3CU3Ef__this_1)); }
	inline NativeClient_t3798002602 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline NativeClient_t3798002602 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(NativeClient_t3798002602 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
