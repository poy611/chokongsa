﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct DefaultComparer_t877068327;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor()
extern "C"  void DefaultComparer__ctor_m1107552019_gshared (DefaultComparer_t877068327 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1107552019(__this, method) ((  void (*) (DefaultComparer_t877068327 *, const MethodInfo*))DefaultComparer__ctor_m1107552019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1130784056_gshared (DefaultComparer_t877068327 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1130784056(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t877068327 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1130784056_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1953214564_gshared (DefaultComparer_t877068327 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1953214564(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t877068327 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1953214564_gshared)(__this, ___x0, ___y1, method)
