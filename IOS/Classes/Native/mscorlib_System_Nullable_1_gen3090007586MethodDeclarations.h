﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3090007586.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::.ctor(T)
extern "C"  void Nullable_1__ctor_m811967786_gshared (Nullable_1_t3090007586 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m811967786(__this, ___value0, method) ((  void (*) (Nullable_1_t3090007586 *, int32_t, const MethodInfo*))Nullable_1__ctor_m811967786_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m4022551322_gshared (Nullable_1_t3090007586 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m4022551322(__this, method) ((  bool (*) (Nullable_1_t3090007586 *, const MethodInfo*))Nullable_1_get_HasValue_m4022551322_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3598251801_gshared (Nullable_1_t3090007586 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3598251801(__this, method) ((  int32_t (*) (Nullable_1_t3090007586 *, const MethodInfo*))Nullable_1_get_Value_m3598251801_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2645772967_gshared (Nullable_1_t3090007586 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2645772967(__this, ___other0, method) ((  bool (*) (Nullable_1_t3090007586 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2645772967_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1363889848_gshared (Nullable_1_t3090007586 * __this, Nullable_1_t3090007586  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1363889848(__this, ___other0, method) ((  bool (*) (Nullable_1_t3090007586 *, Nullable_1_t3090007586 , const MethodInfo*))Nullable_1_Equals_m1363889848_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2542571647_gshared (Nullable_1_t3090007586 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2542571647(__this, method) ((  int32_t (*) (Nullable_1_t3090007586 *, const MethodInfo*))Nullable_1_GetHashCode_m2542571647_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3247752549_gshared (Nullable_1_t3090007586 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3247752549(__this, method) ((  int32_t (*) (Nullable_1_t3090007586 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3247752549_gshared)(__this, method)
// T System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1414262122_gshared (Nullable_1_t3090007586 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1414262122(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3090007586 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1414262122_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2068628217_gshared (Nullable_1_t3090007586 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2068628217(__this, method) ((  String_t* (*) (Nullable_1_t3090007586 *, const MethodInfo*))Nullable_1_ToString_m2068628217_gshared)(__this, method)
