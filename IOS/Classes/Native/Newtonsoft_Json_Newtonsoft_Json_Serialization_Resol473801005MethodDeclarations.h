﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t473801005;
struct ResolverContractKey_t473801005_marshaled_pinvoke;
struct ResolverContractKey_t473801005_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.ResolverContractKey::.ctor(System.Type,System.Type)
extern "C"  void ResolverContractKey__ctor_m1861852512 (ResolverContractKey_t473801005 * __this, Type_t * ___resolverType0, Type_t * ___contractType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.ResolverContractKey::GetHashCode()
extern "C"  int32_t ResolverContractKey_GetHashCode_m3761646975 (ResolverContractKey_t473801005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.ResolverContractKey::Equals(System.Object)
extern "C"  bool ResolverContractKey_Equals_m1480503975 (ResolverContractKey_t473801005 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.ResolverContractKey::Equals(Newtonsoft.Json.Serialization.ResolverContractKey)
extern "C"  bool ResolverContractKey_Equals_m3946561873 (ResolverContractKey_t473801005 * __this, ResolverContractKey_t473801005  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ResolverContractKey_t473801005;
struct ResolverContractKey_t473801005_marshaled_pinvoke;

extern "C" void ResolverContractKey_t473801005_marshal_pinvoke(const ResolverContractKey_t473801005& unmarshaled, ResolverContractKey_t473801005_marshaled_pinvoke& marshaled);
extern "C" void ResolverContractKey_t473801005_marshal_pinvoke_back(const ResolverContractKey_t473801005_marshaled_pinvoke& marshaled, ResolverContractKey_t473801005& unmarshaled);
extern "C" void ResolverContractKey_t473801005_marshal_pinvoke_cleanup(ResolverContractKey_t473801005_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ResolverContractKey_t473801005;
struct ResolverContractKey_t473801005_marshaled_com;

extern "C" void ResolverContractKey_t473801005_marshal_com(const ResolverContractKey_t473801005& unmarshaled, ResolverContractKey_t473801005_marshaled_com& marshaled);
extern "C" void ResolverContractKey_t473801005_marshal_com_back(const ResolverContractKey_t473801005_marshaled_com& marshaled, ResolverContractKey_t473801005& unmarshaled);
extern "C" void ResolverContractKey_t473801005_marshal_com_cleanup(ResolverContractKey_t473801005_marshaled_com& marshaled);
