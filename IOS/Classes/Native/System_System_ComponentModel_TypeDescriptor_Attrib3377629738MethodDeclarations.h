﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/AttributeProvider
struct AttributeProvider_t3377629738;
// System.Attribute[]
struct AttributeU5BU5D_t4055800263;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t3543085017;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3543085017.h"

// System.Void System.ComponentModel.TypeDescriptor/AttributeProvider::.ctor(System.Attribute[],System.ComponentModel.TypeDescriptionProvider)
extern "C"  void AttributeProvider__ctor_m2493812559 (AttributeProvider_t3377629738 * __this, AttributeU5BU5D_t4055800263* ___attributes0, TypeDescriptionProvider_t3543085017 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
