﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t1970079725;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t901821544;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::AddKey(System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyKeyedCollection_AddKey_m2345600686 (JPropertyKeyedCollection_t1970079725 * __this, String_t* ___key0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::ClearItems()
extern "C"  void JPropertyKeyedCollection_ClearItems_m3935754221 (JPropertyKeyedCollection_t1970079725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::Contains(System.String)
extern "C"  bool JPropertyKeyedCollection_Contains_m1253283251 (JPropertyKeyedCollection_t1970079725 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::EnsureDictionary()
extern "C"  void JPropertyKeyedCollection_EnsureDictionary_m2532644590 (JPropertyKeyedCollection_t1970079725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JPropertyKeyedCollection::GetKeyForItem(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JPropertyKeyedCollection_GetKeyForItem_m2049165900 (JPropertyKeyedCollection_t1970079725 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyKeyedCollection_InsertItem_m868515675 (JPropertyKeyedCollection_t1970079725 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveItem(System.Int32)
extern "C"  void JPropertyKeyedCollection_RemoveItem_m4128777986 (JPropertyKeyedCollection_t1970079725 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveKey(System.String)
extern "C"  void JPropertyKeyedCollection_RemoveKey_m2604614591 (JPropertyKeyedCollection_t1970079725 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyKeyedCollection_SetItem_m3813151090 (JPropertyKeyedCollection_t1970079725 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern "C"  bool JPropertyKeyedCollection_TryGetValue_m1145503246 (JPropertyKeyedCollection_t1970079725 * __this, String_t* ___key0, JToken_t3412245951 ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Newtonsoft.Json.Linq.JPropertyKeyedCollection::get_Keys()
extern "C"  Il2CppObject* JPropertyKeyedCollection_get_Keys_m3243439973 (JPropertyKeyedCollection_t1970079725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.ctor()
extern "C"  void JPropertyKeyedCollection__ctor_m2455643658 (JPropertyKeyedCollection_t1970079725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.cctor()
extern "C"  void JPropertyKeyedCollection__cctor_m2628413155 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
