﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntInterpolate
struct IntInterpolate_t1074336890;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntInterpolate::.ctor()
extern "C"  void IntInterpolate__ctor_m3649068332 (IntInterpolate_t1074336890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntInterpolate::Reset()
extern "C"  void IntInterpolate_Reset_m1295501273 (IntInterpolate_t1074336890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntInterpolate::OnEnter()
extern "C"  void IntInterpolate_OnEnter_m1453082115 (IntInterpolate_t1074336890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntInterpolate::OnUpdate()
extern "C"  void IntInterpolate_OnUpdate_m1229431968 (IntInterpolate_t1074336890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
