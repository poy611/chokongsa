﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
struct FetchResponse_t3476068802;
// GooglePlayGames.Native.PInvoke.NativeEvent
struct NativeEvent_t2485247913;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::.ctor(System.IntPtr)
extern "C"  void FetchResponse__ctor_m116777227 (FetchResponse_t3476068802 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::ResponseStatus()
extern "C"  int32_t FetchResponse_ResponseStatus_m180785349 (FetchResponse_t3476068802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::RequestSucceeded()
extern "C"  bool FetchResponse_RequestSucceeded_m1781897497 (FetchResponse_t3476068802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::Data()
extern "C"  NativeEvent_t2485247913 * FetchResponse_Data_m3596182664 (FetchResponse_t3476068802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchResponse_CallDispose_m1011186181 (FetchResponse_t3476068802 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::FromPointer(System.IntPtr)
extern "C"  FetchResponse_t3476068802 * FetchResponse_FromPointer_m527377257 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
