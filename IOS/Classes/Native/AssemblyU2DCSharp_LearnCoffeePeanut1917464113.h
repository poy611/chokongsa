﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearnCoffeePeanut
struct  LearnCoffeePeanut_t1917464113  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] LearnCoffeePeanut::source
	SpriteU5BU5D_t2761310900* ___source_2;
	// UnityEngine.UI.Image LearnCoffeePeanut::desk
	Image_t538875265 * ___desk_3;
	// UnityEngine.UI.Button LearnCoffeePeanut::prevbt
	Button_t3896396478 * ___prevbt_4;
	// UnityEngine.UI.Button LearnCoffeePeanut::nextbt
	Button_t3896396478 * ___nextbt_5;
	// UnityEngine.UI.Text LearnCoffeePeanut::tran
	Text_t9039225 * ___tran_6;
	// UnityEngine.UI.Text LearnCoffeePeanut::descript
	Text_t9039225 * ___descript_7;
	// UnityEngine.UI.Text LearnCoffeePeanut::pageNum
	Text_t9039225 * ___pageNum_8;
	// System.Int32 LearnCoffeePeanut::cursur
	int32_t ___cursur_9;
	// System.String[] LearnCoffeePeanut::arrToolName
	StringU5BU5D_t4054002952* ___arrToolName_10;
	// System.String[] LearnCoffeePeanut::arrToolDscrpt
	StringU5BU5D_t4054002952* ___arrToolDscrpt_11;

public:
	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___source_2)); }
	inline SpriteU5BU5D_t2761310900* get_source_2() const { return ___source_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(SpriteU5BU5D_t2761310900* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier(&___source_2, value);
	}

	inline static int32_t get_offset_of_desk_3() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___desk_3)); }
	inline Image_t538875265 * get_desk_3() const { return ___desk_3; }
	inline Image_t538875265 ** get_address_of_desk_3() { return &___desk_3; }
	inline void set_desk_3(Image_t538875265 * value)
	{
		___desk_3 = value;
		Il2CppCodeGenWriteBarrier(&___desk_3, value);
	}

	inline static int32_t get_offset_of_prevbt_4() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___prevbt_4)); }
	inline Button_t3896396478 * get_prevbt_4() const { return ___prevbt_4; }
	inline Button_t3896396478 ** get_address_of_prevbt_4() { return &___prevbt_4; }
	inline void set_prevbt_4(Button_t3896396478 * value)
	{
		___prevbt_4 = value;
		Il2CppCodeGenWriteBarrier(&___prevbt_4, value);
	}

	inline static int32_t get_offset_of_nextbt_5() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___nextbt_5)); }
	inline Button_t3896396478 * get_nextbt_5() const { return ___nextbt_5; }
	inline Button_t3896396478 ** get_address_of_nextbt_5() { return &___nextbt_5; }
	inline void set_nextbt_5(Button_t3896396478 * value)
	{
		___nextbt_5 = value;
		Il2CppCodeGenWriteBarrier(&___nextbt_5, value);
	}

	inline static int32_t get_offset_of_tran_6() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___tran_6)); }
	inline Text_t9039225 * get_tran_6() const { return ___tran_6; }
	inline Text_t9039225 ** get_address_of_tran_6() { return &___tran_6; }
	inline void set_tran_6(Text_t9039225 * value)
	{
		___tran_6 = value;
		Il2CppCodeGenWriteBarrier(&___tran_6, value);
	}

	inline static int32_t get_offset_of_descript_7() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___descript_7)); }
	inline Text_t9039225 * get_descript_7() const { return ___descript_7; }
	inline Text_t9039225 ** get_address_of_descript_7() { return &___descript_7; }
	inline void set_descript_7(Text_t9039225 * value)
	{
		___descript_7 = value;
		Il2CppCodeGenWriteBarrier(&___descript_7, value);
	}

	inline static int32_t get_offset_of_pageNum_8() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___pageNum_8)); }
	inline Text_t9039225 * get_pageNum_8() const { return ___pageNum_8; }
	inline Text_t9039225 ** get_address_of_pageNum_8() { return &___pageNum_8; }
	inline void set_pageNum_8(Text_t9039225 * value)
	{
		___pageNum_8 = value;
		Il2CppCodeGenWriteBarrier(&___pageNum_8, value);
	}

	inline static int32_t get_offset_of_cursur_9() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___cursur_9)); }
	inline int32_t get_cursur_9() const { return ___cursur_9; }
	inline int32_t* get_address_of_cursur_9() { return &___cursur_9; }
	inline void set_cursur_9(int32_t value)
	{
		___cursur_9 = value;
	}

	inline static int32_t get_offset_of_arrToolName_10() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___arrToolName_10)); }
	inline StringU5BU5D_t4054002952* get_arrToolName_10() const { return ___arrToolName_10; }
	inline StringU5BU5D_t4054002952** get_address_of_arrToolName_10() { return &___arrToolName_10; }
	inline void set_arrToolName_10(StringU5BU5D_t4054002952* value)
	{
		___arrToolName_10 = value;
		Il2CppCodeGenWriteBarrier(&___arrToolName_10, value);
	}

	inline static int32_t get_offset_of_arrToolDscrpt_11() { return static_cast<int32_t>(offsetof(LearnCoffeePeanut_t1917464113, ___arrToolDscrpt_11)); }
	inline StringU5BU5D_t4054002952* get_arrToolDscrpt_11() const { return ___arrToolDscrpt_11; }
	inline StringU5BU5D_t4054002952** get_address_of_arrToolDscrpt_11() { return &___arrToolDscrpt_11; }
	inline void set_arrToolDscrpt_11(StringU5BU5D_t4054002952* value)
	{
		___arrToolDscrpt_11 = value;
		Il2CppCodeGenWriteBarrier(&___arrToolDscrpt_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
