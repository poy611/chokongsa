﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertEnumToString
struct ConvertEnumToString_t140274542;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::.ctor()
extern "C"  void ConvertEnumToString__ctor_m1652167560 (ConvertEnumToString_t140274542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::Reset()
extern "C"  void ConvertEnumToString_Reset_m3593567797 (ConvertEnumToString_t140274542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::OnEnter()
extern "C"  void ConvertEnumToString_OnEnter_m2281821535 (ConvertEnumToString_t140274542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::OnUpdate()
extern "C"  void ConvertEnumToString_OnUpdate_m1150550212 (ConvertEnumToString_t140274542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::DoConvertEnumToString()
extern "C"  void ConvertEnumToString_DoConvertEnumToString_m3501156027 (ConvertEnumToString_t140274542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
