﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowRecipePre/<enableCoru>c__Iterator2C
struct U3CenableCoruU3Ec__Iterator2C_t1488953467;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::.ctor()
extern "C"  void U3CenableCoruU3Ec__Iterator2C__ctor_m2108770640 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShowRecipePre/<enableCoru>c__Iterator2C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CenableCoruU3Ec__Iterator2C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2201829634 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShowRecipePre/<enableCoru>c__Iterator2C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CenableCoruU3Ec__Iterator2C_System_Collections_IEnumerator_get_Current_m2227538070 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowRecipePre/<enableCoru>c__Iterator2C::MoveNext()
extern "C"  bool U3CenableCoruU3Ec__Iterator2C_MoveNext_m4084401828 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::Dispose()
extern "C"  void U3CenableCoruU3Ec__Iterator2C_Dispose_m3497271693 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::Reset()
extern "C"  void U3CenableCoruU3Ec__Iterator2C_Reset_m4050170877 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
