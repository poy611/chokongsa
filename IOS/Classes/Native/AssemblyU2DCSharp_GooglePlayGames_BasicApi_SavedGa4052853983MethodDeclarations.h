﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t4052853983;
struct SavedGameMetadataUpdate_t4052853983_marshaled_pinvoke;
struct SavedGameMetadataUpdate_t4052853983_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4052853983.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1414359808.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"

// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder)
extern "C"  void SavedGameMetadataUpdate__ctor_m2933627399 (SavedGameMetadataUpdate_t4052853983 * __this, Builder_t1414359808  ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1911575107 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
extern "C"  String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m2478192842 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m3528254261 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
extern "C"  ByteU5BU5D_t4260760469* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m2686768578 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
extern "C"  bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_m2853300825 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
extern "C"  Nullable_1_t497649510  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m2719683437 (SavedGameMetadataUpdate_t4052853983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SavedGameMetadataUpdate_t4052853983;
struct SavedGameMetadataUpdate_t4052853983_marshaled_pinvoke;

extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_pinvoke(const SavedGameMetadataUpdate_t4052853983& unmarshaled, SavedGameMetadataUpdate_t4052853983_marshaled_pinvoke& marshaled);
extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_pinvoke_back(const SavedGameMetadataUpdate_t4052853983_marshaled_pinvoke& marshaled, SavedGameMetadataUpdate_t4052853983& unmarshaled);
extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_pinvoke_cleanup(SavedGameMetadataUpdate_t4052853983_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SavedGameMetadataUpdate_t4052853983;
struct SavedGameMetadataUpdate_t4052853983_marshaled_com;

extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_com(const SavedGameMetadataUpdate_t4052853983& unmarshaled, SavedGameMetadataUpdate_t4052853983_marshaled_com& marshaled);
extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_com_back(const SavedGameMetadataUpdate_t4052853983_marshaled_com& marshaled, SavedGameMetadataUpdate_t4052853983& unmarshaled);
extern "C" void SavedGameMetadataUpdate_t4052853983_marshal_com_cleanup(SavedGameMetadataUpdate_t4052853983_marshaled_com& marshaled);
