﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Collections.Generic.IEqualityComparer`1<System.Runtime.InteropServices.HandleRef>
struct IEqualityComparer_1_t2571853705;
// System.Collections.Generic.IDictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct IDictionary_2_t4274440406;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.HandleRef>
struct ICollection_1_t2675409288;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>[]
struct KeyValuePair_2U5BU5D_t1515759630;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>
struct IEnumerator_1_t2212245520;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Runtime.InteropServices.HandleRef,System.Object>
struct KeyCollection_t2028359216;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Runtime.InteropServices.HandleRef,System.Object>
struct ValueCollection_t3397172774;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1718923157.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m755477840_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m755477840(__this, method) ((  void (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2__ctor_m755477840_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m688241607_gshared (Dictionary_2_t401599765 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m688241607(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m688241607_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4171346504_gshared (Dictionary_2_t401599765 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4171346504(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4171346504_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m90958497_gshared (Dictionary_2_t401599765 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m90958497(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t401599765 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m90958497_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3786484405_gshared (Dictionary_2_t401599765 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3786484405(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3786484405_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1449719804_gshared (Dictionary_2_t401599765 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1449719804(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t401599765 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1449719804_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2912867985_gshared (Dictionary_2_t401599765 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2912867985(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t401599765 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2912867985_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3142171528_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3142171528(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3142171528_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m359317390_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m359317390(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m359317390_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3342128268_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3342128268(__this, method) ((  bool (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3342128268_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2222155822_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2222155822(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2222155822_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3055168989_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3055168989(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3055168989_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m4011072084_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m4011072084(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m4011072084_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3729632990_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3729632990(__this, ___key0, method) ((  bool (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3729632990_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m571867675_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m571867675(__this, ___key0, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m571867675_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3640106294_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3640106294(__this, method) ((  bool (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3640106294_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1952486312_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1952486312(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1952486312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m839288314_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m839288314(__this, method) ((  bool (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m839288314_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1307157809_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2_t300380471  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1307157809(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t401599765 *, KeyValuePair_2_t300380471 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1307157809_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2600725109_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2_t300380471  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2600725109(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t401599765 *, KeyValuePair_2_t300380471 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2600725109_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23333589_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2U5BU5D_t1515759630* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23333589(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t401599765 *, KeyValuePair_2U5BU5D_t1515759630*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m23333589_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3557042906_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2_t300380471  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3557042906(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t401599765 *, KeyValuePair_2_t300380471 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3557042906_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m584927732_gshared (Dictionary_2_t401599765 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m584927732(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m584927732_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2274237443_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2274237443(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2274237443_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m662169338_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m662169338(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m662169338_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m142429127_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m142429127(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m142429127_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m456871792_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m456871792(__this, method) ((  int32_t (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_get_Count_m456871792_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m636544791_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m636544791(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , const MethodInfo*))Dictionary_2_get_Item_m636544791_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2417645328_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2417645328(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2417645328_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2301232328_gshared (Dictionary_2_t401599765 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2301232328(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t401599765 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2301232328_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m733525999_gshared (Dictionary_2_t401599765 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m733525999(__this, ___size0, method) ((  void (*) (Dictionary_2_t401599765 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m733525999_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3523708331_gshared (Dictionary_2_t401599765 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3523708331(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3523708331_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t300380471  Dictionary_2_make_pair_m2720968255_gshared (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2720968255(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t300380471  (*) (Il2CppObject * /* static, unused */, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2720968255_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::pick_key(TKey,TValue)
extern "C"  HandleRef_t1780819301  Dictionary_2_pick_key_m397313695_gshared (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m397313695(__this /* static, unused */, ___key0, ___value1, method) ((  HandleRef_t1780819301  (*) (Il2CppObject * /* static, unused */, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m397313695_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3616672571_gshared (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3616672571(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3616672571_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1024889796_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2U5BU5D_t1515759630* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1024889796(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t401599765 *, KeyValuePair_2U5BU5D_t1515759630*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1024889796_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2002773224_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2002773224(__this, method) ((  void (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_Resize_m2002773224_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4014309733_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4014309733(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4014309733_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2456578427_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2456578427(__this, method) ((  void (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_Clear_m2456578427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3438615653_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3438615653(__this, ___key0, method) ((  bool (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , const MethodInfo*))Dictionary_2_ContainsKey_m3438615653_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3471947237_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3471947237(__this, ___value0, method) ((  bool (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3471947237_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3748461806_gshared (Dictionary_2_t401599765 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3748461806(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t401599765 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3748461806_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3321024054_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3321024054(__this, ___sender0, method) ((  void (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3321024054_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1791635819_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1791635819(__this, ___key0, method) ((  bool (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , const MethodInfo*))Dictionary_2_Remove_m1791635819_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m312819646_gshared (Dictionary_2_t401599765 * __this, HandleRef_t1780819301  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m312819646(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t401599765 *, HandleRef_t1780819301 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m312819646_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Keys()
extern "C"  KeyCollection_t2028359216 * Dictionary_2_get_Keys_m1403000477_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1403000477(__this, method) ((  KeyCollection_t2028359216 * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_get_Keys_m1403000477_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Values()
extern "C"  ValueCollection_t3397172774 * Dictionary_2_get_Values_m3699177849_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3699177849(__this, method) ((  ValueCollection_t3397172774 * (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_get_Values_m3699177849_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::ToTKey(System.Object)
extern "C"  HandleRef_t1780819301  Dictionary_2_ToTKey_m4142139898_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4142139898(__this, ___key0, method) ((  HandleRef_t1780819301  (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4142139898_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1928986710_gshared (Dictionary_2_t401599765 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1928986710(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t401599765 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1928986710_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2179630926_gshared (Dictionary_2_t401599765 * __this, KeyValuePair_2_t300380471  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2179630926(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t401599765 *, KeyValuePair_2_t300380471 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2179630926_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1718923157  Dictionary_2_GetEnumerator_m3549520859_gshared (Dictionary_2_t401599765 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3549520859(__this, method) ((  Enumerator_t1718923157  (*) (Dictionary_2_t401599765 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3549520859_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1321277842_gshared (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1321277842(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, HandleRef_t1780819301 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1321277842_gshared)(__this /* static, unused */, ___key0, ___value1, method)
