﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15
struct U3CU3Ec__DisplayClass15_t2936911564;
// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;

#include "codegen/il2cpp-codegen.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"

// System.Void YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15__ctor_m1360110451 (U3CU3Ec__DisplayClass15_t2936911564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15::<ParseDashManifest>b__14(YoutubeExtractor.VideoInfo)
extern "C"  bool U3CU3Ec__DisplayClass15_U3CParseDashManifestU3Eb__14_m3016585392 (U3CU3Ec__DisplayClass15_t2936911564 * __this, VideoInfo_t2099471191 * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
