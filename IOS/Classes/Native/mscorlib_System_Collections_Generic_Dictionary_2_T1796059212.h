﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate3389745971.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Collections.DictionaryEntry>
struct  Transform_1_t1796059212  : public MulticastDelegate_t3389745971
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
