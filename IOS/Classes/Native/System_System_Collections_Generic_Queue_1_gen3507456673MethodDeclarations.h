﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>
struct Queue_1_t3507456673;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>
struct IEnumerator_1_t3183079293;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// HutongGames.Extensions.TextureExtensions/Point[]
struct PointU5BU5D_t178283597;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato501574889.h"

// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::.ctor()
extern "C"  void Queue_1__ctor_m971296492_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1__ctor_m971296492(__this, method) ((  void (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1__ctor_m971296492_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m636367417_gshared (Queue_1_t3507456673 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m636367417(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3507456673 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m636367417_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m4057907577_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m4057907577(__this, method) ((  bool (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m4057907577_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m28712825_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m28712825(__this, method) ((  Il2CppObject * (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m28712825_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3755343101_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3755343101(__this, method) ((  Il2CppObject* (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3755343101_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m2167623304_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m2167623304(__this, method) ((  Il2CppObject * (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m2167623304_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m3079427236_gshared (Queue_1_t3507456673 * __this, PointU5BU5D_t178283597* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m3079427236(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3507456673 *, PointU5BU5D_t178283597*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3079427236_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Dequeue()
extern "C"  Point_t1271214244  Queue_1_Dequeue_m4064872392_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m4064872392(__this, method) ((  Point_t1271214244  (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_Dequeue_m4064872392_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Peek()
extern "C"  Point_t1271214244  Queue_1_Peek_m1963111059_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_Peek_m1963111059(__this, method) ((  Point_t1271214244  (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_Peek_m1963111059_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m1963818307_gshared (Queue_1_t3507456673 * __this, Point_t1271214244  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m1963818307(__this, ___item0, method) ((  void (*) (Queue_1_t3507456673 *, Point_t1271214244 , const MethodInfo*))Queue_1_Enqueue_m1963818307_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m2840452758_gshared (Queue_1_t3507456673 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m2840452758(__this, ___new_size0, method) ((  void (*) (Queue_1_t3507456673 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m2840452758_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m3502350558_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m3502350558(__this, method) ((  int32_t (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_get_Count_m3502350558_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::GetEnumerator()
extern "C"  Enumerator_t501574889  Queue_1_GetEnumerator_m2394345706_gshared (Queue_1_t3507456673 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m2394345706(__this, method) ((  Enumerator_t501574889  (*) (Queue_1_t3507456673 *, const MethodInfo*))Queue_1_GetEnumerator_m2394345706_gshared)(__this, method)
