﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAudioPitch
struct SetAudioPitch_t607907034;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::.ctor()
extern "C"  void SetAudioPitch__ctor_m2065119644 (SetAudioPitch_t607907034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::Reset()
extern "C"  void SetAudioPitch_Reset_m4006519881 (SetAudioPitch_t607907034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnEnter()
extern "C"  void SetAudioPitch_OnEnter_m3991783027 (SetAudioPitch_t607907034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnUpdate()
extern "C"  void SetAudioPitch_OnUpdate_m2619748912 (SetAudioPitch_t607907034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::DoSetAudioPitch()
extern "C"  void SetAudioPitch_DoSetAudioPitch_m3847671675 (SetAudioPitch_t607907034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
