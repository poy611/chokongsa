﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Type
struct Type_t;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>
struct Func_2_t2363589633;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerializatio1550301796.h"
#include "mscorlib_System_Object4170816371.h"

// System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataContractAttribute(System.Type)
extern "C"  DataContractAttribute_t2462274566 * JsonTypeReflector_GetDataContractAttribute_m2639370758 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataMemberAttribute(System.Reflection.MemberInfo)
extern "C"  DataMemberAttribute_t2601848894 * JsonTypeReflector_GetDataMemberAttribute_m3389429271 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type,System.Boolean)
extern "C"  int32_t JsonTypeReflector_GetObjectMemberSerialization_m2119310754 (Il2CppObject * __this /* static, unused */, Type_t * ___objectType0, bool ___ignoreSerializableAttribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Object)
extern "C"  JsonConverter_t2159686854 * JsonTypeReflector_GetJsonConverter_m1420401907 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::CreateJsonConverterInstance(System.Type,System.Object[])
extern "C"  JsonConverter_t2159686854 * JsonTypeReflector_CreateJsonConverterInstance_m2300933777 (Il2CppObject * __this /* static, unused */, Type_t * ___converterType0, ObjectU5BU5D_t1108656482* ___converterArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter> Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverterCreator(System.Type)
extern "C"  Func_2_t2363589633 * JsonTypeReflector_GetJsonConverterCreator_m1375844951 (Il2CppObject * __this /* static, unused */, Type_t * ___converterType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetTypeConverter(System.Type)
extern "C"  TypeConverter_t1753450284 * JsonTypeReflector_GetTypeConverter_m3128606986 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociatedMetadataType(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociatedMetadataType_m1989965351 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociateMetadataTypeFromAttribute(System.Type)
extern "C"  Type_t * JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_m3912836555 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_FullyTrusted()
extern "C"  bool JsonTypeReflector_get_FullyTrusted_m2931227035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern "C"  ReflectionDelegateFactory_t1590616920 * JsonTypeReflector_get_ReflectionDelegateFactory_m2738771862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern "C"  void JsonTypeReflector__cctor_m1929419096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
