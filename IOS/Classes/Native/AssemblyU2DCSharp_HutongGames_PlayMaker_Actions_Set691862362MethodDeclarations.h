﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetSkybox
struct SetSkybox_t691862362;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetSkybox::.ctor()
extern "C"  void SetSkybox__ctor_m2337025820 (SetSkybox_t691862362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::Reset()
extern "C"  void SetSkybox_Reset_m4278426057 (SetSkybox_t691862362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnEnter()
extern "C"  void SetSkybox_OnEnter_m3300613107 (SetSkybox_t691862362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnUpdate()
extern "C"  void SetSkybox_OnUpdate_m2668317872 (SetSkybox_t691862362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
