﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3377690443.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1014499487_gshared (InternalEnumerator_1_t3377690443 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1014499487(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3377690443 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1014499487_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993(__this, method) ((  void (*) (InternalEnumerator_1_t3377690443 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3641528993_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3377690443 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3497864471_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3983211830_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3983211830(__this, method) ((  void (*) (InternalEnumerator_1_t3377690443 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3983211830_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m813678161_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m813678161(__this, method) ((  bool (*) (InternalEnumerator_1_t3377690443 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m813678161_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t300380471  InternalEnumerator_1_get_Current_m2391040072_gshared (InternalEnumerator_1_t3377690443 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2391040072(__this, method) ((  KeyValuePair_2_t300380471  (*) (InternalEnumerator_1_t3377690443 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2391040072_gshared)(__this, method)
