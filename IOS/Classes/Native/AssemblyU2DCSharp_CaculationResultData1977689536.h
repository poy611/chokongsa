﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<CalculationData>
struct List_1_t3581626563;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaculationResultData
struct  CaculationResultData_t1977689536  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<CalculationData> CaculationResultData::resultData
	List_1_t3581626563 * ___resultData_0;

public:
	inline static int32_t get_offset_of_resultData_0() { return static_cast<int32_t>(offsetof(CaculationResultData_t1977689536, ___resultData_0)); }
	inline List_1_t3581626563 * get_resultData_0() const { return ___resultData_0; }
	inline List_1_t3581626563 ** get_address_of_resultData_0() { return &___resultData_0; }
	inline void set_resultData_0(List_1_t3581626563 * value)
	{
		___resultData_0 = value;
		Il2CppCodeGenWriteBarrier(&___resultData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
