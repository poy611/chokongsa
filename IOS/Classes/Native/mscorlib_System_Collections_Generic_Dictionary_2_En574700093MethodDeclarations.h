﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2639316804MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3373985543(__this, ___dictionary0, method) ((  void (*) (Enumerator_t574700093 *, Dictionary_2_t3552343997 *, const MethodInfo*))Enumerator__ctor_m2428567906_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1915014020(__this, method) ((  Il2CppObject * (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m176674121_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m590310222(__this, method) ((  void (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1882469907_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3558229189(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3346431242_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3966129504(__this, method) ((  Il2CppObject * (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1003108197_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2862898162(__this, method) ((  Il2CppObject * (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2962739383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::MoveNext()
#define Enumerator_MoveNext_m4114556798(__this, method) ((  bool (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_MoveNext_m2186011459_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::get_Current()
#define Enumerator_get_Current_m2956337406(__this, method) ((  KeyValuePair_2_t3451124703  (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_get_Current_m1442324889_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m253657607(__this, method) ((  Type_t * (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_get_CurrentKey_m590913164_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m704407815(__this, method) ((  int32_t (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_get_CurrentValue_m60728524_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::Reset()
#define Enumerator_Reset_m4110282457(__this, method) ((  void (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_Reset_m3794315444_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::VerifyState()
#define Enumerator_VerifyState_m2542729250(__this, method) ((  void (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_VerifyState_m520069565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2471289610(__this, method) ((  void (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_VerifyCurrent_m20550117_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Newtonsoft.Json.ReadType>::Dispose()
#define Enumerator_Dispose_m1134957929(__this, method) ((  void (*) (Enumerator_t574700093 *, const MethodInfo*))Enumerator_Dispose_m2433336452_gshared)(__this, method)
