﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass65_0
struct U3CU3Ec__DisplayClass65_0_t1365772830;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass65_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass65_0__ctor_m2746316578 (U3CU3Ec__DisplayClass65_0_t1365772830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass65_0::<SetIsSpecifiedActions>b__0(System.Object)
extern "C"  bool U3CU3Ec__DisplayClass65_0_U3CSetIsSpecifiedActionsU3Eb__0_m1172615547 (U3CU3Ec__DisplayClass65_0_t1365772830 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
