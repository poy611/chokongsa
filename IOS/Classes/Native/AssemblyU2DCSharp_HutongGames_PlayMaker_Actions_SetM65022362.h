﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterialTexture
struct  SetMaterialTexture_t65022362  : public ComponentAction_1_t2052155886
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterialTexture::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterialTexture::materialIndex
	FsmInt_t1596138449 * ___materialIndex_14;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterialTexture::material
	FsmMaterial_t924399665 * ___material_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetMaterialTexture::namedTexture
	FsmString_t952858651 * ___namedTexture_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetMaterialTexture::texture
	FsmTexture_t3073272573 * ___texture_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t65022362, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_materialIndex_14() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t65022362, ___materialIndex_14)); }
	inline FsmInt_t1596138449 * get_materialIndex_14() const { return ___materialIndex_14; }
	inline FsmInt_t1596138449 ** get_address_of_materialIndex_14() { return &___materialIndex_14; }
	inline void set_materialIndex_14(FsmInt_t1596138449 * value)
	{
		___materialIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___materialIndex_14, value);
	}

	inline static int32_t get_offset_of_material_15() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t65022362, ___material_15)); }
	inline FsmMaterial_t924399665 * get_material_15() const { return ___material_15; }
	inline FsmMaterial_t924399665 ** get_address_of_material_15() { return &___material_15; }
	inline void set_material_15(FsmMaterial_t924399665 * value)
	{
		___material_15 = value;
		Il2CppCodeGenWriteBarrier(&___material_15, value);
	}

	inline static int32_t get_offset_of_namedTexture_16() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t65022362, ___namedTexture_16)); }
	inline FsmString_t952858651 * get_namedTexture_16() const { return ___namedTexture_16; }
	inline FsmString_t952858651 ** get_address_of_namedTexture_16() { return &___namedTexture_16; }
	inline void set_namedTexture_16(FsmString_t952858651 * value)
	{
		___namedTexture_16 = value;
		Il2CppCodeGenWriteBarrier(&___namedTexture_16, value);
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t65022362, ___texture_17)); }
	inline FsmTexture_t3073272573 * get_texture_17() const { return ___texture_17; }
	inline FsmTexture_t3073272573 ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(FsmTexture_t3073272573 * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier(&___texture_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
