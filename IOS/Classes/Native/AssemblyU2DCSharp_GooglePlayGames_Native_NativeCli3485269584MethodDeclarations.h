﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m695785687_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m695785687(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1__ctor_m695785687_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey44`1<System.Object>::<>m__17(T)
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m3469203352_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m3469203352(__this, ___result0, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_t3485269584 *, Il2CppObject *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey44_1_U3CU3Em__17_m3469203352_gshared)(__this, ___result0, method)
