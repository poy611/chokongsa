﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupManager
struct PopupManager_t2711269761;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupManager::.ctor()
extern "C"  void PopupManager__ctor_m4087767610 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::Awake()
extern "C"  void PopupManager_Awake_m30405533 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::MakeThisTheOnlyGameManager()
extern "C"  void PopupManager_MakeThisTheOnlyGameManager_m804186804 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::ok()
extern "C"  void PopupManager_ok_m4012166342 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::cancle()
extern "C"  void PopupManager_cancle_m2383394230 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupManager::setText()
extern "C"  void PopupManager_setText_m992375463 (PopupManager_t2711269761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
