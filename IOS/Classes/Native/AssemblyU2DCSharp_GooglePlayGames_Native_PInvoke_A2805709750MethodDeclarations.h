﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
struct FetchAllResponse_t2805709750;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// GooglePlayGames.Native.PInvoke.NativeAchievement
struct NativeAchievement_t2621183934;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.NativeAchievement>
struct IEnumerator_1_t238081687;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C"  void FetchAllResponse__ctor_m2385131215 (FetchAllResponse_t2805709750 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FetchAllResponse_System_Collections_IEnumerable_GetEnumerator_m1093459372 (FetchAllResponse_t2805709750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Status()
extern "C"  int32_t FetchAllResponse_Status_m815322522 (FetchAllResponse_t2805709750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Length()
extern "C"  UIntPtr_t  FetchAllResponse_Length_m1060765677 (FetchAllResponse_t2805709750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetElement(System.UIntPtr)
extern "C"  NativeAchievement_t2621183934 * FetchAllResponse_GetElement_m1361611288 (FetchAllResponse_t2805709750 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.NativeAchievement> GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetEnumerator()
extern "C"  Il2CppObject* FetchAllResponse_GetEnumerator_m3023561884 (FetchAllResponse_t2805709750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchAllResponse_CallDispose_m3288460481 (FetchAllResponse_t2805709750 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C"  FetchAllResponse_t2805709750 * FetchAllResponse_FromPointer_m824102309 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::<GetEnumerator>m__99(System.UIntPtr)
extern "C"  NativeAchievement_t2621183934 * FetchAllResponse_U3CGetEnumeratorU3Em__99_m2223055743 (FetchAllResponse_t2805709750 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
