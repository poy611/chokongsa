﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<System.Object,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080  : public Il2CppObject
{
public:
	// T1 GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2::val1
	Il2CppObject * ___val1_0;
	// T2 GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2::val2
	Il2CppObject * ___val2_1;
	// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<T1,T2> GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2::<>f__ref$133
	U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * ___U3CU3Ef__refU24133_2;

public:
	inline static int32_t get_offset_of_val1_0() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080, ___val1_0)); }
	inline Il2CppObject * get_val1_0() const { return ___val1_0; }
	inline Il2CppObject ** get_address_of_val1_0() { return &___val1_0; }
	inline void set_val1_0(Il2CppObject * value)
	{
		___val1_0 = value;
		Il2CppCodeGenWriteBarrier(&___val1_0, value);
	}

	inline static int32_t get_offset_of_val2_1() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080, ___val2_1)); }
	inline Il2CppObject * get_val2_1() const { return ___val2_1; }
	inline Il2CppObject ** get_address_of_val2_1() { return &___val2_1; }
	inline void set_val2_1(Il2CppObject * value)
	{
		___val2_1 = value;
		Il2CppCodeGenWriteBarrier(&___val2_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24133_2() { return static_cast<int32_t>(offsetof(U3CToOnGameThreadU3Ec__AnonStorey86_2_t1779212080, ___U3CU3Ef__refU24133_2)); }
	inline U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * get_U3CU3Ef__refU24133_2() const { return ___U3CU3Ef__refU24133_2; }
	inline U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 ** get_address_of_U3CU3Ef__refU24133_2() { return &___U3CU3Ef__refU24133_2; }
	inline void set_U3CU3Ef__refU24133_2(U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * value)
	{
		___U3CU3Ef__refU24133_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24133_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
