﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVector2XY
struct GetVector2XY_t1767726594;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::.ctor()
extern "C"  void GetVector2XY__ctor_m3537760932 (GetVector2XY_t1767726594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::Reset()
extern "C"  void GetVector2XY_Reset_m1184193873 (GetVector2XY_t1767726594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnEnter()
extern "C"  void GetVector2XY_OnEnter_m1860853115 (GetVector2XY_t1767726594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnUpdate()
extern "C"  void GetVector2XY_OnUpdate_m985431080 (GetVector2XY_t1767726594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::DoGetVector2XYZ()
extern "C"  void GetVector2XY_DoGetVector2XYZ_m3636436535 (GetVector2XY_t1767726594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
