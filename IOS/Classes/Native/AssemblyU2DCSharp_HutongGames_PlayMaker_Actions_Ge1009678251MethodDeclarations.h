﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseButtonDown
struct GetMouseButtonDown_t1009678251;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::.ctor()
extern "C"  void GetMouseButtonDown__ctor_m2659595547 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::Reset()
extern "C"  void GetMouseButtonDown_Reset_m306028488 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnEnter()
extern "C"  void GetMouseButtonDown_OnEnter_m4052475442 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnUpdate()
extern "C"  void GetMouseButtonDown_OnUpdate_m206246481 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::DoGetMouseButtonDown()
extern "C"  void GetMouseButtonDown_DoGetMouseButtonDown_m3528595575 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
