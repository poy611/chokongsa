﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m12088604(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2571765214 *, Type_t *, FieldInfoU5BU5D_t2567562023*, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::get_Key()
#define KeyValuePair_2_get_Key_m3407941324(__this, method) ((  Type_t * (*) (KeyValuePair_2_t2571765214 *, const MethodInfo*))KeyValuePair_2_get_Key_m2940899609_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m74523533(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2571765214 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::get_Value()
#define KeyValuePair_2_get_Value_m3604286704(__this, method) ((  FieldInfoU5BU5D_t2567562023* (*) (KeyValuePair_2_t2571765214 *, const MethodInfo*))KeyValuePair_2_get_Value_m4250204908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1296427277(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2571765214 *, FieldInfoU5BU5D_t2567562023*, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Reflection.FieldInfo[]>::ToString()
#define KeyValuePair_2_ToString_m1085383003(__this, method) ((  String_t* (*) (KeyValuePair_2_t2571765214 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
