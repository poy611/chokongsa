﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs
struct NetworkViewRemoveRPCs_t613319411;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::.ctor()
extern "C"  void NetworkViewRemoveRPCs__ctor_m3104573347 (NetworkViewRemoveRPCs_t613319411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::Reset()
extern "C"  void NetworkViewRemoveRPCs_Reset_m751006288 (NetworkViewRemoveRPCs_t613319411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::OnEnter()
extern "C"  void NetworkViewRemoveRPCs_OnEnter_m2179411642 (NetworkViewRemoveRPCs_t613319411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::DoRemoveRPCsFromViewID()
extern "C"  void NetworkViewRemoveRPCs_DoRemoveRPCsFromViewID_m3153100520 (NetworkViewRemoveRPCs_t613319411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
