﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct GetAnimatorCurrentStateInfoIsTag_t2329710485;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsTag__ctor_m3945847409 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_Reset_m1592280350 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnEnter()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnEnter_m3189933576 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_OnActionUpdate_m2115496145 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::IsTag()
extern "C"  void GetAnimatorCurrentStateInfoIsTag_IsTag_m2566729119 (GetAnimatorCurrentStateInfoIsTag_t2329710485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
