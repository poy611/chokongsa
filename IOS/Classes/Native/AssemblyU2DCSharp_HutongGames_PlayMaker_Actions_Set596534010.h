﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetParent
struct  SetParent_t596534010  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetParent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetParent::parent
	FsmGameObject_t1697147867 * ___parent_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalPosition
	FsmBool_t1075959796 * ___resetLocalPosition_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalRotation
	FsmBool_t1075959796 * ___resetLocalRotation_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___parent_12)); }
	inline FsmGameObject_t1697147867 * get_parent_12() const { return ___parent_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(FsmGameObject_t1697147867 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier(&___parent_12, value);
	}

	inline static int32_t get_offset_of_resetLocalPosition_13() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___resetLocalPosition_13)); }
	inline FsmBool_t1075959796 * get_resetLocalPosition_13() const { return ___resetLocalPosition_13; }
	inline FsmBool_t1075959796 ** get_address_of_resetLocalPosition_13() { return &___resetLocalPosition_13; }
	inline void set_resetLocalPosition_13(FsmBool_t1075959796 * value)
	{
		___resetLocalPosition_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetLocalPosition_13, value);
	}

	inline static int32_t get_offset_of_resetLocalRotation_14() { return static_cast<int32_t>(offsetof(SetParent_t596534010, ___resetLocalRotation_14)); }
	inline FsmBool_t1075959796 * get_resetLocalRotation_14() const { return ___resetLocalRotation_14; }
	inline FsmBool_t1075959796 ** get_address_of_resetLocalRotation_14() { return &___resetLocalRotation_14; }
	inline void set_resetLocalRotation_14(FsmBool_t1075959796 * value)
	{
		___resetLocalRotation_14 = value;
		Il2CppCodeGenWriteBarrier(&___resetLocalRotation_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
