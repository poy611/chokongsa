﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GiveScore
struct GiveScore_t3448718817;

#include "codegen/il2cpp-codegen.h"

// System.Void GiveScore::.ctor()
extern "C"  void GiveScore__ctor_m7724202 (GiveScore_t3448718817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GiveScore::Start()
extern "C"  void GiveScore_Start_m3249829290 (GiveScore_t3448718817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GiveScore::giveScore(System.Int32)
extern "C"  void GiveScore_giveScore_m2856273914 (GiveScore_t3448718817 * __this, int32_t ___sc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GiveScore::popupActive()
extern "C"  void GiveScore_popupActive_m357850874 (GiveScore_t3448718817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GiveScore::backButton()
extern "C"  void GiveScore_backButton_m1591676211 (GiveScore_t3448718817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
