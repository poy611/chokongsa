﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>
struct DefaultComparer_t2767411944;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::.ctor()
extern "C"  void DefaultComparer__ctor_m2287487798_gshared (DefaultComparer_t2767411944 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2287487798(__this, method) ((  void (*) (DefaultComparer_t2767411944 *, const MethodInfo*))DefaultComparer__ctor_m2287487798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3203284149_gshared (DefaultComparer_t2767411944 * __this, uint32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3203284149(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2767411944 *, uint32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3203284149_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m217385863_gshared (DefaultComparer_t2767411944 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m217385863(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2767411944 *, uint32_t, uint32_t, const MethodInfo*))DefaultComparer_Equals_m217385863_gshared)(__this, ___x0, ___y1, method)
