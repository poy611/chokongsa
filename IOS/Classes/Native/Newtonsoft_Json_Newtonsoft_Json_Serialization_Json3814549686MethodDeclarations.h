﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3814549686;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t554063095;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.Serialization.JsonContainerContract
struct JsonContainerContract_t3227540113;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JsonStringContract
struct JsonStringContract_t4087007991;
// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t505348133;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// System.Array
struct Il2CppArray;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Runtime.Serialization.ISerializable
struct ISerializable_t867484142;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP554063095.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3227540113.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_String7231557.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4087007991.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonO505348133.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonA145179369.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonI624170136.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonD989352188.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalWriter__ctor_m3289379706 (JsonSerializerInternalWriter_t3814549686 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern "C"  void JsonSerializerInternalWriter_Serialize_m1845389304 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern "C"  JsonSerializerProxy_t3893567258 * JsonSerializerInternalWriter_GetInternalSerializer_m2545882106 (JsonSerializerInternalWriter_t3814549686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalWriter_GetContractSafe_m2756520574 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializePrimitive_m3324378778 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonPrimitiveContract_t554063095 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeValue_m3086581371 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonContract_t1328848902 * ___valueContract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ResolveIsReference(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t560925241  JsonSerializerInternalWriter_ResolveIsReference_m927277784 (JsonSerializerInternalWriter_t3814549686 * __this, JsonContract_t1328848902 * ___contract0, JsonProperty_t902655177 * ___property1, JsonContainerContract_t3227540113 * ___collectionContract2, JsonProperty_t902655177 * ___containerProperty3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteReference_m277841359 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___value0, JsonProperty_t902655177 * ___property1, JsonContract_t1328848902 * ___valueContract2, JsonContainerContract_t3227540113 * ___collectionContract3, JsonProperty_t902655177 * ___containerProperty4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteProperty(System.Object,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteProperty_m4220097984 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___memberValue0, JsonProperty_t902655177 * ___property1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_CheckForCircularReference_m530015449 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonProperty_t902655177 * ___property2, JsonContract_t1328848902 * ___contract3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_WriteReference_m4023339013 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetReference(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  String_t* JsonSerializerInternalWriter_GetReference_m1679016913 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern "C"  bool JsonSerializerInternalWriter_TryConvertToString_m531703410 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, String_t** ___s2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern "C"  void JsonSerializerInternalWriter_SerializeString_m1691835026 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonStringContract_t4087007991 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerializing(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern "C"  void JsonSerializerInternalWriter_OnSerializing_m2871776946 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerialized(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern "C"  void JsonSerializerInternalWriter_OnSerialized_m1924349401 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeObject_m33844258 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonObjectContract_t505348133 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CalculatePropertyValues(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract&,System.Object&)
extern "C"  bool JsonSerializerInternalWriter_CalculatePropertyValues_m2719633823 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonContainerContract_t3227540113 * ___contract2, JsonProperty_t902655177 * ___member3, JsonProperty_t902655177 * ___property4, JsonContract_t1328848902 ** ___memberContract5, Il2CppObject ** ___memberValue6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteObjectStart(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_WriteObjectStart_m2317912584 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReferenceIdProperty(Newtonsoft.Json.JsonWriter,System.Type,System.Object)
extern "C"  void JsonSerializerInternalWriter_WriteReferenceIdProperty_m2535982120 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Type_t * ___type1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern "C"  void JsonSerializerInternalWriter_WriteTypeProperty_m2928298040 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m1869668227 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m2077168259 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m1447652835 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeConvertable_m2313093624 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonConverter_t2159686854 * ___converter1, Il2CppObject * ___value2, JsonContract_t1328848902 * ___contract3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,System.Collections.IEnumerable,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeList_m3073154656 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___values1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m2696129800 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppArray * ___values1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.Int32,System.Int32[])
extern "C"  void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m2117425432 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppArray * ___values1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___member3, int32_t ___initialDepth4, Int32U5BU5D_t3230847821* ___indices5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteStartArray(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_WriteStartArray_m942549305 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___values1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___containerContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeISerializable(Newtonsoft.Json.JsonWriter,System.Runtime.Serialization.ISerializable,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeISerializable_m3925782039 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonISerializableContract_t624170136 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteType_m1179267105 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___typeNameHandlingFlag0, JsonContract_t1328848902 * ___contract1, JsonProperty_t902655177 * ___member2, JsonContainerContract_t3227540113 * ___containerContract3, JsonProperty_t902655177 * ___containerProperty4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,System.Collections.IDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  void JsonSerializerInternalWriter_SerializeDictionary_m4132390941 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___values1, JsonDictionaryContract_t989352188 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContainerContract_t3227540113 * ___collectionContract4, JsonProperty_t902655177 * ___containerProperty5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Boolean&)
extern "C"  String_t* JsonSerializerInternalWriter_GetPropertyName_m120991536 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___name1, JsonContract_t1328848902 * ___contract2, bool* ___escape3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void JsonSerializerInternalWriter_HandleError_m15457312 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, int32_t ___initialDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ShouldSerialize_m2571314090 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_IsSpecified_m4290078225 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
