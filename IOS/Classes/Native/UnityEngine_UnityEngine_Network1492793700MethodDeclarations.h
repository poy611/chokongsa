﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.NetworkPlayer[]
struct NetworkPlayerU5BU5D_t132745896;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792.h"

// UnityEngine.NetworkConnectionError UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Network_InitializeServer_m1086070119 (Il2CppObject * __this /* static, unused */, int32_t ___connections0, int32_t ___listenPort1, bool ___useNat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_incomingPassword(System.String)
extern "C"  void Network_set_incomingPassword_m515962091 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)
extern "C"  void Network_set_logLevel_m294590405 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::InitializeSecurity()
extern "C"  void Network_InitializeSecurity_m64123113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkConnectionError UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)
extern "C"  int32_t Network_Internal_ConnectToSingleIP_m3562152815 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, int32_t ___localPort2, String_t* ___password3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkConnectionError UnityEngine.Network::Connect(System.String,System.Int32,System.String)
extern "C"  int32_t Network_Connect_m3659141856 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, String_t* ___password2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Disconnect(System.Int32)
extern "C"  void Network_Disconnect_m2062052710 (Il2CppObject * __this /* static, unused */, int32_t ___timeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Disconnect()
extern "C"  void Network_Disconnect_m2068213653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)
extern "C"  void Network_CloseConnection_m1870717282 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___target0, bool ___sendDisconnectionNotification1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer[] UnityEngine.Network::get_connections()
extern "C"  NetworkPlayerU5BU5D_t132745896* Network_get_connections_m4276471278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::Internal_GetPlayer()
extern "C"  int32_t Network_Internal_GetPlayer_m776499950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer UnityEngine.Network::get_player()
extern "C"  NetworkPlayer_t3231273765  Network_get_player_m1200723560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Network::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  Object_t3071478659 * Network_Instantiate_m2753034863 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, int32_t ___group3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  Object_t3071478659 * Network_INTERNAL_CALL_Instantiate_m4087140154 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566 * ___position1, Quaternion_t1553702882 * ___rotation2, int32_t ___group3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)
extern "C"  void Network_DestroyPlayerObjects_m73641416 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID,System.UInt32)
extern "C"  void Network_Internal_RemoveRPCs_m3085275014 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436  ___viewID1, uint32_t ___channelMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)
extern "C"  void Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436 * ___viewID1, uint32_t ___channelMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkPlayer)
extern "C"  void Network_RemoveRPCs_m3795657583 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkViewID)
extern "C"  void Network_RemoveRPCs_m448463792 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isClient()
extern "C"  bool Network_get_isClient_m4143111185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isServer()
extern "C"  bool Network_get_isServer_m318839177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPeerType UnityEngine.Network::get_peerType()
extern "C"  int32_t Network_get_peerType_m115170590 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::SetLevelPrefix(System.Int32)
extern "C"  void Network_SetLevelPrefix_m3013656638 (Il2CppObject * __this /* static, unused */, int32_t ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetLastPing_m3340324755 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetAveragePing_m3083558248 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Network::get_sendRate()
extern "C"  float Network_get_sendRate_m1227422814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_sendRate(System.Single)
extern "C"  void Network_set_sendRate_m2111190029 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isMessageQueueRunning()
extern "C"  bool Network_get_isMessageQueueRunning_m5081841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)
extern "C"  void Network_set_isMessageQueueRunning_m1610613326 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Internal_GetTime(System.Double&)
extern "C"  void Network_Internal_GetTime_m2753432228 (Il2CppObject * __this /* static, unused */, double* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Network::get_time()
extern "C"  double Network_get_time_m4227697516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::get_minimumAllocatableViewIDs()
extern "C"  int32_t Network_get_minimumAllocatableViewIDs_m2986052927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)
extern "C"  void Network_set_minimumAllocatableViewIDs_m2130893980 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::HavePublicAddress()
extern "C"  bool Network_HavePublicAddress_m1494895090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::get_maxConnections()
extern "C"  int32_t Network_get_maxConnections_m3468981299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_maxConnections(System.Int32)
extern "C"  void Network_set_maxConnections_m2445464568 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
