﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Grinding/<confirm>c__Iterator1A
struct U3CconfirmU3Ec__Iterator1A_t3696604900;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::.ctor()
extern "C"  void U3CconfirmU3Ec__Iterator1A__ctor_m4009319479 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Grinding/<confirm>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CconfirmU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2074749573 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Grinding/<confirm>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CconfirmU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m2260981785 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Grinding/<confirm>c__Iterator1A::MoveNext()
extern "C"  bool U3CconfirmU3Ec__Iterator1A_MoveNext_m2322144325 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::Dispose()
extern "C"  void U3CconfirmU3Ec__Iterator1A_Dispose_m268637876 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::Reset()
extern "C"  void U3CconfirmU3Ec__Iterator1A_Reset_m1655752420 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
