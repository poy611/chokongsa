﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Lists`1<System.Object>
struct  Lists_1_t4287567192  : public Il2CppObject
{
public:

public:
};

struct Lists_1_t4287567192_StaticFields
{
public:
	// System.Collections.Generic.List`1<T> HutongGames.Utility.Lists`1::Empty
	List_1_t1244034627 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Lists_1_t4287567192_StaticFields, ___Empty_0)); }
	inline List_1_t1244034627 * get_Empty_0() const { return ___Empty_0; }
	inline List_1_t1244034627 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(List_1_t1244034627 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
