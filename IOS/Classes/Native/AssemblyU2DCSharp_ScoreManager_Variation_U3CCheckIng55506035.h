﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// ScoreManager_Variation
struct ScoreManager_Variation_t1665153935;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Variation/<CheckIngredient>c__Iterator25
struct  U3CCheckIngredientU3Ec__Iterator25_t55506035  : public Il2CppObject
{
public:
	// System.Int32 ScoreManager_Variation/<CheckIngredient>c__Iterator25::$PC
	int32_t ___U24PC_0;
	// System.Object ScoreManager_Variation/<CheckIngredient>c__Iterator25::$current
	Il2CppObject * ___U24current_1;
	// ScoreManager_Variation ScoreManager_Variation/<CheckIngredient>c__Iterator25::<>f__this
	ScoreManager_Variation_t1665153935 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CCheckIngredientU3Ec__Iterator25_t55506035, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckIngredientU3Ec__Iterator25_t55506035, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CCheckIngredientU3Ec__Iterator25_t55506035, ___U3CU3Ef__this_2)); }
	inline ScoreManager_Variation_t1665153935 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline ScoreManager_Variation_t1665153935 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(ScoreManager_Variation_t1665153935 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
