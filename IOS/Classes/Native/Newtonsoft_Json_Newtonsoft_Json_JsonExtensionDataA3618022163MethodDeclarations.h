﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonExtensionDataAttribute
struct JsonExtensionDataAttribute_t3618022163;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_WriteData()
extern "C"  bool JsonExtensionDataAttribute_get_WriteData_m1859643498 (JsonExtensionDataAttribute_t3618022163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_ReadData()
extern "C"  bool JsonExtensionDataAttribute_get_ReadData_m190097889 (JsonExtensionDataAttribute_t3618022163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
