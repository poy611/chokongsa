﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3167042548.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2623608376.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3359671455.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_617688751.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4004312584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3760597130.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2038752746.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3687755450.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3135543351.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3188160136.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3787353090.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1453856114.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2604642324.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1910642754.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest2979531601.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3090555431.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3353735195.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat604475832.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe3559239239.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2435538904.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1676616792.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220377.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220410.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220447.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220505.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220348.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220352.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3988332409.h"
#include "System_U3CModuleU3E86524790.h"
#include "System_Locale2281372282.h"
#include "System_System_MonoTODOAttribute2091695241.h"
#include "System_System_MonoNotSupportedAttribute89696052.h"
#include "System_System_Collections_Specialized_HybridDictio3032146074.h"
#include "System_System_Collections_Specialized_ListDictiona2682732540.h"
#include "System_System_Collections_Specialized_ListDictiona2044266038.h"
#include "System_System_Collections_Specialized_ListDictiona3271093914.h"
#include "System_System_Collections_Specialized_ListDictiona2450441588.h"
#include "System_System_Collections_Specialized_ListDictiona3666873205.h"
#include "System_System_Collections_Specialized_NameObjectCo1023199937.h"
#include "System_System_Collections_Specialized_NameObjectCo1625138937.h"
#include "System_System_Collections_Specialized_NameObjectCo4030006590.h"
#include "System_System_Collections_Specialized_NameObjectCo1246329035.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "System_System_ComponentModel_Design_Serialization_1276167776.h"
#include "System_System_ComponentModel_ArrayConverter1568537747.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayP1292166168.h"
#include "System_System_ComponentModel_AttributeCollection100867136.h"
#include "System_System_ComponentModel_BaseNumberConverter3737780012.h"
#include "System_System_ComponentModel_BooleanConverter2934406884.h"
#include "System_System_ComponentModel_BrowsableAttribute686446803.h"
#include "System_System_ComponentModel_ByteConverter4141866270.h"
#include "System_System_ComponentModel_CharConverter3176686224.h"
#include "System_System_ComponentModel_CollectionConverter1327433352.h"
#include "System_System_ComponentModel_Component332074787.h"
#include "System_System_ComponentModel_ComponentCollection659278177.h"
#include "System_System_ComponentModel_ComponentConverter3558277359.h"
#include "System_System_ComponentModel_CultureInfoConverter1170086032.h"
#include "System_System_ComponentModel_CultureInfoConverter_1565406081.h"
#include "System_System_ComponentModel_DateTimeConverter211680619.h"
#include "System_System_ComponentModel_DecimalConverter3800356315.h"
#include "System_System_ComponentModel_DefaultEventAttribute2032492649.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib2593178002.h"
#include "System_System_ComponentModel_DefaultValueAttribute3756983154.h"
#include "System_System_ComponentModel_DesignerAttribute1894265111.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1635082169.h"
#include "System_System_ComponentModel_DesignerSerialization1524395549.h"
#include "System_System_ComponentModel_DesignerSerialization2366110769.h"
#include "System_System_ComponentModel_DoubleConverter719751477.h"
#include "System_System_ComponentModel_EditorAttribute1755091989.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1665593056.h"
#include "System_System_ComponentModel_EditorBrowsableState1963658837.h"
#include "System_System_ComponentModel_EnumConverter2767655237.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom1375181057.h"
#include "System_System_ComponentModel_EventDescriptor1405012495.h"
#include "System_System_ComponentModel_EventDescriptorCollec3224620365.h"
#include "System_System_ComponentModel_ListEntry483724442.h"
#include "System_System_ComponentModel_EventHandlerList1056591002.h"
#include "System_System_ComponentModel_ExpandableObjectConver207908787.h"
#include "System_System_ComponentModel_GuidConverter646662333.h"
#include "System_System_ComponentModel_Int16Converter3886635768.h"
#include "System_System_ComponentModel_Int32Converter1078787070.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1000 = { sizeof (ClientCertificateType_t3167042548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1000[6] = 
{
	ClientCertificateType_t3167042548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1001 = { sizeof (HandshakeMessage_t2623608376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1001[4] = 
{
	HandshakeMessage_t2623608376::get_offset_of_context_5(),
	HandshakeMessage_t2623608376::get_offset_of_handshakeType_6(),
	HandshakeMessage_t2623608376::get_offset_of_contentType_7(),
	HandshakeMessage_t2623608376::get_offset_of_cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1002 = { sizeof (HandshakeType_t3359671455)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1002[12] = 
{
	HandshakeType_t3359671455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1003 = { sizeof (TlsClientCertificate_t617688751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1003[2] = 
{
	TlsClientCertificate_t617688751::get_offset_of_clientCertSelected_9(),
	TlsClientCertificate_t617688751::get_offset_of_clientCert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1004 = { sizeof (TlsClientCertificateVerify_t4004312584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1005 = { sizeof (TlsClientFinished_t3760597130), -1, sizeof(TlsClientFinished_t3760597130_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1005[1] = 
{
	TlsClientFinished_t3760597130_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1006 = { sizeof (TlsClientHello_t2038752746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1006[1] = 
{
	TlsClientHello_t2038752746::get_offset_of_random_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1007 = { sizeof (TlsClientKeyExchange_t3687755450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1008 = { sizeof (TlsServerCertificate_t3135543351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1008[1] = 
{
	TlsServerCertificate_t3135543351::get_offset_of_certificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1009 = { sizeof (TlsServerCertificateRequest_t3188160136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1009[2] = 
{
	TlsServerCertificateRequest_t3188160136::get_offset_of_certificateTypes_9(),
	TlsServerCertificateRequest_t3188160136::get_offset_of_distinguisedNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1010 = { sizeof (TlsServerFinished_t3787353090), -1, sizeof(TlsServerFinished_t3787353090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1010[1] = 
{
	TlsServerFinished_t3787353090_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1011 = { sizeof (TlsServerHello_t1453856114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1011[4] = 
{
	TlsServerHello_t1453856114::get_offset_of_compressionMethod_9(),
	TlsServerHello_t1453856114::get_offset_of_random_10(),
	TlsServerHello_t1453856114::get_offset_of_sessionId_11(),
	TlsServerHello_t1453856114::get_offset_of_cipherSuite_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1012 = { sizeof (TlsServerHelloDone_t2604642324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1013 = { sizeof (TlsServerKeyExchange_t1910642754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1013[2] = 
{
	TlsServerKeyExchange_t1910642754::get_offset_of_rsaParams_9(),
	TlsServerKeyExchange_t1910642754::get_offset_of_signedParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1014 = { sizeof (PrimalityTest_t2979531602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1015 = { sizeof (CertificateValidationCallback_t3090555431), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1016 = { sizeof (CertificateValidationCallback2_t3353735195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1017 = { sizeof (CertificateSelectionCallback_t604475832), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1018 = { sizeof (PrivateKeySelectionCallback_t3559239239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1019 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238934), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1019[15] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1020 = { sizeof (U24ArrayTypeU243132_t435538905)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t435538905_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1021 = { sizeof (U24ArrayTypeU24256_t1676616793)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616793_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1022 = { sizeof (U24ArrayTypeU2420_t3379220378)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220378_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1023 = { sizeof (U24ArrayTypeU2432_t3379220411)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3379220411_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1024 = { sizeof (U24ArrayTypeU2448_t3379220448)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t3379220448_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1025 = { sizeof (U24ArrayTypeU2464_t3379220506)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t3379220506_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1026 = { sizeof (U24ArrayTypeU2412_t3379220349)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220349_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1027 = { sizeof (U24ArrayTypeU2416_t3379220353)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220353_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1028 = { sizeof (U24ArrayTypeU244_t3988332409)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t3988332409_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1029 = { sizeof (U3CModuleU3E_t86524792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1030 = { sizeof (Locale_t2281372284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1031 = { sizeof (MonoTODOAttribute_t2091695242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1031[1] = 
{
	MonoTODOAttribute_t2091695242::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1032 = { sizeof (MonoNotSupportedAttribute_t89696052), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1033 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1033[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1034 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1034[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1035[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1036[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1037 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1037[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1038 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1038[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1039 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1039[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1040 = { sizeof (HybridDictionary_t3032146074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1040[3] = 
{
	HybridDictionary_t3032146074::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t3032146074::get_offset_of_hashtable_1(),
	HybridDictionary_t3032146074::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1041 = { sizeof (ListDictionary_t2682732540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1041[4] = 
{
	ListDictionary_t2682732540::get_offset_of_count_0(),
	ListDictionary_t2682732540::get_offset_of_version_1(),
	ListDictionary_t2682732540::get_offset_of_head_2(),
	ListDictionary_t2682732540::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1042 = { sizeof (DictionaryNode_t2044266038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1042[3] = 
{
	DictionaryNode_t2044266038::get_offset_of_key_0(),
	DictionaryNode_t2044266038::get_offset_of_value_1(),
	DictionaryNode_t2044266038::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1043 = { sizeof (DictionaryNodeEnumerator_t3271093914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1043[4] = 
{
	DictionaryNodeEnumerator_t3271093914::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1044 = { sizeof (DictionaryNodeCollection_t2450441588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1044[2] = 
{
	DictionaryNodeCollection_t2450441588::get_offset_of_dict_0(),
	DictionaryNodeCollection_t2450441588::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1045 = { sizeof (DictionaryNodeCollectionEnumerator_t3666873205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1045[2] = 
{
	DictionaryNodeCollectionEnumerator_t3666873205::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t3666873205::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1046 = { sizeof (NameObjectCollectionBase_t1023199937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1046[10] = 
{
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t1023199937::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t1023199937::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t1023199937::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1047 = { sizeof (_Item_t1625138937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1047[2] = 
{
	_Item_t1625138937::get_offset_of_key_0(),
	_Item_t1625138937::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1048 = { sizeof (_KeysEnumerator_t4030006590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1048[2] = 
{
	_KeysEnumerator_t4030006590::get_offset_of_m_collection_0(),
	_KeysEnumerator_t4030006590::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1049 = { sizeof (KeysCollection_t1246329035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1049[1] = 
{
	KeysCollection_t1246329035::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1050 = { sizeof (NameValueCollection_t2791941106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1050[2] = 
{
	NameValueCollection_t2791941106::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t2791941106::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1051 = { sizeof (InstanceDescriptor_t1276167776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1051[3] = 
{
	InstanceDescriptor_t1276167776::get_offset_of_member_0(),
	InstanceDescriptor_t1276167776::get_offset_of_arguments_1(),
	InstanceDescriptor_t1276167776::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1052 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1056 = { sizeof (ArrayConverter_t1568537747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1057 = { sizeof (ArrayPropertyDescriptor_t1292166168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1057[2] = 
{
	ArrayPropertyDescriptor_t1292166168::get_offset_of_index_4(),
	ArrayPropertyDescriptor_t1292166168::get_offset_of_array_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1058 = { sizeof (AttributeCollection_t100867136), -1, sizeof(AttributeCollection_t100867136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1058[2] = 
{
	AttributeCollection_t100867136::get_offset_of_attrList_0(),
	AttributeCollection_t100867136_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1059 = { sizeof (BaseNumberConverter_t3737780012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1059[1] = 
{
	BaseNumberConverter_t3737780012::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1060 = { sizeof (BooleanConverter_t2934406884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1061 = { sizeof (BrowsableAttribute_t686446803), -1, sizeof(BrowsableAttribute_t686446803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1061[4] = 
{
	BrowsableAttribute_t686446803::get_offset_of_browsable_0(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t686446803_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1062 = { sizeof (ByteConverter_t4141866270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1063 = { sizeof (CharConverter_t3176686224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1064 = { sizeof (CollectionConverter_t1327433352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1065 = { sizeof (Component_t332074787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1065[3] = 
{
	Component_t332074787::get_offset_of_event_handlers_1(),
	Component_t332074787::get_offset_of_mySite_2(),
	Component_t332074787::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1066 = { sizeof (ComponentCollection_t659278177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1067 = { sizeof (ComponentConverter_t3558277359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1068 = { sizeof (CultureInfoConverter_t1170086032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1068[1] = 
{
	CultureInfoConverter_t1170086032::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1069 = { sizeof (CultureInfoComparer_t1565406081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1070 = { sizeof (DateTimeConverter_t211680619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1071 = { sizeof (DecimalConverter_t3800356315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1072 = { sizeof (DefaultEventAttribute_t2032492649), -1, sizeof(DefaultEventAttribute_t2032492649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1072[2] = 
{
	DefaultEventAttribute_t2032492649::get_offset_of_eventName_0(),
	DefaultEventAttribute_t2032492649_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1073 = { sizeof (DefaultPropertyAttribute_t2593178002), -1, sizeof(DefaultPropertyAttribute_t2593178002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1073[2] = 
{
	DefaultPropertyAttribute_t2593178002::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t2593178002_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1074 = { sizeof (DefaultValueAttribute_t3756983154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1074[1] = 
{
	DefaultValueAttribute_t3756983154::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1075 = { sizeof (DesignerAttribute_t1894265111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1075[2] = 
{
	DesignerAttribute_t1894265111::get_offset_of_name_0(),
	DesignerAttribute_t1894265111::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1076 = { sizeof (DesignerCategoryAttribute_t1635082169), -1, sizeof(DesignerCategoryAttribute_t1635082169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1076[5] = 
{
	DesignerCategoryAttribute_t1635082169::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1635082169_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1077 = { sizeof (DesignerSerializationVisibility_t1524395549)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1077[4] = 
{
	DesignerSerializationVisibility_t1524395549::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1078 = { sizeof (DesignerSerializationVisibilityAttribute_t2366110769), -1, sizeof(DesignerSerializationVisibilityAttribute_t2366110769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1078[5] = 
{
	DesignerSerializationVisibilityAttribute_t2366110769::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2366110769_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1079 = { sizeof (DoubleConverter_t719751477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1080 = { sizeof (EditorAttribute_t1755091989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1080[2] = 
{
	EditorAttribute_t1755091989::get_offset_of_name_0(),
	EditorAttribute_t1755091989::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1081 = { sizeof (EditorBrowsableAttribute_t1665593056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1081[1] = 
{
	EditorBrowsableAttribute_t1665593056::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1082 = { sizeof (EditorBrowsableState_t1963658837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1082[4] = 
{
	EditorBrowsableState_t1963658837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1083 = { sizeof (EnumConverter_t2767655237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1083[2] = 
{
	EnumConverter_t2767655237::get_offset_of_type_0(),
	EnumConverter_t2767655237::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1084 = { sizeof (EnumComparer_t1375181057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1085 = { sizeof (EventDescriptor_t1405012495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1086 = { sizeof (EventDescriptorCollection_t3224620365), -1, sizeof(EventDescriptorCollection_t3224620365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1086[3] = 
{
	EventDescriptorCollection_t3224620365::get_offset_of_eventList_0(),
	EventDescriptorCollection_t3224620365::get_offset_of_isReadOnly_1(),
	EventDescriptorCollection_t3224620365_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1087 = { sizeof (ListEntry_t483724442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1087[3] = 
{
	ListEntry_t483724442::get_offset_of_key_0(),
	ListEntry_t483724442::get_offset_of_value_1(),
	ListEntry_t483724442::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1088 = { sizeof (EventHandlerList_t1056591002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1088[2] = 
{
	EventHandlerList_t1056591002::get_offset_of_entries_0(),
	EventHandlerList_t1056591002::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1089 = { sizeof (ExpandableObjectConverter_t207908787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1090 = { sizeof (GuidConverter_t646662333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1093 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1094 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1095 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1096 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1098 = { sizeof (Int16Converter_t3886635768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1099 = { sizeof (Int32Converter_t1078787070), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
