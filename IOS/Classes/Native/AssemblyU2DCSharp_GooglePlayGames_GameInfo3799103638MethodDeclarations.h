﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean GooglePlayGames.GameInfo::RequireGooglePlus()
extern "C"  bool GameInfo_RequireGooglePlus_m3689889667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.GameInfo::ApplicationIdInitialized()
extern "C"  bool GameInfo_ApplicationIdInitialized_m1337603328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.GameInfo::IosClientIdInitialized()
extern "C"  bool GameInfo_IosClientIdInitialized_m2332006552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.GameInfo::WebClientIdInitialized()
extern "C"  bool GameInfo_WebClientIdInitialized_m2118387633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.GameInfo::NearbyConnectionsInitialized()
extern "C"  bool GameInfo_NearbyConnectionsInitialized_m141246741 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.GameInfo::ToEscapedToken(System.String)
extern "C"  String_t* GameInfo_ToEscapedToken_m3538393771 (Il2CppObject * __this /* static, unused */, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
