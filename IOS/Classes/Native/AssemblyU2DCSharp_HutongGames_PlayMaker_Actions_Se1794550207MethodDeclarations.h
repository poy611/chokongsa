﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmVector2
struct SetFsmVector2_t1794550207;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::.ctor()
extern "C"  void SetFsmVector2__ctor_m2590125655 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::Reset()
extern "C"  void SetFsmVector2_Reset_m236558596 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnEnter()
extern "C"  void SetFsmVector2_OnEnter_m1716418670 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::DoSetFsmVector2()
extern "C"  void SetFsmVector2_DoSetFsmVector2_m814640667 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnUpdate()
extern "C"  void SetFsmVector2_OnUpdate_m802930581 (SetFsmVector2_t1794550207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
