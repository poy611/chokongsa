﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// UnityEngine.AnimationState
struct AnimationState_t3682323633;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_AnimationState3682323633.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"
#include "UnityEngine_UnityEngine_AnimationClip2007702890.h"

// UnityEngine.Texture2D HutongGames.PlayMaker.ActionHelpers::get_WhiteTexture()
extern "C"  Texture2D_t3884108195 * ActionHelpers_get_WhiteTexture_m2707937660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsVisible(UnityEngine.GameObject)
extern "C"  bool ActionHelpers_IsVisible_m98613596 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::RuntimeError(HutongGames.PlayMaker.FsmStateAction,System.String)
extern "C"  void ActionHelpers_RuntimeError_m586693559 (Il2CppObject * __this /* static, unused */, FsmStateAction_t2366529033 * ___action0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.ActionHelpers::GetGameObjectFsm(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t3799847376 * ActionHelpers_GetGameObjectFsm_m1076592826 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionHelpers::GetRandomWeightedIndex(HutongGames.PlayMaker.FsmFloat[])
extern "C"  int32_t ActionHelpers_GetRandomWeightedIndex_m2288184804 (Il2CppObject * __this /* static, unused */, FsmFloatU5BU5D_t2945380875* ___weights0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::HasAnimationFinished(UnityEngine.AnimationState,System.Single,System.Single)
extern "C"  bool ActionHelpers_HasAnimationFinished_m2541906088 (Il2CppObject * __this /* static, unused */, AnimationState_t3682323633 * ___anim0, float ___prevTime1, float ___currentTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.ActionHelpers::GetPosition(HutongGames.PlayMaker.FsmGameObject,HutongGames.PlayMaker.FsmVector3)
extern "C"  Vector3_t4282066566  ActionHelpers_GetPosition_m819515482 (Il2CppObject * __this /* static, unused */, FsmGameObject_t1697147867 * ___fsmGameObject0, FsmVector3_t533912882 * ___fsmVector31, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsMouseOver(UnityEngine.GameObject,System.Single,System.Int32)
extern "C"  bool ActionHelpers_IsMouseOver_m3587800431 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___gameObject0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit HutongGames.PlayMaker.ActionHelpers::MousePick(System.Single,System.Int32)
extern "C"  RaycastHit_t4003175726  ActionHelpers_MousePick_m4005175030 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.ActionHelpers::MouseOver(System.Single,System.Int32)
extern "C"  GameObject_t3674682005 * ActionHelpers_MouseOver_m945106274 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::DoMousePick(System.Single,System.Int32)
extern "C"  void ActionHelpers_DoMousePick_m834255851 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionHelpers::LayerArrayToLayerMask(HutongGames.PlayMaker.FsmInt[],System.Boolean)
extern "C"  int32_t ActionHelpers_LayerArrayToLayerMask_m3090395306 (Il2CppObject * __this /* static, unused */, FsmIntU5BU5D_t1976821196* ___layers0, bool ___invert1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsLoopingWrapMode(UnityEngine.WrapMode)
extern "C"  bool ActionHelpers_IsLoopingWrapMode_m2744633945 (Il2CppObject * __this /* static, unused */, int32_t ___wrapMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckRayDistance(System.Single)
extern "C"  String_t* ActionHelpers_CheckRayDistance_m621902585 (Il2CppObject * __this /* static, unused */, float ___rayDistance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckForValidEvent(HutongGames.PlayMaker.FsmState,System.String)
extern "C"  String_t* ActionHelpers_CheckForValidEvent_m38008441 (Il2CppObject * __this /* static, unused */, FsmState_t2146334067 * ___state0, String_t* ___eventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckPhysicsSetup(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  String_t* ActionHelpers_CheckPhysicsSetup_m2734257387 (Il2CppObject * __this /* static, unused */, FsmOwnerDefault_t251897112 * ___ownerDefault0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckOwnerPhysicsSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckOwnerPhysicsSetup_m1952038644 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckOwnerPhysics2dSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckOwnerPhysics2dSetup_m2028987970 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckPhysicsSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckPhysicsSetup_m3036050301 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::DebugLog(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.LogLevel,System.String,System.Boolean)
extern "C"  void ActionHelpers_DebugLog_m3793723254 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, int32_t ___logLevel1, String_t* ___text2, bool ___sendToUnityLog3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::LogError(System.String)
extern "C"  void ActionHelpers_LogError_m3511771520 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::LogWarning(System.String)
extern "C"  void ActionHelpers_LogWarning_m2742193068 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::GetValueLabel(HutongGames.PlayMaker.INamedVariable)
extern "C"  String_t* ActionHelpers_GetValueLabel_m1486453096 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::GetValueLabel(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  String_t* ActionHelpers_GetValueLabel_m2696850792 (Il2CppObject * __this /* static, unused */, Fsm_t1527112426 * ___fsm0, FsmOwnerDefault_t251897112 * ___ownerDefault1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::AddAnimationClip(UnityEngine.GameObject,UnityEngine.AnimationClip)
extern "C"  void ActionHelpers_AddAnimationClip_m3103586584 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, AnimationClip_t2007702890 * ___animClip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
