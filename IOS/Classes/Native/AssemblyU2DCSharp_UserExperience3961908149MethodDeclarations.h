﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserExperience
struct UserExperience_t3961908149;

#include "codegen/il2cpp-codegen.h"

// System.Void UserExperience::.ctor()
extern "C"  void UserExperience__ctor_m1985297542 (UserExperience_t3961908149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
