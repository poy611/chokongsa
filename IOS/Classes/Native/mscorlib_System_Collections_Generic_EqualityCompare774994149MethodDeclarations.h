﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct DefaultComparer_t774994149;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor()
extern "C"  void DefaultComparer__ctor_m4049981832_gshared (DefaultComparer_t774994149 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4049981832(__this, method) ((  void (*) (DefaultComparer_t774994149 *, const MethodInfo*))DefaultComparer__ctor_m4049981832_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1510484259_gshared (DefaultComparer_t774994149 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1510484259(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t774994149 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1510484259_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3062922841_gshared (DefaultComparer_t774994149 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3062922841(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t774994149 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3062922841_gshared)(__this, ___x0, ___y1, method)
