﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey8E
struct U3CAcceptInvitationU3Ec__AnonStorey8E_t798450215;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3411188537.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey8E::.ctor()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey8E__ctor_m618667812 (U3CAcceptInvitationU3Ec__AnonStorey8E_t798450215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey8E::<>m__7D(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey8E_U3CU3Em__7D_m1107737482 (U3CAcceptInvitationU3Ec__AnonStorey8E_t798450215 * __this, MultiplayerInvitation_t3411188537 * ___invitation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptInvitation>c__AnonStorey8E::<>m__8C(GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch)
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey8E_U3CU3Em__8C_m1893582413 (U3CAcceptInvitationU3Ec__AnonStorey8E_t798450215 * __this, int32_t ___status0, TurnBasedMatch_t3573041681 * ___match1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
