﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGameVolume
struct  SetGameVolume_t4076511612  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGameVolume::volume
	FsmFloat_t2134102846 * ___volume_11;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGameVolume::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_volume_11() { return static_cast<int32_t>(offsetof(SetGameVolume_t4076511612, ___volume_11)); }
	inline FsmFloat_t2134102846 * get_volume_11() const { return ___volume_11; }
	inline FsmFloat_t2134102846 ** get_address_of_volume_11() { return &___volume_11; }
	inline void set_volume_11(FsmFloat_t2134102846 * value)
	{
		___volume_11 = value;
		Il2CppCodeGenWriteBarrier(&___volume_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(SetGameVolume_t4076511612, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
