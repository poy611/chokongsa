﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;

#include "mscorlib_System_Array1146569071.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2462274566.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2601848894.h"

#pragma once
// System.Runtime.Serialization.DataContractAttribute[]
struct DataContractAttributeU5BU5D_t3726683427  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataContractAttribute_t2462274566 * m_Items[1];

public:
	inline DataContractAttribute_t2462274566 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataContractAttribute_t2462274566 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataContractAttribute_t2462274566 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Runtime.Serialization.DataMemberAttribute[]
struct DataMemberAttributeU5BU5D_t2748850443  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DataMemberAttribute_t2601848894 * m_Items[1];

public:
	inline DataMemberAttribute_t2601848894 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataMemberAttribute_t2601848894 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataMemberAttribute_t2601848894 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
