﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// NetworkMng
struct NetworkMng_t1515215352;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng/<WaitForRequest>c__Iterator11
struct  U3CWaitForRequestU3Ec__Iterator11_t2724973971  : public Il2CppObject
{
public:
	// UnityEngine.WWW NetworkMng/<WaitForRequest>c__Iterator11::www
	WWW_t3134621005 * ___www_0;
	// System.Int32 NetworkMng/<WaitForRequest>c__Iterator11::$PC
	int32_t ___U24PC_1;
	// System.Object NetworkMng/<WaitForRequest>c__Iterator11::$current
	Il2CppObject * ___U24current_2;
	// UnityEngine.WWW NetworkMng/<WaitForRequest>c__Iterator11::<$>www
	WWW_t3134621005 * ___U3CU24U3Ewww_3;
	// NetworkMng NetworkMng/<WaitForRequest>c__Iterator11::<>f__this
	NetworkMng_t1515215352 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CWaitForRequestU3Ec__Iterator11_t2724973971, ___www_0)); }
	inline WWW_t3134621005 * get_www_0() const { return ___www_0; }
	inline WWW_t3134621005 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t3134621005 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier(&___www_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CWaitForRequestU3Ec__Iterator11_t2724973971, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForRequestU3Ec__Iterator11_t2724973971, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ewww_3() { return static_cast<int32_t>(offsetof(U3CWaitForRequestU3Ec__Iterator11_t2724973971, ___U3CU24U3Ewww_3)); }
	inline WWW_t3134621005 * get_U3CU24U3Ewww_3() const { return ___U3CU24U3Ewww_3; }
	inline WWW_t3134621005 ** get_address_of_U3CU24U3Ewww_3() { return &___U3CU24U3Ewww_3; }
	inline void set_U3CU24U3Ewww_3(WWW_t3134621005 * value)
	{
		___U3CU24U3Ewww_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ewww_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CWaitForRequestU3Ec__Iterator11_t2724973971, ___U3CU3Ef__this_4)); }
	inline NetworkMng_t1515215352 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline NetworkMng_t1515215352 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(NetworkMng_t1515215352 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
