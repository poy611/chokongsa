﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchGUIEvent
struct TouchGUIEvent_t3069136972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::.ctor()
extern "C"  void TouchGUIEvent__ctor_m1899468394 (TouchGUIEvent_t3069136972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::Reset()
extern "C"  void TouchGUIEvent_Reset_m3840868631 (TouchGUIEvent_t3069136972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::OnEnter()
extern "C"  void TouchGUIEvent_OnEnter_m3714721729 (TouchGUIEvent_t3069136972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::OnUpdate()
extern "C"  void TouchGUIEvent_OnUpdate_m2620783266 (TouchGUIEvent_t3069136972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouchGUIEvent()
extern "C"  void TouchGUIEvent_DoTouchGUIEvent_m1730433467 (TouchGUIEvent_t3069136972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouch(UnityEngine.Touch)
extern "C"  void TouchGUIEvent_DoTouch_m2560437006 (TouchGUIEvent_t3069136972 * __this, Touch_t4210255029  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouchOffset(UnityEngine.Vector3)
extern "C"  void TouchGUIEvent_DoTouchOffset_m165147018 (TouchGUIEvent_t3069136972 * __this, Vector3_t4282066566  ___touchPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
