﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PluginVersion
struct PluginVersion_t3642060999;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.PluginVersion::.ctor()
extern "C"  void PluginVersion__ctor_m2576876192 (PluginVersion_t3642060999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
