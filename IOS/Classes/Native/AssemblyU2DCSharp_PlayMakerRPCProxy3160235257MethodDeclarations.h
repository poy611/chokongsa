﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerRPCProxy
struct PlayMakerRPCProxy_t3160235257;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void PlayMakerRPCProxy::.ctor()
extern "C"  void PlayMakerRPCProxy__ctor_m1949096082 (PlayMakerRPCProxy_t3160235257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerRPCProxy::Reset()
extern "C"  void PlayMakerRPCProxy_Reset_m3890496319 (PlayMakerRPCProxy_t3160235257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerRPCProxy::ForwardEvent(System.String)
extern "C"  void PlayMakerRPCProxy_ForwardEvent_m3281717083 (PlayMakerRPCProxy_t3160235257 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
