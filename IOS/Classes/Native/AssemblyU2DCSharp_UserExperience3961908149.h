﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserExperience
struct  UserExperience_t3961908149  : public MonoBehaviour_t667441552
{
public:
	// System.Single UserExperience::gameValue
	float ___gameValue_2;
	// System.Single UserExperience::playTime
	float ___playTime_3;
	// System.Single UserExperience::missCount
	float ___missCount_4;

public:
	inline static int32_t get_offset_of_gameValue_2() { return static_cast<int32_t>(offsetof(UserExperience_t3961908149, ___gameValue_2)); }
	inline float get_gameValue_2() const { return ___gameValue_2; }
	inline float* get_address_of_gameValue_2() { return &___gameValue_2; }
	inline void set_gameValue_2(float value)
	{
		___gameValue_2 = value;
	}

	inline static int32_t get_offset_of_playTime_3() { return static_cast<int32_t>(offsetof(UserExperience_t3961908149, ___playTime_3)); }
	inline float get_playTime_3() const { return ___playTime_3; }
	inline float* get_address_of_playTime_3() { return &___playTime_3; }
	inline void set_playTime_3(float value)
	{
		___playTime_3 = value;
	}

	inline static int32_t get_offset_of_missCount_4() { return static_cast<int32_t>(offsetof(UserExperience_t3961908149, ___missCount_4)); }
	inline float get_missCount_4() const { return ___missCount_4; }
	inline float* get_address_of_missCount_4() { return &___missCount_4; }
	inline void set_missCount_4(float value)
	{
		___missCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
