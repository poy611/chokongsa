﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector2XY
struct SetVector2XY_t3316719222;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::.ctor()
extern "C"  void SetVector2XY__ctor_m1779004592 (SetVector2XY_t3316719222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::Reset()
extern "C"  void SetVector2XY_Reset_m3720404829 (SetVector2XY_t3316719222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::OnEnter()
extern "C"  void SetVector2XY_OnEnter_m3913124999 (SetVector2XY_t3316719222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::OnUpdate()
extern "C"  void SetVector2XY_OnUpdate_m181350044 (SetVector2XY_t3316719222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::DoSetVector2XYZ()
extern "C"  void SetVector2XY_DoSetVector2XYZ_m166853711 (SetVector2XY_t3316719222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
