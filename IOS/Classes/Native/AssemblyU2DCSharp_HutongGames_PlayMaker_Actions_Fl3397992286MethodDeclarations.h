﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatSubtract
struct FloatSubtract_t3397992286;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::.ctor()
extern "C"  void FloatSubtract__ctor_m394558360 (FloatSubtract_t3397992286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::Reset()
extern "C"  void FloatSubtract_Reset_m2335958597 (FloatSubtract_t3397992286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnEnter()
extern "C"  void FloatSubtract_OnEnter_m605190511 (FloatSubtract_t3397992286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnUpdate()
extern "C"  void FloatSubtract_OnUpdate_m714596020 (FloatSubtract_t3397992286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::DoFloatSubtract()
extern "C"  void FloatSubtract_DoFloatSubtract_m2286520315 (FloatSubtract_t3397992286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
