﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>
struct Func_2_t3045461041;
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Func_2_t547493001;
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
struct Func_2_t3868603641;

#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRea2566974603.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState
struct  ActiveState_t2038966146  : public MessagingEnabledState_t2566974603
{
public:

public:
};

struct ActiveState_t2038966146_StaticFields
{
public:
	// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache0
	Func_2_t3045461041 * ___U3CU3Ef__amU24cache0_8;
	// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache1
	Func_2_t547493001 * ___U3CU3Ef__amU24cache1_9;
	// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache2
	Func_2_t3868603641 * ___U3CU3Ef__amU24cache2_10;
	// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<>f__am$cache3
	Func_2_t3868603641 * ___U3CU3Ef__amU24cache3_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(ActiveState_t2038966146_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Func_2_t3045461041 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Func_2_t3045461041 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Func_2_t3045461041 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_9() { return static_cast<int32_t>(offsetof(ActiveState_t2038966146_StaticFields, ___U3CU3Ef__amU24cache1_9)); }
	inline Func_2_t547493001 * get_U3CU3Ef__amU24cache1_9() const { return ___U3CU3Ef__amU24cache1_9; }
	inline Func_2_t547493001 ** get_address_of_U3CU3Ef__amU24cache1_9() { return &___U3CU3Ef__amU24cache1_9; }
	inline void set_U3CU3Ef__amU24cache1_9(Func_2_t547493001 * value)
	{
		___U3CU3Ef__amU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_10() { return static_cast<int32_t>(offsetof(ActiveState_t2038966146_StaticFields, ___U3CU3Ef__amU24cache2_10)); }
	inline Func_2_t3868603641 * get_U3CU3Ef__amU24cache2_10() const { return ___U3CU3Ef__amU24cache2_10; }
	inline Func_2_t3868603641 ** get_address_of_U3CU3Ef__amU24cache2_10() { return &___U3CU3Ef__amU24cache2_10; }
	inline void set_U3CU3Ef__amU24cache2_10(Func_2_t3868603641 * value)
	{
		___U3CU3Ef__amU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_11() { return static_cast<int32_t>(offsetof(ActiveState_t2038966146_StaticFields, ___U3CU3Ef__amU24cache3_11)); }
	inline Func_2_t3868603641 * get_U3CU3Ef__amU24cache3_11() const { return ___U3CU3Ef__amU24cache3_11; }
	inline Func_2_t3868603641 ** get_address_of_U3CU3Ef__amU24cache3_11() { return &___U3CU3Ef__amU24cache3_11; }
	inline void set_U3CU3Ef__amU24cache3_11(Func_2_t3868603641 * value)
	{
		___U3CU3Ef__amU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
