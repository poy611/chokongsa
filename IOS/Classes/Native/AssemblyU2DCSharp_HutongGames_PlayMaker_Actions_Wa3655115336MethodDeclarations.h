﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d
struct WakeAllRigidBodies2d_t3655115336;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::.ctor()
extern "C"  void WakeAllRigidBodies2d__ctor_m3357181214 (WakeAllRigidBodies2d_t3655115336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::Reset()
extern "C"  void WakeAllRigidBodies2d_Reset_m1003614155 (WakeAllRigidBodies2d_t3655115336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::OnEnter()
extern "C"  void WakeAllRigidBodies2d_OnEnter_m122435957 (WakeAllRigidBodies2d_t3655115336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::OnUpdate()
extern "C"  void WakeAllRigidBodies2d_OnUpdate_m2929074030 (WakeAllRigidBodies2d_t3655115336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::DoWakeAll()
extern "C"  void WakeAllRigidBodies2d_DoWakeAll_m995053198 (WakeAllRigidBodies2d_t3655115336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
