﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateQuickMatch>c__AnonStorey87
struct U3CCreateQuickMatchU3Ec__AnonStorey87_t871309132;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateQuickMatch>c__AnonStorey87::.ctor()
extern "C"  void U3CCreateQuickMatchU3Ec__AnonStorey87__ctor_m2575981471 (U3CCreateQuickMatchU3Ec__AnonStorey87_t871309132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateQuickMatch>c__AnonStorey87::<>m__76(GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch)
extern "C"  void U3CCreateQuickMatchU3Ec__AnonStorey87_U3CU3Em__76_m1617697054 (U3CCreateQuickMatchU3Ec__AnonStorey87_t871309132 * __this, int32_t ___status0, TurnBasedMatch_t3573041681 * ___match1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
