﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserInfoType
struct UserInfoType_t3869313555;

#include "codegen/il2cpp-codegen.h"

// System.Void UserInfoType::.ctor()
extern "C"  void UserInfoType__ctor_m2250608104 (UserInfoType_t3869313555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
