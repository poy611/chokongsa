﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen4239861309MethodDeclarations.h"

// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m1275520152(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2130612649 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m756414230_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2029591682(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2130612649 *, int32_t, String_t*, MultiplayerInvitation_t3411188537 *, const MethodInfo*))Action_3_Invoke_m3126836050_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m1138893467(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2130612649 *, int32_t, String_t*, MultiplayerInvitation_t3411188537 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m107478785_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1167006648(__this, ___result0, method) ((  void (*) (Action_3_t2130612649 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3590087718_gshared)(__this, ___result0, method)
