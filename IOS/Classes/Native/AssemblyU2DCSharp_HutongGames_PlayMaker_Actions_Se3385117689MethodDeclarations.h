﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector3XYZ
struct SetVector3XYZ_t3385117689;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::.ctor()
extern "C"  void SetVector3XYZ__ctor_m486403421 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::Reset()
extern "C"  void SetVector3XYZ_Reset_m2427803658 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnEnter()
extern "C"  void SetVector3XYZ_OnEnter_m2968948212 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnUpdate()
extern "C"  void SetVector3XYZ_OnUpdate_m976640719 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::DoSetVector3XYZ()
extern "C"  void SetVector3XYZ_DoSetVector3XYZ_m1878137179 (SetVector3XYZ_t3385117689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
