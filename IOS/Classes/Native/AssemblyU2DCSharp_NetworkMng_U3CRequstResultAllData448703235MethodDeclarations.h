﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15
struct U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::.ctor()
extern "C"  void U3CRequstResultAllDataCouroutineU3Ec__Iterator15__ctor_m3542534088 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRequstResultAllDataCouroutineU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3138234250 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRequstResultAllDataCouroutineU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m3523477278 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::MoveNext()
extern "C"  bool U3CRequstResultAllDataCouroutineU3Ec__Iterator15_MoveNext_m1266695468 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::Dispose()
extern "C"  void U3CRequstResultAllDataCouroutineU3Ec__Iterator15_Dispose_m2659443205 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::Reset()
extern "C"  void U3CRequstResultAllDataCouroutineU3Ec__Iterator15_Reset_m1188967029 (U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
