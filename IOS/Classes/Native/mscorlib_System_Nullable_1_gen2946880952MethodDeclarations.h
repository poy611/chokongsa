﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2946880952.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<System.Guid>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3666514454_gshared (Nullable_1_t2946880952 * __this, Guid_t2862754429  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3666514454(__this, ___value0, method) ((  void (*) (Nullable_1_t2946880952 *, Guid_t2862754429 , const MethodInfo*))Nullable_1__ctor_m3666514454_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Guid>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2549092585_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2549092585(__this, method) ((  bool (*) (Nullable_1_t2946880952 *, const MethodInfo*))Nullable_1_get_HasValue_m2549092585_gshared)(__this, method)
// T System.Nullable`1<System.Guid>::get_Value()
extern "C"  Guid_t2862754429  Nullable_1_get_Value_m4071257400_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4071257400(__this, method) ((  Guid_t2862754429  (*) (Nullable_1_t2946880952 *, const MethodInfo*))Nullable_1_get_Value_m4071257400_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3339505760_gshared (Nullable_1_t2946880952 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3339505760(__this, ___other0, method) ((  bool (*) (Nullable_1_t2946880952 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3339505760_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.Guid>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2950403807_gshared (Nullable_1_t2946880952 * __this, Nullable_1_t2946880952  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2950403807(__this, ___other0, method) ((  bool (*) (Nullable_1_t2946880952 *, Nullable_1_t2946880952 , const MethodInfo*))Nullable_1_Equals_m2950403807_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.Guid>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3813908292_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3813908292(__this, method) ((  int32_t (*) (Nullable_1_t2946880952 *, const MethodInfo*))Nullable_1_GetHashCode_m3813908292_gshared)(__this, method)
// T System.Nullable`1<System.Guid>::GetValueOrDefault()
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m2265611842_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2265611842(__this, method) ((  Guid_t2862754429  (*) (Nullable_1_t2946880952 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2265611842_gshared)(__this, method)
// T System.Nullable`1<System.Guid>::GetValueOrDefault(T)
extern "C"  Guid_t2862754429  Nullable_1_GetValueOrDefault_m432857643_gshared (Nullable_1_t2946880952 * __this, Guid_t2862754429  ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m432857643(__this, ___defaultValue0, method) ((  Guid_t2862754429  (*) (Nullable_1_t2946880952 *, Guid_t2862754429 , const MethodInfo*))Nullable_1_GetValueOrDefault_m432857643_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<System.Guid>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2800437186_gshared (Nullable_1_t2946880952 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2800437186(__this, method) ((  String_t* (*) (Nullable_1_t2946880952 *, const MethodInfo*))Nullable_1_ToString_m2800437186_gshared)(__this, method)
