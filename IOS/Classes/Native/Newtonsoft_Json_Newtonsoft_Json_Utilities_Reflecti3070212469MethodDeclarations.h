﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ReflectionMember
struct ReflectionMember_t3070212469;
// System.Type
struct Type_t;
// System.Func`2<System.Object,System.Object>
struct Func_2_t184564025;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"

// System.Type Newtonsoft.Json.Utilities.ReflectionMember::get_MemberType()
extern "C"  Type_t * ReflectionMember_get_MemberType_m16115635 (ReflectionMember_t3070212469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_MemberType(System.Type)
extern "C"  void ReflectionMember_set_MemberType_m81271628 (ReflectionMember_t3070212469 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::get_Getter()
extern "C"  Func_2_t184564025 * ReflectionMember_get_Getter_m2341228654 (ReflectionMember_t3070212469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Getter(System.Func`2<System.Object,System.Object>)
extern "C"  void ReflectionMember_set_Getter_m2796257647 (ReflectionMember_t3070212469 * __this, Func_2_t184564025 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Setter(System.Action`2<System.Object,System.Object>)
extern "C"  void ReflectionMember_set_Setter_m3230499541 (ReflectionMember_t3070212469 * __this, Action_2_t4293064463 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionMember::.ctor()
extern "C"  void ReflectionMember__ctor_m101247272 (ReflectionMember_t3070212469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
