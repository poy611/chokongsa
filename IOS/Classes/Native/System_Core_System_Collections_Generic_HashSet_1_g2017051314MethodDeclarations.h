﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Char>
struct HashSet_1_t2017051314;
// System.Collections.Generic.IEqualityComparer`1<System.Char>
struct IEqualityComparer_1_t3653656942;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t1868568199;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t479520291;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2404294980.h"

// System.Void System.Collections.Generic.HashSet`1<System.Char>::.ctor()
extern "C"  void HashSet_1__ctor_m1766459980_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m1766459980(__this, method) ((  void (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1__ctor_m1766459980_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m564608434_gshared (HashSet_1_t2017051314 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m564608434(__this, ___comparer0, method) ((  void (*) (HashSet_1_t2017051314 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m564608434_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m3894665899_gshared (HashSet_1_t2017051314 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m3894665899(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t2017051314 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3894665899_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m1825456525_gshared (HashSet_1_t2017051314 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m1825456525(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2017051314 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))HashSet_1__ctor_m1825456525_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Char>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3434748502_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3434748502(__this, method) ((  Il2CppObject* (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3434748502_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Char>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1394520217_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1394520217(__this, method) ((  bool (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1394520217_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m4117250787_gshared (HashSet_1_t2017051314 * __this, CharU5BU5D_t3324145743* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m4117250787(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2017051314 *, CharU5BU5D_t3324145743*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m4117250787_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4006494643_gshared (HashSet_1_t2017051314 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4006494643(__this, ___item0, method) ((  void (*) (HashSet_1_t2017051314 *, Il2CppChar, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4006494643_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1587467701_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1587467701(__this, method) ((  Il2CppObject * (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1587467701_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Char>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m594258366_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m594258366(__this, method) ((  int32_t (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_get_Count_m594258366_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m3137001041_gshared (HashSet_1_t2017051314 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m3137001041(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t2017051314 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m3137001041_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m2828300915_gshared (HashSet_1_t2017051314 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m2828300915(__this, ___size0, method) ((  void (*) (HashSet_1_t2017051314 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2828300915_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Char>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m2812904025_gshared (HashSet_1_t2017051314 * __this, int32_t ___index0, int32_t ___hash1, Il2CppChar ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2812904025(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t2017051314 *, int32_t, int32_t, Il2CppChar, const MethodInfo*))HashSet_1_SlotsContainsAt_m2812904025_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m874364387_gshared (HashSet_1_t2017051314 * __this, CharU5BU5D_t3324145743* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m874364387(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2017051314 *, CharU5BU5D_t3324145743*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m874364387_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m397035476_gshared (HashSet_1_t2017051314 * __this, CharU5BU5D_t3324145743* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m397035476(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t2017051314 *, CharU5BU5D_t3324145743*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m397035476_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::Resize()
extern "C"  void HashSet_1_Resize_m3278448492_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m3278448492(__this, method) ((  void (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_Resize_m3278448492_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Char>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m4034549204_gshared (HashSet_1_t2017051314 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m4034549204(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t2017051314 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m4034549204_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Char>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m3222279170_gshared (HashSet_1_t2017051314 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m3222279170(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t2017051314 *, Il2CppChar, const MethodInfo*))HashSet_1_GetItemHashCode_m3222279170_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Char>::Add(T)
extern "C"  bool HashSet_1_Add_m3658162335_gshared (HashSet_1_t2017051314 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define HashSet_1_Add_m3658162335(__this, ___item0, method) ((  bool (*) (HashSet_1_t2017051314 *, Il2CppChar, const MethodInfo*))HashSet_1_Add_m3658162335_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::Clear()
extern "C"  void HashSet_1_Clear_m3467560567_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m3467560567(__this, method) ((  void (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_Clear_m3467560567_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Char>::Contains(T)
extern "C"  bool HashSet_1_Contains_m3437002619_gshared (HashSet_1_t2017051314 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m3437002619(__this, ___item0, method) ((  bool (*) (HashSet_1_t2017051314 *, Il2CppChar, const MethodInfo*))HashSet_1_Contains_m3437002619_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Char>::Remove(T)
extern "C"  bool HashSet_1_Remove_m2358305334_gshared (HashSet_1_t2017051314 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m2358305334(__this, ___item0, method) ((  bool (*) (HashSet_1_t2017051314 *, Il2CppChar, const MethodInfo*))HashSet_1_Remove_m2358305334_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m1223666666_gshared (HashSet_1_t2017051314 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m1223666666(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2017051314 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))HashSet_1_GetObjectData_m1223666666_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Char>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m1673381050_gshared (HashSet_1_t2017051314 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m1673381050(__this, ___sender0, method) ((  void (*) (HashSet_1_t2017051314 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1673381050_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Char>::GetEnumerator()
extern "C"  Enumerator_t2404294980  HashSet_1_GetEnumerator_m2657854232_gshared (HashSet_1_t2017051314 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m2657854232(__this, method) ((  Enumerator_t2404294980  (*) (HashSet_1_t2017051314 *, const MethodInfo*))HashSet_1_GetEnumerator_m2657854232_gshared)(__this, method)
