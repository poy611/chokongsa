﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1255729854MethodDeclarations.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m447704493(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t657955164 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4251406634_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(T1,T2)
#define Action_2_Invoke_m793108894(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t657955164 *, int32_t, TurnBasedMatch_t3573041681 *, const MethodInfo*))Action_2_Invoke_m2069375169_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2378568557(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t657955164 *, int32_t, TurnBasedMatch_t3573041681 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1417546976_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m559464269(__this, ___result0, method) ((  void (*) (Action_2_t657955164 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3583371322_gshared)(__this, ___result0, method)
