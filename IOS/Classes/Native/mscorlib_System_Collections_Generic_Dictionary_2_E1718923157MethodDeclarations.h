﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1718923157.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2838867140_gshared (Enumerator_t1718923157 * __this, Dictionary_2_t401599765 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2838867140(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1718923157 *, Dictionary_2_t401599765 *, const MethodInfo*))Enumerator__ctor_m2838867140_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2596440103_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2596440103(__this, method) ((  Il2CppObject * (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2596440103_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4146917745_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4146917745(__this, method) ((  void (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4146917745_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1595280872_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1595280872(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1595280872_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m761187267_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m761187267(__this, method) ((  Il2CppObject * (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m761187267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2404959637_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2404959637(__this, method) ((  Il2CppObject * (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2404959637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m626881441_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m626881441(__this, method) ((  bool (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_MoveNext_m626881441_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t300380471  Enumerator_get_Current_m2672115835_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2672115835(__this, method) ((  KeyValuePair_2_t300380471  (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_get_Current_m2672115835_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_CurrentKey()
extern "C"  HandleRef_t1780819301  Enumerator_get_CurrentKey_m3344151658_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3344151658(__this, method) ((  HandleRef_t1780819301  (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_get_CurrentKey_m3344151658_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1179426602_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1179426602(__this, method) ((  Il2CppObject * (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_get_CurrentValue_m1179426602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2660327702_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2660327702(__this, method) ((  void (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_Reset_m2660327702_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2820737183_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2820737183(__this, method) ((  void (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_VerifyState_m2820737183_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3348940871_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3348940871(__this, method) ((  void (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_VerifyCurrent_m3348940871_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3592809574_gshared (Enumerator_t1718923157 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3592809574(__this, method) ((  void (*) (Enumerator_t1718923157 *, const MethodInfo*))Enumerator_Dispose_m3592809574_gshared)(__this, method)
