﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.NativeScoreSummary
struct NativeScoreSummary_t3571466807;
// GooglePlayGames.Native.PInvoke.NativeScore
struct NativeScore_t2497620897;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeScoreSummary::.ctor(System.IntPtr)
extern "C"  void NativeScoreSummary__ctor_m3804128727 (NativeScoreSummary_t3571466807 * __this, IntPtr_t ___selfPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeScoreSummary::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void NativeScoreSummary_CallDispose_m267239097 (NativeScoreSummary_t3571466807 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.PInvoke.NativeScoreSummary::ApproximateResults()
extern "C"  uint64_t NativeScoreSummary_ApproximateResults_m7356461 (NativeScoreSummary_t3571466807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeScore GooglePlayGames.Native.PInvoke.NativeScoreSummary::LocalUserScore()
extern "C"  NativeScore_t2497620897 * NativeScoreSummary_LocalUserScore_m3679625278 (NativeScoreSummary_t3571466807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeScoreSummary GooglePlayGames.Native.PInvoke.NativeScoreSummary::FromPointer(System.IntPtr)
extern "C"  NativeScoreSummary_t3571466807 * NativeScoreSummary_FromPointer_m1363397633 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
