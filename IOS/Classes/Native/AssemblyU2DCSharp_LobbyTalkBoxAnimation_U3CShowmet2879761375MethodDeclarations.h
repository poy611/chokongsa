﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10
struct U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::.ctor()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10__ctor_m4093069932 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2995934694 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3070813562 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::MoveNext()
extern "C"  bool U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_MoveNext_m3612066312 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::Dispose()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_Dispose_m3443411881 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LobbyTalkBoxAnimation/<ShowmetheCoffeSizeUpCo>c__Iterator10::Reset()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_Reset_m1739502873 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator10_t2879761375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
