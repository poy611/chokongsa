﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// HutongGames.PlayMaker.Actions.GetFsmBool
struct GetFsmBool_t3246450044;
// HutongGames.PlayMaker.Actions.GetFsmColor
struct GetFsmColor_t1207664135;
// HutongGames.PlayMaker.Actions.GetFsmEnum
struct GetFsmEnum_t3246538643;
// HutongGames.PlayMaker.Actions.GetFsmFloat
struct GetFsmFloat_t1210347776;
// HutongGames.PlayMaker.Actions.GetFsmGameObject
struct GetFsmGameObject_t4125302563;
// HutongGames.PlayMaker.Actions.GetFsmInt
struct GetFsmInt_t541307091;
// HutongGames.PlayMaker.Actions.GetFsmMaterial
struct GetFsmMaterial_t3631711289;
// HutongGames.PlayMaker.Actions.GetFsmObject
struct GetFsmObject_t2760364049;
// HutongGames.PlayMaker.Actions.GetFsmQuaternion
struct GetFsmQuaternion_t2004323440;
// HutongGames.PlayMaker.Actions.GetFsmRect
struct GetFsmRect_t3246916726;
// HutongGames.PlayMaker.Actions.GetFsmState
struct GetFsmState_t1222578997;
// HutongGames.PlayMaker.Actions.GetFsmString
struct GetFsmString_t2891746531;
// HutongGames.PlayMaker.Actions.GetFsmTexture
struct GetFsmTexture_t3559778687;
// HutongGames.PlayMaker.Actions.GetFsmVariable
struct GetFsmVariable_t2083058062;
// HutongGames.PlayMaker.Actions.GetFsmVariables
struct GetFsmVariables_t3796297627;
// HutongGames.PlayMaker.Actions.GetFsmVector2
struct GetFsmVector2_t1020418995;
// HutongGames.PlayMaker.Actions.GetFsmVector3
struct GetFsmVector3_t1020418996;
// HutongGames.PlayMaker.Actions.GetIPhoneSettings
struct GetIPhoneSettings_t3918982924;
// HutongGames.PlayMaker.Actions.GetJointBreak2dInfo
struct GetJointBreak2dInfo_t936465433;
// HutongGames.PlayMaker.Actions.GetJointBreakInfo
struct GetJointBreakInfo_t130542119;
// HutongGames.PlayMaker.Actions.GetKey
struct GetKey_t2986509073;
// HutongGames.PlayMaker.Actions.GetKeyDown
struct GetKeyDown_t2999335123;
// HutongGames.PlayMaker.Actions.GetKeyUp
struct GetKeyUp_t1705176140;
// HutongGames.PlayMaker.Actions.GetLastEvent
struct GetLastEvent_t1262666006;
// HutongGames.PlayMaker.Actions.GetLayer
struct GetLayer_t1705980995;
// HutongGames.PlayMaker.Actions.GetLocationInfo
struct GetLocationInfo_t1970767239;
// HutongGames.PlayMaker.Actions.GetMainCamera
struct GetMainCamera_t2582722562;
// HutongGames.PlayMaker.Actions.GetMass
struct GetMass_t1738563448;
// HutongGames.PlayMaker.Actions.GetMass2d
struct GetMass2d_t725305002;
// HutongGames.PlayMaker.Actions.GetMaterial
struct GetMaterial_t1959127339;
// HutongGames.PlayMaker.Actions.GetMaterialTexture
struct GetMaterialTexture_t3439956006;
// HutongGames.PlayMaker.Actions.GetMouseButton
struct GetMouseButton_t2789993961;
// HutongGames.PlayMaker.Actions.GetMouseButtonDown
struct GetMouseButtonDown_t1009678251;
// HutongGames.PlayMaker.Actions.GetMouseButtonUp
struct GetMouseButtonUp_t1832714532;
// HutongGames.PlayMaker.Actions.GetMouseX
struct GetMouseX_t738295447;
// HutongGames.PlayMaker.Actions.GetMouseY
struct GetMouseY_t738295448;
// HutongGames.PlayMaker.Actions.GetName
struct GetName_t1738593039;
// HutongGames.PlayMaker.Actions.GetNextChild
struct GetNextChild_t464321883;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// HutongGames.PlayMaker.Actions.GetNextLineCast2d
struct GetNextLineCast2d_t3332969372;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// HutongGames.PlayMaker.Actions.GetNextOverlapArea2d
struct GetNextOverlapArea2d_t1935424293;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;
// HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d
struct GetNextOverlapCircle2d_t1098825704;
// HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d
struct GetNextOverlapPoint2d_t3860527186;
// HutongGames.PlayMaker.Actions.GetNextRayCast2d
struct GetNextRayCast2d_t3555256570;
// HutongGames.PlayMaker.Actions.GetOwner
struct GetOwner_t1709396389;
// HutongGames.PlayMaker.Actions.GetParent
struct GetParent_t811151086;
// HutongGames.PlayMaker.Actions.GetParticleCollisionInfo
struct GetParticleCollisionInfo_t3484627724;
// HutongGames.PlayMaker.Actions.GetPosition
struct GetPosition_t2407865645;
// HutongGames.PlayMaker.Actions.GetPreviousStateName
struct GetPreviousStateName_t1600272887;
// HutongGames.PlayMaker.Actions.GetProperty
struct GetProperty_t666919385;
// HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
struct GetQuaternionEulerAngles_t4150772157;
// HutongGames.PlayMaker.Actions.GetQuaternionFromRotation
struct GetQuaternionFromRotation_t638236618;
// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion
struct GetQuaternionMultipliedByQuaternion_t1710736874;
// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector
struct GetQuaternionMultipliedByVector_t1105007759;
// HutongGames.PlayMaker.Actions.GetRandomChild
struct GetRandomChild_t1715933131;
// HutongGames.PlayMaker.Actions.GetRandomObject
struct GetRandomObject_t1343391078;
// HutongGames.PlayMaker.Actions.GetRaycastAllInfo
struct GetRaycastAllInfo_t3534604426;
// HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo
struct GetRayCastHit2dInfo_t2378543630;
// HutongGames.PlayMaker.Actions.GetRaycastHitInfo
struct GetRaycastHitInfo_t1078696316;
// HutongGames.PlayMaker.Actions.GetRectFields
struct GetRectFields_t2216442753;
// HutongGames.PlayMaker.Actions.GetRoot
struct GetRoot_t1738725734;
// HutongGames.PlayMaker.Actions.GetRotation
struct GetRotation_t1619760002;
// HutongGames.PlayMaker.Actions.GetScale
struct GetScale_t1712482364;
// HutongGames.PlayMaker.Actions.GetScreenHeight
struct GetScreenHeight_t21638199;
// HutongGames.PlayMaker.Actions.GetScreenWidth
struct GetScreenWidth_t2390904428;
// HutongGames.PlayMaker.Actions.GetSine
struct GetSine_t1738749713;
// HutongGames.PlayMaker.Actions.GetSpeed
struct GetSpeed_t1712873273;
// HutongGames.PlayMaker.Actions.GetSpeed2d
struct GetSpeed2d_t1806850603;
// HutongGames.PlayMaker.Actions.GetStringLeft
struct GetStringLeft_t1020228988;
// HutongGames.PlayMaker.Actions.GetStringLength
struct GetStringLength_t1895851483;
// HutongGames.PlayMaker.Actions.GetStringRight
struct GetStringRight_t919028285;
// HutongGames.PlayMaker.Actions.GetSubstring
struct GetSubstring_t4098822595;
// HutongGames.PlayMaker.Actions.GetSystemDateTime
struct GetSystemDateTime_t1295868974;
// HutongGames.PlayMaker.Actions.GetTag
struct GetTag_t2986517580;
// HutongGames.PlayMaker.Actions.GetTagCount
struct GetTagCount_t870241689;
// HutongGames.PlayMaker.Actions.GetTan
struct GetTan_t2986517587;
// HutongGames.PlayMaker.Actions.GetTimeInfo
struct GetTimeInfo_t3877339423;
// HutongGames.PlayMaker.Actions.GetTouchCount
struct GetTouchCount_t970498772;
// HutongGames.PlayMaker.Actions.GetTouchInfo
struct GetTouchInfo_t3931743231;
// HutongGames.PlayMaker.Actions.GetTransform
struct GetTransform_t325979870;
// HutongGames.PlayMaker.Actions.GetTrigger2dInfo
struct GetTrigger2dInfo_t3888785898;
// HutongGames.PlayMaker.Actions.GetTriggerInfo
struct GetTriggerInfo_t1760428088;
// HutongGames.PlayMaker.Actions.GetVector2Length
struct GetVector2Length_t648683271;
// HutongGames.PlayMaker.Actions.GetVector2XY
struct GetVector2XY_t1767726594;
// HutongGames.PlayMaker.Actions.GetVector3XYZ
struct GetVector3XYZ_t2610986477;
// HutongGames.PlayMaker.Actions.GetVectorLength
struct GetVectorLength_t2333587853;
// HutongGames.PlayMaker.Actions.GetVelocity
struct GetVelocity_t3794321633;
// HutongGames.PlayMaker.Actions.GetVelocity2d
struct GetVelocity2d_t623964627;
// HutongGames.PlayMaker.Actions.GetVertexCount
struct GetVertexCount_t1542357885;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.Actions.GetVertexPosition
struct GetVertexPosition_t1771096113;
// HutongGames.PlayMaker.Actions.GotoPreviousState
struct GotoPreviousState_t1461179269;
// HutongGames.PlayMaker.Actions.GUIAction
struct GUIAction_t3055477407;
// HutongGames.PlayMaker.Actions.GUIBox
struct GUIBox_t2970443384;
// HutongGames.PlayMaker.Actions.GUIButton
struct GUIButton_t3100740507;
// HutongGames.PlayMaker.Actions.GUIContentAction
struct GUIContentAction_t3188246076;
// HutongGames.PlayMaker.Actions.GUIElementHitTest
struct GUIElementHitTest_t4187190930;
// UnityEngine.GUITexture
struct GUITexture_t4020448292;
// UnityEngine.GUIText
struct GUIText_t3371372606;
// HutongGames.PlayMaker.Actions.GUIHorizontalSlider
struct GUIHorizontalSlider_t2385736974;
// HutongGames.PlayMaker.Actions.GUILabel
struct GUILabel_t3454715681;
// HutongGames.PlayMaker.Actions.GUILayoutAction
struct GUILayoutAction_t2615417833;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2977405297;
// HutongGames.PlayMaker.Actions.GUILayoutBeginArea
struct GUILayoutBeginArea_t3761865337;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246450044.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246450044MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "mscorlib_System_Boolean476798718.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers1898294297MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables963491929.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1207664135.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1207664135MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246538643.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246538643MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1210347776.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1210347776MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "mscorlib_System_Single4291918972.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4125302563.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4125302563MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get541307091.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get541307091MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3631711289.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3631711289MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2760364049.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2760364049MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2004323440.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2004323440MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246916726.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3246916726MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1222578997.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1222578997MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2891746531.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2891746531MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3559778687.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3559778687MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2083058062.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2083058062MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3796297627.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3796297627MethodDeclarations.h"
#include "PlayMaker_ArrayTypes.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020418995.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020418995MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020418996.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020418996MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3918982924.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3918982924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo3820892225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_Device1621710112MethodDeclarations.h"
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration572715224.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get936465433.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get936465433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get130542119.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get130542119MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986509073.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986509073MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2999335123.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2999335123MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1705176140.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1705176140MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1262666006.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1262666006MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1705980995.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1705980995MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1970767239.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1970767239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService3853025142MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959.h"
#include "UnityEngine_UnityEngine_LocationService3853025142.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus1153071304.h"
#include "UnityEngine_UnityEngine_LocationInfo3215517959MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2582722562.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2582722562MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738563448.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738563448MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2322045418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get725305002.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get725305002MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com719239868MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1959127339.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1959127339MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2052155886MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3439956006.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3439956006MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2789993961.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2789993961MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseButton82682337.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1009678251.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1009678251MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1832714532.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1832714532MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get738295447.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get738295447MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get738295448.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get738295448MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738593039.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738593039MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get464321883.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get464321883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3332969372.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3332969372MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_Physics2D9846735MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1935424293.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1935424293MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1098825704.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1098825704MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3860527186.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3860527186MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3555256570.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3555256570MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1709396389.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1709396389MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get811151086.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get811151086MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3484627724.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3484627724MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2407865645.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2407865645MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1600272887.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1600272887MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get666919385.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get666919385MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4150772157.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4150772157MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qua508608383.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get638236618.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get638236618MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1710736874.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1710736874MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1105007759.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1105007759MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1715933131.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1715933131MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1343391078.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1343391078MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3534604426.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3534604426MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ra3603233760.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ra3603233760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2378543630.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2378543630MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1078696316.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1078696316MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2216442753.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2216442753MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738725734.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738725734MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1619760002.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1619760002MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1712482364.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1712482364MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetS21638199.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GetS21638199MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2390904428.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2390904428MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738749713.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1738749713MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1712873273.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1712873273MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption1934292325.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1806850603.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1806850603MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020228988.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1020228988MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1895851483.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1895851483MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get919028285.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get919028285MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4098822595.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge4098822595MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1295868974.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1295868974MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986517580.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986517580MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get870241689.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get870241689MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986517587.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2986517587MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3877339423.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3877339423MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1115130358.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime1076490199MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1115130358MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get970498772.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get970498772MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3931743231.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3931743231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Touch4210255029MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get325979870.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get325979870MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3888785898.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3888785898MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1760428088.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1760428088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get648683271.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get648683271MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1767726594.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1767726594MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2610986477.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2610986477MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2333587853.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge2333587853MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3794321633.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge3794321633MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get623964627.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Get623964627MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1542357885.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1542357885MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1771096113.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ge1771096113MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Go1461179269.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Go1461179269MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3055477407MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2970443384.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2970443384MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3188246076MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3188246076.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3100740507.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3100740507MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU4187190930.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU4187190930MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture4020448292.h"
#include "UnityEngine_UnityEngine_GUIText3371372606.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2385736974.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2385736974MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3454715681.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3454715681MethodDeclarations.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU2615417833MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption331591504.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3761865337.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GU3761865337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout3864601915MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshFilter>()
#define GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(__this, method) ((  MeshFilter_t3839065225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUITexture>()
#define GameObject_GetComponent_TisGUITexture_t4020448292_m3829342502(__this, method) ((  GUITexture_t4020448292 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUIText>()
#define GameObject_GetComponent_TisGUIText_t3371372606_m2550728272(__this, method) ((  GUIText_t3371372606 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::.ctor()
extern "C"  void GetFsmBool__ctor_m1985960554 (GetFsmBool_t3246450044 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmBool_Reset_m3927360791_MetadataUsageId;
extern "C"  void GetFsmBool_Reset_m3927360791 (GetFsmBool_t3246450044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmBool_Reset_m3927360791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnEnter()
extern "C"  void GetFsmBool_OnEnter_m934341569 (GetFsmBool_t3246450044 * __this, const MethodInfo* method)
{
	{
		GetFsmBool_DoGetFsmBool_m1450491865(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnUpdate()
extern "C"  void GetFsmBool_OnUpdate_m2328344226 (GetFsmBool_t3246450044 * __this, const MethodInfo* method)
{
	{
		GetFsmBool_DoGetFsmBool_m1450491865(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::DoGetFsmBool()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmBool_DoGetFsmBool_m1450491865_MetadataUsageId;
extern "C"  void GetFsmBool_DoGetFsmBool_m1450491865 (GetFsmBool_t3246450044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmBool_DoGetFsmBool_m1450491865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmBool_t1075959796 * V_1 = NULL;
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmBool_t1075959796 * L_20 = FsmVariables_GetFsmBool_m2932937039(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmBool_t1075959796 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmBool_t1075959796 * L_22 = __this->get_storeValue_14();
		FsmBool_t1075959796 * L_23 = V_1;
		NullCheck(L_23);
		bool L_24 = FsmBool_get_Value_m3101329097(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmBool_set_Value_m1126216340(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::.ctor()
extern "C"  void GetFsmColor__ctor_m490423567 (GetFsmColor_t1207664135 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmColor_Reset_m2431823804_MetadataUsageId;
extern "C"  void GetFsmColor_Reset_m2431823804 (GetFsmColor_t1207664135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmColor_Reset_m2431823804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmColor_t2131419205 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnEnter()
extern "C"  void GetFsmColor_OnEnter_m2537341222 (GetFsmColor_t1207664135 * __this, const MethodInfo* method)
{
	{
		GetFsmColor_DoGetFsmColor_m1716459675(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnUpdate()
extern "C"  void GetFsmColor_OnUpdate_m481725917 (GetFsmColor_t1207664135 * __this, const MethodInfo* method)
{
	{
		GetFsmColor_DoGetFsmColor_m1716459675(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::DoGetFsmColor()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmColor_DoGetFsmColor_m1716459675_MetadataUsageId;
extern "C"  void GetFsmColor_DoGetFsmColor_m1716459675 (GetFsmColor_t1207664135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmColor_DoGetFsmColor_m1716459675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmColor_t2131419205 * V_1 = NULL;
	{
		FsmColor_t2131419205 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmColor_t2131419205 * L_20 = FsmVariables_GetFsmColor_m414491395(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmColor_t2131419205 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmColor_t2131419205 * L_22 = __this->get_storeValue_14();
		FsmColor_t2131419205 * L_23 = V_1;
		NullCheck(L_23);
		Color_t4194546905  L_24 = FsmColor_get_Value_m1679829997(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmColor_set_Value_m1684002054(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::.ctor()
extern "C"  void GetFsmEnum__ctor_m2883349043 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmEnum_Reset_m529781984_MetadataUsageId;
extern "C"  void GetFsmEnum_Reset_m529781984 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmEnum_Reset_m529781984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmEnum_t1076048395 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnEnter()
extern "C"  void GetFsmEnum_OnEnter_m36253002 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method)
{
	{
		GetFsmEnum_DoGetFsmEnum_m819477575(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnUpdate()
extern "C"  void GetFsmEnum_OnUpdate_m257402425 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method)
{
	{
		GetFsmEnum_DoGetFsmEnum_m819477575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::DoGetFsmEnum()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmEnum_DoGetFsmEnum_m819477575_MetadataUsageId;
extern "C"  void GetFsmEnum_DoGetFsmEnum_m819477575 (GetFsmEnum_t3246538643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmEnum_DoGetFsmEnum_m819477575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmEnum_t1076048395 * V_1 = NULL;
	{
		FsmEnum_t1076048395 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmEnum_t1076048395 * L_20 = FsmVariables_GetFsmEnum_m4084212271(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmEnum_t1076048395 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmEnum_t1076048395 * L_22 = __this->get_storeValue_14();
		FsmEnum_t1076048395 * L_23 = V_1;
		NullCheck(L_23);
		Enum_t2862688501 * L_24 = FsmEnum_get_Value_m4041924749(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmEnum_set_Value_m2534430636(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::.ctor()
extern "C"  void GetFsmFloat__ctor_m3530660406 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmFloat_Reset_m1177093347_MetadataUsageId;
extern "C"  void GetFsmFloat_Reset_m1177093347 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmFloat_Reset_m1177093347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnEnter()
extern "C"  void GetFsmFloat_OnEnter_m3627182221 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method)
{
	{
		GetFsmFloat_DoGetFsmFloat_m1854238587(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnUpdate()
extern "C"  void GetFsmFloat_OnUpdate_m4202025814 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method)
{
	{
		GetFsmFloat_DoGetFsmFloat_m1854238587(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::DoGetFsmFloat()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmFloat_DoGetFsmFloat_m1854238587_MetadataUsageId;
extern "C"  void GetFsmFloat_DoGetFsmFloat_m1854238587 (GetFsmFloat_t1210347776 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmFloat_DoGetFsmFloat_m1854238587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeValue_14();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_11();
		NullCheck(L_2);
		GameObject_t3674682005 * L_4 = Fsm_GetOwnerDefaultTarget_m846013999(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0030;
		}
	}
	{
		return;
	}

IL_0030:
	{
		GameObject_t3674682005 * L_7 = V_0;
		GameObject_t3674682005 * L_8 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
		GameObject_t3674682005 * L_14 = V_0;
		__this->set_goLastFrame_16(L_14);
	}

IL_005f:
	{
		PlayMakerFSM_t3799847376 * L_15 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_15, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0071;
		}
	}
	{
		return;
	}

IL_0071:
	{
		PlayMakerFSM_t3799847376 * L_17 = __this->get_fsm_17();
		NullCheck(L_17);
		FsmVariables_t963491929 * L_18 = PlayMakerFSM_get_FsmVariables_m1986570741(L_17, /*hidden argument*/NULL);
		FsmString_t952858651 * L_19 = __this->get_variableName_13();
		NullCheck(L_19);
		String_t* L_20 = FsmString_get_Value_m872383149(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmFloat_t2134102846 * L_21 = FsmVariables_GetFsmFloat_m1258803409(L_18, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		FsmFloat_t2134102846 * L_22 = V_1;
		if (L_22)
		{
			goto IL_0094;
		}
	}
	{
		return;
	}

IL_0094:
	{
		FsmFloat_t2134102846 * L_23 = __this->get_storeValue_14();
		FsmFloat_t2134102846 * L_24 = V_1;
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		FsmFloat_set_Value_m1568963140(L_23, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::.ctor()
extern "C"  void GetFsmGameObject__ctor_m1546853027 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmGameObject_Reset_m3488253264_MetadataUsageId;
extern "C"  void GetFsmGameObject_Reset_m3488253264 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmGameObject_Reset_m3488253264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnEnter()
extern "C"  void GetFsmGameObject_OnEnter_m4153770426 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method)
{
	{
		GetFsmGameObject_DoGetFsmGameObject_m3089964775(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnUpdate()
extern "C"  void GetFsmGameObject_OnUpdate_m3346390985 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method)
{
	{
		GetFsmGameObject_DoGetFsmGameObject_m3089964775(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::DoGetFsmGameObject()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmGameObject_DoGetFsmGameObject_m3089964775_MetadataUsageId;
extern "C"  void GetFsmGameObject_DoGetFsmGameObject_m3089964775 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmGameObject_DoGetFsmGameObject_m3089964775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmGameObject_t1697147867 * V_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmGameObject_t1697147867 * L_20 = FsmVariables_GetFsmGameObject_m175579311(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmGameObject_t1697147867 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmGameObject_t1697147867 * L_22 = __this->get_storeValue_14();
		FsmGameObject_t1697147867 * L_23 = V_1;
		NullCheck(L_23);
		GameObject_t3674682005 * L_24 = FsmGameObject_get_Value_m673294275(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmGameObject_set_Value_m297051598(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::.ctor()
extern "C"  void GetFsmInt__ctor_m2125959619 (GetFsmInt_t541307091 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmInt_Reset_m4067359856_MetadataUsageId;
extern "C"  void GetFsmInt_Reset_m4067359856 (GetFsmInt_t541307091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmInt_Reset_m4067359856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnEnter()
extern "C"  void GetFsmInt_OnEnter_m2329456858 (GetFsmInt_t541307091 * __this, const MethodInfo* method)
{
	{
		GetFsmInt_DoGetFsmInt_m2392610203(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnUpdate()
extern "C"  void GetFsmInt_OnUpdate_m2627245225 (GetFsmInt_t541307091 * __this, const MethodInfo* method)
{
	{
		GetFsmInt_DoGetFsmInt_m2392610203(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::DoGetFsmInt()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmInt_DoGetFsmInt_m2392610203_MetadataUsageId;
extern "C"  void GetFsmInt_DoGetFsmInt_m2392610203 (GetFsmInt_t541307091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmInt_DoGetFsmInt_m2392610203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		FsmInt_t1596138449 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmInt_t1596138449 * L_20 = FsmVariables_GetFsmInt_m1904423915(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmInt_t1596138449 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmInt_t1596138449 * L_22 = __this->get_storeValue_14();
		FsmInt_t1596138449 * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = FsmInt_get_Value_m27059446(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmInt_set_Value_m2087583461(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::.ctor()
extern "C"  void GetFsmMaterial__ctor_m4155126861 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmMaterial_Reset_m1801559802_MetadataUsageId;
extern "C"  void GetFsmMaterial_Reset_m1801559802 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmMaterial_Reset_m1801559802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_13(L_3);
		__this->set_storeValue_14((FsmMaterial_t924399665 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnEnter()
extern "C"  void GetFsmMaterial_OnEnter_m2444024036 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method)
{
	{
		GetFsmMaterial_DoGetFsmVariable_m2543975976(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnUpdate()
extern "C"  void GetFsmMaterial_OnUpdate_m1883860447 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method)
{
	{
		GetFsmMaterial_DoGetFsmVariable_m2543975976(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::DoGetFsmVariable()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmMaterial_DoGetFsmVariable_m2543975976_MetadataUsageId;
extern "C"  void GetFsmMaterial_DoGetFsmVariable_m2543975976 (GetFsmMaterial_t3631711289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmMaterial_DoGetFsmVariable_m2543975976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmMaterial_t924399665 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_16(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_17(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006a;
		}
	}
	{
		FsmMaterial_t924399665 * L_15 = __this->get_storeValue_14();
		if (L_15)
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmMaterial_t924399665 * L_20 = FsmVariables_GetFsmMaterial_m2644826863(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmMaterial_t924399665 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_009e;
		}
	}
	{
		FsmMaterial_t924399665 * L_22 = __this->get_storeValue_14();
		FsmMaterial_t924399665 * L_23 = V_1;
		NullCheck(L_23);
		Material_t3870600107 * L_24 = FsmMaterial_get_Value_m1376213207(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmMaterial_set_Value_m3341549218(L_22, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::.ctor()
extern "C"  void GetFsmObject__ctor_m2342609269 (GetFsmObject_t2760364049 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmObject_Reset_m4284009506_MetadataUsageId;
extern "C"  void GetFsmObject_Reset_m4284009506 (GetFsmObject_t2760364049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmObject_Reset_m4284009506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_13(L_3);
		__this->set_storeValue_14((FsmObject_t821476169 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnEnter()
extern "C"  void GetFsmObject_OnEnter_m76373004 (GetFsmObject_t2760364049 * __this, const MethodInfo* method)
{
	{
		GetFsmObject_DoGetFsmVariable_m555649024(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnUpdate()
extern "C"  void GetFsmObject_OnUpdate_m1501122487 (GetFsmObject_t2760364049 * __this, const MethodInfo* method)
{
	{
		GetFsmObject_DoGetFsmVariable_m555649024(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::DoGetFsmVariable()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmObject_DoGetFsmVariable_m555649024_MetadataUsageId;
extern "C"  void GetFsmObject_DoGetFsmVariable_m555649024 (GetFsmObject_t2760364049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmObject_DoGetFsmVariable_m555649024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmObject_t821476169 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_16(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_17(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006a;
		}
	}
	{
		FsmObject_t821476169 * L_15 = __this->get_storeValue_14();
		if (L_15)
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmObject_t821476169 * L_20 = FsmVariables_GetFsmObject_m678224879(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmObject_t821476169 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_009e;
		}
	}
	{
		FsmObject_t821476169 * L_22 = __this->get_storeValue_14();
		FsmObject_t821476169 * L_23 = V_1;
		NullCheck(L_23);
		Object_t3071478659 * L_24 = FsmObject_get_Value_m188501991(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmObject_set_Value_m867520242(L_22, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::.ctor()
extern "C"  void GetFsmQuaternion__ctor_m543183862 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmQuaternion_Reset_m2484584099_MetadataUsageId;
extern "C"  void GetFsmQuaternion_Reset_m2484584099 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmQuaternion_Reset_m2484584099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_13(L_3);
		__this->set_storeValue_14((FsmQuaternion_t3871136040 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnEnter()
extern "C"  void GetFsmQuaternion_OnEnter_m1700377165 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method)
{
	{
		GetFsmQuaternion_DoGetFsmVariable_m2558044895(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnUpdate()
extern "C"  void GetFsmQuaternion_OnUpdate_m305643926 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method)
{
	{
		GetFsmQuaternion_DoGetFsmVariable_m2558044895(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::DoGetFsmVariable()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmQuaternion_DoGetFsmVariable_m2558044895_MetadataUsageId;
extern "C"  void GetFsmQuaternion_DoGetFsmVariable_m2558044895 (GetFsmQuaternion_t2004323440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmQuaternion_DoGetFsmVariable_m2558044895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmQuaternion_t3871136040 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_16(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_17(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006a;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_15 = __this->get_storeValue_14();
		if (L_15)
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmQuaternion_t3871136040 * L_20 = FsmVariables_GetFsmQuaternion_m3924878991(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmQuaternion_t3871136040 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_009e;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_22 = __this->get_storeValue_14();
		FsmQuaternion_t3871136040 * L_23 = V_1;
		NullCheck(L_23);
		Quaternion_t1553702882  L_24 = FsmQuaternion_get_Value_m3393858025(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmQuaternion_set_Value_m446581172(L_22, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::.ctor()
extern "C"  void GetFsmRect__ctor_m3107091632 (GetFsmRect_t3246916726 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmRect_Reset_m753524573_MetadataUsageId;
extern "C"  void GetFsmRect_Reset_m753524573 (GetFsmRect_t3246916726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmRect_Reset_m753524573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_13(L_3);
		__this->set_storeValue_14((FsmRect_t1076426478 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::OnEnter()
extern "C"  void GetFsmRect_OnEnter_m304516231 (GetFsmRect_t3246916726 * __this, const MethodInfo* method)
{
	{
		GetFsmRect_DoGetFsmVariable_m1948561893(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::OnUpdate()
extern "C"  void GetFsmRect_OnUpdate_m4278595228 (GetFsmRect_t3246916726 * __this, const MethodInfo* method)
{
	{
		GetFsmRect_DoGetFsmVariable_m1948561893(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmRect::DoGetFsmVariable()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmRect_DoGetFsmVariable_m1948561893_MetadataUsageId;
extern "C"  void GetFsmRect_DoGetFsmVariable_m1948561893 (GetFsmRect_t3246916726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmRect_DoGetFsmVariable_m1948561893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmRect_t1076426478 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_16(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_17(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006a;
		}
	}
	{
		FsmRect_t1076426478 * L_15 = __this->get_storeValue_14();
		if (L_15)
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmRect_t1076426478 * L_20 = FsmVariables_GetFsmRect_m1774888079(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmRect_t1076426478 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_009e;
		}
	}
	{
		FsmRect_t1076426478 * L_22 = __this->get_storeValue_14();
		FsmRect_t1076426478 * L_23 = V_1;
		NullCheck(L_23);
		Rect_t4241904616  L_24 = FsmRect_get_Value_m1002500317(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmRect_set_Value_m1159518952(L_22, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::.ctor()
extern "C"  void GetFsmState__ctor_m264348577 (GetFsmState_t1222578997 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmState_Reset_m2205748814_MetadataUsageId;
extern "C"  void GetFsmState_Reset_m2205748814 (GetFsmState_t1222578997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmState_Reset_m2205748814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_fsmComponent_11((PlayMakerFSM_t3799847376 *)NULL);
		__this->set_gameObject_12((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_13(L_1);
		__this->set_storeResult_14((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnEnter()
extern "C"  void GetFsmState_OnEnter_m27640632 (GetFsmState_t1222578997 * __this, const MethodInfo* method)
{
	{
		GetFsmState_DoGetFsmState_m2029111003(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnUpdate()
extern "C"  void GetFsmState_OnUpdate_m4285386251 (GetFsmState_t1222578997 * __this, const MethodInfo* method)
{
	{
		GetFsmState_DoGetFsmState_m2029111003(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::DoGetFsmState()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmState_DoGetFsmState_m2029111003_MetadataUsageId;
extern "C"  void GetFsmState_DoGetFsmState_m2029111003 (GetFsmState_t1222578997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmState_DoGetFsmState_m2029111003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		PlayMakerFSM_t3799847376 * L_0 = __this->get_fsm_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008a;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_2 = __this->get_fsmComponent_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		PlayMakerFSM_t3799847376 * L_4 = __this->get_fsmComponent_11();
		__this->set_fsm_16(L_4);
		goto IL_0068;
	}

IL_0033:
	{
		Fsm_t1527112426 * L_5 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_6 = __this->get_gameObject_12();
		NullCheck(L_5);
		GameObject_t3674682005 * L_7 = Fsm_GetOwnerDefaultTarget_m846013999(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t3674682005 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_13();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_16(L_13);
	}

IL_0068:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_008a;
		}
	}
	{
		FsmString_t952858651 * L_16 = __this->get_storeResult_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_16);
		FsmString_set_Value_m829393196(L_16, L_17, /*hidden argument*/NULL);
		return;
	}

IL_008a:
	{
		FsmString_t952858651 * L_18 = __this->get_storeResult_14();
		PlayMakerFSM_t3799847376 * L_19 = __this->get_fsm_16();
		NullCheck(L_19);
		String_t* L_20 = PlayMakerFSM_get_ActiveStateName_m1548898773(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmString_set_Value_m829393196(L_18, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::.ctor()
extern "C"  void GetFsmString__ctor_m1704915171 (GetFsmString_t2891746531 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmString_Reset_m3646315408_MetadataUsageId;
extern "C"  void GetFsmString_Reset_m3646315408 (GetFsmString_t2891746531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmString_Reset_m3646315408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnEnter()
extern "C"  void GetFsmString_OnEnter_m1432668154 (GetFsmString_t2891746531 * __this, const MethodInfo* method)
{
	{
		GetFsmString_DoGetFsmString_m3328963943(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnUpdate()
extern "C"  void GetFsmString_OnUpdate_m596599177 (GetFsmString_t2891746531 * __this, const MethodInfo* method)
{
	{
		GetFsmString_DoGetFsmString_m3328963943(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::DoGetFsmString()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmString_DoGetFsmString_m3328963943_MetadataUsageId;
extern "C"  void GetFsmString_DoGetFsmString_m3328963943 (GetFsmString_t2891746531 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmString_DoGetFsmString_m3328963943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmString_t952858651 * V_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmString_t952858651 * L_20 = FsmVariables_GetFsmString_m4055225775(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmString_t952858651 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmString_t952858651 * L_22 = __this->get_storeValue_14();
		FsmString_t952858651 * L_23 = V_1;
		NullCheck(L_23);
		String_t* L_24 = FsmString_get_Value_m872383149(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmString_set_Value_m829393196(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::.ctor()
extern "C"  void GetFsmTexture__ctor_m3908478615 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmTexture_Reset_m1554911556_MetadataUsageId;
extern "C"  void GetFsmTexture_Reset_m1554911556 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmTexture_Reset_m1554911556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_variableName_13(L_3);
		__this->set_storeValue_14((FsmTexture_t3073272573 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnEnter()
extern "C"  void GetFsmTexture_OnEnter_m1638260910 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method)
{
	{
		GetFsmTexture_DoGetFsmVariable_m1943280030(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnUpdate()
extern "C"  void GetFsmTexture_OnUpdate_m2675007317 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method)
{
	{
		GetFsmTexture_DoGetFsmVariable_m1943280030(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::DoGetFsmVariable()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmTexture_DoGetFsmVariable_m1943280030_MetadataUsageId;
extern "C"  void GetFsmTexture_DoGetFsmVariable_m1943280030 (GetFsmTexture_t3559778687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmTexture_DoGetFsmVariable_m1943280030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmTexture_t3073272573 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		__this->set_goLastFrame_16(L_8);
		GameObject_t3674682005 * L_9 = V_0;
		FsmString_t952858651 * L_10 = __this->get_fsmName_12();
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m872383149(L_10, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_12 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_fsm_17(L_12);
	}

IL_004e:
	{
		PlayMakerFSM_t3799847376 * L_13 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006a;
		}
	}
	{
		FsmTexture_t3073272573 * L_15 = __this->get_storeValue_14();
		if (L_15)
		{
			goto IL_006b;
		}
	}

IL_006a:
	{
		return;
	}

IL_006b:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmTexture_t3073272573 * L_20 = FsmVariables_GetFsmTexture_m3423405971(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmTexture_t3073272573 * L_21 = V_1;
		if (!L_21)
		{
			goto IL_009e;
		}
	}
	{
		FsmTexture_t3073272573 * L_22 = __this->get_storeValue_14();
		FsmTexture_t3073272573 * L_23 = V_1;
		NullCheck(L_23);
		Texture_t2526458961 * L_24 = FsmTexture_get_Value_m3156202285(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmTexture_set_Value_m2261522310(L_22, L_24, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::.ctor()
extern "C"  void GetFsmVariable__ctor_m1285944984 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmVar_t1596150537_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVariable_Reset_m3227345221_MetadataUsageId;
extern "C"  void GetFsmVariable_Reset_m3227345221 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVariable_Reset_m3227345221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		FsmVar_t1596150537 * L_2 = (FsmVar_t1596150537 *)il2cpp_codegen_object_new(FsmVar_t1596150537_il2cpp_TypeInfo_var);
		FsmVar__ctor_m2050768746(L_2, /*hidden argument*/NULL);
		__this->set_storeValue_13(L_2);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnEnter()
extern "C"  void GetFsmVariable_OnEnter_m2529244271 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	{
		GetFsmVariable_InitFsmVar_m1467331747(__this, /*hidden argument*/NULL);
		GetFsmVariable_DoGetFsmVariable_m3357830909(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnUpdate()
extern "C"  void GetFsmVariable_OnUpdate_m230720436 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	{
		GetFsmVariable_DoGetFsmVariable_m3357830909(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::InitFsmVar()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2607078876;
extern const uint32_t GetFsmVariable_InitFsmVar_m1467331747_MetadataUsageId;
extern "C"  void GetFsmVariable_InitFsmVar_m1467331747 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVariable_InitFsmVar_m1467331747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_cachedGO_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00e1;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		FsmString_t952858651 * L_9 = __this->get_fsmName_12();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_11 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		__this->set_sourceFsm_16(L_11);
		PlayMakerFSM_t3799847376 * L_12 = __this->get_sourceFsm_16();
		NullCheck(L_12);
		FsmVariables_t963491929 * L_13 = PlayMakerFSM_get_FsmVariables_m1986570741(L_12, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_14 = __this->get_storeValue_13();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_variableName_0();
		NullCheck(L_13);
		NamedVariable_t3211770239 * L_16 = FsmVariables_GetVariable_m1409842114(L_13, L_15, /*hidden argument*/NULL);
		__this->set_sourceVariable_17(L_16);
		Fsm_t1527112426 * L_17 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVariables_t963491929 * L_18 = Fsm_get_Variables_m2281949087(L_17, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_19 = __this->get_storeValue_13();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_variableName_0();
		NullCheck(L_18);
		NamedVariable_t3211770239 * L_21 = FsmVariables_GetVariable_m1409842114(L_18, L_20, /*hidden argument*/NULL);
		__this->set_targetVariable_18(L_21);
		FsmVar_t1596150537 * L_22 = __this->get_storeValue_13();
		NamedVariable_t3211770239 * L_23 = __this->get_targetVariable_18();
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(22 /* HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_VariableType() */, L_23);
		NullCheck(L_22);
		FsmVar_set_Type_m4211541807(L_22, L_24, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_25 = __this->get_storeValue_13();
		NullCheck(L_25);
		String_t* L_26 = L_25->get_variableName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00da;
		}
	}
	{
		Il2CppObject * L_28 = __this->get_sourceVariable_17();
		if (L_28)
		{
			goto IL_00da;
		}
	}
	{
		FsmVar_t1596150537 * L_29 = __this->get_storeValue_13();
		NullCheck(L_29);
		String_t* L_30 = L_29->get_variableName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2607078876, L_30, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_31, /*hidden argument*/NULL);
	}

IL_00da:
	{
		GameObject_t3674682005 * L_32 = V_0;
		__this->set_cachedGO_15(L_32);
	}

IL_00e1:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::DoGetFsmVariable()
extern "C"  void GetFsmVariable_DoGetFsmVariable_m3357830909 (GetFsmVariable_t2083058062 * __this, const MethodInfo* method)
{
	{
		FsmVar_t1596150537 * L_0 = __this->get_storeValue_13();
		NullCheck(L_0);
		bool L_1 = FsmVar_get_IsNone_m3892161437(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		GetFsmVariable_InitFsmVar_m1467331747(__this, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_2 = __this->get_storeValue_13();
		Il2CppObject * L_3 = __this->get_sourceVariable_17();
		NullCheck(L_2);
		FsmVar_GetValueFrom_m764615143(L_2, L_3, /*hidden argument*/NULL);
		FsmVar_t1596150537 * L_4 = __this->get_storeValue_13();
		NamedVariable_t3211770239 * L_5 = __this->get_targetVariable_18();
		NullCheck(L_4);
		FsmVar_ApplyValueTo_m1826228640(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::.ctor()
extern "C"  void GetFsmVariables__ctor_m2056957435 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVariables_Reset_m3998357672_MetadataUsageId;
extern "C"  void GetFsmVariables_Reset_m3998357672 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVariables_Reset_m3998357672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_getVariables_13((FsmVarU5BU5D_t3498949300*)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::InitFsmVars()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* INamedVariableU5BU5D_t2748317531_il2cpp_TypeInfo_var;
extern Il2CppClass* NamedVariableU5BU5D_t2180779430_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2607078876;
extern const uint32_t GetFsmVariables_InitFsmVars_m2275039861_MetadataUsageId;
extern "C"  void GetFsmVariables_InitFsmVars_m2275039861 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVariables_InitFsmVars_m2275039861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_cachedGO_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0110;
		}
	}
	{
		FsmVarU5BU5D_t3498949300* L_8 = __this->get_getVariables_13();
		NullCheck(L_8);
		__this->set_sourceVariables_17(((INamedVariableU5BU5D_t2748317531*)SZArrayNew(INamedVariableU5BU5D_t2748317531_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))));
		FsmVarU5BU5D_t3498949300* L_9 = __this->get_getVariables_13();
		NullCheck(L_9);
		__this->set_targetVariables_18(((NamedVariableU5BU5D_t2180779430*)SZArrayNew(NamedVariableU5BU5D_t2180779430_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))));
		V_1 = 0;
		goto IL_0102;
	}

IL_005d:
	{
		FsmVarU5BU5D_t3498949300* L_10 = __this->get_getVariables_13();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		FsmVar_t1596150537 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = L_13->get_variableName_0();
		V_2 = L_14;
		GameObject_t3674682005 * L_15 = V_0;
		FsmString_t952858651 * L_16 = __this->get_fsmName_12();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_18 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		__this->set_sourceFsm_16(L_18);
		INamedVariableU5BU5D_t2748317531* L_19 = __this->get_sourceVariables_17();
		int32_t L_20 = V_1;
		PlayMakerFSM_t3799847376 * L_21 = __this->get_sourceFsm_16();
		NullCheck(L_21);
		FsmVariables_t963491929 * L_22 = PlayMakerFSM_get_FsmVariables_m1986570741(L_21, /*hidden argument*/NULL);
		String_t* L_23 = V_2;
		NullCheck(L_22);
		NamedVariable_t3211770239 * L_24 = FsmVariables_GetVariable_m1409842114(L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (Il2CppObject *)L_24);
		NamedVariableU5BU5D_t2180779430* L_25 = __this->get_targetVariables_18();
		int32_t L_26 = V_1;
		Fsm_t1527112426 * L_27 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmVariables_t963491929 * L_28 = Fsm_get_Variables_m2281949087(L_27, /*hidden argument*/NULL);
		String_t* L_29 = V_2;
		NullCheck(L_28);
		NamedVariable_t3211770239 * L_30 = FsmVariables_GetVariable_m1409842114(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (NamedVariable_t3211770239 *)L_30);
		FsmVarU5BU5D_t3498949300* L_31 = __this->get_getVariables_13();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		FsmVar_t1596150537 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NamedVariableU5BU5D_t2180779430* L_35 = __this->get_targetVariables_18();
		int32_t L_36 = V_1;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		NamedVariable_t3211770239 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		int32_t L_39 = VirtFuncInvoker0< int32_t >::Invoke(22 /* HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_VariableType() */, L_38);
		NullCheck(L_34);
		FsmVar_set_Type_m4211541807(L_34, L_39, /*hidden argument*/NULL);
		String_t* L_40 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_41 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_00f7;
		}
	}
	{
		INamedVariableU5BU5D_t2748317531* L_42 = __this->get_sourceVariables_17();
		int32_t L_43 = V_1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		Il2CppObject * L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		if (L_45)
		{
			goto IL_00f7;
		}
	}
	{
		String_t* L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2607078876, L_46, /*hidden argument*/NULL);
		FsmStateAction_LogWarning_m567309232(__this, L_47, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		GameObject_t3674682005 * L_48 = V_0;
		__this->set_cachedGO_15(L_48);
		int32_t L_49 = V_1;
		V_1 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_0102:
	{
		int32_t L_50 = V_1;
		FsmVarU5BU5D_t3498949300* L_51 = __this->get_getVariables_13();
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_51)->max_length)))))))
		{
			goto IL_005d;
		}
	}

IL_0110:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnEnter()
extern "C"  void GetFsmVariables_OnEnter_m442867474 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	{
		GetFsmVariables_InitFsmVars_m2275039861(__this, /*hidden argument*/NULL);
		GetFsmVariables_DoGetFsmVariables_m45106459(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnUpdate()
extern "C"  void GetFsmVariables_OnUpdate_m4272516465 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	{
		GetFsmVariables_DoGetFsmVariables_m45106459(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::DoGetFsmVariables()
extern "C"  void GetFsmVariables_DoGetFsmVariables_m45106459 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GetFsmVariables_InitFsmVars_m2275039861(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		FsmVarU5BU5D_t3498949300* L_0 = __this->get_getVariables_13();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		FsmVar_t1596150537 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		INamedVariableU5BU5D_t2748317531* L_4 = __this->get_sourceVariables_17();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_3);
		FsmVar_GetValueFrom_m764615143(L_3, L_7, /*hidden argument*/NULL);
		FsmVarU5BU5D_t3498949300* L_8 = __this->get_getVariables_13();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		FsmVar_t1596150537 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NamedVariableU5BU5D_t2180779430* L_12 = __this->get_targetVariables_18();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NamedVariable_t3211770239 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_11);
		FsmVar_ApplyValueTo_m1826228640(L_11, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_17 = V_0;
		FsmVarU5BU5D_t3498949300* L_18 = __this->get_getVariables_13();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::.ctor()
extern "C"  void GetFsmVector2__ctor_m1276997347 (GetFsmVector2_t1020418995 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVector2_Reset_m3218397584_MetadataUsageId;
extern "C"  void GetFsmVector2_Reset_m3218397584 (GetFsmVector2_t1020418995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVector2_Reset_m3218397584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmVector2_t533912881 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::OnEnter()
extern "C"  void GetFsmVector2_OnEnter_m2520499706 (GetFsmVector2_t1020418995 * __this, const MethodInfo* method)
{
	{
		GetFsmVector2_DoGetFsmVector2_m2550297755(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::OnUpdate()
extern "C"  void GetFsmVector2_OnUpdate_m4254606217 (GetFsmVector2_t1020418995 * __this, const MethodInfo* method)
{
	{
		GetFsmVector2_DoGetFsmVector2_m2550297755(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::DoGetFsmVector2()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVector2_DoGetFsmVector2_m2550297755_MetadataUsageId;
extern "C"  void GetFsmVector2_DoGetFsmVector2_m2550297755 (GetFsmVector2_t1020418995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVector2_DoGetFsmVector2_m2550297755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmVector2_t533912881 * V_1 = NULL;
	{
		FsmVector2_t533912881 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVector2_t533912881 * L_20 = FsmVariables_GetFsmVector2_m3581467435(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmVector2_t533912881 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmVector2_t533912881 * L_22 = __this->get_storeValue_14();
		FsmVector2_t533912881 * L_23 = V_1;
		NullCheck(L_23);
		Vector2_t4282066565  L_24 = FsmVector2_get_Value_m1313754285(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmVector2_set_Value_m2900659718(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::.ctor()
extern "C"  void GetFsmVector3__ctor_m1080483842 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVector3_Reset_m3021884079_MetadataUsageId;
extern "C"  void GetFsmVector3_Reset_m3021884079 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVector3_Reset_m3021884079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_fsmName_12(L_1);
		__this->set_storeValue_14((FsmVector3_t533912882 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::OnEnter()
extern "C"  void GetFsmVector3_OnEnter_m2649582425 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method)
{
	{
		GetFsmVector3_DoGetFsmVector3_m2141474491(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::OnUpdate()
extern "C"  void GetFsmVector3_OnUpdate_m3961203210 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method)
{
	{
		GetFsmVector3_DoGetFsmVector3_m2141474491(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector3::DoGetFsmVector3()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetFsmVector3_DoGetFsmVector3_m2141474491_MetadataUsageId;
extern "C"  void GetFsmVector3_DoGetFsmVector3_m2141474491 (GetFsmVector3_t1020418996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetFsmVector3_DoGetFsmVector3_m2141474491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmVector3_t533912882 * V_1 = NULL;
	{
		FsmVector3_t533912882 * L_0 = __this->get_storeValue_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_2 = __this->get_gameObject_11();
		NullCheck(L_1);
		GameObject_t3674682005 * L_3 = Fsm_GetOwnerDefaultTarget_m846013999(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t3674682005 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		GameObject_t3674682005 * L_6 = V_0;
		GameObject_t3674682005 * L_7 = __this->get_goLastFrame_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t3674682005 * L_9 = V_0;
		__this->set_goLastFrame_16(L_9);
		GameObject_t3674682005 * L_10 = V_0;
		FsmString_t952858651 * L_11 = __this->get_fsmName_12();
		NullCheck(L_11);
		String_t* L_12 = FsmString_get_Value_m872383149(L_11, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_13 = ActionHelpers_GetGameObjectFsm_m1076592826(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_fsm_17(L_13);
	}

IL_005a:
	{
		PlayMakerFSM_t3799847376 * L_14 = __this->get_fsm_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		return;
	}

IL_006c:
	{
		PlayMakerFSM_t3799847376 * L_16 = __this->get_fsm_17();
		NullCheck(L_16);
		FsmVariables_t963491929 * L_17 = PlayMakerFSM_get_FsmVariables_m1986570741(L_16, /*hidden argument*/NULL);
		FsmString_t952858651 * L_18 = __this->get_variableName_13();
		NullCheck(L_18);
		String_t* L_19 = FsmString_get_Value_m872383149(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVector3_t533912882 * L_20 = FsmVariables_GetFsmVector3_m557465897(L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		FsmVector3_t533912882 * L_21 = V_1;
		if (L_21)
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_008f:
	{
		FsmVector3_t533912882 * L_22 = __this->get_storeValue_14();
		FsmVector3_t533912882 * L_23 = V_1;
		NullCheck(L_23);
		Vector3_t4282066566  L_24 = FsmVector3_get_Value_m2779135117(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		FsmVector3_set_Value_m716982822(L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::.ctor()
extern "C"  void GetIPhoneSettings__ctor_m1881332650 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::Reset()
extern "C"  void GetIPhoneSettings_Reset_m3822732887 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method)
{
	{
		__this->set_getScreenCanDarken_11((FsmBool_t1075959796 *)NULL);
		__this->set_getUniqueIdentifier_12((FsmString_t952858651 *)NULL);
		__this->set_getName_13((FsmString_t952858651 *)NULL);
		__this->set_getModel_14((FsmString_t952858651 *)NULL);
		__this->set_getSystemName_15((FsmString_t952858651 *)NULL);
		__this->set_getGeneration_16((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetIPhoneSettings::OnEnter()
extern Il2CppClass* DeviceGeneration_t572715224_il2cpp_TypeInfo_var;
extern const uint32_t GetIPhoneSettings_OnEnter_m3466140929_MetadataUsageId;
extern "C"  void GetIPhoneSettings_OnEnter_m3466140929 (GetIPhoneSettings_t3918982924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetIPhoneSettings_OnEnter_m3466140929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_getScreenCanDarken_11();
		int32_t L_1 = Screen_get_sleepTimeout_m4077361558(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, (bool)((((float)(((float)((float)L_1)))) > ((float)(0.0f)))? 1 : 0), /*hidden argument*/NULL);
		FsmString_t952858651 * L_2 = __this->get_getUniqueIdentifier_12();
		String_t* L_3 = SystemInfo_get_deviceUniqueIdentifier_m983206480(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_3, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_getName_13();
		String_t* L_5 = SystemInfo_get_deviceName_m3161260225(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmString_set_Value_m829393196(L_4, L_5, /*hidden argument*/NULL);
		FsmString_t952858651 * L_6 = __this->get_getModel_14();
		String_t* L_7 = SystemInfo_get_deviceModel_m3014844565(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmString_set_Value_m829393196(L_6, L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_8 = __this->get_getSystemName_15();
		String_t* L_9 = SystemInfo_get_operatingSystem_m2538828082(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmString_set_Value_m829393196(L_8, L_9, /*hidden argument*/NULL);
		FsmString_t952858651 * L_10 = __this->get_getGeneration_16();
		int32_t L_11 = Device_get_generation_m1444092920(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(DeviceGeneration_t572715224_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Enum_t2862688501 *)L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_13);
		NullCheck(L_10);
		FsmString_set_Value_m829393196(L_10, L_14, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::.ctor()
extern "C"  void GetJointBreak2dInfo__ctor_m286833469 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::Reset()
extern "C"  void GetJointBreak2dInfo_Reset_m2228233706 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method)
{
	{
		__this->set_brokenJoint_11((FsmObject_t821476169 *)NULL);
		__this->set_reactionForce_12((FsmVector2_t533912881 *)NULL);
		__this->set_reactionTorque_14((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::StoreInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetJointBreak2dInfo_StoreInfo_m2960510154_MetadataUsageId;
extern "C"  void GetJointBreak2dInfo_StoreInfo_m2960510154 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetJointBreak2dInfo_StoreInfo_m2960510154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Joint2D_t2513613714 * L_1 = Fsm_get_BrokenJoint2D_m3680999647(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmObject_t821476169 * L_3 = __this->get_brokenJoint_11();
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Joint2D_t2513613714 * L_5 = Fsm_get_BrokenJoint2D_m3680999647(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmObject_set_Value_m867520242(L_3, L_5, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_6 = __this->get_reactionForce_12();
		Fsm_t1527112426 * L_7 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Joint2D_t2513613714 * L_8 = Fsm_get_BrokenJoint2D_m3680999647(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t4282066565  L_9 = Joint2D_get_reactionForce_m2999776751(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmVector2_set_Value_m2900659718(L_6, L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = __this->get_reactionForceMagnitude_13();
		Fsm_t1527112426 * L_11 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Joint2D_t2513613714 * L_12 = Fsm_get_BrokenJoint2D_m3680999647(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t4282066565  L_13 = Joint2D_get_reactionForce_m2999776751(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		float L_14 = Vector2_get_magnitude_m1987058139((&V_0), /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_15 = __this->get_reactionTorque_14();
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Joint2D_t2513613714 * L_17 = Fsm_get_BrokenJoint2D_m3680999647(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		float L_18 = Joint2D_get_reactionTorque_m1288556183(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::OnEnter()
extern "C"  void GetJointBreak2dInfo_OnEnter_m160785364 (GetJointBreak2dInfo_t936465433 * __this, const MethodInfo* method)
{
	{
		GetJointBreak2dInfo_StoreInfo_m2960510154(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::.ctor()
extern "C"  void GetJointBreakInfo__ctor_m228273903 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::Reset()
extern "C"  void GetJointBreakInfo_Reset_m2169674140 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method)
{
	{
		__this->set_breakForce_11((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::OnEnter()
extern "C"  void GetJointBreakInfo_OnEnter_m4014584582 (GetJointBreakInfo_t130542119 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_breakForce_11();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		float L_2 = Fsm_get_JointBreakForce_m1141664588(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKey::.ctor()
extern "C"  void GetKey__ctor_m653000821 (GetKey_t2986509073 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKey::Reset()
extern "C"  void GetKey_Reset_m2594401058 (GetKey_t2986509073 * __this, const MethodInfo* method)
{
	{
		__this->set_key_11(0);
		__this->set_storeResult_12((FsmBool_t1075959796 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnEnter()
extern "C"  void GetKey_OnEnter_m4155259660 (GetKey_t2986509073 * __this, const MethodInfo* method)
{
	{
		GetKey_DoGetKey_m86958531(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnUpdate()
extern "C"  void GetKey_OnUpdate_m3392557239 (GetKey_t2986509073 * __this, const MethodInfo* method)
{
	{
		GetKey_DoGetKey_m86958531(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKey::DoGetKey()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetKey_DoGetKey_m86958531_MetadataUsageId;
extern "C"  void GetKey_DoGetKey_m86958531 (GetKey_t2986509073 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetKey_DoGetKey_m86958531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_12();
		int32_t L_1 = __this->get_key_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m1349175653(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::.ctor()
extern "C"  void GetKeyDown__ctor_m4150057203 (GetKeyDown_t2999335123 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::Reset()
extern "C"  void GetKeyDown_Reset_m1796490144 (GetKeyDown_t2999335123 * __this, const MethodInfo* method)
{
	{
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_key_11(0);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetKeyDown_OnUpdate_m1177534329_MetadataUsageId;
extern "C"  void GetKeyDown_OnUpdate_m1177534329 (GetKeyDown_t2999335123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetKeyDown_OnUpdate_m1177534329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_key_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = __this->get_sendEvent_12();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_13();
		bool L_6 = V_0;
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::.ctor()
extern "C"  void GetKeyUp__ctor_m478061466 (GetKeyUp_t1705176140 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::Reset()
extern "C"  void GetKeyUp_Reset_m2419461703 (GetKeyUp_t1705176140 * __this, const MethodInfo* method)
{
	{
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_key_11(0);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetKeyUp_OnUpdate_m1569562482_MetadataUsageId;
extern "C"  void GetKeyUp_OnUpdate_m1569562482 (GetKeyUp_t1705176140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetKeyUp_OnUpdate_m1569562482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_key_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUp_m2739135306(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = __this->get_sendEvent_12();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_13();
		bool L_6 = V_0;
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::.ctor()
extern "C"  void GetLastEvent__ctor_m3205809680 (GetLastEvent_t1262666006 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::Reset()
extern "C"  void GetLastEvent_Reset_m852242621 (GetLastEvent_t1262666006 * __this, const MethodInfo* method)
{
	{
		__this->set_storeEvent_11((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLastEvent::OnEnter()
extern Il2CppCodeGenString* _stringLiteral79219778;
extern const uint32_t GetLastEvent_OnEnter_m683279847_MetadataUsageId;
extern "C"  void GetLastEvent_OnEnter_m683279847 (GetLastEvent_t1262666006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetLastEvent_OnEnter_m683279847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * G_B2_0 = NULL;
	FsmString_t952858651 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	FsmString_t952858651 * G_B3_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_storeEvent_11();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmTransition_t3771611999 * L_2 = Fsm_get_LastTransition_m3649861433(L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (L_2)
		{
			G_B2_0 = L_0;
			goto IL_0020;
		}
	}
	{
		G_B3_0 = _stringLiteral79219778;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_0020:
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmTransition_t3771611999 * L_4 = Fsm_get_LastTransition_m3649861433(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = FsmTransition_get_EventName_m1790647229(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		NullCheck(G_B3_1);
		FsmString_set_Value_m829393196(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLayer::.ctor()
extern "C"  void GetLayer__ctor_m2066637187 (GetLayer_t1705980995 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLayer::Reset()
extern "C"  void GetLayer_Reset_m4008037424 (GetLayer_t1705980995 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeResult_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnEnter()
extern "C"  void GetLayer_OnEnter_m1155174554 (GetLayer_t1705980995 * __this, const MethodInfo* method)
{
	{
		GetLayer_DoGetLayer_m1176715047(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnUpdate()
extern "C"  void GetLayer_OnUpdate_m584232169 (GetLayer_t1705980995 * __this, const MethodInfo* method)
{
	{
		GetLayer_DoGetLayer_m1176715047(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLayer::DoGetLayer()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetLayer_DoGetLayer_m1176715047_MetadataUsageId;
extern "C"  void GetLayer_DoGetLayer_m1176715047 (GetLayer_t1705980995 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetLayer_DoGetLayer_m1176715047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmInt_t1596138449 * L_3 = __this->get_storeResult_12();
		FsmGameObject_t1697147867 * L_4 = __this->get_gameObject_11();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = GameObject_get_layer_m1648550306(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::.ctor()
extern "C"  void GetLocationInfo__ctor_m3145660815 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::Reset()
extern "C"  void GetLocationInfo_Reset_m792093756 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method)
{
	{
		__this->set_longitude_12((FsmFloat_t2134102846 *)NULL);
		__this->set_latitude_13((FsmFloat_t2134102846 *)NULL);
		__this->set_altitude_14((FsmFloat_t2134102846 *)NULL);
		__this->set_horizontalAccuracy_15((FsmFloat_t2134102846 *)NULL);
		__this->set_verticalAccuracy_16((FsmFloat_t2134102846 *)NULL);
		__this->set_errorEvent_17((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::OnEnter()
extern "C"  void GetLocationInfo_OnEnter_m3009762726 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method)
{
	{
		GetLocationInfo_DoGetLocationInfo_m3720127899(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::DoGetLocationInfo()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetLocationInfo_DoGetLocationInfo_m3720127899_MetadataUsageId;
extern "C"  void GetLocationInfo_DoGetLocationInfo_m3720127899 (GetLocationInfo_t1970767239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetLocationInfo_DoGetLocationInfo_m3720127899_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	LocationInfo_t3215517959  V_3;
	memset(&V_3, 0, sizeof(V_3));
	LocationInfo_t3215517959  V_4;
	memset(&V_4, 0, sizeof(V_4));
	LocationInfo_t3215517959  V_5;
	memset(&V_5, 0, sizeof(V_5));
	LocationInfo_t3215517959  V_6;
	memset(&V_6, 0, sizeof(V_6));
	LocationInfo_t3215517959  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_0 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = LocationService_get_status_m435232686(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0022;
		}
	}
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_errorEvent_17();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		LocationService_t3853025142 * L_4 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		LocationInfo_t3215517959  L_5 = LocationService_get_lastData_m2812407679(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = LocationInfo_get_longitude_m3505987842((&V_3), /*hidden argument*/NULL);
		V_0 = L_6;
		LocationService_t3853025142 * L_7 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		LocationInfo_t3215517959  L_8 = LocationService_get_lastData_m2812407679(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = LocationInfo_get_latitude_m1803811355((&V_4), /*hidden argument*/NULL);
		V_1 = L_9;
		LocationService_t3853025142 * L_10 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		LocationInfo_t3215517959  L_11 = LocationService_get_lastData_m2812407679(L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = LocationInfo_get_altitude_m1263330001((&V_5), /*hidden argument*/NULL);
		V_2 = L_12;
		FsmVector3_t533912882 * L_13 = __this->get_vectorPosition_11();
		float L_14 = V_0;
		float L_15 = V_1;
		float L_16 = V_2;
		Vector3_t4282066566  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_14, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmVector3_set_Value_m716982822(L_13, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_longitude_12();
		float L_19 = V_0;
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_latitude_13();
		float L_21 = V_1;
		NullCheck(L_20);
		FsmFloat_set_Value_m1568963140(L_20, L_21, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_22 = __this->get_altitude_14();
		float L_23 = V_2;
		NullCheck(L_22);
		FsmFloat_set_Value_m1568963140(L_22, L_23, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_24 = __this->get_horizontalAccuracy_15();
		LocationService_t3853025142 * L_25 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		LocationInfo_t3215517959  L_26 = LocationService_get_lastData_m2812407679(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = LocationInfo_get_horizontalAccuracy_m1838637356((&V_6), /*hidden argument*/NULL);
		NullCheck(L_24);
		FsmFloat_set_Value_m1568963140(L_24, L_27, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_28 = __this->get_verticalAccuracy_16();
		LocationService_t3853025142 * L_29 = Input_get_location_m4175212201(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		LocationInfo_t3215517959  L_30 = LocationService_get_lastData_m2812407679(L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = LocationInfo_get_verticalAccuracy_m3476844926((&V_7), /*hidden argument*/NULL);
		NullCheck(L_28);
		FsmFloat_set_Value_m1568963140(L_28, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::.ctor()
extern "C"  void GetMainCamera__ctor_m1352661876 (GetMainCamera_t2582722562 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::Reset()
extern "C"  void GetMainCamera_Reset_m3294062113 (GetMainCamera_t2582722562 * __this, const MethodInfo* method)
{
	{
		__this->set_storeGameObject_11((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMainCamera::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetMainCamera_OnEnter_m2219668043_MetadataUsageId;
extern "C"  void GetMainCamera_OnEnter_m2219668043 (GetMainCamera_t2582722562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMainCamera_OnEnter_m2219668043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * G_B2_0 = NULL;
	FsmGameObject_t1697147867 * G_B1_0 = NULL;
	GameObject_t3674682005 * G_B3_0 = NULL;
	FsmGameObject_t1697147867 * G_B3_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_storeGameObject_11();
		Camera_t2727095145 * L_1 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0025;
		}
	}
	{
		Camera_t2727095145 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = ((GameObject_t3674682005 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0026:
	{
		NullCheck(G_B3_1);
		FsmGameObject_set_Value_m297051598(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t GetMass__ctor_m704194238_MetadataUsageId;
extern "C"  void GetMass__ctor_m704194238 (GetMass_t1738563448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMass__ctor_m704194238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass::Reset()
extern "C"  void GetMass_Reset_m2645594475 (GetMass_t1738563448 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_14((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass::OnEnter()
extern "C"  void GetMass_OnEnter_m1812525845 (GetMass_t1738563448 * __this, const MethodInfo* method)
{
	{
		GetMass_DoGetMass_m1944838907(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass::DoGetMass()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t GetMass_DoGetMass_m1944838907_MetadataUsageId;
extern "C"  void GetMass_DoGetMass_m1944838907 (GetMass_t1738563448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMass_DoGetMass_m1944838907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		FsmFloat_t2134102846 * L_5 = __this->get_storeResult_14();
		Rigidbody_t3346577219 * L_6 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_6);
		float L_7 = Rigidbody_get_mass_m3953106025(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmFloat_set_Value_m1568963140(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2621392432_MethodInfo_var;
extern const uint32_t GetMass2d__ctor_m3257245132_MetadataUsageId;
extern "C"  void GetMass2d__ctor_m3257245132 (GetMass2d_t725305002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMass2d__ctor_m3257245132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2621392432(__this, /*hidden argument*/ComponentAction_1__ctor_m2621392432_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::Reset()
extern "C"  void GetMass2d_Reset_m903678073 (GetMass2d_t725305002 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_14((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::OnEnter()
extern "C"  void GetMass2d_OnEnter_m2868108963 (GetMass2d_t725305002 * __this, const MethodInfo* method)
{
	{
		GetMass2d_DoGetMass_m2747933449(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMass2d::DoGetMass()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var;
extern const uint32_t GetMass2d_DoGetMass_m2747933449_MetadataUsageId;
extern "C"  void GetMass2d_DoGetMass_m2747933449 (GetMass2d_t725305002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMass2d_DoGetMass_m2747933449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2944103819(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmFloat_t2134102846 * L_5 = __this->get_storeResult_14();
		Rigidbody2D_t1743771669 * L_6 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		NullCheck(L_6);
		float L_7 = Rigidbody2D_get_mass_m3688503547(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmFloat_set_Value_m1568963140(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t GetMaterial__ctor_m3226280555_MetadataUsageId;
extern "C"  void GetMaterial__ctor_m3226280555 (GetMaterial_t1959127339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMaterial__ctor_m3226280555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::Reset()
extern "C"  void GetMaterial_Reset_m872713496 (GetMaterial_t1959127339 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_material_15((FsmMaterial_t924399665 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_14(L_0);
		__this->set_getSharedMaterial_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::OnEnter()
extern "C"  void GetMaterial_OnEnter_m3175921538 (GetMaterial_t1959127339 * __this, const MethodInfo* method)
{
	{
		GetMaterial_DoGetMaterial_m3903327259(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::DoGetMaterial()
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern const uint32_t GetMaterial_DoGetMaterial_m3903327259_MetadataUsageId;
extern "C"  void GetMaterial_DoGetMaterial_m3903327259 (GetMaterial_t1959127339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMaterial_DoGetMaterial_m3903327259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MaterialU5BU5D_t170856778* V_1 = NULL;
	MaterialU5BU5D_t170856778* V_2 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmInt_t1596138449 * L_5 = __this->get_materialIndex_14();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		bool L_7 = __this->get_getSharedMaterial_16();
		if (L_7)
		{
			goto IL_0055;
		}
	}
	{
		FsmMaterial_t924399665 * L_8 = __this->get_material_15();
		Renderer_t3076687687 * L_9 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_9);
		Material_t3870600107 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmMaterial_set_Value_m3341549218(L_8, L_10, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0055:
	{
		FsmInt_t1596138449 * L_11 = __this->get_materialIndex_14();
		NullCheck(L_11);
		int32_t L_12 = FsmInt_get_Value_m27059446(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_008b;
		}
	}
	{
		bool L_13 = __this->get_getSharedMaterial_16();
		if (!L_13)
		{
			goto IL_008b;
		}
	}
	{
		FsmMaterial_t924399665 * L_14 = __this->get_material_15();
		Renderer_t3076687687 * L_15 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_15);
		Material_t3870600107 * L_16 = Renderer_get_sharedMaterial_m835478880(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmMaterial_set_Value_m3341549218(L_14, L_16, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_008b:
	{
		Renderer_t3076687687 * L_17 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_17);
		MaterialU5BU5D_t170856778* L_18 = Renderer_get_materials_m3755041148(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FsmInt_t1596138449 * L_19 = __this->get_materialIndex_14();
		NullCheck(L_19);
		int32_t L_20 = FsmInt_get_Value_m27059446(L_19, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))) <= ((int32_t)L_20)))
		{
			goto IL_00e8;
		}
	}
	{
		bool L_21 = __this->get_getSharedMaterial_16();
		if (L_21)
		{
			goto IL_00e8;
		}
	}
	{
		Renderer_t3076687687 * L_22 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_22);
		MaterialU5BU5D_t170856778* L_23 = Renderer_get_materials_m3755041148(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		FsmMaterial_t924399665 * L_24 = __this->get_material_15();
		MaterialU5BU5D_t170856778* L_25 = V_1;
		FsmInt_t1596138449 * L_26 = __this->get_materialIndex_14();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		int32_t L_28 = L_27;
		Material_t3870600107 * L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_24);
		FsmMaterial_set_Value_m3341549218(L_24, L_29, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_30 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_31 = V_1;
		NullCheck(L_30);
		Renderer_set_materials_m268031319(L_30, L_31, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_00e8:
	{
		Renderer_t3076687687 * L_32 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_32);
		MaterialU5BU5D_t170856778* L_33 = Renderer_get_materials_m3755041148(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		FsmInt_t1596138449 * L_34 = __this->get_materialIndex_14();
		NullCheck(L_34);
		int32_t L_35 = FsmInt_get_Value_m27059446(L_34, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length))))) <= ((int32_t)L_35)))
		{
			goto IL_0140;
		}
	}
	{
		bool L_36 = __this->get_getSharedMaterial_16();
		if (!L_36)
		{
			goto IL_0140;
		}
	}
	{
		Renderer_t3076687687 * L_37 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_37);
		MaterialU5BU5D_t170856778* L_38 = Renderer_get_sharedMaterials_m1981818007(L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		FsmMaterial_t924399665 * L_39 = __this->get_material_15();
		MaterialU5BU5D_t170856778* L_40 = V_2;
		FsmInt_t1596138449 * L_41 = __this->get_materialIndex_14();
		NullCheck(L_41);
		int32_t L_42 = FsmInt_get_Value_m27059446(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_42);
		int32_t L_43 = L_42;
		Material_t3870600107 * L_44 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_39);
		FsmMaterial_set_Value_m3341549218(L_39, L_44, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_45 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_46 = V_2;
		NullCheck(L_45);
		Renderer_set_sharedMaterials_m1255100914(L_45, L_46, /*hidden argument*/NULL);
	}

IL_0140:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m994342140_MethodInfo_var;
extern const uint32_t GetMaterialTexture__ctor_m368015104_MetadataUsageId;
extern "C"  void GetMaterialTexture__ctor_m368015104 (GetMaterialTexture_t3439956006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMaterialTexture__ctor_m368015104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m994342140(__this, /*hidden argument*/ComponentAction_1__ctor_m994342140_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::Reset()
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t GetMaterialTexture_Reset_m2309415341_MetadataUsageId;
extern "C"  void GetMaterialTexture_Reset_m2309415341 (GetMaterialTexture_t3439956006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMaterialTexture_Reset_m2309415341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_materialIndex_14(L_0);
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral558922319, /*hidden argument*/NULL);
		__this->set_namedTexture_15(L_1);
		__this->set_storedTexture_16((FsmTexture_t3073272573 *)NULL);
		__this->set_getFromSharedMaterial_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::OnEnter()
extern "C"  void GetMaterialTexture_OnEnter_m866925271 (GetMaterialTexture_t3439956006 * __this, const MethodInfo* method)
{
	{
		GetMaterialTexture_DoGetMaterialTexture_m319886765(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::DoGetMaterialTexture()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_renderer_m773530769_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern const uint32_t GetMaterialTexture_DoGetMaterialTexture_m319886765_MetadataUsageId;
extern "C"  void GetMaterialTexture_DoGetMaterialTexture_m319886765 (GetMaterialTexture_t3439956006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMaterialTexture_DoGetMaterialTexture_m319886765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	String_t* V_1 = NULL;
	MaterialU5BU5D_t170856778* V_2 = NULL;
	MaterialU5BU5D_t170856778* V_3 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m3595067967(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m3595067967_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmString_t952858651 * L_5 = __this->get_namedTexture_15();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0041;
		}
	}
	{
		V_1 = _stringLiteral558922319;
	}

IL_0041:
	{
		FsmInt_t1596138449 * L_10 = __this->get_materialIndex_14();
		NullCheck(L_10);
		int32_t L_11 = FsmInt_get_Value_m27059446(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007d;
		}
	}
	{
		bool L_12 = __this->get_getFromSharedMaterial_17();
		if (L_12)
		{
			goto IL_007d;
		}
	}
	{
		FsmTexture_t3073272573 * L_13 = __this->get_storedTexture_16();
		Renderer_t3076687687 * L_14 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_14);
		Material_t3870600107 * L_15 = Renderer_get_material_m2720864603(L_14, /*hidden argument*/NULL);
		String_t* L_16 = V_1;
		NullCheck(L_15);
		Texture_t2526458961 * L_17 = Material_GetTexture_m1284113328(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmTexture_set_Value_m2261522310(L_13, L_17, /*hidden argument*/NULL);
		goto IL_018e;
	}

IL_007d:
	{
		FsmInt_t1596138449 * L_18 = __this->get_materialIndex_14();
		NullCheck(L_18);
		int32_t L_19 = FsmInt_get_Value_m27059446(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b9;
		}
	}
	{
		bool L_20 = __this->get_getFromSharedMaterial_17();
		if (!L_20)
		{
			goto IL_00b9;
		}
	}
	{
		FsmTexture_t3073272573 * L_21 = __this->get_storedTexture_16();
		Renderer_t3076687687 * L_22 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_22);
		Material_t3870600107 * L_23 = Renderer_get_sharedMaterial_m835478880(L_22, /*hidden argument*/NULL);
		String_t* L_24 = V_1;
		NullCheck(L_23);
		Texture_t2526458961 * L_25 = Material_GetTexture_m1284113328(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		FsmTexture_set_Value_m2261522310(L_21, L_25, /*hidden argument*/NULL);
		goto IL_018e;
	}

IL_00b9:
	{
		Renderer_t3076687687 * L_26 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_26);
		MaterialU5BU5D_t170856778* L_27 = Renderer_get_materials_m3755041148(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		FsmInt_t1596138449 * L_28 = __this->get_materialIndex_14();
		NullCheck(L_28);
		int32_t L_29 = FsmInt_get_Value_m27059446(L_28, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))) <= ((int32_t)L_29)))
		{
			goto IL_0126;
		}
	}
	{
		bool L_30 = __this->get_getFromSharedMaterial_17();
		if (L_30)
		{
			goto IL_0126;
		}
	}
	{
		Renderer_t3076687687 * L_31 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_31);
		MaterialU5BU5D_t170856778* L_32 = Renderer_get_materials_m3755041148(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		FsmTexture_t3073272573 * L_33 = __this->get_storedTexture_16();
		Renderer_t3076687687 * L_34 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_34);
		MaterialU5BU5D_t170856778* L_35 = Renderer_get_materials_m3755041148(L_34, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_36 = __this->get_materialIndex_14();
		NullCheck(L_36);
		int32_t L_37 = FsmInt_get_Value_m27059446(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_37);
		int32_t L_38 = L_37;
		Material_t3870600107 * L_39 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		String_t* L_40 = V_1;
		NullCheck(L_39);
		Texture_t2526458961 * L_41 = Material_GetTexture_m1284113328(L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_33);
		FsmTexture_set_Value_m2261522310(L_33, L_41, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_42 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_43 = V_2;
		NullCheck(L_42);
		Renderer_set_materials_m268031319(L_42, L_43, /*hidden argument*/NULL);
		goto IL_018e;
	}

IL_0126:
	{
		Renderer_t3076687687 * L_44 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_44);
		MaterialU5BU5D_t170856778* L_45 = Renderer_get_materials_m3755041148(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		FsmInt_t1596138449 * L_46 = __this->get_materialIndex_14();
		NullCheck(L_46);
		int32_t L_47 = FsmInt_get_Value_m27059446(L_46, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))) <= ((int32_t)L_47)))
		{
			goto IL_018e;
		}
	}
	{
		bool L_48 = __this->get_getFromSharedMaterial_17();
		if (!L_48)
		{
			goto IL_018e;
		}
	}
	{
		Renderer_t3076687687 * L_49 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_49);
		MaterialU5BU5D_t170856778* L_50 = Renderer_get_sharedMaterials_m1981818007(L_49, /*hidden argument*/NULL);
		V_3 = L_50;
		FsmTexture_t3073272573 * L_51 = __this->get_storedTexture_16();
		Renderer_t3076687687 * L_52 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		NullCheck(L_52);
		MaterialU5BU5D_t170856778* L_53 = Renderer_get_sharedMaterials_m1981818007(L_52, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_54 = __this->get_materialIndex_14();
		NullCheck(L_54);
		int32_t L_55 = FsmInt_get_Value_m27059446(L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_55);
		int32_t L_56 = L_55;
		Material_t3870600107 * L_57 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		String_t* L_58 = V_1;
		NullCheck(L_57);
		Texture_t2526458961 * L_59 = Material_GetTexture_m1284113328(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_51);
		FsmTexture_set_Value_m2261522310(L_51, L_59, /*hidden argument*/NULL);
		Renderer_t3076687687 * L_60 = ComponentAction_1_get_renderer_m773530769(__this, /*hidden argument*/ComponentAction_1_get_renderer_m773530769_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_61 = V_3;
		NullCheck(L_60);
		Renderer_set_materials_m268031319(L_60, L_61, /*hidden argument*/NULL);
	}

IL_018e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::.ctor()
extern "C"  void GetMouseButton__ctor_m2319036573 (GetMouseButton_t2789993961 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::Reset()
extern "C"  void GetMouseButton_Reset_m4260436810 (GetMouseButton_t2789993961 * __this, const MethodInfo* method)
{
	{
		__this->set_button_11(0);
		__this->set_storeResult_12((FsmBool_t1075959796 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::OnEnter()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseButton_OnEnter_m3192815924_MetadataUsageId;
extern "C"  void GetMouseButton_OnEnter_m3192815924 (GetMouseButton_t2789993961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseButton_OnEnter_m3192815924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_12();
		int32_t L_1 = __this->get_button_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::OnUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseButton_OnUpdate_m3621572495_MetadataUsageId;
extern "C"  void GetMouseButton_OnUpdate_m3621572495 (GetMouseButton_t2789993961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseButton_OnUpdate_m3621572495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmBool_t1075959796 * L_0 = __this->get_storeResult_12();
		int32_t L_1 = __this->get_button_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmBool_set_Value_m1126216340(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::.ctor()
extern "C"  void GetMouseButtonDown__ctor_m2659595547 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::Reset()
extern "C"  void GetMouseButtonDown_Reset_m306028488 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method)
{
	{
		__this->set_button_11(0);
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_inUpdateOnly_14((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnEnter()
extern "C"  void GetMouseButtonDown_OnEnter_m4052475442 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_inUpdateOnly_14();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetMouseButtonDown_DoGetMouseButtonDown_m3528595575(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnUpdate()
extern "C"  void GetMouseButtonDown_OnUpdate_m206246481 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method)
{
	{
		GetMouseButtonDown_DoGetMouseButtonDown_m3528595575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::DoGetMouseButtonDown()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseButtonDown_DoGetMouseButtonDown_m3528595575_MetadataUsageId;
extern "C"  void GetMouseButtonDown_DoGetMouseButtonDown_m3528595575 (GetMouseButtonDown_t1009678251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseButtonDown_DoGetMouseButtonDown_m3528595575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_button_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = __this->get_sendEvent_12();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_13();
		bool L_6 = V_0;
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::.ctor()
extern "C"  void GetMouseButtonUp__ctor_m3810585026 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::Reset()
extern "C"  void GetMouseButtonUp_Reset_m1457017967 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method)
{
	{
		__this->set_button_11(0);
		__this->set_sendEvent_12((FsmEvent_t2133468028 *)NULL);
		__this->set_storeResult_13((FsmBool_t1075959796 *)NULL);
		__this->set_inUpdateOnly_14((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::OnEnter()
extern "C"  void GetMouseButtonUp_OnEnter_m2051802393 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_inUpdateOnly_14();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetMouseButtonUp_DoGetMouseButtonUp_m1387036649(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::OnUpdate()
extern "C"  void GetMouseButtonUp_OnUpdate_m2609891402 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method)
{
	{
		GetMouseButtonUp_DoGetMouseButtonUp_m1387036649(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::DoGetMouseButtonUp()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseButtonUp_DoGetMouseButtonUp_m1387036649_MetadataUsageId;
extern "C"  void GetMouseButtonUp_DoGetMouseButtonUp_m1387036649 (GetMouseButtonUp_t1832714532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseButtonUp_DoGetMouseButtonUp_m1387036649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_button_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_4 = __this->get_sendEvent_12();
		NullCheck(L_3);
		Fsm_Event_m625948263(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		FsmBool_t1075959796 * L_5 = __this->get_storeResult_13();
		bool L_6 = V_0;
		NullCheck(L_5);
		FsmBool_set_Value_m1126216340(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::.ctor()
extern "C"  void GetMouseX__ctor_m795541631 (GetMouseX_t738295447 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::Reset()
extern "C"  void GetMouseX_Reset_m2736941868 (GetMouseX_t738295447 * __this, const MethodInfo* method)
{
	{
		__this->set_storeResult_11((FsmFloat_t2134102846 *)NULL);
		__this->set_normalize_12((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnEnter()
extern "C"  void GetMouseX_OnEnter_m3698024598 (GetMouseX_t738295447 * __this, const MethodInfo* method)
{
	{
		GetMouseX_DoGetMouseX_m2266641435(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnUpdate()
extern "C"  void GetMouseX_OnUpdate_m2103172205 (GetMouseX_t738295447 * __this, const MethodInfo* method)
{
	{
		GetMouseX_DoGetMouseX_m2266641435(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::DoGetMouseX()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseX_DoGetMouseX_m2266641435_MetadataUsageId;
extern "C"  void GetMouseX_DoGetMouseX_m2266641435 (GetMouseX_t738295447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseX_DoGetMouseX_m2266641435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeResult_11();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_1 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		V_0 = L_2;
		bool L_3 = __this->get_normalize_12();
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		float L_4 = V_0;
		int32_t L_5 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_4/(float)(((float)((float)L_5)))));
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeResult_11();
		float L_7 = V_0;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::.ctor()
extern "C"  void GetMouseY__ctor_m599028126 (GetMouseY_t738295448 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::Reset()
extern "C"  void GetMouseY_Reset_m2540428363 (GetMouseY_t738295448 * __this, const MethodInfo* method)
{
	{
		__this->set_storeResult_11((FsmFloat_t2134102846 *)NULL);
		__this->set_normalize_12((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnEnter()
extern "C"  void GetMouseY_OnEnter_m3827107317 (GetMouseY_t738295448 * __this, const MethodInfo* method)
{
	{
		GetMouseY_DoGetMouseY_m1756108219(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnUpdate()
extern "C"  void GetMouseY_OnUpdate_m1809769198 (GetMouseY_t738295448 * __this, const MethodInfo* method)
{
	{
		GetMouseY_DoGetMouseY_m1756108219(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::DoGetMouseY()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetMouseY_DoGetMouseY_m1756108219_MetadataUsageId;
extern "C"  void GetMouseY_DoGetMouseY_m1756108219 (GetMouseY_t738295448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetMouseY_DoGetMouseY_m1756108219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeResult_11();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_1 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_2();
		V_0 = L_2;
		bool L_3 = __this->get_normalize_12();
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		float L_4 = V_0;
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_4/(float)(((float)((float)L_5)))));
	}

IL_002d:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeResult_11();
		float L_7 = V_0;
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetName::.ctor()
extern "C"  void GetName__ctor_m1058786567 (GetName_t1738593039 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetName::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern const uint32_t GetName_Reset_m3000186804_MetadataUsageId;
extern "C"  void GetName_Reset_m3000186804 (GetName_t1738593039 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetName_Reset_m3000186804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_gameObject_11(L_2);
		__this->set_storeName_12((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetName::OnEnter()
extern "C"  void GetName_OnEnter_m3273337630 (GetName_t1738593039 * __this, const MethodInfo* method)
{
	{
		GetName_DoGetGameObjectName_m200644876(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetName::OnUpdate()
extern "C"  void GetName_OnUpdate_m1822778085 (GetName_t1738593039 * __this, const MethodInfo* method)
{
	{
		GetName_DoGetGameObjectName_m200644876(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetName::DoGetGameObjectName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetName_DoGetGameObjectName_m200644876_MetadataUsageId;
extern "C"  void GetName_DoGetGameObjectName_m200644876 (GetName_t1738593039 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetName_DoGetGameObjectName_m200644876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmString_t952858651 * G_B2_0 = NULL;
	FsmString_t952858651 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	FsmString_t952858651 * G_B3_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmString_t952858651 * L_2 = __this->get_storeName_12();
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_2;
		if (!L_4)
		{
			G_B2_0 = L_2;
			goto IL_0029;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m3709440845(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_002e;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_7;
		G_B3_1 = G_B2_0;
	}

IL_002e:
	{
		NullCheck(G_B3_1);
		FsmString_set_Value_m829393196(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::.ctor()
extern "C"  void GetNextChild__ctor_m3560540011 (GetNextChild_t464321883 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::Reset()
extern "C"  void GetNextChild_Reset_m1206972952 (GetNextChild_t464321883 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeNextChild_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_loopEvent_13((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_14((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::OnEnter()
extern "C"  void GetNextChild_OnEnter_m2276711554 (GetNextChild_t464321883 * __this, const MethodInfo* method)
{
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		GetNextChild_DoGetNextChild_m3232394895(__this, L_2, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::DoGetNextChild(UnityEngine.GameObject)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetNextChild_DoGetNextChild_m3232394895_MetadataUsageId;
extern "C"  void GetNextChild_DoGetNextChild_m3232394895 (GetNextChild_t464321883 * __this, GameObject_t3674682005 * ___parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextChild_DoGetNextChild_m3232394895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		GameObject_t3674682005 * L_2 = __this->get_go_15();
		GameObject_t3674682005 * L_3 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t3674682005 * L_5 = ___parent0;
		__this->set_go_15(L_5);
		__this->set_nextChildIndex_16(0);
	}

IL_002c:
	{
		int32_t L_6 = __this->get_nextChildIndex_16();
		GameObject_t3674682005 * L_7 = __this->get_go_15();
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = GameObject_get_transform_m1278640159(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m2107810675(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_9)))
		{
			goto IL_0060;
		}
	}
	{
		__this->set_nextChildIndex_16(0);
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_11 = __this->get_finishedEvent_14();
		NullCheck(L_10);
		Fsm_Event_m625948263(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		FsmGameObject_t1697147867 * L_12 = __this->get_storeNextChild_12();
		GameObject_t3674682005 * L_13 = ___parent0;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_nextChildIndex_16();
		NullCheck(L_14);
		Transform_t1659122786 * L_16 = Transform_GetChild_m4040462992(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = Component_get_gameObject_m1170635899(L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmGameObject_set_Value_m297051598(L_12, L_17, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_nextChildIndex_16();
		GameObject_t3674682005 * L_19 = __this->get_go_15();
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = Transform_get_childCount_m2107810675(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_18) < ((int32_t)L_21)))
		{
			goto IL_00b5;
		}
	}
	{
		__this->set_nextChildIndex_16(0);
		Fsm_t1527112426 * L_22 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_23 = __this->get_finishedEvent_14();
		NullCheck(L_22);
		Fsm_Event_m625948263(L_22, L_23, /*hidden argument*/NULL);
		return;
	}

IL_00b5:
	{
		int32_t L_24 = __this->get_nextChildIndex_16();
		__this->set_nextChildIndex_16(((int32_t)((int32_t)L_24+(int32_t)1)));
		FsmEvent_t2133468028 * L_25 = __this->get_loopEvent_13();
		if (!L_25)
		{
			goto IL_00df;
		}
	}
	{
		Fsm_t1527112426 * L_26 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_27 = __this->get_loopEvent_13();
		NullCheck(L_26);
		Fsm_Event_m625948263(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::.ctor()
extern "C"  void GetNextLineCast2d__ctor_m3834173722 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t GetNextLineCast2d_Reset_m1480606663_MetadataUsageId;
extern "C"  void GetNextLineCast2d_Reset_m1480606663 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextLineCast2d_Reset_m1480606663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		__this->set_fromGameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_fromPosition_12(L_2);
		__this->set_toGameObject_13((FsmGameObject_t1697147867 *)NULL);
		FsmVector2_t533912881 * L_3 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmVector2_t533912881 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_5 = V_0;
		__this->set_toPosition_14(L_5);
		FsmInt_t1596138449 * L_6 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmInt_t1596138449 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = V_1;
		__this->set_minDepth_15(L_8);
		FsmInt_t1596138449 * L_9 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmInt_t1596138449 * L_10 = V_1;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_11 = V_1;
		__this->set_maxDepth_16(L_11);
		__this->set_layerMask_17(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_12 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_18(L_12);
		__this->set_collidersCount_19((FsmInt_t1596138449 *)NULL);
		__this->set_storeNextCollider_20((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeNextHitPoint_21((FsmVector2_t533912881 *)NULL);
		__this->set_storeNextHitNormal_22((FsmVector2_t533912881 *)NULL);
		__this->set_storeNextHitDistance_23((FsmFloat_t2134102846 *)NULL);
		__this->set_loopEvent_24((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_25((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::OnEnter()
extern "C"  void GetNextLineCast2d_OnEnter_m3245702769 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method)
{
	{
		RaycastHit2DU5BU5D_t889400257* L_0 = __this->get_hits_26();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		RaycastHit2DU5BU5D_t889400257* L_1 = GetNextLineCast2d_GetLineCastAll_m1089683514(__this, /*hidden argument*/NULL);
		__this->set_hits_26(L_1);
		RaycastHit2DU5BU5D_t889400257* L_2 = __this->get_hits_26();
		NullCheck(L_2);
		__this->set_colliderCount_27((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		FsmInt_t1596138449 * L_3 = __this->get_collidersCount_19();
		int32_t L_4 = __this->get_colliderCount_27();
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GetNextLineCast2d_DoGetNextCollider_m3529992618(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::DoGetNextCollider()
extern Il2CppClass* RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var;
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t GetNextLineCast2d_DoGetNextCollider_m3529992618_MetadataUsageId;
extern "C"  void GetNextLineCast2d_DoGetNextCollider_m3529992618 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextLineCast2d_DoGetNextCollider_m3529992618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_nextColliderIndex_28();
		int32_t L_1 = __this->get_colliderCount_27();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0036;
		}
	}
	{
		__this->set_hits_26(((RaycastHit2DU5BU5D_t889400257*)SZArrayNew(RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_nextColliderIndex_28(0);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_25();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		RaycastHit2DU5BU5D_t889400257* L_5 = __this->get_hits_26();
		int32_t L_6 = __this->get_nextColliderIndex_28();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm_RecordLastRaycastHit2DInfo_m3252930453(NULL /*static, unused*/, L_4, (*(RaycastHit2D_t1374744384 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_7 = __this->get_storeNextCollider_20();
		RaycastHit2DU5BU5D_t889400257* L_8 = __this->get_hits_26();
		int32_t L_9 = __this->get_nextColliderIndex_28();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Collider2D_t1552025098 * L_10 = RaycastHit2D_get_collider_m789902306(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmGameObject_set_Value_m297051598(L_7, L_11, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_12 = __this->get_storeNextHitPoint_21();
		RaycastHit2DU5BU5D_t889400257* L_13 = __this->get_hits_26();
		int32_t L_14 = __this->get_nextColliderIndex_28();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Vector2_t4282066565  L_15 = RaycastHit2D_get_point_m2072691227(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmVector2_set_Value_m2900659718(L_12, L_15, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_16 = __this->get_storeNextHitNormal_22();
		RaycastHit2DU5BU5D_t889400257* L_17 = __this->get_hits_26();
		int32_t L_18 = __this->get_nextColliderIndex_28();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Vector2_t4282066565  L_19 = RaycastHit2D_get_normal_m894503390(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmVector2_set_Value_m2900659718(L_16, L_19, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_storeNextHitDistance_23();
		RaycastHit2DU5BU5D_t889400257* L_21 = __this->get_hits_26();
		int32_t L_22 = __this->get_nextColliderIndex_28();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		float L_23 = RaycastHit2D_get_fraction_m2313516650(((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22))), /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmFloat_set_Value_m1568963140(L_20, L_23, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_nextColliderIndex_28();
		int32_t L_25 = __this->get_colliderCount_27();
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0116;
		}
	}
	{
		__this->set_hits_26(((RaycastHit2DU5BU5D_t889400257*)SZArrayNew(RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_nextColliderIndex_28(0);
		Fsm_t1527112426 * L_26 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_27 = __this->get_finishedEvent_25();
		NullCheck(L_26);
		Fsm_Event_m625948263(L_26, L_27, /*hidden argument*/NULL);
		return;
	}

IL_0116:
	{
		int32_t L_28 = __this->get_nextColliderIndex_28();
		__this->set_nextColliderIndex_28(((int32_t)((int32_t)L_28+(int32_t)1)));
		FsmEvent_t2133468028 * L_29 = __this->get_loopEvent_24();
		if (!L_29)
		{
			goto IL_0140;
		}
	}
	{
		Fsm_t1527112426 * L_30 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_31 = __this->get_loopEvent_24();
		NullCheck(L_30);
		Fsm_Event_m625948263(L_30, L_31, /*hidden argument*/NULL);
	}

IL_0140:
	{
		return;
	}
}
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextLineCast2d::GetLineCastAll()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t GetNextLineCast2d_GetLineCastAll_m1089683514_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* GetNextLineCast2d_GetLineCastAll_m1089683514 (GetNextLineCast2d_t3332969372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextLineCast2d_GetLineCastAll_m1089683514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t3674682005 * V_1 = NULL;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t3674682005 * V_3 = NULL;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float G_B10_0 = 0.0f;
	float G_B13_0 = 0.0f;
	{
		FsmVector2_t533912881 * L_0 = __this->get_fromPosition_12();
		NullCheck(L_0);
		Vector2_t4282066565  L_1 = FsmVector2_get_Value_m1313754285(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_3 = __this->get_fromGameObject_11();
		NullCheck(L_2);
		GameObject_t3674682005 * L_4 = Fsm_GetOwnerDefaultTarget_m846013999(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t3674682005 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		Vector2_t4282066565 * L_7 = (&V_0);
		float L_8 = L_7->get_x_1();
		GameObject_t3674682005 * L_9 = V_1;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_6 = L_11;
		float L_12 = (&V_6)->get_x_1();
		L_7->set_x_1(((float)((float)L_8+(float)L_12)));
		Vector2_t4282066565 * L_13 = (&V_0);
		float L_14 = L_13->get_y_2();
		GameObject_t3674682005 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_7 = L_17;
		float L_18 = (&V_7)->get_y_2();
		L_13->set_y_2(((float)((float)L_14+(float)L_18)));
	}

IL_006e:
	{
		FsmVector2_t533912881 * L_19 = __this->get_toPosition_14();
		NullCheck(L_19);
		Vector2_t4282066565  L_20 = FsmVector2_get_Value_m1313754285(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		FsmGameObject_t1697147867 * L_21 = __this->get_toGameObject_13();
		NullCheck(L_21);
		GameObject_t3674682005 * L_22 = FsmGameObject_get_Value_m673294275(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		GameObject_t3674682005 * L_23 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d6;
		}
	}
	{
		Vector2_t4282066565 * L_25 = (&V_2);
		float L_26 = L_25->get_x_1();
		GameObject_t3674682005 * L_27 = V_3;
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		float L_30 = (&V_8)->get_x_1();
		L_25->set_x_1(((float)((float)L_26+(float)L_30)));
		Vector2_t4282066565 * L_31 = (&V_2);
		float L_32 = L_31->get_y_2();
		GameObject_t3674682005 * L_33 = V_3;
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = GameObject_get_transform_m1278640159(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t4282066566  L_35 = Transform_get_position_m2211398607(L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = (&V_9)->get_y_2();
		L_31->set_y_2(((float)((float)L_32+(float)L_36)));
	}

IL_00d6:
	{
		FsmInt_t1596138449 * L_37 = __this->get_minDepth_15();
		NullCheck(L_37);
		bool L_38 = NamedVariable_get_IsNone_m281035543(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0114;
		}
	}
	{
		FsmInt_t1596138449 * L_39 = __this->get_maxDepth_16();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0114;
		}
	}
	{
		Vector2_t4282066565  L_41 = V_0;
		Vector2_t4282066565  L_42 = V_2;
		FsmIntU5BU5D_t1976821196* L_43 = __this->get_layerMask_17();
		FsmBool_t1075959796 * L_44 = __this->get_invertMask_18();
		NullCheck(L_44);
		bool L_45 = FsmBool_get_Value_m3101329097(L_44, /*hidden argument*/NULL);
		int32_t L_46 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_47 = Physics2D_LinecastAll_m740601359(NULL /*static, unused*/, L_41, L_42, L_46, /*hidden argument*/NULL);
		return L_47;
	}

IL_0114:
	{
		FsmInt_t1596138449 * L_48 = __this->get_minDepth_15();
		NullCheck(L_48);
		bool L_49 = NamedVariable_get_IsNone_m281035543(L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_012e;
		}
	}
	{
		G_B10_0 = (-std::numeric_limits<float>::infinity());
		goto IL_013a;
	}

IL_012e:
	{
		FsmInt_t1596138449 * L_50 = __this->get_minDepth_15();
		NullCheck(L_50);
		int32_t L_51 = FsmInt_get_Value_m27059446(L_50, /*hidden argument*/NULL);
		G_B10_0 = (((float)((float)L_51)));
	}

IL_013a:
	{
		V_4 = G_B10_0;
		FsmInt_t1596138449 * L_52 = __this->get_maxDepth_16();
		NullCheck(L_52);
		bool L_53 = NamedVariable_get_IsNone_m281035543(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0156;
		}
	}
	{
		G_B13_0 = (std::numeric_limits<float>::infinity());
		goto IL_0162;
	}

IL_0156:
	{
		FsmInt_t1596138449 * L_54 = __this->get_maxDepth_16();
		NullCheck(L_54);
		int32_t L_55 = FsmInt_get_Value_m27059446(L_54, /*hidden argument*/NULL);
		G_B13_0 = (((float)((float)L_55)));
	}

IL_0162:
	{
		V_5 = G_B13_0;
		Vector2_t4282066565  L_56 = V_0;
		Vector2_t4282066565  L_57 = V_2;
		FsmIntU5BU5D_t1976821196* L_58 = __this->get_layerMask_17();
		FsmBool_t1075959796 * L_59 = __this->get_invertMask_18();
		NullCheck(L_59);
		bool L_60 = FsmBool_get_Value_m3101329097(L_59, /*hidden argument*/NULL);
		int32_t L_61 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_58, L_60, /*hidden argument*/NULL);
		float L_62 = V_4;
		float L_63 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_64 = Physics2D_LinecastAll_m2367926937(NULL /*static, unused*/, L_56, L_57, L_61, L_62, L_63, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::.ctor()
extern "C"  void GetNextOverlapArea2d__ctor_m3960247521 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapArea2d_Reset_m1606680462_MetadataUsageId;
extern "C"  void GetNextOverlapArea2d_Reset_m1606680462 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapArea2d_Reset_m1606680462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		__this->set_firstCornerGameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_firstCornerPosition_12(L_2);
		__this->set_secondCornerGameObject_13((FsmGameObject_t1697147867 *)NULL);
		FsmVector2_t533912881 * L_3 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmVector2_t533912881 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_5 = V_0;
		__this->set_secondCornerPosition_14(L_5);
		FsmInt_t1596138449 * L_6 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmInt_t1596138449 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = V_1;
		__this->set_minDepth_15(L_8);
		FsmInt_t1596138449 * L_9 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmInt_t1596138449 * L_10 = V_1;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_11 = V_1;
		__this->set_maxDepth_16(L_11);
		__this->set_layerMask_17(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_12 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_18(L_12);
		__this->set_collidersCount_19((FsmInt_t1596138449 *)NULL);
		__this->set_storeNextCollider_20((FsmGameObject_t1697147867 *)NULL);
		__this->set_loopEvent_21((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_22((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::OnEnter()
extern "C"  void GetNextOverlapArea2d_OnEnter_m4143539320 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method)
{
	{
		Collider2DU5BU5D_t1758559887* L_0 = __this->get_colliders_23();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		Collider2DU5BU5D_t1758559887* L_1 = GetNextOverlapArea2d_GetOverlapAreaAll_m1375757892(__this, /*hidden argument*/NULL);
		__this->set_colliders_23(L_1);
		Collider2DU5BU5D_t1758559887* L_2 = __this->get_colliders_23();
		NullCheck(L_2);
		__this->set_colliderCount_24((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		FsmInt_t1596138449 * L_3 = __this->get_collidersCount_19();
		int32_t L_4 = __this->get_colliderCount_24();
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GetNextOverlapArea2d_DoGetNextCollider_m3406082801(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::DoGetNextCollider()
extern "C"  void GetNextOverlapArea2d_DoGetNextCollider_m3406082801 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nextColliderIndex_25();
		int32_t L_1 = __this->get_colliderCount_24();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		__this->set_nextColliderIndex_25(0);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_22();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_002a:
	{
		FsmGameObject_t1697147867 * L_4 = __this->get_storeNextCollider_20();
		Collider2DU5BU5D_t1758559887* L_5 = __this->get_colliders_23();
		int32_t L_6 = __this->get_nextColliderIndex_25();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Collider2D_t1552025098 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmGameObject_set_Value_m297051598(L_4, L_9, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_nextColliderIndex_25();
		int32_t L_11 = __this->get_colliderCount_24();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0071;
		}
	}
	{
		__this->set_nextColliderIndex_25(0);
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_finishedEvent_22();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0071:
	{
		int32_t L_14 = __this->get_nextColliderIndex_25();
		__this->set_nextColliderIndex_25(((int32_t)((int32_t)L_14+(int32_t)1)));
		FsmEvent_t2133468028 * L_15 = __this->get_loopEvent_21();
		if (!L_15)
		{
			goto IL_009b;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_loopEvent_21();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::GetOverlapAreaAll()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapArea2d_GetOverlapAreaAll_m1375757892_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapArea2d_GetOverlapAreaAll_m1375757892 (GetNextOverlapArea2d_t1935424293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapArea2d_GetOverlapAreaAll_m1375757892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t3674682005 * V_2 = NULL;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t4282066566  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float G_B10_0 = 0.0f;
	float G_B13_0 = 0.0f;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_firstCornerGameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmVector2_t533912881 * L_3 = __this->get_firstCornerPosition_12();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		Vector2_t4282066565 * L_7 = (&V_1);
		float L_8 = L_7->get_x_1();
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_6 = L_11;
		float L_12 = (&V_6)->get_x_1();
		L_7->set_x_1(((float)((float)L_8+(float)L_12)));
		Vector2_t4282066565 * L_13 = (&V_1);
		float L_14 = L_13->get_y_2();
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_7 = L_17;
		float L_18 = (&V_7)->get_y_2();
		L_13->set_y_2(((float)((float)L_14+(float)L_18)));
	}

IL_006e:
	{
		FsmGameObject_t1697147867 * L_19 = __this->get_secondCornerGameObject_13();
		NullCheck(L_19);
		GameObject_t3674682005 * L_20 = FsmGameObject_get_Value_m673294275(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		FsmVector2_t533912881 * L_21 = __this->get_secondCornerPosition_14();
		NullCheck(L_21);
		Vector2_t4282066565  L_22 = FsmVector2_get_Value_m1313754285(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		GameObject_t3674682005 * L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d6;
		}
	}
	{
		Vector2_t4282066565 * L_25 = (&V_3);
		float L_26 = L_25->get_x_1();
		GameObject_t3674682005 * L_27 = V_2;
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_position_m2211398607(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		float L_30 = (&V_8)->get_x_1();
		L_25->set_x_1(((float)((float)L_26+(float)L_30)));
		Vector2_t4282066565 * L_31 = (&V_3);
		float L_32 = L_31->get_y_2();
		GameObject_t3674682005 * L_33 = V_2;
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = GameObject_get_transform_m1278640159(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t4282066566  L_35 = Transform_get_position_m2211398607(L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = (&V_9)->get_y_2();
		L_31->set_y_2(((float)((float)L_32+(float)L_36)));
	}

IL_00d6:
	{
		FsmInt_t1596138449 * L_37 = __this->get_minDepth_15();
		NullCheck(L_37);
		bool L_38 = NamedVariable_get_IsNone_m281035543(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0114;
		}
	}
	{
		FsmInt_t1596138449 * L_39 = __this->get_maxDepth_16();
		NullCheck(L_39);
		bool L_40 = NamedVariable_get_IsNone_m281035543(L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0114;
		}
	}
	{
		Vector2_t4282066565  L_41 = V_1;
		Vector2_t4282066565  L_42 = V_3;
		FsmIntU5BU5D_t1976821196* L_43 = __this->get_layerMask_17();
		FsmBool_t1075959796 * L_44 = __this->get_invertMask_18();
		NullCheck(L_44);
		bool L_45 = FsmBool_get_Value_m3101329097(L_44, /*hidden argument*/NULL);
		int32_t L_46 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_47 = Physics2D_OverlapAreaAll_m2026172650(NULL /*static, unused*/, L_41, L_42, L_46, /*hidden argument*/NULL);
		return L_47;
	}

IL_0114:
	{
		FsmInt_t1596138449 * L_48 = __this->get_minDepth_15();
		NullCheck(L_48);
		bool L_49 = NamedVariable_get_IsNone_m281035543(L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_012e;
		}
	}
	{
		G_B10_0 = (-std::numeric_limits<float>::infinity());
		goto IL_013a;
	}

IL_012e:
	{
		FsmInt_t1596138449 * L_50 = __this->get_minDepth_15();
		NullCheck(L_50);
		int32_t L_51 = FsmInt_get_Value_m27059446(L_50, /*hidden argument*/NULL);
		G_B10_0 = (((float)((float)L_51)));
	}

IL_013a:
	{
		V_4 = G_B10_0;
		FsmInt_t1596138449 * L_52 = __this->get_maxDepth_16();
		NullCheck(L_52);
		bool L_53 = NamedVariable_get_IsNone_m281035543(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0156;
		}
	}
	{
		G_B13_0 = (std::numeric_limits<float>::infinity());
		goto IL_0162;
	}

IL_0156:
	{
		FsmInt_t1596138449 * L_54 = __this->get_maxDepth_16();
		NullCheck(L_54);
		int32_t L_55 = FsmInt_get_Value_m27059446(L_54, /*hidden argument*/NULL);
		G_B13_0 = (((float)((float)L_55)));
	}

IL_0162:
	{
		V_5 = G_B13_0;
		Vector2_t4282066565  L_56 = V_1;
		Vector2_t4282066565  L_57 = V_3;
		FsmIntU5BU5D_t1976821196* L_58 = __this->get_layerMask_17();
		FsmBool_t1075959796 * L_59 = __this->get_invertMask_18();
		NullCheck(L_59);
		bool L_60 = FsmBool_get_Value_m3101329097(L_59, /*hidden argument*/NULL);
		int32_t L_61 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_58, L_60, /*hidden argument*/NULL);
		float L_62 = V_4;
		float L_63 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_64 = Physics2D_OverlapAreaAll_m2584047348(NULL /*static, unused*/, L_56, L_57, L_61, L_62, L_63, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::.ctor()
extern "C"  void GetNextOverlapCircle2d__ctor_m1703548286 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapCircle2d_Reset_m3644948523_MetadataUsageId;
extern "C"  void GetNextOverlapCircle2d_Reset_m3644948523 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapCircle2d_Reset_m3644948523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		__this->set_fromGameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_fromPosition_12(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		__this->set_radius_13(L_3);
		FsmInt_t1596138449 * L_4 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		FsmInt_t1596138449 * L_5 = V_1;
		NullCheck(L_5);
		NamedVariable_set_UseVariable_m4266138971(L_5, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_6 = V_1;
		__this->set_minDepth_14(L_6);
		FsmInt_t1596138449 * L_7 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		FsmInt_t1596138449 * L_8 = V_1;
		NullCheck(L_8);
		NamedVariable_set_UseVariable_m4266138971(L_8, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_9 = V_1;
		__this->set_maxDepth_15(L_9);
		__this->set_layerMask_16(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_10 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_17(L_10);
		__this->set_collidersCount_18((FsmInt_t1596138449 *)NULL);
		__this->set_storeNextCollider_19((FsmGameObject_t1697147867 *)NULL);
		__this->set_loopEvent_20((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_21((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::OnEnter()
extern "C"  void GetNextOverlapCircle2d_OnEnter_m119091669 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method)
{
	{
		Collider2DU5BU5D_t1758559887* L_0 = __this->get_colliders_22();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		Collider2DU5BU5D_t1758559887* L_1 = GetNextOverlapCircle2d_GetOverlapCircleAll_m1663548798(__this, /*hidden argument*/NULL);
		__this->set_colliders_22(L_1);
		Collider2DU5BU5D_t1758559887* L_2 = __this->get_colliders_22();
		NullCheck(L_2);
		__this->set_colliderCount_23((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		FsmInt_t1596138449 * L_3 = __this->get_collidersCount_18();
		int32_t L_4 = __this->get_colliderCount_23();
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GetNextOverlapCircle2d_DoGetNextCollider_m3130863630(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::DoGetNextCollider()
extern "C"  void GetNextOverlapCircle2d_DoGetNextCollider_m3130863630 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nextColliderIndex_24();
		int32_t L_1 = __this->get_colliderCount_23();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0031;
		}
	}
	{
		__this->set_nextColliderIndex_24(0);
		__this->set_colliders_22((Collider2DU5BU5D_t1758559887*)NULL);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_21();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0031:
	{
		FsmGameObject_t1697147867 * L_4 = __this->get_storeNextCollider_19();
		Collider2DU5BU5D_t1758559887* L_5 = __this->get_colliders_22();
		int32_t L_6 = __this->get_nextColliderIndex_24();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Collider2D_t1552025098 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmGameObject_set_Value_m297051598(L_4, L_9, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_nextColliderIndex_24();
		int32_t L_11 = __this->get_colliderCount_23();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0078;
		}
	}
	{
		__this->set_nextColliderIndex_24(0);
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_finishedEvent_21();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0078:
	{
		int32_t L_14 = __this->get_nextColliderIndex_24();
		__this->set_nextColliderIndex_24(((int32_t)((int32_t)L_14+(int32_t)1)));
		FsmEvent_t2133468028 * L_15 = __this->get_loopEvent_20();
		if (!L_15)
		{
			goto IL_00a2;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_loopEvent_20();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		return;
	}
}
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::GetOverlapCircleAll()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapCircle2d_GetOverlapCircleAll_m1663548798_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapCircle2d_GetOverlapCircleAll_m1663548798 (GetNextOverlapCircle2d_t1098825704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapCircle2d_GetOverlapCircleAll_m1663548798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B8_0 = 0.0f;
	float G_B11_0 = 0.0f;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_fromGameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmVector2_t533912881 * L_3 = __this->get_fromPosition_12();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		Vector2_t4282066565 * L_7 = (&V_1);
		float L_8 = L_7->get_x_1();
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_x_1();
		L_7->set_x_1(((float)((float)L_8+(float)L_12)));
		Vector2_t4282066565 * L_13 = (&V_1);
		float L_14 = L_13->get_y_2();
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = (&V_5)->get_y_2();
		L_13->set_y_2(((float)((float)L_14+(float)L_18)));
	}

IL_006e:
	{
		FsmInt_t1596138449 * L_19 = __this->get_minDepth_14();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b6;
		}
	}
	{
		FsmInt_t1596138449 * L_21 = __this->get_maxDepth_15();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b6;
		}
	}
	{
		Vector2_t4282066565  L_23 = V_1;
		FsmFloat_t2134102846 * L_24 = __this->get_radius_13();
		NullCheck(L_24);
		float L_25 = FsmFloat_get_Value_m4137923823(L_24, /*hidden argument*/NULL);
		FsmIntU5BU5D_t1976821196* L_26 = __this->get_layerMask_16();
		FsmBool_t1075959796 * L_27 = __this->get_invertMask_17();
		NullCheck(L_27);
		bool L_28 = FsmBool_get_Value_m3101329097(L_27, /*hidden argument*/NULL);
		int32_t L_29 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_30 = Physics2D_OverlapCircleAll_m1000299574(NULL /*static, unused*/, L_23, L_25, L_29, /*hidden argument*/NULL);
		return L_30;
	}

IL_00b6:
	{
		FsmInt_t1596138449 * L_31 = __this->get_minDepth_14();
		NullCheck(L_31);
		bool L_32 = NamedVariable_get_IsNone_m281035543(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d0;
		}
	}
	{
		G_B8_0 = (-std::numeric_limits<float>::infinity());
		goto IL_00dc;
	}

IL_00d0:
	{
		FsmInt_t1596138449 * L_33 = __this->get_minDepth_14();
		NullCheck(L_33);
		int32_t L_34 = FsmInt_get_Value_m27059446(L_33, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_34)));
	}

IL_00dc:
	{
		V_2 = G_B8_0;
		FsmInt_t1596138449 * L_35 = __this->get_maxDepth_15();
		NullCheck(L_35);
		bool L_36 = NamedVariable_get_IsNone_m281035543(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00f7;
		}
	}
	{
		G_B11_0 = (std::numeric_limits<float>::infinity());
		goto IL_0103;
	}

IL_00f7:
	{
		FsmInt_t1596138449 * L_37 = __this->get_maxDepth_15();
		NullCheck(L_37);
		int32_t L_38 = FsmInt_get_Value_m27059446(L_37, /*hidden argument*/NULL);
		G_B11_0 = (((float)((float)L_38)));
	}

IL_0103:
	{
		V_3 = G_B11_0;
		Vector2_t4282066565  L_39 = V_1;
		FsmFloat_t2134102846 * L_40 = __this->get_radius_13();
		NullCheck(L_40);
		float L_41 = FsmFloat_get_Value_m4137923823(L_40, /*hidden argument*/NULL);
		FsmIntU5BU5D_t1976821196* L_42 = __this->get_layerMask_16();
		FsmBool_t1075959796 * L_43 = __this->get_invertMask_17();
		NullCheck(L_43);
		bool L_44 = FsmBool_get_Value_m3101329097(L_43, /*hidden argument*/NULL);
		int32_t L_45 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_42, L_44, /*hidden argument*/NULL);
		float L_46 = V_2;
		float L_47 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_48 = Physics2D_OverlapCircleAll_m2585342016(NULL /*static, unused*/, L_39, L_41, L_45, L_46, L_47, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::.ctor()
extern "C"  void GetNextOverlapPoint2d__ctor_m754199844 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapPoint2d_Reset_m2695600081_MetadataUsageId;
extern "C"  void GetNextOverlapPoint2d_Reset_m2695600081 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapPoint2d_Reset_m2695600081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_position_12(L_2);
		FsmInt_t1596138449 * L_3 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmInt_t1596138449 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = V_1;
		__this->set_minDepth_13(L_5);
		FsmInt_t1596138449 * L_6 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmInt_t1596138449 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = V_1;
		__this->set_maxDepth_14(L_8);
		__this->set_layerMask_15(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_9 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_16(L_9);
		__this->set_collidersCount_17((FsmInt_t1596138449 *)NULL);
		__this->set_storeNextCollider_18((FsmGameObject_t1697147867 *)NULL);
		__this->set_loopEvent_19((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_20((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::OnEnter()
extern "C"  void GetNextOverlapPoint2d_OnEnter_m2623272955 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method)
{
	{
		Collider2DU5BU5D_t1758559887* L_0 = __this->get_colliders_21();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		Collider2DU5BU5D_t1758559887* L_1 = GetNextOverlapPoint2d_GetOverlapPointAll_m2207819524(__this, /*hidden argument*/NULL);
		__this->set_colliders_21(L_1);
		Collider2DU5BU5D_t1758559887* L_2 = __this->get_colliders_21();
		NullCheck(L_2);
		__this->set_colliderCount_22((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		FsmInt_t1596138449 * L_3 = __this->get_collidersCount_17();
		int32_t L_4 = __this->get_colliderCount_22();
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GetNextOverlapPoint2d_DoGetNextCollider_m563281588(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::DoGetNextCollider()
extern "C"  void GetNextOverlapPoint2d_DoGetNextCollider_m563281588 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nextColliderIndex_23();
		int32_t L_1 = __this->get_colliderCount_22();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		__this->set_nextColliderIndex_23(0);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_20();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_002a:
	{
		FsmGameObject_t1697147867 * L_4 = __this->get_storeNextCollider_18();
		Collider2DU5BU5D_t1758559887* L_5 = __this->get_colliders_21();
		int32_t L_6 = __this->get_nextColliderIndex_23();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Collider2D_t1552025098 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmGameObject_set_Value_m297051598(L_4, L_9, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_nextColliderIndex_23();
		int32_t L_11 = __this->get_colliderCount_22();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0071;
		}
	}
	{
		__this->set_nextColliderIndex_23(0);
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_13 = __this->get_finishedEvent_20();
		NullCheck(L_12);
		Fsm_Event_m625948263(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0071:
	{
		int32_t L_14 = __this->get_nextColliderIndex_23();
		__this->set_nextColliderIndex_23(((int32_t)((int32_t)L_14+(int32_t)1)));
		FsmEvent_t2133468028 * L_15 = __this->get_loopEvent_19();
		if (!L_15)
		{
			goto IL_009b;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_17 = __this->get_loopEvent_19();
		NullCheck(L_16);
		Fsm_Event_m625948263(L_16, L_17, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::GetOverlapPointAll()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t GetNextOverlapPoint2d_GetOverlapPointAll_m2207819524_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapPoint2d_GetOverlapPointAll_m2207819524 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextOverlapPoint2d_GetOverlapPointAll_m2207819524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B8_0 = 0.0f;
	float G_B11_0 = 0.0f;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmVector2_t533912881 * L_3 = __this->get_position_12();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		Vector2_t4282066565 * L_7 = (&V_1);
		float L_8 = L_7->get_x_1();
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_x_1();
		L_7->set_x_1(((float)((float)L_8+(float)L_12)));
		Vector2_t4282066565 * L_13 = (&V_1);
		float L_14 = L_13->get_y_2();
		GameObject_t3674682005 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		float L_18 = (&V_5)->get_y_2();
		L_13->set_y_2(((float)((float)L_14+(float)L_18)));
	}

IL_006e:
	{
		FsmInt_t1596138449 * L_19 = __this->get_minDepth_13();
		NullCheck(L_19);
		bool L_20 = NamedVariable_get_IsNone_m281035543(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ab;
		}
	}
	{
		FsmInt_t1596138449 * L_21 = __this->get_maxDepth_14();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ab;
		}
	}
	{
		Vector2_t4282066565  L_23 = V_1;
		FsmIntU5BU5D_t1976821196* L_24 = __this->get_layerMask_15();
		FsmBool_t1075959796 * L_25 = __this->get_invertMask_16();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		int32_t L_27 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_28 = Physics2D_OverlapPointAll_m3910604131(NULL /*static, unused*/, L_23, L_27, /*hidden argument*/NULL);
		return L_28;
	}

IL_00ab:
	{
		FsmInt_t1596138449 * L_29 = __this->get_minDepth_13();
		NullCheck(L_29);
		bool L_30 = NamedVariable_get_IsNone_m281035543(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00c5;
		}
	}
	{
		G_B8_0 = (-std::numeric_limits<float>::infinity());
		goto IL_00d1;
	}

IL_00c5:
	{
		FsmInt_t1596138449 * L_31 = __this->get_minDepth_13();
		NullCheck(L_31);
		int32_t L_32 = FsmInt_get_Value_m27059446(L_31, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_32)));
	}

IL_00d1:
	{
		V_2 = G_B8_0;
		FsmInt_t1596138449 * L_33 = __this->get_maxDepth_14();
		NullCheck(L_33);
		bool L_34 = NamedVariable_get_IsNone_m281035543(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00ec;
		}
	}
	{
		G_B11_0 = (std::numeric_limits<float>::infinity());
		goto IL_00f8;
	}

IL_00ec:
	{
		FsmInt_t1596138449 * L_35 = __this->get_maxDepth_14();
		NullCheck(L_35);
		int32_t L_36 = FsmInt_get_Value_m27059446(L_35, /*hidden argument*/NULL);
		G_B11_0 = (((float)((float)L_36)));
	}

IL_00f8:
	{
		V_3 = G_B11_0;
		Vector2_t4282066565  L_37 = V_1;
		FsmIntU5BU5D_t1976821196* L_38 = __this->get_layerMask_15();
		FsmBool_t1075959796 * L_39 = __this->get_invertMask_16();
		NullCheck(L_39);
		bool L_40 = FsmBool_get_Value_m3101329097(L_39, /*hidden argument*/NULL);
		int32_t L_41 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		float L_42 = V_2;
		float L_43 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_44 = Physics2D_OverlapPointAll_m4265382893(NULL /*static, unused*/, L_37, L_41, L_42, L_43, /*hidden argument*/NULL);
		return L_44;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::.ctor()
extern "C"  void GetNextRayCast2d__ctor_m1196257964 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::Reset()
extern Il2CppClass* FsmVector2_t533912881_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var;
extern const uint32_t GetNextRayCast2d_Reset_m3137658201_MetadataUsageId;
extern "C"  void GetNextRayCast2d_Reset_m3137658201 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextRayCast2d_Reset_m3137658201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector2_t533912881 * V_0 = NULL;
	FsmInt_t1596138449 * V_1 = NULL;
	{
		__this->set_fromGameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		FsmVector2_t533912881 * L_0 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector2_t533912881 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_2 = V_0;
		__this->set_fromPosition_12(L_2);
		FsmVector2_t533912881 * L_3 = (FsmVector2_t533912881 *)il2cpp_codegen_object_new(FsmVector2_t533912881_il2cpp_TypeInfo_var);
		FsmVector2__ctor_m1412212034(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		FsmVector2_t533912881 * L_4 = V_0;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_5 = V_0;
		__this->set_direction_13(L_5);
		__this->set_space_14(1);
		FsmInt_t1596138449 * L_6 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmInt_t1596138449 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_8 = V_1;
		__this->set_minDepth_16(L_8);
		FsmInt_t1596138449 * L_9 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmInt_t1596138449 * L_10 = V_1;
		NullCheck(L_10);
		NamedVariable_set_UseVariable_m4266138971(L_10, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_11 = V_1;
		__this->set_maxDepth_17(L_11);
		__this->set_layerMask_18(((FsmIntU5BU5D_t1976821196*)SZArrayNew(FsmIntU5BU5D_t1976821196_il2cpp_TypeInfo_var, (uint32_t)0)));
		FsmBool_t1075959796 * L_12 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		__this->set_invertMask_19(L_12);
		__this->set_collidersCount_20((FsmInt_t1596138449 *)NULL);
		__this->set_storeNextCollider_21((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeNextHitPoint_22((FsmVector2_t533912881 *)NULL);
		__this->set_storeNextHitNormal_23((FsmVector2_t533912881 *)NULL);
		__this->set_storeNextHitDistance_24((FsmFloat_t2134102846 *)NULL);
		__this->set_storeNextHitFraction_25((FsmFloat_t2134102846 *)NULL);
		__this->set_loopEvent_26((FsmEvent_t2133468028 *)NULL);
		__this->set_finishedEvent_27((FsmEvent_t2133468028 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::OnEnter()
extern "C"  void GetNextRayCast2d_OnEnter_m2239363971 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method)
{
	{
		RaycastHit2DU5BU5D_t889400257* L_0 = __this->get_hits_28();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		RaycastHit2DU5BU5D_t889400257* L_1 = GetNextRayCast2d_GetRayCastAll_m3590188560(__this, /*hidden argument*/NULL);
		__this->set_hits_28(L_1);
		RaycastHit2DU5BU5D_t889400257* L_2 = __this->get_hits_28();
		NullCheck(L_2);
		__this->set_colliderCount_29((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		FsmInt_t1596138449 * L_3 = __this->get_collidersCount_20();
		int32_t L_4 = __this->get_colliderCount_29();
		NullCheck(L_3);
		FsmInt_set_Value_m2087583461(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GetNextRayCast2d_DoGetNextCollider_m2227714108(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::DoGetNextCollider()
extern Il2CppClass* RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var;
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern const uint32_t GetNextRayCast2d_DoGetNextCollider_m2227714108_MetadataUsageId;
extern "C"  void GetNextRayCast2d_DoGetNextCollider_m2227714108 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextRayCast2d_DoGetNextCollider_m2227714108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_nextColliderIndex_30();
		int32_t L_1 = __this->get_colliderCount_29();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0036;
		}
	}
	{
		__this->set_hits_28(((RaycastHit2DU5BU5D_t889400257*)SZArrayNew(RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_nextColliderIndex_30(0);
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_3 = __this->get_finishedEvent_27();
		NullCheck(L_2);
		Fsm_Event_m625948263(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		RaycastHit2DU5BU5D_t889400257* L_5 = __this->get_hits_28();
		int32_t L_6 = __this->get_nextColliderIndex_30();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		Fsm_RecordLastRaycastHit2DInfo_m3252930453(NULL /*static, unused*/, L_4, (*(RaycastHit2D_t1374744384 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_7 = __this->get_storeNextCollider_21();
		RaycastHit2DU5BU5D_t889400257* L_8 = __this->get_hits_28();
		int32_t L_9 = __this->get_nextColliderIndex_30();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		Collider2D_t1552025098 * L_10 = RaycastHit2D_get_collider_m789902306(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmGameObject_set_Value_m297051598(L_7, L_11, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_12 = __this->get_storeNextHitPoint_22();
		RaycastHit2DU5BU5D_t889400257* L_13 = __this->get_hits_28();
		int32_t L_14 = __this->get_nextColliderIndex_30();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Vector2_t4282066565  L_15 = RaycastHit2D_get_point_m2072691227(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmVector2_set_Value_m2900659718(L_12, L_15, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_16 = __this->get_storeNextHitNormal_23();
		RaycastHit2DU5BU5D_t889400257* L_17 = __this->get_hits_28();
		int32_t L_18 = __this->get_nextColliderIndex_30();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Vector2_t4282066565  L_19 = RaycastHit2D_get_normal_m894503390(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		NullCheck(L_16);
		FsmVector2_set_Value_m2900659718(L_16, L_19, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_20 = __this->get_storeNextHitDistance_24();
		RaycastHit2DU5BU5D_t889400257* L_21 = __this->get_hits_28();
		int32_t L_22 = __this->get_nextColliderIndex_30();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		float L_23 = RaycastHit2D_get_distance_m467570589(((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22))), /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmFloat_set_Value_m1568963140(L_20, L_23, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_24 = __this->get_storeNextHitFraction_25();
		RaycastHit2DU5BU5D_t889400257* L_25 = __this->get_hits_28();
		int32_t L_26 = __this->get_nextColliderIndex_30();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		float L_27 = RaycastHit2D_get_fraction_m2313516650(((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26))), /*hidden argument*/NULL);
		NullCheck(L_24);
		FsmFloat_set_Value_m1568963140(L_24, L_27, /*hidden argument*/NULL);
		int32_t L_28 = __this->get_nextColliderIndex_30();
		int32_t L_29 = __this->get_colliderCount_29();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0137;
		}
	}
	{
		__this->set_hits_28(((RaycastHit2DU5BU5D_t889400257*)SZArrayNew(RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_nextColliderIndex_30(0);
		Fsm_t1527112426 * L_30 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_31 = __this->get_finishedEvent_27();
		NullCheck(L_30);
		Fsm_Event_m625948263(L_30, L_31, /*hidden argument*/NULL);
		return;
	}

IL_0137:
	{
		int32_t L_32 = __this->get_nextColliderIndex_30();
		__this->set_nextColliderIndex_30(((int32_t)((int32_t)L_32+(int32_t)1)));
		FsmEvent_t2133468028 * L_33 = __this->get_loopEvent_26();
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		Fsm_t1527112426 * L_34 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_35 = __this->get_loopEvent_26();
		NullCheck(L_34);
		Fsm_Event_m625948263(L_34, L_35, /*hidden argument*/NULL);
	}

IL_0161:
	{
		return;
	}
}
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextRayCast2d::GetRayCastAll()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t GetNextRayCast2d_GetRayCastAll_m3590188560_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* GetNextRayCast2d_GetRayCastAll_m3590188560 (GetNextRayCast2d_t3555256570 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetNextRayCast2d_GetRayCastAll_m3590188560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t4282066566  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t4282066565  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t4282066565  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t4282066565  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float G_B15_0 = 0.0f;
	float G_B18_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_distance_15();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0021;
		}
	}
	{
		return ((RaycastHit2DU5BU5D_t889400257*)SZArrayNew(RaycastHit2DU5BU5D_t889400257_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0021:
	{
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_5 = __this->get_fromGameObject_11();
		NullCheck(L_4);
		GameObject_t3674682005 * L_6 = Fsm_GetOwnerDefaultTarget_m846013999(L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		FsmVector2_t533912881 * L_7 = __this->get_fromPosition_12();
		NullCheck(L_7);
		Vector2_t4282066565  L_8 = FsmVector2_get_Value_m1313754285(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		GameObject_t3674682005 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008f;
		}
	}
	{
		Vector2_t4282066565 * L_11 = (&V_1);
		float L_12 = L_11->get_x_1();
		GameObject_t3674682005 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = GameObject_get_transform_m1278640159(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t4282066566  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		V_7 = L_15;
		float L_16 = (&V_7)->get_x_1();
		L_11->set_x_1(((float)((float)L_12+(float)L_16)));
		Vector2_t4282066565 * L_17 = (&V_1);
		float L_18 = L_17->get_y_2();
		GameObject_t3674682005 * L_19 = V_0;
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t4282066566  L_21 = Transform_get_position_m2211398607(L_20, /*hidden argument*/NULL);
		V_8 = L_21;
		float L_22 = (&V_8)->get_y_2();
		L_17->set_y_2(((float)((float)L_18+(float)L_22)));
	}

IL_008f:
	{
		V_2 = (std::numeric_limits<float>::infinity());
		FsmFloat_t2134102846 * L_23 = __this->get_distance_15();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		if ((!(((float)L_24) > ((float)(0.0f)))))
		{
			goto IL_00b6;
		}
	}
	{
		FsmFloat_t2134102846 * L_25 = __this->get_distance_15();
		NullCheck(L_25);
		float L_26 = FsmFloat_get_Value_m4137923823(L_25, /*hidden argument*/NULL);
		V_2 = L_26;
	}

IL_00b6:
	{
		FsmVector2_t533912881 * L_27 = __this->get_direction_13();
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = FsmVector2_get_Value_m1313754285(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		Vector2_t4282066565  L_29 = Vector2_get_normalized_m123128511((&V_9), /*hidden argument*/NULL);
		V_3 = L_29;
		GameObject_t3674682005 * L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_31 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_30, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_013e;
		}
	}
	{
		int32_t L_32 = __this->get_space_14();
		if ((!(((uint32_t)L_32) == ((uint32_t)1))))
		{
			goto IL_013e;
		}
	}
	{
		GameObject_t3674682005 * L_33 = V_0;
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = GameObject_get_transform_m1278640159(L_33, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_35 = __this->get_direction_13();
		NullCheck(L_35);
		Vector2_t4282066565  L_36 = FsmVector2_get_Value_m1313754285(L_35, /*hidden argument*/NULL);
		V_10 = L_36;
		float L_37 = (&V_10)->get_x_1();
		FsmVector2_t533912881 * L_38 = __this->get_direction_13();
		NullCheck(L_38);
		Vector2_t4282066565  L_39 = FsmVector2_get_Value_m1313754285(L_38, /*hidden argument*/NULL);
		V_11 = L_39;
		float L_40 = (&V_11)->get_y_2();
		Vector3_t4282066566  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m2926210380(&L_41, L_37, L_40, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t4282066566  L_42 = Transform_TransformDirection_m83001769(L_34, L_41, /*hidden argument*/NULL);
		V_4 = L_42;
		float L_43 = (&V_4)->get_x_1();
		(&V_3)->set_x_1(L_43);
		float L_44 = (&V_4)->get_y_2();
		(&V_3)->set_y_2(L_44);
	}

IL_013e:
	{
		FsmInt_t1596138449 * L_45 = __this->get_minDepth_16();
		NullCheck(L_45);
		bool L_46 = NamedVariable_get_IsNone_m281035543(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_017d;
		}
	}
	{
		FsmInt_t1596138449 * L_47 = __this->get_maxDepth_17();
		NullCheck(L_47);
		bool L_48 = NamedVariable_get_IsNone_m281035543(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_017d;
		}
	}
	{
		Vector2_t4282066565  L_49 = V_1;
		Vector2_t4282066565  L_50 = V_3;
		float L_51 = V_2;
		FsmIntU5BU5D_t1976821196* L_52 = __this->get_layerMask_18();
		FsmBool_t1075959796 * L_53 = __this->get_invertMask_19();
		NullCheck(L_53);
		bool L_54 = FsmBool_get_Value_m3101329097(L_53, /*hidden argument*/NULL);
		int32_t L_55 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_56 = Physics2D_RaycastAll_m3437166214(NULL /*static, unused*/, L_49, L_50, L_51, L_55, /*hidden argument*/NULL);
		return L_56;
	}

IL_017d:
	{
		FsmInt_t1596138449 * L_57 = __this->get_minDepth_16();
		NullCheck(L_57);
		bool L_58 = NamedVariable_get_IsNone_m281035543(L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0197;
		}
	}
	{
		G_B15_0 = (-std::numeric_limits<float>::infinity());
		goto IL_01a3;
	}

IL_0197:
	{
		FsmInt_t1596138449 * L_59 = __this->get_minDepth_16();
		NullCheck(L_59);
		int32_t L_60 = FsmInt_get_Value_m27059446(L_59, /*hidden argument*/NULL);
		G_B15_0 = (((float)((float)L_60)));
	}

IL_01a3:
	{
		V_5 = G_B15_0;
		FsmInt_t1596138449 * L_61 = __this->get_maxDepth_17();
		NullCheck(L_61);
		bool L_62 = NamedVariable_get_IsNone_m281035543(L_61, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_01bf;
		}
	}
	{
		G_B18_0 = (std::numeric_limits<float>::infinity());
		goto IL_01cb;
	}

IL_01bf:
	{
		FsmInt_t1596138449 * L_63 = __this->get_maxDepth_17();
		NullCheck(L_63);
		int32_t L_64 = FsmInt_get_Value_m27059446(L_63, /*hidden argument*/NULL);
		G_B18_0 = (((float)((float)L_64)));
	}

IL_01cb:
	{
		V_6 = G_B18_0;
		Vector2_t4282066565  L_65 = V_1;
		Vector2_t4282066565  L_66 = V_3;
		float L_67 = V_2;
		FsmIntU5BU5D_t1976821196* L_68 = __this->get_layerMask_18();
		FsmBool_t1075959796 * L_69 = __this->get_invertMask_19();
		NullCheck(L_69);
		bool L_70 = FsmBool_get_Value_m3101329097(L_69, /*hidden argument*/NULL);
		int32_t L_71 = ActionHelpers_LayerArrayToLayerMask_m3090395306(NULL /*static, unused*/, L_68, L_70, /*hidden argument*/NULL);
		float L_72 = V_5;
		float L_73 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_74 = Physics2D_RaycastAll_m1583954576(NULL /*static, unused*/, L_65, L_66, L_67, L_71, L_72, L_73, /*hidden argument*/NULL);
		return L_74;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetOwner::.ctor()
extern "C"  void GetOwner__ctor_m1265119841 (GetOwner_t1709396389 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetOwner::Reset()
extern "C"  void GetOwner_Reset_m3206520078 (GetOwner_t1709396389 * __this, const MethodInfo* method)
{
	{
		__this->set_storeGameObject_11((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetOwner::OnEnter()
extern "C"  void GetOwner_OnEnter_m3991118328 (GetOwner_t1709396389 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_storeGameObject_11();
		GameObject_t3674682005 * L_1 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_1, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParent::.ctor()
extern "C"  void GetParent__ctor_m2679030280 (GetParent_t811151086 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParent::Reset()
extern "C"  void GetParent_Reset_m325463221 (GetParent_t811151086 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_12((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParent::OnEnter()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetParent_OnEnter_m1254417375_MetadataUsageId;
extern "C"  void GetParent_OnEnter_m1254417375 (GetParent_t811151086 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetParent_OnEnter_m1254417375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmGameObject_t1697147867 * G_B3_0 = NULL;
	FsmGameObject_t1697147867 * G_B2_0 = NULL;
	GameObject_t3674682005 * G_B4_0 = NULL;
	FsmGameObject_t1697147867 * G_B4_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_storeResult_12();
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Transform_get_parent_m2236876972(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = L_5;
		if (!L_9)
		{
			G_B3_0 = L_5;
			goto IL_0040;
		}
	}
	{
		G_B4_0 = ((GameObject_t3674682005 *)(NULL));
		G_B4_1 = G_B2_0;
		goto IL_0050;
	}

IL_0040:
	{
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = Transform_get_parent_m2236876972(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = Component_get_gameObject_m1170635899(L_12, /*hidden argument*/NULL);
		G_B4_0 = L_13;
		G_B4_1 = G_B3_0;
	}

IL_0050:
	{
		NullCheck(G_B4_1);
		FsmGameObject_set_Value_m297051598(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_005a:
	{
		FsmGameObject_t1697147867 * L_14 = __this->get_storeResult_12();
		NullCheck(L_14);
		FsmGameObject_set_Value_m297051598(L_14, (GameObject_t3674682005 *)NULL, /*hidden argument*/NULL);
	}

IL_0066:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::.ctor()
extern "C"  void GetParticleCollisionInfo__ctor_m468108506 (GetParticleCollisionInfo_t3484627724 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::Reset()
extern "C"  void GetParticleCollisionInfo_Reset_m2409508743 (GetParticleCollisionInfo_t3484627724 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::StoreCollisionInfo()
extern "C"  void GetParticleCollisionInfo_StoreCollisionInfo_m3670286665 (GetParticleCollisionInfo_t3484627724 * __this, const MethodInfo* method)
{
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Fsm_get_ParticleCollisionGO_m27653975(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmGameObject_set_Value_m297051598(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::OnEnter()
extern "C"  void GetParticleCollisionInfo_OnEnter_m2567404081 (GetParticleCollisionInfo_t3484627724 * __this, const MethodInfo* method)
{
	{
		GetParticleCollisionInfo_StoreCollisionInfo_m3670286665(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPosition::.ctor()
extern "C"  void GetPosition__ctor_m536563881 (GetPosition_t2407865645 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPosition::Reset()
extern "C"  void GetPosition_Reset_m2477964118 (GetPosition_t2407865645 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_12((FsmVector3_t533912882 *)NULL);
		__this->set_x_13((FsmFloat_t2134102846 *)NULL);
		__this->set_y_14((FsmFloat_t2134102846 *)NULL);
		__this->set_z_15((FsmFloat_t2134102846 *)NULL);
		__this->set_space_16(0);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnEnter()
extern "C"  void GetPosition_OnEnter_m3928510016 (GetPosition_t2407865645 * __this, const MethodInfo* method)
{
	{
		GetPosition_DoGetPosition_m1453152219(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnUpdate()
extern "C"  void GetPosition_OnUpdate_m658285571 (GetPosition_t2407865645 * __this, const MethodInfo* method)
{
	{
		GetPosition_DoGetPosition_m1453152219(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPosition::DoGetPosition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetPosition_DoGetPosition_m1453152219_MetadataUsageId;
extern "C"  void GetPosition_DoGetPosition_m1453152219 (GetPosition_t2407865645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetPosition_DoGetPosition_m1453152219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		int32_t L_5 = __this->get_space_16();
		if (L_5)
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		goto IL_0045;
	}

IL_003a:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_localPosition_m668140784(L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
	}

IL_0045:
	{
		V_1 = G_B5_0;
		FsmVector3_t533912882 * L_12 = __this->get_vector_12();
		Vector3_t4282066566  L_13 = V_1;
		NullCheck(L_12);
		FsmVector3_set_Value_m716982822(L_12, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_x_13();
		float L_15 = (&V_1)->get_x_1();
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_y_14();
		float L_17 = (&V_1)->get_y_2();
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_z_15();
		float L_19 = (&V_1)->get_z_3();
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::.ctor()
extern "C"  void GetPreviousStateName__ctor_m4163052111 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::Reset()
extern "C"  void GetPreviousStateName_Reset_m1809485052 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method)
{
	{
		__this->set_storeName_11((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::OnEnter()
extern "C"  void GetPreviousStateName_OnEnter_m1470254694 (GetPreviousStateName_t1600272887 * __this, const MethodInfo* method)
{
	FsmString_t952858651 * G_B2_0 = NULL;
	FsmString_t952858651 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	FsmString_t952858651 * G_B3_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_storeName_11();
		Fsm_t1527112426 * L_1 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmState_t2146334067 * L_2 = Fsm_get_PreviousActiveState_m1798855298(L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (L_2)
		{
			G_B2_0 = L_0;
			goto IL_001c;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		G_B3_1 = G_B1_0;
		goto IL_002c;
	}

IL_001c:
	{
		Fsm_t1527112426 * L_3 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmState_t2146334067 * L_4 = Fsm_get_PreviousActiveState_m1798855298(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = FsmState_get_Name_m3930587579(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002c:
	{
		NullCheck(G_B3_1);
		FsmString_set_Value_m829393196(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetProperty::.ctor()
extern "C"  void GetProperty__ctor_m1032097149 (GetProperty_t666919385 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetProperty::Reset()
extern Il2CppClass* FsmProperty_t3927159007_il2cpp_TypeInfo_var;
extern const uint32_t GetProperty_Reset_m2973497386_MetadataUsageId;
extern "C"  void GetProperty_Reset_m2973497386 (GetProperty_t666919385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetProperty_Reset_m2973497386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmProperty_t3927159007 * V_0 = NULL;
	{
		FsmProperty_t3927159007 * L_0 = (FsmProperty_t3927159007 *)il2cpp_codegen_object_new(FsmProperty_t3927159007_il2cpp_TypeInfo_var);
		FsmProperty__ctor_m1857631456(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmProperty_t3927159007 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_setProperty_20((bool)0);
		FsmProperty_t3927159007 * L_2 = V_0;
		__this->set_targetProperty_11(L_2);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetProperty::OnEnter()
extern "C"  void GetProperty_OnEnter_m3394610708 (GetProperty_t666919385 * __this, const MethodInfo* method)
{
	{
		FsmProperty_t3927159007 * L_0 = __this->get_targetProperty_11();
		NullCheck(L_0);
		FsmProperty_GetValue_m3267962847(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_everyFrame_12();
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetProperty::OnUpdate()
extern "C"  void GetProperty_OnUpdate_m1287276207 (GetProperty_t666919385 * __this, const MethodInfo* method)
{
	{
		FsmProperty_t3927159007 * L_0 = __this->get_targetProperty_11();
		NullCheck(L_0);
		FsmProperty_GetValue_m3267962847(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::.ctor()
extern "C"  void GetQuaternionEulerAngles__ctor_m37301577 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		QuaternionBaseAction__ctor_m1701621177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::Reset()
extern "C"  void GetQuaternionEulerAngles_Reset_m1978701814 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		__this->set_quaternion_13((FsmQuaternion_t3871136040 *)NULL);
		__this->set_eulerAngles_14((FsmVector3_t533912882 *)NULL);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrame_11((bool)1);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrameOption_12(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnEnter()
extern "C"  void GetQuaternionEulerAngles_OnEnter_m878805728 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		GetQuaternionEulerAngles_GetQuatEuler_m3347674999(__this, /*hidden argument*/NULL);
		bool L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnUpdate()
extern "C"  void GetQuaternionEulerAngles_OnUpdate_m606733155 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetQuaternionEulerAngles_GetQuatEuler_m3347674999(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnLateUpdate()
extern "C"  void GetQuaternionEulerAngles_OnLateUpdate_m3778371497 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionEulerAngles_GetQuatEuler_m3347674999(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnFixedUpdate()
extern "C"  void GetQuaternionEulerAngles_OnFixedUpdate_m2041344741 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionEulerAngles_GetQuatEuler_m3347674999(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::GetQuatEuler()
extern "C"  void GetQuaternionEulerAngles_GetQuatEuler_m3347674999 (GetQuaternionEulerAngles_t4150772157 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmVector3_t533912882 * L_0 = __this->get_eulerAngles_14();
		FsmQuaternion_t3871136040 * L_1 = __this->get_quaternion_13();
		NullCheck(L_1);
		Quaternion_t1553702882  L_2 = FsmQuaternion_get_Value_m3393858025(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = Quaternion_get_eulerAngles_m997303795((&V_0), /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmVector3_set_Value_m716982822(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::.ctor()
extern "C"  void GetQuaternionFromRotation__ctor_m393781420 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		QuaternionBaseAction__ctor_m1701621177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::Reset()
extern "C"  void GetQuaternionFromRotation_Reset_m2335181657 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		__this->set_fromDirection_13((FsmVector3_t533912882 *)NULL);
		__this->set_toDirection_14((FsmVector3_t533912882 *)NULL);
		__this->set_result_15((FsmQuaternion_t3871136040 *)NULL);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrame_11((bool)0);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrameOption_12(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnEnter()
extern "C"  void GetQuaternionFromRotation_OnEnter_m4153518467 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		GetQuaternionFromRotation_DoQuatFromRotation_m1731561282(__this, /*hidden argument*/NULL);
		bool L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnUpdate()
extern "C"  void GetQuaternionFromRotation_OnUpdate_m3338580256 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetQuaternionFromRotation_DoQuatFromRotation_m1731561282(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnLateUpdate()
extern "C"  void GetQuaternionFromRotation_OnLateUpdate_m2320688870 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionFromRotation_DoQuatFromRotation_m1731561282(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnFixedUpdate()
extern "C"  void GetQuaternionFromRotation_OnFixedUpdate_m4097823560 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionFromRotation_DoQuatFromRotation_m1731561282(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::DoQuatFromRotation()
extern "C"  void GetQuaternionFromRotation_DoQuatFromRotation_m1731561282 (GetQuaternionFromRotation_t638236618 * __this, const MethodInfo* method)
{
	{
		FsmQuaternion_t3871136040 * L_0 = __this->get_result_15();
		FsmVector3_t533912882 * L_1 = __this->get_fromDirection_13();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = FsmVector3_get_Value_m2779135117(L_1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_3 = __this->get_toDirection_14();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = FsmVector3_get_Value_m2779135117(L_3, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_FromToRotation_m2335489018(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmQuaternion_set_Value_m446581172(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::.ctor()
extern "C"  void GetQuaternionMultipliedByQuaternion__ctor_m3808425100 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		QuaternionBaseAction__ctor_m1701621177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::Reset()
extern "C"  void GetQuaternionMultipliedByQuaternion_Reset_m1454858041 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		__this->set_quaternionA_13((FsmQuaternion_t3871136040 *)NULL);
		__this->set_quaternionB_14((FsmQuaternion_t3871136040 *)NULL);
		__this->set_result_15((FsmQuaternion_t3871136040 *)NULL);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrame_11((bool)0);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrameOption_12(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnEnter()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnEnter_m4271080803 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402(__this, /*hidden argument*/NULL);
		bool L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnUpdate_m2688045376 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnLateUpdate_m4018108166 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnFixedUpdate_m883246888 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByQuaternion_DoQuatMult_m1002659402 (GetQuaternionMultipliedByQuaternion_t1710736874 * __this, const MethodInfo* method)
{
	{
		FsmQuaternion_t3871136040 * L_0 = __this->get_result_15();
		FsmQuaternion_t3871136040 * L_1 = __this->get_quaternionA_13();
		NullCheck(L_1);
		Quaternion_t1553702882  L_2 = FsmQuaternion_get_Value_m3393858025(L_1, /*hidden argument*/NULL);
		FsmQuaternion_t3871136040 * L_3 = __this->get_quaternionB_14();
		NullCheck(L_3);
		Quaternion_t1553702882  L_4 = FsmQuaternion_get_Value_m3393858025(L_3, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmQuaternion_set_Value_m446581172(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::.ctor()
extern "C"  void GetQuaternionMultipliedByVector__ctor_m3619095431 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		QuaternionBaseAction__ctor_m1701621177(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::Reset()
extern "C"  void GetQuaternionMultipliedByVector_Reset_m1265528372 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		__this->set_quaternion_13((FsmQuaternion_t3871136040 *)NULL);
		__this->set_vector3_14((FsmVector3_t533912882 *)NULL);
		__this->set_result_15((FsmVector3_t533912882 *)NULL);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrame_11((bool)0);
		((QuaternionBaseAction_t1884049229 *)__this)->set_everyFrameOption_12(0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnEnter()
extern "C"  void GetQuaternionMultipliedByVector_OnEnter_m2713895326 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		GetQuaternionMultipliedByVector_DoQuatMult_m831878191(__this, /*hidden argument*/NULL);
		bool L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrame_11();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnUpdate_m1659935845 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GetQuaternionMultipliedByVector_DoQuatMult_m831878191(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnLateUpdate_m3106121643 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionMultipliedByVector_DoQuatMult_m831878191(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnFixedUpdate_m2676435747 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((QuaternionBaseAction_t1884049229 *)__this)->get_everyFrameOption_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0012;
		}
	}
	{
		GetQuaternionMultipliedByVector_DoQuatMult_m831878191(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByVector_DoQuatMult_m831878191 (GetQuaternionMultipliedByVector_t1105007759 * __this, const MethodInfo* method)
{
	{
		FsmVector3_t533912882 * L_0 = __this->get_result_15();
		FsmQuaternion_t3871136040 * L_1 = __this->get_quaternion_13();
		NullCheck(L_1);
		Quaternion_t1553702882  L_2 = FsmQuaternion_get_Value_m3393858025(L_1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_3 = __this->get_vector3_14();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = FsmVector3_get_Value_m2779135117(L_3, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmVector3_set_Value_m716982822(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::.ctor()
extern "C"  void GetRandomChild__ctor_m3037302011 (GetRandomChild_t1715933131 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::Reset()
extern "C"  void GetRandomChild_Reset_m683734952 (GetRandomChild_t1715933131 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_12((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::OnEnter()
extern "C"  void GetRandomChild_OnEnter_m1956167186 (GetRandomChild_t1715933131 * __this, const MethodInfo* method)
{
	{
		GetRandomChild_DoGetRandomChild_m1065979831(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::DoGetRandomChild()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetRandomChild_DoGetRandomChild_m1065979831_MetadataUsageId;
extern "C"  void GetRandomChild_DoGetRandomChild_m1065979831 (GetRandomChild_t1715933131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRandomChild_DoGetRandomChild_m1065979831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Transform_get_childCount_m2107810675(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_storeResult_12();
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		int32_t L_13 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_14 = Transform_GetChild_m4040462992(L_11, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmGameObject_set_Value_m297051598(L_9, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::.ctor()
extern "C"  void GetRandomObject__ctor_m567043216 (GetRandomObject_t1343391078 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GetRandomObject_Reset_m2508443453_MetadataUsageId;
extern "C"  void GetRandomObject_Reset_m2508443453 (GetRandomObject_t1343391078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRandomObject_Reset_m2508443453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_withTag_11(L_0);
		__this->set_storeResult_12((FsmGameObject_t1697147867 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::OnEnter()
extern "C"  void GetRandomObject_OnEnter_m3154379879 (GetRandomObject_t1343391078 * __this, const MethodInfo* method)
{
	{
		GetRandomObject_DoGetRandomObject_m2325683003(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::OnUpdate()
extern "C"  void GetRandomObject_OnUpdate_m2430055100 (GetRandomObject_t1343391078 * __this, const MethodInfo* method)
{
	{
		GetRandomObject_DoGetRandomObject_m2325683003(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::DoGetRandomObject()
extern const Il2CppType* GameObject_t3674682005_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GetRandomObject_DoGetRandomObject_m2325683003_MetadataUsageId;
extern "C"  void GetRandomObject_DoGetRandomObject_m2325683003 (GetRandomObject_t1343391078 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRandomObject_DoGetRandomObject_m2325683003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t2662109048* V_0 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_withTag_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, _stringLiteral69913957, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		FsmString_t952858651 * L_3 = __this->get_withTag_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_5 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0045;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameObject_t3674682005_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1015136018* L_7 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = ((GameObjectU5BU5D_t2662109048*)Castclass(L_7, GameObjectU5BU5D_t2662109048_il2cpp_TypeInfo_var));
	}

IL_0045:
	{
		GameObjectU5BU5D_t2662109048* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		FsmGameObject_t1697147867 * L_9 = __this->get_storeResult_12();
		GameObjectU5BU5D_t2662109048* L_10 = V_0;
		GameObjectU5BU5D_t2662109048* L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Random_Range_m75452833(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_12);
		int32_t L_13 = L_12;
		GameObject_t3674682005 * L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		FsmGameObject_set_Value_m297051598(L_9, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0065:
	{
		FsmGameObject_t1697147867 * L_15 = __this->get_storeResult_12();
		NullCheck(L_15);
		FsmGameObject_set_Value_m297051598(L_15, (GameObject_t3674682005 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::.ctor()
extern "C"  void GetRaycastAllInfo__ctor_m3704999916 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::Reset()
extern "C"  void GetRaycastAllInfo_Reset_m1351432857 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method)
{
	{
		__this->set_storeHitObjects_11((FsmArray_t2129666875 *)NULL);
		__this->set_points_12((FsmArray_t2129666875 *)NULL);
		__this->set_normals_13((FsmArray_t2129666875 *)NULL);
		__this->set_distances_14((FsmArray_t2129666875 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::StoreRaycastAllInfo()
extern Il2CppClass* RaycastAll_t3603233760_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665_MetadataUsageId;
extern "C"  void GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		RaycastHitU5BU5D_t528650843* L_0 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		FsmArray_t2129666875 * L_1 = __this->get_storeHitObjects_11();
		RaycastHitU5BU5D_t528650843* L_2 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		NullCheck(L_2);
		NullCheck(L_1);
		FsmArray_Resize_m3367576465(L_1, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_3 = __this->get_points_12();
		RaycastHitU5BU5D_t528650843* L_4 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		NullCheck(L_4);
		NullCheck(L_3);
		FsmArray_Resize_m3367576465(L_3, (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))), /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_5 = __this->get_normals_13();
		RaycastHitU5BU5D_t528650843* L_6 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		NullCheck(L_6);
		NullCheck(L_5);
		FsmArray_Resize_m3367576465(L_5, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		FsmArray_t2129666875 * L_7 = __this->get_distances_14();
		RaycastHitU5BU5D_t528650843* L_8 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		NullCheck(L_8);
		NullCheck(L_7);
		FsmArray_Resize_m3367576465(L_7, (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00e6;
	}

IL_005a:
	{
		FsmArray_t2129666875 * L_9 = __this->get_storeHitObjects_11();
		NullCheck(L_9);
		ObjectU5BU5D_t1108656482* L_10 = FsmArray_get_Values_m1362031434(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		RaycastHitU5BU5D_t528650843* L_12 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		Collider_t2939674232 * L_14 = RaycastHit_get_collider_m3116882274(((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		ArrayElementTypeCheck (L_10, L_15);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (Il2CppObject *)L_15);
		FsmArray_t2129666875 * L_16 = __this->get_points_12();
		NullCheck(L_16);
		ObjectU5BU5D_t1108656482* L_17 = FsmArray_get_Values_m1362031434(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		RaycastHitU5BU5D_t528650843* L_19 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		Vector3_t4282066566  L_21 = RaycastHit_get_point_m4165497838(((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_22 = L_21;
		Il2CppObject * L_23 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		ArrayElementTypeCheck (L_17, L_23);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_23);
		FsmArray_t2129666875 * L_24 = __this->get_normals_13();
		NullCheck(L_24);
		ObjectU5BU5D_t1108656482* L_25 = FsmArray_get_Values_m1362031434(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		RaycastHitU5BU5D_t528650843* L_27 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		Vector3_t4282066566  L_29 = RaycastHit_get_normal_m1346998891(((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = L_29;
		Il2CppObject * L_31 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		ArrayElementTypeCheck (L_25, L_31);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (Il2CppObject *)L_31);
		FsmArray_t2129666875 * L_32 = __this->get_distances_14();
		NullCheck(L_32);
		ObjectU5BU5D_t1108656482* L_33 = FsmArray_get_Values_m1362031434(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		RaycastHitU5BU5D_t528650843* L_35 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		int32_t L_36 = V_0;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		float L_37 = RaycastHit_get_distance_m800944203(((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36))), /*hidden argument*/NULL);
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		ArrayElementTypeCheck (L_33, L_39);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_34), (Il2CppObject *)L_39);
		int32_t L_40 = V_0;
		V_0 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00e6:
	{
		int32_t L_41 = V_0;
		RaycastHitU5BU5D_t528650843* L_42 = ((RaycastAll_t3603233760_StaticFields*)RaycastAll_t3603233760_il2cpp_TypeInfo_var->static_fields)->get_RaycastAllHitInfo_11();
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))))))
		{
			goto IL_005a;
		}
	}
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnEnter()
extern "C"  void GetRaycastAllInfo_OnEnter_m3663726787 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method)
{
	{
		GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnUpdate()
extern "C"  void GetRaycastAllInfo_OnUpdate_m1039940064 (GetRaycastAllInfo_t3534604426 * __this, const MethodInfo* method)
{
	{
		GetRaycastAllInfo_StoreRaycastAllInfo_m1559387665(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::.ctor()
extern "C"  void GetRayCastHit2dInfo__ctor_m2785427688 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::Reset()
extern "C"  void GetRayCastHit2dInfo_Reset_m431860629 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_point_12((FsmVector2_t533912881 *)NULL);
		__this->set_normal_13((FsmVector3_t533912882 *)NULL);
		__this->set_distance_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnEnter()
extern "C"  void GetRayCastHit2dInfo_OnEnter_m423111359 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method)
{
	{
		GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnUpdate()
extern "C"  void GetRayCastHit2dInfo_OnUpdate_m3660076900 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method)
{
	{
		GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::StoreRaycastInfo()
extern Il2CppClass* Fsm_t1527112426_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674_MetadataUsageId;
extern "C"  void GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674 (GetRayCastHit2dInfo_t2378543630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRayCastHit2dInfo_StoreRaycastInfo_m3396674674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t1527112426_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_1 = Fsm_GetLastRaycastHit2DInfo_m382528231(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Collider2D_t1552025098 * L_2 = RaycastHit2D_get_collider_m789902306((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0070;
		}
	}
	{
		FsmGameObject_t1697147867 * L_4 = __this->get_gameObjectHit_11();
		Collider2D_t1552025098 * L_5 = RaycastHit2D_get_collider_m789902306((&V_0), /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmGameObject_set_Value_m297051598(L_4, L_6, /*hidden argument*/NULL);
		FsmVector2_t533912881 * L_7 = __this->get_point_12();
		Vector2_t4282066565  L_8 = RaycastHit2D_get_point_m2072691227((&V_0), /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmVector2_set_Value_m2900659718(L_7, L_8, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_9 = __this->get_normal_13();
		Vector2_t4282066565  L_10 = RaycastHit2D_get_normal_m894503390((&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmVector3_set_Value_m716982822(L_9, L_11, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_12 = __this->get_distance_14();
		float L_13 = RaycastHit2D_get_fraction_m2313516650((&V_0), /*hidden argument*/NULL);
		NullCheck(L_12);
		FsmFloat_set_Value_m1568963140(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::.ctor()
extern "C"  void GetRaycastHitInfo__ctor_m154070842 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::Reset()
extern "C"  void GetRaycastHitInfo_Reset_m2095471079 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_point_12((FsmVector3_t533912882 *)NULL);
		__this->set_normal_13((FsmVector3_t533912882 *)NULL);
		__this->set_distance_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::StoreRaycastInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetRaycastHitInfo_StoreRaycastInfo_m2476388064_MetadataUsageId;
extern "C"  void GetRaycastHitInfo_StoreRaycastInfo_m2476388064 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRaycastHitInfo_StoreRaycastInfo_m2476388064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t4003175726  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastHit_t4003175726  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RaycastHit_t4003175726  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RaycastHit_t4003175726  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RaycastHit_t4003175726  L_1 = Fsm_get_RaycastHitInfo_m576422000(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Collider_t2939674232 * L_2 = RaycastHit_get_collider_m3116882274((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_009c;
		}
	}
	{
		FsmGameObject_t1697147867 * L_4 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_5 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		RaycastHit_t4003175726  L_6 = Fsm_get_RaycastHitInfo_m576422000(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmGameObject_set_Value_m297051598(L_4, L_8, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_9 = __this->get_point_12();
		Fsm_t1527112426 * L_10 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RaycastHit_t4003175726  L_11 = Fsm_get_RaycastHitInfo_m576422000(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Vector3_t4282066566  L_12 = RaycastHit_get_point_m4165497838((&V_2), /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmVector3_set_Value_m716982822(L_9, L_12, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_13 = __this->get_normal_13();
		Fsm_t1527112426 * L_14 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		RaycastHit_t4003175726  L_15 = Fsm_get_RaycastHitInfo_m576422000(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Vector3_t4282066566  L_16 = RaycastHit_get_normal_m1346998891((&V_3), /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmVector3_set_Value_m716982822(L_13, L_16, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_17 = __this->get_distance_14();
		Fsm_t1527112426 * L_18 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		RaycastHit_t4003175726  L_19 = Fsm_get_RaycastHitInfo_m576422000(L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		float L_20 = RaycastHit_get_distance_m800944203((&V_4), /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmFloat_set_Value_m1568963140(L_17, L_20, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnEnter()
extern "C"  void GetRaycastHitInfo_OnEnter_m1424919697 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method)
{
	{
		GetRaycastHitInfo_StoreRaycastInfo_m2476388064(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnUpdate()
extern "C"  void GetRaycastHitInfo_OnUpdate_m356397010 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method)
{
	{
		GetRaycastHitInfo_StoreRaycastInfo_m2476388064(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::.ctor()
extern "C"  void GetRectFields__ctor_m3013048021 (GetRectFields_t2216442753 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::Reset()
extern "C"  void GetRectFields_Reset_m659480962 (GetRectFields_t2216442753 * __this, const MethodInfo* method)
{
	{
		__this->set_rectVariable_11((FsmRect_t1076426478 *)NULL);
		__this->set_storeX_12((FsmFloat_t2134102846 *)NULL);
		__this->set_storeY_13((FsmFloat_t2134102846 *)NULL);
		__this->set_storeWidth_14((FsmFloat_t2134102846 *)NULL);
		__this->set_storeHeight_15((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_16((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnEnter()
extern "C"  void GetRectFields_OnEnter_m122919276 (GetRectFields_t2216442753 * __this, const MethodInfo* method)
{
	{
		GetRectFields_DoGetRectFields_m207739483(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_16();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnUpdate()
extern "C"  void GetRectFields_OnUpdate_m2944056919 (GetRectFields_t2216442753 * __this, const MethodInfo* method)
{
	{
		GetRectFields_DoGetRectFields_m207739483(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::DoGetRectFields()
extern "C"  void GetRectFields_DoGetRectFields_m207739483 (GetRectFields_t2216442753 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		FsmRect_t1076426478 * L_0 = __this->get_rectVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeX_12();
		FsmRect_t1076426478 * L_3 = __this->get_rectVariable_11();
		NullCheck(L_3);
		Rect_t4241904616  L_4 = FsmRect_get_Value_m1002500317(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_6 = __this->get_storeY_13();
		FsmRect_t1076426478 * L_7 = __this->get_rectVariable_11();
		NullCheck(L_7);
		Rect_t4241904616  L_8 = FsmRect_get_Value_m1002500317(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_y_m982386315((&V_1), /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_9, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_10 = __this->get_storeWidth_14();
		FsmRect_t1076426478 * L_11 = __this->get_rectVariable_11();
		NullCheck(L_11);
		Rect_t4241904616  L_12 = FsmRect_get_Value_m1002500317(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = Rect_get_width_m2824209432((&V_2), /*hidden argument*/NULL);
		NullCheck(L_10);
		FsmFloat_set_Value_m1568963140(L_10, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_storeHeight_15();
		FsmRect_t1076426478 * L_15 = __this->get_rectVariable_11();
		NullCheck(L_15);
		Rect_t4241904616  L_16 = FsmRect_get_Value_m1002500317(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRoot::.ctor()
extern "C"  void GetRoot__ctor_m3740661904 (GetRoot_t1738725734 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRoot::Reset()
extern "C"  void GetRoot_Reset_m1387094845 (GetRoot_t1738725734 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeRoot_12((FsmGameObject_t1697147867 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRoot::OnEnter()
extern "C"  void GetRoot_OnEnter_m3575158887 (GetRoot_t1738725734 * __this, const MethodInfo* method)
{
	{
		GetRoot_DoGetRoot_m3774034491(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRoot::DoGetRoot()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetRoot_DoGetRoot_m3774034491_MetadataUsageId;
extern "C"  void GetRoot_DoGetRoot_m3774034491 (GetRoot_t1738725734 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRoot_DoGetRoot_m3774034491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		FsmGameObject_t1697147867 * L_5 = __this->get_storeRoot_12();
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Transform_get_root_m1064615716(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		FsmGameObject_set_Value_m297051598(L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRotation::.ctor()
extern "C"  void GetRotation__ctor_m205269492 (GetRotation_t1619760002 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRotation::Reset()
extern "C"  void GetRotation_Reset_m2146669729 (GetRotation_t1619760002 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_quaternion_12((FsmQuaternion_t3871136040 *)NULL);
		__this->set_vector_13((FsmVector3_t533912882 *)NULL);
		__this->set_xAngle_14((FsmFloat_t2134102846 *)NULL);
		__this->set_yAngle_15((FsmFloat_t2134102846 *)NULL);
		__this->set_zAngle_16((FsmFloat_t2134102846 *)NULL);
		__this->set_space_17(0);
		__this->set_everyFrame_18((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRotation::OnEnter()
extern "C"  void GetRotation_OnEnter_m3382182091 (GetRotation_t1619760002 * __this, const MethodInfo* method)
{
	{
		GetRotation_DoGetRotation_m507767611(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_18();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRotation::OnUpdate()
extern "C"  void GetRotation_OnUpdate_m901989080 (GetRotation_t1619760002 * __this, const MethodInfo* method)
{
	{
		GetRotation_DoGetRotation_m507767611(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetRotation::DoGetRotation()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetRotation_DoGetRotation_m507767611_MetadataUsageId;
extern "C"  void GetRotation_DoGetRotation_m507767611 (GetRotation_t1619760002 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetRotation_DoGetRotation_m507767611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		int32_t L_5 = __this->get_space_17();
		if (L_5)
		{
			goto IL_0093;
		}
	}
	{
		FsmQuaternion_t3871136040 * L_6 = __this->get_quaternion_12();
		GameObject_t3674682005 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = GameObject_get_transform_m1278640159(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Quaternion_t1553702882  L_9 = Transform_get_rotation_m11483428(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmQuaternion_set_Value_m446581172(L_6, L_9, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t4282066566  L_12 = Transform_get_eulerAngles_m1058084741(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		FsmVector3_t533912882 * L_13 = __this->get_vector_13();
		Vector3_t4282066566  L_14 = V_1;
		NullCheck(L_13);
		FsmVector3_set_Value_m716982822(L_13, L_14, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_15 = __this->get_xAngle_14();
		float L_16 = (&V_1)->get_x_1();
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, L_16, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_17 = __this->get_yAngle_15();
		float L_18 = (&V_1)->get_y_2();
		NullCheck(L_17);
		FsmFloat_set_Value_m1568963140(L_17, L_18, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_19 = __this->get_zAngle_16();
		float L_20 = (&V_1)->get_z_3();
		NullCheck(L_19);
		FsmFloat_set_Value_m1568963140(L_19, L_20, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_0093:
	{
		GameObject_t3674682005 * L_21 = V_0;
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_localEulerAngles_m3489183428(L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		FsmQuaternion_t3871136040 * L_24 = __this->get_quaternion_12();
		Vector3_t4282066566  L_25 = V_2;
		Quaternion_t1553702882  L_26 = Quaternion_Euler_m1940911101(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		FsmQuaternion_set_Value_m446581172(L_24, L_26, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_27 = __this->get_vector_13();
		Vector3_t4282066566  L_28 = V_2;
		NullCheck(L_27);
		FsmVector3_set_Value_m716982822(L_27, L_28, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_29 = __this->get_xAngle_14();
		float L_30 = (&V_2)->get_x_1();
		NullCheck(L_29);
		FsmFloat_set_Value_m1568963140(L_29, L_30, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_31 = __this->get_yAngle_15();
		float L_32 = (&V_2)->get_y_2();
		NullCheck(L_31);
		FsmFloat_set_Value_m1568963140(L_31, L_32, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_33 = __this->get_zAngle_16();
		float L_34 = (&V_2)->get_z_3();
		NullCheck(L_33);
		FsmFloat_set_Value_m1568963140(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScale::.ctor()
extern "C"  void GetScale__ctor_m1998820778 (GetScale_t1712482364 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScale::Reset()
extern "C"  void GetScale_Reset_m3940221015 (GetScale_t1712482364 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_12((FsmVector3_t533912882 *)NULL);
		__this->set_xScale_13((FsmFloat_t2134102846 *)NULL);
		__this->set_yScale_14((FsmFloat_t2134102846 *)NULL);
		__this->set_zScale_15((FsmFloat_t2134102846 *)NULL);
		__this->set_space_16(0);
		__this->set_everyFrame_17((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnEnter()
extern "C"  void GetScale_OnEnter_m408114945 (GetScale_t1712482364 * __this, const MethodInfo* method)
{
	{
		GetScale_DoGetScale_m3997279513(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_17();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnUpdate()
extern "C"  void GetScale_OnUpdate_m3195188066 (GetScale_t1712482364 * __this, const MethodInfo* method)
{
	{
		GetScale_DoGetScale_m3997279513(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScale::DoGetScale()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetScale_DoGetScale_m3997279513_MetadataUsageId;
extern "C"  void GetScale_DoGetScale_m3997279513 (GetScale_t1712482364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetScale_DoGetScale_m3997279513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		int32_t L_5 = __this->get_space_16();
		if (L_5)
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t3674682005 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_lossyScale_m3749612506(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		goto IL_0045;
	}

IL_003a:
	{
		GameObject_t3674682005 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_localScale_m3886572677(L_10, /*hidden argument*/NULL);
		G_B5_0 = L_11;
	}

IL_0045:
	{
		V_1 = G_B5_0;
		FsmVector3_t533912882 * L_12 = __this->get_vector_12();
		Vector3_t4282066566  L_13 = V_1;
		NullCheck(L_12);
		FsmVector3_set_Value_m716982822(L_12, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_xScale_13();
		float L_15 = (&V_1)->get_x_1();
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_yScale_14();
		float L_17 = (&V_1)->get_y_2();
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_zScale_15();
		float L_19 = (&V_1)->get_z_3();
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::.ctor()
extern "C"  void GetScreenHeight__ctor_m3989100767 (GetScreenHeight_t21638199 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::Reset()
extern "C"  void GetScreenHeight_Reset_m1635533708 (GetScreenHeight_t21638199 * __this, const MethodInfo* method)
{
	{
		__this->set_storeScreenHeight_11((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::OnEnter()
extern "C"  void GetScreenHeight_OnEnter_m1806737654 (GetScreenHeight_t21638199 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeScreenHeight_11();
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::.ctor()
extern "C"  void GetScreenWidth__ctor_m180630394 (GetScreenWidth_t2390904428 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::Reset()
extern "C"  void GetScreenWidth_Reset_m2122030631 (GetScreenWidth_t2390904428 * __this, const MethodInfo* method)
{
	{
		__this->set_storeScreenWidth_11((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::OnEnter()
extern "C"  void GetScreenWidth_OnEnter_m1178845393 (GetScreenWidth_t2390904428 * __this, const MethodInfo* method)
{
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeScreenWidth_11();
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmFloat_set_Value_m1568963140(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSine::.ctor()
extern "C"  void GetSine__ctor_m3122449221 (GetSine_t1738749713 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSine::Reset()
extern "C"  void GetSine_Reset_m768882162 (GetSine_t1738749713 * __this, const MethodInfo* method)
{
	{
		__this->set_angle_11((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_DegToRad_12(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_result_13((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnEnter()
extern "C"  void GetSine_OnEnter_m2178257372 (GetSine_t1738749713 * __this, const MethodInfo* method)
{
	{
		GetSine_DoSine_m3103512983(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnUpdate()
extern "C"  void GetSine_OnUpdate_m2235028455 (GetSine_t1738749713 * __this, const MethodInfo* method)
{
	{
		GetSine_DoSine_m3103512983(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSine::DoSine()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetSine_DoSine_m3103512983_MetadataUsageId;
extern "C"  void GetSine_DoSine_m3103512983 (GetSine_t1738749713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSine_DoSine_m3103512983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_angle_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmBool_t1075959796 * L_2 = __this->get_DegToRad_12();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4*(float)(0.0174532924f)));
	}

IL_0024:
	{
		FsmFloat_t2134102846 * L_5 = __this->get_result_13();
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = sinf(L_6);
		NullCheck(L_5);
		FsmFloat_set_Value_m1568963140(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t GetSpeed__ctor_m2886150989_MetadataUsageId;
extern "C"  void GetSpeed__ctor_m2886150989 (GetSpeed_t1712873273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSpeed__ctor_m2886150989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::Reset()
extern "C"  void GetSpeed_Reset_m532583930 (GetSpeed_t1712873273 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnEnter()
extern "C"  void GetSpeed_OnEnter_m2728923108 (GetSpeed_t1712873273 * __this, const MethodInfo* method)
{
	{
		GetSpeed_DoGetSpeed_m3185395987(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnUpdate()
extern "C"  void GetSpeed_OnUpdate_m2125797087 (GetSpeed_t1712873273 * __this, const MethodInfo* method)
{
	{
		GetSpeed_DoGetSpeed_m3185395987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::DoGetSpeed()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t GetSpeed_DoGetSpeed_m3185395987_MetadataUsageId;
extern "C"  void GetSpeed_DoGetSpeed_m3185395987 (GetSpeed_t1712873273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSpeed_DoGetSpeed_m3185395987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t3674682005 * G_B5_0 = NULL;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeResult_14();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_1);
		int32_t L_2 = FsmOwnerDefault_get_OwnerOption_m3357910390(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		GameObject_t3674682005 * L_3 = FsmStateAction_get_Owner_m1855633209(__this, /*hidden argument*/NULL);
		G_B5_0 = L_3;
		goto IL_0037;
	}

IL_0027:
	{
		FsmOwnerDefault_t251897112 * L_4 = __this->get_gameObject_13();
		NullCheck(L_4);
		FsmGameObject_t1697147867 * L_5 = FsmOwnerDefault_get_GameObject_m3249227945(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = FsmGameObject_get_Value_m673294275(L_5, /*hidden argument*/NULL);
		G_B5_0 = L_6;
	}

IL_0037:
	{
		V_0 = G_B5_0;
		GameObject_t3674682005 * L_7 = V_0;
		bool L_8 = ComponentAction_1_UpdateCache_m864821753(__this, L_7, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Rigidbody_t3346577219 * L_9 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Rigidbody_get_velocity_m2696244068(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		FsmFloat_t2134102846 * L_11 = __this->get_storeResult_14();
		float L_12 = Vector3_get_magnitude_m989985786((&V_1), /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmFloat_set_Value_m1568963140(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2621392432_MethodInfo_var;
extern const uint32_t GetSpeed2d__ctor_m4173642395_MetadataUsageId;
extern "C"  void GetSpeed2d__ctor_m4173642395 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSpeed2d__ctor_m4173642395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2621392432(__this, /*hidden argument*/ComponentAction_1__ctor_m2621392432_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::Reset()
extern "C"  void GetSpeed2d_Reset_m1820075336 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeResult_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnEnter()
extern "C"  void GetSpeed2d_OnEnter_m3057583026 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method)
{
	{
		GetSpeed2d_DoGetSpeed_m1767578245(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnUpdate()
extern "C"  void GetSpeed2d_OnUpdate_m3724319953 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method)
{
	{
		GetSpeed2d_DoGetSpeed_m1767578245(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::DoGetSpeed()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var;
extern const uint32_t GetSpeed2d_DoGetSpeed_m1767578245_MetadataUsageId;
extern "C"  void GetSpeed2d_DoGetSpeed_m1767578245 (GetSpeed2d_t1806850603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSpeed2d_DoGetSpeed_m1767578245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmFloat_t2134102846 * L_0 = __this->get_storeResult_14();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_3 = __this->get_gameObject_13();
		NullCheck(L_2);
		GameObject_t3674682005 * L_4 = Fsm_GetOwnerDefaultTarget_m846013999(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		bool L_6 = ComponentAction_1_UpdateCache_m2944103819(__this, L_5, /*hidden argument*/ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var);
		if (L_6)
		{
			goto IL_0030;
		}
	}
	{
		return;
	}

IL_0030:
	{
		FsmFloat_t2134102846 * L_7 = __this->get_storeResult_14();
		Rigidbody2D_t1743771669 * L_8 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		NullCheck(L_8);
		Vector2_t4282066565  L_9 = Rigidbody2D_get_velocity_m416159605(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = Vector2_get_magnitude_m1987058139((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::.ctor()
extern "C"  void GetStringLeft__ctor_m4067837754 (GetStringLeft_t1020228988 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::Reset()
extern "C"  void GetStringLeft_Reset_m1714270695 (GetStringLeft_t1020228988 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_charCount_12(L_0);
		__this->set_storeResult_13((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::OnEnter()
extern "C"  void GetStringLeft_OnEnter_m163570833 (GetStringLeft_t1020228988 * __this, const MethodInfo* method)
{
	{
		GetStringLeft_DoGetStringLeft_m3053705147(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::OnUpdate()
extern "C"  void GetStringLeft_OnUpdate_m4204255186 (GetStringLeft_t1020228988 * __this, const MethodInfo* method)
{
	{
		GetStringLeft_DoGetStringLeft_m3053705147(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::DoGetStringLeft()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetStringLeft_DoGetStringLeft_m3053705147_MetadataUsageId;
extern "C"  void GetStringLeft_DoGetStringLeft_m3053705147 (GetStringLeft_t1020228988 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetStringLeft_DoGetStringLeft_m3053705147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmString_t952858651 * L_2 = __this->get_storeResult_13();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		FsmString_t952858651 * L_4 = __this->get_storeResult_13();
		FsmString_t952858651 * L_5 = __this->get_stringVariable_11();
		NullCheck(L_5);
		String_t* L_6 = FsmString_get_Value_m872383149(L_5, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_7 = __this->get_charCount_12();
		NullCheck(L_7);
		int32_t L_8 = FsmInt_get_Value_m27059446(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_stringVariable_11();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m2979997331(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_8, 0, L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_13 = String_Substring_m675079568(L_6, 0, L_12, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmString_set_Value_m829393196(L_4, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::.ctor()
extern "C"  void GetStringLength__ctor_m2752198587 (GetStringLength_t1895851483 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::Reset()
extern "C"  void GetStringLength_Reset_m398631528 (GetStringLength_t1895851483 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_11((FsmString_t952858651 *)NULL);
		__this->set_storeResult_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnEnter()
extern "C"  void GetStringLength_OnEnter_m2849683666 (GetStringLength_t1895851483 * __this, const MethodInfo* method)
{
	{
		GetStringLength_DoGetStringLength_m1261640987(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnUpdate()
extern "C"  void GetStringLength_OnUpdate_m1574407089 (GetStringLength_t1895851483 * __this, const MethodInfo* method)
{
	{
		GetStringLength_DoGetStringLength_m1261640987(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::DoGetStringLength()
extern "C"  void GetStringLength_DoGetStringLength_m1261640987 (GetStringLength_t1895851483 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmInt_t1596138449 * L_1 = __this->get_storeResult_12();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmInt_t1596138449 * L_2 = __this->get_storeResult_12();
		FsmString_t952858651 * L_3 = __this->get_stringVariable_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmInt_set_Value_m2087583461(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::.ctor()
extern "C"  void GetStringRight__ctor_m2505439945 (GetStringRight_t919028285 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::Reset()
extern "C"  void GetStringRight_Reset_m151872886 (GetStringRight_t919028285 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_charCount_12(L_0);
		__this->set_storeResult_13((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnEnter()
extern "C"  void GetStringRight_OnEnter_m1937829984 (GetStringRight_t919028285 * __this, const MethodInfo* method)
{
	{
		GetStringRight_DoGetStringRight_m4250366747(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnUpdate()
extern "C"  void GetStringRight_OnUpdate_m3371714019 (GetStringRight_t919028285 * __this, const MethodInfo* method)
{
	{
		GetStringRight_DoGetStringRight_m4250366747(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::DoGetStringRight()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetStringRight_DoGetStringRight_m4250366747_MetadataUsageId;
extern "C"  void GetStringRight_DoGetStringRight_m4250366747 (GetStringRight_t919028285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetStringRight_DoGetStringRight_m4250366747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		FsmString_t952858651 * L_2 = __this->get_storeResult_13();
		NullCheck(L_2);
		bool L_3 = NamedVariable_get_IsNone_m281035543(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		FsmString_t952858651 * L_4 = __this->get_stringVariable_11();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		FsmInt_t1596138449 * L_6 = __this->get_charCount_12();
		NullCheck(L_6);
		int32_t L_7 = FsmInt_get_Value_m27059446(L_6, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m2979997331(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Clamp_m510460741(NULL /*static, unused*/, L_7, 0, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		FsmString_t952858651 * L_11 = __this->get_storeResult_13();
		String_t* L_12 = V_0;
		String_t* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		int32_t L_16 = V_1;
		NullCheck(L_12);
		String_t* L_17 = String_Substring_m675079568(L_12, ((int32_t)((int32_t)L_14-(int32_t)L_15)), L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmString_set_Value_m829393196(L_11, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::.ctor()
extern "C"  void GetSubstring__ctor_m2357454851 (GetSubstring_t4098822595 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::Reset()
extern "C"  void GetSubstring_Reset_m3887792 (GetSubstring_t4098822595 * __this, const MethodInfo* method)
{
	{
		__this->set_stringVariable_11((FsmString_t952858651 *)NULL);
		FsmInt_t1596138449 * L_0 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_startIndex_12(L_0);
		FsmInt_t1596138449 * L_1 = FsmInt_op_Implicit_m1006909518(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->set_length_13(L_1);
		__this->set_storeResult_14((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnEnter()
extern "C"  void GetSubstring_OnEnter_m1458075418 (GetSubstring_t4098822595 * __this, const MethodInfo* method)
{
	{
		GetSubstring_DoGetSubstring_m2596261671(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnUpdate()
extern "C"  void GetSubstring_OnUpdate_m1384224361 (GetSubstring_t4098822595 * __this, const MethodInfo* method)
{
	{
		GetSubstring_DoGetSubstring_m2596261671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::DoGetSubstring()
extern "C"  void GetSubstring_DoGetSubstring_m2596261671 (GetSubstring_t4098822595 * __this, const MethodInfo* method)
{
	{
		FsmString_t952858651 * L_0 = __this->get_stringVariable_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmString_t952858651 * L_1 = __this->get_storeResult_14();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmString_t952858651 * L_2 = __this->get_storeResult_14();
		FsmString_t952858651 * L_3 = __this->get_stringVariable_11();
		NullCheck(L_3);
		String_t* L_4 = FsmString_get_Value_m872383149(L_3, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_5 = __this->get_startIndex_12();
		NullCheck(L_5);
		int32_t L_6 = FsmInt_get_Value_m27059446(L_5, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_7 = __this->get_length_13();
		NullCheck(L_7);
		int32_t L_8 = FsmInt_get_Value_m27059446(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_9 = String_Substring_m675079568(L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmString_set_Value_m829393196(L_2, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::.ctor()
extern "C"  void GetSystemDateTime__ctor_m3383823560 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::Reset()
extern Il2CppCodeGenString* _stringLiteral3123258170;
extern const uint32_t GetSystemDateTime_Reset_m1030256501_MetadataUsageId;
extern "C"  void GetSystemDateTime_Reset_m1030256501 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSystemDateTime_Reset_m1030256501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_storeString_11((FsmString_t952858651 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral3123258170, /*hidden argument*/NULL);
		__this->set_format_12(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::OnEnter()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t GetSystemDateTime_OnEnter_m4250893983_MetadataUsageId;
extern "C"  void GetSystemDateTime_OnEnter_m4250893983 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSystemDateTime_OnEnter_m4250893983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmString_t952858651 * L_0 = __this->get_storeString_11();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmString_t952858651 * L_2 = __this->get_format_12();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		String_t* L_4 = DateTime_ToString_m3415116655((&V_0), L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmString_set_Value_m829393196(L_0, L_4, /*hidden argument*/NULL);
		bool L_5 = __this->get_everyFrame_13();
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetSystemDateTime::OnUpdate()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t GetSystemDateTime_OnUpdate_m2062253956_MetadataUsageId;
extern "C"  void GetSystemDateTime_OnUpdate_m2062253956 (GetSystemDateTime_t1295868974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetSystemDateTime_OnUpdate_m2062253956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmString_t952858651 * L_0 = __this->get_storeString_11();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmString_t952858651 * L_2 = __this->get_format_12();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		String_t* L_4 = DateTime_ToString_m3415116655((&V_0), L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmString_set_Value_m829393196(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTag::.ctor()
extern "C"  void GetTag__ctor_m3949859226 (GetTag_t2986517580 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTag::Reset()
extern "C"  void GetTag_Reset_m1596292167 (GetTag_t2986517580 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_storeResult_12((FsmString_t952858651 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnEnter()
extern "C"  void GetTag_OnEnter_m2750322417 (GetTag_t2986517580 * __this, const MethodInfo* method)
{
	{
		GetTag_DoGetTag_m3786719481(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnUpdate()
extern "C"  void GetTag_OnUpdate_m2789175666 (GetTag_t2986517580 * __this, const MethodInfo* method)
{
	{
		GetTag_DoGetTag_m3786719481(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTag::DoGetTag()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetTag_DoGetTag_m3786719481_MetadataUsageId;
extern "C"  void GetTag_DoGetTag_m3786719481 (GetTag_t2986517580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTag_DoGetTag_m3786719481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmString_t952858651 * L_3 = __this->get_storeResult_12();
		FsmGameObject_t1697147867 * L_4 = __this->get_gameObject_11();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = FsmGameObject_get_Value_m673294275(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m211612200(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmString_set_Value_m829393196(L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTagCount::.ctor()
extern "C"  void GetTagCount__ctor_m671895997 (GetTagCount_t870241689 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTagCount::Reset()
extern Il2CppCodeGenString* _stringLiteral69913957;
extern const uint32_t GetTagCount_Reset_m2613296234_MetadataUsageId;
extern "C"  void GetTagCount_Reset_m2613296234 (GetTagCount_t870241689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTagCount_Reset_m2613296234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral69913957, /*hidden argument*/NULL);
		__this->set_tag_11(L_0);
		__this->set_storeResult_12((FsmInt_t1596138449 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTagCount::OnEnter()
extern "C"  void GetTagCount_OnEnter_m838687316 (GetTagCount_t870241689 * __this, const MethodInfo* method)
{
	GameObjectU5BU5D_t2662109048* V_0 = NULL;
	FsmInt_t1596138449 * G_B3_0 = NULL;
	FsmInt_t1596138449 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	FsmInt_t1596138449 * G_B4_1 = NULL;
	{
		FsmString_t952858651 * L_0 = __this->get_tag_11();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_2 = GameObject_FindGameObjectsWithTag_m3058873418(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmInt_t1596138449 * L_3 = __this->get_storeResult_12();
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		FsmInt_t1596138449 * L_4 = __this->get_storeResult_12();
		GameObjectU5BU5D_t2662109048* L_5 = V_0;
		G_B2_0 = L_4;
		if (!L_5)
		{
			G_B3_0 = L_4;
			goto IL_0030;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_6 = V_0;
		NullCheck(L_6);
		G_B4_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))));
		G_B4_1 = G_B2_0;
		goto IL_0031;
	}

IL_0030:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0031:
	{
		NullCheck(G_B4_1);
		FsmInt_set_Value_m2087583461(G_B4_1, G_B4_0, /*hidden argument*/NULL);
	}

IL_0036:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTan::.ctor()
extern "C"  void GetTan__ctor_m2574264691 (GetTan_t2986517587 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTan::Reset()
extern "C"  void GetTan_Reset_m220697632 (GetTan_t2986517587 * __this, const MethodInfo* method)
{
	{
		__this->set_angle_11((FsmFloat_t2134102846 *)NULL);
		FsmBool_t1075959796 * L_0 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_DegToRad_12(L_0);
		__this->set_everyFrame_14((bool)0);
		__this->set_result_13((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnEnter()
extern "C"  void GetTan_OnEnter_m3653901450 (GetTan_t2986517587 * __this, const MethodInfo* method)
{
	{
		GetTan_DoTan_m938085415(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnUpdate()
extern "C"  void GetTan_OnUpdate_m735354617 (GetTan_t2986517587 * __this, const MethodInfo* method)
{
	{
		GetTan_DoTan_m938085415(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTan::DoTan()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t GetTan_DoTan_m938085415_MetadataUsageId;
extern "C"  void GetTan_DoTan_m938085415 (GetTan_t2986517587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTan_DoTan_m938085415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		FsmFloat_t2134102846 * L_0 = __this->get_angle_11();
		NullCheck(L_0);
		float L_1 = FsmFloat_get_Value_m4137923823(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmBool_t1075959796 * L_2 = __this->get_DegToRad_12();
		NullCheck(L_2);
		bool L_3 = FsmBool_get_Value_m3101329097(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4*(float)(0.0174532924f)));
	}

IL_0024:
	{
		FsmFloat_t2134102846 * L_5 = __this->get_result_13();
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = tanf(L_6);
		NullCheck(L_5);
		FsmFloat_set_Value_m1568963140(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::.ctor()
extern "C"  void GetTimeInfo__ctor_m1162951927 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::Reset()
extern "C"  void GetTimeInfo_Reset_m3104352164 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method)
{
	{
		__this->set_getInfo_11(5);
		__this->set_storeValue_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnEnter()
extern "C"  void GetTimeInfo_OnEnter_m297033486 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method)
{
	{
		GetTimeInfo_DoGetTimeInfo_m1705919899(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnUpdate()
extern "C"  void GetTimeInfo_OnUpdate_m4046630133 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method)
{
	{
		GetTimeInfo_DoGetTimeInfo_m1705919899(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::DoGetTimeInfo()
extern "C"  void GetTimeInfo_DoGetTimeInfo_m1705919899 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_getInfo_11();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0032;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
		if (L_1 == 2)
		{
			goto IL_005c;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_008c;
		}
		if (L_1 == 5)
		{
			goto IL_00a1;
		}
		if (L_1 == 6)
		{
			goto IL_00b6;
		}
		if (L_1 == 7)
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_00ec;
	}

IL_0032:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeValue_12();
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_3, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_0047:
	{
		FsmFloat_t2134102846 * L_4 = __this->get_storeValue_12();
		float L_5 = Time_get_timeScale_m1970669766(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmFloat_set_Value_m1568963140(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_005c:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeValue_12();
		float L_7 = Time_get_smoothDeltaTime_m1119418976(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		FsmFloat_set_Value_m1568963140(L_6, L_7, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_0071:
	{
		FsmFloat_t2134102846 * L_8 = __this->get_storeValue_12();
		FsmState_t2146334067 * L_9 = FsmStateAction_get_State_m763080396(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		float L_10 = FsmState_get_StateTime_m459577127(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		FsmFloat_set_Value_m1568963140(L_8, L_10, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_008c:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_storeValue_12();
		float L_12 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		FsmFloat_set_Value_m1568963140(L_11, L_12, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_00a1:
	{
		FsmFloat_t2134102846 * L_13 = __this->get_storeValue_12();
		float L_14 = Time_get_timeSinceLevelLoad_m441028310(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		FsmFloat_set_Value_m1568963140(L_13, L_14, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_00b6:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_storeValue_12();
		float L_16 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		FsmFloat_set_Value_m1568963140(L_15, L_16, /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_00cb:
	{
		FsmFloat_t2134102846 * L_17 = __this->get_storeValue_12();
		float L_18 = FsmTime_get_RealtimeSinceStartup_m372847495(NULL /*static, unused*/, /*hidden argument*/NULL);
		FsmState_t2146334067 * L_19 = FsmStateAction_get_State_m763080396(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		float L_20 = FsmState_get_RealStartTime_m648470970(L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmFloat_set_Value_m1568963140(L_17, ((float)((float)L_18-(float)L_20)), /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_00ec:
	{
		FsmFloat_t2134102846 * L_21 = __this->get_storeValue_12();
		NullCheck(L_21);
		FsmFloat_set_Value_m1568963140(L_21, (0.0f), /*hidden argument*/NULL);
		goto IL_0101;
	}

IL_0101:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::.ctor()
extern "C"  void GetTouchCount__ctor_m1907268834 (GetTouchCount_t970498772 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::Reset()
extern "C"  void GetTouchCount_Reset_m3848669071 (GetTouchCount_t970498772 * __this, const MethodInfo* method)
{
	{
		__this->set_storeCount_11((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_12((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnEnter()
extern "C"  void GetTouchCount_OnEnter_m2621009977 (GetTouchCount_t970498772 * __this, const MethodInfo* method)
{
	{
		GetTouchCount_DoGetTouchCount_m337539771(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_12();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnUpdate()
extern "C"  void GetTouchCount_OnUpdate_m3075457322 (GetTouchCount_t970498772 * __this, const MethodInfo* method)
{
	{
		GetTouchCount_DoGetTouchCount_m337539771(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::DoGetTouchCount()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetTouchCount_DoGetTouchCount_m337539771_MetadataUsageId;
extern "C"  void GetTouchCount_DoGetTouchCount_m337539771 (GetTouchCount_t970498772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTouchCount_DoGetTouchCount_m337539771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FsmInt_t1596138449 * L_0 = __this->get_storeCount_11();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmInt_set_Value_m2087583461(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::.ctor()
extern "C"  void GetTouchInfo__ctor_m3849591111 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method)
{
	{
		__this->set_everyFrame_21((bool)1);
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::Reset()
extern Il2CppClass* FsmInt_t1596138449_il2cpp_TypeInfo_var;
extern const uint32_t GetTouchInfo_Reset_m1496024052_MetadataUsageId;
extern "C"  void GetTouchInfo_Reset_m1496024052 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTouchInfo_Reset_m1496024052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmInt_t1596138449 * V_0 = NULL;
	{
		FsmInt_t1596138449 * L_0 = (FsmInt_t1596138449 *)il2cpp_codegen_object_new(FsmInt_t1596138449_il2cpp_TypeInfo_var);
		FsmInt__ctor_m2389102498(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmInt_t1596138449 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_2 = V_0;
		__this->set_fingerId_11(L_2);
		FsmBool_t1075959796 * L_3 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalize_12(L_3);
		__this->set_storePosition_13((FsmVector3_t533912882 *)NULL);
		__this->set_storeDeltaPosition_16((FsmVector3_t533912882 *)NULL);
		__this->set_storeDeltaTime_19((FsmFloat_t2134102846 *)NULL);
		__this->set_storeTapCount_20((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_21((bool)1);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnEnter()
extern "C"  void GetTouchInfo_OnEnter_m881944414 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_screenWidth_22((((float)((float)L_0))));
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_screenHeight_23((((float)((float)L_1))));
		GetTouchInfo_DoGetTouchInfo_m3752312223(__this, /*hidden argument*/NULL);
		bool L_2 = __this->get_everyFrame_21();
		if (L_2)
		{
			goto IL_002f;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnUpdate()
extern "C"  void GetTouchInfo_OnUpdate_m704032421 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method)
{
	{
		GetTouchInfo_DoGetTouchInfo_m3752312223(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::DoGetTouchInfo()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t GetTouchInfo_DoGetTouchInfo_m3752312223_MetadataUsageId;
extern "C"  void GetTouchInfo_DoGetTouchInfo_m3752312223 (GetTouchInfo_t3931743231 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTouchInfo_DoGetTouchInfo_m3752312223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t4210255029  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TouchU5BU5D_t3635654872* V_1 = NULL;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t4282066565  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t4282066565  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t4282066565  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t4282066565  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t4282066565  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector2_t4282066565  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector2_t4282066565  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float G_B7_0 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B15_0 = 0.0f;
	float G_B18_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_01f8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3635654872* L_1 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_01ef;
	}

IL_0018:
	{
		TouchU5BU5D_t3635654872* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		V_0 = (*(Touch_t4210255029 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		FsmInt_t1596138449 * L_4 = __this->get_fingerId_11();
		NullCheck(L_4);
		bool L_5 = NamedVariable_get_IsNone_m281035543(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_6 = Touch_get_fingerId_m1427167959((&V_0), /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_7 = __this->get_fingerId_11();
		NullCheck(L_7);
		int32_t L_8 = FsmInt_get_Value_m27059446(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_01eb;
		}
	}

IL_004c:
	{
		FsmBool_t1075959796 * L_9 = __this->get_normalize_12();
		NullCheck(L_9);
		bool L_10 = FsmBool_get_Value_m3101329097(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0071;
		}
	}
	{
		Vector2_t4282066565  L_11 = Touch_get_position_m1943849441((&V_0), /*hidden argument*/NULL);
		V_7 = L_11;
		float L_12 = (&V_7)->get_x_1();
		G_B7_0 = L_12;
		goto IL_0088;
	}

IL_0071:
	{
		Vector2_t4282066565  L_13 = Touch_get_position_m1943849441((&V_0), /*hidden argument*/NULL);
		V_8 = L_13;
		float L_14 = (&V_8)->get_x_1();
		float L_15 = __this->get_screenWidth_22();
		G_B7_0 = ((float)((float)L_14/(float)L_15));
	}

IL_0088:
	{
		V_3 = G_B7_0;
		FsmBool_t1075959796 * L_16 = __this->get_normalize_12();
		NullCheck(L_16);
		bool L_17 = FsmBool_get_Value_m3101329097(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00ae;
		}
	}
	{
		Vector2_t4282066565  L_18 = Touch_get_position_m1943849441((&V_0), /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = (&V_9)->get_y_2();
		G_B10_0 = L_19;
		goto IL_00c5;
	}

IL_00ae:
	{
		Vector2_t4282066565  L_20 = Touch_get_position_m1943849441((&V_0), /*hidden argument*/NULL);
		V_10 = L_20;
		float L_21 = (&V_10)->get_y_2();
		float L_22 = __this->get_screenHeight_23();
		G_B10_0 = ((float)((float)L_21/(float)L_22));
	}

IL_00c5:
	{
		V_4 = G_B10_0;
		FsmVector3_t533912882 * L_23 = __this->get_storePosition_13();
		NullCheck(L_23);
		bool L_24 = NamedVariable_get_IsNone_m281035543(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00ef;
		}
	}
	{
		FsmVector3_t533912882 * L_25 = __this->get_storePosition_13();
		float L_26 = V_3;
		float L_27 = V_4;
		Vector3_t4282066566  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2926210380(&L_28, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		FsmVector3_set_Value_m716982822(L_25, L_28, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		FsmFloat_t2134102846 * L_29 = __this->get_storeX_14();
		float L_30 = V_3;
		NullCheck(L_29);
		FsmFloat_set_Value_m1568963140(L_29, L_30, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_31 = __this->get_storeY_15();
		float L_32 = V_4;
		NullCheck(L_31);
		FsmFloat_set_Value_m1568963140(L_31, L_32, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_33 = __this->get_normalize_12();
		NullCheck(L_33);
		bool L_34 = FsmBool_get_Value_m3101329097(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_012d;
		}
	}
	{
		Vector2_t4282066565  L_35 = Touch_get_deltaPosition_m3983677995((&V_0), /*hidden argument*/NULL);
		V_11 = L_35;
		float L_36 = (&V_11)->get_x_1();
		G_B15_0 = L_36;
		goto IL_0144;
	}

IL_012d:
	{
		Vector2_t4282066565  L_37 = Touch_get_deltaPosition_m3983677995((&V_0), /*hidden argument*/NULL);
		V_12 = L_37;
		float L_38 = (&V_12)->get_x_1();
		float L_39 = __this->get_screenWidth_22();
		G_B15_0 = ((float)((float)L_38/(float)L_39));
	}

IL_0144:
	{
		V_5 = G_B15_0;
		FsmBool_t1075959796 * L_40 = __this->get_normalize_12();
		NullCheck(L_40);
		bool L_41 = FsmBool_get_Value_m3101329097(L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_016b;
		}
	}
	{
		Vector2_t4282066565  L_42 = Touch_get_deltaPosition_m3983677995((&V_0), /*hidden argument*/NULL);
		V_13 = L_42;
		float L_43 = (&V_13)->get_y_2();
		G_B18_0 = L_43;
		goto IL_0182;
	}

IL_016b:
	{
		Vector2_t4282066565  L_44 = Touch_get_deltaPosition_m3983677995((&V_0), /*hidden argument*/NULL);
		V_14 = L_44;
		float L_45 = (&V_14)->get_y_2();
		float L_46 = __this->get_screenHeight_23();
		G_B18_0 = ((float)((float)L_45/(float)L_46));
	}

IL_0182:
	{
		V_6 = G_B18_0;
		FsmVector3_t533912882 * L_47 = __this->get_storeDeltaPosition_16();
		NullCheck(L_47);
		bool L_48 = NamedVariable_get_IsNone_m281035543(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01ad;
		}
	}
	{
		FsmVector3_t533912882 * L_49 = __this->get_storeDeltaPosition_16();
		float L_50 = V_5;
		float L_51 = V_6;
		Vector3_t4282066566  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector3__ctor_m2926210380(&L_52, L_50, L_51, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_49);
		FsmVector3_set_Value_m716982822(L_49, L_52, /*hidden argument*/NULL);
	}

IL_01ad:
	{
		FsmFloat_t2134102846 * L_53 = __this->get_storeDeltaX_17();
		float L_54 = V_5;
		NullCheck(L_53);
		FsmFloat_set_Value_m1568963140(L_53, L_54, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_55 = __this->get_storeDeltaY_18();
		float L_56 = V_6;
		NullCheck(L_55);
		FsmFloat_set_Value_m1568963140(L_55, L_56, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_57 = __this->get_storeDeltaTime_19();
		float L_58 = Touch_get_deltaTime_m2685662144((&V_0), /*hidden argument*/NULL);
		NullCheck(L_57);
		FsmFloat_set_Value_m1568963140(L_57, L_58, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_59 = __this->get_storeTapCount_20();
		int32_t L_60 = Touch_get_tapCount_m2141151647((&V_0), /*hidden argument*/NULL);
		NullCheck(L_59);
		FsmInt_set_Value_m2087583461(L_59, L_60, /*hidden argument*/NULL);
	}

IL_01eb:
	{
		int32_t L_61 = V_2;
		V_2 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_01ef:
	{
		int32_t L_62 = V_2;
		TouchU5BU5D_t3635654872* L_63 = V_1;
		NullCheck(L_63);
		if ((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length)))))))
		{
			goto IL_0018;
		}
	}

IL_01f8:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTransform::.ctor()
extern "C"  void GetTransform__ctor_m3887102280 (GetTransform_t325979870 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTransform::Reset()
extern Il2CppClass* FsmGameObject_t1697147867_il2cpp_TypeInfo_var;
extern const uint32_t GetTransform_Reset_m1533535221_MetadataUsageId;
extern "C"  void GetTransform_Reset_m1533535221 (GetTransform_t325979870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTransform_Reset_m1533535221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmGameObject_t1697147867 * V_0 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = (FsmGameObject_t1697147867 *)il2cpp_codegen_object_new(FsmGameObject_t1697147867_il2cpp_TypeInfo_var);
		FsmGameObject__ctor_m1048269796(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmGameObject_t1697147867 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmGameObject_t1697147867 * L_2 = V_0;
		__this->set_gameObject_11(L_2);
		__this->set_storeTransform_12((FsmObject_t821476169 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnEnter()
extern "C"  void GetTransform_OnEnter_m2570439455 (GetTransform_t325979870 * __this, const MethodInfo* method)
{
	{
		GetTransform_DoGetGameObjectName_m1570979725(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnUpdate()
extern "C"  void GetTransform_OnUpdate_m1507771140 (GetTransform_t325979870 * __this, const MethodInfo* method)
{
	{
		GetTransform_DoGetGameObjectName_m1570979725(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTransform::DoGetGameObjectName()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetTransform_DoGetGameObjectName_m1570979725_MetadataUsageId;
extern "C"  void GetTransform_DoGetGameObjectName_m1570979725 (GetTransform_t325979870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTransform_DoGetGameObjectName_m1570979725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	FsmObject_t821476169 * G_B2_0 = NULL;
	FsmObject_t821476169 * G_B1_0 = NULL;
	Transform_t1659122786 * G_B3_0 = NULL;
	FsmObject_t821476169 * G_B3_1 = NULL;
	{
		FsmGameObject_t1697147867 * L_0 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = FsmGameObject_get_Value_m673294275(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FsmObject_t821476169 * L_2 = __this->get_storeTransform_12();
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_2;
		if (!L_4)
		{
			G_B2_0 = L_2;
			goto IL_0029;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = ((Transform_t1659122786 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		FsmObject_set_Value_m867520242(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::.ctor()
extern "C"  void GetTrigger2dInfo__ctor_m1638809532 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::Reset()
extern "C"  void GetTrigger2dInfo_Reset_m3580209769 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_shapeCount_12((FsmInt_t1596138449 *)NULL);
		__this->set_physics2dMaterialName_13((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::StoreTriggerInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GetTrigger2dInfo_StoreTriggerInfo_m4123633965_MetadataUsageId;
extern "C"  void GetTrigger2dInfo_StoreTriggerInfo_m4123633965 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTrigger2dInfo_StoreTriggerInfo_m4123633965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmString_t952858651 * G_B4_0 = NULL;
	FsmString_t952858651 * G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	FsmString_t952858651 * G_B5_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Collider2D_t1552025098 * L_1 = Fsm_get_TriggerCollider2D_m1137780652(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmGameObject_t1697147867 * L_3 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Collider2D_t1552025098 * L_5 = Fsm_get_TriggerCollider2D_m1137780652(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmGameObject_set_Value_m297051598(L_3, L_6, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_7 = __this->get_shapeCount_12();
		Fsm_t1527112426 * L_8 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Collider2D_t1552025098 * L_9 = Fsm_get_TriggerCollider2D_m1137780652(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = Collider2D_get_shapeCount_m2388185620(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmInt_set_Value_m2087583461(L_7, L_10, /*hidden argument*/NULL);
		FsmString_t952858651 * L_11 = __this->get_physics2dMaterialName_13();
		Fsm_t1527112426 * L_12 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Collider2D_t1552025098 * L_13 = Fsm_get_TriggerCollider2D_m1137780652(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		PhysicsMaterial2D_t1327932246 * L_14 = Collider2D_get_sharedMaterial_m2954504316(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = L_11;
		if (!L_15)
		{
			G_B4_0 = L_11;
			goto IL_0088;
		}
	}
	{
		Fsm_t1527112426 * L_16 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Collider2D_t1552025098 * L_17 = Fsm_get_TriggerCollider2D_m1137780652(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		PhysicsMaterial2D_t1327932246 * L_18 = Collider2D_get_sharedMaterial_m2954504316(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m3709440845(L_18, /*hidden argument*/NULL);
		G_B5_0 = L_19;
		G_B5_1 = G_B3_0;
		goto IL_008d;
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B5_0 = L_20;
		G_B5_1 = G_B4_0;
	}

IL_008d:
	{
		NullCheck(G_B5_1);
		FsmString_set_Value_m829393196(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::OnEnter()
extern "C"  void GetTrigger2dInfo_OnEnter_m2329658515 (GetTrigger2dInfo_t3888785898 * __this, const MethodInfo* method)
{
	{
		GetTrigger2dInfo_StoreTriggerInfo_m4123633965(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::.ctor()
extern "C"  void GetTriggerInfo__ctor_m712361774 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::Reset()
extern "C"  void GetTriggerInfo_Reset_m2653762011 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObjectHit_11((FsmGameObject_t1697147867 *)NULL);
		__this->set_physicsMaterialName_12((FsmString_t952858651 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::StoreTriggerInfo()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t GetTriggerInfo_StoreTriggerInfo_m3589498491_MetadataUsageId;
extern "C"  void GetTriggerInfo_StoreTriggerInfo_m3589498491 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetTriggerInfo_StoreTriggerInfo_m3589498491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Collider_t2939674232 * L_1 = Fsm_get_TriggerCollider_m2008788780(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		FsmGameObject_t1697147867 * L_3 = __this->get_gameObjectHit_11();
		Fsm_t1527112426 * L_4 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Collider_t2939674232 * L_5 = Fsm_get_TriggerCollider_m2008788780(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmGameObject_set_Value_m297051598(L_3, L_6, /*hidden argument*/NULL);
		FsmString_t952858651 * L_7 = __this->get_physicsMaterialName_12();
		Fsm_t1527112426 * L_8 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Collider_t2939674232 * L_9 = Fsm_get_TriggerCollider_m2008788780(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		PhysicMaterial_t211873335 * L_10 = Collider_get_material_m2158909696(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m3709440845(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		FsmString_set_Value_m829393196(L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::OnEnter()
extern "C"  void GetTriggerInfo_OnEnter_m1071593349 (GetTriggerInfo_t1760428088 * __this, const MethodInfo* method)
{
	{
		GetTriggerInfo_StoreTriggerInfo_m3589498491(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::.ctor()
extern "C"  void GetVector2Length__ctor_m1665383231 (GetVector2Length_t648683271 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::Reset()
extern "C"  void GetVector2Length_Reset_m3606783468 (GetVector2Length_t648683271 * __this, const MethodInfo* method)
{
	{
		__this->set_vector2_11((FsmVector2_t533912881 *)NULL);
		__this->set_storeLength_12((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::OnEnter()
extern "C"  void GetVector2Length_OnEnter_m2097179478 (GetVector2Length_t648683271 * __this, const MethodInfo* method)
{
	{
		GetVector2Length_DoVectorLength_m903341657(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::OnUpdate()
extern "C"  void GetVector2Length_OnUpdate_m4016581037 (GetVector2Length_t648683271 * __this, const MethodInfo* method)
{
	{
		GetVector2Length_DoVectorLength_m903341657(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::DoVectorLength()
extern "C"  void GetVector2Length_DoVectorLength_m903341657 (GetVector2Length_t648683271 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmVector2_t533912881 * L_0 = __this->get_vector2_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmFloat_t2134102846 * L_1 = __this->get_storeLength_12();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeLength_12();
		FsmVector2_t533912881 * L_3 = __this->get_vector2_11();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Vector2_get_magnitude_m1987058139((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::.ctor()
extern "C"  void GetVector2XY__ctor_m3537760932 (GetVector2XY_t1767726594 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::Reset()
extern "C"  void GetVector2XY_Reset_m1184193873 (GetVector2XY_t1767726594 * __this, const MethodInfo* method)
{
	{
		__this->set_vector2Variable_11((FsmVector2_t533912881 *)NULL);
		__this->set_storeX_12((FsmFloat_t2134102846 *)NULL);
		__this->set_storeY_13((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_14((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnEnter()
extern "C"  void GetVector2XY_OnEnter_m1860853115 (GetVector2XY_t1767726594 * __this, const MethodInfo* method)
{
	{
		GetVector2XY_DoGetVector2XYZ_m3636436535(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnUpdate()
extern "C"  void GetVector2XY_OnUpdate_m985431080 (GetVector2XY_t1767726594 * __this, const MethodInfo* method)
{
	{
		GetVector2XY_DoGetVector2XYZ_m3636436535(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::DoGetVector2XYZ()
extern "C"  void GetVector2XY_DoGetVector2XYZ_m3636436535 (GetVector2XY_t1767726594 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		FsmVector2_t533912881 * L_0 = __this->get_vector2Variable_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmFloat_t2134102846 * L_1 = __this->get_storeX_12();
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeX_12();
		FsmVector2_t533912881 * L_3 = __this->get_vector2Variable_11();
		NullCheck(L_3);
		Vector2_t4282066565  L_4 = FsmVector2_get_Value_m1313754285(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
	}

IL_0035:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeY_13();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		FsmFloat_t2134102846 * L_7 = __this->get_storeY_13();
		FsmVector2_t533912881 * L_8 = __this->get_vector2Variable_11();
		NullCheck(L_8);
		Vector2_t4282066565  L_9 = FsmVector2_get_Value_m1313754285(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::.ctor()
extern "C"  void GetVector3XYZ__ctor_m3468242409 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::Reset()
extern "C"  void GetVector3XYZ_Reset_m1114675350 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method)
{
	{
		__this->set_vector3Variable_11((FsmVector3_t533912882 *)NULL);
		__this->set_storeX_12((FsmFloat_t2134102846 *)NULL);
		__this->set_storeY_13((FsmFloat_t2134102846 *)NULL);
		__this->set_storeZ_14((FsmFloat_t2134102846 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnEnter()
extern "C"  void GetVector3XYZ_OnEnter_m3773029248 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method)
{
	{
		GetVector3XYZ_DoGetVector3XYZ_m3613794267(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnUpdate()
extern "C"  void GetVector3XYZ_OnUpdate_m133349059 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method)
{
	{
		GetVector3XYZ_DoGetVector3XYZ_m3613794267(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::DoGetVector3XYZ()
extern "C"  void GetVector3XYZ_DoGetVector3XYZ_m3613794267 (GetVector3XYZ_t2610986477 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3Variable_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmFloat_t2134102846 * L_1 = __this->get_storeX_12();
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeX_12();
		FsmVector3_t533912882 * L_3 = __this->get_vector3Variable_11();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = FsmVector3_get_Value_m2779135117(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
	}

IL_0035:
	{
		FsmFloat_t2134102846 * L_6 = __this->get_storeY_13();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		FsmFloat_t2134102846 * L_7 = __this->get_storeY_13();
		FsmVector3_t533912882 * L_8 = __this->get_vector3Variable_11();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = FsmVector3_get_Value_m2779135117(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		NullCheck(L_7);
		FsmFloat_set_Value_m1568963140(L_7, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		FsmFloat_t2134102846 * L_11 = __this->get_storeZ_14();
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		FsmFloat_t2134102846 * L_12 = __this->get_storeZ_14();
		FsmVector3_t533912882 * L_13 = __this->get_vector3Variable_11();
		NullCheck(L_13);
		Vector3_t4282066566  L_14 = FsmVector3_get_Value_m2779135117(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		float L_15 = (&V_2)->get_z_3();
		NullCheck(L_12);
		FsmFloat_set_Value_m1568963140(L_12, L_15, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::.ctor()
extern "C"  void GetVectorLength__ctor_m2660363337 (GetVectorLength_t2333587853 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::Reset()
extern "C"  void GetVectorLength_Reset_m306796278 (GetVectorLength_t2333587853 * __this, const MethodInfo* method)
{
	{
		__this->set_vector3_11((FsmVector3_t533912882 *)NULL);
		__this->set_storeLength_12((FsmFloat_t2134102846 *)NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::OnEnter()
extern "C"  void GetVectorLength_OnEnter_m495354336 (GetVectorLength_t2333587853 * __this, const MethodInfo* method)
{
	{
		GetVectorLength_DoVectorLength_m1808744591(__this, /*hidden argument*/NULL);
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::DoVectorLength()
extern "C"  void GetVectorLength_DoVectorLength_m1808744591 (GetVectorLength_t2333587853 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FsmVector3_t533912882 * L_0 = __this->get_vector3_11();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		FsmFloat_t2134102846 * L_1 = __this->get_storeLength_12();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		FsmFloat_t2134102846 * L_2 = __this->get_storeLength_12();
		FsmVector3_t533912882 * L_3 = __this->get_vector3_11();
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = FsmVector3_get_Value_m2779135117(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmFloat_set_Value_m1568963140(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m3497509022_MethodInfo_var;
extern const uint32_t GetVelocity__ctor_m2606929781_MetadataUsageId;
extern "C"  void GetVelocity__ctor_m2606929781 (GetVelocity_t3794321633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVelocity__ctor_m2606929781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m3497509022(__this, /*hidden argument*/ComponentAction_1__ctor_m3497509022_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::Reset()
extern "C"  void GetVelocity_Reset_m253362722 (GetVelocity_t3794321633 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_14((FsmVector3_t533912882 *)NULL);
		__this->set_x_15((FsmFloat_t2134102846 *)NULL);
		__this->set_y_16((FsmFloat_t2134102846 *)NULL);
		__this->set_z_17((FsmFloat_t2134102846 *)NULL);
		__this->set_space_18(0);
		__this->set_everyFrame_19((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnEnter()
extern "C"  void GetVelocity_OnEnter_m685314572 (GetVelocity_t3794321633 * __this, const MethodInfo* method)
{
	{
		GetVelocity_DoGetVelocity_m2419104091(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_19();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnUpdate()
extern "C"  void GetVelocity_OnUpdate_m3198441911 (GetVelocity_t3794321633 * __this, const MethodInfo* method)
{
	{
		GetVelocity_DoGetVelocity_m2419104091(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::DoGetVelocity()
extern const MethodInfo* ComponentAction_1_UpdateCache_m864821753_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var;
extern const uint32_t GetVelocity_DoGetVelocity_m2419104091_MetadataUsageId;
extern "C"  void GetVelocity_DoGetVelocity_m2419104091 (GetVelocity_t3794321633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVelocity_DoGetVelocity_m2419104091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m864821753(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m864821753_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Rigidbody_t3346577219 * L_5 = ComponentAction_1_get_rigidbody_m1866302531(__this, /*hidden argument*/ComponentAction_1_get_rigidbody_m1866302531_MethodInfo_var);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Rigidbody_get_velocity_m2696244068(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = __this->get_space_18();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		Vector3_t4282066566  L_10 = V_1;
		NullCheck(L_9);
		Vector3_t4282066566  L_11 = Transform_InverseTransformDirection_m416562129(L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0044:
	{
		FsmVector3_t533912882 * L_12 = __this->get_vector_14();
		Vector3_t4282066566  L_13 = V_1;
		NullCheck(L_12);
		FsmVector3_set_Value_m716982822(L_12, L_13, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_14 = __this->get_x_15();
		float L_15 = (&V_1)->get_x_1();
		NullCheck(L_14);
		FsmFloat_set_Value_m1568963140(L_14, L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_y_16();
		float L_17 = (&V_1)->get_y_2();
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_z_17();
		float L_19 = (&V_1)->get_z_3();
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::.ctor()
extern const MethodInfo* ComponentAction_1__ctor_m2621392432_MethodInfo_var;
extern const uint32_t GetVelocity2d__ctor_m2130033859_MetadataUsageId;
extern "C"  void GetVelocity2d__ctor_m2130033859 (GetVelocity2d_t623964627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVelocity2d__ctor_m2130033859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ComponentAction_1__ctor_m2621392432(__this, /*hidden argument*/ComponentAction_1__ctor_m2621392432_MethodInfo_var);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::Reset()
extern "C"  void GetVelocity2d_Reset_m4071434096 (GetVelocity2d_t623964627 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_13((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_vector_14((FsmVector2_t533912881 *)NULL);
		__this->set_x_15((FsmFloat_t2134102846 *)NULL);
		__this->set_y_16((FsmFloat_t2134102846 *)NULL);
		__this->set_space_17(0);
		__this->set_everyFrame_18((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnEnter()
extern "C"  void GetVelocity2d_OnEnter_m1949834202 (GetVelocity2d_t623964627 * __this, const MethodInfo* method)
{
	{
		GetVelocity2d_DoGetVelocity_m1101566121(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_18();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnUpdate()
extern "C"  void GetVelocity2d_OnUpdate_m3743844777 (GetVelocity2d_t623964627 * __this, const MethodInfo* method)
{
	{
		GetVelocity2d_DoGetVelocity_m1101566121(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::DoGetVelocity()
extern const MethodInfo* ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var;
extern const MethodInfo* ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var;
extern const uint32_t GetVelocity2d_DoGetVelocity_m1101566121_MetadataUsageId;
extern "C"  void GetVelocity2d_DoGetVelocity_m1101566121 (GetVelocity2d_t623964627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVelocity2d_DoGetVelocity_m1101566121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_13();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		bool L_4 = ComponentAction_1_UpdateCache_m2944103819(__this, L_3, /*hidden argument*/ComponentAction_1_UpdateCache_m2944103819_MethodInfo_var);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		Rigidbody2D_t1743771669 * L_5 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		NullCheck(L_5);
		Vector2_t4282066565  L_6 = Rigidbody2D_get_velocity_m416159605(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = __this->get_space_17();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0053;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_8 = ComponentAction_1_get_rigidbody2d_m4015809689(__this, /*hidden argument*/ComponentAction_1_get_rigidbody2d_m4015809689_MethodInfo_var);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = V_1;
		Vector3_t4282066566  L_11 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t4282066566  L_12 = Transform_InverseTransformDirection_m416562129(L_9, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
	}

IL_0053:
	{
		FsmVector2_t533912881 * L_14 = __this->get_vector_14();
		Vector2_t4282066565  L_15 = V_1;
		NullCheck(L_14);
		FsmVector2_set_Value_m2900659718(L_14, L_15, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_16 = __this->get_x_15();
		float L_17 = (&V_1)->get_x_1();
		NullCheck(L_16);
		FsmFloat_set_Value_m1568963140(L_16, L_17, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_18 = __this->get_y_16();
		float L_19 = (&V_1)->get_y_2();
		NullCheck(L_18);
		FsmFloat_set_Value_m1568963140(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::.ctor()
extern "C"  void GetVertexCount__ctor_m69476745 (GetVertexCount_t1542357885 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::Reset()
extern "C"  void GetVertexCount_Reset_m2010876982 (GetVertexCount_t1542357885 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_storeCount_12((FsmInt_t1596138449 *)NULL);
		__this->set_everyFrame_13((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnEnter()
extern "C"  void GetVertexCount_OnEnter_m1734371104 (GetVertexCount_t1542357885 * __this, const MethodInfo* method)
{
	{
		GetVertexCount_DoGetVertexCount_m2112718235(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnUpdate()
extern "C"  void GetVertexCount_OnUpdate_m1359456035 (GetVertexCount_t1542357885 * __this, const MethodInfo* method)
{
	{
		GetVertexCount_DoGetVertexCount_m2112718235(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::DoGetVertexCount()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral174507266;
extern const uint32_t GetVertexCount_DoGetVertexCount_m2112718235_MetadataUsageId;
extern "C"  void GetVertexCount_DoGetVertexCount_m2112718235 (GetVertexCount_t1542357885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVertexCount_DoGetVertexCount_m2112718235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MeshFilter_t3839065225 * V_1 = NULL;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		MeshFilter_t3839065225 * L_6 = GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(L_5, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var);
		V_1 = L_6;
		MeshFilter_t3839065225 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral174507266, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		FsmInt_t1596138449 * L_9 = __this->get_storeCount_12();
		MeshFilter_t3839065225 * L_10 = V_1;
		NullCheck(L_10);
		Mesh_t4241756145 * L_11 = MeshFilter_get_mesh_m484001117(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Mesh_get_vertexCount_m538235264(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		FsmInt_set_Value_m2087583461(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::.ctor()
extern "C"  void GetVertexPosition__ctor_m1741323301 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::Reset()
extern "C"  void GetVertexPosition_Reset_m3682723538 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method)
{
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_space_13(0);
		__this->set_storePosition_14((FsmVector3_t533912882 *)NULL);
		__this->set_everyFrame_15((bool)0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnEnter()
extern "C"  void GetVertexPosition_OnEnter_m2061142716 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method)
{
	{
		GetVertexPosition_DoGetVertexPosition_m3709014619(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_everyFrame_15();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnUpdate()
extern "C"  void GetVertexPosition_OnUpdate_m2899441415 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method)
{
	{
		GetVertexPosition_DoGetVertexPosition_m3709014619(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::DoGetVertexPosition()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral174507266;
extern const uint32_t GetVertexPosition_DoGetVertexPosition_m3709014619_MetadataUsageId;
extern "C"  void GetVertexPosition_DoGetVertexPosition_m3709014619 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GetVertexPosition_DoGetVertexPosition_m3709014619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	MeshFilter_t3839065225 * V_1 = NULL;
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00c3;
		}
	}
	{
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		MeshFilter_t3839065225 * L_6 = GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(L_5, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var);
		V_1 = L_6;
		MeshFilter_t3839065225 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		FsmStateAction_LogError_m3478223492(__this, _stringLiteral174507266, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		int32_t L_9 = __this->get_space_13();
		V_3 = L_9;
		int32_t L_10 = V_3;
		if (!L_10)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_0093;
		}
	}
	{
		goto IL_00c3;
	}

IL_0056:
	{
		MeshFilter_t3839065225 * L_12 = V_1;
		NullCheck(L_12);
		Mesh_t4241756145 * L_13 = MeshFilter_get_mesh_m484001117(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3U5BU5D_t215400611* L_14 = Mesh_get_vertices_m3685486174(L_13, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_15 = __this->get_vertexIndex_12();
		NullCheck(L_15);
		int32_t L_16 = FsmInt_get_Value_m27059446(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		V_2 = (*(Vector3_t4282066566 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16))));
		FsmVector3_t533912882 * L_17 = __this->get_storePosition_14();
		GameObject_t3674682005 * L_18 = V_0;
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = V_2;
		NullCheck(L_19);
		Vector3_t4282066566  L_21 = Transform_TransformPoint_m437395512(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		FsmVector3_set_Value_m716982822(L_17, L_21, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_0093:
	{
		FsmVector3_t533912882 * L_22 = __this->get_storePosition_14();
		MeshFilter_t3839065225 * L_23 = V_1;
		NullCheck(L_23);
		Mesh_t4241756145 * L_24 = MeshFilter_get_mesh_m484001117(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3U5BU5D_t215400611* L_25 = Mesh_get_vertices_m3685486174(L_24, /*hidden argument*/NULL);
		FsmInt_t1596138449 * L_26 = __this->get_vertexIndex_12();
		NullCheck(L_26);
		int32_t L_27 = FsmInt_get_Value_m27059446(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		NullCheck(L_22);
		FsmVector3_set_Value_m716982822(L_22, (*(Vector3_t4282066566 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27)))), /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_00c3:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::.ctor()
extern "C"  void GotoPreviousState__ctor_m303624017 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::Reset()
extern "C"  void GotoPreviousState_Reset_m2245024254 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GotoPreviousState::OnEnter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3810023499;
extern const uint32_t GotoPreviousState_OnEnter_m3411600104_MetadataUsageId;
extern "C"  void GotoPreviousState_OnEnter_m3411600104 (GotoPreviousState_t1461179269 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GotoPreviousState_OnEnter_m3411600104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		FsmState_t2146334067 * L_1 = Fsm_get_PreviousActiveState_m1798855298(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		Fsm_t1527112426 * L_2 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		FsmState_t2146334067 * L_3 = Fsm_get_PreviousActiveState_m1798855298(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = FsmState_get_Name_m3930587579(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3810023499, L_4, /*hidden argument*/NULL);
		FsmStateAction_Log_m3811764982(__this, L_5, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Fsm_GotoPreviousState_m3818426570(L_6, /*hidden argument*/NULL);
	}

IL_003b:
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIAction::.ctor()
extern "C"  void GUIAction__ctor_m141141367 (GUIAction_t3055477407 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIAction::Reset()
extern "C"  void GUIAction_Reset_m2082541604 (GUIAction_t3055477407 * __this, const MethodInfo* method)
{
	{
		__this->set_screenRect_11((FsmRect_t1076426478 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_left_12(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_top_13(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_width_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_height_15(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_16(L_4);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIAction::OnGUI()
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t GUIAction_OnGUI_m3931507313_MetadataUsageId;
extern "C"  void GUIAction_OnGUI_m3931507313 (GUIAction_t3055477407 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIAction_OnGUI_m3931507313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GUIAction_t3055477407 * G_B2_0 = NULL;
	GUIAction_t3055477407 * G_B1_0 = NULL;
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	GUIAction_t3055477407 * G_B3_1 = NULL;
	{
		FsmRect_t1076426478 * L_0 = __this->get_screenRect_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0021;
		}
	}
	{
		FsmRect_t1076426478 * L_2 = __this->get_screenRect_11();
		NullCheck(L_2);
		Rect_t4241904616  L_3 = FsmRect_get_Value_m1002500317(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0021:
	{
		Initobj (Rect_t4241904616_il2cpp_TypeInfo_var, (&V_0));
		Rect_t4241904616  L_4 = V_0;
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_rect_17(G_B3_0);
		FsmFloat_t2134102846 * L_5 = __this->get_left_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		Rect_t4241904616 * L_7 = __this->get_address_of_rect_17();
		FsmFloat_t2134102846 * L_8 = __this->get_left_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0055:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_top_13();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007b;
		}
	}
	{
		Rect_t4241904616 * L_12 = __this->get_address_of_rect_17();
		FsmFloat_t2134102846 * L_13 = __this->get_top_13();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_12, L_14, /*hidden argument*/NULL);
	}

IL_007b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_width_14();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a1;
		}
	}
	{
		Rect_t4241904616 * L_17 = __this->get_address_of_rect_17();
		FsmFloat_t2134102846 * L_18 = __this->get_width_14();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_height_15();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00c7;
		}
	}
	{
		Rect_t4241904616 * L_22 = __this->get_address_of_rect_17();
		FsmFloat_t2134102846 * L_23 = __this->get_height_15();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_22, L_24, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		FsmBool_t1075959796 * L_25 = __this->get_normalized_16();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0137;
		}
	}
	{
		Rect_t4241904616 * L_27 = __this->get_address_of_rect_17();
		Rect_t4241904616 * L_28 = L_27;
		float L_29 = Rect_get_x_m982385354(L_28, /*hidden argument*/NULL);
		int32_t L_30 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_28, ((float)((float)L_29*(float)(((float)((float)L_30))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_31 = __this->get_address_of_rect_17();
		Rect_t4241904616 * L_32 = L_31;
		float L_33 = Rect_get_width_m2824209432(L_32, /*hidden argument*/NULL);
		int32_t L_34 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_32, ((float)((float)L_33*(float)(((float)((float)L_34))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_35 = __this->get_address_of_rect_17();
		Rect_t4241904616 * L_36 = L_35;
		float L_37 = Rect_get_y_m982386315(L_36, /*hidden argument*/NULL);
		int32_t L_38 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_36, ((float)((float)L_37*(float)(((float)((float)L_38))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_39 = __this->get_address_of_rect_17();
		Rect_t4241904616 * L_40 = L_39;
		float L_41 = Rect_get_height_m2154960823(L_40, /*hidden argument*/NULL);
		int32_t L_42 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_40, ((float)((float)L_41*(float)(((float)((float)L_42))))), /*hidden argument*/NULL);
	}

IL_0137:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIBox::.ctor()
extern "C"  void GUIBox__ctor_m2423523566 (GUIBox_t2970443384 * __this, const MethodInfo* method)
{
	{
		GUIContentAction__ctor_m2980405162(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIBox::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUIBox_OnGUI_m1918922216_MetadataUsageId;
extern "C"  void GUIBox_OnGUI_m1918922216 (GUIBox_t2970443384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIBox_OnGUI_m1918922216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContentAction_OnGUI_m2475803812(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = ((GUIContentAction_t3188246076 *)__this)->get_style_21();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Rect_t4241904616  L_3 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		GUIContent_t2094828418 * L_4 = ((GUIContentAction_t3188246076 *)__this)->get_content_22();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Box_m1237592453(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		goto IL_0052;
	}

IL_0031:
	{
		Rect_t4241904616  L_5 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		GUIContent_t2094828418 * L_6 = ((GUIContentAction_t3188246076 *)__this)->get_content_22();
		FsmString_t952858651 * L_7 = ((GUIContentAction_t3188246076 *)__this)->get_style_21();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Box_m3007052244(NULL /*static, unused*/, L_5, L_6, L_9, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIButton::.ctor()
extern "C"  void GUIButton__ctor_m2558482427 (GUIButton_t3100740507 * __this, const MethodInfo* method)
{
	{
		GUIContentAction__ctor_m2980405162(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIButton::Reset()
extern Il2CppCodeGenString* _stringLiteral2001146706;
extern const uint32_t GUIButton_Reset_m204915368_MetadataUsageId;
extern "C"  void GUIButton_Reset_m204915368 (GUIButton_t3100740507 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIButton_Reset_m204915368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContentAction_Reset_m626838103(__this, /*hidden argument*/NULL);
		__this->set_sendEvent_23((FsmEvent_t2133468028 *)NULL);
		__this->set_storeButtonState_24((FsmBool_t1075959796 *)NULL);
		FsmString_t952858651 * L_0 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2001146706, /*hidden argument*/NULL);
		((GUIContentAction_t3188246076 *)__this)->set_style_21(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIButton::OnGUI()
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t GUIButton_OnGUI_m2053881077_MetadataUsageId;
extern "C"  void GUIButton_OnGUI_m2053881077 (GUIButton_t3100740507 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIButton_OnGUI_m2053881077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		GUIContentAction_OnGUI_m2475803812(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		Rect_t4241904616  L_0 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		GUIContent_t2094828418 * L_1 = ((GUIContentAction_t3188246076 *)__this)->get_content_22();
		FsmString_t952858651 * L_2 = ((GUIContentAction_t3188246076 *)__this)->get_style_21();
		NullCheck(L_2);
		String_t* L_3 = FsmString_get_Value_m872383149(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_4 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_5 = GUI_Button_m3806860863(NULL /*static, unused*/, L_0, L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		Fsm_t1527112426 * L_6 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_7 = __this->get_sendEvent_23();
		NullCheck(L_6);
		Fsm_Event_m625948263(L_6, L_7, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0041:
	{
		FsmBool_t1075959796 * L_8 = __this->get_storeButtonState_24();
		if (!L_8)
		{
			goto IL_0058;
		}
	}
	{
		FsmBool_t1075959796 * L_9 = __this->get_storeButtonState_24();
		bool L_10 = V_0;
		NullCheck(L_9);
		FsmBool_set_Value_m1126216340(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::.ctor()
extern "C"  void GUIContentAction__ctor_m2980405162 (GUIContentAction_t3188246076 * __this, const MethodInfo* method)
{
	{
		GUIAction__ctor_m141141367(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUIContentAction_Reset_m626838103_MetadataUsageId;
extern "C"  void GUIContentAction_Reset_m626838103 (GUIContentAction_t3188246076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIContentAction_Reset_m626838103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIAction_Reset_m2082541604(__this, /*hidden argument*/NULL);
		__this->set_image_18((FsmTexture_t3073272573 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_1 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_text_19(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_tooltip_20(L_3);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_5 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_style_21(L_5);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIContentAction::OnGUI()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern const uint32_t GUIContentAction_OnGUI_m2475803812_MetadataUsageId;
extern "C"  void GUIContentAction_OnGUI_m2475803812 (GUIContentAction_t3188246076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIContentAction_OnGUI_m2475803812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIAction_OnGUI_m3931507313(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = __this->get_text_19();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		FsmTexture_t3073272573 * L_2 = __this->get_image_18();
		NullCheck(L_2);
		Texture_t2526458961 * L_3 = FsmTexture_get_Value_m3156202285(L_2, /*hidden argument*/NULL);
		FsmString_t952858651 * L_4 = __this->get_tooltip_20();
		NullCheck(L_4);
		String_t* L_5 = FsmString_get_Value_m872383149(L_4, /*hidden argument*/NULL);
		GUIContent_t2094828418 * L_6 = (GUIContent_t2094828418 *)il2cpp_codegen_object_new(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent__ctor_m905567255(L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		__this->set_content_22(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::.ctor()
extern "C"  void GUIElementHitTest__ctor_m884959972 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::Reset()
extern Il2CppClass* FsmVector3_t533912882_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmFloat_t2134102846_il2cpp_TypeInfo_var;
extern const uint32_t GUIElementHitTest_Reset_m2826360209_MetadataUsageId;
extern "C"  void GUIElementHitTest_Reset_m2826360209 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIElementHitTest_Reset_m2826360209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FsmVector3_t533912882 * V_0 = NULL;
	FsmFloat_t2134102846 * V_1 = NULL;
	{
		__this->set_gameObject_11((FsmOwnerDefault_t251897112 *)NULL);
		__this->set_camera_12((Camera_t2727095145 *)NULL);
		FsmVector3_t533912882 * L_0 = (FsmVector3_t533912882 *)il2cpp_codegen_object_new(FsmVector3_t533912882_il2cpp_TypeInfo_var);
		FsmVector3__ctor_m1215698529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		FsmVector3_t533912882 * L_1 = V_0;
		NullCheck(L_1);
		NamedVariable_set_UseVariable_m4266138971(L_1, (bool)1, /*hidden argument*/NULL);
		FsmVector3_t533912882 * L_2 = V_0;
		__this->set_screenPoint_13(L_2);
		FsmFloat_t2134102846 * L_3 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		FsmFloat_t2134102846 * L_4 = V_1;
		NullCheck(L_4);
		NamedVariable_set_UseVariable_m4266138971(L_4, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = V_1;
		__this->set_screenX_14(L_5);
		FsmFloat_t2134102846 * L_6 = (FsmFloat_t2134102846 *)il2cpp_codegen_object_new(FsmFloat_t2134102846_il2cpp_TypeInfo_var);
		FsmFloat__ctor_m3007896661(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		FsmFloat_t2134102846 * L_7 = V_1;
		NullCheck(L_7);
		NamedVariable_set_UseVariable_m4266138971(L_7, (bool)1, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_8 = V_1;
		__this->set_screenY_15(L_8);
		FsmBool_t1075959796 * L_9 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_16(L_9);
		__this->set_hitEvent_17((FsmEvent_t2133468028 *)NULL);
		FsmBool_t1075959796 * L_10 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_everyFrame_19(L_10);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnEnter()
extern "C"  void GUIElementHitTest_OnEnter_m3729704379 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method)
{
	{
		GUIElementHitTest_DoHitTest_m2885358780(__this, /*hidden argument*/NULL);
		FsmBool_t1075959796 * L_0 = __this->get_everyFrame_19();
		NullCheck(L_0);
		bool L_1 = FsmBool_get_Value_m3101329097(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnUpdate()
extern "C"  void GUIElementHitTest_OnUpdate_m3085245416 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method)
{
	{
		GUIElementHitTest_DoHitTest_m2885358780(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::DoHitTest()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGUITexture_t4020448292_m3829342502_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGUIText_t3371372606_m2550728272_MethodInfo_var;
extern const uint32_t GUIElementHitTest_DoHitTest_m2885358780_MetadataUsageId;
extern "C"  void GUIElementHitTest_DoHitTest_m2885358780 (GUIElementHitTest_t4187190930 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIElementHitTest_DoHitTest_m2885358780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GUITexture_t4020448292 * G_B5_0 = NULL;
	GUIElementHitTest_t4187190930 * G_B5_1 = NULL;
	GUITexture_t4020448292 * G_B4_0 = NULL;
	GUIElementHitTest_t4187190930 * G_B4_1 = NULL;
	Vector3_t4282066566  G_B11_0;
	memset(&G_B11_0, 0, sizeof(G_B11_0));
	{
		Fsm_t1527112426 * L_0 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmOwnerDefault_t251897112 * L_1 = __this->get_gameObject_11();
		NullCheck(L_0);
		GameObject_t3674682005 * L_2 = Fsm_GetOwnerDefaultTarget_m846013999(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t3674682005 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		GameObject_t3674682005 * L_5 = V_0;
		GameObject_t3674682005 * L_6 = __this->get_gameObjectCached_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		GameObject_t3674682005 * L_8 = V_0;
		NullCheck(L_8);
		GUITexture_t4020448292 * L_9 = GameObject_GetComponent_TisGUITexture_t4020448292_m3829342502(L_8, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t4020448292_m3829342502_MethodInfo_var);
		GUITexture_t4020448292 * L_10 = L_9;
		G_B4_0 = L_10;
		G_B4_1 = __this;
		if (L_10)
		{
			G_B5_0 = L_10;
			G_B5_1 = __this;
			goto IL_0044;
		}
	}
	{
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		GUIText_t3371372606 * L_12 = GameObject_GetComponent_TisGUIText_t3371372606_m2550728272(L_11, /*hidden argument*/GameObject_GetComponent_TisGUIText_t3371372606_m2550728272_MethodInfo_var);
		G_B5_0 = ((GUITexture_t4020448292 *)(L_12));
		G_B5_1 = G_B4_1;
	}

IL_0044:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_guiElement_20(G_B5_0);
		GameObject_t3674682005 * L_13 = V_0;
		__this->set_gameObjectCached_21(L_13);
	}

IL_0050:
	{
		GUIElement_t3775428101 * L_14 = __this->get_guiElement_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_14, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		FsmStateAction_Finish_m1833602861(__this, /*hidden argument*/NULL);
		return;
	}

IL_0068:
	{
		FsmVector3_t533912882 * L_16 = __this->get_screenPoint_13();
		NullCheck(L_16);
		bool L_17 = NamedVariable_get_IsNone_m281035543(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008c;
		}
	}
	{
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m1846874791(&L_18, (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B11_0 = L_18;
		goto IL_0097;
	}

IL_008c:
	{
		FsmVector3_t533912882 * L_19 = __this->get_screenPoint_13();
		NullCheck(L_19);
		Vector3_t4282066566  L_20 = FsmVector3_get_Value_m2779135117(L_19, /*hidden argument*/NULL);
		G_B11_0 = L_20;
	}

IL_0097:
	{
		V_1 = G_B11_0;
		FsmFloat_t2134102846 * L_21 = __this->get_screenX_14();
		NullCheck(L_21);
		bool L_22 = NamedVariable_get_IsNone_m281035543(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00ba;
		}
	}
	{
		FsmFloat_t2134102846 * L_23 = __this->get_screenX_14();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		(&V_1)->set_x_1(L_24);
	}

IL_00ba:
	{
		FsmFloat_t2134102846 * L_25 = __this->get_screenY_15();
		NullCheck(L_25);
		bool L_26 = NamedVariable_get_IsNone_m281035543(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00dc;
		}
	}
	{
		FsmFloat_t2134102846 * L_27 = __this->get_screenY_15();
		NullCheck(L_27);
		float L_28 = FsmFloat_get_Value_m4137923823(L_27, /*hidden argument*/NULL);
		(&V_1)->set_y_2(L_28);
	}

IL_00dc:
	{
		FsmBool_t1075959796 * L_29 = __this->get_normalized_16();
		NullCheck(L_29);
		bool L_30 = FsmBool_get_Value_m3101329097(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0114;
		}
	}
	{
		Vector3_t4282066566 * L_31 = (&V_1);
		float L_32 = L_31->get_x_1();
		int32_t L_33 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_31->set_x_1(((float)((float)L_32*(float)(((float)((float)L_33))))));
		Vector3_t4282066566 * L_34 = (&V_1);
		float L_35 = L_34->get_y_2();
		int32_t L_36 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_34->set_y_2(((float)((float)L_35*(float)(((float)((float)L_36))))));
	}

IL_0114:
	{
		GUIElement_t3775428101 * L_37 = __this->get_guiElement_20();
		Vector3_t4282066566  L_38 = V_1;
		Camera_t2727095145 * L_39 = __this->get_camera_12();
		NullCheck(L_37);
		bool L_40 = GUIElement_HitTest_m2790588794(L_37, L_38, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_014d;
		}
	}
	{
		FsmBool_t1075959796 * L_41 = __this->get_storeResult_18();
		NullCheck(L_41);
		FsmBool_set_Value_m1126216340(L_41, (bool)1, /*hidden argument*/NULL);
		Fsm_t1527112426 * L_42 = FsmStateAction_get_Fsm_m4090501600(__this, /*hidden argument*/NULL);
		FsmEvent_t2133468028 * L_43 = __this->get_hitEvent_17();
		NullCheck(L_42);
		Fsm_Event_m625948263(L_42, L_43, /*hidden argument*/NULL);
		goto IL_0159;
	}

IL_014d:
	{
		FsmBool_t1075959796 * L_44 = __this->get_storeResult_18();
		NullCheck(L_44);
		FsmBool_set_Value_m1126216340(L_44, (bool)0, /*hidden argument*/NULL);
	}

IL_0159:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::.ctor()
extern "C"  void GUIHorizontalSlider__ctor_m3244547560 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method)
{
	{
		GUIAction__ctor_m141141367(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::Reset()
extern Il2CppCodeGenString* _stringLiteral3659356677;
extern Il2CppCodeGenString* _stringLiteral2470892273;
extern const uint32_t GUIHorizontalSlider_Reset_m890980501_MetadataUsageId;
extern "C"  void GUIHorizontalSlider_Reset_m890980501 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIHorizontalSlider_Reset_m890980501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIAction_Reset_m2082541604(__this, /*hidden argument*/NULL);
		__this->set_floatVariable_18((FsmFloat_t2134102846 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_leftValue_19(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
		__this->set_rightValue_20(L_1);
		FsmString_t952858651 * L_2 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral3659356677, /*hidden argument*/NULL);
		__this->set_sliderStyle_21(L_2);
		FsmString_t952858651 * L_3 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, _stringLiteral2470892273, /*hidden argument*/NULL);
		__this->set_thumbStyle_22(L_3);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUIHorizontalSlider::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3659356677;
extern Il2CppCodeGenString* _stringLiteral2470892273;
extern const uint32_t GUIHorizontalSlider_OnGUI_m2739946210_MetadataUsageId;
extern "C"  void GUIHorizontalSlider_OnGUI_m2739946210 (GUIHorizontalSlider_t2385736974 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUIHorizontalSlider_OnGUI_m2739946210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B3_2 = 0.0f;
	Rect_t4241904616  G_B3_3;
	memset(&G_B3_3, 0, sizeof(G_B3_3));
	FsmFloat_t2134102846 * G_B3_4 = NULL;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	float G_B2_2 = 0.0f;
	Rect_t4241904616  G_B2_3;
	memset(&G_B2_3, 0, sizeof(G_B2_3));
	FsmFloat_t2134102846 * G_B2_4 = NULL;
	String_t* G_B4_0 = NULL;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	float G_B4_3 = 0.0f;
	Rect_t4241904616  G_B4_4;
	memset(&G_B4_4, 0, sizeof(G_B4_4));
	FsmFloat_t2134102846 * G_B4_5 = NULL;
	GUIStyle_t2990928826 * G_B6_0 = NULL;
	float G_B6_1 = 0.0f;
	float G_B6_2 = 0.0f;
	float G_B6_3 = 0.0f;
	Rect_t4241904616  G_B6_4;
	memset(&G_B6_4, 0, sizeof(G_B6_4));
	FsmFloat_t2134102846 * G_B6_5 = NULL;
	GUIStyle_t2990928826 * G_B5_0 = NULL;
	float G_B5_1 = 0.0f;
	float G_B5_2 = 0.0f;
	float G_B5_3 = 0.0f;
	Rect_t4241904616  G_B5_4;
	memset(&G_B5_4, 0, sizeof(G_B5_4));
	FsmFloat_t2134102846 * G_B5_5 = NULL;
	String_t* G_B7_0 = NULL;
	GUIStyle_t2990928826 * G_B7_1 = NULL;
	float G_B7_2 = 0.0f;
	float G_B7_3 = 0.0f;
	float G_B7_4 = 0.0f;
	Rect_t4241904616  G_B7_5;
	memset(&G_B7_5, 0, sizeof(G_B7_5));
	FsmFloat_t2134102846 * G_B7_6 = NULL;
	{
		GUIAction_OnGUI_m3931507313(__this, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_0 = __this->get_floatVariable_18();
		if (!L_0)
		{
			goto IL_00b0;
		}
	}
	{
		FsmFloat_t2134102846 * L_1 = __this->get_floatVariable_18();
		Rect_t4241904616  L_2 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		FsmFloat_t2134102846 * L_3 = __this->get_floatVariable_18();
		NullCheck(L_3);
		float L_4 = FsmFloat_get_Value_m4137923823(L_3, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_5 = __this->get_leftValue_19();
		NullCheck(L_5);
		float L_6 = FsmFloat_get_Value_m4137923823(L_5, /*hidden argument*/NULL);
		FsmFloat_t2134102846 * L_7 = __this->get_rightValue_20();
		NullCheck(L_7);
		float L_8 = FsmFloat_get_Value_m4137923823(L_7, /*hidden argument*/NULL);
		FsmString_t952858651 * L_9 = __this->get_sliderStyle_21();
		NullCheck(L_9);
		String_t* L_10 = FsmString_get_Value_m872383149(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_12 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		G_B2_0 = L_8;
		G_B2_1 = L_6;
		G_B2_2 = L_4;
		G_B2_3 = L_2;
		G_B2_4 = L_1;
		if (!L_12)
		{
			G_B3_0 = L_8;
			G_B3_1 = L_6;
			G_B3_2 = L_4;
			G_B3_3 = L_2;
			G_B3_4 = L_1;
			goto IL_0068;
		}
	}
	{
		FsmString_t952858651 * L_13 = __this->get_sliderStyle_21();
		NullCheck(L_13);
		String_t* L_14 = FsmString_get_Value_m872383149(L_13, /*hidden argument*/NULL);
		G_B4_0 = L_14;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		G_B4_5 = G_B2_4;
		goto IL_006d;
	}

IL_0068:
	{
		G_B4_0 = _stringLiteral3659356677;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
		G_B4_5 = G_B3_4;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_15 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, G_B4_0, /*hidden argument*/NULL);
		FsmString_t952858651 * L_16 = __this->get_thumbStyle_22();
		NullCheck(L_16);
		String_t* L_17 = FsmString_get_Value_m872383149(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_19 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		G_B5_0 = L_15;
		G_B5_1 = G_B4_1;
		G_B5_2 = G_B4_2;
		G_B5_3 = G_B4_3;
		G_B5_4 = G_B4_4;
		G_B5_5 = G_B4_5;
		if (!L_19)
		{
			G_B6_0 = L_15;
			G_B6_1 = G_B4_1;
			G_B6_2 = G_B4_2;
			G_B6_3 = G_B4_3;
			G_B6_4 = G_B4_4;
			G_B6_5 = G_B4_5;
			goto IL_009c;
		}
	}
	{
		FsmString_t952858651 * L_20 = __this->get_thumbStyle_22();
		NullCheck(L_20);
		String_t* L_21 = FsmString_get_Value_m872383149(L_20, /*hidden argument*/NULL);
		G_B7_0 = L_21;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		G_B7_4 = G_B5_3;
		G_B7_5 = G_B5_4;
		G_B7_6 = G_B5_5;
		goto IL_00a1;
	}

IL_009c:
	{
		G_B7_0 = _stringLiteral2470892273;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		G_B7_4 = G_B6_3;
		G_B7_5 = G_B6_4;
		G_B7_6 = G_B6_5;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_22 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, G_B7_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		float L_23 = GUI_HorizontalSlider_m899690739(NULL /*static, unused*/, G_B7_5, G_B7_4, G_B7_3, G_B7_2, G_B7_1, L_22, /*hidden argument*/NULL);
		NullCheck(G_B7_6);
		FsmFloat_set_Value_m1568963140(G_B7_6, L_23, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILabel::.ctor()
extern "C"  void GUILabel__ctor_m3750176869 (GUILabel_t3454715681 * __this, const MethodInfo* method)
{
	{
		GUIContentAction__ctor_m2980405162(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILabel::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILabel_OnGUI_m3245575519_MetadataUsageId;
extern "C"  void GUILabel_OnGUI_m3245575519 (GUILabel_t3454715681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILabel_OnGUI_m3245575519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContentAction_OnGUI_m2475803812(__this, /*hidden argument*/NULL);
		FsmString_t952858651 * L_0 = ((GUIContentAction_t3188246076 *)__this)->get_style_21();
		NullCheck(L_0);
		String_t* L_1 = FsmString_get_Value_m872383149(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		Rect_t4241904616  L_3 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		GUIContent_t2094828418 * L_4 = ((GUIContentAction_t3188246076 *)__this)->get_content_22();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m2835696956(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		goto IL_0052;
	}

IL_0031:
	{
		Rect_t4241904616  L_5 = ((GUIAction_t3055477407 *)__this)->get_rect_17();
		GUIContent_t2094828418 * L_6 = ((GUIContentAction_t3188246076 *)__this)->get_content_22();
		FsmString_t952858651 * L_7 = ((GUIContentAction_t3188246076 *)__this)->get_style_21();
		NullCheck(L_7);
		String_t* L_8 = FsmString_get_Value_m872383149(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_9 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m2293702269(NULL /*static, unused*/, L_5, L_6, L_9, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::.ctor()
extern "C"  void GUILayoutAction__ctor_m2599973229 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::get_LayoutOptions()
extern Il2CppClass* GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutAction_get_LayoutOptions_m1855901188_MetadataUsageId;
extern "C"  GUILayoutOptionU5BU5D_t2977405297* GUILayoutAction_get_LayoutOptions_m1855901188 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutAction_get_LayoutOptions_m1855901188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUILayoutOptionU5BU5D_t2977405297* L_0 = __this->get_options_12();
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		LayoutOptionU5BU5D_t3507996764* L_1 = __this->get_layoutOptions_11();
		NullCheck(L_1);
		__this->set_options_12(((GUILayoutOptionU5BU5D_t2977405297*)SZArrayNew(GUILayoutOptionU5BU5D_t2977405297_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))));
		V_0 = 0;
		goto IL_003e;
	}

IL_0025:
	{
		GUILayoutOptionU5BU5D_t2977405297* L_2 = __this->get_options_12();
		int32_t L_3 = V_0;
		LayoutOptionU5BU5D_t3507996764* L_4 = __this->get_layoutOptions_11();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		LayoutOption_t964995201 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		GUILayoutOption_t331591504 * L_8 = LayoutOption_GetGUILayoutOption_m1468661926(L_7, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_8);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (GUILayoutOption_t331591504 *)L_8);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_10 = V_0;
		LayoutOptionU5BU5D_t3507996764* L_11 = __this->get_layoutOptions_11();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0025;
		}
	}

IL_004c:
	{
		GUILayoutOptionU5BU5D_t2977405297* L_12 = __this->get_options_12();
		return L_12;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::Reset()
extern Il2CppClass* LayoutOptionU5BU5D_t3507996764_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutAction_Reset_m246406170_MetadataUsageId;
extern "C"  void GUILayoutAction_Reset_m246406170 (GUILayoutAction_t2615417833 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutAction_Reset_m246406170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_layoutOptions_11(((LayoutOptionU5BU5D_t3507996764*)SZArrayNew(LayoutOptionU5BU5D_t3507996764_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::.ctor()
extern "C"  void GUILayoutBeginArea__ctor_m3361698317 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	{
		FsmStateAction__ctor_m2146048618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginArea_Reset_m1008131258_MetadataUsageId;
extern "C"  void GUILayoutBeginArea_Reset_m1008131258 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginArea_Reset_m1008131258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_screenRect_11((FsmRect_t1076426478 *)NULL);
		FsmFloat_t2134102846 * L_0 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_left_12(L_0);
		FsmFloat_t2134102846 * L_1 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_top_13(L_1);
		FsmFloat_t2134102846 * L_2 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_width_14(L_2);
		FsmFloat_t2134102846 * L_3 = FsmFloat_op_Implicit_m3142426606(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		__this->set_height_15(L_3);
		FsmBool_t1075959796 * L_4 = FsmBool_op_Implicit_m2730611352(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		__this->set_normalized_16(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		FsmString_t952858651 * L_6 = FsmString_op_Implicit_m224809487(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_style_17(L_6);
		return;
	}
}
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginArea::OnGUI()
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t2990928826_il2cpp_TypeInfo_var;
extern const uint32_t GUILayoutBeginArea_OnGUI_m2857096967_MetadataUsageId;
extern "C"  void GUILayoutBeginArea_OnGUI_m2857096967 (GUILayoutBeginArea_t3761865337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GUILayoutBeginArea_OnGUI_m2857096967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GUILayoutBeginArea_t3761865337 * G_B2_0 = NULL;
	GUILayoutBeginArea_t3761865337 * G_B1_0 = NULL;
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	GUILayoutBeginArea_t3761865337 * G_B3_1 = NULL;
	{
		FsmRect_t1076426478 * L_0 = __this->get_screenRect_11();
		NullCheck(L_0);
		bool L_1 = NamedVariable_get_IsNone_m281035543(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0021;
		}
	}
	{
		FsmRect_t1076426478 * L_2 = __this->get_screenRect_11();
		NullCheck(L_2);
		Rect_t4241904616  L_3 = FsmRect_get_Value_m1002500317(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0021:
	{
		Initobj (Rect_t4241904616_il2cpp_TypeInfo_var, (&V_0));
		Rect_t4241904616  L_4 = V_0;
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_rect_18(G_B3_0);
		FsmFloat_t2134102846 * L_5 = __this->get_left_12();
		NullCheck(L_5);
		bool L_6 = NamedVariable_get_IsNone_m281035543(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		Rect_t4241904616 * L_7 = __this->get_address_of_rect_18();
		FsmFloat_t2134102846 * L_8 = __this->get_left_12();
		NullCheck(L_8);
		float L_9 = FsmFloat_get_Value_m4137923823(L_8, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0055:
	{
		FsmFloat_t2134102846 * L_10 = __this->get_top_13();
		NullCheck(L_10);
		bool L_11 = NamedVariable_get_IsNone_m281035543(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007b;
		}
	}
	{
		Rect_t4241904616 * L_12 = __this->get_address_of_rect_18();
		FsmFloat_t2134102846 * L_13 = __this->get_top_13();
		NullCheck(L_13);
		float L_14 = FsmFloat_get_Value_m4137923823(L_13, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_12, L_14, /*hidden argument*/NULL);
	}

IL_007b:
	{
		FsmFloat_t2134102846 * L_15 = __this->get_width_14();
		NullCheck(L_15);
		bool L_16 = NamedVariable_get_IsNone_m281035543(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a1;
		}
	}
	{
		Rect_t4241904616 * L_17 = __this->get_address_of_rect_18();
		FsmFloat_t2134102846 * L_18 = __this->get_width_14();
		NullCheck(L_18);
		float L_19 = FsmFloat_get_Value_m4137923823(L_18, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		FsmFloat_t2134102846 * L_20 = __this->get_height_15();
		NullCheck(L_20);
		bool L_21 = NamedVariable_get_IsNone_m281035543(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00c7;
		}
	}
	{
		Rect_t4241904616 * L_22 = __this->get_address_of_rect_18();
		FsmFloat_t2134102846 * L_23 = __this->get_height_15();
		NullCheck(L_23);
		float L_24 = FsmFloat_get_Value_m4137923823(L_23, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_22, L_24, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		FsmBool_t1075959796 * L_25 = __this->get_normalized_16();
		NullCheck(L_25);
		bool L_26 = FsmBool_get_Value_m3101329097(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0137;
		}
	}
	{
		Rect_t4241904616 * L_27 = __this->get_address_of_rect_18();
		Rect_t4241904616 * L_28 = L_27;
		float L_29 = Rect_get_x_m982385354(L_28, /*hidden argument*/NULL);
		int32_t L_30 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_28, ((float)((float)L_29*(float)(((float)((float)L_30))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_31 = __this->get_address_of_rect_18();
		Rect_t4241904616 * L_32 = L_31;
		float L_33 = Rect_get_width_m2824209432(L_32, /*hidden argument*/NULL);
		int32_t L_34 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_width_m3771513595(L_32, ((float)((float)L_33*(float)(((float)((float)L_34))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_35 = __this->get_address_of_rect_18();
		Rect_t4241904616 * L_36 = L_35;
		float L_37 = Rect_get_y_m982386315(L_36, /*hidden argument*/NULL);
		int32_t L_38 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_36, ((float)((float)L_37*(float)(((float)((float)L_38))))), /*hidden argument*/NULL);
		Rect_t4241904616 * L_39 = __this->get_address_of_rect_18();
		Rect_t4241904616 * L_40 = L_39;
		float L_41 = Rect_get_height_m2154960823(L_40, /*hidden argument*/NULL);
		int32_t L_42 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_height_m3398820332(L_40, ((float)((float)L_41*(float)(((float)((float)L_42))))), /*hidden argument*/NULL);
	}

IL_0137:
	{
		Rect_t4241904616  L_43 = __this->get_rect_18();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_44 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		FsmString_t952858651 * L_45 = __this->get_style_17();
		NullCheck(L_45);
		String_t* L_46 = FsmString_get_Value_m872383149(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2990928826_il2cpp_TypeInfo_var);
		GUIStyle_t2990928826 * L_47 = GUIStyle_op_Implicit_m169215436(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		GUILayout_BeginArea_m2566106901(NULL /*static, unused*/, L_43, L_44, L_47, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
