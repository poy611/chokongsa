﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector2Value
struct SetVector2Value_t2485747186;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::.ctor()
extern "C"  void SetVector2Value__ctor_m3560327556 (SetVector2Value_t2485747186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::Reset()
extern "C"  void SetVector2Value_Reset_m1206760497 (SetVector2Value_t2485747186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::OnEnter()
extern "C"  void SetVector2Value_OnEnter_m2072542299 (SetVector2Value_t2485747186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::OnUpdate()
extern "C"  void SetVector2Value_OnUpdate_m3252828488 (SetVector2Value_t2485747186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
