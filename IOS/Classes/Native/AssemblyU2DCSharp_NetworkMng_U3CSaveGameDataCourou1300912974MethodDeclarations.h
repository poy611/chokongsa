﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/<SaveGameDataCouroutine>c__Iterator13
struct U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NetworkMng/<SaveGameDataCouroutine>c__Iterator13::.ctor()
extern "C"  void U3CSaveGameDataCouroutineU3Ec__Iterator13__ctor_m2313372429 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<SaveGameDataCouroutine>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveGameDataCouroutineU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1732221679 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NetworkMng/<SaveGameDataCouroutine>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveGameDataCouroutineU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3817561219 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NetworkMng/<SaveGameDataCouroutine>c__Iterator13::MoveNext()
extern "C"  bool U3CSaveGameDataCouroutineU3Ec__Iterator13_MoveNext_m2898674607 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<SaveGameDataCouroutine>c__Iterator13::Dispose()
extern "C"  void U3CSaveGameDataCouroutineU3Ec__Iterator13_Dispose_m2551095306 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/<SaveGameDataCouroutine>c__Iterator13::Reset()
extern "C"  void U3CSaveGameDataCouroutineU3Ec__Iterator13_Reset_m4254772666 (U3CSaveGameDataCouroutineU3Ec__Iterator13_t1300912974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
