﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmGameObject
struct  FsmGameObject_t1697147867  : public NamedVariable_t3211770239
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmGameObject::value
	GameObject_t3674682005 * ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmGameObject_t1697147867, ___value_5)); }
	inline GameObject_t3674682005 * get_value_5() const { return ___value_5; }
	inline GameObject_t3674682005 ** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(GameObject_t3674682005 * value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier(&___value_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
