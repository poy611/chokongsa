﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t964995201;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t331591504;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201.h"

// System.Void HutongGames.PlayMaker.LayoutOption::.ctor()
extern "C"  void LayoutOption__ctor_m2045150578 (LayoutOption_t964995201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.LayoutOption::.ctor(HutongGames.PlayMaker.LayoutOption)
extern "C"  void LayoutOption__ctor_m3964081607 (LayoutOption_t964995201 * __this, LayoutOption_t964995201 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.LayoutOption::ResetParameters()
extern "C"  void LayoutOption_ResetParameters_m1585145385 (LayoutOption_t964995201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption HutongGames.PlayMaker.LayoutOption::GetGUILayoutOption()
extern "C"  GUILayoutOption_t331591504 * LayoutOption_GetGUILayoutOption_m1468661926 (LayoutOption_t964995201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
