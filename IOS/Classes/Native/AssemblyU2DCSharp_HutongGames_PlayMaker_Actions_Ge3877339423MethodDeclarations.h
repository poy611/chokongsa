﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTimeInfo
struct GetTimeInfo_t3877339423;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::.ctor()
extern "C"  void GetTimeInfo__ctor_m1162951927 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::Reset()
extern "C"  void GetTimeInfo_Reset_m3104352164 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnEnter()
extern "C"  void GetTimeInfo_OnEnter_m297033486 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnUpdate()
extern "C"  void GetTimeInfo_OnUpdate_m4046630133 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::DoGetTimeInfo()
extern "C"  void GetTimeInfo_DoGetTimeInfo_m1705919899 (GetTimeInfo_t3877339423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
