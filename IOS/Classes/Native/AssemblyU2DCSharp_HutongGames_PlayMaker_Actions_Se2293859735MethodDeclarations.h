﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmGameObject
struct SetFsmGameObject_t2293859735;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::.ctor()
extern "C"  void SetFsmGameObject__ctor_m2390144687 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::Reset()
extern "C"  void SetFsmGameObject_Reset_m36577628 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnEnter()
extern "C"  void SetFsmGameObject_OnEnter_m2808236742 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::DoSetFsmGameObject()
extern "C"  void SetFsmGameObject_DoSetFsmGameObject_m2128809935 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnUpdate()
extern "C"  void SetFsmGameObject_OnUpdate_m289552445 (SetFsmGameObject_t2293859735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
