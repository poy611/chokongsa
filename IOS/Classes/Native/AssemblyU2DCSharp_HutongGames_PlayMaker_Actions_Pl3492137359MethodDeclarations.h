﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll
struct PlayerPrefsDeleteAll_t3492137359;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::.ctor()
extern "C"  void PlayerPrefsDeleteAll__ctor_m3297484215 (PlayerPrefsDeleteAll_t3492137359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::Reset()
extern "C"  void PlayerPrefsDeleteAll_Reset_m943917156 (PlayerPrefsDeleteAll_t3492137359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::OnEnter()
extern "C"  void PlayerPrefsDeleteAll_OnEnter_m2883162062 (PlayerPrefsDeleteAll_t3492137359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
