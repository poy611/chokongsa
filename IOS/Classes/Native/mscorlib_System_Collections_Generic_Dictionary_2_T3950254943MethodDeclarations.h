﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct Transform_1_t3950254943;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1430393155_gshared (Transform_1_t3950254943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m1430393155(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3950254943 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1430393155_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Invoke(TKey,TValue)
extern "C"  TypeConvertKey_t866134174  Transform_1_Invoke_m1444894809_gshared (Transform_1_t3950254943 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1444894809(__this, ___key0, ___value1, method) ((  TypeConvertKey_t866134174  (*) (Transform_1_t3950254943 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m1444894809_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1505448888_gshared (Transform_1_t3950254943 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1505448888(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3950254943 *, TypeConvertKey_t866134174 , Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1505448888_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object,Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::EndInvoke(System.IAsyncResult)
extern "C"  TypeConvertKey_t866134174  Transform_1_EndInvoke_m3100034705_gshared (Transform_1_t3950254943 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3100034705(__this, ___result0, method) ((  TypeConvertKey_t866134174  (*) (Transform_1_t3950254943 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3100034705_gshared)(__this, ___result0, method)
