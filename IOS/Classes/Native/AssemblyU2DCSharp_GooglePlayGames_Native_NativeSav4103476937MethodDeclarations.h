﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4210182474.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m501707629_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m501707629(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m501707629_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::<>m__6E(T1,T2)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 * __this, int32_t ___val10, Il2CppObject * ___val21, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376(__this, ___val10, ___val21, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey85_2_t4103476937 *, int32_t, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m2890881376_gshared)(__this, ___val10, ___val21, method)
