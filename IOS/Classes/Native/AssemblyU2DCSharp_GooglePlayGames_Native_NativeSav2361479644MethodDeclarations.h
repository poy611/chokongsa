﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m1028318924_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m1028318924(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey85_2__ctor_m1028318924_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2<System.Object,System.Object>::<>m__6E(T1,T2)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393_gshared (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 * __this, Il2CppObject * ___val10, Il2CppObject * ___val21, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393(__this, ___val10, ___val21, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey85_2_t2361479644 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey85_2_U3CU3Em__6E_m3141807393_gshared)(__this, ___val10, ___val21, method)
