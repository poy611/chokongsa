﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonValue
struct BsonValue_t457156831;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonType2455132538.h"

// System.Void Newtonsoft.Json.Bson.BsonValue::.ctor(System.Object,Newtonsoft.Json.Bson.BsonType)
extern "C"  void BsonValue__ctor_m1245695775 (BsonValue_t457156831 * __this, Il2CppObject * ___value0, int8_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Bson.BsonValue::get_Value()
extern "C"  Il2CppObject * BsonValue_get_Value_m2932680735 (BsonValue_t457156831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::get_Type()
extern "C"  int8_t BsonValue_get_Type_m1837213369 (BsonValue_t457156831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
