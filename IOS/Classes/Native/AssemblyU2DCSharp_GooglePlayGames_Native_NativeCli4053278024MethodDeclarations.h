﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50
struct U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50::.ctor()
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey50__ctor_m4199569187 (U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50::<>m__38()
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey50_U3CU3Em__38_m824837713 (U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
