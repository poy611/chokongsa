﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct GameObjectTagSwitch_t3433592875;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::.ctor()
extern "C"  void GameObjectTagSwitch__ctor_m2134117227 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::Reset()
extern "C"  void GameObjectTagSwitch_Reset_m4075517464 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnEnter()
extern "C"  void GameObjectTagSwitch_OnEnter_m1578983554 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnUpdate()
extern "C"  void GameObjectTagSwitch_OnUpdate_m837409281 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::DoTagSwitch()
extern "C"  void GameObjectTagSwitch_DoTagSwitch_m398210284 (GameObjectTagSwitch_t3433592875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
