﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SceneindexInc
struct SceneindexInc_t2309712408;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneindexInc
struct  SceneindexInc_t2309712408  : public Il2CppObject
{
public:
	// System.Int32 SceneindexInc::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(SceneindexInc_t2309712408, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

struct SceneindexInc_t2309712408_StaticFields
{
public:
	// SceneindexInc SceneindexInc::n
	SceneindexInc_t2309712408 * ___n_0;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(SceneindexInc_t2309712408_StaticFields, ___n_0)); }
	inline SceneindexInc_t2309712408 * get_n_0() const { return ___n_0; }
	inline SceneindexInc_t2309712408 ** get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(SceneindexInc_t2309712408 * value)
	{
		___n_0 = value;
		Il2CppCodeGenWriteBarrier(&___n_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
