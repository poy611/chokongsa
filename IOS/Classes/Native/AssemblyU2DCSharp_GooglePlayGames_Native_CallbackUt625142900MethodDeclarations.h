﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m2275597405_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m2275597405(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m2275597405_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::<>m__15()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m3018797322_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m3018797322(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t625142900 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m3018797322_gshared)(__this, method)
