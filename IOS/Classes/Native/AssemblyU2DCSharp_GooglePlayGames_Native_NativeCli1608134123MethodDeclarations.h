﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E
struct U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
struct FetchAllResponse_t2805709750;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse
struct FetchSelfResponse_t1087840609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_A2805709750.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P1087840609.h"

// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E::.ctor()
extern "C"  void U3CHandleAuthTransitionU3Ec__AnonStorey4E__ctor_m636690400 (U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E::<>m__24(GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse)
extern "C"  void U3CHandleAuthTransitionU3Ec__AnonStorey4E_U3CU3Em__24_m3151409909 (U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123 * __this, FetchAllResponse_t2805709750 * ___results0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey4E::<>m__25(GooglePlayGames.Native.PInvoke.PlayerManager/FetchSelfResponse)
extern "C"  void U3CHandleAuthTransitionU3Ec__AnonStorey4E_U3CU3Em__25_m764259243 (U3CHandleAuthTransitionU3Ec__AnonStorey4E_t1608134123 * __this, FetchSelfResponse_t1087840609 * ___results0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
