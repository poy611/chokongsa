﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlReader4123196108.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport870027186.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma3744455723.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG4120438118.h"
#include "System_Xml_System_Xml_XmlReaderSettings4229224207.h"
#include "System_Xml_System_Xml_XmlResolver3822670287.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace707263863.h"
#include "System_Xml_System_Xml_XmlSpace557686381.h"
#include "System_Xml_System_Xml_XmlText857080694.h"
#include "System_Xml_Mono_Xml2_XmlTextReader1609187425.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo597808448.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeToke982414386.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2016006645.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState2319200395.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt1440432317.h"
#include "System_Xml_System_Xml_XmlTextReader1367920089.h"
#include "System_Xml_System_Xml_XmlTextWriter1523325321.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo1808742809.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil614327009.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState205203294.h"
#include "System_Xml_System_Xml_XmlTokenizedType3753548874.h"
#include "System_Xml_System_Xml_XmlUrlResolver343097532.h"
#include "System_Xml_System_Xml_XmlWhitespace4130926598.h"
#include "System_Xml_System_Xml_XmlWriter4278601340.h"
#include "System_Xml_System_Xml_XmlWriterSettings233403071.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler3074502249.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1676616639.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1676616730.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3379220534.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar435483166.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3379220348.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3988332413.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1676616792.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar435480436.h"
#include "System_Runtime_Serialization_U3CModuleU3E86524790.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2462274566.h"
#include "System_Runtime_Serialization_System_Runtime_Serial2601848894.h"
#include "System_Runtime_Serialization_System_Runtime_Serial1202205191.h"
#include "Newtonsoft_Json_U3CModuleU3E86524790.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin2475221485.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateFormatHandling4014082626.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateParseHandling2108333400.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateTimeZoneHandli2945560484.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli1569448045.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatFormatHandlin3887485542.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatParseHandling3074080948.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting732683613.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonArrayAttribute2333618243.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConstructorAtt1098405378.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonContainerAttri1917602971.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConvert4088335769.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverterAttri1044341340.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverterCollec530165700.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonDictionaryAttrib55363084.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonException3991042933.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonExtensionDataA3618022163.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonIgnoreAttribut4056167888.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonObjectAttribute863918851.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonContainerType288741633.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPropertyAttrib3435603053.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader_State2338717890.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReaderException354140082.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonRequiredAttribu224246243.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializationE2886273343.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializerSett2589405525.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonTextReader2658709200.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonTextWriter2814114432.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonToken4173078175.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter972330355.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter_State671991922.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriterExceptio1037736450.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerializatio1550301796.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHa2626038881.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandl2077487315.h"
#include "Newtonsoft_Json_Newtonsoft_Json_NullValueHandling2754652381.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHandli56081595.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences4230591217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl2761661122.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Required3921306327.h"
#include "Newtonsoft_Json_Newtonsoft_Json_StringEscapeHandli1042460335.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "Newtonsoft_Json_Newtonsoft_Json_WriteState4055692778.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Base64En2146525835.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Collectio246479025.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeInfo2222045584.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ParseRes4245056558.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2565263886.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XmlReader_t4123196108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[2] = 
{
	XmlReader_t4123196108::get_offset_of_binary_0(),
	XmlReader_t4123196108::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XmlReaderBinarySupport_t870027186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[5] = 
{
	XmlReaderBinarySupport_t870027186::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t870027186::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t870027186::get_offset_of_state_2(),
	XmlReaderBinarySupport_t870027186::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t870027186::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (CommandState_t3744455723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[6] = 
{
	CommandState_t3744455723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (CharGetter_t4120438118), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (XmlReaderSettings_t4229224207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[4] = 
{
	XmlReaderSettings_t4229224207::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t4229224207::get_offset_of_conformance_1(),
	XmlReaderSettings_t4229224207::get_offset_of_schemas_2(),
	XmlReaderSettings_t4229224207::get_offset_of_schemasNeedsInitialization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XmlResolver_t3822670287), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlSignificantWhitespace_t707263863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XmlSpace_t557686381)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1707[4] = 
{
	XmlSpace_t557686381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (XmlText_t857080694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XmlTextReader_t1609187425), -1, sizeof(XmlTextReader_t1609187425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[54] = 
{
	XmlTextReader_t1609187425::get_offset_of_cursorToken_2(),
	XmlTextReader_t1609187425::get_offset_of_currentToken_3(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_t1609187425::get_offset_of_attributeTokens_6(),
	XmlTextReader_t1609187425::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_t1609187425::get_offset_of_currentAttribute_8(),
	XmlTextReader_t1609187425::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_t1609187425::get_offset_of_attributeCount_10(),
	XmlTextReader_t1609187425::get_offset_of_parserContext_11(),
	XmlTextReader_t1609187425::get_offset_of_nameTable_12(),
	XmlTextReader_t1609187425::get_offset_of_nsmgr_13(),
	XmlTextReader_t1609187425::get_offset_of_readState_14(),
	XmlTextReader_t1609187425::get_offset_of_disallowReset_15(),
	XmlTextReader_t1609187425::get_offset_of_depth_16(),
	XmlTextReader_t1609187425::get_offset_of_elementDepth_17(),
	XmlTextReader_t1609187425::get_offset_of_depthUp_18(),
	XmlTextReader_t1609187425::get_offset_of_popScope_19(),
	XmlTextReader_t1609187425::get_offset_of_elementNames_20(),
	XmlTextReader_t1609187425::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_t1609187425::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_t1609187425::get_offset_of_isStandalone_23(),
	XmlTextReader_t1609187425::get_offset_of_returnEntityReference_24(),
	XmlTextReader_t1609187425::get_offset_of_entityReferenceName_25(),
	XmlTextReader_t1609187425::get_offset_of_valueBuffer_26(),
	XmlTextReader_t1609187425::get_offset_of_reader_27(),
	XmlTextReader_t1609187425::get_offset_of_peekChars_28(),
	XmlTextReader_t1609187425::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_t1609187425::get_offset_of_peekCharsLength_30(),
	XmlTextReader_t1609187425::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_t1609187425::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_t1609187425::get_offset_of_line_33(),
	XmlTextReader_t1609187425::get_offset_of_column_34(),
	XmlTextReader_t1609187425::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_t1609187425::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_t1609187425::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_t1609187425::get_offset_of_startNodeType_38(),
	XmlTextReader_t1609187425::get_offset_of_currentState_39(),
	XmlTextReader_t1609187425::get_offset_of_nestLevel_40(),
	XmlTextReader_t1609187425::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_t1609187425::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_t1609187425::get_offset_of_namespaces_43(),
	XmlTextReader_t1609187425::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_t1609187425::get_offset_of_resolver_45(),
	XmlTextReader_t1609187425::get_offset_of_normalization_46(),
	XmlTextReader_t1609187425::get_offset_of_checkCharacters_47(),
	XmlTextReader_t1609187425::get_offset_of_prohibitDtd_48(),
	XmlTextReader_t1609187425::get_offset_of_closeInput_49(),
	XmlTextReader_t1609187425::get_offset_of_entityHandling_50(),
	XmlTextReader_t1609187425::get_offset_of_whitespacePool_51(),
	XmlTextReader_t1609187425::get_offset_of_whitespaceCache_52(),
	XmlTextReader_t1609187425::get_offset_of_stateStack_53(),
	XmlTextReader_t1609187425_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_54(),
	XmlTextReader_t1609187425_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XmlTokenInfo_t597808448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[13] = 
{
	XmlTokenInfo_t597808448::get_offset_of_valueCache_0(),
	XmlTokenInfo_t597808448::get_offset_of_Reader_1(),
	XmlTokenInfo_t597808448::get_offset_of_Name_2(),
	XmlTokenInfo_t597808448::get_offset_of_LocalName_3(),
	XmlTokenInfo_t597808448::get_offset_of_Prefix_4(),
	XmlTokenInfo_t597808448::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t597808448::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t597808448::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t597808448::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t597808448::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t597808448::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t597808448::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t597808448::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XmlAttributeTokenInfo_t982414386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[4] = 
{
	XmlAttributeTokenInfo_t982414386::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t982414386::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (TagName_t2016006645)+ sizeof (Il2CppObject), sizeof(TagName_t2016006645_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[3] = 
{
	TagName_t2016006645::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2016006645::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2016006645::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (DtdInputState_t2319200395)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[10] = 
{
	DtdInputState_t2319200395::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (DtdInputStateStack_t1440432317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[1] = 
{
	DtdInputStateStack_t1440432317::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XmlTextReader_t1367920089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[5] = 
{
	XmlTextReader_t1367920089::get_offset_of_entity_2(),
	XmlTextReader_t1367920089::get_offset_of_source_3(),
	XmlTextReader_t1367920089::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t1367920089::get_offset_of_insideAttribute_5(),
	XmlTextReader_t1367920089::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XmlTextWriter_t1523325321), -1, sizeof(XmlTextWriter_t1523325321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[35] = 
{
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t1523325321::get_offset_of_base_stream_4(),
	XmlTextWriter_t1523325321::get_offset_of_source_5(),
	XmlTextWriter_t1523325321::get_offset_of_writer_6(),
	XmlTextWriter_t1523325321::get_offset_of_preserver_7(),
	XmlTextWriter_t1523325321::get_offset_of_preserved_name_8(),
	XmlTextWriter_t1523325321::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t1523325321::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t1523325321::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t1523325321::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t1523325321::get_offset_of_namespaces_13(),
	XmlTextWriter_t1523325321::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t1523325321::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t1523325321::get_offset_of_newline_handling_16(),
	XmlTextWriter_t1523325321::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t1523325321::get_offset_of_state_18(),
	XmlTextWriter_t1523325321::get_offset_of_node_state_19(),
	XmlTextWriter_t1523325321::get_offset_of_nsmanager_20(),
	XmlTextWriter_t1523325321::get_offset_of_open_count_21(),
	XmlTextWriter_t1523325321::get_offset_of_elements_22(),
	XmlTextWriter_t1523325321::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t1523325321::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t1523325321::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t1523325321::get_offset_of_indent_26(),
	XmlTextWriter_t1523325321::get_offset_of_indent_count_27(),
	XmlTextWriter_t1523325321::get_offset_of_indent_char_28(),
	XmlTextWriter_t1523325321::get_offset_of_indent_string_29(),
	XmlTextWriter_t1523325321::get_offset_of_newline_30(),
	XmlTextWriter_t1523325321::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t1523325321::get_offset_of_quote_char_32(),
	XmlTextWriter_t1523325321::get_offset_of_v2_33(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_34(),
	XmlTextWriter_t1523325321_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XmlNodeInfo_t1808742809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[7] = 
{
	XmlNodeInfo_t1808742809::get_offset_of_Prefix_0(),
	XmlNodeInfo_t1808742809::get_offset_of_LocalName_1(),
	XmlNodeInfo_t1808742809::get_offset_of_NS_2(),
	XmlNodeInfo_t1808742809::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t1808742809::get_offset_of_HasElements_4(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t1808742809::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (StringUtil_t614327009), -1, sizeof(StringUtil_t614327009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1718[2] = 
{
	StringUtil_t614327009_StaticFields::get_offset_of_cul_0(),
	StringUtil_t614327009_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XmlDeclState_t205203294)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[5] = 
{
	XmlDeclState_t205203294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XmlTokenizedType_t3753548874)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[14] = 
{
	XmlTokenizedType_t3753548874::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XmlUrlResolver_t343097532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	XmlUrlResolver_t343097532::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XmlWhitespace_t4130926598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XmlWriter_t4278601340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	XmlWriter_t4278601340::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (XmlWriterSettings_t233403071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[11] = 
{
	XmlWriterSettings_t233403071::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t233403071::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t233403071::get_offset_of_conformance_2(),
	XmlWriterSettings_t233403071::get_offset_of_encoding_3(),
	XmlWriterSettings_t233403071::get_offset_of_indent_4(),
	XmlWriterSettings_t233403071::get_offset_of_indentChars_5(),
	XmlWriterSettings_t233403071::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t233403071::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t233403071::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t233403071::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t233403071::get_offset_of_outputMethod_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XmlNodeChangedEventHandler_t3074502249), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[18] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D18_9(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D23_10(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D25_11(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D26_12(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D27_13(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D28_14(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D29_15(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D43_16(),
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D44_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (U24ArrayTypeU24208_t1676616639)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24208_t1676616639_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (U24ArrayTypeU24236_t1676616730)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24236_t1676616730_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U24ArrayTypeU2472_t3379220535)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t3379220535_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (U24ArrayTypeU241532_t435483166)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241532_t435483166_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (U24ArrayTypeU248_t3988332414)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332414_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (U24ArrayTypeU24256_t1676616795)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616795_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (U24ArrayTypeU241280_t435480436)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t435480436_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (U3CModuleU3E_t86524795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (DataContractAttribute_t2462274566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[1] = 
{
	DataContractAttribute_t2462274566::get_offset_of_U3CIsReferenceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (DataMemberAttribute_t2601848894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[4] = 
{
	DataMemberAttribute_t2601848894::get_offset_of_is_required_0(),
	DataMemberAttribute_t2601848894::get_offset_of_emit_default_1(),
	DataMemberAttribute_t2601848894::get_offset_of_name_2(),
	DataMemberAttribute_t2601848894::get_offset_of_order_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (EnumMemberAttribute_t1202205191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[1] = 
{
	EnumMemberAttribute_t1202205191::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (ConstructorHandling_t2475221485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1740[3] = 
{
	ConstructorHandling_t2475221485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (DateFormatHandling_t4014082626)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[3] = 
{
	DateFormatHandling_t4014082626::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (DateParseHandling_t2108333400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	DateParseHandling_t2108333400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (DateTimeZoneHandling_t2945560484)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	DateTimeZoneHandling_t2945560484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (DefaultValueHandling_t1569448045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1744[5] = 
{
	DefaultValueHandling_t1569448045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (FloatFormatHandling_t3887485542)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1745[4] = 
{
	FloatFormatHandling_t3887485542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (FloatParseHandling_t3074080948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[3] = 
{
	FloatParseHandling_t3074080948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (Formatting_t732683613)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	Formatting_t732683613::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (JsonArrayAttribute_t2333618243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (JsonConstructorAttribute_t1098405378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (JsonContainerAttribute_t1917602971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[6] = 
{
	JsonContainerAttribute_t1917602971::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0(),
	JsonContainerAttribute_t1917602971::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1(),
	JsonContainerAttribute_t1917602971::get_offset_of__isReference_2(),
	JsonContainerAttribute_t1917602971::get_offset_of__itemIsReference_3(),
	JsonContainerAttribute_t1917602971::get_offset_of__itemReferenceLoopHandling_4(),
	JsonContainerAttribute_t1917602971::get_offset_of__itemTypeNameHandling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (JsonConvert_t4088335769), -1, sizeof(JsonConvert_t4088335769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1753[8] = 
{
	JsonConvert_t4088335769_StaticFields::get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_True_1(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_False_2(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_Null_3(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_Undefined_4(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_PositiveInfinity_5(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_NegativeInfinity_6(),
	JsonConvert_t4088335769_StaticFields::get_offset_of_NaN_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (JsonConverter_t2159686854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (JsonConverterAttribute_t1044341340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[2] = 
{
	JsonConverterAttribute_t1044341340::get_offset_of__converterType_0(),
	JsonConverterAttribute_t1044341340::get_offset_of_U3CConverterParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (JsonConverterCollection_t530165700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (JsonDictionaryAttribute_t55363084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (JsonException_t3991042933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (JsonExtensionDataAttribute_t3618022163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	JsonExtensionDataAttribute_t3618022163::get_offset_of_U3CWriteDataU3Ek__BackingField_0(),
	JsonExtensionDataAttribute_t3618022163::get_offset_of_U3CReadDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (JsonIgnoreAttribute_t4056167888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (JsonObjectAttribute_t863918851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[2] = 
{
	JsonObjectAttribute_t863918851::get_offset_of__memberSerialization_6(),
	JsonObjectAttribute_t863918851::get_offset_of__itemRequired_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (JsonContainerType_t288741633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1762[5] = 
{
	JsonContainerType_t288741633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (JsonPosition_t3864946409)+ sizeof (Il2CppObject), sizeof(JsonPosition_t3864946409_marshaled_pinvoke), sizeof(JsonPosition_t3864946409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1763[5] = 
{
	JsonPosition_t3864946409_StaticFields::get_offset_of_SpecialCharacters_0(),
	JsonPosition_t3864946409::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t3864946409::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t3864946409::get_offset_of_PropertyName_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t3864946409::get_offset_of_HasIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (JsonPropertyAttribute_t3435603053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[14] = 
{
	JsonPropertyAttribute_t3435603053::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_t3435603053::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_t3435603053::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_t3435603053::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_t3435603053::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_t3435603053::get_offset_of__isReference_5(),
	JsonPropertyAttribute_t3435603053::get_offset_of__order_6(),
	JsonPropertyAttribute_t3435603053::get_offset_of__required_7(),
	JsonPropertyAttribute_t3435603053::get_offset_of__itemIsReference_8(),
	JsonPropertyAttribute_t3435603053::get_offset_of__itemReferenceLoopHandling_9(),
	JsonPropertyAttribute_t3435603053::get_offset_of__itemTypeNameHandling_10(),
	JsonPropertyAttribute_t3435603053::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11(),
	JsonPropertyAttribute_t3435603053::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12(),
	JsonPropertyAttribute_t3435603053::get_offset_of_U3CPropertyNameU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (JsonReader_t816925123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[15] = 
{
	JsonReader_t816925123::get_offset_of__tokenType_0(),
	JsonReader_t816925123::get_offset_of__value_1(),
	JsonReader_t816925123::get_offset_of__quoteChar_2(),
	JsonReader_t816925123::get_offset_of__currentState_3(),
	JsonReader_t816925123::get_offset_of__currentPosition_4(),
	JsonReader_t816925123::get_offset_of__culture_5(),
	JsonReader_t816925123::get_offset_of__dateTimeZoneHandling_6(),
	JsonReader_t816925123::get_offset_of__maxDepth_7(),
	JsonReader_t816925123::get_offset_of__hasExceededMaxDepth_8(),
	JsonReader_t816925123::get_offset_of__dateParseHandling_9(),
	JsonReader_t816925123::get_offset_of__floatParseHandling_10(),
	JsonReader_t816925123::get_offset_of__dateFormatString_11(),
	JsonReader_t816925123::get_offset_of__stack_12(),
	JsonReader_t816925123::get_offset_of_U3CCloseInputU3Ek__BackingField_13(),
	JsonReader_t816925123::get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (State_t2338717890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1766[14] = 
{
	State_t2338717890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (JsonReaderException_t354140082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[3] = 
{
	JsonReaderException_t354140082::get_offset_of_U3CLineNumberU3Ek__BackingField_11(),
	JsonReaderException_t354140082::get_offset_of_U3CLinePositionU3Ek__BackingField_12(),
	JsonReaderException_t354140082::get_offset_of_U3CPathU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (JsonRequiredAttribute_t224246243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (JsonSerializationException_t2886273343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (JsonSerializer_t251850770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[31] = 
{
	JsonSerializer_t251850770::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t251850770::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t251850770::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t251850770::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t251850770::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t251850770::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t251850770::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t251850770::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t251850770::get_offset_of__constructorHandling_8(),
	JsonSerializer_t251850770::get_offset_of__metadataPropertyHandling_9(),
	JsonSerializer_t251850770::get_offset_of__converters_10(),
	JsonSerializer_t251850770::get_offset_of__contractResolver_11(),
	JsonSerializer_t251850770::get_offset_of__traceWriter_12(),
	JsonSerializer_t251850770::get_offset_of__equalityComparer_13(),
	JsonSerializer_t251850770::get_offset_of__binder_14(),
	JsonSerializer_t251850770::get_offset_of__context_15(),
	JsonSerializer_t251850770::get_offset_of__referenceResolver_16(),
	JsonSerializer_t251850770::get_offset_of__formatting_17(),
	JsonSerializer_t251850770::get_offset_of__dateFormatHandling_18(),
	JsonSerializer_t251850770::get_offset_of__dateTimeZoneHandling_19(),
	JsonSerializer_t251850770::get_offset_of__dateParseHandling_20(),
	JsonSerializer_t251850770::get_offset_of__floatFormatHandling_21(),
	JsonSerializer_t251850770::get_offset_of__floatParseHandling_22(),
	JsonSerializer_t251850770::get_offset_of__stringEscapeHandling_23(),
	JsonSerializer_t251850770::get_offset_of__culture_24(),
	JsonSerializer_t251850770::get_offset_of__maxDepth_25(),
	JsonSerializer_t251850770::get_offset_of__maxDepthSet_26(),
	JsonSerializer_t251850770::get_offset_of__checkAdditionalContent_27(),
	JsonSerializer_t251850770::get_offset_of__dateFormatString_28(),
	JsonSerializer_t251850770::get_offset_of__dateFormatStringSet_29(),
	JsonSerializer_t251850770::get_offset_of_Error_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (JsonSerializerSettings_t2589405525), -1, sizeof(JsonSerializerSettings_t2589405525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1771[33] = 
{
	JsonSerializerSettings_t2589405525_StaticFields::get_offset_of_DefaultContext_0(),
	JsonSerializerSettings_t2589405525_StaticFields::get_offset_of_DefaultCulture_1(),
	JsonSerializerSettings_t2589405525::get_offset_of__formatting_2(),
	JsonSerializerSettings_t2589405525::get_offset_of__dateFormatHandling_3(),
	JsonSerializerSettings_t2589405525::get_offset_of__dateTimeZoneHandling_4(),
	JsonSerializerSettings_t2589405525::get_offset_of__dateParseHandling_5(),
	JsonSerializerSettings_t2589405525::get_offset_of__floatFormatHandling_6(),
	JsonSerializerSettings_t2589405525::get_offset_of__floatParseHandling_7(),
	JsonSerializerSettings_t2589405525::get_offset_of__stringEscapeHandling_8(),
	JsonSerializerSettings_t2589405525::get_offset_of__culture_9(),
	JsonSerializerSettings_t2589405525::get_offset_of__checkAdditionalContent_10(),
	JsonSerializerSettings_t2589405525::get_offset_of__maxDepth_11(),
	JsonSerializerSettings_t2589405525::get_offset_of__maxDepthSet_12(),
	JsonSerializerSettings_t2589405525::get_offset_of__dateFormatString_13(),
	JsonSerializerSettings_t2589405525::get_offset_of__dateFormatStringSet_14(),
	JsonSerializerSettings_t2589405525::get_offset_of__typeNameAssemblyFormat_15(),
	JsonSerializerSettings_t2589405525::get_offset_of__defaultValueHandling_16(),
	JsonSerializerSettings_t2589405525::get_offset_of__preserveReferencesHandling_17(),
	JsonSerializerSettings_t2589405525::get_offset_of__nullValueHandling_18(),
	JsonSerializerSettings_t2589405525::get_offset_of__objectCreationHandling_19(),
	JsonSerializerSettings_t2589405525::get_offset_of__missingMemberHandling_20(),
	JsonSerializerSettings_t2589405525::get_offset_of__referenceLoopHandling_21(),
	JsonSerializerSettings_t2589405525::get_offset_of__context_22(),
	JsonSerializerSettings_t2589405525::get_offset_of__constructorHandling_23(),
	JsonSerializerSettings_t2589405525::get_offset_of__typeNameHandling_24(),
	JsonSerializerSettings_t2589405525::get_offset_of__metadataPropertyHandling_25(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CConvertersU3Ek__BackingField_26(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CContractResolverU3Ek__BackingField_27(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CEqualityComparerU3Ek__BackingField_28(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CTraceWriterU3Ek__BackingField_30(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CBinderU3Ek__BackingField_31(),
	JsonSerializerSettings_t2589405525::get_offset_of_U3CErrorU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (ReadType_t3446921512)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[10] = 
{
	ReadType_t3446921512::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (JsonTextReader_t2658709200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[11] = 
{
	JsonTextReader_t2658709200::get_offset_of__reader_15(),
	JsonTextReader_t2658709200::get_offset_of__chars_16(),
	JsonTextReader_t2658709200::get_offset_of__charsUsed_17(),
	JsonTextReader_t2658709200::get_offset_of__charPos_18(),
	JsonTextReader_t2658709200::get_offset_of__lineStartPos_19(),
	JsonTextReader_t2658709200::get_offset_of__lineNumber_20(),
	JsonTextReader_t2658709200::get_offset_of__isEndOfFile_21(),
	JsonTextReader_t2658709200::get_offset_of__stringBuffer_22(),
	JsonTextReader_t2658709200::get_offset_of__stringReference_23(),
	JsonTextReader_t2658709200::get_offset_of__arrayPool_24(),
	JsonTextReader_t2658709200::get_offset_of_NameTable_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (JsonTextWriter_t2814114432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[10] = 
{
	JsonTextWriter_t2814114432::get_offset_of__writer_13(),
	JsonTextWriter_t2814114432::get_offset_of__base64Encoder_14(),
	JsonTextWriter_t2814114432::get_offset_of__indentChar_15(),
	JsonTextWriter_t2814114432::get_offset_of__indentation_16(),
	JsonTextWriter_t2814114432::get_offset_of__quoteChar_17(),
	JsonTextWriter_t2814114432::get_offset_of__quoteName_18(),
	JsonTextWriter_t2814114432::get_offset_of__charEscapeFlags_19(),
	JsonTextWriter_t2814114432::get_offset_of__writeBuffer_20(),
	JsonTextWriter_t2814114432::get_offset_of__arrayPool_21(),
	JsonTextWriter_t2814114432::get_offset_of__indentChars_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (JsonToken_t4173078175)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1775[19] = 
{
	JsonToken_t4173078175::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (JsonWriter_t972330355), -1, sizeof(JsonWriter_t972330355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1776[13] = 
{
	JsonWriter_t972330355_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t972330355_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t972330355::get_offset_of__stack_2(),
	JsonWriter_t972330355::get_offset_of__currentPosition_3(),
	JsonWriter_t972330355::get_offset_of__currentState_4(),
	JsonWriter_t972330355::get_offset_of__formatting_5(),
	JsonWriter_t972330355::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t972330355::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t972330355::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t972330355::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t972330355::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t972330355::get_offset_of__dateFormatString_11(),
	JsonWriter_t972330355::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (State_t671991922)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[11] = 
{
	State_t671991922::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (JsonWriterException_t1037736450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	JsonWriterException_t1037736450::get_offset_of_U3CPathU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (MemberSerialization_t1550301796)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[4] = 
{
	MemberSerialization_t1550301796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (MetadataPropertyHandling_t2626038881)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[4] = 
{
	MetadataPropertyHandling_t2626038881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (MissingMemberHandling_t2077487315)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1781[3] = 
{
	MissingMemberHandling_t2077487315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (NullValueHandling_t2754652381)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	NullValueHandling_t2754652381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ObjectCreationHandling_t56081595)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1783[4] = 
{
	ObjectCreationHandling_t56081595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (PreserveReferencesHandling_t4230591217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1784[5] = 
{
	PreserveReferencesHandling_t4230591217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (ReferenceLoopHandling_t2761661122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1785[4] = 
{
	ReferenceLoopHandling_t2761661122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (Required_t3921306327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[5] = 
{
	Required_t3921306327::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (StringEscapeHandling_t1042460335)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[4] = 
{
	StringEscapeHandling_t1042460335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (TypeNameHandling_t2359325474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[6] = 
{
	TypeNameHandling_t2359325474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (WriteState_t4055692778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[8] = 
{
	WriteState_t4055692778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (Base64Encoder_t2146525835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[4] = 
{
	Base64Encoder_t2146525835::get_offset_of__charsLine_0(),
	Base64Encoder_t2146525835::get_offset_of__writer_1(),
	Base64Encoder_t2146525835::get_offset_of__leftOverBytes_2(),
	Base64Encoder_t2146525835::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (CollectionUtils_t246479025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (PrimitiveTypeCode_t2429291660)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[43] = 
{
	PrimitiveTypeCode_t2429291660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (TypeInformation_t2222045584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[2] = 
{
	TypeInformation_t2222045584::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_t2222045584::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (ParseResult_t4245056558)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1797[5] = 
{
	ParseResult_t4245056558::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ConvertUtils_t2565263886), -1, sizeof(ConvertUtils_t2565263886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1798[3] = 
{
	ConvertUtils_t2565263886_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_t2565263886_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_t2565263886_StaticFields::get_offset_of_CastConverters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (TypeConvertKey_t866134174)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	TypeConvertKey_t866134174::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeConvertKey_t866134174::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
