﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_Int64Converter3884840575.h"
#include "System_System_ComponentModel_LocalizableAttribute723561746.h"
#include "System_System_ComponentModel_MemberDescriptor2617136693.h"
#include "System_System_ComponentModel_MemberDescriptor_Membe247321145.h"
#include "System_System_ComponentModel_NullableConverter2193771621.h"
#include "System_System_ComponentModel_ProgressChangedEventA1382019100.h"
#include "System_System_ComponentModel_PropertyChangedEventAr448576452.h"
#include "System_System_ComponentModel_PropertyDescriptor2073374448.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3344846062.h"
#include "System_System_ComponentModel_ReadOnlyAttribute2204781440.h"
#include "System_System_ComponentModel_ReferenceConverter4239496801.h"
#include "System_System_ComponentModel_ReflectionEventDescri4097774116.h"
#include "System_System_ComponentModel_ReflectionPropertyDesc735670971.h"
#include "System_System_ComponentModel_RefreshEventArgs3234103944.h"
#include "System_System_ComponentModel_SByteConverter3013403921.h"
#include "System_System_ComponentModel_SingleConverter3295811230.h"
#include "System_System_ComponentModel_StringConverter1422224629.h"
#include "System_System_ComponentModel_TimeSpanConverter2449472719.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "System_System_ComponentModel_TypeConverter_Standar1784247913.h"
#include "System_System_ComponentModel_TypeConverterAttribut1738187778.h"
#include "System_System_ComponentModel_TypeDescriptionProvid3543085017.h"
#include "System_System_ComponentModel_TypeDescriptor1537159061.h"
#include "System_System_ComponentModel_TypeDescriptor_Attrib3377629738.h"
#include "System_System_ComponentModel_TypeDescriptor_Wrappe4187483089.h"
#include "System_System_ComponentModel_TypeDescriptor_Defaul2461492885.h"
#include "System_System_ComponentModel_Info3741775290.h"
#include "System_System_ComponentModel_ComponentInfo809310961.h"
#include "System_System_ComponentModel_TypeInfo1685774996.h"
#include "System_System_ComponentModel_UInt16Converter1984177639.h"
#include "System_System_ComponentModel_UInt32Converter3471296237.h"
#include "System_System_ComponentModel_UInt64Converter1982382446.h"
#include "System_System_ComponentModel_WeakObjectWrapper1518976226.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3263379503.h"
#include "System_System_ComponentModel_Win32Exception819808416.h"
#include "System_System_Diagnostics_Stopwatch3420517611.h"
#include "System_System_Diagnostics_TraceLevel4286736575.h"
#include "System_System_IO_Compression_CompressionMode1453657991.h"
#include "System_System_IO_Compression_DeflateStream2030147241.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag2055733333.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet1873379884.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe3250749483.h"
#include "System_System_IO_Compression_GZipStream183418746.h"
#include "System_System_Net_Security_AuthenticatedStream3871465409.h"
#include "System_System_Net_Security_AuthenticationLevel4161053470.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_Security_SslStream490807902.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe3681700014.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"
#include "System_System_Net_Sockets_LingerOption3290254502.h"
#include "System_System_Net_Sockets_MulticastOption979356607.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "System_System_Net_Sockets_ProtocolType3327388960.h"
#include "System_System_Net_Sockets_SelectMode3812195181.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "System_System_Net_Sockets_Socket_SocketOperation4113109398.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult753587752.h"
#include "System_System_Net_Sockets_Socket_Worker3140563676.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall742231849.h"
#include "System_System_Net_Sockets_SocketError4204345479.h"
#include "System_System_Net_Sockets_SocketException3119490894.h"
#include "System_System_Net_Sockets_SocketFlags4205073670.h"
#include "System_System_Net_Sockets_SocketOptionLevel2476940110.h"
#include "System_System_Net_Sockets_SocketOptionName1841276065.h"
#include "System_System_Net_Sockets_SocketShutdown1229168279.h"
#include "System_System_Net_Sockets_SocketType1204660219.h"
#include "System_System_Net_AuthenticationManager3105338575.h"
#include "System_System_Net_Authorization3486603059.h"
#include "System_System_Net_BasicClient720594003.h"
#include "System_System_Net_ChunkStream1623008007.h"
#include "System_System_Net_ChunkStream_State3599614847.h"
#include "System_System_Net_ChunkStream_Chunk3584500059.h"
#include "System_System_Net_Cookie2033273982.h"
#include "System_System_Net_CookieCollection2536410684.h"
#include "System_System_Net_CookieCollection_CookieCollectio2125669164.h"
#include "System_System_Net_CookieContainer230274359.h"
#include "System_System_Net_CookieException2122856709.h"
#include "System_System_Net_DecompressionMethods3697240007.h"
#include "System_System_Net_DefaultCertificatePolicy3264712578.h"
#include "System_System_Net_DigestHeaderParser3747762954.h"
#include "System_System_Net_DigestSession1153412844.h"
#include "System_System_Net_DigestClient3690764489.h"
#include "System_System_Net_Dns3289571299.h"
#include "System_System_Net_DownloadProgressChangedEventArgs1659668818.h"
#include "System_System_Net_EndPoint1026786191.h"
#include "System_System_Net_FileWebRequest1151586641.h"
#include "System_System_Net_FileWebRequest_FileWebStream902488976.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba1444420308.h"
#include "System_System_Net_FileWebRequestCreator1433256879.h"
#include "System_System_Net_FileWebResponse2971811667.h"
#include "System_System_Net_FtpAsyncResult1090897169.h"
#include "System_System_Net_FtpDataStream2383958406.h"
#include "System_System_Net_FtpDataStream_WriteDelegate1637408689.h"
#include "System_System_Net_FtpDataStream_ReadDelegate3891738222.h"
#include "System_System_Net_FtpRequestCreator3707472729.h"
#include "System_System_Net_FtpStatus1259919022.h"
#include "System_System_Net_FtpStatusCode1354479291.h"
#include "System_System_Net_FtpWebRequest3084461655.h"
#include "System_System_Net_FtpWebRequest_RequestState430963524.h"
#include "System_System_Net_FtpWebResponse2761394957.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (Int64Converter_t3884840575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (LocalizableAttribute_t723561746), -1, sizeof(LocalizableAttribute_t723561746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1101[4] = 
{
	LocalizableAttribute_t723561746::get_offset_of_localizable_0(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_Default_1(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t723561746_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (MemberDescriptor_t2617136693), -1, sizeof(MemberDescriptor_t2617136693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1102[4] = 
{
	MemberDescriptor_t2617136693::get_offset_of_name_0(),
	MemberDescriptor_t2617136693::get_offset_of_attrs_1(),
	MemberDescriptor_t2617136693::get_offset_of_attrCollection_2(),
	MemberDescriptor_t2617136693_StaticFields::get_offset_of_default_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (MemberDescriptorComparer_t247321145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (NullableConverter_t2193771621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1104[3] = 
{
	NullableConverter_t2193771621::get_offset_of_nullableType_0(),
	NullableConverter_t2193771621::get_offset_of_underlyingType_1(),
	NullableConverter_t2193771621::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (ProgressChangedEventArgs_t1382019100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1105[2] = 
{
	ProgressChangedEventArgs_t1382019100::get_offset_of_progress_1(),
	ProgressChangedEventArgs_t1382019100::get_offset_of_state_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (PropertyChangedEventArgs_t448576452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1106[1] = 
{
	PropertyChangedEventArgs_t448576452::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (PropertyDescriptor_t2073374448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (PropertyDescriptorCollection_t3344846062), -1, sizeof(PropertyDescriptorCollection_t3344846062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1108[3] = 
{
	PropertyDescriptorCollection_t3344846062_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3344846062::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3344846062::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (ReadOnlyAttribute_t2204781440), -1, sizeof(ReadOnlyAttribute_t2204781440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1109[4] = 
{
	ReadOnlyAttribute_t2204781440::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t2204781440_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (ReferenceConverter_t4239496801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1110[1] = 
{
	ReferenceConverter_t4239496801::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (ReflectionEventDescriptor_t4097774116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1111[5] = 
{
	ReflectionEventDescriptor_t4097774116::get_offset_of__eventType_4(),
	ReflectionEventDescriptor_t4097774116::get_offset_of__componentType_5(),
	ReflectionEventDescriptor_t4097774116::get_offset_of__eventInfo_6(),
	ReflectionEventDescriptor_t4097774116::get_offset_of_add_method_7(),
	ReflectionEventDescriptor_t4097774116::get_offset_of_remove_method_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (ReflectionPropertyDescriptor_t735670971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1112[3] = 
{
	ReflectionPropertyDescriptor_t735670971::get_offset_of__member_4(),
	ReflectionPropertyDescriptor_t735670971::get_offset_of__componentType_5(),
	ReflectionPropertyDescriptor_t735670971::get_offset_of__propertyType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (RefreshEventArgs_t3234103944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1113[2] = 
{
	RefreshEventArgs_t3234103944::get_offset_of_component_1(),
	RefreshEventArgs_t3234103944::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (SByteConverter_t3013403921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (SingleConverter_t3295811230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (StringConverter_t1422224629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (TimeSpanConverter_t2449472719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (TypeConverter_t1753450284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (StandardValuesCollection_t1784247913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[1] = 
{
	StandardValuesCollection_t1784247913::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (TypeConverterAttribute_t1738187778), -1, sizeof(TypeConverterAttribute_t1738187778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1120[2] = 
{
	TypeConverterAttribute_t1738187778_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t1738187778::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (TypeDescriptionProvider_t3543085017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1121[1] = 
{
	TypeDescriptionProvider_t3543085017::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (TypeDescriptor_t1537159061), -1, sizeof(TypeDescriptor_t1537159061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1122[12] = 
{
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_descriptorHandler_2(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentTable_3(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeTable_4(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_editors_5(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeDescriptionProvidersLock_6(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_typeDescriptionProviders_7(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentDescriptionProvidersLock_8(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_componentDescriptionProviders_9(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_onDispose_10(),
	TypeDescriptor_t1537159061_StaticFields::get_offset_of_Refreshed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (AttributeProvider_t3377629738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[1] = 
{
	AttributeProvider_t3377629738::get_offset_of_attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (WrappedTypeDescriptionProvider_t4187483089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1124[1] = 
{
	WrappedTypeDescriptionProvider_t4187483089::get_offset_of_U3CWrappedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (DefaultTypeDescriptionProvider_t2461492885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (Info_t3741775290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1126[6] = 
{
	Info_t3741775290::get_offset_of__infoType_0(),
	Info_t3741775290::get_offset_of__defaultEvent_1(),
	Info_t3741775290::get_offset_of__gotDefaultEvent_2(),
	Info_t3741775290::get_offset_of__defaultProperty_3(),
	Info_t3741775290::get_offset_of__gotDefaultProperty_4(),
	Info_t3741775290::get_offset_of__attributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (ComponentInfo_t809310961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1127[3] = 
{
	ComponentInfo_t809310961::get_offset_of__component_6(),
	ComponentInfo_t809310961::get_offset_of__events_7(),
	ComponentInfo_t809310961::get_offset_of__properties_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (TypeInfo_t1685774996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1128[2] = 
{
	TypeInfo_t1685774996::get_offset_of__events_6(),
	TypeInfo_t1685774996::get_offset_of__properties_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (UInt16Converter_t1984177639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (UInt32Converter_t3471296237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (UInt64Converter_t1982382446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (WeakObjectWrapper_t1518976226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1132[2] = 
{
	WeakObjectWrapper_t1518976226::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t1518976226::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (WeakObjectWrapperComparer_t3263379503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (Win32Exception_t819808416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1134[1] = 
{
	Win32Exception_t819808416::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (Stopwatch_t3420517611), -1, sizeof(Stopwatch_t3420517611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1135[5] = 
{
	Stopwatch_t3420517611_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t3420517611_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t3420517611::get_offset_of_elapsed_2(),
	Stopwatch_t3420517611::get_offset_of_started_3(),
	Stopwatch_t3420517611::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (TraceLevel_t4286736575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1136[6] = 
{
	TraceLevel_t4286736575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (CompressionMode_t1453657991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1137[3] = 
{
	CompressionMode_t1453657991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (DeflateStream_t2030147241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1138[8] = 
{
	DeflateStream_t2030147241::get_offset_of_base_stream_1(),
	DeflateStream_t2030147241::get_offset_of_mode_2(),
	DeflateStream_t2030147241::get_offset_of_leaveOpen_3(),
	DeflateStream_t2030147241::get_offset_of_disposed_4(),
	DeflateStream_t2030147241::get_offset_of_feeder_5(),
	DeflateStream_t2030147241::get_offset_of_z_stream_6(),
	DeflateStream_t2030147241::get_offset_of_io_buffer_7(),
	DeflateStream_t2030147241::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (UnmanagedReadOrWrite_t2055733333), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (ReadMethod_t1873379884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (WriteMethod_t3250749483), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (GZipStream_t183418746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1142[1] = 
{
	GZipStream_t183418746::get_offset_of_deflateStream_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (AuthenticatedStream_t3871465409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1143[2] = 
{
	AuthenticatedStream_t3871465409::get_offset_of_innerStream_1(),
	AuthenticatedStream_t3871465409::get_offset_of_leaveStreamOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (AuthenticationLevel_t4161053470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1144[4] = 
{
	AuthenticationLevel_t4161053470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (SslPolicyErrors_t3099591579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1145[5] = 
{
	SslPolicyErrors_t3099591579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (SslStream_t490807902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1146[3] = 
{
	SslStream_t490807902::get_offset_of_ssl_stream_3(),
	SslStream_t490807902::get_offset_of_validation_callback_4(),
	SslStream_t490807902::get_offset_of_selection_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1147[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (AddressFamily_t3770679850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1148[32] = 
{
	AddressFamily_t3770679850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (LingerOption_t3290254502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1149[2] = 
{
	LingerOption_t3290254502::get_offset_of_enabled_0(),
	LingerOption_t3290254502::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (MulticastOption_t979356607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (NetworkStream_t3953762560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1151[6] = 
{
	NetworkStream_t3953762560::get_offset_of_access_1(),
	NetworkStream_t3953762560::get_offset_of_socket_2(),
	NetworkStream_t3953762560::get_offset_of_owns_socket_3(),
	NetworkStream_t3953762560::get_offset_of_readable_4(),
	NetworkStream_t3953762560::get_offset_of_writeable_5(),
	NetworkStream_t3953762560::get_offset_of_disposed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (ProtocolType_t3327388960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1152[26] = 
{
	ProtocolType_t3327388960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (SelectMode_t3812195181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1153[4] = 
{
	SelectMode_t3812195181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (Socket_t2157335841), -1, sizeof(Socket_t2157335841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1154[22] = 
{
	Socket_t2157335841::get_offset_of_readQ_0(),
	Socket_t2157335841::get_offset_of_writeQ_1(),
	Socket_t2157335841::get_offset_of_islistening_2(),
	Socket_t2157335841::get_offset_of_MinListenPort_3(),
	Socket_t2157335841::get_offset_of_MaxListenPort_4(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t2157335841::get_offset_of_linger_timeout_7(),
	Socket_t2157335841::get_offset_of_socket_8(),
	Socket_t2157335841::get_offset_of_address_family_9(),
	Socket_t2157335841::get_offset_of_socket_type_10(),
	Socket_t2157335841::get_offset_of_protocol_type_11(),
	Socket_t2157335841::get_offset_of_blocking_12(),
	Socket_t2157335841::get_offset_of_blocking_thread_13(),
	Socket_t2157335841::get_offset_of_isbound_14(),
	Socket_t2157335841_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t2157335841::get_offset_of_max_bind_count_16(),
	Socket_t2157335841::get_offset_of_connected_17(),
	Socket_t2157335841::get_offset_of_closed_18(),
	Socket_t2157335841::get_offset_of_disposed_19(),
	Socket_t2157335841::get_offset_of_seed_endpoint_20(),
	Socket_t2157335841_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (SocketOperation_t4113109398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1155[15] = 
{
	SocketOperation_t4113109398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { sizeof (SocketAsyncResult_t753587752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1156[25] = 
{
	SocketAsyncResult_t753587752::get_offset_of_Sock_0(),
	SocketAsyncResult_t753587752::get_offset_of_handle_1(),
	SocketAsyncResult_t753587752::get_offset_of_state_2(),
	SocketAsyncResult_t753587752::get_offset_of_callback_3(),
	SocketAsyncResult_t753587752::get_offset_of_waithandle_4(),
	SocketAsyncResult_t753587752::get_offset_of_delayedException_5(),
	SocketAsyncResult_t753587752::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t753587752::get_offset_of_Buffer_7(),
	SocketAsyncResult_t753587752::get_offset_of_Offset_8(),
	SocketAsyncResult_t753587752::get_offset_of_Size_9(),
	SocketAsyncResult_t753587752::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t753587752::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t753587752::get_offset_of_Addresses_12(),
	SocketAsyncResult_t753587752::get_offset_of_Port_13(),
	SocketAsyncResult_t753587752::get_offset_of_Buffers_14(),
	SocketAsyncResult_t753587752::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t753587752::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t753587752::get_offset_of_total_17(),
	SocketAsyncResult_t753587752::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t753587752::get_offset_of_completed_19(),
	SocketAsyncResult_t753587752::get_offset_of_blocking_20(),
	SocketAsyncResult_t753587752::get_offset_of_error_21(),
	SocketAsyncResult_t753587752::get_offset_of_operation_22(),
	SocketAsyncResult_t753587752::get_offset_of_ares_23(),
	SocketAsyncResult_t753587752::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (Worker_t3140563676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1157[3] = 
{
	Worker_t3140563676::get_offset_of_result_0(),
	Worker_t3140563676::get_offset_of_requireSocketSecurity_1(),
	Worker_t3140563676::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (SocketAsyncCall_t742231849), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (SocketError_t4204345479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1159[48] = 
{
	SocketError_t4204345479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (SocketException_t3119490894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (SocketFlags_t4205073670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1161[11] = 
{
	SocketFlags_t4205073670::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (SocketOptionLevel_t2476940110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1162[6] = 
{
	SocketOptionLevel_t2476940110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (SocketOptionName_t1841276065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1163[44] = 
{
	SocketOptionName_t1841276065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (SocketShutdown_t1229168279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1164[4] = 
{
	SocketShutdown_t1229168279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (SocketType_t1204660219)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1165[7] = 
{
	SocketType_t1204660219::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (AuthenticationManager_t3105338575), -1, sizeof(AuthenticationManager_t3105338575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1166[3] = 
{
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t3105338575_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (Authorization_t3486603059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1167[4] = 
{
	Authorization_t3486603059::get_offset_of_token_0(),
	Authorization_t3486603059::get_offset_of_complete_1(),
	Authorization_t3486603059::get_offset_of_connectionGroupId_2(),
	Authorization_t3486603059::get_offset_of_module_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (BasicClient_t720594003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (ChunkStream_t1623008007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1169[9] = 
{
	ChunkStream_t1623008007::get_offset_of_headers_0(),
	ChunkStream_t1623008007::get_offset_of_chunkSize_1(),
	ChunkStream_t1623008007::get_offset_of_chunkRead_2(),
	ChunkStream_t1623008007::get_offset_of_state_3(),
	ChunkStream_t1623008007::get_offset_of_saved_4(),
	ChunkStream_t1623008007::get_offset_of_sawCR_5(),
	ChunkStream_t1623008007::get_offset_of_gotit_6(),
	ChunkStream_t1623008007::get_offset_of_trailerState_7(),
	ChunkStream_t1623008007::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (State_t3599614847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1170[5] = 
{
	State_t3599614847::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (Chunk_t3584500059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[2] = 
{
	Chunk_t3584500059::get_offset_of_Bytes_0(),
	Chunk_t3584500059::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (Cookie_t2033273982), -1, sizeof(Cookie_t2033273982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1172[18] = 
{
	Cookie_t2033273982::get_offset_of_comment_0(),
	Cookie_t2033273982::get_offset_of_commentUri_1(),
	Cookie_t2033273982::get_offset_of_discard_2(),
	Cookie_t2033273982::get_offset_of_domain_3(),
	Cookie_t2033273982::get_offset_of_expires_4(),
	Cookie_t2033273982::get_offset_of_httpOnly_5(),
	Cookie_t2033273982::get_offset_of_name_6(),
	Cookie_t2033273982::get_offset_of_path_7(),
	Cookie_t2033273982::get_offset_of_port_8(),
	Cookie_t2033273982::get_offset_of_ports_9(),
	Cookie_t2033273982::get_offset_of_secure_10(),
	Cookie_t2033273982::get_offset_of_timestamp_11(),
	Cookie_t2033273982::get_offset_of_val_12(),
	Cookie_t2033273982::get_offset_of_version_13(),
	Cookie_t2033273982_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t2033273982_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t2033273982_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t2033273982::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (CookieCollection_t2536410684), -1, sizeof(CookieCollection_t2536410684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1173[2] = 
{
	CookieCollection_t2536410684::get_offset_of_list_0(),
	CookieCollection_t2536410684_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (CookieCollectionComparer_t2125669164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (CookieContainer_t230274359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1175[4] = 
{
	CookieContainer_t230274359::get_offset_of_capacity_0(),
	CookieContainer_t230274359::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t230274359::get_offset_of_maxCookieSize_2(),
	CookieContainer_t230274359::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (CookieException_t2122856709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (DecompressionMethods_t3697240007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1177[4] = 
{
	DecompressionMethods_t3697240007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (DefaultCertificatePolicy_t3264712578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (DigestHeaderParser_t3747762954), -1, sizeof(DigestHeaderParser_t3747762954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1179[5] = 
{
	DigestHeaderParser_t3747762954::get_offset_of_header_0(),
	DigestHeaderParser_t3747762954::get_offset_of_length_1(),
	DigestHeaderParser_t3747762954::get_offset_of_pos_2(),
	DigestHeaderParser_t3747762954_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t3747762954::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (DigestSession_t1153412844), -1, sizeof(DigestSession_t1153412844_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1180[6] = 
{
	DigestSession_t1153412844_StaticFields::get_offset_of_rng_0(),
	DigestSession_t1153412844::get_offset_of_lastUse_1(),
	DigestSession_t1153412844::get_offset_of__nc_2(),
	DigestSession_t1153412844::get_offset_of_hash_3(),
	DigestSession_t1153412844::get_offset_of_parser_4(),
	DigestSession_t1153412844::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (DigestClient_t3690764489), -1, sizeof(DigestClient_t3690764489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1181[1] = 
{
	DigestClient_t3690764489_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (Dns_t3289571299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (DownloadProgressChangedEventArgs_t1659668818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1183[2] = 
{
	DownloadProgressChangedEventArgs_t1659668818::get_offset_of_received_3(),
	DownloadProgressChangedEventArgs_t1659668818::get_offset_of_total_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (EndPoint_t1026786191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (FileWebRequest_t1151586641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1185[14] = 
{
	FileWebRequest_t1151586641::get_offset_of_uri_6(),
	FileWebRequest_t1151586641::get_offset_of_webHeaders_7(),
	FileWebRequest_t1151586641::get_offset_of_credentials_8(),
	FileWebRequest_t1151586641::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1151586641::get_offset_of_contentLength_10(),
	FileWebRequest_t1151586641::get_offset_of_fileAccess_11(),
	FileWebRequest_t1151586641::get_offset_of_method_12(),
	FileWebRequest_t1151586641::get_offset_of_proxy_13(),
	FileWebRequest_t1151586641::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1151586641::get_offset_of_timeout_15(),
	FileWebRequest_t1151586641::get_offset_of_webResponse_16(),
	FileWebRequest_t1151586641::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t1151586641::get_offset_of_requesting_18(),
	FileWebRequest_t1151586641::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (FileWebStream_t902488976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1186[1] = 
{
	FileWebStream_t902488976::get_offset_of_webRequest_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (GetResponseCallback_t1444420308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (FileWebRequestCreator_t1433256879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (FileWebResponse_t2971811667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[5] = 
{
	FileWebResponse_t2971811667::get_offset_of_responseUri_1(),
	FileWebResponse_t2971811667::get_offset_of_fileStream_2(),
	FileWebResponse_t2971811667::get_offset_of_contentLength_3(),
	FileWebResponse_t2971811667::get_offset_of_webHeaders_4(),
	FileWebResponse_t2971811667::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (FtpAsyncResult_t1090897169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[9] = 
{
	FtpAsyncResult_t1090897169::get_offset_of_response_0(),
	FtpAsyncResult_t1090897169::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t1090897169::get_offset_of_exception_2(),
	FtpAsyncResult_t1090897169::get_offset_of_callback_3(),
	FtpAsyncResult_t1090897169::get_offset_of_stream_4(),
	FtpAsyncResult_t1090897169::get_offset_of_state_5(),
	FtpAsyncResult_t1090897169::get_offset_of_completed_6(),
	FtpAsyncResult_t1090897169::get_offset_of_synch_7(),
	FtpAsyncResult_t1090897169::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (FtpDataStream_t2383958406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1191[5] = 
{
	FtpDataStream_t2383958406::get_offset_of_request_1(),
	FtpDataStream_t2383958406::get_offset_of_networkStream_2(),
	FtpDataStream_t2383958406::get_offset_of_disposed_3(),
	FtpDataStream_t2383958406::get_offset_of_isRead_4(),
	FtpDataStream_t2383958406::get_offset_of_totalRead_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (WriteDelegate_t1637408689), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (ReadDelegate_t3891738222), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (FtpRequestCreator_t3707472729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (FtpStatus_t1259919022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1195[2] = 
{
	FtpStatus_t1259919022::get_offset_of_statusCode_0(),
	FtpStatus_t1259919022::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (FtpStatusCode_t1354479291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1196[38] = 
{
	FtpStatusCode_t1354479291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (FtpWebRequest_t3084461655), -1, sizeof(FtpWebRequest_t3084461655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1197[31] = 
{
	FtpWebRequest_t3084461655::get_offset_of_requestUri_6(),
	FtpWebRequest_t3084461655::get_offset_of_file_name_7(),
	FtpWebRequest_t3084461655::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3084461655::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3084461655::get_offset_of_dataStream_10(),
	FtpWebRequest_t3084461655::get_offset_of_controlStream_11(),
	FtpWebRequest_t3084461655::get_offset_of_controlReader_12(),
	FtpWebRequest_t3084461655::get_offset_of_credentials_13(),
	FtpWebRequest_t3084461655::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3084461655::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3084461655::get_offset_of_proxy_16(),
	FtpWebRequest_t3084461655::get_offset_of_timeout_17(),
	FtpWebRequest_t3084461655::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3084461655::get_offset_of_offset_19(),
	FtpWebRequest_t3084461655::get_offset_of_binary_20(),
	FtpWebRequest_t3084461655::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3084461655::get_offset_of_usePassive_22(),
	FtpWebRequest_t3084461655::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3084461655::get_offset_of_method_24(),
	FtpWebRequest_t3084461655::get_offset_of_renameTo_25(),
	FtpWebRequest_t3084461655::get_offset_of_locker_26(),
	FtpWebRequest_t3084461655::get_offset_of_requestState_27(),
	FtpWebRequest_t3084461655::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3084461655::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3084461655::get_offset_of_requestStream_30(),
	FtpWebRequest_t3084461655::get_offset_of_initial_path_31(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3084461655::get_offset_of_callback_33(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_35(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (RequestState_t430963524)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1198[10] = 
{
	RequestState_t430963524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (FtpWebResponse_t2761394957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1199[12] = 
{
	FtpWebResponse_t2761394957::get_offset_of_stream_1(),
	FtpWebResponse_t2761394957::get_offset_of_uri_2(),
	FtpWebResponse_t2761394957::get_offset_of_statusCode_3(),
	FtpWebResponse_t2761394957::get_offset_of_lastModified_4(),
	FtpWebResponse_t2761394957::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2761394957::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2761394957::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2761394957::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2761394957::get_offset_of_method_9(),
	FtpWebResponse_t2761394957::get_offset_of_disposed_10(),
	FtpWebResponse_t2761394957::get_offset_of_request_11(),
	FtpWebResponse_t2761394957::get_offset_of_contentLength_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
