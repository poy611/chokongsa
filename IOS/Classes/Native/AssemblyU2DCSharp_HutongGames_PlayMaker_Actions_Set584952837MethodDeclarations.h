﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion
struct SetAnimatorApplyRootMotion_t584952837;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::.ctor()
extern "C"  void SetAnimatorApplyRootMotion__ctor_m651144705 (SetAnimatorApplyRootMotion_t584952837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::Reset()
extern "C"  void SetAnimatorApplyRootMotion_Reset_m2592544942 (SetAnimatorApplyRootMotion_t584952837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::OnEnter()
extern "C"  void SetAnimatorApplyRootMotion_OnEnter_m2371532184 (SetAnimatorApplyRootMotion_t584952837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::DoApplyRootMotion()
extern "C"  void SetAnimatorApplyRootMotion_DoApplyRootMotion_m134152346 (SetAnimatorApplyRootMotion_t584952837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
