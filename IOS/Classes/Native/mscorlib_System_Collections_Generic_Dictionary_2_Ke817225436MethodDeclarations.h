﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t202289382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke817225436.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4016180767_gshared (Enumerator_t817225436 * __this, Dictionary_2_t202289382 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4016180767(__this, ___host0, method) ((  void (*) (Enumerator_t817225436 *, Dictionary_2_t202289382 *, const MethodInfo*))Enumerator__ctor_m4016180767_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1002906850_gshared (Enumerator_t817225436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1002906850(__this, method) ((  Il2CppObject * (*) (Enumerator_t817225436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1002906850_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3178761526_gshared (Enumerator_t817225436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3178761526(__this, method) ((  void (*) (Enumerator_t817225436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3178761526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
extern "C"  void Enumerator_Dispose_m2102504065_gshared (Enumerator_t817225436 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2102504065(__this, method) ((  void (*) (Enumerator_t817225436 *, const MethodInfo*))Enumerator_Dispose_m2102504065_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4075626530_gshared (Enumerator_t817225436 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4075626530(__this, method) ((  bool (*) (Enumerator_t817225436 *, const MethodInfo*))Enumerator_MoveNext_m4075626530_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1232773490_gshared (Enumerator_t817225436 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1232773490(__this, method) ((  Il2CppObject * (*) (Enumerator_t817225436 *, const MethodInfo*))Enumerator_get_Current_m1232773490_gshared)(__this, method)
