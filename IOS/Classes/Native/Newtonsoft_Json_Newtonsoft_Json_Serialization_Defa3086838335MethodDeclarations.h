﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Serialization.DefaultContractResolver
struct DefaultContractResolver_t3086838335;
// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct DefaultContractResolverState_t822678628;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo>
struct List_1_t1068734154;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Newtonsoft.Json.Serialization.JsonObjectContract
struct JsonObjectContract_t505348133;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty>
struct IList_1_t3597302380;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Func`1<System.Object>
struct Func_1_t1001010649;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_t2167686411;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct List_1_t2844214233;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t4231331326;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t554063095;
// Newtonsoft.Json.Serialization.JsonLinqContract
struct JsonLinqContract_t1458265766;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.Serialization.JsonStringContract
struct JsonStringContract_t4087007991;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t175677893;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonO505348133.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP717767559.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerializatio1550301796.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::get_Instance()
extern "C"  Il2CppObject * DefaultContractResolver_get_Instance_m719666851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::get_DefaultMembersSearchFlags()
extern "C"  int32_t DefaultContractResolver_get_DefaultMembersSearchFlags_m167208547 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_DefaultMembersSearchFlags(System.Reflection.BindingFlags)
extern "C"  void DefaultContractResolver_set_DefaultMembersSearchFlags_m2242002876 (DefaultContractResolver_t3086838335 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_SerializeCompilerGeneratedMembers()
extern "C"  bool DefaultContractResolver_get_SerializeCompilerGeneratedMembers_m1401393058 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableInterface()
extern "C"  bool DefaultContractResolver_get_IgnoreSerializableInterface_m1288541153 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableAttribute()
extern "C"  bool DefaultContractResolver_get_IgnoreSerializableAttribute_m3288728708 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_IgnoreSerializableAttribute(System.Boolean)
extern "C"  void DefaultContractResolver_set_IgnoreSerializableAttribute_m3324535837 (DefaultContractResolver_t3086838335 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor()
extern "C"  void DefaultContractResolver__ctor_m1854313210 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor(System.Boolean)
extern "C"  void DefaultContractResolver__ctor_m1900776177 (DefaultContractResolver_t3086838335 * __this, bool ___shareCache0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::GetState()
extern "C"  DefaultContractResolverState_t822678628 * DefaultContractResolver_GetState_m1081528253 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContract(System.Type)
extern "C"  JsonContract_t1328848902 * DefaultContractResolver_ResolveContract_m3852046461 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::GetSerializableMembers(System.Type)
extern "C"  List_1_t1068734154 * DefaultContractResolver_GetSerializableMembers_m3498116042 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSerializeEntityMember(System.Reflection.MemberInfo)
extern "C"  bool DefaultContractResolver_ShouldSerializeEntityMember_m252716928 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateObjectContract(System.Type)
extern "C"  JsonObjectContract_t505348133 * DefaultContractResolver_CreateObjectContract_m4055722761 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetExtensionDataMemberForType(System.Type)
extern "C"  MemberInfo_t * DefaultContractResolver_GetExtensionDataMemberForType_m3619176676 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetExtensionDataDelegates(Newtonsoft.Json.Serialization.JsonObjectContract,System.Reflection.MemberInfo)
extern "C"  void DefaultContractResolver_SetExtensionDataDelegates_m1405154887 (Il2CppObject * __this /* static, unused */, JsonObjectContract_t505348133 * ___contract0, MemberInfo_t * ___member1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetAttributeConstructor(System.Type)
extern "C"  ConstructorInfo_t4136801618 * DefaultContractResolver_GetAttributeConstructor_m2313890302 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetParameterizedConstructor(System.Type)
extern "C"  ConstructorInfo_t4136801618 * DefaultContractResolver_GetParameterizedConstructor_m1843482395 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateConstructorParameters(System.Reflection.ConstructorInfo,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  Il2CppObject* DefaultContractResolver_CreateConstructorParameters_m3953662498 (DefaultContractResolver_t3086838335 * __this, ConstructorInfo_t4136801618 * ___constructor0, JsonPropertyCollection_t717767559 * ___memberProperties1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePropertyFromConstructorParameter(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ParameterInfo)
extern "C"  JsonProperty_t902655177 * DefaultContractResolver_CreatePropertyFromConstructorParameter_m1689142330 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___matchingMemberProperty0, ParameterInfo_t2235474049 * ___parameterInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContractConverter(System.Type)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ResolveContractConverter_m2036717981 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::GetDefaultCreator(System.Type)
extern "C"  Func_1_t1001010649 * DefaultContractResolver_GetDefaultCreator_m4153928629 (DefaultContractResolver_t3086838335 * __this, Type_t * ___createdType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::InitializeContract(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void DefaultContractResolver_InitializeContract_m3677143199 (DefaultContractResolver_t3086838335 * __this, JsonContract_t1328848902 * ___contract0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveCallbackMethods(Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern "C"  void DefaultContractResolver_ResolveCallbackMethods_m1268092721 (DefaultContractResolver_t3086838335 * __this, JsonContract_t1328848902 * ___contract0, Type_t * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::GetCallbackMethodsForType(System.Type,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>&)
extern "C"  void DefaultContractResolver_GetCallbackMethodsForType_m2916174971 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, List_1_t2167686411 ** ___onSerializing1, List_1_t2167686411 ** ___onSerialized2, List_1_t2167686411 ** ___onDeserializing3, List_1_t2167686411 ** ___onDeserialized4, List_1_t2844214233 ** ___onError5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipDeserialized(System.Type)
extern "C"  bool DefaultContractResolver_ShouldSkipDeserialized_m1444763906 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipSerializing(System.Type)
extern "C"  bool DefaultContractResolver_ShouldSkipSerializing_m1470070700 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Type> Newtonsoft.Json.Serialization.DefaultContractResolver::GetClassHierarchyForType(System.Type)
extern "C"  List_1_t4231331326 * DefaultContractResolver_GetClassHierarchyForType_m2802386479 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonDictionaryContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDictionaryContract(System.Type)
extern "C"  JsonDictionaryContract_t989352188 * DefaultContractResolver_CreateDictionaryContract_m3175275419 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateArrayContract(System.Type)
extern "C"  JsonArrayContract_t145179369 * DefaultContractResolver_CreateArrayContract_m4054642895 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePrimitiveContract(System.Type)
extern "C"  JsonPrimitiveContract_t554063095 * DefaultContractResolver_CreatePrimitiveContract_m1541772047 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonLinqContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateLinqContract(System.Type)
extern "C"  JsonLinqContract_t1458265766 * DefaultContractResolver_CreateLinqContract_m1780143559 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateISerializableContract(System.Type)
extern "C"  JsonISerializableContract_t624170136 * DefaultContractResolver_CreateISerializableContract_m4042803759 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonStringContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateStringContract(System.Type)
extern "C"  JsonStringContract_t4087007991 * DefaultContractResolver_CreateStringContract_m1753774821 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateContract(System.Type)
extern "C"  JsonContract_t1328848902 * DefaultContractResolver_CreateContract_m1216928007 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsJsonPrimitiveType(System.Type)
extern "C"  bool DefaultContractResolver_IsJsonPrimitiveType_m2431765416 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsIConvertible(System.Type)
extern "C"  bool DefaultContractResolver_IsIConvertible_m977129331 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::CanConvertToString(System.Type)
extern "C"  bool DefaultContractResolver_CanConvertToString_m42657002 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsValidCallback(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern "C"  bool DefaultContractResolver_IsValidCallback_m1386243734 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, ParameterInfoU5BU5D_t2015293532* ___parameters1, Type_t * ___attributeType2, MethodInfo_t * ___currentCallback3, Type_t ** ___prevAttributeType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetClrTypeFullName(System.Type)
extern "C"  String_t* DefaultContractResolver_GetClrTypeFullName_m3764657483 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperties(System.Type,Newtonsoft.Json.MemberSerialization)
extern "C"  Il2CppObject* DefaultContractResolver_CreateProperties_m3032238499 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, int32_t ___memberSerialization1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.DefaultContractResolver::CreateMemberValueProvider(System.Reflection.MemberInfo)
extern "C"  Il2CppObject * DefaultContractResolver_CreateMemberValueProvider_m2120756927 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern "C"  JsonProperty_t902655177 * DefaultContractResolver_CreateProperty_m1393435991 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, int32_t ___memberSerialization1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetPropertySettingsFromAttributes(Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String,System.Type,Newtonsoft.Json.MemberSerialization,System.Boolean&)
extern "C"  void DefaultContractResolver_SetPropertySettingsFromAttributes_m3423519322 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___attributeProvider1, String_t* ___name2, Type_t * ___declaringType3, int32_t ___memberSerialization4, bool* ___allowNonPublicAccess5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateShouldSerializeTest(System.Reflection.MemberInfo)
extern "C"  Predicate_1_t3781873254 * DefaultContractResolver_CreateShouldSerializeTest_m2564111599 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetIsSpecifiedActions(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.MemberInfo,System.Boolean)
extern "C"  void DefaultContractResolver_SetIsSpecifiedActions_m2955552932 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___property0, MemberInfo_t * ___member1, bool ___allowNonPublicAccess2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolvePropertyName(System.String)
extern "C"  String_t* DefaultContractResolver_ResolvePropertyName_m801715995 (DefaultContractResolver_t3086838335 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveDictionaryKey(System.String)
extern "C"  String_t* DefaultContractResolver_ResolveDictionaryKey_m66900062 (DefaultContractResolver_t3086838335 * __this, String_t* ___dictionaryKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetResolvedPropertyName(System.String)
extern "C"  String_t* DefaultContractResolver_GetResolvedPropertyName_m2934057817 (DefaultContractResolver_t3086838335 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.cctor()
extern "C"  void DefaultContractResolver__cctor_m1167038451 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
