﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Runtime.InteropServices.HandleRef,System.Object>
struct Dictionary_2_t401599765;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2628400469.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2482460548_gshared (Enumerator_t2628400469 * __this, Dictionary_2_t401599765 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2482460548(__this, ___host0, method) ((  void (*) (Enumerator_t2628400469 *, Dictionary_2_t401599765 *, const MethodInfo*))Enumerator__ctor_m2482460548_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2742889319_gshared (Enumerator_t2628400469 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2742889319(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628400469 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2742889319_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1419644593_gshared (Enumerator_t2628400469 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1419644593(__this, method) ((  void (*) (Enumerator_t2628400469 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1419644593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1081853734_gshared (Enumerator_t2628400469 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1081853734(__this, method) ((  void (*) (Enumerator_t2628400469 *, const MethodInfo*))Enumerator_Dispose_m1081853734_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3111365857_gshared (Enumerator_t2628400469 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3111365857(__this, method) ((  bool (*) (Enumerator_t2628400469 *, const MethodInfo*))Enumerator_MoveNext_m3111365857_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Runtime.InteropServices.HandleRef,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m830096297_gshared (Enumerator_t2628400469 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m830096297(__this, method) ((  Il2CppObject * (*) (Enumerator_t2628400469 *, const MethodInfo*))Enumerator_get_Current_m830096297_gshared)(__this, method)
