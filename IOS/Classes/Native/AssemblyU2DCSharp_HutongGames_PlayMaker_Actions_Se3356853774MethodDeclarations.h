﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMainCamera
struct SetMainCamera_t3356853774;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::.ctor()
extern "C"  void SetMainCamera__ctor_m2665790184 (SetMainCamera_t3356853774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::Reset()
extern "C"  void SetMainCamera_Reset_m312223125 (SetMainCamera_t3356853774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::OnEnter()
extern "C"  void SetMainCamera_OnEnter_m1415587007 (SetMainCamera_t3356853774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
