﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Component
struct Component_t3501516275;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddComponent
struct  AddComponent_t2040077188  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddComponent::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddComponent::component
	FsmString_t952858651 * ___component_12;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AddComponent::storeComponent
	FsmObject_t821476169 * ___storeComponent_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddComponent::removeOnExit
	FsmBool_t1075959796 * ___removeOnExit_14;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.AddComponent::addedComponent
	Component_t3501516275 * ___addedComponent_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(AddComponent_t2040077188, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_component_12() { return static_cast<int32_t>(offsetof(AddComponent_t2040077188, ___component_12)); }
	inline FsmString_t952858651 * get_component_12() const { return ___component_12; }
	inline FsmString_t952858651 ** get_address_of_component_12() { return &___component_12; }
	inline void set_component_12(FsmString_t952858651 * value)
	{
		___component_12 = value;
		Il2CppCodeGenWriteBarrier(&___component_12, value);
	}

	inline static int32_t get_offset_of_storeComponent_13() { return static_cast<int32_t>(offsetof(AddComponent_t2040077188, ___storeComponent_13)); }
	inline FsmObject_t821476169 * get_storeComponent_13() const { return ___storeComponent_13; }
	inline FsmObject_t821476169 ** get_address_of_storeComponent_13() { return &___storeComponent_13; }
	inline void set_storeComponent_13(FsmObject_t821476169 * value)
	{
		___storeComponent_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeComponent_13, value);
	}

	inline static int32_t get_offset_of_removeOnExit_14() { return static_cast<int32_t>(offsetof(AddComponent_t2040077188, ___removeOnExit_14)); }
	inline FsmBool_t1075959796 * get_removeOnExit_14() const { return ___removeOnExit_14; }
	inline FsmBool_t1075959796 ** get_address_of_removeOnExit_14() { return &___removeOnExit_14; }
	inline void set_removeOnExit_14(FsmBool_t1075959796 * value)
	{
		___removeOnExit_14 = value;
		Il2CppCodeGenWriteBarrier(&___removeOnExit_14, value);
	}

	inline static int32_t get_offset_of_addedComponent_15() { return static_cast<int32_t>(offsetof(AddComponent_t2040077188, ___addedComponent_15)); }
	inline Component_t3501516275 * get_addedComponent_15() const { return ___addedComponent_15; }
	inline Component_t3501516275 ** get_address_of_addedComponent_15() { return &___addedComponent_15; }
	inline void set_addedComponent_15(Component_t3501516275 * value)
	{
		___addedComponent_15 = value;
		Il2CppCodeGenWriteBarrier(&___addedComponent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
