﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutToggle
struct GUILayoutToggle_t3170064647;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::.ctor()
extern "C"  void GUILayoutToggle__ctor_m1532565007 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::Reset()
extern "C"  void GUILayoutToggle_Reset_m3473965244 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::OnGUI()
extern "C"  void GUILayoutToggle_OnGUI_m1027963657 (GUILayoutToggle_t3170064647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
