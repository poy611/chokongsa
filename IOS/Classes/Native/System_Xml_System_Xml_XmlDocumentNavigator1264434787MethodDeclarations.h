﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlDocumentNavigator
struct XmlDocumentNavigator_t1264434787;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Xml.XmlDocument
struct XmlDocument_t730752740;
// System.Xml.XmlAttribute
struct XmlAttribute_t6647939;
// System.String
struct String_t;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1075073278;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "System_Xml_System_Xml_XmlAttribute6647939.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope1935109964.h"

// System.Void System.Xml.XmlDocumentNavigator::.ctor(System.Xml.XmlNode)
extern "C"  void XmlDocumentNavigator__ctor_m3471931459 (XmlDocumentNavigator_t1264434787 * __this, XmlNode_t856910923 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentNavigator::System.Xml.IHasXmlNode.GetNode()
extern "C"  XmlNode_t856910923 * XmlDocumentNavigator_System_Xml_IHasXmlNode_GetNode_m702689740 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument System.Xml.XmlDocumentNavigator::get_Document()
extern "C"  XmlDocument_t730752740 * XmlDocumentNavigator_get_Document_m7060077 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::get_HasAttributes()
extern "C"  bool XmlDocumentNavigator_get_HasAttributes_m1058923188 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::get_HasChildren()
extern "C"  bool XmlDocumentNavigator_get_HasChildren_m1307370588 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlAttribute System.Xml.XmlDocumentNavigator::get_NsNode()
extern "C"  XmlAttribute_t6647939 * XmlDocumentNavigator_get_NsNode_m3885064662 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlDocumentNavigator::set_NsNode(System.Xml.XmlAttribute)
extern "C"  void XmlDocumentNavigator_set_NsNode_m1101437653 (XmlDocumentNavigator_t1264434787 * __this, XmlAttribute_t6647939 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::get_LocalName()
extern "C"  String_t* XmlDocumentNavigator_get_LocalName_m2834513940 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::get_Name()
extern "C"  String_t* XmlDocumentNavigator_get_Name_m2331072175 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::get_NamespaceURI()
extern "C"  String_t* XmlDocumentNavigator_get_NamespaceURI_m2500506037 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeType System.Xml.XmlDocumentNavigator::get_NodeType()
extern "C"  int32_t XmlDocumentNavigator_get_NodeType_m506592452 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::get_Value()
extern "C"  String_t* XmlDocumentNavigator_get_Value_m2053467311 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::get_XmlLang()
extern "C"  String_t* XmlDocumentNavigator_get_XmlLang_m935699875 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::CheckNsNameAppearance(System.String,System.String)
extern "C"  bool XmlDocumentNavigator_CheckNsNameAppearance_m1065663830 (XmlDocumentNavigator_t1264434787 * __this, String_t* ___name0, String_t* ___ns1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XmlDocumentNavigator::Clone()
extern "C"  XPathNavigator_t1075073278 * XmlDocumentNavigator_Clone_m1863322131 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentNavigator_IsDescendant_m1971976468 (XmlDocumentNavigator_t1264434787 * __this, XPathNavigator_t1075073278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentNavigator_IsSamePosition_m590655586 (XmlDocumentNavigator_t1264434787 * __this, XPathNavigator_t1075073278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveTo(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentNavigator_MoveTo_m3255669135 (XmlDocumentNavigator_t1264434787 * __this, XPathNavigator_t1075073278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlDocumentNavigator_MoveToAttribute_m960786274 (XmlDocumentNavigator_t1264434787 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToFirstAttribute()
extern "C"  bool XmlDocumentNavigator_MoveToFirstAttribute_m230671598 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToFirstChild()
extern "C"  bool XmlDocumentNavigator_MoveToFirstChild_m2434809838 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentNavigator_MoveToFirstNamespace_m2538849662 (XmlDocumentNavigator_t1264434787 * __this, int32_t ___namespaceScope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToId(System.String)
extern "C"  bool XmlDocumentNavigator_MoveToId_m3869498405 (XmlDocumentNavigator_t1264434787 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToNamespace(System.String)
extern "C"  bool XmlDocumentNavigator_MoveToNamespace_m1631699207 (XmlDocumentNavigator_t1264434787 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToNext()
extern "C"  bool XmlDocumentNavigator_MoveToNext_m672950677 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToNextAttribute()
extern "C"  bool XmlDocumentNavigator_MoveToNextAttribute_m1929028201 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentNavigator_MoveToNextNamespace_m3133587833 (XmlDocumentNavigator_t1264434787 * __this, int32_t ___namespaceScope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlDocumentNavigator::MoveToParent()
extern "C"  bool XmlDocumentNavigator_MoveToParent_m2212643148 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlDocumentNavigator::MoveToRoot()
extern "C"  void XmlDocumentNavigator_MoveToRoot_m1162910924 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentNavigator::get_Node()
extern "C"  XmlNode_t856910923 * XmlDocumentNavigator_get_Node_m659530107 (XmlDocumentNavigator_t1264434787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentNavigator::GetFirstChild(System.Xml.XmlNode)
extern "C"  XmlNode_t856910923 * XmlDocumentNavigator_GetFirstChild_m1951301969 (XmlDocumentNavigator_t1264434787 * __this, XmlNode_t856910923 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentNavigator::GetNextSibling(System.Xml.XmlNode)
extern "C"  XmlNode_t856910923 * XmlDocumentNavigator_GetNextSibling_m1065547366 (XmlDocumentNavigator_t1264434787 * __this, XmlNode_t856910923 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentNavigator::GetParentNode(System.Xml.XmlNode)
extern "C"  XmlNode_t856910923 * XmlDocumentNavigator_GetParentNode_m1303520977 (XmlDocumentNavigator_t1264434787 * __this, XmlNode_t856910923 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentNavigator::LookupNamespace(System.String)
extern "C"  String_t* XmlDocumentNavigator_LookupNamespace_m2129303642 (XmlDocumentNavigator_t1264434787 * __this, String_t* ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
