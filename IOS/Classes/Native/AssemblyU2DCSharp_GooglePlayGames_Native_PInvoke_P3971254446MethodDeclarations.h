﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerManager/<FetchFriends>c__AnonStoreyA5
struct U3CFetchFriendsU3Ec__AnonStoreyA5_t3971254446;
// GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse
struct FetchListResponse_t676237491;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Pl676237491.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/<FetchFriends>c__AnonStoreyA5::.ctor()
extern "C"  void U3CFetchFriendsU3Ec__AnonStoreyA5__ctor_m2165191597 (U3CFetchFriendsU3Ec__AnonStoreyA5_t3971254446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerManager/<FetchFriends>c__AnonStoreyA5::<>m__CA(GooglePlayGames.Native.PInvoke.PlayerManager/FetchListResponse)
extern "C"  void U3CFetchFriendsU3Ec__AnonStoreyA5_U3CU3Em__CA_m1652292737 (U3CFetchFriendsU3Ec__AnonStoreyA5_t3971254446 * __this, FetchListResponse_t676237491 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
