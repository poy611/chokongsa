﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableBehaviour
struct EnableBehaviour_t2067166088;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::.ctor()
extern "C"  void EnableBehaviour__ctor_m2264218798 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::Reset()
extern "C"  void EnableBehaviour_Reset_m4205619035 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnEnter()
extern "C"  void EnableBehaviour_OnEnter_m2052541701 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::DoEnableBehaviour(UnityEngine.GameObject)
extern "C"  void EnableBehaviour_DoEnableBehaviour_m1719342387 (EnableBehaviour_t2067166088 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnExit()
extern "C"  void EnableBehaviour_OnExit_m2984624627 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.EnableBehaviour::ErrorCheck()
extern "C"  String_t* EnableBehaviour_ErrorCheck_m2524918515 (EnableBehaviour_t2067166088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
