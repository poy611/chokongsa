﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau362240250.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"

// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool DictionaryEnumerator_2_MoveNext_m3181297203_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_MoveNext_m3181297203(__this, method) ((  bool (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_MoveNext_m3181297203_gshared)(__this, method)
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::Reset()
extern "C"  void DictionaryEnumerator_2_Reset_m3656227480_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_Reset_m3656227480(__this, method) ((  void (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_Reset_m3656227480_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1944668977  DictionaryEnumerator_2_get_Current_m739694543_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Current_m739694543(__this, method) ((  KeyValuePair_2_t1944668977  (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_get_Current_m739694543_gshared)(__this, method)
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::Dispose()
extern "C"  void DictionaryEnumerator_2_Dispose_m2874789224_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_Dispose_m2874789224(__this, method) ((  void (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_Dispose_m2874789224_gshared)(__this, method)
// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_System_Collections_IEnumerator_get_Current_m1113041243_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* DictionaryEnumerator_2_GetEnumerator_m2641693378_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_GetEnumerator_m2641693378(__this, method) ((  Il2CppObject* (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_GetEnumerator_m2641693378_gshared)(__this, method)
// System.Collections.IEnumerator Newtonsoft.Json.Serialization.DefaultContractResolver/DictionaryEnumerator`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304_gshared (DictionaryEnumerator_2_t362240250 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t362240250 *, const MethodInfo*))DictionaryEnumerator_2_System_Collections_IEnumerable_GetEnumerator_m3844118304_gshared)(__this, method)
