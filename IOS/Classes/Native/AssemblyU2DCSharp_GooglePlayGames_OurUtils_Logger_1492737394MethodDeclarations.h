﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C
struct U3CeU3Ec__AnonStorey3C_t1492737394;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C::.ctor()
extern "C"  void U3CeU3Ec__AnonStorey3C__ctor_m1047741289 (U3CeU3Ec__AnonStorey3C_t1492737394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger/<e>c__AnonStorey3C::<>m__B()
extern "C"  void U3CeU3Ec__AnonStorey3C_U3CU3Em__B_m2351130546 (U3CeU3Ec__AnonStorey3C_t1492737394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
