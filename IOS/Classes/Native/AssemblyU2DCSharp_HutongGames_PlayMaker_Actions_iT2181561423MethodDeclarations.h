﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateFrom
struct iTweenRotateFrom_t2181561423;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::.ctor()
extern "C"  void iTweenRotateFrom__ctor_m2614513399 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::Reset()
extern "C"  void iTweenRotateFrom_Reset_m260946340 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnEnter()
extern "C"  void iTweenRotateFrom_OnEnter_m3678204174 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnExit()
extern "C"  void iTweenRotateFrom_OnExit_m958855370 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::DoiTween()
extern "C"  void iTweenRotateFrom_DoiTween_m3806311930 (iTweenRotateFrom_t2181561423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
