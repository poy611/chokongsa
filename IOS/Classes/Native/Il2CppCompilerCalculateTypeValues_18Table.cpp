﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2340202136.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2281118987.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ParserTi1773097576.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_DateTime1855814826.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_DateTimeU123347860.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_EnumUtil1026533742.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_EnumUtil2239738249.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_BufferUt3931562959.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_JavaScri3297894178.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_JsonToke1895433808.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun1043944210.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun1191579649.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_MathUtil1534419911.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Miscella2900693491.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_PropertyN815898604.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Property1172375224.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1590616920.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3070212469.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3124613658.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1733456158.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1733456159.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1733456160.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3905956676.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti2260686943.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3872426020.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringBu1402275585.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRe1751946808.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRe1848410412.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringUt3446198942.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeExten889461758.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Validati3410775990.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau822678628.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa3086838335.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1701094040.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1365682496.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1365682497.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1365682498.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1365771869.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1365772830.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa3080038812.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa3048163941.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_ErrorC18794611.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Error792639131.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonA145179369.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3227540113.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1724085472.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seria799500859.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seri1476028681.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exte1258269582.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exten914719770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2259160450.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2259161411.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonD989352188.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4063403306.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonI624170136.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1458265766.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonO505348133.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP554063095.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP717767559.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2068678036.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1128564349.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3659144454.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3090945776.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3751806025.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4039940575.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3814549686.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3893567258.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4087007991.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4005035940.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2221153067.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3898342781.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_OnEr1073380767.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Refl1962998760.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Refl3101200061.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ConvertResult_t2340202136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	ConvertResult_t2340202136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (U3CU3Ec__DisplayClass9_0_t2281118987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	U3CU3Ec__DisplayClass9_0_t2281118987::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ParserTimeZone_t1773097576)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1802[5] = 
{
	ParserTimeZone_t1773097576::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (DateTimeParser_t1855814826)+ sizeof (Il2CppObject), sizeof(DateTimeParser_t1855814826_marshaled_pinvoke), sizeof(DateTimeParser_t1855814826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[26] = 
{
	DateTimeParser_t1855814826::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of__text_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826::get_offset_of__end_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t1855814826_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (DateTimeUtils_t123347860), -1, sizeof(DateTimeUtils_t123347860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[3] = 
{
	DateTimeUtils_t123347860_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_t123347860_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_t123347860_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (EnumUtils_t1026533742), -1, sizeof(EnumUtils_t1026533742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	EnumUtils_t1026533742_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CU3Ec_t2239738249), -1, sizeof(U3CU3Ec_t2239738249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1810[3] = 
{
	U3CU3Ec_t2239738249_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2239738249_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_t2239738249_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (BufferUtils_t3931562959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (JavaScriptUtils_t3297894178), -1, sizeof(JavaScriptUtils_t3297894178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[3] = 
{
	JavaScriptUtils_t3297894178_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_t3297894178_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_t3297894178_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (JsonTokenUtils_t1895433808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (LateBoundReflectionDelegateFactory_t1043944210), -1, sizeof(LateBoundReflectionDelegateFactory_t1043944210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	LateBoundReflectionDelegateFactory_t1043944210_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (U3CU3Ec__DisplayClass3_0_t1191579649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[2] = 
{
	U3CU3Ec__DisplayClass3_0_t1191579649::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_t1191579649::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (MathUtils_t1534419911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (MiscellaneousUtils_t2900693491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (PropertyNameTable_t815898604), -1, sizeof(PropertyNameTable_t815898604_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	PropertyNameTable_t815898604_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_t815898604::get_offset_of__count_1(),
	PropertyNameTable_t815898604::get_offset_of__entries_2(),
	PropertyNameTable_t815898604::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Entry_t1172375224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[3] = 
{
	Entry_t1172375224::get_offset_of_Value_0(),
	Entry_t1172375224::get_offset_of_HashCode_1(),
	Entry_t1172375224::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ReflectionDelegateFactory_t1590616920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (ReflectionMember_t3070212469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[3] = 
{
	ReflectionMember_t3070212469::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_t3070212469::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_t3070212469::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (ReflectionObject_t3124613658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[2] = 
{
	ReflectionObject_t3124613658::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t3124613658::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (U3CU3Ec__DisplayClass13_0_t1733456158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1733456158::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (U3CU3Ec__DisplayClass13_1_t1733456159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[1] = 
{
	U3CU3Ec__DisplayClass13_1_t1733456159::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (U3CU3Ec__DisplayClass13_2_t1733456160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[1] = 
{
	U3CU3Ec__DisplayClass13_2_t1733456160::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ReflectionUtils_t3905956676), -1, sizeof(ReflectionUtils_t3905956676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[1] = 
{
	ReflectionUtils_t3905956676_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (U3CU3Ec_t2260686943), -1, sizeof(U3CU3Ec_t2260686943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[5] = 
{
	U3CU3Ec_t2260686943_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2260686943_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_t2260686943_StaticFields::get_offset_of_U3CU3E9__29_0_2(),
	U3CU3Ec_t2260686943_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_t2260686943_StaticFields::get_offset_of_U3CU3E9__39_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (U3CU3Ec__DisplayClass42_0_t3872426020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	U3CU3Ec__DisplayClass42_0_t3872426020::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (StringBuffer_t1402275585)+ sizeof (Il2CppObject), sizeof(StringBuffer_t1402275585_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[2] = 
{
	StringBuffer_t1402275585::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringBuffer_t1402275585::get_offset_of__position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (StringReference_t1751946808)+ sizeof (Il2CppObject), sizeof(StringReference_t1751946808_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[3] = 
{
	StringReference_t1751946808::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringReference_t1751946808::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringReference_t1751946808::get_offset_of__length_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (StringReferenceExtensions_t1848410412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (StringUtils_t3446198942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (TypeExtensions_t889461758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (ValidationUtils_t3410775990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (ResolverContractKey_t473801005)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[2] = 
{
	ResolverContractKey_t473801005::get_offset_of__resolverType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResolverContractKey_t473801005::get_offset_of__contractType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (DefaultContractResolverState_t822678628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	DefaultContractResolverState_t822678628::get_offset_of_ContractCache_0(),
	DefaultContractResolverState_t822678628::get_offset_of_NameTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (DefaultContractResolver_t3086838335), -1, sizeof(DefaultContractResolver_t3086838335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1847[10] = 
{
	DefaultContractResolver_t3086838335_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_t3086838335_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_t3086838335_StaticFields::get_offset_of_TypeContractCacheLock_2(),
	DefaultContractResolver_t3086838335_StaticFields::get_offset_of__sharedState_3(),
	DefaultContractResolver_t3086838335::get_offset_of__instanceState_4(),
	DefaultContractResolver_t3086838335::get_offset_of__sharedCache_5(),
	DefaultContractResolver_t3086838335::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(),
	DefaultContractResolver_t3086838335::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(),
	DefaultContractResolver_t3086838335::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(),
	DefaultContractResolver_t3086838335::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (U3CU3Ec_t1701094040), -1, sizeof(U3CU3Ec_t1701094040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[7] = 
{
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__30_1_2(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__33_0_3(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__33_1_4(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__36_0_5(),
	U3CU3Ec_t1701094040_StaticFields::get_offset_of_U3CU3E9__60_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (U3CU3Ec__DisplayClass34_0_t1365682496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	U3CU3Ec__DisplayClass34_0_t1365682496::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass34_0_t1365682496::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (U3CU3Ec__DisplayClass34_1_t1365682497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[4] = 
{
	U3CU3Ec__DisplayClass34_1_t1365682497::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass34_1_t1365682497::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass34_1_t1365682497::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass34_1_t1365682497::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (U3CU3Ec__DisplayClass34_2_t1365682498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[2] = 
{
	U3CU3Ec__DisplayClass34_2_t1365682498::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass34_2_t1365682498::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (U3CU3Ec__DisplayClass64_0_t1365771869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[1] = 
{
	U3CU3Ec__DisplayClass64_0_t1365771869::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (U3CU3Ec__DisplayClass65_0_t1365772830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[1] = 
{
	U3CU3Ec__DisplayClass65_0_t1365772830::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (DefaultReferenceResolver_t3080038812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	DefaultReferenceResolver_t3080038812::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (DefaultSerializationBinder_t3048163941), -1, sizeof(DefaultSerializationBinder_t3048163941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1856[2] = 
{
	DefaultSerializationBinder_t3048163941_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_t3048163941::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (TypeNameKey_t2971844791)+ sizeof (Il2CppObject), sizeof(TypeNameKey_t2971844791_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[2] = 
{
	TypeNameKey_t2971844791::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeNameKey_t2971844791::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (ErrorContext_t18794611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[6] = 
{
	ErrorContext_t18794611::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_t18794611::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_t18794611::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_t18794611::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_t18794611::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_t18794611::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (ErrorEventArgs_t792639131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[2] = 
{
	ErrorEventArgs_t792639131::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_t792639131::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (JsonArrayContract_t145179369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[13] = 
{
	JsonArrayContract_t145179369::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t145179369::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t145179369::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t145179369::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t145179369::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t145179369::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t145179369::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t145179369::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t145179369::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t145179369::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t145179369::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t145179369::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t145179369::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (JsonContainerContract_t3227540113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[6] = 
{
	JsonContainerContract_t3227540113::get_offset_of__itemContract_21(),
	JsonContainerContract_t3227540113::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t3227540113::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t3227540113::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t3227540113::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t3227540113::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (JsonContractType_t1724085472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[10] = 
{
	JsonContractType_t1724085472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (SerializationCallback_t799500859), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (SerializationErrorCallback_t1476028681), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (ExtensionDataSetter_t1258269582), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (ExtensionDataGetter_t914719770), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (JsonContract_t1328848902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[21] = 
{
	JsonContract_t1328848902::get_offset_of_IsNullable_0(),
	JsonContract_t1328848902::get_offset_of_IsConvertable_1(),
	JsonContract_t1328848902::get_offset_of_IsEnum_2(),
	JsonContract_t1328848902::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_t1328848902::get_offset_of_InternalReadType_4(),
	JsonContract_t1328848902::get_offset_of_ContractType_5(),
	JsonContract_t1328848902::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_t1328848902::get_offset_of_IsSealed_7(),
	JsonContract_t1328848902::get_offset_of_IsInstantiable_8(),
	JsonContract_t1328848902::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_t1328848902::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_t1328848902::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_t1328848902::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_t1328848902::get_offset_of__onErrorCallbacks_13(),
	JsonContract_t1328848902::get_offset_of__createdType_14(),
	JsonContract_t1328848902::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_t1328848902::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_t1328848902::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_t1328848902::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_t1328848902::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_t1328848902::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U3CU3Ec__DisplayClass73_0_t2259160450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	U3CU3Ec__DisplayClass73_0_t2259160450::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CU3Ec__DisplayClass74_0_t2259161411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[1] = 
{
	U3CU3Ec__DisplayClass74_0_t2259161411::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (JsonDictionaryContract_t989352188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[13] = 
{
	JsonDictionaryContract_t989352188::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_t989352188::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_t989352188::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_t989352188::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_t989352188::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_t989352188::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_t989352188::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_t989352188::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_t989352188::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_t989352188::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_t989352188::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_t989352188::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_t989352188::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (JsonFormatterConverter_t4063403306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[3] = 
{
	JsonFormatterConverter_t4063403306::get_offset_of__reader_0(),
	JsonFormatterConverter_t4063403306::get_offset_of__contract_1(),
	JsonFormatterConverter_t4063403306::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (JsonISerializableContract_t624170136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	JsonISerializableContract_t624170136::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (JsonLinqContract_t1458265766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (JsonObjectContract_t505348133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[13] = 
{
	JsonObjectContract_t505348133::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_t505348133::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_t505348133::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_t505348133::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_t505348133::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_t505348133::get_offset_of_ExtensionDataIsJToken_32(),
	JsonObjectContract_t505348133::get_offset_of__hasRequiredOrDefaultValueProperties_33(),
	JsonObjectContract_t505348133::get_offset_of__parametrizedConstructor_34(),
	JsonObjectContract_t505348133::get_offset_of__overrideConstructor_35(),
	JsonObjectContract_t505348133::get_offset_of__overrideCreator_36(),
	JsonObjectContract_t505348133::get_offset_of__parameterizedCreator_37(),
	JsonObjectContract_t505348133::get_offset_of__creatorParameters_38(),
	JsonObjectContract_t505348133::get_offset_of__extensionDataValueType_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (JsonPrimitiveContract_t554063095), -1, sizeof(JsonPrimitiveContract_t554063095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1880[2] = 
{
	JsonPrimitiveContract_t554063095::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_t554063095_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (JsonProperty_t902655177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[33] = 
{
	JsonProperty_t902655177::get_offset_of__required_0(),
	JsonProperty_t902655177::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_t902655177::get_offset_of__defaultValue_2(),
	JsonProperty_t902655177::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_t902655177::get_offset_of__propertyName_4(),
	JsonProperty_t902655177::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_t902655177::get_offset_of__propertyType_6(),
	JsonProperty_t902655177::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_t902655177::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_t902655177::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_t902655177::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_t902655177::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_t902655177::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_t902655177::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_t902655177::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_t902655177::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_t902655177::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_t902655177::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_t902655177::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_t902655177::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_t902655177::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_t902655177::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_t902655177::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_t902655177::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_t902655177::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_t902655177::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_t902655177::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_t902655177::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_t902655177::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_t902655177::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_t902655177::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_t902655177::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_t902655177::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (JsonPropertyCollection_t717767559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[2] = 
{
	JsonPropertyCollection_t717767559::get_offset_of__type_5(),
	JsonPropertyCollection_t717767559::get_offset_of__list_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (JsonSerializerInternalBase_t2068678036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[5] = 
{
	JsonSerializerInternalBase_t2068678036::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t2068678036::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t2068678036::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_t2068678036::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_t2068678036::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ReferenceEqualsEqualityComparer_t1128564349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (JsonSerializerInternalReader_t3659144454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (PropertyPresence_t2892329490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[4] = 
{
	PropertyPresence_t2892329490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (CreatorPropertyContext_t3090945776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[6] = 
{
	CreatorPropertyContext_t3090945776::get_offset_of_Name_0(),
	CreatorPropertyContext_t3090945776::get_offset_of_Property_1(),
	CreatorPropertyContext_t3090945776::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t3090945776::get_offset_of_Presence_3(),
	CreatorPropertyContext_t3090945776::get_offset_of_Value_4(),
	CreatorPropertyContext_t3090945776::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (U3CU3Ec__DisplayClass36_0_t3751806025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	U3CU3Ec__DisplayClass36_0_t3751806025::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (U3CU3Ec_t4039940575), -1, sizeof(U3CU3Ec_t4039940575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1889[5] = 
{
	U3CU3Ec_t4039940575_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4039940575_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t4039940575_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t4039940575_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t4039940575_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (JsonSerializerInternalWriter_t3814549686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[3] = 
{
	JsonSerializerInternalWriter_t3814549686::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_t3814549686::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_t3814549686::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (JsonSerializerProxy_t3893567258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[3] = 
{
	JsonSerializerProxy_t3893567258::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t3893567258::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t3893567258::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (JsonStringContract_t4087007991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (JsonTypeReflector_t4005035940), -1, sizeof(JsonTypeReflector_t4005035940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1893[4] = 
{
	JsonTypeReflector_t4005035940_StaticFields::get_offset_of__fullyTrusted_0(),
	JsonTypeReflector_t4005035940_StaticFields::get_offset_of_JsonConverterCreatorCache_1(),
	JsonTypeReflector_t4005035940_StaticFields::get_offset_of_AssociatedMetadataTypesCache_2(),
	JsonTypeReflector_t4005035940_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3CU3Ec__DisplayClass18_0_t2221153067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[2] = 
{
	U3CU3Ec__DisplayClass18_0_t2221153067::get_offset_of_converterType_0(),
	U3CU3Ec__DisplayClass18_0_t2221153067::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (U3CU3Ec_t3898342781), -1, sizeof(U3CU3Ec_t3898342781_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[2] = 
{
	U3CU3Ec_t3898342781_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3898342781_StaticFields::get_offset_of_U3CU3E9__18_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (OnErrorAttribute_t1073380767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ReflectionAttributeProvider_t1962998760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[1] = 
{
	ReflectionAttributeProvider_t1962998760::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (ReflectionValueProvider_t3101200061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	ReflectionValueProvider_t3101200061::get_offset_of__memberInfo_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
