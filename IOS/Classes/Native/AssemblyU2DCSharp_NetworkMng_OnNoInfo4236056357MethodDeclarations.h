﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NetworkMng/OnNoInfo
struct OnNoInfo_t4236056357;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void NetworkMng/OnNoInfo::.ctor(System.Object,System.IntPtr)
extern "C"  void OnNoInfo__ctor_m3719063164 (OnNoInfo_t4236056357 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnNoInfo::Invoke()
extern "C"  void OnNoInfo_Invoke_m889861590 (OnNoInfo_t4236056357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NetworkMng/OnNoInfo::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnNoInfo_BeginInvoke_m3282947829 (OnNoInfo_t4236056357 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NetworkMng/OnNoInfo::EndInvoke(System.IAsyncResult)
extern "C"  void OnNoInfo_EndInvoke_m4233000588 (OnNoInfo_t4236056357 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
