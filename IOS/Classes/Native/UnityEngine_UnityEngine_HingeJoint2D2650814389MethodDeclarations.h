﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HingeJoint2D
struct HingeJoint2D_t2650814389;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"
#include "UnityEngine_UnityEngine_JointAngleLimits2D2258250679.h"

// System.Void UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)
extern "C"  void HingeJoint2D_set_useMotor_m1698524074 (HingeJoint2D_t2650814389 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
extern "C"  void HingeJoint2D_set_useLimits_m2925139955 (HingeJoint2D_t2650814389 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor2D UnityEngine.HingeJoint2D::get_motor()
extern "C"  JointMotor2D_t682576033  HingeJoint2D_get_motor_m3950828011 (HingeJoint2D_t2650814389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void HingeJoint2D_set_motor_m3348642590 (HingeJoint2D_t2650814389 * __this, JointMotor2D_t682576033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void HingeJoint2D_INTERNAL_get_motor_m46951694 (HingeJoint2D_t2650814389 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void HingeJoint2D_INTERNAL_set_motor_m1975718274 (HingeJoint2D_t2650814389 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointAngleLimits2D UnityEngine.HingeJoint2D::get_limits()
extern "C"  JointAngleLimits2D_t2258250679  HingeJoint2D_get_limits_m1369141678 (HingeJoint2D_t2650814389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_limits(UnityEngine.JointAngleLimits2D)
extern "C"  void HingeJoint2D_set_limits_m2122926515 (HingeJoint2D_t2650814389 * __this, JointAngleLimits2D_t2258250679  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_get_limits(UnityEngine.JointAngleLimits2D&)
extern "C"  void HingeJoint2D_INTERNAL_get_limits_m2773103577 (HingeJoint2D_t2650814389 * __this, JointAngleLimits2D_t2258250679 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_set_limits(UnityEngine.JointAngleLimits2D&)
extern "C"  void HingeJoint2D_INTERNAL_set_limits_m1683711205 (HingeJoint2D_t2650814389 * __this, JointAngleLimits2D_t2258250679 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
