﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayCompare
struct ArrayCompare_t3109678708;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::.ctor()
extern "C"  void ArrayCompare__ctor_m3602367090 (ArrayCompare_t3109678708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::Reset()
extern "C"  void ArrayCompare_Reset_m1248800031 (ArrayCompare_t3109678708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::OnEnter()
extern "C"  void ArrayCompare_OnEnter_m3817828809 (ArrayCompare_t3109678708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayCompare::DoSequenceEqual()
extern "C"  void ArrayCompare_DoSequenceEqual_m1872995320 (ArrayCompare_t3109678708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
