﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct KeyCollection_t1446515030;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>
struct Dictionary_2_t4114722875;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey>
struct IEnumerator_1_t588742544;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey[]
struct TypeNameKeyU5BU5D_t3177491086;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke434691633.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2171171377_gshared (KeyCollection_t1446515030 * __this, Dictionary_2_t4114722875 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2171171377(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1446515030 *, Dictionary_2_t4114722875 *, const MethodInfo*))KeyCollection__ctor_m2171171377_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m102206597_gshared (KeyCollection_t1446515030 * __this, TypeNameKey_t2971844791  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m102206597(__this, ___item0, method) ((  void (*) (KeyCollection_t1446515030 *, TypeNameKey_t2971844791 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m102206597_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2641432380_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2641432380(__this, method) ((  void (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2641432380_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3101734505_gshared (KeyCollection_t1446515030 * __this, TypeNameKey_t2971844791  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3101734505(__this, ___item0, method) ((  bool (*) (KeyCollection_t1446515030 *, TypeNameKey_t2971844791 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3101734505_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m361344526_gshared (KeyCollection_t1446515030 * __this, TypeNameKey_t2971844791  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m361344526(__this, ___item0, method) ((  bool (*) (KeyCollection_t1446515030 *, TypeNameKey_t2971844791 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m361344526_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1474697102_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1474697102(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1474697102_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1349122478_gshared (KeyCollection_t1446515030 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1349122478(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1446515030 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1349122478_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3257053629_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3257053629(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3257053629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2940614538_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2940614538(__this, method) ((  bool (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2940614538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1513374908_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1513374908(__this, method) ((  bool (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1513374908_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3235342318_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3235342318(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3235342318_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3093552358_gshared (KeyCollection_t1446515030 * __this, TypeNameKeyU5BU5D_t3177491086* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3093552358(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1446515030 *, TypeNameKeyU5BU5D_t3177491086*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3093552358_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::GetEnumerator()
extern "C"  Enumerator_t434691633  KeyCollection_GetEnumerator_m1860923699_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1860923699(__this, method) ((  Enumerator_t434691633  (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_GetEnumerator_m1860923699_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3755357302_gshared (KeyCollection_t1446515030 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3755357302(__this, method) ((  int32_t (*) (KeyCollection_t1446515030 *, const MethodInfo*))KeyCollection_get_Count_m3755357302_gshared)(__this, method)
