﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t1327932246;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C"  void Collider2D_set_isTrigger_m4005580269 (Collider2D_t1552025098 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t1743771669 * Collider2D_get_attachedRigidbody_m2908627162 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Collider2D::get_shapeCount()
extern "C"  int32_t Collider2D_get_shapeCount_m2388185620 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern "C"  PhysicsMaterial2D_t1327932246 * Collider2D_get_sharedMaterial_m2954504316 (Collider2D_t1552025098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
