﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.HostData[]
struct HostDataU5BU5D_t4021970803;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.String UnityEngine.MasterServer::get_ipAddress()
extern "C"  String_t* MasterServer_get_ipAddress_m3869729575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_ipAddress(System.String)
extern "C"  void MasterServer_set_ipAddress_m4189760396 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.MasterServer::get_port()
extern "C"  int32_t MasterServer_get_port_m1334239910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_port(System.Int32)
extern "C"  void MasterServer_set_port_m850451947 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::RequestHostList(System.String)
extern "C"  void MasterServer_RequestHostList_m2054420839 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.HostData[] UnityEngine.MasterServer::PollHostList()
extern "C"  HostDataU5BU5D_t4021970803* MasterServer_PollHostList_m3223787845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)
extern "C"  void MasterServer_RegisterHost_m2388323251 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, String_t* ___gameName1, String_t* ___comment2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::UnregisterHost()
extern "C"  void MasterServer_UnregisterHost_m864917440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::ClearHostList()
extern "C"  void MasterServer_ClearHostList_m2859464953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.MasterServer::get_updateRate()
extern "C"  int32_t MasterServer_get_updateRate_m3442346606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_updateRate(System.Int32)
extern "C"  void MasterServer_set_updateRate_m2854261427 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.MasterServer::get_dedicatedServer()
extern "C"  bool MasterServer_get_dedicatedServer_m2497971081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)
extern "C"  void MasterServer_set_dedicatedServer_m1888993138 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
