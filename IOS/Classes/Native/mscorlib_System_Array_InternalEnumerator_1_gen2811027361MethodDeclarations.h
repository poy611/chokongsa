﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2811027361.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"

// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3131686238_gshared (InternalEnumerator_1_t2811027361 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3131686238(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2811027361 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3131686238_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130(__this, method) ((  void (*) (InternalEnumerator_1_t2811027361 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1068896130_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2811027361 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3620372846_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3195323573_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3195323573(__this, method) ((  void (*) (InternalEnumerator_1_t2811027361 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3195323573_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1523763438_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1523763438(__this, method) ((  bool (*) (InternalEnumerator_1_t2811027361 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1523763438_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1744325285_gshared (InternalEnumerator_1_t2811027361 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1744325285(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2811027361 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1744325285_gshared)(__this, method)
