﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.UseGravity
struct UseGravity_t1437686831;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.UseGravity::.ctor()
extern "C"  void UseGravity__ctor_m1101905175 (UseGravity_t1437686831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::Reset()
extern "C"  void UseGravity_Reset_m3043305412 (UseGravity_t1437686831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::OnEnter()
extern "C"  void UseGravity_OnEnter_m1760646958 (UseGravity_t1437686831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::DoUseGravity()
extern "C"  void UseGravity_DoUseGravity_m2276263551 (UseGravity_t1437686831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
