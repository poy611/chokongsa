﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TempingPointer
struct TempingPointer_t2678149871;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void TempingPointer::.ctor()
extern "C"  void TempingPointer__ctor_m1650546316 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::Start()
extern "C"  void TempingPointer_Start_m597684108 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::FirstSelectButton()
extern "C"  void TempingPointer_FirstSelectButton_m102904008 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TempingPointer::MainCameraMove()
extern "C"  Il2CppObject * TempingPointer_MainCameraMove_m2703110703 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::Init()
extern "C"  void TempingPointer_Init_m1667348040 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::TempingMouseDown()
extern "C"  void TempingPointer_TempingMouseDown_m2262648689 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TempingPointer::TempingSlideUp()
extern "C"  Il2CppObject * TempingPointer_TempingSlideUp_m2215103486 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TempingPointer::TempingSlideDown()
extern "C"  Il2CppObject * TempingPointer_TempingSlideDown_m2220440517 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::TempingMouseUp()
extern "C"  void TempingPointer_TempingMouseUp_m624087722 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::Temping1Button()
extern "C"  void TempingPointer_Temping1Button_m2914373293 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TempingPointer::Temping2Button()
extern "C"  void TempingPointer_Temping2Button_m1106918830 (TempingPointer_t2678149871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
